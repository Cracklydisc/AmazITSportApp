package com.google.protobuf;

import com.google.protobuf.nano.CodedInputByteBufferNano;
import com.google.protobuf.nano.CodedOutputByteBufferNano;
import com.google.protobuf.nano.ExtendableMessageNano;
import com.google.protobuf.nano.InternalNano;
import com.google.protobuf.nano.WireFormatNano;
import java.io.IOException;

public interface DescriptorProtos {

    public static final class DescriptorProto extends ExtendableMessageNano<DescriptorProto> {
        private static volatile DescriptorProto[] _emptyArray;
        private int bitField0_;
        public EnumDescriptorProto[] enumType;
        public FieldDescriptorProto[] extension;
        public ExtensionRange[] extensionRange;
        public FieldDescriptorProto[] field;
        private String name_;
        public DescriptorProto[] nestedType;
        public MessageOptions options;

        public static final class ExtensionRange extends ExtendableMessageNano<ExtensionRange> {
            private static volatile ExtensionRange[] _emptyArray;
            private int bitField0_;
            private int end_;
            private int start_;

            public static ExtensionRange[] emptyArray() {
                if (_emptyArray == null) {
                    synchronized (InternalNano.LAZY_INIT_LOCK) {
                        if (_emptyArray == null) {
                            _emptyArray = new ExtensionRange[0];
                        }
                    }
                }
                return _emptyArray;
            }

            public ExtensionRange() {
                clear();
            }

            public ExtensionRange clear() {
                this.bitField0_ = 0;
                this.start_ = 0;
                this.end_ = 0;
                this.unknownFieldData = null;
                this.cachedSize = -1;
                return this;
            }

            public void writeTo(CodedOutputByteBufferNano output) throws IOException {
                if ((this.bitField0_ & 1) != 0) {
                    output.writeInt32(1, this.start_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    output.writeInt32(2, this.end_);
                }
                super.writeTo(output);
            }

            protected int computeSerializedSize() {
                int size = super.computeSerializedSize();
                if ((this.bitField0_ & 1) != 0) {
                    size += CodedOutputByteBufferNano.computeInt32Size(1, this.start_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    return size + CodedOutputByteBufferNano.computeInt32Size(2, this.end_);
                }
                return size;
            }

            public ExtensionRange mergeFrom(CodedInputByteBufferNano input) throws IOException {
                while (true) {
                    int tag = input.readTag();
                    switch (tag) {
                        case 0:
                            break;
                        case 8:
                            this.start_ = input.readInt32();
                            this.bitField0_ |= 1;
                            continue;
                        case 16:
                            this.end_ = input.readInt32();
                            this.bitField0_ |= 2;
                            continue;
                        default:
                            if (!storeUnknownField(input, tag)) {
                                break;
                            }
                            continue;
                    }
                    return this;
                }
            }
        }

        public static DescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new DescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public DescriptorProto() {
            clear();
        }

        public DescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.field = FieldDescriptorProto.emptyArray();
            this.extension = FieldDescriptorProto.emptyArray();
            this.nestedType = emptyArray();
            this.enumType = EnumDescriptorProto.emptyArray();
            this.extensionRange = ExtensionRange.emptyArray();
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if (this.field != null && this.field.length > 0) {
                for (FieldDescriptorProto element : this.field) {
                    if (element != null) {
                        output.writeMessage(2, element);
                    }
                }
            }
            if (this.nestedType != null && this.nestedType.length > 0) {
                for (DescriptorProto element2 : this.nestedType) {
                    if (element2 != null) {
                        output.writeMessage(3, element2);
                    }
                }
            }
            if (this.enumType != null && this.enumType.length > 0) {
                for (EnumDescriptorProto element3 : this.enumType) {
                    if (element3 != null) {
                        output.writeMessage(4, element3);
                    }
                }
            }
            if (this.extensionRange != null && this.extensionRange.length > 0) {
                for (ExtensionRange element4 : this.extensionRange) {
                    if (element4 != null) {
                        output.writeMessage(5, element4);
                    }
                }
            }
            if (this.extension != null && this.extension.length > 0) {
                for (FieldDescriptorProto element5 : this.extension) {
                    if (element5 != null) {
                        output.writeMessage(6, element5);
                    }
                }
            }
            if (this.options != null) {
                output.writeMessage(7, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if (this.field != null && this.field.length > 0) {
                for (FieldDescriptorProto element : this.field) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(2, element);
                    }
                }
            }
            if (this.nestedType != null && this.nestedType.length > 0) {
                for (DescriptorProto element2 : this.nestedType) {
                    if (element2 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(3, element2);
                    }
                }
            }
            if (this.enumType != null && this.enumType.length > 0) {
                for (EnumDescriptorProto element3 : this.enumType) {
                    if (element3 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(4, element3);
                    }
                }
            }
            if (this.extensionRange != null && this.extensionRange.length > 0) {
                for (ExtensionRange element4 : this.extensionRange) {
                    if (element4 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(5, element4);
                    }
                }
            }
            if (this.extension != null && this.extension.length > 0) {
                for (FieldDescriptorProto element5 : this.extension) {
                    if (element5 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(6, element5);
                    }
                }
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(7, this.options);
            }
            return size;
        }

        public DescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int arrayLength;
                int i;
                FieldDescriptorProto[] newArray;
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 18);
                        if (this.field == null) {
                            i = 0;
                        } else {
                            i = this.field.length;
                        }
                        newArray = new FieldDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.field, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new FieldDescriptorProto();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new FieldDescriptorProto();
                        input.readMessage(newArray[i]);
                        this.field = newArray;
                        continue;
                    case 26:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 26);
                        if (this.nestedType == null) {
                            i = 0;
                        } else {
                            i = this.nestedType.length;
                        }
                        DescriptorProto[] newArray2 = new DescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.nestedType, 0, newArray2, 0, i);
                        }
                        while (i < newArray2.length - 1) {
                            newArray2[i] = new DescriptorProto();
                            input.readMessage(newArray2[i]);
                            input.readTag();
                            i++;
                        }
                        newArray2[i] = new DescriptorProto();
                        input.readMessage(newArray2[i]);
                        this.nestedType = newArray2;
                        continue;
                    case 34:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 34);
                        if (this.enumType == null) {
                            i = 0;
                        } else {
                            i = this.enumType.length;
                        }
                        EnumDescriptorProto[] newArray3 = new EnumDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.enumType, 0, newArray3, 0, i);
                        }
                        while (i < newArray3.length - 1) {
                            newArray3[i] = new EnumDescriptorProto();
                            input.readMessage(newArray3[i]);
                            input.readTag();
                            i++;
                        }
                        newArray3[i] = new EnumDescriptorProto();
                        input.readMessage(newArray3[i]);
                        this.enumType = newArray3;
                        continue;
                    case 42:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 42);
                        if (this.extensionRange == null) {
                            i = 0;
                        } else {
                            i = this.extensionRange.length;
                        }
                        ExtensionRange[] newArray4 = new ExtensionRange[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.extensionRange, 0, newArray4, 0, i);
                        }
                        while (i < newArray4.length - 1) {
                            newArray4[i] = new ExtensionRange();
                            input.readMessage(newArray4[i]);
                            input.readTag();
                            i++;
                        }
                        newArray4[i] = new ExtensionRange();
                        input.readMessage(newArray4[i]);
                        this.extensionRange = newArray4;
                        continue;
                    case 50:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 50);
                        if (this.extension == null) {
                            i = 0;
                        } else {
                            i = this.extension.length;
                        }
                        newArray = new FieldDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.extension, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new FieldDescriptorProto();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new FieldDescriptorProto();
                        input.readMessage(newArray[i]);
                        this.extension = newArray;
                        continue;
                    case 58:
                        if (this.options == null) {
                            this.options = new MessageOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class EnumDescriptorProto extends ExtendableMessageNano<EnumDescriptorProto> {
        private static volatile EnumDescriptorProto[] _emptyArray;
        private int bitField0_;
        private String name_;
        public EnumOptions options;
        public EnumValueDescriptorProto[] value;

        public static EnumDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new EnumDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public EnumDescriptorProto() {
            clear();
        }

        public EnumDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.value = EnumValueDescriptorProto.emptyArray();
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if (this.value != null && this.value.length > 0) {
                for (EnumValueDescriptorProto element : this.value) {
                    if (element != null) {
                        output.writeMessage(2, element);
                    }
                }
            }
            if (this.options != null) {
                output.writeMessage(3, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if (this.value != null && this.value.length > 0) {
                for (EnumValueDescriptorProto element : this.value) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(2, element);
                    }
                }
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(3, this.options);
            }
            return size;
        }

        public EnumDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 18);
                        if (this.value == null) {
                            i = 0;
                        } else {
                            i = this.value.length;
                        }
                        EnumValueDescriptorProto[] newArray = new EnumValueDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.value, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new EnumValueDescriptorProto();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new EnumValueDescriptorProto();
                        input.readMessage(newArray[i]);
                        this.value = newArray;
                        continue;
                    case 26:
                        if (this.options == null) {
                            this.options = new EnumOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class EnumOptions extends ExtendableMessageNano<EnumOptions> {
        private boolean allowAlias_;
        private int bitField0_;
        public UninterpretedOption[] uninterpretedOption;

        public EnumOptions() {
            clear();
        }

        public EnumOptions clear() {
            this.bitField0_ = 0;
            this.allowAlias_ = true;
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeBool(2, this.allowAlias_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(2, this.allowAlias_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public EnumOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 16:
                        this.allowAlias_ = input.readBool();
                        this.bitField0_ |= 1;
                        continue;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class EnumValueDescriptorProto extends ExtendableMessageNano<EnumValueDescriptorProto> {
        private static volatile EnumValueDescriptorProto[] _emptyArray;
        private int bitField0_;
        private String name_;
        private int number_;
        public EnumValueOptions options;

        public static EnumValueDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new EnumValueDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public EnumValueDescriptorProto() {
            clear();
        }

        public EnumValueDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.number_ = 0;
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeInt32(2, this.number_);
            }
            if (this.options != null) {
                output.writeMessage(3, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(2, this.number_);
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(3, this.options);
            }
            return size;
        }

        public EnumValueDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.number_ = input.readInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 26:
                        if (this.options == null) {
                            this.options = new EnumValueOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class EnumValueOptions extends ExtendableMessageNano<EnumValueOptions> {
        public UninterpretedOption[] uninterpretedOption;

        public EnumValueOptions() {
            clear();
        }

        public EnumValueOptions clear() {
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public EnumValueOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class FieldDescriptorProto extends ExtendableMessageNano<FieldDescriptorProto> {
        private static volatile FieldDescriptorProto[] _emptyArray;
        private int bitField0_;
        private String defaultValue_;
        private String extendee_;
        private int label_;
        private String name_;
        private int number_;
        public FieldOptions options;
        private String typeName_;
        private int type_;

        public static FieldDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new FieldDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public FieldDescriptorProto() {
            clear();
        }

        public FieldDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.number_ = 0;
            this.label_ = 1;
            this.type_ = 1;
            this.typeName_ = "";
            this.extendee_ = "";
            this.defaultValue_ = "";
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeString(2, this.extendee_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeInt32(3, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeInt32(4, this.label_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeInt32(5, this.type_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeString(6, this.typeName_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeString(7, this.defaultValue_);
            }
            if (this.options != null) {
                output.writeMessage(8, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(2, this.extendee_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(3, this.number_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(4, this.label_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(5, this.type_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(6, this.typeName_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(7, this.defaultValue_);
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(8, this.options);
            }
            return size;
        }

        public FieldDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int value;
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        this.extendee_ = input.readString();
                        this.bitField0_ |= 32;
                        continue;
                    case 24:
                        this.number_ = input.readInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 32:
                        value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 3:
                                this.label_ = value;
                                this.bitField0_ |= 4;
                                break;
                            default:
                                continue;
                        }
                    case 40:
                        value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                                this.type_ = value;
                                this.bitField0_ |= 8;
                                break;
                            default:
                                continue;
                        }
                    case 50:
                        this.typeName_ = input.readString();
                        this.bitField0_ |= 16;
                        continue;
                    case 58:
                        this.defaultValue_ = input.readString();
                        this.bitField0_ |= 64;
                        continue;
                    case 66:
                        if (this.options == null) {
                            this.options = new FieldOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class FieldOptions extends ExtendableMessageNano<FieldOptions> {
        private int bitField0_;
        private int ctype_;
        private boolean deprecated_;
        private String experimentalMapKey_;
        private boolean lazy_;
        private boolean packed_;
        public UninterpretedOption[] uninterpretedOption;
        private boolean weak_;

        public FieldOptions() {
            clear();
        }

        public FieldOptions clear() {
            this.bitField0_ = 0;
            this.ctype_ = 0;
            this.packed_ = false;
            this.lazy_ = false;
            this.deprecated_ = false;
            this.experimentalMapKey_ = "";
            this.weak_ = false;
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeInt32(1, this.ctype_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeBool(2, this.packed_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeBool(3, this.deprecated_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeBool(5, this.lazy_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeString(9, this.experimentalMapKey_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeBool(10, this.weak_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(1, this.ctype_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(2, this.packed_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(3, this.deprecated_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(5, this.lazy_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(9, this.experimentalMapKey_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(10, this.weak_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public FieldOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        int value = input.readInt32();
                        switch (value) {
                            case 0:
                            case 1:
                            case 2:
                                this.ctype_ = value;
                                this.bitField0_ |= 1;
                                break;
                            default:
                                continue;
                        }
                    case 16:
                        this.packed_ = input.readBool();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.deprecated_ = input.readBool();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.lazy_ = input.readBool();
                        this.bitField0_ |= 4;
                        continue;
                    case 74:
                        this.experimentalMapKey_ = input.readString();
                        this.bitField0_ |= 16;
                        continue;
                    case 80:
                        this.weak_ = input.readBool();
                        this.bitField0_ |= 32;
                        continue;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class FileDescriptorProto extends ExtendableMessageNano<FileDescriptorProto> {
        private static volatile FileDescriptorProto[] _emptyArray;
        private int bitField0_;
        public String[] dependency;
        public EnumDescriptorProto[] enumType;
        public FieldDescriptorProto[] extension;
        public DescriptorProto[] messageType;
        private String name_;
        public FileOptions options;
        private String package__;
        public int[] publicDependency;
        public ServiceDescriptorProto[] service;
        public SourceCodeInfo sourceCodeInfo;
        public int[] weakDependency;

        public static FileDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new FileDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public FileDescriptorProto() {
            clear();
        }

        public FileDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.package__ = "";
            this.dependency = WireFormatNano.EMPTY_STRING_ARRAY;
            this.publicDependency = WireFormatNano.EMPTY_INT_ARRAY;
            this.weakDependency = WireFormatNano.EMPTY_INT_ARRAY;
            this.messageType = DescriptorProto.emptyArray();
            this.enumType = EnumDescriptorProto.emptyArray();
            this.service = ServiceDescriptorProto.emptyArray();
            this.extension = FieldDescriptorProto.emptyArray();
            this.options = null;
            this.sourceCodeInfo = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeString(2, this.package__);
            }
            if (this.dependency != null && this.dependency.length > 0) {
                for (String element : this.dependency) {
                    if (element != null) {
                        output.writeString(3, element);
                    }
                }
            }
            if (this.messageType != null && this.messageType.length > 0) {
                for (DescriptorProto element2 : this.messageType) {
                    if (element2 != null) {
                        output.writeMessage(4, element2);
                    }
                }
            }
            if (this.enumType != null && this.enumType.length > 0) {
                for (EnumDescriptorProto element3 : this.enumType) {
                    if (element3 != null) {
                        output.writeMessage(5, element3);
                    }
                }
            }
            if (this.service != null && this.service.length > 0) {
                for (ServiceDescriptorProto element4 : this.service) {
                    if (element4 != null) {
                        output.writeMessage(6, element4);
                    }
                }
            }
            if (this.extension != null && this.extension.length > 0) {
                for (FieldDescriptorProto element5 : this.extension) {
                    if (element5 != null) {
                        output.writeMessage(7, element5);
                    }
                }
            }
            if (this.options != null) {
                output.writeMessage(8, this.options);
            }
            if (this.sourceCodeInfo != null) {
                output.writeMessage(9, this.sourceCodeInfo);
            }
            if (this.publicDependency != null && this.publicDependency.length > 0) {
                for (int writeInt32 : this.publicDependency) {
                    output.writeInt32(10, writeInt32);
                }
            }
            if (this.weakDependency != null && this.weakDependency.length > 0) {
                for (int writeInt322 : this.weakDependency) {
                    output.writeInt32(11, writeInt322);
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int dataSize;
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(2, this.package__);
            }
            if (this.dependency != null && this.dependency.length > 0) {
                int dataCount = 0;
                dataSize = 0;
                for (String element : this.dependency) {
                    if (element != null) {
                        dataCount++;
                        dataSize += CodedOutputByteBufferNano.computeStringSizeNoTag(element);
                    }
                }
                size = (size + dataSize) + (dataCount * 1);
            }
            if (this.messageType != null && this.messageType.length > 0) {
                for (DescriptorProto element2 : this.messageType) {
                    if (element2 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(4, element2);
                    }
                }
            }
            if (this.enumType != null && this.enumType.length > 0) {
                for (EnumDescriptorProto element3 : this.enumType) {
                    if (element3 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(5, element3);
                    }
                }
            }
            if (this.service != null && this.service.length > 0) {
                for (ServiceDescriptorProto element4 : this.service) {
                    if (element4 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(6, element4);
                    }
                }
            }
            if (this.extension != null && this.extension.length > 0) {
                for (FieldDescriptorProto element5 : this.extension) {
                    if (element5 != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(7, element5);
                    }
                }
            }
            if (this.options != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(8, this.options);
            }
            if (this.sourceCodeInfo != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(9, this.sourceCodeInfo);
            }
            if (this.publicDependency != null && this.publicDependency.length > 0) {
                dataSize = 0;
                for (int element6 : this.publicDependency) {
                    dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element6);
                }
                size = (size + dataSize) + (this.publicDependency.length * 1);
            }
            if (this.weakDependency == null || this.weakDependency.length <= 0) {
                return size;
            }
            dataSize = 0;
            for (int element62 : this.weakDependency) {
                dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element62);
            }
            return (size + dataSize) + (this.weakDependency.length * 1);
        }

        public FileDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int arrayLength;
                int i;
                int[] newArray;
                int limit;
                int startPos;
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        this.package__ = input.readString();
                        this.bitField0_ |= 2;
                        continue;
                    case 26:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 26);
                        i = this.dependency == null ? 0 : this.dependency.length;
                        String[] newArray2 = new String[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.dependency, 0, newArray2, 0, i);
                        }
                        while (i < newArray2.length - 1) {
                            newArray2[i] = input.readString();
                            input.readTag();
                            i++;
                        }
                        newArray2[i] = input.readString();
                        this.dependency = newArray2;
                        continue;
                    case 34:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 34);
                        if (this.messageType == null) {
                            i = 0;
                        } else {
                            i = this.messageType.length;
                        }
                        DescriptorProto[] newArray3 = new DescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.messageType, 0, newArray3, 0, i);
                        }
                        while (i < newArray3.length - 1) {
                            newArray3[i] = new DescriptorProto();
                            input.readMessage(newArray3[i]);
                            input.readTag();
                            i++;
                        }
                        newArray3[i] = new DescriptorProto();
                        input.readMessage(newArray3[i]);
                        this.messageType = newArray3;
                        continue;
                    case 42:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 42);
                        if (this.enumType == null) {
                            i = 0;
                        } else {
                            i = this.enumType.length;
                        }
                        EnumDescriptorProto[] newArray4 = new EnumDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.enumType, 0, newArray4, 0, i);
                        }
                        while (i < newArray4.length - 1) {
                            newArray4[i] = new EnumDescriptorProto();
                            input.readMessage(newArray4[i]);
                            input.readTag();
                            i++;
                        }
                        newArray4[i] = new EnumDescriptorProto();
                        input.readMessage(newArray4[i]);
                        this.enumType = newArray4;
                        continue;
                    case 50:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 50);
                        if (this.service == null) {
                            i = 0;
                        } else {
                            i = this.service.length;
                        }
                        ServiceDescriptorProto[] newArray5 = new ServiceDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.service, 0, newArray5, 0, i);
                        }
                        while (i < newArray5.length - 1) {
                            newArray5[i] = new ServiceDescriptorProto();
                            input.readMessage(newArray5[i]);
                            input.readTag();
                            i++;
                        }
                        newArray5[i] = new ServiceDescriptorProto();
                        input.readMessage(newArray5[i]);
                        this.service = newArray5;
                        continue;
                    case 58:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 58);
                        if (this.extension == null) {
                            i = 0;
                        } else {
                            i = this.extension.length;
                        }
                        FieldDescriptorProto[] newArray6 = new FieldDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.extension, 0, newArray6, 0, i);
                        }
                        while (i < newArray6.length - 1) {
                            newArray6[i] = new FieldDescriptorProto();
                            input.readMessage(newArray6[i]);
                            input.readTag();
                            i++;
                        }
                        newArray6[i] = new FieldDescriptorProto();
                        input.readMessage(newArray6[i]);
                        this.extension = newArray6;
                        continue;
                    case 66:
                        if (this.options == null) {
                            this.options = new FileOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    case 74:
                        if (this.sourceCodeInfo == null) {
                            this.sourceCodeInfo = new SourceCodeInfo();
                        }
                        input.readMessage(this.sourceCodeInfo);
                        continue;
                    case 80:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 80);
                        i = this.publicDependency == null ? 0 : this.publicDependency.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.publicDependency, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = input.readInt32();
                            input.readTag();
                            i++;
                        }
                        newArray[i] = input.readInt32();
                        this.publicDependency = newArray;
                        continue;
                    case 82:
                        limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readInt32();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.publicDependency == null ? 0 : this.publicDependency.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.publicDependency, 0, newArray, 0, i);
                        }
                        while (i < newArray.length) {
                            newArray[i] = input.readInt32();
                            i++;
                        }
                        this.publicDependency = newArray;
                        input.popLimit(limit);
                        continue;
                    case 88:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 88);
                        i = this.weakDependency == null ? 0 : this.weakDependency.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.weakDependency, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = input.readInt32();
                            input.readTag();
                            i++;
                        }
                        newArray[i] = input.readInt32();
                        this.weakDependency = newArray;
                        continue;
                    case 90:
                        limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readInt32();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.weakDependency == null ? 0 : this.weakDependency.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.weakDependency, 0, newArray, 0, i);
                        }
                        while (i < newArray.length) {
                            newArray[i] = input.readInt32();
                            i++;
                        }
                        this.weakDependency = newArray;
                        input.popLimit(limit);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class FileDescriptorSet extends ExtendableMessageNano<FileDescriptorSet> {
        public FileDescriptorProto[] file;

        public FileDescriptorSet() {
            clear();
        }

        public FileDescriptorSet clear() {
            this.file = FileDescriptorProto.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.file != null && this.file.length > 0) {
                for (FileDescriptorProto element : this.file) {
                    if (element != null) {
                        output.writeMessage(1, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.file != null && this.file.length > 0) {
                for (FileDescriptorProto element : this.file) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(1, element);
                    }
                }
            }
            return size;
        }

        public FileDescriptorSet mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 10);
                        if (this.file == null) {
                            i = 0;
                        } else {
                            i = this.file.length;
                        }
                        FileDescriptorProto[] newArray = new FileDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.file, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new FileDescriptorProto();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new FileDescriptorProto();
                        input.readMessage(newArray[i]);
                        this.file = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class FileOptions extends ExtendableMessageNano<FileOptions> {
        private int bitField0_;
        private boolean ccGenericServices_;
        private String goPackage_;
        private boolean javaGenerateEqualsAndHash_;
        private boolean javaGenericServices_;
        private boolean javaMultipleFiles_;
        private String javaOuterClassname_;
        private String javaPackage_;
        private int optimizeFor_;
        private boolean pyGenericServices_;
        public UninterpretedOption[] uninterpretedOption;

        public FileOptions() {
            clear();
        }

        public FileOptions clear() {
            this.bitField0_ = 0;
            this.javaPackage_ = "";
            this.javaOuterClassname_ = "";
            this.javaMultipleFiles_ = false;
            this.javaGenerateEqualsAndHash_ = false;
            this.optimizeFor_ = 1;
            this.goPackage_ = "";
            this.ccGenericServices_ = false;
            this.javaGenericServices_ = false;
            this.pyGenericServices_ = false;
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.javaPackage_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeString(8, this.javaOuterClassname_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeInt32(9, this.optimizeFor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeBool(10, this.javaMultipleFiles_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeString(11, this.goPackage_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeBool(16, this.ccGenericServices_);
            }
            if ((this.bitField0_ & 128) != 0) {
                output.writeBool(17, this.javaGenericServices_);
            }
            if ((this.bitField0_ & 256) != 0) {
                output.writeBool(18, this.pyGenericServices_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeBool(20, this.javaGenerateEqualsAndHash_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.javaPackage_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(8, this.javaOuterClassname_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(9, this.optimizeFor_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(10, this.javaMultipleFiles_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(11, this.goPackage_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(16, this.ccGenericServices_);
            }
            if ((this.bitField0_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(17, this.javaGenericServices_);
            }
            if ((this.bitField0_ & 256) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(18, this.pyGenericServices_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(20, this.javaGenerateEqualsAndHash_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public FileOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.javaPackage_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 66:
                        this.javaOuterClassname_ = input.readString();
                        this.bitField0_ |= 2;
                        continue;
                    case 72:
                        int value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 3:
                                this.optimizeFor_ = value;
                                this.bitField0_ |= 16;
                                break;
                            default:
                                continue;
                        }
                    case 80:
                        this.javaMultipleFiles_ = input.readBool();
                        this.bitField0_ |= 4;
                        continue;
                    case 90:
                        this.goPackage_ = input.readString();
                        this.bitField0_ |= 32;
                        continue;
                    case 128:
                        this.ccGenericServices_ = input.readBool();
                        this.bitField0_ |= 64;
                        continue;
                    case 136:
                        this.javaGenericServices_ = input.readBool();
                        this.bitField0_ |= 128;
                        continue;
                    case 144:
                        this.pyGenericServices_ = input.readBool();
                        this.bitField0_ |= 256;
                        continue;
                    case 160:
                        this.javaGenerateEqualsAndHash_ = input.readBool();
                        this.bitField0_ |= 8;
                        continue;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class MessageOptions extends ExtendableMessageNano<MessageOptions> {
        private int bitField0_;
        private boolean messageSetWireFormat_;
        private boolean noStandardDescriptorAccessor_;
        public UninterpretedOption[] uninterpretedOption;

        public MessageOptions() {
            clear();
        }

        public MessageOptions clear() {
            this.bitField0_ = 0;
            this.messageSetWireFormat_ = false;
            this.noStandardDescriptorAccessor_ = false;
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeBool(1, this.messageSetWireFormat_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeBool(2, this.noStandardDescriptorAccessor_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(1, this.messageSetWireFormat_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(2, this.noStandardDescriptorAccessor_);
            }
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public MessageOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.messageSetWireFormat_ = input.readBool();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.noStandardDescriptorAccessor_ = input.readBool();
                        this.bitField0_ |= 2;
                        continue;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class MethodDescriptorProto extends ExtendableMessageNano<MethodDescriptorProto> {
        private static volatile MethodDescriptorProto[] _emptyArray;
        private int bitField0_;
        private String inputType_;
        private String name_;
        public MethodOptions options;
        private String outputType_;

        public static MethodDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new MethodDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public MethodDescriptorProto() {
            clear();
        }

        public MethodDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.inputType_ = "";
            this.outputType_ = "";
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeString(2, this.inputType_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeString(3, this.outputType_);
            }
            if (this.options != null) {
                output.writeMessage(4, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(2, this.inputType_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(3, this.outputType_);
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(4, this.options);
            }
            return size;
        }

        public MethodDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        this.inputType_ = input.readString();
                        this.bitField0_ |= 2;
                        continue;
                    case 26:
                        this.outputType_ = input.readString();
                        this.bitField0_ |= 4;
                        continue;
                    case 34:
                        if (this.options == null) {
                            this.options = new MethodOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class MethodOptions extends ExtendableMessageNano<MethodOptions> {
        public UninterpretedOption[] uninterpretedOption;

        public MethodOptions() {
            clear();
        }

        public MethodOptions clear() {
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public MethodOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class ServiceDescriptorProto extends ExtendableMessageNano<ServiceDescriptorProto> {
        private static volatile ServiceDescriptorProto[] _emptyArray;
        private int bitField0_;
        public MethodDescriptorProto[] method;
        private String name_;
        public ServiceOptions options;

        public static ServiceDescriptorProto[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new ServiceDescriptorProto[0];
                    }
                }
            }
            return _emptyArray;
        }

        public ServiceDescriptorProto() {
            clear();
        }

        public ServiceDescriptorProto clear() {
            this.bitField0_ = 0;
            this.name_ = "";
            this.method = MethodDescriptorProto.emptyArray();
            this.options = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(1, this.name_);
            }
            if (this.method != null && this.method.length > 0) {
                for (MethodDescriptorProto element : this.method) {
                    if (element != null) {
                        output.writeMessage(2, element);
                    }
                }
            }
            if (this.options != null) {
                output.writeMessage(3, this.options);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(1, this.name_);
            }
            if (this.method != null && this.method.length > 0) {
                for (MethodDescriptorProto element : this.method) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(2, element);
                    }
                }
            }
            if (this.options != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(3, this.options);
            }
            return size;
        }

        public ServiceDescriptorProto mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        this.name_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 18:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 18);
                        if (this.method == null) {
                            i = 0;
                        } else {
                            i = this.method.length;
                        }
                        MethodDescriptorProto[] newArray = new MethodDescriptorProto[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.method, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new MethodDescriptorProto();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new MethodDescriptorProto();
                        input.readMessage(newArray[i]);
                        this.method = newArray;
                        continue;
                    case 26:
                        if (this.options == null) {
                            this.options = new ServiceOptions();
                        }
                        input.readMessage(this.options);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class ServiceOptions extends ExtendableMessageNano<ServiceOptions> {
        public UninterpretedOption[] uninterpretedOption;

        public ServiceOptions() {
            clear();
        }

        public ServiceOptions clear() {
            this.uninterpretedOption = UninterpretedOption.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        output.writeMessage(999, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.uninterpretedOption != null && this.uninterpretedOption.length > 0) {
                for (UninterpretedOption element : this.uninterpretedOption) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(999, element);
                    }
                }
            }
            return size;
        }

        public ServiceOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 7994:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 7994);
                        if (this.uninterpretedOption == null) {
                            i = 0;
                        } else {
                            i = this.uninterpretedOption.length;
                        }
                        UninterpretedOption[] newArray = new UninterpretedOption[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.uninterpretedOption, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new UninterpretedOption();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new UninterpretedOption();
                        input.readMessage(newArray[i]);
                        this.uninterpretedOption = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SourceCodeInfo extends ExtendableMessageNano<SourceCodeInfo> {
        public Location[] location;

        public static final class Location extends ExtendableMessageNano<Location> {
            private static volatile Location[] _emptyArray;
            private int bitField0_;
            private String leadingComments_;
            public int[] path;
            public int[] span;
            private String trailingComments_;

            public static Location[] emptyArray() {
                if (_emptyArray == null) {
                    synchronized (InternalNano.LAZY_INIT_LOCK) {
                        if (_emptyArray == null) {
                            _emptyArray = new Location[0];
                        }
                    }
                }
                return _emptyArray;
            }

            public Location() {
                clear();
            }

            public Location clear() {
                this.bitField0_ = 0;
                this.path = WireFormatNano.EMPTY_INT_ARRAY;
                this.span = WireFormatNano.EMPTY_INT_ARRAY;
                this.leadingComments_ = "";
                this.trailingComments_ = "";
                this.unknownFieldData = null;
                this.cachedSize = -1;
                return this;
            }

            public void writeTo(CodedOutputByteBufferNano output) throws IOException {
                int dataSize;
                if (this.path != null && this.path.length > 0) {
                    dataSize = 0;
                    for (int element : this.path) {
                        dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element);
                    }
                    output.writeRawVarint32(10);
                    output.writeRawVarint32(dataSize);
                    for (int writeInt32NoTag : this.path) {
                        output.writeInt32NoTag(writeInt32NoTag);
                    }
                }
                if (this.span != null && this.span.length > 0) {
                    dataSize = 0;
                    for (int element2 : this.span) {
                        dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element2);
                    }
                    output.writeRawVarint32(18);
                    output.writeRawVarint32(dataSize);
                    for (int writeInt32NoTag2 : this.span) {
                        output.writeInt32NoTag(writeInt32NoTag2);
                    }
                }
                if ((this.bitField0_ & 1) != 0) {
                    output.writeString(3, this.leadingComments_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    output.writeString(4, this.trailingComments_);
                }
                super.writeTo(output);
            }

            protected int computeSerializedSize() {
                int dataSize;
                int size = super.computeSerializedSize();
                if (this.path != null && this.path.length > 0) {
                    dataSize = 0;
                    for (int element : this.path) {
                        dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element);
                    }
                    size = ((size + dataSize) + 1) + CodedOutputByteBufferNano.computeRawVarint32Size(dataSize);
                }
                if (this.span != null && this.span.length > 0) {
                    dataSize = 0;
                    for (int element2 : this.span) {
                        dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element2);
                    }
                    size = ((size + dataSize) + 1) + CodedOutputByteBufferNano.computeRawVarint32Size(dataSize);
                }
                if ((this.bitField0_ & 1) != 0) {
                    size += CodedOutputByteBufferNano.computeStringSize(3, this.leadingComments_);
                }
                if ((this.bitField0_ & 2) != 0) {
                    return size + CodedOutputByteBufferNano.computeStringSize(4, this.trailingComments_);
                }
                return size;
            }

            public Location mergeFrom(CodedInputByteBufferNano input) throws IOException {
                while (true) {
                    int tag = input.readTag();
                    int arrayLength;
                    int i;
                    int[] newArray;
                    int limit;
                    int startPos;
                    switch (tag) {
                        case 0:
                            break;
                        case 8:
                            arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 8);
                            i = this.path == null ? 0 : this.path.length;
                            newArray = new int[(i + arrayLength)];
                            if (i != 0) {
                                System.arraycopy(this.path, 0, newArray, 0, i);
                            }
                            while (i < newArray.length - 1) {
                                newArray[i] = input.readInt32();
                                input.readTag();
                                i++;
                            }
                            newArray[i] = input.readInt32();
                            this.path = newArray;
                            continue;
                        case 10:
                            limit = input.pushLimit(input.readRawVarint32());
                            arrayLength = 0;
                            startPos = input.getPosition();
                            while (input.getBytesUntilLimit() > 0) {
                                input.readInt32();
                                arrayLength++;
                            }
                            input.rewindToPosition(startPos);
                            i = this.path == null ? 0 : this.path.length;
                            newArray = new int[(i + arrayLength)];
                            if (i != 0) {
                                System.arraycopy(this.path, 0, newArray, 0, i);
                            }
                            while (i < newArray.length) {
                                newArray[i] = input.readInt32();
                                i++;
                            }
                            this.path = newArray;
                            input.popLimit(limit);
                            continue;
                        case 16:
                            arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 16);
                            i = this.span == null ? 0 : this.span.length;
                            newArray = new int[(i + arrayLength)];
                            if (i != 0) {
                                System.arraycopy(this.span, 0, newArray, 0, i);
                            }
                            while (i < newArray.length - 1) {
                                newArray[i] = input.readInt32();
                                input.readTag();
                                i++;
                            }
                            newArray[i] = input.readInt32();
                            this.span = newArray;
                            continue;
                        case 18:
                            limit = input.pushLimit(input.readRawVarint32());
                            arrayLength = 0;
                            startPos = input.getPosition();
                            while (input.getBytesUntilLimit() > 0) {
                                input.readInt32();
                                arrayLength++;
                            }
                            input.rewindToPosition(startPos);
                            i = this.span == null ? 0 : this.span.length;
                            newArray = new int[(i + arrayLength)];
                            if (i != 0) {
                                System.arraycopy(this.span, 0, newArray, 0, i);
                            }
                            while (i < newArray.length) {
                                newArray[i] = input.readInt32();
                                i++;
                            }
                            this.span = newArray;
                            input.popLimit(limit);
                            continue;
                        case 26:
                            this.leadingComments_ = input.readString();
                            this.bitField0_ |= 1;
                            continue;
                        case 34:
                            this.trailingComments_ = input.readString();
                            this.bitField0_ |= 2;
                            continue;
                        default:
                            if (!storeUnknownField(input, tag)) {
                                break;
                            }
                            continue;
                    }
                    return this;
                }
            }
        }

        public SourceCodeInfo() {
            clear();
        }

        public SourceCodeInfo clear() {
            this.location = Location.emptyArray();
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.location != null && this.location.length > 0) {
                for (Location element : this.location) {
                    if (element != null) {
                        output.writeMessage(1, element);
                    }
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.location != null && this.location.length > 0) {
                for (Location element : this.location) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(1, element);
                    }
                }
            }
            return size;
        }

        public SourceCodeInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 10:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 10);
                        if (this.location == null) {
                            i = 0;
                        } else {
                            i = this.location.length;
                        }
                        Location[] newArray = new Location[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.location, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new Location();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new Location();
                        input.readMessage(newArray[i]);
                        this.location = newArray;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class UninterpretedOption extends ExtendableMessageNano<UninterpretedOption> {
        private static volatile UninterpretedOption[] _emptyArray;
        private String aggregateValue_;
        private int bitField0_;
        private double doubleValue_;
        private String identifierValue_;
        public NamePart[] name;
        private long negativeIntValue_;
        private long positiveIntValue_;
        private byte[] stringValue_;

        public static final class NamePart extends ExtendableMessageNano<NamePart> {
            private static volatile NamePart[] _emptyArray;
            public boolean isExtension;
            public String namePart;

            public static NamePart[] emptyArray() {
                if (_emptyArray == null) {
                    synchronized (InternalNano.LAZY_INIT_LOCK) {
                        if (_emptyArray == null) {
                            _emptyArray = new NamePart[0];
                        }
                    }
                }
                return _emptyArray;
            }

            public NamePart() {
                clear();
            }

            public NamePart clear() {
                this.namePart = "";
                this.isExtension = false;
                this.unknownFieldData = null;
                this.cachedSize = -1;
                return this;
            }

            public void writeTo(CodedOutputByteBufferNano output) throws IOException {
                output.writeString(1, this.namePart);
                output.writeBool(2, this.isExtension);
                super.writeTo(output);
            }

            protected int computeSerializedSize() {
                return (super.computeSerializedSize() + CodedOutputByteBufferNano.computeStringSize(1, this.namePart)) + CodedOutputByteBufferNano.computeBoolSize(2, this.isExtension);
            }

            public NamePart mergeFrom(CodedInputByteBufferNano input) throws IOException {
                while (true) {
                    int tag = input.readTag();
                    switch (tag) {
                        case 0:
                            break;
                        case 10:
                            this.namePart = input.readString();
                            continue;
                        case 16:
                            this.isExtension = input.readBool();
                            continue;
                        default:
                            if (!storeUnknownField(input, tag)) {
                                break;
                            }
                            continue;
                    }
                    return this;
                }
            }
        }

        public static UninterpretedOption[] emptyArray() {
            if (_emptyArray == null) {
                synchronized (InternalNano.LAZY_INIT_LOCK) {
                    if (_emptyArray == null) {
                        _emptyArray = new UninterpretedOption[0];
                    }
                }
            }
            return _emptyArray;
        }

        public UninterpretedOption() {
            clear();
        }

        public UninterpretedOption clear() {
            this.bitField0_ = 0;
            this.name = NamePart.emptyArray();
            this.identifierValue_ = "";
            this.positiveIntValue_ = 0;
            this.negativeIntValue_ = 0;
            this.doubleValue_ = 0.0d;
            this.stringValue_ = WireFormatNano.EMPTY_BYTES;
            this.aggregateValue_ = "";
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.name != null && this.name.length > 0) {
                for (NamePart element : this.name) {
                    if (element != null) {
                        output.writeMessage(2, element);
                    }
                }
            }
            if ((this.bitField0_ & 1) != 0) {
                output.writeString(3, this.identifierValue_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt64(4, this.positiveIntValue_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeInt64(5, this.negativeIntValue_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeDouble(6, this.doubleValue_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeBytes(7, this.stringValue_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeString(8, this.aggregateValue_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.name != null && this.name.length > 0) {
                for (NamePart element : this.name) {
                    if (element != null) {
                        size += CodedOutputByteBufferNano.computeMessageSize(2, element);
                    }
                }
            }
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeStringSize(3, this.identifierValue_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt64Size(4, this.positiveIntValue_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeInt64Size(5, this.negativeIntValue_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeDoubleSize(6, this.doubleValue_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeBytesSize(7, this.stringValue_);
            }
            if ((this.bitField0_ & 32) != 0) {
                return size + CodedOutputByteBufferNano.computeStringSize(8, this.aggregateValue_);
            }
            return size;
        }

        public UninterpretedOption mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 18:
                        int i;
                        int arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 18);
                        if (this.name == null) {
                            i = 0;
                        } else {
                            i = this.name.length;
                        }
                        NamePart[] newArray = new NamePart[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.name, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = new NamePart();
                            input.readMessage(newArray[i]);
                            input.readTag();
                            i++;
                        }
                        newArray[i] = new NamePart();
                        input.readMessage(newArray[i]);
                        this.name = newArray;
                        continue;
                    case 26:
                        this.identifierValue_ = input.readString();
                        this.bitField0_ |= 1;
                        continue;
                    case 32:
                        this.positiveIntValue_ = input.readUInt64();
                        this.bitField0_ |= 2;
                        continue;
                    case 40:
                        this.negativeIntValue_ = input.readInt64();
                        this.bitField0_ |= 4;
                        continue;
                    case 49:
                        this.doubleValue_ = input.readDouble();
                        this.bitField0_ |= 8;
                        continue;
                    case 58:
                        this.stringValue_ = input.readBytes();
                        this.bitField0_ |= 16;
                        continue;
                    case 66:
                        this.aggregateValue_ = input.readString();
                        this.bitField0_ |= 32;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }
}
