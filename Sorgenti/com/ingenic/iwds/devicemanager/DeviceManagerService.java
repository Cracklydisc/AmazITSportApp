package com.ingenic.iwds.devicemanager;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.os.RemoteException;
import com.ingenic.iwds.devicemanager.IDeviceManagerService.Stub;
import com.ingenic.iwds.smartsense.SensorService;
import com.ingenic.iwds.utils.IwdsLog;

public class DeviceManagerService extends Service {
    private static Context f85a;
    private C1130a f86b = new C1130a();

    private static class C1130a extends Stub {
        private C1130a() {
        }

        public synchronized boolean isWearOnRightHand() throws RemoteException {
            return DeviceManagerService.m69c(DeviceManagerService.f85a, "isOnRightHand", false);
        }

        public synchronized boolean setWearOnRightHand(boolean isOnRightHand) throws RemoteException {
            boolean wearOnRightHand;
            DeviceManagerService.m70d(DeviceManagerService.f85a, "isOnRightHand", isOnRightHand);
            wearOnRightHand = SensorService.setWearOnRightHand(isOnRightHand);
            if (wearOnRightHand) {
                DeviceManagerService.f85a.sendBroadcast(new Intent(isOnRightHand ? "iwds.devicemanager.action.wear_on_right_hand" : "iwds.devicemanager.action.wear_on_left_hand"));
            }
            return wearOnRightHand;
        }
    }

    public void onCreate() {
        IwdsLog.m264d(this, "onCreate");
        super.onCreate();
        f85a = getBaseContext();
        SensorService.setWearOnRightHand(m69c(f85a, "isOnRightHand", false));
    }

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind");
        return this.f86b;
    }

    private static boolean m69c(Context context, String str, boolean z) {
        return context.getSharedPreferences("DeviceManagerService", 0).getBoolean(str, z);
    }

    private static void m70d(Context context, String str, boolean z) {
        Editor edit = context.getSharedPreferences("DeviceManagerService", 0).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }
}
