package com.ingenic.iwds.devicemanager;

import android.os.IBinder;
import android.os.RemoteException;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.devicemanager.IDeviceManagerService.Stub;
import com.ingenic.iwds.utils.IwdsLog;

public class DeviceManagerServiceManager extends ServiceManagerContext {
    private IDeviceManagerService f88a;

    class C11311 extends ServiceClientProxy {
        final /* synthetic */ DeviceManagerServiceManager f87a;

        public void onServiceConnected(IBinder service) {
            this.f87a.f88a = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
        }
    }

    public void setWearOnRightHand(boolean isOnRightHand) {
        try {
            this.f88a.setWearOnRightHand(isOnRightHand);
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in setWearOnRightHand: " + e.toString());
        }
    }

    public boolean isWearOnRightHand() {
        boolean z = false;
        try {
            z = this.f88a.isWearOnRightHand();
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in isWearOnRightHand: " + e.toString());
        }
        return z;
    }
}
