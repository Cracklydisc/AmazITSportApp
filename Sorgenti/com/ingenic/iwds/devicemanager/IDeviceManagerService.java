package com.ingenic.iwds.devicemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDeviceManagerService extends IInterface {

    public static abstract class Stub extends Binder implements IDeviceManagerService {

        private static class C1132a implements IDeviceManagerService {
            private IBinder f89a;

            C1132a(IBinder iBinder) {
                this.f89a = iBinder;
            }

            public IBinder asBinder() {
                return this.f89a;
            }

            public boolean isWearOnRightHand() throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.devicemanager.IDeviceManagerService");
                    this.f89a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean setWearOnRightHand(boolean isRightHand) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.devicemanager.IDeviceManagerService");
                    obtain.writeInt(isRightHand ? 1 : 0);
                    this.f89a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.devicemanager.IDeviceManagerService");
        }

        public static IDeviceManagerService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.devicemanager.IDeviceManagerService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IDeviceManagerService)) {
                return new C1132a(obj);
            }
            return (IDeviceManagerService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int i = 0;
            boolean isWearOnRightHand;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.devicemanager.IDeviceManagerService");
                    isWearOnRightHand = isWearOnRightHand();
                    reply.writeNoException();
                    if (isWearOnRightHand) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.devicemanager.IDeviceManagerService");
                    if (data.readInt() != 0) {
                        isWearOnRightHand = true;
                    } else {
                        isWearOnRightHand = false;
                    }
                    isWearOnRightHand = setWearOnRightHand(isWearOnRightHand);
                    reply.writeNoException();
                    if (isWearOnRightHand) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.devicemanager.IDeviceManagerService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    boolean isWearOnRightHand() throws RemoteException;

    boolean setWearOnRightHand(boolean z) throws RemoteException;
}
