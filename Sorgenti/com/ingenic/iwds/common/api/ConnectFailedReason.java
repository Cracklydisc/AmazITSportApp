package com.ingenic.iwds.common.api;

import com.ingenic.iwds.utils.IwdsAssert;

public class ConnectFailedReason {
    private int f55a;

    ConnectFailedReason(int reasonCode) {
        this.f55a = reasonCode;
    }

    public String toString() {
        switch (this.f55a) {
            case 0:
                return "success";
            case 1:
                return "service is unavailable";
            case 2:
                return "service authentication failure";
            default:
                IwdsAssert.dieIf((Object) this, true, "Implement me.");
                return "Assert";
        }
    }
}
