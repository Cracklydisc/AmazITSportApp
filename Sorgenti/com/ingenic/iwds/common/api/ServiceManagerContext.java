package com.ingenic.iwds.common.api;

import android.content.Context;
import android.os.IBinder;
import com.ingenic.iwds.utils.IwdsAssert;

public abstract class ServiceManagerContext {
    private Context mContext;
    protected ServiceClientProxy m_serviceClientProxy;

    protected static abstract class ServiceClientProxy {
        public abstract void onServiceConnected(IBinder iBinder);

        public abstract void onServiceDisconnected(boolean z);

        protected ServiceClientProxy() {
        }
    }

    public ServiceManagerContext(Context context) {
        IwdsAssert.dieIf((Object) this, context == null, "Context can not be null.");
        this.mContext = context;
    }

    public Context getContext() {
        return this.mContext;
    }

    void onServiceConnected(IBinder binder) {
        this.m_serviceClientProxy.onServiceConnected(binder);
    }

    void onServiceDisconnected(boolean unexpected) {
        this.m_serviceClientProxy.onServiceDisconnected(unexpected);
    }
}
