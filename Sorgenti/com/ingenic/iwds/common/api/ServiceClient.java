package com.ingenic.iwds.common.api;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ServiceClient {
    private static HashMap<String, C1095b> f65h;
    private static List<String> f66i = new ArrayList(Arrays.asList(new String[]{"com.ingenic.watchmanager", "com.ingenic.iwds.device", "com.ingenic.iwds.phone", "com.ingenic.watchconnector", "com.huami.watch.launcher", "com.huami.watch.phonemanager", "com.huami.mobile.launcher", "com.huami.watch.hmwatchmanager", "com.huami.watch.huamiwatchmanager"}));
    private Context f67a;
    private String f68b;
    private ConnectionCallbacks f69c;
    private C1098a f70d;
    private ServiceManagerContext f71e;
    private C1095b f72f;
    private Intent f73g;

    public interface ConnectionCallbacks {
        void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason connectFailedReason);

        void onConnected(ServiceClient serviceClient);

        void onDisconnected(ServiceClient serviceClient, boolean z);
    }

    private abstract class C1095b {
        public Intent f56b;
        final /* synthetic */ ServiceClient f57c;

        public abstract ServiceManagerContext mo1686a(Context context);

        public abstract String mo1687a();

        private C1095b(ServiceClient serviceClient) {
            this.f57c = serviceClient;
        }

        public void m48a(Intent intent) {
            this.f56b = intent;
        }

        public Intent m49b() {
            return this.f56b;
        }
    }

    class C10961 extends C1095b {
        final /* synthetic */ ServiceClient f58a;

        C10961(ServiceClient serviceClient) {
            this.f58a = serviceClient;
            super();
        }

        public ServiceManagerContext mo1686a(Context context) {
            return new ConnectionServiceManager(context);
        }

        public String mo1687a() {
            return "com.ingenic.iwds.uniconnect.ConnectionService";
        }
    }

    private class C1098a extends Handler {
        final /* synthetic */ ServiceClient f60a;
        private int f61b;
        private boolean f62c;
        private boolean f63d;
        private ServiceConnection f64e;

        class C10971 implements ServiceConnection {
            final /* synthetic */ C1098a f59a;

            C10971(C1098a c1098a) {
                this.f59a = c1098a;
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                this.f59a.m53a(service);
            }

            public void onServiceDisconnected(ComponentName name) {
                this.f59a.m57d();
            }
        }

        private C1098a(ServiceClient serviceClient) {
            this.f60a = serviceClient;
            this.f61b = 0;
            this.f64e = new C10971(this);
        }

        public int m52a() {
            int i;
            synchronized (this) {
                i = this.f61b;
            }
            return i;
        }

        public void m55b() {
            Message obtain = Message.obtain(this);
            obtain.what = 19;
            obtain.sendToTarget();
        }

        public void m56c() {
            Message obtain = Message.obtain(this);
            obtain.what = 87;
            obtain.sendToTarget();
        }

        public void m54a(ConnectFailedReason connectFailedReason) {
            Message obtain = Message.obtain(this);
            obtain.what = 9;
            obtain.obj = connectFailedReason;
            obtain.sendToTarget();
        }

        public void m53a(IBinder iBinder) {
            Message obtain = Message.obtain(this);
            obtain.what = 20;
            obtain.obj = iBinder;
            obtain.sendToTarget();
        }

        public void m57d() {
            Message obtain = Message.obtain(this);
            obtain.what = 14;
            obtain.sendToTarget();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r6) {
            /*
            r5 = this;
            r4 = 3;
            r3 = 2;
            r0 = 1;
            r1 = 0;
            r2 = r6.what;
            switch(r2) {
                case 9: goto L_0x00dc;
                case 14: goto L_0x00ab;
                case 19: goto L_0x000a;
                case 20: goto L_0x007a;
                case 87: goto L_0x0049;
                default: goto L_0x0009;
            };
        L_0x0009:
            return;
        L_0x000a:
            monitor-enter(r5);
            r1 = r5.f61b;	 Catch:{ all -> 0x001c }
            if (r1 == r0) goto L_0x0013;
        L_0x000f:
            r1 = r5.f61b;	 Catch:{ all -> 0x001c }
            if (r1 != r4) goto L_0x001f;
        L_0x0013:
            r0 = r5.f63d;	 Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x001a;
        L_0x0017:
            r0 = 0;
            r5.f63d = r0;	 Catch:{ all -> 0x001c }
        L_0x001a:
            monitor-exit(r5);	 Catch:{ all -> 0x001c }
            goto L_0x0009;
        L_0x001c:
            r0 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x001c }
            throw r0;
        L_0x001f:
            r1 = r5.f61b;	 Catch:{ all -> 0x001c }
            if (r1 != r3) goto L_0x0028;
        L_0x0023:
            r0 = 1;
            r5.f62c = r0;	 Catch:{ all -> 0x001c }
            monitor-exit(r5);	 Catch:{ all -> 0x001c }
            goto L_0x0009;
        L_0x0028:
            r1 = 1;
            r5.f61b = r1;	 Catch:{ all -> 0x001c }
            monitor-exit(r5);	 Catch:{ all -> 0x001c }
            r1 = r5.f60a;
            r1 = r1.f67a;
            r2 = r5.f60a;
            r2 = r2.f73g;
            r3 = r5.f64e;
            r1 = r1.bindService(r2, r3, r0);
            if (r1 != 0) goto L_0x0009;
        L_0x0040:
            r1 = new com.ingenic.iwds.common.api.ConnectFailedReason;
            r1.<init>(r0);
            r5.m54a(r1);
            goto L_0x0009;
        L_0x0049:
            monitor-enter(r5);
            r1 = r5.f61b;	 Catch:{ all -> 0x005b }
            if (r1 == r3) goto L_0x0052;
        L_0x004e:
            r1 = r5.f61b;	 Catch:{ all -> 0x005b }
            if (r1 != 0) goto L_0x005e;
        L_0x0052:
            r0 = r5.f62c;	 Catch:{ all -> 0x005b }
            if (r0 == 0) goto L_0x0059;
        L_0x0056:
            r0 = 0;
            r5.f62c = r0;	 Catch:{ all -> 0x005b }
        L_0x0059:
            monitor-exit(r5);	 Catch:{ all -> 0x005b }
            goto L_0x0009;
        L_0x005b:
            r0 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x005b }
            throw r0;
        L_0x005e:
            r1 = r5.f61b;	 Catch:{ all -> 0x005b }
            if (r1 != r0) goto L_0x0067;
        L_0x0062:
            r0 = 1;
            r5.f63d = r0;	 Catch:{ all -> 0x005b }
            monitor-exit(r5);	 Catch:{ all -> 0x005b }
            goto L_0x0009;
        L_0x0067:
            r0 = 2;
            r5.f61b = r0;	 Catch:{ all -> 0x005b }
            monitor-exit(r5);	 Catch:{ all -> 0x005b }
            r0 = r5.f60a;
            r0 = r0.f67a;
            r1 = r5.f64e;
            r0.unbindService(r1);
            r5.m57d();
            goto L_0x0009;
        L_0x007a:
            r0 = r5.f60a;
            r2 = r0.f71e;
            r0 = r6.obj;
            r0 = (android.os.IBinder) r0;
            r2.onServiceConnected(r0);
            monitor-enter(r5);
            r0 = 3;
            r5.f61b = r0;	 Catch:{ all -> 0x00a8 }
            monitor-exit(r5);	 Catch:{ all -> 0x00a8 }
            r0 = r5.f60a;
            r0 = r0.f69c;
            r2 = r5.f60a;
            r0.onConnected(r2);
            r0 = r5.f62c;
            if (r0 == 0) goto L_0x009d;
        L_0x009b:
            r5.f62c = r1;
        L_0x009d:
            r0 = r5.f63d;
            if (r0 == 0) goto L_0x0009;
        L_0x00a1:
            r5.f63d = r1;
            r5.m56c();
            goto L_0x0009;
        L_0x00a8:
            r0 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x00a8 }
            throw r0;
        L_0x00ab:
            monitor-enter(r5);
            r2 = r5.f61b;	 Catch:{ all -> 0x00d9 }
            if (r2 != r4) goto L_0x00f5;
        L_0x00b0:
            r2 = 0;
            r5.f61b = r2;	 Catch:{ all -> 0x00d9 }
            monitor-exit(r5);	 Catch:{ all -> 0x00d9 }
            r2 = r5.f60a;
            r2 = r2.f69c;
            r3 = r5.f60a;
            r2.onDisconnected(r3, r0);
            r2 = r5.f60a;
            r2 = r2.f71e;
            r2.onServiceDisconnected(r0);
            r0 = r5.f63d;
            if (r0 == 0) goto L_0x00ce;
        L_0x00cc:
            r5.f63d = r1;
        L_0x00ce:
            r0 = r5.f62c;
            if (r0 == 0) goto L_0x0009;
        L_0x00d2:
            r5.f62c = r1;
            r5.m55b();
            goto L_0x0009;
        L_0x00d9:
            r0 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x00d9 }
            throw r0;
        L_0x00dc:
            monitor-enter(r5);
            r0 = 0;
            r5.f61b = r0;	 Catch:{ all -> 0x00f2 }
            monitor-exit(r5);	 Catch:{ all -> 0x00f2 }
            r0 = r5.f60a;
            r1 = r0.f69c;
            r2 = r5.f60a;
            r0 = r6.obj;
            r0 = (com.ingenic.iwds.common.api.ConnectFailedReason) r0;
            r1.onConnectFailed(r2, r0);
            goto L_0x0009;
        L_0x00f2:
            r0 = move-exception;
            monitor-exit(r5);	 Catch:{ all -> 0x00f2 }
            throw r0;
        L_0x00f5:
            r0 = r1;
            goto L_0x00b0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ingenic.iwds.common.api.ServiceClient.a.handleMessage(android.os.Message):void");
        }
    }

    private Intent m58a(Context context, String str) {
        Intent intent = new Intent(str);
        List<ResolveInfo> queryIntentServices = context.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.size() == 0) {
            return null;
        }
        String str2;
        String str3;
        for (ResolveInfo resolveInfo : queryIntentServices) {
            if (f66i.contains(resolveInfo.serviceInfo.packageName)) {
                String str4 = resolveInfo.serviceInfo.packageName;
                str2 = resolveInfo.serviceInfo.name;
                str3 = str4;
                break;
            }
        }
        str3 = null;
        str2 = null;
        if (str3 == null || str2 == null) {
            return null;
        }
        ComponentName componentName = new ComponentName(str3, str2);
        Intent intent2 = new Intent(intent);
        intent2.setComponent(componentName);
        return intent2;
    }

    private void m60a(Context context) {
        f65h = new HashMap();
        f65h.put("service_connection", new C10961(this));
        for (Object obj : f65h.keySet()) {
            C1095b c1095b = (C1095b) f65h.get(obj);
            c1095b.m48a(m58a(context.getApplicationContext(), c1095b.mo1687a()));
        }
    }

    public ServiceClient(Context context, String serviceName, ConnectionCallbacks callbacks) {
        boolean z;
        boolean z2 = true;
        IwdsAssert.dieIf((Object) this, context == null, "Context is null.");
        if (serviceName == null || serviceName.isEmpty()) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Service name is null or empty.");
        if (callbacks == null) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Callbacks is null.");
        this.f67a = context.getApplicationContext();
        if (this.f67a == null) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Application context is null");
        this.f69c = callbacks;
        this.f70d = new C1098a();
        this.f68b = serviceName;
        if (f65h == null) {
            m60a(this.f67a);
        }
        this.f72f = (C1095b) f65h.get(this.f68b);
        IwdsAssert.dieIf((Object) this, this.f72f == null, "Unsupported service: " + this.f68b);
        this.f71e = this.f72f.mo1686a(this.f67a);
        this.f73g = this.f72f.m49b();
        if (this.f73g != null) {
            z2 = false;
        }
        IwdsAssert.dieIf((Object) this, z2, "Unable to find service bind intent: " + this.f68b);
    }

    public void connect() {
        this.f70d.m55b();
    }

    public void disconnect() {
        this.f70d.m56c();
    }

    public boolean isConnected() {
        return this.f70d.m52a() == 3;
    }

    public ServiceManagerContext getServiceManagerContext() {
        return this.f71e;
    }
}
