package com.ingenic.iwds.common.exception;

public class FileTransferException extends IwdsException {
    public FileTransferException(String msg) {
        super(msg);
    }
}
