package com.ingenic.iwds.common.exception;

import android.os.RemoteException;
import com.ingenic.iwds.datatransactor.FileTransferErrorCode;
import com.ingenic.iwds.uniconnect.UniconnectErrorCode;
import com.ingenic.iwds.utils.IwdsAssert;
import java.io.IOException;

public class IwdsException extends RuntimeException {
    public IwdsException(String msg) {
        super(msg);
    }

    public static void throwUniconnectIOException(int error) throws IOException {
        IOException iOException = new IOException("Failed on code: " + error + "(" + UniconnectErrorCode.errorString(error) + ")");
        switch (error) {
            case -6:
                iOException.initCause(new PortDisconnectedException());
                break;
            case -5:
                iOException.initCause(new PortClosedException());
                break;
            case -4:
                iOException.initCause(new RemoteException());
                break;
            case -3:
                iOException.initCause(new PortBusyException());
                break;
            case -2:
                iOException.initCause(new LinkDisconnectedException());
                break;
            case -1:
                iOException.initCause(new LinkUnbondedException());
                break;
            default:
                IwdsAssert.dieIf("IwdsException.throwIOException", true, "Implement me.");
                break;
        }
        throw iOException;
    }

    public static void throwFileTransferException(int error) throws FileTransferException {
        FileTransferException fileTransferException = new FileTransferException("Failed on code: " + error + "(" + FileTransferErrorCode.errorString(error) + ")");
        switch (error) {
            case 1:
                fileTransferException.initCause(new FileStatusException());
                break;
            case 2:
                fileTransferException.initCause(new SDCardNotMountedException());
                break;
            case 3:
                fileTransferException.initCause(new SDCardFullException());
                break;
            default:
                IwdsAssert.dieIf("IwdsException.throwFileTransferException", true, "Implement me.");
                break;
        }
        throw fileTransferException;
    }
}
