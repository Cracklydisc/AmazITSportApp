package com.ingenic.iwds.remotewakelock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.ingenic.iwds.remotewakelock.IRemoteWakeLockService.Stub;

public class RemoteWakeLockService extends Service {
    private RemoteWakeLockProxy f182a;
    private C1189a f183b;

    private class C1189a extends Stub {
        final /* synthetic */ RemoteWakeLockService f181a;

        private C1189a(RemoteWakeLockService remoteWakeLockService) {
            this.f181a = remoteWakeLockService;
        }

        public int registerRemoteWakeLockCallback(IRemoteWakeLockCallback callback) {
            return this.f181a.f182a.m226a(callback);
        }

        public void unregisterRemoteWakeLockCallback(int callerId) {
            this.f181a.f182a.m227a(callerId);
        }

        public void newRemoteWakeLock(int callerId, int id, int levelAndFlags, String tag) {
            this.f181a.f182a.m229a(callerId, id, levelAndFlags, tag);
        }

        public void destroyRemoteWakeLock(int callerId, int id) {
            this.f181a.f182a.m228a(callerId, id);
        }

        public void acquireWakeLock(int callerId, int id, long timeout) {
            this.f181a.f182a.m230a(callerId, id, timeout);
        }

        public void releaseWakeLock(int callerId, int id) {
            this.f181a.f182a.m231b(callerId, id);
        }
    }

    public void onCreate() {
        super.onCreate();
        this.f182a = RemoteWakeLockProxy.getInstance(this);
        this.f183b = new C1189a();
    }

    public IBinder onBind(Intent intent) {
        return this.f183b;
    }
}
