package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class DeleteInfo extends C1178a implements Parcelable {
    public static final Creator<DeleteInfo> CREATOR = new C11801();

    /* compiled from: RemoteWakeLockInfo */
    static class C11801 implements Creator<DeleteInfo> {
        C11801() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m152a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m153a(i);
        }

        public DeleteInfo m152a(Parcel parcel) {
            return new DeleteInfo(parcel);
        }

        public DeleteInfo[] m153a(int i) {
            return new DeleteInfo[i];
        }
    }

    public DeleteInfo(int callerId, int id) {
        this.a = callerId;
        this.b = id;
    }

    protected DeleteInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
    }
}
