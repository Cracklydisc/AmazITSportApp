package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class CMDInfo extends C1178a implements Parcelable {
    public static final Creator<CMDInfo> CREATOR = new C11771();
    private int f144c;
    private long f145d;

    /* compiled from: RemoteWakeLockInfo */
    static class C11771 implements Creator<CMDInfo> {
        C11771() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m148a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m149a(i);
        }

        public CMDInfo m148a(Parcel parcel) {
            return new CMDInfo(parcel);
        }

        public CMDInfo[] m149a(int i) {
            return new CMDInfo[i];
        }
    }

    public CMDInfo(int callerId, int id) {
        this(callerId, callerId, 0);
    }

    public CMDInfo(int callerId, int id, int cmd) {
        this(callerId, id, cmd, -1);
    }

    public CMDInfo(int callerId, int id, int cmd, long timeout) {
        this.f144c = 0;
        this.f145d = -1;
        this.a = callerId;
        this.b = id;
        this.f144c = cmd;
        this.f145d = timeout;
    }

    protected CMDInfo(Parcel in) {
        this.f144c = 0;
        this.f145d = -1;
        readFromParcel(in);
    }

    public int getCmd() {
        return this.f144c;
    }

    public long getTimeout() {
        return this.f145d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        dest.writeInt(this.f144c);
        dest.writeLong(this.f145d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        this.f144c = in.readInt();
        this.f145d = in.readLong();
    }
}
