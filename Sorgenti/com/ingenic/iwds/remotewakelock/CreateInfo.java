package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class CreateInfo extends C1178a implements Parcelable {
    public static final Creator<CreateInfo> CREATOR = new C11791();
    private int f146c;
    private String f147d;

    /* compiled from: RemoteWakeLockInfo */
    static class C11791 implements Creator<CreateInfo> {
        C11791() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m150a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m151a(i);
        }

        public CreateInfo m150a(Parcel parcel) {
            return new CreateInfo(parcel);
        }

        public CreateInfo[] m151a(int i) {
            return new CreateInfo[i];
        }
    }

    public CreateInfo(int callerId, int id, int levelAndFlags, String tag) {
        this.a = callerId;
        this.b = id;
        this.f146c = levelAndFlags;
        this.f147d = tag;
    }

    protected CreateInfo(Parcel in) {
        readFromParcel(in);
    }

    public int getLevelAndFlags() {
        return this.f146c;
    }

    public String getTag() {
        return this.f147d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        dest.writeInt(this.f146c);
        dest.writeString(this.f147d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        this.f146c = in.readInt();
        this.f147d = in.readString();
    }
}
