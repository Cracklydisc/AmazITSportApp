package com.ingenic.iwds.remotewakelock;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.SparseArray;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.utils.IwdsUtils;
import com.ingenic.iwds.utils.SimpleIDAlloter;

public class RemoteWakeLockProxy {
    private static RemoteWakeLockProxy f169a;
    private final PowerManager f170b;
    private final C1187b f171c;
    private final DataTransactor f172d;
    private volatile boolean f173e;
    private SimpleIDAlloter f174f = SimpleIDAlloter.newInstance();
    private HandlerThread f175g;
    private Handler f176h;
    private SparseArray<IRemoteWakeLockCallback> f177i;
    private SparseArray<SparseArray<WakeLock>> f178j;
    private SparseArray<SparseArray<CreateInfo>> f179k;
    private SparseArray<SparseArray<CreateInfo>> f180l;

    private class C1186a extends Handler {
        final /* synthetic */ RemoteWakeLockProxy f167a;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.f167a.m194a(msg.arg1, (IRemoteWakeLockCallback) msg.obj);
                    return;
                case 2:
                    this.f167a.m209b(msg.arg1);
                    return;
                case 3:
                    this.f167a.m211b((CreateInfo) msg.obj);
                    return;
                case 4:
                    this.f167a.m217c((CreateInfo) msg.obj);
                    return;
                case 5:
                    this.f167a.m195a((DataTransactResult) msg.obj);
                    return;
                case 6:
                    this.f167a.m205a(msg.obj);
                    return;
                case 7:
                    this.f167a.m215c();
                    return;
                case 8:
                    this.f167a.m219d();
                    return;
                case 9:
                    this.f167a.m216c(msg.arg1, msg.arg2);
                    return;
                case 10:
                    this.f167a.m223e();
                    return;
                case 11:
                    this.f167a.m225f();
                    return;
                case 12:
                    this.f167a.m206a(((Boolean) msg.obj).booleanValue());
                    return;
                default:
                    return;
            }
        }
    }

    private class C1187b implements DataTransactorCallback {
        final /* synthetic */ RemoteWakeLockProxy f168a;

        private C1187b(RemoteWakeLockProxy remoteWakeLockProxy) {
            this.f168a = remoteWakeLockProxy;
        }

        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        }

        public void onChannelAvailable(boolean isAvailable) {
            this.f168a.f173e = isAvailable;
            if (isAvailable) {
                this.f168a.f176h.sendEmptyMessage(8);
            } else {
                this.f168a.f176h.sendEmptyMessage(7);
                this.f168a.f176h.sendEmptyMessage(10);
            }
            this.f168a.f176h.obtainMessage(12, Boolean.valueOf(isAvailable)).sendToTarget();
        }

        public void onSendResult(DataTransactResult result) {
            this.f168a.f176h.obtainMessage(5, result).sendToTarget();
        }

        public void onDataArrived(Object object) {
            this.f168a.f176h.obtainMessage(6, object).sendToTarget();
        }

        public void onSendFileProgress(int progress) {
        }

        public void onRecvFileProgress(int progress) {
        }
    }

    private RemoteWakeLockProxy(Context context) {
        this.f170b = (PowerManager) context.getSystemService("power");
        this.f171c = new C1187b();
        this.f172d = new DataTransactor(context, this.f171c, "5B354F08-DE47-7918-B0C1-13280E013FEE");
    }

    public static synchronized RemoteWakeLockProxy getInstance(Context context) {
        RemoteWakeLockProxy remoteWakeLockProxy;
        synchronized (RemoteWakeLockProxy.class) {
            if (f169a == null) {
                f169a = new RemoteWakeLockProxy(context);
            }
            remoteWakeLockProxy = f169a;
        }
        return remoteWakeLockProxy;
    }

    public void stopTransaction() {
        if (this.f172d != null && this.f172d.isStarted()) {
            this.f172d.stop();
        }
        m208b();
    }

    protected void finalize() throws Throwable {
        stopTransaction();
        super.finalize();
    }

    private void m208b() {
        this.f176h.sendEmptyMessage(11);
    }

    int m226a(IRemoteWakeLockCallback iRemoteWakeLockCallback) {
        int allocation = this.f174f.allocation();
        this.f176h.obtainMessage(1, allocation, -1, iRemoteWakeLockCallback).sendToTarget();
        return allocation;
    }

    void m227a(int i) {
        this.f176h.obtainMessage(2, i, -1).sendToTarget();
    }

    void m229a(int i, int i2, int i3, String str) {
        m197a(new CreateInfo(i, i2, i3, str));
    }

    private void m197a(CreateInfo createInfo) {
        if (this.f173e) {
            this.f172d.send((Object) createInfo);
        } else {
            this.f176h.obtainMessage(3, createInfo).sendToTarget();
        }
    }

    void m228a(int i, int i2) {
        this.f176h.obtainMessage(9, i, i2);
        if (this.f173e) {
            this.f172d.send(new DeleteInfo(i, i2));
        }
    }

    void m230a(int i, int i2, long j) {
        if (this.f173e) {
            this.f172d.send(new CMDInfo(i, i2, 1, j));
            return;
        }
        m189a(1, i, i2, j);
    }

    void m231b(int i, int i2) {
        if (this.f173e) {
            this.f172d.send(new CMDInfo(i, i2));
        } else {
            IwdsLog.m269w(this, "DataTransactor is invailable.Donot need to release.");
        }
    }

    private void m194a(int i, IRemoteWakeLockCallback iRemoteWakeLockCallback) {
        if (this.f177i == null) {
            this.f177i = new SparseArray();
        }
        synchronized (this.f177i) {
            this.f177i.put(i, iRemoteWakeLockCallback);
        }
    }

    private void m209b(int i) {
        if (this.f177i == null) {
            this.f174f.initialize();
            return;
        }
        synchronized (this.f177i) {
            this.f177i.delete(i);
        }
        if (this.f177i.size() == 0) {
            this.f174f.initialize();
        }
    }

    private void m211b(CreateInfo createInfo) {
        if (this.f179k == null) {
            this.f179k = new SparseArray();
        }
        IwdsUtils.addInArray(this.f179k, createInfo.getCallerId(), createInfo.getId(), createInfo);
    }

    private void m217c(CreateInfo createInfo) {
        if (this.f180l == null) {
            this.f180l = new SparseArray();
        }
        IwdsUtils.addInArray(this.f180l, createInfo.getCallerId(), createInfo.getId(), createInfo);
    }

    private void m216c(int i, int i2) {
        IwdsUtils.deleteInArray(this.f179k, i, i2);
        IwdsUtils.deleteInArray(this.f180l, i, i2);
    }

    private void m195a(DataTransactResult dataTransactResult) {
        int resultCode = dataTransactResult.getResultCode();
        Object transferedObject = dataTransactResult.getTransferedObject();
        if (transferedObject instanceof CreateInfo) {
            m192a(resultCode, (CreateInfo) transferedObject);
        } else if (transferedObject instanceof CMDInfo) {
            m191a(resultCode, (CMDInfo) transferedObject);
        } else if (transferedObject instanceof DeleteInfo) {
            m193a(resultCode, (DeleteInfo) transferedObject);
        }
    }

    private void m192a(int i, CreateInfo createInfo) {
        if (i == 0) {
            this.f176h.obtainMessage(4, createInfo).sendToTarget();
        } else {
            this.f176h.obtainMessage(3, createInfo).sendToTarget();
        }
    }

    private void m191a(int i, CMDInfo cMDInfo) {
        int cmd = cMDInfo.getCmd();
        if (cmd == 1) {
            m189a(i, cMDInfo.getCallerId(), cMDInfo.getId(), cMDInfo.getTimeout());
        } else if (cmd == 0) {
            m188a(i, cMDInfo.getCallerId(), cMDInfo.getId());
        }
    }

    private void m189a(int i, int i2, int i3, long j) {
        if (this.f177i != null) {
            IRemoteWakeLockCallback iRemoteWakeLockCallback = (IRemoteWakeLockCallback) this.f177i.get(i2);
            if (iRemoteWakeLockCallback != null) {
                iRemoteWakeLockCallback.performAcquireResult(i3, i, j);
            }
        }
    }

    private void m188a(int i, int i2, int i3) {
        if (i != 0) {
            IwdsLog.m265e((Object) this, "Release remote wakelock: " + i3 + " of caller: " + i2 + " failed.Result code: " + i);
        }
    }

    private void m193a(int i, DeleteInfo deleteInfo) {
        if (i != 0) {
            int id = deleteInfo.getId();
            IwdsLog.m265e((Object) this, "Delete remote wakelock: " + id + "of caller: " + deleteInfo.getCallerId() + " failed.Result code: " + i);
        }
    }

    private void m205a(Object obj) {
        if (obj instanceof CreateInfo) {
            m221d((CreateInfo) obj);
        } else if (obj instanceof CMDInfo) {
            m196a((CMDInfo) obj);
        } else if (obj instanceof DeleteInfo) {
            m198a((DeleteInfo) obj);
        }
    }

    private void m221d(CreateInfo createInfo) {
        m190a(createInfo.getCallerId(), createInfo.getId(), this.f170b.newWakeLock(createInfo.getLevelAndFlags(), createInfo.getTag()));
    }

    private void m190a(int i, int i2, WakeLock wakeLock) {
        if (this.f178j == null) {
            this.f178j = new SparseArray();
        }
        IwdsUtils.addInArray(this.f178j, i, i2, wakeLock);
    }

    private void m198a(DeleteInfo deleteInfo) {
        if (this.f178j != null) {
            int callerId = deleteInfo.getCallerId();
            SparseArray sparseArray = (SparseArray) this.f178j.get(callerId);
            if (sparseArray != null) {
                int id = deleteInfo.getId();
                WakeLock wakeLock = (WakeLock) sparseArray.get(id);
                if (wakeLock != null) {
                    if (wakeLock.isHeld()) {
                        wakeLock.release();
                    }
                    synchronized (this.f178j) {
                        sparseArray.delete(id);
                        if (sparseArray.size() == 0) {
                            this.f178j.delete(callerId);
                        }
                    }
                }
            }
        }
    }

    private void m196a(CMDInfo cMDInfo) {
        int cmd = cMDInfo.getCmd();
        if (cmd == 1) {
            m210b(cMDInfo.getCallerId(), cMDInfo.getId(), cMDInfo.getTimeout());
        } else if (cmd == 0) {
            m220d(cMDInfo.getCallerId(), cMDInfo.getId());
        }
    }

    private void m210b(int i, int i2, long j) {
        WakeLock wakeLock = (WakeLock) m187a(this.f178j, i, i2);
        if (wakeLock != null && !wakeLock.isHeld()) {
            if (j < 0) {
                wakeLock.acquire();
            } else {
                wakeLock.acquire(j);
            }
        }
    }

    private void m220d(int i, int i2) {
        WakeLock wakeLock = (WakeLock) m187a(this.f178j, i, i2);
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    private static <T> T m187a(SparseArray<SparseArray<T>> sparseArray, int i, int i2) {
        if (sparseArray == null) {
            return null;
        }
        SparseArray sparseArray2 = (SparseArray) sparseArray.get(i);
        if (sparseArray2 == null) {
            return null;
        }
        return sparseArray2.get(i2);
    }

    private void m215c() {
        if (this.f180l != null) {
            if (this.f179k == null) {
                this.f179k = new SparseArray();
            }
            int size = this.f180l.size();
            for (int i = 0; i < size; i++) {
                int keyAt = this.f180l.keyAt(i);
                SparseArray sparseArray = (SparseArray) this.f180l.valueAt(i);
                synchronized (this.f179k) {
                    this.f179k.put(keyAt, sparseArray);
                }
            }
            synchronized (this.f180l) {
                this.f180l.clear();
            }
        }
    }

    private void m219d() {
        if (this.f179k != null) {
            for (int size = this.f179k.size() - 1; size >= 0; size--) {
                SparseArray sparseArray = (SparseArray) this.f179k.valueAt(size);
                int size2 = sparseArray.size() - 1;
                while (size2 >= 0) {
                    if (this.f173e) {
                        Object obj = (CreateInfo) sparseArray.valueAt(size2);
                        synchronized (this.f179k) {
                            sparseArray.removeAt(size2);
                            if (sparseArray.size() == 0) {
                                this.f179k.removeAt(size);
                            }
                        }
                        this.f172d.send(obj);
                        size2--;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void m223e() {
        if (this.f178j != null) {
            int size = this.f178j.size();
            for (int i = 0; i < size; i++) {
                SparseArray sparseArray = (SparseArray) this.f178j.valueAt(i);
                if (sparseArray != null) {
                    int size2 = sparseArray.size();
                    for (int i2 = 0; i2 < size2; i2++) {
                        WakeLock wakeLock = (WakeLock) sparseArray.valueAt(i2);
                        if (wakeLock.isHeld()) {
                            wakeLock.release();
                        }
                    }
                }
            }
            synchronized (this.f178j) {
                this.f178j.clear();
            }
        }
    }

    private void m225f() {
        if (this.f175g != null) {
            this.f175g.quit();
            this.f175g = null;
        }
        this.f176h = null;
    }

    private void m206a(boolean z) {
        if (this.f177i != null) {
            int size = this.f177i.size();
            for (int i = 0; i < size; i++) {
                ((IRemoteWakeLockCallback) this.f177i.valueAt(i)).performAvailableChanged(z);
            }
        }
    }
}
