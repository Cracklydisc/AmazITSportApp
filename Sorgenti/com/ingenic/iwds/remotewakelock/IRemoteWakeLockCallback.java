package com.ingenic.iwds.remotewakelock;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

interface IRemoteWakeLockCallback extends IInterface {

    public static class RemoteWakeLockCallbackPorxy implements IRemoteWakeLockCallback {
        private IBinder f148a;

        public RemoteWakeLockCallbackPorxy(IBinder remote) {
            this.f148a = remote;
        }

        public IBinder asBinder() {
            return this.f148a;
        }

        public void performAcquireResult(int id, int resultCode, long timeout) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback");
            obtain.writeInt(id);
            obtain.writeInt(resultCode);
            obtain.writeLong(timeout);
            try {
                this.f148a.transact(1, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void performAvailableChanged(boolean isAvailable) {
            int i = 0;
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback");
            if (isAvailable) {
                i = 1;
            }
            obtain.writeInt(i);
            try {
                this.f148a.transact(2, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }
    }

    public static abstract class Stub extends Binder implements IRemoteWakeLockCallback {
        public static IRemoteWakeLockCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IRemoteWakeLockCallback iRemoteWakeLockCallback = (IRemoteWakeLockCallback) obj.queryLocalInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback");
            return iRemoteWakeLockCallback == null ? new RemoteWakeLockCallbackPorxy(obj) : iRemoteWakeLockCallback;
        }

        public IBinder asBinder() {
            return this;
        }

        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback");
                    performAcquireResult(data.readInt(), data.readInt(), data.readLong());
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback");
                    performAvailableChanged(data.readInt() != 0);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void performAcquireResult(int i, int i2, long j);

    void performAvailableChanged(boolean z);
}
