package com.ingenic.iwds.remotewakelock;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.SimpleIDAlloter;

public class RemoteWakeLockManager extends ServiceManagerContext {
    private IRemoteWakeLockService f158a;
    private RemoteWakeLockCallback f159b;
    private IRemoteWakeLockCallback f160c;
    private Handler f161d;
    private Handler f162e;
    private HandlerThread f163f;
    private int f164g;
    private SparseArray<RemoteWakeLock> f165h;
    private SimpleIDAlloter f166i;

    class C11811 extends ServiceClientProxy {
        final /* synthetic */ RemoteWakeLockManager f150a;

        public void onServiceDisconnected(boolean unexpected) {
            this.f150a.m172a(unexpected);
        }

        public void onServiceConnected(IBinder binder) {
            this.f150a.m162a(binder);
        }
    }

    public final class RemoteWakeLock {
        final /* synthetic */ RemoteWakeLockManager f151a;
        private int f152b;
        private String f153c;
        private boolean f154d;

        private void m156a(boolean z) {
            this.f154d = z;
            if (this.f151a.f162e.hasMessages(5, this)) {
                this.f151a.f162e.removeMessages(5, this);
            }
        }
    }

    public interface RemoteWakeLockCallback {
        void onAcquireResult(RemoteWakeLock remoteWakeLock, long j, int i);
    }

    private class C1182a extends Stub {
        final /* synthetic */ RemoteWakeLockManager f155a;

        public void performAcquireResult(int id, int resultCode, long timeout) {
            this.f155a.f162e.obtainMessage(4, id, resultCode, Long.valueOf(timeout)).sendToTarget();
        }

        public void performAvailableChanged(boolean isAvailable) {
            if (!isAvailable && this.f155a.f165h != null) {
                int size = this.f155a.f165h.size();
                for (int i = 0; i < size; i++) {
                    RemoteWakeLock remoteWakeLock = (RemoteWakeLock) this.f155a.f165h.valueAt(i);
                    if (this.f155a.f162e.hasMessages(5, remoteWakeLock)) {
                        this.f155a.f162e.removeMessages(5, remoteWakeLock);
                    }
                    this.f155a.f162e.obtainMessage(5, remoteWakeLock).sendToTarget();
                }
            }
        }
    }

    private class C1183b extends Handler {
        final /* synthetic */ RemoteWakeLockManager f156a;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 4:
                    this.f156a.m165a((RemoteWakeLock) msg.obj, msg.getData().getLong("timeout"), msg.arg1);
                    return;
                default:
                    return;
            }
        }
    }

    private class C1184c extends Handler {
        final /* synthetic */ RemoteWakeLockManager f157a;

        public C1184c(RemoteWakeLockManager remoteWakeLockManager, Looper looper) {
            this.f157a = remoteWakeLockManager;
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.f157a.m163a((RemoteWakeLock) msg.obj);
                    return;
                case 2:
                    this.f157a.m164a((RemoteWakeLock) msg.obj, msg.getData().getLong("timeout", -1));
                    return;
                case 3:
                    this.f157a.m176b((RemoteWakeLock) msg.obj);
                    return;
                case 4:
                    this.f157a.m160a(msg.arg1, msg.arg2, ((Long) msg.obj).longValue());
                    return;
                case 5:
                    this.f157a.m180c((RemoteWakeLock) msg.obj);
                    return;
                case 6:
                    this.f157a.m184e();
                    return;
                case 7:
                    this.f157a.m182d();
                    return;
                default:
                    return;
            }
        }
    }

    private void m162a(IBinder iBinder) {
        m159a();
        this.f158a = IRemoteWakeLockService.Stub.asInterface(iBinder);
        if (this.f158a != null) {
            this.f164g = this.f158a.registerRemoteWakeLockCallback(this.f160c);
        }
        IwdsAssert.dieIf((Object) this, this.f164g == 0, "Caller is invalid.");
    }

    private void m172a(boolean z) {
        m179c();
        if (this.f158a != null) {
            this.f158a.unregisterRemoteWakeLockCallback(this.f164g);
        }
        m175b();
    }

    private void m159a() {
        this.f163f = new HandlerThread("RemoteWakeLock");
        this.f163f.start();
        this.f162e = new C1184c(this, this.f163f.getLooper());
    }

    private void m175b() {
        this.f162e.sendEmptyMessage(7);
    }

    private void m179c() {
        this.f162e.sendEmptyMessage(6);
    }

    private void m182d() {
        if (this.f163f != null) {
            this.f163f.quit();
            this.f163f = null;
        }
        this.f162e = null;
    }

    private void m163a(RemoteWakeLock remoteWakeLock) {
        m161a(m173b(remoteWakeLock.f152b, remoteWakeLock.f153c), remoteWakeLock);
    }

    private int m173b(int i, String str) {
        int allocation = this.f166i.allocation();
        this.f158a.newRemoteWakeLock(this.f164g, allocation, i, str);
        return allocation;
    }

    private void m161a(int i, RemoteWakeLock remoteWakeLock) {
        if (this.f165h == null) {
            this.f165h = new SparseArray();
        }
        synchronized (this.f165h) {
            this.f165h.put(i, remoteWakeLock);
        }
    }

    private void m184e() {
        if (this.f165h != null) {
            int size = this.f165h.size();
            for (int i = 0; i < size && this.f158a != null; i++) {
                this.f158a.destroyRemoteWakeLock(this.f164g, this.f165h.keyAt(i));
            }
            synchronized (this.f165h) {
                this.f165h.clear();
            }
        }
    }

    private void m164a(RemoteWakeLock remoteWakeLock, long j) {
        int a = m158a(this.f165h, (Object) remoteWakeLock);
        if (a >= 0) {
            this.f158a.acquireWakeLock(this.f164g, a, j);
        }
    }

    private void m176b(RemoteWakeLock remoteWakeLock) {
        int a = m158a(this.f165h, (Object) remoteWakeLock);
        if (a >= 0) {
            this.f158a.releaseWakeLock(this.f164g, a);
        }
    }

    private void m160a(int i, int i2, long j) {
        RemoteWakeLock remoteWakeLock = (RemoteWakeLock) this.f165h.get(i);
        if (remoteWakeLock != null) {
            Message obtainMessage = this.f161d.obtainMessage(4, i2, -1, remoteWakeLock);
            Bundle bundle = new Bundle();
            bundle.putLong("timeout", j);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
            if (i2 == 0) {
                remoteWakeLock.m156a(true);
                if (j > 0) {
                    this.f162e.sendMessageDelayed(this.f162e.obtainMessage(5, remoteWakeLock), j);
                    return;
                }
                return;
            }
            remoteWakeLock.m156a(false);
        }
    }

    private static <T> int m158a(SparseArray<T> sparseArray, T t) {
        if (t == null || sparseArray == null) {
            return -1;
        }
        int indexOfValue = sparseArray.indexOfValue(t);
        if (indexOfValue >= 0) {
            return sparseArray.keyAt(indexOfValue);
        }
        return -1;
    }

    private void m180c(RemoteWakeLock remoteWakeLock) {
        if (remoteWakeLock != null) {
            remoteWakeLock.m156a(false);
        }
    }

    private void m165a(RemoteWakeLock remoteWakeLock, long j, int i) {
        if (this.f159b != null) {
            this.f159b.onAcquireResult(remoteWakeLock, j, i);
        }
    }
}
