package com.ingenic.iwds.remotewakelock;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

interface IRemoteWakeLockService extends IInterface {

    public static class RemoteWakeLockProxy implements IRemoteWakeLockService {
        private IBinder f149a;

        public RemoteWakeLockProxy(IBinder remote) {
            this.f149a = remote;
        }

        public IBinder asBinder() {
            return this.f149a;
        }

        public int registerRemoteWakeLockCallback(IRemoteWakeLockCallback callback) {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
            try {
                this.f149a.transact(1, obtain, obtain2, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain2.readException();
            int readInt = obtain2.readInt();
            obtain.recycle();
            obtain2.recycle();
            return readInt;
        }

        public void unregisterRemoteWakeLockCallback(int callerId) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeInt(callerId);
            try {
                this.f149a.transact(2, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void newRemoteWakeLock(int callerId, int id, int levelAndFlags, String tag) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            obtain.writeInt(levelAndFlags);
            obtain.writeString(tag);
            try {
                this.f149a.transact(3, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void destroyRemoteWakeLock(int callerId, int id) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            try {
                this.f149a.transact(4, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void acquireWakeLock(int callerId, int id, long timeout) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            obtain.writeLong(timeout);
            try {
                this.f149a.transact(5, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void releaseWakeLock(int callerId, int id) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            try {
                this.f149a.transact(6, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }
    }

    public static abstract class Stub extends Binder implements IRemoteWakeLockService {
        public static IRemoteWakeLockService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IRemoteWakeLockService iRemoteWakeLockService = (IRemoteWakeLockService) obj.queryLocalInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
            return iRemoteWakeLockService == null ? new RemoteWakeLockProxy(obj) : iRemoteWakeLockService;
        }

        public IBinder asBinder() {
            return this;
        }

        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    int registerRemoteWakeLockCallback = registerRemoteWakeLockCallback(com.ingenic.iwds.remotewakelock.IRemoteWakeLockCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(registerRemoteWakeLockCallback);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    unregisterRemoteWakeLockCallback(data.readInt());
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    newRemoteWakeLock(data.readInt(), data.readInt(), data.readInt(), data.readString());
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    destroyRemoteWakeLock(data.readInt(), data.readInt());
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    acquireWakeLock(data.readInt(), data.readInt(), data.readLong());
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.remotewakelock.IRemoteWakeLockService");
                    releaseWakeLock(data.readInt(), data.readInt());
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void acquireWakeLock(int i, int i2, long j);

    void destroyRemoteWakeLock(int i, int i2);

    void newRemoteWakeLock(int i, int i2, int i3, String str);

    int registerRemoteWakeLockCallback(IRemoteWakeLockCallback iRemoteWakeLockCallback);

    void releaseWakeLock(int i, int i2);

    void unregisterRemoteWakeLockCallback(int i);
}
