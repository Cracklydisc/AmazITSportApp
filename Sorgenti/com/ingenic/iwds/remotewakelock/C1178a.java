package com.ingenic.iwds.remotewakelock;

/* compiled from: RemoteWakeLockInfo */
abstract class C1178a {
    protected int f142a;
    protected int f143b;

    C1178a() {
    }

    public int getCallerId() {
        return this.f142a;
    }

    public int getId() {
        return this.f143b;
    }
}
