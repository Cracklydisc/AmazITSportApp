package com.ingenic.iwds;

import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

public class HardwareList {
    private static String f27a = "HardwareList: ";
    private static String[] f28b = new String[]{"bluetooth.chip", "lcd.exterior", "lcd.dpi", "lcd.ppi", "lcd.size"};
    private static Hashtable<String, String> f29c;

    static {
        Throwable th;
        BufferedReader bufferedReader = null;
        f29c = null;
        f29c = new Hashtable();
        BufferedReader bufferedReader2;
        try {
            bufferedReader2 = new BufferedReader(new FileReader("/proc/hardware/list"));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        break;
                    }
                    int i;
                    String[] split = readLine.split(":");
                    String str = "HardwareList";
                    boolean z = split == null || split.length < 2;
                    IwdsAssert.dieIf(str, z, "Invalid line: " + readLine);
                    readLine = split[0].trim();
                    String trim = split[1].trim();
                    for (Object equals : f28b) {
                        if (readLine.equals(equals)) {
                            i = 1;
                            break;
                        }
                    }
                    i = 0;
                    if (i != 0) {
                        str = "HardwareList";
                        if (f29c.get(readLine) != null) {
                            z = true;
                        } else {
                            z = false;
                        }
                        IwdsAssert.dieIf(str, z, "Dunplicate Key: " + readLine);
                        f29c.put(readLine, trim);
                    }
                } catch (FileNotFoundException e) {
                } catch (IOException e2) {
                }
            }
            if (bufferedReader2 != null) {
                try {
                    bufferedReader2.close();
                } catch (IOException e3) {
                    IwdsLog.m266e(f27a, "Exception occurred trying to close /proc/hardware/list");
                }
            }
        } catch (FileNotFoundException e4) {
            bufferedReader2 = null;
            try {
                IwdsLog.m266e(f27a, "Exception occurred trying to open /proc/hardware/list");
                f29c = null;
                if (bufferedReader2 != null) {
                    try {
                        bufferedReader2.close();
                    } catch (IOException e5) {
                        IwdsLog.m266e(f27a, "Exception occurred trying to close /proc/hardware/list");
                    }
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                bufferedReader = bufferedReader2;
                th = th3;
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e6) {
                        IwdsLog.m266e(f27a, "Exception occurred trying to close /proc/hardware/list");
                    }
                }
                throw th;
            }
        } catch (IOException e7) {
            bufferedReader2 = null;
            IwdsLog.m266e(f27a, "Exception occurred trying to read /proc/hardware/list");
            f29c = null;
            if (bufferedReader2 != null) {
                try {
                    bufferedReader2.close();
                } catch (IOException e8) {
                    IwdsLog.m266e(f27a, "Exception occurred trying to close /proc/hardware/list");
                }
            }
        } catch (Throwable th4) {
            th = th4;
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            throw th;
        }
    }

    private HardwareList() {
    }

    public static synchronized String getHardwareValue(String key, String defs) {
        synchronized (HardwareList.class) {
            String hardwareValue = getHardwareValue(key);
            if (hardwareValue != null) {
                defs = hardwareValue;
            }
        }
        return defs;
    }

    public static synchronized String getHardwareValue(String key) {
        String str;
        synchronized (HardwareList.class) {
            if (f29c == null) {
                str = null;
            } else {
                str = (String) f29c.get(key);
            }
        }
        return str;
    }
}
