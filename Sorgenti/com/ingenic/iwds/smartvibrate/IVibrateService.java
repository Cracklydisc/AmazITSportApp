package com.ingenic.iwds.smartvibrate;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVibrateService extends IInterface {

    public static abstract class Stub extends Binder implements IVibrateService {

        private static class C1204a implements IVibrateService {
            private IBinder f185a;

            C1204a(IBinder iBinder) {
                this.f185a = iBinder;
            }

            public IBinder asBinder() {
                return this.f185a;
            }

            public int Drv2605Vibrate(int[] effect) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartvibrate.IVibrateService");
                    obtain.writeIntArray(effect);
                    this.f185a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    obtain2.readIntArray(effect);
                    return readInt;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.smartvibrate.IVibrateService");
        }

        public static IVibrateService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.smartvibrate.IVibrateService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IVibrateService)) {
                return new C1204a(obj);
            }
            return (IVibrateService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.smartvibrate.IVibrateService");
                    int[] createIntArray = data.createIntArray();
                    int Drv2605Vibrate = Drv2605Vibrate(createIntArray);
                    reply.writeNoException();
                    reply.writeInt(Drv2605Vibrate);
                    reply.writeIntArray(createIntArray);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.smartvibrate.IVibrateService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    int Drv2605Vibrate(int[] iArr) throws RemoteException;
}
