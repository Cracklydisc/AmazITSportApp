package com.ingenic.iwds.smartvibrate;

import android.os.IBinder;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartvibrate.IVibrateService.Stub;

public class VibrateServiceManager extends ServiceManagerContext {
    private IVibrateService f188b;

    class C12061 extends ServiceClientProxy {
        final /* synthetic */ VibrateServiceManager f187a;

        public void onServiceConnected(IBinder service) {
            this.f187a.f188b = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
        }
    }

    public class VibrateModes {
    }
}
