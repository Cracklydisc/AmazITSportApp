package com.ingenic.iwds.smartvibrate;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.ingenic.iwds.smartvibrate.IVibrateService.Stub;
import com.ingenic.iwds.utils.IwdsLog;

public class VibrateService extends Service {
    private VibrateServiceStub f186a = new VibrateServiceStub();

    private static class VibrateServiceStub extends Stub {
        private static final native int nativeSpecialVibrate(int[] iArr);

        private VibrateServiceStub() {
        }

        public int Drv2605Vibrate(int[] effect) throws RemoteException {
            return nativeSpecialVibrate(effect);
        }
    }

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind()");
        return this.f186a;
    }
}
