package com.ingenic.iwds.remotedevice;

public interface RemoteDeviceSettingListener {
    void onDoneSetting(int i, int i2);

    void onGetSetting(int i, int i2);
}
