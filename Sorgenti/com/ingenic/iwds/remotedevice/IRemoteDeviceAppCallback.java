package com.ingenic.iwds.remotedevice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDeviceAppCallback extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteDeviceAppCallback {

        private static class Proxy implements IRemoteDeviceAppCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onRemoteAppInfoListAvailable(RemoteApplicationInfoList appInfoList) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (appInfoList != null) {
                        obtain.writeInt(1);
                        appInfoList.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onRemoteStorageInfoAvailable(RemoteStorageInfo storageInfo) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (storageInfo != null) {
                        obtain.writeInt(1);
                        storageInfo.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onSendFileProgressForInstall(String packageName, int progress) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    obtain.writeString(packageName);
                    obtain.writeInt(progress);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDoneInstallApp(String packageName, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    obtain.writeString(packageName);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDoneDeleteApp(String packageName, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    obtain.writeString(packageName);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onResponsePkgSizeInfo(RemotePackageStats stats, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (stats != null) {
                        obtain.writeInt(1);
                        stats.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onResponseClearAppDataOrCache(String packageName, int requestType, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    obtain.writeString(packageName);
                    obtain.writeInt(requestType);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onResponseClearAllAppDataAndCache(int totalClearCount, int index, String packageName, int typeOfIndex, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    obtain.writeInt(totalClearCount);
                    obtain.writeInt(index);
                    obtain.writeString(packageName);
                    obtain.writeInt(typeOfIndex);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
        }

        public static IRemoteDeviceAppCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteDeviceAppCallback)) {
                return new Proxy(obj);
            }
            return (IRemoteDeviceAppCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            RemotePackageStats remotePackageStats = null;
            switch (code) {
                case 1:
                    RemoteApplicationInfoList remoteApplicationInfoList;
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (data.readInt() != 0) {
                        remoteApplicationInfoList = (RemoteApplicationInfoList) RemoteApplicationInfoList.CREATOR.createFromParcel(data);
                    }
                    onRemoteAppInfoListAvailable(remoteApplicationInfoList);
                    reply.writeNoException();
                    return true;
                case 2:
                    RemoteStorageInfo remoteStorageInfo;
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (data.readInt() != 0) {
                        remoteStorageInfo = (RemoteStorageInfo) RemoteStorageInfo.CREATOR.createFromParcel(data);
                    }
                    onRemoteStorageInfoAvailable(remoteStorageInfo);
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    onSendFileProgressForInstall(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    onDoneInstallApp(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    onDoneDeleteApp(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    if (data.readInt() != 0) {
                        remotePackageStats = (RemotePackageStats) RemotePackageStats.CREATOR.createFromParcel(data);
                    }
                    onResponsePkgSizeInfo(remotePackageStats, data.readInt());
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    onResponseClearAppDataOrCache(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    onResponseClearAllAppDataAndCache(data.readInt(), data.readInt(), data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onDoneDeleteApp(String str, int i) throws RemoteException;

    void onDoneInstallApp(String str, int i) throws RemoteException;

    void onRemoteAppInfoListAvailable(RemoteApplicationInfoList remoteApplicationInfoList) throws RemoteException;

    void onRemoteStorageInfoAvailable(RemoteStorageInfo remoteStorageInfo) throws RemoteException;

    void onResponseClearAllAppDataAndCache(int i, int i2, String str, int i3, int i4) throws RemoteException;

    void onResponseClearAppDataOrCache(String str, int i, int i2) throws RemoteException;

    void onResponsePkgSizeInfo(RemotePackageStats remotePackageStats, int i) throws RemoteException;

    void onSendFileProgressForInstall(String str, int i) throws RemoteException;
}
