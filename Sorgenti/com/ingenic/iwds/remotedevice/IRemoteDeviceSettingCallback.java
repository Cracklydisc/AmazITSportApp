package com.ingenic.iwds.remotedevice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDeviceSettingCallback extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteDeviceSettingCallback {

        private static class Proxy implements IRemoteDeviceSettingCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onDoneSetting(int type, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
                    obtain.writeInt(type);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onGetSetting(int type, int returnCode) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
                    obtain.writeInt(type);
                    obtain.writeInt(returnCode);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
        }

        public static IRemoteDeviceSettingCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteDeviceSettingCallback)) {
                return new Proxy(obj);
            }
            return (IRemoteDeviceSettingCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
                    onDoneSetting(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
                    onGetSetting(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onDoneSetting(int i, int i2) throws RemoteException;

    void onGetSetting(int i, int i2) throws RemoteException;
}
