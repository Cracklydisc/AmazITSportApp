package com.ingenic.iwds.remotedevice;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

class RemoteApplicationInfoList implements Parcelable {
    public static final Creator<RemoteApplicationInfoList> CREATOR = new C11561();
    private List<RemoteApplicationInfo> remoteAppInfoList;

    static class C11561 implements Creator<RemoteApplicationInfoList> {
        C11561() {
        }

        public RemoteApplicationInfoList createFromParcel(Parcel source) {
            return new RemoteApplicationInfoList(source);
        }

        public RemoteApplicationInfoList[] newArray(int size) {
            return new RemoteApplicationInfoList[size];
        }
    }

    public RemoteApplicationInfoList(PackageManager pm, List<PackageInfo> pkgList) {
        this.remoteAppInfoList = new ArrayList();
        for (PackageInfo remoteApplicationInfo : pkgList) {
            this.remoteAppInfoList.add(new RemoteApplicationInfo(pm, remoteApplicationInfo));
        }
    }

    private RemoteApplicationInfoList(Parcel in) {
        this.remoteAppInfoList = new ArrayList();
        in.readList(this.remoteAppInfoList, RemoteApplicationInfo.class.getClassLoader());
    }

    public List<RemoteApplicationInfo> getList() {
        return this.remoteAppInfoList;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.remoteAppInfoList);
    }
}
