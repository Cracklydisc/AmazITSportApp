package com.ingenic.iwds.remotedevice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDeviceStatusCallback extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteDeviceStatusCallback {

        private static class Proxy implements IRemoteDeviceStatusCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onRemoteDeviceReady(boolean isReady) throws RemoteException {
                int i = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback");
                    if (!isReady) {
                        i = 0;
                    }
                    obtain.writeInt(i);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback");
        }

        public static IRemoteDeviceStatusCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteDeviceStatusCallback)) {
                return new Proxy(obj);
            }
            return (IRemoteDeviceStatusCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback");
                    onRemoteDeviceReady(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onRemoteDeviceReady(boolean z) throws RemoteException;
}
