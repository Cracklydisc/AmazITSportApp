package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class RemoteProcessInfo implements Parcelable {
    public static final Creator<RemoteProcessInfo> CREATOR = new C11741();
    public int memSize;
    public int pid;
    public String processName;
    public int uid;

    static class C11741 implements Creator<RemoteProcessInfo> {
        C11741() {
        }

        public RemoteProcessInfo createFromParcel(Parcel source) {
            return new RemoteProcessInfo(source);
        }

        public RemoteProcessInfo[] newArray(int size) {
            return new RemoteProcessInfo[size];
        }
    }

    public RemoteProcessInfo(String processName, int pid, int uid, int memSize) {
        this.processName = processName;
        this.pid = pid;
        this.uid = uid;
        this.memSize = memSize;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.processName);
        dest.writeInt(this.pid);
        dest.writeInt(this.uid);
        dest.writeInt(this.memSize);
    }

    void readFromParcel(Parcel source) {
        this.processName = source.readString();
        this.pid = source.readInt();
        this.uid = source.readInt();
        this.memSize = source.readInt();
    }

    private RemoteProcessInfo(Parcel source) {
        readFromParcel(source);
    }
}
