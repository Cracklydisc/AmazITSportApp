package com.ingenic.iwds.remotedevice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDeviceService extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteDeviceService {

        private static class Proxy implements IRemoteDeviceService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public boolean registerStatusListener(String uuid, IRemoteDeviceStatusCallback callback) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterStatusListener(IRemoteDeviceStatusCallback callback) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean registerAppListener(String uuid, IRemoteDeviceAppCallback callback) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterAppListener(IRemoteDeviceAppCallback callback) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean registerProcessListener(String uuid, IRemoteDeviceProcessCallback callback) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterProcessListener(IRemoteDeviceProcessCallback callback) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean registerSettingListener(String uuid, IRemoteDeviceSettingCallback callback) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterSettingListener(IRemoteDeviceSettingCallback callback) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestGetAppList(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestGetStorageInfo(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestInstallApp(String uuid, String apkFilePath, boolean isInstalledInExternal) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeString(apkFilePath);
                    if (isInstalledInExternal) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestDeleteApp(String uuid, String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeString(packageName);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestPkgSizeInfo(String uuid, String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeString(packageName);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestClearAppDataOrCache(String uuid, String packageName, int requestType) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeString(packageName);
                    obtain.writeInt(requestType);
                    this.mRemote.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestClearAllAppDataAndCache(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestSystemMemoryInfo(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestRunningAppProcessInfo(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestKillProcess(String uuid, String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeString(packageName);
                    this.mRemote.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestDoSetting(String uuid, int type, int value) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeInt(type);
                    obtain.writeInt(value);
                    this.mRemote.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void requestGetSetting(String uuid, int type) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    obtain.writeString(uuid);
                    obtain.writeInt(type);
                    this.mRemote.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.remotedevice.IRemoteDeviceService");
        }

        public static IRemoteDeviceService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteDeviceService)) {
                return new Proxy(obj);
            }
            return (IRemoteDeviceService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean z = false;
            boolean registerStatusListener;
            int i;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    registerStatusListener = registerStatusListener(data.readString(), com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (registerStatusListener) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    unregisterStatusListener(com.ingenic.iwds.remotedevice.IRemoteDeviceStatusCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    registerStatusListener = registerAppListener(data.readString(), com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (registerStatusListener) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    unregisterAppListener(com.ingenic.iwds.remotedevice.IRemoteDeviceAppCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    registerStatusListener = registerProcessListener(data.readString(), com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (registerStatusListener) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    unregisterProcessListener(com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    registerStatusListener = registerSettingListener(data.readString(), com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (registerStatusListener) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 8:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    unregisterSettingListener(com.ingenic.iwds.remotedevice.IRemoteDeviceSettingCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestGetAppList(data.readString());
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestGetStorageInfo(data.readString());
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    String readString = data.readString();
                    String readString2 = data.readString();
                    if (data.readInt() != 0) {
                        z = true;
                    }
                    requestInstallApp(readString, readString2, z);
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestDeleteApp(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestPkgSizeInfo(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestClearAppDataOrCache(data.readString(), data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestClearAllAppDataAndCache(data.readString());
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestSystemMemoryInfo(data.readString());
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestRunningAppProcessInfo(data.readString());
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestKillProcess(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestDoSetting(data.readString(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    requestGetSetting(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.remotedevice.IRemoteDeviceService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    boolean registerAppListener(String str, IRemoteDeviceAppCallback iRemoteDeviceAppCallback) throws RemoteException;

    boolean registerProcessListener(String str, IRemoteDeviceProcessCallback iRemoteDeviceProcessCallback) throws RemoteException;

    boolean registerSettingListener(String str, IRemoteDeviceSettingCallback iRemoteDeviceSettingCallback) throws RemoteException;

    boolean registerStatusListener(String str, IRemoteDeviceStatusCallback iRemoteDeviceStatusCallback) throws RemoteException;

    void requestClearAllAppDataAndCache(String str) throws RemoteException;

    void requestClearAppDataOrCache(String str, String str2, int i) throws RemoteException;

    void requestDeleteApp(String str, String str2) throws RemoteException;

    void requestDoSetting(String str, int i, int i2) throws RemoteException;

    void requestGetAppList(String str) throws RemoteException;

    void requestGetSetting(String str, int i) throws RemoteException;

    void requestGetStorageInfo(String str) throws RemoteException;

    void requestInstallApp(String str, String str2, boolean z) throws RemoteException;

    void requestKillProcess(String str, String str2) throws RemoteException;

    void requestPkgSizeInfo(String str, String str2) throws RemoteException;

    void requestRunningAppProcessInfo(String str) throws RemoteException;

    void requestSystemMemoryInfo(String str) throws RemoteException;

    void unregisterAppListener(IRemoteDeviceAppCallback iRemoteDeviceAppCallback) throws RemoteException;

    void unregisterProcessListener(IRemoteDeviceProcessCallback iRemoteDeviceProcessCallback) throws RemoteException;

    void unregisterSettingListener(IRemoteDeviceSettingCallback iRemoteDeviceSettingCallback) throws RemoteException;

    void unregisterStatusListener(IRemoteDeviceStatusCallback iRemoteDeviceStatusCallback) throws RemoteException;
}
