package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

/* compiled from: RemoteProcessInfo */
class RemoteProcessInfoList implements Parcelable {
    public static final Creator<RemoteProcessInfoList> CREATOR = new C11751();
    List<RemoteProcessInfo> processInfoList;

    /* compiled from: RemoteProcessInfo */
    static class C11751 implements Creator<RemoteProcessInfoList> {
        C11751() {
        }

        public RemoteProcessInfoList createFromParcel(Parcel source) {
            return new RemoteProcessInfoList(source);
        }

        public RemoteProcessInfoList[] newArray(int size) {
            return new RemoteProcessInfoList[size];
        }
    }

    public RemoteProcessInfoList() {
        this.processInfoList = new ArrayList();
    }

    public void add(RemoteProcessInfo processInfo) {
        this.processInfoList.add(processInfo);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.processInfoList);
    }

    private RemoteProcessInfoList(Parcel source) {
        this.processInfoList = new ArrayList();
        source.readList(this.processInfoList, RemoteProcessInfo.class.getClassLoader());
    }
}
