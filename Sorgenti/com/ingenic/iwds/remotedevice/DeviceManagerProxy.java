package com.ingenic.iwds.remotedevice;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDataObserver.Stub;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageInstallObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.devicemanager.DeviceManagerServiceManager;
import com.ingenic.iwds.utils.IwdsLog;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@SuppressLint({"NewApi"})
public class DeviceManagerProxy {
    private static String mAppUuid = "C3554F59-EA68-84F1-8C79-96907EF327D6";
    private ActivityManager mAM;
    private DataTransactor mAppTransactor;
    private DataTransactorCallback mAppTransactorCallback = new C11542();
    public Context mContext;
    private DeviceManagerServiceManager mDM;
    private ConnectionCallbacks mDMConnectionCallbacks = new C11531();
    private ServiceClient mDMServiceClient;
    private HashMap<String, Boolean> mInstallAppLocations;
    private PackageManager mPM;

    class C11531 implements ConnectionCallbacks {
        C11531() {
        }

        public void onConnected(ServiceClient serviceClient) {
            DeviceManagerProxy.this.mDM = (DeviceManagerServiceManager) DeviceManagerProxy.this.mDMServiceClient.getServiceManagerContext();
            DeviceManagerProxy.this.notifyDMServieConnected(true);
        }

        public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
            IwdsLog.m267i(this, "device management service disconnected");
            DeviceManagerProxy.this.notifyDMServieConnected(false);
        }

        public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
            IwdsLog.m265e((Object) this, "Failed to connect to device management service: " + reason.toString());
        }
    }

    class C11542 implements DataTransactorCallback {
        C11542() {
        }

        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        }

        public void onChannelAvailable(boolean isAvailable) {
            if (isAvailable) {
                DeviceManagerProxy.this.mDMServiceClient.connect();
            } else {
                DeviceManagerProxy.this.mDMServiceClient.disconnect();
            }
        }

        public void onSendResult(DataTransactResult result) {
            if (result.getResultCode() != 0) {
                IwdsLog.m265e((Object) this, "send failed, error code " + result.getResultCode());
            }
        }

        public void onDataArrived(Object object) {
            if (object instanceof File) {
                DeviceManagerProxy.this.onApkFileReceived((File) object);
                return;
            }
            RemoteRequest remoteRequest = (RemoteRequest) object;
            switch (remoteRequest.type) {
                case 0:
                case 1:
                    DeviceManagerProxy.this.onRequestDeleteAppDataOrCache(remoteRequest);
                    return;
                case 2:
                    DeviceManagerProxy.this.onRequestDeleteAllAppDataAndCache();
                    return;
                case 3:
                    DeviceManagerProxy.this.onRequestGetAppList();
                    return;
                case 4:
                    DeviceManagerProxy.this.onRequestGetStorageInfo();
                    return;
                case 5:
                    DeviceManagerProxy.this.onRequestInstallApp(remoteRequest);
                    return;
                case 7:
                    DeviceManagerProxy.this.onRequestDeleteApp(remoteRequest.packageName);
                    return;
                case 8:
                    DeviceManagerProxy.this.onRequestPkgSizeInfo(remoteRequest.packageName);
                    return;
                case 9:
                    DeviceManagerProxy.this.onRequestSystemMemoryInfo();
                    return;
                case 10:
                    DeviceManagerProxy.this.onRequestRunningAppProcessInfo();
                    return;
                case 11:
                    DeviceManagerProxy.this.onRequestKillProcess(remoteRequest.packageName);
                    return;
                case 12:
                    DeviceManagerProxy.this.onRequestDoSetting(remoteRequest.subType, remoteRequest.value);
                    return;
                case 13:
                    DeviceManagerProxy.this.onRequestGetSetting(remoteRequest.subType);
                    return;
                default:
                    return;
            }
        }

        public void onSendFileProgress(int progress) {
        }

        public void onRecvFileProgress(int progress) {
        }
    }

    private class ClearAllAppDataCacheObserver extends Stub {
        private int mIndex = 0;
        private int mTotalClearCount;

        public ClearAllAppDataCacheObserver(List<PackageInfo> clearPkgList) {
            this.mTotalClearCount = clearPkgList.size() * 2;
        }

        public void onRemoveCompleted(String packageName, boolean isSucceeded) throws RemoteException {
            int i = 1;
            int i2 = isSucceeded ? 0 : -119;
            this.mIndex++;
            if ((this.mIndex & 1) == 1) {
                i = 0;
            }
            Object clearAllAppDataCacheResponse = new ClearAllAppDataCacheResponse(this.mTotalClearCount, this.mIndex, packageName, i, i2);
            if (i == 0) {
                DeviceManagerProxy.this.sendClearAppDataBroadcast(packageName);
            }
            DeviceManagerProxy.this.mAppTransactor.send(clearAllAppDataCacheResponse);
        }
    }

    private class ClearCacheObserver extends Stub {
        private ClearCacheObserver() {
        }

        public void onRemoveCompleted(String packageName, boolean isSucceeded) throws RemoteException {
            DeviceManagerProxy.this.mAppTransactor.send(new ResponseWithName(packageName, 1, isSucceeded ? 0 : -119));
        }
    }

    private class ClearDataObserver extends Stub {
        private ClearDataObserver() {
        }

        public void onRemoveCompleted(String packageName, boolean isSucceeded) throws RemoteException {
            Object responseWithName = new ResponseWithName(packageName, 0, isSucceeded ? 0 : -119);
            DeviceManagerProxy.this.sendClearAppDataBroadcast(packageName);
            DeviceManagerProxy.this.mAppTransactor.send(responseWithName);
        }
    }

    private class PackageDeleteObserver extends IPackageDeleteObserver.Stub {
        private PackageDeleteObserver() {
        }

        public void packageDeleted(String packageName, int returnCode) {
            IwdsLog.m264d(this, " package: " + packageName + " deletion return code = " + returnCode);
            if (returnCode != 1) {
                returnCode = RemoteDeviceManagerInfo.toElfReturnCode(returnCode);
            }
            DeviceManagerProxy.this.mAppTransactor.send(new ResponseWithName(packageName, 7, returnCode));
        }
    }

    private class PackageInstallObserver extends IPackageInstallObserver.Stub {
        private PackageInstallObserver() {
        }

        public void packageInstalled(String packageName, int returnCode) {
            IwdsLog.m264d(this, " package: " + packageName + " installation return code = " + returnCode);
            DeviceManagerProxy.this.mAppTransactor.send(new ResponseWithName(packageName, 5, returnCode));
        }
    }

    private class PackageStatsObserver extends IPackageStatsObserver.Stub {
        private PackageStatsObserver() {
        }

        public void onGetStatsCompleted(PackageStats stats, boolean isSucceeded) throws RemoteException {
            String str = stats.packageName;
            IwdsLog.m264d(this, " package: " + str + " get pkgSizeInfo isSucceeded?  " + isSucceeded);
            RemotePackageStats remotePackageStats = new RemotePackageStats(stats);
            Object pkgInfoResponse = new PkgInfoResponse(str, isSucceeded ? 0 : -119);
            pkgInfoResponse.pkgStats = remotePackageStats;
            DeviceManagerProxy.this.mAppTransactor.send(pkgInfoResponse);
        }
    }

    private DeviceManagerProxy() {
    }

    private void onRequestGetAppList() {
        IwdsLog.m264d(this, " received request to get app list.");
        this.mAppTransactor.send(new AppListResponse(new RemoteApplicationInfoList(this.mPM, this.mPM.getInstalledPackages(8192))));
    }

    private void onRequestGetStorageInfo() {
        long freeSpace;
        long j = 0;
        File dataDirectory = Environment.getDataDirectory();
        long freeSpace2 = dataDirectory.getFreeSpace();
        long totalSpace = dataDirectory.getTotalSpace();
        dataDirectory = new File("/storage/sdcard1");
        boolean isDirectory = dataDirectory.isDirectory();
        if (isDirectory) {
            freeSpace = dataDirectory.getFreeSpace();
            j = dataDirectory.getTotalSpace();
        } else {
            freeSpace = 0;
        }
        this.mAppTransactor.send(new StorageInfoResponse(new RemoteStorageInfo(freeSpace2, totalSpace, isDirectory, freeSpace, j)));
    }

    private void onRequestInstallApp(RemoteRequest request) {
        long usableSpace = Environment.getExternalStorageDirectory().getUsableSpace();
        long j = request.requiredSize;
        int i = 1;
        String str = request.apkFilePath;
        if (usableSpace < j) {
            i = -4;
        }
        IwdsLog.m264d(this, "usableSpace: " + usableSpace + " bytes, requiredSize: " + j + " bytes, errorCode: " + i);
        this.mInstallAppLocations.put(str.substring(str.lastIndexOf(47) + 1), Boolean.valueOf(request.isInstalledInExternal));
        this.mAppTransactor.send(new ConfirmInstallResponse(request.packageName, str, i));
    }

    private void onApkFileReceived(File apkFile) {
        int i;
        IwdsLog.m264d(this, apkFile + " received for installation. size: " + apkFile.length());
        Uri fromFile = Uri.fromFile(apkFile);
        IPackageInstallObserver packageInstallObserver = new PackageInstallObserver();
        String str = "";
        if (((Boolean) this.mInstallAppLocations.remove(apkFile.getName())).booleanValue()) {
            i = 10;
        } else {
            i = 18;
        }
        this.mPM.installPackage(fromFile, packageInstallObserver, i, str);
    }

    private void onRequestDeleteApp(String packageName) {
        IwdsLog.m264d(this, " on request to delete " + packageName);
        this.mPM.deletePackage(packageName, new PackageDeleteObserver(), 2);
    }

    private void onRequestPkgSizeInfo(String packageName) {
        IwdsLog.m264d(this, " on request to get " + packageName + " sizeInfo");
        this.mPM.getPackageSizeInfo(packageName, new PackageStatsObserver());
    }

    private void onRequestDeleteAppDataOrCache(RemoteRequest request) {
        String str = request.packageName;
        if (request.type == 0) {
            IwdsLog.m264d(this, " on request to clear user data of " + str);
            this.mAM.clearApplicationUserData(str, new ClearDataObserver());
            return;
        }
        IwdsLog.m264d(this, " on request to clear cache of " + str);
        this.mPM.deleteApplicationCacheFiles(str, new ClearCacheObserver());
    }

    private void sendClearAppDataBroadcast(String packageName) {
        packageName = packageName.replace(".", "_").toUpperCase(Locale.getDefault());
        this.mContext.sendBroadcast(new Intent(String.format("ingenic.intent.action.%s_DATA_CLEARED", new Object[]{packageName})));
    }

    private void onRequestDeleteAllAppDataAndCache() {
        List<PackageInfo> installedPackages = this.mPM.getInstalledPackages(8192);
        List<PackageInfo> arrayList = new ArrayList();
        for (PackageInfo packageInfo : installedPackages) {
            String str = packageInfo.packageName;
            if (!(!str.contains("com.ingenic") || str.equals("com.ingenic.iwds.device") || str.equals("com.ingenic.launcher"))) {
                arrayList.add(packageInfo);
            }
        }
        IPackageDataObserver clearAllAppDataCacheObserver = new ClearAllAppDataCacheObserver(arrayList);
        for (PackageInfo packageInfo2 : arrayList) {
            String str2 = packageInfo2.packageName;
            this.mPM.deleteApplicationCacheFiles(str2, clearAllAppDataCacheObserver);
            this.mAM.clearApplicationUserData(str2, clearAllAppDataCacheObserver);
        }
    }

    private void onRequestSystemMemoryInfo() {
        MemoryInfo memoryInfo = new MemoryInfo();
        this.mAM.getMemoryInfo(memoryInfo);
        this.mAppTransactor.send(new SysMemResponse(memoryInfo.availMem, memoryInfo.totalMem));
    }

    private void onRequestRunningAppProcessInfo() {
        RemoteProcessInfoList remoteProcessInfoList = new RemoteProcessInfoList();
        for (RunningAppProcessInfo runningAppProcessInfo : this.mAM.getRunningAppProcesses()) {
            int i = runningAppProcessInfo.pid;
            int i2 = runningAppProcessInfo.uid;
            String str = runningAppProcessInfo.processName;
            int totalPss = this.mAM.getProcessMemoryInfo(new int[]{i})[0].getTotalPss();
            IwdsLog.m264d(this, "processName: " + str + "  pid: " + i + " uid:" + i2 + " memorySize is -->" + totalPss + "kB");
            remoteProcessInfoList.add(new RemoteProcessInfo(str, i, i2, totalPss));
            String[] strArr = runningAppProcessInfo.pkgList;
            IwdsLog.m264d(this, "process id is " + i + " has " + strArr.length);
            for (String str2 : strArr) {
                IwdsLog.m264d(this, " packageName " + str2 + " in process id is -->" + i);
            }
        }
        this.mAppTransactor.send(new ProcessInfoResponse(remoteProcessInfoList));
    }

    private void onRequestKillProcess(String packageName) {
        IwdsLog.m264d(this, " on request to kill " + packageName);
        this.mAM.forceStopPackage(packageName);
        this.mAppTransactor.send(new ResponseWithName(packageName, 11, 0));
    }

    private void onRequestDoSetting(int subType, int value) {
        boolean z = true;
        switch (subType) {
            case 0:
                if (value != 1) {
                    z = false;
                }
                this.mDM.setWearOnRightHand(z);
                break;
        }
        this.mAppTransactor.send(new SettingResponse(12, subType, value));
    }

    private void onRequestGetSetting(int subType) {
        int i = -119;
        switch (subType) {
            case 0:
                if (!this.mDM.isWearOnRightHand()) {
                    i = 2;
                    break;
                } else {
                    i = 1;
                    break;
                }
        }
        this.mAppTransactor.send(new SettingResponse(13, subType, i));
    }

    private void notifyDMServieConnected(boolean isConnected) {
        this.mAppTransactor.send(new RemoteResponse(isConnected ? 20 : 21));
    }
}
