package com.ingenic.iwds.remotedevice;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.SparseArray;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.remotedevice.IRemoteDeviceService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

public class RemoteDeviceService extends Service {
    private static String mAppUuid = "C3554F59-EA68-84F1-8C79-96907EF327D6";
    private RemoteCallbackList<IRemoteDeviceAppCallback> mAppCallbacks;
    private DataTransactor mAppTransactor;
    private AppTransactorCallback mAppTransactorCallback = new AppTransactorCallback();
    private LinkedList<String> mClearAllAppDataUUIDs;
    private HashMap<String, String> mClearAppDataUUIDs;
    private LinkedHashMap<String, String> mDeleteAppUUIDs;
    private LinkedList<String> mDoSettingUUIDs;
    private LinkedList<String> mGetAppListUUIDs;
    private HashMap<String, String> mGetPkgSizeUUIDs;
    private LinkedList<String> mGetRunningProcessUUIDs;
    private LinkedList<String> mGetSettingUUIDs;
    private LinkedList<String> mGetStorgeInfoUUIDs;
    private LinkedList<String> mGetSysMemUUIDs;
    private ServiceHandler mHandler;
    private HandlerThread mHandlerThread;
    private LinkedHashMap<String, String> mInstallAppUUIDs;
    private LinkedList<String> mKillProcessUUIDs;
    private ListenerRegistration mListenerRegistration;
    private RemoteCallbackList<IRemoteDeviceProcessCallback> mProcessCallbacks;
    private RemoteDeviceServiceStub mService = new RemoteDeviceServiceStub();
    private RemoteCallbackList<IRemoteDeviceSettingCallback> mSettingCallbacks;
    private RemoteCallbackList<IRemoteDeviceStatusCallback> mStatusCallbacks;

    private class AppTransactorCallback implements DataTransactorCallback {
        private AppTransactorCallback() {
        }

        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        }

        public void onChannelAvailable(boolean isAvailable) {
            RemoteDeviceService.this.mHandler.onRemoteDeviceStatusChanged(isAvailable);
        }

        public void onSendResult(DataTransactResult result) {
            if ((result.getTransferedObject() instanceof File) && result.getResultCode() != 0) {
                RemoteDeviceService.this.mHandler.handleAppResponse(new ResponseWithName(5, -105));
            }
        }

        public void onDataArrived(Object object) {
            if (object instanceof RemoteResponse) {
                RemoteDeviceService.this.mHandler.handleAppResponse((RemoteResponse) object);
            }
        }

        public void onSendFileProgress(int progress) {
            RemoteDeviceService.this.mHandler.onSendFileProgress(progress);
        }

        public void onRecvFileProgress(int progress) {
        }
    }

    private class ListenerRegistration {
        SparseArray<HashMap<IInterface, String>> mCallbacks = new SparseArray();

        ListenerRegistration() {
            this.mCallbacks.put(0, new HashMap());
            this.mCallbacks.put(1, new HashMap());
            this.mCallbacks.put(2, new HashMap());
            this.mCallbacks.put(3, new HashMap());
        }

        void addListener(int type, String uuid, IInterface callback) {
            ((HashMap) this.mCallbacks.get(type)).put(callback, uuid);
        }

        String removeListener(int type, IInterface callback) {
            return (String) ((HashMap) this.mCallbacks.get(type)).remove(callback);
        }

        boolean isListenerRegistered(int type, IInterface callback) {
            return ((HashMap) this.mCallbacks.get(type)).containsKey(callback);
        }
    }

    private class RemoteDeviceServiceStub extends Stub {
        private RemoteDeviceServiceStub() {
        }

        public boolean registerStatusListener(String uuid, IRemoteDeviceStatusCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.registerStatusListener(uuid, callback);
                while (!RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(0, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void unregisterStatusListener(IRemoteDeviceStatusCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.unregisterStatusListener(callback);
                while (RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(0, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public boolean registerAppListener(String uuid, IRemoteDeviceAppCallback callback) throws RemoteException {
            boolean z = true;
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.registerAppListener(uuid, callback);
                while (!RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(1, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                        z = false;
                    }
                }
            }
            return z;
        }

        public void unregisterAppListener(IRemoteDeviceAppCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.unregisterAppListener(callback);
                while (RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(1, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public boolean registerProcessListener(String uuid, IRemoteDeviceProcessCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.registerProcessListener(uuid, callback);
                while (!RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(2, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void unregisterProcessListener(IRemoteDeviceProcessCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.unregisterProcessListener(callback);
                while (RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(2, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public void requestGetAppList(String uuid) throws RemoteException {
            IwdsLog.m264d(this, " request to get AppList, uuid " + uuid);
            RemoteDeviceService.this.mHandler.requestGetAppList(uuid);
        }

        public void requestInstallApp(String uuid, String apkFilePath, boolean isInstalledInExternal) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestInstallApp(uuid, apkFilePath, isInstalledInExternal);
        }

        public void requestDeleteApp(String uuid, String packageName) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestDeleteApp(uuid, packageName);
        }

        public void requestPkgSizeInfo(String uuid, String packageName) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestPkgSizeInfo(uuid, packageName);
        }

        public void requestClearAppDataOrCache(String uuid, String packageName, int requestType) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestClearAppDataOrCache(uuid, packageName, requestType);
        }

        public void requestKillProcess(String uuid, String packageName) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestKillProcess(uuid, packageName);
        }

        public void requestRunningAppProcessInfo(String uuid) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestRunningAppProcessInfo(uuid);
        }

        public void requestSystemMemoryInfo(String uuid) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestSystemMemoryInfo(uuid);
        }

        public void requestClearAllAppDataAndCache(String uuid) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestClearAllAppDataAndCache(uuid);
        }

        public void requestGetStorageInfo(String uuid) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestGetStorageInfo(uuid);
        }

        public boolean registerSettingListener(String uuid, IRemoteDeviceSettingCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.registerSettingListener(uuid, callback);
                while (!RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(3, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void unregisterSettingListener(IRemoteDeviceSettingCallback callback) throws RemoteException {
            synchronized (RemoteDeviceService.this.mListenerRegistration) {
                RemoteDeviceService.this.mHandler.unregisterSettingListener(callback);
                while (RemoteDeviceService.this.mListenerRegistration.isListenerRegistered(3, callback)) {
                    try {
                        RemoteDeviceService.this.mListenerRegistration.wait();
                    } catch (InterruptedException e) {
                    }
                }
            }
        }

        public void requestGetSetting(String uuid, int type) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestGetSetting(uuid, type);
        }

        public void requestDoSetting(String uuid, int type, int value) throws RemoteException {
            RemoteDeviceService.this.mHandler.requestDoSetting(uuid, type, value);
        }
    }

    private class ServiceHandler extends Handler {
        private boolean mIsRemoteDeviceReady = false;
        private boolean mIsRemoteServiceConnected = false;

        private class CallbackMessage<T> {
            T callback;
            String uuid;

            CallbackMessage(String uuid, T callback) {
                this.uuid = uuid;
                this.callback = callback;
            }
        }

        private class MsgObject {
            String apkFilePath;
            boolean isInstalledInExternal;
            String packageName;
            String uuid;

            public MsgObject(String uuid, String apkFilePath, boolean isInstalledInExternal) {
                this.uuid = uuid;
                this.apkFilePath = apkFilePath;
                this.isInstalledInExternal = isInstalledInExternal;
            }

            public MsgObject(String uuid, String packageName) {
                this.uuid = uuid;
                this.packageName = packageName;
            }
        }

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            boolean z = true;
            CallbackMessage callbackMessage;
            String str;
            IRemoteDeviceStatusCallback iRemoteDeviceStatusCallback;
            IRemoteDeviceAppCallback iRemoteDeviceAppCallback;
            IRemoteDeviceProcessCallback iRemoteDeviceProcessCallback;
            IRemoteDeviceSettingCallback iRemoteDeviceSettingCallback;
            String removeListener;
            MsgObject msgObject;
            int i;
            switch (msg.what) {
                case 0:
                    if (msg.arg1 != 1) {
                        z = false;
                    }
                    if (!z) {
                        this.mIsRemoteServiceConnected = false;
                    }
                    doOnRemoteDeviceStatusChanged(z);
                    this.mIsRemoteDeviceReady = z;
                    return;
                case 1:
                    callbackMessage = (CallbackMessage) msg.obj;
                    str = callbackMessage.uuid;
                    iRemoteDeviceStatusCallback = (IRemoteDeviceStatusCallback) callbackMessage.callback;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mStatusCallbacks.register(iRemoteDeviceStatusCallback, str);
                        RemoteDeviceService.this.mListenerRegistration.addListener(0, str, iRemoteDeviceStatusCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    doOnRemoteDeviceStatusChanged(str, this.mIsRemoteDeviceReady);
                    return;
                case 2:
                    callbackMessage = (CallbackMessage) msg.obj;
                    str = callbackMessage.uuid;
                    iRemoteDeviceAppCallback = (IRemoteDeviceAppCallback) callbackMessage.callback;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mAppCallbacks.register(iRemoteDeviceAppCallback, str);
                        IwdsLog.m264d(this, "register app " + str);
                        RemoteDeviceService.this.mListenerRegistration.addListener(1, str, iRemoteDeviceAppCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    return;
                case 3:
                    callbackMessage = (CallbackMessage) msg.obj;
                    str = callbackMessage.uuid;
                    iRemoteDeviceProcessCallback = (IRemoteDeviceProcessCallback) callbackMessage.callback;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mProcessCallbacks.register(iRemoteDeviceProcessCallback, str);
                        IwdsLog.m264d(this, "register process " + str);
                        RemoteDeviceService.this.mListenerRegistration.addListener(2, str, iRemoteDeviceProcessCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    return;
                case 4:
                    callbackMessage = (CallbackMessage) msg.obj;
                    str = callbackMessage.uuid;
                    iRemoteDeviceSettingCallback = (IRemoteDeviceSettingCallback) callbackMessage.callback;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mSettingCallbacks.register(iRemoteDeviceSettingCallback, str);
                        IwdsLog.m264d(this, "register sensor listener " + str);
                        RemoteDeviceService.this.mListenerRegistration.addListener(3, str, iRemoteDeviceSettingCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    return;
                case 10:
                    iRemoteDeviceStatusCallback = (IRemoteDeviceStatusCallback) msg.obj;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mStatusCallbacks.unregister(iRemoteDeviceStatusCallback);
                        RemoteDeviceService.this.mListenerRegistration.removeListener(0, iRemoteDeviceStatusCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    return;
                case 11:
                    iRemoteDeviceAppCallback = (IRemoteDeviceAppCallback) msg.obj;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mAppCallbacks.unregister(iRemoteDeviceAppCallback);
                        removeListener = RemoteDeviceService.this.mListenerRegistration.removeListener(1, iRemoteDeviceAppCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    RemoteDeviceService.this.clearAppRequestStack(removeListener);
                    return;
                case 12:
                    iRemoteDeviceProcessCallback = (IRemoteDeviceProcessCallback) msg.obj;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mProcessCallbacks.unregister(iRemoteDeviceProcessCallback);
                        removeListener = RemoteDeviceService.this.mListenerRegistration.removeListener(2, iRemoteDeviceProcessCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    RemoteDeviceService.this.clearProcessRequestStack(removeListener);
                    return;
                case 13:
                    iRemoteDeviceSettingCallback = (IRemoteDeviceSettingCallback) msg.obj;
                    synchronized (RemoteDeviceService.this.mListenerRegistration) {
                        RemoteDeviceService.this.mSettingCallbacks.unregister(iRemoteDeviceSettingCallback);
                        removeListener = RemoteDeviceService.this.mListenerRegistration.removeListener(3, iRemoteDeviceSettingCallback);
                        RemoteDeviceService.this.mListenerRegistration.notifyAll();
                    }
                    RemoteDeviceService.this.clearSettingRequestStack(removeListener);
                    return;
                case 20:
                    RemoteDeviceService.this.mGetAppListUUIDs.addFirst((String) msg.obj);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(3));
                    return;
                case 21:
                    msgObject = (MsgObject) msg.obj;
                    doRequestInstallApp(msgObject.uuid, msgObject.apkFilePath, msgObject.isInstalledInExternal);
                    return;
                case 22:
                    msgObject = (MsgObject) msg.obj;
                    doRequestDeleteApp(msgObject.uuid, msgObject.packageName);
                    return;
                case 23:
                    msgObject = (MsgObject) msg.obj;
                    doRequestPkgSizeInfo(msgObject.uuid, msgObject.packageName);
                    return;
                case 24:
                    msgObject = (MsgObject) msg.obj;
                    doRequestClearAppDataOrCache(msgObject.uuid, msgObject.packageName, msg.arg1);
                    return;
                case 25:
                    RemoteDeviceService.this.mGetSysMemUUIDs.addFirst((String) msg.obj);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(9));
                    return;
                case 26:
                    msgObject = (MsgObject) msg.obj;
                    RemoteDeviceService.this.mKillProcessUUIDs.addFirst(msgObject.uuid);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(msgObject.packageName, 11));
                    return;
                case 27:
                    RemoteDeviceService.this.mGetRunningProcessUUIDs.addFirst((String) msg.obj);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(10));
                    return;
                case 28:
                    RemoteDeviceService.this.mGetStorgeInfoUUIDs.addFirst((String) msg.obj);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(4));
                    return;
                case 29:
                    removeListener = (String) msg.obj;
                    if (RemoteDeviceService.this.mClearAllAppDataUUIDs.isEmpty() && RemoteDeviceService.this.mClearAppDataUUIDs.isEmpty()) {
                        RemoteDeviceService.this.mClearAllAppDataUUIDs.addFirst(removeListener);
                        RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(2));
                        return;
                    }
                    callbackClearAllAppDataAndCache(removeListener, new ClearAllAppDataCacheResponse(-200));
                    return;
                case 30:
                    removeListener = (String) msg.obj;
                    i = msg.arg1;
                    int i2 = msg.arg2;
                    if (this.mIsRemoteServiceConnected) {
                        RemoteDeviceService.this.mDoSettingUUIDs.addFirst(removeListener);
                        RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(12, i, i2));
                        return;
                    }
                    callbackDoneSetting(removeListener, i, -201);
                    return;
                case 31:
                    removeListener = (String) msg.obj;
                    i = msg.arg1;
                    if (this.mIsRemoteServiceConnected) {
                        RemoteDeviceService.this.mGetSettingUUIDs.addFirst(removeListener);
                        RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(13, i));
                        return;
                    }
                    callbackGetSetting(removeListener, i, -201);
                    return;
                case 40:
                    doOnAppListReceived((AppListResponse) msg.obj);
                    return;
                case 41:
                    doOnConfirmInstallApp((ConfirmInstallResponse) msg.obj);
                    return;
                case 42:
                    doOnDoneInstallApp((ResponseWithName) msg.obj);
                    return;
                case 43:
                    doOnDoneDeleteApp((ResponseWithName) msg.obj);
                    return;
                case 44:
                    doOnResponsePkgSizeInfo((PkgInfoResponse) msg.obj);
                    return;
                case 45:
                    doOnResponseClearAppDataOrCache((ResponseWithName) msg.obj);
                    return;
                case 46:
                    doOnResponseSysMemInfo((SysMemResponse) msg.obj);
                    return;
                case 47:
                    doOnResponseRunningProcesses((ProcessInfoResponse) msg.obj);
                    return;
                case 48:
                    doOnStorageInfoReceived((StorageInfoResponse) msg.obj);
                    return;
                case 49:
                    doOnResponseClearAllAppDataAndCache((ClearAllAppDataCacheResponse) msg.obj);
                    return;
                case 50:
                    doOnKillProcess((ResponseWithName) msg.obj);
                    return;
                case 51:
                    this.mIsRemoteServiceConnected = true;
                    return;
                case 52:
                    this.mIsRemoteServiceConnected = false;
                    return;
                case 53:
                    doOnDoneSetting((SettingResponse) msg.obj);
                    return;
                case 54:
                    doOnGetSetting((SettingResponse) msg.obj);
                    return;
                case 60:
                    doOnSendFileProgressForInstall(msg.arg1);
                    return;
                default:
                    return;
            }
        }

        private void doOnSendFileProgressForInstall(int progress) {
            if (!RemoteDeviceService.this.mInstallAppUUIDs.isEmpty()) {
                String access$2200 = RemoteDeviceService.this.getPackageName(RemoteDeviceService.this.mInstallAppUUIDs);
                String str = (String) RemoteDeviceService.this.mInstallAppUUIDs.get(access$2200);
                int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
                int i = 0;
                while (i < beginBroadcast) {
                    if (str.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                        try {
                            ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onSendFileProgressForInstall(access$2200, progress);
                            break;
                        } catch (RemoteException e) {
                        }
                    } else {
                        i++;
                    }
                }
                RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
            }
        }

        private void doOnRemoteDeviceStatusChanged(String uuid, boolean isReady) {
            int beginBroadcast = RemoteDeviceService.this.mStatusCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mStatusCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceStatusCallback) RemoteDeviceService.this.mStatusCallbacks.getBroadcastItem(i)).onRemoteDeviceReady(isReady);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mStatusCallbacks.finishBroadcast();
        }

        private void doOnRemoteDeviceStatusChanged(boolean isReady) {
            if (!isReady) {
                RemoteDeviceService.this.clearAllRequestStack();
            }
            int beginBroadcast = RemoteDeviceService.this.mStatusCallbacks.beginBroadcast();
            for (int i = 0; i < beginBroadcast; i++) {
                try {
                    ((IRemoteDeviceStatusCallback) RemoteDeviceService.this.mStatusCallbacks.getBroadcastItem(i)).onRemoteDeviceReady(isReady);
                } catch (RemoteException e) {
                }
            }
            RemoteDeviceService.this.mStatusCallbacks.finishBroadcast();
        }

        private void doRequestInstallApp(String uuid, String apkFilePath, boolean isInstalledInExternal) {
            if (new File(apkFilePath).isFile()) {
                PackageInfo packageArchiveInfo = RemoteDeviceService.this.getPackageManager().getPackageArchiveInfo(apkFilePath, 0);
                if (packageArchiveInfo == null) {
                    callbackDoneInstallApp(uuid, apkFilePath, -2);
                    return;
                }
                String str = packageArchiveInfo.packageName;
                IwdsLog.m264d(this, " request to install app " + str + ", uuid " + uuid);
                if (RemoteDeviceService.this.mInstallAppUUIDs.isEmpty()) {
                    RemoteDeviceService.this.mInstallAppUUIDs.put(str, uuid);
                    RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(str, 5, apkFilePath, new File(apkFilePath).length(), isInstalledInExternal));
                    return;
                }
                callbackDoneInstallApp(uuid, str, -200);
                return;
            }
            callbackDoneInstallApp(uuid, apkFilePath, -3);
        }

        private void doRequestDeleteApp(String uuid, String packageName) {
            if (RemoteDeviceService.this.mDeleteAppUUIDs.isEmpty()) {
                RemoteDeviceService.this.mDeleteAppUUIDs.put(packageName, uuid);
                RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(packageName, 7));
                return;
            }
            callbackDoneDeleteApp(uuid, packageName, -200);
        }

        private void doRequestPkgSizeInfo(String uuid, String packageName) {
            if (RemoteDeviceService.this.mGetPkgSizeUUIDs.get(packageName) != null) {
                callbackResponsePkgSizeInfo(uuid, new RemotePackageStats(packageName), -200);
                return;
            }
            RemoteDeviceService.this.mGetPkgSizeUUIDs.put(packageName, uuid);
            RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(packageName, 8));
        }

        private void doRequestClearAppDataOrCache(String uuid, String packageName, int requestType) {
            if (RemoteDeviceService.this.mClearAppDataUUIDs.get(packageName) == null && RemoteDeviceService.this.mClearAllAppDataUUIDs.isEmpty()) {
                RemoteDeviceService.this.mClearAppDataUUIDs.put(packageName, uuid);
                RemoteDeviceService.this.mAppTransactor.send(new RemoteRequest(packageName, requestType));
                return;
            }
            callbackResponseClearAppDataOrCache(uuid, packageName, requestType, -200);
        }

        private void callbackAppListReceived(String uuid, RemoteApplicationInfoList appList) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onRemoteAppInfoListAvailable(appList);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnAppListReceived(AppListResponse response) {
            if (!RemoteDeviceService.this.mGetAppListUUIDs.isEmpty()) {
                callbackAppListReceived((String) RemoteDeviceService.this.mGetAppListUUIDs.removeLast(), response.appList);
            }
        }

        private void callbackStorageInfoReceived(String uuid, RemoteStorageInfo storageInfo) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onRemoteStorageInfoAvailable(storageInfo);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnStorageInfoReceived(StorageInfoResponse response) {
            if (!RemoteDeviceService.this.mGetStorgeInfoUUIDs.isEmpty()) {
                callbackStorageInfoReceived((String) RemoteDeviceService.this.mGetStorgeInfoUUIDs.removeLast(), response.storageInfo);
            }
        }

        private void doOnConfirmInstallApp(ConfirmInstallResponse response) {
            Object obj = 1;
            if (!RemoteDeviceService.this.mInstallAppUUIDs.isEmpty()) {
                int i = response.returnCode;
                File file = new File(response.apkFilePath);
                IwdsLog.m264d(this, "installation confirm, returnCode " + i);
                String access$2200 = RemoteDeviceService.this.getPackageName(RemoteDeviceService.this.mInstallAppUUIDs);
                if (i == 1) {
                    obj = null;
                }
                boolean isFile = file.isFile();
                if (obj == null && isFile) {
                    RemoteDeviceService.this.mAppTransactor.send(file);
                    return;
                }
                int i2;
                if (obj != null) {
                    i2 = i;
                } else {
                    i2 = -3;
                }
                doOnDoneInstallApp(new ResponseWithName(access$2200, 5, i2));
            }
        }

        private void callbackDoneInstallApp(String uuid, String packageName, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onDoneInstallApp(packageName, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnDoneInstallApp(ResponseWithName done) {
            if (!RemoteDeviceService.this.mInstallAppUUIDs.isEmpty()) {
                int i = done.returnCode;
                String access$2200 = RemoteDeviceService.this.getPackageName(RemoteDeviceService.this.mInstallAppUUIDs);
                String str = (String) RemoteDeviceService.this.mInstallAppUUIDs.remove(access$2200);
                IwdsLog.m264d(this, access$2200 + " installation done, return code: " + i + " uuid " + str);
                callbackDoneInstallApp(str, access$2200, i);
            }
        }

        private void callbackDoneDeleteApp(String uuid, String packageName, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onDoneDeleteApp(packageName, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnDoneDeleteApp(ResponseWithName done) {
            if (!RemoteDeviceService.this.mDeleteAppUUIDs.isEmpty()) {
                int i = done.returnCode;
                String access$2200 = RemoteDeviceService.this.getPackageName(RemoteDeviceService.this.mDeleteAppUUIDs);
                String str = (String) RemoteDeviceService.this.mDeleteAppUUIDs.remove(access$2200);
                IwdsLog.m264d(this, access$2200 + " deletion done, return code: " + i + " uuid " + str);
                callbackDoneDeleteApp(str, access$2200, i);
            }
        }

        private void callbackResponsePkgSizeInfo(String uuid, RemotePackageStats stats, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        IwdsLog.m264d(this, " callbackResponsePkgSizeInfo stats " + stats + " name " + stats.packageName + " N " + beginBroadcast + " i " + i);
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onResponsePkgSizeInfo(stats, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnResponsePkgSizeInfo(PkgInfoResponse response) {
            RemotePackageStats remotePackageStats = response.pkgStats;
            String str = response.pkgStats.packageName;
            IwdsLog.m264d(this, " doOnResponsePkgSizeInfo packageName" + str);
            str = (String) RemoteDeviceService.this.mGetPkgSizeUUIDs.remove(str);
            if (str != null) {
                callbackResponsePkgSizeInfo(str, remotePackageStats, response.returnCode);
            }
        }

        private void callbackResponseClearAppDataOrCache(String uuid, String packageName, int requestType, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i)).onResponseClearAppDataOrCache(packageName, requestType, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnResponseClearAppDataOrCache(ResponseWithName response) {
            String str = response.packageName;
            String str2 = (String) RemoteDeviceService.this.mClearAppDataUUIDs.remove(str);
            if (str2 != null) {
                callbackResponseClearAppDataOrCache(str2, str, response.type, response.returnCode);
            }
        }

        private void callbackClearAllAppDataAndCache(String uuid, ClearAllAppDataCacheResponse response) {
            int i = response.totalCount;
            int i2 = response.index;
            String str = response.packageName;
            int i3 = response.typeOfIndex;
            int i4 = response.returnCode;
            int beginBroadcast = RemoteDeviceService.this.mAppCallbacks.beginBroadcast();
            int i5 = 0;
            while (i5 < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mAppCallbacks.getBroadcastCookie(i5))) {
                    try {
                        ((IRemoteDeviceAppCallback) RemoteDeviceService.this.mAppCallbacks.getBroadcastItem(i5)).onResponseClearAllAppDataAndCache(i, i2, str, i3, i4);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i5++;
                }
            }
            RemoteDeviceService.this.mAppCallbacks.finishBroadcast();
        }

        private void doOnResponseClearAllAppDataAndCache(ClearAllAppDataCacheResponse response) {
            if (!RemoteDeviceService.this.mClearAllAppDataUUIDs.isEmpty()) {
                String str = (String) RemoteDeviceService.this.mClearAllAppDataUUIDs.getLast();
                if (response.index == response.totalCount) {
                    RemoteDeviceService.this.mClearAllAppDataUUIDs.removeLast();
                }
                callbackClearAllAppDataAndCache(str, response);
            }
        }

        private void callbackResponseSysMemInfo(String uuid, long availSize, long totalSize) {
            int beginBroadcast = RemoteDeviceService.this.mProcessCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mProcessCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceProcessCallback) RemoteDeviceService.this.mProcessCallbacks.getBroadcastItem(i)).onResponseSystemMemoryInfo(availSize, totalSize);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mProcessCallbacks.finishBroadcast();
        }

        private void doOnResponseSysMemInfo(SysMemResponse response) {
            if (!RemoteDeviceService.this.mGetSysMemUUIDs.isEmpty()) {
                callbackResponseSysMemInfo((String) RemoteDeviceService.this.mGetSysMemUUIDs.removeLast(), response.availSysMemSize, response.totalSysMemSize);
            }
        }

        private void callbackResponseRunningProcesses(String uuid, RemoteProcessInfoList infoList) {
            int beginBroadcast = RemoteDeviceService.this.mProcessCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mProcessCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceProcessCallback) RemoteDeviceService.this.mProcessCallbacks.getBroadcastItem(i)).onResponseRunningAppProcessInfo(infoList);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mProcessCallbacks.finishBroadcast();
        }

        private void doOnResponseRunningProcesses(ProcessInfoResponse response) {
            if (!RemoteDeviceService.this.mGetRunningProcessUUIDs.isEmpty()) {
                callbackResponseRunningProcesses((String) RemoteDeviceService.this.mGetRunningProcessUUIDs.removeLast(), response.processList);
            }
        }

        private void callbackDoneKillProcess(String uuid, String packageName) {
            int beginBroadcast = RemoteDeviceService.this.mProcessCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mProcessCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceProcessCallback) RemoteDeviceService.this.mProcessCallbacks.getBroadcastItem(i)).onDoneKillProcess(packageName);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mProcessCallbacks.finishBroadcast();
        }

        private void doOnKillProcess(ResponseWithName response) {
            if (!RemoteDeviceService.this.mKillProcessUUIDs.isEmpty()) {
                callbackDoneKillProcess((String) RemoteDeviceService.this.mKillProcessUUIDs.removeLast(), response.packageName);
            }
        }

        private void callbackGetSetting(String uuid, int subType, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mSettingCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mSettingCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceSettingCallback) RemoteDeviceService.this.mSettingCallbacks.getBroadcastItem(i)).onGetSetting(subType, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mSettingCallbacks.finishBroadcast();
        }

        private void doOnGetSetting(SettingResponse response) {
            if (!RemoteDeviceService.this.mGetSettingUUIDs.isEmpty()) {
                callbackGetSetting((String) RemoteDeviceService.this.mGetSettingUUIDs.removeLast(), response.subType, response.returnCode);
            }
        }

        private void callbackDoneSetting(String uuid, int subType, int returnCode) {
            int beginBroadcast = RemoteDeviceService.this.mSettingCallbacks.beginBroadcast();
            int i = 0;
            while (i < beginBroadcast) {
                if (uuid.equals(RemoteDeviceService.this.mSettingCallbacks.getBroadcastCookie(i))) {
                    try {
                        ((IRemoteDeviceSettingCallback) RemoteDeviceService.this.mSettingCallbacks.getBroadcastItem(i)).onDoneSetting(subType, returnCode);
                        break;
                    } catch (RemoteException e) {
                    }
                } else {
                    i++;
                }
            }
            RemoteDeviceService.this.mSettingCallbacks.finishBroadcast();
        }

        private void doOnDoneSetting(SettingResponse response) {
            if (!RemoteDeviceService.this.mDoSettingUUIDs.isEmpty()) {
                callbackDoneSetting((String) RemoteDeviceService.this.mDoSettingUUIDs.removeLast(), response.subType, response.returnCode);
            }
        }

        void registerStatusListener(String uuid, IRemoteDeviceStatusCallback callback) {
            Message.obtain(this, 1, new CallbackMessage(uuid, callback)).sendToTarget();
        }

        void unregisterStatusListener(IRemoteDeviceStatusCallback callback) {
            Message.obtain(this, 10, callback).sendToTarget();
        }

        void registerAppListener(String uuid, IRemoteDeviceAppCallback callback) {
            Message.obtain(this, 2, new CallbackMessage(uuid, callback)).sendToTarget();
        }

        void unregisterAppListener(IRemoteDeviceAppCallback callback) {
            Message.obtain(this, 11, callback).sendToTarget();
        }

        void registerProcessListener(String uuid, IRemoteDeviceProcessCallback callback) {
            Message.obtain(this, 3, new CallbackMessage(uuid, callback)).sendToTarget();
        }

        void unregisterProcessListener(IRemoteDeviceProcessCallback callback) {
            Message.obtain(this, 12, callback).sendToTarget();
        }

        void registerSettingListener(String uuid, IRemoteDeviceSettingCallback callback) {
            Message.obtain(this, 4, new CallbackMessage(uuid, callback)).sendToTarget();
        }

        void unregisterSettingListener(IRemoteDeviceSettingCallback callback) {
            Message.obtain(this, 13, callback).sendToTarget();
        }

        void requestGetAppList(String uuid) {
            Message.obtain(this, 20, uuid).sendToTarget();
        }

        void requestGetStorageInfo(String uuid) {
            Message.obtain(this, 28, uuid).sendToTarget();
        }

        void requestInstallApp(String uuid, String apkFilePath, boolean isInstalledInExternal) {
            Message.obtain(this, 21, new MsgObject(uuid, apkFilePath, isInstalledInExternal)).sendToTarget();
        }

        void requestDeleteApp(String uuid, String packageName) {
            Message.obtain(this, 22, new MsgObject(uuid, packageName)).sendToTarget();
        }

        void requestPkgSizeInfo(String uuid, String packageName) {
            Message.obtain(this, 23, new MsgObject(uuid, packageName)).sendToTarget();
        }

        void requestClearAppDataOrCache(String uuid, String packageName, int requestType) {
            Message obtain = Message.obtain(this, 24, new MsgObject(uuid, packageName));
            obtain.arg1 = requestType;
            obtain.sendToTarget();
        }

        void requestClearAllAppDataAndCache(String uuid) {
            Message.obtain(this, 29, uuid).sendToTarget();
        }

        void requestKillProcess(String uuid, String packageName) {
            Message.obtain(this, 26, new MsgObject(uuid, packageName)).sendToTarget();
        }

        void requestRunningAppProcessInfo(String uuid) {
            Message.obtain(this, 27, uuid).sendToTarget();
        }

        void requestSystemMemoryInfo(String uuid) {
            Message.obtain(this, 25, uuid).sendToTarget();
        }

        void requestGetSetting(String uuid, int type) {
            Message obtain = Message.obtain(this, 31, uuid);
            obtain.arg1 = type;
            obtain.sendToTarget();
        }

        void requestDoSetting(String uuid, int type, int value) {
            Message.obtain(this, 30, type, value, uuid).sendToTarget();
        }

        void onRemoteDeviceStatusChanged(boolean isReady) {
            int i = 0;
            Message obtain = Message.obtain(this, 0);
            if (isReady) {
                i = 1;
            }
            obtain.arg1 = i;
            obtain.sendToTarget();
        }

        void onSendFileProgress(int progress) {
            Message obtain = Message.obtain(this, 60);
            obtain.arg1 = progress;
            obtain.sendToTarget();
        }

        void handleAppResponse(RemoteResponse response) {
            Message obtain = Message.obtain(this);
            obtain.obj = response;
            switch (response.type) {
                case 0:
                case 1:
                    obtain.what = 45;
                    break;
                case 2:
                    obtain.what = 49;
                    break;
                case 3:
                    obtain.what = 40;
                    break;
                case 4:
                    obtain.what = 48;
                    break;
                case 5:
                    obtain.what = 42;
                    break;
                case 6:
                    obtain.what = 41;
                    break;
                case 7:
                    obtain.what = 43;
                    break;
                case 8:
                    obtain.what = 44;
                    break;
                case 9:
                    obtain.what = 46;
                    break;
                case 10:
                    obtain.what = 47;
                    break;
                case 11:
                    obtain.what = 50;
                    break;
                case 12:
                    obtain.what = 53;
                    break;
                case 13:
                    obtain.what = 54;
                    break;
                case 20:
                    obtain.what = 51;
                    break;
                case 21:
                    obtain.what = 52;
                    break;
            }
            obtain.sendToTarget();
        }
    }

    public void onCreate() {
        IwdsLog.m264d(this, "onCreate");
        super.onCreate();
        if (this.mAppTransactor == null) {
            this.mAppTransactor = new DataTransactor(this, this.mAppTransactorCallback, mAppUuid);
        }
        this.mHandlerThread = new HandlerThread("RemoteDeviceService");
        this.mHandlerThread.start();
        this.mHandler = new ServiceHandler(this.mHandlerThread.getLooper());
        this.mStatusCallbacks = new RemoteCallbackList();
        this.mAppCallbacks = new RemoteCallbackList();
        this.mProcessCallbacks = new RemoteCallbackList();
        this.mSettingCallbacks = new RemoteCallbackList();
        this.mListenerRegistration = new ListenerRegistration();
        this.mGetAppListUUIDs = new LinkedList();
        this.mGetStorgeInfoUUIDs = new LinkedList();
        this.mClearAllAppDataUUIDs = new LinkedList();
        this.mInstallAppUUIDs = new LinkedHashMap();
        this.mDeleteAppUUIDs = new LinkedHashMap();
        this.mGetPkgSizeUUIDs = new HashMap();
        this.mClearAppDataUUIDs = new HashMap();
        this.mGetSysMemUUIDs = new LinkedList();
        this.mGetRunningProcessUUIDs = new LinkedList();
        this.mKillProcessUUIDs = new LinkedList();
        this.mGetSettingUUIDs = new LinkedList();
        this.mDoSettingUUIDs = new LinkedList();
    }

    public void onDestroy() {
        IwdsLog.m264d(this, "onDestroy");
        super.onDestroy();
        this.mAppTransactor = null;
        this.mHandlerThread.quit();
        try {
            this.mHandlerThread.join();
        } catch (InterruptedException e) {
        }
        this.mHandlerThread = null;
    }

    public boolean onUnbind(Intent intent) {
        IwdsLog.m264d(this, "onUnbind");
        this.mAppTransactor.stop();
        return super.onUnbind(intent);
    }

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind");
        this.mAppTransactor.start();
        return this.mService;
    }

    private void removeListItemByValue(LinkedList<String> list, Object o) {
        while (list.contains(o)) {
            list.remove(o);
        }
    }

    private void removeMapItemByValue(HashMap<String, String> map, Object value) {
        Collection values = map.values();
        while (values.contains(value)) {
            values.remove(value);
        }
    }

    private void clearAppRequestStack(String uuid) {
        removeListItemByValue(this.mGetAppListUUIDs, uuid);
        removeListItemByValue(this.mGetStorgeInfoUUIDs, uuid);
        removeListItemByValue(this.mClearAllAppDataUUIDs, uuid);
        removeMapItemByValue(this.mInstallAppUUIDs, uuid);
        removeMapItemByValue(this.mDeleteAppUUIDs, uuid);
        removeMapItemByValue(this.mGetPkgSizeUUIDs, uuid);
        removeMapItemByValue(this.mClearAppDataUUIDs, uuid);
    }

    private void clearProcessRequestStack(String uuid) {
        removeListItemByValue(this.mGetSysMemUUIDs, uuid);
        removeListItemByValue(this.mGetRunningProcessUUIDs, uuid);
        removeListItemByValue(this.mKillProcessUUIDs, uuid);
    }

    private void clearSettingRequestStack(String uuid) {
        removeListItemByValue(this.mDoSettingUUIDs, uuid);
        removeListItemByValue(this.mGetSettingUUIDs, uuid);
    }

    private void clearAllAppRequestStack() {
        this.mGetAppListUUIDs.clear();
        this.mGetStorgeInfoUUIDs.clear();
        this.mClearAllAppDataUUIDs.clear();
        this.mInstallAppUUIDs.clear();
        this.mDeleteAppUUIDs.clear();
        this.mGetPkgSizeUUIDs.clear();
        this.mClearAppDataUUIDs.clear();
    }

    private void clearProcessRequestStack() {
        this.mGetSysMemUUIDs.clear();
        this.mGetRunningProcessUUIDs.clear();
        this.mKillProcessUUIDs.clear();
    }

    private void clearSensorRequestStack() {
        this.mDoSettingUUIDs.clear();
        this.mGetSettingUUIDs.clear();
    }

    private void clearAllRequestStack() {
        clearAllAppRequestStack();
        clearProcessRequestStack();
        clearSensorRequestStack();
    }

    private String getPackageName(HashMap<String, String> map) {
        boolean z = true;
        if (map.isEmpty()) {
            return null;
        }
        if (map.size() == 1) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "the map should contain only one element");
        return (String) ((Entry) map.entrySet().iterator().next()).getKey();
    }
}
