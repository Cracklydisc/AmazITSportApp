package com.ingenic.iwds.remotedevice;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class RemoteApplicationInfo implements Parcelable {
    public static final Creator<RemoteApplicationInfo> CREATOR = new C11551();
    public ApplicationInfo applicationInfo;
    private String backupAgentName;
    private String className;
    private int compatibleWidthLimitDp;
    private String dataDir;
    private int descriptionRes;
    private boolean enabled;
    private long firstInstallTime;
    private int flags;
    public Bitmap iconBitmap;
    private byte[] iconData;
    public CharSequence label;
    private int largestWidthLimitDp;
    private long lastUpdateTime;
    private String manageSpaceActivityName;
    private String nativeLibraryDir;
    public String packageName;
    private String permission;
    private String processName;
    private String publicSourceDir;
    private int requiresSmallestWidthDp;
    private String sharedUserId;
    private int sharedUserLabel;
    private String sourceDir;
    private int targetSdkVersion;
    private String taskAffinity;
    private int theme;
    private int uiOptions;
    private int uid;
    public int versionCode;
    public String versionName;

    static class C11551 implements Creator<RemoteApplicationInfo> {
        C11551() {
        }

        public RemoteApplicationInfo createFromParcel(Parcel source) {
            return new RemoteApplicationInfo(source);
        }

        public RemoteApplicationInfo[] newArray(int size) {
            return new RemoteApplicationInfo[size];
        }
    }

    RemoteApplicationInfo(PackageManager pm, PackageInfo orig) {
        this.uiOptions = 0;
        this.flags = 0;
        this.requiresSmallestWidthDp = 0;
        this.compatibleWidthLimitDp = 0;
        this.largestWidthLimitDp = 0;
        this.enabled = true;
        this.taskAffinity = orig.applicationInfo.taskAffinity;
        this.permission = orig.applicationInfo.permission;
        this.processName = orig.applicationInfo.processName;
        this.className = orig.applicationInfo.className;
        this.theme = orig.applicationInfo.theme;
        this.flags = orig.applicationInfo.flags;
        this.requiresSmallestWidthDp = orig.applicationInfo.requiresSmallestWidthDp;
        this.compatibleWidthLimitDp = orig.applicationInfo.compatibleWidthLimitDp;
        this.largestWidthLimitDp = orig.applicationInfo.largestWidthLimitDp;
        this.sourceDir = orig.applicationInfo.sourceDir;
        this.publicSourceDir = orig.applicationInfo.publicSourceDir;
        this.nativeLibraryDir = orig.applicationInfo.nativeLibraryDir;
        this.dataDir = orig.applicationInfo.dataDir;
        this.uid = orig.applicationInfo.uid;
        this.targetSdkVersion = orig.applicationInfo.targetSdkVersion;
        this.enabled = orig.applicationInfo.enabled;
        this.manageSpaceActivityName = orig.applicationInfo.manageSpaceActivityName;
        this.descriptionRes = orig.applicationInfo.descriptionRes;
        this.uiOptions = orig.applicationInfo.uiOptions;
        this.backupAgentName = orig.applicationInfo.backupAgentName;
        this.iconData = Bitmap2Bytes(drawable2Bitmap(pm.getApplicationIcon(orig.applicationInfo)));
        this.label = orig.applicationInfo.loadLabel(pm);
        this.packageName = orig.packageName;
        this.versionCode = orig.versionCode;
        this.versionName = orig.versionName;
        this.sharedUserId = orig.sharedUserId;
        this.sharedUserLabel = orig.sharedUserLabel;
        this.firstInstallTime = orig.firstInstallTime;
        this.lastUpdateTime = orig.lastUpdateTime;
    }

    private Bitmap drawable2Bitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != -1 ? Config.ARGB_8888 : Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    private byte[] Bitmap2Bytes(Bitmap bm) {
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(CompressFormat.JPEG, 20, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public String toString() {
        return "RemoteApplicationInfo{" + Integer.toHexString(System.identityHashCode(this)) + " " + this.packageName + "}";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int parcelableFlags) {
        dest.writeString(this.taskAffinity);
        dest.writeString(this.permission);
        dest.writeString(this.processName);
        dest.writeString(this.className);
        dest.writeInt(this.theme);
        dest.writeInt(this.flags);
        dest.writeInt(this.requiresSmallestWidthDp);
        dest.writeInt(this.compatibleWidthLimitDp);
        dest.writeInt(this.largestWidthLimitDp);
        dest.writeString(this.sourceDir);
        dest.writeString(this.publicSourceDir);
        dest.writeString(this.nativeLibraryDir);
        dest.writeString(this.dataDir);
        dest.writeInt(this.uid);
        dest.writeInt(this.targetSdkVersion);
        dest.writeInt(this.enabled ? 1 : 0);
        dest.writeString(this.manageSpaceActivityName);
        dest.writeString(this.backupAgentName);
        dest.writeInt(this.descriptionRes);
        dest.writeInt(this.uiOptions);
        dest.writeInt(this.iconData.length);
        dest.writeByteArray(this.iconData);
        TextUtils.writeToParcel(this.label, dest, parcelableFlags);
        dest.writeString(this.packageName);
        dest.writeInt(this.versionCode);
        dest.writeString(this.versionName);
        dest.writeString(this.sharedUserId);
        dest.writeInt(this.sharedUserLabel);
        dest.writeLong(this.firstInstallTime);
        dest.writeLong(this.lastUpdateTime);
    }

    private RemoteApplicationInfo(Parcel source) {
        boolean z = true;
        this.uiOptions = 0;
        this.flags = 0;
        this.requiresSmallestWidthDp = 0;
        this.compatibleWidthLimitDp = 0;
        this.largestWidthLimitDp = 0;
        this.enabled = true;
        this.applicationInfo = new ApplicationInfo();
        ApplicationInfo applicationInfo = this.applicationInfo;
        String readString = source.readString();
        this.taskAffinity = readString;
        applicationInfo.taskAffinity = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.permission = readString;
        applicationInfo.permission = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.processName = readString;
        applicationInfo.processName = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.className = readString;
        applicationInfo.className = readString;
        applicationInfo = this.applicationInfo;
        int readInt = source.readInt();
        this.theme = readInt;
        applicationInfo.theme = readInt;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.flags = readInt;
        applicationInfo.flags = readInt;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.requiresSmallestWidthDp = readInt;
        applicationInfo.requiresSmallestWidthDp = readInt;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.compatibleWidthLimitDp = readInt;
        applicationInfo.compatibleWidthLimitDp = readInt;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.largestWidthLimitDp = readInt;
        applicationInfo.largestWidthLimitDp = readInt;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.sourceDir = readString;
        applicationInfo.sourceDir = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.publicSourceDir = readString;
        applicationInfo.publicSourceDir = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.nativeLibraryDir = readString;
        applicationInfo.nativeLibraryDir = readString;
        applicationInfo = this.applicationInfo;
        readString = source.readString();
        this.dataDir = readString;
        applicationInfo.dataDir = readString;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.uid = readInt;
        applicationInfo.uid = readInt;
        applicationInfo = this.applicationInfo;
        readInt = source.readInt();
        this.targetSdkVersion = readInt;
        applicationInfo.targetSdkVersion = readInt;
        applicationInfo = this.applicationInfo;
        if (source.readInt() == 0) {
            z = false;
        }
        this.enabled = z;
        applicationInfo.enabled = z;
        ApplicationInfo applicationInfo2 = this.applicationInfo;
        String readString2 = source.readString();
        this.manageSpaceActivityName = readString2;
        applicationInfo2.manageSpaceActivityName = readString2;
        applicationInfo2 = this.applicationInfo;
        readString2 = source.readString();
        this.backupAgentName = readString2;
        applicationInfo2.backupAgentName = readString2;
        applicationInfo2 = this.applicationInfo;
        int readInt2 = source.readInt();
        this.descriptionRes = readInt2;
        applicationInfo2.descriptionRes = readInt2;
        applicationInfo2 = this.applicationInfo;
        readInt2 = source.readInt();
        this.uiOptions = readInt2;
        applicationInfo2.uiOptions = readInt2;
        this.iconData = new byte[source.readInt()];
        source.readByteArray(this.iconData);
        this.iconBitmap = bytes2Bitmap(this.iconData);
        this.label = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(source);
        this.packageName = source.readString();
        this.versionCode = source.readInt();
        this.versionName = source.readString();
        this.sharedUserId = source.readString();
        this.sharedUserLabel = source.readInt();
        this.firstInstallTime = source.readLong();
        this.lastUpdateTime = source.readLong();
    }

    private Bitmap bytes2Bitmap(byte[] b) {
        if (b.length == 0) {
            return null;
        }
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }
}
