package com.ingenic.iwds.remotedevice;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDeviceProcessCallback extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteDeviceProcessCallback {

        private static class Proxy implements IRemoteDeviceProcessCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onResponseSystemMemoryInfo(long availMemSize, long totalMemSize) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    obtain.writeLong(availMemSize);
                    obtain.writeLong(totalMemSize);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onResponseRunningAppProcessInfo(RemoteProcessInfoList processInfoList) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    if (processInfoList != null) {
                        obtain.writeInt(1);
                        processInfoList.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onDoneKillProcess(String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    obtain.writeString(packageName);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
        }

        public static IRemoteDeviceProcessCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteDeviceProcessCallback)) {
                return new Proxy(obj);
            }
            return (IRemoteDeviceProcessCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    onResponseSystemMemoryInfo(data.readLong(), data.readLong());
                    reply.writeNoException();
                    return true;
                case 2:
                    RemoteProcessInfoList remoteProcessInfoList;
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    if (data.readInt() != 0) {
                        remoteProcessInfoList = (RemoteProcessInfoList) RemoteProcessInfoList.CREATOR.createFromParcel(data);
                    } else {
                        remoteProcessInfoList = null;
                    }
                    onResponseRunningAppProcessInfo(remoteProcessInfoList);
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    onDoneKillProcess(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.remotedevice.IRemoteDeviceProcessCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onDoneKillProcess(String str) throws RemoteException;

    void onResponseRunningAppProcessInfo(RemoteProcessInfoList remoteProcessInfoList) throws RemoteException;

    void onResponseSystemMemoryInfo(long j, long j2) throws RemoteException;
}
