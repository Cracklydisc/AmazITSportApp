package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.Serializable;

public class RemoteDeviceManagerInfo {

    static class RemoteResponse implements Parcelable {
        public static final Creator<RemoteResponse> CREATOR = new C11621();
        int returnCode;
        int type;

        static class C11621 implements Creator<RemoteResponse> {
            C11621() {
            }

            public RemoteResponse createFromParcel(Parcel source) {
                return new RemoteResponse(source);
            }

            public RemoteResponse[] newArray(int size) {
                return new RemoteResponse[size];
            }
        }

        public RemoteResponse(int type) {
            this.type = type;
        }

        public RemoteResponse(int type, int returnCode) {
            this.type = type;
            this.returnCode = returnCode;
        }

        public RemoteResponse(Parcel source) {
            readFromParcel(source);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.type);
            dest.writeInt(this.returnCode);
        }

        public void readFromParcel(Parcel source) {
            this.type = source.readInt();
            this.returnCode = source.readInt();
        }
    }

    static class AppListResponse extends RemoteResponse {
        public static final Creator<AppListResponse> CREATOR = new C11571();
        RemoteApplicationInfoList appList;

        static class C11571 implements Creator<AppListResponse> {
            C11571() {
            }

            public AppListResponse createFromParcel(Parcel source) {
                return new AppListResponse(source);
            }

            public AppListResponse[] newArray(int size) {
                return new AppListResponse[size];
            }
        }

        public AppListResponse(RemoteApplicationInfoList appList) {
            super(3, 0);
            this.appList = appList;
        }

        public AppListResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.appList, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.appList = (RemoteApplicationInfoList) source.readParcelable(AppListResponse.class.getClassLoader());
        }
    }

    static class ResponseWithName extends RemoteResponse {
        public static final Creator<ResponseWithName> CREATOR = new C11631();
        String packageName;

        static class C11631 implements Creator<ResponseWithName> {
            C11631() {
            }

            public ResponseWithName createFromParcel(Parcel source) {
                return new ResponseWithName(source);
            }

            public ResponseWithName[] newArray(int size) {
                return new ResponseWithName[size];
            }
        }

        public ResponseWithName(String packageName, int type, int returnCode) {
            super(type, returnCode);
            this.packageName = packageName;
        }

        public ResponseWithName(int type, int returnCode) {
            super(type, returnCode);
        }

        public ResponseWithName(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
        }
    }

    static class ClearAllAppDataCacheResponse extends ResponseWithName {
        public static final Creator<ClearAllAppDataCacheResponse> CREATOR = new C11581();
        int index;
        int totalCount;
        int typeOfIndex;

        static class C11581 implements Creator<ClearAllAppDataCacheResponse> {
            C11581() {
            }

            public ClearAllAppDataCacheResponse createFromParcel(Parcel source) {
                return new ClearAllAppDataCacheResponse(source);
            }

            public ClearAllAppDataCacheResponse[] newArray(int size) {
                return new ClearAllAppDataCacheResponse[size];
            }
        }

        public ClearAllAppDataCacheResponse(int totalCount, int index, String packageName, int typeOfIndex, int returnCode) {
            super(packageName, 2, returnCode);
            this.totalCount = totalCount;
            this.index = index;
            this.typeOfIndex = typeOfIndex;
        }

        public ClearAllAppDataCacheResponse(int returnCode) {
            super(null, 2, returnCode);
        }

        public ClearAllAppDataCacheResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
            dest.writeInt(this.totalCount);
            dest.writeInt(this.index);
            dest.writeInt(this.typeOfIndex);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
            this.totalCount = source.readInt();
            this.index = source.readInt();
            this.typeOfIndex = source.readInt();
        }
    }

    static class ConfirmInstallResponse extends ResponseWithName {
        public static final Creator<ConfirmInstallResponse> CREATOR = new C11591();
        String apkFilePath;

        static class C11591 implements Creator<ConfirmInstallResponse> {
            C11591() {
            }

            public ConfirmInstallResponse createFromParcel(Parcel source) {
                return new ConfirmInstallResponse(source);
            }

            public ConfirmInstallResponse[] newArray(int size) {
                return new ConfirmInstallResponse[size];
            }
        }

        public ConfirmInstallResponse(String packageName, String apkFilePath, int returnCode) {
            super(packageName, 6, returnCode);
            this.apkFilePath = apkFilePath;
        }

        public ConfirmInstallResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.apkFilePath);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.apkFilePath = source.readString();
        }
    }

    static class PkgInfoResponse extends RemoteResponse {
        public static final Creator<PkgInfoResponse> CREATOR = new C11601();
        String packageName;
        RemotePackageStats pkgStats;

        static class C11601 implements Creator<PkgInfoResponse> {
            C11601() {
            }

            public PkgInfoResponse createFromParcel(Parcel source) {
                return new PkgInfoResponse(source);
            }

            public PkgInfoResponse[] newArray(int size) {
                return new PkgInfoResponse[size];
            }
        }

        public PkgInfoResponse(String packageName, int returnCode) {
            super(8, returnCode);
            this.packageName = packageName;
        }

        public PkgInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
            dest.writeParcelable(this.pkgStats, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
            this.pkgStats = (RemotePackageStats) source.readParcelable(PkgInfoResponse.class.getClassLoader());
        }
    }

    static class ProcessInfoResponse extends RemoteResponse {
        public static final Creator<ProcessInfoResponse> CREATOR = new C11611();
        RemoteProcessInfoList processList;

        static class C11611 implements Creator<ProcessInfoResponse> {
            C11611() {
            }

            public ProcessInfoResponse createFromParcel(Parcel source) {
                return new ProcessInfoResponse(source);
            }

            public ProcessInfoResponse[] newArray(int size) {
                return new ProcessInfoResponse[size];
            }
        }

        public ProcessInfoResponse(RemoteProcessInfoList processList) {
            super(10, 0);
            this.processList = processList;
        }

        public ProcessInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.processList, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.processList = (RemoteProcessInfoList) source.readParcelable(ProcessInfoResponse.class.getClassLoader());
        }
    }

    static class RemoteRequest implements Serializable {
        private static final long serialVersionUID = -87257129103004422L;
        public String apkFilePath;
        public boolean isInstalledInExternal;
        public String packageName;
        public long requiredSize;
        public int subType;
        public int type;
        public int value;

        public RemoteRequest(int type) {
            this.type = type;
        }

        public RemoteRequest(int type, int subType) {
            this.type = type;
            this.subType = subType;
        }

        public RemoteRequest(int type, int subType, int value) {
            this.type = type;
            this.subType = subType;
            this.value = value;
        }

        public RemoteRequest(String packageName, int type) {
            this.packageName = packageName;
            this.type = type;
        }

        public RemoteRequest(String packageName, int type, String apkFilePath, long requiredSize, boolean isInstalledInExternal) {
            this(packageName, type);
            this.apkFilePath = apkFilePath;
            this.requiredSize = requiredSize;
            this.isInstalledInExternal = isInstalledInExternal;
        }
    }

    static class SettingResponse extends RemoteResponse {
        public static final Creator<SettingResponse> CREATOR = new C11641();
        int subType;

        static class C11641 implements Creator<SettingResponse> {
            C11641() {
            }

            public SettingResponse createFromParcel(Parcel source) {
                return new SettingResponse(source);
            }

            public SettingResponse[] newArray(int size) {
                return new SettingResponse[size];
            }
        }

        public SettingResponse(int type, int subType, int returnCode) {
            super(type, returnCode);
            this.subType = subType;
        }

        public SettingResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(this.subType);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.subType = source.readInt();
        }
    }

    static class StorageInfoResponse extends RemoteResponse {
        public static final Creator<StorageInfoResponse> CREATOR = new C11651();
        RemoteStorageInfo storageInfo;

        static class C11651 implements Creator<StorageInfoResponse> {
            C11651() {
            }

            public StorageInfoResponse createFromParcel(Parcel source) {
                return new StorageInfoResponse(source);
            }

            public StorageInfoResponse[] newArray(int size) {
                return new StorageInfoResponse[size];
            }
        }

        public StorageInfoResponse(RemoteStorageInfo storageInfo) {
            super(4, 0);
            this.storageInfo = storageInfo;
        }

        public StorageInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.storageInfo, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.storageInfo = (RemoteStorageInfo) source.readParcelable(StorageInfoResponse.class.getClassLoader());
        }
    }

    static class SysMemResponse extends RemoteResponse {
        public static final Creator<SysMemResponse> CREATOR = new C11661();
        long availSysMemSize;
        long totalSysMemSize;

        static class C11661 implements Creator<SysMemResponse> {
            C11661() {
            }

            public SysMemResponse createFromParcel(Parcel source) {
                return new SysMemResponse(source);
            }

            public SysMemResponse[] newArray(int size) {
                return new SysMemResponse[size];
            }
        }

        public SysMemResponse(long availSysMemSize, long totalSysMemSize) {
            super(9, 0);
            this.availSysMemSize = availSysMemSize;
            this.totalSysMemSize = totalSysMemSize;
        }

        public SysMemResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeLong(this.availSysMemSize);
            dest.writeLong(this.totalSysMemSize);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.availSysMemSize = source.readLong();
            this.totalSysMemSize = source.readLong();
        }
    }

    static int toElfReturnCode(int returnCode) {
        return -110 + returnCode;
    }
}
