package com.ingenic.iwds.remotedevice;

import android.content.pm.PackageStats;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.remotedevice.IRemoteDeviceService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;

public class RemoteDeviceServiceManager extends ServiceManagerContext {
    private IRemoteDeviceService mService;

    class C11681 extends ServiceClientProxy {
        final /* synthetic */ RemoteDeviceServiceManager this$0;

        public void onServiceConnected(IBinder service) {
            this.this$0.mService = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
        }
    }

    private class RemoteDeviceAppCallback extends IRemoteDeviceAppCallback.Stub {
        private RemoteDeviceAppListener m_appListener;
        private Handler m_handler;

        class C11691 extends Handler {
            final /* synthetic */ RemoteDeviceAppCallback this$1;

            public void handleMessage(Message msg) {
                int i;
                switch (msg.what) {
                    case 0:
                    case 1:
                        this.this$1.m_appListener.onResponseClearAppDataOrCache((String) msg.obj, msg.arg1, msg.arg2);
                        return;
                    case 2:
                        ClearAllAppDataMsgObj clearAllAppDataMsgObj = (ClearAllAppDataMsgObj) msg.obj;
                        this.this$1.m_appListener.onResponseClearAllAppDataAndCache(clearAllAppDataMsgObj.totalCount, clearAllAppDataMsgObj.index, clearAllAppDataMsgObj.packageName, clearAllAppDataMsgObj.typeOfIndex, clearAllAppDataMsgObj.returnCode);
                        return;
                    case 3:
                        this.this$1.m_appListener.onRemoteAppInfoListAvailable(((RemoteApplicationInfoList) msg.obj).getList());
                        return;
                    case 4:
                        this.this$1.m_appListener.onRemoteStorageInfoAvailable((RemoteStorageInfo) msg.obj);
                        return;
                    case 5:
                        String str = (String) msg.obj;
                        i = msg.arg1;
                        int i2 = msg.arg2;
                        if (i == 0) {
                            this.this$1.m_appListener.onDoneInstallApp(str, i2);
                            return;
                        } else {
                            this.this$1.m_appListener.onSendFileProgressForInstall(str, i2);
                            return;
                        }
                    case 7:
                        this.this$1.m_appListener.onDoneDeleteApp((String) msg.obj, msg.arg1);
                        return;
                    case 8:
                        PackageStats packageStats = (PackageStats) msg.obj;
                        i = msg.arg1;
                        IwdsLog.m267i(this, " TYPE_PKG_SIZE_INFO stats " + packageStats);
                        this.this$1.m_appListener.onResponsePkgSizeInfo(packageStats, i);
                        return;
                    default:
                        IwdsAssert.dieIf((Object) this, true, "Implement me.");
                        return;
                }
            }
        }

        private class ClearAllAppDataMsgObj {
            int index;
            String packageName;
            int returnCode;
            int totalCount;
            int typeOfIndex;

            public ClearAllAppDataMsgObj(int totalCount, int index, String packageName, int typeOfIndex, int returnCode) {
                this.totalCount = totalCount;
                this.index = index;
                this.packageName = packageName;
                this.typeOfIndex = typeOfIndex;
                this.returnCode = returnCode;
            }
        }

        public void onRemoteAppInfoListAvailable(RemoteApplicationInfoList remoteAppInfoList) throws RemoteException {
            Message.obtain(this.m_handler, 3, remoteAppInfoList).sendToTarget();
        }

        public void onRemoteStorageInfoAvailable(RemoteStorageInfo storageInfo) throws RemoteException {
            Message.obtain(this.m_handler, 4, storageInfo).sendToTarget();
        }

        public void onSendFileProgressForInstall(String packageName, int progress) throws RemoteException {
            Message obtain = Message.obtain(this.m_handler, 5, packageName);
            obtain.arg1 = 1;
            obtain.arg2 = progress;
            obtain.sendToTarget();
        }

        public void onDoneInstallApp(String packageName, int returnCode) throws RemoteException {
            Message obtain = Message.obtain(this.m_handler, 5, packageName);
            obtain.arg1 = 0;
            obtain.arg2 = returnCode;
            obtain.sendToTarget();
        }

        public void onDoneDeleteApp(String packageName, int returnCode) throws RemoteException {
            Message obtain = Message.obtain(this.m_handler, 7, packageName);
            obtain.arg1 = returnCode;
            obtain.sendToTarget();
        }

        public void onResponsePkgSizeInfo(RemotePackageStats stats, int returnCode) throws RemoteException {
            PackageStats localPackageStats = stats.getLocalPackageStats();
            IwdsLog.m264d(this, " ===localStats " + localPackageStats);
            Message obtain = Message.obtain(this.m_handler, 8, localPackageStats);
            obtain.arg1 = returnCode;
            obtain.sendToTarget();
        }

        public void onResponseClearAppDataOrCache(String packageName, int requestType, int returnCode) throws RemoteException {
            Message obtain = Message.obtain(this.m_handler, requestType, packageName);
            obtain.arg1 = requestType;
            obtain.arg2 = returnCode;
            obtain.sendToTarget();
        }

        public void onResponseClearAllAppDataAndCache(int totalClearCount, int index, String packageName, int typeOfIndex, int returnCode) throws RemoteException {
            Message.obtain(this.m_handler, 2, new ClearAllAppDataMsgObj(totalClearCount, index, packageName, typeOfIndex, returnCode)).sendToTarget();
        }
    }

    private class RemoteDeviceProcessCallback extends IRemoteDeviceProcessCallback.Stub {
        private Handler m_handler;
        private RemoteDeviceProcessListener m_processListener;

        class C11701 extends Handler {
            final /* synthetic */ RemoteDeviceProcessCallback this$1;

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 9:
                        MemoryInfoMsg memoryInfoMsg = (MemoryInfoMsg) msg.obj;
                        this.this$1.m_processListener.onResponseSystemMemoryInfo(memoryInfoMsg.availSize, memoryInfoMsg.totalSize);
                        return;
                    case 10:
                        this.this$1.m_processListener.onResponseRunningAppProcessInfo(((RemoteProcessInfoList) msg.obj).processInfoList);
                        return;
                    case 11:
                        this.this$1.m_processListener.onDoneKillProcess((String) msg.obj);
                        return;
                    default:
                        return;
                }
            }
        }

        private class MemoryInfoMsg {
            long availSize;
            long totalSize;

            public MemoryInfoMsg(long availSize, long totalSize) {
                this.availSize = availSize;
                this.totalSize = totalSize;
            }
        }

        public void onResponseSystemMemoryInfo(long availMemSize, long totalMemSize) throws RemoteException {
            Message.obtain(this.m_handler, 9, new MemoryInfoMsg(availMemSize, totalMemSize)).sendToTarget();
        }

        public void onResponseRunningAppProcessInfo(RemoteProcessInfoList processInfoList) throws RemoteException {
            Message.obtain(this.m_handler, 10, processInfoList).sendToTarget();
        }

        public void onDoneKillProcess(String packageName) throws RemoteException {
            Message.obtain(this.m_handler, 11, packageName).sendToTarget();
        }
    }

    private class RemoteDeviceSettingCallback extends IRemoteDeviceSettingCallback.Stub {
        private RemoteDeviceSettingListener m_SettingListener;
        private Handler m_handler;

        class C11711 extends Handler {
            final /* synthetic */ RemoteDeviceSettingCallback this$1;

            public void handleMessage(Message msg) {
                int i = msg.arg1;
                int i2 = msg.arg2;
                switch (msg.what) {
                    case 12:
                        this.this$1.m_SettingListener.onDoneSetting(i, i2);
                        return;
                    case 13:
                        this.this$1.m_SettingListener.onGetSetting(i, i2);
                        return;
                    default:
                        return;
                }
            }
        }

        public void onDoneSetting(int type, int returnCode) throws RemoteException {
            Message.obtain(this.m_handler, 12, type, returnCode).sendToTarget();
        }

        public void onGetSetting(int type, int returnCode) throws RemoteException {
            Message.obtain(this.m_handler, 13, type, returnCode).sendToTarget();
        }
    }

    private class RemoteDeviceStatusCallback extends IRemoteDeviceStatusCallback.Stub {
        private Handler m_handler;
        private RemoteDeviceStatusListener m_statusListener;

        class C11721 extends Handler {
            final /* synthetic */ RemoteDeviceStatusCallback this$1;

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        this.this$1.m_statusListener.onRemoteDeviceReady(((Boolean) msg.obj).booleanValue());
                        return;
                    default:
                        return;
                }
            }
        }

        public void onRemoteDeviceReady(boolean isReady) throws RemoteException {
            Message.obtain(this.m_handler, 0, Boolean.valueOf(isReady)).sendToTarget();
        }
    }
}
