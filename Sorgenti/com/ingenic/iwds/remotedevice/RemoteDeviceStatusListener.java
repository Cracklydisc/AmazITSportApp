package com.ingenic.iwds.remotedevice;

public interface RemoteDeviceStatusListener {
    void onRemoteDeviceReady(boolean z);
}
