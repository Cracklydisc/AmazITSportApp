package com.ingenic.iwds.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INotificationProxyService extends IInterface {

    public static abstract class Stub extends Binder implements INotificationProxyService {

        private static class C1086a implements INotificationProxyService {
            private IBinder f34a;

            C1086a(IBinder iBinder) {
                this.f34a = iBinder;
            }

            public IBinder asBinder() {
                return this.f34a;
            }

            public boolean notify(String packageName, int id, Note note) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationProxyService");
                    obtain.writeString(packageName);
                    obtain.writeInt(id);
                    if (note != null) {
                        obtain.writeInt(1);
                        note.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f34a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void cancel(String packageName, int id) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationProxyService");
                    obtain.writeString(packageName);
                    obtain.writeInt(id);
                    this.f34a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void cancelAll(String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationProxyService");
                    obtain.writeString(packageName);
                    this.f34a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean registerBackend(INotificationServiceBackend backend, String packageName, String uuid) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationProxyService");
                    obtain.writeStrongBinder(backend != null ? backend.asBinder() : null);
                    obtain.writeString(packageName);
                    obtain.writeString(uuid);
                    this.f34a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean unregisterBackend(String packageName, String uuid) throws RemoteException {
                boolean z = false;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationProxyService");
                    obtain.writeString(packageName);
                    obtain.writeString(uuid);
                    this.f34a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.app.INotificationProxyService");
        }

        public static INotificationProxyService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.app.INotificationProxyService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof INotificationProxyService)) {
                return new C1086a(obj);
            }
            return (INotificationProxyService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int i = 0;
            boolean notify;
            switch (code) {
                case 1:
                    Note note;
                    int i2;
                    data.enforceInterface("com.ingenic.iwds.app.INotificationProxyService");
                    String readString = data.readString();
                    int readInt = data.readInt();
                    if (data.readInt() != 0) {
                        note = (Note) Note.CREATOR.createFromParcel(data);
                    } else {
                        note = null;
                    }
                    notify = notify(readString, readInt, note);
                    reply.writeNoException();
                    if (notify) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    reply.writeInt(i2);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationProxyService");
                    cancel(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationProxyService");
                    cancelAll(data.readString());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationProxyService");
                    notify = registerBackend(com.ingenic.iwds.app.INotificationServiceBackend.Stub.asInterface(data.readStrongBinder()), data.readString(), data.readString());
                    reply.writeNoException();
                    if (notify) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationProxyService");
                    notify = unregisterBackend(data.readString(), data.readString());
                    reply.writeNoException();
                    if (notify) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.app.INotificationProxyService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void cancel(String str, int i) throws RemoteException;

    void cancelAll(String str) throws RemoteException;

    boolean notify(String str, int i, Note note) throws RemoteException;

    boolean registerBackend(INotificationServiceBackend iNotificationServiceBackend, String str, String str2) throws RemoteException;

    boolean unregisterBackend(String str, String str2) throws RemoteException;
}
