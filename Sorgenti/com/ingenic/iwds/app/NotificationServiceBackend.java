package com.ingenic.iwds.app;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.ingenic.iwds.app.INotificationServiceBackend.Stub;

public class NotificationServiceBackend {
    INotificationServiceBackend f53a = new C10932(this);
    private Handler f54b = new C10921(this);

    class C10921 extends Handler {
        final /* synthetic */ NotificationServiceBackend f47a;

        C10921(NotificationServiceBackend notificationServiceBackend) {
            this.f47a = notificationServiceBackend;
        }

        public void handleMessage(Message msg) {
            C1094a c1094a;
            switch (msg.what) {
                case 3:
                    this.f47a.onCancelAll(((C1094a) msg.obj).f49a);
                    return;
                case 19:
                    c1094a = (C1094a) msg.obj;
                    this.f47a.onHandle(c1094a.f49a, c1094a.f50b, c1094a.f51c);
                    return;
                case 87:
                    c1094a = (C1094a) msg.obj;
                    this.f47a.onCancel(c1094a.f49a, c1094a.f50b);
                    return;
                default:
                    return;
            }
        }
    }

    class C10932 extends Stub {
        final /* synthetic */ NotificationServiceBackend f48a;

        C10932(NotificationServiceBackend notificationServiceBackend) {
            this.f48a = notificationServiceBackend;
        }

        public boolean onHandleNotification(String packageName, int id, Note note) throws RemoteException {
            Message.obtain(this.f48a.f54b, 19, new C1094a(this.f48a, packageName, id, note)).sendToTarget();
            return true;
        }

        public void onCancelNotification(String packageName, int id) throws RemoteException {
            Message.obtain(this.f48a.f54b, 87, new C1094a(this.f48a, packageName, id, null)).sendToTarget();
        }

        public void onCancelAllNotification(String packageName) throws RemoteException {
            Message.obtain(this.f48a.f54b, 3, new C1094a(this.f48a, packageName, 0, null)).sendToTarget();
        }
    }

    private class C1094a {
        public String f49a;
        public int f50b;
        public Note f51c;
        final /* synthetic */ NotificationServiceBackend f52d;

        public C1094a(NotificationServiceBackend notificationServiceBackend, String str, int i, Note note) {
            this.f52d = notificationServiceBackend;
            this.f49a = str;
            this.f50b = i;
            this.f51c = note;
        }
    }

    public boolean onHandle(String packageName, int id, Note note) {
        return true;
    }

    public void onCancel(String packageName, int id) {
    }

    public void onCancelAll(String packageName) {
    }
}
