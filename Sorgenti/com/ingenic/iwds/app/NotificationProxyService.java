package com.ingenic.iwds.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import com.ingenic.iwds.app.INotificationProxyService.Stub;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class NotificationProxyService extends Service {
    private NotificationProxyServiceStub f44a = new NotificationProxyServiceStub(this);

    public class NotificationProxyServiceStub extends Stub {
        final /* synthetic */ NotificationProxyService f41a;
        private C1090a f42b;
        private HashMap<String, String> f43c;

        private class C1090a {
            final /* synthetic */ NotificationProxyServiceStub f39a;
            private ArrayList<C1089a> f40b = new ArrayList();

            private final class C1089a implements DeathRecipient {
                INotificationServiceBackend f36a;
                String f37b;
                final /* synthetic */ C1090a f38c;

                C1089a(C1090a c1090a, INotificationServiceBackend iNotificationServiceBackend, String str) {
                    this.f38c = c1090a;
                    this.f36a = iNotificationServiceBackend;
                    this.f37b = str;
                }

                public void binderDied() {
                    IwdsLog.m266e("NotificationProxyService", "Backend died: " + this.f37b);
                    synchronized (this.f36a) {
                        this.f38c.f40b.remove(this);
                    }
                }
            }

            public C1090a(NotificationProxyServiceStub notificationProxyServiceStub) {
                this.f39a = notificationProxyServiceStub;
            }

            public INotificationServiceBackend m40a(String str) {
                Iterator it = this.f40b.iterator();
                while (it.hasNext()) {
                    C1089a c1089a = (C1089a) it.next();
                    if (c1089a.f37b.equals(str)) {
                        return c1089a.f36a;
                    }
                }
                return null;
            }

            public boolean m42a(INotificationServiceBackend iNotificationServiceBackend, String str) {
                if (m40a(str) != null) {
                    return false;
                }
                IBinder asBinder = iNotificationServiceBackend.asBinder();
                try {
                    DeathRecipient c1089a = new C1089a(this, iNotificationServiceBackend, str);
                    asBinder.linkToDeath(c1089a, 0);
                    this.f40b.add(c1089a);
                    return true;
                } catch (RemoteException e) {
                    return false;
                }
            }

            public boolean m41a(INotificationServiceBackend iNotificationServiceBackend) {
                DeathRecipient deathRecipient;
                Iterator it = this.f40b.iterator();
                while (it.hasNext()) {
                    deathRecipient = (C1089a) it.next();
                    if (deathRecipient.f36a.equals(iNotificationServiceBackend)) {
                        break;
                    }
                }
                deathRecipient = null;
                if (deathRecipient == null) {
                    return false;
                }
                this.f40b.remove(deathRecipient);
                deathRecipient.f36a.asBinder().unlinkToDeath(deathRecipient, 0);
                return true;
            }

            public int m38a() {
                return this.f40b.size();
            }

            public void m43b() {
            }

            public INotificationServiceBackend m39a(int i) {
                return ((C1089a) this.f40b.get(i)).f36a;
            }
        }

        public NotificationProxyServiceStub(NotificationProxyService notificationProxyService) {
            this.f41a = notificationProxyService;
            synchronized (this) {
                this.f42b = new C1090a(this);
                this.f43c = new HashMap();
                this.f43c.put("com.ingenic.launcher", "9207c288-dd9f-8fdd-0b88-3ea582bbbeb2");
                this.f43c.put("com.ingenic.iwds.test.notificationproxyservice.backend", "396bdc12-b834-bc70-f12c-1196ce75f99c");
            }
        }

        public void cancel(String packageName, int id) throws RemoteException {
            synchronized (this) {
                int a = this.f42b.m38a();
                for (int i = 0; i < a; i++) {
                    try {
                        this.f42b.m39a(i).onCancelNotification(packageName, id);
                    } catch (RemoteException e) {
                    }
                }
                this.f42b.m43b();
            }
        }

        public void cancelAll(String packageName) throws RemoteException {
            synchronized (this) {
                int a = this.f42b.m38a();
                for (int i = 0; i < a; i++) {
                    try {
                        this.f42b.m39a(i).onCancelAllNotification(packageName);
                    } catch (RemoteException e) {
                    }
                }
                this.f42b.m43b();
            }
        }

        public boolean notify(String packageName, int id, Note note) throws RemoteException {
            synchronized (this) {
                int a = this.f42b.m38a();
                for (int i = 0; i < a; i++) {
                    try {
                        this.f42b.m39a(i).onHandleNotification(packageName, id, note);
                    } catch (RemoteException e) {
                    }
                }
                this.f42b.m43b();
            }
            return true;
        }

        public boolean registerBackend(INotificationServiceBackend backend, String packageName, String uuid) throws RemoteException {
            synchronized (this) {
                String str = (String) this.f43c.get(packageName);
                if (str == null || !str.equals(uuid)) {
                    IwdsLog.m265e((Object) this, "Unqualified applicant: " + packageName);
                    return false;
                } else if (this.f42b.m40a(packageName) != null) {
                    IwdsLog.m265e((Object) this, "Already registered applicant: " + packageName);
                    return false;
                } else {
                    boolean a = this.f42b.m42a(backend, packageName);
                    return a;
                }
            }
        }

        public boolean unregisterBackend(String packageName, String uuid) throws RemoteException {
            synchronized (this) {
                String str = (String) this.f43c.get(packageName);
                if (str == null || !str.equals(uuid)) {
                    IwdsLog.m265e((Object) this, "Unqualified applicant: " + packageName);
                    return false;
                }
                INotificationServiceBackend a = this.f42b.m40a(packageName);
                if (a == null) {
                    IwdsLog.m265e((Object) this, "No such backend: " + packageName);
                    return false;
                }
                boolean a2 = this.f42b.m41a(a);
                return a2;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f44a;
    }
}
