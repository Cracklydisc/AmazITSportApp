package com.ingenic.iwds.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class ConnectionHelper implements ConnectionCallbacks {
    private ConnectionServiceManager m_connectionService;
    private Context m_context;
    private ArrayList<DeviceDescriptor> m_devices;
    private IntentFilter m_filter = new IntentFilter();
    private BroadcastReceiver m_receiver = new C10851(this);
    private ServiceClient m_serviceClient;

    class C10851 extends BroadcastReceiver {
        final /* synthetic */ ConnectionHelper f33a;

        C10851(ConnectionHelper connectionHelper) {
            this.f33a = connectionHelper;
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("iwds.uniconnect.action.connected_address")) {
                this.f33a.handleConnectedDevice((DeviceDescriptor) intent.getParcelableExtra("DeviceDescriptor"));
            } else if (intent.getAction().equals("iwds.uniconnect.action.disconnected_address")) {
                this.f33a.handleDisconnectedDevice((DeviceDescriptor) intent.getParcelableExtra("DeviceDescriptor"));
            }
        }
    }

    public abstract void onConnectedDevice(DeviceDescriptor deviceDescriptor);

    public abstract void onDisconnectedDevice(DeviceDescriptor deviceDescriptor);

    public abstract void onServiceConnected(ConnectionServiceManager connectionServiceManager);

    public abstract void onServiceDisconnected(boolean z);

    public ConnectionHelper(Context context) {
        boolean z = true;
        IwdsAssert.dieIf((Object) this, context == null, "Context is null.");
        this.m_context = context.getApplicationContext();
        if (this.m_context != null) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Application context is null");
        this.m_devices = new ArrayList();
        this.m_serviceClient = new ServiceClient(this.m_context, "service_connection", this);
        this.m_filter.addAction("iwds.uniconnect.action.connected_address");
        this.m_filter.addAction("iwds.uniconnect.action.disconnected_address");
    }

    private void handleConnectedDevice(DeviceDescriptor deviceDescriptor) {
        if (!this.m_devices.contains(deviceDescriptor)) {
            this.m_devices.add(deviceDescriptor);
            onConnectedDevice(deviceDescriptor);
        }
    }

    private void handleDisconnectedDevice(DeviceDescriptor deviceDescriptor) {
        if (this.m_devices.remove(deviceDescriptor)) {
            onDisconnectedDevice(deviceDescriptor);
        }
    }

    public void start() {
        this.m_serviceClient.connect();
    }

    public void stop() {
        this.m_serviceClient.disconnect();
    }

    public boolean isStarted() {
        return this.m_serviceClient.isConnected();
    }

    public void onConnected(ServiceClient serviceClient) {
        int i = 0;
        if (this.m_connectionService == null) {
            this.m_connectionService = (ConnectionServiceManager) this.m_serviceClient.getServiceManagerContext();
            onServiceConnected(this.m_connectionService);
            this.m_context.registerReceiver(this.m_receiver, this.m_filter);
            DeviceDescriptor[] connectedDeviceDescriptors = this.m_connectionService.getConnectedDeviceDescriptors();
            ArrayList arrayList = (ArrayList) this.m_devices.clone();
            int length = connectedDeviceDescriptors == null ? 0 : connectedDeviceDescriptors.length;
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                int i2;
                DeviceDescriptor deviceDescriptor = (DeviceDescriptor) it.next();
                for (i2 = 0; i2 < length; i2++) {
                    if (connectedDeviceDescriptors[i2].equals(deviceDescriptor)) {
                        i2 = 1;
                        break;
                    }
                }
                i2 = 0;
                if (i2 == 0) {
                    handleDisconnectedDevice(deviceDescriptor);
                }
            }
            if (connectedDeviceDescriptors != null) {
                int length2 = connectedDeviceDescriptors.length;
                while (i < length2) {
                    handleConnectedDevice(connectedDeviceDescriptors[i]);
                    i++;
                }
            }
        }
    }

    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        if (this.m_connectionService != null) {
            this.m_context.unregisterReceiver(this.m_receiver);
            this.m_connectionService = null;
            Iterator it = this.m_devices.iterator();
            while (it.hasNext()) {
                onDisconnectedDevice((DeviceDescriptor) it.next());
            }
            this.m_devices.clear();
        }
        onServiceDisconnected(unexpected);
    }

    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsAssert.dieIf((Object) this, true, "Failed to connect to ConnectionService: " + reason.toString());
    }
}
