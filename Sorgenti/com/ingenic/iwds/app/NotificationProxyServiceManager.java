package com.ingenic.iwds.app;

import android.os.IBinder;
import com.ingenic.iwds.app.INotificationProxyService.Stub;
import com.ingenic.iwds.common.api.ServiceManagerContext;

public class NotificationProxyServiceManager extends ServiceManagerContext {
    private INotificationProxyService f46a;

    class C10911 extends ServiceClientProxy {
        final /* synthetic */ NotificationProxyServiceManager f45a;

        public void onServiceConnected(IBinder service) {
            this.f45a.f46a = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
        }
    }
}
