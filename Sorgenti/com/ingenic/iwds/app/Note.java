package com.ingenic.iwds.app;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Note implements Parcelable {
    public static final Creator<Note> CREATOR = new C10881();
    public String content;
    public PendingIntent pendingIntent;
    public String title;

    static class C10881 implements Creator<Note> {
        C10881() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m35a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m36a(i);
        }

        public Note m35a(Parcel parcel) {
            Note note = new Note();
            note.title = parcel.readString();
            note.content = parcel.readString();
            if (parcel.readInt() != 0) {
                note.pendingIntent = (PendingIntent) PendingIntent.CREATOR.createFromParcel(parcel);
            }
            return note;
        }

        public Note[] m36a(int i) {
            return new Note[i];
        }
    }

    private Note() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.content);
        if (this.pendingIntent == null) {
            dest.writeInt(0);
            return;
        }
        dest.writeInt(1);
        this.pendingIntent.writeToParcel(dest, flags);
    }

    public String toString() {
        return "Note [title=" + this.title + ", content=" + this.content + ", pendingIntent=" + this.pendingIntent + "]";
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.pendingIntent == null ? 0 : this.pendingIntent.hashCode()) + (((this.content == null ? 0 : this.content.hashCode()) + 31) * 31)) * 31;
        if (this.title != null) {
            i = this.title.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Note note = (Note) obj;
        if (this.content == null) {
            if (note.content != null) {
                return false;
            }
        } else if (!this.content.equals(note.content)) {
            return false;
        }
        if (this.pendingIntent == null) {
            if (note.pendingIntent != null) {
                return false;
            }
        } else if (!this.pendingIntent.equals(note.pendingIntent)) {
            return false;
        }
        if (this.title == null) {
            if (note.title != null) {
                return false;
            }
            return true;
        } else if (this.title.equals(note.title)) {
            return true;
        } else {
            return false;
        }
    }
}
