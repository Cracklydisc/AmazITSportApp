package com.ingenic.iwds.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INotificationServiceBackend extends IInterface {

    public static abstract class Stub extends Binder implements INotificationServiceBackend {

        private static class C1087a implements INotificationServiceBackend {
            private IBinder f35a;

            C1087a(IBinder iBinder) {
                this.f35a = iBinder;
            }

            public IBinder asBinder() {
                return this.f35a;
            }

            public boolean onHandleNotification(String packageName, int id, Note note) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationServiceBackend");
                    obtain.writeString(packageName);
                    obtain.writeInt(id);
                    if (note != null) {
                        obtain.writeInt(1);
                        note.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f35a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onCancelNotification(String packageName, int id) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationServiceBackend");
                    obtain.writeString(packageName);
                    obtain.writeInt(id);
                    this.f35a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onCancelAllNotification(String packageName) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.app.INotificationServiceBackend");
                    obtain.writeString(packageName);
                    this.f35a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.app.INotificationServiceBackend");
        }

        public static INotificationServiceBackend asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.app.INotificationServiceBackend");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof INotificationServiceBackend)) {
                return new C1087a(obj);
            }
            return (INotificationServiceBackend) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    Note note;
                    data.enforceInterface("com.ingenic.iwds.app.INotificationServiceBackend");
                    String readString = data.readString();
                    int readInt = data.readInt();
                    if (data.readInt() != 0) {
                        note = (Note) Note.CREATOR.createFromParcel(data);
                    } else {
                        note = null;
                    }
                    boolean onHandleNotification = onHandleNotification(readString, readInt, note);
                    reply.writeNoException();
                    reply.writeInt(onHandleNotification ? 1 : 0);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationServiceBackend");
                    onCancelNotification(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.app.INotificationServiceBackend");
                    onCancelAllNotification(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.app.INotificationServiceBackend");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onCancelAllNotification(String str) throws RemoteException;

    void onCancelNotification(String str, int i) throws RemoteException;

    boolean onHandleNotification(String str, int i, Note note) throws RemoteException;
}
