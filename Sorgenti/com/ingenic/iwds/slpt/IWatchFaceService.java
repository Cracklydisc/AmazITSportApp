package com.ingenic.iwds.slpt;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IWatchFaceService extends IInterface {

    public static abstract class Stub extends Binder implements IWatchFaceService {

        private static class C1190a implements IWatchFaceService {
            private IBinder f184a;

            public IBinder asBinder() {
                return this.f184a;
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.slpt.IWatchFaceService");
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.slpt.IWatchFaceService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }
}
