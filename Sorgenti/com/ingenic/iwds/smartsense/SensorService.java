package com.ingenic.iwds.smartsense;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import com.ingenic.iwds.smartsense.ISensorService.Stub;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SensorService extends Service {
    private SensorServiceStub m_service = new SensorServiceStub();

    private static class SensorServiceStub extends Stub {
        private ArrayList<SensorEventCallback> m_callbacks = new ArrayList();
        private ArrayList<Sensor> m_sensorList = new ArrayList();

        private class SensorEventCallback implements DeathRecipient {
            private HashMap<String, ISensorEventCallback> m_listeners = new HashMap();
            Sensor sensor;

            SensorEventCallback(Sensor s) {
                this.sensor = s;
            }

            public boolean registerListener(String uuid, ISensorEventCallback callback) {
                synchronized (this.m_listeners) {
                    try {
                        callback.asBinder().linkToDeath(this, 0);
                        this.m_listeners.put(uuid, callback);
                        if (this.m_listeners.size() == 1) {
                            SensorServiceStub.setActive(this.sensor.getType(), true);
                        }
                    } catch (RemoteException e) {
                        IwdsLog.m265e((Object) this, "Exception in registerListener: " + e.toString());
                        return false;
                    }
                }
                return true;
            }

            public void unregisterListener(String uuid) {
                synchronized (this.m_listeners) {
                    ISensorEventCallback iSensorEventCallback = (ISensorEventCallback) this.m_listeners.remove(uuid);
                    if (iSensorEventCallback == null) {
                        return;
                    }
                    iSensorEventCallback.asBinder().unlinkToDeath(this, 0);
                    if (this.m_listeners.size() != 0) {
                        return;
                    }
                    SensorServiceStub.setActive(this.sensor.getType(), false);
                }
            }

            public void binderDied() {
                synchronized (this.m_listeners) {
                    Iterator it = this.m_listeners.keySet().iterator();
                    while (it.hasNext()) {
                        if (!((ISensorEventCallback) this.m_listeners.get((String) it.next())).asBinder().isBinderAlive()) {
                            it.remove();
                        }
                    }
                    if (this.m_listeners.size() == 0) {
                        SensorServiceStub.setActive(this.sensor.getType(), false);
                    }
                }
            }
        }

        private static final native void forEachSensor(Sensor sensor, int i);

        private static final native int getSensorCount();

        private static final native void installSensorEventCallback(int i, SensorEventCallback sensorEventCallback);

        private static final native boolean setActive(int i, boolean z);

        private static final native boolean setWearOnRightHand(boolean z);

        public SensorServiceStub() {
            int sensorCount = getSensorCount();
            for (int i = 0; i < sensorCount; i++) {
                Sensor sensor = new Sensor();
                forEachSensor(sensor, i);
                SensorEventCallback sensorEventCallback = new SensorEventCallback(sensor);
                installSensorEventCallback(sensorEventCallback.sensor.getType(), sensorEventCallback);
                this.m_callbacks.add(sensorEventCallback);
                this.m_sensorList.add(sensor);
            }
        }

        public ArrayList<Sensor> getSensorList() throws RemoteException {
            return this.m_sensorList;
        }

        public Sensor getDefaultSensor(int sensorType) throws RemoteException {
            Sensor sensor;
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    SensorEventCallback sensorEventCallback = (SensorEventCallback) it.next();
                    if (sensorEventCallback.sensor.getType() == sensorType) {
                        sensor = sensorEventCallback.sensor;
                        break;
                    }
                }
                sensor = null;
            }
            return sensor;
        }

        public boolean registerListener(String uuid, ISensorEventCallback callback, Sensor sensor, int rate) throws RemoteException {
            boolean z;
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    SensorEventCallback sensorEventCallback = (SensorEventCallback) it.next();
                    if (sensorEventCallback.sensor.getType() == sensor.getType()) {
                        sensorEventCallback.registerListener(uuid, callback);
                        z = true;
                        break;
                    }
                }
                z = false;
            }
            return z;
        }

        public void unregisterListener(String uuid) throws RemoteException {
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    ((SensorEventCallback) it.next()).unregisterListener(uuid);
                }
            }
        }
    }

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind()");
        return this.m_service;
    }

    public static boolean setWearOnRightHand(boolean isOnRightHand) {
        return SensorServiceStub.setWearOnRightHand(isOnRightHand);
    }
}
