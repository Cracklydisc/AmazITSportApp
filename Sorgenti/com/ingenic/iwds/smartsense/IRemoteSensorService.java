package com.ingenic.iwds.smartsense;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IRemoteSensorService extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteSensorService {

        private static class Proxy implements IRemoteSensorService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public boolean registerRemoteSensorListener(String uuid, IRemoteSensorCallback callback) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterRemoteSensorListener(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public List<Sensor> getSensorList() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    List<Sensor> createTypedArrayList = obtain2.createTypedArrayList(Sensor.CREATOR);
                    return createTypedArrayList;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public Sensor getDefaultSensor(int sensorType) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    Sensor sensor;
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    obtain.writeInt(sensorType);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        sensor = (Sensor) Sensor.CREATOR.createFromParcel(obtain2);
                    } else {
                        sensor = null;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return sensor;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public boolean registerListener(String uuid, IRemoteSensorEventCallback callback, Sensor sensor, int rate) throws RemoteException {
                boolean z = true;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    obtain.writeString(uuid);
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    if (sensor != null) {
                        obtain.writeInt(1);
                        sensor.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(rate);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return z;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void unregisterListener(String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    obtain.writeString(uuid);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.smartsense.IRemoteSensorService");
        }

        public static IRemoteSensorService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteSensorService)) {
                return new Proxy(obj);
            }
            return (IRemoteSensorService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int i = 0;
            boolean registerRemoteSensorListener;
            Sensor defaultSensor;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    registerRemoteSensorListener = registerRemoteSensorListener(data.readString(), com.ingenic.iwds.smartsense.IRemoteSensorCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(registerRemoteSensorListener ? 1 : 0);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    unregisterRemoteSensorListener(data.readString());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    List sensorList = getSensorList();
                    reply.writeNoException();
                    reply.writeTypedList(sensorList);
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    defaultSensor = getDefaultSensor(data.readInt());
                    reply.writeNoException();
                    if (defaultSensor != null) {
                        reply.writeInt(1);
                        defaultSensor.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    String readString = data.readString();
                    IRemoteSensorEventCallback asInterface = com.ingenic.iwds.smartsense.IRemoteSensorEventCallback.Stub.asInterface(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        defaultSensor = (Sensor) Sensor.CREATOR.createFromParcel(data);
                    } else {
                        defaultSensor = null;
                    }
                    registerRemoteSensorListener = registerListener(readString, asInterface, defaultSensor, data.readInt());
                    reply.writeNoException();
                    if (registerRemoteSensorListener) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    unregisterListener(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.smartsense.IRemoteSensorService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    Sensor getDefaultSensor(int i) throws RemoteException;

    List<Sensor> getSensorList() throws RemoteException;

    boolean registerListener(String str, IRemoteSensorEventCallback iRemoteSensorEventCallback, Sensor sensor, int i) throws RemoteException;

    boolean registerRemoteSensorListener(String str, IRemoteSensorCallback iRemoteSensorCallback) throws RemoteException;

    void unregisterListener(String str) throws RemoteException;

    void unregisterRemoteSensorListener(String str) throws RemoteException;
}
