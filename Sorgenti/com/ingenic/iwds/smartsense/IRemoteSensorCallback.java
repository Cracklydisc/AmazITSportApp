package com.ingenic.iwds.smartsense;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IRemoteSensorCallback extends IInterface {

    public static abstract class Stub extends Binder implements IRemoteSensorCallback {

        private static class Proxy implements IRemoteSensorCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onSensorAvailable(List<Sensor> sensorList) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.IRemoteSensorCallback");
                    obtain.writeTypedList(sensorList);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.smartsense.IRemoteSensorCallback");
        }

        public static IRemoteSensorCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.smartsense.IRemoteSensorCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IRemoteSensorCallback)) {
                return new Proxy(obj);
            }
            return (IRemoteSensorCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.smartsense.IRemoteSensorCallback");
                    onSensorAvailable(data.createTypedArrayList(Sensor.CREATOR));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.smartsense.IRemoteSensorCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onSensorAvailable(List<Sensor> list) throws RemoteException;
}
