package com.ingenic.iwds.smartsense;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SensorEvent implements Parcelable {
    public static final Creator<SensorEvent> CREATOR = new C12001();
    public int accuracy;
    public int sensorType;
    public final float[] values = new float[this.valuesSize];
    private final int valuesSize;

    static class C12001 implements Creator<SensorEvent> {
        C12001() {
        }

        public SensorEvent createFromParcel(Parcel source) {
            int readInt = source.readInt();
            SensorEvent sensorEvent = new SensorEvent(readInt);
            for (int i = 0; i < readInt; i++) {
                sensorEvent.values[i] = source.readFloat();
            }
            sensorEvent.accuracy = source.readInt();
            sensorEvent.sensorType = source.readInt();
            return sensorEvent;
        }

        public SensorEvent[] newArray(int size) {
            return new SensorEvent[size];
        }
    }

    SensorEvent(int valSize) {
        this.valuesSize = valSize;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.valuesSize);
        for (int i = 0; i < this.valuesSize; i++) {
            dest.writeFloat(this.values[i]);
        }
        dest.writeInt(this.accuracy);
        dest.writeInt(this.sensorType);
    }
}
