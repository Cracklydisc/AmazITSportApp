package com.ingenic.iwds.smartsense;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.datatransactor.DataTransactor;

public class RemoteSensorRequest implements Parcelable {
    public static final Creator<RemoteSensorRequest> CREATOR = new C11931();
    private DataTransactor sender;
    public Sensor sensor;
    public int sensorRate;
    public int type;

    static class C11931 implements Creator<RemoteSensorRequest> {
        C11931() {
        }

        public RemoteSensorRequest createFromParcel(Parcel source) {
            RemoteSensorRequest remoteSensorRequest = new RemoteSensorRequest();
            remoteSensorRequest.type = source.readInt();
            remoteSensorRequest.sensorRate = source.readInt();
            if (source.readInt() != 0) {
                remoteSensorRequest.sensor = (Sensor) source.readParcelable(Sensor.class.getClassLoader());
            }
            return remoteSensorRequest;
        }

        public RemoteSensorRequest[] newArray(int size) {
            return new RemoteSensorRequest[size];
        }
    }

    public static RemoteSensorRequest obtain() {
        return new RemoteSensorRequest();
    }

    public static RemoteSensorRequest obtain(DataTransactor sender) {
        RemoteSensorRequest obtain = obtain();
        obtain.sender = sender;
        return obtain;
    }

    public void sendToRemote() {
        this.sender.send((Object) this);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeInt(this.sensorRate);
        if (this.sensor != null) {
            dest.writeInt(1);
            dest.writeParcelable(this.sensor, flags);
            return;
        }
        dest.writeInt(0);
    }
}
