package com.ingenic.iwds.smartsense;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.smartsense.IRemoteSensorService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RemoteSensorService extends Service implements DataTransactorCallback {
    private ServiceHandler m_handler;
    private RemoteSensorServiceStub m_service = new RemoteSensorServiceStub();
    private DataTransactor m_transactor;

    private class RemoteSensorServiceStub extends Stub {
        private ArrayList<RemoteSensorEventCallback> m_callbacks = new ArrayList();
        private RemoteSensorCallback m_remoteSensorCallback = new RemoteSensorCallback();
        private ArrayList<Sensor> m_sensorList;
        private Object m_sensorListLock = new Object();

        private class RemoteSensorCallback implements DeathRecipient {
            private HashMap<String, IRemoteSensorCallback> m_listeners = new HashMap();

            RemoteSensorCallback() {
            }

            public void onSensorAvailable(ArrayList<Sensor> sensorList) {
                synchronized (this.m_listeners) {
                    for (IRemoteSensorCallback onSensorAvailable : this.m_listeners.values()) {
                        try {
                            onSensorAvailable.onSensorAvailable(sensorList);
                        } catch (RemoteException e) {
                            IwdsLog.m265e((Object) this, "Exception in onSensorAvailable: " + e.toString());
                        }
                    }
                }
            }

            public boolean registerRemoteSensorListener(String uuid, IRemoteSensorCallback callback) {
                boolean z = false;
                synchronized (this.m_listeners) {
                    try {
                        callback.asBinder().linkToDeath(this, 0);
                        this.m_listeners.put(uuid, callback);
                        z = true;
                    } catch (RemoteException e) {
                        IwdsLog.m265e((Object) this, "Exception in registerRemoteSensorListener: " + e.toString());
                    }
                }
                return z;
            }

            public void unregisterRemoteSensorListener(String uuid) {
                synchronized (this.m_listeners) {
                    IRemoteSensorCallback iRemoteSensorCallback = (IRemoteSensorCallback) this.m_listeners.remove(uuid);
                    if (iRemoteSensorCallback == null) {
                        return;
                    }
                    iRemoteSensorCallback.asBinder().unlinkToDeath(this, 0);
                }
            }

            public void binderDied() {
                synchronized (this.m_listeners) {
                    Iterator it = this.m_listeners.keySet().iterator();
                    while (it.hasNext()) {
                        if (!((IRemoteSensorCallback) this.m_listeners.get((String) it.next())).asBinder().isBinderAlive()) {
                            it.remove();
                        }
                    }
                }
            }
        }

        private class RemoteSensorEventCallback implements DeathRecipient {
            private HashMap<String, IRemoteSensorEventCallback> m_listeners = new HashMap();
            Sensor sensor;

            RemoteSensorEventCallback(Sensor s) {
                this.sensor = s;
            }

            public boolean registerListener(String uuid, IRemoteSensorEventCallback callback, int rate) {
                synchronized (this.m_listeners) {
                    try {
                        callback.asBinder().linkToDeath(this, 0);
                        this.m_listeners.put(uuid, callback);
                        if (this.m_listeners.size() == 1) {
                            RemoteSensorService.this.m_handler.requestRegisterListener(this.sensor, rate);
                        }
                    } catch (RemoteException e) {
                        IwdsLog.m265e((Object) this, "Exception in registerListener: " + e.toString());
                        return false;
                    }
                }
                return true;
            }

            public void onSensorEvent(SensorEvent event) {
                synchronized (this.m_listeners) {
                    for (IRemoteSensorEventCallback onSensorChanged : this.m_listeners.values()) {
                        try {
                            onSensorChanged.onSensorChanged(event);
                        } catch (RemoteException e) {
                            IwdsLog.m265e((Object) this, "Exception in onSensorEvent: " + e.toString());
                        }
                    }
                }
            }

            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                synchronized (this.m_listeners) {
                    for (IRemoteSensorEventCallback onAccuracyChanged : this.m_listeners.values()) {
                        try {
                            onAccuracyChanged.onAccuracyChanged(sensor, accuracy);
                        } catch (RemoteException e) {
                            IwdsLog.m265e((Object) this, "Exception in onAccuracyChanged: " + e.toString());
                        }
                    }
                }
            }

            public void unregisterListener(String uuid) {
                synchronized (this.m_listeners) {
                    IRemoteSensorEventCallback iRemoteSensorEventCallback = (IRemoteSensorEventCallback) this.m_listeners.remove(uuid);
                    if (iRemoteSensorEventCallback == null) {
                        return;
                    }
                    iRemoteSensorEventCallback.asBinder().unlinkToDeath(this, 0);
                    if (this.m_listeners.size() != 0) {
                        return;
                    }
                    RemoteSensorService.this.m_handler.requestUnregisterListener(this.sensor);
                }
            }

            public void binderDied() {
                synchronized (this.m_listeners) {
                    Iterator it = this.m_listeners.keySet().iterator();
                    while (it.hasNext()) {
                        if (!((IRemoteSensorEventCallback) this.m_listeners.get((String) it.next())).asBinder().isBinderAlive()) {
                            it.remove();
                        }
                    }
                    if (this.m_listeners.size() == 0) {
                        RemoteSensorService.this.m_handler.requestUnregisterListener(this.sensor);
                    }
                }
            }
        }

        public void onSensorAvailable(ArrayList<Sensor> sensorList) {
            synchronized (this.m_sensorListLock) {
                if (this.m_sensorList == null && sensorList != null) {
                    int size = sensorList.size();
                    for (int i = 0; i < size; i++) {
                        this.m_callbacks.add(new RemoteSensorEventCallback((Sensor) sensorList.get(i)));
                    }
                } else if (sensorList == null && !this.m_callbacks.isEmpty()) {
                    this.m_callbacks.clear();
                }
                this.m_sensorList = sensorList;
                this.m_remoteSensorCallback.onSensorAvailable(this.m_sensorList);
            }
        }

        public void onRemoteSensorEvent(SensorEvent event) {
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    RemoteSensorEventCallback remoteSensorEventCallback = (RemoteSensorEventCallback) it.next();
                    if (remoteSensorEventCallback.sensor.getType() == event.sensorType) {
                        remoteSensorEventCallback.onSensorEvent(event);
                    }
                }
            }
        }

        public void onRemoteSensorAccuracyChanged(Sensor sensor, int accuracy) {
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    RemoteSensorEventCallback remoteSensorEventCallback = (RemoteSensorEventCallback) it.next();
                    if (remoteSensorEventCallback.sensor.getType() == sensor.getType()) {
                        remoteSensorEventCallback.onAccuracyChanged(sensor, accuracy);
                    }
                }
            }
        }

        public Sensor getDefaultSensor(int sensorType) throws RemoteException {
            synchronized (this.m_sensorListLock) {
                if (this.m_sensorList == null) {
                    return null;
                }
                Iterator it = this.m_sensorList.iterator();
                while (it.hasNext()) {
                    Sensor sensor = (Sensor) it.next();
                    if (sensor.getType() == sensorType) {
                        return sensor;
                    }
                }
                return null;
            }
        }

        public List<Sensor> getSensorList() throws RemoteException {
            List list;
            synchronized (this.m_sensorListLock) {
                list = this.m_sensorList;
            }
            return list;
        }

        public boolean registerListener(String uuid, IRemoteSensorEventCallback callback, Sensor sensor, int rate) throws RemoteException {
            boolean z;
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    RemoteSensorEventCallback remoteSensorEventCallback = (RemoteSensorEventCallback) it.next();
                    if (remoteSensorEventCallback.sensor.getType() == sensor.getType()) {
                        remoteSensorEventCallback.registerListener(uuid, callback, rate);
                        z = true;
                        break;
                    }
                }
                z = false;
            }
            return z;
        }

        public void unregisterListener(String uuid) throws RemoteException {
            synchronized (this.m_callbacks) {
                Iterator it = this.m_callbacks.iterator();
                while (it.hasNext()) {
                    ((RemoteSensorEventCallback) it.next()).unregisterListener(uuid);
                }
            }
        }

        public boolean registerRemoteSensorListener(String uuid, IRemoteSensorCallback callback) throws RemoteException {
            boolean registerRemoteSensorListener;
            synchronized (this.m_remoteSensorCallback) {
                registerRemoteSensorListener = this.m_remoteSensorCallback.registerRemoteSensorListener(uuid, callback);
            }
            RemoteSensorService.this.m_handler.requestSensorList();
            return registerRemoteSensorListener;
        }

        public void unregisterRemoteSensorListener(String uuid) throws RemoteException {
            synchronized (this.m_remoteSensorCallback) {
                this.m_remoteSensorCallback.unregisterRemoteSensorListener(uuid);
            }
        }
    }

    private class ServiceHandler extends Handler {
        private boolean m_isRequestingSensorList;
        private boolean m_remoteServiceConnected;

        private ServiceHandler() {
        }

        public void requestSensorList() {
            Message obtain = Message.obtain(this);
            obtain.what = 4;
            obtain.sendToTarget();
        }

        public void requestRegisterListener(Sensor sensor, int sensorRate) {
            Message obtain = Message.obtain(this);
            obtain.what = 5;
            obtain.arg1 = sensorRate;
            obtain.obj = sensor;
            obtain.sendToTarget();
        }

        public void requestUnregisterListener(Sensor sensor) {
            Message obtain = Message.obtain(this);
            obtain.what = 6;
            obtain.obj = sensor;
            obtain.sendToTarget();
        }

        public void setRemoteServiceState(boolean connected) {
            int i = 1;
            Message obtain = Message.obtain(this);
            obtain.what = 1;
            if (!connected) {
                i = 0;
            }
            obtain.arg1 = i;
            obtain.sendToTarget();
        }

        public void setSensorList(ArrayList<Sensor> sensorList) {
            Message obtain = Message.obtain(this);
            obtain.what = 0;
            obtain.obj = sensorList;
            obtain.sendToTarget();
        }

        public void handleResponse(RemoteSensorResponse response) {
            Message obtain = Message.obtain(this);
            switch (response.type) {
                case 0:
                    obtain.what = 0;
                    obtain.obj = response.sensorList;
                    break;
                case 1:
                    obtain.what = 1;
                    obtain.arg1 = response.result;
                    break;
                case 2:
                    obtain.what = 2;
                    obtain.obj = response.sensorEvent;
                    break;
                case 3:
                    obtain.what = 3;
                    obtain.arg1 = response.accuracy;
                    obtain.obj = response.sensor;
                    break;
                default:
                    IwdsAssert.dieIf((Object) this, true, "Unsupported remote response");
                    return;
            }
            obtain.sendToTarget();
        }

        public void handleMessage(Message msg) {
            RemoteSensorRequest obtain = RemoteSensorRequest.obtain(RemoteSensorService.this.m_transactor);
            Sensor sensor;
            switch (msg.what) {
                case 0:
                    this.m_isRequestingSensorList = false;
                    RemoteSensorService.this.m_service.onSensorAvailable((ArrayList) msg.obj);
                    return;
                case 1:
                    if (msg.arg1 == 1) {
                        IwdsLog.m267i(this, "Sensor service on remote device connected");
                        this.m_remoteServiceConnected = true;
                        requestSensorList();
                        return;
                    }
                    IwdsLog.m267i(this, "Sensor service on remote device disconnected");
                    this.m_remoteServiceConnected = false;
                    return;
                case 2:
                    RemoteSensorService.this.m_service.onRemoteSensorEvent((SensorEvent) msg.obj);
                    return;
                case 3:
                    RemoteSensorService.this.m_service.onRemoteSensorAccuracyChanged((Sensor) msg.obj, msg.arg1);
                    return;
                case 4:
                    if (!this.m_remoteServiceConnected) {
                        IwdsLog.m269w(this, "Sensor service on remote device not connected yet");
                        setSensorList(null);
                        return;
                    } else if (this.m_remoteServiceConnected && this.m_isRequestingSensorList) {
                        IwdsLog.m269w(this, "Already requesting remote sensor list, waiting...");
                        return;
                    } else {
                        this.m_isRequestingSensorList = true;
                        IwdsLog.m267i(this, "Try to request remote device sensor list");
                        obtain.type = 0;
                        obtain.sendToRemote();
                        return;
                    }
                case 5:
                    int i = msg.arg1;
                    sensor = (Sensor) msg.obj;
                    IwdsLog.m267i(this, "Try to request register remote senor listener: " + sensor.toString());
                    obtain.type = 1;
                    obtain.sensor = sensor;
                    obtain.sensorRate = i;
                    obtain.sendToRemote();
                    return;
                case 6:
                    sensor = (Sensor) msg.obj;
                    IwdsLog.m267i(this, "Try to request unregister remote senor listener: " + sensor.toString());
                    obtain.type = 2;
                    obtain.sensor = sensor;
                    obtain.sendToRemote();
                    return;
                default:
                    return;
            }
        }
    }

    public void onCreate() {
        IwdsLog.m264d(this, "onCreate");
        super.onCreate();
        this.m_transactor = new DataTransactor(this, this, "c1dc19e2-17a4-0797-0000-68a0dd4bfb68");
        this.m_handler = new ServiceHandler();
    }

    public void onDestroy() {
        IwdsLog.m264d(this, "onDestroy");
        super.onDestroy();
    }

    public boolean onUnbind(Intent intent) {
        IwdsLog.m264d(this, "onUnbind");
        this.m_transactor.stop();
        return super.onUnbind(intent);
    }

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind");
        this.m_transactor.start();
        return this.m_service;
    }

    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    public void onChannelAvailable(boolean isAvailable) {
        if (!isAvailable && this.m_service != null) {
            this.m_handler.setSensorList(null);
            this.m_handler.setRemoteServiceState(false);
        }
    }

    public void onSendResult(DataTransactResult result) {
    }

    public void onDataArrived(Object object) {
        if (object instanceof RemoteSensorResponse) {
            this.m_handler.handleResponse((RemoteSensorResponse) object);
        }
    }

    public void onSendFileProgress(int progress) {
    }

    public void onRecvFileProgress(int progress) {
    }
}
