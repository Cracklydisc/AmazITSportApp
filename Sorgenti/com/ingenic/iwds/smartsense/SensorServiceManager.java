package com.ingenic.iwds.smartsense;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.ISensorService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class SensorServiceManager extends ServiceManagerContext {
    private HashMap<SensorEventListener, ArrayList<EventCallback>> m_listeners;
    private ISensorService m_service;

    class C12011 extends ServiceClientProxy {
        final /* synthetic */ SensorServiceManager this$0;

        public void onServiceConnected(IBinder service) {
            this.this$0.m_service = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
            this.this$0.unregisterAllListeners();
        }
    }

    private class EventCallback extends ISensorEventCallback.Stub {
        private Handler m_handler = new C12021();
        private SensorEventListener m_listener;
        Sensor sensor;
        String uuid;

        class C12021 extends Handler {
            C12021() {
            }

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 19:
                        EventCallback.this.m_listener.onSensorChanged((SensorEvent) msg.obj);
                        return;
                    case 87:
                        EventCallback.this.m_listener.onAccuracyChanged((Sensor) msg.obj, msg.arg1);
                        return;
                    default:
                        IwdsAssert.dieIf((Object) this, true, "Implement me.");
                        return;
                }
            }
        }

        public EventCallback(SensorEventListener listener, Sensor s) {
            this.m_listener = listener;
            this.sensor = s;
            this.uuid = UUID.randomUUID().toString();
        }

        public void onSensorChanged(SensorEvent event) throws RemoteException {
            Message.obtain(this.m_handler, 19, event).sendToTarget();
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) throws RemoteException {
            Message.obtain(this.m_handler, 87, accuracy, 0, sensor).sendToTarget();
        }
    }

    public List<Sensor> getSensorList() {
        try {
            return this.m_service.getSensorList();
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in getSensorList: " + e.toString());
            return null;
        }
    }

    public boolean registerListener(SensorEventListener listener, Sensor sensor, int rate) {
        boolean z = true;
        boolean z2 = false;
        IwdsAssert.dieIf((Object) this, listener == null ? true : z2, "Listener is null.");
        if (sensor != null) {
            z = z2;
        }
        IwdsAssert.dieIf((Object) this, z, "Sensor is null.");
        ArrayList arrayList = (ArrayList) this.m_listeners.get(listener);
        if (arrayList != null) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (((EventCallback) it.next()).sensor.getType() == sensor.getType()) {
                    break;
                }
            }
        }
        ISensorEventCallback eventCallback = new EventCallback(listener, sensor);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.m_listeners.put(listener, arrayList);
        }
        arrayList.add(eventCallback);
        try {
            z2 = this.m_service.registerListener(eventCallback.uuid, eventCallback, sensor, rate);
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in registerListener: " + e.toString());
        }
        return z2;
    }

    public void unregisterListener(SensorEventListener listener, Sensor sensor) {
        boolean z = true;
        IwdsAssert.dieIf((Object) this, listener == null, "Listener is null.");
        if (sensor != null) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Sensor is null.");
        ArrayList arrayList = (ArrayList) this.m_listeners.get(listener);
        if (arrayList != null) {
            try {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    EventCallback eventCallback = (EventCallback) it.next();
                    if (eventCallback.sensor.getType() == sensor.getType()) {
                        this.m_service.unregisterListener(eventCallback.uuid);
                        it.remove();
                        break;
                    }
                }
                if (arrayList.isEmpty()) {
                    this.m_listeners.remove(listener);
                }
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in unregisterListener: " + e.toString());
            }
        }
    }

    private void unregisterAllListeners() {
        for (Object obj : this.m_listeners.keySet()) {
            try {
                Iterator it = ((ArrayList) this.m_listeners.get(obj)).iterator();
                while (it.hasNext()) {
                    this.m_service.unregisterListener(((EventCallback) it.next()).uuid);
                }
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in unregisterListener: " + e.toString());
            }
        }
        this.m_listeners.clear();
    }
}
