package com.ingenic.iwds.smartsense;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Sensor implements Parcelable {
    public static final Creator<Sensor> CREATOR = new C11991();
    private float m_maxRange;
    private int m_minDelay;
    private float m_resolution;
    private int m_type;

    static class C11991 implements Creator<Sensor> {
        C11991() {
        }

        public Sensor createFromParcel(Parcel source) {
            Sensor sensor = new Sensor();
            sensor.m_type = source.readInt();
            sensor.m_maxRange = source.readFloat();
            sensor.m_resolution = source.readFloat();
            sensor.m_minDelay = source.readInt();
            return sensor;
        }

        public Sensor[] newArray(int size) {
            return new Sensor[size];
        }
    }

    Sensor() {
    }

    public int getType() {
        return this.m_type;
    }

    public String toString() {
        return "{type=" + this.m_type + ", maxRange=" + this.m_maxRange + ", resolution=" + this.m_resolution + "," + "minDelay=" + this.m_minDelay + "}";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.m_type);
        dest.writeFloat(this.m_maxRange);
        dest.writeFloat(this.m_resolution);
        dest.writeInt(this.m_minDelay);
    }
}
