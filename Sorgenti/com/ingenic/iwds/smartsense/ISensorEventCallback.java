package com.ingenic.iwds.smartsense;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISensorEventCallback extends IInterface {

    public static abstract class Stub extends Binder implements ISensorEventCallback {

        private static class Proxy implements ISensorEventCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onSensorChanged(SensorEvent event) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.ISensorEventCallback");
                    if (event != null) {
                        obtain.writeInt(1);
                        event.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void onAccuracyChanged(Sensor sensor, int accuracy) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.smartsense.ISensorEventCallback");
                    if (sensor != null) {
                        obtain.writeInt(1);
                        sensor.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(accuracy);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.smartsense.ISensorEventCallback");
        }

        public static ISensorEventCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.smartsense.ISensorEventCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof ISensorEventCallback)) {
                return new Proxy(obj);
            }
            return (ISensorEventCallback) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Sensor sensor = null;
            switch (code) {
                case 1:
                    SensorEvent sensorEvent;
                    data.enforceInterface("com.ingenic.iwds.smartsense.ISensorEventCallback");
                    if (data.readInt() != 0) {
                        sensorEvent = (SensorEvent) SensorEvent.CREATOR.createFromParcel(data);
                    }
                    onSensorChanged(sensorEvent);
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.smartsense.ISensorEventCallback");
                    if (data.readInt() != 0) {
                        sensor = (Sensor) Sensor.CREATOR.createFromParcel(data);
                    }
                    onAccuracyChanged(sensor, data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.smartsense.ISensorEventCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onAccuracyChanged(Sensor sensor, int i) throws RemoteException;

    void onSensorChanged(SensorEvent sensorEvent) throws RemoteException;
}
