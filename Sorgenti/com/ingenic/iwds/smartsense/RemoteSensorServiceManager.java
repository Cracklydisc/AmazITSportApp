package com.ingenic.iwds.smartsense;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.IRemoteSensorService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RemoteSensorServiceManager extends ServiceManagerContext {
    private HashMap<SensorEventListener, ArrayList<EventCallback>> m_listeners;
    private boolean m_remoteAvaliable;
    private IRemoteSensorService m_service;

    class C11961 extends ServiceClientProxy {
        final /* synthetic */ RemoteSensorServiceManager this$0;

        public void onServiceConnected(IBinder service) {
            this.this$0.m_service = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
            this.this$0.unregisterAllListeners();
        }
    }

    private class EventCallback extends IRemoteSensorEventCallback.Stub {
        private Handler m_handler;
        private SensorEventListener m_listener;
        String uuid;

        class C11971 extends Handler {
            final /* synthetic */ EventCallback this$1;

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 19:
                        this.this$1.m_listener.onSensorChanged((SensorEvent) msg.obj);
                        return;
                    case 87:
                        this.this$1.m_listener.onAccuracyChanged((Sensor) msg.obj, msg.arg1);
                        return;
                    default:
                        IwdsAssert.dieIf((Object) this, true, "Implement me.");
                        return;
                }
            }
        }

        public void onSensorChanged(SensorEvent event) throws RemoteException {
            Message.obtain(this.m_handler, 19, event).sendToTarget();
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) throws RemoteException {
            Message.obtain(this.m_handler, 87, accuracy, 0, sensor).sendToTarget();
        }
    }

    private class RemoteSensorCallback extends IRemoteSensorCallback.Stub {
        private Handler m_handler;
        private RemoteSensorListener m_listener;
        private boolean status;
        final /* synthetic */ RemoteSensorServiceManager this$0;

        class C11981 extends Handler {
            ArrayList<Sensor> sensorList;
            final /* synthetic */ RemoteSensorCallback this$1;

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        this.sensorList = (ArrayList) msg.obj;
                        if (this.sensorList == null) {
                            this.this$1.this$0.m_remoteAvaliable = false;
                            this.this$1.this$0.unregisterAllListeners();
                        } else {
                            this.this$1.this$0.m_remoteAvaliable = true;
                        }
                        if (this.this$1.this$0.m_remoteAvaliable != this.this$1.status) {
                            this.this$1.m_listener.onSensorAvailable(this.sensorList);
                        }
                        this.this$1.status = this.this$1.this$0.m_remoteAvaliable;
                        return;
                    default:
                        IwdsAssert.dieIf((Object) this, true, "Implement me.");
                        return;
                }
            }
        }

        public void onSensorAvailable(List<Sensor> sensorList) throws RemoteException {
            Message.obtain(this.m_handler, 0, sensorList).sendToTarget();
        }
    }

    private void unregisterAllListeners() {
        for (Object obj : this.m_listeners.keySet()) {
            try {
                Iterator it = ((ArrayList) this.m_listeners.get(obj)).iterator();
                while (it.hasNext()) {
                    this.m_service.unregisterListener(((EventCallback) it.next()).uuid);
                }
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in unregisterListener: " + e.toString());
            }
        }
        this.m_listeners.clear();
    }
}
