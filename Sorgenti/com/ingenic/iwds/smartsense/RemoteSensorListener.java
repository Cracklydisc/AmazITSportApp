package com.ingenic.iwds.smartsense;

import java.util.ArrayList;

public interface RemoteSensorListener {
    void onSensorAvailable(ArrayList<Sensor> arrayList);
}
