package com.ingenic.iwds.smartsense;

import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.util.ArrayList;
import java.util.Iterator;

public class SensorServiceProxy implements ConnectionCallbacks, DataTransactorCallback {
    private ServiceProxytHandler m_handler;
    private SensorEventListener m_listener = new C12031();
    private ServiceClient m_sensorServiceClient;
    private SensorServiceManager m_service;
    private DataTransactor m_transactor;
    private WakeLock m_wakeLock;

    class C12031 implements SensorEventListener {
        C12031() {
        }

        public void onSensorChanged(SensorEvent event) {
            SensorServiceProxy.this.wakeLockAcquire();
            SensorServiceProxy.this.m_handler.notifySensorChanged(event);
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            SensorServiceProxy.this.wakeLockAcquire();
            SensorServiceProxy.this.m_handler.notifySensorAccuracyChanged(sensor, accuracy);
        }
    }

    private class ServiceProxytHandler extends Handler {
        private boolean m_channelAvailable;
        private ArrayList<Sensor> m_sensorList;
        private boolean m_serviceConnected;
        final /* synthetic */ SensorServiceProxy this$0;

        public void setChannelState(boolean available) {
            Message obtain = Message.obtain(this);
            obtain.what = 6;
            obtain.arg1 = available ? 1 : 0;
            obtain.sendToTarget();
        }

        public void notifySensorServiceConnected(boolean connected) {
            Message obtain = Message.obtain(this);
            obtain.what = 3;
            obtain.arg1 = connected ? 1 : 0;
            obtain.sendToTarget();
        }

        public void notifySensorChanged(SensorEvent event) {
            Message obtain = Message.obtain(this);
            obtain.what = 4;
            obtain.obj = event;
            obtain.sendToTarget();
        }

        public void notifySensorAccuracyChanged(Sensor sensor, int accuracy) {
            Message obtain = Message.obtain(this);
            obtain.what = 5;
            obtain.arg1 = accuracy;
            obtain.obj = sensor;
            obtain.sendToTarget();
        }

        public void handleRequest(RemoteSensorRequest request) {
            Message obtain = Message.obtain(this);
            switch (request.type) {
                case 0:
                    obtain.what = 0;
                    break;
                case 1:
                    obtain.what = 1;
                    obtain.arg1 = request.sensorRate;
                    obtain.obj = request.sensor;
                    break;
                case 2:
                    obtain.what = 2;
                    obtain.obj = request.sensor;
                    break;
                default:
                    IwdsAssert.dieIf((Object) this, true, "Unsupported remote request");
                    return;
            }
            obtain.sendToTarget();
        }

        public void handleMessage(Message msg) {
            boolean z = false;
            boolean z2 = true;
            RemoteSensorResponse obtain = RemoteSensorResponse.obtain(this.this$0.m_transactor);
            int i;
            Sensor sensor;
            switch (msg.what) {
                case 0:
                    if (this.m_channelAvailable) {
                        obtain.type = 0;
                        obtain.sensorList = (ArrayList) this.this$0.m_service.getSensorList();
                        IwdsLog.m267i(this, "Get sensor list: " + obtain.sensorList.toString());
                        obtain.sendToRemote();
                        return;
                    }
                    IwdsLog.m265e((Object) this, "Transfer channel unavailable");
                    return;
                case 1:
                    i = msg.arg1;
                    sensor = (Sensor) msg.obj;
                    IwdsLog.m267i(this, "Register sensor listener: " + sensor.toString());
                    if (this.this$0.m_service.registerListener(this.this$0.m_listener, sensor, i)) {
                        this.m_sensorList.add(sensor);
                        return;
                    }
                    return;
                case 2:
                    sensor = (Sensor) msg.obj;
                    IwdsLog.m267i(this, "Unregister sensor listener: " + sensor.toString());
                    this.this$0.m_service.unregisterListener(this.this$0.m_listener, sensor);
                    this.m_sensorList.remove(sensor);
                    return;
                case 3:
                    if (msg.arg1 == 1) {
                        z = true;
                    }
                    this.m_serviceConnected = z;
                    if (this.m_channelAvailable) {
                        obtain.type = 1;
                        obtain.result = msg.arg1;
                        obtain.sendToRemote();
                        return;
                    }
                    IwdsLog.m265e((Object) this, "Transfer channel unavailable");
                    return;
                case 4:
                    if (!this.m_serviceConnected) {
                        IwdsLog.m267i(this, "Notify sensor changed, but sensor service already disconnected.");
                        this.this$0.wakeLockRelease();
                        return;
                    } else if (this.m_channelAvailable) {
                        SensorEvent sensorEvent = (SensorEvent) msg.obj;
                        obtain.type = 2;
                        obtain.sensorEvent = sensorEvent;
                        obtain.sendToRemote();
                        return;
                    } else {
                        IwdsLog.m265e((Object) this, "Transfer channel unavailable");
                        this.this$0.wakeLockRelease();
                        return;
                    }
                case 5:
                    if (!this.m_serviceConnected) {
                        IwdsLog.m267i(this, "Notify sensor accuracy changed, but sensor service already disconnected.");
                        this.this$0.wakeLockRelease();
                        return;
                    } else if (this.m_channelAvailable) {
                        sensor = (Sensor) msg.obj;
                        i = msg.arg1;
                        obtain.type = 3;
                        obtain.sensor = sensor;
                        obtain.accuracy = i;
                        obtain.sendToRemote();
                        return;
                    } else {
                        IwdsLog.m265e((Object) this, "Transfer channel unavailable");
                        this.this$0.wakeLockRelease();
                        return;
                    }
                case 6:
                    if (msg.arg1 != 1) {
                        z2 = false;
                    }
                    this.m_channelAvailable = z2;
                    Iterator it = this.m_sensorList.iterator();
                    while (it.hasNext()) {
                        this.this$0.m_service.unregisterListener(this.this$0.m_listener, (Sensor) it.next());
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private SensorServiceProxy() {
    }

    private void wakeLockAcquire() {
        if (!this.m_wakeLock.isHeld()) {
            this.m_wakeLock.acquire();
        }
    }

    private void wakeLockRelease() {
        if (this.m_wakeLock.isHeld()) {
            this.m_wakeLock.release();
        }
    }

    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
    }

    public void onChannelAvailable(boolean isAvailable) {
        if (isAvailable) {
            this.m_handler.setChannelState(true);
            IwdsLog.m267i(this, "Try to connect sensor service");
            this.m_sensorServiceClient.connect();
            return;
        }
        this.m_handler.setChannelState(false);
        this.m_handler.notifySensorServiceConnected(false);
        IwdsLog.m267i(this, "Try to disconnect sensor service");
        this.m_sensorServiceClient.disconnect();
    }

    public void onSendResult(DataTransactResult result) {
        int i = ((RemoteSensorResponse) result.getTransferedObject()).type;
        if (i == 2 || i == 3) {
            wakeLockRelease();
        }
    }

    public void onDataArrived(Object object) {
        if (object instanceof RemoteSensorRequest) {
            this.m_handler.handleRequest((RemoteSensorRequest) object);
        }
    }

    public void onSendFileProgress(int progress) {
    }

    public void onRecvFileProgress(int progress) {
    }

    public void onConnected(ServiceClient serviceClient) {
        IwdsLog.m267i(this, "Sensor service connected");
        this.m_service = (SensorServiceManager) this.m_sensorServiceClient.getServiceManagerContext();
        this.m_handler.notifySensorServiceConnected(true);
    }

    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.m267i(this, "Sensor service disconnected");
    }

    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsAssert.dieIf((Object) this, true, "Failed to connect to sensor service: " + reason.toString());
    }
}
