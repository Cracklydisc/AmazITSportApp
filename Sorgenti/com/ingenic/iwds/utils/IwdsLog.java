package com.ingenic.iwds.utils;

import android.util.Log;

public class IwdsLog {
    public static void m264d(Object who, String message) {
        Log.d("IWDS---" + who.getClass().getSimpleName(), message);
    }

    public static void m267i(Object who, String message) {
        Log.i("IWDS---" + who.getClass().getSimpleName(), message);
    }

    public static void m265e(Object who, String message) {
        Log.e("IWDS---" + who.getClass().getSimpleName(), message);
    }

    public static void m269w(Object who, String message) {
        Log.w("IWDS---" + who.getClass().getSimpleName(), message);
    }

    public static void m268v(String tag, String message) {
        Log.v("IWDS---" + tag, message);
    }

    public static void m266e(String tag, String message) {
        Log.e("IWDS---" + tag, message);
    }
}
