package com.ingenic.iwds.utils;

public class SimpleIDAlloter {
    private int f246a = 0;

    private SimpleIDAlloter() {
    }

    public static SimpleIDAlloter newInstance() {
        return new SimpleIDAlloter();
    }

    public int allocation() {
        int i = this.f246a + 1;
        this.f246a = i;
        return i;
    }

    public void initialize() {
        this.f246a = 0;
    }
}
