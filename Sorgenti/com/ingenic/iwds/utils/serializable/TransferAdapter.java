package com.ingenic.iwds.utils.serializable;

import android.os.FileObserver;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.common.exception.FileTransferException;
import com.ingenic.iwds.common.exception.IwdsException;
import com.ingenic.iwds.os.SafeParcelable;
import com.ingenic.iwds.uniconnect.Connection;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;

public final class TransferAdapter {

    public interface TransferAdapterCallback {
        void onFileTransferError(FileTransferException fileTransferException);

        void onRecvFileInterrupted(int i);

        void onRecvFileProgress(long j, long j2);

        void onSendFileInterrupted(int i);

        void onSendFileProgress(long j, long j2);
    }

    private static class C1220a extends FileObserver {
        private TransferAdapterCallback f247a;

        public C1220a(String str, TransferAdapterCallback transferAdapterCallback) {
            super(str);
            this.f247a = transferAdapterCallback;
        }

        public void startWatching() {
            super.startWatching();
            IwdsLog.m268v("TransferAdapter", "FileObserver: startWatching");
        }

        public void stopWatching() {
            super.stopWatching();
            IwdsLog.m268v("TransferAdapter", "FileObserver: stopWatching");
        }

        public void onEvent(int event, String path) {
            switch (event) {
                case 2:
                case 512:
                case 1024:
                case 2048:
                    break;
                case 4:
                    if (new File(path).canRead()) {
                        return;
                    }
                    break;
                default:
                    return;
            }
            try {
                IwdsException.throwFileTransferException(1);
            } catch (FileTransferException e) {
                this.f247a.onFileTransferError(e);
                stopWatching();
            }
        }
    }

    public static void send(Connection connection, File file, int index, TransferAdapterCallback callback) throws IOException, FileTransferException {
        int i;
        RandomAccessFile randomAccessFile;
        IwdsAssert.dieIf("TransferAdapter", connection == null, "connection == null");
        IwdsAssert.dieIf("TransferAdapter", callback == null, "callback == null");
        if (file == null) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: file == null");
            m270a(null, null);
        }
        if (!(file.exists() && file.isFile() && file.length() > 0)) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: file not exist or is dir or empty!");
            m270a(null, null);
        }
        if (!file.canRead()) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: file can not be read!");
            m270a(null, null);
        }
        C1220a c1220a = new C1220a(file.getAbsolutePath(), callback);
        c1220a.startWatching();
        long length = file.length();
        if (length % ((long) 65536) == 0) {
            i = (int) (length / ((long) 65536));
        } else {
            i = (int) ((length / ((long) 65536)) + 1);
        }
        if (index > i || index < 0) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: invalid index!");
            m270a(null, c1220a);
        }
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(new byte[]{(byte) 30});
        byte[] bytes = file.getName().getBytes(Charset.forName("UTF-8"));
        int length2 = bytes.length;
        outputStream.write(new byte[]{(byte) ((-16777216 & length2) >> 24), (byte) ((16711680 & length2) >> 16), (byte) ((65280 & length2) >> 8), (byte) ((length2 & 255) >> 0)});
        outputStream.write(bytes);
        outputStream.write(new byte[]{(byte) ((int) ((-72057594037927936L & length) >> 56)), (byte) ((int) ((71776119061217280L & length) >> 48)), (byte) ((int) ((280375465082880L & length) >> 40)), (byte) ((int) ((1095216660480L & length) >> 32)), (byte) ((int) ((4278190080L & length) >> 24)), (byte) ((int) ((16711680 & length) >> 16)), (byte) ((int) ((65280 & length) >> 8)), (byte) ((int) ((255 & length) >> null))});
        outputStream.write(new byte[]{(byte) ((-16777216 & index) >> 24), (byte) ((16711680 & index) >> 16), (byte) ((65280 & index) >> 8), (byte) ((index & 255) >> 0)});
        long j = (long) (index * 65536);
        long j2 = length - j;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
        } catch (FileNotFoundException e) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: file not exist, the file may be moved or deleted!");
            m270a(null, c1220a);
            randomAccessFile = null;
        }
        try {
            length2 = randomAccessFile.skipBytes((int) j);
            if (((long) length2) != j) {
                IwdsLog.m266e("TransferAdapter", "Unable to send file: skip error: writeSofar=" + j + ", skiped=" + length2 + ", the file contents may be modified!");
                m270a(randomAccessFile, c1220a);
            }
        } catch (IOException e2) {
            IwdsLog.m266e("TransferAdapter", "Unable to send file: file skip io exception, the file may be moved or deleted!");
            m270a(randomAccessFile, c1220a);
        }
        byte[] bArr = new byte[65536];
        long j3 = j;
        int index2 = index;
        j = j2;
        while (j > 0) {
            int min = (int) Math.min((long) 65536, j);
            int i2 = 0;
            length2 = min;
            while (length2 > 0) {
                try {
                    int read = randomAccessFile.read(bArr, i2, length2);
                    if (read < 0) {
                        IwdsLog.m266e("TransferAdapter", "Unable to send file: file read error, readBytes = " + read + ", the file contents may be modified!s");
                        m270a(randomAccessFile, c1220a);
                    }
                    length2 -= read;
                    i2 += read;
                } catch (IOException e3) {
                    IwdsLog.m266e("TransferAdapter", "Unable to send file: file read io exception, the file may be moved or deleted!");
                    m270a(randomAccessFile, c1220a);
                }
            }
            try {
                outputStream.write(bArr, 0, min);
                index = index2 + 1;
                if (index2 > i) {
                    IwdsLog.m266e("TransferAdapter", "Unable to send file: index out of bound:, current=" + index + ", total=" + i + ", the file contents may be modified!s");
                    m270a(randomAccessFile, c1220a);
                }
                j2 = ((long) min) + j3;
                long j4 = j - ((long) min);
                callback.onSendFileProgress(j2, length);
                j = j4;
                j3 = j2;
                index2 = index;
            } catch (IOException e4) {
                try {
                    randomAccessFile.close();
                } catch (IOException e5) {
                }
                IwdsLog.m266e("TransferAdapter", "Unable to send file: connection io exception");
                callback.onSendFileInterrupted(index2);
                throw e4;
            }
        }
        try {
            randomAccessFile.close();
        } catch (IOException e6) {
        }
        c1220a.stopWatching();
    }

    private static void m270a(RandomAccessFile randomAccessFile, C1220a c1220a) throws FileTransferException {
        if (randomAccessFile != null) {
            try {
                randomAccessFile.close();
            } catch (IOException e) {
            }
        }
        if (c1220a != null) {
            c1220a.stopWatching();
        }
        IwdsException.throwFileTransferException(1);
    }

    public static <T1 extends Parcelable, T2 extends SafeParcelable> void send(Connection connection, Object object, Creator<T1> parcelableCreator, SafeParcelable.Creator<T2> safeParcelableCreator) throws IOException {
        boolean z = true;
        IwdsAssert.dieIf("TransferAdapter", connection == null, "connection == null");
        String str = "TransferAdapter";
        if (object != null) {
            z = false;
        }
        IwdsAssert.dieIf(str, z, "object == null");
        connection.getOutputStream().write(ByteArrayUtils.encode(object, parcelableCreator, safeParcelableCreator));
    }

    public static <T1 extends Parcelable, T2 extends SafeParcelable> Object recv(Connection connection, Creator<T1> parcelableCeator, SafeParcelable.Creator<T2> safeParcelableCreator, TransferAdapterCallback callback) throws IOException, FileTransferException {
        boolean z = true;
        IwdsAssert.dieIf("TransferAdapter", connection == null, "connection == null");
        String str = "TransferAdapter";
        if (callback != null) {
            z = false;
        }
        IwdsAssert.dieIf(str, z, "callback == null");
        return ByteArrayUtils.decode(connection, parcelableCeator, safeParcelableCreator, callback);
    }
}
