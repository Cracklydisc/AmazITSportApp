package com.ingenic.iwds.utils.serializable;

import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.StatFs;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.ingenic.iwds.common.exception.FileTransferException;
import com.ingenic.iwds.common.exception.IwdsException;
import com.ingenic.iwds.os.SafeParcel;
import com.ingenic.iwds.os.SafeParcelable;
import com.ingenic.iwds.uniconnect.Connection;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.utils.serializable.TransferAdapter.TransferAdapterCallback;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class ByteArrayUtils {
    private static String sDesiredStoragePath = null;
    private static final HashMap<String, Creator> sParcelableCreators = new HashMap();
    private static final HashMap<String, SafeParcelable.Creator> sSafeParcelableCreators = new HashMap();

    private static byte[] expand(int pos, int length, byte[] buffer) {
        int i = pos + length;
        if (buffer != null && i <= buffer.length) {
            return buffer;
        }
        Object obj = new byte[i];
        if (buffer != null) {
            System.arraycopy(buffer, 0, obj, 0, pos);
        }
        return obj;
    }

    private static int writeByte(byte[] buffer, int offset, byte value) {
        int offset2 = offset + 1;
        buffer[offset] = value;
        return offset2;
    }

    private static int writeBoolean(byte[] buffer, int offset, boolean value) {
        if (value) {
            int offset2 = offset + 1;
            buffer[offset] = (byte) 1;
            return offset2;
        }
        offset2 = offset + 1;
        buffer[offset] = (byte) 0;
        return offset2;
    }

    private static int writeInt(byte[] buffer, int offset, int value) {
        int i = offset + 1;
        buffer[offset] = (byte) ((-16777216 & value) >> 24);
        offset = i + 1;
        buffer[i] = (byte) ((16711680 & value) >> 16);
        i = offset + 1;
        buffer[offset] = (byte) ((65280 & value) >> 8);
        offset = i + 1;
        buffer[i] = (byte) ((value & 255) >> 0);
        return offset;
    }

    private static int writeShort(byte[] buffer, int offset, short value) {
        int i = offset + 1;
        buffer[offset] = (byte) ((65280 & value) >> 8);
        offset = i + 1;
        buffer[i] = (byte) ((value & 255) >> 0);
        return offset;
    }

    private static int writeLong(byte[] buffer, int offset, long value) {
        int i = offset + 1;
        buffer[offset] = (byte) ((int) ((-72057594037927936L & value) >> 56));
        offset = i + 1;
        buffer[i] = (byte) ((int) ((71776119061217280L & value) >> 48));
        i = offset + 1;
        buffer[offset] = (byte) ((int) ((280375465082880L & value) >> 40));
        offset = i + 1;
        buffer[i] = (byte) ((int) ((1095216660480L & value) >> 32));
        i = offset + 1;
        buffer[offset] = (byte) ((int) ((4278190080L & value) >> 24));
        offset = i + 1;
        buffer[i] = (byte) ((int) ((16711680 & value) >> 16));
        i = offset + 1;
        buffer[offset] = (byte) ((int) ((65280 & value) >> 8));
        offset = i + 1;
        buffer[i] = (byte) ((int) ((255 & value) >> null));
        return offset;
    }

    private static int writeFloat(byte[] buffer, int offset, float value) {
        return writeInt(buffer, offset, Float.floatToIntBits(value));
    }

    private static int writeDouble(byte[] buffer, int offset, double value) {
        return writeLong(buffer, offset, Double.doubleToLongBits(value));
    }

    private static int writeChar(byte[] buffer, int offset, char value) {
        int i = offset + 1;
        buffer[offset] = (byte) ((65280 & value) >> 8);
        offset = i + 1;
        buffer[i] = (byte) ((value & 255) >> 0);
        return offset;
    }

    private static byte[] writeString(byte[] buffer, int offset, String value) {
        Object bytes = value.getBytes(Charset.forName("UTF-8"));
        int length = bytes.length;
        buffer = expand(offset, length + 4, buffer);
        offset = writeInt(buffer, offset, length);
        System.arraycopy(bytes, 0, buffer, offset, length);
        offset += length;
        return buffer;
    }

    private static byte[] writeCharSequence(byte[] buffer, int offset, CharSequence value) {
        Object bytes = value.toString().getBytes(Charset.forName("UTF-8"));
        int length = bytes.length;
        buffer = expand(offset, length + 4, buffer);
        offset = writeInt(buffer, offset, length);
        System.arraycopy(bytes, 0, buffer, offset, length);
        offset += length;
        return buffer;
    }

    private static <T extends Parcelable> byte[] writeParcelable(byte[] buffer, int offset, Parcelable parcelable, Creator<T> creator) {
        Parcel obtain = Parcel.obtain();
        parcelable.writeToParcel(obtain, 0);
        Object marshall = obtain.marshall();
        obtain.recycle();
        if (creator == null) {
            buffer = writeString(buffer, offset, parcelable.getClass().getName());
            offset = buffer.length;
        }
        buffer = expand(offset, 4, buffer);
        int writeInt = writeInt(buffer, offset, marshall.length);
        buffer = expand(writeInt, marshall.length, buffer);
        System.arraycopy(marshall, 0, buffer, writeInt, marshall.length);
        writeInt += marshall.length;
        return buffer;
    }

    private static <T extends SafeParcelable> byte[] writeSafeParcelable(byte[] buffer, int offset, SafeParcelable safeParcelable, SafeParcelable.Creator<T> creator) {
        SafeParcel obtain = SafeParcel.obtain();
        safeParcelable.writeToParcel(obtain, 0);
        Object marshall = obtain.marshall();
        obtain.recycle();
        if (creator == null) {
            buffer = writeString(buffer, offset, safeParcelable.getClass().getName());
            offset = buffer.length;
        }
        buffer = expand(offset, 4, buffer);
        int writeInt = writeInt(buffer, offset, marshall.length);
        buffer = expand(writeInt, marshall.length, buffer);
        System.arraycopy(marshall, 0, buffer, writeInt, marshall.length);
        writeInt += marshall.length;
        return buffer;
    }

    private static byte[] encodeByte(byte value) {
        byte[] bArr = new byte[2];
        writeByte(bArr, writeByte(bArr, 0, (byte) 20), value);
        return bArr;
    }

    private static byte[] encodeBoolean(boolean value) {
        byte[] bArr = new byte[2];
        writeBoolean(bArr, writeByte(bArr, 0, (byte) 9), value);
        return bArr;
    }

    private static byte[] encodeShort(short value) {
        byte[] bArr = new byte[3];
        writeShort(bArr, writeByte(bArr, 0, (byte) 5), value);
        return bArr;
    }

    private static byte[] encodeInt(int value) {
        byte[] bArr = new byte[5];
        writeInt(bArr, writeByte(bArr, 0, (byte) 1), value);
        return bArr;
    }

    private static byte[] encodeLong(long value) {
        byte[] bArr = new byte[9];
        writeLong(bArr, writeByte(bArr, 0, (byte) 6), value);
        return bArr;
    }

    private static byte[] encodeFloat(float value) {
        byte[] bArr = new byte[5];
        writeFloat(bArr, writeByte(bArr, 0, (byte) 7), value);
        return bArr;
    }

    private static byte[] encodeDouble(double value) {
        byte[] bArr = new byte[9];
        writeDouble(bArr, writeByte(bArr, 0, (byte) 8), value);
        return bArr;
    }

    private static byte[] encodeChar(char value) {
        byte[] bArr = new byte[3];
        writeChar(bArr, writeByte(bArr, 0, (byte) 25), value);
        return bArr;
    }

    private static byte[] encodeString(String value) {
        byte[] bArr = new byte[1];
        return writeString(bArr, writeByte(bArr, 0, (byte) 0), value);
    }

    private static <T extends Parcelable> byte[] encodeParcelable(Parcelable parcelable, Creator<T> creator) {
        byte[] bArr = new byte[1];
        return writeParcelable(bArr, writeByte(bArr, 0, (byte) 4), parcelable, creator);
    }

    private static <T extends SafeParcelable> byte[] encodeSafeParcelable(SafeParcelable safeParcelable, SafeParcelable.Creator<T> creator) {
        byte[] bArr = new byte[1];
        return writeSafeParcelable(bArr, writeByte(bArr, 0, (byte) 3), safeParcelable, creator);
    }

    private static byte[] encodeByteArray(byte[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 1) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 13), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeByte(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeBooleanArray(boolean[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 1) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 23), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeBoolean(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeShortArray(short[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 2) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 26), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeShort(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeIntArray(int[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 4) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 18), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeInt(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeLongArray(long[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 8) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 19), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeLong(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeFloatArray(float[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 4) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 27), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeFloat(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeDoubleArray(double[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 8) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 28), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeDouble(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeCharArray(char[] values) {
        int i = 0;
        int length = values.length;
        byte[] bArr = new byte[(((length * 2) + 1) + 4)];
        length = writeInt(bArr, writeByte(bArr, 0, (byte) 29), length);
        int length2 = values.length;
        while (i < length2) {
            length = writeChar(bArr, length, values[i]);
            i++;
        }
        return bArr;
    }

    private static byte[] encodeStringArray(String[] values) {
        int i = 0;
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 14), values.length);
        int length = values.length;
        while (i < length) {
            bArr = writeString(bArr, writeInt, values[i]);
            writeInt = bArr.length;
            i++;
        }
        return bArr;
    }

    private static byte[] encodeMap(Map value) {
        byte[] bArr = new byte[5];
        Set<Entry> entrySet = value.entrySet();
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 2), entrySet.size());
        int i = writeInt;
        byte[] bArr2 = bArr;
        for (Entry entry : entrySet) {
            Object encode = encode(entry.getKey(), null, null);
            Object expand = expand(i, encode.length, bArr2);
            System.arraycopy(encode, 0, expand, i, encode.length);
            i += encode.length;
            encode = encode(entry.getValue(), null, null);
            Object expand2 = expand(i, encode.length, expand);
            System.arraycopy(encode, 0, expand2, i, encode.length);
            i = encode.length + i;
            expand = expand2;
        }
        return bArr2;
    }

    private static byte[] encodeList(List value) {
        int size = value.size();
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 11), size);
        byte[] bArr2 = bArr;
        for (int i = 0; i < size; i++) {
            Object encode = encode(value.get(i), null, null);
            bArr2 = expand(writeInt, encode.length, bArr2);
            System.arraycopy(encode, 0, bArr2, writeInt, encode.length);
            writeInt += encode.length;
        }
        return bArr2;
    }

    private static byte[] encodeArray(Object[] values) {
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 17), values.length);
        int i = writeInt;
        byte[] bArr2 = bArr;
        for (Object encode : values) {
            Object encode2 = encode(encode2, null, null);
            bArr2 = expand(i, encode2.length, bArr2);
            System.arraycopy(encode2, 0, bArr2, i, encode2.length);
            i += encode2.length;
        }
        return bArr2;
    }

    private static byte[] encodeCharSequence(CharSequence value) {
        byte[] bArr = new byte[1];
        byte[] writeCharSequence = writeCharSequence(bArr, writeByte(bArr, 0, (byte) 10), value);
        int length = writeCharSequence.length;
        return writeCharSequence;
    }

    private static byte[] encodeCharSequenceArray(CharSequence[] values) {
        int i = 0;
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 24), values.length);
        int length = values.length;
        while (i < length) {
            bArr = writeCharSequence(bArr, writeInt, values[i]);
            writeInt = bArr.length;
            i++;
        }
        return bArr;
    }

    private static byte[] encodeSerializable(Serializable value) {
        IOException e;
        byte[] bArr = new byte[1];
        int writeByte = writeByte(bArr, 0, (byte) 21);
        String name = value.getClass().getName();
        Object writeString = writeString(bArr, writeByte, name);
        int length = writeString.length;
        OutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(value);
            objectOutputStream.close();
            Object toByteArray = byteArrayOutputStream.toByteArray();
            bArr = expand(length, toByteArray.length + 4, writeString);
            try {
                writeByte = writeInt(bArr, length, toByteArray.length);
                System.arraycopy(toByteArray, 0, bArr, writeByte, toByteArray.length);
                writeByte += toByteArray.length;
            } catch (IOException e2) {
                e = e2;
                IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable encountered IOException writing serializable object (name = " + name + ")");
                e.printStackTrace();
                return bArr;
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            bArr = writeString;
            e = iOException;
            IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable encountered IOException writing serializable object (name = " + name + ")");
            e.printStackTrace();
            return bArr;
        }
        return bArr;
    }

    private static byte[] encodeSparseArray(SparseArray values) {
        int size = values.size();
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 12), size);
        byte[] bArr2 = bArr;
        for (int i = 0; i < size; i++) {
            bArr2 = expand(writeInt, 4, bArr2);
            writeInt = writeInt(bArr2, writeInt, values.keyAt(i));
            Object encode = encode(values.valueAt(i), null, null);
            bArr2 = expand(writeInt, encode.length, bArr2);
            System.arraycopy(encode, 0, bArr2, writeInt, encode.length);
            writeInt += encode.length;
        }
        return bArr2;
    }

    private static byte[] encodeSparseBooleanArray(SparseBooleanArray values) {
        int i = 0;
        int size = values.size();
        byte[] bArr = new byte[((size * 5) + 5)];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 22), size);
        while (i < size) {
            writeInt = writeBoolean(bArr, writeInt(bArr, writeInt, values.keyAt(i)), values.valueAt(i));
            i++;
        }
        return bArr;
    }

    private static byte[] encodeParcelableArray(Parcelable[] values) {
        int i = 0;
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 16), values.length);
        int length = values.length;
        while (i < length) {
            bArr = writeParcelable(bArr, writeInt, values[i], null);
            writeInt = bArr.length;
            i++;
        }
        return bArr;
    }

    private static byte[] encodeSafeParcelableArray(SafeParcelable[] values) {
        int i = 0;
        byte[] bArr = new byte[5];
        int writeInt = writeInt(bArr, writeByte(bArr, 0, (byte) 15), values.length);
        int length = values.length;
        while (i < length) {
            bArr = writeSafeParcelable(bArr, writeInt, values[i], null);
            writeInt = bArr.length;
            i++;
        }
        return bArr;
    }

    public static <T1 extends Parcelable, T2 extends SafeParcelable> byte[] encode(Object v, Creator<T1> parcelableCreator, SafeParcelable.Creator<T2> safeParcelableCreator) {
        IwdsAssert.dieIf("ByteArrayUtils", v == null, "encode object is null");
        if (v instanceof String) {
            return encodeString((String) v);
        }
        if (v instanceof Integer) {
            return encodeInt(((Integer) v).intValue());
        }
        if (v instanceof Map) {
            return encodeMap((Map) v);
        }
        if (v instanceof SafeParcelable) {
            return encodeSafeParcelable((SafeParcelable) v, safeParcelableCreator);
        }
        if (v instanceof Parcelable) {
            return encodeParcelable((Parcelable) v, parcelableCreator);
        }
        if (v instanceof Short) {
            return encodeShort(((Short) v).shortValue());
        }
        if (v instanceof Long) {
            return encodeLong(((Long) v).longValue());
        }
        if (v instanceof Float) {
            return encodeFloat(((Float) v).floatValue());
        }
        if (v instanceof Double) {
            return encodeDouble(((Double) v).doubleValue());
        }
        if (v instanceof Boolean) {
            return encodeBoolean(((Boolean) v).booleanValue());
        }
        if (v instanceof CharSequence) {
            return encodeCharSequence((CharSequence) v);
        }
        if (v instanceof List) {
            return encodeList((List) v);
        }
        if (v instanceof SparseArray) {
            return encodeSparseArray((SparseArray) v);
        }
        if (v instanceof boolean[]) {
            return encodeBooleanArray((boolean[]) v);
        }
        if (v instanceof byte[]) {
            return encodeByteArray((byte[]) v);
        }
        if (v instanceof String[]) {
            return encodeStringArray((String[]) v);
        }
        if (v instanceof CharSequence[]) {
            return encodeCharSequenceArray((CharSequence[]) v);
        }
        if (v instanceof SafeParcel[]) {
            return encodeSafeParcelableArray((SafeParcelable[]) v);
        }
        if (v instanceof Parcelable[]) {
            return encodeParcelableArray((Parcelable[]) v);
        }
        if (v instanceof int[]) {
            return encodeIntArray((int[]) v);
        }
        if (v instanceof long[]) {
            return encodeLongArray((long[]) v);
        }
        if (v instanceof Byte) {
            return encodeByte(((Byte) v).byteValue());
        }
        if (v instanceof Character) {
            return encodeChar(((Character) v).charValue());
        }
        if (v instanceof char[]) {
            return encodeCharArray((char[]) v);
        }
        if (v instanceof short[]) {
            return encodeShortArray((short[]) v);
        }
        if (v instanceof float[]) {
            return encodeFloatArray((float[]) v);
        }
        if (v instanceof double[]) {
            return encodeDoubleArray((double[]) v);
        }
        if (v instanceof SparseBooleanArray) {
            return encodeSparseBooleanArray((SparseBooleanArray) v);
        }
        if (v instanceof File) {
            IwdsAssert.dieIf("ByteArrayUtils", true, "Unsupport File object serialization");
            return null;
        }
        Class cls = v.getClass();
        if (cls.isArray() && cls.getComponentType() == Object.class) {
            return encodeArray((Object[]) v);
        }
        if (v instanceof Serializable) {
            return encodeSerializable((Serializable) v);
        }
        IwdsAssert.dieIf("ByteArrayUtils", true, "Unsupported object type: " + v.getClass().getName());
        return null;
    }

    private static byte readByte(byte[] bytes) {
        return bytes[0];
    }

    private static boolean readBoolean(byte[] bytes) {
        if (bytes[0] == (byte) 1) {
            return true;
        }
        return false;
    }

    private static short readShort(byte[] bytes) {
        return (short) (((bytes[0] << 8) & 65280) | ((bytes[1] << 0) & 255));
    }

    private static int readInt(byte[] bytes) {
        return ((((bytes[0] << 24) & -16777216) | ((bytes[1] << 16) & 16711680)) | ((bytes[2] << 8) & 65280)) | ((bytes[3] << 0) & 255);
    }

    private static long readLong(byte[] bytes) {
        return ((((((((((long) bytes[0]) << 56) & -72057594037927936L) | ((((long) bytes[1]) << 48) & 71776119061217280L)) | ((((long) bytes[2]) << 40) & 280375465082880L)) | ((((long) bytes[3]) << 32) & 1095216660480L)) | ((((long) bytes[4]) << 24) & 4278190080L)) | ((((long) bytes[5]) << 16) & 16711680)) | ((((long) bytes[6]) << 8) & 65280)) | ((((long) bytes[7]) << 0) & 255);
    }

    private static float readFloat(byte[] bytes) {
        return Float.intBitsToFloat(((((bytes[0] << 24) & -16777216) | ((bytes[1] << 16) & 16711680)) | ((bytes[2] << 8) & 65280)) | ((bytes[3] << 0) & 255));
    }

    private static double readDouble(byte[] bytes) {
        return Double.longBitsToDouble(((((((((((long) bytes[0]) << 56) & -72057594037927936L) | ((((long) bytes[1]) << 48) & 71776119061217280L)) | ((((long) bytes[2]) << 40) & 280375465082880L)) | ((((long) bytes[3]) << 32) & 1095216660480L)) | ((((long) bytes[4]) << 24) & 4278190080L)) | ((((long) bytes[5]) << 16) & 16711680)) | ((((long) bytes[6]) << 8) & 65280)) | ((((long) bytes[7]) << 0) & 255));
    }

    private static char readChar(byte[] bytes) {
        return (char) (((bytes[0] << 8) & 65280) | ((bytes[1] << 0) & 255));
    }

    private static String readString(byte[] bytes) {
        return new String(bytes, Charset.forName("UTF-8"));
    }

    private static <T extends Parcelable> Object readParcelable(byte[] buffer, Creator<T> creator) {
        IwdsAssert.dieIf("ByteArrayUtils", creator == null, "creator == null");
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(buffer, 0, buffer.length);
        obtain.setDataPosition(0);
        Parcelable parcelable = (Parcelable) creator.createFromParcel(obtain);
        obtain.recycle();
        return parcelable;
    }

    private static <T extends SafeParcelable> Object readSafeParcelable(byte[] buffer, SafeParcelable.Creator<T> creator) {
        IwdsAssert.dieIf("ByteArrayUtils", creator == null, "creator == null");
        SafeParcel obtain = SafeParcel.obtain();
        obtain.unmarshall(buffer, 0, buffer.length);
        obtain.setDataPosition(0);
        SafeParcelable safeParcelable = (SafeParcelable) creator.createFromParcel(obtain);
        obtain.recycle();
        return safeParcelable;
    }

    private static byte decodeByte(Connection connection) throws IOException {
        byte[] bArr = new byte[1];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readByte(bArr);
    }

    private static String decodeString(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        int i = 0;
        byte[] bArr = new byte[decodeInt];
        InputStream inputStream = connection.getInputStream();
        while (decodeInt > 0) {
            int read = inputStream.read(bArr, i, decodeInt);
            i += read;
            decodeInt -= read;
        }
        return readString(bArr);
    }

    private static int decodeInt(Connection connection) throws IOException {
        byte[] bArr = new byte[4];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readInt(bArr);
    }

    private static short decodeShort(Connection connection) throws IOException {
        byte[] bArr = new byte[2];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readShort(bArr);
    }

    private static long decodeLong(Connection connection) throws IOException {
        byte[] bArr = new byte[8];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readLong(bArr);
    }

    private static float decodeFloat(Connection connection) throws IOException {
        byte[] bArr = new byte[4];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readFloat(bArr);
    }

    private static double decodeDouble(Connection connection) throws IOException {
        byte[] bArr = new byte[8];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readDouble(bArr);
    }

    private static char decodeChar(Connection connection) throws IOException {
        byte[] bArr = new byte[2];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readChar(bArr);
    }

    private static boolean decodeBoolean(Connection connection) throws IOException {
        byte[] bArr = new byte[1];
        int i = 0;
        int length = bArr.length;
        InputStream inputStream = connection.getInputStream();
        while (length > 0) {
            int read = inputStream.read(bArr, i, length);
            i += read;
            length -= read;
        }
        return readBoolean(bArr);
    }

    private static boolean[] decodeBooleanArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        boolean[] zArr = new boolean[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            zArr[i] = decodeBoolean(connection);
        }
        return zArr;
    }

    private static byte[] decodeByteArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        byte[] bArr = new byte[decodeInt];
        int i = 0;
        InputStream inputStream = connection.getInputStream();
        while (decodeInt > 0) {
            int read = inputStream.read(bArr, i, decodeInt);
            i += read;
            decodeInt -= read;
        }
        return bArr;
    }

    private static String[] decodeStringArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        String[] strArr = new String[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            strArr[i] = decodeString(connection);
        }
        return strArr;
    }

    private static int[] decodeIntArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        int[] iArr = new int[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            iArr[i] = decodeInt(connection);
        }
        return iArr;
    }

    private static long[] decodeLongArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        long[] jArr = new long[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            jArr[i] = decodeLong(connection);
        }
        return jArr;
    }

    private static short[] decodeShortArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        short[] sArr = new short[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            sArr[i] = decodeShort(connection);
        }
        return sArr;
    }

    private static float[] decodeFloatArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        float[] fArr = new float[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            fArr[i] = decodeFloat(connection);
        }
        return fArr;
    }

    private static double[] decodeDoubleArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        double[] dArr = new double[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            dArr[i] = decodeDouble(connection);
        }
        return dArr;
    }

    private static char[] decodeCharArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        char[] cArr = new char[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            cArr[i] = decodeChar(connection);
        }
        return cArr;
    }

    private static <T extends Parcelable> Creator<T> readParcelableCreator(String name) {
        Creator<T> creator;
        synchronized (sParcelableCreators) {
            creator = (Creator) sParcelableCreators.get(name);
            if (creator == null) {
                try {
                    creator = (Creator) Class.forName(name).getField("CREATOR").get(null);
                } catch (IllegalAccessException e) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Illegal access when unmarshalling: " + name);
                } catch (ClassNotFoundException e2) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Class not found when unmarshalling: " + name);
                } catch (ClassCastException e3) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                } catch (NoSuchFieldException e4) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                } catch (NullPointerException e5) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires the CREATOR object to be static on class " + name);
                }
                if (creator == null) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                }
                sParcelableCreators.put(name, creator);
            }
        }
        return creator;
    }

    private static <T extends SafeParcelable> SafeParcelable.Creator<T> readSafeParcelableCreator(String name) {
        SafeParcelable.Creator<T> creator;
        synchronized (sSafeParcelableCreators) {
            creator = (SafeParcelable.Creator) sSafeParcelableCreators.get(name);
            if (creator == null) {
                try {
                    creator = (SafeParcelable.Creator) Class.forName(name).getField("CREATOR").get(null);
                } catch (IllegalAccessException e) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Illegal access when unmarshalling: " + name);
                } catch (ClassNotFoundException e2) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Class not found when unmarshalling: " + name);
                } catch (ClassCastException e3) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                } catch (NoSuchFieldException e4) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                } catch (NullPointerException e5) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires the CREATOR object to be static on class " + name);
                }
                if (creator == null) {
                    IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable protocol requires a Parcelable.Creator object called  CREATOR on class " + name);
                }
                sSafeParcelableCreators.put(name, creator);
            }
        }
        return creator;
    }

    private static <T extends Parcelable> Object decodeParcelable(Connection connection, Creator<T> creator) throws IOException {
        Creator creator2;
        if (creator == null) {
            creator2 = readParcelableCreator(decodeString(connection));
        }
        int decodeInt = decodeInt(connection);
        byte[] bArr = new byte[decodeInt];
        int i = 0;
        InputStream inputStream = connection.getInputStream();
        while (decodeInt > 0) {
            int read = inputStream.read(bArr, i, decodeInt);
            i += read;
            decodeInt -= read;
        }
        return readParcelable(bArr, creator2);
    }

    private static <T extends SafeParcelable> Object decodeSafeParcelable(Connection connection, SafeParcelable.Creator<T> creator) throws IOException {
        SafeParcelable.Creator creator2;
        if (creator == null) {
            creator2 = readSafeParcelableCreator(decodeString(connection));
        }
        int decodeInt = decodeInt(connection);
        byte[] bArr = new byte[decodeInt];
        int i = 0;
        InputStream inputStream = connection.getInputStream();
        while (decodeInt > 0) {
            int read = inputStream.read(bArr, i, decodeInt);
            i += read;
            decodeInt -= read;
        }
        return readSafeParcelable(bArr, creator2);
    }

    private static HashMap decodeHashMap(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        HashMap hashMap = new HashMap(decodeInt);
        for (int i = 0; i < decodeInt; i++) {
            hashMap.put(decode(connection, null, null, null), decode(connection, null, null, null));
        }
        return hashMap;
    }

    private static ArrayList decodeArrayList(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        ArrayList arrayList = new ArrayList(decodeInt);
        for (int i = 0; i < decodeInt; i++) {
            arrayList.add(decode(connection, null, null, null));
        }
        return arrayList;
    }

    private static Object[] decodeArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        Object[] objArr = new Object[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            objArr[i] = decode(connection, null, null, null);
        }
        return objArr;
    }

    private static CharSequence decodeCharSequence(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        byte[] bArr = new byte[decodeInt];
        int i = 0;
        InputStream inputStream = connection.getInputStream();
        while (decodeInt > 0) {
            int read = inputStream.read(bArr, i, decodeInt);
            i += read;
            decodeInt -= read;
        }
        return readString(bArr);
    }

    private static CharSequence[] decodeCharSequenceArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        CharSequence[] charSequenceArr = new CharSequence[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            charSequenceArr[i] = decodeCharSequence(connection);
        }
        return charSequenceArr;
    }

    private static Serializable decodeSerializable(Connection connection) throws IOException {
        String decodeString = decodeString(connection);
        IwdsAssert.dieIf("ByteArrayUtils", decodeString == null, "Bad serializable name null");
        try {
            return (Serializable) new ObjectInputStream(new ByteArrayInputStream(decodeByteArray(connection))).readObject();
        } catch (IOException e) {
            IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable encountered IOException reading a Serializable object (name = " + decodeString + ")");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e2) {
            IwdsAssert.dieIf("ByteArrayUtils", true, "Parcelable encounteredClassNotFoundException reading a Serializable object(name = " + decodeString + ")");
            return null;
        }
    }

    private static SparseArray decodeSparseArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        SparseArray sparseArray = new SparseArray(decodeInt);
        for (int i = 0; i < decodeInt; i++) {
            sparseArray.put(decodeInt(connection), decode(connection, null, null, null));
        }
        return sparseArray;
    }

    private static SparseBooleanArray decodeSparseBooleanArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        SparseBooleanArray sparseBooleanArray = new SparseBooleanArray(decodeInt);
        for (int i = 0; i < decodeInt; i++) {
            sparseBooleanArray.put(decodeInt(connection), decodeBoolean(connection));
        }
        return sparseBooleanArray;
    }

    private static Parcelable[] decodeParcelableArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        Parcelable[] parcelableArr = new Parcelable[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            parcelableArr[i] = (Parcelable) decodeParcelable(connection, null);
        }
        return parcelableArr;
    }

    private static SafeParcelable[] decodeSafeParcelableArray(Connection connection) throws IOException {
        int decodeInt = decodeInt(connection);
        SafeParcelable[] safeParcelableArr = new SafeParcelable[decodeInt];
        for (int i = 0; i < decodeInt; i++) {
            safeParcelableArr[i] = (SafeParcelable) decodeSafeParcelable(connection, null);
        }
        return safeParcelableArr;
    }

    private static File decodeFile(Connection connection, TransferAdapterCallback callback) throws IOException, FileTransferException {
        int i;
        RandomAccessFile randomAccessFile;
        StatFs statFs = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            if (sDesiredStoragePath == null) {
                sDesiredStoragePath = Environment.getExternalStorageDirectory().getPath() + "/iwds";
            }
            File file = new File(sDesiredStoragePath);
            if (!(file.isDirectory() || file.mkdir())) {
                IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: can't create base directory " + file.getPath());
                IwdsException.throwFileTransferException(1);
            }
            statFs = new StatFs(file.getPath());
        } else {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: no external storage");
            IwdsException.throwFileTransferException(2);
        }
        String decodeString = decodeString(connection);
        if (decodeString == null || decodeString.isEmpty()) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: name is null or empty");
            IwdsException.throwFileTransferException(1);
        }
        File file2 = new File(sDesiredStoragePath, decodeString);
        long decodeLong = decodeLong(connection);
        if (decodeLong <= 0) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: invalid file length");
            IwdsException.throwFileTransferException(1);
        }
        if (decodeLong % ((long) 65536) == 0) {
            i = (int) (decodeLong / ((long) 65536));
        } else {
            i = (int) ((decodeLong / ((long) 65536)) + 1);
        }
        int decodeInt = decodeInt(connection);
        if (decodeInt > i || decodeInt < 0) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: chunk index out of bound");
            IwdsException.throwFileTransferException(1);
        }
        long j = (long) (decodeInt * 65536);
        if (j > file2.length()) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: file length less than already read length");
            IwdsException.throwFileTransferException(1);
        }
        long j2 = decodeLong - j;
        if (VERSION.SDK_INT >= 18) {
            if (statFs.getBlockSizeLong() * (statFs.getAvailableBlocksLong() - 4) < j2) {
                IwdsLog.m266e("ByteArrayUtils", "Unabled to receive file: not enough free space");
                IwdsException.throwFileTransferException(3);
            }
        } else if (((long) statFs.getBlockSize()) * (((long) statFs.getAvailableBlocks()) - 4) < j2) {
            IwdsLog.m266e("ByteArrayUtils", "Unabled to receive file: not enough free space");
            IwdsException.throwFileTransferException(3);
        }
        try {
            randomAccessFile = new RandomAccessFile(file2, "rwd");
        } catch (FileNotFoundException e) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: error create file " + decodeString);
            IwdsException.throwFileTransferException(1);
            randomAccessFile = null;
        }
        try {
            randomAccessFile.seek(j);
        } catch (IOException e2) {
            try {
                randomAccessFile.close();
            } catch (IOException e3) {
            }
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: error seek file to " + j);
            IwdsException.throwFileTransferException(1);
        }
        InputStream inputStream = connection.getInputStream();
        byte[] bArr = new byte[65536];
        int i2 = decodeInt;
        long j3 = j;
        j = j2;
        while (j > 0) {
            int min = (int) Math.min((long) 65536, j);
            int i3 = 0;
            int i4 = min;
            while (i4 > 0) {
                try {
                    int read = inputStream.read(bArr, i3, i4);
                    i4 -= read;
                    i3 += read;
                } catch (IOException e4) {
                    try {
                        randomAccessFile.close();
                    } catch (IOException e5) {
                    }
                    IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: connection io exception");
                    callback.onRecvFileInterrupted(i2);
                    throw e4;
                }
            }
            try {
                randomAccessFile.write(bArr, 0, min);
            } catch (IOException e6) {
                try {
                    randomAccessFile.close();
                } catch (IOException e7) {
                }
                IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: file write io exception");
                callback.onRecvFileInterrupted(i2);
                IwdsException.throwFileTransferException(1);
            }
            i4 = i2 + 1;
            if (i2 > i) {
                IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: index out of bound:, current=" + i4 + ", total=" + i);
                IwdsException.throwFileTransferException(1);
            }
            j3 += (long) min;
            j2 = j - ((long) min);
            callback.onRecvFileProgress(j3, decodeLong);
            j = j2;
            i2 = i4;
        }
        try {
            randomAccessFile.close();
        } catch (IOException e8) {
        }
        if (file2.length() != decodeLong) {
            IwdsLog.m266e("ByteArrayUtils", "Unable to receive file: error received file length: recvLen=" + file2.length() + ", sendLen=" + decodeLong);
            IwdsException.throwFileTransferException(1);
        }
        return file2;
    }

    public static <T1 extends Parcelable, T2 extends SafeParcelable> Object decode(Connection connection, Creator<T1> parcelableCreator, SafeParcelable.Creator<T2> safeParcelableCreator, TransferAdapterCallback callback) throws IOException, FileTransferException {
        boolean z = true;
        IwdsAssert.dieIf("ByteArrayUtils", connection == null, "connection == null");
        byte decodeByte = decodeByte(connection);
        switch (decodeByte) {
            case (byte) 0:
                return decodeString(connection);
            case (byte) 1:
                return Integer.valueOf(decodeInt(connection));
            case (byte) 2:
                return decodeHashMap(connection);
            case (byte) 3:
                return decodeSafeParcelable(connection, safeParcelableCreator);
            case (byte) 4:
                return decodeParcelable(connection, parcelableCreator);
            case (byte) 5:
                return Short.valueOf(decodeShort(connection));
            case (byte) 6:
                return Long.valueOf(decodeLong(connection));
            case (byte) 7:
                return Float.valueOf(decodeFloat(connection));
            case (byte) 8:
                return Double.valueOf(decodeDouble(connection));
            case (byte) 9:
                return Boolean.valueOf(decodeBoolean(connection));
            case (byte) 10:
                return decodeCharSequence(connection);
            case (byte) 11:
                return decodeArrayList(connection);
            case (byte) 12:
                return decodeSparseArray(connection);
            case (byte) 13:
                return decodeByteArray(connection);
            case (byte) 14:
                return decodeStringArray(connection);
            case (byte) 15:
                return decodeSafeParcelableArray(connection);
            case (byte) 16:
                return decodeParcelableArray(connection);
            case (byte) 17:
                return decodeArray(connection);
            case (byte) 18:
                return decodeIntArray(connection);
            case (byte) 19:
                return decodeLongArray(connection);
            case (byte) 20:
                return Byte.valueOf(decodeByte(connection));
            case (byte) 21:
                return decodeSerializable(connection);
            case (byte) 22:
                return decodeSparseBooleanArray(connection);
            case (byte) 23:
                return decodeBooleanArray(connection);
            case (byte) 24:
                return decodeCharSequenceArray(connection);
            case (byte) 25:
                return Character.valueOf(decodeChar(connection));
            case (byte) 26:
                return decodeShortArray(connection);
            case (byte) 27:
                return decodeFloatArray(connection);
            case (byte) 28:
                return decodeDoubleArray(connection);
            case (byte) 29:
                return decodeCharArray(connection);
            case (byte) 30:
                String str = "ByteArrayUtils";
                if (callback != null) {
                    z = false;
                }
                IwdsAssert.dieIf(str, z, "TransferAdapterCallback is null");
                return decodeFile(connection, callback);
            default:
                IwdsAssert.dieIf("ByteArrayUtils", true, "Unsupported object type code: " + decodeByte);
                return null;
        }
    }
}
