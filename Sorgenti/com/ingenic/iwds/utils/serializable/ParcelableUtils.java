package com.ingenic.iwds.utils.serializable;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ParcelableUtils {
    public static Parcel unmarshall(byte[] bytes) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bytes, 0, bytes.length);
        obtain.setDataPosition(0);
        return obtain;
    }

    public static <T> T unmarshall(byte[] bytes, Creator<T> creator) {
        Parcel unmarshall = unmarshall(bytes);
        T createFromParcel = creator.createFromParcel(unmarshall);
        unmarshall.recycle();
        return createFromParcel;
    }
}
