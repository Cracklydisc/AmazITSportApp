package com.ingenic.iwds.utils;

public class IwdsAssert {
    public static void dieIf(Object who, boolean condition, String message) {
        if (condition) {
            IwdsLog.m265e(who, "============= IWDS Assert Failed ============");
            IwdsLog.m265e(who, "Message: " + message);
            IwdsLog.m265e(who, "=============================================");
            Thread.dumpStack();
            IwdsLog.m265e(who, "============== IWDS Assert End ==============");
            ((IwdsAssert) null).die();
        }
    }

    public static void dieIf(String tag, boolean condition, String message) {
        if (condition) {
            IwdsLog.m266e(tag, "============= IWDS Assert Failed ============");
            IwdsLog.m266e(tag, "Message: " + message);
            IwdsLog.m266e(tag, "=============================================");
            Thread.dumpStack();
            IwdsLog.m266e(tag, "============== IWDS Assert End ==============");
            ((IwdsAssert) null).die();
        }
    }

    private void die() {
    }
}
