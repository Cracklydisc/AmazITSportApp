package com.ingenic.iwds.utils;

import android.util.SparseArray;

public class IwdsUtils {
    private IwdsUtils() {
    }

    public static <T> void addInArray(SparseArray<SparseArray<T>> array, int firstKey, int secondKey, T value) {
        if (array != null) {
            SparseArray sparseArray = (SparseArray) array.get(firstKey);
            if (sparseArray == null) {
                sparseArray = new SparseArray();
                synchronized (array) {
                    array.put(firstKey, sparseArray);
                }
            }
            synchronized (array) {
                sparseArray.put(secondKey, value);
            }
        }
    }

    public static <T> void deleteInArray(SparseArray<SparseArray<T>> array, int firstKey, int secondKey) {
        if (array != null) {
            SparseArray sparseArray = (SparseArray) array.get(firstKey);
            if (sparseArray != null) {
                synchronized (array) {
                    sparseArray.delete(secondKey);
                    if (sparseArray.size() == 0) {
                        array.delete(firstKey);
                    }
                }
            }
        }
    }
}
