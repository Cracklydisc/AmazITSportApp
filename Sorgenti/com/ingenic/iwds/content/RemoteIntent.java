package com.ingenic.iwds.content;

import android.content.ClipData;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.os.RemoteBundle;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class RemoteIntent implements Parcelable, Cloneable {
    public static final Creator<RemoteIntent> CREATOR = new C10991();
    private String f74a;
    private Uri f75b;
    private String f76c;
    private int f77d;
    private String f78e;
    private ComponentName f79f;
    private Rect f80g;
    private Set<String> f81h;
    private RemoteIntent f82i;
    private ClipData f83j;
    private RemoteBundle f84k;

    static class C10991 implements Creator<RemoteIntent> {
        C10991() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m64a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m65a(i);
        }

        public RemoteIntent m64a(Parcel parcel) {
            return new RemoteIntent(parcel);
        }

        public RemoteIntent[] m65a(int i) {
            return new RemoteIntent[i];
        }
    }

    public RemoteIntent(RemoteIntent in) {
        this.f74a = in.f74a;
        this.f75b = in.f75b;
        this.f76c = in.f76c;
        this.f77d = in.f77d;
        this.f78e = in.f78e;
        this.f79f = in.f79f;
        if (in.f80g != null) {
            this.f80g = new Rect(in.f80g);
        }
        if (in.f81h != null) {
            this.f81h = new HashSet(in.f81h);
        }
        if (in.f82i != null) {
            this.f82i = new RemoteIntent(in.f82i);
        }
        if (in.f83j != null) {
            this.f83j = new ClipData(in.f83j);
        }
        if (in.f84k != null) {
            this.f84k = new RemoteBundle(in.f84k);
        }
    }

    protected RemoteIntent(Intent intent) {
        this.f74a = intent.getAction();
        this.f75b = intent.getData();
        this.f76c = intent.getType();
        this.f77d = intent.getFlags();
        this.f78e = intent.getPackage();
        this.f79f = intent.getComponent();
        this.f80g = intent.getSourceBounds();
        Collection categories = intent.getCategories();
        if (categories != null) {
            this.f81h = new HashSet(categories);
        }
        Intent selector = intent.getSelector();
        if (selector != null) {
            this.f82i = new RemoteIntent(selector);
        }
        ClipData clipData = intent.getClipData();
        if (clipData != null) {
            this.f83j = new ClipData(clipData);
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.f84k = RemoteBundle.fromBunble(extras);
        }
    }

    protected RemoteIntent(Parcel source) {
        this.f74a = source.readString();
        this.f75b = (Uri) Uri.CREATOR.createFromParcel(source);
        this.f76c = source.readString();
        this.f77d = source.readInt();
        this.f78e = source.readString();
        this.f79f = ComponentName.readFromParcel(source);
        if (source.readInt() != 0) {
            this.f80g = (Rect) Rect.CREATOR.createFromParcel(source);
        }
        int readInt = source.readInt();
        if (readInt > 0) {
            this.f81h = new HashSet();
            for (int i = 0; i < readInt; i++) {
                this.f81h.add(source.readString().intern());
            }
        } else {
            this.f81h = null;
        }
        if (source.readInt() != 0) {
            this.f82i = (RemoteIntent) CREATOR.createFromParcel(source);
        }
        if (source.readInt() != 0) {
            this.f83j = (ClipData) ClipData.CREATOR.createFromParcel(source);
        }
        if (source.readInt() != 0) {
            this.f84k = (RemoteBundle) RemoteBundle.CREATOR.createFromParcel(source);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.f74a);
        Uri.writeToParcel(dest, this.f75b);
        dest.writeString(this.f76c);
        dest.writeInt(this.f77d);
        dest.writeString(this.f78e);
        ComponentName.writeToParcel(this.f79f, dest);
        if (this.f80g != null) {
            dest.writeInt(1);
            this.f80g.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f81h != null) {
            dest.writeInt(this.f81h.size());
            for (String writeString : this.f81h) {
                dest.writeString(writeString);
            }
        } else {
            dest.writeInt(0);
        }
        if (this.f82i != null) {
            dest.writeInt(1);
            this.f82i.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f83j != null) {
            dest.writeInt(1);
            this.f83j.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f84k != null) {
            dest.writeInt(1);
            this.f84k.writeToParcel(dest, flags);
            return;
        }
        dest.writeInt(0);
    }

    public Object clone() {
        return new RemoteIntent(this);
    }

    public static RemoteIntent fromIntent(Intent intent) {
        if (intent == null) {
            return null;
        }
        return new RemoteIntent(intent);
    }

    public Intent toIntent() {
        Intent toIntent;
        Bundle bundle = null;
        Intent intent = new Intent(this.f74a);
        intent.setData(this.f75b);
        intent.setType(this.f76c);
        intent.setFlags(this.f77d);
        intent.setPackage(this.f78e);
        intent.setComponent(this.f79f);
        intent.setSourceBounds(this.f80g);
        if (this.f81h != null) {
            for (String intern : this.f81h) {
                intent.addCategory(intern.intern());
            }
        }
        if (this.f82i != null) {
            toIntent = this.f82i.toIntent();
        } else {
            toIntent = null;
        }
        intent.setSelector(toIntent);
        intent.setClipData(this.f83j);
        if (this.f84k != null) {
            bundle = this.f84k.toBundle();
        }
        intent.putExtras(bundle);
        return intent;
    }
}
