package com.ingenic.iwds;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.ingenic.iwds.utils.IwdsLog;

public class IwdsService extends Service {
    public void onCreate() {
        IwdsLog.m264d(this, "onCreate()");
        CharSequence loadLabel = getApplication().getApplicationInfo().loadLabel(getPackageManager());
        Notification notification = new Notification(getApplication().getApplicationInfo().icon, loadLabel, System.currentTimeMillis());
        Intent intent = new Intent("com.ingenic.iwds.notification.clicked");
        intent.setFlags(intent.getFlags() | 32768);
        notification.setLatestEventInfo(this, loadLabel, "", PendingIntent.getActivity(this, 0, intent, 0));
        notification.flags |= 2;
        startForeground(9999, notification);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
