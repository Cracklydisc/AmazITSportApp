package com.ingenic.iwds.remotebroadcast;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.content.RemoteIntent;

class RemoteBroadcast implements Parcelable {
    public static final Creator<RemoteBroadcast> CREATOR = new C11381();
    private int f100a;
    private RemoteIntent f101b;
    private String f102c;
    private boolean f103d;
    private boolean f104e;

    static class C11381 implements Creator<RemoteBroadcast> {
        C11381() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m79a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m80a(i);
        }

        public RemoteBroadcast m79a(Parcel parcel) {
            return new RemoteBroadcast(parcel);
        }

        public RemoteBroadcast[] m80a(int i) {
            return new RemoteBroadcast[i];
        }
    }

    public RemoteBroadcast(int callerId, Intent intent, String permission) {
        this(callerId, intent, permission, false, false);
    }

    public RemoteBroadcast(int callerId, Intent intent, String permission, boolean sticky, boolean ordered) {
        this(callerId, RemoteIntent.fromIntent(intent), permission, sticky, ordered);
    }

    public RemoteBroadcast(int callerId, RemoteIntent intent, String permission, boolean sticky, boolean ordered) {
        this.f100a = callerId;
        this.f101b = intent;
        this.f102c = permission;
        this.f103d = sticky;
        this.f104e = ordered;
    }

    protected RemoteBroadcast(Parcel in) {
        readFromParcel(in);
    }

    public int getCallerId() {
        return this.f100a;
    }

    public boolean isSticky() {
        return this.f103d;
    }

    public boolean isOrdered() {
        return this.f104e;
    }

    public RemoteIntent getIntent() {
        return this.f101b;
    }

    public String getPermission() {
        return this.f102c;
    }

    public int describeContents() {
        return this.f101b != null ? this.f101b.describeContents() : 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        dest.writeInt(this.f100a);
        if (this.f101b != null) {
            dest.writeInt(1);
            this.f101b.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.f102c);
        if (this.f103d) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (!this.f104e) {
            i2 = 0;
        }
        dest.writeInt(i2);
    }

    public void readFromParcel(Parcel in) {
        boolean z;
        boolean z2 = true;
        this.f100a = in.readInt();
        if (in.readInt() != 0) {
            this.f101b = (RemoteIntent) RemoteIntent.CREATOR.createFromParcel(in);
        }
        this.f102c = in.readString();
        if (in.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.f103d = z;
        if (in.readInt() == 0) {
            z2 = false;
        }
        this.f104e = z2;
    }
}
