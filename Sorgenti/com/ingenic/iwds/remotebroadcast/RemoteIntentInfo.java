package com.ingenic.iwds.remotebroadcast;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.content.RemoteIntent;

/* compiled from: RemoteBroadcastInfo */
class RemoteIntentInfo extends C1150a implements Parcelable {
    public static final Creator<RemoteIntentInfo> CREATOR = new C11511();
    private RemoteIntent f141c;

    /* compiled from: RemoteBroadcastInfo */
    static class C11511 implements Creator<RemoteIntentInfo> {
        C11511() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m144a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m145a(i);
        }

        public RemoteIntentInfo m144a(Parcel parcel) {
            return new RemoteIntentInfo(parcel);
        }

        public RemoteIntentInfo[] m145a(int i) {
            return new RemoteIntentInfo[i];
        }
    }

    public RemoteIntentInfo(int callerId, int id, RemoteIntent intent) {
        this.a = callerId;
        this.b = id;
        this.f141c = intent;
    }

    protected RemoteIntentInfo(Parcel in) {
        readFromParcel(in);
    }

    public RemoteIntent getIntent() {
        return this.f141c;
    }

    public int describeContents() {
        return this.f141c != null ? this.f141c.describeContents() : 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        if (this.f141c != null) {
            dest.writeInt(1);
            this.f141c.writeToParcel(dest, flags);
            return;
        }
        dest.writeInt(0);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        if (in.readInt() != 0) {
            this.f141c = (RemoteIntent) RemoteIntent.CREATOR.createFromParcel(in);
        }
    }

    public String toString() {
        return "RemoteIntentInfo[-id" + this.b + "-Intent:" + this.f141c + "]";
    }
}
