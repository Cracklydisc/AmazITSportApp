package com.ingenic.iwds.remotebroadcast;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteBroadcastInfo */
class UnregisterInfo extends C1150a implements Parcelable {
    public static final Creator<UnregisterInfo> CREATOR = new C11521();

    /* compiled from: RemoteBroadcastInfo */
    static class C11521 implements Creator<UnregisterInfo> {
        C11521() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m146a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m147a(i);
        }

        public UnregisterInfo m146a(Parcel parcel) {
            return new UnregisterInfo(parcel);
        }

        public UnregisterInfo[] m147a(int i) {
            return new UnregisterInfo[i];
        }
    }

    public UnregisterInfo(int callerId, int id) {
        this.a = callerId;
        this.b = id;
    }

    protected UnregisterInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
    }
}
