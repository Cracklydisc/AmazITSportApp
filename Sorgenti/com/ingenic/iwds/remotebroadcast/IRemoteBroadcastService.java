package com.ingenic.iwds.remotebroadcast;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

interface IRemoteBroadcastService extends IInterface {

    public static class RemoteBroadcastServiceProxy implements IRemoteBroadcastService {
        private IBinder f99a;

        public RemoteBroadcastServiceProxy(IBinder remote) {
            this.f99a = remote;
        }

        public IBinder asBinder() {
            return this.f99a;
        }

        public int registerRemoteBroadcastCallback(IRemoteBroadcastCallback callback) {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
            try {
                this.f99a.transact(1, obtain, obtain2, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain2.readException();
            int readInt = obtain2.readInt();
            obtain.recycle();
            obtain2.recycle();
            return readInt;
        }

        public void unregisterRemoteBroadcastCallback(int callerId) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            obtain.writeInt(callerId);
            try {
                this.f99a.transact(2, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void registerRemoteReceiver(int callerId, int id, IntentFilter filter, String requestPermission) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            if (filter != null) {
                obtain.writeInt(1);
                filter.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            obtain.writeString(requestPermission);
            try {
                this.f99a.transact(3, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void unregisterRemoteReceiver(int callerId, int id) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            obtain.writeInt(callerId);
            obtain.writeInt(id);
            try {
                this.f99a.transact(4, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void sendRemoteBroadcast(int callerId, Intent intent, String perm) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            obtain.writeInt(callerId);
            if (intent != null) {
                obtain.writeInt(1);
                intent.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            obtain.writeString(perm);
            try {
                this.f99a.transact(5, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }
    }

    public static abstract class Stub extends Binder implements IRemoteBroadcastService {
        public static IRemoteBroadcastService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IRemoteBroadcastService iRemoteBroadcastService = (IRemoteBroadcastService) obj.queryLocalInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
            return iRemoteBroadcastService == null ? new RemoteBroadcastServiceProxy(obj) : iRemoteBroadcastService;
        }

        public IBinder asBinder() {
            return this;
        }

        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Intent intent = null;
            int readInt;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
                    int registerRemoteBroadcastCallback = registerRemoteBroadcastCallback(com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(registerRemoteBroadcastCallback);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
                    unregisterRemoteBroadcastCallback(data.readInt());
                    return true;
                case 3:
                    IntentFilter intentFilter;
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
                    readInt = data.readInt();
                    int readInt2 = data.readInt();
                    if (data.readInt() != 0) {
                        intentFilter = (IntentFilter) IntentFilter.CREATOR.createFromParcel(data);
                    }
                    registerRemoteReceiver(readInt, readInt2, intentFilter, data.readString());
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
                    unregisterRemoteReceiver(data.readInt(), data.readInt());
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService");
                    readInt = data.readInt();
                    if (data.readInt() != 0) {
                        intent = (Intent) Intent.CREATOR.createFromParcel(data);
                    }
                    sendRemoteBroadcast(readInt, intent, data.readString());
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    int registerRemoteBroadcastCallback(IRemoteBroadcastCallback iRemoteBroadcastCallback);

    void registerRemoteReceiver(int i, int i2, IntentFilter intentFilter, String str);

    void sendRemoteBroadcast(int i, Intent intent, String str);

    void unregisterRemoteBroadcastCallback(int i);

    void unregisterRemoteReceiver(int i, int i2);
}
