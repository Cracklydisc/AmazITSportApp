package com.ingenic.iwds.remotebroadcast;

/* compiled from: RemoteBroadcastInfo */
abstract class C1150a {
    protected int f137a;
    protected int f138b;

    C1150a() {
    }

    public int getCallerId() {
        return this.f137a;
    }

    public int getId() {
        return this.f138b;
    }
}
