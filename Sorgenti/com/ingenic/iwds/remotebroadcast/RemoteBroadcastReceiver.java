package com.ingenic.iwds.remotebroadcast;

import android.content.Context;
import android.content.Intent;

public abstract class RemoteBroadcastReceiver {
    public abstract void onReceive(Context context, Intent intent);
}
