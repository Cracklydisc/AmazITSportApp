package com.ingenic.iwds.remotebroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.SparseArray;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.content.RemoteIntent;
import com.ingenic.iwds.datatransactor.DataTransactor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.utils.IwdsUtils;
import com.ingenic.iwds.utils.SimpleIDAlloter;

public class RemoteBroadcastProxy {
    private static RemoteBroadcastProxy f122a;
    private final Context f123b;
    private final DataTransactor f124c;
    private final C1145b f125d;
    private SparseArray<IRemoteBroadcastCallback> f126e;
    private SparseArray<SparseArray<BroadcastReceiver>> f127f;
    private SparseArray<SparseArray<RemoteIntentFilterInfo>> f128g;
    private SparseArray<SparseArray<RemoteIntentFilterInfo>> f129h;
    private volatile boolean f130i = false;
    private HandlerThread f131j;
    private C1144a f132k;
    private SimpleIDAlloter f133l;

    private class C1144a extends Handler {
        final /* synthetic */ RemoteBroadcastProxy f117a;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.f117a.m101a(msg.arg1, (IRemoteBroadcastCallback) msg.obj);
                    return;
                case 2:
                    this.f117a.m129d((RemoteIntentFilterInfo) msg.obj);
                    return;
                case 3:
                    this.f117a.m124c(msg.arg1, msg.arg2);
                    return;
                case 4:
                    this.f117a.m130e();
                    return;
                case 5:
                    this.f117a.m127d();
                    return;
                case 6:
                    this.f117a.m118b();
                    return;
                case 7:
                    this.f117a.m106a((DataTransactResult) msg.obj);
                    return;
                case 8:
                    this.f117a.m115a(msg.obj);
                    return;
                case 9:
                    this.f117a.m123c();
                    return;
                case 10:
                    this.f117a.m99a();
                    return;
                default:
                    return;
            }
        }
    }

    private class C1145b implements DataTransactorCallback {
        final /* synthetic */ RemoteBroadcastProxy f118a;

        private C1145b(RemoteBroadcastProxy remoteBroadcastProxy) {
            this.f118a = remoteBroadcastProxy;
        }

        public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        }

        public void onChannelAvailable(boolean isAvailable) {
            this.f118a.f130i = isAvailable;
            if (isAvailable) {
                this.f118a.f132k.sendEmptyMessage(5);
                return;
            }
            this.f118a.f132k.sendEmptyMessage(4);
            this.f118a.f132k.sendEmptyMessage(6);
        }

        public void onSendResult(DataTransactResult result) {
            this.f118a.f132k.obtainMessage(7, result).sendToTarget();
        }

        public void onDataArrived(Object object) {
            this.f118a.f132k.obtainMessage(8, object).sendToTarget();
        }

        public void onSendFileProgress(int progress) {
        }

        public void onRecvFileProgress(int progress) {
        }
    }

    private class C1146c extends BroadcastReceiver {
        final /* synthetic */ RemoteBroadcastProxy f119a;
        private int f120b;
        private int f121c;

        public C1146c(RemoteBroadcastProxy remoteBroadcastProxy, int i, int i2) {
            this.f119a = remoteBroadcastProxy;
            this.f120b = i;
            this.f121c = i2;
        }

        public void onReceive(Context context, Intent intent) {
            if (!intent.getBooleanExtra("is_remote_broadcast", false)) {
                this.f119a.f124c.send(new RemoteIntentInfo(this.f120b, this.f121c, RemoteIntent.fromIntent(intent)));
            }
        }
    }

    public static synchronized RemoteBroadcastProxy getInstance(Context context) {
        RemoteBroadcastProxy remoteBroadcastProxy;
        synchronized (RemoteBroadcastProxy.class) {
            if (f122a == null) {
                f122a = new RemoteBroadcastProxy(context);
            }
            remoteBroadcastProxy = f122a;
        }
        return remoteBroadcastProxy;
    }

    private RemoteBroadcastProxy(Context context) {
        this.f123b = context;
        this.f133l = SimpleIDAlloter.newInstance();
        this.f125d = new C1145b();
        this.f124c = new DataTransactor(context, this.f125d, "94D8C7BC-0AFC-E5D4-D10A-1D381DF2CD72");
    }

    public void stopTransaction() {
        if (this.f124c != null && this.f124c.isStarted()) {
            this.f124c.stop();
        }
        this.f132k.sendEmptyMessage(10);
    }

    private void m99a() {
        if (this.f131j != null) {
            this.f131j.quit();
            this.f131j = null;
        }
        this.f132k = null;
    }

    protected void finalize() throws Throwable {
        this.f132k.sendEmptyMessage(9);
        stopTransaction();
        super.finalize();
    }

    void m138a(int i, int i2, IntentFilter intentFilter, String str) {
        m126c(new RemoteIntentFilterInfo(i, i2, intentFilter, str));
    }

    void m137a(int i, int i2) {
        this.f124c.send(new UnregisterInfo(i, i2));
        this.f132k.obtainMessage(3, i, i2).sendToTarget();
    }

    private void m113a(RemoteIntentInfo remoteIntentInfo) {
        RemoteIntent intent = remoteIntentInfo.getIntent();
        m100a(remoteIntentInfo.getCallerId(), remoteIntentInfo.getId(), intent != null ? intent.toIntent() : null);
    }

    private void m100a(int i, int i2, Intent intent) {
        if (this.f126e != null) {
            IRemoteBroadcastCallback iRemoteBroadcastCallback = (IRemoteBroadcastCallback) this.f126e.get(i);
            if (iRemoteBroadcastCallback != null) {
                iRemoteBroadcastCallback.performReceive(i2, intent);
            }
        }
    }

    private void m112a(RemoteIntentFilterInfo remoteIntentFilterInfo) {
        if (remoteIntentFilterInfo != null) {
            if (this.f127f == null) {
                this.f127f = new SparseArray();
            }
            int callerId = remoteIntentFilterInfo.getCallerId();
            int id = remoteIntentFilterInfo.getId();
            BroadcastReceiver c1146c = new C1146c(this, callerId, id);
            IwdsUtils.addInArray(this.f127f, callerId, id, c1146c);
            this.f123b.registerReceiver(c1146c, remoteIntentFilterInfo.getFilter(), remoteIntentFilterInfo.getPermission(), null);
        }
    }

    private void m119b(int i, int i2) {
        if (this.f127f != null) {
            SparseArray sparseArray = (SparseArray) this.f127f.get(i);
            if (sparseArray != null) {
                BroadcastReceiver broadcastReceiver = (BroadcastReceiver) sparseArray.get(i2);
                if (broadcastReceiver != null) {
                    synchronized (this.f127f) {
                        sparseArray.delete(i2);
                        if (sparseArray.size() == 0) {
                            this.f127f.delete(i);
                        }
                    }
                    if (broadcastReceiver != null) {
                        this.f123b.unregisterReceiver(broadcastReceiver);
                    }
                }
            }
        }
    }

    private void m118b() {
        if (this.f127f != null) {
            int size = this.f127f.size();
            for (int i = 0; i < size; i++) {
                SparseArray sparseArray = (SparseArray) this.f127f.valueAt(i);
                int size2 = sparseArray.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    this.f123b.unregisterReceiver((BroadcastReceiver) sparseArray.valueAt(size2));
                }
                synchronized (this.f127f) {
                    sparseArray.clear();
                }
            }
            synchronized (this.f127f) {
                this.f127f.clear();
            }
        }
    }

    void m139a(int i, Intent intent, String str) {
        m140a(new RemoteBroadcast(i, intent, str));
    }

    void m140a(RemoteBroadcast remoteBroadcast) {
        this.f124c.send((Object) remoteBroadcast);
    }

    int m135a(IRemoteBroadcastCallback iRemoteBroadcastCallback) {
        if (iRemoteBroadcastCallback == null) {
            return 0;
        }
        int allocation = this.f133l.allocation();
        this.f132k.obtainMessage(1, allocation, -1, iRemoteBroadcastCallback).sendToTarget();
        return allocation;
    }

    private void m101a(int i, IRemoteBroadcastCallback iRemoteBroadcastCallback) {
        if (iRemoteBroadcastCallback != null) {
            if (this.f126e == null) {
                this.f126e = new SparseArray();
            }
            synchronized (this.f126e) {
                this.f126e.put(i, iRemoteBroadcastCallback);
            }
        }
    }

    void m136a(int i) {
        if (this.f126e == null) {
            this.f133l.initialize();
            return;
        }
        synchronized (this.f126e) {
            this.f126e.delete(i);
        }
        if (this.f126e.size() == 0) {
            this.f133l.initialize();
        }
    }

    private void m121b(RemoteIntentFilterInfo remoteIntentFilterInfo) {
        if (this.f128g == null) {
            this.f128g = new SparseArray();
        }
        IwdsUtils.addInArray(this.f128g, remoteIntentFilterInfo.getCallerId(), remoteIntentFilterInfo.getId(), remoteIntentFilterInfo);
    }

    private void m123c() {
        if (this.f128g != null) {
            synchronized (this.f128g) {
                this.f128g.clear();
            }
        }
        if (this.f129h != null) {
            synchronized (this.f129h) {
                this.f129h.clear();
            }
        }
    }

    private void m124c(int i, int i2) {
        IwdsUtils.deleteInArray(this.f129h, i, i2);
        IwdsUtils.deleteInArray(this.f128g, i, i2);
    }

    private void m127d() {
        if (this.f129h != null) {
            for (int size = this.f129h.size() - 1; size >= 0; size--) {
                SparseArray sparseArray = (SparseArray) this.f129h.valueAt(size);
                int size2 = sparseArray.size() - 1;
                while (size2 >= 0) {
                    if (this.f130i) {
                        this.f124c.send((RemoteIntentFilterInfo) sparseArray.valueAt(size2));
                        synchronized (this.f129h) {
                            sparseArray.removeAt(size2);
                            if (sparseArray.size() == 0) {
                                this.f129h.removeAt(size);
                            }
                        }
                        size2--;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void m126c(RemoteIntentFilterInfo remoteIntentFilterInfo) {
        if (this.f130i) {
            this.f124c.send((Object) remoteIntentFilterInfo);
        } else {
            this.f132k.obtainMessage(2, remoteIntentFilterInfo).sendToTarget();
        }
    }

    private void m129d(RemoteIntentFilterInfo remoteIntentFilterInfo) {
        if (this.f129h == null) {
            this.f129h = new SparseArray();
        }
        IwdsUtils.addInArray(this.f129h, remoteIntentFilterInfo.getCallerId(), remoteIntentFilterInfo.getId(), remoteIntentFilterInfo);
    }

    private void m130e() {
        if (this.f128g != null) {
            if (this.f129h == null) {
                this.f129h = new SparseArray();
            }
            int size = this.f128g.size();
            for (int i = 0; i < size; i++) {
                int keyAt = this.f128g.keyAt(i);
                SparseArray sparseArray = (SparseArray) this.f128g.valueAt(i);
                synchronized (this.f129h) {
                    this.f129h.put(keyAt, sparseArray);
                }
            }
            synchronized (this.f128g) {
                this.f128g.clear();
            }
        }
    }

    private void m106a(DataTransactResult dataTransactResult) {
        int resultCode = dataTransactResult.getResultCode();
        Object transferedObject = dataTransactResult.getTransferedObject();
        if (transferedObject instanceof RemoteBroadcast) {
            m102a(resultCode, (RemoteBroadcast) transferedObject);
        } else if (transferedObject instanceof RemoteIntentFilterInfo) {
            m103a(resultCode, (RemoteIntentFilterInfo) transferedObject);
        } else if (transferedObject instanceof RemoteIntentInfo) {
            m104a(resultCode, (RemoteIntentInfo) transferedObject);
        } else if (transferedObject instanceof UnregisterInfo) {
            m105a(resultCode, (UnregisterInfo) transferedObject);
        }
    }

    private void m102a(int i, RemoteBroadcast remoteBroadcast) {
        IRemoteBroadcastCallback iRemoteBroadcastCallback = (IRemoteBroadcastCallback) this.f126e.get(remoteBroadcast.getCallerId());
        if (iRemoteBroadcastCallback != null) {
            RemoteIntent intent = remoteBroadcast.getIntent();
            iRemoteBroadcastCallback.performSendResult(intent != null ? intent.toIntent() : null, remoteBroadcast.getPermission(), i);
        }
    }

    private void m103a(int i, RemoteIntentFilterInfo remoteIntentFilterInfo) {
        if (i == 0) {
            m121b(remoteIntentFilterInfo);
        } else {
            m129d(remoteIntentFilterInfo);
        }
    }

    private void m104a(int i, RemoteIntentInfo remoteIntentInfo) {
        if (i != 0) {
            IwdsLog.m265e((Object) this, "Local broadcast has received.But send to remote failed: " + i);
        }
    }

    private void m105a(int i, UnregisterInfo unregisterInfo) {
    }

    private void m115a(Object obj) {
        if (obj instanceof RemoteBroadcast) {
            m120b((RemoteBroadcast) obj);
        } else if (obj instanceof RemoteIntentFilterInfo) {
            m132e((RemoteIntentFilterInfo) obj);
        } else if (obj instanceof RemoteIntentInfo) {
            m122b((RemoteIntentInfo) obj);
        } else if (obj instanceof UnregisterInfo) {
            m114a((UnregisterInfo) obj);
        }
    }

    private void m120b(RemoteBroadcast remoteBroadcast) {
        RemoteIntent intent = remoteBroadcast.getIntent();
        Intent toIntent = intent != null ? intent.toIntent() : null;
        if (toIntent == null) {
            toIntent = new Intent();
        }
        toIntent.putExtra("is_remote_broadcast", true);
        String permission = remoteBroadcast.getPermission();
        boolean isSticky = remoteBroadcast.isSticky();
        boolean isOrdered = remoteBroadcast.isOrdered();
        if (isSticky && isOrdered) {
            IwdsLog.m265e((Object) this, "Unsupport to send broadcast with sticky & ordered!");
        } else if (isSticky) {
            this.f123b.sendStickyBroadcast(toIntent);
        } else if (isOrdered) {
            this.f123b.sendOrderedBroadcast(toIntent, permission);
        } else {
            this.f123b.sendBroadcast(toIntent, permission);
        }
    }

    private void m132e(RemoteIntentFilterInfo remoteIntentFilterInfo) {
        m112a(remoteIntentFilterInfo);
    }

    private void m122b(RemoteIntentInfo remoteIntentInfo) {
        m113a(remoteIntentInfo);
    }

    private void m114a(UnregisterInfo unregisterInfo) {
        m119b(unregisterInfo.getCallerId(), unregisterInfo.getId());
    }
}
