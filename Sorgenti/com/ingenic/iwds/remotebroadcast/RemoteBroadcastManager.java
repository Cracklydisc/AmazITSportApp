package com.ingenic.iwds.remotebroadcast;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback.Stub;
import com.ingenic.iwds.utils.SimpleIDAlloter;

public class RemoteBroadcastManager extends ServiceManagerContext {
    private IRemoteBroadcastService f109a;
    private Handler f110b;
    private int f111c;
    private RemoteBroadcastCallback f112d;
    private SparseArray<RemoteBroadcastReceiver> f113e;
    private HandlerThread f114f;
    private Handler f115g;
    private SimpleIDAlloter f116h;

    class C11391 extends ServiceClientProxy {
        final /* synthetic */ RemoteBroadcastManager f105a;

        public void onServiceDisconnected(boolean unexpected) {
            this.f105a.m90a(unexpected);
        }

        public void onServiceConnected(IBinder binder) {
            this.f105a.m84a(binder);
        }
    }

    public interface RemoteBroadcastCallback {
        void onCallerError(int i);

        void onSendResult(Intent intent, String str, int i);
    }

    private class C1140a extends Handler {
        final /* synthetic */ RemoteBroadcastManager f106a;

        public C1140a(RemoteBroadcastManager remoteBroadcastManager, Looper looper) {
            this.f106a = remoteBroadcastManager;
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (this.f106a.f112d != null) {
                        this.f106a.f112d.onCallerError(msg.arg1);
                        return;
                    }
                    return;
                case 3:
                    this.f106a.m83a(msg.arg1, (RemoteBroadcastReceiver) msg.obj);
                    return;
                case 4:
                    this.f106a.m89a((RemoteBroadcastReceiver) msg.obj);
                    return;
                case 6:
                    this.f106a.m94b(msg.arg1 != 0);
                    return;
                case 7:
                    this.f106a.m92b();
                    return;
                default:
                    return;
            }
        }
    }

    private class C1141b extends Stub {
        final /* synthetic */ RemoteBroadcastManager f107a;

        private C1141b(RemoteBroadcastManager remoteBroadcastManager) {
            this.f107a = remoteBroadcastManager;
        }

        public void performSendResult(Intent intent, String perm, int resultCode) {
            Message obtainMessage = this.f107a.f110b.obtainMessage(2, resultCode, -1, intent);
            Bundle bundle = new Bundle();
            bundle.putString("permission", perm);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
        }

        public void performReceive(int id, Intent intent) {
            this.f107a.f110b.obtainMessage(5, id, -1, intent).sendToTarget();
        }
    }

    private class C1142c extends Handler {
        final /* synthetic */ RemoteBroadcastManager f108a;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 2:
                    if (this.f108a.f112d != null) {
                        String string;
                        Bundle data = msg.getData();
                        if (data != null) {
                            string = data.getString("permission", null);
                        } else {
                            string = null;
                        }
                        this.f108a.f112d.onSendResult((Intent) msg.obj, string, msg.arg1);
                        return;
                    }
                    return;
                case 5:
                    int i = msg.arg1;
                    if (i >= 0 && msg.obj != null) {
                        RemoteBroadcastReceiver remoteBroadcastReceiver = (RemoteBroadcastReceiver) this.f108a.f113e.get(i);
                        if (remoteBroadcastReceiver != null) {
                            remoteBroadcastReceiver.onReceive(this.f108a.getContext(), (Intent) msg.obj);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void m81a() {
        if (this.f114f == null) {
            this.f114f = new HandlerThread("RemoteBroadcast");
        }
        this.f114f.start();
        this.f115g = new C1140a(this, this.f114f.getLooper());
    }

    private void m84a(IBinder iBinder) {
        m81a();
        this.f109a = IRemoteBroadcastService.Stub.asInterface(iBinder);
        if (this.f109a != null) {
            this.f111c = this.f109a.registerRemoteBroadcastCallback(new C1141b());
        }
    }

    private void m90a(boolean z) {
        this.f115g.obtainMessage(6, z ? 1 : 0, -1).sendToTarget();
        if (this.f109a != null) {
            this.f109a.unregisterRemoteBroadcastCallback(this.f111c);
        }
        this.f115g.sendEmptyMessage(7);
    }

    private void m94b(boolean z) {
        if (this.f113e != null) {
            int size = this.f113e.size();
            if (size > 0) {
                if (!(this.f109a == null || z)) {
                    for (int i = 0; i < size; i++) {
                        this.f109a.unregisterRemoteReceiver(this.f111c, this.f113e.keyAt(i));
                    }
                }
                synchronized (this.f113e) {
                    this.f113e.clear();
                }
                if (!z) {
                    throw new RuntimeException("Are you missing a call to unregisterRemoteReceiver()?");
                }
            }
        }
    }

    private void m92b() {
        if (this.f114f != null) {
            this.f114f.quit();
            this.f114f = null;
        }
        this.f115g = null;
    }

    private void m83a(int i, RemoteBroadcastReceiver remoteBroadcastReceiver) {
        if (this.f113e == null) {
            this.f113e = new SparseArray();
        }
        synchronized (this.f113e) {
            this.f113e.put(i, remoteBroadcastReceiver);
        }
    }

    private void m89a(RemoteBroadcastReceiver remoteBroadcastReceiver) {
        if (this.f111c <= 0) {
            this.f110b.obtainMessage(1, this.f111c, -1).sendToTarget();
        } else if (this.f113e == null) {
            this.f116h.initialize();
        } else {
            int indexOfValue = this.f113e.indexOfValue(remoteBroadcastReceiver);
            int size = this.f113e.size();
            if (indexOfValue >= 0 && indexOfValue < size) {
                m82a(indexOfValue);
                this.f109a.unregisterRemoteReceiver(this.f111c, this.f113e.keyAt(indexOfValue));
            }
        }
    }

    private void m82a(int i) {
        if (this.f113e != null && i >= 0 && i < this.f113e.size()) {
            synchronized (this.f113e) {
                this.f113e.removeAt(i);
            }
            if (this.f113e.size() == 0) {
                this.f116h.initialize();
            }
        }
    }
}
