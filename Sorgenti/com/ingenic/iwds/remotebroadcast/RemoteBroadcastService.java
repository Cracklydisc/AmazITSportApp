package com.ingenic.iwds.remotebroadcast;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import com.ingenic.iwds.remotebroadcast.IRemoteBroadcastService.Stub;

public class RemoteBroadcastService extends Service {
    private C1148a f135a;
    private RemoteBroadcastProxy f136b;

    private class C1148a extends Stub {
        final /* synthetic */ RemoteBroadcastService f134a;

        private C1148a(RemoteBroadcastService remoteBroadcastService) {
            this.f134a = remoteBroadcastService;
        }

        public void registerRemoteReceiver(int callerId, int id, IntentFilter filter, String requestPermission) {
            this.f134a.f136b.m138a(callerId, id, filter, requestPermission);
        }

        public void unregisterRemoteReceiver(int callerId, int receiverId) {
            this.f134a.f136b.m137a(callerId, receiverId);
        }

        public void sendRemoteBroadcast(int callerId, Intent intent, String perm) {
            this.f134a.f136b.m139a(callerId, intent, perm);
        }

        public int registerRemoteBroadcastCallback(IRemoteBroadcastCallback callback) {
            return this.f134a.f136b.m135a(callback);
        }

        public void unregisterRemoteBroadcastCallback(int callerId) {
            this.f134a.f136b.m136a(callerId);
        }
    }

    public void onCreate() {
        super.onCreate();
        this.f136b = RemoteBroadcastProxy.getInstance(this);
        this.f135a = new C1148a();
    }

    public IBinder onBind(Intent intent) {
        return this.f135a;
    }
}
