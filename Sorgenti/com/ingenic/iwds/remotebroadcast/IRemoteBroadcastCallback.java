package com.ingenic.iwds.remotebroadcast;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

interface IRemoteBroadcastCallback extends IInterface {

    public static class RemoteBroadcastCallbackProxy implements IRemoteBroadcastCallback {
        private IBinder f98a;

        public RemoteBroadcastCallbackProxy(IBinder remote) {
            this.f98a = remote;
        }

        public IBinder asBinder() {
            return this.f98a;
        }

        public void performSendResult(Intent intent, String perm, int resultCode) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback");
            if (intent != null) {
                obtain.writeInt(1);
                intent.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            obtain.writeString(perm);
            obtain.writeInt(resultCode);
            try {
                this.f98a.transact(1, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }

        public void performReceive(int id, Intent intent) {
            Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback");
            obtain.writeInt(id);
            if (intent != null) {
                obtain.writeInt(1);
                intent.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            try {
                this.f98a.transact(2, obtain, null, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            obtain.recycle();
        }
    }

    public static abstract class Stub extends Binder implements IRemoteBroadcastCallback {
        public static IRemoteBroadcastCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IRemoteBroadcastCallback iRemoteBroadcastCallback = (IRemoteBroadcastCallback) obj.queryLocalInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback");
            return iRemoteBroadcastCallback == null ? new RemoteBroadcastCallbackProxy(obj) : iRemoteBroadcastCallback;
        }

        public IBinder asBinder() {
            return this;
        }

        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Intent intent = null;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback");
                    if (data.readInt() != 0) {
                        intent = (Intent) Intent.CREATOR.createFromParcel(data);
                    }
                    performSendResult(intent, data.readString(), data.readInt());
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.remotebroadcast.IRemoteBroadcastCallback");
                    int readInt = data.readInt();
                    if (data.readInt() != 0) {
                        intent = (Intent) Intent.CREATOR.createFromParcel(data);
                    }
                    performReceive(readInt, intent);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void performReceive(int i, Intent intent);

    void performSendResult(Intent intent, String str, int i);
}
