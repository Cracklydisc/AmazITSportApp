package com.ingenic.iwds.remotebroadcast;

import android.content.IntentFilter;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteBroadcastInfo */
class RemoteIntentFilterInfo extends C1150a implements Parcelable {
    public static final Creator<RemoteIntentFilterInfo> CREATOR = new C11491();
    private IntentFilter f139c;
    private String f140d;

    /* compiled from: RemoteBroadcastInfo */
    static class C11491 implements Creator<RemoteIntentFilterInfo> {
        C11491() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m142a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m143a(i);
        }

        public RemoteIntentFilterInfo m142a(Parcel parcel) {
            return new RemoteIntentFilterInfo(parcel);
        }

        public RemoteIntentFilterInfo[] m143a(int i) {
            return new RemoteIntentFilterInfo[i];
        }
    }

    public RemoteIntentFilterInfo(int callerId, int id, IntentFilter filter, String permission) {
        this.a = callerId;
        this.b = id;
        this.f139c = filter;
        this.f140d = permission;
    }

    protected RemoteIntentFilterInfo(Parcel in) {
        readFromParcel(in);
    }

    public IntentFilter getFilter() {
        return this.f139c;
    }

    public String getPermission() {
        return this.f140d;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        if (this.f139c != null) {
            dest.writeInt(1);
            this.f139c.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.f140d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        if (in.readInt() != 0) {
            this.f139c = (IntentFilter) IntentFilter.CREATOR.createFromParcel(in);
        }
        this.f140d = in.readString();
    }

    public String toString() {
        return "RemoteIntentFilterInfo[id:" + this.b + "-IntentFilter:" + this.f139c + "-permission:" + this.f140d + "]";
    }
}
