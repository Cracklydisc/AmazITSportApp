package com.ingenic.iwds.datatransactor;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import java.io.Serializable;

public class ProviderTransactionModel<T> implements DataTransactorCallback {
    private ProviderTransactionModelCallback<T> m_callback;

    public interface ProviderTransactionModelCallback<T> {
        void onChannelAvailable(boolean z);

        void onLinkConnected(DeviceDescriptor deviceDescriptor, boolean z);

        void onObjectArrived(T t);

        void onRequest();

        void onRequestFailed();

        void onSendResult(DataTransactResult dataTransactResult);
    }

    public static class Request implements Serializable {
        private static final long serialVersionUID = -8398132141094564822L;
    }

    public static class RequestFailed implements Serializable {
        private static final long serialVersionUID = -130038790971468020L;
    }

    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        this.m_callback.onLinkConnected(descriptor, isConnected);
    }

    public void onChannelAvailable(boolean isAvailable) {
        this.m_callback.onChannelAvailable(isAvailable);
    }

    public void onSendResult(DataTransactResult result) {
        this.m_callback.onSendResult(result);
    }

    public void onDataArrived(Object object) {
        if (object instanceof Request) {
            this.m_callback.onRequest();
        } else if (object instanceof RequestFailed) {
            this.m_callback.onRequestFailed();
        } else {
            this.m_callback.onObjectArrived(object);
        }
    }

    public void onSendFileProgress(int progress) {
    }

    public void onRecvFileProgress(int progress) {
    }
}
