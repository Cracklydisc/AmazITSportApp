package com.ingenic.iwds.datatransactor;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.app.ConnectionHelper;
import com.ingenic.iwds.common.exception.FileStatusException;
import com.ingenic.iwds.common.exception.FileTransferException;
import com.ingenic.iwds.common.exception.LinkDisconnectedException;
import com.ingenic.iwds.common.exception.LinkUnbondedException;
import com.ingenic.iwds.common.exception.PortClosedException;
import com.ingenic.iwds.common.exception.PortDisconnectedException;
import com.ingenic.iwds.common.exception.SerializeException;
import com.ingenic.iwds.os.SafeParcelable;
import com.ingenic.iwds.uniconnect.Connection;
import com.ingenic.iwds.uniconnect.ConnectionServiceManager;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.utils.serializable.TransferAdapter;
import com.ingenic.iwds.utils.serializable.TransferAdapter.TransferAdapterCallback;
import java.io.File;
import java.io.IOException;

public class DataTransactor extends TransactorParcelableCreator<Parcelable, SafeParcelable> {
    private CallbackHandler m_callbackHandler;
    private Controller m_controller;
    private Helper m_helper;
    private ObjectExchangeCallback m_obexCallback;
    private Reader m_reader;
    private Sender m_sender;
    private TransferAdapterObserver m_tranfserAdapterObserver;
    private String m_uuid;

    private class CallbackHandler extends Handler {
        private DataTransactorInterruptCallback i_callback = null;
        private DataTransactorCallback m_callback;

        CallbackHandler(DataTransactorCallback callback) {
            this.m_callback = callback;
        }

        public void handleMessage(Message msg) {
            boolean z = false;
            switch (msg.what) {
                case 1:
                    DataTransactorCallback dataTransactorCallback = this.m_callback;
                    DeviceDescriptor deviceDescriptor = (DeviceDescriptor) msg.obj;
                    if (msg.arg1 != 0) {
                        z = true;
                    }
                    dataTransactorCallback.onLinkConnected(deviceDescriptor, z);
                    return;
                case 2:
                    DataTransactorCallback dataTransactorCallback2 = this.m_callback;
                    if (msg.arg1 != 0) {
                        z = true;
                    }
                    dataTransactorCallback2.onChannelAvailable(z);
                    return;
                case 3:
                    this.m_callback.onSendResult((DataTransactResult) msg.obj);
                    return;
                case 4:
                    this.m_callback.onDataArrived(msg.obj);
                    return;
                case 5:
                    if (msg.arg1 == 1) {
                        this.m_callback.onSendFileProgress(msg.arg2);
                        return;
                    } else {
                        this.m_callback.onRecvFileProgress(msg.arg2);
                        return;
                    }
                case 6:
                    if (this.i_callback == null) {
                        return;
                    }
                    if (msg.arg1 == 1) {
                        this.i_callback.onSendFileInterrupted(msg.arg2);
                        return;
                    } else {
                        this.i_callback.onRecvFileInterrupted(msg.arg2);
                        return;
                    }
                default:
                    IwdsAssert.dieIf((Object) this, true, "Implement me.");
                    return;
            }
        }

        public void notifyLinkConnected(DeviceDescriptor descriptor, boolean connected) {
            int i = 1;
            Message obtain = Message.obtain(this);
            obtain.what = 1;
            if (!connected) {
                i = 0;
            }
            obtain.arg1 = i;
            obtain.obj = descriptor;
            obtain.sendToTarget();
        }

        public void notifyConnectionConnected(boolean connected) {
            Message obtain = Message.obtain(this);
            obtain.what = 2;
            obtain.arg1 = connected ? 1 : 0;
            obtain.sendToTarget();
        }

        public void notifySendResult(DataTransactResult result) {
            Message obtain = Message.obtain(this);
            obtain.what = 3;
            obtain.obj = result;
            obtain.sendToTarget();
        }

        public void notifyDataArrived(Object object) {
            Message obtain = Message.obtain(this);
            obtain.what = 4;
            obtain.obj = object;
            obtain.sendToTarget();
        }

        public void notifyFileTransactionProgress(int progress, boolean isSend) {
            Message obtain = Message.obtain(this);
            obtain.what = 5;
            obtain.arg1 = isSend ? 1 : 0;
            obtain.arg2 = progress;
            obtain.sendToTarget();
        }

        public void notifyFileTransactionInterrupted(int index, boolean isSend) {
            Message obtain = Message.obtain(this);
            obtain.what = 6;
            obtain.arg1 = isSend ? 1 : 0;
            obtain.arg2 = index;
            obtain.sendToTarget();
        }
    }

    private class Controller {
        private Connection m_connection;
        private DeviceDescriptor m_deviceDescriptor;
        private boolean m_readyToSend;
        private ConnectionServiceManager m_service;

        private Controller() {
            this.m_readyToSend = false;
        }

        public void start(ConnectionServiceManager service) {
            this.m_service = service;
            this.m_connection = this.m_service.createConnection();
        }

        public void stop() {
            this.m_connection = null;
            this.m_service = null;
        }

        public void send(Object object) {
            synchronized (this) {
                if (this.m_readyToSend) {
                    DataTransactor.this.m_sender.send(object);
                    return;
                }
                DataTransactor.this.m_callbackHandler.notifySendResult(new DataTransactResult(object, 1));
            }
        }

        public void send(File file, int index) {
            synchronized (this) {
                if (this.m_readyToSend) {
                    DataTransactor.this.m_sender.send(file, index);
                    return;
                }
                DataTransactor.this.m_callbackHandler.notifySendResult(new DataTransactResult(file, 1));
            }
        }

        private void handleDeviceConnected(DeviceDescriptor descriptor) {
            if (this.m_deviceDescriptor == null) {
                DeviceDescriptor filerDeviceDescriptor = DataTransactor.this.filerDeviceDescriptor(descriptor);
                if (filerDeviceDescriptor != null) {
                    this.m_deviceDescriptor = filerDeviceDescriptor;
                    DataTransactor.this.startReader(this.m_deviceDescriptor, this.m_connection);
                }
            }
        }

        private void handleDeviceDisconnected(DeviceDescriptor descriptor) {
            if (descriptor.equals(this.m_deviceDescriptor)) {
                DataTransactor.this.stopReader();
                this.m_deviceDescriptor = null;
            }
        }

        public void setReadyToSend(boolean isReady) {
            synchronized (this) {
                if (isReady) {
                    DataTransactor.this.startSender(this.m_connection);
                    this.m_readyToSend = true;
                } else {
                    this.m_readyToSend = false;
                    DataTransactor.this.stopSender();
                }
            }
        }
    }

    public class DataTransactResult {
        private int m_resultCode;
        private Object m_transferedObject;

        DataTransactResult(Object transferedObject, int resultCode) {
            this.m_transferedObject = transferedObject;
            this.m_resultCode = resultCode;
        }

        public int getResultCode() {
            return this.m_resultCode;
        }

        public Object getTransferedObject() {
            return this.m_transferedObject;
        }
    }

    public interface DataTransactorCallback {
        void onChannelAvailable(boolean z);

        void onDataArrived(Object obj);

        void onLinkConnected(DeviceDescriptor deviceDescriptor, boolean z);

        void onRecvFileProgress(int i);

        void onSendFileProgress(int i);

        void onSendResult(DataTransactResult dataTransactResult);
    }

    public interface DataTransactorInterruptCallback {
        void onRecvFileInterrupted(int i);

        void onSendFileInterrupted(int i);
    }

    private class Helper extends ConnectionHelper {
        public Helper(Context context) {
            super(context);
        }

        public void onServiceConnected(ConnectionServiceManager connectionServiceManager) {
            DataTransactor.this.m_controller.start(connectionServiceManager);
        }

        public void onServiceDisconnected(boolean unexpected) {
            DataTransactor.this.m_controller.stop();
        }

        public void onConnectedDevice(DeviceDescriptor deviceDescriptor) {
            DataTransactor.this.m_controller.handleDeviceConnected(deviceDescriptor);
        }

        public void onDisconnectedDevice(DeviceDescriptor deviceDescriptor) {
            DataTransactor.this.m_controller.handleDeviceDisconnected(deviceDescriptor);
        }
    }

    public interface ObjectExchangeCallback {
        Object recv(Connection connection) throws SerializeException, IOException;

        void send(Connection connection, Object obj) throws SerializeException, IOException;
    }

    private class Reader {
        private Connection m_connection;
        private DeviceDescriptor m_deviceDescriptor;
        private ReadThread m_thread;

        private class ReadThread extends Thread {
            private boolean m_isRunning = false;
            private boolean m_requestStop = false;
            private Object m_runLock = new Object();

            public ReadThread() {
                super("DataTransactor_reader: UUID: " + DataTransactor.this.m_uuid);
            }

            public void start() {
                super.start();
            }

            public void stopThread() {
                synchronized (this.m_runLock) {
                    this.m_requestStop = true;
                    if (this.m_isRunning) {
                        closeConnection();
                        return;
                    }
                }
            }

            private void setRunning(boolean isRunning) {
                synchronized (this.m_runLock) {
                    this.m_isRunning = isRunning;
                }
            }

            private boolean openConnection() {
                synchronized (this) {
                    int open = Reader.this.m_connection.open(Reader.this.m_deviceDescriptor, DataTransactor.this.m_uuid);
                    IwdsAssert.dieIf((Object) this, open == -3, "Uuid was conflict.");
                    if (open < 0) {
                        return false;
                    }
                    return true;
                }
            }

            private void closeConnection() {
                synchronized (this) {
                    Reader.this.m_connection.close();
                }
            }

            public void run() {
                setRunning(true);
                DataTransactor.this.m_callbackHandler.notifyLinkConnected(Reader.this.m_deviceDescriptor, true);
                Throwable th = null;
                do {
                    synchronized (this.m_runLock) {
                        if (this.m_requestStop) {
                            break;
                        } else if (!openConnection()) {
                            break;
                        } else {
                            setName("DataTransactor_reader: UUID: " + DataTransactor.this.m_uuid + ", Port: " + Reader.this.m_connection.getPort());
                            if (Reader.this.m_connection.handshake() != 0) {
                                closeConnection();
                                break;
                            }
                            DataTransactor.this.m_controller.setReadyToSend(true);
                            DataTransactor.this.m_callbackHandler.notifyConnectionConnected(true);
                            while (true) {
                                try {
                                    Object recv;
                                    if (DataTransactor.this.m_obexCallback != null) {
                                        recv = DataTransactor.this.m_obexCallback.recv(Reader.this.m_connection);
                                    } else {
                                        recv = TransferAdapter.recv(Reader.this.m_connection, DataTransactor.this.m_parcelableCreator, DataTransactor.this.m_safeParcelableCreator, DataTransactor.this.m_tranfserAdapterObserver);
                                    }
                                    DataTransactor.this.m_callbackHandler.notifyDataArrived(recv);
                                } catch (SerializeException e) {
                                    IwdsAssert.dieIf((Object) this, true, "Serialize exception: " + e);
                                } catch (FileTransferException e2) {
                                    DataTransactor.this.m_callbackHandler.notifyDataArrived(e2);
                                } catch (IOException e3) {
                                    th = e3.getCause();
                                }
                            }
                        }
                    }
                } while (th instanceof RemoteException);
                DataTransactor.this.m_callbackHandler.notifyLinkConnected(Reader.this.m_deviceDescriptor, false);
                setRunning(false);
                DataTransactor.this.m_callbackHandler.notifyConnectionConnected(false);
                DataTransactor.this.m_controller.setReadyToSend(false);
                closeConnection();
                if ((th instanceof LinkDisconnectedException) || (th instanceof LinkUnbondedException) || (th instanceof PortClosedException)) {
                    break;
                } else if (th instanceof RemoteException) {
                }
                DataTransactor.this.m_callbackHandler.notifyLinkConnected(Reader.this.m_deviceDescriptor, false);
                setRunning(false);
            }
        }

        private Reader() {
        }

        public void start(DeviceDescriptor descriptor, Connection connection) {
            this.m_deviceDescriptor = descriptor;
            this.m_connection = connection;
            this.m_thread = new ReadThread();
            this.m_thread.start();
        }

        public void stop() {
            this.m_thread.stopThread();
        }
    }

    private class Sender {
        private Connection m_connection;
        private SendHandler m_handler;
        private HandlerThread m_thread;

        public class SendHandler extends Handler {
            public SendHandler(Looper looper) {
                super(looper);
            }

            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        DataTransactResult dataTransactResult = null;
                        Object obj = msg.obj;
                        try {
                            if (DataTransactor.this.m_obexCallback != null) {
                                DataTransactor.this.m_obexCallback.send(Sender.this.m_connection, obj);
                            } else if (obj instanceof File) {
                                TransferAdapter.send(Sender.this.m_connection, (File) obj, msg.arg1, DataTransactor.this.m_tranfserAdapterObserver);
                            } else {
                                TransferAdapter.send(Sender.this.m_connection, obj, DataTransactor.this.m_parcelableCreator, DataTransactor.this.m_safeParcelableCreator);
                            }
                            dataTransactResult = new DataTransactResult(obj, 0);
                        } catch (SerializeException e) {
                            IwdsAssert.dieIf((Object) this, true, "Serialize exception: " + e);
                        } catch (IOException e2) {
                            DataTransactResult dataTransactResult2;
                            Throwable cause = e2.getCause();
                            if ((cause instanceof LinkDisconnectedException) || (cause instanceof LinkUnbondedException)) {
                                dataTransactResult2 = new DataTransactResult(obj, 2);
                            } else if ((cause instanceof PortDisconnectedException) || (cause instanceof PortClosedException)) {
                                dataTransactResult2 = new DataTransactResult(obj, 1);
                            } else if (cause instanceof RemoteException) {
                                dataTransactResult2 = new DataTransactResult(obj, 3);
                            } else {
                                dataTransactResult2 = null;
                            }
                            dataTransactResult = dataTransactResult2;
                        } catch (FileTransferException e3) {
                            if (e3.getCause() instanceof FileStatusException) {
                                dataTransactResult = new DataTransactResult(obj, 4);
                            }
                        }
                        DataTransactor.this.m_callbackHandler.notifySendResult(dataTransactResult);
                        return;
                    case 1:
                        Sender.this.m_thread.quit();
                        return;
                    default:
                        return;
                }
            }
        }

        private Sender() {
        }

        public void start(Connection connection) {
            if (this.m_thread == null) {
                this.m_connection = connection;
                this.m_thread = new HandlerThread("DataTransactor_sender: UUID: " + DataTransactor.this.m_uuid + ", Port: " + this.m_connection.getPort());
                this.m_thread.start();
                this.m_handler = new SendHandler(this.m_thread.getLooper());
            }
        }

        public void stop() {
            if (this.m_thread != null) {
                Message.obtain(this.m_handler, 1).sendToTarget();
                try {
                    this.m_thread.join();
                } catch (InterruptedException e) {
                }
                this.m_handler = null;
                this.m_thread = null;
            }
        }

        public void send(Object object) {
            if (this.m_thread != null) {
                Message obtain = Message.obtain(this.m_handler);
                obtain.what = 0;
                obtain.obj = object;
                obtain.sendToTarget();
            }
        }

        public void send(File file, int index) {
            if (this.m_thread != null) {
                Message obtain = Message.obtain(this.m_handler);
                obtain.what = 0;
                obtain.arg1 = index;
                obtain.obj = file;
                obtain.sendToTarget();
            }
        }
    }

    private class TransferAdapterObserver implements TransferAdapterCallback {
        private TransferAdapterObserver() {
        }

        public void onSendFileProgress(long currentBytes, long totalBytes) {
            int i = (int) ((100 * currentBytes) / totalBytes);
            IwdsLog.m267i(this, "File send progress: " + i);
            DataTransactor.this.m_callbackHandler.notifyFileTransactionProgress(i, true);
        }

        public void onRecvFileProgress(long currentBytes, long totalBytes) {
            int i = (int) ((100 * currentBytes) / totalBytes);
            IwdsLog.m267i(this, "File recv progress: " + i);
            DataTransactor.this.m_callbackHandler.notifyFileTransactionProgress(i, false);
        }

        public void onSendFileInterrupted(int index) {
            IwdsLog.m267i(this, "File send interrupted index: " + index);
            DataTransactor.this.m_callbackHandler.notifyFileTransactionInterrupted(index, true);
        }

        public void onRecvFileInterrupted(int index) {
            IwdsLog.m267i(this, "File recv interrupted index: " + index);
            DataTransactor.this.m_callbackHandler.notifyFileTransactionInterrupted(index, false);
        }

        public void onFileTransferError(FileTransferException e) {
            IwdsLog.m267i(this, "File transfer error!");
            DataTransactor.this.m_callbackHandler.notifyDataArrived(e);
        }
    }

    public DataTransactor(Context context, DataTransactorCallback callback, String uuid) {
        boolean z = false;
        IwdsAssert.dieIf((Object) this, callback == null, "Callback is null.");
        if (uuid == null || uuid.isEmpty()) {
            z = true;
        }
        IwdsAssert.dieIf((Object) this, z, "Uuid is null or empty.");
        this.m_uuid = uuid;
        this.m_helper = new Helper(context);
        this.m_callbackHandler = new CallbackHandler(callback);
        this.m_reader = new Reader();
        this.m_sender = new Sender();
        this.m_controller = new Controller();
        this.m_tranfserAdapterObserver = new TransferAdapterObserver();
    }

    public void start() {
        this.m_helper.start();
    }

    public void stop() {
        this.m_helper.stop();
    }

    public boolean isStarted() {
        return this.m_helper.isStarted();
    }

    public void send(Object object) {
        IwdsAssert.dieIf((Object) this, object == null, "Object is null.");
        if (object instanceof File) {
            send((File) object);
        } else {
            this.m_controller.send(object);
        }
    }

    public void send(File file, int index) {
        IwdsAssert.dieIf((Object) this, file == null, "file is null.");
        this.m_controller.send(file, index);
    }

    public void send(File file) {
        boolean z;
        if (file == null) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "file is null.");
        send(file, 0);
    }

    protected DeviceDescriptor filerDeviceDescriptor(DeviceDescriptor descriptor) {
        return descriptor;
    }

    private void startReader(DeviceDescriptor descriptor, Connection connection) {
        this.m_reader.start(descriptor, connection);
    }

    private void stopReader() {
        this.m_reader.stop();
    }

    private void startSender(Connection connection) {
        this.m_sender.start(connection);
    }

    private void stopSender() {
        this.m_sender.stop();
    }

    protected void finalize() throws Throwable {
        super.finalize();
        stop();
    }
}
