package com.ingenic.iwds.datatransactor;

import com.ingenic.iwds.utils.IwdsAssert;

public class FileTransferErrorCode {
    public static String errorString(int errorCode) {
        switch (errorCode) {
            case 0:
                return "no error";
            case 1:
                return "file status error";
            case 2:
                return "no sdcard";
            case 3:
                return "sdcard full";
            default:
                IwdsAssert.dieIf("FileTransferErrorCode", true, "Unknown error code: " + errorCode);
                return null;
        }
    }
}
