package com.ingenic.iwds.datatransactor;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.exception.FileStatusException;
import com.ingenic.iwds.common.exception.FileTransferException;
import com.ingenic.iwds.common.exception.SDCardFullException;
import com.ingenic.iwds.common.exception.SDCardNotMountedException;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorInterruptCallback;
import com.ingenic.iwds.utils.IwdsAssert;
import java.io.File;
import java.io.Serializable;

public class FileTransactionModel implements DataTransactorCallback, DataTransactorInterruptCallback {
    private FileTransactionInterruptCallback i_callback;
    private FileTransactionModelCallback m_callback;
    private File m_file;
    private FileInfo m_fileInfo;
    protected DataTransactor m_transactor;

    private static class CancelForReceiveFile implements Serializable {
        private static final long serialVersionUID = 3611791257589864674L;

        private CancelForReceiveFile() {
        }
    }

    private static class ConfirmForReceiveFile implements Serializable {
        private static final long serialVersionUID = -7030085924484744118L;

        private ConfirmForReceiveFile() {
        }
    }

    public interface FileTransactionInterruptCallback {
        void onFileTransferError(int i);

        void onRecvFileInterrupted(int i);

        void onSendFileInterrupted(int i);
    }

    public interface FileTransactionModelCallback {
        void onCancelForReceiveFile();

        void onChannelAvailable(boolean z);

        void onConfirmForReceiveFile();

        void onFileArrived(File file);

        void onLinkConnected(DeviceDescriptor deviceDescriptor, boolean z);

        void onRecvFileProgress(int i);

        void onRequestSendFile(FileInfo fileInfo);

        void onSendFileProgress(int i);

        void onSendResult(DataTransactResult dataTransactResult);
    }

    private static class RequestSendFile implements Serializable {
        private static final long serialVersionUID = 5098980108404157152L;
        private FileInfo info;
    }

    public void stop() {
        this.m_transactor.stop();
    }

    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        this.m_callback.onLinkConnected(descriptor, isConnected);
    }

    public void onChannelAvailable(boolean isAvailable) {
        this.m_callback.onChannelAvailable(isAvailable);
    }

    public void onSendResult(DataTransactResult result) {
        this.m_callback.onSendResult(result);
    }

    public void onDataArrived(Object object) {
        if (object instanceof RequestSendFile) {
            this.m_callback.onRequestSendFile(((RequestSendFile) object).info);
        } else if (object instanceof ConfirmForReceiveFile) {
            this.m_transactor.send(this.m_file, this.m_fileInfo.chunkIndex);
            this.m_callback.onConfirmForReceiveFile();
        } else if (object instanceof CancelForReceiveFile) {
            this.m_callback.onCancelForReceiveFile();
        } else if (object instanceof File) {
            this.m_callback.onFileArrived((File) object);
        } else if (object instanceof FileTransferException) {
            Throwable cause = ((FileTransferException) object).getCause();
            if (this.i_callback == null) {
                return;
            }
            if (cause instanceof FileStatusException) {
                this.i_callback.onFileTransferError(1);
                stop();
            } else if (cause instanceof SDCardNotMountedException) {
                this.i_callback.onFileTransferError(2);
            } else if (cause instanceof SDCardFullException) {
                this.i_callback.onFileTransferError(3);
            } else {
                IwdsAssert.dieIf((Object) this, true, "Implement me.");
            }
        }
    }

    public void onSendFileProgress(int progress) {
        this.m_callback.onSendFileProgress(progress);
    }

    public void onRecvFileProgress(int progress) {
        this.m_callback.onRecvFileProgress(progress);
    }

    public void onSendFileInterrupted(int index) {
        if (this.i_callback != null) {
            this.i_callback.onSendFileInterrupted(index);
        }
    }

    public void onRecvFileInterrupted(int index) {
        if (this.i_callback != null) {
            this.i_callback.onRecvFileInterrupted(index);
        }
    }
}
