package com.ingenic.iwds.datatransactor;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.os.SafeParcelable;

public abstract class TransactorParcelableCreator<T1 extends Parcelable, T2 extends SafeParcelable> {
    protected Creator<T1> m_parcelableCreator;
    protected SafeParcelable.Creator<T2> m_safeParcelableCreator;
}
