package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class MusicControlTransactionModel extends ProviderTransactionModel<MusicControlInfo> {

    public interface MusicControlTransactionModelCallback extends ProviderTransactionModelCallback<MusicControlInfo> {
    }
}
