package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class NotificationTransactionModel extends ProviderTransactionModel<NotificationInfo> {

    public interface NotificationTransactionModelCallback extends ProviderTransactionModelCallback<NotificationInfo> {
    }
}
