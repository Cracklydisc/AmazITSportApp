package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class SyncTimeInfo implements Parcelable {
    public static final Creator<SyncTimeInfo> CREATOR = new C11171();
    public long currenttime = System.currentTimeMillis();
    public String timezoneid = TimeZone.getDefault().getID();

    static class C11171 implements Creator<SyncTimeInfo> {
        C11171() {
        }

        public SyncTimeInfo createFromParcel(Parcel source) {
            SyncTimeInfo syncTimeInfo = new SyncTimeInfo();
            syncTimeInfo.currenttime = source.readLong();
            syncTimeInfo.timezoneid = source.readString();
            return syncTimeInfo;
        }

        public SyncTimeInfo[] newArray(int size) {
            return new SyncTimeInfo[size];
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.currenttime);
        dest.writeString(this.timezoneid);
    }

    private String TimetoString(long currtime) {
        if (currtime < 0) {
            return null;
        }
        return new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss ").format(new Date(currtime));
    }

    public String toString() {
        return "currenttime = " + TimetoString(this.currenttime) + ", timezoneid =" + this.timezoneid + "]";
    }

    public int describeContents() {
        return 0;
    }
}
