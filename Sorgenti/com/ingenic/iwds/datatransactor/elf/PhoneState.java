package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PhoneState implements Parcelable {
    public static final Creator<PhoneState> CREATOR = new C11121();
    public String name;
    public String number;
    public int state;

    static class C11121 implements Creator<PhoneState> {
        C11121() {
        }

        public PhoneState createFromParcel(Parcel source) {
            PhoneState phoneState = new PhoneState();
            phoneState.state = source.readInt();
            phoneState.number = source.readString();
            phoneState.name = source.readString();
            return phoneState;
        }

        public PhoneState[] newArray(int size) {
            return new PhoneState[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.state);
        dest.writeString(this.number);
        dest.writeString(this.name);
    }

    public String toString() {
        return "PhoneStete:state:" + this.state + ",number:" + this.number + ",name:" + this.name;
    }
}
