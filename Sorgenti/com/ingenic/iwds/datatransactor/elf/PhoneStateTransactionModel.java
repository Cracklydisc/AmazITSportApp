package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class PhoneStateTransactionModel extends ProviderTransactionModel<PhoneState> {

    public interface PhoneStateCallback extends ProviderTransactionModelCallback<PhoneState> {
    }
}
