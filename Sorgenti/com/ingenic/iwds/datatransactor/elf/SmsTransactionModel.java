package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class SmsTransactionModel extends ProviderTransactionModel<SmsInfo> {

    public interface SmsTransactionCallback extends ProviderTransactionModelCallback<SmsInfo> {
    }
}
