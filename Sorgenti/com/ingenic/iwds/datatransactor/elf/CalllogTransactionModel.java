package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class CalllogTransactionModel extends ProviderTransactionModel<CalllogInfo> {

    public interface CalllogTransactionCallback extends ProviderTransactionModelCallback<CalllogInfo> {
    }
}
