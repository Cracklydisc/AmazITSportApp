package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Arrays;

public class CameraFrameInfo implements Parcelable {
    public static final Creator<CameraFrameInfo> CREATOR = new C11031();
    public byte[] frameData;

    static class C11031 implements Creator<CameraFrameInfo> {
        C11031() {
        }

        public CameraFrameInfo createFromParcel(Parcel source) {
            CameraFrameInfo cameraFrameInfo = new CameraFrameInfo();
            cameraFrameInfo.frameData = new byte[source.readInt()];
            source.readByteArray(cameraFrameInfo.frameData);
            return cameraFrameInfo;
        }

        public CameraFrameInfo[] newArray(int size) {
            return new CameraFrameInfo[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.frameData.length);
        dest.writeByteArray(this.frameData);
    }

    public String toString() {
        return "CameraFrameInfo [frameData=" + Arrays.toString(this.frameData) + "]";
    }
}
