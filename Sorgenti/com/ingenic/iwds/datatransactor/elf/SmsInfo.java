package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SmsInfo implements Parcelable {
    public static final Creator<SmsInfo> CREATOR = new C11161();
    private String address;
    private String body;
    private long date;
    private int id;
    private String person;
    private int protocol;
    private int read;
    private int thread_id;
    private int type;

    static class C11161 implements Creator<SmsInfo> {
        C11161() {
        }

        public SmsInfo createFromParcel(Parcel source) {
            SmsInfo smsInfo = new SmsInfo();
            smsInfo.id = source.readInt();
            smsInfo.thread_id = source.readInt();
            smsInfo.address = source.readString();
            smsInfo.person = source.readString();
            smsInfo.body = source.readString();
            smsInfo.date = source.readLong();
            smsInfo.read = source.readInt();
            smsInfo.protocol = source.readInt();
            smsInfo.type = source.readInt();
            return smsInfo;
        }

        public SmsInfo[] newArray(int size) {
            return new SmsInfo[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.thread_id);
        dest.writeString(this.address);
        dest.writeString(this.person);
        dest.writeString(this.body);
        dest.writeLong(this.date);
        dest.writeInt(this.read);
        dest.writeInt(this.protocol);
        dest.writeInt(this.type);
    }

    public String toString() {
        return "Sms:id:" + this.id + ",thread_id:" + this.thread_id + ",address:" + this.address + ",person:" + this.person + ",body:" + this.body + ",date:" + this.date + ",read:" + this.read + ",protocol:" + this.protocol + ",type:" + this.type;
    }
}
