package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class HealthTransactionModel extends ProviderTransactionModel<HealthInfo> {

    public interface HealthTransactionModelCallback extends ProviderTransactionModelCallback<HealthInfo> {
    }
}
