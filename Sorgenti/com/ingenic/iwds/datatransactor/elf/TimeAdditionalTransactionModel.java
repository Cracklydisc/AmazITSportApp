package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class TimeAdditionalTransactionModel extends ProviderTransactionModel<TimeAdditionalInfo> {

    public interface TimeAdditionalInfoTransactionModelCallback extends ProviderTransactionModelCallback<TimeAdditionalInfo> {
    }
}
