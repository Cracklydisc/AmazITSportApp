package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactorCallback;
import java.io.Serializable;

public class CameraTransactionModel implements DataTransactorCallback {
    private CameraTransactionModelCallback m_callback;

    public interface CameraTransactionModelCallback {
        void onChannelAvailable(boolean z);

        void onLinkConnected(DeviceDescriptor deviceDescriptor, boolean z);

        void onObjectArrived(CameraFrameInfo cameraFrameInfo);

        void onRequestStartPreview(CameraPreviewSizeInfo cameraPreviewSizeInfo);

        void onRequestStartPreviewFailed();

        void onRequestStopPreview();

        void onRequestStopPreviewFailed();

        void onRequestTakePicture();

        void onRequestTakePictureFailed();

        void onSendResult(DataTransactResult dataTransactResult);

        void onTakePictureDone(CameraFrameInfo cameraFrameInfo);
    }

    public static class RequestStartPreview implements Serializable {
        private static final long serialVersionUID = -5609409498789811806L;
        public CameraPreviewSizeInfo sizeInfo;
    }

    public static class RequestStartPreviewFailed implements Serializable {
        private static final long serialVersionUID = 8110140648716686091L;
    }

    public static class RequestStopPreview implements Serializable {
        private static final long serialVersionUID = 2465019248251532073L;
    }

    public static class RequestStopPreviewFailed implements Serializable {
        private static final long serialVersionUID = -4042505134553328080L;
    }

    public static class RequestTakePicture implements Serializable {
        private static final long serialVersionUID = 4843107640702082657L;
    }

    public static class RequestTakePictureFailed implements Serializable {
        private static final long serialVersionUID = -1074205716986715196L;
    }

    public static class TakePictureDone implements Parcelable {
        public static final Creator<TakePictureDone> CREATOR = new C11041();
        public CameraFrameInfo frame;

        static class C11041 implements Creator<TakePictureDone> {
            C11041() {
            }

            public TakePictureDone createFromParcel(Parcel source) {
                TakePictureDone takePictureDone = new TakePictureDone();
                takePictureDone.frame = (CameraFrameInfo) CameraFrameInfo.CREATOR.createFromParcel(source);
                return takePictureDone;
            }

            public TakePictureDone[] newArray(int size) {
                return new TakePictureDone[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            this.frame.writeToParcel(dest, flags);
        }
    }

    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        this.m_callback.onLinkConnected(descriptor, isConnected);
    }

    public void onChannelAvailable(boolean isAvailable) {
        this.m_callback.onChannelAvailable(isAvailable);
    }

    public void onSendResult(DataTransactResult result) {
        this.m_callback.onSendResult(result);
    }

    public void onDataArrived(Object object) {
        if (object instanceof RequestStartPreview) {
            this.m_callback.onRequestStartPreview(((RequestStartPreview) object).sizeInfo);
        } else if (object instanceof RequestStopPreview) {
            this.m_callback.onRequestStopPreview();
        } else if (object instanceof RequestTakePicture) {
            this.m_callback.onRequestTakePicture();
        } else if (object instanceof RequestTakePictureFailed) {
            this.m_callback.onRequestTakePictureFailed();
        } else if (object instanceof RequestStartPreviewFailed) {
            this.m_callback.onRequestStartPreviewFailed();
        } else if (object instanceof RequestStopPreviewFailed) {
            this.m_callback.onRequestStopPreviewFailed();
        } else if (object instanceof TakePictureDone) {
            this.m_callback.onTakePictureDone(((TakePictureDone) object).frame);
        } else {
            this.m_callback.onObjectArrived((CameraFrameInfo) object);
        }
    }

    public void onSendFileProgress(int progress) {
    }

    public void onRecvFileProgress(int progress) {
    }
}
