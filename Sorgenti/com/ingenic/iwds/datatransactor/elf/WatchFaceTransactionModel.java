package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class WatchFaceTransactionModel extends ProviderTransactionModel<WatchFaceInfo> {

    public interface WatchFaceTransactionModelCallback extends ProviderTransactionModelCallback<WatchFaceInfo> {
    }
}
