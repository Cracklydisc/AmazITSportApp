package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class WeatherTransactionModel extends ProviderTransactionModel<WeatherInfoArray> {

    public interface WeatherInfoTransactionModelCallback extends ProviderTransactionModelCallback<WeatherInfoArray> {
    }
}
