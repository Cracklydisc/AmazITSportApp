package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class NotificationInfo implements Parcelable {
    public static final Creator<NotificationInfo> CREATOR = new C11111();
    public String appName;
    public String content;
    public long id;
    public String packageName;
    public int read;
    public String title;
    public long updateTime;

    static class C11111 implements Creator<NotificationInfo> {
        C11111() {
        }

        public NotificationInfo createFromParcel(Parcel source) {
            NotificationInfo notificationInfo = new NotificationInfo();
            notificationInfo.id = -1;
            notificationInfo.appName = source.readString();
            notificationInfo.packageName = source.readString();
            notificationInfo.title = source.readString();
            notificationInfo.content = source.readString();
            notificationInfo.updateTime = source.readLong();
            notificationInfo.read = -1;
            return notificationInfo;
        }

        public NotificationInfo[] newArray(int size) {
            return new NotificationInfo[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.appName);
        dest.writeString(this.packageName);
        dest.writeString(this.title);
        dest.writeString(this.content);
        dest.writeLong(this.updateTime);
    }

    public String toString() {
        return "NotificationInfo [id=" + this.id + ", appName=" + this.appName + ", packageName=" + this.packageName + ", title=" + this.title + ", content=" + this.content + ", updateTime=" + this.updateTime + ", read=" + this.read + "]";
    }
}
