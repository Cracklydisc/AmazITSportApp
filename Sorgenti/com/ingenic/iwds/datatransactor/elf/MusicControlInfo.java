package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class MusicControlInfo implements Parcelable {
    public static final Creator<MusicControlInfo> CREATOR = new C11101();
    public int cmd = -99;
    public String musicAlbum = null;
    public String musicArtist = null;
    public String songName = null;
    public int volumeCurrent = 5;
    public int volumeMax = 15;

    static class C11101 implements Creator<MusicControlInfo> {
        C11101() {
        }

        public MusicControlInfo createFromParcel(Parcel source) {
            MusicControlInfo musicControlInfo = new MusicControlInfo();
            musicControlInfo.cmd = source.readInt();
            musicControlInfo.musicAlbum = source.readString();
            musicControlInfo.musicArtist = source.readString();
            musicControlInfo.songName = source.readString();
            musicControlInfo.volumeMax = source.readInt();
            musicControlInfo.volumeCurrent = source.readInt();
            return musicControlInfo;
        }

        public MusicControlInfo[] newArray(int size) {
            return new MusicControlInfo[size];
        }
    }

    public enum COMMAND {
        START,
        PAUSE,
        NEXT,
        PREV
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.cmd);
        dest.writeString(this.musicAlbum);
        dest.writeString(this.musicArtist);
        dest.writeString(this.songName);
        dest.writeInt(this.volumeMax);
        dest.writeInt(this.volumeCurrent);
    }

    public String toString() {
        return "MusicControlInfo [cmd=" + this.cmd + ", musicAlbum=" + this.musicAlbum + ", musicArtist=" + this.musicArtist + ", songName=" + this.songName + "]";
    }
}
