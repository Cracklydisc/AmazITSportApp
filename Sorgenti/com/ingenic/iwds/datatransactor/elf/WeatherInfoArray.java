package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;

public class WeatherInfoArray implements Parcelable {
    public static final Creator<WeatherInfoArray> CREATOR = new C11271();
    public WeatherInfo[] data;

    static class C11271 implements Creator<WeatherInfoArray> {
        C11271() {
        }

        public WeatherInfoArray createFromParcel(Parcel source) {
            return new WeatherInfoArray((WeatherInfo[]) source.createTypedArray(WeatherInfo.CREATOR));
        }

        public WeatherInfoArray[] newArray(int size) {
            return new WeatherInfoArray[size];
        }
    }

    public static class WeatherInfo implements Parcelable {
        public static final Creator<WeatherInfo> CREATOR = new C11281();
        public String city;
        public int currentTemp = -1;
        public String date;
        public int dayIndex = -1;
        public String dayOfWeek;
        public int maximumTemp = -1;
        public int minimumTemp = -1;
        public String tempUnit;
        public String updateTime;
        public String weather;
        public String weatherCode;

        static class C11281 implements Creator<WeatherInfo> {
            C11281() {
            }

            public WeatherInfo createFromParcel(Parcel source) {
                WeatherInfo weatherInfo = new WeatherInfo();
                weatherInfo.city = source.readString();
                weatherInfo.weather = source.readString();
                weatherInfo.weatherCode = source.readString();
                weatherInfo.date = source.readString();
                weatherInfo.dayOfWeek = source.readString();
                weatherInfo.updateTime = source.readString();
                weatherInfo.tempUnit = source.readString();
                weatherInfo.currentTemp = source.readInt();
                weatherInfo.minimumTemp = source.readInt();
                weatherInfo.maximumTemp = source.readInt();
                weatherInfo.dayIndex = source.readInt();
                return weatherInfo;
            }

            public WeatherInfo[] newArray(int size) {
                return new WeatherInfo[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.city);
            dest.writeString(this.weather);
            dest.writeString(this.weatherCode);
            dest.writeString(this.date);
            dest.writeString(this.dayOfWeek);
            dest.writeString(this.updateTime);
            dest.writeString(this.tempUnit);
            dest.writeInt(this.currentTemp);
            dest.writeInt(this.minimumTemp);
            dest.writeInt(this.maximumTemp);
            dest.writeInt(this.dayIndex);
        }

        public String toString() {
            return "WeatherInfo [city=" + this.city + ", weather=" + this.weather + ", weatherCode=" + this.weatherCode + ", date=" + this.date + ", dayOfWeek=" + this.dayOfWeek + ", updateTime=" + this.updateTime + ", tempUnit=" + this.tempUnit + ", currentTemp=" + this.currentTemp + ", minimumTemp=" + this.minimumTemp + ", maximumTemp=" + this.maximumTemp + ", dayIndex=" + this.dayIndex + "]";
        }
    }

    public WeatherInfoArray(WeatherInfo[] weatherArray) {
        boolean z = weatherArray == null || weatherArray.length <= 0;
        IwdsAssert.dieIf((Object) this, z, "Weather array is null or length <= 0.");
        this.data = weatherArray;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(this.data, flags);
    }

    public String toString() {
        String str = "WeatherInfoArray []:\n";
        int length = this.data.length;
        for (int i = 0; i < length; i++) {
            str = str + "data[" + i + "]:" + this.data[i].toString() + "\n";
        }
        return str;
    }
}
