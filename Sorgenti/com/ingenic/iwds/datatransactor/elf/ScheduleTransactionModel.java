package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;

public class ScheduleTransactionModel extends ProviderTransactionModel<ScheduleInfo> {

    public interface ScheduleTransactionModelCallback extends ProviderTransactionModelCallback<ScheduleInfo> {
    }
}
