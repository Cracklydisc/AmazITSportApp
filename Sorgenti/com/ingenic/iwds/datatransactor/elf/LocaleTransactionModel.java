package com.ingenic.iwds.datatransactor.elf;

import com.ingenic.iwds.datatransactor.ProviderTransactionModel;
import com.ingenic.iwds.datatransactor.ProviderTransactionModel.ProviderTransactionModelCallback;
import java.util.Locale;

public class LocaleTransactionModel extends ProviderTransactionModel<Locale> {

    public interface LocaleTransactionModelCallback extends ProviderTransactionModelCallback<Locale> {
    }
}
