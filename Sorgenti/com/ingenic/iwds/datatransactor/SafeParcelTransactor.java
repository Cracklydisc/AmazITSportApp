package com.ingenic.iwds.datatransactor;

import com.ingenic.iwds.os.SafeParcelable;

public class SafeParcelTransactor<T extends SafeParcelable> extends DataTransactor {
    public void start() {
        super.start();
    }

    public void stop() {
        super.stop();
    }

    public boolean isStarted() {
        return super.isStarted();
    }

    public void send(Object object) {
        super.send(object);
    }
}
