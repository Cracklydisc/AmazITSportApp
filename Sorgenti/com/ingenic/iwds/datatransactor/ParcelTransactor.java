package com.ingenic.iwds.datatransactor;

import android.os.Parcelable;

public class ParcelTransactor<T extends Parcelable> extends DataTransactor {
    public void start() {
        super.start();
    }

    public void stop() {
        super.stop();
    }

    public boolean isStarted() {
        return super.isStarted();
    }

    public void send(Object object) {
        super.send(object);
    }
}
