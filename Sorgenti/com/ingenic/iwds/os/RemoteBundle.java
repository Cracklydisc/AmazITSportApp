package com.ingenic.iwds.os;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.ingenic.iwds.content.RemoteIntent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RemoteBundle implements Parcelable, Cloneable {
    public static final Creator<RemoteBundle> CREATOR = new C11331();
    private Map<String, Object> f90a;

    static class C11331 implements Creator<RemoteBundle> {
        C11331() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m72a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m73a(i);
        }

        public RemoteBundle m72a(Parcel parcel) {
            return new RemoteBundle(parcel);
        }

        public RemoteBundle[] m73a(int i) {
            return new RemoteBundle[i];
        }
    }

    public RemoteBundle(RemoteBundle in) {
        if (in.f90a != null) {
            this.f90a = new HashMap(in.f90a);
        }
    }

    protected RemoteBundle(Bundle bundle) {
        Iterator it = bundle.keySet().iterator();
        if (it != null) {
            if (this.f90a == null) {
                this.f90a = new HashMap();
            }
            while (it.hasNext()) {
                String str = (String) it.next();
                Object obj = bundle.get(str);
                if (obj instanceof Bundle) {
                    this.f90a.put(str, fromBunble((Bundle) obj));
                } else {
                    int length;
                    Parcelable parcelable;
                    if (obj instanceof Parcelable[]) {
                        Parcelable[] parcelableArr = (Parcelable[]) obj;
                        length = parcelableArr.length;
                        for (int i = 0; i < length; i++) {
                            parcelable = parcelableArr[i];
                            if (parcelable instanceof Bundle) {
                                parcelableArr[i] = fromBunble((Bundle) parcelable);
                            } else if (parcelable instanceof Intent) {
                                parcelableArr[i] = RemoteIntent.fromIntent((Intent) parcelable);
                            }
                        }
                    } else if ((obj instanceof ArrayList) && obj != null) {
                        ArrayList arrayList = (ArrayList) obj;
                        int size = arrayList.size();
                        if (size > 0) {
                            Object obj2 = arrayList.get(0);
                            if (obj2 instanceof Parcelable) {
                                for (length = 0; length < size; length++) {
                                    parcelable = (Parcelable) arrayList.get(length);
                                    if (parcelable instanceof Bundle) {
                                        arrayList.set(length, fromBunble((Bundle) obj2));
                                    } else if (parcelable instanceof Intent) {
                                        arrayList.set(length, RemoteIntent.fromIntent((Intent) obj2));
                                    }
                                }
                            }
                        }
                    }
                    this.f90a.put(str, obj);
                }
            }
        }
    }

    protected RemoteBundle(Parcel source) {
        int readInt = source.readInt();
        if (readInt > 0) {
            this.f90a = new HashMap(readInt);
            source.readMap(this.f90a, getClass().getClassLoader());
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        if (this.f90a != null) {
            dest.writeInt(this.f90a.size());
            dest.writeMap(this.f90a);
            return;
        }
        dest.writeInt(0);
    }

    public Object clone() {
        return new RemoteBundle(this);
    }

    public static RemoteBundle fromBunble(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        return new RemoteBundle(bundle);
    }

    public Bundle toBundle() {
        if (this.f90a == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        Iterator it = this.f90a.keySet().iterator();
        if (it != null) {
            while (it.hasNext()) {
                String str = (String) it.next();
                Object obj = this.f90a.get(str);
                if (obj instanceof Byte) {
                    bundle.putByte(str, ((Byte) obj).byteValue());
                } else if (obj instanceof Short) {
                    bundle.putShort(str, ((Short) obj).shortValue());
                } else if (obj instanceof Integer) {
                    bundle.putInt(str, ((Integer) obj).intValue());
                } else if (obj instanceof Long) {
                    bundle.putLong(str, ((Long) obj).longValue());
                } else if (obj instanceof Float) {
                    bundle.putFloat(str, ((Float) obj).floatValue());
                } else if (obj instanceof Double) {
                    bundle.putDouble(str, ((Double) obj).doubleValue());
                } else if (obj instanceof Boolean) {
                    bundle.putBoolean(str, ((Boolean) obj).booleanValue());
                } else if (obj instanceof Character) {
                    bundle.putChar(str, ((Character) obj).charValue());
                } else if (obj instanceof String) {
                    bundle.putString(str, (String) obj);
                } else if (obj instanceof CharSequence) {
                    bundle.putCharSequence(str, (CharSequence) obj);
                } else if (obj instanceof RemoteBundle) {
                    bundle.putBundle(str, ((RemoteBundle) obj).toBundle());
                } else if (obj instanceof IBinder) {
                    bundle.putBinder(str, (IBinder) obj);
                } else if (obj instanceof byte[]) {
                    bundle.putByteArray(str, (byte[]) obj);
                } else if (obj instanceof short[]) {
                    bundle.putShortArray(str, (short[]) obj);
                } else if (obj instanceof int[]) {
                    bundle.putIntArray(str, (int[]) obj);
                } else if (obj instanceof long[]) {
                    bundle.putLongArray(str, (long[]) obj);
                } else if (obj instanceof float[]) {
                    bundle.putFloatArray(str, (float[]) obj);
                } else if (obj instanceof double[]) {
                    bundle.putDoubleArray(str, (double[]) obj);
                } else if (obj instanceof boolean[]) {
                    bundle.putBooleanArray(str, (boolean[]) obj);
                } else if (obj instanceof char[]) {
                    bundle.putCharArray(str, (char[]) obj);
                } else if (obj instanceof String[]) {
                    bundle.putStringArray(str, (String[]) obj);
                } else if (obj instanceof CharSequence[]) {
                    bundle.putCharSequenceArray(str, (CharSequence[]) obj);
                } else if (obj instanceof ArrayList) {
                    if (obj != null) {
                        ArrayList arrayList = (ArrayList) obj;
                        if (arrayList.size() > 0) {
                            Object obj2 = arrayList.get(0);
                            if (obj2 instanceof Integer) {
                                bundle.putIntegerArrayList(str, arrayList);
                            } else if (obj2 instanceof String) {
                                bundle.putStringArrayList(str, arrayList);
                            } else if (obj2 instanceof CharSequence) {
                                bundle.putCharSequenceArrayList(str, arrayList);
                            } else if (obj2 instanceof Parcelable) {
                                r7 = arrayList.size();
                                for (r4 = 0; r4 < r7; r4++) {
                                    r2 = (Parcelable) arrayList.get(r4);
                                    if (r2 instanceof RemoteBundle) {
                                        arrayList.set(r4, ((RemoteBundle) r2).toBundle());
                                    } else if (r2 instanceof RemoteIntent) {
                                        arrayList.set(r4, ((RemoteIntent) r2).toIntent());
                                    }
                                }
                                bundle.putParcelableArrayList(str, arrayList);
                            }
                        }
                    }
                } else if (obj instanceof SparseArray) {
                    SparseArray sparseArray = (SparseArray) obj;
                    r7 = sparseArray.size();
                    for (r4 = 0; r4 < r7; r4++) {
                        int keyAt = sparseArray.keyAt(r4);
                        r2 = (Parcelable) sparseArray.get(keyAt);
                        if (r2 instanceof RemoteBundle) {
                            sparseArray.put(keyAt, ((RemoteBundle) r2).toBundle());
                        } else if (r2 instanceof RemoteIntent) {
                            sparseArray.put(keyAt, ((RemoteIntent) r2).toIntent());
                        }
                    }
                    bundle.putSparseParcelableArray(str, sparseArray);
                } else if (obj instanceof Parcelable) {
                    if (obj instanceof RemoteIntent) {
                        bundle.putParcelable(str, ((RemoteIntent) obj).toIntent());
                    } else {
                        bundle.putParcelable(str, (Parcelable) obj);
                    }
                } else if (obj instanceof Serializable) {
                    bundle.putSerializable(str, (Serializable) obj);
                } else if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    r7 = parcelableArr.length;
                    for (r4 = 0; r4 < r7; r4++) {
                        r2 = parcelableArr[r4];
                        if (r2 instanceof RemoteBundle) {
                            parcelableArr[r4] = ((RemoteBundle) r2).toBundle();
                        } else if (r2 instanceof RemoteIntent) {
                            parcelableArr[r4] = ((RemoteIntent) r2).toIntent();
                        }
                    }
                    bundle.putParcelableArray(str, parcelableArr);
                }
            }
        }
        return bundle;
    }
}
