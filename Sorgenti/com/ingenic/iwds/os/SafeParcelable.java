package com.ingenic.iwds.os;

public interface SafeParcelable {

    public interface Creator<T> {
        T createFromParcel(SafeParcel safeParcel);
    }

    public interface ClassLoaderCreator<T> extends Creator<T> {
    }

    void writeToParcel(SafeParcel safeParcel, int i);
}
