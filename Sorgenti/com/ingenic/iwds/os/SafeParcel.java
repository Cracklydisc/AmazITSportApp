package com.ingenic.iwds.os;

import com.ingenic.iwds.os.SafeParcelable.Creator;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.HashMap;

public final class SafeParcel {
    public static final Creator<String> STRING_CREATOR = new C11341();
    private static final SafeParcel[] f92a = new SafeParcel[256];
    private static final SafeParcel[] f93b = new SafeParcel[256];
    private static final HashMap<ClassLoader, HashMap<String, Creator>> f94e = new HashMap();
    private long f95c;
    private boolean f96d;

    static class C11341 implements Creator<String> {
        C11341() {
        }

        public /* synthetic */ Object createFromParcel(SafeParcel safeParcel) {
            return m74a(safeParcel);
        }

        public String m74a(SafeParcel safeParcel) {
            return safeParcel.readString();
        }
    }

    class C11352 extends ObjectInputStream {
        final /* synthetic */ ClassLoader f91a;

        protected Class<?> resolveClass(ObjectStreamClass osClass) throws IOException, ClassNotFoundException {
            if (this.f91a != null) {
                Class<?> cls = Class.forName(osClass.getName(), false, this.f91a);
                if (cls != null) {
                    return cls;
                }
            }
            return super.resolveClass(osClass);
        }
    }

    private static native long nativeCreate();

    private static native void nativeDestroy(long j);

    private static native void nativeFreeBuffer(long j);

    private static native byte[] nativeMarshall(long j);

    private static native long nativeReadLong(long j);

    private static native String nativeReadString(long j);

    private static native void nativeSetDataPosition(long j, int i);

    private static native void nativeUnmarshall(long j, byte[] bArr, int i, int i2);

    private static native void nativeWriteLong(long j, long j2);

    static {
        System.loadLibrary("safeparcel");
    }

    private SafeParcel(long nativePtr) {
        m76a(nativePtr);
    }

    private void m76a(long j) {
        if (j != 0) {
            this.f95c = j;
            this.f96d = false;
            return;
        }
        this.f95c = nativeCreate();
        this.f96d = true;
    }

    public static SafeParcel obtain() {
        synchronized (f92a) {
            for (int i = 0; i < 256; i++) {
                SafeParcel safeParcel = f92a[i];
                if (safeParcel != null) {
                    f92a[i] = null;
                    return safeParcel;
                }
            }
            return new SafeParcel(0);
        }
    }

    public final void recycle() {
        SafeParcel[] safeParcelArr;
        m75a();
        if (this.f96d) {
            safeParcelArr = f92a;
        } else {
            this.f95c = 0;
            safeParcelArr = f93b;
        }
        synchronized (safeParcelArr) {
            for (int i = 0; i < 256; i++) {
                if (safeParcelArr[i] == null) {
                    safeParcelArr[i] = this;
                    return;
                }
            }
        }
    }

    private void m75a() {
        if (this.f96d) {
            nativeFreeBuffer(this.f95c);
        }
    }

    private void m77b() {
        if (this.f95c != 0) {
            if (this.f96d) {
                nativeDestroy(this.f95c);
            }
            this.f95c = 0;
        }
    }

    protected void finalize() throws Throwable {
        m77b();
    }

    public final void setDataPosition(int pos) {
        nativeSetDataPosition(this.f95c, pos);
    }

    public final byte[] marshall() {
        return nativeMarshall(this.f95c);
    }

    public final void unmarshall(byte[] data, int offest, int length) {
        nativeUnmarshall(this.f95c, data, offest, length);
    }

    public final void writeLong(long val) {
        nativeWriteLong(this.f95c, val);
    }

    public final String readString() {
        return nativeReadString(this.f95c);
    }

    public final long readLong() {
        return nativeReadLong(this.f95c);
    }
}
