package com.ingenic.iwds.os;

import com.ingenic.iwds.os.SafeParcelable.Creator;
import java.util.UUID;

public class SafeParcelUuid implements SafeParcelable {
    public static final Creator<SafeParcelUuid> CREATOR = new C11361();
    private UUID f97a;

    static class C11361 implements Creator<SafeParcelUuid> {
        C11361() {
        }

        public /* synthetic */ Object createFromParcel(SafeParcel safeParcel) {
            return m78a(safeParcel);
        }

        public SafeParcelUuid m78a(SafeParcel safeParcel) {
            return new SafeParcelUuid(new UUID(safeParcel.readLong(), safeParcel.readLong()));
        }
    }

    public SafeParcelUuid(UUID uuid) {
        this.f97a = uuid;
    }

    public String toString() {
        return this.f97a.toString();
    }

    public int hashCode() {
        return this.f97a.hashCode();
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!(object instanceof SafeParcelUuid)) {
            return false;
        }
        return this.f97a.equals(((SafeParcelUuid) object).f97a);
    }

    public void writeToParcel(SafeParcel dest, int flags) {
        dest.writeLong(this.f97a.getMostSignificantBits());
        dest.writeLong(this.f97a.getLeastSignificantBits());
    }
}
