package com.ingenic.iwds.uniconnect;

import com.ingenic.iwds.utils.IwdsAssert;

public class UniconnectErrorCode {
    public static String errorString(int error) {
        switch (error) {
            case -6:
                return "port was disconnected";
            case -5:
                return "port was closed";
            case -4:
                return "remote service was died";
            case -3:
                return "port is busy(grabbed)";
            case -2:
                return "link was disconnected";
            case -1:
                return "link was unbonded";
            default:
                IwdsAssert.dieIf("UniconnectErrorCode", true, "Implement me.");
                return null;
        }
    }
}
