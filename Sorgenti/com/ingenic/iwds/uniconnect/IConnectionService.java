package com.ingenic.iwds.uniconnect;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;

public interface IConnectionService extends IInterface {

    public static abstract class Stub extends Binder implements IConnectionService {

        private static class C1215a implements IConnectionService {
            private IBinder f214a;

            C1215a(IBinder iBinder) {
                this.f214a = iBinder;
            }

            public IBinder asBinder() {
                return this.f214a;
            }

            public IConnection createConnection(IConnectionCallBack callback) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnectionService");
                    obtain.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.f214a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    IConnection asInterface = com.ingenic.iwds.uniconnect.IConnection.Stub.asInterface(obtain2.readStrongBinder());
                    return asInterface;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public DeviceDescriptor[] getConnectedDeviceDescriptors() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnectionService");
                    this.f214a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    DeviceDescriptor[] deviceDescriptorArr = (DeviceDescriptor[]) obtain2.createTypedArray(DeviceDescriptor.CREATOR);
                    return deviceDescriptorArr;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.uniconnect.IConnectionService");
        }

        public static IConnectionService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.uniconnect.IConnectionService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IConnectionService)) {
                return new C1215a(obj);
            }
            return (IConnectionService) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnectionService");
                    IConnection createConnection = createConnection(com.ingenic.iwds.uniconnect.IConnectionCallBack.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeStrongBinder(createConnection != null ? createConnection.asBinder() : null);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnectionService");
                    Parcelable[] connectedDeviceDescriptors = getConnectedDeviceDescriptors();
                    reply.writeNoException();
                    reply.writeTypedArray(connectedDeviceDescriptors, 1);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.uniconnect.IConnectionService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    IConnection createConnection(IConnectionCallBack iConnectionCallBack) throws RemoteException;

    DeviceDescriptor[] getConnectedDeviceDescriptors() throws RemoteException;
}
