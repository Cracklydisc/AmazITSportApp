package com.ingenic.iwds.uniconnect;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.uniconnect.IConnectionService.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;

public class ConnectionServiceManager extends ServiceManagerContext {
    private IConnectionService f211a;

    class C12111 extends ServiceClientProxy {
        final /* synthetic */ ConnectionServiceManager f209a;

        C12111(ConnectionServiceManager connectionServiceManager) {
            this.f209a = connectionServiceManager;
        }

        public void onServiceConnected(IBinder service) {
            this.f209a.f211a = Stub.asInterface(service);
        }

        public void onServiceDisconnected(boolean unexpected) {
        }
    }

    class C12122 extends IConnectionCallBack.Stub {
        final /* synthetic */ ConnectionServiceManager f210a;

        C12122(ConnectionServiceManager connectionServiceManager) {
            this.f210a = connectionServiceManager;
        }

        public void stateChanged(int state, int arg0) throws RemoteException {
        }
    }

    public ConnectionServiceManager(Context context) {
        super(context);
        this.m_serviceClientProxy = new C12111(this);
    }

    public Connection createConnection() {
        return m241a(new C12122(this));
    }

    public DeviceDescriptor[] getConnectedDeviceDescriptors() {
        try {
            return this.f211a.getConnectedDeviceDescriptors();
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in getConnectedDeviceDescriptors: " + e);
            return null;
        }
    }

    private Connection m241a(IConnectionCallBack iConnectionCallBack) {
        IwdsAssert.dieIf((Object) this, iConnectionCallBack == null, "Connection callback is null.");
        try {
            return new Connection(getContext(), this.f211a.createConnection(iConnectionCallBack));
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in createConnection: " + e);
            return null;
        }
    }
}
