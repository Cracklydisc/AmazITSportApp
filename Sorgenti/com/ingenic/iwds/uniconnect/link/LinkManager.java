package com.ingenic.iwds.uniconnect.link;

import android.content.Context;

public class LinkManager {
    private Context f241a;
    private AdapterManager f242b;

    private static final native String nativeGetRemoteAddress(String str);

    Context m261b() {
        return this.f241a;
    }

    AdapterManager m262d() {
        return this.f242b;
    }

    String m263e(String str) {
        return nativeGetRemoteAddress(str);
    }
}
