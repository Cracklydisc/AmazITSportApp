package com.ingenic.iwds.uniconnect.link;

import com.ingenic.iwds.DeviceDescriptor;
import java.util.ArrayList;

public class RemoteDeviceDescriptorStorage {
    private static RemoteDeviceDescriptorStorage f243a;
    private static ArrayList<DeviceDescriptor> f244b;

    public static RemoteDeviceDescriptorStorage getInstance() {
        if (f243a == null) {
            f243a = new RemoteDeviceDescriptorStorage();
        }
        return f243a;
    }

    private RemoteDeviceDescriptorStorage() {
        f244b = new ArrayList();
    }

    public void addDeviceDescriptors(DeviceDescriptor deviceDescriptor) {
        synchronized (f244b) {
            f244b.add(deviceDescriptor);
        }
    }

    public void removeDeviceDescriptor(DeviceDescriptor deviceDescriptor) {
        synchronized (f244b) {
            f244b.remove(deviceDescriptor);
        }
    }

    public DeviceDescriptor[] getDeviceDescriptorsArray() {
        DeviceDescriptor[] deviceDescriptorArr;
        synchronized (f244b) {
            Object[] toArray = f244b.toArray();
            deviceDescriptorArr = new DeviceDescriptor[toArray.length];
            for (int i = 0; i < deviceDescriptorArr.length; i++) {
                deviceDescriptorArr[i] = (DeviceDescriptor) toArray[i];
            }
        }
        return deviceDescriptorArr;
    }
}
