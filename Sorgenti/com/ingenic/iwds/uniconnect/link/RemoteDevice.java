package com.ingenic.iwds.uniconnect.link;

public abstract class RemoteDevice {
    public static int TYPE_BLUETOOTH_CLASSIC = 0;
    public static int TYPE_BLUETOOTH_DUAL = 1;
    public static int TYPE_BLUETOOTH_LE = 2;
    public static int TYPE_BLUETOOTH_UNKNOWN = 3;
}
