package com.ingenic.iwds.uniconnect.link;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.ingenic.iwds.uniconnect.link.Adapter.DeviceDiscoveryCallbacks;
import com.ingenic.iwds.utils.IwdsLog;

public class AndroidBtAdapter extends Adapter {
    private BluetoothAdapter f219a;
    private DeviceDiscoveryCallbacks f220b;
    private boolean f221c;
    private int f222d;

    class C12161 extends BroadcastReceiver {
        final /* synthetic */ AndroidBtAdapter f218a;

        public void onReceive(Context context, Intent intent) {
            if (this.f218a.f221c) {
                String action = intent.getAction();
                switch (this.f218a.f222d) {
                    case 0:
                        if ("android.bluetooth.adapter.action.DISCOVERY_STARTED".equals(action)) {
                            this.f218a.clearRemoteDevices();
                            this.f218a.f220b.onDiscoveryStarted();
                            this.f218a.f222d = 1;
                            return;
                        }
                        return;
                    case 1:
                        if ("android.bluetooth.device.action.FOUND".equals(action)) {
                            RemoteDevice androidBtDevice = new AndroidBtDevice((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"));
                            this.f218a.addRemoteDevice(androidBtDevice);
                            this.f218a.f220b.onDeviceFound(androidBtDevice);
                            this.f218a.f222d = 1;
                            return;
                        } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                            this.f218a.f220b.onDiscoveryFinished();
                            this.f218a.f221c = false;
                            this.f218a.f222d = 0;
                            return;
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            }
        }
    }

    public boolean enable() {
        IwdsLog.m269w(this, "Call m_btAdapter.enable");
        return this.f219a.enable();
    }

    public void disable() {
        IwdsLog.m269w(this, "Call m_btAdapter.disable");
        this.f219a.disable();
    }
}
