package com.ingenic.iwds.uniconnect.link;

import java.util.ArrayList;

public abstract class Adapter {
    public static String TAG_ANDROID_BT_DATA_CHANNEL = "Android BT data channel";
    public static String TAG_BLE_DATA_CHANNEL = "BLE data channel";
    private String f215d;
    private ArrayList<RemoteDevice> f216e;

    public interface DeviceDiscoveryCallbacks {
        void onDeviceFound(RemoteDevice remoteDevice);

        void onDiscoveryFinished();

        void onDiscoveryStarted();
    }

    public abstract void disable();

    public abstract boolean enable();

    public String getLinkTag() {
        return this.f215d;
    }

    protected void addRemoteDevice(RemoteDevice device) {
        this.f216e.add(device);
    }

    protected void clearRemoteDevices() {
        this.f216e.clear();
    }
}
