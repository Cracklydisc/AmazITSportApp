package com.ingenic.iwds.uniconnect.link;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.IwdsApplication;
import com.ingenic.iwds.uniconnect.Connection;
import com.ingenic.iwds.uniconnect.IConnectionCallBack.Stub;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.utils.serializable.ParcelableUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Link {
    private LinkManager f232a;
    private String f233b;
    private DeviceDescriptor f234f;
    private DeviceDescriptor f235g;
    private Connection f236h;
    private String f237i;
    private HandlerThread f238j;
    private HandlerThread f239k;
    private int f240n;

    class C12171 extends Stub {
        public void stateChanged(int state, int arg0) throws RemoteException {
        }
    }

    public static class LinkState {
    }

    private class C1218a extends Handler {
        final /* synthetic */ Link f224a;
        private DataInputStream f225b;
        private DataOutputStream f226c;
        private byte[] f227d;

        public void handleMessage(Message msg) {
            Intent intent;
            switch (msg.what) {
                case 0:
                    try {
                        byte[] bArr = new byte[this.f225b.readInt()];
                        this.f225b.read(bArr);
                        this.f224a.f235g = (DeviceDescriptor) ParcelableUtils.unmarshall(bArr, DeviceDescriptor.CREATOR);
                        Message.obtain(this, 1).sendToTarget();
                        return;
                    } catch (IOException e) {
                        this.f226c = null;
                        this.f225b = null;
                        this.f224a.f236h.close();
                        return;
                    }
                case 1:
                    try {
                        this.f226c.writeInt(this.f227d.length);
                        this.f226c.write(this.f227d);
                        Message.obtain(this, 2).sendToTarget();
                        IwdsLog.m267i(this, "Send local device descriptor: " + this.f224a.f234f.toString());
                        return;
                    } catch (IOException e2) {
                        this.f226c = null;
                        this.f225b = null;
                        this.f224a.f236h.close();
                        return;
                    }
                case 2:
                    if (this.f224a.f235g != null) {
                        IwdsLog.m267i(this, "Connect to remote device: " + this.f224a.f235g.toString());
                        this.f224a.m251a(1);
                        RemoteDeviceDescriptorStorage.getInstance().addDeviceDescriptors(this.f224a.f235g);
                        intent = new Intent("iwds.uniconnect.action.connected_address");
                        intent.putExtra("DeviceDescriptor", this.f224a.f235g);
                        this.f224a.f232a.m261b().sendBroadcast(intent);
                    }
                    this.f226c = null;
                    this.f225b = null;
                    this.f224a.f236h.close();
                    return;
                case 3:
                    if (this.f224a.f235g != null) {
                        IwdsLog.m267i(this, "Disconnect from remote device: " + this.f224a.f235g.toString());
                        this.f224a.m251a(0);
                        RemoteDeviceDescriptorStorage.getInstance().removeDeviceDescriptor(this.f224a.f235g);
                        intent = new Intent("iwds.uniconnect.action.disconnected_address");
                        intent.putExtra("DeviceDescriptor", this.f224a.f235g);
                        this.f224a.f232a.m261b().sendBroadcast(intent);
                        this.f224a.f235g = null;
                        return;
                    }
                    return;
                case 4:
                    this.f224a.f239k.quit();
                    return;
                case 5:
                    if (this.f224a.f236h.open((String) msg.obj, this.f224a.f237i) < 0) {
                        return;
                    }
                    if (this.f224a.f236h.handshake() < 0) {
                        this.f224a.f236h.close();
                        return;
                    }
                    this.f225b = new DataInputStream(this.f224a.f236h.getInputStream());
                    this.f226c = new DataOutputStream(this.f224a.f236h.getOutputStream());
                    Message.obtain(this, 0).sendToTarget();
                    return;
                default:
                    return;
            }
        }
    }

    private class C1219b extends Handler {
        final /* synthetic */ Link f228a;
        private DataInputStream f229b;
        private DataOutputStream f230c;
        private byte[] f231d;

        public void handleMessage(Message msg) {
            Intent intent;
            switch (msg.what) {
                case 0:
                    try {
                        this.f230c.writeInt(this.f231d.length);
                        this.f230c.write(this.f231d);
                        Message.obtain(this, 1).sendToTarget();
                        IwdsLog.m267i(this, "Send local device descriptor: " + this.f228a.f234f.toString());
                        return;
                    } catch (IOException e) {
                        this.f230c = null;
                        this.f229b = null;
                        this.f228a.f236h.close();
                        return;
                    }
                case 1:
                    try {
                        byte[] bArr = new byte[this.f229b.readInt()];
                        this.f229b.read(bArr);
                        DeviceDescriptor deviceDescriptor = (DeviceDescriptor) ParcelableUtils.unmarshall(bArr, DeviceDescriptor.CREATOR);
                        if ("02:00:00:00:00:00".equals(deviceDescriptor.devAddress)) {
                            deviceDescriptor.devAddress = this.f228a.f232a.m263e(this.f228a.f233b);
                        }
                        this.f228a.f235g = deviceDescriptor;
                        Message.obtain(this, 2).sendToTarget();
                        return;
                    } catch (IOException e2) {
                        this.f230c = null;
                        this.f229b = null;
                        this.f228a.f236h.close();
                        return;
                    }
                case 2:
                    if (this.f228a.f235g != null) {
                        IwdsLog.m267i(this, "Connect to remote device: " + this.f228a.f235g.toString());
                        this.f228a.m251a(1);
                        RemoteDeviceDescriptorStorage.getInstance().addDeviceDescriptors(this.f228a.f235g);
                        intent = new Intent("iwds.uniconnect.action.connected_address");
                        intent.putExtra("DeviceDescriptor", this.f228a.f235g);
                        this.f228a.f232a.m261b().sendBroadcast(intent);
                    }
                    this.f230c = null;
                    this.f229b = null;
                    this.f228a.f236h.close();
                    return;
                case 3:
                    if (this.f228a.f235g != null) {
                        IwdsLog.m267i(this, "Disconnect from remote device: " + this.f228a.f235g.toString());
                        this.f228a.m251a(0);
                        RemoteDeviceDescriptorStorage.getInstance().removeDeviceDescriptor(this.f228a.f235g);
                        intent = new Intent("iwds.uniconnect.action.disconnected_address");
                        intent.putExtra("DeviceDescriptor", this.f228a.f235g);
                        this.f228a.f232a.m261b().sendBroadcast(intent);
                        this.f228a.f235g = null;
                        return;
                    }
                    return;
                case 4:
                    this.f228a.f238j.quit();
                    return;
                case 5:
                    if (this.f228a.f236h.open((String) msg.obj, this.f228a.f237i) >= 0) {
                        int handshake = this.f228a.f236h.handshake();
                        if (handshake < 0) {
                            this.f228a.f236h.close();
                            if (handshake == -11) {
                                this.f228a.m250a();
                                return;
                            }
                            return;
                        }
                        this.f229b = new DataInputStream(this.f228a.f236h.getInputStream());
                        this.f230c = new DataOutputStream(this.f228a.f236h.getOutputStream());
                        Message.obtain(this, 0).sendToTarget();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void m251a(int i) {
        synchronized (this) {
            if (i == 1) {
                this.f240n = 1;
            } else {
                this.f240n = 0;
            }
        }
    }

    private void m250a() {
        WakeLock newWakeLock = ((PowerManager) IwdsApplication.getAppContext().getSystemService("power")).newWakeLock(1, getClass().getName());
        IwdsLog.m264d(this, "Hold wake lock to avoid system sleeping when restart adapter.");
        newWakeLock.acquire(20000);
        IwdsLog.m269w(this, "Restarting adapter...");
        Adapter adapter = this.f232a.m262d().getAdapter(this.f233b);
        adapter.disable();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        adapter.enable();
    }
}
