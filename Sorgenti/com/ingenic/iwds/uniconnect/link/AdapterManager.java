package com.ingenic.iwds.uniconnect.link;

import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;
import java.util.Iterator;

public class AdapterManager {
    private ArrayList<Adapter> f217d;

    public Adapter getAdapter(String linkTag) {
        Iterator it = this.f217d.iterator();
        while (it.hasNext()) {
            Adapter adapter = (Adapter) it.next();
            if (adapter.getLinkTag().equals(linkTag)) {
                return adapter;
            }
        }
        IwdsAssert.dieIf("Adapter", true, "Unsupport link type, tag: " + linkTag);
        return null;
    }
}
