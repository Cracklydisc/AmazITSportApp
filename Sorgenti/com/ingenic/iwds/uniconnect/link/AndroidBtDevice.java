package com.ingenic.iwds.uniconnect.link;

import android.bluetooth.BluetoothDevice;

public class AndroidBtDevice extends RemoteDevice {
    private BluetoothDevice f223a;

    AndroidBtDevice(BluetoothDevice device) {
        this.f223a = device;
    }

    public String getName() {
        return this.f223a.getName();
    }

    public int hashCode() {
        return (this.f223a == null ? 0 : this.f223a.hashCode()) + 31;
    }

    public String toString() {
        return "AndroidBtDevice [Name=" + getName() + ", MAC=" + this.f223a + "]";
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AndroidBtDevice androidBtDevice = (AndroidBtDevice) obj;
        if (this.f223a == null) {
            if (androidBtDevice.f223a != null) {
                return false;
            }
            return true;
        } else if (this.f223a.equals(androidBtDevice.f223a)) {
            return true;
        } else {
            return false;
        }
    }
}
