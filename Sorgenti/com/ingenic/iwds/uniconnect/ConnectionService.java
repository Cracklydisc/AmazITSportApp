package com.ingenic.iwds.uniconnect;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.uniconnect.IConnection.Stub;
import com.ingenic.iwds.uniconnect.link.RemoteDeviceDescriptorStorage;
import com.ingenic.iwds.utils.IwdsLog;
import java.nio.ByteBuffer;
import java.util.Locale;

public class ConnectionService extends Service {
    private C1210a f208a = new C1210a();

    public static class ConnectionStub extends Stub implements DeathRecipient {
        private ByteBuffer f201a;
        private ByteBuffer f202b;
        private String f203c;
        private long f204d = -1;
        private Object f205e = new Object();
        private IConnectionCallBack f206f;

        public ConnectionStub(IConnectionCallBack callBack) {
            this.f206f = callBack;
        }

        public void binderDied() {
            synchronized (this.f205e) {
                if (this.f204d >= 0) {
                    ConnectionService.nativeDestroyConnection(this.f203c, this.f204d);
                    this.f203c = null;
                    this.f204d = -1;
                }
                this.f201a = null;
                this.f202b = null;
                this.f205e = null;
            }
        }

        public long open(String userName, int userPid, String address, String uuid) throws RemoteException {
            long j;
            synchronized (this.f205e) {
                this.f203c = address;
                this.f204d = ConnectionService.nativeCreateConnectionByUuid(userName, userPid, this.f203c, uuid.toLowerCase(Locale.ENGLISH), this.f206f);
                if (this.f204d >= 0) {
                    int b = ConnectionService.nativeGetMaxPayloadSize(this.f203c, this.f204d);
                    if (b < 0) {
                        j = (long) b;
                    } else {
                        allocReadBuffer(b);
                        this.f201a = ByteBuffer.allocateDirect(b);
                        this.f202b = ByteBuffer.allocateDirect(b + 10);
                        this.f206f.asBinder().linkToDeath(this, 0);
                    }
                }
                j = this.f204d;
            }
            return j;
        }

        public void close() throws RemoteException {
            synchronized (this.f205e) {
                if (this.f204d >= 0) {
                    ConnectionService.nativeDestroyConnection(this.f203c, this.f204d);
                    this.f206f.asBinder().unlinkToDeath(this, 0);
                    this.f203c = null;
                    this.f204d = -1;
                }
            }
        }

        public int read(byte[] buffer, int offset, int maxSize) throws RemoteException {
            int i;
            synchronized (this.f205e) {
                String str = this.f203c;
                long j = this.f204d;
                if (j == -1) {
                    i = -5;
                } else {
                    synchronized (this.f201a) {
                        i = ConnectionService.nativeRead(str, j, this.f201a, 0, maxSize);
                        if (i >= 0) {
                            this.f201a.get(buffer, offset, i);
                            this.f201a.clear();
                        }
                    }
                }
            }
            return i;
        }

        public int write(byte[] buffer, int offset, int maxSize) throws RemoteException {
            int i;
            synchronized (this.f205e) {
                String str = this.f203c;
                long j = this.f204d;
                if (j == -1) {
                    i = -5;
                } else {
                    synchronized (this.f202b) {
                        this.f202b.position(10);
                        this.f202b.put(buffer, offset, maxSize);
                        i = ConnectionService.nativeWrite(str, j, this.f202b, 10, maxSize);
                    }
                }
            }
            return i;
        }

        public int available() throws RemoteException {
            synchronized (this.f205e) {
                String str = this.f203c;
                long j = this.f204d;
                if (j == -1) {
                    return -5;
                }
                return ConnectionService.nativeAvailable(str, j);
            }
        }

        public int handshake() throws RemoteException {
            synchronized (this.f205e) {
                String str = this.f203c;
                long j = this.f204d;
                if (j == -1) {
                    return -5;
                }
                return ConnectionService.nativeHandshake(str, j);
            }
        }

        public int getMaxPayloadSize() throws RemoteException {
            synchronized (this.f205e) {
                String str = this.f203c;
                long j = this.f204d;
                if (j == -1) {
                    return -5;
                }
                return ConnectionService.nativeGetMaxPayloadSize(str, j);
            }
        }
    }

    private class C1210a extends IConnectionService.Stub {
        final /* synthetic */ ConnectionService f207a;

        private C1210a(ConnectionService connectionService) {
            this.f207a = connectionService;
        }

        public IConnection createConnection(IConnectionCallBack callBack) throws RemoteException {
            return new ConnectionStub(callBack);
        }

        public DeviceDescriptor[] getConnectedDeviceDescriptors() throws RemoteException {
            return RemoteDeviceDescriptorStorage.getInstance().getDeviceDescriptorsArray();
        }
    }

    private static final native int nativeAvailable(String str, long j);

    private static final native long nativeCreateConnectionByUuid(String str, int i, String str2, String str3, IConnectionCallBack iConnectionCallBack);

    private static final native void nativeDestroyConnection(String str, long j);

    private static final native int nativeGetMaxPayloadSize(String str, long j);

    private static final native int nativeHandshake(String str, long j);

    private static final native int nativeRead(String str, long j, ByteBuffer byteBuffer, int i, int i2);

    private static final native int nativeWrite(String str, long j, ByteBuffer byteBuffer, int i, int i2);

    public IBinder onBind(Intent intent) {
        IwdsLog.m264d(this, "onBind()");
        return this.f208a;
    }
}
