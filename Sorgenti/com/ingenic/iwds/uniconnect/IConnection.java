package com.ingenic.iwds.uniconnect;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IConnection extends IInterface {

    public static abstract class Stub extends Binder implements IConnection {
        private byte[] f200a = null;

        private static class C1213a implements IConnection {
            private IBinder f212a;

            C1213a(IBinder iBinder) {
                this.f212a = iBinder;
            }

            public IBinder asBinder() {
                return this.f212a;
            }

            public long open(String userName, int userPid, String address, String uuid) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    obtain.writeString(userName);
                    obtain.writeInt(userPid);
                    obtain.writeString(address);
                    obtain.writeString(uuid);
                    this.f212a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    long readLong = obtain2.readLong();
                    return readLong;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void close() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    this.f212a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int write(byte[] buffer, int offset, int maxSize) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    obtain.writeByteArray(buffer, offset, maxSize);
                    this.f212a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    return readInt;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int read(byte[] buffer, int offset, int maxSize) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    if (buffer == null) {
                        obtain.writeInt(-1);
                    } else {
                        obtain.writeInt(maxSize);
                    }
                    this.f212a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    if (readInt >= 0) {
                        System.arraycopy(obtain2.createByteArray(), 0, buffer, offset, readInt);
                    }
                    obtain2.recycle();
                    obtain.recycle();
                    return readInt;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int available() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    this.f212a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    return readInt;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int handshake() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    this.f212a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    return readInt;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public int getMaxPayloadSize() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.ingenic.iwds.uniconnect.IConnection");
                    this.f212a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    int readInt = obtain2.readInt();
                    return readInt;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.uniconnect.IConnection");
        }

        public void allocReadBuffer(int bufferSize) {
            this.f200a = new byte[bufferSize];
        }

        public static IConnection asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("com.ingenic.iwds.uniconnect.IConnection");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IConnection)) {
                return new C1213a(obj);
            }
            return (IConnection) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            byte[] createByteArray;
            int write;
            switch (code) {
                case 1:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    long open = open(data.readString(), data.readInt(), data.readString(), data.readString());
                    reply.writeNoException();
                    reply.writeLong(open);
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    close();
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    createByteArray = data.createByteArray();
                    write = write(createByteArray, 0, createByteArray.length);
                    reply.writeNoException();
                    reply.writeInt(write);
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    int readInt = data.readInt();
                    if (readInt < 0) {
                        createByteArray = null;
                    } else {
                        createByteArray = this.f200a;
                    }
                    readInt = read(createByteArray, 0, readInt);
                    reply.writeNoException();
                    reply.writeInt(readInt);
                    if (readInt >= 0) {
                        reply.writeByteArray(createByteArray);
                    }
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    write = available();
                    reply.writeNoException();
                    reply.writeInt(write);
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    write = handshake();
                    reply.writeNoException();
                    reply.writeInt(write);
                    return true;
                case 7:
                    data.enforceInterface("com.ingenic.iwds.uniconnect.IConnection");
                    write = getMaxPayloadSize();
                    reply.writeNoException();
                    reply.writeInt(write);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.uniconnect.IConnection");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    int available() throws RemoteException;

    void close() throws RemoteException;

    int getMaxPayloadSize() throws RemoteException;

    int handshake() throws RemoteException;

    long open(String str, int i, String str2, String str3) throws RemoteException;

    int read(byte[] bArr, int i, int i2) throws RemoteException;

    int write(byte[] bArr, int i, int i2) throws RemoteException;
}
