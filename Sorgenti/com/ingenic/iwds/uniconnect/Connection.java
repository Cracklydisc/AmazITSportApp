package com.ingenic.iwds.uniconnect;

import android.content.Context;
import android.os.Process;
import android.os.RemoteException;
import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.common.exception.IwdsException;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Connection {
    private Context f191a;
    private int f192b = -1;
    private IConnection f193c;
    private String f194d;
    private String f195e;
    private long f196f = -1;
    private InputStream f197g;
    private OutputStream f198h;
    private boolean f199i = true;

    private static final class C1207a extends InputStream {
        private Connection f189a;

        public C1207a(Connection connection) {
            this.f189a = connection;
        }

        public int available() throws IOException {
            int available = this.f189a.available();
            if (available < 0) {
                IwdsException.throwUniconnectIOException(available);
            }
            return available;
        }

        public void close() throws IOException {
        }

        public int read() throws IOException {
            byte[] bArr = new byte[1];
            int i = 0;
            while (i != 1) {
                i = this.f189a.read(bArr, 0, 1);
                if (i < 0) {
                    IwdsException.throwUniconnectIOException(i);
                }
            }
            return bArr[0] & 255;
        }

        public int read(byte[] buffer, int offset, int maxSize) throws IOException {
            int read = this.f189a.read(buffer, offset, maxSize);
            if (read < 0) {
                IwdsException.throwUniconnectIOException(read);
            }
            return read;
        }
    }

    private static final class C1208b extends OutputStream {
        private Connection f190a;

        public C1208b(Connection connection) {
            this.f190a = connection;
        }

        public void close() throws IOException {
        }

        public void write(int oneByte) throws IOException {
            byte[] bArr = new byte[]{(byte) oneByte};
            int i = 0;
            while (i != 1) {
                i = this.f190a.write(bArr, 0, 1);
                if (i < 0) {
                    IwdsException.throwUniconnectIOException(i);
                }
            }
        }

        public void write(byte[] buffer, int i, int maxSize) throws IOException {
            while (maxSize > 0) {
                int write = this.f190a.write(buffer, i, maxSize);
                if (write < 0) {
                    IwdsException.throwUniconnectIOException(write);
                }
                i += write;
                maxSize -= write;
            }
        }

        public void flush() throws IOException {
        }
    }

    public Connection(Context context, IConnection connection) {
        boolean z = true;
        IwdsAssert.dieIf((Object) this, context == null, "Context is null.");
        if (connection != null) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Connection is null.");
        synchronized (this) {
            this.f191a = context;
            this.f193c = connection;
            this.f197g = new C1207a(this);
            this.f198h = new C1208b(this);
        }
    }

    public InputStream getInputStream() {
        return this.f197g;
    }

    public OutputStream getOutputStream() {
        return this.f198h;
    }

    public long getPort() {
        long j;
        synchronized (this) {
            j = this.f196f;
        }
        return j;
    }

    public int open(DeviceDescriptor deviceDescriptor, String uuid) {
        IwdsAssert.dieIf((Object) this, deviceDescriptor == null, "Device descriptor is null.");
        return open(deviceDescriptor.devAddress, uuid);
    }

    public int open(String address, String uuid) {
        boolean z = true;
        int i = 0;
        boolean z2 = address == null || address.isEmpty();
        IwdsAssert.dieIf((Object) this, z2, "Address is null or empty.");
        if (uuid == null || uuid.isEmpty()) {
            z2 = true;
        } else {
            z2 = false;
        }
        IwdsAssert.dieIf((Object) this, z2, "Uuid is null or empty.");
        if (uuid.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}") || uuid.equals("{THIS-IS-GOD-MASTER}")) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Invalid UUID: " + uuid + "(need a 8-4-4-4-12 style UUID).");
        synchronized (this) {
            if (this.f199i) {
                this.f194d = address;
                this.f195e = uuid;
                try {
                    long open = this.f193c.open(this.f191a.getPackageName(), Process.myPid(), address, this.f195e);
                    if (open < 0) {
                        i = (int) open;
                    } else {
                        this.f192b = this.f193c.getMaxPayloadSize();
                        this.f196f = open;
                        this.f199i = false;
                    }
                } catch (RemoteException e) {
                    IwdsLog.m265e((Object) this, "Exception in open: " + e);
                    i = -4;
                }
            }
        }
        return i;
    }

    public void close() {
        synchronized (this) {
            if (this.f199i) {
                return;
            }
            try {
                this.f193c.close();
                this.f196f = -1;
                this.f199i = true;
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in close: " + e);
            }
        }
    }

    public int write(byte[] buffer, int offset, int maxSize) {
        boolean z;
        if (buffer == null || (offset | maxSize) < 0 || buffer.length < offset + maxSize) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Buffer is null or buffer exceed.");
        int i = 0;
        while (maxSize > 0) {
            try {
                int write = this.f193c.write(buffer, offset, Math.min(maxSize, this.f192b));
                if (write >= 0) {
                    i += write;
                    offset += write;
                    maxSize -= write;
                } else if (i > 0) {
                    return i;
                } else {
                    return write;
                }
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in write: " + e);
                if (i <= 0) {
                    return -4;
                }
                return i;
            }
        }
        return i;
    }

    public int read(byte[] buffer, int offset, int maxSize) {
        boolean z;
        if (buffer == null || (offset | maxSize) < 0 || buffer.length < offset + maxSize) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Buffer is null or buffer exceed.");
        int i = 0;
        while (maxSize > 0) {
            try {
                int read = this.f193c.read(buffer, offset, Math.min(maxSize, this.f192b));
                if (read >= 0) {
                    i += read;
                    offset += read;
                    maxSize -= read;
                } else if (i > 0) {
                    return i;
                } else {
                    return read;
                }
            } catch (RemoteException e) {
                IwdsLog.m265e((Object) this, "Exception in read: " + e);
                if (i <= 0) {
                    return -4;
                }
                return i;
            }
        }
        return i;
    }

    public int available() {
        try {
            return this.f193c.available();
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in available: " + e);
            return -4;
        }
    }

    public int handshake() {
        try {
            return this.f193c.handshake();
        } catch (RemoteException e) {
            IwdsLog.m265e((Object) this, "Exception in handshake: " + e);
            return -4;
        }
    }
}
