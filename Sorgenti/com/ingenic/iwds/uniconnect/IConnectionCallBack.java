package com.ingenic.iwds.uniconnect;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IConnectionCallBack extends IInterface {

    public static abstract class Stub extends Binder implements IConnectionCallBack {

        private static class C1214a implements IConnectionCallBack {
            private IBinder f213a;

            C1214a(IBinder iBinder) {
                this.f213a = iBinder;
            }

            public IBinder asBinder() {
                return this.f213a;
            }

            public void stateChanged(int state, int arg0) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("IConnectionCallBack");
                    obtain.writeInt(state);
                    obtain.writeInt(arg0);
                    this.f213a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "IConnectionCallBack");
        }

        public static IConnectionCallBack asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface queryLocalInterface = obj.queryLocalInterface("IConnectionCallBack");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IConnectionCallBack)) {
                return new C1214a(obj);
            }
            return (IConnectionCallBack) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("IConnectionCallBack");
                    stateChanged(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("IConnectionCallBack");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void stateChanged(int i, int i2) throws RemoteException;
}
