package com.squareup.okhttp.internal.framed;

import java.io.IOException;

public interface IncomingStreamHandler {
    public static final IncomingStreamHandler REFUSE_INCOMING_STREAMS = new C12561();

    static class C12561 implements IncomingStreamHandler {
        C12561() {
        }

        public void receive(FramedStream stream) throws IOException {
            stream.close(ErrorCode.REFUSED_STREAM);
        }
    }

    void receive(FramedStream framedStream) throws IOException;
}
