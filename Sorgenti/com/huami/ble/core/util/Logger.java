package com.huami.ble.core.util;

import android.util.Log;

public class Logger {
    public static void m0D(String tag, String msg) {
        Log.d("HmCoreBT_" + tag, msg);
    }

    public static void m2W(String tag, String msg) {
        Log.w("HmCoreBT_" + tag, msg);
    }

    public static void m1E(String tag, String msg) {
        Log.e("HmCoreBT_" + tag, msg);
    }
}
