package com.huami.ble.core.util;

import android.bluetooth.BluetoothAdapter;

public class HmBleUtils {
    public static boolean checkAdapterStateOn() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter != null && adapter.getState() == 12) {
            return true;
        }
        Logger.m2W("HmBleUtils", "BT Adapter is not turned ON");
        return false;
    }
}
