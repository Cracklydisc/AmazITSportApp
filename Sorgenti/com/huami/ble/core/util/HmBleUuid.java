package com.huami.ble.core.util;

import android.os.ParcelUuid;

public class HmBleUuid {
    public static final ParcelUuid ACCEL_ENCRYPT_R_UUID = ParcelUuid.fromString("0000fede-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid ACCEL_ENCRYPT_W_UUID = ParcelUuid.fromString("0000fedd-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid APPEARANCE = ParcelUuid.fromString("00002a01-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid BATTERY_LEVEL = ParcelUuid.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid BODY_SENSOR_LOCATION = ParcelUuid.fromString("00002a38-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid CHAR_CLIENT_CONFIG = ParcelUuid.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid DISTANCE_AUTH_CHARA_UUID = ParcelUuid.fromString("0000fed2-0000-3512-2118-0009af100700");
    public static final ParcelUuid DISTANCE_CONFIG_DES_UUID = ParcelUuid.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid DISTANCE_MONITOR_SERVICE = ParcelUuid.fromString("0000fed1-0000-3512-2118-0009af100700");
    public static final ParcelUuid FIRMWARE_REVISION_STRING = ParcelUuid.fromString("00002a26-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid HEART_RATE = ParcelUuid.fromString("0000180d-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid HEART_RATE_MEASUREMENT = ParcelUuid.fromString("00002a37-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid MANUFACTURER_STRING = ParcelUuid.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid MODEL_NUMBER_STRING = ParcelUuid.fromString("00002a24-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid[] SUPPORT_SERVICES = new ParcelUuid[]{HEART_RATE};
    public static final ParcelUuid UUID_HUAMI_MIUI_SERVICE = ParcelUuid.fromString("0000fee0-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid UUID_HUAMI_SERVICE = ParcelUuid.fromString("0000fed0-0000-3512-2118-0009af100700");
    public static final ParcelUuid UUID_HUAMI_UNLOCK_MIUI_SERVICE = ParcelUuid.fromString("0000fee1-0000-1000-8000-00805f9b34fb");
    public static final ParcelUuid UUID_MI_SERVICE = ParcelUuid.fromString("0000fe95-0000-1000-8000-00805f9b34fb");
}
