package com.huami.ble.core.util;

public final class BleDeviceFilter {
    static final Filter ALL_FILTER = new AllFilter();
    private static final Filter[] FILTERS = new Filter[]{ALL_FILTER, SUPPORT_DEV_FILTER, LEQI_FILTER};
    static final Filter LEQI_FILTER = new LEQIDeviceFilter();
    static final Filter SUPPORT_DEV_FILTER = new HmSupportDeviceFilter();

    public interface Filter {
    }

    private static final class AllFilter implements Filter {
        private AllFilter() {
        }
    }

    private static final class HmSupportDeviceFilter implements Filter {
        private HmSupportDeviceFilter() {
        }
    }

    private static final class LEQIDeviceFilter implements Filter {
        private LEQIDeviceFilter() {
        }
    }

    private BleDeviceFilter() {
    }
}
