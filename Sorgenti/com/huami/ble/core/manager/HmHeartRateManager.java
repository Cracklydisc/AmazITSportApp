package com.huami.ble.core.manager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.huami.ble.core.aidl.IHeartRate;
import com.huami.ble.core.aidl.IHeartRate.Stub;
import com.huami.ble.core.manager.BluetoothLeProfile.LeServiceListener;
import com.huami.ble.core.util.HmBleUtils;
import com.huami.ble.core.util.Logger;

public class HmHeartRateManager implements BluetoothLeProfile {
    private static HmHeartRateManager mInstance;
    private final IBluetoothStateChangeCallback mBluetoothStateChangeCallback = new C03791();
    private final ServiceConnection mConnection = new C03802();
    private Context mContext;
    private IHeartRate mService;
    private LeServiceListener mServiceListener;

    class C03791 implements IBluetoothStateChangeCallback {
        C03791() {
        }

        public void onBluetoothStateChange(boolean up) {
            Logger.m0D("HmHeartRateManager", "onBluetoothStateChange: up=" + up);
            if (up) {
                synchronized (HmHeartRateManager.this.mConnection) {
                    try {
                        if (HmHeartRateManager.this.mService == null) {
                            Logger.m0D("HmHeartRateManager", "Binding service...");
                            HmHeartRateManager.this.doBind();
                        }
                    } catch (Exception re) {
                        Logger.m1E("HmHeartRateManager", re.toString());
                    }
                }
                return;
            }
            Logger.m0D("HmHeartRateManager", "Unbinding service...");
            synchronized (HmHeartRateManager.this.mConnection) {
                try {
                    HmHeartRateManager.this.mService = null;
                    HmHeartRateManager.this.mContext.unbindService(HmHeartRateManager.this.mConnection);
                } catch (Exception re2) {
                    Logger.m1E("HmHeartRateManager", re2.toString());
                }
            }
        }
    }

    class C03802 implements ServiceConnection {
        C03802() {
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            Logger.m0D("HmHeartRateManager", "Proxy object connected");
            HmHeartRateManager.this.mService = Stub.asInterface(service);
            if (HmHeartRateManager.this.mServiceListener != null) {
                Logger.m0D("HmHeartRateManager", " mInstance = " + HmHeartRateManager.mInstance + "   HmHeartRateManager.this = " + HmHeartRateManager.this);
                HmHeartRateManager.this.mServiceListener.onServiceConnected(1, HmHeartRateManager.this);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            Logger.m0D("HmHeartRateManager", "Proxy object disconnected");
            HmHeartRateManager.this.mService = null;
            if (HmHeartRateManager.this.mServiceListener != null) {
                HmHeartRateManager.this.mServiceListener.onServiceDisconnected(1);
            }
        }
    }

    public interface HmHeartRateCallback {
    }

    static synchronized HmHeartRateManager getInstance(Context context, LeServiceListener l) {
        HmHeartRateManager hmHeartRateManager;
        synchronized (HmHeartRateManager.class) {
            if (mInstance == null) {
                mInstance = new HmHeartRateManager(context, l);
            }
            hmHeartRateManager = mInstance;
        }
        return hmHeartRateManager;
    }

    private HmHeartRateManager(Context context, LeServiceListener l) {
        this.mContext = context;
        this.mServiceListener = l;
        BTStateChangeMonitor.getInstance(this.mContext).registerCallback(this.mBluetoothStateChangeCallback);
        doBind();
    }

    boolean doBind() {
        if (HmBleUtils.checkAdapterStateOn()) {
            Logger.m0D("HmHeartRateManager", "doBind");
            Intent intent = new Intent(IHeartRate.class.getName());
            intent.setPackage("com.huami.ble.core");
            intent.putExtra("pkgName", this.mContext.getPackageName());
            if (this.mContext.bindService(intent, this.mConnection, 1)) {
                return true;
            }
            Logger.m1E("HmHeartRateManager", "Could not bind to  IHeartRate with " + intent);
            return false;
        }
        Logger.m2W("HmHeartRateManager", "BT Adapter is not turned ON, not bind");
        return false;
    }

    public void finalize() {
        cleanup();
    }

    public void cleanup() {
        synchronized (this.mConnection) {
            if (this.mService != null) {
                try {
                    this.mService = null;
                    this.mContext.unbindService(this.mConnection);
                } catch (Exception re) {
                    Logger.m1E("HmHeartRateManager", re.toString());
                }
            }
        }
        this.mServiceListener = null;
        BTStateChangeMonitor.getInstance(this.mContext).registerCallback(this.mBluetoothStateChangeCallback);
        mInstance = null;
    }
}
