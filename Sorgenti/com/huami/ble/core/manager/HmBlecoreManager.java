package com.huami.ble.core.manager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.huami.ble.core.aidl.IBleCoreService;
import com.huami.ble.core.aidl.IBleCoreService.Stub;
import com.huami.ble.core.manager.BluetoothLeProfile.LeServiceListener;
import com.huami.ble.core.util.HmBleUtils;
import com.huami.ble.core.util.Logger;

public class HmBlecoreManager implements BluetoothLeProfile {
    private static HmBlecoreManager mInstance;
    private final IBluetoothStateChangeCallback mBluetoothStateChangeCallback = new C03771();
    private final ServiceConnection mConnection = new C03782();
    private Context mContext;
    private IBleCoreService mService;
    private LeServiceListener mServiceListener;

    class C03771 implements IBluetoothStateChangeCallback {
        C03771() {
        }

        public void onBluetoothStateChange(boolean up) {
            Logger.m0D("HmBlecoreManager", "onBluetoothStateChange: up=" + up);
            if (up) {
                synchronized (HmBlecoreManager.this.mConnection) {
                    try {
                        if (HmBlecoreManager.this.mService == null) {
                            Logger.m0D("HmBlecoreManager", "Binding service...");
                            HmBlecoreManager.this.doBind();
                        }
                    } catch (Exception re) {
                        Logger.m1E("HmBlecoreManager", re.toString());
                    }
                }
                return;
            }
            Logger.m0D("HmBlecoreManager", "Unbinding service...");
            synchronized (HmBlecoreManager.this.mConnection) {
                try {
                    HmBlecoreManager.this.mService = null;
                    HmBlecoreManager.this.mContext.unbindService(HmBlecoreManager.this.mConnection);
                } catch (Exception re2) {
                    Logger.m1E("HmBlecoreManager", re2.toString());
                }
            }
        }
    }

    class C03782 implements ServiceConnection {
        C03782() {
        }

        public void onServiceConnected(ComponentName className, IBinder service) {
            Logger.m0D("HmBlecoreManager", "Proxy object connected mServiceListener = " + HmBlecoreManager.this.mServiceListener);
            HmBlecoreManager.this.mService = Stub.asInterface(service);
            if (HmBlecoreManager.this.mServiceListener != null) {
                Logger.m0D("HmBlecoreManager", " mInstance = " + HmBlecoreManager.mInstance + "   HmBlecoreManager.this = " + HmBlecoreManager.this);
                HmBlecoreManager.this.mServiceListener.onServiceConnected(0, HmBlecoreManager.this);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            Logger.m0D("HmBlecoreManager", "Proxy object disconnected mServiceListener = " + HmBlecoreManager.this.mServiceListener);
            HmBlecoreManager.this.mService = null;
            if (HmBlecoreManager.this.mServiceListener != null) {
                HmBlecoreManager.this.mServiceListener.onServiceDisconnected(0);
            }
        }
    }

    static synchronized HmBlecoreManager getInstance(Context context, LeServiceListener l) {
        HmBlecoreManager hmBlecoreManager;
        synchronized (HmBlecoreManager.class) {
            if (mInstance == null) {
                mInstance = new HmBlecoreManager(context, l);
            }
            hmBlecoreManager = mInstance;
        }
        return hmBlecoreManager;
    }

    private HmBlecoreManager(Context context, LeServiceListener l) {
        this.mContext = context;
        this.mServiceListener = l;
        BTStateChangeMonitor.getInstance(this.mContext).registerCallback(this.mBluetoothStateChangeCallback);
        doBind();
    }

    boolean doBind() {
        if (HmBleUtils.checkAdapterStateOn()) {
            Logger.m0D("HmBlecoreManager", "doBind");
            Intent intent = new Intent(IBleCoreService.class.getName());
            intent.setPackage("com.huami.ble.core");
            intent.putExtra("pkgName", this.mContext.getPackageName());
            if (this.mContext.bindService(intent, this.mConnection, 1)) {
                return true;
            }
            Logger.m1E("HmBlecoreManager", "Could not bind to IBleCoreService with " + intent);
            return false;
        }
        Logger.m2W("HmBlecoreManager", "BT Adapter is not turned ON, not bind");
        return false;
    }

    public void finalize() {
        cleanup();
    }

    public void cleanup() {
        synchronized (this.mConnection) {
            if (this.mService != null) {
                try {
                    this.mService = null;
                    this.mContext.unbindService(this.mConnection);
                } catch (Exception re) {
                    Logger.m1E("HmBlecoreManager", re.toString());
                }
            }
        }
        this.mServiceListener = null;
        BTStateChangeMonitor.getInstance(this.mContext).registerCallback(this.mBluetoothStateChangeCallback);
        mInstance = null;
    }

    public boolean sportReady(int sportType) {
        boolean z = false;
        if (this.mService != null && HmBleUtils.checkAdapterStateOn()) {
            try {
                z = this.mService.sportReady(sportType);
            } catch (RemoteException e) {
                Logger.m1E("HmBlecoreManager", e.toString());
            }
        }
        return z;
    }

    public boolean sportUnready(int sportType) {
        boolean z = false;
        if (this.mService != null && HmBleUtils.checkAdapterStateOn()) {
            try {
                z = this.mService.sportUnready(sportType);
            } catch (RemoteException e) {
                Logger.m1E("HmBlecoreManager", e.toString());
            }
        }
        return z;
    }

    public int getProfileConnectionState(int profile) {
        int state = 0;
        if (this.mService != null && HmBleUtils.checkAdapterStateOn()) {
            try {
                state = this.mService.getProfileConnectionState(profile);
            } catch (RemoteException e) {
                Logger.m1E("HmBlecoreManager", e.toString());
                return state;
            }
        }
        return state;
    }
}
