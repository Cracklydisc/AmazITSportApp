package com.huami.ble.core.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.huami.ble.core.util.Logger;
import java.util.ArrayList;
import java.util.Collection;

public class BTStateChangeMonitor {
    public static BTStateChangeMonitor mInstance;
    private final Collection<IBluetoothStateChangeCallback> mBTStateChangeCallbacks = new ArrayList();
    private Context mContext;
    private BroadcastReceiver mStateChange = new C03761();

    class C03761 extends BroadcastReceiver {
        C03761() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
                int newState = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                int prevState = intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", Integer.MIN_VALUE);
                Logger.m0D("BTStateChangeMonitor", "Bluetooth state change new state = " + newState);
                BTStateChangeMonitor.this.bluetoothStateChangeHandler(prevState, newState);
            }
        }
    }

    public static BTStateChangeMonitor getInstance(Context cxt) {
        if (mInstance == null) {
            mInstance = new BTStateChangeMonitor(cxt);
        }
        return mInstance;
    }

    private BTStateChangeMonitor(Context cxt) {
        this.mContext = cxt;
        this.mContext.registerReceiver(this.mStateChange, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    public void registerCallback(IBluetoothStateChangeCallback callback) {
        synchronized (this.mBTStateChangeCallbacks) {
            this.mBTStateChangeCallbacks.add(callback);
        }
    }

    private void bluetoothStateChangeHandler(int prevState, int newState) {
        if (prevState == newState) {
            return;
        }
        if (newState == 12 || newState == 10) {
            sendBluetoothStateCallback(newState == 12);
        }
    }

    private void sendBluetoothStateCallback(boolean isUp) {
        Logger.m0D("BTStateChangeMonitor", "Broadcasting onBluetoothStateChange(" + isUp + ") to " + this.mBTStateChangeCallbacks.size() + " receivers.");
        for (IBluetoothStateChangeCallback callback : this.mBTStateChangeCallbacks) {
            callback.onBluetoothStateChange(isUp);
        }
    }
}
