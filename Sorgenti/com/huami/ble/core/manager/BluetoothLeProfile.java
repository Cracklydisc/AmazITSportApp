package com.huami.ble.core.manager;

public interface BluetoothLeProfile {

    public interface LeServiceListener {
        void onServiceConnected(int i, BluetoothLeProfile bluetoothLeProfile);

        void onServiceDisconnected(int i);
    }

    void cleanup();
}
