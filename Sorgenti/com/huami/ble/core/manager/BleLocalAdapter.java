package com.huami.ble.core.manager;

import android.content.Context;
import com.huami.ble.core.manager.BluetoothLeProfile.LeServiceListener;

public class BleLocalAdapter {
    private static BleLocalAdapter mInstance;

    public static BleLocalAdapter getAdapter() {
        if (mInstance == null) {
            mInstance = new BleLocalAdapter();
        }
        return mInstance;
    }

    private BleLocalAdapter() {
    }

    public boolean getManager(Context context, LeServiceListener listener, int id) {
        if (id == 0) {
            HmBlecoreManager instance = HmBlecoreManager.getInstance(context, listener);
            return true;
        } else if (id != 1) {
            return false;
        } else {
            HmHeartRateManager hrManager = HmHeartRateManager.getInstance(context, listener);
            return true;
        }
    }
}
