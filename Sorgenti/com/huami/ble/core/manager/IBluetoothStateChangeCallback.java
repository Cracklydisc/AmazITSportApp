package com.huami.ble.core.manager;

public interface IBluetoothStateChangeCallback {
    void onBluetoothStateChange(boolean z);
}
