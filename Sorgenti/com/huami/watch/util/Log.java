package com.huami.watch.util;

import android.os.Process;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Log {
    private static final Printer filePrinter = new FileLoggerPrinter(settings);
    private static final Printer printer = new LoggerPrinter(settings);
    private static final Settings settings = new Settings();

    public interface Printer {
        void mo1652d(String str, String str2, Object... objArr);

        void mo1653e(String str, String str2, Object... objArr);

        void mo1654e(String str, Throwable th, String str2, Object... objArr);

        void mo1655i(String str, String str2, Object... objArr);

        void mo1656v(String str, String str2, Object... objArr);

        void mo1657w(String str, String str2, Object... objArr);

        void mo1658w(String str, Throwable th, String str2, Object... objArr);
    }

    private static class LoggerPrinter implements Printer {
        private static final ThreadLocal<Integer> LOCAL_METHOD_COUNT = new ThreadLocal();
        private static final ThreadLocal<String> LOCAL_TAG = new ThreadLocal();
        private static String TAG = "LOGGER";
        protected Settings settings = new Settings();

        public LoggerPrinter(Settings settings) {
            this.settings = settings;
        }

        public void mo1652d(String tag, String message, Object... args) {
            log(3, tag, message, args);
        }

        public void mo1653e(String tag, String message, Object... args) {
            mo1654e(tag, null, message, args);
        }

        public void mo1654e(String tag, Throwable throwable, String message, Object... args) {
            logThrowable(6, throwable, tag, message, args);
        }

        private void logThrowable(int logType, Throwable throwable, String messageTag, String message, Object[] args) {
            if (!(throwable == null || message == null)) {
                message = message + '\n' + android.util.Log.getStackTraceString(throwable);
            }
            if (throwable != null && message == null) {
                message = android.util.Log.getStackTraceString(throwable);
            }
            if (message == null) {
                message = "No message/exception is set";
            }
            log(logType, messageTag, message, args);
        }

        public void mo1657w(String tag, String message, Object... args) {
            mo1658w(tag, null, message, args);
        }

        public void mo1658w(String tag, Throwable throwable, String message, Object... args) {
            logThrowable(5, throwable, tag, message, args);
        }

        public void mo1655i(String tag, String message, Object... args) {
            log(4, tag, message, args);
        }

        public void mo1656v(String tag, String message, Object... args) {
            log(2, tag, message, args);
        }

        protected boolean isLogNeed() {
            return this.settings.isLogToConsole();
        }

        protected boolean isLogSlim() {
            return false;
        }

        private synchronized void log(int logType, String messageTag, String msg, Object... args) {
            if (isLogNeed()) {
                String tag = getTag();
                String message = createMessage(msg, args);
                int methodCount = getMethodCount();
                logTopBorder(logType, tag);
                logHeaderContent(logType, tag, methodCount);
                byte[] bytes = message.getBytes();
                int length = bytes.length;
                String formattedMessageTag = "[" + messageTag + "] ";
                if (length <= 4000) {
                    if (methodCount > 0 && !isLogSlim()) {
                        logDivider(logType, tag);
                    }
                    logContent(logType, tag, formattedMessageTag + message);
                } else {
                    if (methodCount > 0) {
                        if (!isLogSlim()) {
                            logDivider(logType, tag);
                        }
                    }
                    for (int i = 0; i < length; i += 4000) {
                        logContent(logType, tag, formattedMessageTag + new String(bytes, i, Math.min(length - i, 4000)));
                    }
                    logBottomBorder(logType, tag);
                }
            }
        }

        private void logTopBorder(int logType, String tag) {
            logChunk(logType, tag, "────────────────────────────────────────────");
        }

        private void logHeaderContent(int logType, String tag, int methodCount) {
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            String level = "";
            int stackOffset = getStackOffset(trace) + this.settings.getMethodOffset();
            if (methodCount + stackOffset > trace.length) {
                methodCount = (trace.length - stackOffset) - 1;
            }
            StringBuilder builder = new StringBuilder();
            int i = methodCount;
            while (i > 0) {
                int stackIndex = i + stackOffset;
                if (stackIndex < trace.length) {
                    if (this.settings.isShowThreadInfo() && i == methodCount) {
                        builder.append("(").append(Thread.currentThread().getName()).append(") ");
                    }
                    builder.append(level).append(getSimpleClassName(trace[stackIndex].getClassName())).append(".").append(trace[stackIndex].getMethodName()).append("(");
                    builder.append(trace[stackIndex].getLineNumber()).append(")");
                    level = " -> ";
                }
                i--;
            }
            logChunk(logType, tag, builder.toString());
        }

        private void logBottomBorder(int logType, String tag) {
        }

        private void logDivider(int logType, String tag) {
            logChunk(logType, tag, " ───────────────────────────────────────────");
        }

        private void logContent(int logType, String tag, String chunk) {
            for (String line : chunk.split(System.getProperty("line.separator"))) {
                logChunk(logType, tag, line);
            }
        }

        protected void logChunk(int logType, String tag, String chunk) {
            String finalTag = tag;
            switch (logType) {
                case 2:
                    android.util.Log.v(finalTag, chunk);
                    return;
                case 4:
                    android.util.Log.i(finalTag, chunk);
                    return;
                case 5:
                    android.util.Log.w(finalTag, chunk);
                    return;
                case 6:
                    android.util.Log.e(finalTag, chunk);
                    return;
                case 7:
                    android.util.Log.wtf(finalTag, chunk);
                    return;
                default:
                    android.util.Log.d(finalTag, chunk);
                    return;
            }
        }

        private String getSimpleClassName(String name) {
            return name.substring(name.lastIndexOf(".") + 1);
        }

        private String getTag() {
            String tag = (String) LOCAL_TAG.get();
            if (tag == null) {
                return TAG;
            }
            LOCAL_TAG.remove();
            return tag;
        }

        private String createMessage(String message, Object... args) {
            return args.length == 0 ? message : String.format(message, args);
        }

        private int getMethodCount() {
            Integer count = (Integer) LOCAL_METHOD_COUNT.get();
            int result = this.settings.getMethodCount();
            if (count != null) {
                LOCAL_METHOD_COUNT.remove();
                result = count.intValue();
            }
            if (result >= 0) {
                return result;
            }
            throw new IllegalStateException("methodCount cannot be negative");
        }

        private int getStackOffset(StackTraceElement[] trace) {
            for (int i = 3; i < trace.length; i++) {
                String name = trace[i].getClassName();
                if (!name.equals(LoggerPrinter.class.getName()) && !name.equals(Log.class.getName())) {
                    return i - 1;
                }
            }
            return -1;
        }
    }

    private static class FileLoggerPrinter extends LoggerPrinter {
        public FileLoggerPrinter(Settings settings) {
            super(settings);
        }

        protected boolean isLogNeed() {
            return this.settings.isLogToFile();
        }

        protected boolean isLogSlim() {
            return true;
        }

        protected void logChunk(int logType, String tag, String chunk) {
            File logFile = this.settings.getLogFile();
            if (logFile != null) {
                String logLevel;
                if (logFile.exists() && logFile.length() > 16777216) {
                    logFile.delete();
                }
                if (!logFile.exists()) {
                    try {
                        logFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                switch (logType) {
                    case 2:
                        logLevel = "V";
                        break;
                    case 4:
                        logLevel = "I";
                        break;
                    case 5:
                        logLevel = "W";
                        break;
                    case 6:
                        logLevel = "E";
                        break;
                    case 7:
                        logLevel = "A";
                        break;
                    default:
                        logLevel = "D";
                        break;
                }
                try {
                    BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                    buf.append(time()).append(" ").append(logLevel).append("(").append(String.valueOf(Process.myPid())).append(") ").append(chunk);
                    buf.newLine();
                    buf.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }

        private static String time() {
            return new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS", Locale.US).format(new Date());
        }
    }

    public enum LogLevel {
        NONE,
        CONSOLE_ONLY,
        FILE_ONLY,
        FULL
    }

    public static class Settings {
        private File logFile;
        private LogLevel logLevel;
        private int methodCount = 2;
        private int methodOffset = 0;
        private boolean showThreadInfo = true;

        public Settings() {
            this.logLevel = Config.isDebug() ? LogLevel.CONSOLE_ONLY : LogLevel.NONE;
        }

        public boolean isShowThreadInfo() {
            return this.showThreadInfo;
        }

        public int getMethodCount() {
            return this.methodCount;
        }

        public int getMethodOffset() {
            return this.methodOffset;
        }

        public File getLogFile() {
            return this.logFile;
        }

        public boolean isLogToFile() {
            return this.logLevel == LogLevel.FILE_ONLY || this.logLevel == LogLevel.FULL;
        }

        public boolean isLogToConsole() {
            return this.logLevel == LogLevel.CONSOLE_ONLY || this.logLevel == LogLevel.FULL;
        }
    }

    private Log() {
    }

    public static void m26d(String tag, String message, Object... args) {
        printer.mo1652d(tag, message, args);
        filePrinter.mo1652d(tag, message, args);
    }

    public static void m28e(String tag, String message, Object... args) {
        printer.mo1653e(tag, message, args);
        filePrinter.mo1653e(tag, message, args);
    }

    public static void m27e(String tag, String message, Throwable throwable, Object... args) {
        printer.mo1654e(tag, throwable, message, args);
        filePrinter.mo1654e(tag, throwable, message, args);
    }

    public static void m29i(String tag, String message, Object... args) {
        printer.mo1655i(tag, message, args);
        filePrinter.mo1655i(tag, message, args);
    }

    public static void m30v(String tag, String message, Object... args) {
        printer.mo1656v(tag, message, args);
    }

    public static void m32w(String tag, String message, Object... args) {
        printer.mo1657w(tag, message, args);
        filePrinter.mo1657w(tag, message, args);
    }

    public static void m31w(String tag, String message, Throwable throwable, Object... args) {
        printer.mo1658w(tag, throwable, message, args);
        filePrinter.mo1658w(tag, throwable, message, args);
    }
}
