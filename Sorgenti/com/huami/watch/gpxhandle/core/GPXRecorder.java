package com.huami.watch.gpxhandle.core;

import android.location.Location;
import android.text.TextUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class GPXRecorder {
    private static GPXRecorder ourInstance;

    public static GPXRecorder getInstance() {
        if (ourInstance == null) {
            synchronized (GPX.class) {
                if (ourInstance == null) {
                    ourInstance = new GPXRecorder();
                }
            }
        }
        return ourInstance;
    }

    public File saveToFile(GPX gpxObj) {
        IOException e;
        Throwable th;
        File dir = new File(Constants.OUTPUT_PATH);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File f = new File(Constants.OUTPUT_PATH + File.separator + gpxObj.getFileName());
        if (f.exists()) {
            f.delete();
        }
        FileOutputStream foo = null;
        PrintWriter printWriter = null;
        try {
            f.createNewFile();
            FileOutputStream foo2 = new FileOutputStream(f);
            try {
                PrintWriter printWriter2 = new PrintWriter(foo2);
                try {
                    printWriter2.append(theHeader());
                    printWriter2.append('\n');
                    printWriter2.append(theMetaData(gpxObj));
                    printWriter2.append('\n').flush();
                    printWriter2.append(ponitStart());
                    printWriter2.append('\n').flush();
                    while (gpxObj.hasMorePoints()) {
                        printWriter2.append(formatPoint(gpxObj, gpxObj.nextPoint()));
                        printWriter2.append('\n');
                    }
                    printWriter2.append(ponitEnd()).append('\n').flush();
                    printWriter2.append(theFooter());
                    printWriter2.flush();
                    if (foo2 != null) {
                        try {
                            foo2.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (printWriter2 != null) {
                        printWriter2.close();
                        printWriter = printWriter2;
                        foo = foo2;
                    } else {
                        foo = foo2;
                    }
                } catch (IOException e3) {
                    e2 = e3;
                    printWriter = printWriter2;
                    foo = foo2;
                    try {
                        e2.printStackTrace();
                        if (foo != null) {
                            try {
                                foo.close();
                            } catch (IOException e22) {
                                e22.printStackTrace();
                            }
                        }
                        if (printWriter != null) {
                            printWriter.close();
                        }
                        return f;
                    } catch (Throwable th2) {
                        th = th2;
                        if (foo != null) {
                            try {
                                foo.close();
                            } catch (IOException e222) {
                                e222.printStackTrace();
                            }
                        }
                        if (printWriter != null) {
                            printWriter.close();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    printWriter = printWriter2;
                    foo = foo2;
                    if (foo != null) {
                        foo.close();
                    }
                    if (printWriter != null) {
                        printWriter.close();
                    }
                    throw th;
                }
            } catch (IOException e4) {
                e222 = e4;
                foo = foo2;
                e222.printStackTrace();
                if (foo != null) {
                    foo.close();
                }
                if (printWriter != null) {
                    printWriter.close();
                }
                return f;
            } catch (Throwable th4) {
                th = th4;
                foo = foo2;
                if (foo != null) {
                    foo.close();
                }
                if (printWriter != null) {
                    printWriter.close();
                }
                throw th;
            }
        } catch (IOException e5) {
            e222 = e5;
            e222.printStackTrace();
            if (foo != null) {
                foo.close();
            }
            if (printWriter != null) {
                printWriter.close();
            }
            return f;
        }
        return f;
    }

    private CharSequence formatPoint(GPX gpxObj, Location location) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<trkpt lat=\"").append(location.getLatitude()).append("\" lon=\"").append(location.getLongitude()).append("\">");
        stringBuilder.append(filterAltitude(location.getAltitude())).append("\n<time>").append(getFormatedTime(gpxObj, location.getTime())).append("</time>").append("\n<extensions>").append("\n<heartrate>" + location.getExtras().getInt("heartrate") + "</heartrate>\n").append("<ns3:TrackPointExtension>").append("<ns3:hr>").append(location.getExtras().getInt("heartrate")).append("</ns3:hr>").append("</ns3:TrackPointExtension>").append("\n</extensions>").append("</trkpt>\n");
        return stringBuilder.toString();
    }

    private CharSequence filterAltitude(double altitude) {
        if (Double.compare(altitude, -20000.0d) == 0) {
            return "";
        }
        return new StringBuffer().append("\n<ele>").append(altitude).append("</ele>").toString();
    }

    private String ponitStart() {
        return "<trk>\n" + theExtraHeaderDes() + "\n<trkseg>";
    }

    private String theExtraHeaderDes() {
        return "<name>Sport</name>\n<number>0</number>\n<type>Running</type>";
    }

    private String ponitEnd() {
        return "</trkseg>\n</trk>";
    }

    private String theHeader() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n<gpx xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/11.xsd\"\nxmlns=\"http://www.topografix.com/GPX/1/1\"\nxmlns:ns3=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"\nxmlns:ns2=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" \nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:ns1=\"http://www.cluetrust.com/XML/GPXDATA/1/0\" \ncreator=\"Huami Amazfit Sports Watch\" version=\"1.1\">";
    }

    private String theFooter() {
        return "</gpx>";
    }

    private String getFormatedTime(GPX gpxObj, long time) {
        return GPX.sFormat.format(Long.valueOf(gpxObj.getSportTimeUTC() + time)).trim();
    }

    private String theMetaData(GPX gpxObj) {
        if (gpxObj == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder("<metadata>\n");
        stringBuilder.append("<name>").append(gpxObj.getAuthor()).append("</name>\n");
        stringBuilder.append("<author>\n").append("<name>").append(gpxObj.getAuthor()).append("</name>").append("\n</author>");
        stringBuilder.append("<time>").append(gpxObj.getSportTime()).append("</time>\n");
        if (TextUtils.isEmpty(gpxObj.getDescription())) {
            stringBuilder.append("\n<desc>\n").append(gpxObj.getDescription()).append("\n</desc>");
        }
        stringBuilder.append("\n</metadata>");
        return stringBuilder.toString();
    }
}
