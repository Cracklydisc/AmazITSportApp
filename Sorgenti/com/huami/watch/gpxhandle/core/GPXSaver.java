package com.huami.watch.gpxhandle.core;

import android.content.Context;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.gpxhandle.core.GPX.GPXBuilder;
import java.io.File;

public class GPXSaver {
    private Context context;
    private String fileName;
    private boolean makeCancel;
    private MediaScannerConnection mediaScannerConnection;
    private GPXSaverCallbacks theListener;
    private TaskManager theSaver;
    private String trackId;

    public interface GPXSaverCallbacks extends IGPXPointOffer {
        Object onFetchData();

        long onGetSportTime();

        void onPostHandleData();

        void onPreHandleData();

        void onSaveFinish(String str, File file, int i);
    }

    public GPXSaver(Context context, String trackId, String fileName, GPXSaverCallbacks theListener) {
        this.trackId = trackId;
        this.fileName = fileName;
        this.context = context.getApplicationContext();
        this.theSaver = new TaskManager("save-gpx-" + trackId);
        this.theListener = theListener;
    }

    public synchronized void startSave() {
        this.theSaver.next(trackData()).next(handleData()).next(notifyListener()).execute(new TaskOperation());
    }

    private Task trackData() {
        return new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                Object obj = null;
                try {
                    if (GPXSaver.this.theListener != null) {
                        obj = GPXSaver.this.theListener.onFetchData();
                    }
                    taskOperation.appendTaskParam(obj);
                } catch (Exception e) {
                    e.printStackTrace();
                    taskOperation.appendTaskParam(null);
                }
                return taskOperation;
            }
        };
    }

    private Task handleData() {
        return new Task(RunningStatus.WORK_THREAD) {

            class C05071 implements IGPXPointOffer {
                C05071() {
                }

                public boolean hasMorePoints() {
                    if (GPXSaver.this.makeCancel) {
                        return false;
                    }
                    return GPXSaver.this.theListener.hasMorePoints();
                }

                public Location nextPoint() {
                    return GPXSaver.this.theListener.nextPoint();
                }
            }

            public TaskOperation onExecute(TaskOperation taskOperation) {
                Object c = taskOperation.getTaskParams()[0];
                GPXSaver.this.theListener.onPreHandleData();
                if (c == null) {
                    taskOperation.setTaskParamsEmpty();
                    taskOperation.appendTaskParam(Integer.valueOf(1));
                    taskOperation.appendTaskParam(null);
                    return null;
                }
                GPXSaver.this.theListener.onPreHandleData();
                File f = GPXRecorder.getInstance().saveToFile(new GPXBuilder(GPXSaver.this.fileName).pointOffer(new C05071()).description("Amazfit Watch").author("Amazfit Sport Record").sportTimeUTC(GPXSaver.this.theListener.onGetSportTime()).build());
                GPXSaver.this.theListener.onPostHandleData();
                taskOperation.setTaskParamsEmpty();
                if (GPXSaver.this.makeCancel) {
                    taskOperation.appendTaskParam(Integer.valueOf(-1));
                    taskOperation.appendTaskParam(f);
                    return taskOperation;
                }
                taskOperation.appendTaskParam(Integer.valueOf(0));
                taskOperation.appendTaskParam(f);
                return taskOperation;
            }
        };
    }

    private Task notifyListener() {
        return new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                Integer resultCode = Integer.valueOf(1);
                File f = null;
                try {
                    Object[] results = taskOperation.getTaskParams();
                    resultCode = (Integer) results[0];
                    f = (File) results[1];
                } catch (Exception e) {
                    e.printStackTrace();
                }
                final File fs = f;
                try {
                    GPXSaver.this.mediaScannerConnection = new MediaScannerConnection(GPXSaver.this.context, new MediaScannerConnectionClient() {
                        public void onMediaScannerConnected() {
                            if (GPXSaver.this.mediaScannerConnection != null && fs != null) {
                                GPXSaver.this.mediaScannerConnection.scanFile(fs.getAbsolutePath(), "text/plain");
                            }
                        }

                        public void onScanCompleted(String path, Uri uri) {
                            if (GPXSaver.this.mediaScannerConnection != null) {
                                GPXSaver.this.mediaScannerConnection.disconnect();
                            }
                        }
                    });
                    GPXSaver.this.mediaScannerConnection.connect();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                if (GPXSaver.this.theListener != null) {
                    GPXSaver.this.theListener.onSaveFinish(GPXSaver.this.trackId, f, resultCode.intValue());
                }
                if (GPXSaver.this.makeCancel && f != null && f.exists()) {
                    f.delete();
                }
                return null;
            }
        };
    }

    public synchronized void cancelSave() {
        this.makeCancel = true;
    }
}
