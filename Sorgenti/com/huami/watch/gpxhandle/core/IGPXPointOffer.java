package com.huami.watch.gpxhandle.core;

import android.location.Location;

public interface IGPXPointOffer {
    boolean hasMorePoints();

    Location nextPoint();
}
