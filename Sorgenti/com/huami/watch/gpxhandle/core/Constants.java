package com.huami.watch.gpxhandle.core;

import android.os.Environment;
import java.io.File;

public class Constants {
    public static final String OUTPUT_PATH = (Environment.getExternalStorageDirectory() + File.separator + "gpxdata");
}
