package com.huami.watch.gpxhandle.core;

import android.location.Location;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GPX implements IGPXPointOffer {
    public static SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    public static SimpleDateFormat sFormat2 = new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss");
    private final String author;
    private final String description;
    private final String fileNameToSave;
    private final IGPXPointOffer pointOffer;
    private String sportTime;
    private long sportTimeUTC;

    public static class GPXBuilder {
        private String author;
        private String description;
        private String fileName;
        private IGPXPointOffer pointOffer;
        private long sportTimeUTC;

        public GPXBuilder(String filename) {
            this.fileName = filename;
        }

        public GPXBuilder description(String description) {
            this.description = description;
            return this;
        }

        public GPXBuilder author(String author) {
            this.author = author;
            return this;
        }

        public GPXBuilder sportTimeUTC(long time) {
            this.sportTimeUTC = time;
            return this;
        }

        public GPXBuilder pointOffer(IGPXPointOffer pointOffer) {
            this.pointOffer = pointOffer;
            return this;
        }

        public GPX build() {
            return new GPX();
        }
    }

    private GPX(GPXBuilder gpxBuilder) {
        this.description = gpxBuilder.description;
        this.author = gpxBuilder.author;
        this.pointOffer = gpxBuilder.pointOffer;
        SimpleDateFormat simpleDateFormat = sFormat;
        long access$300 = gpxBuilder.sportTimeUTC;
        this.sportTimeUTC = access$300;
        this.sportTime = simpleDateFormat.format(Long.valueOf(access$300)).trim();
        this.fileNameToSave = sFormat2.format(Long.valueOf(Calendar.getInstance().getTimeInMillis())) + "" + gpxBuilder.fileName + ".gpx";
    }

    public String getFileName() {
        return this.fileNameToSave;
    }

    public String getDescription() {
        return this.description;
    }

    public String getAuthor() {
        return this.author;
    }

    public boolean hasMorePoints() {
        return this.pointOffer == null ? false : this.pointOffer.hasMorePoints();
    }

    public Location nextPoint() {
        return this.pointOffer.nextPoint();
    }

    public String getSportTime() {
        return this.sportTime;
    }

    public long getSportTimeUTC() {
        return this.sportTimeUTC;
    }
}
