package com.huami.watch.ui;

import com.huami.watch.newsport.C0532R;

public final class C1070R {

    public static final class anim {
    }

    public static final class animator {
        public static final int time_picker_ok_vector_alpha = 2131099649;
        public static final int time_picker_ok_vector_path = 2131099650;
        public static final int time_picker_ok_vector_scalex = 2131099651;
        public static final int time_picker_ok_vector_scaley = 2131099652;
    }

    public static final class array {
    }

    public static final class attr {
    }

    public static final class color {
        public static final int altitude_end_color = 2131296460;
        public static final int altitude_start_color = 2131296459;
        public static final int average_line_color = 2131296369;
        public static final int average_text_color = 2131296370;
        public static final int bench_green_color = 2131296366;
        public static final int bench_red_color = 2131296368;
        public static final int bench_yellow_color = 2131296367;
        public static final int black = 2131296372;
        public static final int color_chart_benchmark_line = 2131296456;
        public static final int color_green = 2131296450;
        public static final int color_red = 2131296451;
        public static final int color_shadow = 2131296455;
        public static final int color_xaxis = 2131296452;
        public static final int color_xaxis_label = 2131296453;
        public static final int color_xaxis_start = 2131296454;
        public static final int cw_page_indicator = 2131296448;
        public static final int cw_page_indicator_focused = 2131296449;
        public static final int date_color = 2131296382;
        public static final int main_text_color = 2131296466;
        public static final int white = 2131296371;
    }

    public static final class dimen {
        public static final int average_hundred_text_padding_left = 2131361995;
        public static final int average_line_width = 2131361992;
        public static final int average_text_padding_left = 2131361994;
        public static final int average_text_size = 2131361993;
        public static final int blt_info_margin_top = 2131362026;
        public static final int common_txt_size = 2131361795;
        public static final int indicator_center_distance = 2131361999;
        public static final int indicator_radius = 2131361997;
        public static final int indicator_radius_focused = 2131361998;
        public static final int menu_item_layout_margin_top = 2131362011;
        public static final int menu_touch_area = 2131362000;
        public static final int min_max_margin = 2131361996;
        public static final int picker_default_nagetive_text_size = 2131362012;
        public static final int pickview_unit_size = 2131362020;
        public static final int setting_icon_txt_margin = 2131361800;
        public static final int settings_icon_rx = 2131361792;
        public static final int settings_item_height = 2131361801;
        public static final int size_linechart_line_width = 2131362006;
        public static final int size_linechart_minmax_font = 2131362007;
        public static final int size_linechart_no_yaxis_padding_left = 2131362019;
        public static final int size_linechart_point_padding_left = 2131362018;
        public static final int size_linechart_width = 2131362017;
        public static final int size_linechart_yoffset = 2131362010;
        public static final int size_shadow_dy1 = 2131362001;
        public static final int size_shadow_radius1 = 2131362002;
        public static final int size_xaxis_label_font = 2131362004;
        public static final int smaller_txt_size = 2131361798;
    }

    public static final class drawable {
        public static final int health_all_heart_date_line = 2130837720;
        public static final int sport_heart_max = 2130837942;
        public static final int sport_heart_min = 2130837943;
        public static final int system_dialog_button_bg_negative = 2130838241;
        public static final int system_dialog_button_bg_positive = 2130838242;
        public static final int time_picker_ok_vector = 2130838244;
    }

    public static final class id {
        public static final int anchor_view = 2131624329;
        public static final int button_empty = 2131624358;
        public static final int content = 2131624095;
        public static final int footer = 2131624018;
        public static final int message = 2131624017;
        public static final int negativeButton = 2131624359;
        public static final int picker1 = 2131624362;
        public static final int picker2 = 2131624363;
        public static final int picker_ampm = 2131624364;
        public static final int picker_listview = 2131624330;
        public static final int picker_unit = 2131624331;
        public static final int positiveButton = 2131624063;
        public static final int title = 2131624015;
        public static final int title_empty = 2131624016;
    }

    public static final class integer {
    }

    public static final class interpolator {
    }

    public static final class layout {
        public static final int picker_column_with_title = 2130968689;
        public static final int system_dialog = 2130968703;
        public static final int time_select1 = 2130968707;
        public static final int time_select2 = 2130968708;
        public static final int time_select3 = 2130968709;
    }

    public static final class mipmap {
    }

    public static final class string {
        public static final int average_text = 2131231603;
        public static final int menu_item_high_light_scale_factor = 2131231604;
        public static final int no_heartrate_data_hint = 2131230772;
        public static final int picker_default_nagetive_text = 2131231606;
        public static final int title_settings = 2131231608;
    }

    public static final class style {
        public static final int HmDialog = 2131492920;
        public static final int PromptDialog = 2131492915;
    }

    public static final class styleable {
        public static final int[] BoxInsetLayout_Layout = new int[]{C0532R.attr.hm_layout_box, C0532R.attr.layout_box};
        public static final int[] CircledImageView = new int[]{16843033, C0532R.attr.circle_color, C0532R.attr.circle_radius, C0532R.attr.circle_radius_pressed, C0532R.attr.circle_border_width, C0532R.attr.circle_border_color, C0532R.attr.circle_padding, C0532R.attr.shadow_width};
        public static final int[] DelayedConfirmationView = new int[]{C0532R.attr.update_interval};
        public static final int[] DotsPageIndicator = new int[]{C0532R.attr.dotSpacing, C0532R.attr.dotRadius, C0532R.attr.dotRadiusSelected, C0532R.attr.dotColor, C0532R.attr.dotColorSelected, C0532R.attr.dotFadeWhenIdle, C0532R.attr.dotFadeOutDelay, C0532R.attr.dotFadeOutDuration, C0532R.attr.dotFadeInDuration, C0532R.attr.dotShadowColor, C0532R.attr.dotShadowRadius, C0532R.attr.dotShadowDx, C0532R.attr.dotShadowDy};
        public static final int[] HillViewData = new int[]{C0532R.attr.radius, C0532R.attr.rect_length, C0532R.attr.hill_even_color, C0532R.attr.hill_up_color, C0532R.attr.hill_down_color, C0532R.attr.hill_text_size};
        public static final int[] PageListView = new int[]{C0532R.attr.centerItemInPage, C0532R.attr.itemCountPerPage};
        public static final int[] RealDataDisplay = new int[]{C0532R.attr.curve_height, C0532R.attr.curve_color, C0532R.attr.text_color, C0532R.attr.textsize, C0532R.attr.curve_padding_bottom, C0532R.attr.curve_padding_top};
        public static final int[] RecyclerView = new int[]{16842948, C0532R.attr.layoutManager, C0532R.attr.spanCount, C0532R.attr.reverseLayout, C0532R.attr.stackFromEnd};
        public static final int[] WatchViewStub = new int[]{C0532R.attr.rectLayout, C0532R.attr.roundLayout};
        public static final int[] WearableHeaderTextView = new int[]{C0532R.attr.circular_layout_gravity, C0532R.attr.circular_text_size};
    }
}
