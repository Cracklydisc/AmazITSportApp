package com.huami.watch.watchmanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IHmWatchService extends IInterface {

    public static abstract class Stub extends Binder implements IHmWatchService {

        private static class Proxy implements IHmWatchService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void disableFeature(int featureId, boolean disable, IBinder user) throws RemoteException {
                int i = 1;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.watchmanager.IHmWatchService");
                    _data.writeInt(featureId);
                    if (!disable) {
                        i = 0;
                    }
                    _data.writeInt(i);
                    _data.writeStrongBinder(user);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean isFeatureDisabled(int featureId) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.watchmanager.IHmWatchService");
                    _data.writeInt(featureId);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.watchmanager.IHmWatchService");
        }

        public static IHmWatchService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.watchmanager.IHmWatchService");
            if (iin == null || !(iin instanceof IHmWatchService)) {
                return new Proxy(obj);
            }
            return (IHmWatchService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int _arg1 = 0;
            switch (code) {
                case 1:
                    boolean _arg12;
                    data.enforceInterface("com.huami.watch.watchmanager.IHmWatchService");
                    int _arg0 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg12 = true;
                    }
                    disableFeature(_arg0, _arg12, data.readStrongBinder());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.watchmanager.IHmWatchService");
                    boolean _result = isFeatureDisabled(data.readInt());
                    reply.writeNoException();
                    if (_result) {
                        _arg1 = 1;
                    }
                    reply.writeInt(_arg1);
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.watchmanager.IHmWatchService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void disableFeature(int i, boolean z, IBinder iBinder) throws RemoteException;

    boolean isFeatureDisabled(int i) throws RemoteException;
}
