package com.huami.watch.watchmanager;

import android.content.Context;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.huami.watch.watchmanager.IHmWatchService.Stub;

public class HmWatchManager {
    private static HmWatchManager sInstance = null;
    private static IHmWatchService sService = null;
    private IBinder mClient = new Binder();
    private Context mContext;

    private IHmWatchService getService() throws RemoteException {
        if (sService != null) {
            return sService;
        }
        sService = Stub.asInterface(ServiceManager.getService("watch_manager"));
        if (sService != null) {
            return sService;
        }
        throw new RemoteException("Can't get Service:watch_manager");
    }

    public static HmWatchManager getHmWatchManager(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("context can't be null!");
        }
        if (sInstance == null) {
            synchronized (HmWatchManager.class) {
                if (sInstance == null) {
                    sInstance = new HmWatchManager(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    private HmWatchManager(Context context) {
        this.mContext = context;
    }

    public void disableFeature(int featureId, boolean disable) {
        Log.d("HmWatchManager", "configFeatureStatus featureId:" + featureId + " disable:" + disable);
        try {
            getService().disableFeature(featureId, disable, this.mClient);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
