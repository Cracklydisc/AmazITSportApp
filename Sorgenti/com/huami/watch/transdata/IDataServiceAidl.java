package com.huami.watch.transdata;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;

public interface IDataServiceAidl extends IInterface {

    public static abstract class Stub extends Binder implements IDataServiceAidl {

        private static class Proxy implements IDataServiceAidl {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void triggered(String pkg, Map params) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transdata.IDataServiceAidl");
                    _data.writeString(pkg);
                    _data.writeMap(params);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void registerListener(String pkg, IDataListener listener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transdata.IDataServiceAidl");
                    _data.writeString(pkg);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void unRegisterListener(String pkg) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transdata.IDataServiceAidl");
                    _data.writeString(pkg);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.transdata.IDataServiceAidl");
        }

        public static IDataServiceAidl asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.transdata.IDataServiceAidl");
            if (iin == null || !(iin instanceof IDataServiceAidl)) {
                return new Proxy(obj);
            }
            return (IDataServiceAidl) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.transdata.IDataServiceAidl");
                    triggered(data.readString(), data.readHashMap(getClass().getClassLoader()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.transdata.IDataServiceAidl");
                    registerListener(data.readString(), com.huami.watch.transdata.IDataListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.transdata.IDataServiceAidl");
                    unRegisterListener(data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.transdata.IDataServiceAidl");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void registerListener(String str, IDataListener iDataListener) throws RemoteException;

    void triggered(String str, Map map) throws RemoteException;

    void unRegisterListener(String str) throws RemoteException;
}
