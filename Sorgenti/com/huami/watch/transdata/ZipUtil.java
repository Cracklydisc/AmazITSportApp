package com.huami.watch.transdata;

import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;

public class ZipUtil {
    public static byte[] gZip(byte[] data) {
        byte[] b = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(bos);
            gzip.write(data);
            gzip.finish();
            gzip.close();
            b = bos.toByteArray();
            bos.close();
            return b;
        } catch (Exception ex) {
            ex.printStackTrace();
            return b;
        }
    }
}
