package com.huami.watch.transdata;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import com.huami.watch.transdata.IDataServiceAidl.Stub;
import com.huami.watch.transdata.transport.DataTransporter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataService extends Service {
    private DataBinder dataBinder = new DataBinder();
    private DataTransporter dataTransporter;

    public interface ClientListener {
        DataTrans getData(String str, String str2, String str3);

        void onDataResult(String str, String str2, String str3);
    }

    public class DataBinder extends Stub {
        private ConcurrentHashMap<String, IDataListener> listeners;

        public void triggered(String pkg, Map params) throws RemoteException {
            TLog.pro("triggered by " + pkg);
            sendTrigger(pkg, params);
        }

        public void registerListener(final String pkg, IDataListener listener) throws RemoteException {
            TLog.pro("registerListener by " + pkg);
            if (this.listeners == null) {
                this.listeners = new ConcurrentHashMap();
            }
            listener.asBinder().linkToDeath(new DeathRecipient() {
                public void binderDied() {
                    IDataListener binder = (IDataListener) DataBinder.this.listeners.remove(pkg);
                    if (!(binder == null || binder.asBinder() == null)) {
                        binder.asBinder().unlinkToDeath(this, 0);
                    }
                    TLog.pro("linkToDeath:IDataListener");
                }
            }, 0);
            this.listeners.put(pkg, listener);
            if (DataService.this.dataTransporter != null) {
                DataService.this.dataTransporter.registerNew(pkg);
            }
        }

        public void unRegisterListener(String pkg) throws RemoteException {
            if (this.listeners != null) {
                this.listeners.remove(pkg);
            }
        }

        public ConcurrentHashMap<String, IDataListener> getDataListener() {
            return this.listeners;
        }

        private void sendTrigger(String pkg, Map params) {
            if (DataService.this.dataTransporter != null) {
                DataService.this.dataTransporter.sendTrigger(pkg);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        TLog.pro("on bind ..");
        return this.dataBinder;
    }

    public void onCreate() {
        super.onCreate();
        TLog.pro("on create ..");
        if (this.dataTransporter == null) {
            this.dataTransporter = new DataTransporter(this);
        }
        sendConnectedStateToClient();
    }

    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        TLog.pro("on TaskRemoved ..");
    }

    public IDataListener getClientForPkg(String pkg) {
        if (this.dataBinder == null || this.dataBinder.getDataListener() == null) {
            return null;
        }
        return (IDataListener) this.dataBinder.getDataListener().get(pkg);
    }

    private void sendConnectedStateToClient() {
        sendBroadcast(new Intent("com.huami.watch.DATA_SERVICE_CONNECTED_ACTION"));
    }
}
