package com.huami.watch.transdata.transport;

import android.text.TextUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestData {
    private String dataType;
    private String pkgName;
    private JSONObject requestInfo;
    private String sessionId;
    private String subAction;

    public RequestData(HashMap headers, String data) {
        if (!TextUtils.isEmpty(data)) {
            parser(headers, data);
        }
    }

    public void parser(HashMap headers, String data) {
        try {
            JSONObject obj = new JSONObject(data);
            this.dataType = obj.optString("dataType");
            this.subAction = obj.optString("subAction");
            this.requestInfo = obj.optJSONObject("request");
            this.pkgName = DataTransHelper.translationPackage(this.dataType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (headers != null) {
            this.sessionId = (String) headers.get("sessionId");
        }
    }

    public String getDataType() {
        return this.dataType;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public String getRequestInfo() {
        return this.requestInfo == null ? "" : this.requestInfo.toString();
    }
}
