package com.huami.watch.transdata.transport;

import android.text.TextUtils;

public class DataTransHelper {
    public static String translationPackage(String dataType) {
        String pkg = "";
        if (TextUtils.equals(dataType, "SPORT") || TextUtils.equals(dataType, "FIRSTBEAT")) {
            pkg = "com.huami.watch.newsport";
        }
        if (TextUtils.equals(dataType, "HEALTH")) {
            return "com.huami.watch.health";
        }
        return pkg;
    }
}
