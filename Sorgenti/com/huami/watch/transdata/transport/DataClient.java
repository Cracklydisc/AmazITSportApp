package com.huami.watch.transdata.transport;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import com.huami.watch.transdata.DataService.ClientListener;
import com.huami.watch.transdata.DataTrans;
import com.huami.watch.transdata.IDataListener;
import com.huami.watch.transdata.IDataServiceAidl;
import com.huami.watch.transdata.IDataServiceAidl.Stub;
import com.huami.watch.transdata.TLog;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class DataClient {
    private static final String TAG = DataClient.class.getName();
    private AtomicBoolean isBind = new AtomicBoolean(false);
    private final IDataListener listener = new C09813();
    private Context mContext;
    private ClientListener mListener = null;
    private IDataServiceAidl mService = null;
    private BroadcastReceiver serverConnectionsStatusListener;
    private final ServiceConnection serviceConnection = new C09802();

    class C09791 extends BroadcastReceiver {
        C09791() {
        }

        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), "com.huami.watch.DATA_SERVICE_CONNECTED_ACTION")) {
                DataClient.this.init(DataClient.this.mContext);
            } else if (!TextUtils.equals(intent.getAction(), "com.huami.watch.DATA_SERVICE_CONNECTED_ACTION")) {
            }
        }
    }

    class C09802 implements ServiceConnection {
        C09802() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            DataClient.this.mService = Stub.asInterface(service);
            try {
                DataClient.this.mService.registerListener(DataClient.this.mContext.getPackageName(), DataClient.this.listener);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            if (DataClient.this.isBind != null) {
                DataClient.this.isBind.set(false);
            }
            DataClient.this.unRegisterListener();
            TLog.pro("onServiceDisconnected:" + name.getPackageName());
        }
    }

    class C09813 extends IDataListener.Stub {
        C09813() {
        }

        public DataTrans getData(String sessionId, String dataType, String params) throws RemoteException {
            if (DataClient.this.mListener != null) {
                return DataClient.this.mListener.getData(sessionId, dataType, params);
            }
            return null;
        }

        public void onDataResult(String sessionId, String dataType, String data) throws RemoteException {
            if (DataClient.this.mListener != null) {
                TLog.pro("IDataListener onDataResult");
                DataClient.this.mListener.onDataResult(sessionId, dataType, data);
            }
        }
    }

    public DataClient(Context context) {
        this.mContext = context;
        init(this.mContext);
    }

    private void registerServerConnectionsStatusListener() {
        if (this.mContext == null) {
            TLog.pro("Context is null");
            return;
        }
        if (this.serverConnectionsStatusListener == null) {
            this.serverConnectionsStatusListener = new C09791();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.huami.watch.DATA_SERVICE_CONNECTED_ACTION");
        intentFilter.addAction("com.huami.watch.DATA_SERVICE_DISCONNECTED_ACTION");
        this.mContext.registerReceiver(this.serverConnectionsStatusListener, intentFilter);
    }

    public void init(Context context) {
        if (context == null) {
            TLog.pro("I also want to do init, but context is null, so ...");
            return;
        }
        if (this.mContext == null) {
            this.mContext = context;
        }
        Intent intent = new Intent("com.huami.watch.wearservices.DATA_SERVICE");
        intent.setPackage("com.huami.watch.wearservices");
        intent.setComponent(new ComponentName("com.huami.watch.wearservices", "com.huami.watch.transdata.DataService"));
        boolean bindRes = this.mContext.bindService(intent, this.serviceConnection, 1);
        this.isBind.set(bindRes);
        TLog.pro("bindService:" + bindRes);
        registerServerConnectionsStatusListener();
    }

    public void registerListener(ClientListener listener) {
        TLog.pro("registerListener:" + this.mContext.getPackageName());
        this.mListener = listener;
    }

    public void triggerSync(Map params) {
        if (this.mService != null) {
            try {
                this.mService.triggered(this.mContext.getPackageName(), params);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void unRegisterListener() {
        if (this.mService != null) {
            try {
                this.mService.unRegisterListener(this.mContext.getPackageName());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
