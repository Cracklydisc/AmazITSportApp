package com.huami.watch.transdata.transport;

import android.content.Intent;
import android.os.RemoteException;
import android.text.TextUtils;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transdata.DataService;
import com.huami.watch.transdata.DataTrans;
import com.huami.watch.transdata.IDataListener;
import com.huami.watch.transdata.RegisterNewListener;
import com.huami.watch.transdata.TLog;
import com.huami.watch.transport.DataBundle;
import com.huami.watch.transport.DataTransportResult;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.ChannelListener;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import com.huami.watch.transport.Transporter.ServiceConnectionListener;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;

public class DataTransporter implements RegisterNewListener, ChannelListener, DataListener, ServiceConnectionListener {
    private DataService mDataService;
    private int mDataTransporterState = 0;
    private long mDataTransporterStateWaitTime = 0;
    private RequestData mLastRequestData = null;
    private Transporter mTransporter;
    private TaskManager taskManager;

    public DataTransporter(DataService dataService) {
        this.mDataService = dataService;
        init();
    }

    private void init() {
        this.taskManager = new TaskManager("data_transporter_tasks");
        this.mTransporter = Transporter.get(this.mDataService, "com.huami.watch.http-support.userdata_trans");
        this.mTransporter.addDataListener(this);
        this.mTransporter.addChannelListener(this);
        this.mTransporter.addServiceConnectionListener(this);
        this.mTransporter.connectTransportService();
    }

    private void sendDataToBle(final String sessionId, String action, DataBundle dataBundle, final String clientId) {
        if (this.mTransporter != null) {
            this.mTransporter.send(action, dataBundle, new DataSendResultCallback() {
                public void onResultBack(DataTransportResult dataTransportResult) {
                    if (dataTransportResult.getResultCode() == 0) {
                        TLog.data(sessionId, "send data to ble successful," + clientId);
                    } else {
                        TLog.data(sessionId, "send data to ble failed ,code:" + dataTransportResult.getResultCode() + ",client:" + clientId);
                    }
                }
            });
            TLog.data(sessionId, "send data to ble ..., client:" + clientId);
        }
    }

    private void requestUserData(DataBundle data) {
        clearDataStateWait();
        try {
            byte[] dataBytes = data.getByteArray("data");
            if (dataBytes != null) {
                RequestData requestData = new RequestData((LinkedHashMap) data.getSerializable("headers"), new String(dataBytes, "UTF-8"));
                String dataType = requestData.getDataType();
                if (TextUtils.equals(dataType, "TRIGGER")) {
                    TLog.data(requestData.getSessionId(), "---TRIGGER FROM ASSIST---");
                    this.mDataService.sendBroadcast(new Intent("com.huami.watch.trans_new_channel_prepare"));
                    return;
                }
                TLog.data(requestData.getSessionId(), "request user data:" + dataType + ":::" + requestData.getPkgName());
                IDataListener client = this.mDataService.getClientForPkg(requestData.getPkgName());
                if (client != null) {
                    requestDataToClient(requestData, client);
                    return;
                }
                TLog.data(requestData.getSessionId(), dataType + ": no client ...and save ...");
                this.mLastRequestData = requestData;
                this.mDataTransporterState = 1;
                this.mDataTransporterStateWaitTime = System.currentTimeMillis() / 1000;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void requestDataToClient(RequestData requestData, IDataListener client) {
        try {
            String sessionId = requestData.getSessionId();
            DataTrans item = client.getData(String.valueOf(sessionId), requestData.getDataType(), requestData.getRequestInfo());
            item.addPublicHeaders(String.valueOf(sessionId));
            TLog.data(requestData.getSessionId(), "get data from " + requestData.getPkgName());
            LinkedHashMap headers = item.getSendHeaders();
            DataBundle dataBundle = new DataBundle();
            dataBundle.putByteArray("data", item.getData());
            dataBundle.putSerializable("headers", headers);
            TLog.data(sessionId, "ACTION_USER_SEND_DATA---Header:" + headers.toString() + " ,data_length:" + (item.getData() == null ? 0 : item.getData().length));
            sendDataToBle(sessionId, "com.huami.watch.user_data", dataBundle, requestData.getPkgName());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void reRequestUserData() {
        if (this.mDataTransporterState == 1) {
            if ((System.currentTimeMillis() / 1000) - this.mDataTransporterStateWaitTime > 15) {
                clearDataStateWait();
            } else if (this.mLastRequestData == null) {
                clearDataStateWait();
            } else {
                RequestData requestData = this.mLastRequestData;
                IDataListener client = this.mDataService.getClientForPkg(requestData.getPkgName());
                if (client != null) {
                    TLog.data(requestData.getSessionId(), requestData.getDataType() + "--- send again ...");
                    requestDataToClient(requestData, client);
                    clearDataStateWait();
                }
            }
        }
    }

    public void registerNew(String pkg) {
        reRequestUserData();
    }

    private void clearDataStateWait() {
        this.mDataTransporterState = 0;
        this.mLastRequestData = null;
    }

    private void requestUserDataResult(DataBundle data) {
        try {
            byte[] dataBytes = data.getByteArray("data");
            if (dataBytes != null) {
                ResultData resultData = new ResultData((LinkedHashMap) data.getSerializable("headers"), new String(dataBytes, "UTF-8"));
                TLog.data(resultData.getSessionId(), "result user data," + resultData.getPkgName());
                IDataListener client = this.mDataService.getClientForPkg(resultData.getPkgName());
                if (client != null) {
                    try {
                        client.onDataResult(String.valueOf(resultData.getSessionId()), resultData.getDataType(), resultData.getResultInfo());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                this.mDataTransporterState = 0;
            }
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
    }

    public void sendTrigger(String pkg) {
        DataBundle bundle = new DataBundle();
        long sessionId = System.currentTimeMillis();
        LinkedHashMap<String, String> headers = new LinkedHashMap();
        headers.put("sessionId", String.valueOf(sessionId));
        bundle.putSerializable("headers", headers);
        TLog.data(String.valueOf(sessionId), "Trigger:" + pkg);
        sendDataToBle(String.valueOf(sessionId), "com.huami.watch.trigger", bundle, pkg);
    }

    public void onDataReceived(TransportDataItem transportDataItem) {
        String action = transportDataItem.getAction();
        final DataBundle data = transportDataItem.getData();
        if (action != null || data != null) {
            if (TextUtils.equals(action, "com.huami.watch.phone_user_data")) {
                this.taskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        DataTransporter.this.requestUserData(data);
                        return null;
                    }
                }).execute();
            } else if (TextUtils.equals(action, "com.huami.watch.phone_data_result")) {
                this.taskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        DataTransporter.this.requestUserDataResult(data);
                        return null;
                    }
                }).execute();
            }
        }
    }

    public void onChannelChanged(boolean b) {
        TLog.pro("transporter onChannelChanged :" + b);
    }
}
