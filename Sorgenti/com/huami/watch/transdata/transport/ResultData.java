package com.huami.watch.transdata.transport;

import android.text.TextUtils;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class ResultData {
    private String dataType;
    private String pkgName;
    private String resultInfo;
    private String sessionId;
    private String subAction;

    public ResultData(HashMap header, String data) {
        if (!TextUtils.isEmpty(data)) {
            parser(header, data);
        }
    }

    public void parser(HashMap headers, String data) {
        try {
            JSONObject obj = new JSONObject(data);
            this.dataType = obj.optString("dataType");
            this.subAction = obj.optString("subAction");
            this.resultInfo = obj.optJSONObject("result").toString();
            this.pkgName = DataTransHelper.translationPackage(this.dataType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (headers != null) {
            this.sessionId = (String) headers.get("sessionId");
        }
    }

    public String getDataType() {
        return this.dataType;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public String getResultInfo() {
        return this.resultInfo;
    }
}
