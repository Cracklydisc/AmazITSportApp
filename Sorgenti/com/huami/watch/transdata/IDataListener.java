package com.huami.watch.transdata;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDataListener extends IInterface {

    public static abstract class Stub extends Binder implements IDataListener {

        private static class Proxy implements IDataListener {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public DataTrans getData(String sessionId, String dataType, String params) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    DataTrans _result;
                    _data.writeInterfaceToken("com.huami.watch.transdata.IDataListener");
                    _data.writeString(sessionId);
                    _data.writeString(dataType);
                    _data.writeString(params);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (DataTrans) DataTrans.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void onDataResult(String sessionId, String dataType, String data) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transdata.IDataListener");
                    _data.writeString(sessionId);
                    _data.writeString(dataType);
                    _data.writeString(data);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.transdata.IDataListener");
        }

        public static IDataListener asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.transdata.IDataListener");
            if (iin == null || !(iin instanceof IDataListener)) {
                return new Proxy(obj);
            }
            return (IDataListener) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.transdata.IDataListener");
                    DataTrans _result = getData(data.readString(), data.readString(), data.readString());
                    reply.writeNoException();
                    if (_result != null) {
                        reply.writeInt(1);
                        _result.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.transdata.IDataListener");
                    onDataResult(data.readString(), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.transdata.IDataListener");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    DataTrans getData(String str, String str2, String str3) throws RemoteException;

    void onDataResult(String str, String str2, String str3) throws RemoteException;
}
