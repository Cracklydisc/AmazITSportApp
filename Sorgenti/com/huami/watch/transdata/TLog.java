package com.huami.watch.transdata;

import com.huami.watch.util.Log;

public class TLog {
    public static void data(String sessionId, String msg) {
        Log.m29i("TRANS_SERVICE_DATA", "[sessionId:" + sessionId + "]" + msg, new Object[0]);
    }

    public static void pro(String msg) {
        Log.m29i("TRANS_SERVICE_PRO", msg, new Object[0]);
    }
}
