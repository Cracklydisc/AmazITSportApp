package com.huami.watch.transdata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class DataTrans implements Parcelable {
    public static final Creator<DataTrans> CREATOR = new C09781();
    private byte[] data;
    private String dataType;
    private HashMap<String, String> headers;
    private int subType;

    static class C09781 implements Creator<DataTrans> {
        C09781() {
        }

        public DataTrans createFromParcel(Parcel in) {
            return new DataTrans(in);
        }

        public DataTrans[] newArray(int size) {
            return new DataTrans[size];
        }
    }

    public DataTrans(String dataType, int subType) {
        this.dataType = dataType;
        this.subType = subType;
    }

    public DataTrans(Parcel in) {
        this.data = in.createByteArray();
        this.dataType = in.readString();
        this.subType = in.readInt();
        this.headers = in.readHashMap(HashMap.class.getClassLoader());
    }

    public byte[] getDataWithGZip(byte[] dataByte) {
        if (dataByte != null) {
            return ZipUtil.gZip(dataByte);
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByteArray(this.data);
        dest.writeString(this.dataType);
        dest.writeInt(this.subType);
        dest.writeMap(this.headers);
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] data) {
        setData(data, true);
    }

    public void setData(byte[] data, boolean needCompress) {
        if (needCompress) {
            this.data = getDataWithGZip(data);
        } else {
            this.data = data;
        }
    }

    public HashMap<String, String> addPublicHeaders(String sessionId) {
        addHeader("dataType", this.dataType);
        addHeader("subType", String.valueOf(this.subType));
        addHeader("sessionId", sessionId);
        return this.headers;
    }

    public void addHeader(String key, String value) {
        if (this.headers == null) {
            this.headers = new HashMap();
        }
        this.headers.put(key, value);
    }

    public LinkedHashMap getSendHeaders() {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap();
        if (this.headers != null && this.headers.size() > 0) {
            linkedHashMap.putAll(this.headers);
        }
        return linkedHashMap;
    }
}
