package com.huami.watch.picker;

public final class TimeFields {

    private static class TimeField implements Comparable {
        private final int mFirstIndexInPattern;

        public int compareTo(TimeField timefield) {
            return Integer.compare(this.mFirstIndexInPattern, timefield.mFirstIndexInPattern);
        }

        public int compareTo(Object obj) {
            return compareTo((TimeField) obj);
        }
    }
}
