package com.huami.watch.picker;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup.OnHierarchyChangeListener;
import android.widget.FrameLayout;
import com.huami.watch.indicator.PageIndicatorListener;
import com.huami.watch.indicator.PagerIndicatorAdapter;
import java.util.ArrayList;
import java.util.List;

public class TeaserLayout extends FrameLayout implements OnHierarchyChangeListener, PagerIndicatorAdapter {
    private float mActivatedScale;
    private ActivatorPassivator mActivatorPassivator;
    private int mCenterIndex;
    private GestureDetectorCompat mGestureDetector;
    private GestureWatcher mGestureWatcher;
    private Boolean mHorizontalScrollDetected;
    private Listener mListener;
    private float mPassivatedScale;
    private float mStartX;
    private float mStartY;
    private float mTeaserPercentHiding;
    private float mTeaserTapPercent;
    private int mTouchSlop;
    private PageIndicatorListener pageIndicatorListener;

    public interface ActivatorPassivator {
        void onActivate(View view, int i);

        void onPassivate(View view, int i);
    }

    protected interface Listener {
        void onCenterIndexChanged(int i);

        void onScrollListener(List<TeaserViewInfo> list);
    }

    class C09642 extends SimpleAnimatorListener {
        C09642() {
        }

        public void onAnimationEnd(Animator animator) {
        }
    }

    private class GestureWatcher extends SimpleOnGestureListener {
        private float mDownX;
        private ScrollHelper mLeftScrollHelper;
        private ScrollHelper mRightScrollHelper;
        private float mScrollDistance;
        private ValueAnimator mScrollLeftAnimator;
        private float mScrollPercentage;
        private ValueAnimator mScrollRightAnimator;

        class C09651 implements AnimatorUpdateListener {
            C09651() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                GestureWatcher.this.mRightScrollHelper.handleHorizontally(((Float) animation.getAnimatedValue()).floatValue());
            }
        }

        class C09673 implements AnimatorUpdateListener {
            C09673() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                GestureWatcher.this.mLeftScrollHelper.handleHorizontally(((Float) animation.getAnimatedValue()).floatValue());
            }
        }

        private GestureWatcher() {
            this.mScrollLeftAnimator = null;
            this.mScrollRightAnimator = null;
        }

        private boolean canMoveLeft() {
            return TeaserLayout.this.mCenterIndex > 0;
        }

        private boolean canMoveRight() {
            return TeaserLayout.this.mCenterIndex < TeaserLayout.this.getChildCount() + -1;
        }

        private void onUp(MotionEvent motionevent) {
            if (((double) this.mScrollPercentage) >= 0.5d) {
                final int nextRightIndex = TeaserLayout.this.mCenterIndex + 1;
                final int nextLeftIndex = TeaserLayout.this.mCenterIndex - 1;
                if (this.mScrollDistance < 0.0f && canMoveRight()) {
                    cancelAnimator(this.mScrollRightAnimator);
                    this.mScrollRightAnimator = this.mRightScrollHelper.createScrollAnimator();
                    this.mScrollRightAnimator.addUpdateListener(new C09651());
                    this.mScrollRightAnimator.addListener(new SimpleAnimatorListener() {
                        public void onAnimationEnd(Animator animator) {
                            TeaserLayout.this.setCenterIndex(nextRightIndex, true);
                        }
                    });
                    this.mScrollRightAnimator.start();
                    return;
                } else if (this.mScrollDistance > 0.0f && canMoveLeft()) {
                    cancelAnimator(this.mScrollLeftAnimator);
                    this.mScrollLeftAnimator = this.mLeftScrollHelper.createScrollAnimator();
                    this.mScrollLeftAnimator.addUpdateListener(new C09673());
                    this.mScrollLeftAnimator.addListener(new SimpleAnimatorListener() {
                        public void onAnimationEnd(Animator animator) {
                            TeaserLayout.this.setCenterIndex(nextLeftIndex, true);
                        }
                    });
                    this.mScrollLeftAnimator.start();
                    return;
                } else {
                    return;
                }
            }
            TeaserLayout.this.animateToCenter(TeaserLayout.this.mCenterIndex);
        }

        private void cancelAnimator(ValueAnimator valueAnimator) {
            if (valueAnimator != null && valueAnimator.isRunning()) {
                valueAnimator.cancel();
            }
        }

        public boolean onDown(MotionEvent motionevent) {
            this.mScrollDistance = 0.0f;
            this.mDownX = motionevent.getX();
            cancelAnimator(this.mScrollLeftAnimator);
            cancelAnimator(this.mScrollRightAnimator);
            if (this.mLeftScrollHelper != null) {
                this.mLeftScrollHelper.destory();
                this.mLeftScrollHelper = null;
            }
            if (this.mRightScrollHelper != null) {
                this.mRightScrollHelper.destory();
                this.mRightScrollHelper = null;
            }
            if (TeaserLayout.this.pageIndicatorListener != null) {
                TeaserLayout.this.pageIndicatorListener.onPageIndicatorStateChanged(1);
            }
            this.mScrollPercentage = 0.0f;
            return true;
        }

        public boolean onScroll(MotionEvent downEvent, MotionEvent currentEvent, float scrollX, float scrollY) {
            this.mScrollDistance = currentEvent.getX() - downEvent.getX();
            this.mScrollPercentage = Math.min(1.0f, Math.abs(this.mScrollDistance) / (((float) TeaserLayout.this.getWidth()) / 2.0f));
            if (this.mScrollDistance > 0.0f && canMoveLeft()) {
                if (this.mLeftScrollHelper == null) {
                    ArrayList<TeaserViewInfo> leftViewsSet = new ArrayList();
                    TeaserLayout.this.initTeaserViewInfo(TeaserLayout.this.mCenterIndex - 1, leftViewsSet);
                    this.mLeftScrollHelper = new ScrollHelper(leftViewsSet);
                }
                this.mLeftScrollHelper.handleHorizontally(this.mScrollPercentage);
            } else if (this.mScrollDistance < 0.0f && canMoveRight()) {
                if (this.mRightScrollHelper == null) {
                    ArrayList<TeaserViewInfo> rightViewsSet = new ArrayList();
                    TeaserLayout.this.initTeaserViewInfo(TeaserLayout.this.mCenterIndex + 1, rightViewsSet);
                    this.mRightScrollHelper = new ScrollHelper(rightViewsSet);
                }
                this.mRightScrollHelper.handleHorizontally(this.mScrollPercentage);
            }
            return true;
        }

        public boolean onSingleTapUp(MotionEvent motionevent) {
            int width = TeaserLayout.this.getWidth();
            float delta = TeaserLayout.this.mTeaserTapPercent * ((float) width);
            float x = this.mDownX;
            if (x < delta && canMoveLeft()) {
                TeaserLayout.this.setCenterIndex(TeaserLayout.this.mCenterIndex - 1, true);
            } else if (x + delta > ((float) width) && canMoveRight()) {
                TeaserLayout.this.setCenterIndex(TeaserLayout.this.mCenterIndex + 1, true);
                return true;
            }
            if (TeaserLayout.this.pageIndicatorListener != null) {
                TeaserLayout.this.pageIndicatorListener.onPageIndicatorStateChanged(0);
            }
            return true;
        }
    }

    private class ScrollHelper {
        private float mCurrentFraction = 0.0f;
        private List<TeaserViewInfo> mViewInfos;

        public ScrollHelper(List<TeaserViewInfo> infos) {
            this.mViewInfos = infos;
        }

        private void destory() {
            if (this.mViewInfos != null) {
                this.mViewInfos.clear();
                this.mViewInfos = null;
            }
        }

        private void handleHorizontally(float fraction) {
            if (TeaserLayout.this.getWidth() != 0) {
                this.mCurrentFraction = fraction;
                int halfWidth = (TeaserLayout.this.getRight() - TeaserLayout.this.getLeft()) / 2;
                int centerX = halfWidth;
                TeaserViewInfo centerInfo = null;
                for (TeaserViewInfo viewInfo : this.mViewInfos) {
                    float width;
                    float temp;
                    viewInfo.currentX = viewInfo.startX + ((viewInfo.endX - viewInfo.startX) * fraction);
                    viewInfo.view.setX(viewInfo.currentX);
                    float centerLeft = (float) (halfWidth - (viewInfo.view.getWidth() / 2));
                    if (viewInfo.view.getX() + ((float) (viewInfo.view.getWidth() / 2)) <= ((float) centerX)) {
                        width = centerLeft + (((float) viewInfo.view.getWidth()) * TeaserLayout.this.mTeaserPercentHiding);
                    } else {
                        width = (((float) (TeaserLayout.this.getRight() - TeaserLayout.this.getLeft())) - (((float) viewInfo.view.getWidth()) * (1.0f - TeaserLayout.this.mTeaserPercentHiding))) - centerLeft;
                    }
                    if (viewInfo.view.getX() + ((float) (viewInfo.view.getWidth() / 2)) < ((float) centerX)) {
                        temp = (viewInfo.view.getX() - centerLeft) / width;
                    } else {
                        temp = (viewInfo.view.getX() - centerLeft) / width;
                    }
                    if (viewInfo.index == TeaserLayout.this.mCenterIndex) {
                        centerInfo = viewInfo;
                    }
                    if (temp > 1.0f) {
                        temp = 1.0f;
                    }
                    if (temp < -1.0f) {
                        temp = -1.0f;
                    }
                    viewInfo.percent = temp;
                }
                if (TeaserLayout.this.mListener != null) {
                    TeaserLayout.this.mListener.onScrollListener(this.mViewInfos);
                }
                if (centerInfo != null && TeaserLayout.this.pageIndicatorListener != null) {
                    TeaserLayout.this.pageIndicatorListener.onPageIndicatorScrolled(TeaserLayout.this.mCenterIndex, -centerInfo.percent, 0);
                }
            }
        }

        private ValueAnimator createScrollAnimator() {
            if (this.mCurrentFraction < 0.0f) {
                this.mCurrentFraction = 0.0f;
            }
            if (this.mCurrentFraction > 1.0f) {
                this.mCurrentFraction = 1.0f;
            }
            long time = (long) (300.0f * (1.0f - this.mCurrentFraction));
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[]{this.mCurrentFraction, 1.0f});
            valueAnimator.setDuration(time);
            return valueAnimator;
        }
    }

    private static class TargetState {
        private final float[] scale;
        private final int[] f25x;

        public int getCount() {
            return this.f25x.length;
        }

        private TargetState(int i) {
            this.f25x = new int[i];
            this.scale = new float[i];
        }
    }

    public static class TeaserViewInfo {
        private float currentX;
        private float endX;
        public int index;
        public float percent;
        private float startX;
        public View view;
    }

    public TeaserLayout(Context context) {
        this(context, null);
    }

    public TeaserLayout(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public TeaserLayout(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        init(context);
        setOnHierarchyChangeListener(this);
    }

    private void init(Context context) {
        this.mGestureWatcher = new GestureWatcher();
        this.mGestureDetector = new GestureDetectorCompat(context, this.mGestureWatcher);
        this.mGestureDetector.setIsLongpressEnabled(false);
        this.mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        this.mCenterIndex = -1;
        this.mTeaserPercentHiding = 0.4f;
        this.mActivatedScale = 1.0f;
        this.mPassivatedScale = 0.5f;
        this.mTeaserTapPercent = 0.25f;
    }

    protected int getCenterIndex() {
        return this.mCenterIndex;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void measureTeaserLayout(View child, int widthMeasureSpec, int heightMeasureSpec) {
        child.measure(MeasureSpec.makeMeasureSpec((int) (((float) MeasureSpec.getSize(widthMeasureSpec)) / (1.0f + (2.0f * this.mTeaserPercentHiding))), Integer.MIN_VALUE), heightMeasureSpec);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (this.mCenterIndex != -1) {
            immediatelyCenter(this.mCenterIndex);
            onActivatorPassivator();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        switch (motionevent.getAction()) {
            case 0:
                this.mStartX = motionevent.getX();
                this.mStartY = motionevent.getY();
                this.mHorizontalScrollDetected = null;
                this.mGestureDetector.onTouchEvent(motionevent);
                break;
            case 1:
                this.mGestureDetector.onTouchEvent(motionevent);
                break;
            case 2:
                float dx = Math.abs(this.mStartX - motionevent.getX());
                float dy = Math.abs(this.mStartY - motionevent.getY());
                if (this.mHorizontalScrollDetected == null) {
                    if ((dx * dx) + (dy * dy) > ((float) (this.mTouchSlop * this.mTouchSlop))) {
                        if (dx > dy) {
                            this.mHorizontalScrollDetected = Boolean.valueOf(true);
                        } else {
                            this.mHorizontalScrollDetected = Boolean.valueOf(false);
                        }
                    }
                    if (this.mCenterIndex >= 0 && this.mCenterIndex < getChildCount()) {
                        View centerView = getChildAt(this.mCenterIndex);
                        if (this.mStartX < centerView.getX() || this.mStartX > centerView.getX() + ((float) (centerView.getRight() - centerView.getLeft()))) {
                            return true;
                        }
                    }
                }
                if (this.mHorizontalScrollDetected != null && this.mHorizontalScrollDetected.booleanValue()) {
                    return this.mGestureDetector.onTouchEvent(motionevent);
                }
        }
        return false;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionevent) {
        if (motionevent.getAction() == 1) {
            this.mGestureWatcher.onUp(motionevent);
        }
        return this.mGestureDetector.onTouchEvent(motionevent);
    }

    private void immediatelyCenter(int index) {
        ArrayList<TeaserViewInfo> viewsSet = new ArrayList();
        initTeaserViewInfo(index, viewsSet);
        new ScrollHelper(viewsSet).handleHorizontally(1.0f);
    }

    public boolean canScrollHorizontally(int i) {
        return this.mCenterIndex > 0;
    }

    private void animateToCenter(int centerindex) {
        ArrayList<TeaserViewInfo> leftViewsSet = new ArrayList();
        initTeaserViewInfo(centerindex, leftViewsSet);
        final ScrollHelper scrollHelper = new ScrollHelper(leftViewsSet);
        ValueAnimator animator = scrollHelper.createScrollAnimator();
        animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                scrollHelper.handleHorizontally(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        animator.addListener(new C09642());
        animator.start();
    }

    private void initTeaserViewInfo(int centerIndex, List<TeaserViewInfo> infos) {
        TargetState targetstate = getTargetState(centerIndex);
        for (int j = 0; j < targetstate.getCount(); j++) {
            View view = getChildAt(j);
            TeaserViewInfo viewInfo = new TeaserViewInfo();
            viewInfo.startX = view.getX();
            viewInfo.endX = (float) targetstate.f25x[j];
            viewInfo.view = view;
            viewInfo.index = j;
            infos.add(viewInfo);
        }
    }

    public void setActivatorPassivator(ActivatorPassivator activatorpassivator) {
        this.mActivatorPassivator = activatorpassivator;
        onActivatorPassivator();
    }

    private void onActivatorPassivator() {
        if (this.mActivatorPassivator != null) {
            for (int viewIndex = 0; viewIndex < getChildCount(); viewIndex++) {
                View view = getChildAt(viewIndex);
                if (viewIndex == this.mCenterIndex) {
                    this.mActivatorPassivator.onActivate(view, this.mCenterIndex);
                } else {
                    this.mActivatorPassivator.onPassivate(view, viewIndex);
                }
            }
        }
    }

    public void setCenterIndex(int centerIndex, boolean animate) {
        if (centerIndex >= 0 && centerIndex < getChildCount() && this.mCenterIndex != centerIndex) {
            if (this.mActivatorPassivator != null) {
                if (this.mCenterIndex >= 0 && centerIndex < getChildCount()) {
                    this.mActivatorPassivator.onPassivate(getChildAt(this.mCenterIndex), this.mCenterIndex);
                }
                if (centerIndex >= 0 && centerIndex < getChildCount()) {
                    this.mActivatorPassivator.onActivate(getChildAt(centerIndex), centerIndex);
                }
            }
            this.mCenterIndex = centerIndex;
            if (animate) {
                animateToCenter(centerIndex);
            } else {
                immediatelyCenter(centerIndex);
            }
            if (this.mListener != null) {
                this.mListener.onCenterIndexChanged(this.mCenterIndex);
            }
        }
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    private TargetState getTargetState(int centerIndex) {
        TargetState targetState = new TargetState(getChildCount());
        int totalWidth = getWidth();
        int halfWidth = totalWidth / 2;
        targetState.f25x[centerIndex] = halfWidth - (getChildAt(centerIndex).getWidth() / 2);
        View leftView = getChildAt(centerIndex - 1);
        if (leftView != null) {
            targetState.f25x[centerIndex - 1] = -Math.round(this.mTeaserPercentHiding * ((float) leftView.getWidth()));
        }
        View rightView = getChildAt(centerIndex + 1);
        if (rightView != null) {
            int childWidth = rightView.getWidth();
            targetState.f25x[centerIndex + 1] = totalWidth - (childWidth - Math.round(this.mTeaserPercentHiding * ((float) childWidth)));
        }
        for (int leftChildIndex = centerIndex - 2; leftChildIndex >= 0; leftChildIndex--) {
            int leftRightChildX = targetState.f25x[leftChildIndex + 1];
            int leftRightChildWidth = getChildAt(leftChildIndex + 1).getWidth() / 2;
            targetState.f25x[leftChildIndex] = ((leftRightChildX + leftRightChildWidth) - halfWidth) - Math.round(this.mTeaserPercentHiding * ((float) getChildAt(leftChildIndex).getWidth()));
        }
        for (int rightChildIndex = centerIndex + 2; rightChildIndex < targetState.getCount(); rightChildIndex++) {
            int rightLeftChildX = targetState.f25x[rightChildIndex - 1];
            int rightLeftChildWidth = getChildAt(rightChildIndex - 1).getWidth() / 2;
            int rightChildWidth = getChildAt(rightChildIndex).getWidth();
            targetState.f25x[rightChildIndex] = ((rightLeftChildX + rightLeftChildWidth) + halfWidth) - (rightChildWidth - Math.round(this.mTeaserPercentHiding * ((float) rightChildWidth)));
        }
        for (int k1 = 0; k1 < targetState.getCount(); k1++) {
            float scaleSize;
            float[] scale = targetState.scale;
            if (k1 == centerIndex) {
                scaleSize = this.mActivatedScale;
            } else {
                scaleSize = this.mPassivatedScale;
            }
            scale[k1] = scaleSize;
        }
        return targetState;
    }

    public void addPageIndicatorListener(PageIndicatorListener listener) {
        this.pageIndicatorListener = listener;
    }

    public void removePageIndicatorListener(PageIndicatorListener listener) {
        this.pageIndicatorListener = null;
    }

    public int getCount() {
        return getChildCount();
    }

    public int getCurrentIndex() {
        return this.mCenterIndex;
    }

    public void onChildViewAdded(View parent, View child) {
        if (this.pageIndicatorListener != null) {
            this.pageIndicatorListener.onPageIndicatorNumChanged(getChildCount());
        }
    }

    public void onChildViewRemoved(View parent, View child) {
        if (this.pageIndicatorListener != null) {
            this.pageIndicatorListener.onPageIndicatorNumChanged(getChildCount());
        }
    }
}
