package com.huami.watch.menu;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.IconDrawable;
import clc.utils.venus.widgets.common.XTextView;
import clc.utils.venus.widgets.senior.IconBoard;
import clc.utils.venus.widgets.wheel.WheelView.ScrollProcMonitor;
import com.huami.watch.ui.C1070R;
import com.huami.watch.utils.LayoutUtils;

public class MenuItem extends BaseDrawableGroup implements ScrollProcMonitor {
    private boolean mClickable = true;
    private XTextView mDevName;
    private XTextView mDevStatus;
    private IconBoard mIconBoard;
    private IconDrawable mIconDrawable;
    private int mIconTextMargin;
    private BaseDrawableGroup mInfoGroup;
    private float mScaleFactor = 1.0f;
    private float mTitleSize;
    private float mValueSize;

    public MenuItem(XViewWrapper context, RectF region, int iconResId, String title, String value, float titleTextSize, float valueTextSize) {
        super(context);
        this.mTitleSize = titleTextSize;
        this.mValueSize = valueTextSize;
        resize(region);
        Log.d("MenuItem", "menu load resId :" + iconResId);
        doInit(iconResId, title, value, null);
        this.mIconBoard.setScaleFactor(1.0f - this.mScaleFactor);
    }

    public MenuItem(XViewWrapper context, RectF region, int iconResId, String title, String value, float titleTextSize, float valueTextSize, Bitmap iconBitmap) {
        boolean z = true;
        super(context);
        this.mTitleSize = titleTextSize;
        this.mValueSize = valueTextSize;
        resize(region);
        String str = "MenuItem";
        StringBuilder append = new StringBuilder().append("menu load resId :").append(iconResId).append(" , iconBitmap is null?:");
        if (iconBitmap != null) {
            z = false;
        }
        Log.d(str, append.append(z).toString());
        doInit(iconResId, title, value, iconBitmap);
        this.mIconBoard.setScaleFactor(1.0f - this.mScaleFactor);
    }

    private void doInit(int iconResId, String title, String value, Bitmap iconBitmap) {
        Log.d("Menu", "WifiHotSpotItem     doInit");
        this.mScaleFactor = Float.valueOf(this.mContext.getResources().getString(C1070R.string.menu_item_high_light_scale_factor)).floatValue();
        int iconBoardId = iconResId;
        if (iconBoardId != -1 && iconBitmap == null) {
            this.mIconDrawable = new IconDrawable(this.mContext, ((BitmapDrawable) this.mContext.getResources().getDrawable(iconBoardId)).getBitmap());
            this.mIconDrawable.setEnableHaloFeature(true);
            this.mIconDrawable.setHolaScaleRange(0.75f, 1.17f, 1.0f);
            this.mIconBoard = new IconBoard(this.mContext, this.mIconDrawable);
        } else if (iconBitmap != null) {
            Log.d("Menu", " icon bitmap  is not null ");
            this.mIconDrawable = new IconDrawable(this.mContext, iconBitmap);
            this.mIconDrawable.setEnableHaloFeature(true);
            this.mIconDrawable.setHolaScaleRange(0.75f, 1.17f, 1.0f);
            this.mIconBoard = new IconBoard(this.mContext, this.mIconDrawable);
        }
        RectF textRegionForName = new RectF(0.0f, 0.0f, 300.0f, 50.0f);
        RectF textRegionForStat = new RectF(0.0f, 0.0f, 300.0f, 30.0f);
        RectF itemRegion = new RectF(0.0f, 0.0f, 300.0f, this.mIconBoard.getHeight());
        this.mInfoGroup = new BaseDrawableGroup(this.mContext);
        this.mInfoGroup.resize(itemRegion);
        Resources r = this.mContext.getResources();
        this.mDevName = new XTextView(this.mContext, title, textRegionForName, Align.LEFT);
        if (this.mTitleSize > 0.0f) {
            this.mDevName.setTextSize(this.mTitleSize);
        } else {
            this.mDevName.setTextSize((float) r.getDimensionPixelSize(C1070R.dimen.common_txt_size));
        }
        this.mDevName.setTextColor(r.getColor(C1070R.color.main_text_color));
        this.mIconTextMargin = LayoutUtils.getIconTextMargin(this.mContext.getContext());
        this.mIconBoard.setRelativeX((float) r.getDimensionPixelOffset(C1070R.dimen.settings_icon_rx));
        if (value != null) {
            this.mDevStatus = new XTextView(this.mContext, value, textRegionForStat, Align.LEFT);
            this.mDevStatus.setTextColor(r.getColor(C1070R.color.main_text_color));
            if (this.mValueSize > 0.0f) {
                this.mDevStatus.setTextSize(this.mValueSize);
            } else {
                this.mDevStatus.setTextSize((float) r.getDimensionPixelSize(C1070R.dimen.smaller_txt_size));
            }
            this.mDevName.setRelativeX((float) this.mIconTextMargin);
            this.mDevName.setRelativeY(r.getDimension(C1070R.dimen.blt_info_margin_top));
            this.mDevStatus.setRelativeX((float) this.mIconTextMargin);
            this.mDevStatus.setRelativeY(this.mDevName.getRelativeY() + this.mDevName.getHeight());
        } else {
            this.mDevName.setRelativeX((float) this.mIconTextMargin);
            this.mDevName.setRelativeY(this.mIconBoard.getRelativeY() + ((this.mIconBoard.getHeight() - this.mDevName.getHeight()) / 2.0f));
        }
        this.mInfoGroup.setRelativeX(this.mIconBoard.getRelativeX() + this.mIconBoard.getWidth());
        this.mInfoGroup.addItem(this.mDevName);
        if (this.mDevStatus != null) {
            this.mInfoGroup.addItem(this.mDevStatus);
        }
        addItem(this.mInfoGroup);
        addItem(this.mIconBoard);
    }

    public void updateProc(boolean focused, float proc, int index) {
        float f = 0.0f;
        this.mIconBoard.setProgress(focused, proc);
        float abs = Math.abs(proc);
        float scale = 1.0f - ((1.0f - this.mScaleFactor) * abs);
        Matrix m = this.mInfoGroup.getMatrix();
        float relativeX = this.mInfoGroup.getRelativeX() + ((float) this.mIconTextMargin);
        if (proc <= 0.0f) {
            f = this.mInfoGroup.getHeight();
        }
        m.setScale(scale, scale, relativeX, f);
        if (abs > 0.4f) {
            this.mInfoGroup.setAlpha(0.64f);
        } else {
            this.mInfoGroup.setAlpha((-0.90000004f * abs) + 1.0f);
        }
        this.mClickable = abs < 0.2f;
    }

    public boolean isItemVisible() {
        return isVisible();
    }

    public IconDrawable getIconDrawable() {
        return this.mIconDrawable;
    }

    public boolean isClickable() {
        return this.mClickable;
    }
}
