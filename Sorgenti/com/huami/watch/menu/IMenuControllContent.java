package com.huami.watch.menu;

import android.view.View;
import android.view.ViewPropertyAnimator;

public interface IMenuControllContent {

    public interface MenuCloseListener {
        void onMenuClose();
    }

    public interface MenuListenner extends MenuCloseListener {
        void onMenuOpen();
    }

    public interface MenuOnItemClickListener {
        void onItemClick(int i, int i2, View view);
    }

    public interface MenuonCenterProximityListener {
        void onCenterProximity(View view, boolean z, int i, int i2);
    }

    ViewPropertyAnimator animateMenu();

    void bringMenuToFront();

    float canScrollMenuUp();

    int getMenuHeight();

    float getMenuTranslationY();

    boolean isListCanScroll();

    boolean isVisible();

    void notifyMenuJustPosition();

    void offsetMenu(int i);

    void performCloseListener();

    void performOpenListener();

    void scrollMenu(float f);

    void scrollToTop();

    void setMenuTranslationY(float f);

    void setVisible(boolean z);

    void showScrollBar();
}
