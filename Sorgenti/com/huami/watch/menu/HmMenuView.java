package com.huami.watch.menu;

import android.content.Context;
import android.util.AttributeSet;

public class HmMenuView extends NewMenuLayout {
    public HmMenuView(Context context) {
        super(context);
    }

    public HmMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmMenuView(Context context, AttributeSet attrs, int titleResId) {
        super(context, attrs, titleResId);
    }
}
