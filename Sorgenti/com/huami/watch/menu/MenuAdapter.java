package com.huami.watch.menu;

import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.util.ArrayMap;
import android.util.Log;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.DrawableItem.OnClickListener;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.wheel.AbstractWheelAdapter;
import clc.utils.venus.widgets.wheel.WheelViewItemsLayout;
import com.fox.venus.cutewheel.HMItemTitle;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.menu.NewMenuLayout.SimpleMenuAdpter;

public class MenuAdapter extends AbstractWheelAdapter implements OnClickListener {
    private boolean isClickPendding = false;
    private ArrayMap<String, MenuItem> mBSSIDtoActionsMap = null;
    private XViewWrapper mContext;
    private SimpleMenuAdpter mSimpleMenuAdpter;
    private HMItemTitle mTitle = null;
    private String mTitleContent = null;

    public MenuAdapter(XViewWrapper context, int titleResId) {
        this.mContext = context;
        this.mBSSIDtoActionsMap = new ArrayMap();
        this.mTitleContent = this.mContext.getContext().getString(titleResId);
    }

    public void setSimpleMenuAdapter(SimpleMenuAdpter simpleAdapter) {
        this.mSimpleMenuAdpter = simpleAdapter;
    }

    public int getItemsCount() {
        if (this.mSimpleMenuAdpter == null) {
            return 1;
        }
        return this.mSimpleMenuAdpter.getCount() + 1;
    }

    public DrawableItem getItem(int index, DrawableItem convertView, BaseDrawableGroup parent) {
        if (this.mSimpleMenuAdpter == null) {
            return null;
        }
        Log.d("Menu", "WifiSpotAdapter : position  " + index);
        if (!(parent instanceof WheelViewItemsLayout)) {
            return null;
        }
        DrawableItem item;
        WheelViewItemsLayout itemLayout = (WheelViewItemsLayout) parent;
        RectF itemRegion = new RectF(0.0f, 0.0f, itemLayout.getWidth(), itemLayout.getItemHeight());
        if (index == 0) {
            if (this.mTitle == null) {
                this.mTitle = new HMItemTitle(parent.getXContext(), -1, this.mTitleContent, itemRegion, Align.CENTER);
                this.mTitle.setTextColor(-1);
                this.mTitle.setTextSize(33);
                item = this.mTitle;
            } else {
                this.mTitle.setRelativeX(0.0f);
                this.mTitle.setRelativeY(0.0f);
                if (this.mTitle.getParent() != null) {
                    ((BaseDrawableGroup) this.mTitle.getParent()).removeItem(this.mTitle, false);
                }
                this.mTitle.reuse();
            }
            item = this.mTitle;
        } else {
            DrawableItem bda;
            MenuInfo info = this.mSimpleMenuAdpter.getItemMenu(index - 1);
            Log.d("Menu_item", "======= index :" + index + "   cache  :  " + ((MenuItem) this.mBSSIDtoActionsMap.get(String.valueOf(index))));
            if (info.iconBitmap == null) {
                Log.d("Menu_item", "=====iconBitmap====: is null ");
                bda = new MenuItem(this.mContext, itemRegion, info.iconResId, info.title, info.content, info.titleSize, info.valueSize);
            } else {
                Log.d("Menu_item", "=====iconBitmap====: is not null ");
                bda = new MenuItem(this.mContext, itemRegion, info.iconResId, info.title, info.content, info.titleSize, info.valueSize, info.iconBitmap);
            }
            item = bda;
            item.setTag(Integer.valueOf(index - 1));
            item.setRelativeX((itemLayout.getWidth() / 2.0f) - (item.getWidth() / 2.0f));
            this.mBSSIDtoActionsMap.put(String.valueOf(index), bda);
        }
        item.setOnClickListener(this);
        return item;
    }

    public void onClick(DrawableItem item) {
        if (this.mSimpleMenuAdpter != null && (item instanceof MenuItem)) {
            final MenuItem m = (MenuItem) item;
            if (!this.isClickPendding && m.isClickable()) {
                this.isClickPendding = true;
                Runnable onEndAction = new Runnable() {
                    public void run() {
                        MenuAdapter.this.mSimpleMenuAdpter.omMenuItemClick(((Integer) m.getTag()).intValue());
                        MenuAdapter.this.isClickPendding = false;
                    }
                };
                if (m.getIconDrawable() != null) {
                    m.getIconDrawable().startHaloClickAnimation(onEndAction);
                } else {
                    onEndAction.run();
                }
            }
        }
    }
}
