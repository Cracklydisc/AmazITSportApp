package com.huami.watch.menu;

import android.animation.Animator;
import android.content.Context;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import com.huami.watch.ui.C1070R;

public class MenuController {
    private boolean isMenuEnable = false;
    private SimpleAnimatorListener mAnimatorListener = new MenuSimpleAnimatorListener();
    boolean mIsAllowedToDragDown;
    private ViewPropertyAnimator mMenuAnimator;
    private int mMenuHeight = 0;
    private IMenuControllContent mMenuLayout = null;
    public int mMenuState;
    private int mMenuTouchArea;
    public FrameLayout mParentFrame;
    private int mPreMenuSate;
    private float mTranslationY;

    private class MenuSimpleAnimatorListener extends SimpleAnimatorListener {
        private MenuSimpleAnimatorListener() {
        }

        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
            if (MenuController.this.mMenuState == 0 && MenuController.this.mPreMenuSate == 1) {
                MenuController.this.mMenuLayout.scrollToTop();
                MenuController.this.mMenuLayout.performCloseListener();
                MenuController.this.mMenuLayout.setVisible(false);
                MenuController.this.mMenuLayout.setMenuTranslationY(0.0f);
            } else if (MenuController.this.mMenuState == 1 && MenuController.this.mPreMenuSate == 0) {
                MenuController.this.mMenuLayout.setMenuTranslationY((float) MenuController.this.mMenuHeight);
                MenuController.this.mMenuLayout.performOpenListener();
            }
            Log.d("Menu", " set translate  onAnimationEnd : " + MenuController.this.mMenuLayout.getMenuTranslationY());
        }
    }

    public void init(Context c, FrameLayout menuSwipeDismissLayout) {
        this.mMenuTouchArea = (int) c.getResources().getDimension(C1070R.dimen.menu_touch_area);
        this.mParentFrame = menuSwipeDismissLayout;
    }

    protected void onParentMeasure(int arg0, int arg1) {
        if (this.mMenuLayout != null) {
            this.mMenuHeight = this.mMenuLayout.getMenuHeight();
            Log.d("menu", "menu height is " + this.mMenuHeight);
        }
    }

    protected void onParentLayout(boolean changed, int left, int top, int right, int bottom) {
        if (this.mMenuLayout != null) {
            this.mMenuLayout.offsetMenu(-this.mMenuHeight);
        }
    }

    public IMenuControllContent getMenuLayout() {
        return this.mMenuLayout;
    }

    private void updateMenuState(MotionEvent ev, float mDownY, VelocityTracker mVelocityTracker, int mMinFlingVelocity, long mAnimationTime) {
        float deltaY = ev.getRawY() - mDownY;
        if (this.mMenuState == 0) {
            if (deltaY > ((float) getHeight()) * 0.25f) {
                this.mMenuState = 1;
            }
        } else if (deltaY < ((float) (-getHeight())) * 0.25f || this.mMenuLayout.getMenuTranslationY() < ((float) getHeight()) * 0.25f) {
            this.mMenuState = 0;
        }
        Log.i("Menu", "mMenuState = " + this.mMenuState + ",deltaY = " + deltaY + ",ev.getRawY() = " + ev.getRawY() + ", mDownY = " + mDownY + "   Y :  " + this.mMenuLayout.getMenuTranslationY());
    }

    private int getHeight() {
        return this.mParentFrame.getHeight();
    }

    protected boolean canListScrollY(MenuController menuController, boolean checkV, float dy, float x, float y) {
        if (this.mMenuLayout.isListCanScroll()) {
            return true;
        }
        return false;
    }

    private boolean thisTouchIsTop(View content, MotionEvent ev) {
        int y = (int) ev.getY();
        int top = content.getTop();
        return y >= top && y <= this.mMenuTouchArea + top;
    }

    public boolean onInterceptTouchDown(MenuSwipeDismissLayout menuSwipeDismissLayout, MotionEvent ev) {
        if (this.mMenuLayout == null) {
            this.mIsAllowedToDragDown = false;
            return false;
        }
        if (this.mMenuAnimator != null) {
            this.mMenuAnimator.cancel();
        }
        this.mTranslationY = this.mMenuLayout.getMenuTranslationY();
        Log.d("Menu", "onInterceptTouchDown   menu translateY " + this.mTranslationY);
        if (thisTouchIsTop(menuSwipeDismissLayout, ev) && this.mMenuState == 0) {
            this.mIsAllowedToDragDown = true;
            if (!this.mMenuLayout.isVisible()) {
                this.mMenuLayout.setVisible(true);
            }
        } else {
            checkMenuState();
            this.mIsAllowedToDragDown = false;
        }
        Log.i("menuwear", "onInterceptTouchDown : mIsUnableToDragDown = " + this.mIsAllowedToDragDown);
        return this.mIsAllowedToDragDown;
    }

    public boolean onInterceptTouchMove(MenuSwipeDismissLayout menuSwipeDismissLayout, MotionEvent ev, float dy, float x, float y) {
        boolean mIsAlowedToMove = false;
        if (this.mMenuState == 1 && canListScrollY(this, false, dy, x, y)) {
            mIsAlowedToMove = true;
        }
        Log.i("menuwear", "onInterceptTouchMove : mIsUnableToMove = " + mIsAlowedToMove);
        return mIsAlowedToMove;
    }

    public boolean onTouchEventUp(MotionEvent ev, float mDownY, int mMinFlingVelocity, VelocityTracker mVelocityTracker, long mAnimationTime) {
        this.mPreMenuSate = this.mMenuState;
        updateMenuState(ev, mDownY, mVelocityTracker, mMinFlingVelocity, mAnimationTime);
        this.mMenuAnimator = this.mMenuLayout.animateMenu();
        this.mMenuAnimator.setListener(this.mAnimatorListener);
        switch (this.mMenuState) {
            case 0:
                this.mMenuAnimator.translationY(0.0f).setDuration(mAnimationTime).start();
                break;
            case 1:
                this.mMenuAnimator.translationY((float) this.mMenuHeight).setDuration(mAnimationTime).start();
                this.mMenuLayout.showScrollBar();
                this.mMenuLayout.bringMenuToFront();
                this.mMenuLayout.notifyMenuJustPosition();
                break;
        }
        Log.i("onTouchEvent", "mMenuState=" + this.mMenuState + ",getHeight()=" + getHeight() + ",mMenuHeight=" + this.mMenuHeight);
        return true;
    }

    private void checkMenuState() {
        if (this.mMenuLayout != null && this.mMenuLayout.getMenuTranslationY() < ((float) this.mMenuHeight) && this.mMenuState == 1) {
            Log.d("Menu", "menu state my be error  .  menu  translate Y " + this.mMenuLayout.getMenuTranslationY());
            this.mMenuState = 0;
        }
    }

    public boolean onTouchEventCancel() {
        this.mMenuLayout.setMenuTranslationY((float) (-this.mMenuHeight));
        return true;
    }

    public void setTranslationY(MotionEvent ev, float mDownY, float lastY) {
        float translateY = this.mMenuLayout.getMenuTranslationY();
        float innerOffset = ((ev.getRawY() - lastY) + translateY) - ((float) getHeight());
        if (innerOffset >= 0.0f) {
            this.mMenuLayout.scrollMenu(innerOffset);
            float menuTranslate = (ev.getRawY() - lastY) - innerOffset;
            Log.d("Menu_BUG", "mMenuLayout infos :  menuTranslateY : " + menuTranslate);
            if (menuTranslate > 0.0f) {
                this.mMenuLayout.setMenuTranslationY(translateY + menuTranslate);
                return;
            }
            return;
        }
        float moveDelt = ev.getRawY() - lastY;
        float menuCanoffset = 0.0f;
        if (translateY >= ((float) getHeight())) {
            menuCanoffset = this.mMenuLayout.canScrollMenuUp();
            if (menuCanoffset != 0.0f) {
                menuCanoffset = -Math.min(Math.abs(moveDelt), menuCanoffset);
                this.mMenuLayout.scrollMenu(menuCanoffset);
            }
        }
        Log.d("Menu_BUG", "mMenuLayout infos :  moveDelt : " + moveDelt + "    menuCanoffset  :" + menuCanoffset);
        this.mMenuLayout.setMenuTranslationY((moveDelt - menuCanoffset) + translateY);
    }

    public void bringFront() {
        if (this.mMenuLayout != null) {
            this.mMenuLayout.bringMenuToFront();
        }
    }

    public boolean isEnable() {
        return this.isMenuEnable;
    }
}
