package com.huami.watch.menu;

import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

public class MenuSwipeDismissLayout extends FrameLayout {
    public static final boolean DEBUG = Log.isLoggable("MenuSwipeDismissLayout", 2);
    private int mActiveTouchId;
    private long mAnimationTime;
    private TimeInterpolator mCancelInterpolator;
    private boolean mDiscardIntercept;
    private TimeInterpolator mDismissInterpolator;
    private boolean mDismissed;
    private OnDismissedListener mDismissedListener;
    private float mDownX;
    private float mDownY;
    private float mLastX;
    private float mLastY;
    private int mMaxFlingVelocity;
    private boolean mMenuCanDrag;
    MenuController mMenuController;
    private int mMinFlingVelocity;
    private OnSwipeProgressChangedListener mProgressListener;
    private int mSlop;
    private boolean mSwiping;
    private float mTranslationX;
    private VelocityTracker mVelocityTracker;

    public interface OnDismissedListener {
        void onDismissed(MenuSwipeDismissLayout menuSwipeDismissLayout);
    }

    public interface OnSwipeProgressChangedListener {
        void onSwipeCancelled(MenuSwipeDismissLayout menuSwipeDismissLayout);

        void onSwipeProgressChanged(MenuSwipeDismissLayout menuSwipeDismissLayout, float f, float f2);
    }

    private class ScrollerView extends View {
        public ScrollerView(Context context) {
            super(context);
        }

        public boolean canScrollHorizontally(int direction) {
            if (MenuSwipeDismissLayout.this.mMenuController.mMenuState == 0 || MenuSwipeDismissLayout.this.mMenuController.getMenuLayout() == null) {
                return super.canScrollHorizontally(direction);
            }
            Log.d("Menu", "canScrollHorizontally true");
            return true;
        }
    }

    public MenuSwipeDismissLayout(Context context) {
        this(context, null);
    }

    public MenuSwipeDismissLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuSwipeDismissLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mSwiping = false;
        this.mMenuCanDrag = false;
        this.mMenuController = null;
        init(context);
        addView(new ScrollerView(context));
    }

    private void init(Context context) {
        ViewConfiguration vc = ViewConfiguration.get(getContext());
        this.mSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        this.mAnimationTime = (long) getContext().getResources().getInteger(17694720);
        this.mCancelInterpolator = new DecelerateInterpolator(1.5f);
        this.mDismissInterpolator = new AccelerateInterpolator(1.5f);
        this.mMenuController = new MenuController();
        this.mMenuController.init(context, this);
    }

    protected void onMeasure(int arg0, int arg1) {
        super.onMeasure(arg0, arg1);
        this.mMenuController.onParentMeasure(arg0, arg1);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.mMenuController.onParentLayout(changed, left, top, right, bottom);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r21) {
        /*
        r20 = this;
        r0 = r20;
        r2 = r0.mMenuController;
        r15 = r2.isEnable();
        r2 = DEBUG;
        if (r2 == 0) goto L_0x0024;
    L_0x000c:
        r2 = "MenuSwipeDismissLayout";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "menu touch is enable :";
        r3 = r3.append(r4);
        r3 = r3.append(r15);
        r3 = r3.toString();
        android.util.Log.d(r2, r3);
    L_0x0024:
        r0 = r20;
        r2 = r0.mMenuController;
        r2 = r2.isEnable();
        if (r2 != 0) goto L_0x0031;
    L_0x002e:
        r19 = 0;
    L_0x0030:
        return r19;
    L_0x0031:
        r0 = r20;
        r2 = r0.mTranslationX;
        r3 = 0;
        r0 = r21;
        r0.offsetLocation(r2, r3);
        r2 = r21.getActionMasked();
        switch(r2) {
            case 0: goto L_0x007c;
            case 1: goto L_0x0102;
            case 2: goto L_0x0109;
            case 3: goto L_0x0102;
            case 4: goto L_0x0042;
            case 5: goto L_0x00cb;
            case 6: goto L_0x00db;
            default: goto L_0x0042;
        };
    L_0x0042:
        r0 = r20;
        r2 = r0.mDiscardIntercept;
        if (r2 != 0) goto L_0x01a2;
    L_0x0048:
        r0 = r20;
        r2 = r0.mSwiping;
        if (r2 != 0) goto L_0x005b;
    L_0x004e:
        r0 = r20;
        r2 = r0.mMenuController;
        r2 = r2.mMenuState;
        r0 = r20;
        r3 = r0.mMenuController;
        r3 = 1;
        if (r2 != r3) goto L_0x01a2;
    L_0x005b:
        r19 = 1;
    L_0x005d:
        r2 = DEBUG;
        if (r2 == 0) goto L_0x0030;
    L_0x0061:
        r2 = "MenuSwipeDismissLayout";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "onInterceptTouchEvent return ret :";
        r3 = r3.append(r4);
        r0 = r19;
        r3 = r3.append(r0);
        r3 = r3.toString();
        android.util.Log.d(r2, r3);
        goto L_0x0030;
    L_0x007c:
        r20.resetMembers();
        r2 = r21.getRawX();
        r0 = r20;
        r0.mDownX = r2;
        r0 = r20;
        r0.mLastX = r2;
        r2 = r21.getRawY();
        r0 = r20;
        r0.mDownY = r2;
        r0 = r20;
        r0.mLastY = r2;
        r2 = 0;
        r0 = r21;
        r2 = r0.getPointerId(r2);
        r0 = r20;
        r0.mActiveTouchId = r2;
        r2 = android.view.VelocityTracker.obtain();
        r0 = r20;
        r0.mVelocityTracker = r2;
        r0 = r20;
        r2 = r0.mVelocityTracker;
        r0 = r21;
        r2.addMovement(r0);
        r0 = r20;
        r2 = r0.mMenuController;
        r0 = r20;
        r1 = r21;
        r2 = r2.onInterceptTouchDown(r0, r1);
        r0 = r20;
        r0.mMenuCanDrag = r2;
        r0 = r20;
        r0 = r0.mMenuCanDrag;
        r19 = r0;
        goto L_0x0030;
    L_0x00cb:
        r14 = r21.getActionIndex();
        r0 = r21;
        r2 = r0.getPointerId(r14);
        r0 = r20;
        r0.mActiveTouchId = r2;
        goto L_0x0042;
    L_0x00db:
        r14 = r21.getActionIndex();
        r0 = r21;
        r17 = r0.getPointerId(r14);
        r0 = r20;
        r2 = r0.mActiveTouchId;
        r0 = r17;
        if (r0 != r2) goto L_0x0042;
    L_0x00ed:
        if (r14 != 0) goto L_0x00ff;
    L_0x00ef:
        r16 = 1;
    L_0x00f1:
        r0 = r21;
        r1 = r16;
        r2 = r0.getPointerId(r1);
        r0 = r20;
        r0.mActiveTouchId = r2;
        goto L_0x0042;
    L_0x00ff:
        r16 = 0;
        goto L_0x00f1;
    L_0x0102:
        r20.resetMembers();
        r19 = 0;
        goto L_0x0030;
    L_0x0109:
        r2 = r21.getRawX();
        r0 = r20;
        r0.mLastX = r2;
        r2 = r21.getRawY();
        r0 = r20;
        r0.mLastY = r2;
        r0 = r20;
        r2 = r0.mVelocityTracker;
        if (r2 == 0) goto L_0x0042;
    L_0x011f:
        r0 = r20;
        r2 = r0.mActiveTouchId;
        r0 = r21;
        r18 = r0.findPointerIndex(r2);
        r2 = -1;
        r0 = r18;
        if (r0 != r2) goto L_0x0135;
    L_0x012e:
        r2 = 1;
        r0 = r20;
        r0.mDiscardIntercept = r2;
        goto L_0x0042;
    L_0x0135:
        r2 = r21.getRawX();
        r0 = r20;
        r3 = r0.mDownX;
        r5 = r2 - r3;
        r2 = r21.getRawY();
        r0 = r20;
        r3 = r0.mDownY;
        r11 = r2 - r3;
        r0 = r21;
        r1 = r18;
        r6 = r0.getX(r1);
        r0 = r21;
        r1 = r18;
        r7 = r0.getY(r1);
        r2 = java.lang.Math.abs(r5);
        r3 = java.lang.Math.abs(r11);
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 <= 0) goto L_0x017c;
    L_0x0165:
        r2 = 0;
        r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1));
        if (r2 == 0) goto L_0x0198;
    L_0x016a:
        r4 = 0;
        r2 = r20;
        r3 = r20;
        r2 = r2.canScroll(r3, r4, r5, r6, r7);
        if (r2 == 0) goto L_0x0198;
    L_0x0175:
        r2 = 1;
        r0 = r20;
        r0.mDiscardIntercept = r2;
        goto L_0x0042;
    L_0x017c:
        r0 = r20;
        r8 = r0.mMenuController;
        r9 = r20;
        r10 = r21;
        r12 = r6;
        r13 = r7;
        r2 = r8.onInterceptTouchMove(r9, r10, r11, r12, r13);
        if (r2 == 0) goto L_0x0193;
    L_0x018c:
        r2 = 1;
        r0 = r20;
        r0.mDiscardIntercept = r2;
        goto L_0x0042;
    L_0x0193:
        r2 = 0;
        r0 = r20;
        r0.mDiscardIntercept = r2;
    L_0x0198:
        r20.updateSwiping(r21);
        r2 = 1;
        r0 = r20;
        r0.mMenuCanDrag = r2;
        goto L_0x0042;
    L_0x01a2:
        r19 = 0;
        goto L_0x005d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.menu.MenuSwipeDismissLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mVelocityTracker == null || !this.mMenuCanDrag) {
            return super.onTouchEvent(ev);
        }
        switch (ev.getActionMasked()) {
            case 1:
                updateDismiss(ev);
                if (this.mDismissed) {
                    dismiss();
                } else if (this.mSwiping) {
                    cancel();
                }
                this.mMenuController.onTouchEventUp(ev, this.mDownY, this.mMinFlingVelocity, this.mVelocityTracker, this.mAnimationTime);
                resetMembers();
                break;
            case 2:
                this.mVelocityTracker.addMovement(ev);
                updateSwiping(ev);
                if (!this.mSwiping) {
                    this.mMenuController.setTranslationY(ev, this.mDownY, this.mLastY);
                    this.mLastX = ev.getRawX();
                    this.mLastY = ev.getRawY();
                    break;
                }
                if (getContext() instanceof Activity) {
                    ((Activity) getContext()).convertToTranslucent(null, null);
                }
                setProgress(ev.getRawX() - this.mDownX);
                break;
            case 3:
                this.mMenuController.onTouchEventCancel();
                cancel();
                resetMembers();
                break;
        }
        return true;
    }

    private void setProgress(float deltaX) {
        this.mTranslationX = deltaX;
        if (this.mProgressListener != null && deltaX >= 0.0f) {
            this.mProgressListener.onSwipeProgressChanged(this, deltaX / ((float) getWidth()), deltaX);
        }
    }

    private void dismiss() {
        if (this.mDismissedListener != null) {
            this.mDismissedListener.onDismissed(this);
        }
    }

    protected void cancel() {
        if (this.mProgressListener != null) {
            this.mProgressListener.onSwipeCancelled(this);
        }
    }

    private void resetMembers() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
        }
        this.mVelocityTracker = null;
        this.mTranslationX = 0.0f;
        this.mDownX = 0.0f;
        this.mDownY = 0.0f;
        this.mSwiping = false;
        this.mDismissed = false;
        this.mDiscardIntercept = false;
    }

    private void updateSwiping(MotionEvent ev) {
        float deltaX = ev.getRawX() - this.mDownX;
        float deltaY = ev.getRawY() - this.mDownY;
        if (this.mMenuController.mMenuState != 0 && deltaY >= 0.0f) {
            this.mDiscardIntercept = true;
        }
        if (!this.mSwiping && this.mMenuController.mMenuState == 0) {
            if ((deltaX * deltaX) + (deltaY * deltaY) > ((float) (this.mSlop * this.mSlop))) {
                if (deltaY > ((float) (this.mSlop * 2)) && Math.abs(deltaX) < ((float) (this.mSlop * 2)) && this.mMenuController.mIsAllowedToDragDown) {
                    this.mMenuController.bringFront();
                }
                if (DEBUG) {
                    Log.e("MenuSwipeDismissLayout", "updateSwiping. deltaX:" + deltaX + " deltaY:" + deltaY + " slop:" + this.mSlop + " mSwiping:" + this.mSwiping);
                    return;
                }
                return;
            }
            this.mSwiping = false;
        }
    }

    private void updateDismiss(MotionEvent ev) {
        float deltaX = ev.getRawX() - this.mDownX;
        if (!this.mDismissed) {
            this.mVelocityTracker.addMovement(ev);
            this.mVelocityTracker.computeCurrentVelocity(1000);
            if (deltaX > ((float) getWidth()) * 0.33f && ev.getRawX() >= this.mLastX) {
                this.mDismissed = true;
            }
        }
        if (this.mDismissed && this.mSwiping && deltaX < ((float) getWidth()) * 0.33f) {
            this.mDismissed = false;
        }
    }

    protected boolean canScroll(View v, boolean checkV, float dx, float x, float y) {
        if (v instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) v;
            int scrollX = v.getScrollX();
            int scrollY = v.getScrollY();
            for (int i = group.getChildCount() - 1; i >= 0; i--) {
                View child = group.getChildAt(i);
                if (((float) scrollX) + x >= ((float) child.getLeft()) && ((float) scrollX) + x < ((float) child.getRight()) && ((float) scrollY) + y >= ((float) child.getTop()) && ((float) scrollY) + y < ((float) child.getBottom())) {
                    if (canScroll(child, true, dx, (((float) scrollX) + x) - ((float) child.getLeft()), (((float) scrollY) + y) - ((float) child.getTop()))) {
                        return true;
                    }
                }
            }
        }
        return checkV && v.canScrollHorizontally((int) (-dx));
    }

    public MenuController getMenuController() {
        return this.mMenuController;
    }
}
