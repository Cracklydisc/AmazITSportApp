package com.huami.watch.menu;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewPropertyAnimator;
import clc.utils.venus.XViewWrapper;
import com.fox.venus.cutewheel.HMWearableWheelView;
import com.huami.watch.menu.IMenuControllContent.MenuCloseListener;
import com.huami.watch.menu.IMenuControllContent.MenuListenner;
import com.huami.watch.ui.C1070R;
import java.util.ArrayList;

public class NewMenuLayout extends XViewWrapper implements IMenuControllContent {
    boolean inited;
    MenuAdapter mActionsAdapter;
    private MenuCloseListener mMenuCloserListener;
    boolean mMenuEnable;
    private MenuListenner mMenuListener;
    HMWearableWheelView mMenuWheel;
    private DataSetObserver mObserver;
    private SimpleMenuAdpter mSimpleAdpter;
    private int mTitleResId;

    public static abstract class SimpleMenuAdpter {
        protected final ArrayList<DataSetObserver> mObservers = new ArrayList();

        public abstract int getCount();

        public abstract MenuInfo getItemMenu(int i);

        public abstract void omMenuItemClick(int i);

        public void registerDataSetObserver(DataSetObserver observer) {
            if (observer == null) {
                throw new IllegalArgumentException("The observer is null.");
            }
            synchronized (this.mObservers) {
                if (this.mObservers.contains(observer)) {
                    throw new IllegalStateException("Observer " + observer + " is already registered.");
                }
                this.mObservers.add(observer);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer == null) {
                throw new IllegalArgumentException("The observer is null.");
            }
            synchronized (this.mObservers) {
                int index = this.mObservers.indexOf(observer);
                if (index == -1) {
                    throw new IllegalStateException("Observer " + observer + " was not registered.");
                }
                this.mObservers.remove(index);
            }
        }

        public void notifyDataChanged() {
            synchronized (this.mObservers) {
                for (int i = this.mObservers.size() - 1; i >= 0; i--) {
                    ((DataSetObserver) this.mObservers.get(i)).onChanged();
                }
            }
        }
    }

    public static class MenuInfo {
        public String content;
        public Bitmap iconBitmap;
        public int iconResId;
        public Object tag;
        public String title;
        public float titleSize;
        public float valueSize;
    }

    private class SimpleDataSetObserver extends DataSetObserver {
        private SimpleDataSetObserver() {
        }

        public void onChanged() {
            NewMenuLayout.this.notifyDataChanged();
        }
    }

    public NewMenuLayout(Context context) {
        this(context, null);
    }

    public NewMenuLayout(Context context, AttributeSet attrs) {
        this(context, attrs, C1070R.string.title_settings);
    }

    public NewMenuLayout(Context context, AttributeSet attrs, int titleResId) {
        super(context, attrs);
        this.mMenuEnable = false;
        this.mObserver = null;
        this.inited = false;
        this.mObserver = new SimpleDataSetObserver();
        this.mTitleResId = titleResId;
        initMenu();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        initMenu();
    }

    private void initMenu() {
        setBackgroundDrawable(new ColorDrawable(-16777216));
        int marginTop = getResources().getDimensionPixelSize(C1070R.dimen.menu_item_layout_margin_top);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        RectF region = new RectF(0.0f, 0.0f, (float) dm.widthPixels, (float) dm.widthPixels);
        if (this.mMenuWheel == null) {
            this.mMenuWheel = new HMWearableWheelView((XViewWrapper) this, region);
            this.mMenuWheel.setMarginTop(marginTop);
            addDrawingItem(this.mMenuWheel);
            this.mActionsAdapter = new MenuAdapter(this, this.mTitleResId);
            this.mMenuWheel.setViewAdapter(this.mActionsAdapter);
        }
        this.mActionsAdapter.setSimpleMenuAdapter(this.mSimpleAdpter);
        if (this.inited) {
            this.mMenuWheel.standup(false, false);
        } else {
            this.inited = true;
            this.mMenuWheel.standup(false, true);
            this.mMenuWheel.setCurrentItem(1);
        }
        invalidate();
    }

    public boolean isListCanScroll() {
        return !this.mMenuWheel.isViewBottomEdged();
    }

    public void setSimpleMenuAdapter(SimpleMenuAdpter adpter) {
        if (!(this.mSimpleAdpter == null || adpter == this.mSimpleAdpter)) {
            this.mSimpleAdpter.unregisterDataSetObserver(this.mObserver);
        }
        if (adpter != this.mSimpleAdpter) {
            this.mSimpleAdpter = adpter;
            this.mSimpleAdpter.registerDataSetObserver(this.mObserver);
            initMenu();
            notifyDataChanged();
        }
    }

    public int getMenuHeight() {
        return getMeasuredHeight();
    }

    public void offsetMenu(int i) {
        offsetTopAndBottom(i);
    }

    public ViewPropertyAnimator animateMenu() {
        return animate();
    }

    public float getMenuTranslationY() {
        return getTranslationY();
    }

    public void scrollToTop() {
        this.mMenuWheel.setCurrentItem(0, true);
    }

    public void bringMenuToFront() {
        bringToFront();
    }

    public void setMenuTranslationY(float f) {
        setTranslationY(f);
    }

    public void performCloseListener() {
        if (this.mMenuListener != null) {
            this.mMenuListener.onMenuClose();
        } else if (this.mMenuCloserListener != null) {
            this.mMenuCloserListener.onMenuClose();
        }
    }

    public void notifyDataChanged() {
        this.mMenuWheel.standup(false, false);
        this.mMenuWheel.doScroll(0.0f);
    }

    public void scrollMenu(float offset) {
        this.mMenuWheel.doScroll(offset);
    }

    public void setScrollToCurrentItem(int position) {
        this.mMenuWheel.setCurrentItem(position, true);
    }

    public float canScrollMenuUp() {
        return this.mMenuWheel.canScrollUp();
    }

    public void notifyMenuJustPosition() {
        this.mMenuWheel.justify();
    }

    public void showScrollBar() {
        this.mMenuWheel.showScrollBar();
    }

    public boolean isVisible() {
        return getVisibility() == 0;
    }

    public void setVisible(boolean visible) {
        if (visible) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    public void performOpenListener() {
        if (this.mMenuListener != null) {
            this.mMenuListener.onMenuOpen();
        }
    }
}
