package com.huami.watch.menu;

import android.annotation.SuppressLint;
import android.support.wearable.view.WearableListView;
import android.support.wearable.view.WearableListView.Adapter;
import android.support.wearable.view.WearableListView.ClickListener;
import android.support.wearable.view.WearableListView.OnScrollListener;
import android.support.wearable.view.WearableListView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.huami.watch.menu.IMenuControllContent.MenuOnItemClickListener;
import com.huami.watch.menu.IMenuControllContent.MenuonCenterProximityListener;
import java.util.HashMap;
import java.util.List;

@SuppressLint({"NewApi"})
public abstract class MenuWearLayout extends LinearLayout implements IMenuControllContent {
    HashMap<Integer, Integer> itemMap;
    public boolean mListCanScroll;
    WearableListView mListView;
    private MenuOnItemClickListener mListener;
    boolean mMenuEnable;
    private MenuonCenterProximityListener mProximityListener;
    List<Integer> maps;

    class C05291 implements ClickListener {
        final /* synthetic */ MenuWearLayout this$0;

        public void onTopEmptyRegionClick() {
        }

        public void onClick(ViewHolder viewHolder) {
            if (this.this$0.mListener != null) {
                int menuId = ((Integer) this.this$0.maps.get(viewHolder.getPosition())).intValue();
                this.this$0.mListener.onItemClick(menuId, ((Integer) this.this$0.itemMap.get(Integer.valueOf(menuId))).intValue(), viewHolder.itemView);
            }
        }
    }

    class C05302 implements OnScrollListener {
        int mCentralPosition;
        final /* synthetic */ MenuWearLayout this$0;

        public void onAbsoluteScrollChange(int absolute) {
            Log.i("menuwear", "onAbsoluteScrollChange" + absolute + ">>" + (this.this$0.mListView.getChildAt(0).getHeight() * (this.this$0.maps.size() - 2)));
        }

        public void onCentralPositionChanged(int centralPosition) {
            this.mCentralPosition = centralPosition;
            Log.i("menuwear", "onCentralPositionChanged>>>>>>>>>>>>>>" + centralPosition);
        }

        public void onScroll(int scroll) {
            Log.i("menuwear", "onScroll>>" + scroll);
            if (this.this$0.maps.size() - 1 == this.mCentralPosition && scroll >= 0) {
                this.this$0.setListCanScrollFlag(false);
                Log.i("menuwear", ">>false");
            }
        }

        public void onScrollStateChanged(int scrollState) {
            if (scrollState == 1) {
                this.this$0.setListCanScrollFlag(true);
                Log.i("menuwear", ">>true");
            }
            Log.i("menuwear", "onScrollStateChanged>>" + scrollState);
        }
    }

    private class MyWearAdapter extends Adapter {
        private final LayoutInflater mInflater;
        final /* synthetic */ MenuWearLayout this$0;

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new mViewHolder(this.mInflater.inflate(viewType, null));
        }

        public void onBindViewHolder(ViewHolder holder, int position) {
            int menuId = ((Integer) this.this$0.maps.get(position)).intValue();
            this.this$0.setMenuItemContent(holder.itemView, menuId, ((Integer) this.this$0.itemMap.get(Integer.valueOf(menuId))).intValue());
            holder.itemView.setTag(Integer.valueOf(menuId));
        }

        public int getItemCount() {
            return this.this$0.maps.size();
        }

        public long getItemId(int position) {
            return (long) ((Integer) this.this$0.maps.get(position)).intValue();
        }

        public int getItemViewType(int position) {
            return ((Integer) this.this$0.itemMap.get(this.this$0.maps.get(position))).intValue();
        }
    }

    private class mViewHolder extends ViewHolder {
        public mViewHolder(View itemView) {
            super(itemView);
        }

        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (MenuWearLayout.this.mProximityListener != null) {
                int menuId = ((Integer) MenuWearLayout.this.maps.get(MenuWearLayout.this.mListView.getChildAdapterPosition(this.itemView))).intValue();
                MenuWearLayout.this.mProximityListener.onCenterProximity(this.itemView, isCentralItem, menuId, ((Integer) MenuWearLayout.this.itemMap.get(Integer.valueOf(menuId))).intValue());
            }
        }
    }

    public void setMenuItemContent(View clickView, int menuId, int menuLayout) {
    }

    public void setListCanScrollFlag(boolean b) {
        this.mListCanScroll = b;
    }

    public boolean isEnabled() {
        return this.mMenuEnable;
    }
}
