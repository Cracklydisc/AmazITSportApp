package com.huami.watch.keyevent_lib;

import android.app.Activity;
import android.app.Dialog;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

public class KeyEventDispatcher {
    public void dispatchKeyevent(Activity activity, KeyEvent event) {
        if (activity != null) {
            dispatchKeyevent(activity.getWindow().getDecorView(), event);
        }
    }

    public void dispatchKeyevent(Dialog dialog, KeyEvent event) {
        if (dialog != null) {
            dispatchKeyevent(dialog.getWindow().getDecorView(), event);
        }
    }

    public void dispatchKeyevent(View view, KeyEvent event) {
        if (view instanceof ViewGroup) {
            if (view != null && view.getVisibility() == 0) {
                if (isExpectedView(view)) {
                    inject2Target(view, event);
                }
                ViewGroup vg = (ViewGroup) view;
                for (int i = vg.getChildCount() - 1; i >= 0; i--) {
                    dispatchKeyevent(vg.getChildAt(i), event);
                }
            }
        } else if (view != null && view.getVisibility() == 0 && isExpectedView(view)) {
            inject2Target(view, event);
        }
    }

    private void inject2Target(Object consumer, KeyEvent event) {
        Object accept = ReflectUtil.invoke(consumer, "canAccept", true, new Class[0], new Object[0]);
        if (accept != null && ((Boolean) accept).booleanValue()) {
            ReflectUtil.invoke(consumer, "injectKeyevent", true, new Class[]{KeyEvent.class}, event);
        }
    }

    private boolean isExpectedView(View v) {
        Class[] interfaces = v.getClass().getInterfaces();
        if (interfaces == null) {
            return false;
        }
        for (Class c : interfaces) {
            if (TextUtils.equals(c.getName(), KeyeventConsumer.class.getName())) {
                return true;
            }
        }
        return false;
    }
}
