package com.huami.watch.keyevent_lib;

import android.view.KeyEvent;

public interface KeyeventConsumer {
    boolean canAccept();

    void injectKeyevent(KeyEvent keyEvent);
}
