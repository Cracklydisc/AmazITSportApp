package com.huami.watch.keyevent_lib;

import android.view.KeyEvent;

public class HMKeyDef {

    public enum HMKeyEvent {
        KEY_UP(135),
        KEY_CENTER(140),
        KEY_DOWN(136),
        KEY_NOT_DEF(-1);
        
        private boolean attached;
        private KeyEvent theArdEvent;
        private int theArdKeyCode;

        private HMKeyEvent(int code) {
            this.theArdKeyCode = code;
        }

        public void attachKeyevent(KeyEvent event) {
            this.theArdKeyCode = event == null ? KEY_NOT_DEF.realAndKeyCode() : event.getKeyCode();
            this.theArdEvent = event;
        }

        public int realAndKeyCode() {
            if (this.attached) {
            }
            return this.theArdKeyCode;
        }
    }

    private HMKeyDef() {
    }

    public static HMKeyEvent translate(KeyEvent event) {
        for (HMKeyEvent the : HMKeyEvent.values()) {
            if (event.getKeyCode() == the.realAndKeyCode()) {
                the.attachKeyevent(event);
                return the;
            }
        }
        HMKeyEvent.KEY_NOT_DEF.attachKeyevent(event);
        return HMKeyEvent.KEY_NOT_DEF;
    }

    public static void dettachAll() {
    }
}
