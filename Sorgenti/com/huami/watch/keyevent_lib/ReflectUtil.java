package com.huami.watch.keyevent_lib;

import android.text.TextUtils;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

public final class ReflectUtil {
    private static final String TAG = ReflectUtil.class.getSimpleName();
    private static ConcurrentHashMap<String, Method> mMethodCacheList = new ConcurrentHashMap();

    public static Object invoke(Object theInstance, String methodName, boolean checkSuper, Class<?>[] methodParamType, Object... params) {
        return invoke(theInstance.getClass(), theInstance, methodName, checkSuper, methodParamType, params);
    }

    public static Object invoke(Class<?> theClazz, Object theInstance, String methodName, boolean checkSuper, Class<?>[] methodParamType, Object... params) {
        if (theClazz == null || theInstance == null || TextUtils.isEmpty(methodName)) {
            return null;
        }
        try {
            String key = buildCacheKey(theClazz, methodName, methodParamType, checkSuper);
            Method method = tryGetFromCache(key);
            if (method != null) {
                return method.invoke(theInstance, params);
            }
            method = getMethod(theClazz, methodName, methodParamType, checkSuper);
            if (method == null) {
                return null;
            }
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            Object returnObj = method.invoke(theInstance, params);
            mMethodCacheList.put(key, method);
            return returnObj;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return null;
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public static Method getMethod(Class<?> clazz, String methodName, Class<?>[] methodParamType, boolean checkSuperClass) {
        Method method = null;
        if (checkSuperClass) {
            Class<?> c = clazz;
            while (c != Object.class) {
                try {
                    method = c.getDeclaredMethod(methodName, methodParamType);
                    if (method != null) {
                        break;
                    }
                    c = c.getSuperclass();
                } catch (NoSuchMethodException e) {
                }
            }
        } else {
            try {
                method = clazz.getDeclaredMethod(methodName, methodParamType);
            } catch (NoSuchMethodException e2) {
                try {
                    method = clazz.getMethod(methodName, methodParamType);
                } catch (NoSuchMethodException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return method;
    }

    private static Method tryGetFromCache(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        return (Method) mMethodCacheList.get(key);
    }

    private static String buildCacheKey(Class<?> theClazz, String methodName, Class<?>[] methodParamType, boolean checkSuper) {
        StringBuilder s = new StringBuilder();
        s.append(theClazz.hashCode());
        s.append(methodName.hashCode());
        for (Class o : methodParamType) {
            s.append(o.hashCode());
        }
        return String.valueOf(s.toString().hashCode());
    }
}
