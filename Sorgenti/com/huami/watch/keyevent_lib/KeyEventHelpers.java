package com.huami.watch.keyevent_lib;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.KeyEvent;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import java.util.concurrent.ConcurrentHashMap;

public final class KeyEventHelpers {
    private EventCallBack callBack;
    private Handler handler;
    private HandlerThread handlerThread = new HandlerThread("key_event_launcher");
    private ConcurrentHashMap<Integer, Key> keyList = new ConcurrentHashMap();
    private TaskManager mainTask = new TaskManager("key_inject");

    public interface EventCallBack {
        boolean onKeyClick(HMKeyEvent hMKeyEvent);

        boolean onKeyLongOneSecond(HMKeyEvent hMKeyEvent);

        boolean onKeyLongOneSecondTimeOut(HMKeyEvent hMKeyEvent);

        boolean onKeyLongThreeSecond(HMKeyEvent hMKeyEvent);

        boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hMKeyEvent);
    }

    class Key {
        private Runnable currentRunnable;
        private EventCallBack eventCallBack;
        private boolean isShortContinue;
        public boolean isUpAlready;
        public int keyCode;
        public KeyEvent keyEvent;
        private Runnable onLongEvent1;
        private Runnable onLongEvent3;
        private Runnable onShortEvent;

        class C05141 implements Runnable {
            C05141() {
            }

            public void run() {
                if (Key.this.isUpAlready) {
                    KeyEventHelpers.this.mainTask.next(new Task(RunningStatus.UI_THREAD) {
                        public TaskOperation onExecute(TaskOperation taskOperation) {
                            Key.this.isShortContinue = Key.this.eventCallBack.onKeyClick(HMKeyDef.translate(Key.this.keyEvent));
                            return null;
                        }
                    }).execute();
                    Key key = (Key) KeyEventHelpers.this.keyList.get(Integer.valueOf(Key.this.keyCode));
                    if (key != null) {
                        Log.i("KEY_HELPER_DEBUG", key.hashCode() + "");
                        key.shutDown();
                        KeyEventHelpers.this.keyList.remove(Integer.valueOf(Key.this.keyCode));
                        return;
                    }
                    return;
                }
                KeyEventHelpers.this.handler.postDelayed(Key.this.onLongEvent1, 100);
                Key.this.currentRunnable = Key.this.onLongEvent1;
            }
        }

        class C05172 implements Runnable {
            C05172() {
            }

            public void run() {
                if (Key.this.isUpAlready) {
                    KeyEventHelpers.this.mainTask.next(new Task(RunningStatus.UI_THREAD) {
                        public TaskOperation onExecute(TaskOperation taskOperation) {
                            Key.this.isShortContinue = Key.this.eventCallBack.onKeyLongOneSecond(HMKeyDef.translate(Key.this.keyEvent));
                            return null;
                        }
                    }).execute();
                    Key key = (Key) KeyEventHelpers.this.keyList.get(Integer.valueOf(Key.this.keyCode));
                    if (key != null) {
                        Log.i("KEY_HELPER_DEBUG", key.hashCode() + "");
                        key.shutDown();
                        KeyEventHelpers.this.keyList.remove(Integer.valueOf(Key.this.keyCode));
                        return;
                    }
                    return;
                }
                KeyEventHelpers.this.mainTask.next(new Task(RunningStatus.UI_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        Key.this.isShortContinue = Key.this.eventCallBack.onKeyLongOneSecondTimeOut(HMKeyDef.translate(Key.this.keyEvent));
                        return null;
                    }
                }).execute();
                KeyEventHelpers.this.handler.postDelayed(Key.this.onLongEvent3, 2000);
                Key.this.currentRunnable = Key.this.onLongEvent3;
            }
        }

        class C05203 implements Runnable {
            C05203() {
            }

            public void run() {
                if (Key.this.isUpAlready) {
                    KeyEventHelpers.this.mainTask.next(new Task(RunningStatus.UI_THREAD) {
                        public TaskOperation onExecute(TaskOperation taskOperation) {
                            Key.this.isShortContinue = Key.this.eventCallBack.onKeyLongThreeSecond(HMKeyDef.translate(Key.this.keyEvent));
                            return null;
                        }
                    }).execute();
                    Key key = (Key) KeyEventHelpers.this.keyList.get(Integer.valueOf(Key.this.keyCode));
                    if (key != null) {
                        key.shutDown();
                        KeyEventHelpers.this.keyList.remove(Integer.valueOf(Key.this.keyCode));
                    }
                } else {
                    KeyEventHelpers.this.mainTask.next(new Task(RunningStatus.UI_THREAD) {
                        public TaskOperation onExecute(TaskOperation taskOperation) {
                            Key.this.isShortContinue = Key.this.eventCallBack.onKeyLongThreeSecondTimeOut(HMKeyDef.translate(Key.this.keyEvent));
                            return null;
                        }
                    }).execute();
                    Key.this.currentRunnable = null;
                }
                Key.this.shutDown();
            }
        }

        public void setUpAlready(boolean upAlready) {
            this.isUpAlready = upAlready;
        }

        public Key(KeyEvent event, EventCallBack callback) {
            this.keyCode = event.getKeyCode();
            this.eventCallBack = callback;
            this.keyEvent = event;
            init();
        }

        public Runnable getCurrentRunnable() {
            return this.currentRunnable;
        }

        private void init() {
            this.onShortEvent = new C05141();
            this.onLongEvent1 = new C05172();
            this.onLongEvent3 = new C05203();
            KeyEventHelpers.this.handler.postDelayed(this.onShortEvent, 500);
            this.currentRunnable = this.onShortEvent;
        }

        public void shutDown() {
            Log.i("eventCallBack", "shutDown:::" + KeyEventHelpers.getText(this.keyCode));
            KeyEventHelpers.this.handler.removeCallbacksAndMessages(null);
        }
    }

    public KeyEventHelpers(EventCallBack callBack) {
        this.callBack = callBack;
        this.handlerThread.start();
        this.handler = new Handler(this.handlerThread.getLooper());
    }

    public void eventKeyDown(KeyEvent keyEvent) {
        if (isSystemFlag(keyEvent)) {
            int keycode = keyEvent.getKeyCode();
            if (this.keyList.contains(Integer.valueOf(keycode))) {
                this.keyList.remove(Integer.valueOf(keycode));
            }
            if (keycode != 4) {
                this.keyList.put(Integer.valueOf(keycode), new Key(keyEvent, this.callBack));
            }
        }
    }

    public void eventKeyUp(KeyEvent keyEvent) {
        if (isSystemFlag(keyEvent) && this.keyList != null) {
            int keycode = keyEvent.getKeyCode();
            Key key = (Key) this.keyList.get(Integer.valueOf(keycode));
            if (!(key == null || key.getCurrentRunnable() == null)) {
                ((Key) this.keyList.get(Integer.valueOf(keycode))).setUpAlready(true);
                this.handler.removeCallbacks(key.getCurrentRunnable());
                if (key.getCurrentRunnable() != null) {
                    this.handler.post(key.getCurrentRunnable());
                }
            }
            HMKeyDef.dettachAll();
        }
    }

    private boolean isSystemFlag(KeyEvent keyEvent) {
        return keyEvent != null && keyEvent.getFlags() == 8;
    }

    public static String getText(int keycode) {
        if (keycode == 135) {
            return "KEYCODE_F5";
        }
        if (keycode == 136) {
            return "KEYCODE_F5";
        }
        if (keycode == 140) {
            return "F10";
        }
        return keycode + "";
    }
}
