package com.huami.watch.keyevent_lib;

import android.util.Log;
import android.view.KeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;

public class KeyeventProcessor {
    private String KeyEventTAG = "KeyeventProcessor";
    private EventCallBack eventCallBack;
    private KeyEventHelpers keyEventHelpers;

    public KeyeventProcessor(EventCallBack eventCallBack) {
        this.eventCallBack = eventCallBack;
    }

    public void injectKeyEvent(KeyEvent event) {
        if (event.getAction() == 1) {
            onKeyUp(event);
        } else if (event.getAction() == 0) {
            onKeyDown(event);
        } else {
            throw new RuntimeException("only support action up and action down");
        }
    }

    private void onKeyDown(KeyEvent event) {
        Log.i(this.KeyEventTAG, "onKeyDown:::" + event.getKeyCode() + "---" + event.toString());
        if (event.getRepeatCount() <= 0) {
            if (this.keyEventHelpers == null) {
                this.keyEventHelpers = new KeyEventHelpers(this.eventCallBack);
            }
            this.keyEventHelpers.eventKeyDown(event);
        }
    }

    private void onKeyUp(KeyEvent event) {
        Log.i(this.KeyEventTAG, "onKeyUp:::" + event.getKeyCode() + "---" + event.toString());
        if (this.keyEventHelpers == null) {
            this.keyEventHelpers = new KeyEventHelpers(this.eventCallBack);
        }
        this.keyEventHelpers.eventKeyUp(event);
    }

    public void setKeyeventListener(EventCallBack callbak) {
        this.eventCallBack = callbak;
    }
}
