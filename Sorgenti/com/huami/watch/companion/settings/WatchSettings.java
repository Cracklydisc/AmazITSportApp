package com.huami.watch.companion.settings;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class WatchSettings {

    public static class SettingsEntry {
        public static final String[] COLUMNS_ALL = new String[]{"key", "value", "cloud_sync_state", "watch_sync_state"};
        public static final String[] COLUMNS_EMPTY = new String[0];
        public static final String[] COLUMNS_KEY_VALUE = new String[]{"key", "value"};
        public static final String[] COLUMNS_VALUE = new String[]{"value"};
        public static final Uri CONTENT_URI = Uri.parse("content://com.huami.watch.companion.settings");
    }

    public static boolean put(ContentResolver resolver, String key, String value) {
        return put(resolver, key, value, null);
    }

    public static boolean put(ContentResolver resolver, String key, String value, ContentObserver observer) {
        boolean success = false;
        ContentValues values = new ContentValues();
        values.put("key", key);
        values.put("value", value);
        values.put("cloud_sync_state", Integer.valueOf(0));
        Cursor c = null;
        try {
            c = resolver.query(SettingsEntry.CONTENT_URI, SettingsEntry.COLUMNS_EMPTY, "key=?", new String[]{key}, null);
            if (c == null || !c.moveToFirst()) {
                if (Log.isLoggable("UserSettings-Watch", 3)) {
                    Log.d("UserSettings-Watch", "Insert Values : " + values.toString());
                }
                success = resolver.insert(SettingsEntry.CONTENT_URI, values) != null;
            } else {
                if (Log.isLoggable("UserSettings-Watch", 3)) {
                    Log.d("UserSettings-Watch", "Update Values : " + values.toString());
                }
                success = resolver.update(SettingsEntry.CONTENT_URI, values, "key=?", new String[]{key}) > 0;
            }
            if (success) {
                Log.d("UserSettings-Watch", "Put Success, NotifyChange now!!");
                resolver.notifyChange(SettingsEntry.CONTENT_URI, observer);
            }
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
        }
        return success;
    }

    public static String get(ContentResolver resolver, String key) {
        String value = null;
        Cursor c = null;
        try {
            ContentResolver contentResolver = resolver;
            c = contentResolver.query(SettingsEntry.CONTENT_URI, SettingsEntry.COLUMNS_VALUE, "key=?", new String[]{key}, null);
            if (c != null && c.moveToFirst()) {
                value = c.getString(0);
            }
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
        }
        if (Log.isLoggable("UserSettings-Watch", 3)) {
            Log.d("UserSettings-Watch", "Get : " + key + ", " + value);
        }
        return value;
    }
}
