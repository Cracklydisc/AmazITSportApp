package com.huami.watch.sensorhub;

import com.google.protobuf.nano.CodedInputByteBufferNano;
import com.google.protobuf.nano.CodedOutputByteBufferNano;
import com.google.protobuf.nano.ExtendableMessageNano;
import com.google.protobuf.nano.InvalidProtocolBufferNanoException;
import com.google.protobuf.nano.MessageNano;
import com.google.protobuf.nano.WireFormatNano;
import java.io.IOException;

public interface SensorHubProtos {

    public static final class RiddingInfo extends ExtendableMessageNano<RiddingInfo> {
        private int bitField0_;
        private float mDistance3DAscend_;
        private float mDistance3DDescend_;
        private float mDistance3DFlat_;
        private float mSpeed3D_;
        private int mTimeAscend_;
        private int mTimeDescend_;
        private int mTimeFlat_;

        public float getMDistance3DAscend() {
            return this.mDistance3DAscend_;
        }

        public int getMTimeAscend() {
            return this.mTimeAscend_;
        }

        public float getMDistance3DDescend() {
            return this.mDistance3DDescend_;
        }

        public int getMTimeDescend() {
            return this.mTimeDescend_;
        }

        public RiddingInfo() {
            clear();
        }

        public RiddingInfo clear() {
            this.bitField0_ = 0;
            this.mDistance3DAscend_ = 0.0f;
            this.mSpeed3D_ = 0.0f;
            this.mTimeAscend_ = 0;
            this.mDistance3DDescend_ = 0.0f;
            this.mTimeDescend_ = 0;
            this.mDistance3DFlat_ = 0.0f;
            this.mTimeFlat_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeFloat(1, this.mDistance3DAscend_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeFloat(2, this.mSpeed3D_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.mTimeAscend_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeFloat(4, this.mDistance3DDescend_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeUInt32(5, this.mTimeDescend_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeFloat(6, this.mDistance3DFlat_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeUInt32(7, this.mTimeFlat_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(1, this.mDistance3DAscend_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(2, this.mSpeed3D_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.mTimeAscend_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(4, this.mDistance3DDescend_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(5, this.mTimeDescend_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(6, this.mDistance3DFlat_);
            }
            if ((this.bitField0_ & 64) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(7, this.mTimeFlat_);
            }
            return size;
        }

        public RiddingInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 13:
                        this.mDistance3DAscend_ = input.readFloat();
                        this.bitField0_ |= 1;
                        continue;
                    case 21:
                        this.mSpeed3D_ = input.readFloat();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.mTimeAscend_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 37:
                        this.mDistance3DDescend_ = input.readFloat();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.mTimeDescend_ = input.readUInt32();
                        this.bitField0_ |= 16;
                        continue;
                    case 53:
                        this.mDistance3DFlat_ = input.readFloat();
                        this.bitField0_ |= 32;
                        continue;
                    case 56:
                        this.mTimeFlat_ = input.readUInt32();
                        this.bitField0_ |= 64;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SenorAlgoSetting extends ExtendableMessageNano<SenorAlgoSetting> {
        private int bitField0_;
        private int mMainVersion_;
        private int mMinorVersion_;
        private int mReleaseVersion_;

        public SenorAlgoSetting() {
            clear();
        }

        public SenorAlgoSetting clear() {
            this.bitField0_ = 0;
            this.mMainVersion_ = 0;
            this.mMinorVersion_ = 0;
            this.mReleaseVersion_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.mMainVersion_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt32(2, this.mMinorVersion_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.mReleaseVersion_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.mMainVersion_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(2, this.mMinorVersion_);
            }
            if ((this.bitField0_ & 4) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(3, this.mReleaseVersion_);
            }
            return size;
        }

        public SenorAlgoSetting mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.mMainVersion_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.mMinorVersion_ = input.readUInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.mReleaseVersion_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportAcmInfo extends ExtendableMessageNano<SportAcmInfo> {
        private int bitField0_;
        private float mAscendMeter_;
        private float mBasicCalories_;
        private float mDescendMeter_;
        private float mDistance_;
        private float mSportCalories_;
        private int mStep_;
        private float mTimer_;

        public float getMTimer() {
            return this.mTimer_;
        }

        public float getMDistance() {
            return this.mDistance_;
        }

        public int getMStep() {
            return this.mStep_;
        }

        public float getMSportCalories() {
            return this.mSportCalories_;
        }

        public float getMAscendMeter() {
            return this.mAscendMeter_;
        }

        public float getMDescendMeter() {
            return this.mDescendMeter_;
        }

        public SportAcmInfo() {
            clear();
        }

        public SportAcmInfo clear() {
            this.bitField0_ = 0;
            this.mTimer_ = 0.0f;
            this.mDistance_ = 0.0f;
            this.mStep_ = 0;
            this.mSportCalories_ = 0.0f;
            this.mBasicCalories_ = 0.0f;
            this.mAscendMeter_ = 0.0f;
            this.mDescendMeter_ = 0.0f;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeFloat(1, this.mTimer_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeFloat(2, this.mDistance_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.mStep_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeFloat(4, this.mSportCalories_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeFloat(5, this.mBasicCalories_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeFloat(6, this.mAscendMeter_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeFloat(7, this.mDescendMeter_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(1, this.mTimer_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(2, this.mDistance_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.mStep_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(4, this.mSportCalories_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(5, this.mBasicCalories_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(6, this.mAscendMeter_);
            }
            if ((this.bitField0_ & 64) != 0) {
                return size + CodedOutputByteBufferNano.computeFloatSize(7, this.mDescendMeter_);
            }
            return size;
        }

        public SportAcmInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 13:
                        this.mTimer_ = input.readFloat();
                        this.bitField0_ |= 1;
                        continue;
                    case 21:
                        this.mDistance_ = input.readFloat();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.mStep_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 37:
                        this.mSportCalories_ = input.readFloat();
                        this.bitField0_ |= 8;
                        continue;
                    case 45:
                        this.mBasicCalories_ = input.readFloat();
                        this.bitField0_ |= 16;
                        continue;
                    case 53:
                        this.mAscendMeter_ = input.readFloat();
                        this.bitField0_ |= 32;
                        continue;
                    case 61:
                        this.mDescendMeter_ = input.readFloat();
                        this.bitField0_ |= 64;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportAvgInfo extends ExtendableMessageNano<SportAvgInfo> {
        private int bitField0_;
        private float mAltitude_;
        private float mCadence_;
        private int mHeartRate_;
        private float mPace_;
        private float mSpeed_;
        private float mVerticalSpeed_;

        public float getMSpeed() {
            return this.mSpeed_;
        }

        public float getMPace() {
            return this.mPace_;
        }

        public float getMCadence() {
            return this.mCadence_;
        }

        public int getMHeartRate() {
            return this.mHeartRate_;
        }

        public float getMAltitude() {
            return this.mAltitude_;
        }

        public float getMVerticalSpeed() {
            return this.mVerticalSpeed_;
        }

        public SportAvgInfo() {
            clear();
        }

        public SportAvgInfo clear() {
            this.bitField0_ = 0;
            this.mSpeed_ = 0.0f;
            this.mPace_ = 0.0f;
            this.mCadence_ = 0.0f;
            this.mHeartRate_ = 0;
            this.mAltitude_ = 0.0f;
            this.mVerticalSpeed_ = 0.0f;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeFloat(1, this.mSpeed_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeFloat(2, this.mPace_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeFloat(3, this.mCadence_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeUInt32(4, this.mHeartRate_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeFloat(5, this.mAltitude_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeFloat(6, this.mVerticalSpeed_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(1, this.mSpeed_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(2, this.mPace_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(3, this.mCadence_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(4, this.mHeartRate_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(5, this.mAltitude_);
            }
            if ((this.bitField0_ & 32) != 0) {
                return size + CodedOutputByteBufferNano.computeFloatSize(6, this.mVerticalSpeed_);
            }
            return size;
        }

        public SportAvgInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 13:
                        this.mSpeed_ = input.readFloat();
                        this.bitField0_ |= 1;
                        continue;
                    case 21:
                        this.mPace_ = input.readFloat();
                        this.bitField0_ |= 2;
                        continue;
                    case 29:
                        this.mCadence_ = input.readFloat();
                        this.bitField0_ |= 4;
                        continue;
                    case 32:
                        this.mHeartRate_ = input.readUInt32();
                        this.bitField0_ |= 8;
                        continue;
                    case 45:
                        this.mAltitude_ = input.readFloat();
                        this.bitField0_ |= 16;
                        continue;
                    case 53:
                        this.mVerticalSpeed_ = input.readFloat();
                        this.bitField0_ |= 32;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportConfig extends ExtendableMessageNano<SportConfig> {
        private int bitField0_;
        private int bitField1_;
        private boolean mIs3DDistanceEnabled_;
        private boolean mIsAlldayAutoHeartRateEnabled_;
        private boolean mIsFlipEnabled_;
        private boolean mIsMeasureHrInCrossCountryRun_;
        private boolean mIsMedalTodayStepGoal1AlertEnabled_;
        private boolean mIsMedalTodayStepGoal2AlertEnabled_;
        private boolean mIsMedalTodayStepGoal3AlertEnabled_;
        private boolean mIsMedalTotalStepGoal1AlertEnabled_;
        private boolean mIsMedalTotalStepGoal2AlertEnabled_;
        private boolean mIsMedalTotalStepGoal3AlertEnabled_;
        private boolean mIsMedalTotalStepGoal4AlertEnabled_;
        private boolean mIsSafeHeartRateAlertEnabled_;
        private boolean mIsSedentaryMinAlertEnabled_;
        private boolean mIsSporKiloMeterAlertEnabled_;
        private boolean mIsSportAutoPauseEnabled_;
        private boolean mIsSportFastTargetPaceAlertEnabled_;
        private boolean mIsSportHrZoneAlertEnabled_;
        private boolean mIsSportLapMeterAlertEnabled_;
        private boolean mIsSportTargetCaloriesAlertEnabled_;
        private boolean mIsSportTargetDistanceAlertEnabled_;
        private boolean mIsSportTargetPaceAlertEnabled_;
        private boolean mIsSportTargetTimerAlertEnabled_;
        private boolean mIsTodayStepGoalAlertEnabled_;
        private boolean mIsUseThaWorkoutSet_;
        private boolean mIsWearDetectEnabled_;
        private int mMedalTodayStepGoal1_;
        private int mMedalTodayStepGoal2_;
        private int mMedalTodayStepGoal3_;
        private int mMedalTotalStepGoal1_;
        private int mMedalTotalStepGoal2_;
        private int mMedalTotalStepGoal3_;
        private int mMedalTotalStepGoal4_;
        private int mSedentaryMin_;
        private float mSportAutoPauseSpeedTh_;
        private float mSportCorrectDistance_;
        private float mSportFastTargetPace_;
        private float mSportHrZoneLower_;
        private float mSportHrZoneUpper_;
        private float mSportKiloMeter_;
        private float mSportLapMeter_;
        private int mSportSafeHeartRate_;
        private float mSportTargetCalories_;
        private float mSportTargetDistance_;
        private float mSportTargetPace_;
        private float mSportTargetTimer_;
        private int mSportTargetTrainingEffect_;
        public int[] mStepModel;
        private int mSwimPoolLen_;
        private int mTodayStepGoal_;
        private int mUserAge_;
        private float mUserBasicCalories_;
        private int mUserBodyFatRate_;
        private int mUserExerciseRate_;
        private float mUserHeightMeter_;
        private boolean mUserIsLeftHander_;
        private boolean mUserIsVeganism_;
        private boolean mUserIsWearOnLeft_;
        private int mUserSex_;
        private int mUserSkin_;
        private float mUserStepMeter_;
        private float mUserWeightKg_;

        public SportConfig setMTodayStepGoal(int value) {
            this.mTodayStepGoal_ = value;
            this.bitField0_ |= 1;
            return this;
        }

        public SportConfig setMIsTodayStepGoalAlertEnabled(boolean value) {
            this.mIsTodayStepGoalAlertEnabled_ = value;
            this.bitField0_ |= 2;
            return this;
        }

        public SportConfig setMSedentaryMin(int value) {
            this.mSedentaryMin_ = value;
            this.bitField0_ |= 4;
            return this;
        }

        public SportConfig setMIsSedentaryMinAlertEnabled(boolean value) {
            this.mIsSedentaryMinAlertEnabled_ = value;
            this.bitField0_ |= 8;
            return this;
        }

        public SportConfig setMSportLapMeter(float value) {
            this.mSportLapMeter_ = value;
            this.bitField0_ |= 16;
            return this;
        }

        public SportConfig setMIsSportLapMeterAlertEnabled(boolean value) {
            this.mIsSportLapMeterAlertEnabled_ = value;
            this.bitField0_ |= 32;
            return this;
        }

        public SportConfig setMSportSafeHeartRate(int value) {
            this.mSportSafeHeartRate_ = value;
            this.bitField0_ |= 64;
            return this;
        }

        public SportConfig setMIsSafeHeartRateAlertEnabled(boolean value) {
            this.mIsSafeHeartRateAlertEnabled_ = value;
            this.bitField0_ |= 128;
            return this;
        }

        public SportConfig setMIsSportAutoPauseEnabled(boolean value) {
            this.mIsSportAutoPauseEnabled_ = value;
            this.bitField0_ |= 256;
            return this;
        }

        public SportConfig setMSportTargetTimer(float value) {
            this.mSportTargetTimer_ = value;
            this.bitField0_ |= 512;
            return this;
        }

        public SportConfig setMIsSportTargetTimerAlertEnabled(boolean value) {
            this.mIsSportTargetTimerAlertEnabled_ = value;
            this.bitField0_ |= 1024;
            return this;
        }

        public SportConfig setMSportTargetDistance(float value) {
            this.mSportTargetDistance_ = value;
            this.bitField0_ |= 2048;
            return this;
        }

        public SportConfig setMIsSportTargetDistanceAlertEnabled(boolean value) {
            this.mIsSportTargetDistanceAlertEnabled_ = value;
            this.bitField0_ |= 4096;
            return this;
        }

        public SportConfig setMSportTargetPace(float value) {
            this.mSportTargetPace_ = value;
            this.bitField0_ |= 8192;
            return this;
        }

        public SportConfig setMIsSportTargetPaceAlertEnabled(boolean value) {
            this.mIsSportTargetPaceAlertEnabled_ = value;
            this.bitField0_ |= 16384;
            return this;
        }

        public SportConfig setMUserIsLeftHander(boolean value) {
            this.mUserIsLeftHander_ = value;
            this.bitField1_ |= 256;
            return this;
        }

        public SportConfig setMSportTargetCalories(float value) {
            this.mSportTargetCalories_ = value;
            this.bitField1_ |= 8192;
            return this;
        }

        public SportConfig setMIsSportTargetCaloriesAlertEnabled(boolean value) {
            this.mIsSportTargetCaloriesAlertEnabled_ = value;
            this.bitField1_ |= 16384;
            return this;
        }

        public SportConfig setMSportHrZoneLower(float value) {
            this.mSportHrZoneLower_ = value;
            this.bitField1_ |= 32768;
            return this;
        }

        public SportConfig setMSportHrZoneUpper(float value) {
            this.mSportHrZoneUpper_ = value;
            this.bitField1_ |= 65536;
            return this;
        }

        public SportConfig setMIsSportHrZoneAlertEnabled(boolean value) {
            this.mIsSportHrZoneAlertEnabled_ = value;
            this.bitField1_ |= 131072;
            return this;
        }

        public SportConfig setMSportKiloMeter(float value) {
            this.mSportKiloMeter_ = value;
            this.bitField1_ |= 262144;
            return this;
        }

        public SportConfig setMIsSporKiloMeterAlertEnabled(boolean value) {
            this.mIsSporKiloMeterAlertEnabled_ = value;
            this.bitField1_ |= 524288;
            return this;
        }

        public SportConfig setMSwimPoolLen(int value) {
            this.mSwimPoolLen_ = value;
            this.bitField1_ |= 1048576;
            return this;
        }

        public SportConfig setMSportTargetTrainingEffect(int value) {
            this.mSportTargetTrainingEffect_ = value;
            this.bitField1_ |= 2097152;
            return this;
        }

        public SportConfig setMIsMeasureHrInCrossCountryRun(boolean value) {
            this.mIsMeasureHrInCrossCountryRun_ = value;
            this.bitField1_ |= 4194304;
            return this;
        }

        public SportConfig setMIsUseThaWorkoutSet(boolean value) {
            this.mIsUseThaWorkoutSet_ = value;
            this.bitField1_ |= 8388608;
            return this;
        }

        public SportConfig setMIs3DDistanceEnabled(boolean value) {
            this.mIs3DDistanceEnabled_ = value;
            this.bitField1_ |= 16777216;
            return this;
        }

        public SportConfig setMSportAutoPauseSpeedTh(float value) {
            this.mSportAutoPauseSpeedTh_ = value;
            this.bitField1_ |= 33554432;
            return this;
        }

        public SportConfig setMSportFastTargetPace(float value) {
            this.mSportFastTargetPace_ = value;
            this.bitField1_ |= 67108864;
            return this;
        }

        public SportConfig setMIsSportFastTargetPaceAlertEnabled(boolean value) {
            this.mIsSportFastTargetPaceAlertEnabled_ = value;
            this.bitField1_ |= 134217728;
            return this;
        }

        public SportConfig() {
            clear();
        }

        public SportConfig clear() {
            this.bitField0_ = 0;
            this.bitField1_ = 0;
            this.mTodayStepGoal_ = 0;
            this.mIsTodayStepGoalAlertEnabled_ = false;
            this.mSedentaryMin_ = 0;
            this.mIsSedentaryMinAlertEnabled_ = false;
            this.mSportLapMeter_ = 0.0f;
            this.mIsSportLapMeterAlertEnabled_ = false;
            this.mSportSafeHeartRate_ = 0;
            this.mIsSafeHeartRateAlertEnabled_ = false;
            this.mIsSportAutoPauseEnabled_ = false;
            this.mSportTargetTimer_ = 0.0f;
            this.mIsSportTargetTimerAlertEnabled_ = false;
            this.mSportTargetDistance_ = 0.0f;
            this.mIsSportTargetDistanceAlertEnabled_ = false;
            this.mSportTargetPace_ = 0.0f;
            this.mIsSportTargetPaceAlertEnabled_ = false;
            this.mMedalTodayStepGoal1_ = 8000;
            this.mIsMedalTodayStepGoal1AlertEnabled_ = true;
            this.mMedalTodayStepGoal2_ = 20000;
            this.mIsMedalTodayStepGoal2AlertEnabled_ = true;
            this.mMedalTodayStepGoal3_ = 50000;
            this.mIsMedalTodayStepGoal3AlertEnabled_ = true;
            this.mMedalTotalStepGoal1_ = 50000;
            this.mIsMedalTotalStepGoal1AlertEnabled_ = true;
            this.mMedalTotalStepGoal2_ = 300000;
            this.mIsMedalTotalStepGoal2AlertEnabled_ = true;
            this.mMedalTotalStepGoal3_ = 1000000;
            this.mIsMedalTotalStepGoal3AlertEnabled_ = true;
            this.mMedalTotalStepGoal4_ = 5000000;
            this.mIsMedalTotalStepGoal4AlertEnabled_ = true;
            this.mUserAge_ = 0;
            this.mUserHeightMeter_ = 0.0f;
            this.mUserWeightKg_ = 0.0f;
            this.mUserStepMeter_ = 0.0f;
            this.mUserBasicCalories_ = 0.0f;
            this.mUserSex_ = 0;
            this.mUserIsWearOnLeft_ = false;
            this.mUserBodyFatRate_ = 0;
            this.mUserExerciseRate_ = 0;
            this.mUserIsVeganism_ = false;
            this.mUserSkin_ = 0;
            this.mUserIsLeftHander_ = false;
            this.mIsAlldayAutoHeartRateEnabled_ = false;
            this.mSportCorrectDistance_ = 0.0f;
            this.mStepModel = WireFormatNano.EMPTY_INT_ARRAY;
            this.mIsFlipEnabled_ = false;
            this.mIsWearDetectEnabled_ = false;
            this.mSportTargetCalories_ = 0.0f;
            this.mIsSportTargetCaloriesAlertEnabled_ = false;
            this.mSportHrZoneLower_ = 0.0f;
            this.mSportHrZoneUpper_ = 0.0f;
            this.mIsSportHrZoneAlertEnabled_ = false;
            this.mSportKiloMeter_ = 0.0f;
            this.mIsSporKiloMeterAlertEnabled_ = false;
            this.mSwimPoolLen_ = 0;
            this.mSportTargetTrainingEffect_ = 0;
            this.mIsMeasureHrInCrossCountryRun_ = false;
            this.mIsUseThaWorkoutSet_ = false;
            this.mIs3DDistanceEnabled_ = false;
            this.mSportAutoPauseSpeedTh_ = 0.0f;
            this.mSportFastTargetPace_ = 0.0f;
            this.mIsSportFastTargetPaceAlertEnabled_ = false;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.mTodayStepGoal_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeBool(2, this.mIsTodayStepGoalAlertEnabled_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.mSedentaryMin_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeBool(4, this.mIsSedentaryMinAlertEnabled_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeFloat(5, this.mSportLapMeter_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeBool(6, this.mIsSportLapMeterAlertEnabled_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeUInt32(7, this.mSportSafeHeartRate_);
            }
            if ((this.bitField0_ & 128) != 0) {
                output.writeBool(8, this.mIsSafeHeartRateAlertEnabled_);
            }
            if ((this.bitField0_ & 256) != 0) {
                output.writeBool(9, this.mIsSportAutoPauseEnabled_);
            }
            if ((this.bitField0_ & 512) != 0) {
                output.writeFloat(10, this.mSportTargetTimer_);
            }
            if ((this.bitField0_ & 1024) != 0) {
                output.writeBool(11, this.mIsSportTargetTimerAlertEnabled_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                output.writeFloat(12, this.mSportTargetDistance_);
            }
            if ((this.bitField0_ & 4096) != 0) {
                output.writeBool(13, this.mIsSportTargetDistanceAlertEnabled_);
            }
            if ((this.bitField0_ & 8192) != 0) {
                output.writeFloat(14, this.mSportTargetPace_);
            }
            if ((this.bitField0_ & 16384) != 0) {
                output.writeBool(15, this.mIsSportTargetPaceAlertEnabled_);
            }
            if ((this.bitField0_ & 32768) != 0) {
                output.writeUInt32(16, this.mMedalTodayStepGoal1_);
            }
            if ((this.bitField0_ & 65536) != 0) {
                output.writeBool(17, this.mIsMedalTodayStepGoal1AlertEnabled_);
            }
            if ((this.bitField0_ & 131072) != 0) {
                output.writeUInt32(18, this.mMedalTodayStepGoal2_);
            }
            if ((this.bitField0_ & 262144) != 0) {
                output.writeBool(19, this.mIsMedalTodayStepGoal2AlertEnabled_);
            }
            if ((this.bitField0_ & 524288) != 0) {
                output.writeUInt32(20, this.mMedalTodayStepGoal3_);
            }
            if ((this.bitField0_ & 1048576) != 0) {
                output.writeBool(21, this.mIsMedalTodayStepGoal3AlertEnabled_);
            }
            if ((this.bitField0_ & 2097152) != 0) {
                output.writeUInt32(22, this.mMedalTotalStepGoal1_);
            }
            if ((this.bitField0_ & 4194304) != 0) {
                output.writeBool(23, this.mIsMedalTotalStepGoal1AlertEnabled_);
            }
            if ((this.bitField0_ & 8388608) != 0) {
                output.writeUInt32(24, this.mMedalTotalStepGoal2_);
            }
            if ((this.bitField0_ & 16777216) != 0) {
                output.writeBool(25, this.mIsMedalTotalStepGoal2AlertEnabled_);
            }
            if ((this.bitField0_ & 33554432) != 0) {
                output.writeUInt32(26, this.mMedalTotalStepGoal3_);
            }
            if ((this.bitField0_ & 67108864) != 0) {
                output.writeBool(27, this.mIsMedalTotalStepGoal3AlertEnabled_);
            }
            if ((this.bitField0_ & 134217728) != 0) {
                output.writeUInt32(28, this.mMedalTotalStepGoal4_);
            }
            if ((this.bitField0_ & 268435456) != 0) {
                output.writeBool(29, this.mIsMedalTotalStepGoal4AlertEnabled_);
            }
            if ((this.bitField0_ & 536870912) != 0) {
                output.writeUInt32(30, this.mUserAge_);
            }
            if ((this.bitField0_ & 1073741824) != 0) {
                output.writeFloat(31, this.mUserHeightMeter_);
            }
            if ((this.bitField0_ & Integer.MIN_VALUE) != 0) {
                output.writeFloat(32, this.mUserWeightKg_);
            }
            if ((this.bitField1_ & 1) != 0) {
                output.writeFloat(33, this.mUserStepMeter_);
            }
            if ((this.bitField1_ & 2) != 0) {
                output.writeFloat(34, this.mUserBasicCalories_);
            }
            if ((this.bitField1_ & 4) != 0) {
                output.writeInt32(35, this.mUserSex_);
            }
            if ((this.bitField1_ & 8) != 0) {
                output.writeBool(36, this.mUserIsWearOnLeft_);
            }
            if ((this.bitField1_ & 16) != 0) {
                output.writeUInt32(37, this.mUserBodyFatRate_);
            }
            if ((this.bitField1_ & 32) != 0) {
                output.writeUInt32(38, this.mUserExerciseRate_);
            }
            if ((this.bitField1_ & 64) != 0) {
                output.writeBool(39, this.mUserIsVeganism_);
            }
            if ((this.bitField1_ & 128) != 0) {
                output.writeInt32(40, this.mUserSkin_);
            }
            if ((this.bitField1_ & 256) != 0) {
                output.writeBool(41, this.mUserIsLeftHander_);
            }
            if ((this.bitField1_ & 512) != 0) {
                output.writeBool(42, this.mIsAlldayAutoHeartRateEnabled_);
            }
            if ((this.bitField1_ & 1024) != 0) {
                output.writeFloat(43, this.mSportCorrectDistance_);
            }
            if (this.mStepModel != null && this.mStepModel.length > 0) {
                for (int writeInt32 : this.mStepModel) {
                    output.writeInt32(44, writeInt32);
                }
            }
            if ((this.bitField1_ & 2048) != 0) {
                output.writeBool(45, this.mIsFlipEnabled_);
            }
            if ((this.bitField1_ & 4096) != 0) {
                output.writeBool(46, this.mIsWearDetectEnabled_);
            }
            if ((this.bitField1_ & 8192) != 0) {
                output.writeFloat(47, this.mSportTargetCalories_);
            }
            if ((this.bitField1_ & 16384) != 0) {
                output.writeBool(48, this.mIsSportTargetCaloriesAlertEnabled_);
            }
            if ((this.bitField1_ & 32768) != 0) {
                output.writeFloat(49, this.mSportHrZoneLower_);
            }
            if ((this.bitField1_ & 65536) != 0) {
                output.writeFloat(50, this.mSportHrZoneUpper_);
            }
            if ((this.bitField1_ & 131072) != 0) {
                output.writeBool(51, this.mIsSportHrZoneAlertEnabled_);
            }
            if ((this.bitField1_ & 262144) != 0) {
                output.writeFloat(52, this.mSportKiloMeter_);
            }
            if ((this.bitField1_ & 524288) != 0) {
                output.writeBool(53, this.mIsSporKiloMeterAlertEnabled_);
            }
            if ((this.bitField1_ & 1048576) != 0) {
                output.writeUInt32(54, this.mSwimPoolLen_);
            }
            if ((this.bitField1_ & 2097152) != 0) {
                output.writeUInt32(55, this.mSportTargetTrainingEffect_);
            }
            if ((this.bitField1_ & 4194304) != 0) {
                output.writeBool(56, this.mIsMeasureHrInCrossCountryRun_);
            }
            if ((this.bitField1_ & 8388608) != 0) {
                output.writeBool(57, this.mIsUseThaWorkoutSet_);
            }
            if ((this.bitField1_ & 16777216) != 0) {
                output.writeBool(58, this.mIs3DDistanceEnabled_);
            }
            if ((this.bitField1_ & 33554432) != 0) {
                output.writeFloat(59, this.mSportAutoPauseSpeedTh_);
            }
            if ((this.bitField1_ & 67108864) != 0) {
                output.writeFloat(60, this.mSportFastTargetPace_);
            }
            if ((this.bitField1_ & 134217728) != 0) {
                output.writeBool(61, this.mIsSportFastTargetPaceAlertEnabled_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.mTodayStepGoal_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(2, this.mIsTodayStepGoalAlertEnabled_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.mSedentaryMin_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(4, this.mIsSedentaryMinAlertEnabled_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(5, this.mSportLapMeter_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(6, this.mIsSportLapMeterAlertEnabled_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(7, this.mSportSafeHeartRate_);
            }
            if ((this.bitField0_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(8, this.mIsSafeHeartRateAlertEnabled_);
            }
            if ((this.bitField0_ & 256) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(9, this.mIsSportAutoPauseEnabled_);
            }
            if ((this.bitField0_ & 512) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(10, this.mSportTargetTimer_);
            }
            if ((this.bitField0_ & 1024) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(11, this.mIsSportTargetTimerAlertEnabled_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(12, this.mSportTargetDistance_);
            }
            if ((this.bitField0_ & 4096) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(13, this.mIsSportTargetDistanceAlertEnabled_);
            }
            if ((this.bitField0_ & 8192) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(14, this.mSportTargetPace_);
            }
            if ((this.bitField0_ & 16384) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(15, this.mIsSportTargetPaceAlertEnabled_);
            }
            if ((this.bitField0_ & 32768) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(16, this.mMedalTodayStepGoal1_);
            }
            if ((this.bitField0_ & 65536) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(17, this.mIsMedalTodayStepGoal1AlertEnabled_);
            }
            if ((this.bitField0_ & 131072) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(18, this.mMedalTodayStepGoal2_);
            }
            if ((this.bitField0_ & 262144) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(19, this.mIsMedalTodayStepGoal2AlertEnabled_);
            }
            if ((this.bitField0_ & 524288) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(20, this.mMedalTodayStepGoal3_);
            }
            if ((this.bitField0_ & 1048576) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(21, this.mIsMedalTodayStepGoal3AlertEnabled_);
            }
            if ((this.bitField0_ & 2097152) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(22, this.mMedalTotalStepGoal1_);
            }
            if ((this.bitField0_ & 4194304) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(23, this.mIsMedalTotalStepGoal1AlertEnabled_);
            }
            if ((this.bitField0_ & 8388608) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(24, this.mMedalTotalStepGoal2_);
            }
            if ((this.bitField0_ & 16777216) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(25, this.mIsMedalTotalStepGoal2AlertEnabled_);
            }
            if ((this.bitField0_ & 33554432) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(26, this.mMedalTotalStepGoal3_);
            }
            if ((this.bitField0_ & 67108864) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(27, this.mIsMedalTotalStepGoal3AlertEnabled_);
            }
            if ((this.bitField0_ & 134217728) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(28, this.mMedalTotalStepGoal4_);
            }
            if ((this.bitField0_ & 268435456) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(29, this.mIsMedalTotalStepGoal4AlertEnabled_);
            }
            if ((this.bitField0_ & 536870912) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(30, this.mUserAge_);
            }
            if ((this.bitField0_ & 1073741824) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(31, this.mUserHeightMeter_);
            }
            if ((this.bitField0_ & Integer.MIN_VALUE) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(32, this.mUserWeightKg_);
            }
            if ((this.bitField1_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(33, this.mUserStepMeter_);
            }
            if ((this.bitField1_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(34, this.mUserBasicCalories_);
            }
            if ((this.bitField1_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(35, this.mUserSex_);
            }
            if ((this.bitField1_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(36, this.mUserIsWearOnLeft_);
            }
            if ((this.bitField1_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(37, this.mUserBodyFatRate_);
            }
            if ((this.bitField1_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(38, this.mUserExerciseRate_);
            }
            if ((this.bitField1_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(39, this.mUserIsVeganism_);
            }
            if ((this.bitField1_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(40, this.mUserSkin_);
            }
            if ((this.bitField1_ & 256) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(41, this.mUserIsLeftHander_);
            }
            if ((this.bitField1_ & 512) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(42, this.mIsAlldayAutoHeartRateEnabled_);
            }
            if ((this.bitField1_ & 1024) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(43, this.mSportCorrectDistance_);
            }
            if (this.mStepModel != null && this.mStepModel.length > 0) {
                int dataSize = 0;
                for (int element : this.mStepModel) {
                    dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element);
                }
                size = (size + dataSize) + (this.mStepModel.length * 2);
            }
            if ((this.bitField1_ & 2048) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(45, this.mIsFlipEnabled_);
            }
            if ((this.bitField1_ & 4096) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(46, this.mIsWearDetectEnabled_);
            }
            if ((this.bitField1_ & 8192) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(47, this.mSportTargetCalories_);
            }
            if ((this.bitField1_ & 16384) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(48, this.mIsSportTargetCaloriesAlertEnabled_);
            }
            if ((this.bitField1_ & 32768) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(49, this.mSportHrZoneLower_);
            }
            if ((this.bitField1_ & 65536) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(50, this.mSportHrZoneUpper_);
            }
            if ((this.bitField1_ & 131072) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(51, this.mIsSportHrZoneAlertEnabled_);
            }
            if ((this.bitField1_ & 262144) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(52, this.mSportKiloMeter_);
            }
            if ((this.bitField1_ & 524288) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(53, this.mIsSporKiloMeterAlertEnabled_);
            }
            if ((this.bitField1_ & 1048576) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(54, this.mSwimPoolLen_);
            }
            if ((this.bitField1_ & 2097152) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(55, this.mSportTargetTrainingEffect_);
            }
            if ((this.bitField1_ & 4194304) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(56, this.mIsMeasureHrInCrossCountryRun_);
            }
            if ((this.bitField1_ & 8388608) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(57, this.mIsUseThaWorkoutSet_);
            }
            if ((this.bitField1_ & 16777216) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(58, this.mIs3DDistanceEnabled_);
            }
            if ((this.bitField1_ & 33554432) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(59, this.mSportAutoPauseSpeedTh_);
            }
            if ((this.bitField1_ & 67108864) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(60, this.mSportFastTargetPace_);
            }
            if ((this.bitField1_ & 134217728) != 0) {
                return size + CodedOutputByteBufferNano.computeBoolSize(61, this.mIsSportFastTargetPaceAlertEnabled_);
            }
            return size;
        }

        public SportConfig mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int value;
                int arrayLength;
                int i;
                int[] newArray;
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.mTodayStepGoal_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.mIsTodayStepGoalAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.mSedentaryMin_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 32:
                        this.mIsSedentaryMinAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 8;
                        continue;
                    case 45:
                        this.mSportLapMeter_ = input.readFloat();
                        this.bitField0_ |= 16;
                        continue;
                    case 48:
                        this.mIsSportLapMeterAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 32;
                        continue;
                    case 56:
                        this.mSportSafeHeartRate_ = input.readUInt32();
                        this.bitField0_ |= 64;
                        continue;
                    case 64:
                        this.mIsSafeHeartRateAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 128;
                        continue;
                    case 72:
                        this.mIsSportAutoPauseEnabled_ = input.readBool();
                        this.bitField0_ |= 256;
                        continue;
                    case 85:
                        this.mSportTargetTimer_ = input.readFloat();
                        this.bitField0_ |= 512;
                        continue;
                    case 88:
                        this.mIsSportTargetTimerAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 1024;
                        continue;
                    case 101:
                        this.mSportTargetDistance_ = input.readFloat();
                        this.bitField0_ |= 2048;
                        continue;
                    case 104:
                        this.mIsSportTargetDistanceAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 4096;
                        continue;
                    case 117:
                        this.mSportTargetPace_ = input.readFloat();
                        this.bitField0_ |= 8192;
                        continue;
                    case 120:
                        this.mIsSportTargetPaceAlertEnabled_ = input.readBool();
                        this.bitField0_ |= 16384;
                        continue;
                    case 128:
                        this.mMedalTodayStepGoal1_ = input.readUInt32();
                        this.bitField0_ |= 32768;
                        continue;
                    case 136:
                        this.mIsMedalTodayStepGoal1AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 65536;
                        continue;
                    case 144:
                        this.mMedalTodayStepGoal2_ = input.readUInt32();
                        this.bitField0_ |= 131072;
                        continue;
                    case 152:
                        this.mIsMedalTodayStepGoal2AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 262144;
                        continue;
                    case 160:
                        this.mMedalTodayStepGoal3_ = input.readUInt32();
                        this.bitField0_ |= 524288;
                        continue;
                    case 168:
                        this.mIsMedalTodayStepGoal3AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 1048576;
                        continue;
                    case 176:
                        this.mMedalTotalStepGoal1_ = input.readUInt32();
                        this.bitField0_ |= 2097152;
                        continue;
                    case 184:
                        this.mIsMedalTotalStepGoal1AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 4194304;
                        continue;
                    case 192:
                        this.mMedalTotalStepGoal2_ = input.readUInt32();
                        this.bitField0_ |= 8388608;
                        continue;
                    case 200:
                        this.mIsMedalTotalStepGoal2AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 16777216;
                        continue;
                    case 208:
                        this.mMedalTotalStepGoal3_ = input.readUInt32();
                        this.bitField0_ |= 33554432;
                        continue;
                    case 216:
                        this.mIsMedalTotalStepGoal3AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 67108864;
                        continue;
                    case 224:
                        this.mMedalTotalStepGoal4_ = input.readUInt32();
                        this.bitField0_ |= 134217728;
                        continue;
                    case 232:
                        this.mIsMedalTotalStepGoal4AlertEnabled_ = input.readBool();
                        this.bitField0_ |= 268435456;
                        continue;
                    case 240:
                        this.mUserAge_ = input.readUInt32();
                        this.bitField0_ |= 536870912;
                        continue;
                    case 253:
                        this.mUserHeightMeter_ = input.readFloat();
                        this.bitField0_ |= 1073741824;
                        continue;
                    case 261:
                        this.mUserWeightKg_ = input.readFloat();
                        this.bitField0_ |= Integer.MIN_VALUE;
                        continue;
                    case 269:
                        this.mUserStepMeter_ = input.readFloat();
                        this.bitField1_ |= 1;
                        continue;
                    case 277:
                        this.mUserBasicCalories_ = input.readFloat();
                        this.bitField1_ |= 2;
                        continue;
                    case 280:
                        value = input.readInt32();
                        switch (value) {
                            case 0:
                            case 1:
                            case 2:
                                this.mUserSex_ = value;
                                this.bitField1_ |= 4;
                                break;
                            default:
                                continue;
                        }
                    case 288:
                        this.mUserIsWearOnLeft_ = input.readBool();
                        this.bitField1_ |= 8;
                        continue;
                    case 296:
                        this.mUserBodyFatRate_ = input.readUInt32();
                        this.bitField1_ |= 16;
                        continue;
                    case 304:
                        this.mUserExerciseRate_ = input.readUInt32();
                        this.bitField1_ |= 32;
                        continue;
                    case 312:
                        this.mUserIsVeganism_ = input.readBool();
                        this.bitField1_ |= 64;
                        continue;
                    case 320:
                        value = input.readInt32();
                        switch (value) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                                this.mUserSkin_ = value;
                                this.bitField1_ |= 128;
                                break;
                            default:
                                continue;
                        }
                    case 328:
                        this.mUserIsLeftHander_ = input.readBool();
                        this.bitField1_ |= 256;
                        continue;
                    case 336:
                        this.mIsAlldayAutoHeartRateEnabled_ = input.readBool();
                        this.bitField1_ |= 512;
                        continue;
                    case 349:
                        this.mSportCorrectDistance_ = input.readFloat();
                        this.bitField1_ |= 1024;
                        continue;
                    case 352:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 352);
                        i = this.mStepModel == null ? 0 : this.mStepModel.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mStepModel, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = input.readInt32();
                            input.readTag();
                            i++;
                        }
                        newArray[i] = input.readInt32();
                        this.mStepModel = newArray;
                        continue;
                    case 354:
                        int limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        int startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readInt32();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.mStepModel == null ? 0 : this.mStepModel.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mStepModel, 0, newArray, 0, i);
                        }
                        while (i < newArray.length) {
                            newArray[i] = input.readInt32();
                            i++;
                        }
                        this.mStepModel = newArray;
                        input.popLimit(limit);
                        continue;
                    case 360:
                        this.mIsFlipEnabled_ = input.readBool();
                        this.bitField1_ |= 2048;
                        continue;
                    case 368:
                        this.mIsWearDetectEnabled_ = input.readBool();
                        this.bitField1_ |= 4096;
                        continue;
                    case 381:
                        this.mSportTargetCalories_ = input.readFloat();
                        this.bitField1_ |= 8192;
                        continue;
                    case 384:
                        this.mIsSportTargetCaloriesAlertEnabled_ = input.readBool();
                        this.bitField1_ |= 16384;
                        continue;
                    case 397:
                        this.mSportHrZoneLower_ = input.readFloat();
                        this.bitField1_ |= 32768;
                        continue;
                    case 405:
                        this.mSportHrZoneUpper_ = input.readFloat();
                        this.bitField1_ |= 65536;
                        continue;
                    case 408:
                        this.mIsSportHrZoneAlertEnabled_ = input.readBool();
                        this.bitField1_ |= 131072;
                        continue;
                    case 421:
                        this.mSportKiloMeter_ = input.readFloat();
                        this.bitField1_ |= 262144;
                        continue;
                    case 424:
                        this.mIsSporKiloMeterAlertEnabled_ = input.readBool();
                        this.bitField1_ |= 524288;
                        continue;
                    case 432:
                        this.mSwimPoolLen_ = input.readUInt32();
                        this.bitField1_ |= 1048576;
                        continue;
                    case 440:
                        this.mSportTargetTrainingEffect_ = input.readUInt32();
                        this.bitField1_ |= 2097152;
                        continue;
                    case 448:
                        this.mIsMeasureHrInCrossCountryRun_ = input.readBool();
                        this.bitField1_ |= 4194304;
                        continue;
                    case 456:
                        this.mIsUseThaWorkoutSet_ = input.readBool();
                        this.bitField1_ |= 8388608;
                        continue;
                    case 464:
                        this.mIs3DDistanceEnabled_ = input.readBool();
                        this.bitField1_ |= 16777216;
                        continue;
                    case 477:
                        this.mSportAutoPauseSpeedTh_ = input.readFloat();
                        this.bitField1_ |= 33554432;
                        continue;
                    case 485:
                        this.mSportFastTargetPace_ = input.readFloat();
                        this.bitField1_ |= 67108864;
                        continue;
                    case 488:
                        this.mIsSportFastTargetPaceAlertEnabled_ = input.readBool();
                        this.bitField1_ |= 134217728;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportEteInfo extends ExtendableMessageNano<SportEteInfo> {
        private int bitField0_;
        private int eTEdailyPerformance_;
        private int eTEmaximalMET_;
        private int eTEphraseNumber_;
        private int eTEphraseVariable_;
        private int eTEresourceRecovery_;
        private int eTEtrainingEffect_;
        private int eTEtrainingLoadPeak_;

        public int getETEtrainingLoadPeak() {
            return this.eTEtrainingLoadPeak_;
        }

        public int getETEtrainingEffect() {
            return this.eTEtrainingEffect_;
        }

        public int getETEmaximalMET() {
            return this.eTEmaximalMET_;
        }

        public int getETEresourceRecovery() {
            return this.eTEresourceRecovery_;
        }

        public int getETEdailyPerformance() {
            return this.eTEdailyPerformance_;
        }

        public int getETEphraseNumber() {
            return this.eTEphraseNumber_;
        }

        public SportEteInfo() {
            clear();
        }

        public SportEteInfo clear() {
            this.bitField0_ = 0;
            this.eTEtrainingLoadPeak_ = 0;
            this.eTEtrainingEffect_ = 0;
            this.eTEmaximalMET_ = 0;
            this.eTEresourceRecovery_ = 0;
            this.eTEdailyPerformance_ = 0;
            this.eTEphraseNumber_ = 0;
            this.eTEphraseVariable_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.eTEtrainingLoadPeak_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt32(2, this.eTEtrainingEffect_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.eTEmaximalMET_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeUInt32(4, this.eTEresourceRecovery_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeInt32(5, this.eTEdailyPerformance_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeUInt32(6, this.eTEphraseNumber_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeUInt32(7, this.eTEphraseVariable_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.eTEtrainingLoadPeak_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(2, this.eTEtrainingEffect_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.eTEmaximalMET_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(4, this.eTEresourceRecovery_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(5, this.eTEdailyPerformance_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(6, this.eTEphraseNumber_);
            }
            if ((this.bitField0_ & 64) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(7, this.eTEphraseVariable_);
            }
            return size;
        }

        public SportEteInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.eTEtrainingLoadPeak_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.eTEtrainingEffect_ = input.readUInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.eTEmaximalMET_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 32:
                        this.eTEresourceRecovery_ = input.readUInt32();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.eTEdailyPerformance_ = input.readInt32();
                        this.bitField0_ |= 16;
                        continue;
                    case 48:
                        this.eTEphraseNumber_ = input.readUInt32();
                        this.bitField0_ |= 32;
                        continue;
                    case 56:
                        this.eTEphraseVariable_ = input.readUInt32();
                        this.bitField0_ |= 64;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportSetStatistics extends ExtendableMessageNano<SportSetStatistics> {
        private int bitField0_;
        private int mSportSetStatus_;
        private float mSportSetTotalCalories_;
        private float mSportSetTotalDistance_;
        private long mSportSetTotalTime_;

        public long getMSportSetTotalTime() {
            return this.mSportSetTotalTime_;
        }

        public float getMSportSetTotalDistance() {
            return this.mSportSetTotalDistance_;
        }

        public float getMSportSetTotalCalories() {
            return this.mSportSetTotalCalories_;
        }

        public SportSetStatistics() {
            clear();
        }

        public SportSetStatistics clear() {
            this.bitField0_ = 0;
            this.mSportSetStatus_ = 1;
            this.mSportSetTotalTime_ = 0;
            this.mSportSetTotalDistance_ = 0.0f;
            this.mSportSetTotalCalories_ = 0.0f;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeInt32(1, this.mSportSetStatus_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt64(2, this.mSportSetTotalTime_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeFloat(3, this.mSportSetTotalDistance_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeFloat(4, this.mSportSetTotalCalories_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(1, this.mSportSetStatus_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt64Size(2, this.mSportSetTotalTime_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(3, this.mSportSetTotalDistance_);
            }
            if ((this.bitField0_ & 8) != 0) {
                return size + CodedOutputByteBufferNano.computeFloatSize(4, this.mSportSetTotalCalories_);
            }
            return size;
        }

        public SportSetStatistics mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        int value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 4:
                            case 8:
                            case 16:
                                this.mSportSetStatus_ = value;
                                this.bitField0_ |= 1;
                                break;
                            default:
                                continue;
                        }
                    case 16:
                        this.mSportSetTotalTime_ = input.readUInt64();
                        this.bitField0_ |= 2;
                        continue;
                    case 29:
                        this.mSportSetTotalDistance_ = input.readFloat();
                        this.bitField0_ |= 4;
                        continue;
                    case 37:
                        this.mSportSetTotalCalories_ = input.readFloat();
                        this.bitField0_ |= 8;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportStatistics extends ExtendableMessageNano<SportStatistics> {
        private int bitField0_;
        private int mCurSportGpsStatus_;
        private int mCurSportStatus_;
        private int mCurSportType_;
        public long[] mGPSLonLatPoint;
        public int[] mOutStepModel;
        public SportAvgInfo mRealtimeAvg;
        private int mReminder_;
        public RiddingInfo mRiddingInfo;
        public SportAcmInfo mRtSportLapAcm;
        public SportAvgInfo mRtSportLapAvg;
        public SportAcmInfo mSportAcm;
        public SportAvgInfo mSportAvg;
        public SportAcmInfo mSportCurSubAcm;
        private long mSportCurSubStartTimestamp_;
        private float mSportElapsedTime_;
        private long mSportEndTimestamp_;
        public SportEteInfo mSportEteInfo;
        public SportAcmInfo mSportKiloAcm;
        public SportAvgInfo mSportKiloAvg;
        private int mSportKiloNum_;
        public SportAcmInfo mSportLapAcm;
        public SportAvgInfo mSportLapAvg;
        private int mSportLapNum_;
        public SportAvgInfo mSportMaxAvg;
        public SportAvgInfo mSportMaxLapAvg;
        public SportAcmInfo mSportMileAcm;
        public SportAvgInfo mSportMileAvg;
        private int mSportMileNum_;
        public SportAvgInfo mSportMinAvg;
        public SportAcmInfo mSportPreSubAcm;
        public SportSetStatistics mSportSetStat;
        private long mSportStartTimestamp_;
        public SportAcmInfo mSportTempAcm;
        public SportThaInfo mSportThaInfo;
        public SportThaWorkout mSportThaWorkout;
        public SwimInfo mSwimInfo;
        public TennisInfo mTennisInfo;
        public SportAcmInfo mTodayAcm;
        private int mTodayFloorCount_;
        public SportAcmInfo mTotalAcm;
        public SportAcmInfo mTotalTempAcm;

        public int getMCurSportGpsStatus() {
            return this.mCurSportGpsStatus_;
        }

        public int getMSportLapNum() {
            return this.mSportLapNum_;
        }

        public int getMSportKiloNum() {
            return this.mSportKiloNum_;
        }

        public SportStatistics() {
            clear();
        }

        public SportStatistics clear() {
            this.bitField0_ = 0;
            this.mSportStartTimestamp_ = 0;
            this.mSportEndTimestamp_ = 0;
            this.mSportCurSubStartTimestamp_ = 0;
            this.mSportElapsedTime_ = 0.0f;
            this.mReminder_ = 0;
            this.mCurSportType_ = 0;
            this.mCurSportStatus_ = 1;
            this.mCurSportGpsStatus_ = 1;
            this.mTotalAcm = null;
            this.mTotalTempAcm = null;
            this.mTodayAcm = null;
            this.mRealtimeAvg = null;
            this.mSportAcm = null;
            this.mSportTempAcm = null;
            this.mSportAvg = null;
            this.mSportPreSubAcm = null;
            this.mSportCurSubAcm = null;
            this.mSportLapNum_ = 0;
            this.mSportLapAcm = null;
            this.mSportLapAvg = null;
            this.mSportKiloNum_ = 0;
            this.mSportKiloAcm = null;
            this.mSportKiloAvg = null;
            this.mSportMileNum_ = 0;
            this.mSportMileAcm = null;
            this.mSportMileAvg = null;
            this.mSportMaxAvg = null;
            this.mSportMinAvg = null;
            this.mRtSportLapAcm = null;
            this.mRtSportLapAvg = null;
            this.mRiddingInfo = null;
            this.mOutStepModel = WireFormatNano.EMPTY_INT_ARRAY;
            this.mTodayFloorCount_ = 0;
            this.mSportSetStat = null;
            this.mSwimInfo = null;
            this.mSportMaxLapAvg = null;
            this.mSportEteInfo = null;
            this.mSportThaInfo = null;
            this.mSportThaWorkout = null;
            this.mGPSLonLatPoint = WireFormatNano.EMPTY_LONG_ARRAY;
            this.mTennisInfo = null;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt64(1, this.mSportStartTimestamp_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt64(2, this.mSportEndTimestamp_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt64(3, this.mSportCurSubStartTimestamp_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeFloat(4, this.mSportElapsedTime_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeUInt32(5, this.mReminder_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeInt32(6, this.mCurSportType_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeInt32(7, this.mCurSportStatus_);
            }
            if ((this.bitField0_ & 128) != 0) {
                output.writeInt32(8, this.mCurSportGpsStatus_);
            }
            if (this.mTotalAcm != null) {
                output.writeMessage(9, this.mTotalAcm);
            }
            if (this.mTotalTempAcm != null) {
                output.writeMessage(10, this.mTotalTempAcm);
            }
            if (this.mTodayAcm != null) {
                output.writeMessage(11, this.mTodayAcm);
            }
            if (this.mRealtimeAvg != null) {
                output.writeMessage(12, this.mRealtimeAvg);
            }
            if (this.mSportAcm != null) {
                output.writeMessage(13, this.mSportAcm);
            }
            if (this.mSportTempAcm != null) {
                output.writeMessage(14, this.mSportTempAcm);
            }
            if (this.mSportAvg != null) {
                output.writeMessage(15, this.mSportAvg);
            }
            if (this.mSportPreSubAcm != null) {
                output.writeMessage(16, this.mSportPreSubAcm);
            }
            if (this.mSportCurSubAcm != null) {
                output.writeMessage(17, this.mSportCurSubAcm);
            }
            if ((this.bitField0_ & 256) != 0) {
                output.writeUInt32(18, this.mSportLapNum_);
            }
            if (this.mSportLapAcm != null) {
                output.writeMessage(19, this.mSportLapAcm);
            }
            if (this.mSportLapAvg != null) {
                output.writeMessage(20, this.mSportLapAvg);
            }
            if ((this.bitField0_ & 512) != 0) {
                output.writeUInt32(21, this.mSportKiloNum_);
            }
            if (this.mSportKiloAcm != null) {
                output.writeMessage(22, this.mSportKiloAcm);
            }
            if (this.mSportKiloAvg != null) {
                output.writeMessage(23, this.mSportKiloAvg);
            }
            if ((this.bitField0_ & 1024) != 0) {
                output.writeUInt32(24, this.mSportMileNum_);
            }
            if (this.mSportMileAcm != null) {
                output.writeMessage(25, this.mSportMileAcm);
            }
            if (this.mSportMileAvg != null) {
                output.writeMessage(26, this.mSportMileAvg);
            }
            if (this.mSportMaxAvg != null) {
                output.writeMessage(27, this.mSportMaxAvg);
            }
            if (this.mSportMinAvg != null) {
                output.writeMessage(28, this.mSportMinAvg);
            }
            if (this.mRtSportLapAcm != null) {
                output.writeMessage(29, this.mRtSportLapAcm);
            }
            if (this.mRtSportLapAvg != null) {
                output.writeMessage(30, this.mRtSportLapAvg);
            }
            if (this.mRiddingInfo != null) {
                output.writeMessage(31, this.mRiddingInfo);
            }
            if (this.mOutStepModel != null && this.mOutStepModel.length > 0) {
                for (int writeInt32 : this.mOutStepModel) {
                    output.writeInt32(32, writeInt32);
                }
            }
            if ((this.bitField0_ & 2048) != 0) {
                output.writeUInt32(33, this.mTodayFloorCount_);
            }
            if (this.mSportSetStat != null) {
                output.writeMessage(34, this.mSportSetStat);
            }
            if (this.mSwimInfo != null) {
                output.writeMessage(35, this.mSwimInfo);
            }
            if (this.mSportMaxLapAvg != null) {
                output.writeMessage(36, this.mSportMaxLapAvg);
            }
            if (this.mSportEteInfo != null) {
                output.writeMessage(37, this.mSportEteInfo);
            }
            if (this.mSportThaInfo != null) {
                output.writeMessage(38, this.mSportThaInfo);
            }
            if (this.mSportThaWorkout != null) {
                output.writeMessage(39, this.mSportThaWorkout);
            }
            if (this.mGPSLonLatPoint != null && this.mGPSLonLatPoint.length > 0) {
                for (long writeInt64 : this.mGPSLonLatPoint) {
                    output.writeInt64(40, writeInt64);
                }
            }
            if (this.mTennisInfo != null) {
                output.writeMessage(41, this.mTennisInfo);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int dataSize;
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt64Size(1, this.mSportStartTimestamp_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt64Size(2, this.mSportEndTimestamp_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt64Size(3, this.mSportCurSubStartTimestamp_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(4, this.mSportElapsedTime_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(5, this.mReminder_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(6, this.mCurSportType_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(7, this.mCurSportStatus_);
            }
            if ((this.bitField0_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(8, this.mCurSportGpsStatus_);
            }
            if (this.mTotalAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(9, this.mTotalAcm);
            }
            if (this.mTotalTempAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(10, this.mTotalTempAcm);
            }
            if (this.mTodayAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(11, this.mTodayAcm);
            }
            if (this.mRealtimeAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(12, this.mRealtimeAvg);
            }
            if (this.mSportAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(13, this.mSportAcm);
            }
            if (this.mSportTempAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(14, this.mSportTempAcm);
            }
            if (this.mSportAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(15, this.mSportAvg);
            }
            if (this.mSportPreSubAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(16, this.mSportPreSubAcm);
            }
            if (this.mSportCurSubAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(17, this.mSportCurSubAcm);
            }
            if ((this.bitField0_ & 256) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(18, this.mSportLapNum_);
            }
            if (this.mSportLapAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(19, this.mSportLapAcm);
            }
            if (this.mSportLapAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(20, this.mSportLapAvg);
            }
            if ((this.bitField0_ & 512) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(21, this.mSportKiloNum_);
            }
            if (this.mSportKiloAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(22, this.mSportKiloAcm);
            }
            if (this.mSportKiloAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(23, this.mSportKiloAvg);
            }
            if ((this.bitField0_ & 1024) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(24, this.mSportMileNum_);
            }
            if (this.mSportMileAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(25, this.mSportMileAcm);
            }
            if (this.mSportMileAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(26, this.mSportMileAvg);
            }
            if (this.mSportMaxAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(27, this.mSportMaxAvg);
            }
            if (this.mSportMinAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(28, this.mSportMinAvg);
            }
            if (this.mRtSportLapAcm != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(29, this.mRtSportLapAcm);
            }
            if (this.mRtSportLapAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(30, this.mRtSportLapAvg);
            }
            if (this.mRiddingInfo != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(31, this.mRiddingInfo);
            }
            if (this.mOutStepModel != null && this.mOutStepModel.length > 0) {
                dataSize = 0;
                for (int element : this.mOutStepModel) {
                    dataSize += CodedOutputByteBufferNano.computeInt32SizeNoTag(element);
                }
                size = (size + dataSize) + (this.mOutStepModel.length * 2);
            }
            if ((this.bitField0_ & 2048) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(33, this.mTodayFloorCount_);
            }
            if (this.mSportSetStat != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(34, this.mSportSetStat);
            }
            if (this.mSwimInfo != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(35, this.mSwimInfo);
            }
            if (this.mSportMaxLapAvg != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(36, this.mSportMaxLapAvg);
            }
            if (this.mSportEteInfo != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(37, this.mSportEteInfo);
            }
            if (this.mSportThaInfo != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(38, this.mSportThaInfo);
            }
            if (this.mSportThaWorkout != null) {
                size += CodedOutputByteBufferNano.computeMessageSize(39, this.mSportThaWorkout);
            }
            if (this.mGPSLonLatPoint != null && this.mGPSLonLatPoint.length > 0) {
                dataSize = 0;
                for (long element2 : this.mGPSLonLatPoint) {
                    dataSize += CodedOutputByteBufferNano.computeInt64SizeNoTag(element2);
                }
                size = (size + dataSize) + (this.mGPSLonLatPoint.length * 2);
            }
            if (this.mTennisInfo != null) {
                return size + CodedOutputByteBufferNano.computeMessageSize(41, this.mTennisInfo);
            }
            return size;
        }

        public SportStatistics mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int value;
                int arrayLength;
                int i;
                int[] newArray;
                int limit;
                int startPos;
                long[] newArray2;
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.mSportStartTimestamp_ = input.readUInt64();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.mSportEndTimestamp_ = input.readUInt64();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.mSportCurSubStartTimestamp_ = input.readUInt64();
                        this.bitField0_ |= 4;
                        continue;
                    case 37:
                        this.mSportElapsedTime_ = input.readFloat();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.mReminder_ = input.readUInt32();
                        this.bitField0_ |= 16;
                        continue;
                    case 48:
                        value = input.readInt32();
                        switch (value) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                            case 20:
                                this.mCurSportType_ = value;
                                this.bitField0_ |= 32;
                                break;
                            default:
                                continue;
                        }
                    case 56:
                        value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 4:
                            case 8:
                            case 16:
                                this.mCurSportStatus_ = value;
                                this.bitField0_ |= 64;
                                break;
                            default:
                                continue;
                        }
                    case 64:
                        value = input.readInt32();
                        switch (value) {
                            case 1:
                            case 2:
                            case 4:
                            case 8:
                            case 16:
                                this.mCurSportGpsStatus_ = value;
                                this.bitField0_ |= 128;
                                break;
                            default:
                                continue;
                        }
                    case 74:
                        if (this.mTotalAcm == null) {
                            this.mTotalAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mTotalAcm);
                        continue;
                    case 82:
                        if (this.mTotalTempAcm == null) {
                            this.mTotalTempAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mTotalTempAcm);
                        continue;
                    case 90:
                        if (this.mTodayAcm == null) {
                            this.mTodayAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mTodayAcm);
                        continue;
                    case 98:
                        if (this.mRealtimeAvg == null) {
                            this.mRealtimeAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mRealtimeAvg);
                        continue;
                    case 106:
                        if (this.mSportAcm == null) {
                            this.mSportAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportAcm);
                        continue;
                    case 114:
                        if (this.mSportTempAcm == null) {
                            this.mSportTempAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportTempAcm);
                        continue;
                    case 122:
                        if (this.mSportAvg == null) {
                            this.mSportAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportAvg);
                        continue;
                    case 130:
                        if (this.mSportPreSubAcm == null) {
                            this.mSportPreSubAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportPreSubAcm);
                        continue;
                    case 138:
                        if (this.mSportCurSubAcm == null) {
                            this.mSportCurSubAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportCurSubAcm);
                        continue;
                    case 144:
                        this.mSportLapNum_ = input.readUInt32();
                        this.bitField0_ |= 256;
                        continue;
                    case 154:
                        if (this.mSportLapAcm == null) {
                            this.mSportLapAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportLapAcm);
                        continue;
                    case 162:
                        if (this.mSportLapAvg == null) {
                            this.mSportLapAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportLapAvg);
                        continue;
                    case 168:
                        this.mSportKiloNum_ = input.readUInt32();
                        this.bitField0_ |= 512;
                        continue;
                    case 178:
                        if (this.mSportKiloAcm == null) {
                            this.mSportKiloAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportKiloAcm);
                        continue;
                    case 186:
                        if (this.mSportKiloAvg == null) {
                            this.mSportKiloAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportKiloAvg);
                        continue;
                    case 192:
                        this.mSportMileNum_ = input.readUInt32();
                        this.bitField0_ |= 1024;
                        continue;
                    case 202:
                        if (this.mSportMileAcm == null) {
                            this.mSportMileAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mSportMileAcm);
                        continue;
                    case 210:
                        if (this.mSportMileAvg == null) {
                            this.mSportMileAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportMileAvg);
                        continue;
                    case 218:
                        if (this.mSportMaxAvg == null) {
                            this.mSportMaxAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportMaxAvg);
                        continue;
                    case 226:
                        if (this.mSportMinAvg == null) {
                            this.mSportMinAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportMinAvg);
                        continue;
                    case 234:
                        if (this.mRtSportLapAcm == null) {
                            this.mRtSportLapAcm = new SportAcmInfo();
                        }
                        input.readMessage(this.mRtSportLapAcm);
                        continue;
                    case 242:
                        if (this.mRtSportLapAvg == null) {
                            this.mRtSportLapAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mRtSportLapAvg);
                        continue;
                    case 250:
                        if (this.mRiddingInfo == null) {
                            this.mRiddingInfo = new RiddingInfo();
                        }
                        input.readMessage(this.mRiddingInfo);
                        continue;
                    case 256:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 256);
                        i = this.mOutStepModel == null ? 0 : this.mOutStepModel.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mOutStepModel, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = input.readInt32();
                            input.readTag();
                            i++;
                        }
                        newArray[i] = input.readInt32();
                        this.mOutStepModel = newArray;
                        continue;
                    case 258:
                        limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readInt32();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.mOutStepModel == null ? 0 : this.mOutStepModel.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mOutStepModel, 0, newArray, 0, i);
                        }
                        while (i < newArray.length) {
                            newArray[i] = input.readInt32();
                            i++;
                        }
                        this.mOutStepModel = newArray;
                        input.popLimit(limit);
                        continue;
                    case 264:
                        this.mTodayFloorCount_ = input.readUInt32();
                        this.bitField0_ |= 2048;
                        continue;
                    case 274:
                        if (this.mSportSetStat == null) {
                            this.mSportSetStat = new SportSetStatistics();
                        }
                        input.readMessage(this.mSportSetStat);
                        continue;
                    case 282:
                        if (this.mSwimInfo == null) {
                            this.mSwimInfo = new SwimInfo();
                        }
                        input.readMessage(this.mSwimInfo);
                        continue;
                    case 290:
                        if (this.mSportMaxLapAvg == null) {
                            this.mSportMaxLapAvg = new SportAvgInfo();
                        }
                        input.readMessage(this.mSportMaxLapAvg);
                        continue;
                    case 298:
                        if (this.mSportEteInfo == null) {
                            this.mSportEteInfo = new SportEteInfo();
                        }
                        input.readMessage(this.mSportEteInfo);
                        continue;
                    case 306:
                        if (this.mSportThaInfo == null) {
                            this.mSportThaInfo = new SportThaInfo();
                        }
                        input.readMessage(this.mSportThaInfo);
                        continue;
                    case 314:
                        if (this.mSportThaWorkout == null) {
                            this.mSportThaWorkout = new SportThaWorkout();
                        }
                        input.readMessage(this.mSportThaWorkout);
                        continue;
                    case 320:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 320);
                        i = this.mGPSLonLatPoint == null ? 0 : this.mGPSLonLatPoint.length;
                        newArray2 = new long[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mGPSLonLatPoint, 0, newArray2, 0, i);
                        }
                        while (i < newArray2.length - 1) {
                            newArray2[i] = input.readInt64();
                            input.readTag();
                            i++;
                        }
                        newArray2[i] = input.readInt64();
                        this.mGPSLonLatPoint = newArray2;
                        continue;
                    case 322:
                        limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readInt64();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.mGPSLonLatPoint == null ? 0 : this.mGPSLonLatPoint.length;
                        newArray2 = new long[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mGPSLonLatPoint, 0, newArray2, 0, i);
                        }
                        while (i < newArray2.length) {
                            newArray2[i] = input.readInt64();
                            i++;
                        }
                        this.mGPSLonLatPoint = newArray2;
                        input.popLimit(limit);
                        continue;
                    case 330:
                        if (this.mTennisInfo == null) {
                            this.mTennisInfo = new TennisInfo();
                        }
                        input.readMessage(this.mTennisInfo);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }

        public static SportStatistics parseFrom(byte[] data) throws InvalidProtocolBufferNanoException {
            return (SportStatistics) MessageNano.mergeFrom(new SportStatistics(), data);
        }
    }

    public static final class SportThaInfo extends ExtendableMessageNano<SportThaInfo> {
        private int bitField0_;
        private int fitnessClass_;
        private int fitnessLevelIncrease_;
        private int trainingLoadTrend_;
        private int vo2MaxTrend_;
        private int wtlStatus_;
        private int wtlSumOptimalMax_;
        private int wtlSumOptimalMin_;
        private int wtlSumOverreaching_;
        private int wtlSum_;

        public int getVo2MaxTrend() {
            return this.vo2MaxTrend_;
        }

        public int getTrainingLoadTrend() {
            return this.trainingLoadTrend_;
        }

        public int getWtlStatus() {
            return this.wtlStatus_;
        }

        public int getWtlSum() {
            return this.wtlSum_;
        }

        public int getWtlSumOptimalMin() {
            return this.wtlSumOptimalMin_;
        }

        public int getWtlSumOptimalMax() {
            return this.wtlSumOptimalMax_;
        }

        public int getWtlSumOverreaching() {
            return this.wtlSumOverreaching_;
        }

        public int getFitnessClass() {
            return this.fitnessClass_;
        }

        public int getFitnessLevelIncrease() {
            return this.fitnessLevelIncrease_;
        }

        public SportThaInfo() {
            clear();
        }

        public SportThaInfo clear() {
            this.bitField0_ = 0;
            this.vo2MaxTrend_ = 0;
            this.trainingLoadTrend_ = 0;
            this.wtlStatus_ = 0;
            this.wtlSum_ = 0;
            this.wtlSumOptimalMin_ = 0;
            this.wtlSumOptimalMax_ = 0;
            this.wtlSumOverreaching_ = 0;
            this.fitnessClass_ = 0;
            this.fitnessLevelIncrease_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.vo2MaxTrend_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt32(2, this.trainingLoadTrend_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.wtlStatus_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeUInt32(4, this.wtlSum_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeUInt32(5, this.wtlSumOptimalMin_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeUInt32(6, this.wtlSumOptimalMax_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeUInt32(7, this.wtlSumOverreaching_);
            }
            if ((this.bitField0_ & 128) != 0) {
                output.writeUInt32(8, this.fitnessClass_);
            }
            if ((this.bitField0_ & 256) != 0) {
                output.writeUInt32(9, this.fitnessLevelIncrease_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.vo2MaxTrend_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(2, this.trainingLoadTrend_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.wtlStatus_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(4, this.wtlSum_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(5, this.wtlSumOptimalMin_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(6, this.wtlSumOptimalMax_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(7, this.wtlSumOverreaching_);
            }
            if ((this.bitField0_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(8, this.fitnessClass_);
            }
            if ((this.bitField0_ & 256) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(9, this.fitnessLevelIncrease_);
            }
            return size;
        }

        public SportThaInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.vo2MaxTrend_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.trainingLoadTrend_ = input.readUInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.wtlStatus_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 32:
                        this.wtlSum_ = input.readUInt32();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.wtlSumOptimalMin_ = input.readUInt32();
                        this.bitField0_ |= 16;
                        continue;
                    case 48:
                        this.wtlSumOptimalMax_ = input.readUInt32();
                        this.bitField0_ |= 32;
                        continue;
                    case 56:
                        this.wtlSumOverreaching_ = input.readUInt32();
                        this.bitField0_ |= 64;
                        continue;
                    case 64:
                        this.fitnessClass_ = input.readUInt32();
                        this.bitField0_ |= 128;
                        continue;
                    case 72:
                        this.fitnessLevelIncrease_ = input.readUInt32();
                        this.bitField0_ |= 256;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SportThaWorkout extends ExtendableMessageNano<SportThaWorkout> {
        private int bitField0_;
        private int distance_;
        private int duration_;
        private int heartRate_;
        private int intensity_;
        private int phrase_;
        private int runningSpeed_;
        private int trainingEffect_;

        public int getPhrase() {
            return this.phrase_;
        }

        public int getDuration() {
            return this.duration_;
        }

        public int getTrainingEffect() {
            return this.trainingEffect_;
        }

        public int getDistance() {
            return this.distance_;
        }

        public int getIntensity() {
            return this.intensity_;
        }

        public int getHeartRate() {
            return this.heartRate_;
        }

        public int getRunningSpeed() {
            return this.runningSpeed_;
        }

        public SportThaWorkout() {
            clear();
        }

        public SportThaWorkout clear() {
            this.bitField0_ = 0;
            this.phrase_ = 0;
            this.duration_ = 0;
            this.trainingEffect_ = 0;
            this.distance_ = 0;
            this.intensity_ = 0;
            this.heartRate_ = 0;
            this.runningSpeed_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.phrase_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt32(2, this.duration_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeUInt32(3, this.trainingEffect_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeUInt32(4, this.distance_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeUInt32(5, this.intensity_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeUInt32(6, this.heartRate_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeUInt32(7, this.runningSpeed_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.phrase_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(2, this.duration_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(3, this.trainingEffect_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(4, this.distance_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(5, this.intensity_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(6, this.heartRate_);
            }
            if ((this.bitField0_ & 64) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(7, this.runningSpeed_);
            }
            return size;
        }

        public SportThaWorkout mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.phrase_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.duration_ = input.readUInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        this.trainingEffect_ = input.readUInt32();
                        this.bitField0_ |= 4;
                        continue;
                    case 32:
                        this.distance_ = input.readUInt32();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.intensity_ = input.readUInt32();
                        this.bitField0_ |= 16;
                        continue;
                    case 48:
                        this.heartRate_ = input.readUInt32();
                        this.bitField0_ |= 32;
                        continue;
                    case 56:
                        this.runningSpeed_ = input.readUInt32();
                        this.bitField0_ |= 64;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class SwimInfo extends ExtendableMessageNano<SwimInfo> {
        private int bitField0_;
        private float mAvgDistancePerStroke_;
        private float mAvgStrokeSpeed_;
        private float mLapStrokeSpeed_;
        private int mLapStrokes_;
        private int mLapSwolf_;
        private float mMaxStrokeSpeed_;
        private float mRtDistancePerStroke_;
        private float mRtStrokeSpeed_;
        private int mSwimStyle_;
        private int mSwolfPerFixedMeters_;
        private int mTotalStrokes_;
        private int mTotalTrips_;

        public int getMTotalStrokes() {
            return this.mTotalStrokes_;
        }

        public int getMTotalTrips() {
            return this.mTotalTrips_;
        }

        public float getMRtDistancePerStroke() {
            return this.mRtDistancePerStroke_;
        }

        public float getMRtStrokeSpeed() {
            return this.mRtStrokeSpeed_;
        }

        public float getMAvgDistancePerStroke() {
            return this.mAvgDistancePerStroke_;
        }

        public float getMAvgStrokeSpeed() {
            return this.mAvgStrokeSpeed_;
        }

        public float getMMaxStrokeSpeed() {
            return this.mMaxStrokeSpeed_;
        }

        public int getMSwolfPerFixedMeters() {
            return this.mSwolfPerFixedMeters_;
        }

        public int getMSwimStyle() {
            return this.mSwimStyle_;
        }

        public SwimInfo() {
            clear();
        }

        public SwimInfo clear() {
            this.bitField0_ = 0;
            this.mTotalStrokes_ = 0;
            this.mTotalTrips_ = 0;
            this.mRtDistancePerStroke_ = 0.0f;
            this.mRtStrokeSpeed_ = 0.0f;
            this.mAvgDistancePerStroke_ = 0.0f;
            this.mAvgStrokeSpeed_ = 0.0f;
            this.mMaxStrokeSpeed_ = 0.0f;
            this.mSwolfPerFixedMeters_ = 0;
            this.mLapStrokes_ = 0;
            this.mLapStrokeSpeed_ = 0.0f;
            this.mLapSwolf_ = 0;
            this.mSwimStyle_ = 0;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeUInt32(1, this.mTotalStrokes_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeUInt32(2, this.mTotalTrips_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeFloat(3, this.mRtDistancePerStroke_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeFloat(4, this.mRtStrokeSpeed_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeFloat(5, this.mAvgDistancePerStroke_);
            }
            if ((this.bitField0_ & 32) != 0) {
                output.writeFloat(6, this.mAvgStrokeSpeed_);
            }
            if ((this.bitField0_ & 64) != 0) {
                output.writeFloat(7, this.mMaxStrokeSpeed_);
            }
            if ((this.bitField0_ & 128) != 0) {
                output.writeUInt32(8, this.mSwolfPerFixedMeters_);
            }
            if ((this.bitField0_ & 256) != 0) {
                output.writeUInt32(9, this.mLapStrokes_);
            }
            if ((this.bitField0_ & 512) != 0) {
                output.writeFloat(10, this.mLapStrokeSpeed_);
            }
            if ((this.bitField0_ & 1024) != 0) {
                output.writeUInt32(11, this.mLapSwolf_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                output.writeUInt32(12, this.mSwimStyle_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(1, this.mTotalStrokes_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(2, this.mTotalTrips_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(3, this.mRtDistancePerStroke_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(4, this.mRtStrokeSpeed_);
            }
            if ((this.bitField0_ & 16) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(5, this.mAvgDistancePerStroke_);
            }
            if ((this.bitField0_ & 32) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(6, this.mAvgStrokeSpeed_);
            }
            if ((this.bitField0_ & 64) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(7, this.mMaxStrokeSpeed_);
            }
            if ((this.bitField0_ & 128) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(8, this.mSwolfPerFixedMeters_);
            }
            if ((this.bitField0_ & 256) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(9, this.mLapStrokes_);
            }
            if ((this.bitField0_ & 512) != 0) {
                size += CodedOutputByteBufferNano.computeFloatSize(10, this.mLapStrokeSpeed_);
            }
            if ((this.bitField0_ & 1024) != 0) {
                size += CodedOutputByteBufferNano.computeUInt32Size(11, this.mLapSwolf_);
            }
            if ((this.bitField0_ & 2048) != 0) {
                return size + CodedOutputByteBufferNano.computeUInt32Size(12, this.mSwimStyle_);
            }
            return size;
        }

        public SwimInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.mTotalStrokes_ = input.readUInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.mTotalTrips_ = input.readUInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 29:
                        this.mRtDistancePerStroke_ = input.readFloat();
                        this.bitField0_ |= 4;
                        continue;
                    case 37:
                        this.mRtStrokeSpeed_ = input.readFloat();
                        this.bitField0_ |= 8;
                        continue;
                    case 45:
                        this.mAvgDistancePerStroke_ = input.readFloat();
                        this.bitField0_ |= 16;
                        continue;
                    case 53:
                        this.mAvgStrokeSpeed_ = input.readFloat();
                        this.bitField0_ |= 32;
                        continue;
                    case 61:
                        this.mMaxStrokeSpeed_ = input.readFloat();
                        this.bitField0_ |= 64;
                        continue;
                    case 64:
                        this.mSwolfPerFixedMeters_ = input.readUInt32();
                        this.bitField0_ |= 128;
                        continue;
                    case 72:
                        this.mLapStrokes_ = input.readUInt32();
                        this.bitField0_ |= 256;
                        continue;
                    case 85:
                        this.mLapStrokeSpeed_ = input.readFloat();
                        this.bitField0_ |= 512;
                        continue;
                    case 88:
                        this.mLapSwolf_ = input.readUInt32();
                        this.bitField0_ |= 1024;
                        continue;
                    case 96:
                        this.mSwimStyle_ = input.readUInt32();
                        this.bitField0_ |= 2048;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }

    public static final class TennisInfo extends ExtendableMessageNano<TennisInfo> {
        public int[] mSwingCnt;

        public TennisInfo() {
            clear();
        }

        public TennisInfo clear() {
            this.mSwingCnt = WireFormatNano.EMPTY_INT_ARRAY;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if (this.mSwingCnt != null && this.mSwingCnt.length > 0) {
                for (int writeUInt32 : this.mSwingCnt) {
                    output.writeUInt32(1, writeUInt32);
                }
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if (this.mSwingCnt == null || this.mSwingCnt.length <= 0) {
                return size;
            }
            int dataSize = 0;
            for (int element : this.mSwingCnt) {
                dataSize += CodedOutputByteBufferNano.computeUInt32SizeNoTag(element);
            }
            return (size + dataSize) + (this.mSwingCnt.length * 1);
        }

        public TennisInfo mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                int arrayLength;
                int i;
                int[] newArray;
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        arrayLength = WireFormatNano.getRepeatedFieldArrayLength(input, 8);
                        i = this.mSwingCnt == null ? 0 : this.mSwingCnt.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mSwingCnt, 0, newArray, 0, i);
                        }
                        while (i < newArray.length - 1) {
                            newArray[i] = input.readUInt32();
                            input.readTag();
                            i++;
                        }
                        newArray[i] = input.readUInt32();
                        this.mSwingCnt = newArray;
                        continue;
                    case 10:
                        int limit = input.pushLimit(input.readRawVarint32());
                        arrayLength = 0;
                        int startPos = input.getPosition();
                        while (input.getBytesUntilLimit() > 0) {
                            input.readUInt32();
                            arrayLength++;
                        }
                        input.rewindToPosition(startPos);
                        i = this.mSwingCnt == null ? 0 : this.mSwingCnt.length;
                        newArray = new int[(i + arrayLength)];
                        if (i != 0) {
                            System.arraycopy(this.mSwingCnt, 0, newArray, 0, i);
                        }
                        while (i < newArray.length) {
                            newArray[i] = input.readUInt32();
                            i++;
                        }
                        this.mSwingCnt = newArray;
                        input.popLimit(limit);
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }
}
