package com.huami.watch.sensorhub;

import com.huami.watch.klvp.KlvpResponse;

public interface WakeupCallback {
    void onWakeup(KlvpResponse klvpResponse);
}
