package com.huami.watch.klvp;

public class KlvpStream {

    public enum KlvpHeader {
        HEADER_NULL,
        HEADER_SINGLE_TODAY_STEP_GOAL,
        HEADER_PERSON_STATISTICS,
        HEADER_SPORT_STATISTICS,
        HEADER_SPORT_CONFIG,
        HEADER_SPORT_MIN_STATISTICS_ARRAY
    }

    private static native KlvpResponse[] nativeReadResponses(WakelockCallback wakelockCallback);

    private static native void nativeSendRequestToSensorHub(char c, short s, byte b, byte b2, byte b3, short s2, byte[] bArr);

    static {
        System.load("/system/lib/hw/klvp.watch.so");
    }

    public static void sendRequestToSensorHub(char direction, short pairId, byte msgRemain, byte cmd, byte response, short target, byte[] value) {
        nativeSendRequestToSensorHub(direction, pairId, msgRemain, cmd, response, target, value);
    }

    public static KlvpResponse[] readResponses(WakelockCallback callback) {
        return nativeReadResponses(callback);
    }
}
