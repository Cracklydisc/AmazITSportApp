package com.huami.watch.klvp;

public interface ResponseCallback {
    void onResponse(KlvpResponse klvpResponse);
}
