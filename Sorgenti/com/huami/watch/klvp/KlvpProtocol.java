package com.huami.watch.klvp;

import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.util.SparseArray;
import com.huami.watch.sensorhub.WakeupCallback;

public class KlvpProtocol {
    private static short globalPairId = (short) 0;
    private static volatile KlvpProtocol mInstance = null;
    private static WakeupCallback mWakeupCallback;
    private SparseArray<ResponseCallback> mCallbacks;
    private WakeLock mWakeLock;
    private WakelockCallback mWakelockCallback;

    class C05211 implements WakelockCallback {
    }

    class C05222 extends Thread {
        final /* synthetic */ KlvpProtocol this$0;

        public void run() {
            while (true) {
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                KlvpResponse[] responses = this.this$0.readResponses(this.this$0.mWakelockCallback);
                if (responses == null) {
                    Log.w("hm_KlvpProtocol", "read null response!");
                } else {
                    for (KlvpResponse response : responses) {
                        if (response != null) {
                            if (response.cmd == (byte) 3) {
                                if (KlvpProtocol.mWakeupCallback != null) {
                                    KlvpProtocol.mWakeupCallback.onWakeup(response);
                                } else {
                                    Log.e("hm_KlvpProtocol", "no wakeup callback registered!!!!");
                                }
                                if (this.this$0.mWakeLock.isHeld()) {
                                    this.this$0.mWakeLock.release();
                                    Log.i("hm_KlvpProtocol", "release wake lock hold by jni");
                                }
                            }
                            short pairId = response.pairId;
                            ResponseCallback callback = (ResponseCallback) this.this$0.mCallbacks.get(pairId);
                            if (callback != null) {
                                callback.onResponse(response);
                                synchronized (this.this$0.mCallbacks) {
                                    this.this$0.mCallbacks.remove(pairId);
                                }
                            } else {
                                continue;
                            }
                        } else {
                            Log.w("hm_KlvpProtocol", "KlvpProtocol get null response !!!");
                        }
                    }
                    continue;
                }
            }
        }
    }

    class KlvpRequestHandler extends Handler {
        final /* synthetic */ KlvpProtocol this$0;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    this.this$0.sendRequest(msg.obj);
                    return;
                default:
                    return;
            }
        }
    }

    private void sendRequest(KlvpRequest request) {
        KlvpStream.sendRequestToSensorHub('a', request.pairId, request.msgRemain, request.cmd, (byte) 0, request.target, request.configValue);
    }

    public KlvpResponse[] readResponses(WakelockCallback callback) {
        return KlvpStream.readResponses(callback);
    }
}
