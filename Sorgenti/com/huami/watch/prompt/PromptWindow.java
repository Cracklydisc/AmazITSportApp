package com.huami.watch.prompt;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.media.AudioAttributes;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.wearable.view.BoxInsetLayout;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import com.huami.watch.ui.C1070R;
import com.huami.watch.utils.Gloable;

public class PromptWindow extends Dialog implements android.content.DialogInterface.OnDismissListener, OnClickListener {
    public static final boolean DEBUG = Gloable.DEBUG;
    private static final String TAG = PromptWindow.class.getSimpleName();
    private int WAKE_LOCK_TIMEOUT;
    private boolean mCancel;
    private Context mContext;
    private IDialogKeyEventListener mDialogKeyListener;
    private OnDismissListener mDismissListener;
    private boolean mFromSlpt;
    private Handler mHandler;
    private boolean mHasTouch;
    private boolean mInterceptTouchEvent;
    private BoxInsetLayout mParent;
    PowerManager mPowerManager;
    WakeLock mWakeLock;

    public interface IDialogKeyEventListener {
        void dispatchDialogKeyEvent(@NonNull KeyEvent keyEvent);
    }

    public interface OnDismissListener {
        void onDismiss();
    }

    private class EatTouchBoxInsetLayout extends BoxInsetLayout {
        public EatTouchBoxInsetLayout(Context context) {
            super(context);
        }

        public boolean onInterceptTouchEvent(MotionEvent ev) {
            PromptWindow.this.mHasTouch = true;
            if (PromptWindow.this.mInterceptTouchEvent) {
                return true;
            }
            return false;
        }
    }

    public static class LayoutParams extends android.support.wearable.view.BoxInsetLayout.LayoutParams {
        public LayoutParams(int width, int height) {
            super(width, height);
        }
    }

    private class PromptHandler extends Handler {
        private PromptHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    PromptWindow.this.dismiss();
                    return;
                default:
                    return;
            }
        }
    }

    public PromptWindow(Context context) {
        this(context, C1070R.style.PromptDialog);
    }

    private PromptWindow(Context context, int theme) {
        super(context, C1070R.style.PromptDialog);
        this.WAKE_LOCK_TIMEOUT = 6000;
        this.mHasTouch = false;
        this.mFromSlpt = false;
        this.mCancel = false;
        this.mInterceptTouchEvent = true;
        this.mDialogKeyListener = null;
        this.mContext = context;
        getWindow().setType(2003);
        setOnDismissListener(this);
        setCanceledOnTouchOutside(false);
        this.mHandler = new PromptHandler();
        this.mPowerManager = (PowerManager) context.getSystemService("power");
        this.mWakeLock = this.mPowerManager.newWakeLock(268435466, "PromptWindow");
    }

    public void setContentView(View view) {
        setContentView(view, new android.view.ViewGroup.LayoutParams(-1, getScreenHeight(this.mContext)));
    }

    public void setContentView(int layoutResID) {
        if (this.mParent == null) {
            this.mParent = new EatTouchBoxInsetLayout(this.mContext);
            this.mParent.setOnClickListener(this);
        } else {
            this.mParent.removeAllViews();
        }
        LayoutInflater.from(this.mContext).inflate(layoutResID, this.mParent, true);
        super.setContentView(this.mParent, new android.view.ViewGroup.LayoutParams(-1, getScreenHeight(this.mContext)));
    }

    public void setContentView(View view, LayoutParams params) {
        android.support.wearable.view.BoxInsetLayout.LayoutParams boxlp;
        if (this.mParent == null) {
            this.mParent = new EatTouchBoxInsetLayout(this.mContext);
            this.mParent.setOnClickListener(this);
        } else {
            this.mParent.removeAllViews();
        }
        if (params != null) {
            boxlp = new android.support.wearable.view.BoxInsetLayout.LayoutParams((android.support.wearable.view.BoxInsetLayout.LayoutParams) params);
        } else {
            boxlp = new android.support.wearable.view.BoxInsetLayout.LayoutParams(-2, -2, 17, 15);
        }
        this.mParent.addView(view, boxlp);
        super.setContentView(this.mParent, new android.view.ViewGroup.LayoutParams(-1, getScreenHeight(this.mContext)));
    }

    public void setAutoDismissDelay(int delay) {
        if (delay > 0) {
            this.WAKE_LOCK_TIMEOUT = delay;
        }
    }

    public final void onDismiss(DialogInterface dialog) {
        if (DEBUG) {
            Log.d(TAG, "PromptWindow call onDismiss method " + dialog);
        }
        this.mHandler.removeMessages(1);
        if (this.mDismissListener != null) {
            this.mDismissListener.onDismiss();
        }
        if (this.mParent != null) {
            this.mParent.removeAllViews();
            this.mParent = null;
        }
        releaseWakeLock();
        if (!(this.mHasTouch || !this.mFromSlpt || this.mCancel)) {
            this.mPowerManager.goToSleep(SystemClock.uptimeMillis());
        }
        this.mHasTouch = false;
        this.mFromSlpt = false;
    }

    public void setOnDismissListener(OnDismissListener dismissListener) {
        this.mDismissListener = dismissListener;
    }

    public void show() {
        acquirewakeLock();
        this.mHandler.sendEmptyMessageDelayed(1, (long) this.WAKE_LOCK_TIMEOUT);
        super.show();
    }

    public void showWithVibrator(long[] pattern, int repeat, AudioAttributes attributes) {
        show();
        ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(pattern, repeat, attributes);
    }

    private int getScreenHeight(Context context) {
        Display display = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    private void releaseWakeLock() {
        Log.d(TAG, "PromptWindow releaseWakeLock" + this);
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    private void acquirewakeLock() {
        Log.d(TAG, "PromptWindow acquirewakeLock" + this);
        if (this.mPowerManager.isScreenOn()) {
            this.mFromSlpt = false;
        } else {
            this.mFromSlpt = true;
        }
        this.mWakeLock.acquire((long) (this.WAKE_LOCK_TIMEOUT + 2000));
    }

    public void onClick(View v) {
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        Log.d(TAG, "PromptWindow onWindowFocusChanged " + this + " " + hasFocus);
        if (!hasFocus) {
            this.mCancel = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public void setDialogKeyListener(IDialogKeyEventListener listener) {
        this.mDialogKeyListener = listener;
    }

    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        Log.d(TAG, "PromptWindow dispatchKeyEvent, " + event);
        this.mHasTouch = true;
        if (event.getKeyCode() == 140 && event.getAction() == 1 && event.getFlags() == 8) {
            dismiss();
        }
        if (this.mDialogKeyListener != null) {
            this.mDialogKeyListener.dispatchDialogKeyEvent(event);
        }
        return super.dispatchKeyEvent(event);
    }
}
