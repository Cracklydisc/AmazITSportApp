package com.huami.watch.wifitrans.control.client;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import clc.component.flower.BeeClient;
import clc.component.flower.BeeClient.OnBigDataListener;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class AbstractWifiTransClient extends BeeClient {
    private TaskManager mClientTaskManger = new TaskManager("wifi-trans-of-client");
    private ConcurrentLinkedQueue<DataItem> mRequestQueue = new ConcurrentLinkedQueue();
    private DataItem mResponseItem;
    private TaskManager mServerTaskManger = new TaskManager("wifi-trans-of-server");
    private Thread myThread;

    protected abstract int onStartWifiTrans();

    protected abstract int requestCount();

    public AbstractWifiTransClient(Context context) {
        super(context);
    }

    protected String fetchActionForHostAvalible() {
        return "com.huami.watch.wifitrans.SERVICE_AVALIBLE";
    }

    protected String getFlowerPkg() {
        return "com.huami.watch.wearservices";
    }

    protected String getFlowerAction() {
        return "com.huami.watch.wearservices.WIFI_TRANS_SERVICE";
    }

    protected void dataFromFlower(int reqId, final Bundle dataRev) {
        super.dataFromFlower(reqId, dataRev);
        switch (reqId) {
            case 0:
                this.mClientTaskManger.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        AbstractWifiTransClient.this.onStartWifiTrans();
                        Log.i("wifi_trans", "5.onStartWifiTrans SELF:" + AbstractWifiTransClient.this.mContext.getPackageName());
                        return null;
                    }
                }).execute();
                return;
            case 2:
                this.mServerTaskManger.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        AbstractWifiTransClient.this.onServerResponse(dataRev);
                        return null;
                    }
                }).execute();
                return;
            case 8:
                this.mClientTaskManger.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        int count = AbstractWifiTransClient.this.requestCount();
                        Log.i("wifi_trans", "2.CLIENT CMD_C2S_COUNT_NUM::" + count + "SELF:" + AbstractWifiTransClient.this.mContext.getPackageName());
                        AbstractWifiTransClient.this.sendCountToServer(count);
                        return null;
                    }
                }).execute();
                return;
            case 12:
                onDeleteCacheSucess();
                return;
            case 13:
                finished();
                return;
            default:
                return;
        }
    }

    public synchronized DataItem request(DataItem item) {
        DataItem dataItem;
        this.myThread = Thread.currentThread();
        if (item == null) {
            dataItem = null;
        } else {
            final DataItem dataItem2 = item;
            final Bundle b = new Bundle();
            b.putString("data", dataItem2.toString());
            request(1, b, new OnBigDataListener() {
                public void onSendBigData() {
                    dataItem2.addFlags(32768);
                    dataItem2.addExtraPair("d-path", DataUtils.saveToFileAndManage("wifi_" + dataItem2.getIdentifier(), dataItem2.getData()));
                    dataItem2.setData(null);
                    b.putString("data", dataItem2.toString());
                    AbstractWifiTransClient.this.request(1, b, null);
                }
            });
            try {
                synchronized (this.myThread) {
                    this.myThread.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dataItem = this.mResponseItem;
        }
        return dataItem;
    }

    private void onServerResponse(Bundle revData) {
        String data = revData.getString("data");
        if (!TextUtils.isEmpty(data)) {
            this.mResponseItem = DataItem.from(data);
            if (this.myThread != null) {
                synchronized (this.myThread) {
                    this.myThread.interrupt();
                }
            }
        } else if (this.myThread != null) {
            synchronized (this.myThread) {
                this.myThread.interrupt();
            }
        }
    }

    protected void onFlowerOffline() {
        super.onFlowerOffline();
        this.mRequestQueue = null;
        this.mResponseItem = null;
        if (this.myThread != null) {
            synchronized (this.myThread) {
                this.myThread.interrupt();
            }
        }
        Log.i("wifi_trans", "onFlowerOffline and thread interrupt...");
    }

    private void sendCountToServer(int count) {
        Bundle b = new Bundle();
        b.putInt("count_start", count);
        request(6, b, null);
    }

    public void onDeleteCacheSucess() {
        if (this.myThread != null) {
            synchronized (this.myThread) {
                this.myThread.interrupt();
                Log.i("wifi_trans", "onDeleteCacheSucess myThread.interrupt()" + this.mContext.getPackageName());
            }
        }
    }

    public void tellServerFinishSelf(int res, int count) {
        this.myThread = Thread.currentThread();
        Log.i("wifi_trans", "6. client tell server finish :" + this.mContext.getPackageName() + " res:" + res + "-----------count:" + count);
        if (res == 137) {
            request(10, null, null);
            try {
                synchronized (this.myThread) {
                    Log.i("wifi_trans", "tell success myThread.wait() 120000" + this.mContext.getPackageName());
                    this.myThread.wait(120000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Bundle b = new Bundle();
        b.putInt("count", count);
        b.putInt("load_res", res);
        request(5, b, null);
    }

    public void finished() {
        if (this.myThread != null) {
            synchronized (this.myThread) {
                this.myThread.interrupt();
                Log.i("wifi_trans", "finished myThread.interrupt()" + this.mContext.getPackageName());
            }
        }
    }
}
