package com.huami.watch.wifitrans.control.server;

import android.content.ComponentName;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import clc.component.flower.AbstractFlowerService;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.Utils;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.global.SyncNodeSwitcher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.wifitrans.WCommonParams;
import com.huami.watch.wifitrans.WTokenHolder;
import com.huami.watch.wifitrans.WifiHttpRequestor;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WifiTransServer extends AbstractFlowerService {
    private HashMap<String, Integer> allClientNumTable = new HashMap();
    private HashMap<String, Integer> allClientResTable = new HashMap();
    private DataCacher dataCacher;
    private ConcurrentHashMap<String, ConcurrentLinkedQueue<DataItem>> requestQueueMap = new ConcurrentHashMap();
    private TaskManager taskManager = new TaskManager("cloud-wifi-trans");

    protected void dispatchClientReq(int reqId, Bundle data, String uuid) {
        switch (reqId) {
            case 1:
                String things = data.getString("data");
                if (!TextUtils.isEmpty(things)) {
                    ConcurrentLinkedQueue q = (ConcurrentLinkedQueue) this.requestQueueMap.get(uuid);
                    if (q == null) {
                        ConcurrentHashMap concurrentHashMap = this.requestQueueMap;
                        q = new ConcurrentLinkedQueue();
                        concurrentHashMap.put(uuid, q);
                    }
                    DataItem item = DataItem.from(things);
                    q.add(item);
                    item.addExtraPair("uuid", uuid);
                    kickToCloud(item);
                    return;
                }
                return;
            case 3:
                saveToken(data);
                return;
            case 4:
                for (String client : retriveAllClientIds()) {
                    postDataToClient(client, 0, null);
                }
                return;
            case 5:
                int res = data.getInt("load_res");
                Log.i("wifi_trans", "8.CMD_C2S_UPLOAD_FINISH:::" + findComponentNameByUUID(uuid).getPackageName() + ":" + res);
                addResValues(uuid, res);
                onResultReceive();
                return;
            case 6:
                int num = data.getInt("count_start");
                Log.i("wifi_trans", "3.SERVER-CMD_C2S_START_COUNT:::" + findComponentNameByUUID(uuid) + ":" + num);
                addNumValues(uuid, num);
                if (num == 0) {
                    addResValues(uuid, 137);
                }
                onCountReceive();
                return;
            case 10:
                if (this.dataCacher == null) {
                    this.dataCacher = DataCacher.getInstance(this);
                }
                String targetPkg = findComponentNameByUUID(uuid).getPackageName();
                int deleteState = this.dataCacher.deleteByOwnerWithState(targetPkg, 1, 5);
                Log.i("wifi_trans", "switchState count: " + this.dataCacher.updateByOwnerToState(targetPkg, 9, 1));
                Log.i("wifi_trans", "7.server delete local.deleteState:" + deleteState + " FROM :" + findComponentNameByUUID(uuid));
                postDataToClient(uuid, 12, null);
                return;
            case 99:
                SyncNodeSwitcher.self().changeTo(0, false);
                Log.i("wifi_trans", "1.SERVER-CMD_S2C_START:::" + findComponentNameByUUID(uuid));
                WCommonParams.doInit(this);
                initAllClientStart();
                initAllClientRes();
                for (String client2 : retriveAllClientIds()) {
                    postDataToClient(client2, 8, null);
                }
                return;
            default:
                return;
        }
    }

    void kickToCloud(final DataItem item) {
        this.taskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                String uuid = item.getExtraValByKey("uuid");
                String dPath = null;
                if (item.hasFlag(32768)) {
                    dPath = item.getExtraValByKey("d-path");
                    String bigData = Utils.readFileToString(dPath);
                    item.removeExtraPairByKey("d-path");
                    item.setData(bigData);
                }
                DataItem res = WifiHttpRequestor.getInstance().request(WifiTransServer.this.getApplicationContext(), item);
                Bundle b = new Bundle();
                b.putString("data", res.toString());
                WifiTransServer.this.postDataToClient(uuid, 2, b);
                if (WifiTransServer.this.isInAllClientSet()) {
                    WifiTransServer.this.sendToAdmin(9, null);
                }
                if (dPath != null) {
                    new File(dPath).delete();
                }
                return null;
            }
        }).execute();
    }

    private void sendToAdmin(int cmd, Bundle bundle) {
        try {
            String uuid = findUUIDByComponentName(new ComponentName("com.huami.watch.wifiuploaddata", "com.huami.watch.wifiuploaddata.upload.AdminWifiTransClient"));
            if (!TextUtils.isEmpty(uuid)) {
                postDataToClient(uuid, cmd, bundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("wifi_trans", "sendToAdmin has null object");
        }
    }

    private void sendToAllClient(int cmd) {
        for (String client : retriveAllClientIds()) {
            postDataToClient(client, cmd, null);
        }
    }

    protected void onBeeDropping(ComponentName cname) {
        super.onBeeDropping(cname);
        String uuid = findUUIDByComponentName(cname);
        if (TextUtils.isEmpty(uuid)) {
            Log.i("wifi_trans", "onBeeDropping():uuid is null !!!-" + cname);
            onResultReceive();
            onCountReceive();
            return;
        }
        if (this.allClientNumTable != null) {
            this.allClientNumTable.remove(uuid);
            onCountReceive();
        }
        if (this.allClientResTable != null) {
            this.allClientResTable.remove(uuid);
            onResultReceive();
        }
    }

    private void onResultReceive() {
        if (isInAllClientRes()) {
            Bundle b = new Bundle();
            b.putBoolean("all_res", allRes());
            SyncNodeSwitcher.self().changeTo(0, true);
            sendToAdmin(11, b);
            sendToAllClient(13);
            try {
                if (this.dataCacher == null) {
                    this.dataCacher = DataCacher.getInstance(this);
                }
                Log.i("wifi_trans", "upload all data state when finished---> count: " + this.dataCacher.uploadToState(9, 1));
            } catch (Exception e) {
                Log.i("wifi_trans", "uploadToState Exception...");
                e.printStackTrace();
            }
        }
    }

    private void onCountReceive() {
        if (isInAllClientSet()) {
            Bundle b = new Bundle();
            b.putInt("all_count", allCount());
            sendToAdmin(7, b);
        }
    }

    protected String fetchFlowerAvalibleAction() {
        return "com.huami.watch.wifitrans.SERVICE_AVALIBLE";
    }

    private void saveToken(Bundle b) {
        if (b != null) {
            String token = b.getString("token");
            String provider = b.getString("provider");
            String uid = b.getString("uid");
            if (!TextUtils.isEmpty(token)) {
                getSharedPreferences("common", 0).edit().putString("t", token).putString("p", provider).putString("u", uid).commit();
                WTokenHolder.getHolder().updateTokenHolder(getApplicationContext());
            }
        }
    }

    private void initAllClientStart() {
        this.allClientNumTable.clear();
        for (String uuId : retriveAllClientIds()) {
            ComponentName pckName = findComponentNameByUUID(uuId);
            if (pckName != null) {
                Log.i("wifi_trans", "pckName::" + pckName.getPackageName());
                if (TextUtils.equals("com.huami.watch.wifiuploaddata", pckName.getPackageName()) && TextUtils.equals("com.huami.watch.wifiuploaddata.upload.AdminWifiTransClient", pckName.getClassName())) {
                    this.allClientNumTable.put(uuId, Integer.valueOf(0));
                } else {
                    this.allClientNumTable.put(uuId, Integer.valueOf(-1));
                }
            }
        }
    }

    private void initAllClientRes() {
        this.allClientResTable.clear();
        for (String uuId : retriveAllClientIds()) {
            ComponentName pckName = findComponentNameByUUID(uuId);
            if (pckName != null) {
                Log.i("wifi_trans", "allClientResTable:pckName::" + pckName.getPackageName());
                if (TextUtils.equals("com.huami.watch.wifiuploaddata", pckName.getPackageName()) && TextUtils.equals("com.huami.watch.wifiuploaddata.upload.AdminWifiTransClient", pckName.getClassName())) {
                    this.allClientResTable.put(uuId, Integer.valueOf(137));
                } else {
                    this.allClientResTable.put(uuId, Integer.valueOf(-1));
                }
            }
        }
    }

    private void addNumValues(String uuId, int num) {
        if (this.allClientNumTable.containsKey(uuId)) {
            this.allClientNumTable.put(uuId, Integer.valueOf(num));
        }
    }

    private void addResValues(String uuId, int resCode) {
        if (this.allClientResTable.containsKey(uuId)) {
            this.allClientResTable.put(uuId, Integer.valueOf(resCode));
        }
    }

    private boolean isInAllClientSet() {
        for (String clientId : this.allClientNumTable.keySet()) {
            if (((Integer) this.allClientNumTable.get(clientId)).intValue() == -1) {
                return false;
            }
        }
        return true;
    }

    private boolean isInAllClientRes() {
        for (String clientId : this.allClientResTable.keySet()) {
            if (((Integer) this.allClientResTable.get(clientId)).intValue() == -1) {
                return false;
            }
        }
        return true;
    }

    private int allCount() {
        int count = 0;
        for (String clientId : this.allClientNumTable.keySet()) {
            count += ((Integer) this.allClientNumTable.get(clientId)).intValue();
        }
        return count;
    }

    private boolean allRes() {
        for (String clientId : this.allClientResTable.keySet()) {
            if (((Integer) this.allClientResTable.get(clientId)).intValue() == 153) {
                return false;
            }
        }
        return true;
    }

    public void onDestroy() {
        SyncNodeSwitcher.self().changeTo(0, true);
        super.onDestroy();
    }
}
