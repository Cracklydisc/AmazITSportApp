package com.huami.watch.wifitrans;

public class WURLCompleter {

    public static class URLSeat {
        public static final String PROVIDER = WRAP("PRD");
        public static final String TOKEN = WRAP("TK");
        public static final String USER_ID = WRAP("UID");

        private static String WRAP(String str) {
            return "[=>..^$^S]" + str + "[E*$*..<=]";
        }
    }
}
