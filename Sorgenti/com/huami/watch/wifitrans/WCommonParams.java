package com.huami.watch.wifitrans;

import android.content.Context;
import clc.utils.taskmanager.TaskManager;
import com.huami.watch.transport.httpsupport.global.PublicParamsBuilder;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.transporter.http.URLCompleter;
import com.huami.watch.transport.httpsupport.transporter.http.URLCompleter.URLSeat;
import com.squareup.okhttp.Request.Builder;
import java.util.TimeZone;

public class WCommonParams {
    private static TaskManager sTaskManager = new TaskManager();
    private static String token = null;

    static class C10761 extends PublicParamsBuilder {
        C10761() {
        }

        public synchronized void fillPublicHeader(Context context, DataItem dataItem, Builder requestBuilder) {
            WCommonParams.buildCommonHeaderParams(context, dataItem, requestBuilder);
        }

        public synchronized String completeURL(Context context, String url) {
            WTokenHolder holder;
            holder = WTokenHolder.getHolder();
            return URLCompleter.fillWithTokenRelated(URLCompleter.fillWithTokenRelated(URLCompleter.fillWithTokenRelated(url, URLSeat.USER_ID, holder.getUid()), URLSeat.PROVIDER, holder.getProvider()), URLSeat.TOKEN, holder.getAccessToken());
        }
    }

    public static void doInit(Context context) {
        WTokenHolder.getHolder().updateTokenHolder(context);
        WifiHttpRequestor.getInstance().setPublicParamsBuilder(initPulicParamBuilder(context));
    }

    private static PublicParamsBuilder initPulicParamBuilder(Context context) {
        return new C10761();
    }

    private static void buildCommonHeaderParams(Context context, DataItem item, Builder requestBuilder) {
        requestBuilder.addHeader(PublicParamsBuilder.HEAD_APP_NAME, "com.huami.watch");
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_APP_TOKERN, WTokenHolder.getHolder().getAccessToken());
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_CALLID, String.valueOf(System.currentTimeMillis() / 1000));
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_COUNTRY, String.valueOf(context.getResources().getConfiguration().locale.getCountry()));
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_TIMEZONE, String.valueOf(TimeZone.getDefault().getID()));
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_CHANNEL, WAppUtil.getMetaDataChannel(context));
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_PLATFORM, "android_phone");
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_APP_VER, String.valueOf(WAppUtil.getVersionCode(context)));
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_PROTO_VER, "1.0");
        requestBuilder.addHeader(PublicParamsBuilder.HEADER_LOCALE, context.getResources().getConfiguration().locale.getLanguage());
    }
}
