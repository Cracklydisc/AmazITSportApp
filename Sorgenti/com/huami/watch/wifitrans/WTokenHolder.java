package com.huami.watch.wifitrans;

import android.content.Context;
import android.content.SharedPreferences;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;

public class WTokenHolder {
    private static WTokenHolder sInstance;
    private static TaskManager sTaskManager = new TaskManager();
    private String accessToken;
    private String provider;
    private String uid;

    public static synchronized WTokenHolder getHolder() {
        WTokenHolder wTokenHolder;
        synchronized (WTokenHolder.class) {
            if (sInstance == null) {
                sInstance = new WTokenHolder();
            }
            wTokenHolder = sInstance;
        }
        return wTokenHolder;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public String getUid() {
        return this.uid;
    }

    public String getProvider() {
        return this.provider;
    }

    public void update(Context context) {
        SharedPreferences pref = context.getSharedPreferences("common", 0);
        this.accessToken = pref.getString("t", "");
        this.provider = pref.getString("p", "");
        this.uid = pref.getString("u", "");
    }

    public void updateTokenHolder(final Context context) {
        sTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                WTokenHolder.getHolder().update(context);
                return null;
            }
        }).execute();
    }
}
