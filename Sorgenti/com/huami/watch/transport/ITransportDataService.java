package com.huami.watch.transport;

import android.content.ComponentName;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITransportDataService extends IInterface {

    public static abstract class Stub extends Binder implements ITransportDataService {

        private static class Proxy implements ITransportDataService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void registerDataListener(String name, ITransportDataListener mConnectListener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeString(name);
                    _data.writeStrongBinder(mConnectListener != null ? mConnectListener.asBinder() : null);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void unregisterDataListener(String name) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeString(name);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void registerChannelListener(String name, ITransportChannelListener mChannelListener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeString(name);
                    _data.writeStrongBinder(mChannelListener != null ? mChannelListener.asBinder() : null);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void unregisterChannelListener(String name) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeString(name);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void registersendCallbackListener(ITransportCallbackListener mCallbackListener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeStrongBinder(mCallbackListener != null ? mCallbackListener.asBinder() : null);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void unregistersendCallbackListener(ITransportCallbackListener mCallbackListener) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    _data.writeStrongBinder(mCallbackListener != null ? mCallbackListener.asBinder() : null);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void registerComponentName(ComponentName mComponentName, String name) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    if (mComponentName != null) {
                        _data.writeInt(1);
                        mComponentName.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(name);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void sendData(TransportDataItem item) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataService");
                    if (item != null) {
                        _data.writeInt(1);
                        item.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.transport.ITransportDataService");
        }

        public static ITransportDataService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.transport.ITransportDataService");
            if (iin == null || !(iin instanceof ITransportDataService)) {
                return new Proxy(obj);
            }
            return (ITransportDataService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    registerDataListener(data.readString(), com.huami.watch.transport.ITransportDataListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    unregisterDataListener(data.readString());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    registerChannelListener(data.readString(), com.huami.watch.transport.ITransportChannelListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    unregisterChannelListener(data.readString());
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    registersendCallbackListener(com.huami.watch.transport.ITransportCallbackListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    unregistersendCallbackListener(com.huami.watch.transport.ITransportCallbackListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 7:
                    ComponentName _arg0;
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    if (data.readInt() != 0) {
                        _arg0 = (ComponentName) ComponentName.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    registerComponentName(_arg0, data.readString());
                    reply.writeNoException();
                    return true;
                case 8:
                    TransportDataItem _arg02;
                    data.enforceInterface("com.huami.watch.transport.ITransportDataService");
                    if (data.readInt() != 0) {
                        _arg02 = (TransportDataItem) TransportDataItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    sendData(_arg02);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.transport.ITransportDataService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void registerChannelListener(String str, ITransportChannelListener iTransportChannelListener) throws RemoteException;

    void registerComponentName(ComponentName componentName, String str) throws RemoteException;

    void registerDataListener(String str, ITransportDataListener iTransportDataListener) throws RemoteException;

    void registersendCallbackListener(ITransportCallbackListener iTransportCallbackListener) throws RemoteException;

    void sendData(TransportDataItem transportDataItem) throws RemoteException;

    void unregisterChannelListener(String str) throws RemoteException;

    void unregisterDataListener(String str) throws RemoteException;

    void unregistersendCallbackListener(ITransportCallbackListener iTransportCallbackListener) throws RemoteException;
}
