package com.huami.watch.transport;

import android.support.annotation.NonNull;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;

public class TransporterBLE extends Transporter {
    private TransporterBLE() {
    }

    public void send(@NonNull String action, DataBundle data, DataSendResultCallback result) {
    }

    public void addDataListener(DataListener listener) {
    }

    public void removeDataListener(DataListener listener) {
    }
}
