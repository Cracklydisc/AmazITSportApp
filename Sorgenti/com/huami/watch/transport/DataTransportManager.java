package com.huami.watch.transport;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.util.ArrayMap;
import com.huami.watch.transport.DataApi.CallbackListener;
import com.huami.watch.transport.HmApiClient.ConnectionCallbacks;
import com.huami.watch.transport.HmApiClient.OnConnectionFailedListener;
import com.huami.watch.transport.ITransportDataService.Stub;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.util.ArrayList;

public class DataTransportManager {
    public static final boolean DEBUG = Config.isDebug();
    private static final ArrayList<String> sWhiteList = new ArrayList();
    private Intent mBindIntent;
    private ConnectionCallbacks mConnectionCallback;
    private OnConnectionFailedListener mConnectionFailedListener;
    private Context mContext;
    private ArrayMap<String, CallbackListener> mDataCallbacks;
    private TransportCallbackListener mTransportCallback;
    private ITransportDataService mTransportService;

    private class BindServiceHandler extends Handler {
        private int mState;
        private ServiceConnection mTransportServiceConnection;
        final /* synthetic */ DataTransportManager this$0;

        class C09911 implements ServiceConnection {
            final /* synthetic */ BindServiceHandler this$1;

            public void onServiceConnected(ComponentName name, IBinder service) {
                this.this$1.this$0.mTransportService = Stub.asInterface(service);
                this.this$1.notifyServiceConnected(service);
            }

            public void onServiceDisconnected(ComponentName name) {
                this.this$1.notifyServiceDisconnected();
            }
        }

        public void notifyConnectFailed(int reason) {
            Message msg = Message.obtain(this);
            msg.what = 9;
            msg.obj = Integer.valueOf(reason);
            msg.sendToTarget();
        }

        public void notifyServiceConnected(IBinder service) {
            Message msg = Message.obtain(this);
            msg.what = 20;
            msg.obj = service;
            msg.sendToTarget();
        }

        public void notifyServiceDisconnected() {
            Message msg = Message.obtain(this);
            msg.what = 14;
            msg.sendToTarget();
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 9:
                    synchronized (this) {
                        this.mState = 0;
                    }
                    this.this$0.mConnectionFailedListener.onServicesConnectionFailed(new ConnectionResult(((Integer) msg.obj).intValue()));
                    return;
                case 14:
                    synchronized (this) {
                        if (this.mState == 3) {
                            this.mState = 0;
                        }
                    }
                    this.this$0.mConnectionCallback.onServicesDisConnected(new ConnectionResult(3));
                    return;
                case 19:
                    boolean ok = false;
                    try {
                        Log.m26d("Trans-Manager", "HmApiClient [" + this.mTransportServiceConnection + "] connect" + " to Transport service", new Object[0]);
                        ok = this.this$0.mContext.bindService(this.this$0.mBindIntent, this.mTransportServiceConnection, 1);
                    } catch (Exception e) {
                        Log.m27e("Trans-Manager", "Failed binding to DataTransportService.", e, new Object[0]);
                    }
                    if (!ok) {
                        notifyConnectFailed(1);
                        return;
                    }
                    return;
                case 20:
                    synchronized (this) {
                        this.mState = 3;
                    }
                    this.this$0.mConnectionCallback.onServicesConnected(new Bundle());
                    try {
                        this.this$0.mTransportService.registersendCallbackListener(this.this$0.mTransportCallback);
                        return;
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                        Log.m28e("Trans-Manager", "register callback list error!", new Object[0]);
                        return;
                    }
                case 87:
                    try {
                        this.this$0.mTransportService.unregistersendCallbackListener(this.this$0.mTransportCallback);
                        Log.m26d("Trans-Manager", "HmApiClient [" + this.mTransportServiceConnection + "] " + "disconnect to Transport service", new Object[0]);
                    } catch (RemoteException e22) {
                        Log.m27e("Trans-Manager", "register callback listener error : ", e22, new Object[0]);
                    }
                    this.this$0.mContext.unbindService(this.mTransportServiceConnection);
                    notifyServiceDisconnected();
                    return;
                default:
                    return;
            }
        }
    }

    private class TransportCallbackListener extends ITransportCallbackListener.Stub {
        final /* synthetic */ DataTransportManager this$0;

        public void onResultBack(DataTransportResult result) throws RemoteException {
            Log.m30v("Trans-Manager", "OnResultBack, Uri : " + result.getTransportItem().getUri() + ", Time : " + result.getTransportItem().getCreateTime(), new Object[0]);
            String key = String.valueOf(result.getTransportItem().getCreateTime());
            CallbackListener listener = (CallbackListener) this.this$0.mDataCallbacks.get(key);
            if (listener != null) {
                listener.onResultBack(result);
                this.this$0.mDataCallbacks.remove(key);
            }
        }
    }

    static {
        sWhiteList.add("com.ingenic.iwds.device");
        sWhiteList.add("com.huami.watch.launcher");
        sWhiteList.add("com.ingenic.watchmanager");
        sWhiteList.add("com.huami.watch.hmwatchmanager");
        sWhiteList.add("com.huami.watch.wearservices");
    }
}
