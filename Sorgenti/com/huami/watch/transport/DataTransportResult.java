package com.huami.watch.transport;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class DataTransportResult implements Parcelable {
    public static final Creator<DataTransportResult> CREATOR = new C09921();
    private int resultCode;
    private TransportDataItem transportItem;

    static class C09921 implements Creator<DataTransportResult> {
        C09921() {
        }

        public DataTransportResult createFromParcel(Parcel in) {
            return new DataTransportResult(in);
        }

        public DataTransportResult[] newArray(int size) {
            return new DataTransportResult[size];
        }
    }

    public DataTransportResult(TransportDataItem item, int code) {
        this.transportItem = item;
        this.resultCode = code;
    }

    public DataTransportResult(Parcel in) {
        this.transportItem = (TransportDataItem) in.readParcelable(TransportDataItem.class.getClassLoader());
        this.resultCode = in.readInt();
    }

    public TransportDataItem getTransportItem() {
        return this.transportItem;
    }

    public int getResultCode() {
        return this.resultCode;
    }

    public String toString() {
        switch (this.resultCode) {
            case 0:
                return "OK";
            case 1:
                return "FAILED_CHANNEL_UNAVAILABLE";
            case 2:
                return "FAILED_LINK_DISCONNECTED";
            case 3:
                return "FAILED_IWDS_CRASH";
            case 4:
                return "FAILED_TRANSPORT_SERVICE_UNCONNECTED";
            default:
                return "NONE";
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.transportItem, 0);
        dest.writeInt(this.resultCode);
    }
}
