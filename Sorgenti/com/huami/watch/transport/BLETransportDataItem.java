package com.huami.watch.transport;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class BLETransportDataItem extends TransportDataItem {
    public static final Creator<BLETransportDataItem> CREATOR = new C09851();
    private String value;

    static class C09851 implements Creator<BLETransportDataItem> {
        C09851() {
        }

        public BLETransportDataItem createFromParcel(Parcel source) {
            return new BLETransportDataItem(source);
        }

        public BLETransportDataItem[] newArray(int size) {
            return new BLETransportDataItem[size];
        }
    }

    public BLETransportDataItem(Parcel source) {
        super(source);
        this.value = source.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.value);
    }
}
