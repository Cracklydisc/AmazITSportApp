package com.huami.watch.transport;

import android.os.RemoteException;
import com.huami.watch.transport.ITransportChannelListener.Stub;
import com.huami.watch.util.Config;
import java.util.ArrayList;
import java.util.Iterator;

public class ChannelApi {
    public static final boolean DEBUG = Config.isDebug();
    private static ChannelApi mChannelApi;
    private boolean hasRegister = false;
    private ITransportChannelListener mChannelListener = new TransportChannelListener();
    private ArrayList<ChannelListener> mListenerList = new ArrayList();

    public interface ChannelListener {
        void onChannelChanged(boolean z);
    }

    private class TransportChannelListener extends Stub {
        private TransportChannelListener() {
        }

        public void onChannelChanged(boolean available) throws RemoteException {
            Iterator i$ = ChannelApi.this.mListenerList.iterator();
            while (i$.hasNext()) {
                ((ChannelListener) i$.next()).onChannelChanged(available);
            }
        }
    }

    public static ChannelApi getInstance() {
        if (mChannelApi == null) {
            mChannelApi = new ChannelApi();
        }
        return mChannelApi;
    }

    protected ChannelApi() {
    }
}
