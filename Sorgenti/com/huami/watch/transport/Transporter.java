package com.huami.watch.transport;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;

public abstract class Transporter {

    public interface ChannelListener {
        void onChannelChanged(boolean z);
    }

    public interface DataSendResultCallback {
        void onResultBack(DataTransportResult dataTransportResult);
    }

    public interface DataListener {
        void onDataReceived(TransportDataItem transportDataItem);
    }

    public interface ServiceConnectionListener {
    }

    public static class ConnectionResult implements Parcelable {
        public static final Creator<ConnectionResult> CREATOR = new C09961();
        private int mResultCode;

        static class C09961 implements Creator<ConnectionResult> {
            C09961() {
            }

            public ConnectionResult createFromParcel(Parcel in) {
                return new ConnectionResult(in);
            }

            public ConnectionResult[] newArray(int size) {
                return new ConnectionResult[size];
            }
        }

        public String toString() {
            switch (this.mResultCode) {
                case 0:
                    return "success";
                case 1:
                    return "service is unavailable";
                case 2:
                    return "service authentication failure";
                default:
                    return "Assert";
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mResultCode);
        }

        private ConnectionResult(Parcel in) {
            this.mResultCode = in.readInt();
        }
    }

    public abstract void addDataListener(DataListener dataListener);

    public abstract void removeDataListener(DataListener dataListener);

    public abstract void send(@NonNull String str, DataBundle dataBundle, DataSendResultCallback dataSendResultCallback);

    public static Transporter get(@NonNull Context context, @NonNull String moduleName) {
        return TransporterClassic.get(context, moduleName);
    }

    public void connectTransportService() {
    }

    public boolean isTransportServiceConnected() {
        return true;
    }

    public void send(@NonNull String action, DataBundle data) {
        send(action, data, null);
    }

    public void addChannelListener(ChannelListener listener) {
    }

    public void removeChannelListener(ChannelListener listener) {
    }

    public void addServiceConnectionListener(ServiceConnectionListener listener) {
    }

    public void removeServiceConnectionListener(ServiceConnectionListener listener) {
    }
}
