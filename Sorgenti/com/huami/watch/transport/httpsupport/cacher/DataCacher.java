package com.huami.watch.transport.httpsupport.cacher;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DataCacher extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static volatile DataCacher INSTANCE;

    public synchronized boolean delete(String identifier) {
        return getWritableDatabase().delete("dataitems", new StringBuilder().append("identifier= '").append(identifier).append("'").toString(), null) > 0;
    }

    public static synchronized DataCacher getInstance(Context context) {
        DataCacher dataCacher;
        synchronized (DataCacher.class) {
            if (INSTANCE == null) {
                synchronized (DataCacher.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new DataCacher(context);
                    }
                }
            }
            dataCacher = INSTANCE;
        }
        return dataCacher;
    }

    public synchronized boolean deleteByState(int state) {
        return getWritableDatabase().delete("dataitems", new StringBuilder().append("state= '").append(state).append("'").toString(), null) > 0;
    }

    public synchronized int deleteByOwnerWithState(String ower, int... states) {
        int delCount;
        if (states != null) {
            if (states.length != 0) {
                SQLiteDatabase db = getWritableDatabase();
                try {
                    db.beginTransaction();
                    StringBuilder stb = new StringBuilder();
                    stb.append("owner");
                    stb.append("= '");
                    stb.append(ower);
                    stb.append("'");
                    stb.append(" and ");
                    stb.append("state");
                    stb.append("=");
                    stb.append("(");
                    for (int i = 0; i < states.length; i++) {
                        stb.append(" '");
                        stb.append(states[i]);
                        stb.append("' ");
                        if (i != states.length - 1) {
                            stb.append(" or ");
                        }
                    }
                    stb.append(")");
                    Log.i("wifi_trans", stb.toString());
                    delCount = db.delete("dataitems", stb.toString(), null);
                    db.setTransactionSuccessful();
                    if (db != null) {
                        db.endTransaction();
                    }
                } catch (Throwable th) {
                    if (db != null) {
                        db.endTransaction();
                    }
                }
            }
        }
        delCount = 0;
        return delCount;
    }

    public synchronized int updateByOwnerToState(String owner, int foundState, int toState) {
        int countUpdate;
        ContentValues values = new ContentValues();
        values.put("state", Integer.valueOf(toState));
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            Log.i("wifi_trans", "update STATE for " + owner + " , to : " + toState);
            countUpdate = db.update("dataitems", values, "owner = ? and state = ?", new String[]{owner, String.valueOf(foundState)});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return countUpdate;
    }

    public synchronized int uploadToState(int foundState, int toState) {
        int countUpdate;
        ContentValues values = new ContentValues();
        values.put("state", Integer.valueOf(toState));
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            Log.i("wifi_trans", "uploadToState:update STATE for " + foundState + " , to : " + toState);
            countUpdate = db.update("dataitems", values, "state = ?", new String[]{String.valueOf(foundState)});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return countUpdate;
    }

    public synchronized boolean save(DataItem item) {
        boolean z = true;
        synchronized (this) {
            if (!(item.hasFlag(8192) || item.hasFlag(65536))) {
                z = save(Arrays.asList(new DataItem[]{item}));
            }
        }
        return z;
    }

    private synchronized boolean save(List<DataItem> items) {
        boolean saved;
        SQLiteDatabase db = getWritableDatabase();
        saved = false;
        db.beginTransaction();
        try {
            for (DataItem item : items) {
                if (!item.hasFlag(8192)) {
                    ContentValues values = new ContentValues();
                    values.put("identifier", item.getIdentifier());
                    values.put("action", item.getAction());
                    values.put("owner", item.getOwner());
                    values.put("url", item.getUrl());
                    values.put("method", item.getMethod());
                    values.put("flags", Integer.valueOf(item.getFlags()));
                    values.put("state", Integer.valueOf(item.getState()));
                    values.put("data", item.getData());
                    values.put("extra", item.getExtraData().toString());
                    values.put("code", Integer.valueOf(item.getCode()));
                    if (db.update("dataitems", values, "identifier=?", new String[]{String.valueOf(item.getIdentifier())}) > 0) {
                        saved = true;
                    } else {
                        saved = false;
                    }
                    if (!saved) {
                        if (db.insertOrThrow("dataitems", null, values) >= 0) {
                            saved = true;
                        } else {
                            saved = false;
                        }
                    }
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return saved;
    }

    private DataCacher(Context context) {
        super(context, "springchannel.db", null, DATABASE_VERSION);
        clearProcessingFlag();
    }

    private void clearProcessingFlag() {
        List<DataItem> item = queryAll();
        if (item != null && !item.isEmpty()) {
            for (DataItem i : item) {
                i.clearFlags(2048);
            }
            save((List) item);
        }
    }

    public synchronized DataItem query(String identifier) {
        return query(identifier, 1);
    }

    public synchronized DataItem query(String identifier, int mode) {
        DataItem dataItem;
        Exception ex;
        Cursor cursor = getReadableDatabase().rawQuery(String.format(Locale.US, "select * from %s where %s='%s'", new Object[]{"dataitems", "identifier", identifier}), null);
        dataItem = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    if (accept(mode, cursor.getInt(cursor.getColumnIndex("flags")), cursor.getString(cursor.getColumnIndex("identifier")))) {
                        DataItem item = new DataItem();
                        try {
                            item.setIdentifier(cursor.getString(cursor.getColumnIndex("identifier")));
                            item.setAction(cursor.getString(cursor.getColumnIndex("action")));
                            item.setOwner(cursor.getString(cursor.getColumnIndex("owner")));
                            item.setUrl(cursor.getString(cursor.getColumnIndex("url")));
                            item.setMethod(cursor.getString(cursor.getColumnIndex("method")));
                            item.setFlags(cursor.getInt(cursor.getColumnIndex("flags")));
                            item.setState(cursor.getInt(cursor.getColumnIndex("state")));
                            item.setData(cursor.getString(cursor.getColumnIndex("data")));
                            item.setExtraData(cursor.getString(cursor.getColumnIndex("extra")));
                            item.setCode(cursor.getInt(cursor.getColumnIndex("code")));
                            dataItem = item;
                        } catch (Exception e) {
                            ex = e;
                            dataItem = item;
                            try {
                                ex.printStackTrace();
                                dataItem = null;
                                if (cursor != null) {
                                    cursor.close();
                                }
                                return dataItem;
                            } catch (Throwable th) {
                                if (cursor != null) {
                                    cursor.close();
                                }
                            }
                        }
                    } else {
                        dataItem = null;
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                }
            } catch (Exception e2) {
                ex = e2;
                ex.printStackTrace();
                dataItem = null;
                if (cursor != null) {
                    cursor.close();
                }
                return dataItem;
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return dataItem;
    }

    public synchronized List<DataItem> queryAll() {
        return queryAll(1);
    }

    public synchronized List<DataItem> queryAll(int mode) {
        List<DataItem> list;
        Exception e;
        Cursor cursor = getReadableDatabase().rawQuery(String.format(Locale.US, "select * from %s", new Object[]{"dataitems"}), null);
        list = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    List<DataItem> list2 = list;
                    try {
                        if (accept(mode, cursor.getInt(cursor.getColumnIndex("flags")), cursor.getString(cursor.getColumnIndex("identifier")))) {
                            DataItem item = new DataItem();
                            item.setIdentifier(cursor.getString(cursor.getColumnIndex("identifier")));
                            item.setAction(cursor.getString(cursor.getColumnIndex("action")));
                            item.setOwner(cursor.getString(cursor.getColumnIndex("owner")));
                            item.setUrl(cursor.getString(cursor.getColumnIndex("url")));
                            item.setMethod(cursor.getString(cursor.getColumnIndex("method")));
                            item.setFlags(cursor.getInt(cursor.getColumnIndex("flags")));
                            item.setState(cursor.getInt(cursor.getColumnIndex("state")));
                            item.setData(cursor.getString(cursor.getColumnIndex("data")));
                            item.setExtraData(cursor.getString(cursor.getColumnIndex("extra")));
                            item.setCode(cursor.getInt(cursor.getColumnIndex("code")));
                            if (list2 == null) {
                                list = new ArrayList();
                            } else {
                                list = list2;
                            }
                            try {
                                list.add(item);
                            } catch (Exception e2) {
                                e = e2;
                                e.printStackTrace();
                                SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR 2！");
                                if (!cursor.moveToNext()) {
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return list;
                                }
                            }
                        }
                        list = list2;
                    } catch (Exception e3) {
                        e = e3;
                        list = list2;
                        e.printStackTrace();
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR 2！");
                        if (cursor.moveToNext()) {
                            if (cursor != null) {
                                cursor.close();
                            }
                            return list;
                        }
                    }
                    try {
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        list = null;
                        if (cursor != null) {
                            cursor.close();
                        }
                    } catch (Throwable th) {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } while (cursor.moveToNext());
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return list;
    }

    public synchronized List<DataItem> query(int state) {
        return query(state, 1);
    }

    public synchronized List<DataItem> query(int state, int mode) {
        List<DataItem> list;
        Exception e;
        Throwable th;
        Cursor cursor = getReadableDatabase().rawQuery(String.format(Locale.US, "select * from %s where %s=%d", new Object[]{"dataitems", "state", Integer.valueOf(state)}), null);
        list = null;
        DataItem item = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    DataItem item2 = item;
                    List<DataItem> list2 = list;
                    try {
                        int flag = cursor.getInt(cursor.getColumnIndex("flags"));
                        String id = cursor.getString(cursor.getColumnIndex("identifier"));
                        if (accept(mode, flag, id)) {
                            item = new DataItem();
                            try {
                                item.setIdentifier(id);
                                item.setAction(cursor.getString(cursor.getColumnIndex("action")));
                                item.setOwner(cursor.getString(cursor.getColumnIndex("owner")));
                                item.setUrl(cursor.getString(cursor.getColumnIndex("url")));
                                item.setMethod(cursor.getString(cursor.getColumnIndex("method")));
                                item.setFlags(flag);
                                item.setState(cursor.getInt(cursor.getColumnIndex("state")));
                                item.setData(cursor.getString(cursor.getColumnIndex("data")));
                                item.setExtraData(cursor.getString(cursor.getColumnIndex("extra")));
                                item.setCode(cursor.getInt(cursor.getColumnIndex("code")));
                                if (list2 == null) {
                                    list = new ArrayList();
                                } else {
                                    list = list2;
                                }
                                try {
                                    list.add(item);
                                } catch (Exception e2) {
                                    e = e2;
                                    try {
                                        e.printStackTrace();
                                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR！");
                                        if (!cursor.moveToNext()) {
                                            if (cursor != null) {
                                                cursor.close();
                                            }
                                            return list;
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                        list = null;
                                        if (cursor != null) {
                                            cursor.close();
                                        }
                                    } catch (Throwable th2) {
                                        th = th2;
                                        if (cursor != null) {
                                            cursor.close();
                                        }
                                        throw th;
                                    }
                                }
                            } catch (Exception e3) {
                                e = e3;
                                list = list2;
                                e.printStackTrace();
                                SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR！");
                                if (cursor.moveToNext()) {
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return list;
                                }
                            }
                        }
                        item = item2;
                        list = list2;
                    } catch (Exception e4) {
                        e = e4;
                        item = item2;
                        list = list2;
                        e.printStackTrace();
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR！");
                        if (cursor.moveToNext()) {
                            if (cursor != null) {
                                cursor.close();
                            }
                            return list;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        item = item2;
                    }
                } while (cursor.moveToNext());
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return list;
    }

    private boolean accept(int mode, int flag, String id) {
        if (mode == 2) {
            if ((flag & 16384) != 0) {
                return true;
            }
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "Ignore concurrent item ==>" + id);
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "Ignore concurrent item ==>" + id);
            return false;
        } else if (mode != 1 || (flag & 16384) == 0) {
            return true;
        } else {
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "Ignore serial item ==>" + id);
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "Ignore serial item ==>" + id);
            return false;
        }
    }

    public synchronized List<DataItem> queryAllHolyBaby(String parent) {
        List<DataItem> list;
        Exception e;
        Cursor cursor = getReadableDatabase().rawQuery(String.format(Locale.US, "select * from %s where %s='%s'", new Object[]{"dataitems", "owner", "_holy_board"}), null);
        list = new ArrayList();
        DataItem item = null;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                DataItem item2 = item;
                try {
                    String id = cursor.getString(cursor.getColumnIndex("identifier"));
                    item = new DataItem();
                    try {
                        item.setExtraData(cursor.getString(cursor.getColumnIndex("extra")));
                        if (TextUtils.equals(item.getExtraValByKey("target"), parent)) {
                            item.setIdentifier(id);
                            item.setOwner(cursor.getString(cursor.getColumnIndex("owner")));
                            item.setData(cursor.getString(cursor.getColumnIndex("data")));
                            list.add(item);
                        } else {
                            Log.i("WH-SERIAL_MODE", "忽略 : " + item.getExtraValByKey("target") + " FOR: " + parent);
                        }
                    } catch (Exception e2) {
                        e = e2;
                        try {
                            e.printStackTrace();
                            SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR！");
                            if (!cursor.moveToNext()) {
                                if (cursor != null) {
                                    cursor.close();
                                }
                                return list;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            list = null;
                            if (cursor != null) {
                                cursor.close();
                            }
                        } catch (Throwable th) {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    item = item2;
                    e.printStackTrace();
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "Illegal state CURSOR！");
                    if (cursor.moveToNext()) {
                        if (cursor != null) {
                            cursor.close();
                        }
                        return list;
                    }
                }
            } while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
        return list;
    }

    public void onCreate(SQLiteDatabase db) {
        if (!isTableExists(db, "dataitems")) {
            db.execSQL("CREATE TABLE dataitems(targetWho INTEGER PRIMARY KEY,identifier text,action text,owner text,url text,method text,flags integer,code integer,state integer,data text,extra text)");
            db.setVersion(3);
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        upgradeDB(db, oldVersion, newVersion);
    }

    void upgradeDB(SQLiteDatabase db, int fromVer, int toVer) {
        switch (fromVer) {
            case 1:
                try {
                    db.execSQL("ALTER TABLE dataitems ADD COLUMN extra TEXT;");
                    break;
                } catch (SQLException e) {
                    e.printStackTrace();
                    break;
                }
            case 2:
                try {
                    db.execSQL("ALTER TABLE dataitems ADD COLUMN code INTEGER DEFAULT 0;");
                    break;
                } catch (SQLException e2) {
                    e2.printStackTrace();
                    break;
                }
        }
        fromVer++;
        db.setVersion(fromVer);
        if (fromVer < toVer) {
            upgradeDB(db, fromVer, toVer);
        }
    }

    private synchronized boolean isTableExists(SQLiteDatabase db, String tableName) {
        boolean z;
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                z = true;
            } else {
                cursor.close();
            }
        }
        z = false;
        return z;
    }

    public static void cleanUp() {
        INSTANCE = null;
    }
}
