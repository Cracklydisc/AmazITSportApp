package com.huami.watch.transport.httpsupport.transporter.server;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.AlarmDriver;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.autotrigger.AutoTransTriggerTransporter;
import com.huami.watch.transport.httpsupport.autotrigger.WifiTransAutomaticAlarm;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmManager;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.control.service.HttpTransportManager;
import com.huami.watch.transport.httpsupport.global.SyncNodeSwitcher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import com.huami.watch.transport.httpsupport.translogutils.DataParser;
import com.huami.watch.transport.httpsupport.translogutils.TransLogs;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class DuplexDataExchangeService extends Service {
    private static Exchangee sExchangee;
    private static ConcurrentHashMap<String, Messenger> sPkg2ClientMap = null;
    private int cacheClient2Me = 0;
    private int cacheMe2Assist = 0;
    private int dataSuccMe2Assist = 0;
    private int dataSuccMe2Client = 0;
    private Context mContext = null;
    private Messenger mMessenger = null;
    private PendingIntent mPiCollectDataIntent;
    private Handler mWorkHandler = null;
    private HandlerThread mWorkerThread;
    private int reqAssist2MeCount = 0;
    private int reqClient2MeCount = 0;
    private long timeStartRunning = 0;

    public static abstract class Exchangee {
        private Context localContext;

        public abstract void onDataFromClient(String str);

        public abstract void query(String str);

        public Exchangee(Context context) {
            this.localContext = context;
        }

        public boolean sendToClient(DataItem item) {
            if (item == null) {
                return true;
            }
            Messenger client = (Messenger) DuplexDataExchangeService.sPkg2ClientMap.get(item.getOwner());
            TransLogs.m11i(new DataParser(item, 2).getImportanceInfo());
            if (client == null) {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "sendToClient ---> " + item.getOwner() + " , " + item.getAction() + " , 发现: 客户端不存在...通知并缓存到下次处理！");
                }
                SolidLogger.getInstance().with("WH-BLT", "sendToClient ---> " + item.getOwner() + " , " + item.getAction() + " , 发现客户端不存在...通知并####缓存####到下次处理！");
                return false;
            }
            Message message = Message.obtain();
            Bundle b = new Bundle();
            b.putString("@@@inner-key[![[--][data]", item.toString());
            b.putString("@@@inner-key[![[--][uuid]", item.getIdentifier());
            message.setData(b);
            message.replyTo = client;
            try {
                client.send(message);
                return true;
            } catch (Exception e) {
                if (e instanceof TransactionTooLargeException) {
                    File f = new File("/sdcard/.http-trans" + File.separator + item.getIdentifier());
                    if (f.exists()) {
                        f.delete();
                    }
                    String path = DataUtils.saveToFileAndManage(item.getIdentifier(), item.getData());
                    item.setData(null);
                    item.addFlags(32768);
                    item.addExtraPair("d-path", path);
                    b.putString("@@@inner-key[![[--][data]", item.toString());
                    try {
                        client.send(message);
                        return true;
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                        return true;
                    }
                }
                e.printStackTrace();
                return false;
            }
        }

        public boolean dispatchToAllClient(DataItem item) {
            if (!Command.isInternal(item.getAction())) {
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "NOT internal action for : " + item);
                }
                SolidLogger.getInstance().with("WH-SRV", "NOT internal action for : " + item);
                return false;
            } else if (DuplexDataExchangeService.sPkg2ClientMap == null) {
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "Service 内部map是空的。。。。！");
                }
                SolidLogger.getInstance().with("WH-SRV", "Service 内部map是空的。。。。！");
                return false;
            } else {
                String targetPackage = item.getOwner();
                if (TextUtils.isEmpty(item.getExtraValByKey("target"))) {
                    targetPackage = "*";
                }
                if (TextUtils.isEmpty(targetPackage)) {
                    targetPackage = "*";
                }
                if (TextUtils.equals("*", targetPackage)) {
                    try {
                        for (String pkg : DuplexDataExchangeService.sPkg2ClientMap.keySet()) {
                            sendSpecCmdToPkg(item, pkg);
                        }
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "Failed to dispatch for : " + item, e);
                        }
                        SolidLogger.getInstance().with("WH-SRV", "Failed to dispatch for : " + item, e);
                        return false;
                    }
                }
                boolean hasPkg;
                Set<String> pkgs = DuplexDataExchangeService.sPkg2ClientMap.keySet();
                if (pkgs == null || !pkgs.contains(targetPackage)) {
                    hasPkg = false;
                } else {
                    hasPkg = true;
                }
                if (hasPkg) {
                    try {
                        sendSpecCmdToPkg(item, targetPackage);
                        return true;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return false;
                    }
                }
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "Failed to dispatch for : " + targetPackage + " , NOT FOUND PKG. ACTION IS: " + item.getExtraValByKey("who"));
                }
                SolidLogger.getInstance().with("WH-SRV", "Failed to dispatch for : " + targetPackage + " , NOT FOUND PKG. ACTION IS: " + item.getExtraValByKey("who"));
                return false;
            }
        }

        private void sendSpecCmdToPkg(DataItem item, String pkg) {
            if (TextUtils.isEmpty(pkg)) {
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "扔掉了 ！Drop for : " + pkg);
                }
                SolidLogger.getInstance().with("WH-SRV", "扔掉了 ！Drop for : " + pkg);
                return;
            }
            item.setOwner(pkg);
            sendToClient(item);
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "Duplex exchange 继续下报 ---> " + item.getOwner() + " , " + item.getAction());
            }
            SolidLogger.getInstance().with("WH-SRV", "Duplex exchange 继续下报 ---> " + item.getOwner() + " , " + item.getAction());
        }

        public synchronized boolean hasClient() {
            boolean z;
            z = (DuplexDataExchangeService.sPkg2ClientMap == null || DuplexDataExchangeService.sPkg2ClientMap.isEmpty()) ? false : true;
            return z;
        }
    }

    class C10661 implements Runnable {
        C10661() {
        }

        public void run() {
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "<><><><><>发出 Service Ready..<><><><><>");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "<><><><><>Host Ready notify Service Ready..<><><><><>");
            DuplexDataExchangeService.this.mContext.sendBroadcast(new Intent("com.huami.watch.httpsupport.EXCHANGEE_READY_TO ACCEPT"));
        }
    }

    public interface OnTransStateChangeListener {
    }

    public void onCreate() {
        super.onCreate();
        reset();
        this.mContext = getApplicationContext();
        SolidLogger.withContext(this.mContext);
        HttpTransportManager.doInit(this.mContext);
        this.mPiCollectDataIntent = AlarmDriver.startAlarmDriver(this, 7200000);
        Log.i("WH-SRV", "-------------- Init Http-Support API -------ver 1.09.4 ---------");
        SolidLogger.getInstance().with("WH-SRV", "-------------- Init Http-Support API -------ver 1.10 ---------");
        WifiTransAutomaticAlarm wifiTransAutomaticAlarm = new WifiTransAutomaticAlarm(this);
        AutoAlarmManager.getInstance(this).addAutoAlarms(wifiTransAutomaticAlarm.getName(), wifiTransAutomaticAlarm.getAutoAlarmList());
    }

    private void reset() {
        this.timeStartRunning = System.currentTimeMillis();
        onDestroy();
        SyncNodeSwitcher.self().changeTo(0, true);
    }

    public void onStart(Intent intent, int startId) {
        if (this.mWorkHandler == null) {
            initExchangeHandler();
        }
        this.mWorkHandler.postDelayed(new C10661(), 388);
    }

    private synchronized void initExchangeHandler() {
        if (sPkg2ClientMap == null) {
            sPkg2ClientMap = new ConcurrentHashMap();
        } else {
            sPkg2ClientMap.clear();
        }
        if (this.mWorkerThread == null) {
            this.mWorkerThread = new HandlerThread("wear-http-host-service");
            this.mWorkerThread.start();
            this.mWorkerThread.setPriority(10);
        }
        this.mWorkHandler = new Handler(this.mWorkerThread.getLooper()) {
            public void handleMessage(Message msg) {
                if (msg.what == 2147483628) {
                    String pkg = DuplexDataExchangeService.this.mapPkg2Client(msg);
                    DuplexDataExchangeService.this.linkToDeath(pkg);
                    DuplexDataExchangeService.this.tryToSendBackToClient(pkg, 2000);
                    return;
                }
                Bundle data = msg.getData();
                pkg = data.getString("@@@inner-key[![[--][pkg]");
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "===WearService 收到一个从: '" + pkg + "' 发来的请求<<<<<<<<");
                }
                SolidLogger.getInstance().with("WH-ASSIST", "===WearService 收到一个从: '" + pkg + "' 发来的请求<<<<<<<<");
                if (TextUtils.isEmpty(pkg)) {
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", "[DataService] ==> NO PKG !! IGNORE. " + msg.getData());
                    }
                    SolidLogger.getInstance().with("WH-SRV", "[DataService] ==> NO PKG !! IGNORE. " + msg.getData());
                    return;
                }
                if (((Messenger) DuplexDataExchangeService.sPkg2ClientMap.get(pkg)) == null) {
                    Messenger replyTo = msg.replyTo;
                    DuplexDataExchangeService.sPkg2ClientMap.put(pkg, msg.replyTo);
                }
                DuplexDataExchangeService.this.dataFromClient(pkg, data);
            }
        };
    }

    private void linkToDeath(final String pkg) {
        if (!TextUtils.isEmpty(pkg)) {
            Messenger m = (Messenger) sPkg2ClientMap.get(pkg);
            if (m == null) {
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "===没有信使于link death with: '" + pkg + "' .");
                }
                SolidLogger.getInstance().with("WH-SRV", "===没有信使于link death with: '" + pkg + "' .");
                return;
            }
            try {
                m.getBinder().linkToDeath(new DeathRecipient() {
                    public void binderDied() {
                        if (GlobalDefine.DEBUG_SRV) {
                            Log.i("WH-SRV", "@@@@@@ 客户端  ' " + pkg + "' 挂掉了..Mapping...");
                        }
                        SolidLogger.getInstance().with("WH-SRV", "@@@@@@ 客户端  ' " + pkg + "' 挂掉了..整理Mapp...");
                        if (DuplexDataExchangeService.sPkg2ClientMap != null) {
                            DuplexDataExchangeService.sPkg2ClientMap.remove(pkg);
                        }
                    }
                }, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void tryToSendBackToClient(final String pkg, long delay) {
        if (!TextUtils.isEmpty(pkg)) {
            this.mWorkHandler.postDelayed(new Runnable() {
                public void run() {
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", "===握手完毕，尝试检查client: '" + pkg + "' 是否有暂存在service端的请求...");
                    }
                    SolidLogger.getInstance().with("WH-SRV", "====____===握手完毕，尝试检查client: '" + pkg + "' 是否有暂存在service端的请求...");
                    DataCacher cacher = DataCacher.getInstance(DuplexDataExchangeService.this.mContext);
                    if (cacher == null) {
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "Cacher 竟然是 NULL？？？");
                        }
                        SolidLogger.getInstance().with("WH-SRV", "Cacher 竟然是 NULL？？？");
                        return;
                    }
                    List<DataItem> itemList = cacher.query(2, 0);
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", "该client: '" + pkg + "' 有 [ " + (itemList == null ? 0 : itemList.size()) + " ] 个请求需要回传....");
                    }
                    SolidLogger.getInstance().with("WH-SRV", "该client: '" + pkg + "' 有 [ " + (itemList == null ? 0 : itemList.size()) + " ] 个请求需要回传....");
                    if (itemList != null) {
                        for (DataItem item : itemList) {
                            if (item != null) {
                                if (GlobalDefine.DEBUG_SERVER) {
                                    Log.i("WH-SRV", "尝试回传曾经缓存 的client: '" + pkg + "' 的请求返回 ： " + item.getIdentifier() + " , " + item.getAction());
                                }
                                SolidLogger.getInstance().with("WH-SRV", "尝试回传曾经缓存 的client: '" + pkg + "' 的请求返回 ： " + item.getIdentifier() + " , " + item.getAction());
                                if (DuplexDataExchangeService.sExchangee != null) {
                                    item.setState(0);
                                    if (DuplexDataExchangeService.sExchangee.sendToClient(item)) {
                                        if (GlobalDefine.DEBUG_SERVER) {
                                            Log.i("WH-SRV", "client: '" + pkg + "' 的请求  " + item.getIdentifier() + " , " + item.getAction() + " , 尝试从数据库移除...");
                                        }
                                        SolidLogger.getInstance().with("WH-SRV", "client: '" + pkg + "' 的请求  " + item.getIdentifier() + " , " + item.getAction() + " , 尝试从数据库移除...");
                                        boolean res = cacher.delete(item.getIdentifier());
                                        if (GlobalDefine.DEBUG_SERVER) {
                                            Log.i("WH-SRV", "client: '" + pkg + "' 的请求  " + item.getIdentifier() + " , " + item.getAction() + " , 移除完毕 , 结果" + res);
                                        }
                                        SolidLogger.getInstance().with("WH-SRV", "client: '" + pkg + "' 的请求  " + item.getIdentifier() + " , " + item.getAction() + " , 移除完毕 , 结果" + res);
                                    }
                                }
                            }
                        }
                    }
                }
            }, delay);
        }
    }

    private String mapPkg2Client(Message msg) {
        String pkg = msg.getData().getString("@@@inner-key[![[--][pkg]");
        if (TextUtils.isEmpty(pkg)) {
            return null;
        }
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "client: '" + pkg + "' 请求握手...");
        }
        SolidLogger.getInstance().with("WH-SRV", "client: '" + pkg + "' 请求握手...");
        Messenger client = msg.replyTo;
        if (client == null || sPkg2ClientMap.contains(client)) {
            return null;
        }
        sPkg2ClientMap.put(pkg, client);
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "client: '" + pkg + "' >>>>> 握手成功.");
        }
        SolidLogger.getInstance().with("WH-SRV", "client: '" + pkg + "' >>>>> 握手成功.");
        return pkg;
    }

    private void dataFromClient(String pkg, Bundle data) {
        int key = data.getInt("@@@inner-key[![[--][schedule]", -1);
        if (key != -1) {
            switch (key) {
                case -101:
                    if (sExchangee != null) {
                        sExchangee.query(data.getString("@@@inner-key[![[--][uuid]"));
                        return;
                    }
                    return;
            }
        }
        if (sExchangee != null) {
            sExchangee.onDataFromClient(data.getString("@@@inner-key[![[--][data]"));
        }
    }

    public IBinder onBind(Intent intent) {
        if (this.mWorkHandler == null) {
            initExchangeHandler();
        }
        if (this.mMessenger == null) {
            this.mMessenger = new Messenger(this.mWorkHandler);
        }
        return this.mMessenger.getBinder();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 1;
    }

    public static void setExchangee(Exchangee e) {
        if (e != null) {
            sExchangee = e;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        AlarmDriver.cancelAlarmDriver(this, this.mPiCollectDataIntent);
        if (this.mWorkHandler != null) {
            this.mWorkHandler.removeCallbacksAndMessages(Integer.valueOf(0));
            this.mWorkHandler = null;
        }
        if (this.mWorkerThread != null) {
            this.mWorkerThread.quitSafely();
            this.mWorkerThread = null;
        }
        if (sPkg2ClientMap != null) {
            sPkg2ClientMap.clear();
            sPkg2ClientMap = null;
        }
        DataCacher.cleanUp();
        SyncNodeSwitcher.self().destory();
        AutoTransTriggerTransporter.getInstance(this).clear();
        AutoAlarmManager.getInstance(this).clearAll();
    }
}
