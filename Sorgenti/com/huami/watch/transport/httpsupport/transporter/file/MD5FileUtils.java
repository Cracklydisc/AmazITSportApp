package com.huami.watch.transport.httpsupport.transporter.file;

import com.huami.watch.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5FileUtils {
    protected static char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    protected static MessageDigest messagedigest;

    static {
        messagedigest = null;
        try {
            messagedigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.m29i("FwUpgrade-Md5FileUtils", "MD5FileUtil messagedigest初始化失败", new Object[0]);
        }
    }
}
