package com.huami.watch.transport.httpsupport.transporter.file;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class LDInfo implements Parcelable {
    public static final Creator<LDInfo> CREATOR = new C10591();
    public String CMD;
    public String fileName;
    public int index;
    public String md5;

    static class C10591 implements Creator<LDInfo> {
        C10591() {
        }

        public LDInfo createFromParcel(Parcel in) {
            return new LDInfo(in);
        }

        public LDInfo[] newArray(int size) {
            return new LDInfo[size];
        }
    }

    protected LDInfo(Parcel in) {
        this.fileName = in.readString();
        this.index = in.readInt();
        this.CMD = in.readString();
        this.md5 = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fileName);
        dest.writeInt(this.index);
        dest.writeString(this.CMD);
        dest.writeString(this.md5);
    }
}
