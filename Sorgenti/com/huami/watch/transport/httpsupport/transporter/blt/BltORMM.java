package com.huami.watch.transport.httpsupport.transporter.blt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.ArrayList;
import java.util.List;

public class BltORMM extends SQLiteOpenHelper {
    private static volatile BltORMM INSTANCE;

    public static class ORM {
        public static int STATE_OUT = 0;
        public String identity;
        public Long stamp;
        public int state;
    }

    public static synchronized BltORMM getInstance(Context context) {
        BltORMM bltORMM;
        synchronized (BltORMM.class) {
            if (INSTANCE == null) {
                synchronized (DataCacher.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new BltORMM(context);
                    }
                }
            }
            bltORMM = INSTANCE;
        }
        return bltORMM;
    }

    private BltORMM(Context context) {
        super(context, "orm.db", null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        if (!isTableExists(db, "orm")) {
            db.execSQL("CREATE TABLE orm(_id LONG PRIMARY KEY,identifier text,state INTEGER, stamp LONG)");
            db.setVersion(1);
        }
    }

    private synchronized boolean isTableExists(SQLiteDatabase db, String tableName) {
        boolean z;
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                z = true;
            } else {
                cursor.close();
            }
        }
        z = false;
        return z;
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public boolean mark(DataItem item) {
        if (item.hasFlag(8192)) {
            return false;
        }
        ORM o = new ORM();
        o.identity = item.getIdentifier();
        o.stamp = Long.valueOf(SystemClock.elapsedRealtime());
        o.state = ORM.STATE_OUT;
        return saveSingle(o);
    }

    public boolean markDown(DataItem item) {
        if (item.hasFlag(8192)) {
            return false;
        }
        ORM o = new ORM();
        o.identity = item.getIdentifier();
        return remove(o);
    }

    public boolean markLook(DataItem item) {
        if (item.hasFlag(8192)) {
            return true;
        }
        ORM o = new ORM();
        o.identity = item.getIdentifier();
        return has(o);
    }

    public boolean saveSingle(ORM o) {
        List<ORM> l = new ArrayList();
        l.add(o);
        return save(l);
    }

    public boolean save(List<ORM> os) {
        SQLiteDatabase db = getWritableDatabase();
        boolean saved = false;
        db.beginTransaction();
        try {
            for (ORM o : os) {
                ContentValues values = new ContentValues();
                values.put("identifier", o.identity);
                values.put("state", Integer.valueOf(o.state));
                values.put("stamp", o.stamp);
                if (db.update("orm", values, "identifier=?", new String[]{String.valueOf(o.identity)}) > 0) {
                    saved = true;
                } else {
                    saved = false;
                }
                if (!saved) {
                    if (db.insertOrThrow("orm", null, values) >= 0) {
                        saved = true;
                    } else {
                        saved = false;
                    }
                }
            }
            db.setTransactionSuccessful();
            return saved;
        } finally {
            db.endTransaction();
        }
    }

    private boolean has(ORM o) {
        boolean res = true;
        Cursor c = getWritableDatabase().query("orm", null, "identifier = ?", new String[]{o.identity}, null, null, null);
        if (c == null) {
            return false;
        }
        if (c.getCount() <= 0) {
            res = false;
        }
        c.close();
        return res;
    }

    private boolean remove(ORM o) {
        if (getWritableDatabase().delete("orm", "identifier = ?", new String[]{o.identity}) > 0) {
            return true;
        }
        return false;
    }
}
