package com.huami.watch.transport.httpsupport.transporter.blt;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.provider.Settings.System;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.DataBundle;
import com.huami.watch.transport.DataTransportResult;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.ChannelListener;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.control.assist.WearHttpCompanion;
import com.huami.watch.transport.httpsupport.control.service.HttpTransportManager;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import com.huami.watch.transport.httpsupport.translogutils.DataParser;
import com.huami.watch.transport.httpsupport.translogutils.TransLogs;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class BltTransporter implements DataListener {
    public static boolean IS_BLE = false;
    private static long TIME_OUT_FOR_CALLBACK_FAILED_TRIGGER = 15000;
    private static Handler sBLTTimeOutHandler = new Handler(sTimeOutMonitor.getLooper());
    private static HandlerThread sTimeOutMonitor = new HandlerThread("http-support-timeout-monitor");
    private static ConcurrentHashMap<String, TimeOutCallbackTrigger> sTimeOutTriggerMap = new ConcurrentHashMap();
    private Runnable looksideAssist2Watch = new C10584();
    private Runnable looksideWatch2Assist = new C10573();
    private Context mContext;
    private Transporter mTransporter = null;

    public interface IResultReceiver {
        void onResult(int i);
    }

    class C10551 implements ChannelListener {
        C10551() {
        }

        public void onChannelChanged(boolean available) {
            if (available && BltTransporter.this.mContext != null) {
                Intent i = new Intent();
                i.setAction("com.huami.watch.WATCH_CONNED_4_COMPANION");
                BltTransporter.this.mContext.sendBroadcast(i);
            }
        }
    }

    class C10573 implements Runnable {
        C10573() {
        }

        public void run() {
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "<WATCH>  LOOKSIDE LAUNCH#$%^&*$%^&++++++++++++++++++!");
            }
            SolidLogger.getInstance().with("WH-BLT", "<WATCH>  LOOKSIDE LAUNCH#$%^&*$%^&++++++++++++++++++!");
            List<DataItem> itemList = DataCacher.getInstance(BltTransporter.this.mContext).query(1, 0);
            if (itemList != null && itemList.size() > 0) {
                HttpTransportManager.getInstance(BltTransporter.this.mContext).tryProcessCachedRequestWhilePhoneAvalible(BltTransporter.this.mContext);
            }
        }
    }

    class C10584 implements Runnable {
        C10584() {
        }

        public void run() {
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "<PHONE> --> LOOKSIDE LAUNCH#$%^&*$%^&++++++++++++++++++!");
            }
            SolidLogger.getInstance().with("WH-BLT", "<PHONE> --> LOOKSIDE LAUNCH#$%^&*$%^&++++++++++++++++++!");
            List<DataItem> itemList = DataCacher.getInstance(BltTransporter.this.mContext).query(3, 0);
            if (itemList != null && itemList.size() > 0) {
                WearHttpCompanion.getInstance().tryProcessCachedResponseWhileChannelAvalible();
            }
        }
    }

    private class TimeOutCallbackTrigger implements Runnable {
        DataItem item;
        boolean onTheWay = false;
        IResultReceiver responseListener;

        TimeOutCallbackTrigger(DataItem item, IResultReceiver responseListener) {
            this.item = item;
            this.responseListener = responseListener;
        }

        public void run() {
            if (this.item.getIdentifier() != null) {
                if (BltTransporter.sTimeOutTriggerMap == null || BltTransporter.sTimeOutTriggerMap.get(this.item.getIdentifier()) != null) {
                    if (GlobalDefine.DEBUG_BLT) {
                        Log.i("WH-BLT", "callbackTrigger deployed:" + this.item.getIdentifier());
                    }
                    SolidLogger.getInstance().with("WH-BLT", "callbackTrigger deployed:" + this.item.getIdentifier());
                    this.onTheWay = true;
                    BltTransporter.this.dataOutResponseTo(this.item, -1, this.responseListener);
                    return;
                }
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "Not Found Time Out Trigger For:" + this.item.getIdentifier() + " . IGNORED.");
                }
                SolidLogger.getInstance().with("WH-BLT", "Not Found Time Out Trigger For:" + this.item.getIdentifier() + " . IGNORED.");
            }
        }
    }

    static {
        sTimeOutMonitor.start();
    }

    public final void start(Context context) {
        IS_BLE = System.getInt(context.getContentResolver(), "com.huami.watch.key.DEVICE_BOND_TYPE", 0) == 2;
        this.mContext = context;
        this.mTransporter = Transporter.get(context, "com.huami.watch.http-support.manager");
        this.mTransporter.addDataListener(this);
        this.mTransporter.connectTransportService();
        this.mTransporter.addChannelListener(new C10551());
        TIME_OUT_FOR_CALLBACK_FAILED_TRIGGER = System.getInt(context.getContentResolver(), "com.huami.watch.key.DEVICE_BOND_TYPE", 0) == 2 ? 300000 : 15000;
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "\n\t\t\t*当前callback 保证的time-out时间: " + (TIME_OUT_FOR_CALLBACK_FAILED_TRIGGER / 1000) + " 秒\n");
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "\n\t\t\t*当前callback 保证的time-out时间: " + (TIME_OUT_FOR_CALLBACK_FAILED_TRIGGER / 1000) + " 秒\n");
        }
    }

    public boolean check() {
        if (this.mTransporter == null) {
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "Transporter is NULL!");
            }
            SolidLogger.getInstance().with("WH-SRV", "Transporter is NULL!");
            return false;
        }
        if (this.mContext != null && GlobalDefine.IS_WATCH) {
            boolean conn;
            if (System.getInt(this.mContext.getContentResolver(), "com.huami.watch.extra.DEVICE_CONNECTION_STATUS", 0) > 0 || System.getInt(this.mContext.getContentResolver(), "com.huami.watch.extra.conn.ctrl", 0) > 0) {
                conn = true;
            } else {
                conn = false;
            }
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "\n\t\t\tSettings.System.getInt(mContext.getContentResolver(), CONN_BLE_CTRL, 0) = " + System.getInt(this.mContext.getContentResolver(), "com.huami.watch.extra.conn.ctrl", 0) + "\n");
            }
            if (!conn) {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "\n\t\t\t******  手表跟手机压根就没连接! ******\n");
                }
                SolidLogger.getInstance().with("WH-BLT", "\n\t\t\t******  手表跟手机压根就没连接! ******\n");
                return false;
            }
        }
        if (this.mTransporter.isTransportServiceConnected()) {
            return true;
        }
        if (GlobalDefine.DEBUG_BLT) {
            Log.i("WH-BLT", "Transporter service NOT connected!");
        }
        SolidLogger.getInstance().with("WH-BLT", "Transporter service NOT connected!");
        return false;
    }

    public final boolean dataOut(final DataItem dataItem, final IResultReceiver response) {
        if (check()) {
            DataBundle dataBundle = DataUtils.createDataItemToDataBundle(dataItem);
            if (Command.isInternal(dataItem.getAction())) {
                dataBundle.putInt("priority", 3);
            } else {
                int pri = 0;
                try {
                    pri = Integer.valueOf(dataItem.getExtraValByKey("p")).intValue();
                } catch (Exception e) {
                }
                dataBundle.putInt("priority", pri);
            }
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "Sending ---> " + dataItem.getOwner() + " , " + dataItem.getAction());
            }
            SolidLogger.getInstance().with("WH-BLT", "Sending ---> " + dataItem.getOwner() + " , " + dataItem.getAction());
            final TimeOutCallbackTrigger callbackTrigger = new TimeOutCallbackTrigger(dataItem, response);
            this.mTransporter.send(dataItem.getAction(), dataBundle, new DataSendResultCallback() {
                public void onResultBack(DataTransportResult dataTransportResult) {
                    int code = dataTransportResult.getResultCode();
                    TransLogs.m11i(new DataParser(dataItem, 1).bleSendResText(dataItem, code));
                    if (callbackTrigger.onTheWay) {
                        BltTransporter.sBLTTimeOutHandler.removeCallbacks(callbackTrigger);
                        if (GlobalDefine.DEBUG_BLT) {
                            Log.i("WH-BLT", "callbackTrigger.onTheWay ON the way.....SHIT.....:" + dataItem.getIdentifier());
                        }
                        SolidLogger.getInstance().with("WH-BLT", "callbackTrigger.onTheWay ON the way.....SHIT.....:" + dataItem.getIdentifier());
                        BltTransporter.this.dataOutResponseTo(dataItem, code, response);
                    } else {
                        if (GlobalDefine.DEBUG_BLT) {
                            Log.i("WH-BLT", "callbackTrigger.onTheWay NOT on the way:" + dataItem.getIdentifier());
                        }
                        SolidLogger.getInstance().with("WH-BLT", "callbackTrigger.onTheWay NOT on the way:" + dataItem.getIdentifier());
                        BltTransporter.sBLTTimeOutHandler.removeCallbacks(callbackTrigger);
                        BltTransporter.this.dataOutResponseTo(dataItem, code, response);
                    }
                    if (code == 0) {
                        BltTransporter.this.lookSideBlt();
                    }
                }
            });
            if (!IS_BLE) {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "WITH --- CALL BACK --- DELAY !FOR ANDROID.");
                }
                sBLTTimeOutHandler.postDelayed(callbackTrigger, TIME_OUT_FOR_CALLBACK_FAILED_TRIGGER);
                sTimeOutTriggerMap.put(dataItem.getIdentifier(), callbackTrigger);
            }
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "After send ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , targetWho " + dataItem.getIdentifier());
            }
            SolidLogger.getInstance().with("WH-BLT", "After send ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , targetWho " + dataItem.getIdentifier());
            return true;
        }
        if (GlobalDefine.IS_WATCH) {
            dataItem.setState(1);
        } else {
            dataItem.setState(3);
        }
        DataCacher.getInstance(this.mContext).save(dataItem);
        return false;
    }

    public synchronized boolean removeMark(DataItem dataItem) {
        boolean res;
        res = BltORMM.getInstance(this.mContext).markDown(dataItem);
        if (GlobalDefine.DEBUG_BLT) {
            Log.i("WH-BLT", "Remove mark ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier() + " res : " + res);
        }
        SolidLogger.getInstance().with("WH-BLT", "Remove mark ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier() + " res : " + res);
        return res;
    }

    public synchronized boolean hasMark(DataItem dataItem) {
        boolean z = true;
        synchronized (this) {
            if (dataItem.hasFlag(8192)) {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "FLAG_DATA_SYNC_FLOW 放行 ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-BLT", "FLAG_DATA_SYNC_FLOW 放行 ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
            } else if (BltORMM.getInstance(this.mContext).markLook(dataItem)) {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "Has mark ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-BLT", "Has mark ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
            } else {
                if (GlobalDefine.DEBUG_BLT) {
                    Log.i("WH-BLT", "Ignore from mark down ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-BLT", "Ignore from mark down ---> " + dataItem.getOwner() + " , " + dataItem.getIdentifier());
                z = false;
            }
        }
        return z;
    }

    public final void onDataReceived(TransportDataItem transportDataItem) {
        ((PowerManager) this.mContext.getApplicationContext().getSystemService("power")).newWakeLock(1, "wake-for-http-sync").acquire(5000);
        DataItem dataItem = DataUtils.createDataBundleToDataItem(transportDataItem.getData());
        if (GlobalDefine.DEBUG_BLT) {
            Log.i("WH-BLT", "on data received from WearCompanion: " + dataItem.toShortString());
        }
        SolidLogger.getInstance().with("WH-BLT", "on data received from WearCompanion: " + dataItem.toShortString());
        if (filterInnerCommand(dataItem)) {
            handleInnerCommand(dataItem);
        } else {
            plainItemDataIn(dataItem);
        }
    }

    private boolean filterInnerCommand(DataItem item) {
        if (!Command.isInternal(item.getAction())) {
            return false;
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "==> Take as 内部命令: " + item.toShortString());
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "==> Take as 内部命令: " + item.toShortString());
        }
        return true;
    }

    protected void handleInnerCommand(DataItem item) {
    }

    protected void plainItemDataIn(DataItem dataItem) {
    }

    private synchronized void dataOutResponseTo(DataItem dataItem, int code, IResultReceiver response) {
        if (dataItem.getIdentifier() != null) {
            boolean has = (sTimeOutTriggerMap == null || sTimeOutTriggerMap.remove(dataItem.getIdentifier()) == null) ? false : true;
            if (GlobalDefine.DEBUG_BLT) {
                Log.i("WH-BLT", "Data send result code rev ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , CODE： " + code + " , targetWho : " + dataItem.getIdentifier() + " , remove trigger res: " + has);
            }
            SolidLogger.getInstance().with("WH-BLT", "Data send result code rev ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , CODE： " + code + " , targetWho : " + dataItem.getIdentifier() + " , remove trigger res: " + has);
            if (response != null) {
                response.onResult(code);
            }
        }
    }

    public void lookSideBlt() {
        Runnable target = GlobalDefine.IS_WATCH ? this.looksideWatch2Assist : this.looksideAssist2Watch;
        if (GlobalDefine.DEBUG_BLT) {
            Log.i("WH-BLT", (GlobalDefine.IS_WATCH ? "<WATCH> " : "<PHONE> ") + "==> LOOKSIDE TOUCH REMOVE & RELAUNCH! ");
        }
        SolidLogger.getInstance().with("WH-BLT", (GlobalDefine.IS_WATCH ? "<WATCH> " : "<PHONE> ") + "==> LOOKSIDE TOUCH REMOVE & RELAUNCH! ");
        sBLTTimeOutHandler.removeCallbacks(target);
        sBLTTimeOutHandler.postDelayed(target, 40000);
    }
}
