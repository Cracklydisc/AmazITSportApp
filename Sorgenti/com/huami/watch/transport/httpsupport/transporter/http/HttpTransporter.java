package com.huami.watch.transport.httpsupport.transporter.http;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.global.PublicParamsBuilder;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class HttpTransporter {
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient mHttpClient;
    private AtomicBoolean mInited = new AtomicBoolean(false);
    private PublicParamsBuilder mPublicParamsBuilder;

    class C10601 extends Builder {
        C10601() {
        }

        public Builder addHeader(String name, String value) {
            HttpTransporter.this.printHeaderInfo(name, value);
            return super.addHeader(name, value);
        }

        public Builder header(String name, String value) {
            HttpTransporter.this.printHeaderInfo(name, value);
            return super.header(name, value);
        }

        public Builder removeHeader(String name) {
            SolidLogger.getInstance().with("WH-ASSIST", "HEADER OUT ====  for : " + hashCode() + " with : [" + name + "]");
            if (GlobalDefine.DEBUG_HTTP) {
                Log.i("WH-HTTP", "HEADER OUE ====  for : " + hashCode() + " with : [" + name + "]");
            }
            return super.removeHeader(name);
        }
    }

    public DataItem request(Context context, DataItem dataItem, Map<?, ?> map) {
        if (!this.mInited.get()) {
            synchronized (HttpTransporter.class) {
                if (!this.mInited.get()) {
                    this.mHttpClient = new OkHttpClient();
                    this.mInited.set(true);
                }
            }
        }
        if ("get".equals(dataItem.getMethod())) {
            return get(context, dataItem);
        }
        if ("post".equals(dataItem.getMethod())) {
            return post(context, dataItem);
        }
        return null;
    }

    public DataItem get(Context context, DataItem dataItem) {
        String url = dataItem.getUrl();
        Builder builder = new Builder();
        if (this.mPublicParamsBuilder != null) {
            String urlFilled = this.mPublicParamsBuilder.completeURL(context, url);
            if (!TextUtils.isEmpty(urlFilled)) {
                url = urlFilled;
            }
            this.mPublicParamsBuilder.fillPublicHeader(context, dataItem, builder);
        }
        builder.url(url);
        if (dataItem.hasHeader() && (dataItem.getFlags() & 16) != 0) {
            autoFillHeader(context, dataItem, builder);
        }
        Request request = builder.build();
        SolidLogger.getInstance().with("WH-ASSIST", "Request Info : " + request.toString() + " , " + request.hashCode());
        Map<String, List<String>> map = request.headers().toMultimap();
        if (GlobalDefine.DEBUG_HTTP) {
            Log.i("WH-HTTP", "HEADER INFO ====  for " + request.hashCode() + " BEGIN>>");
        }
        SolidLogger.getInstance().with("WH-ASSIST", "HEADER INFO ====  for " + request.hashCode() + " BEGIN>>");
        for (String next : map.keySet()) {
            for (String str : (List) map.get(next)) {
                if (GlobalDefine.DEBUG_HTTP) {
                    Log.i("WH-HTTP", "[" + next + ":" + str + "]");
                }
                SolidLogger.getInstance().with("WH-ASSIST", "[" + next + ":" + str + "]");
            }
        }
        if (GlobalDefine.DEBUG_HTTP) {
            Log.i("WH-HTTP", "HEADER INDO ====  for " + request.hashCode() + " END<<");
        }
        SolidLogger.getInstance().with("WH-ASSIST", "HEADER INDO ====  for " + request.hashCode() + " END<<");
        Response response = null;
        try {
            Log.i("WH-SERIAL_MODE", "HTTP 请求  开始请求x: " + dataItem.getIdentifier() + "  who: " + dataItem.trackWho());
            TrafficStasis.getInstance(context).appendChannelUpload(dataItem.getOwner(), request.headers().toString().getBytes(), dataItem.getData().getBytes());
            response = this.mHttpClient.newCall(request).execute();
            if (!(response == null || response.code() == 200)) {
                Log.i("WH-SERIAL_MODE", "HTTP Response  :" + response.toString());
            }
        } catch (Exception e) {
            Log.i("WH-SERIAL_MODE", "HTTP Response  : Exception:" + e.toString());
            dataItem.setState(4);
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", dataItem.getIdentifier() + " --> 00 上传并没有成功，网络问题？进行下一步处理...", e);
            }
            SolidLogger.getInstance().with("WH-ASSIST", dataItem.getIdentifier() + " --> 00 上传并没有成功，网络问题？进行下一步处理...", e);
        }
        return createResponseDataItem(context, dataItem, response);
    }

    private void autoFillHeader(Context context, DataItem dataItem, Builder builder) {
        Iterator<String> headers = dataItem.retrieveAllHeaderKeys();
        if (headers != null) {
            while (headers.hasNext()) {
                String key = (String) headers.next();
                String v = dataItem.getHeaderValByKey(key);
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "Auto Fill Header , [" + key + "=" + v + "]");
                }
                SolidLogger.getInstance().with("WH-ASSIST", "Auto Fill Header , [" + key + "=" + v + "]");
                try {
                    builder.addHeader(key, v);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (GlobalDefine.DEBUG_COMPANION) {
                        Log.i("WH-ASSIST", "HEADER : [" + key + "=" + v + "]  is ILLEGAL");
                    }
                    SolidLogger.getInstance().with("WH-ASSIST", "HEADER : [" + key + "=" + v + "]  is ILLEGAL");
                }
            }
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "Auto Fill Header end .");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "Auto Fill Header end .");
        }
    }

    private synchronized DataItem createResponseDataItem(Context context, DataItem requestDataItem, Response response) {
        DataItem responseDataItem;
        responseDataItem = new DataItem();
        responseDataItem.setUrl(requestDataItem.getUrl());
        responseDataItem.setFlags(requestDataItem.getFlags());
        responseDataItem.setAction(requestDataItem.getAction());
        responseDataItem.setIdentifier(requestDataItem.getIdentifier());
        responseDataItem.setMethod(requestDataItem.getMethod());
        responseDataItem.setOwner(requestDataItem.getOwner());
        if (response == null || response.code() != 200) {
            responseDataItem.setData(requestDataItem.getData());
            responseDataItem.setExtraData(requestDataItem.getExtraData().toString());
            responseDataItem.setCode(response == null ? -1 : response.code());
            responseDataItem.setState(4);
        } else {
            responseDataItem.setCode(response.code());
            responseDataItem.setState(0);
            try {
                responseDataItem.setData(response.body().string());
                TrafficStasis.getInstance(context).appendChannelDownload(requestDataItem.getOwner(), resStr.getBytes());
                responseDataItem.setUrl("");
                if (requestDataItem.hasFlag(64)) {
                    requestDataItem.getExtraData().remove("key_header");
                    responseDataItem.setExtraData(requestDataItem.getExtraData().toString());
                } else {
                    responseDataItem.addExtraPair("target", requestDataItem.trackWho());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return responseDataItem;
    }

    public DataItem post(Context context, DataItem dataItem) {
        return post(context, dataItem, MEDIA_TYPE_JSON);
    }

    private void printHeaderInfo(String name, String value) {
        SolidLogger.getInstance().with("WH-ASSIST", "HEADER In ====  for : " + hashCode() + " with : [" + name + " , " + value + "]");
        if (GlobalDefine.DEBUG_HTTP) {
            Log.i("WH-HTTP", "HEADER In ====  for : " + hashCode() + " with : [" + name + " , " + value + "]");
        }
    }

    public DataItem post(Context context, DataItem dataItem, MediaType mediaType) {
        String url = dataItem.getUrl();
        Builder requestBuilder = new C10601();
        if (this.mPublicParamsBuilder != null) {
            String urlFilled = this.mPublicParamsBuilder.completeURL(context, url);
            if (!TextUtils.isEmpty(urlFilled)) {
                url = urlFilled;
            }
            this.mPublicParamsBuilder.fillPublicHeader(context, dataItem, requestBuilder);
        }
        requestBuilder.url(url);
        String contentType = null;
        if (dataItem.hasHeader() && (dataItem.getFlags() & 16) != 0) {
            autoFillHeader(context, dataItem, requestBuilder);
            contentType = dataItem.getHeaderValByKey("Content-Type");
        }
        if (!TextUtils.isEmpty(contentType)) {
            mediaType = MediaType.parse(contentType);
        }
        if (dataItem.getData() != null) {
            String sendData = dataItem.getData();
            byte[] byteData = sendData.getBytes();
            if (dataItem.hasFlag(8)) {
                byteData = dataItem.decompress(sendData);
            } else {
                byteData = dataItem.removePrefix(sendData).getBytes();
            }
            try {
                RequestBody body = RequestBody.create(mediaType, byteData);
                if (GlobalDefine.DEBUG) {
                    Log.i("WH-ASSIST", "Request Info : " + body.toString() + " , " + body.hashCode());
                }
                SolidLogger.getInstance().with("WH-ASSIST", "Request Info : " + body.toString() + " , " + body.hashCode());
                requestBuilder.post(body);
                TrafficStasis.getInstance(context).appendChannelUpload(dataItem.getOwner(), byteData);
            } catch (Exception e) {
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "在create request body的时候出错！", e);
                }
                SolidLogger.getInstance().with("WH-ASSIST", "在create request body的时候出错！" + dataItem.toShortString());
                return null;
            }
        }
        Request request = requestBuilder.build();
        TrafficStasis.getInstance(context).appendChannelUpload(dataItem.getOwner(), request.urlString().getBytes(), request.headers().toString().getBytes());
        Response response = null;
        try {
            response = this.mHttpClient.newCall(request).execute();
            if (!(response == null || response.code() == 200)) {
                Log.i("WH-SERIAL_MODE", "HTTP Response  :" + response.toString());
            }
        } catch (Exception e2) {
            dataItem.setState(4);
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", dataItem.getIdentifier() + " --> 上传并没有成功，网络问题？进行下一步处理...", e2);
            }
            SolidLogger.getInstance().with("WH-ASSIST", dataItem.getIdentifier() + " --> 上传并没有成功，网络问题？进行下一步处理...", e2);
        }
        return createResponseDataItem(context, dataItem, response);
    }
}
