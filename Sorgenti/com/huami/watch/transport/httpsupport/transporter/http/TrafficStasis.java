package com.huami.watch.transport.httpsupport.transporter.http;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.AlarmDriver;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TrafficStasis {
    private static Handler mHandler;
    private static TrafficStasis sInstance;
    private ConcurrentHashMap<String, Long> cacheMap;
    private Context mContext;
    private BroadcastReceiver mTimingReset;
    private OnTrafficListener onTrafficListener;
    private Task task = new Task(RunningStatus.WORK_THREAD) {
        public TaskOperation onExecute(TaskOperation taskOperation) {
            TrafficStasis.this.logAllChannels();
            return null;
        }
    };
    private TaskManager taskManager = new TaskManager("task-of-traffic-print");
    private Runnable taskToPrint = new C10644();

    class C10644 implements Runnable {
        C10644() {
        }

        public void run() {
            TrafficStasis.this.taskManager.next(TrafficStasis.this.task).execute();
        }
    }

    class C10655 extends BroadcastReceiver {
        C10655() {
        }

        public void onReceive(Context context, Intent intent) {
            if (TrafficStasis.this.onTrafficListener != null) {
                SolidLogger.getInstance().with2("Traffic", "=======ClEAR--FOR--TIMING---======");
                SharedPreferences pref = TrafficStasis.this.mContext.getSharedPreferences("traffic_stat.xml", 0);
                for (String key : pref.getAll().keySet()) {
                    TrafficStasis.this.onTrafficListener.onTrafficExtract(key, pref.getLong(key, -1), -2);
                }
            }
            TrafficStasis.this.resetAll();
            AlarmDriver.inject(context, 0, 23, 59, 59, 86400000, intent, 0);
        }
    }

    public interface OnTrafficListener {
        void onTrafficExtract(String str, long j, long j2);
    }

    public static synchronized TrafficStasis getInstance(Context context) {
        TrafficStasis trafficStasis;
        synchronized (TrafficStasis.class) {
            if (sInstance == null) {
                sInstance = new TrafficStasis(context);
            }
            trafficStasis = sInstance;
        }
        return trafficStasis;
    }

    private TrafficStasis(Context context) {
        this.mContext = context;
        mHandler = new Handler(Looper.getMainLooper());
        initAll();
        initTiming(context);
    }

    private void initAll() {
        if (this.cacheMap == null) {
            this.cacheMap = new ConcurrentHashMap();
        } else {
            this.cacheMap.clear();
        }
        Map<String, ?> map = this.mContext.getSharedPreferences("traffic_stat.xml", 0).getAll();
        for (String key : map.keySet()) {
            this.cacheMap.put(key, (Long) map.get(key));
        }
    }

    public synchronized void appendChannelUpload(String key, byte[]... others) {
        long len = 0;
        if (others != null) {
            try {
                for (byte[] bs : others) {
                    if (bs != null) {
                        len += (long) bs.length;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        final long val = len;
        final String str = key;
        try {
            this.taskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    SharedPreferences pref = TrafficStasis.this.mContext.getSharedPreferences("traffic_stat.xml", 0);
                    long curr = pref.getLong(str, -1);
                    if (curr == -1) {
                        pref.edit().putLong(str, val).commit();
                    } else {
                        pref.edit().putLong(str, val + curr).commit();
                    }
                    SolidLogger.getInstance().with2("Traffic", ">>> upload Channel [" + str + "] will increase from : " + TrafficStasis.format(curr) + " to : " + TrafficStasis.format(val + curr) + " by: " + TrafficStasis.format(val));
                    return null;
                }
            }).execute();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (mHandler != null) {
            mHandler.removeCallbacks(this.taskToPrint);
            mHandler.postDelayed(this.taskToPrint, 2300);
        }
    }

    public synchronized void appendChannelDownload(String key, byte[]... others) {
        if (others != null) {
            long len = 0;
            try {
                for (byte[] bs : others) {
                    if (bs != null) {
                        len += (long) bs.length;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            final long val = len;
            final String str = key;
            this.taskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    SharedPreferences pref = TrafficStasis.this.mContext.getSharedPreferences("traffic_stat.xml", 0);
                    long curr = pref.getLong(str, -1);
                    if (curr == -1) {
                        pref.edit().putLong(str, val).commit();
                    } else {
                        pref.edit().putLong(str, val + curr).commit();
                    }
                    SolidLogger.getInstance().with2("Traffic", "<<< download Channel [" + str + "] will increase from : " + TrafficStasis.format(curr) + " to : " + TrafficStasis.format(val + curr) + " by: " + TrafficStasis.format(val));
                    return null;
                }
            }).execute();
            if (mHandler != null) {
                mHandler.removeCallbacks(this.taskToPrint);
                mHandler.postDelayed(this.taskToPrint, 2300);
            }
        }
    }

    public synchronized void resetAll() {
        this.mContext.getSharedPreferences("traffic_stat.xml", 0).edit().clear().commit();
    }

    public synchronized void logAllChannels() {
        SharedPreferences pref = this.mContext.getSharedPreferences("traffic_stat.xml", 0);
        Map<String, ?> map = pref.getAll();
        SolidLogger.getInstance().with2("Traffic", "--- print all now --- start\n");
        for (String key : map.keySet()) {
            SolidLogger.getInstance().with2("Traffic", "--- [" + key + "] ------------" + " now at : \t" + format(pref.getLong(key, -1)));
            this.onTrafficListener.onTrafficExtract(key, pref.getLong(key, -1), 0);
        }
        SolidLogger.getInstance().with2("Traffic", "--- print all now --- end\n");
    }

    private void initTiming(Context context) {
        AlarmDriver.inject(context, 0, 23, 59, 59, 86400000, new Intent("clear_traffic_data"), 0);
        if (this.mTimingReset != null) {
            try {
                context.unregisterReceiver(this.mTimingReset);
            } catch (Exception e) {
            }
        } else {
            this.mTimingReset = new C10655();
        }
        IntentFilter f = new IntentFilter();
        f.addAction("clear_traffic_data");
        context.registerReceiver(this.mTimingReset, f);
    }

    public static String format(long val) {
        float kb = ((float) val) / 1024.0f;
        return (kb < 1.0f ? "0" : "") + new DecimalFormat("#.00").format((double) kb) + " KB";
    }
}
