package com.huami.watch.transport.httpsupport.transporter.http;

import android.text.TextUtils;

public class URLCompleter {

    public static class URLSeat {
        public static final String PROVIDER = WRAP("PRD");
        public static final String TOKEN = WRAP("TK");
        public static final String USER_ID = WRAP("UID");

        private static String WRAP(String str) {
            return "[=>..^$^S]" + str + "[E*$*..<=]";
        }
    }

    public static String fillWithTokenRelated(String original, String marker, String with) {
        if (TextUtils.isEmpty(original) || TextUtils.isEmpty(marker)) {
            return original;
        }
        if (with == null) {
            with = "";
        }
        return original.replace(marker, with);
    }
}
