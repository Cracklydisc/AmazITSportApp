package com.huami.watch.transport.httpsupport;

import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Utils {

    public static class BytesZipUtil {
        public static byte[] gZip(byte[] data) {
            Throwable ex;
            Throwable th;
            byte[] b = null;
            ByteArrayOutputStream bos = null;
            GZIPOutputStream gzip = null;
            try {
                ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
                try {
                    GZIPOutputStream gzip2 = new GZIPOutputStream(bos2);
                    try {
                        gzip2.write(data);
                        gzip2.finish();
                        gzip2.close();
                        b = bos2.toByteArray();
                        bos2.close();
                        if (bos2 != null) {
                            try {
                                bos2.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (gzip2 != null) {
                            try {
                                gzip2.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                                gzip = gzip2;
                                bos = bos2;
                            }
                        }
                        gzip = gzip2;
                        bos = bos2;
                    } catch (Throwable th2) {
                        th = th2;
                        gzip = gzip2;
                        bos = bos2;
                        if (bos != null) {
                            bos.close();
                        }
                        if (gzip != null) {
                            gzip.close();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bos = bos2;
                    if (bos != null) {
                        bos.close();
                    }
                    if (gzip != null) {
                        gzip.close();
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                ex = th4;
                ex.printStackTrace();
                if (bos != null) {
                    bos.close();
                }
                if (gzip != null) {
                    gzip.close();
                }
                return b;
            }
            return b;
        }

        public static byte[] unGZip(byte[] data) {
            Exception ex;
            Throwable th;
            byte[] b = null;
            ByteArrayInputStream bis = null;
            GZIPInputStream gzip = null;
            ByteArrayOutputStream baos = null;
            try {
                ByteArrayInputStream bis2 = new ByteArrayInputStream(data);
                try {
                    GZIPInputStream gzip2 = new GZIPInputStream(bis2);
                    try {
                        byte[] buf = new byte[1024];
                        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                        while (true) {
                            try {
                                int num = gzip2.read(buf, 0, buf.length);
                                if (num == -1) {
                                    break;
                                }
                                baos2.write(buf, 0, num);
                            } catch (Exception e) {
                                ex = e;
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            } catch (Throwable th2) {
                                th = th2;
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            }
                        }
                        b = baos2.toByteArray();
                        baos2.flush();
                        baos2.close();
                        gzip2.close();
                        bis2.close();
                        if (bis2 != null) {
                            try {
                                bis2.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (gzip2 != null) {
                            try {
                                gzip2.close();
                            } catch (IOException e22) {
                                e22.printStackTrace();
                            }
                        }
                        if (baos2 != null) {
                            try {
                                baos2.close();
                            } catch (IOException e222) {
                                e222.printStackTrace();
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            }
                        }
                        baos = baos2;
                        gzip = gzip2;
                        bis = bis2;
                    } catch (Exception e3) {
                        ex = e3;
                        gzip = gzip2;
                        bis = bis2;
                        try {
                            ex.printStackTrace();
                            if (bis != null) {
                                try {
                                    bis.close();
                                } catch (IOException e2222) {
                                    e2222.printStackTrace();
                                }
                            }
                            if (gzip != null) {
                                try {
                                    gzip.close();
                                } catch (IOException e22222) {
                                    e22222.printStackTrace();
                                }
                            }
                            if (baos != null) {
                                try {
                                    baos.close();
                                } catch (IOException e222222) {
                                    e222222.printStackTrace();
                                }
                            }
                            return b;
                        } catch (Throwable th3) {
                            th = th3;
                            if (bis != null) {
                                try {
                                    bis.close();
                                } catch (IOException e2222222) {
                                    e2222222.printStackTrace();
                                }
                            }
                            if (gzip != null) {
                                try {
                                    gzip.close();
                                } catch (IOException e22222222) {
                                    e22222222.printStackTrace();
                                }
                            }
                            if (baos != null) {
                                try {
                                    baos.close();
                                } catch (IOException e222222222) {
                                    e222222222.printStackTrace();
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        gzip = gzip2;
                        bis = bis2;
                        if (bis != null) {
                            bis.close();
                        }
                        if (gzip != null) {
                            gzip.close();
                        }
                        if (baos != null) {
                            baos.close();
                        }
                        throw th;
                    }
                } catch (Exception e4) {
                    ex = e4;
                    bis = bis2;
                    ex.printStackTrace();
                    if (bis != null) {
                        bis.close();
                    }
                    if (gzip != null) {
                        gzip.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                    return b;
                } catch (Throwable th5) {
                    th = th5;
                    bis = bis2;
                    if (bis != null) {
                        bis.close();
                    }
                    if (gzip != null) {
                        gzip.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                    throw th;
                }
            } catch (Exception e5) {
                ex = e5;
                ex.printStackTrace();
                if (bis != null) {
                    bis.close();
                }
                if (gzip != null) {
                    gzip.close();
                }
                if (baos != null) {
                    baos.close();
                }
                return b;
            }
            return b;
        }
    }

    private Utils() {
    }

    public static void log(String message) {
        Log.i("CloudServiceTag", message);
    }

    public static boolean writeStringToFile(File file, String data, String encoding) throws IOException {
        OutputStream outputStream = null;
        try {
            outputStream = openOutputStream(file);
            write(data, outputStream, encoding);
            return true;
        } finally {
            closeQuietly(outputStream);
        }
    }

    public static void closeQuietly(OutputStream output) {
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
            }
        }
    }

    public static FileOutputStream openOutputStream(File file) throws IOException {
        if (!file.exists()) {
            File parent = file.getParentFile();
            if (!(parent == null || parent.exists() || parent.mkdirs())) {
                throw new IOException("File '" + file + "' could not be created");
            }
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (!file.canWrite()) {
            throw new IOException("File '" + file + "' cannot be written to");
        }
        return new FileOutputStream(file);
    }

    public static boolean write(String data, OutputStream output, String encoding) throws IOException {
        if (data == null) {
            return false;
        }
        if (encoding == null) {
            write(data, output);
        } else {
            output.write(data.getBytes(encoding));
        }
        return true;
    }

    public static void write(String data, OutputStream output) throws IOException {
        if (data != null) {
            output.write(data.getBytes());
        }
    }

    public static String readFileToString(String strFilePath) {
        String content = "";
        File file = new File(strFilePath);
        if (file.isDirectory()) {
            Log.d("TestFile", "The File doesn't not exist.");
        } else {
            try {
                InputStream instream = new FileInputStream(file);
                if (instream != null) {
                    BufferedReader buffreader = new BufferedReader(new InputStreamReader(instream));
                    while (true) {
                        String line = buffreader.readLine();
                        if (line == null) {
                            break;
                        }
                        content = content + line + "\n";
                    }
                    instream.close();
                }
            } catch (FileNotFoundException e) {
                Log.d("TestFile", "The File doesn't not exist.");
            } catch (IOException e2) {
                Log.d("TestFile", e2.getMessage());
            }
        }
        return content;
    }
}
