package com.huami.watch.transport.httpsupport.translogutils;

import android.os.Environment;
import java.io.File;

public class Util {
    public static final boolean DEBUG = new File(Environment.getExternalStorageDirectory() + "/springchannel.ini").exists();
    public static final boolean DEBUG_SERVER = (DEBUG & 1);
    public static final String[] TRANS_TAGS = new String[]{"_SPORT", "_HEALTH", "_TEST"};
}
