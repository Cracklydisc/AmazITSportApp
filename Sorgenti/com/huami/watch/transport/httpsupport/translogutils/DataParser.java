package com.huami.watch.transport.httpsupport.translogutils;

import com.huami.watch.transport.httpsupport.model.DataItem;

public class DataParser {
    private DataItem dataItem;
    private int flag;

    public DataParser(DataItem dataItem, int flag) {
        this.dataItem = dataItem;
        this.flag = flag;
    }

    public String getImportanceInfo() {
        if (this.dataItem == null || this.dataItem.getData() == null || this.dataItem.getData().length() <= 0) {
            return "";
        }
        StringBuffer info = new StringBuffer();
        if (2 == this.flag) {
            info.append("-receice");
        } else if (1 == this.flag) {
            info.append("-send");
        } else {
            info.append("error");
        }
        info.append("-id:" + this.dataItem.getIdentifier());
        info.append("-action:" + this.dataItem.getAction());
        info.append("-owner:" + this.dataItem.getOwner());
        info.append("-isBigData:" + (this.dataItem.hasFlag(32768) ? "big" : "small"));
        return info.toString();
    }

    public String bleSendResText(DataItem dataItem, int code) {
        StringBuffer stuBuf = new StringBuffer();
        if (code == 0) {
            stuBuf.append("-send-blt_sucess-");
            stuBuf.append("-id:" + dataItem.getIdentifier());
            stuBuf.append("-action-" + dataItem.getAction());
            return stuBuf.toString();
        }
        stuBuf.append("-send-blt_failed");
        stuBuf.append("-id:" + dataItem.getIdentifier());
        stuBuf.append("-action" + dataItem.getAction());
        return stuBuf.toString();
    }

    public String bleReceiveResText() {
        StringBuffer stuBuf = new StringBuffer();
        stuBuf.append("-receive-blt_sucess");
        stuBuf.append("-id:" + this.dataItem.getIdentifier());
        stuBuf.append("-action" + this.dataItem.getAction());
        return stuBuf.toString();
    }

    public String getCacheDeleteStateText(boolean res) {
        StringBuffer info = new StringBuffer();
        info.append(res ? "--delete cache data sucess:" : "--delete cache data failed:");
        info.append("-id:" + this.dataItem.getIdentifier());
        info.append("-action:" + this.dataItem.getAction());
        return info.toString();
    }

    public String getCachaThisText() {
        StringBuffer info = new StringBuffer();
        info.append("-cache this to local-");
        info.append("-id:" + this.dataItem.getIdentifier());
        info.append("-action:" + this.dataItem.getAction());
        return info.toString();
    }
}
