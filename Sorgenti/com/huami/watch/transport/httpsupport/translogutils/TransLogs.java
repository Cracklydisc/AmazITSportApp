package com.huami.watch.transport.httpsupport.translogutils;

import android.util.Log;
import clc.utils.debug.slog.SolidLogger;

public class TransLogs {
    public static void ii(String msg) {
        if (Util.DEBUG_SERVER) {
            Log.i("LOG_TRANS_NEW", msg);
        }
        SolidLogger.getInstance().with("LOG_TRANS_NEW", msg);
    }

    public static void m11i(String msg) {
        if (Util.DEBUG_SERVER) {
            Log.i("LOG_TRANS", msg);
        }
        SolidLogger.getInstance().with("LOG_TRANS", msg);
    }
}
