package com.huami.watch.transport.httpsupport.StageFright;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessStateMonitor {
    private Jack jack;
    private byte mLastState;
    private byte mState;
    private ConcurrentHashMap<String, Boolean> mWho2FinishMap;
    private ConcurrentHashMap<String, Boolean> myExtractId2StateMap;
    public String name;
    private String reportId;
    private int sumOfFinishedAssist2Cloud;
    private int sumOfFinishedRom2Assist;
    private int sumOfNeedAssist2Cloud;
    private int sumOfNeedRom2Assist;
    private long syncStateTime;
    private String trackWho;

    public interface Jack {
        void onFinish(int i, boolean z, long j);

        void onUpdate(int i, int i2, int i3);

        String targetAtAction();

        String targetAtPackage();
    }

    public void appendSumOfNeedRom2Assist(int append) {
        this.sumOfNeedRom2Assist += append;
    }

    public void appendSumOfNeedAssist2Cloud(int append) {
        Log.i("CloudSyncMonitor", "now append sum : " + append, new Throwable());
        this.sumOfNeedAssist2Cloud += append;
    }

    public void finishOneRom2Assist(DataItem item) {
        if (theJack() != null) {
            this.sumOfFinishedRom2Assist++;
            Log.i("CloudSyncMonitor", this.name + " , " + targetWho() + " ==> ROM到助手 ： 总数： " + this.sumOfNeedRom2Assist + " , 已完成：" + this.sumOfFinishedRom2Assist + " , item：" + item.getIdentifier() + " , " + item.trackWho());
            this.jack.onUpdate(4, this.sumOfNeedRom2Assist, this.sumOfFinishedRom2Assist);
        }
    }

    public void finishOneAssist2Cloud(DataItem item) {
        if (theJack() != null) {
            this.sumOfFinishedAssist2Cloud++;
            Log.i("CloudSyncMonitor", this.name + " , " + targetWho() + " ==> 助手到云端 ： 总数： " + this.sumOfNeedAssist2Cloud + " , 已完成：" + this.sumOfFinishedAssist2Cloud + " , item：" + item.getIdentifier() + " , " + item.trackWho(), new Throwable());
            this.jack.onUpdate(3, this.sumOfNeedAssist2Cloud, this.sumOfFinishedAssist2Cloud);
        }
    }

    public void setSumOfNeedAssist2Cloud(int sum) {
        this.sumOfNeedAssist2Cloud = sum;
        this.sumOfFinishedRom2Assist = 0;
    }

    public ProcessStateMonitor() {
        this.mState = (byte) 0;
        this.mLastState = this.mState;
        this.sumOfNeedRom2Assist = 0;
        this.sumOfNeedAssist2Cloud = 0;
        this.sumOfFinishedRom2Assist = 0;
        this.sumOfFinishedAssist2Cloud = 0;
        this.syncStateTime = System.currentTimeMillis();
        this.mWho2FinishMap = new ConcurrentHashMap();
        this.trackWho = null;
        this.syncStateTime = System.currentTimeMillis();
        StringBuilder append = new StringBuilder().append("processor is : ");
        String str = "" + new Random().nextLong();
        this.name = str;
        Log.i("CloudSyncMonitor", append.append(str).toString());
    }

    protected void onReportArrival(Context context, ProcessStateMonitor monitor, DataItem reportItem) {
        Serializer.getInstance(context).onEventProcessReportItem(monitor, reportItem);
    }

    protected String targetWho() {
        return trackWho();
    }

    protected String targetAtPackage() {
        return this.jack == null ? "" : this.jack.targetAtPackage();
    }

    protected String targetAtAction() {
        return this.jack == null ? "" : this.jack.targetAtAction();
    }

    public String trackWho() {
        if (TextUtils.isEmpty(this.trackWho)) {
            this.trackWho = targetAtPackage() + "#" + targetAtAction();
        }
        return this.trackWho;
    }

    public void onFeedSingleLazyGirl(Context context, DataItem item) {
        if (!Command.isInternal(item.getAction())) {
            if (this.myExtractId2StateMap == null) {
                this.myExtractId2StateMap = new ConcurrentHashMap();
            }
            if (Boolean.valueOf(this.myExtractId2StateMap.containsKey(item.getIdentifier())).booleanValue()) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", trackWho() + "arrival OUR TRAFFIC tracked : " + item.getIdentifier());
                }
                finishOneRom2Assist(item);
                this.myExtractId2StateMap.put(item.getIdentifier(), Boolean.valueOf(true));
            } else if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", trackWho() + "arrival OUR TRAFFIC tracked : " + item.getIdentifier() + " BUT NOT EXPECTED !");
            }
            Log.i("WH-SERIAL_MODE", trackWho() + " ==> arrival ids 剩余量如下 \n");
            int sum = 0;
            for (String id : this.myExtractId2StateMap.keySet()) {
                if (!((Boolean) this.myExtractId2StateMap.get(id)).booleanValue()) {
                    sum++;
                }
                Log.i("WH-SERIAL_MODE", "arrival: " + id + " ：" + this.myExtractId2StateMap.get(id) + "\n");
            }
            Log.i("WH-SERIAL_MODE", trackWho() + " ==> arrival ids 剩余量 ： " + sum);
            SolidLogger.getInstance().with("WH-SERIAL_MODE", trackWho() + " ==> arrival ids 剩余量 ： " + sum);
            boolean ret = true;
            for (Boolean b : this.myExtractId2StateMap.values()) {
                ret &= b.booleanValue();
            }
            if (ret) {
                SyncProcessManager.getInstance().tellWith(trackWho(), (byte) 7, new int[0]);
                onUpdateAssist2CloudState((byte) 2);
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", hashCode() + " , " + trackWho() + " arrival Traffic 数据track完成了呢！");
                        Log.i("WH-SERIAL_MODE", hashCode() + " , " + trackWho() + "arrival 移除掉了time out callback！");
                    }
                    SolidLogger.getInstance().with("WH-SRV", hashCode() + "  arrival Traffic 数据track完成了呢！");
                    SolidLogger.getInstance().with("WH-SRV", hashCode() + "  arrival 移除掉了time out callback！");
                }
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", trackWho() + "arrival -------->>>>> 开始分析Reportlist！");
                }
                SolidLogger.getInstance().with("WH-SRV", trackWho() + "arrival -------->>>>> 开始分析 reportlist！");
                Serializer.getInstance(context).onEventOfDanceWith(this);
            }
        }
    }

    public void startWaitForDataItems(HashSet<String> thisIds) {
        if (this.myExtractId2StateMap == null) {
            this.myExtractId2StateMap = new ConcurrentHashMap();
        }
        Iterator i$ = thisIds.iterator();
        while (i$.hasNext()) {
            this.myExtractId2StateMap.put((String) i$.next(), Boolean.valueOf(false));
        }
        for (String id : this.myExtractId2StateMap.keySet()) {
            if (((Boolean) this.myExtractId2StateMap.get(id)).booleanValue()) {
                this.myExtractId2StateMap.remove(id);
            }
        }
    }

    public String getReportId() {
        return this.reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public void inJack(Jack j) {
        this.jack = j;
    }

    private Jack theJack() {
        return this.jack;
    }

    public void onUpdateRom2AssistState(byte state) {
        if (this.jack != null) {
            Log.i("CloudSyncMonitor", this.name + " , " + targetWho() + " ==> ROM到助手 ： 总数： " + this.sumOfNeedRom2Assist + " , 已完成：" + this.sumOfFinishedRom2Assist);
            if (this.sumOfNeedRom2Assist != 0 || this.sumOfFinishedRom2Assist != 0) {
                this.jack.onUpdate(state, this.sumOfNeedRom2Assist, this.sumOfFinishedRom2Assist);
                this.mState = state;
            }
        }
    }

    public void onUpdateAssist2CloudState(byte state) {
        if (this.jack != null) {
            Log.i("CloudSyncMonitor", this.name + " , " + targetWho() + " ==> 助手到云端 ： 总数： " + this.sumOfNeedAssist2Cloud + " , 已完成：" + this.sumOfFinishedAssist2Cloud);
            if (this.sumOfNeedAssist2Cloud != 0 || this.sumOfFinishedAssist2Cloud != 0) {
                this.jack.onUpdate(state, this.sumOfNeedAssist2Cloud, this.sumOfFinishedAssist2Cloud);
                this.mState = state;
            }
        }
    }

    public void onFinish(String action, int code) {
        if (isAllHolyFinished() || code != 0) {
            if (theJack() != null) {
                this.jack.onFinish(code, this.sumOfFinishedAssist2Cloud > 0, this.syncStateTime);
            }
            Log.i("CloudSyncMonitor", this.name + ">>>>>> FINISH '" + targetAtPackage() + "#" + targetAtAction() + " OF : " + action + "' DATA SYNC ! CODE: " + code, new Throwable());
            this.sumOfFinishedRom2Assist = 0;
            this.sumOfFinishedAssist2Cloud = 0;
            this.sumOfNeedAssist2Cloud = 0;
            this.sumOfNeedRom2Assist = 0;
            AnalysisStage.getInstance().removeReportArrivalListener(this);
            return;
        }
        Log.i("CloudSyncMonitor", this.name + ">>>>>> 调用了FINISH ' 但是还不是时候！！！！" + action);
    }

    public HashSet<String> extractTargetActions() {
        HashSet<String> res = new HashSet();
        res.addAll(Arrays.asList(targetAtAction().split("\\|")));
        return res;
    }

    public void addNewHolyWho2Queque(String who) {
        this.mWho2FinishMap.put(who, Boolean.valueOf(false));
    }

    public void finishHolyWho(String who) {
        this.mWho2FinishMap.put(who, Boolean.valueOf(true));
    }

    public String pollHolyParentTarget() {
        for (String key : this.mWho2FinishMap.keySet()) {
            Boolean b = (Boolean) this.mWho2FinishMap.get(key);
            if (b != null && !b.booleanValue()) {
                return key;
            }
        }
        return null;
    }

    public boolean isAllHolyFinished() {
        boolean res = true;
        for (Boolean b : this.mWho2FinishMap.values()) {
            if (b == null) {
                res &= 0;
            }
            res &= b.booleanValue();
        }
        return res;
    }
}
