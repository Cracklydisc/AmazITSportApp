package com.huami.watch.transport.httpsupport.StageFright;

import android.text.TextUtils;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReportListInfo {
    private static int myVersion = 1;
    public boolean isKey;
    private ConcurrentHashMap<Integer, HashSet<String>> mLoc2WhomMap;
    private ConcurrentHashMap<String, ConcurrentLinkedQueue<DataItem>> mOwner2StoringWellItemMap;
    public int sum;

    public static int getVersion() {
        return myVersion;
    }

    public ReportListInfo() {
        this(Integer.valueOf("1").intValue());
    }

    public ReportListInfo(int version) {
        this.isKey = false;
        this.mOwner2StoringWellItemMap = new ConcurrentHashMap();
        this.mLoc2WhomMap = new ConcurrentHashMap();
        myVersion = version;
    }

    public void addItem(Integer loc, DataItem item) {
        String who = item.trackWho();
        ConcurrentLinkedQueue<DataItem> list = (ConcurrentLinkedQueue) this.mOwner2StoringWellItemMap.get(who);
        if (list == null) {
            list = new ConcurrentLinkedQueue();
            this.mOwner2StoringWellItemMap.put(who, list);
        }
        HashSet<String> whom = (HashSet) this.mLoc2WhomMap.get(loc);
        if (whom == null) {
            whom = new HashSet();
            this.mLoc2WhomMap.put(loc, whom);
        }
        list.add(item);
        whom.add(who);
    }

    public ConcurrentHashMap<String, ConcurrentLinkedQueue<DataItem>> getThings() {
        return this.mOwner2StoringWellItemMap;
    }

    public ConcurrentHashMap<Integer, HashSet<String>> getWhomMapThings() {
        return this.mLoc2WhomMap;
    }

    public static synchronized boolean hasData(ReportListInfo info) {
        boolean z;
        synchronized (ReportListInfo.class) {
            z = (info.getThings().isEmpty() || info.getThings().keySet().isEmpty()) ? false : true;
        }
        return z;
    }

    public static ReportListInfo from(String str) {
        return from(str, false);
    }

    public static ReportListInfo from(String str, boolean markKey) {
        ReportListInfo infos = new ReportListInfo(myVersion);
        infos.isKey = markKey;
        try {
            JSONObject jsonObject = new JSONObject(str);
            Integer ver = Integer.valueOf(0);
            if (jsonObject.has("ver")) {
                ver = Integer.valueOf(jsonObject.getInt("ver"));
            }
            if (ver == null) {
                ver = Integer.valueOf(0);
            }
            myVersion = ver.intValue();
            if (ver.intValue() == 0) {
                parseAtVer0(infos, jsonObject);
            } else if (ver.intValue() == 1) {
                parseAtVer1(infos, jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return infos;
    }

    private static void parseAtVer0(ReportListInfo infos, JSONObject jsonObject) {
        int allSum = 0;
        Iterator<String> itr = jsonObject.keys();
        while (itr.hasNext()) {
            String location = (String) itr.next();
            JSONObject singleItem = jsonObject.optJSONObject(location);
            if (singleItem != null) {
                allSum = singleItem.optInt("sum");
                String who = singleItem.optString("who");
                if (!TextUtils.isEmpty(who)) {
                    String[] whos = who.split("#");
                    String data = singleItem.optString("data");
                    if (!TextUtils.isEmpty(data)) {
                        for (String cell : data.split("#")) {
                            DataItem item = new DataItem();
                            item.setIdentifier(cell);
                            item.setOwner(whos[0]);
                            item.setAction(whos[1]);
                            infos.addItem(Integer.valueOf(Integer.valueOf(location).intValue()), item);
                        }
                    }
                }
            }
        }
        infos.sum = allSum;
    }

    private static void parseAtVer1(ReportListInfo frame, JSONObject jsonObject) throws JSONException {
        int allSum = 0;
        Iterator<String> itr = jsonObject.keys();
        while (itr.hasNext()) {
            String location = (String) itr.next();
            JSONArray arrayItems = jsonObject.optJSONArray(location);
            if (arrayItems != null) {
                int len = arrayItems.length();
                for (int i = 0; i < len; i++) {
                    JSONObject singleItem = arrayItems.getJSONObject(i);
                    String data = singleItem.optString("data");
                    if (!TextUtils.isEmpty(data)) {
                        allSum += singleItem.optInt("sum");
                        String who = singleItem.optString("who");
                        if (!TextUtils.isEmpty(who)) {
                            String[] whos = who.split("#");
                            for (String cell : data.split("#")) {
                                DataItem item = new DataItem();
                                item.setIdentifier(cell);
                                item.setOwner(whos[0]);
                                item.setAction(whos[1]);
                                frame.addItem(Integer.valueOf(location), item);
                            }
                        }
                    }
                }
            }
        }
        frame.sum = allSum;
    }

    public String flatten() {
        if (myVersion == 0) {
            return flattenV0();
        }
        if (myVersion == 1) {
            return flattenV1();
        }
        return null;
    }

    private String flattenV0() {
        JSONObject root = new JSONObject();
        try {
            for (Integer l : this.mLoc2WhomMap.keySet()) {
                if (l != null) {
                    Iterator i$;
                    String key;
                    JSONObject singleItem = new JSONObject();
                    root.put(String.valueOf(l), singleItem);
                    ConcurrentLinkedQueue<DataItem> list = null;
                    for (String key2 : this.mOwner2StoringWellItemMap.keySet()) {
                        list = (ConcurrentLinkedQueue) this.mOwner2StoringWellItemMap.get(key2);
                        if (list != null) {
                            break;
                        }
                    }
                    if (list != null) {
                        i$ = list.iterator();
                        while (i$.hasNext()) {
                            DataItem id = (DataItem) i$.next();
                            key2 = id.getOwner() + "#" + id.getAction();
                            boolean hasData = singleItem.has("data");
                            String wdata = wrapTheData(id);
                            if (hasData) {
                                singleItem.put("data", new StringBuffer(singleItem.optString("data")).append("#").append(wdata).toString());
                                singleItem.put("sum", singleItem.optInt("sum") + 1);
                            } else {
                                singleItem.put("who", key2);
                                singleItem.put("sum", 1);
                                singleItem.put("data", wdata);
                            }
                        }
                        continue;
                    } else {
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root.toString();
    }

    public String flattenV1() {
        JSONObject root = new JSONObject();
        try {
            for (Integer l : this.mLoc2WhomMap.keySet()) {
                if (l != null) {
                    JSONArray arrayItems = new JSONArray();
                    root.put(String.valueOf(l), arrayItems);
                    Iterator it = ((HashSet) this.mLoc2WhomMap.get(l)).iterator();
                    while (it.hasNext()) {
                        String who = (String) it.next();
                        HashMap<String, JSONObject> cache = new HashMap();
                        JSONObject singleItem = (JSONObject) cache.get(who);
                        Iterator i$ = ((ConcurrentLinkedQueue) this.mOwner2StoringWellItemMap.get(who)).iterator();
                        while (i$.hasNext()) {
                            String wdata = wrapTheData((DataItem) i$.next());
                            if (singleItem == null) {
                                singleItem = new JSONObject();
                                singleItem.put("who", who);
                                singleItem.put("sum", 1);
                                singleItem.put("data", wdata);
                                arrayItems.put(singleItem);
                                cache.put(who, singleItem);
                            } else {
                                singleItem.put("data", new StringBuffer(singleItem.optString("data")).append("#").append(wdata).toString());
                                singleItem.put("sum", singleItem.optInt("sum") + 1);
                            }
                        }
                    }
                    continue;
                }
            }
            root.put("ver", myVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return root.toString();
    }

    private String wrapTheData(DataItem id) {
        return id.hasFlag(16384) ? "." + id.getIdentifier() : id.getIdentifier();
    }

    public static synchronized boolean isItemNeedSerial(DataItem item) {
        boolean z = false;
        synchronized (ReportListInfo.class) {
            if (item != null) {
                if (item.getIdentifier().indexOf(".") != -1 || item.hasFlag(16384)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static ReportListInfo translateV0TOV1(ReportListInfo target, int toVer) {
        JSONObject jSONObject;
        JSONException e;
        if (getVersion() == toVer) {
            return target;
        }
        String str = target.flatten();
        JSONObject newObj = new JSONObject();
        try {
            JSONObject obj = new JSONObject(str);
            try {
                Iterator<String> keys = obj.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    JSONArray array;
                    if (newObj.has(key)) {
                        array = newObj.optJSONArray(key);
                        if (array != null) {
                            array.put(obj.getJSONObject(key));
                        }
                    } else {
                        array = new JSONArray();
                        newObj.put(key, array);
                        array.put(obj.getJSONObject(key));
                    }
                }
                newObj.put("ver", "1");
                jSONObject = obj;
            } catch (JSONException e2) {
                e = e2;
                jSONObject = obj;
            }
        } catch (JSONException e3) {
            e = e3;
            e.printStackTrace();
            return from(newObj.toString());
        }
        return from(newObj.toString());
    }
}
