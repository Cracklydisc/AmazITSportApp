package com.huami.watch.transport.httpsupport.StageFright;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.TaskManager;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

public class AnalysisStage {
    public static final Long INTERVAL_OF_TWICE_TRIGGER = Long.valueOf(180000);
    private static final HashSet<String> NEED_FIRST_BOOT_FLAG = new HashSet();
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static TaskManager sTaskManager = new TaskManager("analysis-stage");
    private static volatile AnalysisStage singleton;
    private ConcurrentHashMap<String, ProcessStateMonitor> mWho2ReportArrivalListenerMap;

    private AnalysisStage() {
    }

    public static AnalysisStage getInstance() {
        if (singleton == null) {
            synchronized (AnalysisStage.class) {
                if (singleton == null) {
                    singleton = new AnalysisStage();
                }
            }
        }
        return singleton;
    }

    public void addReportArrivalListener(ProcessStateMonitor l) {
        if (this.mWho2ReportArrivalListenerMap == null) {
            this.mWho2ReportArrivalListenerMap = new ConcurrentHashMap();
        }
        this.mWho2ReportArrivalListenerMap.put(l.trackWho(), l);
    }

    public ProcessStateMonitor findProcessStateMonitor(String wholeWho) {
        if (this.mWho2ReportArrivalListenerMap == null || TextUtils.isEmpty(wholeWho)) {
            return null;
        }
        String[] things = wholeWho.split("#");
        String pkg = things[0];
        String action = things[1];
        for (String keyPrefix : this.mWho2ReportArrivalListenerMap.keySet()) {
            if (keyPrefix.startsWith(pkg)) {
                HashSet<String> actions = new HashSet();
                actions.addAll(Arrays.asList(keyPrefix.split("#")[1].split("\\|")));
                if (actions.contains(action) || actions.contains("*")) {
                    return (ProcessStateMonitor) this.mWho2ReportArrivalListenerMap.get(keyPrefix);
                }
            }
        }
        return null;
    }

    public void removeReportArrivalListener(ProcessStateMonitor l) {
        if (l != null) {
            this.mWho2ReportArrivalListenerMap.remove(l.trackWho());
        }
    }

    public void handleReportHub(Context context, DataItem item) {
        String id = item.getIdentifier();
        String who = item.trackWho();
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "Touch handle report hub for : " + id);
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "Touch handle report hub for : " + id);
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "Real handle report hub for : " + item.getIdentifier());
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "Real handle report hub for : " + item.getIdentifier());
        if (this.mWho2ReportArrivalListenerMap != null) {
            ProcessStateMonitor reportArrivalListener = (ProcessStateMonitor) this.mWho2ReportArrivalListenerMap.get(who);
            if (reportArrivalListener != null) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "找到Report 处理器 : " + reportArrivalListener.targetWho() + " for : " + item.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "找到Report 处理器 : " + reportArrivalListener.targetWho() + " for : " + item.getIdentifier());
                reportArrivalListener.onReportArrival(context, reportArrivalListener, item);
                return;
            }
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "未能找到Report 处理器 for : " + item.getIdentifier());
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "未能找到Report 处理器 for : " + item.getIdentifier());
        }
    }
}
