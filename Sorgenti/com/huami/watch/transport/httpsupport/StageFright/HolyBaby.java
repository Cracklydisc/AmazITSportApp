package com.huami.watch.transport.httpsupport.StageFright;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.TaskManager;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class HolyBaby extends DataItem {
    private ConcurrentLinkedQueue<String> homeWork;
    private String parent;

    class C10081 implements Comparator<String> {
        C10081() {
        }

        public int compare(String lhs, String rhs) {
            Long timel = HolyBaby.this.parseTimeStamp(lhs);
            Long timer = HolyBaby.this.parseTimeStamp(rhs);
            if (timel.longValue() > timer.longValue()) {
                return 1;
            }
            if (timel == timer) {
                return 0;
            }
            return -1;
        }
    }

    public HolyBaby() {
        super(null, "_holy_", "_holy_board", "get");
        addExtraPair("gradle", "");
        setIdentifier(System.currentTimeMillis() + "");
    }

    public void setOwner(String owner) {
        super.setOwner("_holy_board");
    }

    public static HolyBaby from(DataItem item) {
        HolyBaby baby = new HolyBaby();
        baby.setIdentifier(item.getIdentifier());
        baby.addExtraPair("gradle", item.getExtraValByKey("gradle"));
        baby.addExtraPair("target", item.trackWho());
        baby.withParent(item.trackWho());
        return baby;
    }

    public void addExtraPair(String key, String val) {
        super.addExtraPair(key, val);
        if (TextUtils.equals(key, "gradle")) {
            lookupAllHomeWork();
        }
    }

    public void fillHomeWork(Context context, ConcurrentLinkedQueue<String> serialIds) {
        String[] arrayedItemIds = (String[]) serialIds.toArray(new String[0]);
        sortByTimeStamp(arrayedItemIds);
        StringBuffer sb = new StringBuffer();
        for (String id : arrayedItemIds) {
            sb.append(id + "#");
        }
        addExtraPair("gradle", sb.toString());
        DataCacher.getInstance(context).save((DataItem) this);
    }

    private String[] sortByTimeStamp(String[] arrayedItem) {
        Arrays.sort(arrayedItem, new C10081());
        return arrayedItem;
    }

    private Long parseTimeStamp(String ids) {
        if (TextUtils.isEmpty(ids)) {
            return Long.valueOf(0);
        }
        String timeStamp;
        String[] timeStr = ids.split("_");
        if (timeStr == null || timeStr.length < 2 || TextUtils.isEmpty(timeStr[1])) {
            timeStamp = String.valueOf(Long.MIN_VALUE);
        } else {
            timeStamp = timeStr[1];
        }
        Long time = Long.valueOf(0);
        try {
            return Long.valueOf(timeStamp);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return time;
        }
    }

    public boolean finishHomeWork(Context context, String singleWork) {
        if (this.homeWork == null) {
            return true;
        }
        String check = (String) this.homeWork.peek();
        if (TextUtils.equals(check, singleWork)) {
            this.homeWork.poll();
            DataCacher.getInstance(context).save((DataItem) this);
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "BABY : " + getIdentifier() + " 完成了作业 ==> " + singleWork);
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "BABY : " + getIdentifier() + " 完成了作业 ==> " + singleWork);
            return true;
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "作业没对上！！！： check " + check + " --> against " + singleWork);
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "作业没对上！！！： check " + check + " --> against " + singleWork);
        return false;
    }

    public String nextHomeWork() {
        return this.homeWork == null ? null : (String) this.homeWork.peek();
    }

    public ConcurrentLinkedQueue<String> lookupAllHomeWork() {
        String holy = getExtraValByKey("gradle");
        if (TextUtils.isEmpty(holy)) {
            return null;
        }
        String[] strs = holy.split("#");
        if (this.homeWork == null) {
            this.homeWork = new ConcurrentLinkedQueue();
        }
        for (String s : strs) {
            if (!this.homeWork.contains(s)) {
                this.homeWork.add(s);
            }
        }
        return this.homeWork;
    }

    public boolean hasMoreHomeWork() {
        return (this.homeWork == null || this.homeWork.isEmpty()) ? false : true;
    }

    public void withParent(String parent) {
        this.parent = parent;
        addExtraPair("target", parent);
    }

    public String parent() {
        return this.parent;
    }

    public static HashSet<HolyBaby> zygoteAddAndFillHomeWorkAndKickToFly(Context context, ProcessStateMonitor monitor, ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> sWho2UploadSerial, TaskManager taskManager) {
        HashSet<HolyBaby> holyBabies = new HashSet();
        for (String who : sWho2UploadSerial.keySet()) {
            if (TextUtils.isEmpty(who)) {
                Log.i("WH-SERIAL_MODE", "空的===================== target DROPped !!!!!!!!!!!!!!!!!!!!!");
            } else {
                String whoPackage = who.split("#")[0];
                String whoAction = who.split("#")[1];
                String targetPackage = monitor.targetAtPackage();
                boolean allAction = TextUtils.equals(monitor.targetAtAction(), "*");
                if (!TextUtils.equals(targetPackage, "*") && !TextUtils.equals(whoPackage, targetPackage)) {
                    Log.i("WH-SERIAL_MODE", "注意！！！ 111 忽略了 不关心的who ： " + who + " , 只关心： " + targetPackage);
                } else if (allAction || monitor.extractTargetActions().contains(whoAction)) {
                    Log.i("WH-SERIAL_MODE", "*****OK GO --> 接纳了 who ： " + who);
                    HolyBaby baby = new HolyBaby();
                    baby.withParent(who);
                    holyBabies.add(baby);
                    baby.fillHomeWork(context, (ConcurrentLinkedQueue) sWho2UploadSerial.get(who));
                    HolyBoard.getInstance(context).addBaby(baby);
                    monitor.addNewHolyWho2Queque(who);
                } else {
                    Log.i("WH-SERIAL_MODE", "注意！！！  忽略了 不关心的who ： " + who + " , 只关心： " + targetPackage);
                }
            }
        }
        if (!TextUtils.isEmpty(monitor.pollHolyParentTarget())) {
            HolyBoard.getInstance(context).kickToFly(taskManager, monitor, monitor.pollHolyParentTarget());
        }
        return holyBabies;
    }
}
