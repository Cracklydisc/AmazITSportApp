package com.huami.watch.transport.httpsupport.StageFright;

import android.util.Log;

public class SyncProcessManager {
    private static String currDesc = "";
    private static int sCurrTotalToCloud = 0;
    private static int sHandledToTalToCloud = 0;
    private static volatile SyncProcessManager singleton;
    private byte mCurrState = (byte) 0;

    private SyncProcessManager() {
    }

    public static SyncProcessManager getInstance() {
        if (singleton == null) {
            synchronized (SyncProcessManager.class) {
                if (singleton == null) {
                    singleton = new SyncProcessManager();
                }
            }
        }
        return singleton;
    }

    public int getPercent() {
        float p = ((float) sHandledToTalToCloud) / ((float) Math.max(sCurrTotalToCloud, sHandledToTalToCloud));
        log("percent : " + p);
        if (p > 1.0f) {
            p = 1.0f;
        } else if (p < 0.0f) {
            p = 0.0f;
        }
        return (int) (100.0f * p);
    }

    private void log(String log) {
        Log.i("yzls", "me -> " + log, new Throwable());
    }

    public void tellWith(String who, byte state, int... info) {
        switch (state) {
            case (byte) 0:
                sCurrTotalToCloud = 0;
                log("正在更新...");
                sHandledToTalToCloud = 0;
                currDesc = "正在更新手表数据...";
                break;
            case (byte) 1:
                log("正在收集手表数据...");
                break;
            case (byte) 2:
                log("正在统计...");
                break;
            case (byte) 3:
                sCurrTotalToCloud = info[0];
                log("正在上传云端... [" + sHandledToTalToCloud + "/" + Math.max(sCurrTotalToCloud, sHandledToTalToCloud) + "]");
                currDesc = "已上传云端 " + getPercent() + "%";
                break;
            case (byte) 4:
                log("同步失败");
                break;
            case (byte) 5:
                log("同步成功");
                break;
            case (byte) 6:
                int total = 0;
                int remain = 0;
                if (info != null && info.length > 0) {
                    total = info[0];
                    if (info.length > 1) {
                        remain = info[1];
                    }
                }
                if (total - remain != total) {
                    log("正在同步手表数据... [" + (total - remain) + "/" + total + "]");
                    currDesc = "正在同步手表数据... " + (((int) (((float) (total - remain)) / ((float) total))) * 100) + "%";
                    break;
                }
                return;
            case (byte) 7:
                log("检查手表数据...");
                break;
            case (byte) 8:
                log("准备上传... " + info[0]);
                sCurrTotalToCloud = info[0];
                break;
            case (byte) 9:
                sHandledToTalToCloud++;
                int handled = sHandledToTalToCloud;
                int all = Math.max(sCurrTotalToCloud, sHandledToTalToCloud);
                if (handled != all) {
                    log("正在上传云端... [" + handled + "/" + all + "]");
                    currDesc = "已上传云端 " + getPercent() + "%";
                    break;
                }
                break;
        }
        this.mCurrState = state;
        updateListener(who);
    }

    void updateListener(String who) {
        String[] things = new String[2];
        try {
            things = who.split("#");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (things != null) {
            String pkg = things[0];
            String action = things[1];
        }
    }
}
