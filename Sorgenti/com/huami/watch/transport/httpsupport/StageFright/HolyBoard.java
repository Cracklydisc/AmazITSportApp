package com.huami.watch.transport.httpsupport.StageFright;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.control.assist.WearHttpCompanion;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class HolyBoard {
    private static volatile HolyBoard singleton;
    private Context mContext;
    private ConcurrentHashMap<String, WeakReference<ConcurrentLinkedQueue<HolyBaby>>> who2BabiesWeakRef = new ConcurrentHashMap();

    class C10091 implements Comparator<DataItem> {
        C10091() {
        }

        public int compare(DataItem lhs, DataItem rhs) {
            Long l = null;
            Long r = null;
            try {
                String lo = lhs.getIdentifier();
                String ro = rhs.getIdentifier();
                l = Long.valueOf(lo);
                r = Long.valueOf(ro);
                if (l == null) {
                    l = Long.valueOf(0);
                }
                if (r == null) {
                    r = Long.valueOf(0);
                }
                if (l == r) {
                    return 0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return l.longValue() - r.longValue() < 0 ? -1 : 1;
        }
    }

    private HolyBoard(Context context) {
        this.mContext = context;
    }

    public static HolyBoard getInstance(Context context) {
        if (singleton == null) {
            synchronized (HolyBoard.class) {
                if (singleton == null) {
                    singleton = new HolyBoard(context);
                }
            }
        }
        return singleton;
    }

    private synchronized void wentBackToGradleHead(HolyBaby headBaby) {
        getGradle(headBaby.parent(), true);
    }

    public synchronized void addBaby(HolyBaby baby) {
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "增加了新的HOLY BABY : " + baby.getIdentifier() + "  , parent : " + baby.parent());
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "增加了新的HOLY BABY : " + baby.getIdentifier() + "  , parent : " + baby.parent());
        getGradle(baby.parent(), false).add(baby);
        getGradle(baby.parent(), true);
        DataCacher.getInstance(this.mContext).save((DataItem) baby);
    }

    private synchronized HolyBaby pollBabe(String parent) {
        return (HolyBaby) getGradle(parent, false).poll();
    }

    private synchronized ConcurrentLinkedQueue<HolyBaby> getGradle(String parent) {
        return getGradle(parent, false);
    }

    private synchronized ConcurrentLinkedQueue<HolyBaby> getGradle(String parent, boolean updateForced) {
        WeakReference<ConcurrentLinkedQueue<HolyBaby>> babiesWeakRef;
        babiesWeakRef = (WeakReference) this.who2BabiesWeakRef.get(parent);
        if (babiesWeakRef == null || babiesWeakRef.get() == null || updateForced) {
            babiesWeakRef = new WeakReference(new ConcurrentLinkedQueue());
            this.who2BabiesWeakRef.put(parent, babiesWeakRef);
            extractAllBabies(parent);
        }
        return (ConcurrentLinkedQueue) babiesWeakRef.get();
    }

    private synchronized ConcurrentLinkedQueue<HolyBaby> extractAllBabies(String parent) {
        List<DataItem> holyBabyList = DataCacher.getInstance(this.mContext).queryAllHolyBaby(parent);
        List<DataItem> itemsToFilter = new ArrayList();
        for (DataItem item : holyBabyList) {
            try {
                Long.valueOf(item.getIdentifier());
            } catch (Exception e) {
                itemsToFilter.add(item);
                DataCacher.getInstance(this.mContext).delete(item.getIdentifier());
            }
        }
        holyBabyList.removeAll(itemsToFilter);
        updateBabyOutOrder(parent, holyBabyList);
        return (ConcurrentLinkedQueue) ((WeakReference) this.who2BabiesWeakRef.get(parent)).get();
    }

    private void updateBabyOutOrder(String parent, List<DataItem> originHolyBabyList) {
        Collections.sort(originHolyBabyList, new C10091());
        for (DataItem item : originHolyBabyList) {
            getGradle(parent).add(HolyBaby.from(item));
        }
    }

    public HolyBaby pollNextBabyAndSendToStage(String targetParent) {
        ConcurrentLinkedQueue<HolyBaby> holyBabies = getGradle(targetParent, true);
        if (holyBabies == null) {
            return null;
        }
        return (HolyBaby) holyBabies.poll();
    }

    public synchronized void kickToFly(TaskManager theGuardWithBabe, ProcessStateMonitor monitor, String target) {
        HolyBaby babyInState = pollNextBabyAndSendToStage(target);
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "BABE 下一个 ===> 下一个BABE 是： " + babyInState + " , for: " + target);
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "BABE 下一个 ===> 下一个BABE 是： " + babyInState + " , for: " + target);
        if (babyInState == null) {
            while (getGradle(target).peek() != null) {
                babyInState = pollBabe(target);
                if (babyInState != null) {
                    break;
                }
            }
            if (babyInState == null) {
                monitor.finishHolyWho(target);
                SyncProcessManager.getInstance().tellWith(target, (byte) 5, new int[0]);
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "================看上去已经没有任务了######！ , for: " + target);
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "================看上去已经没有任务了######！ , for: " + target);
                if (monitor.isAllHolyFinished()) {
                    monitor.onFinish(target, 0);
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "================所有已经完成了任务######！ , for: " + monitor.targetWho());
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "================所有已经完成了任务######！ , for: " + monitor.targetWho());
                } else {
                    String nextTarget = monitor.pollHolyParentTarget();
                    if (!TextUtils.isEmpty(nextTarget)) {
                        kickToFly(theGuardWithBabe, monitor, nextTarget);
                    }
                }
            } else {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "BABE 下一个 ===> 经过查询后，找到 下一个BABE 是： " + babyInState + " , for: " + target);
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "BABE 下一个 ===> 经过查询后，找到 下一个BABE 是： " + babyInState + " , for: " + target);
            }
        }
        coreWork(theGuardWithBabe, babyInState, babyInState.nextHomeWork(), monitor);
    }

    private void coreWork(TaskManager theGuardWithBabe, HolyBaby babyInstage, String nowHomeWork, ProcessStateMonitor monitor) {
        final String str = nowHomeWork;
        final HolyBaby holyBaby = babyInstage;
        final ProcessStateMonitor processStateMonitor = monitor;
        final TaskManager taskManager = theGuardWithBabe;
        theGuardWithBabe.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                DataItem realItem = DataCacher.getInstance(HolyBoard.this.mContext).query(str, 2);
                if (realItem == null) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "BABE CRY~~~~ 并没有找到 ==> REAL ITEM 并没有：　" + str + " , 问题严重了....");
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "BABE CRY~~~~ 并没有找到 ==> REAL ITEM 并没有：　" + str + " , 问题严重了....");
                    holyBaby.finishHomeWork(HolyBoard.this.mContext, str);
                    String next = holyBaby.nextHomeWork();
                    if (TextUtils.isEmpty(next)) {
                        HolyBoard.this.graduateTheChild(holyBaby);
                        if (TextUtils.isEmpty(holyBaby.parent())) {
                            Log.i("WH-SERIAL_MODE", "PARENT is NULL !!!!!!!!!!!!!!!!!!!!!!!!! === IGNORE " + holyBaby);
                        } else {
                            String nextParent = processStateMonitor.pollHolyParentTarget();
                            if (!TextUtils.isEmpty(nextParent)) {
                                HolyBoard.this.kickToFly(taskManager, processStateMonitor, nextParent);
                            }
                        }
                    } else {
                        HolyBoard.this.coreWork(taskManager, holyBaby, next, processStateMonitor);
                    }
                } else {
                    realItem.addFlags(16384);
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "REAL ITEM 找到了：　" + realItem);
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "REAL ITEM 找到了：　" + realItem);
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "顺序： " + realItem.getIdentifier() + " , for : " + holyBaby.parent() + ":" + holyBaby.getIdentifier() + ":" + processStateMonitor.name + ":" + processStateMonitor.hashCode());
                    serialData2Server(taskManager, realItem);
                    processStateMonitor.finishOneAssist2Cloud(realItem);
                }
                return null;
            }

            private void serialData2Server(TaskManager guarder, DataItem item) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "上传 : " + item.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "上传 : " + item.getIdentifier());
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "Before send data to server from Wear Componion By HTTP : " + item.toShortString());
                }
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "Before send data to server from Wear Componion By HTTP : " + item.toShortString());
                }
                handleRequestResult(guarder, WearHttpCompanion.getInstance().getHttpTransporter().request(HolyBoard.this.mContext, item, new HashMap()));
            }

            private void handleRequestResult(TaskManager guarder, DataItem itemResult) {
                if (itemResult == null) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "创建请求的时候发生错误，请检查。该次请求将被忽略！%%%%%%%%%%%%%% : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction());
                    }
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "创建请求的时候发生错误，请检查。该次请求将被忽略！%%%%%%%%%%%%%% : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction());
                    }
                    if (DataCacher.getInstance(HolyBoard.this.mContext).delete(itemResult.getIdentifier())) {
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            Log.i("WH-SERIAL_MODE", "从数据库中移除了如下请求item : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction());
                        }
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            SolidLogger.getInstance().with("WH-SERIAL_MODE", "从数据库中移除了如下请求item : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction());
                        }
                    } else {
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            Log.i("WH-SERIAL_MODE", "从数据库中移除了如下请求item : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction() + "失败！！！！！！！");
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", "从数据库中移除了如下请求item : targetWho : " + itemResult.getIdentifier() + " , owner: " + itemResult.getOwner() + " , action: " + itemResult.getAction() + "失败！！！！！！！");
                    }
                    SyncProcessManager.getInstance().tellWith(itemResult.trackWho(), (byte) 9, new int[0]);
                    return;
                }
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "r已返回， Wear Componion By HTTP : " + itemResult.toShortString());
                }
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "r已返回，After send data to server from Wear Componion By HTTP : " + itemResult.toShortString());
                }
                handleResultCodeInternal(guarder, itemResult, holyBaby, processStateMonitor);
            }

            private void handleResultCodeInternal(TaskManager gurader, DataItem itemResult, final HolyBaby babyInStage, final ProcessStateMonitor monitor) {
                if (itemResult.getCode() == 200) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            Log.i("WH-SERIAL_MODE", "HTTP 请求  成功了 : " + itemResult.toShortString());
                            Log.i("WH-SERIAL_MODE", "HTTP 请求  成功x: " + itemResult.getIdentifier() + "  who: " + itemResult.trackWho());
                        }
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "HTTP 请求  成功了 : " + itemResult.toShortString());
                    }
                    SyncProcessManager.getInstance().tellWith(babyInStage.parent(), (byte) 9, new int[0]);
                    itemResult.setState(0);
                    itemResult.addFlags(4);
                    taskManager.next(WearHttpCompanion.getInstance().sendDataToApp(itemResult));
                    HolyBoard.this.finishSingleWorkAfterReqHttp(gurader, babyInStage, monitor, str);
                    return;
                }
                HolyBoard.this.wentBackToGradleHead(babyInStage);
                String url;
                if (GlobalDefine.isOverSea()) {
                    url = itemResult.getUrl();
                    if (!(url == null || url.contains("-us".toLowerCase()) || url.contains("-us".toUpperCase()))) {
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            Log.i("WH-SERIAL_MODE", "DROPPED !账号是海外的，但这个数据是国内的，但是url是 " + itemResult.getUrl() + " , " + itemResult.toShortString());
                        }
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "DROPPED !账号是海外的，但这个数据是国内的，但是url是 " + itemResult.getUrl() + " , " + itemResult.toShortString());
                        HolyBoard.this.finishSingleWorkAfterReqHttp(gurader, babyInStage, monitor, str);
                        return;
                    }
                }
                url = itemResult.getUrl();
                if (url != null && (url.contains("-us".toLowerCase()) || url.contains("-us".toUpperCase()))) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "DROPPED !账号是国内的，但这个数据是海外的，但是url是 " + itemResult.getUrl() + " , " + itemResult.toShortString());
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "DROPPED !账号是国内的，但这个数据是海外的，但是url是 " + itemResult.getUrl() + " , " + itemResult.toShortString());
                    HolyBoard.this.finishSingleWorkAfterReqHttp(gurader, babyInStage, monitor, str);
                    return;
                }
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "HTTP 请求并没有成功，需要重试 : " + itemResult.toShortString() + " code : " + itemResult.getCode());
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "HTTP 请求并没有成功，需要重试 : " + itemResult.toShortString() + " code : " + itemResult.getCode());
                taskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        monitor.onFinish(babyInStage.parent(), -1);
                        return null;
                    }
                }).execute();
            }
        }).execute();
    }

    private void finishSingleWorkAfterReqHttp(TaskManager gurader, HolyBaby babyInStage, final ProcessStateMonitor monitor, String nowHomeWork) {
        babyInStage.finishHomeWork(this.mContext, nowHomeWork);
        if (babyInStage.hasMoreHomeWork()) {
            coreWork(gurader, babyInStage, babyInStage.nextHomeWork(), monitor);
            return;
        }
        graduateTheChild(babyInStage);
        monitor.finishHolyWho(babyInStage.parent());
        String nextHoly = monitor.pollHolyParentTarget();
        if (TextUtils.isEmpty(nextHoly)) {
            gurader.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    if (monitor.isAllHolyFinished()) {
                        monitor.onFinish(monitor.targetWho(), 0);
                    }
                    return null;
                }
            }).execute();
        } else {
            kickToFly(gurader, monitor, nextHoly);
        }
    }

    private void graduateTheChild(HolyBaby child) {
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "************ Congratulate graduation for : " + child + "  *********** ");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "************ Congratulate graduation for : " + child + "  *********** ");
        if (DataCacher.getInstance(this.mContext).delete(child.getIdentifier())) {
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "************WOWOWOWO*********** ");
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "************WOWOWOWO*********** ");
            return;
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "*WARNING ==>>> 但是这个child并没有在数据库... : " + child + "  *********** ");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "*WARNING ==>>> 但是这个child并没有在数据库... : " + child + "  *********** ");
    }
}
