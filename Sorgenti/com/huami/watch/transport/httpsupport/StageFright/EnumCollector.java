package com.huami.watch.transport.httpsupport.StageFright;

import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.global.GlobalDataSyncKicker;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class EnumCollector {
    private static ConcurrentLinkedQueue<DataItem> sActionList;
    private static TaskManager sInnerCommandHandleTaskManager;
    private OnReportResultListener mData2LowerResultListener;
    private OnReportResultListener mData2UpperResultListener;
    private String name = "def";

    public interface OnReportResultListener {
        void onReportResult(int i);
    }

    class C10065 implements OnReportResultListener {
        C10065() {
        }

        public void onReportResult(int code) {
            if (code != 0) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "onMessageDispatchResult FAILED ! ");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "onMessageDispatchResult FAILED ! ");
            } else if (GlobalDefine.DEBUG_SERIAL_MODE) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "onMessageDispatchResult succeed ! ");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "onMessageDispatchResult succeed ! ");
            }
        }
    }

    class C10076 implements OnReportResultListener {
        C10076() {
        }

        public void onReportResult(int code) {
            if (code != 0) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "mData2UpperResultListener FAILED ! ");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "mData2UpperResultListener FAILED ! ");
            } else if (GlobalDefine.DEBUG_SERIAL_MODE) {
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "mData2UpperResultListener succeed ! ");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "mData2UpperResultListener succeed ! ");
            }
        }
    }

    protected abstract void data2Lower(DataItem dataItem, OnReportResultListener onReportResultListener);

    protected abstract void data2Upper(DataItem dataItem, OnReportResultListener onReportResultListener);

    protected abstract void dataLocal2Upper(DataItem dataItem);

    public void setName(String name) {
        this.name = name;
    }

    public EnumCollector() {
        if (sActionList == null) {
            sActionList = new ConcurrentLinkedQueue();
        }
        if (sInnerCommandHandleTaskManager == null) {
            sInnerCommandHandleTaskManager = new TaskManager("enum-collector");
        }
    }

    public void addActionAndExecute(DataItem item) {
        if (item == null) {
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "add action ：NULL !!!!!!! IGNORE");
            }
            SolidLogger.getInstance().with("WH-SRV", "add action ：NULL !!!!!!! IGNORE");
            return;
        }
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SERIAL_MODE", "add action ： " + item.getAction() + " , AT: ", new Throwable());
            Log.i("WH-SERIAL_MODE", "==> list is : \" + sActionList.toString()");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "add action ： " + item.getAction() + " , AT: ", new Throwable());
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "==> list is : \" + sActionList.toString()");
        if (!sActionList.contains(item)) {
            sActionList.add(item);
        }
        handleActionQueue();
    }

    private synchronized void handleActionQueue() {
        Iterator i$ = sActionList.iterator();
        while (i$.hasNext()) {
            DataItem item = (DataItem) i$.next();
            if (item != null) {
                sActionList.remove(item);
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-SERIAL_MODE", "'" + this.name + "' ==> handleActionQueue " + item.getIdentifier() + " --> to handleSingleAction");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + this.name + "' ==> handleActionQueue " + item.getIdentifier() + " --> to handleSingleAction");
                handleSingleAction(item);
            }
        }
        sInnerCommandHandleTaskManager.execute();
    }

    protected void onQueryCommandArrival(DataItem command) {
    }

    private void handleSingleAction(final DataItem item) {
        String action = item.getAction();
        if (TextUtils.equals(action, "_report_sync_list")) {
            sInnerCommandHandleTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "'" + EnumCollector.this.name + "' ==> CMD report upload: " + item + " ---> dataFromLower");
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + EnumCollector.this.name + "' ==> CMD report upload: " + item + " ---> dataFromLower");
                    EnumCollector.this.dataFromLower(item);
                    return null;
                }
            });
        } else if (TextUtils.equals(action, "_query_sync_list")) {
            sInnerCommandHandleTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    EnumCollector.this.onQueryCommandArrival(item);
                    String ver = item.getExtraValByKey("ver");
                    if (TextUtils.isEmpty(ver)) {
                        ver = "0";
                    }
                    GlobalDefine.CURR_REPORT_LIST_VER = Integer.valueOf(ver).intValue();
                    EnumCollector.this.dataFromUpper(item);
                    return null;
                }
            });
        } else if (TextUtils.equals(action, "_come_on")) {
            sInnerCommandHandleTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "'" + EnumCollector.this.name + "' ==> CMD TRIGGER POOR: " + item + " --ENERGY -- !");
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + EnumCollector.this.name + "' ==> CMD TRIGGER POOR: " + item + " --ENERGY -- !");
                    EnumCollector.this.triggerPoorEnergy(item);
                    return null;
                }
            });
        } else {
            onCustomCommand(item);
        }
    }

    protected void triggerPoorEnergy(DataItem item) {
        data2Lower(item, null);
    }

    protected void dataFromLower(final DataItem itemAction) {
        OnReportResultListener c10054;
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "'" + this.name + "' ==> dataFromLower " + itemAction.getIdentifier() + " --> to data2Upper");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + this.name + "' ==> dataFromLower " + itemAction.getIdentifier() + " --> to data2Upper");
        if (this.mData2UpperResultListener == null) {
            c10054 = new OnReportResultListener() {
                public void onReportResult(int code) {
                    if (code == 0) {
                        if (GlobalDefine.DEBUG_SERIAL_MODE) {
                            Log.i("WH-SERIAL_MODE", "EnumCollector-" + EnumCollector.this.name + "-" + itemAction.getIdentifier() + "' report to upper succeed ! " + itemAction);
                        }
                        SolidLogger.getInstance().with("WH-SERIAL_MODE", "EnumCollector-" + EnumCollector.this.name + "-" + itemAction.getIdentifier() + "' report to upper succeed ! " + itemAction);
                        return;
                    }
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "EnumCollector-" + EnumCollector.this.name + "-" + itemAction.getIdentifier() + "' report to upper FAILED ! " + itemAction);
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "EnumCollector-" + EnumCollector.this.name + "-" + itemAction.getIdentifier() + "' report to upper FAILED ! " + itemAction);
                }
            };
        } else {
            c10054 = this.mData2UpperResultListener;
        }
        data2Upper(itemAction, c10054);
    }

    protected void dataFromUpper(DataItem actionItem) {
        OnReportResultListener c10065;
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "'" + this.name + "' ==> dataFromUpper " + actionItem.getIdentifier() + " --> to data2Lower");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + this.name + "' ==> dataFromUpper " + actionItem.getIdentifier() + " --> to data2Lower");
        if (this.mData2LowerResultListener == null) {
            c10065 = new C10065();
        } else {
            c10065 = this.mData2LowerResultListener;
        }
        data2Lower(actionItem, c10065);
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "'" + this.name + "' ==> data2Lower " + actionItem.getIdentifier() + " --> to dataLocal2Upper");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + this.name + "' ==> data2Lower " + actionItem.getIdentifier() + " --> to dataLocal2Upper");
        String[] target = actionItem.trackTargetPackageAndAction();
        DataItem localDataItem = GlobalDataSyncKicker.getInstance().genInternalEnumCollectAction(target[0], target[1], "_report_sync_list");
        localDataItem.setIdentifier(actionItem.getIdentifier());
        localDataItem.setExtraData(actionItem.getExtraData().toString());
        dataLocal2Upper(localDataItem);
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            Log.i("WH-SERIAL_MODE", "'" + this.name + "' ==> dataLocal2Upper " + actionItem.getIdentifier() + " --> to data2Upper");
        }
        SolidLogger.getInstance().with("WH-SERIAL_MODE", "'" + this.name + "' ==> dataLocal2Upper " + actionItem.getIdentifier() + " --> to data2Upper");
        if (this.mData2UpperResultListener == null) {
            c10065 = new C10076();
        } else {
            c10065 = this.mData2UpperResultListener;
        }
        data2Upper(localDataItem, c10065);
    }

    protected void onCustomCommand(DataItem customCommand) {
    }
}
