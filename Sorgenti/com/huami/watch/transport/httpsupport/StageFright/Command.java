package com.huami.watch.transport.httpsupport.StageFright;

import java.util.HashSet;

public final class Command {
    public static final HashSet<String> INTERNAL_COMMAND = new HashSet();

    static {
        INTERNAL_COMMAND.add("sync-internal-for-assist");
        INTERNAL_COMMAND.add("tell-internal-for-assist");
        INTERNAL_COMMAND.add("_report_sync_list");
        INTERNAL_COMMAND.add("_query_sync_list");
        INTERNAL_COMMAND.add("_come_on");
        INTERNAL_COMMAND.add("_client_trigger_report");
        INTERNAL_COMMAND.add("_arrival_token");
        INTERNAL_COMMAND.add("_query_token");
    }

    public static synchronized boolean isInternal(String command) {
        boolean contains;
        synchronized (Command.class) {
            contains = INTERNAL_COMMAND.contains(command);
        }
        return contains;
    }
}
