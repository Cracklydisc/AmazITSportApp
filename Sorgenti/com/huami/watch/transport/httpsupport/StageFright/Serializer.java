package com.huami.watch.transport.httpsupport.StageFright;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.control.assist.WearHttpCompanion;
import com.huami.watch.transport.httpsupport.global.GlobalDataSyncKicker;
import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Serializer {
    private static volatile Serializer INSTANCE;
    private static final HashSet<String> INTERESTED_NODE = new HashSet();
    private static ConcurrentHashMap<String, HashSet<ReportListInfo>> mId2CurrentReportListSetMap;
    private static TaskManager sNoSerialMachineTaskManager = new TaskManager("p-machine");
    private static TaskManager sSerialMachineTaskManager = new TaskManager("s-machine");
    private boolean busy = false;
    private Context mContext = null;

    static {
        INTERESTED_NODE.add("com.huami.watch.wearservices");
        INTERESTED_NODE.add("com.huami.watch.hmwatchmanager");
        INTERESTED_NODE.add("com.huami.watch.test");
    }

    public static synchronized Serializer getInstance(Context context) {
        Serializer serializer;
        synchronized (Serializer.class) {
            if (INSTANCE == null) {
                synchronized (Serializer.class) {
                    if (INSTANCE == null) {
                        INSTANCE = new Serializer(context);
                    }
                }
            }
            serializer = INSTANCE;
        }
        return serializer;
    }

    private Serializer(Context context) {
        this.mContext = context;
    }

    public synchronized void onEventProcessReportItem(final ProcessStateMonitor monitor, final DataItem reportItem) {
        sSerialMachineTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                Serializer.this.busy = true;
                ReportListInfo report = Serializer.this.drawReportFrom(reportItem);
                if (report == null) {
                    Serializer.this.busy = false;
                } else {
                    if (Serializer.mId2CurrentReportListSetMap == null) {
                        Serializer.mId2CurrentReportListSetMap = new ConcurrentHashMap();
                    }
                    HashSet<ReportListInfo> reportListInfos = (HashSet) Serializer.mId2CurrentReportListSetMap.get(reportItem.getIdentifier());
                    if (reportListInfos == null) {
                        ConcurrentHashMap access$200 = Serializer.mId2CurrentReportListSetMap;
                        String identifier = reportItem.getIdentifier();
                        reportListInfos = new HashSet();
                        access$200.put(identifier, reportListInfos);
                    }
                    reportListInfos.add(report);
                    if (TextUtils.isEmpty(reportItem.getExtraValByKey("keyf"))) {
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "arrival ，收到了非主键 ： " + reportItem);
                        }
                        SolidLogger.getInstance().with("WH-SRV", "arrival ，收到了非主键 ： " + reportItem);
                    } else {
                        SyncProcessManager.getInstance().tellWith(reportItem.trackWho(), (byte) 2, new int[0]);
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "arrival ，收到了\"主键\" ： " + reportItem);
                        }
                        SolidLogger.getInstance().with("WH-SRV", "arrival ，收到了\"主键\" ： " + reportItem);
                        if (Serializer.this.checkIfDataNeedSyncFromLower(reportListInfos)) {
                            SyncProcessManager.getInstance().tellWith(reportItem.trackWho(), (byte) 6, new int[0]);
                            if (GlobalDefine.DEBUG_SERVER) {
                                Log.i("WH-SRV", "KEYF说#####需要触发数据上报的action，因为有数据存在");
                            }
                            SolidLogger.getInstance().with("WH-SRV", "KEYF说#####需要触发数据上报的action，因为有数据存在");
                            Serializer.this.kickUploadPoorMachine(monitor, reportListInfos);
                            Serializer.this.busy = true;
                        } else {
                            Serializer.this.startUpload(monitor, reportItem.trackWho());
                        }
                    }
                }
                return null;
            }
        }).execute();
    }

    public synchronized void onEventOfDanceWith(ProcessStateMonitor monitor) {
        startUpload(monitor, monitor.trackWho());
    }

    private void startUpload(ProcessStateMonitor monitor, String who) {
        if (mId2CurrentReportListSetMap == null) {
            mId2CurrentReportListSetMap = new ConcurrentHashMap();
        }
        if (monitor.getReportId() != null) {
            int count = letsDance(monitor, wholeFuseAndSort((HashSet) mId2CurrentReportListSetMap.get(monitor.getReportId())));
            if (count > 0) {
                SyncProcessManager.getInstance().tellWith(who, (byte) 8, count);
                SyncProcessManager.getInstance().tellWith(who, (byte) 3, count);
                monitor.setSumOfNeedAssist2Cloud(count);
                monitor.onUpdateAssist2CloudState((byte) 5);
            } else if (count == 0) {
                SyncProcessManager.getInstance().tellWith(who, (byte) 5, new int[0]);
                monitor.onFinish(who, 0);
                AnalysisStage.getInstance().removeReportArrivalListener(monitor);
            }
            mId2CurrentReportListSetMap.remove(monitor.getReportId());
        }
    }

    public synchronized ConcurrentLinkedQueue<ConcurrentHashMap<String, List<DataItem>>> wholeFuseAndSort(HashSet<ReportListInfo> infoList) {
        ConcurrentLinkedQueue<ConcurrentHashMap<String, List<DataItem>>> concurrentLinkedQueue;
        if (infoList == null) {
            concurrentLinkedQueue = null;
        } else {
            concurrentLinkedQueue = new ConcurrentLinkedQueue();
            Iterator it = infoList.iterator();
            while (it.hasNext()) {
                ReportListInfo info = (ReportListInfo) it.next();
                if (info != null) {
                    for (ConcurrentLinkedQueue<DataItem> item : info.getThings().values()) {
                        if (item != null) {
                            ConcurrentHashMap<String, List<DataItem>> who2ThingsItemMap = new ConcurrentHashMap();
                            Iterator i$ = item.iterator();
                            while (i$.hasNext()) {
                                DataItem cell = (DataItem) i$.next();
                                if (cell != null) {
                                    String who = cell.getOwner() + "#" + cell.getAction();
                                    if (who2ThingsItemMap.get(who) == null) {
                                        List<DataItem> node = new ArrayList();
                                        node.add(cell);
                                        who2ThingsItemMap.put(who, node);
                                    } else {
                                        ((List) who2ThingsItemMap.get(who)).add(cell);
                                    }
                                }
                            }
                            concurrentLinkedQueue.add(who2ThingsItemMap);
                        }
                    }
                    continue;
                }
            }
        }
        return concurrentLinkedQueue;
    }

    private ReportListInfo drawReportFrom(DataItem item) {
        if (item == null) {
            return null;
        }
        return ReportListInfo.from(item.getExtraValByKey("report-list"), !TextUtils.isEmpty(item.getExtraValByKey("keyf")));
    }

    private int letsDance(ProcessStateMonitor monitor, ConcurrentLinkedQueue<ConcurrentHashMap<String, List<DataItem>>> results) {
        ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> sWho2UploadSerial = new ConcurrentHashMap();
        ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> sWho2UploadNOSerial = new ConcurrentHashMap();
        int countAll = 0;
        if (results != null) {
            Iterator it = results.iterator();
            while (it.hasNext()) {
                ConcurrentHashMap<String, List<DataItem>> r = (ConcurrentHashMap) it.next();
                if (r != null) {
                    Set<String> whos = r.keySet();
                    String targetPkg = monitor.targetAtPackage();
                    HashSet<String> targetActions = monitor.extractTargetActions();
                    boolean inAllAction = TextUtils.equals(monitor.targetAtAction(), "*");
                    boolean inAllPackage = TextUtils.equals(targetPkg, "*");
                    for (String who : whos) {
                        Log.i("WH-SERIAL_MODE", "===> 这个item action的list: " + who + " , 即将被处理.");
                        for (DataItem item : (List) r.get(who)) {
                            if (item != null) {
                                String pkg = item.getOwner();
                                String action = item.getAction();
                                if (!TextUtils.equals(pkg, targetPkg) && !inAllPackage) {
                                    Log.i("WH-SERIAL_MODE", "letsDance ===> 扔掉：" + item.toShortString() + " , for ： " + monitor.trackWho());
                                } else if (!targetActions.contains(action) && !inAllAction) {
                                    Log.i("WH-SERIAL_MODE", "letsDance ===> xx扔掉：" + item.toShortString() + " , for ： " + monitor.trackWho());
                                } else if (ReportListInfo.isItemNeedSerial(item)) {
                                    String id = item.getIdentifier().replace(".", "");
                                    countAll++;
                                    ConcurrentLinkedQueue<String> specifiedSerialWhoItems = (ConcurrentLinkedQueue) sWho2UploadSerial.get(who);
                                    if (specifiedSerialWhoItems == null) {
                                        specifiedSerialWhoItems = new ConcurrentLinkedQueue();
                                        sWho2UploadSerial.put(who, specifiedSerialWhoItems);
                                    }
                                    specifiedSerialWhoItems.add(id);
                                } else {
                                    countAll++;
                                    ConcurrentLinkedQueue<String> specifiedNoSerialWhoItems = (ConcurrentLinkedQueue) sWho2UploadNOSerial.get(who);
                                    if (specifiedNoSerialWhoItems == null) {
                                        specifiedNoSerialWhoItems = new ConcurrentLinkedQueue();
                                        sWho2UploadNOSerial.put(who, specifiedNoSerialWhoItems);
                                    }
                                    specifiedNoSerialWhoItems.add(item.getIdentifier());
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!(sWho2UploadSerial.isEmpty() || sWho2UploadSerial.keySet().isEmpty())) {
            kickUploadSerialMachine(monitor, sWho2UploadSerial);
        }
        if (!(sWho2UploadNOSerial.isEmpty() || sWho2UploadNOSerial.keySet().isEmpty())) {
            kickUploadNoSerialMachine(monitor, sWho2UploadNOSerial);
        }
        return countAll;
    }

    private void kickUploadSerialMachine(final ProcessStateMonitor monitor, final ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> sWho2UploadSerial) {
        sSerialMachineTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                HashSet<HolyBaby> babies = HolyBaby.zygoteAddAndFillHomeWorkAndKickToFly(Serializer.this.mContext, monitor, sWho2UploadSerial, Serializer.sSerialMachineTaskManager);
                if (babies == null || babies.isEmpty()) {
                    monitor.onUpdateAssist2CloudState((byte) 2);
                    sWho2UploadSerial.clear();
                } else {
                    monitor.onUpdateAssist2CloudState((byte) 2);
                    sWho2UploadSerial.clear();
                }
                return null;
            }
        }).execute();
    }

    private void kickUploadNoSerialMachine(ProcessStateMonitor monitor, ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> sWho2UploadNOSerial) {
        for (final String who : sWho2UploadNOSerial.keySet()) {
            final ConcurrentLinkedQueue<String> idList = (ConcurrentLinkedQueue) sWho2UploadNOSerial.get(who);
            monitor.appendSumOfNeedAssist2Cloud(idList.size());
            Iterator i$ = idList.iterator();
            while (i$.hasNext()) {
                final String _id = (String) i$.next();
                final ProcessStateMonitor processStateMonitor = monitor;
                sNoSerialMachineTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        DataItem realItem = DataCacher.getInstance(Serializer.this.mContext).query(_id);
                        if (realItem == null) {
                            Log.i("WH-SERIAL_MODE", "并没有找到 ==> REAL ITEM 并没有：　" + _id + " , 问题严重但还好是并发传....");
                            SyncProcessManager.getInstance().tellWith(who, (byte) 9, new int[0]);
                            processStateMonitor.finishOneAssist2Cloud(realItem);
                        } else {
                            Log.i("WH-SERIAL_MODE", "REAL ITEM 找到了：　" + realItem);
                            relaxData2Server(realItem);
                            processStateMonitor.finishOneAssist2Cloud(realItem);
                        }
                        return null;
                    }

                    private void relaxData2Server(DataItem realItem) {
                        idList.poll();
                        realItem.addFlags(4);
                        WearHttpCompanion.getInstance().sendDataToServer(realItem);
                    }
                });
            }
        }
        sNoSerialMachineTaskManager.execute();
    }

    private void kickUploadPoorMachine(ProcessStateMonitor monitor, HashSet<ReportListInfo> currReportSet) {
        ConcurrentHashMap<String, HashSet<String>> who2Ids = toItemIdSet(currReportSet);
        if (who2Ids == null || who2Ids.isEmpty()) {
            startUpload(monitor, monitor.trackWho());
            return;
        }
        Set<String> keys = who2Ids.keySet();
        HashSet<String> thisIds = new HashSet();
        for (String who : keys) {
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "kickUploadPoorMachine ：" + monitor.trackWho() + " hash:" + monitor.hashCode());
            }
            thisIds.addAll((Collection) who2Ids.get(who));
        }
        int total = thisIds.size();
        if (monitor != null) {
            monitor.appendSumOfNeedRom2Assist(total);
            monitor.startWaitForDataItems(thisIds);
            monitor.onUpdateRom2AssistState((byte) 1);
        }
        WearHttpCompanion.getInstance().getCollector().addActionAndExecute(GlobalDataSyncKicker.getInstance().genInternalSyncAction(null, "_come_on"));
    }

    private ConcurrentHashMap<String, HashSet<String>> toItemIdSet(HashSet<ReportListInfo> currReportSet) {
        ConcurrentHashMap<String, HashSet<String>> res = new ConcurrentHashMap();
        int sum = 0;
        Iterator it = currReportSet.iterator();
        while (it.hasNext()) {
            ReportListInfo info = (ReportListInfo) it.next();
            for (Integer i : info.getWhomMapThings().keySet()) {
                if (i.intValue() != 2) {
                    Iterator it2 = ((HashSet) info.getWhomMapThings().get(i)).iterator();
                    while (it2.hasNext()) {
                        String who = (String) it2.next();
                        HashSet<String> currList = (HashSet) res.get(who);
                        if (currList == null) {
                            currList = new HashSet();
                            res.put(who, currList);
                        }
                        Iterator i$ = ((ConcurrentLinkedQueue) info.getThings().get(who)).iterator();
                        while (i$.hasNext()) {
                            String id = ((DataItem) i$.next()).getIdentifier().replace(".", "");
                            if (!currList.contains(id)) {
                                sum++;
                                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                                    Log.i("WH-SERIAL_MODE", "arrival --> add targetWho to WAIT: " + who + " : " + id + " 当前数目：" + sum);
                                }
                            }
                            currList.add(id);
                        }
                    }
                }
            }
        }
        if (GlobalDefine.DEBUG_SERIAL_MODE) {
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "toItemIdSet : " + res);
            }
            SolidLogger.getInstance().with("WH-SRV", "toItemIdSet : " + res);
        }
        return res;
    }

    private boolean checkIfDataNeedSyncFromLower(HashSet<ReportListInfo> reports) {
        Iterator i$ = reports.iterator();
        while (i$.hasNext()) {
            ReportListInfo info = (ReportListInfo) i$.next();
            if (info.isKey && ReportListInfo.hasData(info)) {
                return true;
            }
        }
        return false;
    }
}
