package com.huami.watch.transport.httpsupport.control.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector.OnReportResultListener;
import com.huami.watch.transport.httpsupport.StageFright.ReportListInfo;
import com.huami.watch.transport.httpsupport.Utils;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.global.GlobalDataSyncKicker;
import com.huami.watch.transport.httpsupport.global.SyncNodeSwitcher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import com.huami.watch.transport.httpsupport.translogutils.DataParser;
import com.huami.watch.transport.httpsupport.translogutils.TransLogs;
import com.huami.watch.transport.httpsupport.transporter.blt.BltORMM;
import com.huami.watch.transport.httpsupport.transporter.blt.BltTransporter;
import com.huami.watch.transport.httpsupport.transporter.blt.BltTransporter.IResultReceiver;
import com.huami.watch.transport.httpsupport.transporter.server.DuplexDataExchangeService;
import com.huami.watch.transport.httpsupport.transporter.server.DuplexDataExchangeService.Exchangee;
import java.io.File;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class HttpTransportManager {
    private static Method bleTransContainMethod;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static HttpTransportManager sInstance = null;
    private Task coreTaskOfProcessCachedRequestWhilePhoneAvalible = null;
    private BltTransporter mBltTranser;
    private Context mContext;
    private TaskManager mData2AppClientOverMSGTaskManager;
    private TaskManager mData2AssistOverBltTaskManager;
    private DataCacher mDataCacher;
    private EnumCollector mEnumCollector;
    private Exchangee mExchangeeForClient;
    private ConcurrentLinkedQueue<String> sendingItemalreadyList;

    class C10442 extends BltTransporter {
        C10442() {
        }

        protected void plainItemDataIn(DataItem dataItem) {
            if (HttpTransportManager.this.mBltTranser.hasMark(dataItem)) {
                HttpTransportManager.this.mBltTranser.removeMark(dataItem);
                TransLogs.m11i(new DataParser(dataItem, 2).bleReceiveResText());
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "===>>> 助手传来了数据: " + dataItem.getIdentifier());
                }
                SolidLogger.getInstance().with("WH-SRV", "===>>> 助手传来了数据: " + dataItem.getIdentifier());
                Log.i("WH-SRV", "===>>> 助手传来了数据: " + dataItem.toShortString());
                HttpTransportManager.this.handleDataFromRemote(dataItem);
            }
        }

        protected void handleInnerCommand(DataItem item) {
            super.handleInnerCommand(item);
            if (HttpTransportManager.this.mEnumCollector != null) {
                HttpTransportManager.this.mEnumCollector.addActionAndExecute(item);
            }
        }
    }

    class C10494 extends EnumCollector {
        private ConcurrentHashMap<String, Local2Upper> mId2UpperTaskMap = new ConcurrentHashMap();

        class Local2Upper implements Runnable {
            DataItem targetItem;

            Local2Upper(DataItem item) {
                this.targetItem = item;
            }

            public void run() {
                HttpTransportManager.this.mData2AssistOverBltTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        C10494.this.mId2UpperTaskMap.remove(Local2Upper.this.targetItem.getIdentifier());
                        C10494.this.realReport2Upper(Local2Upper.this.targetItem);
                        return null;
                    }
                }).execute();
                if (this.targetItem != null) {
                }
            }
        }

        C10494() {
        }

        protected void dataFromUpper(DataItem actionItem) {
            if (GlobalDefine.CURR_REPORT_LIST_VER == 1) {
                addActionAndExecute(GlobalDataSyncKicker.getInstance().genInternalSyncAction("*", "sync-internal-for-assist"));
            }
            super.dataFromUpper(actionItem);
        }

        protected void data2Lower(DataItem item, OnReportResultListener l) {
            if (HttpTransportManager.this.mExchangeeForClient.hasClient()) {
                boolean res = HttpTransportManager.this.mExchangeeForClient.dispatchToAllClient(item);
                if (l != null) {
                    l.onReportResult(res ? 0 : -1);
                    return;
                }
                return;
            }
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "没有client ---> " + item.getOwner() + " , " + item.getAction());
            }
            SolidLogger.getInstance().with("WH-SRV", "没有client ---> " + item.getOwner() + " , " + item.getAction());
        }

        protected void data2Upper(DataItem item, final OnReportResultListener l) {
            if (HttpTransportManager.this.mBltTranser == null && item != null) {
                return;
            }
            if (HttpTransportManager.this.mContext == null || !TextUtils.equals(item.getOwner(), HttpTransportManager.this.mContext.getPackageName())) {
                HttpTransportManager.this.mBltTranser.dataOut(item, new IResultReceiver() {
                    public void onResult(int resCode) {
                        if (l != null) {
                            l.onReportResult(resCode);
                        }
                    }
                });
            } else if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "自己不再这里上传: " + item.getOwner() + " , id: " + item.getIdentifier());
            }
        }

        private void real2Upper(DataItem item, final OnReportResultListener l) {
            if (HttpTransportManager.this.mBltTranser != null || item == null) {
                HttpTransportManager.this.mBltTranser.dataOut(item, new IResultReceiver() {
                    public void onResult(int resCode) {
                        if (l != null) {
                            l.onReportResult(resCode);
                        }
                    }
                });
            }
        }

        protected void dataLocal2Upper(DataItem item) {
            String id = item.getIdentifier();
            Local2Upper task = (Local2Upper) this.mId2UpperTaskMap.get(id);
            if (task == null) {
                ConcurrentHashMap concurrentHashMap = this.mId2UpperTaskMap;
                task = new Local2Upper(item);
                concurrentHashMap.put(id, task);
            }
            HttpTransportManager.sHandler.removeCallbacks(task);
            HttpTransportManager.sHandler.postDelayed(task, 3280);
        }

        private void realReport2Upper(DataItem item) {
            DataCacher cache = DataCacher.getInstance(HttpTransportManager.this.mContext);
            ReportListInfo infoList = new ReportListInfo(GlobalDefine.CURR_REPORT_LIST_VER);
            List<DataItem> list1 = cache.query(1, 0);
            if (list1 != null) {
                String targetPackage = item.trackTargetPackageAndAction()[0];
                HashSet<String> actions = item.trackTargetActionSet();
                for (DataItem i : list1) {
                    if (TextUtils.equals("*", targetPackage) || GlobalDefine.CURR_REPORT_LIST_VER == 0) {
                        infoList.addItem(Integer.valueOf(1), i);
                    } else if (TextUtils.equals(targetPackage, i.getOwner()) && (actions.contains(i.getAction()) || actions.contains("*"))) {
                        infoList.addItem(Integer.valueOf(1), i);
                    }
                }
            }
            item.setOwner(HttpTransportManager.this.mContext.getPackageName());
            item.addExtraPair("report-list", infoList.flatten());
            item.addExtraPair("keyf", "1");
            real2Upper(item, null);
        }

        protected void triggerPoorEnergy(DataItem item) {
            HttpTransportManager.this.tryProcessCachedRequestWhilePhoneAvalible(HttpTransportManager.this.mContext);
            HttpTransportManager.this.mExchangeeForClient.dispatchToAllClient(item);
        }

        protected void onCustomCommand(DataItem customCommand) {
            String action = customCommand.getAction();
            if (TextUtils.equals(action, "sync-internal-for-assist")) {
                if (TextUtils.isEmpty(customCommand.getExtraValByKey("new-sync"))) {
                    Log.i("WH-SERIAL_MODE", "---xx----------感觉对方是个“旧的”助手");
                    GlobalDefine.saveSyncSupport(false);
                } else {
                    Log.i("WH-SERIAL_MODE", "---xx----------感觉对方是个“新的”助手");
                    GlobalDefine.saveSyncSupport(true);
                }
                data2Lower(customCommand, null);
            } else if (TextUtils.equals(action, "tell-internal-for-assist")) {
                data2Upper(customCommand, null);
            } else if (TextUtils.equals(action, "_client_trigger_report")) {
                String[] target = customCommand.trackTargetPackageAndAction();
                HttpTransportManager.this.triggerOnceReportTypedUploadCache(target[0] == null ? "没有target" : target[0], target[1] == null ? "没有target" : target[1]);
            } else if (TextUtils.equals(action, "_query_token")) {
                data2Upper(customCommand, null);
            } else if (TextUtils.equals(action, "_arrival_token")) {
                data2Lower(customCommand, null);
            }
        }
    }

    private class ReportTriggerForCacheRunnable implements Runnable {
        public String targetAction;
        public String targetPkg;

        ReportTriggerForCacheRunnable(String pkg, String action) {
            this.targetPkg = pkg;
            this.targetAction = action;
        }

        public void run() {
            if (HttpTransportManager.this.mContext != null && HttpTransportManager.this.mEnumCollector != null) {
                DataItem actionReport = GlobalDataSyncKicker.getInstance().genInternalEnumCollectAction(this.targetPkg, this.targetAction, "_query_sync_list");
                actionReport.addExtraPair("ver", "1");
                actionReport.addExtraPair("self", "1");
                HttpTransportManager.this.mEnumCollector.addActionAndExecute(actionReport);
            }
        }
    }

    public static void doInit(Context context) {
        if (sInstance == null) {
            synchronized (HttpTransportManager.class) {
                if (sInstance == null) {
                    sInstance = new HttpTransportManager(context);
                }
            }
        }
    }

    private HttpTransportManager(Context context) {
        this.mContext = context.getApplicationContext();
        this.mExchangeeForClient = new Exchangee(this.mContext) {
            public void onDataFromClient(String item) {
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "\n这次请求的数据是onDataFromClient ---> " + item + " \n=========\n");
                }
                SolidLogger.getInstance().with("WH-SRV", "\n这次请求的数据是onDataFromClient ---> " + item + " \n=========\n");
                if (GlobalDefine.DEBUG_SERVER) {
                    String data = item;
                    try {
                        data = DataUtils.drillUnzipData(DataItem.from(item));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i("WH-SRV", "数据 ==> Data 区域: " + data);
                }
                DataItem bean = DataItem.from(item);
                if (!SyncNodeSwitcher.self().mayGo(0)) {
                    SolidLogger.getInstance().with("WH-SRV", "##$@@@@@@@@@@@@@@@@@ WIFI TRANS 正在进行，透传回避！！ for : " + bean.toShortString());
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.w("WH-SRV", "##$@@@@@@@@@@@@@@@@@ WIFI TRANS 正在进行，透传回避！！ for : " + bean.toShortString());
                    }
                    bean.setState(9);
                    DataCacher.getInstance(HttpTransportManager.this.mContext).save(bean);
                } else if (Command.isInternal(bean.getAction())) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-SERIAL_MODE", "onDataFromClient --> " + bean.getAction());
                    }
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "onDataFromClient --> " + bean.getAction());
                    HttpTransportManager.this.mEnumCollector.addActionAndExecute(bean);
                } else {
                    SolidLogger.getInstance().with("WH-SERIAL_MODE", "NEW Send To Assist === > " + bean.toShortString() + " ， 助手支持串行情况：" + GlobalDefine.supportNewSync());
                    SolidLogger.getInstance().with("WH-SRV", "数据 ==> Data 区域: " + DataUtils.drillUnzipData(DataItem.from(item)));
                    if (!TextUtils.isEmpty(bean.getExtraValByKey("d-path"))) {
                        String path = bean.getExtraValByKey("d-path");
                        if (GlobalDefine.DEBUG_LD) {
                            SolidLogger.getInstance().with("WH-LD", "@@@@  需要填充 data : " + bean.getIdentifier() + " <> " + path);
                        }
                        if (GlobalDefine.DEBUG_LD) {
                            Log.i("WH-LD", "@@@@@  需要填充 data : " + bean.getIdentifier() + " <> " + path);
                        }
                        HttpTransportManager.this.fillDataByFile(path, bean);
                    }
                    bean.setState(1);
                    DataCacher.getInstance(HttpTransportManager.this.mContext).save(bean);
                    if (TextUtils.equals(bean.getExtraValByKey("test"), "1")) {
                        if (GlobalDefine.DEBUG_LD) {
                            Log.i("WH-LD", "@@@@@  收到测试数据 ： " + bean.toShortString() + " 只缓存！");
                        }
                        realSendData2Assist(bean, true);
                    }
                }
            }

            public void realSendData2Assist(final DataItem bean, final boolean needLooksideDB) {
                HttpTransportManager.this.mData2AssistOverBltTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation taskOperation) {
                        HttpTransportManager.this.sendDataItemToAssist(bean, needLooksideDB);
                        return null;
                    }
                }).execute();
            }

            public void query(String itemDesc) {
            }
        };
        this.mDataCacher = DataCacher.getInstance(context);
        this.mData2AppClientOverMSGTaskManager = new TaskManager("Data2AppClientOverMSGTaskManager");
        this.mData2AssistOverBltTaskManager = new TaskManager("Data2AssistOverBltTaskManager");
        DuplexDataExchangeService.setExchangee(this.mExchangeeForClient);
        if (this.mBltTranser == null) {
            this.mBltTranser = new C10442();
            this.mBltTranser.start(this.mContext);
        }
        initEnumCollector();
        startService(this.mContext);
    }

    private void fillDataByFile(final String path, DataItem bean) {
        String data = Utils.readFileToString(path);
        bean.removeExtraPairByKey("d-path");
        bean.setData(data);
        this.mData2AssistOverBltTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                new File(path).delete();
                return null;
            }
        });
    }

    private void initEnumCollector() {
        if (this.mEnumCollector == null) {
            this.mEnumCollector = new C10494();
            this.mEnumCollector.setName("EC-HttpTransportManager");
        }
    }

    public void triggerOnceReportTypedUploadCache(final String pkg, final String targetAction) {
        this.mData2AssistOverBltTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                new ReportTriggerForCacheRunnable(pkg, targetAction).run();
                return null;
            }
        }).execute();
    }

    private void startService(Context context) {
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "\n\t====================\tWEAR HTTP SERVICE START\t========================\n");
        }
        SolidLogger.getInstance().with("WH-SRV", "\n\t====================\tWEAR HTTP SERVICE START\t========================\n");
        try {
            context.startService(new Intent(context, DuplexDataExchangeService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static HttpTransportManager getInstance(Context context) {
        doInit(context);
        return sInstance;
    }

    private synchronized void process(DataItem item) {
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "PROCESS DATA NOW ---> " + item.getOwner() + " , " + item.getAction());
            String sub = item.getData();
            int len = sub.length();
            if (!TextUtils.isEmpty(sub)) {
                sub = sub.substring(0, Math.min(200, len));
            }
            Log.i("WH-SRV", "SHOW SOME DATA ? ------> \n" + sub + " , " + item.getExtraData() + " \n ==============\n\n");
        }
        SolidLogger.getInstance().with("WH-SRV", "PROCESS DATA NOW ---> " + item.getOwner() + " , " + item.getAction());
        switch (item.getState()) {
            case 0:
                if (!(this.mContext == null || item == null)) {
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", "尝试将数据返回给client ---> " + item.getIdentifier() + " , " + item.getAction());
                    }
                    SolidLogger.getInstance().with("WH-SRV", "尝试将数据返回给client ---> " + item.getIdentifier() + " , " + item.getAction());
                    if (!this.mExchangeeForClient.sendToClient(item)) {
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "客户端不存在，将缓存 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                        }
                        SolidLogger.getInstance().with("WH-SRV", "客户端不存在，将缓存 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                        item.setState(2);
                        this.mDataCacher.save(item);
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "缓存完成 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                        }
                        SolidLogger.getInstance().with("WH-SRV", "缓存完成 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                        break;
                    }
                    if (GlobalDefine.DEBUG_SERVER) {
                        Log.i("WH-SRV", "回传成功 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                    }
                    SolidLogger.getInstance().with("WH-SRV", "回传成功 ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                    break;
                }
            default:
                if (GlobalDefine.DEBUG_SERVER) {
                    Log.i("WH-SRV", "不认识的STATE ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                }
                SolidLogger.getInstance().with("WH-SRV", "不认识的STATE ---> " + item.getOwner() + " , " + item.getAction() + " , state: " + item.getState());
                break;
        }
    }

    private void sendByBlt(DataItem item, IResultReceiver resListener) {
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "Will send ---> " + item.getOwner() + " , " + item.getAction() + " , to 助手.");
        }
        SolidLogger.getInstance().with("WH-SRV", "Will send ---> " + item.getOwner() + " , " + item.getAction() + " , to 助手.");
        if (this.mBltTranser.check()) {
            this.mBltTranser.dataOut(item, resListener);
            if (GlobalDefine.DEBUG_SERVER) {
                Log.i("WH-SRV", "Data Sent Result Code ---> " + item.getOwner() + " , " + item.getAction() + " , to Assistant. ");
            }
            SolidLogger.getInstance().with("WH-SRV", "Data Sent Result Code ---> " + item.getOwner() + " , " + item.getAction() + " , to Assistant. ");
            return;
        }
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "手表未连接，数据将被缓存 , will CACHE ---> " + item + " \n\n=========\n\n\n");
        }
        SolidLogger.getInstance().with("WH-SRV", "手表未连接，数据将被缓存 , will ######CACHE######  ---> " + item + " \n");
        item.setState(1);
        this.mDataCacher.save(item);
    }

    private synchronized void handleDataFromRemote(final DataItem dataItem) {
        if (Command.isInternal(dataItem.getAction())) {
            Log.i("WH-SERIAL_MODE", "Data from Service WITH ");
        }
        if (GlobalDefine.DEBUG_SERVER) {
            Log.i("WH-SRV", "Will handleDataFromRemote ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , state : " + (dataItem.getState() == 0 ? "已经助手处理" : "未经助手处理过"));
        }
        SolidLogger.getInstance().with("WH-SRV", "Will handleDataFromRemote ---> " + dataItem.getOwner() + " , " + dataItem.getAction() + " , state : " + (dataItem.getState() == 0 ? "已经助手处理" : "未经助手处理过"));
        this.mData2AppClientOverMSGTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation operation) {
                HttpTransportManager.this.process(dataItem);
                return null;
            }
        }).execute();
    }

    private synchronized Runnable tryToSendDataItemToAssist(final DataItem bean, final boolean needLookAsideDB) {
        return new Runnable() {
            public void run() {
                String item;
                final DataParser dataParser = new DataParser(bean, 1);
                TransLogs.m11i(dataParser.getImportanceInfo());
                if (GlobalDefine.DEBUG_SERVER) {
                    item = bean.toString();
                    String str = "WH-SRV";
                    StringBuilder append = new StringBuilder().append("尝试将下面数据发送到手助端>>>>> ");
                    if (item.length() > 1000) {
                        item = item.substring(0, 1000);
                    }
                    Log.i(str, append.append(item).append(" ,是否大数据: ").append(bean.hasFlag(32768)).toString());
                }
                item = bean.toString();
                SolidLogger instance = SolidLogger.getInstance();
                String str2 = "WH-SRV";
                StringBuilder append2 = new StringBuilder().append("尝试将下面数据发送到手助端>>>>> ");
                if (item.length() > 1000) {
                    item = item.substring(0, 1000);
                }
                instance.with(str2, append2.append(item).toString());
                TransLogs.ii("***ble发送****" + bean.getIdentifier());
                HttpTransportManager.this.sendByBlt(bean, new IResultReceiver() {
                    public void onResult(int resCode) {
                        HttpTransportManager.this.removeItemToSendingList(bean.getIdentifier());
                        if (resCode == 0) {
                            BltORMM.getInstance(HttpTransportManager.this.mContext).mark(bean);
                            if (GlobalDefine.DEBUG_SERVER) {
                                Log.i("WH-SRV", "蓝牙传输成功 SUCCESS ---> " + bean + " \n=========\n\n\n");
                            }
                            SolidLogger.getInstance().with("WH-SRV", "蓝牙传输成功 SUCCESS ---> " + bean + " \n");
                            if (needLookAsideDB) {
                                boolean res = HttpTransportManager.this.mDataCacher.delete(bean.getIdentifier());
                                if (GlobalDefine.DEBUG_SERVER) {
                                    Log.i("WH-SRV", "从数据库删除完成 ---> " + bean.getIdentifier() + " , " + bean.getOwner() + " , 删除结果: " + res + " \n=========\n\n\n");
                                }
                                SolidLogger.getInstance().with("WH-SRV", "从数据库删除完成 ---> " + bean.getIdentifier() + " , " + bean.getOwner() + " , 删除结果: " + res + " \n=========\n\n\n");
                                TransLogs.m11i(dataParser.getCacheDeleteStateText(res));
                            }
                            if (HttpTransportManager.this.mBltTranser == null) {
                                return;
                            }
                            if (!BltTransporter.IS_BLE) {
                                HttpTransportManager.this.mBltTranser.lookSideBlt();
                                return;
                            } else if (System.getInt(HttpTransportManager.this.mContext.getContentResolver(), "free_num", 0) > 0) {
                                HttpTransportManager.this.mBltTranser.lookSideBlt();
                                return;
                            } else {
                                return;
                            }
                        }
                        if (GlobalDefine.DEBUG_SERVER) {
                            Log.i("WH-SRV", "\n上传到手助失败，即将缓存: ---> " + bean + " \n=========\n\n\n");
                        }
                        SolidLogger.getInstance().with("WH-SRV", "\n上传到手助失败@@@，即将缓存: ---> " + bean + " \n");
                        bean.setState(1);
                        HttpTransportManager.this.mDataCacher.save(bean);
                        TransLogs.m11i(dataParser.getCachaThisText());
                    }
                });
            }
        };
    }

    private static boolean getBleDataTransContainId(String itemId) {
        boolean z = false;
        try {
            if (bleTransContainMethod == null) {
                bleTransContainMethod = Class.forName("com.huami.watch.ble.trasnfer.BLEDataTransfor").getMethod("containsKeyId", new Class[]{String.class});
            }
            Object res = bleTransContainMethod.invoke(null, new Object[]{itemId});
            if (res instanceof Boolean) {
                z = ((Boolean) res).booleanValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return z;
    }

    private boolean isSendingNow(String itemId) {
        if (!(this.sendingItemalreadyList == null || itemId == null || !this.sendingItemalreadyList.contains(itemId))) {
            if (getBleDataTransContainId(itemId)) {
                TransLogs.ii("***BLE过滤了****" + itemId);
                return true;
            }
            TransLogs.ii("***BLE中不存在****" + itemId);
        }
        TransLogs.ii("***isSendingNow false");
        return false;
    }

    private void addItemToSendingList(String itemId) {
        if (this.sendingItemalreadyList == null || itemId == null) {
            TransLogs.ii("***未能加入列表中****itemId:" + itemId + "sendingItemalreadyList is null?:" + (this.sendingItemalreadyList == null));
            return;
        }
        this.sendingItemalreadyList.add(itemId);
        TransLogs.ii("***加入列表中****" + itemId);
    }

    private void removeItemToSendingList(String itemId) {
        if (this.sendingItemalreadyList != null && itemId != null && this.sendingItemalreadyList.contains(itemId)) {
            this.sendingItemalreadyList.remove(itemId);
            TransLogs.ii("***从列表中删除****" + itemId);
        }
    }

    private void sendDataItemToAssist(DataItem item, boolean needLook) {
        if (this.sendingItemalreadyList == null) {
            this.sendingItemalreadyList = new ConcurrentLinkedQueue();
        }
        String itemId = item.getIdentifier();
        if (itemId == null || isSendingNow(itemId)) {
            TransLogs.ii("***在列表中忽略了****" + itemId);
            return;
        }
        tryToSendDataItemToAssist(item, needLook).run();
        if (!Command.isInternal(item.getAction())) {
            addItemToSendingList(itemId);
        }
        TransLogs.ii("***不在列表中并且发送了****" + itemId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void tryProcessCachedRequestWhilePhoneAvalible(android.content.Context r4) {
        /*
        r3 = this;
        monitor-enter(r3);
        r0 = r3.mDataCacher;	 Catch:{ all -> 0x0050 }
        if (r0 != 0) goto L_0x0030;
    L_0x0005:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_SERVER;	 Catch:{ all -> 0x0050 }
        if (r0 == 0) goto L_0x0010;
    L_0x0009:
        r0 = "WH-SRV";
        r1 = "DataCacher is 空，重新初始化...";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0050 }
    L_0x0010:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0050 }
        r1 = "WH-SRV";
        r2 = "DataCacher is 空，重新初始化...";
        r0.with(r1, r2);	 Catch:{ all -> 0x0050 }
        r0 = r3.mContext;	 Catch:{ all -> 0x0050 }
        if (r0 != 0) goto L_0x002d;
    L_0x001f:
        r3.mContext = r4;	 Catch:{ all -> 0x0050 }
        r0 = com.huami.watch.transport.httpsupport.cacher.DataCacher.getInstance(r4);	 Catch:{ all -> 0x0050 }
        r3.mDataCacher = r0;	 Catch:{ all -> 0x0050 }
        r0 = r3.mDataCacher;	 Catch:{ all -> 0x0050 }
        if (r0 != 0) goto L_0x0030;
    L_0x002b:
        monitor-exit(r3);
        return;
    L_0x002d:
        r4 = r3.mContext;	 Catch:{ all -> 0x0050 }
        goto L_0x001f;
    L_0x0030:
        r0 = r3.coreTaskOfProcessCachedRequestWhilePhoneAvalible;	 Catch:{ all -> 0x0050 }
        if (r0 != 0) goto L_0x003d;
    L_0x0034:
        r0 = new com.huami.watch.transport.httpsupport.control.service.HttpTransportManager$8;	 Catch:{ all -> 0x0050 }
        r1 = clc.utils.taskmanager.Task.RunningStatus.WORK_THREAD;	 Catch:{ all -> 0x0050 }
        r0.<init>(r1);	 Catch:{ all -> 0x0050 }
        r3.coreTaskOfProcessCachedRequestWhilePhoneAvalible = r0;	 Catch:{ all -> 0x0050 }
    L_0x003d:
        r0 = r3.mData2AssistOverBltTaskManager;	 Catch:{ all -> 0x0050 }
        r1 = r3.coreTaskOfProcessCachedRequestWhilePhoneAvalible;	 Catch:{ all -> 0x0050 }
        r0.removeTask(r1);	 Catch:{ all -> 0x0050 }
        r0 = r3.mData2AssistOverBltTaskManager;	 Catch:{ all -> 0x0050 }
        r1 = r3.coreTaskOfProcessCachedRequestWhilePhoneAvalible;	 Catch:{ all -> 0x0050 }
        r0 = r0.next(r1);	 Catch:{ all -> 0x0050 }
        r0.execute();	 Catch:{ all -> 0x0050 }
        goto L_0x002b;
    L_0x0050:
        r0 = move-exception;
        monitor-exit(r3);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.transport.httpsupport.control.service.HttpTransportManager.tryProcessCachedRequestWhilePhoneAvalible(android.content.Context):void");
    }
}
