package com.huami.watch.transport.httpsupport.control.assist;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.AnalysisStage;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector.OnReportResultListener;
import com.huami.watch.transport.httpsupport.StageFright.ProcessStateMonitor;
import com.huami.watch.transport.httpsupport.StageFright.ProcessStateMonitor.Jack;
import com.huami.watch.transport.httpsupport.StageFright.ReportListInfo;
import com.huami.watch.transport.httpsupport.StageFright.SyncProcessManager;
import com.huami.watch.transport.httpsupport.Utils;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.global.ManualDataSyncMonitor;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import com.huami.watch.transport.httpsupport.transporter.blt.BltTransporter;
import com.huami.watch.transport.httpsupport.transporter.blt.BltTransporter.IResultReceiver;
import com.huami.watch.transport.httpsupport.transporter.http.HttpTransporter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class WearHttpCompanion {
    private static AtomicBoolean sHasInited = new AtomicBoolean(false);
    private static WearHttpCompanion sInstance = null;
    private AtomicBoolean lookingSideNet;
    private BltTransporter mBluetoothTransporter;
    private Context mContext;
    private TaskManager mData2SrvOverNetworkTaskManager;
    private TaskManager mData2WatchOverBltTaskManager;
    private DataCacher mDataCacher;
    private ManualDataSyncMonitor mDataSyncMonitor;
    private EnumCollector mEnumCollector;
    private HttpTransporter mHttpTransporter;
    private TokenArrivalListener tokenArrivalListener;

    class C10341 extends EnumCollector {

        class C10311 extends ProcessStateMonitor {
            C10311() {
            }
        }

        C10341() {
        }

        protected void data2Lower(DataItem item, OnReportResultListener l) {
            WearHttpCompanion.this.data2ClientByBlt(item);
        }

        protected void data2Upper(DataItem reportListItem, OnReportResultListener l) {
            boolean isAuto = TextUtils.equals(reportListItem.getExtraValByKey("self"), "1");
            boolean isKey = TextUtils.equals(reportListItem.getExtraValByKey("keyf"), "1");
            if (isAuto && isKey) {
                if (AnalysisStage.getInstance().findProcessStateMonitor(reportListItem.trackWho()) != null) {
                    if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-ASSIST", "已经在同步了，所以此次忽略" + reportListItem.toShortString());
                    }
                    SolidLogger.getInstance().with("WH-ASSIST", "已经在同步了，所以此次忽略 " + reportListItem.toShortString());
                    return;
                }
                ProcessStateMonitor monitor = new C10311();
                AnalysisStage.getInstance().addReportArrivalListener(monitor);
                final ProcessStateMonitor autoMonitor = monitor;
                autoMonitor.setReportId(reportListItem.getIdentifier());
                final String[] target = reportListItem.trackTargetPackageAndAction();
                monitor.inJack(new Jack() {
                    public String targetAtPackage() {
                        return target[0];
                    }

                    public String targetAtAction() {
                        return target[1];
                    }

                    public void onUpdate(int state, int allInsum, int percent) {
                    }

                    public void onFinish(int code, boolean newDataUploaded, long startTime) {
                        AnalysisStage.getInstance().removeReportArrivalListener(autoMonitor);
                    }
                });
                AnalysisStage.getInstance().addReportArrivalListener(autoMonitor);
                DataItem localReport = new DataItem();
                localReport.setAction("_report_sync_list");
                localReport.setOwner(WearHttpCompanion.this.mContext.getPackageName());
                localReport.setExtraData(reportListItem.getExtraData().toString());
                dataLocal2Upper(localReport);
                localReport.removeExtraPairByKey("keyf");
                localReport.removeExtraPairByKey("report-list");
                localReport.setIdentifier(reportListItem.getIdentifier());
                AnalysisStage.getInstance().handleReportHub(WearHttpCompanion.this.mContext, localReport);
                if (GlobalDefine.DEBUG_SERIAL_MODE) {
                    Log.i("WH-ASSIST", " AUTO MODE 添加了local data report ");
                }
                SolidLogger.getInstance().with("WH-SERIAL_MODE", "AUTO MODE 添加了local data report ");
            }
            AnalysisStage.getInstance().handleReportHub(WearHttpCompanion.this.mContext, reportListItem);
        }

        protected void dataLocal2Upper(DataItem item) {
            DataCacher cache = DataCacher.getInstance(WearHttpCompanion.this.mContext);
            ReportListInfo infoList = new ReportListInfo();
            List<DataItem> list1 = cache.query(4, 0);
            if (list1 != null) {
                String[] pkgAndAction = item.trackTargetPackageAndAction();
                String pkg = pkgAndAction[0];
                boolean careAllAction = TextUtils.equals("*", pkgAndAction[1]);
                HashSet<String> actions = item.trackTargetActionSet();
                for (DataItem i : list1) {
                    if (TextUtils.equals(pkg, i.getOwner())) {
                        if (!(careAllAction || actions.contains(i.getAction()) || !GlobalDefine.DEBUG_SERIAL_MODE)) {
                            Log.i("WH-ASSIST", "需要: " + careAllAction + "忽略了这个action： " + i.toShortString());
                        }
                        infoList.addItem(Integer.valueOf(2), i);
                    } else if (GlobalDefine.DEBUG_SERIAL_MODE) {
                        Log.i("WH-ASSIST", "需要: " + pkg + " , 忽略了这个家伙： " + i.toShortString());
                    }
                }
            }
            item.addExtraPair("report-list", infoList.flatten());
            item.setOwner(WearHttpCompanion.this.mContext.getPackageName());
            item.setAction("_report_sync_list");
        }

        protected void dataFromLower(DataItem itemAction) {
            if (TextUtils.equals(itemAction.getAction(), "_report_sync_list")) {
                ReportListInfo reportInfo = ReportListInfo.from(itemAction.getExtraValByKey("report-list"), !TextUtils.isEmpty(itemAction.getExtraValByKey("keyf")));
                if (ReportListInfo.getVersion() == 0) {
                    itemAction.addExtraPair("report-list", ReportListInfo.translateV0TOV1(reportInfo, Integer.valueOf("1").intValue()).flatten());
                }
            }
            super.dataFromLower(itemAction);
        }

        protected void onCustomCommand(final DataItem customCommand) {
            super.onCustomCommand(customCommand);
            String action = customCommand.getAction();
            if (TextUtils.equals(action, "sync-internal-for-assist")) {
                data2Lower(customCommand, null);
            } else if (TextUtils.equals(action, "_client_trigger_report")) {
                dataLocal2Upper(customCommand);
            } else if (TextUtils.equals(action, "_query_token")) {
                Log.i("wifi_trans", "rom--cmd" + action + "--customCommand--" + customCommand.toString());
                if (WearHttpCompanion.this.tokenArrivalListener != null) {
                    Task task = new Task(RunningStatus.WORK_THREAD) {
                        public TaskOperation onExecute(TaskOperation taskOperation) {
                            String token = WearHttpCompanion.this.tokenArrivalListener.callBack("_query_token");
                            DataItem dataItem = new DataItem();
                            dataItem.setAction("_arrival_token");
                            dataItem.setOwner(customCommand.getOwner());
                            dataItem.addExtraPair("target", customCommand.getExtraValByKey("target"));
                            dataItem.setData(token);
                            Log.i("wifi_trans", "trans---sendToRom--" + dataItem.toString());
                            C10341.this.data2Lower(dataItem, null);
                            return null;
                        }
                    };
                    if (WearHttpCompanion.this.mData2SrvOverNetworkTaskManager != null) {
                        WearHttpCompanion.this.mData2SrvOverNetworkTaskManager.next(task).execute();
                    }
                }
            }
        }
    }

    class C10352 extends BltTransporter {
        final /* synthetic */ WearHttpCompanion this$0;

        protected void plainItemDataIn(DataItem dataItem) {
            lookSideBlt();
            ProcessStateMonitor monitor = AnalysisStage.getInstance().findProcessStateMonitor(dataItem.trackWho());
            if (monitor != null) {
                monitor.onFeedSingleLazyGirl(this.this$0.mContext, dataItem);
            }
            this.this$0.separateDataItems(dataItem);
        }

        protected void handleInnerCommand(DataItem dataItem) {
            super.handleInnerCommand(dataItem);
            if (!Command.isInternal(dataItem.getAction())) {
                return;
            }
            if (TextUtils.equals(dataItem.getAction(), "tell-internal-for-assist")) {
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "rev internal CMD : " + dataItem.toShortString());
                }
                SolidLogger.getInstance().with("WH-ASSIST", "rev internal CMD : " + dataItem.toShortString());
                this.this$0.handleInternalCMD(dataItem);
                return;
            }
            this.this$0.mEnumCollector.addActionAndExecute(dataItem);
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "2 rev internal CMD : " + dataItem.toShortString());
            }
        }
    }

    public interface TokenArrivalListener {
        String callBack(String str);
    }

    public interface TrafficMonitor {
    }

    private void doInitEnumCollector() {
        this.mEnumCollector = new C10341();
        this.mEnumCollector.setName("EC-WearHttpCompanion");
    }

    public EnumCollector getCollector() {
        if (this.mEnumCollector == null) {
            doInitEnumCollector();
        }
        return this.mEnumCollector;
    }

    private void separateDataItems(DataItem item) {
        if (item.hasFlag(16384)) {
            item.setState(4);
            DataCacher.getInstance(this.mContext).save(item);
            if (GlobalDefine.DEBUG_SERIAL_MODE) {
                Log.i("WH-SERIAL_MODE", "Immediately(直接就) ==> Saved Serial Item : " + item.getIdentifier());
            }
            SolidLogger.getInstance().with("WH-SERIAL_MODE", "Immediately(直接就) ==> Saved Serial Item : " + item.getIdentifier());
            return;
        }
        handlePlainDataItemToServer(item);
    }

    public static WearHttpCompanion getInstance() {
        if (sHasInited.get()) {
            return sInstance;
        }
        throw new IllegalStateException("## Call doInit firstly !");
    }

    private void process(final DataItem item) {
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "process in wear companion for : " + item.getOwner());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "process in wear companion for : " + item.getOwner());
        int state = item.getState();
        switch (state) {
            case 0:
                this.mData2WatchOverBltTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                    public TaskOperation onExecute(TaskOperation operation) {
                        if (item.hasFlag(512)) {
                        }
                        if (item.hasFlag(4)) {
                            if (GlobalDefine.DEBUG_COMPANION) {
                                Log.i("WH-ASSIST", "===> 将从数据库删除: " + item.toShortString());
                            }
                            SolidLogger.getInstance().with("WH-ASSIST", "===> 将从数据库删除: " + item.toShortString());
                            boolean res = DataCacher.getInstance(WearHttpCompanion.this.mContext.getApplicationContext()).delete(item.getIdentifier());
                            if (GlobalDefine.DEBUG_COMPANION) {
                                Log.i("WH-ASSIST", "===> 删除结果: " + item.getIdentifier() + " : " + res);
                            }
                            SolidLogger.getInstance().with("WH-ASSIST", "===> 删除结果: " + item.getIdentifier() + " : " + res);
                            if (GlobalDefine.DEBUG_COMPANION) {
                                Log.i("WH-ASSIST", "===> LOOK SIDE OR NET <=====");
                            }
                            SolidLogger.getInstance().with("WH-ASSIST", "===> LOOK SIDE OR NET <=====");
                            WearHttpCompanion.this.tryProcessCachedRequestWhileNetWorkAvalible();
                        }
                        if (GlobalDefine.DEBUG_COMPANION) {
                            Log.i("WH-ASSIST", "now  TRY  send back to client, will to wear service " + item.toShortString());
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", "Now TRY send back to client, will to wear service " + item.toShortString());
                        if (item.hasFlag(128)) {
                            boolean fail;
                            if (GlobalDefine.DEBUG_COMPANION) {
                                boolean z;
                                fail = 200 < item.getCode() || item.getCode() > 299;
                                String str = "WH-ASSIST";
                                StringBuilder append = new StringBuilder().append("IS ONE SHOT REQ : ").append(item.toShortString()).append(" , 结果： ");
                                if (fail) {
                                    z = false;
                                } else {
                                    z = true;
                                }
                                Log.i(str, append.append(z).append(" , code: ").append(item.getCode()).toString());
                            }
                            if (200 < item.getCode() || item.getCode() > 299) {
                                fail = true;
                            } else {
                                fail = false;
                            }
                            SolidLogger.getInstance().with("WH-ASSIST", "IS ONE SHOT REQ : " + item.toShortString() + " , 结果：" + fail + " , code: " + item.getCode());
                        } else {
                            WearHttpCompanion.this.data2ClientByBlt(item);
                        }
                        return null;
                    }
                }).execute();
                return;
            case 4:
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "send : " + item.getOwner() + " , targetWho : " + item.getIdentifier() + " , action: " + item.getAction() + " , FAILED ! Will ###CACHE### it.");
                }
                SolidLogger.getInstance().with("WH-ASSIST", "send : " + item.getOwner() + " , targetWho : " + item.getIdentifier() + " , action: " + item.getAction() + " , FAILED ! Will ###CACHE### it.");
                if (this.mDataCacher == null) {
                    this.mDataCacher = DataCacher.getInstance(this.mContext.getApplicationContext());
                }
                if (this.mDataCacher != null) {
                    boolean res = this.mDataCacher.save(item);
                    if (GlobalDefine.DEBUG_COMPANION) {
                        Log.i("WH-ASSIST", "Cached : " + item.getOwner() + " , targetWho : " + item.getIdentifier() + " , action: " + item.getAction() + "--> result: " + res);
                    }
                    SolidLogger.getInstance().with("WH-ASSIST", "Cached : " + item.getOwner() + " , targetWho : " + item.getIdentifier() + " , action: " + item.getAction() + "--> result: " + res);
                    return;
                }
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "Cacher is NULLL!LL!L!L!L!L");
                }
                SolidLogger.getInstance().with("WH-ASSIST", "Cacher is NULLL!LL!L!L!L!L");
                return;
            case 5:
                data2ServerByHttp(item);
                return;
            default:
                Utils.log("Unimplemented state:" + state);
                return;
        }
    }

    private void data2ClientByBlt(final DataItem item) {
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "send data to client from Wear Componion By BLT. owner: " + item.getOwner() + " , targetWho: " + item.getIdentifier() + " , action" + item.getAction());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "send data to client from Wear Componion By BLT. owner: " + item.getOwner() + " , targetWho: " + item.getIdentifier() + " , action" + item.getAction());
        if (this.mBluetoothTransporter.check()) {
            this.mBluetoothTransporter.dataOut(item, new IResultReceiver() {
                public void onResult(int result) {
                    if (result != 0) {
                        if (GlobalDefine.DEBUG_COMPANION) {
                            Log.i("WH-ASSIST", "手表暂时不通，缓存Item: " + item.toShortString());
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", "手表暂时不通，缓存Item: " + item.toShortString());
                        if (WearHttpCompanion.this.mDataCacher == null) {
                            WearHttpCompanion.this.mDataCacher = DataCacher.getInstance(WearHttpCompanion.this.mContext);
                        }
                        item.setState(3);
                        WearHttpCompanion.this.mDataCacher.save(item);
                    } else if ((item.getFlags() & 4) != 0) {
                        boolean res = WearHttpCompanion.this.mDataCacher.delete(item.getIdentifier());
                        if (GlobalDefine.DEBUG_COMPANION) {
                            Log.i("WH-ASSIST", ">>>传输成功后，删除本条Item: " + item.toShortString() + " , 结果： " + res);
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", ">>>传输成功后，删除本条Item: " + item.getIdentifier() + " , 结果： " + res);
                    }
                }
            });
            return;
        }
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "1 手表暂时不通，缓存Item: " + item.toShortString());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "1 手表暂时不通，缓存Item: " + item.toShortString());
        if (this.mDataCacher == null) {
            this.mDataCacher = DataCacher.getInstance(this.mContext);
        }
        item.setState(3);
        this.mDataCacher.save(item);
    }

    private void data2ServerByHttp(DataItem item) {
        if (item.hasFlag(4096)) {
            String slotInExtraKey = item.getExtraValByKey("extra_slot_point");
            if (!TextUtils.isEmpty(slotInExtraKey)) {
                try {
                    JSONObject obj = new JSONObject(item.getExtraValByKey("extra_slot_frame"));
                    try {
                        obj.put(slotInExtraKey, item.getData());
                        item.setData(obj.toString());
                    } catch (JSONException e) {
                        JSONObject jSONObject = obj;
                    }
                } catch (JSONException e2) {
                }
            }
        }
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "Before send data to server from Wear Componion By HTTP : " + item.toShortString());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "Before send data to server from Wear Componion By HTTP : " + item.toShortString());
        if (item.hasFlag(256)) {
            handleExfFlag(item);
        }
        DataItem result = this.mHttpTransporter.request(this.mContext, item, new HashMap());
        if (result == null) {
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "创建请求的时候发生错误，请检查。该次请求将被忽略！%%%%%%%%%%%%%% : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction());
            }
            SolidLogger.getInstance().with("WH-ASSIST", "创建请求的时候发生错误，请检查。该次请求将被忽略！%%%%%%%%%%%%%% : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction());
            if (this.mDataCacher.delete(item.getIdentifier())) {
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "从数据库中移除了如下请求item : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction());
                }
                SolidLogger.getInstance().with("WH-ASSIST", "从数据库中移除了如下请求item : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction());
                return;
            }
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "从数据库中移除了如下请求item : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction() + "失败！！！！！！！");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "从数据库中移除了如下请求item : targetWho : " + item.getIdentifier() + " , owner: " + item.getOwner() + " , action: " + item.getAction() + "失败！！！！！！！");
            return;
        }
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "r已返回， Wear Componion By HTTP : " + result.toShortString());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "r已返回，After send data to server from Wear Componion By HTTP : " + result.toShortString());
        if (result.getCode() != 200) {
            result.setState(4);
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "\n\n未能-成功-发送 : " + item.getIdentifier() + " , 数据： " + DataUtils.drillUnzipData(item) + "\n\n");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "\n\n未能-成功-发送 : " + item.getIdentifier() + " , 数据： " + DataUtils.drillUnzipData(item) + "\n\n");
        } else {
            SyncProcessManager.getInstance().tellWith(item.trackWho(), (byte) 9, new int[0]);
            result.setState(0);
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "\n\n已經成功-发送 : " + item.getIdentifier() + " , 数据： " + DataUtils.drillUnzipData(item) + "\n\n");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "\n\n已經-成功-发送 : " + item.getIdentifier() + " , 数据： " + DataUtils.drillUnzipData(item) + "\n\n");
        }
        process(result);
    }

    private void handlePlainDataItemToServer(final DataItem dataItem) {
        if (GlobalDefine.DEBUG_COMPANION) {
            Log.i("WH-ASSIST", "handlePlainDataItemToServer : " + dataItem.toShortString());
        }
        SolidLogger.getInstance().with("WH-ASSIST", "Handle Data From WearService : " + dataItem.toShortString());
        this.mData2SrvOverNetworkTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation operation) {
                WearHttpCompanion.this.sendDataToServer(dataItem);
                return null;
            }
        }).execute();
    }

    public Task sendDataToApp(final DataItem itemToApp) {
        if (itemToApp == null) {
            return null;
        }
        if (!TextUtils.isEmpty(itemToApp.getOwner())) {
            return new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation operation) {
                    if (TextUtils.isEmpty(itemToApp.getAction())) {
                        if (GlobalDefine.DEBUG_COMPANION) {
                            Log.i("WH-ASSIST", "sendDataToApp WARNNING: NO ACTION FOUND FOR: " + itemToApp.toShortString());
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", "sendDataToApp WARNNING: NO ACTION FOUND FOR: " + itemToApp.toShortString());
                    }
                    WearHttpCompanion.this.data2ClientByBlt(itemToApp);
                    return null;
                }
            };
        }
        throw new IllegalArgumentException("No Owner ! " + itemToApp);
    }

    public void sendDataToServer(DataItem itemToApp) {
        if (itemToApp != null) {
            if (TextUtils.isEmpty(itemToApp.getOwner())) {
                throw new IllegalArgumentException("No Owner ! " + itemToApp);
            }
            if (TextUtils.isEmpty(itemToApp.getAction())) {
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "sendDataToServer WARNNING: NO ACTION FOUND FOR: " + itemToApp.toShortString());
                }
                SolidLogger.getInstance().with("WH-ASSIST", "sendDataToServer WARNNING: NO ACTION FOUND FOR: " + itemToApp.toShortString());
            }
            data2ServerByHttp(itemToApp);
        }
    }

    public synchronized void tryProcessCachedRequestWhileNetWorkAvalible() {
        if (!(this.mDataCacher == null || this.lookingSideNet.get())) {
            this.mData2SrvOverNetworkTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation arg0) {
                    if (!WearHttpCompanion.this.lookingSideNet.get()) {
                        WearHttpCompanion.this.lookingSideNet.set(true);
                        List<DataItem> itemList = WearHttpCompanion.this.mDataCacher.query(4);
                        if (GlobalDefine.DEBUG_COMPANION) {
                            Log.i("WH-ASSIST", "------> 尝试处理缓存在“手助”的请求 有 ==> [ " + (itemList == null ? 0 : itemList.size()) + " ] 个请求需要重新发起....");
                        }
                        SolidLogger.getInstance().with("WH-ASSIST", "------> 尝试处理缓存在“手助”的请求 有 ==> [ " + (itemList == null ? 0 : itemList.size()) + " ] 个请求需要重新发起....");
                        if (itemList == null) {
                            WearHttpCompanion.this.lookingSideNet.set(false);
                        } else {
                            for (DataItem item : itemList) {
                                if (item != null) {
                                    item.setState(5);
                                    item.addFlags(4);
                                    WearHttpCompanion.this.sendDataToServer(item);
                                }
                            }
                            WearHttpCompanion.this.lookingSideNet.set(false);
                        }
                    }
                    return null;
                }
            }).execute();
        }
    }

    public void tryProcessCachedResponseWhileChannelAvalible() {
        if (this.mDataCacher != null) {
            this.mData2SrvOverNetworkTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation paramTaskOperation) {
                    List<DataItem> itemList = WearHttpCompanion.this.mDataCacher.query(3, 0);
                    if (GlobalDefine.DEBUG_COMPANION) {
                        Log.i("WH-ASSIST", "------> 尝试处理缓存在“手助”的 \"SERVER返回结果\" 有 ==> [ " + (itemList == null ? 0 : itemList.size()) + " ] 个SERVER 返回结果需要回传....");
                    }
                    SolidLogger.getInstance().with("WH-ASSIST", "------> 尝试处理缓存在“手助”的 \"SERVER返回结果\" 有 ==> [ " + (itemList == null ? 0 : itemList.size()) + " ] 个SERVER 返回结果需要回传....");
                    if (itemList != null) {
                        for (DataItem item : itemList) {
                            if (item != null) {
                                item.setState(0);
                                item.addFlags(4);
                                Task t = WearHttpCompanion.this.sendDataToApp(item);
                                if (t != null) {
                                    try {
                                        WearHttpCompanion.this.mData2WatchOverBltTaskManager.next(t);
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        }
                        WearHttpCompanion.this.mData2WatchOverBltTaskManager.execute();
                    }
                    return null;
                }
            }).execute();
        }
    }

    private boolean handleExfFlag(DataItem item) {
        if (item.hasFlag(1024)) {
            String existsFilePath = item.getExtraValByKey("exf_path");
            if (!TextUtils.isEmpty(existsFilePath)) {
                boolean isValid;
                File f = new File(existsFilePath);
                if (f.exists() && f.isFile()) {
                    isValid = true;
                } else {
                    isValid = false;
                }
                if (GlobalDefine.DEBUG_COMPANION) {
                    Log.i("WH-ASSIST", "已经存在了etf文件 : " + item.toShortString() + " , 有效吗： " + isValid);
                }
                SolidLogger.getInstance().with("WH-ASSIST", "已经存在了etf文件 : " + item.toShortString() + " , 有效吗： " + isValid);
                if (isValid) {
                    return true;
                }
            }
        }
        String dataExf = item.getExtraValByKey("exf");
        if (TextUtils.isEmpty(dataExf)) {
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "No Extra Exf for : " + item.toShortString() + " , but with THE FLAG!");
            }
            SolidLogger.getInstance().with("WH-ASSIST", "No Extra Exf for : " + item.toShortString() + " , but with THE FLAG!");
            return false;
        }
        boolean res = true;
        String filePathAndName = this.mContext.getFilesDir() + File.separator + (item.getIdentifier() + System.currentTimeMillis());
        try {
            Utils.writeStringToFile(new File(filePathAndName), dataExf, "UTF-8");
            item.removeExtraPairByKey("exf");
            item.addFlags(1024);
            item.addExtraPair("exf_path", filePathAndName);
        } catch (IOException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    public void notifyDataSyncSucceed(DataItem item) {
        if (this.mDataSyncMonitor != null) {
            this.mDataSyncMonitor.notifyStateChangeFor(item, 0);
        }
    }

    public void notifyDataSyncFailed(DataItem item) {
        if (this.mDataSyncMonitor != null) {
            this.mDataSyncMonitor.notifyStateChangeFor(item, -1);
        }
    }

    private void handleInternalCMD(DataItem dataItem) {
        if (Boolean.valueOf(dataItem.getExtraValByKey("res")).booleanValue()) {
            notifyDataSyncSucceed(dataItem);
        } else {
            notifyDataSyncFailed(dataItem);
        }
    }

    public HttpTransporter getHttpTransporter() {
        return this.mHttpTransporter;
    }
}
