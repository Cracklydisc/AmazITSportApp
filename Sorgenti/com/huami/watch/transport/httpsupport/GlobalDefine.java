package com.huami.watch.transport.httpsupport;

import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import java.io.File;

public class GlobalDefine {
    public static int CURR_REPORT_LIST_VER = Integer.valueOf("1").intValue();
    public static final boolean DEBUG = new File(Environment.getExternalStorageDirectory() + "/springchannel.ini").exists();
    public static final boolean DEBUG_BLT = (DEBUG & 1);
    public static final boolean DEBUG_CLIENT = (DEBUG & 1);
    public static final boolean DEBUG_COMPANION = (DEBUG & 1);
    public static final boolean DEBUG_CRASH_MONITOR = (DEBUG & 1);
    public static final boolean DEBUG_HTTP = (DEBUG & 1);
    public static final boolean DEBUG_LD = (DEBUG & 1);
    public static final boolean DEBUG_SERIAL_MODE = (DEBUG & 1);
    public static final boolean DEBUG_SERVER = (DEBUG & 1);
    public static final boolean DEBUG_SRV = (DEBUG & 1);
    private static boolean IS_SUPPORT_NEW_SYNC = true;
    public static final boolean IS_WATCH = TextUtils.equals(Build.DEVICE, "watch");
    private static boolean sIsOverSea = false;

    public static void saveSyncSupport(boolean supportNew) {
        IS_SUPPORT_NEW_SYNC = supportNew;
    }

    public static boolean supportNewSync() {
        return IS_SUPPORT_NEW_SYNC;
    }

    public static boolean isOverSea() {
        return sIsOverSea;
    }
}
