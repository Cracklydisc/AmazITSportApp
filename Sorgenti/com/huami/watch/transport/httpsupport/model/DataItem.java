package com.huami.watch.transport.httpsupport.model;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.Utils.BytesZipUtil;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class DataItem {
    private String action;
    private int code;
    private String data;
    private JSONObject extra;
    private int flags;
    private String identifier;
    private String method;
    private String owner;
    private int state;
    private long timeout;
    private String url;

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataItem(String url, String action, String owner, String method) {
        this(url, action, owner, method, (String) null);
    }

    public DataItem(String url, String action, String owner, String method, String data) {
        this(url, action, owner, method, data, data != null);
    }

    public DataItem(String url, String action, String owner, String method, String data, boolean cache) {
        this(url, action, owner, method, data, cache, false);
    }

    public DataItem(String url, String action, String owner, String method, String data, boolean cache, boolean compress) {
        this(url, action, owner, method, data, cache, compress, 0);
    }

    public DataItem(String url, String action, String owner, String method, String data, boolean cache, boolean compress, long timeout) {
        int flags = 0;
        if (cache) {
            flags = 0 | 1;
        }
        if (compress) {
            flags |= 2;
        }
        setIdentifier(DataUtils.generateId());
        setUrl(url);
        setAction(action);
        setOwner(owner);
        setMethod(method);
        setData(data);
        setFlags(flags);
        setTimeout(timeout);
        setState(5);
        this.extra = new JSONObject();
    }

    public static DataItem from(String string) {
        DataItem item = new DataItem();
        try {
            JSONObject obj = new JSONObject(string);
            item.setIdentifier(obj.optString("I"));
            item.setUrl(obj.optString("U"));
            item.setAction(obj.optString("A"));
            item.setOwner(obj.optString("O"));
            item.setMethod(obj.optString("M"));
            item.setData(obj.optString("D"));
            item.setFlags(obj.optInt("F"));
            item.setTimeout(obj.optLong("T"));
            item.setState(obj.optInt("S"));
            item.setCode(obj.optInt("C"));
            String extra = obj.getString("E");
            if (extra != null) {
                item.setExtraData(extra);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public String toString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("I", this.identifier);
            obj.put("U", this.url);
            obj.put("A", this.action);
            obj.put("O", this.owner);
            obj.put("M", this.method);
            if (this.data == null) {
                this.data = "";
            }
            this.data = compress(this.data);
            obj.put("D", this.data);
            obj.put("F", this.flags);
            obj.put("T", this.timeout);
            obj.put("S", this.state);
            obj.put("E", this.extra == null ? "" : this.extra.toString());
            obj.put("C", this.code);
            if (hasFlag(16384)) {
                obj.put("isSerial", "YES");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    private String compress(String data) {
        if (data.startsWith("<!@@%C%&&!>")) {
            return data;
        }
        return "<!@@%C%&&!>" + Base64.encodeToString(BytesZipUtil.gZip(data.getBytes()), 0);
    }

    public byte[] decompress(String data) {
        String target = data;
        if (data.startsWith("<!@@%C%&&!>")) {
            target = data.substring("<!@@%C%&&!>".length(), data.length() - 1);
        }
        return BytesZipUtil.unGZip(Base64.decode(target, 0));
    }

    public String removePrefix(String data) {
        try {
            if (data.startsWith("<!@@%C%&&!>")) {
                data = data.substring("<!@@%C%&&!>".length(), data.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getMethod() {
        return this.method;
    }

    public void setMethod(String method) {
        if (TextUtils.isEmpty(method)) {
            method = "get";
        }
        this.method = method;
    }

    public int getFlags() {
        return this.flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public void addFlags(int flags) {
        this.flags |= flags;
    }

    public void clearFlags(int flags) {
        this.flags &= flags ^ -1;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public String getData() {
        return this.data;
    }

    public boolean hasFlag(int flag) {
        return (this.flags & flag) == flag;
    }

    public void setData(String data) {
        if (data == null) {
            data = "";
        }
        this.data = compress(data);
    }

    public String toShortString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("I", this.identifier);
            obj.put("U", this.url);
            obj.put("A", this.action);
            obj.put("O", this.owner);
            obj.put("M", this.method);
            obj.put("F", this.flags);
            obj.put("T", this.timeout);
            obj.put("S", this.state);
            obj.put("E", this.extra == null ? "" : this.extra.toString());
            obj.put("D", TextUtils.isEmpty(this.data) ? "[EMPTY]" : "▂ #有<数据>料# ▂");
            obj.put("C", this.code);
            if (hasFlag(16384)) {
                obj.put("isSerial", "YES");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }

    public JSONObject getExtraData() {
        if (this.extra == null) {
            this.extra = new JSONObject();
        }
        return this.extra;
    }

    public void setExtraData(String b) {
        if (TextUtils.isEmpty(b)) {
            this.extra = null;
            return;
        }
        try {
            this.extra = new JSONObject(b);
        } catch (JSONException e) {
            this.extra = null;
            SolidLogger.getInstance().with("WH-SRV", "setExtraData ERROR. Failed To Set Extra Data For : [" + b + "] . NEED JSON.");
            Log.i("WH-SRV", "setExtraData ERROR. Failed To Set Extra Data For : [" + b + "] . NEED JSON. Stack is :", e);
        }
    }

    public void addExtraPair(String key, String val) {
        if (TextUtils.isEmpty(key)) {
            throw new IllegalArgumentException("KEY Must Not Be NULL!");
        }
        if (this.extra == null) {
            this.extra = new JSONObject();
        }
        try {
            this.extra.put(key, val);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getExtraValByKey(String key) {
        if (TextUtils.isEmpty(key) || this.extra == null) {
            return null;
        }
        return this.extra.optString(key);
    }

    public Iterator<String> retrieveAllHeaderKeys() {
        if (this.extra == null) {
            return null;
        }
        JSONObject obj = this.extra.optJSONObject("key_header");
        if (obj != null) {
            return obj.keys();
        }
        return null;
    }

    public String getHeaderValByKey(String key) {
        if (this.extra == null) {
            return null;
        }
        JSONObject obj = this.extra.optJSONObject("key_header");
        if (obj != null) {
            return obj.optString(key);
        }
        return null;
    }

    public boolean removeExtraPairByKey(String key) {
        if (this.extra != null && this.extra.remove(key) == null) {
            return false;
        }
        return true;
    }

    public boolean addHeader(String key, String val) {
        JSONObject jSONObject;
        String str;
        JSONObject header;
        JSONException e;
        boolean res;
        JSONObject jSONObject2 = null;
        if (!hasHeader() && this.extra == null) {
            this.extra = new JSONObject();
            try {
                jSONObject = this.extra;
                str = "key_header";
                header = new JSONObject();
                try {
                    jSONObject.put(str, header);
                    jSONObject2 = header;
                } catch (JSONException e2) {
                    e = e2;
                    jSONObject2 = header;
                    e.printStackTrace();
                    res = true;
                    if (!this.extra.has("key_header")) {
                        jSONObject = this.extra;
                        str = "key_header";
                        header = new JSONObject();
                        try {
                            jSONObject.put(str, header);
                            jSONObject2 = header;
                        } catch (JSONException e3) {
                            e = e3;
                            jSONObject2 = header;
                            e.printStackTrace();
                            res = false;
                            return res;
                        }
                    }
                    if (jSONObject2 == null) {
                        jSONObject2 = this.extra.getJSONObject("key_header");
                    }
                    if (jSONObject2 != null) {
                        return false;
                    }
                    jSONObject2.put(key, val);
                    return res;
                }
            } catch (JSONException e4) {
                e = e4;
                e.printStackTrace();
                res = true;
                if (this.extra.has("key_header")) {
                    jSONObject = this.extra;
                    str = "key_header";
                    header = new JSONObject();
                    jSONObject.put(str, header);
                    jSONObject2 = header;
                }
                if (jSONObject2 == null) {
                    jSONObject2 = this.extra.getJSONObject("key_header");
                }
                if (jSONObject2 != null) {
                    return false;
                }
                jSONObject2.put(key, val);
                return res;
            }
        }
        res = true;
        try {
            if (this.extra.has("key_header")) {
                jSONObject = this.extra;
                str = "key_header";
                header = new JSONObject();
                jSONObject.put(str, header);
                jSONObject2 = header;
            }
            if (jSONObject2 == null) {
                jSONObject2 = this.extra.getJSONObject("key_header");
            }
            if (jSONObject2 != null) {
                return false;
            }
            jSONObject2.put(key, val);
            return res;
        } catch (JSONException e5) {
            e = e5;
            e.printStackTrace();
            res = false;
            return res;
        }
    }

    public boolean hasHeader() {
        return this.extra != null && this.extra.has("key_header");
    }

    public String trackWho() {
        String res = getExtraValByKey("target");
        if (TextUtils.isEmpty(res)) {
            return this.owner + "#" + this.action;
        }
        return res;
    }

    public String[] trackTargetPackageAndAction() {
        if (Command.isInternal(this.action)) {
            String who = getExtraValByKey("target");
            if (TextUtils.isEmpty(who)) {
                who = getOwner();
            }
            if (TextUtils.isEmpty(who)) {
                who = "*";
            }
            String[] tmp = who.split("#");
            if (tmp != null && tmp.length >= 2) {
                return tmp;
            }
            return new String[]{who, "*"};
        }
        throw new IllegalArgumentException("Error For Extract Target Package! " + toString());
    }

    public HashSet<String> trackTargetActionSet() {
        HashSet<String> set = new HashSet();
        set.addAll(Arrays.asList(trackTargetPackageAndAction()[1].split("\\|")));
        return set;
    }
}
