package com.huami.watch.transport.httpsupport.model;

import android.text.TextUtils;
import android.util.Base64;
import com.huami.watch.transport.DataBundle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

public class DataUtils {
    private static boolean checkedVal = false;

    public static class BytesZipUtil {
        public static byte[] unGZip(byte[] data) {
            Exception ex;
            Throwable th;
            byte[] b = null;
            ByteArrayInputStream bis = null;
            GZIPInputStream gzip = null;
            ByteArrayOutputStream baos = null;
            try {
                ByteArrayInputStream bis2 = new ByteArrayInputStream(data);
                try {
                    GZIPInputStream gzip2 = new GZIPInputStream(bis2);
                    try {
                        byte[] buf = new byte[1024];
                        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                        while (true) {
                            try {
                                int num = gzip2.read(buf, 0, buf.length);
                                if (num == -1) {
                                    break;
                                }
                                baos2.write(buf, 0, num);
                            } catch (Exception e) {
                                ex = e;
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            } catch (Throwable th2) {
                                th = th2;
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            }
                        }
                        b = baos2.toByteArray();
                        baos2.flush();
                        baos2.close();
                        gzip2.close();
                        bis2.close();
                        if (bis2 != null) {
                            try {
                                bis2.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (gzip2 != null) {
                            try {
                                gzip2.close();
                            } catch (IOException e22) {
                                e22.printStackTrace();
                            }
                        }
                        if (baos2 != null) {
                            try {
                                baos2.close();
                            } catch (IOException e222) {
                                e222.printStackTrace();
                                baos = baos2;
                                gzip = gzip2;
                                bis = bis2;
                            }
                        }
                        baos = baos2;
                        gzip = gzip2;
                        bis = bis2;
                    } catch (Exception e3) {
                        ex = e3;
                        gzip = gzip2;
                        bis = bis2;
                        try {
                            ex.printStackTrace();
                            if (bis != null) {
                                try {
                                    bis.close();
                                } catch (IOException e2222) {
                                    e2222.printStackTrace();
                                }
                            }
                            if (gzip != null) {
                                try {
                                    gzip.close();
                                } catch (IOException e22222) {
                                    e22222.printStackTrace();
                                }
                            }
                            if (baos != null) {
                                try {
                                    baos.close();
                                } catch (IOException e222222) {
                                    e222222.printStackTrace();
                                }
                            }
                            return b;
                        } catch (Throwable th3) {
                            th = th3;
                            if (bis != null) {
                                try {
                                    bis.close();
                                } catch (IOException e2222222) {
                                    e2222222.printStackTrace();
                                }
                            }
                            if (gzip != null) {
                                try {
                                    gzip.close();
                                } catch (IOException e22222222) {
                                    e22222222.printStackTrace();
                                }
                            }
                            if (baos != null) {
                                try {
                                    baos.close();
                                } catch (IOException e222222222) {
                                    e222222222.printStackTrace();
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th4) {
                        th = th4;
                        gzip = gzip2;
                        bis = bis2;
                        if (bis != null) {
                            bis.close();
                        }
                        if (gzip != null) {
                            gzip.close();
                        }
                        if (baos != null) {
                            baos.close();
                        }
                        throw th;
                    }
                } catch (Exception e4) {
                    ex = e4;
                    bis = bis2;
                    ex.printStackTrace();
                    if (bis != null) {
                        bis.close();
                    }
                    if (gzip != null) {
                        gzip.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                    return b;
                } catch (Throwable th5) {
                    th = th5;
                    bis = bis2;
                    if (bis != null) {
                        bis.close();
                    }
                    if (gzip != null) {
                        gzip.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                    throw th;
                }
            } catch (Exception e5) {
                ex = e5;
                ex.printStackTrace();
                if (bis != null) {
                    bis.close();
                }
                if (gzip != null) {
                    gzip.close();
                }
                if (baos != null) {
                    baos.close();
                }
                return b;
            }
            return b;
        }
    }

    public static final class DataItemKey {

        public static final class EXTRA {
        }
    }

    public static final String generateId() {
        String head = UUID.randomUUID().toString().substring(0, 8);
        String timeStamp = String.valueOf(getTimeStamp());
        String end = timeStamp.substring(2, timeStamp.length() - 1);
        StringBuffer buf = new StringBuffer();
        buf.append(head).append("_").append(end);
        return buf.toString();
    }

    public static final Long getTimeStamp() {
        return Long.valueOf(System.currentTimeMillis());
    }

    public static final String drillUnzipData(DataItem item) {
        if (item == null) {
            return null;
        }
        try {
            String data = item.getData();
            if (TextUtils.isEmpty(data)) {
                return null;
            }
            data = item.removePrefix(data);
            if (TextUtils.isEmpty(data)) {
                return "";
            }
            try {
                return new String(BytesZipUtil.unGZip(Base64.decode(data, 0)));
            } catch (Exception e) {
                return "";
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static DataBundle createDataItemToDataBundle(DataItem dataItem) {
        DataBundle dataBundle = new DataBundle();
        dataBundle.putString("key_action", dataItem.getAction());
        dataBundle.putString("key_owner", dataItem.getOwner());
        dataBundle.putString("key_url", dataItem.getUrl());
        dataBundle.putInt("key_state", dataItem.getState());
        dataBundle.putString("key_id", dataItem.getIdentifier());
        dataBundle.putString("key_method", dataItem.getMethod());
        dataBundle.putString("key_data", dataItem.getData());
        dataBundle.putInt("key_flags", dataItem.getFlags());
        dataBundle.putLong("key_timeout", dataItem.getTimeout());
        dataBundle.putString("key_extra", dataItem.getExtraData().toString());
        dataBundle.putInt("key_code", dataItem.getCode());
        return dataBundle;
    }

    public static DataItem createDataBundleToDataItem(DataBundle dataBundle) {
        DataItem dataItem = new DataItem();
        dataItem.setAction(dataBundle.getString("key_action"));
        dataItem.setOwner(dataBundle.getString("key_owner"));
        dataItem.setUrl(dataBundle.getString("key_url"));
        dataItem.setState(dataBundle.getInt("key_state"));
        dataItem.setIdentifier(dataBundle.getString("key_id"));
        dataItem.setMethod(dataBundle.getString("key_method"));
        dataItem.setData(dataBundle.getString("key_data"));
        dataItem.setFlags(dataBundle.getInt("key_flags"));
        dataItem.setTimeout(dataBundle.getLong("key_timeout"));
        dataItem.setExtraData(dataBundle.getString("key_extra"));
        dataItem.setCode(dataBundle.getInt("key_code"));
        return dataItem;
    }

    public static synchronized String saveToFileAndManage(String id, String data) {
        String pathAndName;
        synchronized (DataUtils.class) {
            pathAndName = "/sdcard/.dpath" + File.separator + id;
            File f = new File(pathAndName);
            if (f.exists()) {
                f.delete();
            }
            dumpToFile(pathAndName, data);
        }
        return pathAndName;
    }

    private static synchronized void dumpToFile(java.lang.String r13, java.lang.String r14) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Exception block dominator not found, method:com.huami.watch.transport.httpsupport.model.DataUtils.dumpToFile(java.lang.String, java.lang.String):void. bs: [B:6:0x000c, B:29:0x0083]
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:86)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r9 = com.huami.watch.transport.httpsupport.model.DataUtils.class;
        monitor-enter(r9);
        r6 = 0;
        r7 = new java.io.RandomAccessFile;	 Catch:{ FileNotFoundException -> 0x0023 }
        r8 = "rw";	 Catch:{ FileNotFoundException -> 0x0023 }
        r7.<init>(r13, r8);	 Catch:{ FileNotFoundException -> 0x0023 }
        r6 = r7;
    L_0x000c:
        r0 = r14.toString();	 Catch:{ all -> 0x00a1 }
        r4 = r6.length();	 Catch:{ IOException -> 0x00c5 }
        r6.seek(r4);	 Catch:{ IOException -> 0x00c5 }
        r8 = r0.getBytes();	 Catch:{ IOException -> 0x00c5 }
        r6.write(r8);	 Catch:{ IOException -> 0x00c5 }
        r6.close();	 Catch:{ IOException -> 0x00c5 }
    L_0x0021:
        monitor-exit(r9);
        return;
    L_0x0023:
        r1 = move-exception;
        r8 = 0;
        r10 = java.io.File.separator;	 Catch:{ IOException -> 0x0068 }
        r10 = r13.lastIndexOf(r10);	 Catch:{ IOException -> 0x0068 }
        r3 = r13.substring(r8, r10);	 Catch:{ IOException -> 0x0068 }
        r8 = new java.io.File;	 Catch:{ IOException -> 0x0068 }
        r8.<init>(r3);	 Catch:{ IOException -> 0x0068 }
        r8.mkdirs();	 Catch:{ IOException -> 0x0068 }
        r8 = new java.io.File;	 Catch:{ IOException -> 0x0068 }
        r8.<init>(r13);	 Catch:{ IOException -> 0x0068 }
        r8.createNewFile();	 Catch:{ IOException -> 0x0068 }
        r7 = new java.io.RandomAccessFile;	 Catch:{ IOException -> 0x0068 }
        r8 = "rw";	 Catch:{ IOException -> 0x0068 }
        r7.<init>(r13, r8);	 Catch:{ IOException -> 0x0068 }
        if (r7 == 0) goto L_0x00ce;
    L_0x0048:
        r7.close();	 Catch:{ IOException -> 0x004d }
        r6 = r7;
        goto L_0x000c;
    L_0x004d:
        r2 = move-exception;
        r8 = "slog";	 Catch:{ all -> 0x00cb }
        r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00cb }
        r10.<init>();	 Catch:{ all -> 0x00cb }
        r11 = "file close failed : ";	 Catch:{ all -> 0x00cb }
        r10 = r10.append(r11);	 Catch:{ all -> 0x00cb }
        r10 = r10.append(r13);	 Catch:{ all -> 0x00cb }
        r10 = r10.toString();	 Catch:{ all -> 0x00cb }
        android.util.Log.i(r8, r10, r1);	 Catch:{ all -> 0x00cb }
        r6 = r7;
        goto L_0x000c;
    L_0x0068:
        r2 = move-exception;
        r8 = "slog";	 Catch:{ all -> 0x00a4 }
        r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a4 }
        r10.<init>();	 Catch:{ all -> 0x00a4 }
        r11 = "file write failed : ";	 Catch:{ all -> 0x00a4 }
        r10 = r10.append(r11);	 Catch:{ all -> 0x00a4 }
        r10 = r10.append(r13);	 Catch:{ all -> 0x00a4 }
        r10 = r10.toString();	 Catch:{ all -> 0x00a4 }
        android.util.Log.i(r8, r10, r1);	 Catch:{ all -> 0x00a4 }
        if (r6 == 0) goto L_0x0021;
    L_0x0083:
        r6.close();	 Catch:{ IOException -> 0x0087 }
        goto L_0x0021;
    L_0x0087:
        r2 = move-exception;
        r8 = "slog";	 Catch:{ all -> 0x00a1 }
        r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a1 }
        r10.<init>();	 Catch:{ all -> 0x00a1 }
        r11 = "file close failed : ";	 Catch:{ all -> 0x00a1 }
        r10 = r10.append(r11);	 Catch:{ all -> 0x00a1 }
        r10 = r10.append(r13);	 Catch:{ all -> 0x00a1 }
        r10 = r10.toString();	 Catch:{ all -> 0x00a1 }
        android.util.Log.i(r8, r10, r1);	 Catch:{ all -> 0x00a1 }
        goto L_0x0021;
    L_0x00a1:
        r8 = move-exception;
    L_0x00a2:
        monitor-exit(r9);
        throw r8;
    L_0x00a4:
        r8 = move-exception;
        if (r6 == 0) goto L_0x00aa;
    L_0x00a7:
        r6.close();	 Catch:{ IOException -> 0x00ab }
    L_0x00aa:
        throw r8;	 Catch:{ all -> 0x00a1 }
    L_0x00ab:
        r2 = move-exception;	 Catch:{ all -> 0x00a1 }
        r10 = "slog";	 Catch:{ all -> 0x00a1 }
        r11 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a1 }
        r11.<init>();	 Catch:{ all -> 0x00a1 }
        r12 = "file close failed : ";	 Catch:{ all -> 0x00a1 }
        r11 = r11.append(r12);	 Catch:{ all -> 0x00a1 }
        r11 = r11.append(r13);	 Catch:{ all -> 0x00a1 }
        r11 = r11.toString();	 Catch:{ all -> 0x00a1 }
        android.util.Log.i(r10, r11, r1);	 Catch:{ all -> 0x00a1 }
        goto L_0x00aa;	 Catch:{ all -> 0x00a1 }
    L_0x00c5:
        r1 = move-exception;	 Catch:{ all -> 0x00a1 }
        r1.printStackTrace();	 Catch:{ all -> 0x00a1 }
        goto L_0x0021;
    L_0x00cb:
        r8 = move-exception;
        r6 = r7;
        goto L_0x00a2;
    L_0x00ce:
        r6 = r7;
        goto L_0x000c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.transport.httpsupport.model.DataUtils.dumpToFile(java.lang.String, java.lang.String):void");
    }
}
