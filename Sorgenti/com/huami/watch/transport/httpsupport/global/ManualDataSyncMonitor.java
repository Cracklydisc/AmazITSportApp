package com.huami.watch.transport.httpsupport.global;

import com.huami.watch.transport.httpsupport.model.DataItem;
import java.util.Arrays;
import java.util.List;

public class ManualDataSyncMonitor {
    private List<String> mInterestList;

    public static final class TransState {
    }

    public ManualDataSyncMonitor() {
        String[] interest = interestAt();
        if (interest != null && interest.length > 0) {
            this.mInterestList = Arrays.asList(interest);
        }
    }

    protected String[] interestAt() {
        return null;
    }

    public boolean isInterested(String pkgName) {
        if (this.mInterestList == null) {
            return false;
        }
        return this.mInterestList.contains(pkgName);
    }

    public final void notifyStateChangeFor(DataItem item, int state) {
        if (item != null) {
            String pkgName = item.getOwner();
            if (isInterested(pkgName)) {
                String action = item.getAction();
                String time = item.getExtraValByKey("time");
                long timeL = System.currentTimeMillis();
                try {
                    timeL = Long.valueOf(time).longValue();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                onStateChange(pkgName, action, state, timeL);
            }
        }
    }

    protected void onStateChange(String pkgName, String action, int state, long time) {
    }
}
