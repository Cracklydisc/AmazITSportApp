package com.huami.watch.transport.httpsupport.global;

import android.util.SparseBooleanArray;

public class SyncNodeSwitcher {
    private static SyncNodeSwitcher sInstance;
    private SparseBooleanArray mSwitchStateMap;

    private SyncNodeSwitcher() {
        if (this.mSwitchStateMap == null) {
            this.mSwitchStateMap = new SparseBooleanArray();
            this.mSwitchStateMap.put(0, true);
            this.mSwitchStateMap.put(1, true);
        }
    }

    public static synchronized SyncNodeSwitcher self() {
        SyncNodeSwitcher syncNodeSwitcher;
        synchronized (SyncNodeSwitcher.class) {
            if (sInstance == null) {
                sInstance = new SyncNodeSwitcher();
            }
            syncNodeSwitcher = sInstance;
        }
        return syncNodeSwitcher;
    }

    public void changeTo(int switchCode, boolean newState) {
        synchronized (this.mSwitchStateMap) {
            switch (switchCode) {
                case 0:
                    this.mSwitchStateMap.put(switchCode, newState);
                    break;
            }
        }
    }

    public boolean mayGo(int switchCode) {
        boolean z;
        synchronized (this.mSwitchStateMap) {
            z = this.mSwitchStateMap.get(switchCode);
        }
        return z;
    }

    public synchronized void destory() {
        if (this.mSwitchStateMap != null) {
            this.mSwitchStateMap.clear();
        }
        sInstance = null;
    }
}
