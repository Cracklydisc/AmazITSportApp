package com.huami.watch.transport.httpsupport.global;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.control.assist.WearHttpCompanion;
import com.huami.watch.transport.httpsupport.control.service.HttpTransportManager;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.transporter.server.DuplexDataExchangeService;

public class GlobalDataSyncKicker {
    private static final boolean IS_WATCH = GlobalDefine.IS_WATCH;
    private static GlobalDataSyncKicker instance;

    private GlobalDataSyncKicker() {
    }

    public static synchronized GlobalDataSyncKicker getInstance() {
        GlobalDataSyncKicker globalDataSyncKicker;
        synchronized (GlobalDataSyncKicker.class) {
            if (instance == null) {
                instance = new GlobalDataSyncKicker();
            }
            globalDataSyncKicker = instance;
        }
        return globalDataSyncKicker;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void kick(android.content.Context r4, int r5) {
        /*
        r3 = this;
        monitor-enter(r3);
        r0 = IS_WATCH;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x0042;
    L_0x0005:
        r0 = r5 & 1;
        if (r0 == 0) goto L_0x001f;
    L_0x0009:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_BLT;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x0014;
    L_0x000d:
        r0 = "WH-BLT";
        r1 = "[KICK]----> CODE_APP_2_SRV not implement ====> ";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0097 }
    L_0x0014:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0097 }
        r1 = "WH-BLT";
        r2 = "[KICK]----> CODE_APP_2_SRV not implement ====> ";
        r0.with(r1, r2);	 Catch:{ all -> 0x0097 }
    L_0x001f:
        r0 = r5 & 4;
        if (r0 == 0) goto L_0x003c;
    L_0x0023:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_BLT;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x002e;
    L_0x0027:
        r0 = "WH-BLT";
        r1 = "[KICK]----> KICK_CODE_SRV_2_ASSIST  ====> ";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0097 }
    L_0x002e:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0097 }
        r1 = "WH-BLT";
        r2 = "[KICK]----> KICK_CODE_SRV_2_ASSIST ====> ";
        r0.with(r1, r2);	 Catch:{ all -> 0x0097 }
        r3.handleSrv2Assist(r4);	 Catch:{ all -> 0x0097 }
    L_0x003c:
        r0 = r5 & 32;
        if (r0 == 0) goto L_0x0040;
    L_0x0040:
        monitor-exit(r3);
        return;
    L_0x0042:
        r0 = r5 & 8;
        if (r0 == 0) goto L_0x005f;
    L_0x0046:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_BLT;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x0051;
    L_0x004a:
        r0 = "WH-BLT";
        r1 = "[KICK]----> KICK_CODE_ASSIST_2_CLOUD  ====> ";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0097 }
    L_0x0051:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0097 }
        r1 = "WH-BLT";
        r2 = "[KICK]----> KICK_CODE_ASSIST_2_CLOUD ====> ";
        r0.with(r1, r2);	 Catch:{ all -> 0x0097 }
        r3.handleAssist2Cloud(r4);	 Catch:{ all -> 0x0097 }
    L_0x005f:
        r0 = r5 & 16;
        if (r0 == 0) goto L_0x007c;
    L_0x0063:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_BLT;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x006e;
    L_0x0067:
        r0 = "WH-BLT";
        r1 = "[KICK]----> KICK_CODE_ASSIST_2_WATCH  ====> ";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0097 }
    L_0x006e:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0097 }
        r1 = "WH-BLT";
        r2 = "[KICK]----> KICK_CODE_ASSIST_2_WATCH ====> ";
        r0.with(r1, r2);	 Catch:{ all -> 0x0097 }
        r3.handleAssist2Srv();	 Catch:{ all -> 0x0097 }
    L_0x007c:
        r0 = r5 & 64;
        if (r0 == 0) goto L_0x0040;
    L_0x0080:
        r0 = com.huami.watch.transport.httpsupport.GlobalDefine.DEBUG_BLT;	 Catch:{ all -> 0x0097 }
        if (r0 == 0) goto L_0x008b;
    L_0x0084:
        r0 = "WH-BLT";
        r1 = "[KICK]----> KICK_CODE_ASSIST_SYNC_DATA  ====> ";
        android.util.Log.i(r0, r1);	 Catch:{ all -> 0x0097 }
    L_0x008b:
        r0 = clc.utils.debug.slog.SolidLogger.getInstance();	 Catch:{ all -> 0x0097 }
        r1 = "WH-BLT";
        r2 = "[KICK]----> KICK_CODE_ASSIST_SYNC_DATA ====> ";
        r0.with(r1, r2);	 Catch:{ all -> 0x0097 }
        goto L_0x0040;
    L_0x0097:
        r0 = move-exception;
        monitor-exit(r3);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.transport.httpsupport.global.GlobalDataSyncKicker.kick(android.content.Context, int):void");
    }

    private void handleAssist2Cloud(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeInfo = manager.getActiveNetworkInfo();
        if (GlobalDefine.DEBUG_COMPANION) {
            try {
                Log.i("WH-ASSIST", "****handleAssist2Cloud 同步，当前: " + "mobile:" + manager.getNetworkInfo(0).isConnected() + "\n" + "wifi:" + manager.getNetworkInfo(1).isConnected() + "\n" + "active:" + activeInfo.getTypeName());
            } catch (Exception e) {
            }
        }
        try {
            SolidLogger.getInstance().with("WH-ASSIST", "****handleAssist2Cloud 同步，当前: " + "mobile:" + manager.getNetworkInfo(0).isConnected() + "\n" + "wifi:" + manager.getNetworkInfo(1).isConnected() + "\n" + "active:" + activeInfo.getTypeName());
        } catch (Exception e2) {
        }
        if (activeInfo != null && activeInfo.isConnected()) {
            WearHttpCompanion assist = WearHttpCompanion.getInstance();
            if (assist == null) {
                if (GlobalDefine.DEBUG_SRV) {
                    Log.i("WH-SRV", "***同步 WearHttpCompanion(请求网络用的哟) 初始化失败 !*");
                }
                SolidLogger.getInstance().with("WH-SRV", "***WearHttpCompanion(请求网络用的哟) 同步 初始化失败 !*");
                return;
            }
            assist.tryProcessCachedRequestWhileNetWorkAvalible();
        }
    }

    private void handleSrv2Assist(Context context) {
        boolean conned = false;
        startService(context, new Intent());
        if (System.getInt(context.getContentResolver(), "com.huami.watch.extra.DEVICE_CONNECTION_STATUS", 0) > 0 || System.getInt(context.getContentResolver(), "com.huami.watch.extra.conn.ctrl", 0) > 0) {
            conned = true;
        }
        if (GlobalDefine.DEBUG_BLT) {
            Log.i("WH-BLT", "****handleSrv2Assist 同步，当前: " + (conned ? "已连接" : "断开了"));
        }
        SolidLogger.getInstance().with("WH-BLT", "****handleSrv2Assist 同步，当前: " + (conned ? "已连接" : "断开了"));
        if (conned) {
            HttpTransportManager m = HttpTransportManager.getInstance(context);
            if (m == null) {
                if (GlobalDefine.DEBUG_SRV) {
                    Log.i("WH-SRV", "***Http Transport Manager 初始化失败 !*");
                }
                SolidLogger.getInstance().with("WH-SRV", "***Http Transport Manager 初始化失败 !*");
                return;
            }
            if (GlobalDefine.DEBUG_SRV) {
                Log.i("WH-SRV", "handleSrv2Assist===> 即将出发缓存任务");
            }
            SolidLogger.getInstance().with("WH-SRV", "handleSrv2Assist===> 即将出发缓存任务");
            m.tryProcessCachedRequestWhilePhoneAvalible(context);
        }
    }

    private void handleAssist2Srv() {
        WearHttpCompanion assist = WearHttpCompanion.getInstance();
        if (assist != null) {
            assist.tryProcessCachedResponseWhileChannelAvalible();
        }
    }

    private void startService(Context context, Intent source) {
        Intent service = new Intent(context, DuplexDataExchangeService.class);
        service.setAction(source.getAction());
        service.putExtras(source);
        context.startService(service);
    }

    public synchronized DataItem genInternalSyncAction(String pkg, String action) {
        DataItem item;
        item = new DataItem(null, action, pkg, null);
        item.addFlags(8192);
        item.setState(0);
        item.addExtraPair("new-sync", "1");
        return item;
    }

    public synchronized DataItem genInternalEnumCollectAction(String targetPackage, String targetAction, String cmdAction) {
        DataItem item;
        if (TextUtils.isEmpty(targetPackage)) {
            targetPackage = "*";
        }
        if (TextUtils.isEmpty(targetAction)) {
            targetAction = "*";
        }
        item = new DataItem(null, cmdAction, targetPackage, null);
        item.setData("<!@@%C%&&!>");
        item.addFlags(8192);
        item.setState(0);
        item.addExtraPair("target", targetPackage + "#" + targetAction);
        return item;
    }
}
