package com.huami.watch.transport.httpsupport.global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.AlarmDriver;
import com.huami.watch.transport.httpsupport.GlobalDefine;

public class ConnectionReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SolidLogger.withContext(context);
        if (TextUtils.equals(intent.getAction(), "com.huami.watch.httpsupport.COLLECT_DATA")) {
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "------> 收到Call of duty ,设置driver...");
            }
            SolidLogger.getInstance().with("WH-SRV", "------> 收到Call of duty ,设置driver...");
            AlarmDriver.startAlarmDriver(context, intent.getLongExtra("itvl", 3600000));
        } else if (GlobalDefine.IS_WATCH) {
            handleWearServiceOnReceive(context, intent);
        } else {
            handleWearCompanionOnReceive(context, intent);
        }
    }

    private void handleWearCompanionOnReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.equals(action, "android.net.conn.CONNECTIVITY_CHANGE")) {
            GlobalDataSyncKicker.getInstance().kick(context, 8);
        } else if (TextUtils.equals("com.huami.watch.WATCH_CONNED_4_COMPANION", action)) {
            GlobalDataSyncKicker.getInstance().kick(context, 16);
        } else if (TextUtils.equals("com.huami.watch.companion.action.UnbindDeviceStart", action)) {
            if (GlobalDefine.DEBUG_COMPANION) {
                Log.i("WH-ASSIST", "------> 收到手助解绑消息 即将触发，缓存手助信息的重传");
            }
            SolidLogger.getInstance().with("WH-SRV", "------> 收到手助解绑消息 即将触发，缓存手助信息的重传");
            GlobalDataSyncKicker.getInstance().kick(context, 8);
        }
    }

    private void handleWearServiceOnReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.huami.watch.action.DEVICE_CONNECTION_CHANGED".equals(action) || "com.huami.watch.action.DEVICE_CONNECTION_CHANGED_IOS".equals(action)) {
            GlobalDataSyncKicker.getInstance().kick(context, 4);
        }
    }
}
