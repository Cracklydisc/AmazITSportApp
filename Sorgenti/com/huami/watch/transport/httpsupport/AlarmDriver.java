package com.huami.watch.transport.httpsupport;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.util.Calendar;

public class AlarmDriver {
    public static PendingIntent startAlarmDriver(Context context, long interval) {
        if (context == null || interval < 0) {
            return null;
        }
        Intent intent = new Intent("com.huami.watch.httpsupport.COLLECT_DATA");
        intent.putExtra("itvl", interval);
        return inject(context, 1, interval, intent, 0);
    }

    public static void inject(Context context, int alarmType, int hour, int min, int second, long intervalTime, Intent intent, int reqCode) {
        if (context != null) {
            Log.i("HmAlarm", "setCustomRepeatAlarm : time : " + hour + min + second + " intervalTime : " + intervalTime + " Intent : " + intent);
            Calendar currentCalendar = Calendar.getInstance();
            currentCalendar.setTimeInMillis(System.currentTimeMillis());
            Calendar alarmCalendar = Calendar.getInstance();
            alarmCalendar.setTimeInMillis(System.currentTimeMillis());
            alarmCalendar.set(11, hour);
            alarmCalendar.set(12, min);
            alarmCalendar.set(13, second);
            while (alarmCalendar.getTimeInMillis() < currentCalendar.getTimeInMillis()) {
                Log.i("HmAlarm", "alarmCalendar.before(currentCalendar)");
                alarmCalendar.add(13, (int) (intervalTime / 1000));
            }
            int i = alarmType;
            ((AlarmManager) context.getSystemService("alarm")).setRepeating(i, alarmCalendar.getTimeInMillis(), intervalTime, PendingIntent.getBroadcast(context, reqCode, intent, 268435456));
        }
    }

    public static PendingIntent inject(Context context, int alarmType, long repeatInterval, Intent intent, int reqCode) {
        if (context == null) {
            return null;
        }
        Log.i("HmAlarm", ".setCustomRepeatAlarm :  intervalTime : " + repeatInterval + " Intent : " + intent);
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTimeInMillis(System.currentTimeMillis());
        currentCalendar.add(13, (int) (repeatInterval / 1000));
        PendingIntent pi = PendingIntent.getBroadcast(context, reqCode, intent, 268435456);
        ((AlarmManager) context.getSystemService("alarm")).set(alarmType, currentCalendar.getTimeInMillis(), pi);
        return pi;
    }

    public static void cancelAlarmDriver(Context context, PendingIntent pi) {
        if (context != null && pi != null) {
            ((AlarmManager) context.getSystemService("alarm")).cancel(pi);
        }
    }
}
