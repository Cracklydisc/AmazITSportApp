package com.huami.watch.transport.httpsupport.client;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector;
import com.huami.watch.transport.httpsupport.StageFright.EnumCollector.OnReportResultListener;
import com.huami.watch.transport.httpsupport.StageFright.ReportListInfo;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class WearHttpClient {
    private AtomicBoolean mBinded = new AtomicBoolean(false);
    private ComponentName mBindedServiceComponent = null;
    private Context mContext;
    EnumCollector mEnumCollector;
    private BroadcastReceiver mHostReadyReceiver = new C10231();
    private OnServiceStateChangeListener mOnServiceStateChangeListener;
    private ServiceConnection mReadyOnnection = null;
    private Messenger mRemoteService;
    private Messenger mReplyMessenger;
    private Handler mWorkerHandler;
    private HandlerThread mWorkerThread;
    private OnCustomCommandListener onCustomCommandListener;
    private OnQueryCommandListener onQueryCommandListener;

    public interface OnCustomCommandListener {
        void onCustomCommand(DataItem dataItem);
    }

    public interface OnQueryCommandListener {
        void onQueryCommand(DataItem dataItem);
    }

    class C10231 extends BroadcastReceiver {
        C10231() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("com.huami.watch.httpsupport.EXCHANGEE_READY_TO ACCEPT".equalsIgnoreCase(intent.getAction())) {
                WearHttpClient.this.tryToBindService();
            }
        }
    }

    class C10242 extends EnumCollector {
        C10242() {
        }

        protected void data2Upper(DataItem item, OnReportResultListener l) {
            String pkgName = WearHttpClient.this.mContext.getPackageName();
            item.setOwner(pkgName);
            l.onReportResult(WearHttpClient.this.data2Host(pkgName, item));
        }

        protected void data2Lower(DataItem item, OnReportResultListener l) {
        }

        protected void dataLocal2Upper(DataItem item) {
            DataCacher cache = DataCacher.getInstance(WearHttpClient.this.mContext);
            ReportListInfo infoList = new ReportListInfo();
            List<DataItem> list1 = cache.query(8, 0);
            if (list1 != null) {
                for (DataItem i : list1) {
                    infoList.addItem(Integer.valueOf(0), i);
                }
            }
            item.addExtraPair("report-list", infoList.flatten());
            item.setOwner(WearHttpClient.this.mContext.getPackageName());
        }

        protected void onQueryCommandArrival(DataItem command) {
            super.onQueryCommandArrival(command);
            if (WearHttpClient.this.onQueryCommandListener != null) {
                WearHttpClient.this.onQueryCommandListener.onQueryCommand(command);
            }
        }

        protected void triggerPoorEnergy(DataItem item) {
            super.triggerPoorEnergy(item);
            WearHttpClient.this.tryToSendCachedItemToService();
            WearHttpClient.this.triggerPoorEnergy(item);
        }

        protected void onCustomCommand(DataItem customCommand) {
            if (TextUtils.equals(customCommand.getAction(), "sync-internal-for-assist")) {
                if (TextUtils.isEmpty(customCommand.getExtraValByKey("new-sync"))) {
                    Log.i("WH-SERIAL_MODE", "-------------感觉对方是个“旧的”助手");
                    GlobalDefine.saveSyncSupport(false);
                } else {
                    Log.i("WH-SERIAL_MODE", "-------------感觉对方是个“新的”助手");
                    GlobalDefine.saveSyncSupport(true);
                }
            }
            if (WearHttpClient.this.onCustomCommandListener != null) {
                WearHttpClient.this.onCustomCommandListener.onCustomCommand(customCommand);
            }
        }
    }

    class C10273 implements ServiceConnection {

        class C10251 implements DeathRecipient {
            C10251() {
            }

            public void binderDied() {
                if (GlobalDefine.DEBUG_CLIENT) {
                    Log.i("WH-APP", "[CLIENT] ==>  HOST DEAD!");
                }
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>  HOST DEAD!");
                if (WearHttpClient.this.mOnServiceStateChangeListener != null) {
                    WearHttpClient.this.mOnServiceStateChangeListener.onServiceOffline();
                }
                WearHttpClient.this.rezygote();
            }
        }

        class C10262 implements Runnable {
            C10262() {
            }

            public void run() {
                WearHttpClient.this.rezygote();
            }
        }

        C10273() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            WearHttpClient.this.mReadyOnnection = this;
            WearHttpClient.this.mRemoteService = new Messenger(service);
            try {
                WearHttpClient.this.mRemoteService.getBinder().linkToDeath(new C10251(), 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            if (WearHttpClient.this.mOnServiceStateChangeListener != null) {
                WearHttpClient.this.mOnServiceStateChangeListener.onServiceOnline();
            }
            WearHttpClient.this.mBinded.set(true);
            WearHttpClient.this.handshake();
        }

        public void onServiceDisconnected(ComponentName name) {
            if (GlobalDefine.DEBUG_CLIENT) {
                Log.i("WH-APP", "[CLIENT] ==> CLIENT onServiceDisconnected : [" + WearHttpClient.this.mContext.getPackageName() + "] DISCONNECT with host.");
            }
            SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==> CLIENT onServiceDisconnected : [" + WearHttpClient.this.mContext.getPackageName() + "] DISCONNECT with host.");
            WearHttpClient.this.mBinded.set(false);
            WearHttpClient.this.mWorkerHandler.post(new C10262());
        }
    }

    class C10306 implements Runnable {
        C10306() {
        }

        public void run() {
            Object obj = null;
            List<DataItem> items = DataCacher.getInstance(WearHttpClient.this.mContext).query(8, 0);
            if (GlobalDefine.DEBUG_CLIENT) {
                Log.i("WH-APP", "\t\t>>>>>cached client things will launch : " + (items == null ? null : Integer.valueOf(items.size())));
            }
            SolidLogger instance = SolidLogger.getInstance();
            String str = "WH-APP";
            StringBuilder append = new StringBuilder().append("\t\t>>>>>cached client things will launch : ");
            if (items != null) {
                obj = Integer.valueOf(items.size());
            }
            instance.with(str, append.append(obj).toString());
            if (items != null) {
                for (DataItem item : items) {
                    item.setState(5);
                    WearHttpClient.this.data2Host(item.getOwner(), item);
                }
                DataCacher.getInstance(WearHttpClient.this.mContext).deleteByState(8);
            }
        }
    }

    public void setOnCustomCommandListener(OnCustomCommandListener l) {
        this.onCustomCommandListener = l;
    }

    public void setOnQueryCommandListener(OnQueryCommandListener l) {
        this.onQueryCommandListener = l;
    }

    public WearHttpClient(Context context) {
        this.mContext = context;
        initHandler();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watch.httpsupport.EXCHANGEE_READY_TO ACCEPT");
        this.mContext.registerReceiver(this.mHostReadyReceiver, filter);
        doInitEnumCollector();
        tryToBindService();
    }

    private void doInitEnumCollector() {
        this.mEnumCollector = new C10242();
        this.mEnumCollector.setName("EC-WearHttpClient");
    }

    protected void triggerPoorEnergy(DataItem item) {
    }

    private synchronized ServiceConnection generateConnection() {
        return new C10273();
    }

    private void tryToBindService() {
        if (GlobalDefine.DEBUG_CLIENT) {
            Log.i("WH-APP", this.mContext.getPackageName() + " try to rebind host data service <><><><>");
        }
        SolidLogger.getInstance().with("WH-APP", this.mContext.getPackageName() + " try to rebind host data service <><><><>");
        try {
            String str;
            Intent service = new Intent("com.huami.watch.transport.exchangee.HOST_SERVICE");
            service.setPackage("com.huami.watch.wearservices");
            service.putExtra("@@@inner-key[![[--]", this.mContext.getPackageName());
            this.mBindedServiceComponent = this.mContext.startService(service);
            if (this.mBindedServiceComponent == null) {
                if (GlobalDefine.DEBUG_CLIENT) {
                    Log.i("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 失败");
                }
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 失败");
            } else {
                if (GlobalDefine.DEBUG_CLIENT) {
                    Log.i("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 成功");
                }
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 成功");
            }
            boolean res = this.mContext.bindService(service, generateConnection(), 8);
            if (GlobalDefine.DEBUG_CLIENT) {
                Log.i("WH-APP", "[CLIENT] ==> [" + this.mContext.getPackageName() + "] client bind service res================> : " + res);
            }
            SolidLogger instance = SolidLogger.getInstance();
            String str2 = "WH-APP";
            StringBuilder append = new StringBuilder().append("[CLIENT] ==> [").append(this.mContext.getPackageName()).append("] client bind service res================> : ");
            if (res) {
                str = "SUCCESS";
            } else {
                str = "FAILED";
            }
            instance.with(str2, append.append(str).toString());
        } catch (Exception e) {
            if (GlobalDefine.DEBUG_CLIENT) {
                Log.i("WH-APP", "[CLIENT] ==> bind service failed : ", e);
            }
            SolidLogger.getInstance().with("WH-APP", "[CLIENT]" + this.mContext.getPackageName() + " ==> bind service failed ");
        }
    }

    private void rezygote() {
        if (GlobalDefine.DEBUG_CLIENT) {
            Log.i("WH-APP", "[CLIENT] ==> ##########REZYGOTE REMOTE############ :" + this.mBindedServiceComponent);
        }
        try {
            if (this.mBindedServiceComponent != null) {
                Intent i = new Intent();
                i.setComponent(this.mBindedServiceComponent);
                this.mContext.stopService(i);
            }
            this.mContext.unbindService(this.mReadyOnnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tryToBindService();
    }

    private void initHandler() {
        if (this.mWorkerThread == null) {
            this.mWorkerThread = new HandlerThread("wear-http-client-handler");
            this.mWorkerThread.start();
        }
        if (this.mWorkerHandler == null) {
            this.mWorkerHandler = new Handler(this.mWorkerThread.getLooper()) {
                public void handleMessage(Message msg) {
                    if (GlobalDefine.DEBUG_CLIENT) {
                        Log.i("WH-APP", "client '" + WearHttpClient.this.mContext.getPackageName() + "' received msg !" + msg);
                    }
                    SolidLogger.getInstance().with("WH-APP", "client '" + WearHttpClient.this.mContext.getPackageName() + "' received msg !" + msg);
                    WearHttpClient.this.receiveRemoteMsg(msg);
                }
            };
        }
        if (this.mReplyMessenger == null) {
            this.mReplyMessenger = new Messenger(this.mWorkerHandler);
        }
    }

    private void receiveRemoteMsg(Message msg) {
        Bundle data = msg.getData();
        if (data != null) {
            dataFromHost(data.getString("@@@inner-key[![[--][data]"));
        }
    }

    protected void dataFromHost(String dataRev) {
        if (GlobalDefine.DEBUG_CLIENT) {
            Log.i("WH-APP", "'" + this.mContext.getPackageName() + "' received data from host! " + DataItem.from(dataRev).toShortString());
        }
        SolidLogger.getInstance().with("WH-APP", "'" + this.mContext.getPackageName() + "' received data from host! " + DataItem.from(dataRev).toShortString());
    }

    int data2Host(String pkg, final DataItem item) {
        Message message = Message.obtain();
        Bundle b = new Bundle();
        b.putString("@@@inner-key[![[--][data]", item.toString());
        b.putString("@@@inner-key[![[--][pkg]", pkg);
        message.setData(b);
        message.replyTo = this.mReplyMessenger;
        try {
            this.mRemoteService.send(message);
        } catch (Exception e) {
            if (e instanceof TransactionTooLargeException) {
                item.addFlags(32768);
                item.addExtraPair("d-path", DataUtils.saveToFileAndManage(item.getIdentifier(), item.getData()));
                item.setData(null);
                b.putString("@@@inner-key[![[--][data]", item.toString());
                try {
                    this.mRemoteService.send(message);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            } else {
                e.printStackTrace();
                tryToBindService();
                this.mWorkerHandler.post(new Runnable() {
                    public void run() {
                        if (GlobalDefine.DEBUG_CLIENT) {
                            Log.i("WH-APP", "CACHED in Client :" + item.getIdentifier());
                        }
                        SolidLogger.getInstance().with("WH-APP", "CACHED in Client :" + item.getIdentifier());
                        DataCacher cacher = DataCacher.getInstance(WearHttpClient.this.mContext);
                        item.setState(8);
                        cacher.save(item);
                    }
                });
            }
        }
        return 0;
    }

    private synchronized void handshake() {
        String pkg = this.mContext.getPackageName();
        if (GlobalDefine.DEBUG_CLIENT) {
            Log.i("WH-APP", "\t\t>>>>>> [client] '" + pkg + "' req hand shake ...");
        }
        SolidLogger.getInstance().with("WH-APP", ">>>>>> [client] '" + pkg + "' req hand shake ...");
        Message message = Message.obtain();
        message.what = 2147483628;
        Bundle b = new Bundle();
        b.putString("@@@inner-key[![[--][pkg]", pkg);
        message.setData(b);
        message.replyTo = this.mReplyMessenger;
        try {
            this.mRemoteService.send(message);
            tryToSendCachedItemToService();
        } catch (RemoteException e) {
            e.printStackTrace();
            tryToBindService();
        }
    }

    private synchronized void tryToSendCachedItemToService() {
        if (this.mWorkerHandler != null) {
            this.mWorkerHandler.post(new C10306());
        }
    }
}
