package com.huami.watch.transport.httpsupport.client;

public interface OnServiceStateChangeListener {
    void onServiceOffline();

    void onServiceOnline();
}
