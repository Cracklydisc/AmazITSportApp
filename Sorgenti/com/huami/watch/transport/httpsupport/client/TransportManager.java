package com.huami.watch.transport.httpsupport.client;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.StageFright.Command;
import com.huami.watch.transport.httpsupport.Utils;
import com.huami.watch.transport.httpsupport.client.WearHttpClient.OnCustomCommandListener;
import com.huami.watch.transport.httpsupport.client.WearHttpClient.OnQueryCommandListener;
import com.huami.watch.transport.httpsupport.global.GlobalDataSyncKicker;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.translogutils.DataParser;
import com.huami.watch.transport.httpsupport.translogutils.TransLogs;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public class TransportManager implements OnCustomCommandListener, OnQueryCommandListener {
    private WearHttpClient mClient = null;
    private Context mContext;
    private OnCmdArrivalListener mOnCmdArrivalListener;
    private OnDataArrivalListener mOnDataArrivalListener;
    private AtomicBoolean mSerialMode = new AtomicBoolean(false);
    private TaskManager mTaskManager = new TaskManager("file-manage");

    public interface OnDataArrivalListener {
        void onDataArrival(DataItem dataItem);
    }

    public interface OnCmdArrivalListener {
        void onCmdArrival(DataItem dataItem);
    }

    public void onCustomCommand(DataItem commandItem) {
        if (TextUtils.equals(commandItem.getAction(), "sync-internal-for-assist")) {
            boolean needDo = doRemainsDataTrans();
            Log.i("test_trans_log", "doRemainsDataTrans()..." + needDo);
            if (!needDo) {
                tellUploadResultForMe(true, null, null);
            }
        } else if (TextUtils.equals(commandItem.getAction(), "_arrival_token") && this.mOnCmdArrivalListener != null) {
            Log.i("wifi_trans", "trans---onCustomCommand--" + commandItem.toString());
            this.mOnCmdArrivalListener.onCmdArrival(commandItem);
        }
    }

    public void onQueryCommand(DataItem commandItem) {
    }

    public void setOnDataArrivalListener(OnDataArrivalListener l) {
        this.mOnDataArrivalListener = l;
    }

    public void setAsSerialMode(boolean serialMode) {
        this.mSerialMode.set(serialMode);
    }

    public TransportManager(Context context) {
        if (context == null) {
            throw new NullPointerException("Context is NULL for transport manager.");
        }
        this.mContext = context.getApplicationContext();
        if (this.mContext == null) {
            this.mContext = context;
        }
        SolidLogger.withContext(context);
        initClient(context);
    }

    private void initClient(Context context) {
        if (this.mClient == null) {
            this.mClient = new WearHttpClient(context.getApplicationContext()) {
                protected void dataFromHost(String dataRev) {
                    super.dataFromHost(dataRev);
                    DataItem item = DataItem.from(dataRev);
                    if (Command.isInternal(item.getAction())) {
                        this.mEnumCollector.addActionAndExecute(item);
                        return;
                    }
                    String dpath = item.getExtraValByKey("d-path");
                    if (!TextUtils.isEmpty(dpath)) {
                        TransportManager.this.fillDataByFile(dpath, item);
                    }
                    if (TransportManager.this.mOnDataArrivalListener != null) {
                        if (GlobalDefine.DEBUG_CLIENT) {
                            Log.i("WH-APP", "onDataArrival for : " + item.getIdentifier());
                        }
                        SolidLogger.getInstance().with("WH-APP", "onDataArrival for : " + item.getIdentifier());
                        TransportManager.this.mOnDataArrivalListener.onDataArrival(item);
                        TransLogs.m11i(new DataParser(item, 2).getImportanceInfo());
                    }
                }

                protected void triggerPoorEnergy(DataItem item) {
                    TransportManager.this.triggerPoorEnergy(item);
                }
            };
        }
        this.mClient.setOnCustomCommandListener(this);
        this.mClient.setOnQueryCommandListener(this);
    }

    protected void triggerPoorEnergy(DataItem item) {
    }

    private void fillDataByFile(final String path, DataItem bean) {
        String data = Utils.readFileToString(path);
        bean.removeExtraPairByKey("d-path");
        bean.setData(data);
        this.mTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                new File(path).delete();
                return null;
            }
        }).execute();
    }

    private void sendInternal(DataItem item) {
        if (this.mSerialMode.get()) {
            item.addFlags(16384);
        } else {
            item.clearFlags(16384);
        }
        SolidLogger.getInstance().with("WH-APP", ".2 Send Req for " + item.toShortString() + "...");
        if (checkService()) {
            item.setFlags(item.getFlags() | 1);
            if (this.mClient == null) {
                initClient(this.mContext);
            }
            if (this.mClient != null) {
                String pkg = item.getOwner();
                if (TextUtils.isEmpty(pkg)) {
                    if (GlobalDefine.DEBUG_CLIENT) {
                        Log.i("WH-APP", " pkg is null for sender...");
                    }
                    SolidLogger.getInstance().with("WH-APP", " pkg is null for sender...");
                    return;
                }
                this.mClient.data2Host(pkg, item);
            }
        }
    }

    public final void sendSync(DataItem item) {
        sendInternal(item);
    }

    private boolean checkService() {
        return true;
    }

    protected boolean doRemainsDataTrans() {
        return false;
    }

    public final void tellUploadResultForMe(boolean success, String time, String data) {
        String pkg = this.mContext.getPackageName();
        DataItem item = GlobalDataSyncKicker.getInstance().genInternalSyncAction(pkg, "tell-internal-for-assist");
        item.addExtraPair("time", time);
        item.addExtraPair("data", data);
        item.addExtraPair("res", String.valueOf(success));
        item.setState(5);
        this.mClient.data2Host(pkg, item);
    }

    public final void triggerUploadManual(String targetAction) {
        triggerUploadManual(this.mContext.getPackageName(), targetAction);
    }

    public final void triggerUploadManual(String targetPackage, String targetAction) {
        try {
            DataItem item = GlobalDataSyncKicker.getInstance().genInternalEnumCollectAction(targetPackage, targetAction, "_client_trigger_report");
            item.setOwner(this.mContext.getPackageName());
            this.mClient.data2Host(targetPackage, item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
