package com.huami.watch.transport.httpsupport.autotrigger;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Global;
import android.util.Log;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmQueue.AutoAlarm;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmQueue.AutoAlarmAlertListener;

public class TransportDataAutomaticAlarm {
    public static String name = "automatic_trans_alert";
    private Context context;

    class C10181 implements AutoAlarmAlertListener {
        final /* synthetic */ TransportDataAutomaticAlarm this$0;

        public boolean autoAlarmAlert(AutoAlarm autoAlarm) {
            boolean isAirPlane = false;
            Log.i(TransportDataAutomaticAlarm.name, "autoAlarmAlert:com.huami.watch.http_support.TRANS_AUTOMATIC");
            if (Global.getInt(this.this$0.context.getContentResolver(), "airplane_mode_on", 0) == 1) {
                isAirPlane = true;
            }
            if (isAirPlane) {
                Log.i(TransportDataAutomaticAlarm.name, "maybe switched on the air plane mode...");
            } else {
                this.this$0.context.sendBroadcast(new Intent("com.huami.watch.http_support.TRANS_AUTOMATIC"));
                Log.i(TransportDataAutomaticAlarm.name, "send:com.huami.watch.http_support.TRANS_AUTOMATIC");
            }
            return true;
        }
    }
}
