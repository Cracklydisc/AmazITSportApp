package com.huami.watch.transport.httpsupport.autotrigger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.transport.DataTransportResult;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.ChannelListener;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import com.huami.watch.transport.Transporter.ServiceConnectionListener;

public class AutoTransTriggerTransporter implements ChannelListener, DataListener, ServiceConnectionListener {
    private static final String[] apps = new String[]{"com.huami.watch.health", "com.huami.watch.sport"};
    private static AutoTransTriggerTransporter instance;
    private Context mContext;
    private BroadcastReceiver receiver;
    private Transporter transporter;

    class C10161 extends BroadcastReceiver {
        C10161() {
        }

        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals("com.huami.watch.http_support.TRANS_AUTOMATIC", intent.getAction())) {
                Log.i("trans_data", "close auto blue trans already ...");
            }
        }
    }

    class C10172 implements DataSendResultCallback {
        public void onResultBack(DataTransportResult result) {
            if (result == null) {
                Log.i("trans_data", "trigger sync data send null result...");
                return;
            }
            Log.i("trans_data", "trigger sync data send result:" + (result.getResultCode() == 0 ? "OK" : "FAILED ,CODE:" + result.getResultCode()));
        }
    }

    public static AutoTransTriggerTransporter getInstance(Context context) {
        if (instance == null) {
            synchronized (AutoTransTriggerTransporter.class) {
                if (instance == null) {
                    instance = new AutoTransTriggerTransporter(context);
                }
            }
        }
        return instance;
    }

    public AutoTransTriggerTransporter(Context context) {
        this.mContext = context;
        initTransporter();
        initReceiver();
    }

    private void initReceiver() {
        if (this.receiver == null) {
            this.receiver = new C10161();
        }
        this.mContext.registerReceiver(this.receiver, new IntentFilter("com.huami.watch.http_support.TRANS_AUTOMATIC"));
        Log.i("trans_data", "registerReceiver");
    }

    private void initTransporter() {
        this.transporter = Transporter.get(this.mContext, "com.huami.watch.companion.syncdata");
        this.transporter.addDataListener(this);
        this.transporter.addChannelListener(this);
        this.transporter.addServiceConnectionListener(this);
        this.transporter.connectTransportService();
    }

    public void clear() {
        if (this.transporter != null) {
            this.transporter.removeServiceConnectionListener(this);
            this.transporter.removeDataListener(this);
            this.transporter.removeChannelListener(this);
        }
        if (!(this.receiver == null || this.mContext == null)) {
            this.mContext.unregisterReceiver(this.receiver);
            Log.i("trans_data", "unregisterReceiver");
        }
        instance = null;
    }

    public void onChannelChanged(boolean available) {
        Log.i("trans_data", "onChannelChanged:" + available);
    }

    public void onDataReceived(TransportDataItem item) {
        Log.i("trans_data", "onDataReceived");
    }
}
