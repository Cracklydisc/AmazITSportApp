package com.huami.watch.transport.httpsupport.autotrigger;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Global;
import android.util.Log;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmQueue.AutoAlarm;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmQueue.AutoAlarmAlertListener;
import java.util.ArrayList;
import java.util.List;

public class WifiTransAutomaticAlarm {
    public static String name = "auto_wifi_upload_alert";
    private List<AutoAlarm> autoAlarmList;
    private Context context;
    private AutoAlarmAlertListener listener;

    class C10191 implements AutoAlarmAlertListener {
        C10191() {
        }

        public boolean autoAlarmAlert(AutoAlarm autoAlarm) {
            boolean isAirPlane = false;
            Log.i(WifiTransAutomaticAlarm.name, "autoAlarmAlert:com.huami.watch.wifiuploaddata.AUTOMATIC");
            if (Global.getInt(WifiTransAutomaticAlarm.this.context.getContentResolver(), "airplane_mode_on", 0) == 1) {
                isAirPlane = true;
            }
            if (isAirPlane) {
                Log.i(WifiTransAutomaticAlarm.name, "maybe switched on the air plane mode...");
            } else {
                WifiTransAutomaticAlarm.this.context.sendBroadcast(new Intent("com.huami.watch.wifiuploaddata.AUTOMATIC"));
                Log.i(WifiTransAutomaticAlarm.name, "send:com.huami.watch.wifiuploaddata.AUTOMATIC");
            }
            return true;
        }
    }

    public WifiTransAutomaticAlarm(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        this.listener = new C10191();
        this.autoAlarmList = new ArrayList();
        this.autoAlarmList.add(new AutoAlarm(2000, 11, 0, 30, this.listener));
        this.autoAlarmList.add(new AutoAlarm(2001, 22, 0, 30, this.listener));
    }

    public List<AutoAlarm> getAutoAlarmList() {
        return this.autoAlarmList;
    }

    public String getName() {
        return name;
    }
}
