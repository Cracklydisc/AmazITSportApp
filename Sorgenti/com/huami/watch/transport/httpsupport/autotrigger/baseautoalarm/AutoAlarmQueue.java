package com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class AutoAlarmQueue {
    private int ID = (hashCode() + new Random(100).nextInt());
    private List<AutoAlarm> alarms;
    private AutoAlarm currentAlarm;
    private Context mContext;
    private String name;

    public interface AutoAlarmAlertListener {
        boolean autoAlarmAlert(AutoAlarm autoAlarm);
    }

    class C10201 implements Comparator<AutoAlarm> {
        C10201() {
        }

        public int compare(AutoAlarm lhs, AutoAlarm rhs) {
            try {
                if (lhs.getUtcTime() > rhs.getUtcTime()) {
                    return 1;
                }
                if (lhs.getUtcTime() < rhs.getUtcTime()) {
                    return -1;
                }
                return lhs.getUtcTime() == rhs.getUtcTime() ? 0 : 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
    }

    public static class AutoAlarm {
        private int hour;
        private int id;
        private int jitter;
        public AutoAlarmAlertListener listener;
        private int minute;

        public AutoAlarm(int id, int hour, int minute, int jitter, AutoAlarmAlertListener listener) {
            this.id = id;
            this.hour = hour;
            this.minute = minute;
            this.jitter = jitter;
            this.listener = listener;
        }

        public int getId() {
            return this.id;
        }

        long getUtcAddJitter() {
            Calendar calender = Calendar.getInstance();
            calender.setTimeInMillis(getUTC());
            calender.add(12, (int) (Math.random() * ((double) this.jitter)));
            return calender.getTimeInMillis();
        }

        long getUtcTime() {
            return getUTC();
        }

        long nextDayUTC(boolean isJitter) {
            Calendar calender = Calendar.getInstance();
            calender.setTimeInMillis(getUtcTime());
            calender.add(5, 1);
            if (isJitter) {
                calender.add(12, (int) (Math.random() * ((double) this.jitter)));
            }
            return calender.getTimeInMillis();
        }

        private long getUTC() {
            Calendar calendar = Calendar.getInstance();
            calendar.set(11, this.hour);
            calendar.set(12, this.minute);
            return calendar.getTimeInMillis();
        }
    }

    public AutoAlarmQueue(Context context, String name, List<AutoAlarm> autoAlarms) {
        this.mContext = context;
        this.name = name;
        this.alarms = autoAlarms;
        init();
    }

    private void init() {
        sort();
        settingAlarmClock(this.mContext);
        Log.i(this.name, "init");
    }

    private void settingAlarmClock(Context context) {
        if (context != null) {
            long alarmTime = calculateTime();
            Log.i(this.name, "settingAlarmClock:" + alarmTime + ",id:" + this.ID);
            Intent intent = new Intent("com.huami.watch.http_support.AUTOMATIC_TRANS_DATA");
            intent.putExtra("name", this.name);
            intent.putExtra("id", this.currentAlarm.getId());
            ((AlarmManager) context.getSystemService("alarm")).setExact(0, alarmTime, PendingIntent.getBroadcast(context, this.ID, intent, 134217728));
            saveNextAlarmTime(context, alarmTime);
        }
    }

    private void cancelAlarm(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, this.ID, new Intent("com.huami.watch.http_support.AUTOMATIC_TRANS_DATA"), 134217728));
        Log.i(this.name, "cancelAlarm:" + this.ID);
    }

    public void timeChanged(Context context) {
        Log.i(this.name, "timeChanged:" + this.name);
        if (System.currentTimeMillis() < getNextAlarmTime(context)) {
            cancelAlarm(this.mContext);
            settingAlarmClock(context);
        }
    }

    public void notice(Context context, long id) {
        for (AutoAlarm aul : this.alarms) {
            if (!(aul == null || aul.listener == null || ((long) aul.id) != id)) {
                boolean isRefreshAlarm = aul.listener.autoAlarmAlert(aul);
                Log.i(this.name, "notice:" + aul.id);
                if (isRefreshAlarm) {
                    settingAlarmClock(context);
                }
            }
        }
    }

    private long calculateTime() {
        long timeNow = Calendar.getInstance().getTimeInMillis();
        int i = 0;
        while (i < this.alarms.size()) {
            if (i == this.alarms.size() - 1) {
                this.currentAlarm = (AutoAlarm) this.alarms.get(0);
                return ((AutoAlarm) this.alarms.get(0)).nextDayUTC(true);
            } else if (timeNow < ((AutoAlarm) this.alarms.get(i)).getUtcTime()) {
                this.currentAlarm = (AutoAlarm) this.alarms.get(i);
                return ((AutoAlarm) this.alarms.get(i)).getUtcAddJitter();
            } else if (timeNow < ((AutoAlarm) this.alarms.get(i)).getUtcTime() || timeNow > ((AutoAlarm) this.alarms.get(i + 1)).getUtcTime()) {
                i++;
            } else {
                this.currentAlarm = (AutoAlarm) this.alarms.get(i + 1);
                return ((AutoAlarm) this.alarms.get(i + 1)).getUtcAddJitter();
            }
        }
        return 0;
    }

    private void sort() {
        Collections.sort(this.alarms, new C10201());
    }

    public void clear() {
    }

    private void saveNextAlarmTime(Context context, long time) {
        context.getSharedPreferences(this.name, 0).edit().putLong(this.name, time).apply();
    }

    private long getNextAlarmTime(Context context) {
        return context.getSharedPreferences(this.name, 0).getLong(this.name, 0);
    }
}
