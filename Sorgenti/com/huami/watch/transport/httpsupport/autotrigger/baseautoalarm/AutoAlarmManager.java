package com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.transport.httpsupport.autotrigger.baseautoalarm.AutoAlarmQueue.AutoAlarm;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class AutoAlarmManager {
    public static String TAG = AutoAlarmManager.class.getName();
    private static AutoAlarmManager instance;
    private HashMap<String, AutoAlarmQueue> autoAlarmQueues;
    private AutoAlarmBroadCastReceiver broadCastReceiver = new AutoAlarmBroadCastReceiver();
    private Context mContext;

    public class AutoAlarmBroadCastReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Log.i(AutoAlarmBroadCastReceiver.class.getName(), "receive:" + intent.getAction());
            if (TextUtils.equals("android.intent.action.TIMEZONE_CHANGED", intent.getAction()) || TextUtils.equals("android.intent.action.DATE_CHANGED", intent.getAction()) || TextUtils.equals("android.intent.action.TIME_SET", intent.getAction())) {
                AutoAlarmManager.getInstance(context).onTimeChanged();
            } else if (TextUtils.equals(intent.getAction(), "com.huami.watch.http_support.AUTOMATIC_TRANS_DATA")) {
                AutoAlarmManager.getInstance(context).onAlert(intent.getStringExtra("name"), (long) intent.getIntExtra("id", 0));
            }
        }
    }

    private AutoAlarmManager(Context context) {
        this.mContext = context;
        this.autoAlarmQueues = new HashMap();
        init();
    }

    private void init() {
        if (this.mContext != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.huami.watch.http_support.AUTOMATIC_TRANS_DATA");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.DATE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_SET");
            this.mContext.registerReceiver(this.broadCastReceiver, intentFilter);
        }
    }

    public static AutoAlarmManager getInstance(Context context) {
        if (instance == null) {
            synchronized (AutoAlarmManager.class) {
                if (instance == null) {
                    instance = new AutoAlarmManager(context);
                }
            }
        }
        return instance;
    }

    public AutoAlarmManager addAutoAlarms(String name, List<AutoAlarm> autoAlarms) {
        this.autoAlarmQueues.put(name, new AutoAlarmQueue(this.mContext, name, autoAlarms));
        Log.i(TAG, "autoAlarmQueues.add:" + name);
        return this;
    }

    public void onTimeChanged() {
        Log.i(TAG, "onTimeChanged");
        Set<String> set = this.autoAlarmQueues.keySet();
        if (this.autoAlarmQueues != null && set.size() > 0) {
            for (String key : set) {
                ((AutoAlarmQueue) this.autoAlarmQueues.get(key)).timeChanged(this.mContext);
            }
        }
    }

    public void onAlert(String name, long id) {
        if (this.autoAlarmQueues != null && this.autoAlarmQueues.size() > 0) {
            AutoAlarmQueue queue = (AutoAlarmQueue) this.autoAlarmQueues.get(name);
            if (queue != null) {
                queue.notice(this.mContext, id);
                Log.i(TAG, "onAlert:" + name + ",id:" + id);
            }
        }
    }

    public void clearAll() {
        Set<String> set = this.autoAlarmQueues.keySet();
        if (this.autoAlarmQueues != null && set.size() > 0) {
            for (String key : set) {
                AutoAlarmQueue queue = (AutoAlarmQueue) this.autoAlarmQueues.get(key);
                if (queue != null) {
                    queue.clear();
                }
            }
            if (!(this.mContext == null || this.broadCastReceiver == null)) {
                this.mContext.unregisterReceiver(this.broadCastReceiver);
            }
            instance = null;
            Log.i(TAG, "clearAll");
        }
    }
}
