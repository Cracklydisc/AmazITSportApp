package com.huami.watch.transport;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITransportDataListener extends IInterface {

    public static abstract class Stub extends Binder implements ITransportDataListener {

        private static class Proxy implements ITransportDataListener {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onDataReceived(TransportDataItem mTransportDataItem) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportDataListener");
                    if (mTransportDataItem != null) {
                        _data.writeInt(1);
                        mTransportDataItem.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.transport.ITransportDataListener");
        }

        public static ITransportDataListener asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.transport.ITransportDataListener");
            if (iin == null || !(iin instanceof ITransportDataListener)) {
                return new Proxy(obj);
            }
            return (ITransportDataListener) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    TransportDataItem _arg0;
                    data.enforceInterface("com.huami.watch.transport.ITransportDataListener");
                    if (data.readInt() != 0) {
                        _arg0 = (TransportDataItem) TransportDataItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onDataReceived(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.transport.ITransportDataListener");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onDataReceived(TransportDataItem transportDataItem) throws RemoteException;
}
