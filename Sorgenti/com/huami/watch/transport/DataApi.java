package com.huami.watch.transport;

import android.os.RemoteException;
import android.support.v4.util.ArrayMap;
import com.huami.watch.transport.ITransportDataListener.Stub;
import com.huami.watch.util.Config;

public class DataApi {
    public static final boolean DEBUG = Config.isDebug();
    private static DataApi mDataApi;
    private boolean hasRegister = false;
    private ITransportDataListener mDataListener = new TransportDataListener();
    private ArrayMap<String, DataListener> mListenerList = new ArrayMap();

    public interface CallbackListener {
        void onResultBack(DataTransportResult dataTransportResult);
    }

    public interface DataListener {
        void onDataReceived(TransportDataItem transportDataItem);
    }

    private class TransportDataListener extends Stub {
        private TransportDataListener() {
        }

        public void onDataReceived(TransportDataItem mTransportDataItem) throws RemoteException {
            DataListener listener = (DataListener) DataApi.this.mListenerList.get(mTransportDataItem.getModuleName());
            if (listener != null) {
                listener.onDataReceived(mTransportDataItem);
            }
        }
    }

    public static DataApi getInstance() {
        if (mDataApi == null) {
            mDataApi = new DataApi();
        }
        return mDataApi;
    }

    protected DataApi() {
    }
}
