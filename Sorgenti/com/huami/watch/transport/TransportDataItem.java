package com.huami.watch.transport;

import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Date;

public class TransportDataItem implements Parcelable {
    public static final Creator<TransportDataItem> CREATOR = new C09941();
    private DataBundle data;
    private long time;
    private Uri uri;
    private Builder uriBuild = new Builder();

    static class C09941 implements Creator<TransportDataItem> {
        C09941() {
        }

        public TransportDataItem createFromParcel(Parcel source) {
            return new TransportDataItem(source);
        }

        public TransportDataItem[] newArray(int size) {
            return new TransportDataItem[size];
        }
    }

    public TransportDataItem(String name) {
        this.uriBuild.authority(name.trim());
        this.uriBuild.scheme("Transport");
        this.uriBuild.appendPath("action");
        this.uri = this.uriBuild.build();
        this.time = new Date().getTime();
    }

    public void addAction(String action) {
        Builder builder = this.uri.buildUpon();
        builder.appendEncodedPath(action);
        this.uri = builder.build();
    }

    public String getAction() {
        return this.uri.getLastPathSegment();
    }

    public String getModuleName() {
        return this.uri.getAuthority();
    }

    public String getUri() {
        return this.uri.toString();
    }

    public long getCreateTime() {
        return this.time;
    }

    public TransportDataItem(Parcel source) {
        this.uri = Uri.parse(source.readString());
        this.time = source.readLong();
        this.data = new DataBundle(source, source.readInt());
        this.data.setClassLoader(getClass().getClassLoader());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uri.toString());
        dest.writeLong(this.time);
        this.data.writeToParcel(dest, 0);
    }

    public String toString() {
        return "TransportDataItem [action=" + this.uri + "] [map=" + this.data.toString() + "] [time=" + this.time + "]";
    }

    public int describeContents() {
        return 0;
    }

    public DataBundle getData() {
        return this.data;
    }

    public void setData(DataBundle data) {
        this.data = data;
    }
}
