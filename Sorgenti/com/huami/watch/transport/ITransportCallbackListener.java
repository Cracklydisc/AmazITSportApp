package com.huami.watch.transport;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITransportCallbackListener extends IInterface {

    public static abstract class Stub extends Binder implements ITransportCallbackListener {

        private static class Proxy implements ITransportCallbackListener {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onResultBack(DataTransportResult result) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.transport.ITransportCallbackListener");
                    if (result != null) {
                        _data.writeInt(1);
                        result.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.transport.ITransportCallbackListener");
        }

        public static ITransportCallbackListener asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.transport.ITransportCallbackListener");
            if (iin == null || !(iin instanceof ITransportCallbackListener)) {
                return new Proxy(obj);
            }
            return (ITransportCallbackListener) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    DataTransportResult _arg0;
                    data.enforceInterface("com.huami.watch.transport.ITransportCallbackListener");
                    if (data.readInt() != 0) {
                        _arg0 = (DataTransportResult) DataTransportResult.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onResultBack(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.transport.ITransportCallbackListener");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onResultBack(DataTransportResult dataTransportResult) throws RemoteException;
}
