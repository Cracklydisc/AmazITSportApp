package com.huami.watch.transport;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.util.LongSparseArray;
import com.huami.watch.transport.ITransportCallbackListener.Stub;
import com.huami.watch.transport.Transporter.ChannelListener;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import com.huami.watch.transport.Transporter.ServiceConnectionListener;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TransporterClassic extends Transporter {
    public static final boolean DEBUG = Config.isDebug();
    private static TransportServiceStartReceiver mTransportServiceReceiver;
    private static Map<String, WeakReference<Transporter>> sTransporterHub = new ArrayMap();
    private boolean mChannelAvailable = false;
    private final List<ChannelListener> mChannelListeners = new ArrayList();
    private Context mContext;
    private final List<DataListener> mDataReceiveListeners = new ArrayList();
    private final LongSparseArray<DataSendResultCallback> mDataSendResultCallbacks = new LongSparseArray();
    private boolean mIsTransportServiceConnected;
    private boolean mIsTransportServiceConnecting;
    private String mModuleName;
    private String mModuleTag;
    private ITransportChannelListener mServiceChannelListener = new C09993();
    private final List<ServiceConnectionListener> mServiceConnectionListeners = new ArrayList();
    private ITransportDataListener mServiceDataReceiveListener = new C09982();
    private ITransportCallbackListener mServiceDataSendResultListener = new C09971();
    private ServiceConnectionListener mServiceServiceConnectionListener = new C10004();
    private ITransportDataService mTransportService;
    private Intent mTransportServiceIntent;

    class C09971 extends Stub {
        C09971() {
        }

        public void onResultBack(DataTransportResult result) throws RemoteException {
            long key = result.getTransportItem().getCreateTime();
            synchronized (TransporterClassic.this.mDataSendResultCallbacks) {
                DataSendResultCallback listener = (DataSendResultCallback) TransporterClassic.this.mDataSendResultCallbacks.get(key);
                TransporterClassic.this.mDataSendResultCallbacks.remove(key);
            }
            if (listener != null) {
                if (TransporterClassic.DEBUG) {
                    Log.m26d(TransporterClassic.this.mModuleTag, "OnResult : " + result.getTransportItem() + ", " + key + ", " + result.toString(), new Object[0]);
                }
                listener.onResultBack(result);
            }
        }
    }

    class C09982 extends ITransportDataListener.Stub {
        C09982() {
        }

        public void onDataReceived(TransportDataItem dataItem) throws RemoteException {
            String action = dataItem.getAction();
            if (TransporterClassic.DEBUG) {
                Log.m26d(TransporterClassic.this.mModuleTag, "OnDataReceived Action : " + action, new Object[0]);
            }
            synchronized (TransporterClassic.this.mDataReceiveListeners) {
                if (TransporterClassic.this.mDataReceiveListeners.size() > 0) {
                    for (DataListener listener : TransporterClassic.this.mDataReceiveListeners) {
                        listener.onDataReceived(dataItem);
                    }
                }
            }
        }
    }

    class C09993 extends ITransportChannelListener.Stub {
        C09993() {
        }

        public void onChannelChanged(boolean available) {
            if (TransporterClassic.DEBUG) {
                Log.m26d(TransporterClassic.this.mModuleTag, "OnChannelChanged Available : " + available, new Object[0]);
            }
            TransporterClassic.this.mChannelAvailable = available;
            synchronized (TransporterClassic.this.mChannelListeners) {
                if (TransporterClassic.this.mChannelListeners.size() > 0) {
                    for (ChannelListener listener : TransporterClassic.this.mChannelListeners) {
                        listener.onChannelChanged(available);
                    }
                }
            }
        }
    }

    class C10004 implements ServiceConnectionListener {
        C10004() {
        }
    }

    class C10015 implements ServiceConnection {
        C10015() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.m26d(TransporterClassic.this.mModuleTag, "OnServiceConnected!!", new Object[0]);
            TransporterClassic.this.mTransportService = ITransportDataService.Stub.asInterface(service);
            TransporterClassic.this.mIsTransportServiceConnecting = false;
            TransporterClassic.this.mIsTransportServiceConnected = true;
            try {
                TransporterClassic.this.mTransportService.registersendCallbackListener(TransporterClassic.this.mServiceDataSendResultListener);
                TransporterClassic.this.mTransportService.registerChannelListener(TransporterClassic.this.mModuleName, TransporterClassic.this.mServiceChannelListener);
                TransporterClassic.this.mTransportService.registerDataListener(TransporterClassic.this.mModuleName, TransporterClassic.this.mServiceDataReceiveListener);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.m26d(TransporterClassic.this.mModuleTag, "OnServiceDisconnected!!", new Object[0]);
            try {
                if (TransporterClassic.this.mTransportService != null) {
                    TransporterClassic.this.mTransportService.unregistersendCallbackListener(TransporterClassic.this.mServiceDataSendResultListener);
                    TransporterClassic.this.mTransportService.unregisterChannelListener(TransporterClassic.this.mModuleName);
                    TransporterClassic.this.mTransportService.unregisterDataListener(TransporterClassic.this.mModuleName);
                } else {
                    Log.m32w(TransporterClassic.this.mModuleTag, "OnTransportServiceDisconnected : mTransportService is Null!!", new Object[0]);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            TransporterClassic.this.mTransportService = null;
            TransporterClassic.this.mIsTransportServiceConnecting = false;
            TransporterClassic.this.mIsTransportServiceConnected = false;
            Log.m32w(TransporterClassic.this.mModuleTag, "Re-Connect To TransportService!!", new Object[0]);
            TransporterClassic.this.connectTransportService();
        }
    }

    private static class TransportServiceStartReceiver extends BroadcastReceiver {
        private TransportServiceStartReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (TransporterClassic.DEBUG) {
                Log.m26d("Transporter-Classic", "On TransportService Start Received!!", new Object[0]);
            }
            for (WeakReference<Transporter> ref : TransporterClassic.sTransporterHub.values()) {
                if (ref != null) {
                    Transporter transporter = (Transporter) ref.get();
                    if (transporter != null) {
                        transporter.connectTransportService();
                    }
                }
            }
        }
    }

    private TransporterClassic(Context context, String moduleName) {
        this.mContext = context;
        this.mModuleName = moduleName;
        this.mModuleTag = "Transporter-Classic[" + moduleName + "]";
    }

    public static Transporter get(@NonNull Context context, @NonNull String moduleName) {
        Transporter transporter = null;
        WeakReference<Transporter> ref = (WeakReference) sTransporterHub.get(moduleName);
        if (ref != null) {
            transporter = (Transporter) ref.get();
        }
        if (transporter == null) {
            transporter = new TransporterClassic(context, moduleName);
            sTransporterHub.put(moduleName, new WeakReference(transporter));
        }
        if (mTransportServiceReceiver == null) {
            mTransportServiceReceiver = new TransportServiceStartReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.huami.watch.transport.DataTransportService.Start");
            context.getApplicationContext().registerReceiver(mTransportServiceReceiver, filter);
        }
        return transporter;
    }

    public void connectTransportService() {
        if (DEBUG) {
            Log.m26d(this.mModuleTag, "Connect TransportService, Now Is Connected : " + this.mIsTransportServiceConnected + ", Is Connecting : " + this.mIsTransportServiceConnecting, new Object[0]);
        }
        if (!this.mIsTransportServiceConnected && !this.mIsTransportServiceConnecting) {
            if (this.mTransportServiceIntent == null) {
                this.mTransportServiceIntent = createIntentFromAction(this.mContext, "com.huami.watch.transport.DataTransportService");
                if (this.mTransportServiceIntent == null) {
                    Log.m28e(this.mModuleTag, "DataTransportService Not Found!!", new Object[0]);
                    return;
                }
            }
            this.mIsTransportServiceConnecting = true;
            boolean success = this.mContext.bindService(this.mTransportServiceIntent, new C10015(), 1);
            if (DEBUG) {
                Log.m26d(this.mModuleTag, "Connect TransportService : " + (success ? "Success" : "Failed"), new Object[0]);
            }
        }
    }

    public boolean isTransportServiceConnected() {
        if (DEBUG) {
            Log.m26d(this.mModuleTag, "TransportService Now Is Connected : " + this.mIsTransportServiceConnected, new Object[0]);
        }
        return this.mIsTransportServiceConnected;
    }

    private Intent createIntentFromAction(Context context, String action) {
        Intent implicitIntent = new Intent(action);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentServices(implicitIntent, 0);
        String className = null;
        String packageName = null;
        if (resolveInfoList == null || resolveInfoList.size() == 0) {
            return null;
        }
        Iterator i$ = resolveInfoList.iterator();
        if (i$.hasNext()) {
            ResolveInfo resolveInfo = (ResolveInfo) i$.next();
            packageName = resolveInfo.serviceInfo.packageName;
            className = resolveInfo.serviceInfo.name;
        }
        if (packageName == null || className == null) {
            return null;
        }
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    public void send(@NonNull String action, DataBundle data, DataSendResultCallback result) {
        TransportDataItem dataItem = new TransportDataItem(this.mModuleName);
        dataItem.addAction(action);
        if (data == null) {
            data = new DataBundle();
        }
        dataItem.setData(data);
        if (isTransportServiceConnected()) {
            if (DEBUG) {
                Log.m26d(this.mModuleTag, "Send : " + dataItem, new Object[0]);
            }
            if (result != null) {
                synchronized (this.mDataSendResultCallbacks) {
                    this.mDataSendResultCallbacks.put(dataItem.getCreateTime(), result);
                }
            }
            try {
                this.mTransportService.sendData(dataItem);
                return;
            } catch (RemoteException e) {
                e.printStackTrace();
                synchronized (this.mDataSendResultCallbacks) {
                    this.mDataSendResultCallbacks.remove(dataItem.getCreateTime());
                    return;
                }
            }
        }
        if (DEBUG) {
            Log.m32w(this.mModuleTag, "Send : " + dataItem + ", without service connected!!", new Object[0]);
        }
        if (result != null) {
            result.onResultBack(new DataTransportResult(dataItem, 4));
        }
    }

    public void addDataListener(DataListener listener) {
        synchronized (this.mDataReceiveListeners) {
            if (!this.mDataReceiveListeners.contains(listener)) {
                this.mDataReceiveListeners.add(listener);
            }
        }
    }

    public void removeDataListener(DataListener listener) {
        synchronized (this.mDataReceiveListeners) {
            if (this.mDataReceiveListeners.contains(listener)) {
                this.mDataReceiveListeners.remove(listener);
            }
        }
    }

    public void addChannelListener(ChannelListener listener) {
        if (listener != null) {
            try {
                listener.onChannelChanged(this.mChannelAvailable);
            } catch (Exception e) {
            }
        }
        synchronized (this.mChannelListeners) {
            if (!this.mChannelListeners.contains(listener)) {
                this.mChannelListeners.add(listener);
            }
        }
    }

    public void removeChannelListener(ChannelListener listener) {
        synchronized (this.mChannelListeners) {
            if (this.mChannelListeners.contains(listener)) {
                this.mChannelListeners.remove(listener);
            }
        }
    }

    public void addServiceConnectionListener(ServiceConnectionListener listener) {
        synchronized (this.mServiceConnectionListeners) {
            if (!this.mServiceConnectionListeners.contains(listener)) {
                this.mServiceConnectionListeners.add(listener);
            }
        }
    }

    public void removeServiceConnectionListener(ServiceConnectionListener listener) {
        synchronized (this.mServiceConnectionListeners) {
            if (this.mServiceConnectionListeners.contains(listener)) {
                this.mServiceConnectionListeners.remove(listener);
            }
        }
    }
}
