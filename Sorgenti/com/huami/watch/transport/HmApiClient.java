package com.huami.watch.transport;

import android.os.Bundle;

public class HmApiClient {

    public static final class Builder {
    }

    public interface ConnectionCallbacks {
        void onServicesConnected(Bundle bundle);

        void onServicesDisConnected(ConnectionResult connectionResult);
    }

    public interface OnConnectionFailedListener {
        void onServicesConnectionFailed(ConnectionResult connectionResult);
    }
}
