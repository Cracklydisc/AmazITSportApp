package com.huami.watch.sensor;

import android.hardware.HealthSensorHistoryData;
import android.hardware.HmGpsLocationData;
import android.hardware.IConfigFinishDispatcher;
import android.hardware.IDataDispatcher;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.huami.watch.klvp.IKlvpResponseDispatcher;
import com.huami.watch.klvp.KlvpRequest;

public interface IHmSensorHubService extends IInterface {

    public static abstract class Stub extends Binder implements IHmSensorHubService {

        private static class Proxy implements IHmSensorHubService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void configureSensorhub(int cmd, byte[] arg) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeInt(cmd);
                    _data.writeByteArray(arg);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void configureSensorHubGps(int mode, IBinder client) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeInt(mode);
                    _data.writeStrongBinder(client);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void configureSensorHubAlgorithm(int alg, int type, float distance, String clientId, IConfigFinishDispatcher callback) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeInt(alg);
                    _data.writeInt(type);
                    _data.writeFloat(distance);
                    _data.writeString(clientId);
                    _data.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void configureSensorHubAlgorithmWP(int alg, int type, Bundle bundle, String clientId, IConfigFinishDispatcher callback) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeInt(alg);
                    _data.writeInt(type);
                    if (bundle != null) {
                        _data.writeInt(1);
                        bundle.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(clientId);
                    _data.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int configureSensorHubWakeupSource(int wakeup_source, boolean enable, float[] parames, String callingPackage) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeInt(wakeup_source);
                    if (enable) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    _data.writeFloatArray(parames);
                    _data.writeString(callingPackage);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public byte[] getHeartHistoryData() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                    byte[] _result = _reply.createByteArray();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public HmGpsLocationData[] getAllGpsLocation() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                    HmGpsLocationData[] _result = (HmGpsLocationData[]) _reply.createTypedArray(HmGpsLocationData.CREATOR);
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean getGpsState() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public HealthSensorHistoryData requestHealthSensorHistoryData() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    HealthSensorHistoryData _result;
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (HealthSensorHistoryData) HealthSensorHistoryData.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int releaseHealthSensorHistoryData(HealthSensorHistoryData data) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    if (data != null) {
                        _data.writeInt(1);
                        data.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public long requestHealthSensorHistory(IBinder client, IDataDispatcher dataDispatcher) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeStrongBinder(client);
                    _data.writeStrongBinder(dataDispatcher != null ? dataDispatcher.asBinder() : null);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                    long _result = _reply.readLong();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void releaseHealthSensorHistory(HealthSensorHistoryData data, long sessionId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    if (data != null) {
                        _data.writeInt(1);
                        data.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeLong(sessionId);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void cancelHealthSensorHistory(long sessionId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeLong(sessionId);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int sendKlvpRequest(KlvpRequest request, IKlvpResponseDispatcher realdataDispatcher) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    if (request != null) {
                        _data.writeInt(1);
                        request.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeStrongBinder(realdataDispatcher != null ? realdataDispatcher.asBinder() : null);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void configSLPTUpdate1Hz(boolean is1Hz) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    if (is1Hz) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public SensorDataInfo getSensorDataInfo() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    SensorDataInfo _result;
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (SensorDataInfo) SensorDataInfo.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void requestWearDetection(boolean on, IBinder binder) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    if (on) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    _data.writeStrongBinder(binder);
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void requestWearDetections(String tag, boolean on, IBinder binder) throws RemoteException {
                int i = 0;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeString(tag);
                    if (on) {
                        i = 1;
                    }
                    _data.writeInt(i);
                    _data.writeStrongBinder(binder);
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public int syncGpxTrailData(byte[] data, int len) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.sensor.IHmSensorHubService");
                    _data.writeByteArray(data);
                    _data.writeInt(len);
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                    int _result = _reply.readInt();
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.sensor.IHmSensorHubService");
        }

        public static IHmSensorHubService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.sensor.IHmSensorHubService");
            if (iin == null || !(iin instanceof IHmSensorHubService)) {
                return new Proxy(obj);
            }
            return (IHmSensorHubService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int _result;
            HealthSensorHistoryData _arg0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    configureSensorhub(data.readInt(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    configureSensorHubGps(data.readInt(), data.readStrongBinder());
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    configureSensorHubAlgorithm(data.readInt(), data.readInt(), data.readFloat(), data.readString(), android.hardware.IConfigFinishDispatcher.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 4:
                    Bundle _arg2;
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    int _arg02 = data.readInt();
                    int _arg1 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg2 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg2 = null;
                    }
                    configureSensorHubAlgorithmWP(_arg02, _arg1, _arg2, data.readString(), android.hardware.IConfigFinishDispatcher.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    _result = configureSensorHubWakeupSource(data.readInt(), data.readInt() != 0, data.createFloatArray(), data.readString());
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 6:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    byte[] _result2 = getHeartHistoryData();
                    reply.writeNoException();
                    reply.writeByteArray(_result2);
                    return true;
                case 7:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    HmGpsLocationData[] _result3 = getAllGpsLocation();
                    reply.writeNoException();
                    reply.writeTypedArray(_result3, 1);
                    return true;
                case 8:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    boolean _result4 = getGpsState();
                    reply.writeNoException();
                    reply.writeInt(_result4 ? 1 : 0);
                    return true;
                case 9:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    HealthSensorHistoryData _result5 = requestHealthSensorHistoryData();
                    reply.writeNoException();
                    if (_result5 != null) {
                        reply.writeInt(1);
                        _result5.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 10:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    if (data.readInt() != 0) {
                        _arg0 = (HealthSensorHistoryData) HealthSensorHistoryData.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    _result = releaseHealthSensorHistoryData(_arg0);
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 11:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    long _result6 = requestHealthSensorHistory(data.readStrongBinder(), android.hardware.IDataDispatcher.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeLong(_result6);
                    return true;
                case 12:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    if (data.readInt() != 0) {
                        _arg0 = (HealthSensorHistoryData) HealthSensorHistoryData.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    releaseHealthSensorHistory(_arg0, data.readLong());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    cancelHealthSensorHistory(data.readLong());
                    reply.writeNoException();
                    return true;
                case 14:
                    KlvpRequest _arg03;
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    if (data.readInt() != 0) {
                        _arg03 = (KlvpRequest) KlvpRequest.CREATOR.createFromParcel(data);
                    } else {
                        _arg03 = null;
                    }
                    _result = sendKlvpRequest(_arg03, com.huami.watch.klvp.IKlvpResponseDispatcher.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 15:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    configSLPTUpdate1Hz(data.readInt() != 0);
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    SensorDataInfo _result7 = getSensorDataInfo();
                    reply.writeNoException();
                    if (_result7 != null) {
                        reply.writeInt(1);
                        _result7.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 17:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    requestWearDetection(data.readInt() != 0, data.readStrongBinder());
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    requestWearDetections(data.readString(), data.readInt() != 0, data.readStrongBinder());
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.huami.watch.sensor.IHmSensorHubService");
                    _result = syncGpxTrailData(data.createByteArray(), data.readInt());
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.sensor.IHmSensorHubService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void cancelHealthSensorHistory(long j) throws RemoteException;

    void configSLPTUpdate1Hz(boolean z) throws RemoteException;

    void configureSensorHubAlgorithm(int i, int i2, float f, String str, IConfigFinishDispatcher iConfigFinishDispatcher) throws RemoteException;

    void configureSensorHubAlgorithmWP(int i, int i2, Bundle bundle, String str, IConfigFinishDispatcher iConfigFinishDispatcher) throws RemoteException;

    void configureSensorHubGps(int i, IBinder iBinder) throws RemoteException;

    int configureSensorHubWakeupSource(int i, boolean z, float[] fArr, String str) throws RemoteException;

    void configureSensorhub(int i, byte[] bArr) throws RemoteException;

    HmGpsLocationData[] getAllGpsLocation() throws RemoteException;

    boolean getGpsState() throws RemoteException;

    byte[] getHeartHistoryData() throws RemoteException;

    SensorDataInfo getSensorDataInfo() throws RemoteException;

    void releaseHealthSensorHistory(HealthSensorHistoryData healthSensorHistoryData, long j) throws RemoteException;

    int releaseHealthSensorHistoryData(HealthSensorHistoryData healthSensorHistoryData) throws RemoteException;

    long requestHealthSensorHistory(IBinder iBinder, IDataDispatcher iDataDispatcher) throws RemoteException;

    HealthSensorHistoryData requestHealthSensorHistoryData() throws RemoteException;

    void requestWearDetection(boolean z, IBinder iBinder) throws RemoteException;

    void requestWearDetections(String str, boolean z, IBinder iBinder) throws RemoteException;

    int sendKlvpRequest(KlvpRequest klvpRequest, IKlvpResponseDispatcher iKlvpResponseDispatcher) throws RemoteException;

    int syncGpxTrailData(byte[] bArr, int i) throws RemoteException;
}
