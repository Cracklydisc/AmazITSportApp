package com.huami.watch.sensor;

public class HmSensorManager {
    public static HmSensorManager _instance = null;

    public static class HealthSensorHistory {
        public byte[] data;
        public int id = -1;

        public HealthSensorHistory(int id2, byte[] data2) {
            this.id = id2;
            this.data = data2;
        }
    }

    static {
        System.load("/system/lib/libsensorhub.so");
    }
}
