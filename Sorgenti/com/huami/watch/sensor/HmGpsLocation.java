package com.huami.watch.sensor;

import android.hardware.HmGpsLocationData;

public class HmGpsLocation {
    private float HDOP;
    private int accuracy;
    private float altitude;
    private double atmospheric;
    private int bar;
    private float course;
    private int gloIndex;
    private int gpsNum;
    private double latitude;
    private double longitude;
    private String nmea;
    private int pointType;
    private int reserved1;
    private int reserved2;
    private float speed;
    private long timeStamp;
    private int version;

    public HmGpsLocation(HmGpsLocationData data) {
        this.version = data.version;
        this.longitude = data.longitude;
        this.latitude = data.latitude;
        this.timeStamp = data.timeStamp;
        this.gloIndex = data.gloIndex;
        this.speed = data.speed;
        this.course = data.course;
        this.HDOP = data.HDOP;
        this.altitude = data.altitude;
        this.bar = data.bar;
        this.pointType = data.pointType;
        this.gpsNum = data.gpsNum;
        this.accuracy = data.accuracy;
        this.atmospheric = data.atmospheric;
        this.reserved1 = data.reserved1;
        this.reserved2 = data.reserved2;
        this.nmea = data.nmea;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public double getAltitude() {
        return (double) this.altitude;
    }

    public long getTimestamp() {
        return this.timeStamp;
    }

    public int getGloIndex() {
        return this.gloIndex;
    }

    public float getSpeed() {
        return this.speed;
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public int getPointType() {
        return this.pointType;
    }

    public int getBar() {
        return this.bar;
    }

    public float getCourse() {
        return this.course;
    }

    public String toString() {
        return "HmGpsLocation{version=" + this.version + ", longitude=" + this.longitude + ", latitude=" + this.latitude + ", timeStamp=" + this.timeStamp + ", gloIndex=" + this.gloIndex + ", speed=" + this.speed + ", course=" + this.course + ", HDOP=" + this.HDOP + ", altitude=" + this.altitude + ", bar=" + this.bar + ", pointType=" + this.pointType + ", gpsNum=" + this.gpsNum + ", accuracy=" + this.accuracy + ", atmospheric=" + this.atmospheric + ", reserved1=" + this.reserved1 + ", reserved2=" + this.reserved2 + ", nmea='" + this.nmea + '\'' + '}';
    }
}
