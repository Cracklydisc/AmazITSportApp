package com.huami.watch.sensor;

import android.content.Context;
import android.content.Intent;
import android.hardware.HealthSensorHistoryData;
import android.hardware.HmGpsLocationData;
import android.hardware.IConfigFinishDispatcher;
import android.hardware.IDataDispatcher;
import android.hardware.IDataDispatcher.Stub;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.SparseArray;
import com.google.protobuf.nano.CodedOutputByteBufferNano;
import com.google.protobuf.nano.InvalidProtocolBufferNanoException;
import com.huami.watch.klvp.IKlvpResponseDispatcher;
import com.huami.watch.klvp.KlvpRequest;
import com.huami.watch.klvp.KlvpResponse;
import com.huami.watch.sensor.HmSensorManager.HealthSensorHistory;
import com.huami.watch.sensorhub.SensorHubProtos.SportConfig;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class HmSensorHubConfigManager {
    private static final boolean DEBUG = Log.isLoggable("HmSensorHubManager", 3);
    private static volatile HmSensorHubConfigManager sInstance = null;
    private static IHmSensorHubService sService = null;
    private IBinder mCallback = new Binder();
    private final IConfigFinishDispatcher mConfigFinishDispatcher = new C09733();
    private HashMap<String, OnConfigFinishListener> mConfigFinishIdListenerMap = new HashMap();
    private final Object mConfigFinishListenerLock = new Object();
    private Context mContext;
    private HashMap<Long, OnDataReadyListener> mDataReadyListenerMap = new HashMap();
    private EventHandlerDelegate mFinishEventHandlerDelegate;
    private final IDataDispatcher mHealthDataDispatcher = new C09711();
    private final IKlvpResponseDispatcher mKlvpResponseDispatcher = new C09722();
    private SparseArray<Integer> mPairIdTypeMap = new SparseArray();
    private SparseArray<OnKlvpDataListener> mRealTimeDataListenerMap = new SparseArray();

    public interface OnKlvpDataListener {
        void onHealthDataReady(int i, float f);

        void onSportDataReady(SportStatistics sportStatistics);
    }

    public interface OnConfigFinishListener {
        void onFinish(int i, String str);
    }

    class C09711 extends Stub {
        C09711() {
        }

        public void dispatchData(long sessionId, HealthSensorHistoryData data) {
            HealthSensorHistory localData = null;
            if (data != null) {
                localData = new HealthSensorHistory(data.id, data.data);
            }
            if (!HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(1, new DataInfo(sessionId, localData)))) {
                OnDataReadyListener dataListener = HmSensorHubConfigManager.this.findDataReadyListener(sessionId);
                Log.w("HmSensorHubManager", "callback thread has exit when dispatch mHealthData");
                if (dataListener != null) {
                    dataListener.onDataReady(-2, null);
                    HmSensorHubConfigManager.this.unregisterDataReadyListener(sessionId);
                    return;
                }
                Log.w("HmSensorHubManager", "null listener!!");
            }
        }
    }

    class C09722 extends IKlvpResponseDispatcher.Stub {
        C09722() {
        }

        public void dispatchResponse(KlvpResponse response) {
            if (!HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(2, response))) {
                Log.w("HmSensorHubManager", "send message failed when dispatch klvp response data");
            }
        }
    }

    class C09733 extends IConfigFinishDispatcher.Stub {
        C09733() {
        }

        public void dispatchConfigFinish(int resultCode, String msg, String clientId) {
            HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(0, new ConfigFinishInfo(resultCode, msg, clientId)));
        }
    }

    class ConfigFinishInfo {
        public String clientId;
        public String msg;
        public int resultCode;

        public ConfigFinishInfo(int resultCode, String msg, String clientId) {
            this.resultCode = resultCode;
            this.msg = msg;
            this.clientId = clientId;
        }
    }

    class DataInfo {
        public HealthSensorHistory data;
        public long sessionId;

        public DataInfo(long sessionId, HealthSensorHistory data) {
            this.sessionId = sessionId;
            this.data = data;
        }
    }

    private class EventHandlerDelegate {
        private final Handler mHandler;

        EventHandlerDelegate() {
            Looper looper;
            if (Looper.myLooper() != null) {
                looper = Looper.myLooper();
            } else {
                looper = Looper.getMainLooper();
            }
            Log.i("HmSensorHubManager", "HmSensorHubManager use the thread:" + looper.getThread() + "to handle callback" + " owner process:" + Process.myPid() + " threads id:" + Process.myTid());
            if (looper != null) {
                this.mHandler = new Handler(looper, HmSensorHubConfigManager.this) {
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 0:
                                OnConfigFinishListener listener;
                                ConfigFinishInfo info = msg.obj;
                                String clientId = info.clientId;
                                String r_msg = info.msg;
                                int resultCode = info.resultCode;
                                synchronized (HmSensorHubConfigManager.this.mConfigFinishListenerLock) {
                                    listener = HmSensorHubConfigManager.this.findConfigFinishListener(clientId);
                                }
                                if (listener != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "HmSensorHubConfigManager dispatching onFinish(" + resultCode + "," + r_msg + ") ");
                                    }
                                    listener.onFinish(resultCode, r_msg);
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "dispathing done, unregister callback listener~");
                                    }
                                    HmSensorHubConfigManager.this.unregisterConfigFinishListener(clientId);
                                    return;
                                }
                                Log.w("HmSensorHubManager", "can't find listener for client:" + clientId);
                                return;
                            case 1:
                                DataInfo dataInfo = msg.obj;
                                OnDataReadyListener dataListener = HmSensorHubConfigManager.this.findDataReadyListener(dataInfo.sessionId);
                                if (dataListener != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "HmSensorHubConfigManager dispatching onDataReady(" + dataInfo.sessionId + "," + dataInfo.data + ") ");
                                    }
                                    dataListener.onDataReady(dataInfo.sessionId, dataInfo.data);
                                    HmSensorHubConfigManager.this.unregisterDataReadyListener(dataInfo.sessionId);
                                    return;
                                }
                                Log.w("HmSensorHubManager", "null listener!!");
                                return;
                            case 2:
                                KlvpResponse response = msg.obj;
                                if (response == null) {
                                    Log.w("HmSensorHubManager", " response is null !!!");
                                    return;
                                }
                                OnKlvpDataListener callback = HmSensorHubConfigManager.this.findRealtimeCallback(response.pairId);
                                if (callback != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "response target:" + response.target);
                                    }
                                    switch (response.target) {
                                        case (short) 2:
                                            SportStatistics sportStatistics = null;
                                            try {
                                                sportStatistics = SportStatistics.parseFrom(response.responseValues);
                                            } catch (InvalidProtocolBufferNanoException e) {
                                                e.printStackTrace();
                                            }
                                            Integer typeInt = (Integer) HmSensorHubConfigManager.this.mPairIdTypeMap.get(response.pairId);
                                            int type = -1;
                                            if (typeInt != null) {
                                                type = typeInt.intValue();
                                            }
                                            if (sportStatistics == null) {
                                                Log.w("HmSensorHubManager", "probuf decode wrong of SportStatistics!!!");
                                                break;
                                            }
                                            switch (type) {
                                                case 0:
                                                    callback.onHealthDataReady(0, -1.0f);
                                                    break;
                                                case 1:
                                                    float distance = 0.0f;
                                                    if (sportStatistics.mTodayAcm != null) {
                                                        distance = sportStatistics.mTodayAcm.getMDistance();
                                                    }
                                                    callback.onHealthDataReady(1, distance);
                                                    break;
                                                case 2:
                                                    callback.onSportDataReady(sportStatistics);
                                                    break;
                                                default:
                                                    Log.w("HmSensorHubManager", "unknown type " + type + "of pairid:" + response.pairId);
                                                    break;
                                            }
                                        default:
                                            Log.e("HmSensorHubManager", "undefined target");
                                            break;
                                    }
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "callback called !!!!");
                                    }
                                    HmSensorHubConfigManager.this.unregisterKlvpCallback(response.pairId);
                                    return;
                                }
                                Log.d("HmSensorHubManager", "callback is null of pairId :" + response.pairId + "maybe it's config cmd");
                                return;
                            default:
                                return;
                        }
                    }
                };
            } else {
                this.mHandler = null;
            }
        }

        Handler getHandler() {
            return this.mHandler;
        }
    }

    public interface OnDataReadyListener {
        void onDataReady(long j, HealthSensorHistory healthSensorHistory);
    }

    public static class SwimInfo implements Parcelable {
        public static final Creator<SwimInfo> CREATOR = new C09751();
        public float mAvgDistancePerStroke = 0.0f;
        public float mAvgStrokeSpeed = 0.0f;
        public float mLapStrokeSpeed = 0.0f;
        public int mLapStrokes = 0;
        public int mLapSwolf = 0;
        public float mMaxStrokeSpeed = 0.0f;
        public float mRtDistancePerStroke = 0.0f;
        public float mRtStrokeSpeed = 0.0f;
        public int mSwolfPerFixedMeters = 0;
        public int mTotalStrokes = 0;
        public int mTotalTrips = 0;

        static class C09751 implements Creator<SwimInfo> {
            C09751() {
            }

            public SwimInfo createFromParcel(Parcel in) {
                return new SwimInfo(in);
            }

            public SwimInfo[] newArray(int size) {
                return new SwimInfo[size];
            }
        }

        protected SwimInfo(Parcel in) {
            this.mSwolfPerFixedMeters = in.readInt();
            this.mTotalStrokes = in.readInt();
            this.mTotalTrips = in.readInt();
            this.mRtDistancePerStroke = in.readFloat();
            this.mRtStrokeSpeed = in.readFloat();
            this.mAvgStrokeSpeed = in.readFloat();
            this.mMaxStrokeSpeed = in.readFloat();
            this.mAvgDistancePerStroke = in.readFloat();
            this.mLapStrokes = in.readInt();
            this.mLapStrokeSpeed = in.readFloat();
            this.mLapSwolf = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mSwolfPerFixedMeters);
            dest.writeInt(this.mTotalStrokes);
            dest.writeInt(this.mTotalTrips);
            dest.writeFloat(this.mRtDistancePerStroke);
            dest.writeFloat(this.mRtStrokeSpeed);
            dest.writeFloat(this.mAvgStrokeSpeed);
            dest.writeFloat(this.mMaxStrokeSpeed);
            dest.writeFloat(this.mAvgDistancePerStroke);
            dest.writeInt(this.mLapStrokes);
            dest.writeFloat(this.mLapStrokeSpeed);
            dest.writeInt(this.mLapSwolf);
        }

        public String toString() {
            return "SwimInfo{mSwolfPerFixedMeters=" + this.mSwolfPerFixedMeters + ", mTotalStrokes=" + this.mTotalStrokes + ", mTotalTrips=" + this.mTotalTrips + ", mRtDistancePerStroke=" + this.mRtDistancePerStroke + ", mRtStrokeSpeed=" + this.mRtStrokeSpeed + ", mAvgStrokeSpeed=" + this.mAvgStrokeSpeed + ", mMaxStrokeSpeed=" + this.mMaxStrokeSpeed + ", mAvgDistancePerStroke=" + this.mAvgDistancePerStroke + ", mLapStrokes=" + this.mLapStrokes + ", mLapStrokeSpeed=" + this.mLapStrokeSpeed + ", mLapSwolf=" + this.mLapSwolf + '}';
        }
    }

    public static class UserInfo {
        private boolean mHasUserAge = false;
        private boolean mHasUserBodyFatRate = false;
        private boolean mHasUserExerciseRate = false;
        private boolean mHasUserHeightMeter = false;
        private boolean mHasUserIsLeftHandler = false;
        private boolean mHasUserIsVegnism = false;
        private boolean mHasUserIsWearOnLeft = false;
        private boolean mHasUserSex = false;
        private boolean mHasUserSkin = false;
        private boolean mHasUserWeightKg = false;
        private int mUserAge;
        private int mUserBodyFatRate;
        private int mUserExerciseRate;
        private float mUserHeightMeter;
        private boolean mUserIsLeftHander;
        private boolean mUserIsVeganism;
        private boolean mUserIsWearOnLeft;
        private int mUserSex;
        private int mUserSkin;
        private float mUserWeightKg;

        public String toString() {
            return "UserInfo{mUserSex=" + this.mUserSex + ", mHasUserSex=" + this.mHasUserSex + ", mUserAge=" + this.mUserAge + ", mHasUserAge=" + this.mHasUserAge + ", mUserHeightMeter=" + this.mUserHeightMeter + ", mHasUserHeightMeter=" + this.mHasUserHeightMeter + ", mUserWeightKg=" + this.mUserWeightKg + ", mHasUserWeightKg=" + this.mHasUserWeightKg + ", mUserIsWearOnLeft=" + this.mUserIsWearOnLeft + ", mHasUserIsWearOnLeft=" + this.mHasUserIsWearOnLeft + ", mUserBodyFatRate=" + this.mUserBodyFatRate + ", mHasUserBodyFatRate=" + this.mHasUserBodyFatRate + ", mUserExerciseRate=" + this.mUserExerciseRate + ", mHasUserExerciseRate=" + this.mHasUserExerciseRate + ", mUserIsVeganism=" + this.mUserIsVeganism + ", mHasUserIsVegnism=" + this.mHasUserIsVegnism + ", mUserSkin=" + this.mUserSkin + ", mHasUserSkin=" + this.mHasUserSkin + ", mUserIsLeftHander=" + this.mUserIsLeftHander + ", mHasUserIsLeftHandler=" + this.mHasUserIsLeftHandler + '}';
        }
    }

    private OnConfigFinishListener findConfigFinishListener(String clientId) {
        return (OnConfigFinishListener) this.mConfigFinishIdListenerMap.get(clientId);
    }

    private OnDataReadyListener findDataReadyListener(long sessionId) {
        return (OnDataReadyListener) this.mDataReadyListenerMap.get(Long.valueOf(sessionId));
    }

    private OnKlvpDataListener findRealtimeCallback(int id) {
        return (OnKlvpDataListener) this.mRealTimeDataListenerMap.get(id);
    }

    private void unregisterDataReadyListener(long sesionId) {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "unregisterDataReadyListener of session:" + sesionId);
        }
        synchronized (this.mDataReadyListenerMap) {
            this.mDataReadyListenerMap.remove(Long.valueOf(sesionId));
        }
    }

    private void registerKlvpCallback(int id, OnKlvpDataListener callback) {
        synchronized (this.mRealTimeDataListenerMap) {
            this.mRealTimeDataListenerMap.put(id, callback);
            if (DEBUG) {
                Log.d("HmSensorHubManager", "registerRealtimeCallback register callback of pairid:" + id);
            }
        }
    }

    private void unregisterKlvpCallback(int id) {
        synchronized (this.mRealTimeDataListenerMap) {
            if (DEBUG) {
                Log.d("HmSensorHubManager", "remove callback of pairid:" + id);
            }
            this.mRealTimeDataListenerMap.remove(id);
        }
    }

    private String getIdForConfigFinishListener(int type, OnConfigFinishListener l) {
        if (l != null) {
            return new String(toString() + l.toString() + type);
        }
        Log.w("HmSensorHubManager", "null listener is not a good idea !!!");
        return new String(toString() + type);
    }

    private void registerConfigFinishListener(int type, OnConfigFinishListener l) {
        String clientId = getIdForConfigFinishListener(type, l);
        if (DEBUG) {
            Log.d("HmSensorHubManager", "client :" + clientId + "  registerConfigFinishListener l :" + l);
        }
        synchronized (this.mConfigFinishListenerLock) {
            if (this.mConfigFinishIdListenerMap.containsKey(getIdForConfigFinishListener(type, l))) {
                Log.w("HmSensorHubManager", "client " + clientId + " 's listener already exist ");
                return;
            }
            if (DEBUG) {
                Log.d("HmSensorHubManager", "client :" + clientId + "  has registerConfigFinishListener l :" + l);
            }
            this.mConfigFinishIdListenerMap.put(getIdForConfigFinishListener(type, l), l);
        }
    }

    private void unregisterConfigFinishListener(String clientId) {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "unregisterConfigFinishListener of client:" + clientId);
        }
        synchronized (this.mConfigFinishListenerLock) {
            this.mConfigFinishIdListenerMap.remove(clientId);
        }
    }

    public static HmSensorHubConfigManager getHmSensorHubConfigManager(Context context) {
        return new HmSensorHubConfigManager(context.getApplicationContext());
    }

    public HmSensorHubConfigManager(Context context) {
        this.mContext = context;
        this.mFinishEventHandlerDelegate = new EventHandlerDelegate();
    }

    public void requestRealtimeData(int type, OnKlvpDataListener callback) {
        try {
            IHmSensorHubService service = getService();
            KlvpRequest request = new KlvpRequest();
            request.cmd = (byte) 2;
            request.msgRemain = (byte) 0;
            request.target = (short) 2;
            request.configValue = null;
            int pairId = service.sendKlvpRequest(request, this.mKlvpResponseDispatcher);
            if (DEBUG) {
                Log.d("HmSensorHubManager", "get pair id from sensor hub service:" + pairId + " be the key of callback:" + callback);
            }
            this.mPairIdTypeMap.put(pairId, Integer.valueOf(type));
            registerKlvpCallback(pairId, callback);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public byte[] getHeartHistoryData() {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "getHeartHistoryData");
        }
        byte[] data = null;
        try {
            data = getService().getHeartHistoryData();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<HmGpsLocation> getAllGpsLocation() {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "getAllGpsLocation");
        }
        HmGpsLocationData[] data = null;
        try {
            data = getService().getAllGpsLocation();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        ArrayList<HmGpsLocation> locationData = readFromLocationDataArray(data);
        if (DEBUG) {
            Log.d("HmSensorHubManager", "getAllGpsLocation ---- done");
        }
        return locationData;
    }

    private ArrayList<HmGpsLocation> readFromLocationDataArray(HmGpsLocationData[] data) {
        ArrayList<HmGpsLocation> locationData = new ArrayList();
        if (data == null) {
            return null;
        }
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null) {
                Log.w("HmSensorHubManager", "there is null array member in HmGpsLocationData[] length is :" + data.length + "null index:" + i);
            } else {
                locationData.add(new HmGpsLocation(data[i]));
            }
        }
        return locationData;
    }

    public void setSLPTUpdateAt1Hz(boolean is1Hz) {
        try {
            getService().configSLPTUpdate1Hz(is1Hz);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void notifySLPTChangeItem(boolean start, int type, long timestamp) {
        Intent intent = new Intent(start ? "com.huami.watchface.SlptWatchService.enable.sportView" : "com.huami.watchface.SlptWatchService.disable.sportView");
        long startTimestamp = timestamp / 1000;
        if (start) {
            Log.d("HmSensorHubManager", "Update SLPT by 1Hz");
            intent.putExtra("clockperiod", 1);
        } else {
            Log.i("HmSensorHubManager", "Defaut SLPT update rate");
            intent.putExtra("clockperiod", 0);
        }
        intent.putExtra("timestamp", startTimestamp);
        intent.putExtra("sport_view_type", -1);
        intent.putExtra("sport_change_to", type);
        intent.putExtra("key_multi_sport", true);
        Log.i("HmSensorHubManager", "notifySLPTChangeItem Start:" + start);
        this.mContext.sendOrderedBroadcast(intent, null);
    }

    public void startChangeItemTimer(int changeToSportType, long timestamp) {
        notifySLPTChangeItem(true, changeToSportType, timestamp);
    }

    public void stopChangeItemTimer(long timestamp) {
        notifySLPTChangeItem(false, 0, timestamp);
    }

    public void startMixedSport(OnConfigFinishListener l, long timestamp) {
        configureSensorHubAlgorithm(4, 2, l);
    }

    public void stopMixedSport(OnConfigFinishListener l, long timestamp, Bundle bundle) {
        configureSensorHubAlgorithm(4, 8, bundle, l);
    }

    public void startSport(int type, long timestamp, Bundle bundle, OnConfigFinishListener l) {
        configureSensorHubAlgorithm(0, type, bundle, l);
    }

    public void pauseSport(int type, long timestamp, Bundle bundle, OnConfigFinishListener l) {
        if (bundle != null) {
            bundle.putLong("timestamp", timestamp / 1000);
        }
        configureSensorHubAlgorithm(1, type, bundle, l);
    }

    public void continueSport(int type, long timestamp, Bundle bundle, OnConfigFinishListener l) {
        configureSensorHubAlgorithm(2, type, bundle, l);
    }

    public void stopSport(int type, Bundle bundle, OnConfigFinishListener l) {
        configureSensorHubAlgorithm(3, type, bundle, l);
    }

    public void setEnable(int wakeupSource, boolean enable, float... params) {
        setEnable(wakeupSource, enable, null, params);
    }

    public void setEnable(int wakeupSource, boolean enable, OnKlvpDataListener callback, float... params) {
        configureWakeupSourceByKlvp(wakeupSource, enable, params, callback);
    }

    private static IHmSensorHubService getService() throws RemoteException {
        if (sService != null) {
            return sService;
        }
        sService = IHmSensorHubService.Stub.asInterface(ServiceManager.getService("hm_sensor_hub_service"));
        if (sService != null) {
            return sService;
        }
        throw new RemoteException("Can't get Service:hm_sensor_hub_service");
    }

    public void startBatchModeGPS() {
        configureSensorHubGps(1);
    }

    public void stopBatchModeGPS() {
        configureSensorHubGps(0);
    }

    public void configureSensorHubGps(int mode) {
        try {
            getService().configureSensorHubGps(mode, this.mCallback);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void configureSensorHubAlgorithm(int alg, int type, OnConfigFinishListener l) {
        registerConfigFinishListener(alg, l);
        configureSensorHubAlgorithm(alg, type, -1.0f, getIdForConfigFinishListener(alg, l));
    }

    private void configureSensorHubAlgorithm(int alg, int type, Bundle bundle, OnConfigFinishListener l) {
        registerConfigFinishListener(alg, l);
        try {
            getService().configureSensorHubAlgorithmWP(alg, type, bundle, getIdForConfigFinishListener(alg, l), this.mConfigFinishDispatcher);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void configureSensorHubAlgorithm(int alg, int type, float distance, String clientId) {
        try {
            getService().configureSensorHubAlgorithm(alg, type, distance, clientId, this.mConfigFinishDispatcher);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private int configureWakeupSourceByKlvp(int wakeup_source, boolean enable, float[] params, OnKlvpDataListener callback) {
        SportConfig sportConfig = new SportConfig();
        switch (wakeup_source) {
            case 2:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_GPS_TOTAL_DISTANCE_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMSportTargetDistance(params[0]);
                }
                sportConfig.setMIsSportTargetDistanceAlertEnabled(enable);
                break;
            case 3:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_GPS_REPEAT_DISTANCE_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMSportLapMeter(params[0]);
                }
                sportConfig.setMIsSportLapMeterAlertEnabled(enable);
                break;
            case 4:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_HEARTRATE_EXCEED_TARGET_ZONE_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMSportSafeHeartRate((int) params[0]);
                }
                sportConfig.setMIsSafeHeartRateAlertEnabled(enable);
                break;
            case 6:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_ALGORITHM_PACE_MIN_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMSportTargetPace(params[0]);
                }
                sportConfig.setMIsSportTargetPaceAlertEnabled(enable);
                break;
            case 7:
                if (params != null && params.length > 0 && DEBUG) {
                    Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_ALGORITHM_SPORT_STATE_CHANGE_TYPE , the value :" + params[0]);
                }
                sportConfig.setMIsSportAutoPauseEnabled(enable);
                break;
            case 8:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_LONG_SITTING_TYPE , the value :" + params[0] + "but do nothing, wait implements of algo");
                    }
                    sportConfig.setMSedentaryMin((int) params[0]);
                }
                sportConfig.setMIsSedentaryMinAlertEnabled(enable);
                break;
            case 9:
                if (params != null && params.length > 0) {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_ACHIEVE_DAILY_STEP_TARGET_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMTodayStepGoal((int) params[0]);
                }
                sportConfig.setMIsTodayStepGoalAlertEnabled(enable);
                break;
            case 10:
                if (params != null && params.length > 0 && DEBUG) {
                    Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_LIFE_TIME_STEP_TARGET_TYPE , the value :" + params[0] + "bug it's write in code, no set func provide.");
                    break;
                }
            case 11:
                if (params == null || params.length <= 1) {
                    Log.e("HmSensorHubManager", "WAKEUP_SOURCE_HEARTRATE_REACH_RATE_ZONE_TYPE need two parmas!!!, use defautl.");
                } else {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_HEARTRATE_REACH_RATE_ZONE_TYPE , the value :" + params[0] + " - " + params[1]);
                    }
                    if (params[0] > params[1]) {
                        throw new IllegalArgumentException("first parameter should not larger than second parameter");
                    }
                    sportConfig.setMSportHrZoneLower(params[0]);
                    sportConfig.setMSportHrZoneUpper(params[1]);
                }
                sportConfig.setMIsSportHrZoneAlertEnabled(enable);
                break;
            case 12:
                if (params == null || params.length <= 0) {
                    Log.w("HmSensorHubManager", "WAKEUP_SOURCE_CALORIES_TARGET_TYPE parmas is null , not set target value.");
                } else {
                    if (DEBUG) {
                        Log.d("HmSensorHubManager", "config WAKEUP_SOURCE_CALORIES_TARGET_TYPE , the value :" + params[0]);
                    }
                    sportConfig.setMSportTargetCalories(params[0]);
                }
                sportConfig.setMIsSportTargetCaloriesAlertEnabled(enable);
                break;
            case 13:
                sportConfig.setMIsSporKiloMeterAlertEnabled(enable);
                break;
            case 14:
                break;
            default:
                Log.w("HmSensorHubManager", "wakeup source of type " + wakeup_source + "not define yet");
                break;
        }
        return setKlvpConfig(sportConfig, callback);
    }

    public boolean getGpsState() {
        try {
            return getService().getGpsState();
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void writeSportChannel(byte[] data, int len) {
        try {
            getService().syncGpxTrailData(data, len);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void syncGpxTrailData(byte[] data, int len) {
        try {
            getService().syncGpxTrailData(data, len);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public int setKlvpConfig(SportConfig config, OnKlvpDataListener callback) {
        byte[] flatArray = new byte[config.getSerializedSize()];
        Log.d("HmSensorHubManager", "setKlvpConfig protobuf bytes:" + flatArray.length + "(bytes)");
        try {
            config.writeTo(CodedOutputByteBufferNano.newInstance(flatArray));
        } catch (IOException e) {
            e.printStackTrace();
        }
        KlvpRequest request = new KlvpRequest();
        request.cmd = (byte) 1;
        request.msgRemain = (byte) 0;
        request.configValue = flatArray;
        request.target = (short) 4;
        try {
            int pairId = getService().sendKlvpRequest(request, this.mKlvpResponseDispatcher);
            if (callback == null) {
                return 1;
            }
            registerKlvpCallback(pairId, callback);
            return 1;
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public void setIsUseThaWorkoutSet(boolean value) {
        SportConfig config = new SportConfig();
        config.setMIsUseThaWorkoutSet(value);
        setKlvpConfig(config, null);
    }
}
