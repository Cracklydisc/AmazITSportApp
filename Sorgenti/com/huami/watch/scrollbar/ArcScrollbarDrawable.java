package com.huami.watch.scrollbar;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;
import android.widget.ScrollBarDrawable;
import com.huami.watch.utils.Gloable;

public class ArcScrollbarDrawable extends ScrollBarDrawable {
    public static final boolean DEBUG = Gloable.DEBUG;
    private final float RANGE_TRACK_ANGLE = 45.0f;
    private final float START_TRACK_ANGLE = 337.5f;
    private final float STROKE_WIDHTH = 4.5f;
    private float alpha = 1.0f;
    private float mRangeAngle;
    private View mScrollbarView;
    Paint paint;
    private float startAngle;

    public ArcScrollbarDrawable(View scrollbarView) {
        this.mScrollbarView = scrollbarView;
        init();
    }

    protected void drawTrack(Canvas canvas, Rect bounds, boolean vertical) {
        if (vertical) {
            drawVerticalTack(canvas, bounds);
        } else {
            super.drawTrack(canvas, bounds, vertical);
        }
    }

    protected void drawThumb(Canvas canvas, Rect bounds, int offset, int length, boolean vertical) {
        if (vertical) {
            drawVerticalThumb(canvas, bounds, offset, length);
        } else {
            super.drawThumb(canvas, bounds, offset, length, vertical);
        }
    }

    public void init() {
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Style.STROKE);
    }

    private void drawVerticalTack(Canvas canvas, Rect bounds) {
        float width = (float) this.mScrollbarView.getWidth();
        float height = (float) this.mScrollbarView.getHeight();
        float maxScreen = Math.max(width, height);
        bounds.bottom = bounds.top + ((int) maxScreen);
        float topCoordinate = (float) bounds.top;
        float bottomCoordinate = (float) bounds.bottom;
        if (DEBUG) {
            Log.d("Arc", "width :" + width + "   height :" + height + "  bounds height : " + bounds.height() + "   bounds width :" + bounds.width() + "  maxScreen:" + maxScreen);
        }
        this.paint.setColor(-1);
        this.paint.setAlpha((int) (51.0f * this.alpha));
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeWidth(4.5f);
        this.paint.setStrokeCap(Cap.ROUND);
        RectF oval = new RectF();
        if (DEBUG) {
            Log.i("Arc", "  top" + bounds.top + "   bottom" + bounds.bottom + "   left" + bounds.left + "  right" + bounds.right);
        }
        oval.left = 10.0f;
        oval.top = topCoordinate + 10.0f;
        oval.right = maxScreen - 10.0f;
        oval.bottom = bottomCoordinate - 10.0f;
        canvas.drawArc(oval, 337.5f, 45.0f, false, this.paint);
    }

    private void drawVerticalThumb(Canvas canvas, Rect bounds, int offset, int length) {
        float maxScreen = Math.max((float) this.mScrollbarView.getWidth(), (float) this.mScrollbarView.getHeight());
        bounds.bottom = bounds.top + ((int) maxScreen);
        float topCoordinate = (float) bounds.top;
        float bottomCoordinate = (float) bounds.bottom;
        float thumbAngle = (((float) length) * 45.0f) / ((float) bounds.height());
        this.startAngle = 337.5f + ((((float) offset) * 45.0f) / ((float) bounds.height()));
        this.mRangeAngle = thumbAngle;
        RectF oval = new RectF();
        oval.left = 10.0f;
        oval.top = 10.0f + topCoordinate;
        oval.right = maxScreen - 10.0f;
        oval.bottom = bottomCoordinate - 10.0f;
        this.paint.setColor(-1);
        this.paint.setAlpha((int) (163.0f * this.alpha));
        this.paint.setStrokeWidth(4.5f);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeCap(Cap.ROUND);
        canvas.drawArc(oval, this.startAngle, this.mRangeAngle, false, this.paint);
    }

    public void setAlpha(int alpha) {
        super.setAlpha(alpha);
        this.alpha = ((float) alpha) / 255.0f;
    }
}
