package com.huami.watch.indicator;

public interface PageIndicatorListener {
    void onPageIndicatorNumChanged(int i);

    void onPageIndicatorScrolled(int i, float f, int i2);

    void onPageIndicatorSelected(int i);

    void onPageIndicatorStateChanged(int i);
}
