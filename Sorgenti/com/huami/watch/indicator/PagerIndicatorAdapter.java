package com.huami.watch.indicator;

public interface PagerIndicatorAdapter {
    void addPageIndicatorListener(PageIndicatorListener pageIndicatorListener);

    int getCount();

    int getCurrentIndex();

    void removePageIndicatorListener(PageIndicatorListener pageIndicatorListener);
}
