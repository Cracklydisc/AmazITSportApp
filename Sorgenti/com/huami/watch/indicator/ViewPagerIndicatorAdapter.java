package com.huami.watch.indicator;

import android.database.DataSetObserver;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public class ViewPagerIndicatorAdapter extends DataSetObserver implements OnPageChangeListener, PagerIndicatorAdapter {
    private ViewPager mViewPager;
    private PageIndicatorListener mpIndicatorListener;

    public void onPageScrollStateChanged(int arg0) {
        if (this.mpIndicatorListener != null) {
            this.mpIndicatorListener.onPageIndicatorStateChanged(arg0);
        }
    }

    public void onPageScrolled(int arg0, float arg1, int arg2) {
        if (this.mpIndicatorListener != null) {
            this.mpIndicatorListener.onPageIndicatorScrolled(arg0, arg1, arg2);
        }
    }

    public void onPageSelected(int arg0) {
        if (this.mpIndicatorListener != null) {
            this.mpIndicatorListener.onPageIndicatorSelected(arg0);
        }
    }

    public void addPageIndicatorListener(PageIndicatorListener pageIndicatorListener) {
        this.mpIndicatorListener = pageIndicatorListener;
        registerListener();
    }

    public void removePageIndicatorListener(PageIndicatorListener listener) {
        this.mpIndicatorListener = null;
        unregisterListener();
    }

    public void registerListener() {
        if (this.mViewPager != null && this.mViewPager.getAdapter() != null) {
            this.mViewPager.addOnPageChangeListener(this);
            this.mViewPager.getAdapter().registerDataSetObserver(this);
        }
    }

    public void unregisterListener() {
        if (this.mViewPager != null && this.mViewPager.getAdapter() != null) {
            this.mViewPager.removeOnPageChangeListener(this);
            this.mViewPager.getAdapter().unregisterDataSetObserver(this);
        }
    }

    public void onChanged() {
        super.onChanged();
        if (this.mViewPager == null || this.mViewPager.getAdapter() == null || this.mpIndicatorListener == null) {
            this.mpIndicatorListener.onPageIndicatorNumChanged(0);
        } else {
            this.mpIndicatorListener.onPageIndicatorNumChanged(this.mViewPager.getAdapter().getCount());
        }
    }

    public int getCount() {
        if (this.mViewPager == null || this.mViewPager.getAdapter() == null) {
            return 0;
        }
        return this.mViewPager.getAdapter().getCount();
    }

    public int getCurrentIndex() {
        if (this.mViewPager != null) {
            return this.mViewPager.getCurrentItem();
        }
        return 0;
    }
}
