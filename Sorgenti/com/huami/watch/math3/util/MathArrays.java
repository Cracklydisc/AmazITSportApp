package com.huami.watch.math3.util;

public class MathArrays {

    static /* synthetic */ class C05261 {
        static final /* synthetic */ int[] $SwitchMap$com$huami$watch$math3$util$MathArrays$OrderDirection = new int[OrderDirection.values().length];

        static {
            try {
                $SwitchMap$com$huami$watch$math3$util$MathArrays$OrderDirection[OrderDirection.INCREASING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$huami$watch$math3$util$MathArrays$OrderDirection[OrderDirection.DECREASING.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public interface Function {
    }

    public enum OrderDirection {
        INCREASING,
        DECREASING
    }

    public enum Position {
        HEAD,
        TAIL
    }

    private MathArrays() {
    }

    public static boolean equals(double[] x, double[] y) {
        boolean z = true;
        if (x == null || y == null) {
            if (((y == null ? 1 : 0) ^ (x == null ? 1 : 0)) != 0) {
                z = false;
            }
            return z;
        } else if (x.length != y.length) {
            return false;
        } else {
            for (int i = 0; i < x.length; i++) {
                if (!Precision.equals(x[i], y[i])) {
                    return false;
                }
            }
            return true;
        }
    }
}
