package com.huami.watch.math3.util;

public class Precision {
    public static final double EPSILON = Double.longBitsToDouble(4368491638549381120L);
    private static final long NEGATIVE_ZERO_DOUBLE_BITS = Double.doubleToRawLongBits(-0.0d);
    private static final int NEGATIVE_ZERO_FLOAT_BITS = Float.floatToRawIntBits(-0.0f);
    private static final long POSITIVE_ZERO_DOUBLE_BITS = Double.doubleToRawLongBits(0.0d);
    private static final int POSITIVE_ZERO_FLOAT_BITS = Float.floatToRawIntBits(0.0f);
    public static final double SAFE_MIN = Double.longBitsToDouble(4503599627370496L);

    private Precision() {
    }

    public static boolean equals(double x, double y) {
        return equals(x, y, 1);
    }

    public static boolean equals(double x, double y, int maxUlps) {
        boolean isEqual;
        long xInt = Double.doubleToRawLongBits(x);
        long yInt = Double.doubleToRawLongBits(y);
        if (((xInt ^ yInt) & Long.MIN_VALUE) != 0) {
            long deltaPlus;
            long deltaMinus;
            if (xInt < yInt) {
                deltaPlus = yInt - POSITIVE_ZERO_DOUBLE_BITS;
                deltaMinus = xInt - NEGATIVE_ZERO_DOUBLE_BITS;
            } else {
                deltaPlus = xInt - POSITIVE_ZERO_DOUBLE_BITS;
                deltaMinus = yInt - NEGATIVE_ZERO_DOUBLE_BITS;
            }
            if (deltaPlus > ((long) maxUlps)) {
                isEqual = false;
            } else {
                isEqual = deltaMinus <= ((long) maxUlps) - deltaPlus;
            }
        } else if (FastMath.abs(xInt - yInt) <= ((long) maxUlps)) {
            isEqual = true;
        } else {
            isEqual = false;
        }
        if (!isEqual || Double.isNaN(x) || Double.isNaN(y)) {
            return false;
        }
        return true;
    }
}
