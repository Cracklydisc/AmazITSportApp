package com.huami.watch.math3.util;

import java.util.Arrays;

public final class MathUtils {
    private MathUtils() {
    }

    public static int hash(double[] value) {
        return Arrays.hashCode(value);
    }
}
