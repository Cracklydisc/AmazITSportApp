package com.huami.watch.math3.analysis.differentiation;

import com.huami.watch.math3.Field;
import com.huami.watch.math3.RealFieldElement;
import com.huami.watch.math3.exception.DimensionMismatchException;
import com.huami.watch.math3.exception.NumberIsTooLargeException;
import com.huami.watch.math3.util.MathArrays;
import com.huami.watch.math3.util.MathUtils;
import java.io.Serializable;

public class DerivativeStructure implements RealFieldElement<DerivativeStructure>, Serializable {
    private static final long serialVersionUID = 20120730;
    private transient DSCompiler compiler;
    private final double[] data;

    class C05251 implements Field<DerivativeStructure> {
    }

    private static class DataTransferObject implements Serializable {
        private static final long serialVersionUID = 20120730;
        private final double[] data;
        private final int order;
        private final int variables;

        public DataTransferObject(int variables, int order, double[] data) {
            this.variables = variables;
            this.order = order;
            this.data = data;
        }

        private Object readResolve() {
            return new DerivativeStructure(this.variables, this.order, this.data);
        }
    }

    private DerivativeStructure(DSCompiler compiler) {
        this.compiler = compiler;
        this.data = new double[compiler.getSize()];
    }

    public DerivativeStructure(int parameters, int order) throws NumberIsTooLargeException {
        this(DSCompiler.getCompiler(parameters, order));
    }

    public DerivativeStructure(int parameters, int order, double... derivatives) throws DimensionMismatchException, NumberIsTooLargeException {
        this(parameters, order);
        if (derivatives.length != this.data.length) {
            throw new DimensionMismatchException(derivatives.length, this.data.length);
        }
        System.arraycopy(derivatives, 0, this.data, 0, this.data.length);
    }

    public int getFreeParameters() {
        return this.compiler.getFreeParameters();
    }

    public int getOrder() {
        return this.compiler.getOrder();
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DerivativeStructure)) {
            return false;
        }
        DerivativeStructure rhs = (DerivativeStructure) other;
        if (getFreeParameters() == rhs.getFreeParameters() && getOrder() == rhs.getOrder() && MathArrays.equals(this.data, rhs.data)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (((getFreeParameters() * 229) + 227) + (getOrder() * 233)) + (MathUtils.hash(this.data) * 239);
    }

    private Object writeReplace() {
        return new DataTransferObject(this.compiler.getFreeParameters(), this.compiler.getOrder(), this.data);
    }
}
