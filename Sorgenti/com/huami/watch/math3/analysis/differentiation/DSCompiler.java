package com.huami.watch.math3.analysis.differentiation;

import com.huami.watch.math3.exception.NumberIsTooLargeException;
import com.huami.watch.math3.util.FastMath;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DSCompiler {
    private static AtomicReference<DSCompiler[][]> compilers = new AtomicReference(null);
    private final int[][][] compIndirection;
    private final int[][] derivativesIndirection;
    private final int[] lowerIndirection;
    private final int[][][] multIndirection;
    private final int order;
    private final int parameters;
    private final int[][] sizes;

    private DSCompiler(int parameters, int order, DSCompiler valueCompiler, DSCompiler derivativeCompiler) throws NumberIsTooLargeException {
        this.parameters = parameters;
        this.order = order;
        this.sizes = compileSizes(parameters, order, valueCompiler);
        this.derivativesIndirection = compileDerivativesIndirection(parameters, order, valueCompiler, derivativeCompiler);
        this.lowerIndirection = compileLowerIndirection(parameters, order, valueCompiler, derivativeCompiler);
        this.multIndirection = compileMultiplicationIndirection(parameters, order, valueCompiler, derivativeCompiler, this.lowerIndirection);
        this.compIndirection = compileCompositionIndirection(parameters, order, valueCompiler, derivativeCompiler, this.sizes, this.derivativesIndirection);
    }

    public static DSCompiler getCompiler(int parameters, int order) throws NumberIsTooLargeException {
        DSCompiler[][] cache = (DSCompiler[][]) compilers.get();
        if (cache != null && cache.length > parameters && cache[parameters].length > order && cache[parameters][order] != null) {
            return cache[parameters][order];
        }
        int i;
        int maxParameters = FastMath.max(parameters, cache == null ? 0 : cache.length);
        if (cache == null) {
            i = 0;
        } else {
            i = cache[0].length;
        }
        DSCompiler[][] newCache = (DSCompiler[][]) Array.newInstance(DSCompiler.class, new int[]{maxParameters + 1, FastMath.max(order, i) + 1});
        if (cache != null) {
            for (int i2 = 0; i2 < cache.length; i2++) {
                System.arraycopy(cache[i2], 0, newCache[i2], 0, cache[i2].length);
            }
        }
        for (int diag = 0; diag <= parameters + order; diag++) {
            int o = FastMath.max(0, diag - parameters);
            while (o <= FastMath.min(order, diag)) {
                int p = diag - o;
                if (newCache[p][o] == null) {
                    DSCompiler derivativeCompiler;
                    DSCompiler valueCompiler = p == 0 ? null : newCache[p - 1][o];
                    if (o == 0) {
                        derivativeCompiler = null;
                    } else {
                        derivativeCompiler = newCache[p][o - 1];
                    }
                    newCache[p][o] = new DSCompiler(p, o, valueCompiler, derivativeCompiler);
                }
                o++;
            }
        }
        compilers.compareAndSet(cache, newCache);
        return newCache[parameters][order];
    }

    private static int[][] compileSizes(int parameters, int order, DSCompiler valueCompiler) {
        int[][] sizes = (int[][]) Array.newInstance(Integer.TYPE, new int[]{parameters + 1, order + 1});
        if (parameters == 0) {
            Arrays.fill(sizes[0], 1);
        } else {
            System.arraycopy(valueCompiler.sizes, 0, sizes, 0, parameters);
            sizes[parameters][0] = 1;
            for (int i = 0; i < order; i++) {
                sizes[parameters][i + 1] = sizes[parameters][i] + sizes[parameters - 1][i + 1];
            }
        }
        return sizes;
    }

    private static int[][] compileDerivativesIndirection(int parameters, int order, DSCompiler valueCompiler, DSCompiler derivativeCompiler) {
        if (parameters == 0 || order == 0) {
            return (int[][]) Array.newInstance(Integer.TYPE, new int[]{1, parameters});
        }
        int i;
        int vSize = valueCompiler.derivativesIndirection.length;
        int dSize = derivativeCompiler.derivativesIndirection.length;
        int[][] derivativesIndirection = (int[][]) Array.newInstance(Integer.TYPE, new int[]{vSize + dSize, parameters});
        for (i = 0; i < vSize; i++) {
            System.arraycopy(valueCompiler.derivativesIndirection[i], 0, derivativesIndirection[i], 0, parameters - 1);
        }
        for (i = 0; i < dSize; i++) {
            System.arraycopy(derivativeCompiler.derivativesIndirection[i], 0, derivativesIndirection[vSize + i], 0, parameters);
            int[] iArr = derivativesIndirection[vSize + i];
            int i2 = parameters - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return derivativesIndirection;
    }

    private static int[] compileLowerIndirection(int parameters, int order, DSCompiler valueCompiler, DSCompiler derivativeCompiler) {
        if (parameters == 0 || order <= 1) {
            return new int[]{0};
        }
        int vSize = valueCompiler.lowerIndirection.length;
        int dSize = derivativeCompiler.lowerIndirection.length;
        int[] lowerIndirection = new int[(vSize + dSize)];
        System.arraycopy(valueCompiler.lowerIndirection, 0, lowerIndirection, 0, vSize);
        for (int i = 0; i < dSize; i++) {
            lowerIndirection[vSize + i] = valueCompiler.getSize() + derivativeCompiler.lowerIndirection[i];
        }
        return lowerIndirection;
    }

    private static int[][][] compileMultiplicationIndirection(int parameters, int order, DSCompiler valueCompiler, DSCompiler derivativeCompiler, int[] lowerIndirection) {
        if (parameters == 0 || order == 0) {
            int[][][] iArr = new int[1][][];
            iArr[0] = new int[][]{new int[]{1, 0, 0}};
            return iArr;
        }
        int vSize = valueCompiler.multIndirection.length;
        int dSize = derivativeCompiler.multIndirection.length;
        iArr = new int[(vSize + dSize)][][];
        System.arraycopy(valueCompiler.multIndirection, 0, iArr, 0, vSize);
        for (int i = 0; i < dSize; i++) {
            int j;
            int[][] dRow = derivativeCompiler.multIndirection[i];
            List<int[]> row = new ArrayList(dRow.length * 2);
            for (j = 0; j < dRow.length; j++) {
                row.add(new int[]{dRow[j][0], lowerIndirection[dRow[j][1]], dRow[j][2] + vSize});
                row.add(new int[]{dRow[j][0], dRow[j][1] + vSize, lowerIndirection[dRow[j][2]]});
            }
            List<int[]> combined = new ArrayList(row.size());
            for (j = 0; j < row.size(); j++) {
                int[] termJ = (int[]) row.get(j);
                if (termJ[0] > 0) {
                    for (int k = j + 1; k < row.size(); k++) {
                        int[] termK = (int[]) row.get(k);
                        if (termJ[1] == termK[1] && termJ[2] == termK[2]) {
                            termJ[0] = termJ[0] + termK[0];
                            termK[0] = 0;
                        }
                    }
                    combined.add(termJ);
                }
            }
            iArr[vSize + i] = (int[][]) combined.toArray(new int[combined.size()][]);
        }
        return iArr;
    }

    private static int[][][] compileCompositionIndirection(int parameters, int order, DSCompiler valueCompiler, DSCompiler derivativeCompiler, int[][] sizes, int[][] derivativesIndirection) throws NumberIsTooLargeException {
        if (parameters == 0 || order == 0) {
            int[][][] iArr = new int[1][][];
            iArr[0] = new int[][]{new int[]{1, 0}};
            return iArr;
        }
        int vSize = valueCompiler.compIndirection.length;
        int dSize = derivativeCompiler.compIndirection.length;
        iArr = new int[(vSize + dSize)][][];
        System.arraycopy(valueCompiler.compIndirection, 0, iArr, 0, vSize);
        for (int i = 0; i < dSize; i++) {
            int j;
            int l;
            List<int[]> row = new ArrayList();
            for (int[] term : derivativeCompiler.compIndirection[i]) {
                int[] derivedTermF = new int[(term.length + 1)];
                derivedTermF[0] = term[0];
                derivedTermF[1] = term[1] + 1;
                int[] orders = new int[parameters];
                orders[parameters - 1] = 1;
                derivedTermF[term.length] = getPartialDerivativeIndex(parameters, order, sizes, orders);
                for (j = 2; j < term.length; j++) {
                    derivedTermF[j] = convertIndex(term[j], parameters, derivativeCompiler.derivativesIndirection, parameters, order, sizes);
                }
                Arrays.sort(derivedTermF, 2, derivedTermF.length);
                row.add(derivedTermF);
                for (l = 2; l < term.length; l++) {
                    int[] derivedTermG = new int[term.length];
                    derivedTermG[0] = term[0];
                    derivedTermG[1] = term[1];
                    for (j = 2; j < term.length; j++) {
                        derivedTermG[j] = convertIndex(term[j], parameters, derivativeCompiler.derivativesIndirection, parameters, order, sizes);
                        if (j == l) {
                            System.arraycopy(derivativesIndirection[derivedTermG[j]], 0, orders, 0, parameters);
                            int i2 = parameters - 1;
                            orders[i2] = orders[i2] + 1;
                            derivedTermG[j] = getPartialDerivativeIndex(parameters, order, sizes, orders);
                        }
                    }
                    Arrays.sort(derivedTermG, 2, derivedTermG.length);
                    row.add(derivedTermG);
                }
            }
            List<int[]> combined = new ArrayList(row.size());
            for (j = 0; j < row.size(); j++) {
                Object termJ = (int[]) row.get(j);
                if (termJ[0] > 0) {
                    for (int k = j + 1; k < row.size(); k++) {
                        int[] termK = (int[]) row.get(k);
                        boolean equals = termJ.length == termK.length;
                        l = 1;
                        while (equals && l < termJ.length) {
                            equals &= termJ[l] == termK[l] ? 1 : 0;
                            l++;
                        }
                        if (equals) {
                            termJ[0] = termJ[0] + termK[0];
                            termK[0] = 0;
                        }
                    }
                    combined.add(termJ);
                }
            }
            iArr[vSize + i] = (int[][]) combined.toArray(new int[combined.size()][]);
        }
        return iArr;
    }

    private static int getPartialDerivativeIndex(int parameters, int order, int[][] sizes, int... orders) throws NumberIsTooLargeException {
        int index = 0;
        int m = order;
        int ordersSum = 0;
        int i = parameters - 1;
        while (i >= 0) {
            int derivativeOrder = orders[i];
            ordersSum += derivativeOrder;
            if (ordersSum > order) {
                throw new NumberIsTooLargeException(Integer.valueOf(ordersSum), Integer.valueOf(order), true);
            }
            int derivativeOrder2 = derivativeOrder;
            int m2 = m;
            while (true) {
                derivativeOrder = derivativeOrder2 - 1;
                if (derivativeOrder2 <= 0) {
                    break;
                }
                index += sizes[i][m2];
                derivativeOrder2 = derivativeOrder;
                m2--;
            }
            i--;
            m = m2;
        }
        return index;
    }

    private static int convertIndex(int index, int srcP, int[][] srcDerivativesIndirection, int destP, int destO, int[][] destSizes) throws NumberIsTooLargeException {
        int[] orders = new int[destP];
        System.arraycopy(srcDerivativesIndirection[index], 0, orders, 0, FastMath.min(srcP, destP));
        return getPartialDerivativeIndex(destP, destO, destSizes, orders);
    }

    public int getFreeParameters() {
        return this.parameters;
    }

    public int getOrder() {
        return this.order;
    }

    public int getSize() {
        return this.sizes[this.parameters][this.order];
    }
}
