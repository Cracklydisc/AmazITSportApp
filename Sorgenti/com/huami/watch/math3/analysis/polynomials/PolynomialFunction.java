package com.huami.watch.math3.analysis.polynomials;

import com.huami.watch.math3.analysis.DifferentiableUnivariateFunction;
import com.huami.watch.math3.analysis.ParametricUnivariateFunction;
import com.huami.watch.math3.analysis.differentiation.UnivariateDifferentiableFunction;
import com.huami.watch.math3.util.FastMath;
import java.io.Serializable;
import java.util.Arrays;

public class PolynomialFunction implements DifferentiableUnivariateFunction, UnivariateDifferentiableFunction, Serializable {
    private static final long serialVersionUID = -7726511984200295583L;
    private final double[] coefficients;

    public static class Parametric implements ParametricUnivariateFunction {
    }

    private static String toString(double coeff) {
        String c = Double.toString(coeff);
        if (c.endsWith(".0")) {
            return c.substring(0, c.length() - 2);
        }
        return c;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        if (this.coefficients[0] != 0.0d) {
            s.append(toString(this.coefficients[0]));
        } else if (this.coefficients.length == 1) {
            return "0";
        }
        for (int i = 1; i < this.coefficients.length; i++) {
            if (this.coefficients[i] != 0.0d) {
                if (s.length() > 0) {
                    if (this.coefficients[i] < 0.0d) {
                        s.append(" - ");
                    } else {
                        s.append(" + ");
                    }
                } else if (this.coefficients[i] < 0.0d) {
                    s.append("-");
                }
                double absAi = FastMath.abs(this.coefficients[i]);
                if (absAi - 1.0d != 0.0d) {
                    s.append(toString(absAi));
                    s.append(' ');
                }
                s.append("x");
                if (i > 1) {
                    s.append('^');
                    s.append(Integer.toString(i));
                }
            }
        }
        return s.toString();
    }

    public int hashCode() {
        return Arrays.hashCode(this.coefficients) + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PolynomialFunction)) {
            return false;
        }
        if (Arrays.equals(this.coefficients, ((PolynomialFunction) obj).coefficients)) {
            return true;
        }
        return false;
    }
}
