package com.huami.watch.math3.exception;

import com.huami.watch.math3.exception.util.LocalizedFormats;

public class MathInternalError extends MathIllegalStateException {
    private static final long serialVersionUID = -6276776513966934846L;

    public MathInternalError() {
        getContext().addMessage(LocalizedFormats.INTERNAL_ERROR, "https://issues.apache.org/jira/browse/MATH");
    }
}
