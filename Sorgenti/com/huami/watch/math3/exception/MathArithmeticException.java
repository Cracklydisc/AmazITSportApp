package com.huami.watch.math3.exception;

import com.huami.watch.math3.exception.util.ExceptionContext;
import com.huami.watch.math3.exception.util.ExceptionContextProvider;
import com.huami.watch.math3.exception.util.LocalizedFormats;

public class MathArithmeticException extends ArithmeticException implements ExceptionContextProvider {
    private static final long serialVersionUID = -6024911025449780478L;
    private final ExceptionContext context = new ExceptionContext(this);

    public MathArithmeticException() {
        this.context.addMessage(LocalizedFormats.ARITHMETIC_EXCEPTION, new Object[0]);
    }

    public String getMessage() {
        return this.context.getMessage();
    }

    public String getLocalizedMessage() {
        return this.context.getLocalizedMessage();
    }
}
