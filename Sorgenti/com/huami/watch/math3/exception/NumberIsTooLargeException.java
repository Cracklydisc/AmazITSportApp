package com.huami.watch.math3.exception;

import com.huami.watch.math3.exception.util.Localizable;
import com.huami.watch.math3.exception.util.LocalizedFormats;

public class NumberIsTooLargeException extends MathIllegalNumberException {
    private static final long serialVersionUID = 4330003017885151975L;
    private final boolean boundIsAllowed;
    private final Number max;

    public NumberIsTooLargeException(Number wrong, Number max, boolean boundIsAllowed) {
        this(boundIsAllowed ? LocalizedFormats.NUMBER_TOO_LARGE : LocalizedFormats.NUMBER_TOO_LARGE_BOUND_EXCLUDED, wrong, max, boundIsAllowed);
    }

    public NumberIsTooLargeException(Localizable specific, Number wrong, Number max, boolean boundIsAllowed) {
        super(specific, wrong, max);
        this.max = max;
        this.boundIsAllowed = boundIsAllowed;
    }
}
