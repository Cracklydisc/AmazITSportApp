package com.huami.watch.math3.exception;

import com.huami.watch.math3.util.MathArrays.OrderDirection;

public class NonMonotonicSequenceException extends MathIllegalNumberException {
    private static final long serialVersionUID = 3596849179428944575L;
    private final OrderDirection direction;
    private final int index;
    private final Number previous;
    private final boolean strict;
}
