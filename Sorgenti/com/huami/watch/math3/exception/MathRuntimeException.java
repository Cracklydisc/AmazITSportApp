package com.huami.watch.math3.exception;

import com.huami.watch.math3.exception.util.ExceptionContext;
import com.huami.watch.math3.exception.util.ExceptionContextProvider;

public class MathRuntimeException extends RuntimeException implements ExceptionContextProvider {
    private static final long serialVersionUID = 20120926;
    private final ExceptionContext context;

    public String getMessage() {
        return this.context.getMessage();
    }

    public String getLocalizedMessage() {
        return this.context.getLocalizedMessage();
    }
}
