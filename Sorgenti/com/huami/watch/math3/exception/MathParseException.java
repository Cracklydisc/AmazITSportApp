package com.huami.watch.math3.exception;

import com.huami.watch.math3.exception.util.ExceptionContextProvider;

public class MathParseException extends MathIllegalStateException implements ExceptionContextProvider {
    private static final long serialVersionUID = -6024911025449780478L;
}
