package com.huami.watch.log;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class TracesDefine {
    public static final SimpleDateFormat FILE_DATA_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", Locale.CHINESE);
    public static final SimpleDateFormat FILE_NAME_DATA_FORMAT = new SimpleDateFormat("MM_dd_HH_mm_ss", Locale.CHINESE);

    public static final class PathAndFileName {
    }
}
