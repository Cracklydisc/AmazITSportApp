package com.huami.watch.common.transport;

public interface IResponseReceiver {
    void onResponse(int i, Object obj, String str);
}
