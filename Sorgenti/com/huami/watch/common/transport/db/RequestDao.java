package com.huami.watch.common.transport.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.transport.transport.Request;

public class RequestDao extends Dao<Request> {
    private static RequestDao sInstance = null;
    private TransportDataHelper mDBHelper;

    public com.huami.watch.common.transport.transport.Request select(android.content.Context r13, java.lang.String r14, java.lang.String[] r15) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:9:0x003c]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r12 = this;
        if (r13 != 0) goto L_0x000b;
    L_0x0002:
        r1 = "HeartRateDao";
        r2 = "context should not be null";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r11 = 0;
    L_0x000a:
        return r11;
    L_0x000b:
        r1 = r12.mDBHelper;
        r0 = r1.getReadableDatabase();
        r8 = 0;
        r1 = "request";	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r2 = 1;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r3 = 0;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r4 = "data";	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r5 = 0;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r6 = 0;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r7 = 0;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r3 = r14;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r4 = r15;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r8 = r0.query(r1, r2, r3, r4, r5, r6, r7);	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r11 = 0;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r1 = r8.moveToNext();	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        if (r1 == 0) goto L_0x003a;	 Catch:{ Exception -> 0x0040, all -> 0x004d }
    L_0x002c:
        r1 = "data";	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r1 = r8.getColumnIndex(r1);	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r10 = r8.getString(r1);	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        r11 = com.huami.watch.common.transport.transport.Request.createFromJSON(r10);	 Catch:{ Exception -> 0x0040, all -> 0x004d }
    L_0x003a:
        if (r8 == 0) goto L_0x000a;
    L_0x003c:
        r8.close();
        goto L_0x000a;
    L_0x0040:
        r9 = move-exception;
        r1 = "HeartRateDao";	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        com.huami.watch.common.log.Debug.printException(r1, r9);	 Catch:{ Exception -> 0x0040, all -> 0x004d }
        if (r8 == 0) goto L_0x004b;
    L_0x0048:
        r8.close();
    L_0x004b:
        r11 = 0;
        goto L_0x000a;
    L_0x004d:
        r1 = move-exception;
        if (r8 == 0) goto L_0x0053;
    L_0x0050:
        r8.close();
    L_0x0053:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.common.transport.db.RequestDao.select(android.content.Context, java.lang.String, java.lang.String[]):com.huami.watch.common.transport.transport.Request");
    }

    public java.util.List<com.huami.watch.common.transport.transport.Request> selectAll(android.content.Context r15, java.lang.String r16, java.lang.String[] r17, java.lang.String r18, java.lang.String r19) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r14 = this;
        r1 = r14.mDBHelper;
        r0 = r1.getReadableDatabase();
        r9 = 0;
        r1 = "request";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r2 = 1;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r3 = 0;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r4 = "data";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r5 = 0;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r6 = 0;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r3 = r16;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r4 = r17;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r7 = r18;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r8 = r19;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r13 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r13.<init>();	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
    L_0x0024:
        r1 = r9.moveToNext();	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        if (r1 == 0) goto L_0x005a;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
    L_0x002a:
        r1 = "data";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r12 = r9.getString(r1);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r11 = com.huami.watch.common.transport.transport.Request.createFromJSON(r12);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        if (r11 != 0) goto L_0x004f;	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
    L_0x003a:
        r1 = "HeartRateDao";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        r2 = "request is null!";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        com.huami.watch.common.log.Debug.m6w(r1, r2);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        goto L_0x0024;
    L_0x0042:
        r10 = move-exception;
        r1 = "HeartRateDao";	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        com.huami.watch.common.log.Debug.printException(r1, r10);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        if (r9 == 0) goto L_0x004d;
    L_0x004a:
        r9.close();
    L_0x004d:
        r13 = 0;
    L_0x004e:
        return r13;
    L_0x004f:
        r13.add(r11);	 Catch:{ Exception -> 0x0042, all -> 0x0053 }
        goto L_0x0024;
    L_0x0053:
        r1 = move-exception;
        if (r9 == 0) goto L_0x0059;
    L_0x0056:
        r9.close();
    L_0x0059:
        throw r1;
    L_0x005a:
        if (r9 == 0) goto L_0x004e;
    L_0x005c:
        r9.close();
        goto L_0x004e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.common.transport.db.RequestDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.common.transport.transport.Request>");
    }

    public boolean insert(Context context, Request request) {
        SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues(3);
        String reqJson = request.toJSON();
        values.put("action", request.getAction());
        values.put("data", reqJson);
        values.put("uuid", request.getUUID());
        if (db.insert("request", null, values) < 0) {
            return false;
        }
        return true;
    }

    public boolean update(Context context, Request request) {
        SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
        ContentValues values = new ContentValues(3);
        String reqJson = request.toJSON();
        values.put("action", request.getAction());
        values.put("data", reqJson);
        values.put("uuid", request.getUUID());
        if (((long) db.update("request", values, "uuid=?", new String[]{request.getUUID()})) < 0) {
            return false;
        }
        return true;
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("HeartRateDao", "context should not be null");
            return false;
        }
        this.mDBHelper.getWritableDatabase().delete("request", whereClause, whereClauseArg);
        return true;
    }
}
