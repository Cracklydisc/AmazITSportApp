package com.huami.watch.common.transport.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

class TransportDataHelper extends SQLiteOpenHelper {

    public static class HttpRequestEntry implements BaseColumns {
    }

    public static class RequestEntry implements BaseColumns {
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE request(action TEXT, data TEXT, uuid TEXT)");
        db.execSQL("CREATE TABLE http_request(status INTEGER, req_id TEXT, content TEXT)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS request");
        db.execSQL("DROP TABLE IF EXISTS http_request");
        onCreate(db);
    }
}
