package com.huami.watch.common.transport.transport;

import android.os.Handler;
import android.os.Looper;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.transport.IResponseReceiver;
import com.huami.watch.transport.DataBundle;
import com.huami.watch.transport.DataTransportResult;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.ChannelListener;
import com.huami.watch.transport.Transporter.DataListener;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class BTransporter implements ChannelListener, DataListener {
    private Handler mMainThreadHandler;
    private List<IBRequestListener> mRequestListeners;
    Transporter mTransporter;

    public interface IBRequestListener {
        void onBRequest(Request request);
    }

    public BTransporter() {
        this.mTransporter = null;
        this.mMainThreadHandler = null;
        this.mRequestListeners = new LinkedList();
        this.mMainThreadHandler = new Handler(Looper.getMainLooper());
    }

    private static String parseResultCodeToReadableString(int code) {
        switch (code) {
            case 0:
                return "ok";
            case 1:
                return "channel is not available";
            case 2:
                return "link has disconnected";
            case 3:
                return "service crash";
            default:
                return "unknown error while transport";
        }
    }

    private boolean check() {
        if (this.mTransporter == null) {
            Debug.m6w("BTransporter", "BTransporter should not be null");
            return false;
        } else if (this.mTransporter.isTransportServiceConnected()) {
            return true;
        } else {
            Debug.m6w("BTransporter", "Transport service has not be connected");
            return false;
        }
    }

    public boolean send(Request request, final IResponseReceiver response) {
        Debug.m3d("BTransporter", "send : " + request);
        if (!check()) {
            return false;
        }
        DataBundle bundle = new DataBundle();
        Map<String, String> data = request.getData();
        if (data != null) {
            for (String k : data.keySet()) {
                bundle.putString(k, (String) data.get(k));
            }
        }
        bundle.putString("__uuid", request.getUUID());
        this.mTransporter.send(request.getAction(), bundle, new DataSendResultCallback() {
            public void onResultBack(DataTransportResult dataTransportResult) {
                if (response != null) {
                    int code = dataTransportResult.getResultCode();
                    response.onResponse(code, null, BTransporter.parseResultCodeToReadableString(code));
                }
            }
        });
        return true;
    }

    void notifyRequest(Request request) {
        Debug.m3d("BTransporter", "notifyRequest. listeners:" + this.mRequestListeners);
        for (IBRequestListener rl : new LinkedList(this.mRequestListeners)) {
            rl.onBRequest(request);
        }
    }

    public void onDataReceived(TransportDataItem transportDataItem) {
        Debug.m3d("BTransporter", "onDataReceived");
        DataBundle bundle = transportDataItem.getData();
        Set<String> bundleKeys = bundle.keySet();
        Request request = Request.createRequest(transportDataItem.getAction());
        request.setUUID(bundle.getString("__uuid"));
        Map<String, String> data = new TreeMap();
        for (String s : bundleKeys) {
            data.put(s, bundle.getString(s));
        }
        request.setData(data);
        notifyRequest(request);
    }

    public void onChannelChanged(boolean b) {
        Debug.m3d("BTransporter", "onChannelChanged");
    }
}
