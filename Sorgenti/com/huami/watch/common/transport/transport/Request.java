package com.huami.watch.common.transport.transport;

import com.huami.watch.common.JSONAble;
import com.huami.watch.common.log.Debug;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class Request implements JSONAble {
    private String mAction = null;
    private Map<String, String> mData = new TreeMap();
    private int mRequestType = 1;
    private int mRetryCount = 0;
    private String mUUID = UUID.randomUUID().toString();

    private static final class JSONKeys {
        private JSONKeys() {
        }
    }

    public String toString() {
        return "Request{mAction='" + this.mAction + '\'' + ", mData=" + this.mData + ", mRetryCount=" + this.mRetryCount + ", mUUID='" + this.mUUID + '\'' + ", mRequestType=" + this.mRequestType + '}';
    }

    public static Request createRequest(String action) {
        Request request = new Request();
        request.mAction = action;
        return request;
    }

    private Request() {
    }

    public static Request createFromJSON(String json) {
        JSONException e;
        Request request = null;
        try {
            JSONObject jsonObject;
            Request request2 = new Request();
            try {
                jsonObject = new JSONObject(json);
            } catch (JSONException e2) {
                e = e2;
                request = request2;
                Debug.printException("Request", e);
                return request;
            }
            JSONObject jSONObject;
            try {
                request2.initObjectFromJSON(jsonObject);
                jSONObject = jsonObject;
                return request2;
            } catch (JSONException e3) {
                e = e3;
                jSONObject = jsonObject;
                request = request2;
                Debug.printException("Request", e);
                return request;
            }
        } catch (JSONException e4) {
            e = e4;
            Debug.printException("Request", e);
            return request;
        }
    }

    public int getRetryCount() {
        return this.mRetryCount;
    }

    public void addRetryCount() {
        this.mRetryCount++;
    }

    public String getUUID() {
        return this.mUUID;
    }

    public void setUUID(String UUID) {
        this.mUUID = UUID;
    }

    public String getAction() {
        return this.mAction;
    }

    public Map<String, String> getData() {
        return this.mData;
    }

    public void setParam(String key, String value) {
        if (this.mData == null) {
            this.mData = new TreeMap();
        }
        this.mData.put(key, value);
    }

    public String getParam(String key) {
        if (this.mData == null) {
            return null;
        }
        return (String) this.mData.get(key);
    }

    void setData(Map<String, String> data) {
        this.mData = data;
    }

    public String toJSON() {
        JSONObject obj = new JSONObject();
        try {
            initJSONFromObject(obj);
        } catch (JSONException e) {
            Debug.printException("Request", e);
        }
        return obj.toString();
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("action", this.mAction);
        jsonObject.put("uuid", this.mUUID);
        jsonObject.put("retry_count", this.mRetryCount);
        if (this.mData != null) {
            jsonObject.put("data", new JSONObject(this.mData));
        }
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.mAction = jsonObject.getString("action");
        this.mUUID = jsonObject.getString("uuid");
        this.mRetryCount = jsonObject.getInt("retry_count");
        JSONObject data = jsonObject.getJSONObject("data");
        Iterator<String> keys = data.keys();
        this.mData = new TreeMap();
        while (keys.hasNext()) {
            String k = (String) keys.next();
            this.mData.put(k, data.getString(k));
        }
    }
}
