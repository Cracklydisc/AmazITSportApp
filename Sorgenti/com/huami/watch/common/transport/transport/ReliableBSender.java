package com.huami.watch.common.transport.transport;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.huami.watch.common.db.AsyncDatabaseManager;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.transport.IResponseReceiver;
import com.huami.watch.common.transport.strategy.IResendStrategy;
import com.huami.watch.common.transport.strategy.IStrategyListener;
import java.lang.ref.WeakReference;
import java.util.List;

public class ReliableBSender implements IStrategyListener {
    private static Handler mChildThreadHandler = null;
    private Context mContext;
    private AsyncDatabaseManager<Request> mRequestDataManager;
    private IResendStrategy mResendStrategy;
    private BTransporter mTransporter;

    private static class TransportHandler extends Handler {
        private WeakReference<ReliableBSender> mTransporterWeakReference;

        public void handleMessage(Message msg) {
            final ReliableBSender transporter = (ReliableBSender) this.mTransporterWeakReference.get();
            if (transporter != null) {
                switch (msg.what) {
                    case 1:
                        transporter.mRequestDataManager.selectAll(transporter.mContext, null, null, null, null, new Callback() {
                            protected void doCallback(int resultCode, Object params) {
                                List<Request> requestList = (List) params;
                                if (requestList == null) {
                                    Debug.m6w("ReliableBSender", "request list is null");
                                    return;
                                }
                                for (Request r : requestList) {
                                    transporter.send(r, null);
                                }
                            }
                        });
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void onResend() {
        mChildThreadHandler.obtainMessage(1).sendToTarget();
    }

    public boolean send(final Request request, final IResponseReceiver response) {
        Debug.m3d("ReliableBSender", "send : " + request + ", response : " + response);
        if (request.getRetryCount() == 0) {
            this.mRequestDataManager.add(this.mContext, request, null);
        }
        return this.mTransporter.send(request, new IResponseReceiver() {
            public void onResponse(int code, Object r, String errorMsg) {
                Debug.m3d("ReliableBSender", "onResponse. code : " + code + ", errorMsg : " + errorMsg);
                if (code == 0) {
                    ReliableBSender.this.mRequestDataManager.remove(ReliableBSender.this.mContext, "uuid=?", new String[]{request.getUUID()}, null);
                } else {
                    request.addRetryCount();
                    ReliableBSender.this.mRequestDataManager.update(ReliableBSender.this.mContext, request, null);
                    ReliableBSender.this.mResendStrategy.planNextRequest(ReliableBSender.this);
                }
                if (response != null) {
                    response.onResponse(code, r, errorMsg);
                }
            }
        });
    }
}
