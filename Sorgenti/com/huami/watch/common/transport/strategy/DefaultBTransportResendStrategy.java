package com.huami.watch.common.transport.strategy;

import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.ChannelListener;

public class DefaultBTransportResendStrategy implements IResendStrategy {
    private Transporter mTransporter;

    private class MyChannelListener implements ChannelListener {
        private IStrategyListener mStrategyListener = null;

        public MyChannelListener(IStrategyListener strategyListener) {
            this.mStrategyListener = strategyListener;
        }

        public void onChannelChanged(boolean b) {
            if (b && this.mStrategyListener != null) {
                DefaultBTransportResendStrategy.this.mTransporter.removeChannelListener(this);
                this.mStrategyListener.onResend();
            }
        }
    }

    public void planNextRequest(IStrategyListener listener) {
        this.mTransporter.addChannelListener(new MyChannelListener(listener));
    }
}
