package com.huami.watch.common.transport.strategy;

public interface IStrategyListener {
    void onResend();
}
