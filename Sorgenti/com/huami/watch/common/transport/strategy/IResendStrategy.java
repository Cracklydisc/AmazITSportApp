package com.huami.watch.common.transport.strategy;

public interface IResendStrategy {
    void planNextRequest(IStrategyListener iStrategyListener);
}
