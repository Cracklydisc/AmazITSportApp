package com.huami.watch.common.transport.http;

public class HttpResponse {
    private String mContent = "";
    private String mErrorMsg = "";
    private String mRequestId = null;
    private int mResponseCode = 0;
    private String mResponseMessage = "";
    private int mResultCode = 0;

    void setRequestId(String requestId) {
        this.mRequestId = requestId;
    }

    private HttpResponse() {
    }

    public static HttpResponse createHttpResponse(String requestId) {
        if (requestId == null) {
            throw new IllegalArgumentException("request id should not be null");
        } else if (requestId.isEmpty()) {
            throw new IllegalArgumentException("request id should not be empty");
        } else {
            HttpResponse response = new HttpResponse();
            response.setRequestId(requestId);
            return response;
        }
    }

    public String getResponseMessage() {
        return this.mResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.mResponseMessage = responseMessage;
    }

    public String toString() {
        return "HttpResponse{mContent='" + this.mContent + '\'' + ", mResponseCode=" + this.mResponseCode + ", mResponseMessage='" + this.mResponseMessage + '\'' + ", mResultCode=" + this.mResultCode + ", mErrorMsg='" + this.mErrorMsg + '\'' + '}';
    }

    public int getResponseCode() {
        return this.mResponseCode;
    }

    public void setResponseCode(int responseCode) {
        this.mResponseCode = responseCode;
    }

    public String getErrorMsg() {
        return this.mErrorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.mErrorMsg = errorMsg;
    }

    public int getResultCode() {
        return this.mResultCode;
    }

    public void setResultCode(int resultCode) {
        this.mResultCode = resultCode;
    }

    public String getContent() {
        return this.mContent;
    }

    public void setContent(String content) {
        this.mContent = content;
    }
}
