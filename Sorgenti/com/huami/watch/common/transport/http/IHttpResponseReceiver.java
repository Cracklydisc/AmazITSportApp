package com.huami.watch.common.transport.http;

public interface IHttpResponseReceiver {
    void onResponse(HttpRequest httpRequest, HttpResponse httpResponse);
}
