package com.huami.watch.common.transport.http;

import com.huami.watch.common.log.Debug;
import com.huami.watch.common.transport.transport.BTransporter.IBRequestListener;
import com.huami.watch.common.transport.transport.ReliableBSender;
import com.huami.watch.common.transport.transport.Request;
import java.util.Map;

public class HttpTransporterServer implements IHttpResponseReceiver, IBRequestListener {
    private IHttpSender mHttpSender;
    private ReliableBSender mReliableTransporter;

    public void onBRequest(Request request) {
        Debug.m3d("HttpTransporterServer", "onBRequest." + request);
        HttpRequest httpRequest = HttpRequest.createGetRequest();
        Map<String, String> data = request.getData();
        httpRequest.setUrl((String) data.get("__url__"));
        httpRequest.setRequestMethod((String) data.get("__method__"));
        for (String k : data.keySet()) {
            httpRequest.setParam(k, (String) data.get(k));
        }
        if (this.mHttpSender == null) {
            Debug.m6w("HttpTransporterServer", "http sender is null");
        } else {
            this.mHttpSender.sendHttpRequest(httpRequest, this);
        }
    }

    public void onResponse(HttpRequest httpRequest, HttpResponse httpResponse) {
        Debug.m3d("HttpTransporterServer", "onResponse. httpRequest:" + httpRequest + ", httpResponse : " + httpResponse);
        Request request = Request.createRequest("http_response");
        request.setParam("__req_uuid__", httpRequest.getRequestId());
        request.setParam("__resp_code__", "" + httpResponse.getResponseCode());
        request.setParam("__resp_content__", httpResponse.getContent());
        request.setParam("__resp_msg__", httpResponse.getResponseMessage());
        this.mReliableTransporter.send(request, null);
    }
}
