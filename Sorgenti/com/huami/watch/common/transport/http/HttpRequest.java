package com.huami.watch.common.transport.http;

import com.huami.watch.common.JSONAble;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpRequest implements JSONAble {
    private Map<String, String> mParams = new TreeMap();
    private String mRequestId = null;
    private String mRequestMethod = "GET";
    private int mStatus = 1;
    private String mUrl = null;

    private static final class JSONKeys {
        private JSONKeys() {
        }
    }

    public int getStatus() {
        return this.mStatus;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }

    private HttpRequest() {
    }

    public static HttpRequest createGetRequest() {
        HttpRequest request = new HttpRequest();
        request.setRequestMethod("GET");
        request.mRequestId = UUID.randomUUID().toString();
        return request;
    }

    public String getRequestId() {
        return this.mRequestId;
    }

    public String getRequestMethod() {
        return this.mRequestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.mRequestMethod = requestMethod;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public void setParam(String key, String value) {
        this.mParams.put(key, value);
    }

    public String toString() {
        return "HttpRequest{mRequestMethod=" + this.mRequestMethod + ", mUrl='" + this.mUrl + '\'' + ", mParams=" + this.mParams + '}';
    }

    public static HttpRequest createFromJSON(String requestJSON) {
        HttpRequest httpRequest = new HttpRequest();
        try {
            httpRequest.initObjectFromJSON(new JSONObject(requestJSON));
            return httpRequest;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            initJSONFromObject(jsonObject);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("req_id", this.mRequestId);
        jsonObject.put("req_method", this.mRequestMethod);
        jsonObject.put("url", this.mUrl);
        jsonObject.put("params", new JSONObject(this.mParams));
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.mRequestId = jsonObject.optString("req_id");
        this.mRequestMethod = jsonObject.optString("req_method");
        this.mUrl = jsonObject.optString("url");
        JSONObject paramsObject = jsonObject.optJSONObject("params");
        this.mParams = new TreeMap();
        Iterator<String> keys = paramsObject.keys();
        while (keys.hasNext()) {
            String k = (String) keys.next();
            this.mParams.put(k, paramsObject.getString(k));
        }
    }
}
