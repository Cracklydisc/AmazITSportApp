package com.huami.watch.common.transport.http;

public interface IHttpSender {
    void sendHttpRequest(HttpRequest httpRequest, IHttpResponseReceiver iHttpResponseReceiver);
}
