package com.huami.watch.common.transport.http;

import com.huami.watch.common.log.Debug;
import com.huami.watch.common.transport.transport.BTransporter.IBRequestListener;
import com.huami.watch.common.transport.transport.Request;

public class HttpTransporterClient implements IBRequestListener {
    private IHttpResponseListener mResponseListener;

    public interface IHttpResponseListener {
        void onHttpResponse(HttpResponse httpResponse);
    }

    public void onBRequest(Request request) {
        Debug.m3d("HttpTransporterClient", "onBRequest|request:" + request);
        if (this.mResponseListener != null) {
            HttpResponse response = HttpResponse.createHttpResponse(request.getParam("__req_uuid__"));
            response.setResponseCode(Integer.parseInt(request.getParam("__resp_code__")));
            response.setResponseMessage(request.getParam("__resp_msg__"));
            response.setContent(request.getParam("__resp_content__"));
            this.mResponseListener.onHttpResponse(response);
        }
    }
}
