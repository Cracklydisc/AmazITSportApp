package com.huami.watch.common.transport.http;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import com.huami.watch.common.log.Debug;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DefaultHttpSendStrategy implements IHttpSendStrategy, IHttpSender {
    private ConnectivityManager mConnectivityManager;
    private Context mContext;
    private IntentFilter mFilter;

    private class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        private HttpRequest mHttpRequest = null;
        private IHttpResponseReceiver mResponseReceiver = null;

        public ConnectivityBroadcastReceiver(HttpRequest httpRequest, IHttpResponseReceiver responseReceiver) {
            this.mHttpRequest = httpRequest;
            this.mResponseReceiver = responseReceiver;
        }

        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.getType() == 1) {
                DefaultHttpSendStrategy.this.sendHttpRequest(this.mHttpRequest, this.mResponseReceiver);
                DefaultHttpSendStrategy.this.mContext.unregisterReceiver(this);
            }
        }
    }

    private static class HttpRequestTask extends AsyncTask<Void, Void, HttpResponse> {
        private HttpRequest mHttpRequest = null;
        private IHttpResponseReceiver mResponseReceiver = null;
        private WeakReference<DefaultHttpSendStrategy> mSendStrategyWeakReference = null;

        public HttpRequestTask(DefaultHttpSendStrategy sendStrategy, HttpRequest request, IHttpResponseReceiver responseReceiver) {
            this.mHttpRequest = request;
            this.mSendStrategyWeakReference = new WeakReference(sendStrategy);
            this.mResponseReceiver = responseReceiver;
        }

        protected HttpResponse doInBackground(Void... params) {
            DefaultHttpSendStrategy sendStrategy = (DefaultHttpSendStrategy) this.mSendStrategyWeakReference.get();
            if (sendStrategy == null) {
                return null;
            }
            return sendStrategy.send(this.mHttpRequest);
        }

        protected void onPostExecute(HttpResponse httpResponse) {
            if (this.mSendStrategyWeakReference == null) {
                Debug.m6w("DefaultHttpSendStrategy", "sender reference is null");
                return;
            }
            DefaultHttpSendStrategy defaultHttpSendStrategy = (DefaultHttpSendStrategy) this.mSendStrategyWeakReference.get();
            if (defaultHttpSendStrategy == null) {
                Debug.m5i("DefaultHttpSendStrategy", "sender is null");
            } else {
                defaultHttpSendStrategy.onResponse(this.mHttpRequest, httpResponse, this.mResponseReceiver);
            }
        }
    }

    private void onResponse(HttpRequest request, HttpResponse response, IHttpResponseReceiver responseReceiver) {
        Debug.m3d("DefaultHttpSendStrategy", "onResponse : " + response);
        if (response == null) {
            response = HttpResponse.createHttpResponse(request.getRequestId());
            response.setResultCode(2);
            response.setErrorMsg("Cannot get response, may be request invalid.");
            responseReceiver.onResponse(request, response);
        } else if (response.getResultCode() == 1) {
            Debug.m3d("DefaultHttpSendStrategy", "request failed. " + response.getErrorMsg());
            planNextRequest(request, responseReceiver);
        } else {
            responseReceiver.onResponse(request, response);
        }
    }

    public void planNextRequest(HttpRequest httpRequest, IHttpResponseReceiver receiver) {
        this.mContext.registerReceiver(new ConnectivityBroadcastReceiver(httpRequest, receiver), this.mFilter);
    }

    public void sendHttpRequest(HttpRequest request, IHttpResponseReceiver receiver) {
        Debug.m3d("DefaultHttpSendStrategy", "sendHttpRequest : " + request);
        request.setStatus(1);
        NetworkInfo networkInfo = this.mConnectivityManager.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            planNextRequest(request, receiver);
        } else {
            new HttpRequestTask(this, request, receiver).execute(new Void[0]);
        }
    }

    public HttpResponse send(HttpRequest request) {
        HttpResponse response = HttpResponse.createHttpResponse(request.getRequestId());
        try {
            URL url = new URL(request.getUrl());
            try {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod(request.getRequestMethod());
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.connect();
                response.setResponseCode(conn.getResponseCode());
                response.setResultCode(0);
                response.setResponseMessage(conn.getResponseMessage());
                response.setContent(readInputStream(conn.getInputStream(), conn.getContentEncoding()));
            } catch (IOException e) {
                Debug.printException("DefaultHttpSendStrategy", e);
                response.setResultCode(1);
                response.setErrorMsg(e.getLocalizedMessage());
            }
            URL url2 = url;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            response.setResultCode(2);
        }
        return response;
    }

    private String readInputStream(InputStream in, String charsetName) {
        if (charsetName == null) {
            try {
                charsetName = "UTF-8";
            } catch (UnsupportedEncodingException e) {
                Debug.printException("DefaultHttpSendStrategy", e);
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, charsetName));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                return sb.toString();
            }
            sb.append(line);
        }
    }
}
