package com.huami.watch.common.log;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Debug {
    private static final Level LOG_LEVEL = Level.Verb;
    private static File file = null;
    private static BufferedWriter gBufferedWriter = null;
    private static Context gContext = null;
    private static ExecutorService gExecutorService;
    private static String gTag = "";

    static class C03931 implements FilenameFilter {
        C03931() {
        }

        public boolean accept(File dir, String filename) {
            if (filename.startsWith("log")) {
                return true;
            }
            return false;
        }
    }

    static class C03942 implements Comparator<File> {
        C03942() {
        }

        public int compare(File lhs, File rhs) {
            if (lhs.lastModified() < rhs.lastModified()) {
                return -1;
            }
            if (lhs.lastModified() > rhs.lastModified()) {
                return 1;
            }
            return 0;
        }
    }

    static class C03953 extends AsyncTask<Void, Void, Void> {
        final /* synthetic */ Level val$level;
        final /* synthetic */ String val$msg;
        final /* synthetic */ String val$tag;
        final /* synthetic */ long val$threadId;
        final /* synthetic */ long val$timestamp;

        protected Void doInBackground(Void... voids) {
            try {
                BufferedWriter writer = Debug.getWriter(Debug.gContext);
                if (writer != null) {
                    writer.write("[" + DataFormatUtils.parseMilliSecondToFormattedTime(this.val$timestamp, "yyyy.MM.dd HH:mm:ss:SSS", true) + "][Thread:" + this.val$threadId + "][" + this.val$level.name() + "]" + "[" + this.val$tag + "] " + this.val$msg + "\n");
                    writer.flush();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            return null;
        }
    }

    private enum Level {
        Verb(0),
        Debug(1),
        Info(2),
        Warn(3),
        Error(4),
        Disable(5);
        
        private int mValue;

        private Level(int value) {
            this.mValue = -1;
            this.mValue = value;
        }
    }

    static {
        gExecutorService = null;
        gExecutorService = Executors.newSingleThreadExecutor();
    }

    public static void init(Context context) {
        gContext = context;
    }

    public static void setTag(String tag) {
        gTag = tag;
    }

    public static void printException(String tag, Throwable throwable) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        throwable.printStackTrace(new PrintStream(out));
        m4e(tag, out.toString());
    }

    private static BufferedWriter getWriter(Context context) throws FileNotFoundException {
        if (context == null) {
            return null;
        }
        if (file == null) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(1);
            int month = calendar.get(2);
            int date = calendar.get(5);
            int hour = calendar.get(11);
            File[] files = context.getFilesDir().listFiles(new C03931());
            if (3 > null && files != null && files.length > 3) {
                Arrays.sort(files, new C03942());
                int deleteNum = files.length - 3;
                Log.d(gTag, "sorted files : " + Arrays.toString(files));
                for (int i = 0; i < deleteNum; i++) {
                    Log.d(gTag, "delete file : " + files[i].getName());
                    files[i].delete();
                }
            }
            file = new File(context.getFilesDir(), "log_" + year + "_" + month + "_" + date + "_" + hour + ".txt");
        }
        if (gBufferedWriter == null) {
            gBufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
        }
        return gBufferedWriter;
    }

    public static void m6w(String tag, String msg) {
        if (msg == null) {
            msg = "null";
        }
        Log.w(gTag, tag + " -- " + msg);
    }

    public static void m5i(String tag, String msg) {
        if (msg == null) {
            msg = "null";
        }
        Log.i(gTag, tag + " -- " + msg);
    }

    public static void m4e(String tag, String msg) {
        if (msg == null) {
            msg = "null";
        }
        Log.e(gTag, tag + " -- " + msg);
    }

    public static void m3d(String tag, String msg) {
        if (msg == null) {
            msg = "null";
        }
        Log.d(gTag, tag + " -- " + msg);
    }
}
