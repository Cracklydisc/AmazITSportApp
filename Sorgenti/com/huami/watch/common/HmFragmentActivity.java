package com.huami.watch.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;

public class HmFragmentActivity extends FragmentActivity {
    private KeyEventDispatcher keyEventDispatcher;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        View xv = ((ViewGroup) getWindow().getDecorView()).getChildAt(0);
        if (xv != null && xv.getClass().getName().endsWith("SwipeDismissLayout")) {
            xv.setFitsSystemWindows(false);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        this.keyEventDispatcher.dispatchKeyevent((Activity) this, event);
        return super.dispatchKeyEvent(event);
    }
}
