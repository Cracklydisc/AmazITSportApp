package com.huami.watch.common;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;
import com.huami.watch.menu.IMenuControllContent.MenuListenner;
import com.huami.watch.menu.IMenuControllContent.MenuOnItemClickListener;
import com.huami.watch.menu.MenuController;
import com.huami.watch.menu.MenuSwipeDismissLayout;
import com.huami.watch.menu.MenuSwipeDismissLayout.OnDismissedListener;
import com.huami.watch.menu.MenuSwipeDismissLayout.OnSwipeProgressChangedListener;
import com.huami.watch.menu.NewMenuLayout;
import com.huami.watch.menu.NewMenuLayout.SimpleMenuAdpter;

public abstract class HmMenuActivity extends HmFragmentActivity implements MenuListenner, MenuOnItemClickListener {
    private KeyEventDispatcher mKeyEventDispatcher;
    private MenuController mMenuController = null;
    private NewMenuLayout mMenuWearLayout = null;
    private MenuSwipeDismissLayout mRootLayout = null;

    class C03821 extends NewMenuLayout {
        final /* synthetic */ HmMenuActivity this$0;

        public void notifyMenuJustPosition() {
            super.notifyMenuJustPosition();
            this.this$0.MenuVisibleToUser();
        }
    }

    class C03832 implements OnDismissedListener {
        final /* synthetic */ HmMenuActivity this$0;

        public void onDismissed(MenuSwipeDismissLayout layout) {
            this.this$0.onConfigDismiss();
        }
    }

    class C03843 implements OnDismissedListener {
        public void onDismissed(MenuSwipeDismissLayout layout) {
        }
    }

    class C03854 implements OnSwipeProgressChangedListener {
        final /* synthetic */ HmMenuActivity this$0;

        public void onSwipeProgressChanged(MenuSwipeDismissLayout layout, float progress, float translate) {
            int flags;
            LayoutParams newParams = this.this$0.getWindow().getAttributes();
            newParams.x = (int) translate;
            newParams.alpha = 1.0f - (0.5f * progress);
            this.this$0.getWindow().setAttributes(newParams);
            if (newParams.x == 0) {
                flags = 1024;
            } else {
                flags = 512;
            }
            this.this$0.getWindow().setFlags(flags, 1536);
        }

        public void onSwipeCancelled(MenuSwipeDismissLayout layout) {
            LayoutParams newParams = this.this$0.getWindow().getAttributes();
            newParams.x = 0;
            newParams.alpha = 1.0f;
            this.this$0.getWindow().setAttributes(newParams);
            this.this$0.getWindow().setFlags(1024, 1536);
        }
    }

    public static abstract class HmMeunAdaper extends SimpleMenuAdpter {
        public void onDataChanged() {
        }
    }

    protected void onConfigDismiss() {
    }

    private void initBeforeSetContent() {
        this.mRootLayout = new MenuSwipeDismissLayout(this);
        this.mMenuController = this.mRootLayout.getMenuController();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void onResume() {
        super.onResume();
    }

    public void setContentView(View view) {
        initBeforeSetContent();
        super.setContentView(this.mRootLayout);
        this.mRootLayout.addView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        initBeforeSetContent();
        super.setContentView(this.mRootLayout);
        this.mRootLayout.addView(view, params);
    }

    public void setContentView(int layoutResID) {
        initBeforeSetContent();
        View contentView = getLayoutInflater().inflate(layoutResID, this.mRootLayout, false);
        super.setContentView(this.mRootLayout);
        this.mRootLayout.addView(contentView);
    }

    protected void MenuVisibleToUser() {
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.mKeyEventDispatcher == null) {
            this.mKeyEventDispatcher = new KeyEventDispatcher();
        }
        this.mKeyEventDispatcher.dispatchKeyevent((Activity) this, event);
        return super.dispatchKeyEvent(event);
    }
}
