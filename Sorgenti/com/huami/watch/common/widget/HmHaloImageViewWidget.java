package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import com.huami.watch.utils.QuadInterpolator;

public class HmHaloImageViewWidget extends HmAnimatedOverlayImageView {
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private float SCALE_FROM;
    private float SCALE_TO;
    private boolean clickFinished;
    private Runnable clickTask;
    private boolean mLoading;
    private OnClickListener mOnClickListener;
    private IClickStateListener mStateListener;
    private boolean onClickAfterAnimation;

    class C04291 implements Runnable {
        C04291() {
        }

        public void run() {
            if (HmHaloImageViewWidget.this.mOnClickListener != null) {
                HmHaloImageViewWidget.this.mOnClickListener.onClick(HmHaloImageViewWidget.this);
            }
            HmHaloImageViewWidget.this.clickFinished = true;
        }
    }

    class C04302 implements OnClickListener {
        C04302() {
        }

        public void onClick(View v) {
            if (HmHaloImageViewWidget.this.mStateListener != null) {
                HmHaloImageViewWidget.this.mStateListener.enterClickState();
            }
            if (!HmHaloImageViewWidget.this.clickFinished) {
                return;
            }
            if (HmHaloImageViewWidget.this.onClickAfterAnimation) {
                if (HmHaloImageViewWidget.this.mLoading) {
                    HmHaloImageViewWidget.this.playLoadingAnimationAfterClick(HmHaloImageViewWidget.this.clickTask);
                } else {
                    HmHaloImageViewWidget.this.playClickAnimation(HmHaloImageViewWidget.this.clickTask);
                }
                HmHaloImageViewWidget.this.clickFinished = false;
                return;
            }
            if (HmHaloImageViewWidget.this.mLoading) {
                HmHaloImageViewWidget.this.playLoadingAnimationAfterClick(null);
            } else {
                HmHaloImageViewWidget.this.playClickAnimation(null);
            }
            if (HmHaloImageViewWidget.this.clickTask != null) {
                HmHaloImageViewWidget.this.clickTask.run();
            }
            HmHaloImageViewWidget.this.clickFinished = true;
        }
    }

    public interface IClickStateListener {
        void enterClickState();
    }

    static {
        HmAnimatedOverlayImageView.registerAnimatorDrawableTypeInternal(-101, HmOverlayClickHaloDrawble.class);
        HmAnimatedOverlayImageView.registerAnimatorDrawableTypeInternal(-102, HmOverlayLoadingDrawble.class);
    }

    public HmHaloImageViewWidget(Context context) {
        this(context, null);
    }

    public HmHaloImageViewWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HmHaloImageViewWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.clickFinished = true;
        this.clickTask = new C04291();
        this.onClickAfterAnimation = true;
        this.mLoading = false;
        this.SCALE_FROM = 1.0f;
        this.SCALE_TO = 0.9f;
        addOverlayAnimationFeature(-101);
        addOverlayAnimationFeature(-102);
    }

    private void initClickTask() {
        super.setOnClickListener(new C04302());
    }

    public void playClickAnimation(final Runnable callback) {
        ScaleAnimation mScaleAnim = new ScaleAnimation(this.SCALE_FROM, this.SCALE_TO, this.SCALE_FROM, this.SCALE_TO, 1, 0.5f, 1, 0.5f);
        mScaleAnim.setInterpolator(QUAD_OUT);
        mScaleAnim.setDuration(125);
        mScaleAnim.setRepeatCount(1);
        mScaleAnim.setRepeatMode(2);
        mScaleAnim.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                HmHaloImageViewWidget.this.playOverlayDrawableAnimation(callback, -101);
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(mScaleAnim);
    }

    public void playLoadingAnimationAfterClick(final Runnable callback) {
        ScaleAnimation mScaleAnim = new ScaleAnimation(this.SCALE_FROM, this.SCALE_TO, this.SCALE_FROM, this.SCALE_TO, 1, 0.5f, 1, 0.5f);
        mScaleAnim.setInterpolator(QUAD_OUT);
        mScaleAnim.setDuration(125);
        mScaleAnim.setRepeatCount(1);
        mScaleAnim.setRepeatMode(2);
        mScaleAnim.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                HmHaloImageViewWidget.this.setEnabled(false);
            }

            public void onAnimationRepeat(Animation animation) {
                HmHaloImageViewWidget.this.playOverlayDrawableAnimation(callback, -101, -102);
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(mScaleAnim);
    }

    public void setOnClickListener(OnClickListener l) {
        if (l != null) {
            if (!isClickable()) {
                setClickable(true);
            }
            initClickTask();
        } else {
            super.setOnClickListener(null);
            this.clickFinished = true;
        }
        this.mOnClickListener = l;
    }
}
