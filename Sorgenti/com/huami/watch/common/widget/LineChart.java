package com.huami.watch.common.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import com.huami.watch.math3.analysis.interpolation.SplineInterpolator;
import com.huami.watch.ui.C1070R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class LineChart<T extends Number> extends XAxisChart {
    private static final float[] BENCH_SECTION = new float[]{0.0f, 0.14f, 0.36f, 0.64f, 0.86f, 1.0f};
    private static final float[] HEART_BENCH_SECTION = new float[]{0.0f, 0.33f, 1.0f};
    private int mAltitudeEndColor;
    private int mAltitudeStartColor;
    private float mAveCoordinate;
    private T mAveValue;
    private float mAverageHundredTextPadding;
    private Paint mAveragePaint;
    private String mAverageText;
    private float mAverageTextPaddingLeft;
    private Paint mAverageTextPaint;
    private Shader mBenchGradient;
    private float mBenchmarkCoordinate;
    private boolean mBenchmarkEnabled;
    private Paint mBenchmarkPaint;
    private Path mBenchmarkPath;
    private T mBenchmarkValue;
    private int mChartType;
    private Paint mClosePathPaint;
    private Context mContext;
    private int[] mCoverColors;
    private float[] mCoverRanges;
    private int[] mCurveColors;
    private float[] mCurveRanges;
    private boolean mDataProvided;
    protected boolean mDrawStave;
    private float mEveryPointValueHeight;
    private int mGreenColor;
    private AtomicBoolean mIsDataLoadFinished;
    private boolean mIsDrawMinOrMax;
    private boolean mIsShowAverageLine;
    private boolean mIsShowBenchZone;
    private boolean mIsShowDates;
    private boolean mIsShowDigitTop;
    private boolean mIsShownCoverColor;
    private boolean mIsShownLineNum;
    private float mLineWidth;
    private ILineChartLoadingListener mLoadingListener;
    private float mMaxBenchmarkCoordinate;
    private T mMaxBenchmarkValue;
    private float mMaxCoordinate;
    private T mMaxOrigin;
    private boolean mMaxPainted;
    private float mMinBenchmarkCoordinate;
    private T mMinBenchmarkValue;
    private float mMinCoordinate;
    private boolean mMinMaxFound;
    private float mMinOrMaxMargin;
    private T mMinOrigin;
    private boolean mMinPainted;
    private String mNoDataHint;
    private PointF mNoDataLocation;
    private Paint mNoDataPaint;
    private float mNoyAxisPaddingLeft;
    private boolean mPaintBenchmark;
    private float mPointPaddingLeft;
    private int mRedColor;
    private boolean mShowEveryPoint;
    protected float mStaveLabelWidth;
    private float[] mStavePositions;
    private float mVMinMaxHeight;
    private float mVMinOffset;
    private Paint mValueMinMaxPaint;
    private List<Path> mValueSetJointClosedPaths;
    private Paint mValueSetJointPaint;
    private List<Path> mValueSetJointPaths;
    private Paint mValueSetLineLowerPaint;
    private List<ValueSet> mValues;
    private int mYellowColor;

    class C04731 implements AnimatorUpdateListener {
        final /* synthetic */ LineChart this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            this.this$0.x = ((Float) animation.getAnimatedValue()).floatValue();
            this.this$0.invalidate();
        }
    }

    class C04742 implements AnimatorUpdateListener {
        C04742() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            LineChart.this.y = ((Float) animation.getAnimatedValue()).floatValue();
            LineChart.this.invalidate();
        }
    }

    public static final class ChartType {
    }

    public interface ILineChartLoadingListener {
        void onLoadingFinished();
    }

    private class ValueSet implements Comparable<ValueSet> {
        private List<PointF> dots;
        private PointF first;
        private PointF last;
        private List<PointF> maxDots;
        private List<PointF> minDots;
        private Path requiredClosedPath;
        private Path requiredPath;
        final /* synthetic */ LineChart this$0;
        private List<T> values;
        private List<Float> xIndices;

        public float minCoordinate() {
            if (this.dots.size() < 1) {
                throw new IllegalStateException();
            }
            float min = Float.MAX_VALUE;
            for (PointF p : this.dots) {
                if (min > p.y) {
                    min = p.y;
                }
            }
            return min;
        }

        public float maxCoordinate() {
            if (this.dots.size() < 1) {
                throw new IllegalStateException();
            }
            float max = -3.4028235E38f;
            for (PointF p : this.dots) {
                if (max < p.y) {
                    max = p.y;
                }
            }
            return max;
        }

        public PointF first() {
            return this.first;
        }

        public PointF last() {
            return this.last;
        }

        public T minOrigin() {
            T min = (Number) this.values.get(0);
            for (int i = 1; i < this.values.size(); i++) {
                if (((Number) this.values.get(i)).doubleValue() < min.doubleValue()) {
                    min = (Number) this.values.get(i);
                }
            }
            return min;
        }

        public T maxOrigin() {
            T max = (Number) this.values.get(0);
            for (int i = 1; i < this.values.size(); i++) {
                if (((Number) this.values.get(i)).doubleValue() > max.doubleValue()) {
                    max = (Number) this.values.get(i);
                }
            }
            return max;
        }

        public void draw(Canvas canvas) {
            if (this.this$0.mChartType == 2 || this.this$0.mIsShownCoverColor) {
                canvas.drawPath(this.requiredClosedPath, this.this$0.mClosePathPaint);
            }
            canvas.drawPath(this.requiredPath, this.this$0.mValueSetLineLowerPaint);
            if (this.this$0.mShowEveryPoint) {
                drawEveryPoint(canvas);
            } else {
                drawMinMax(canvas);
            }
        }

        private void drawEveryPoint(Canvas canvas) {
            int i = 0;
            for (PointF point : this.dots) {
                float numWicth = this.this$0.mValueMinMaxPaint.measureText(String.valueOf(point.y));
                Bitmap minBitmap = BitmapFactory.decodeResource(this.this$0.getResources(), C1070R.drawable.sport_heart_min);
                this.this$0.mEveryTextPaint.setColor(this.this$0.mContext.getResources().getColor(C1070R.color.black));
                canvas.drawCircle(point.x, point.y, 4.0f, this.this$0.mEveryTextPaint);
                this.this$0.mEveryTextPaint.setColor(this.this$0.mContext.getResources().getColor(C1070R.color.white));
                canvas.drawCircle(point.x, point.y, 3.0f, this.this$0.mEveryTextPaint);
                canvas.drawText(String.valueOf(((Number) this.values.get(i)).intValue()), point.x - (this.this$0.mEveryTextPaint.measureText(String.valueOf(((Number) this.values.get(i)).intValue())) / 2.0f), (point.y - this.this$0.mMinOrMaxMargin) - 2.0f, this.this$0.mEveryTextPaint);
                Log.i("LineChart", "every:" + point.y + " local" + (point.x - (numWicth / 2.0f)));
                i++;
                minBitmap.recycle();
            }
        }

        private void drawMinMax(Canvas canvas) {
            if (this.this$0.mIsDrawMinOrMax) {
                String str;
                String max = String.valueOf(this.this$0.mMaxOrigin.intValue());
                String min = String.valueOf(this.this$0.mMinOrigin.intValue());
                float minWidth = this.this$0.mValueMinMaxPaint.measureText(min);
                float maxWidth = this.this$0.mValueMinMaxPaint.measureText(max);
                for (PointF point : this.minDots) {
                    if (!this.this$0.mMinPainted) {
                        Bitmap minBitmap = BitmapFactory.decodeResource(this.this$0.getResources(), C1070R.drawable.sport_heart_min);
                        canvas.drawBitmap(minBitmap, point.x - ((float) (minBitmap.getWidth() / 2)), (point.y - ((float) (minBitmap.getHeight() / 2))) - this.this$0.mVMinMaxHeight, this.this$0.mValueMinMaxPaint);
                        if (this.this$0.mYLabels != null) {
                            str = this.this$0.mYLabels[0];
                        } else {
                            str = min;
                        }
                        canvas.drawText(str, this.this$0.mIsShowDigitTop ? point.x - (minWidth / 2.0f) : point.x + ((float) (minBitmap.getWidth() / 4)), point.y + this.this$0.mMinOrMaxMargin, this.this$0.mValueMinMaxPaint);
                        Log.i("LineChart", "min value:" + min);
                        this.this$0.mMinPainted = true;
                        minBitmap.recycle();
                    }
                }
                for (PointF point2 : this.maxDots) {
                    if (!this.this$0.mMaxPainted) {
                        Bitmap maxBitmap = BitmapFactory.decodeResource(this.this$0.getResources(), C1070R.drawable.sport_heart_max);
                        canvas.drawBitmap(maxBitmap, point2.x - ((float) (maxBitmap.getWidth() / 2)), point2.y - ((float) (maxBitmap.getHeight() / 2)), this.this$0.mValueMinMaxPaint);
                        if (this.this$0.mYLabels != null) {
                            str = this.this$0.mYLabels[this.this$0.mYLabels.length - 1];
                        } else {
                            str = max;
                        }
                        canvas.drawText(str, this.this$0.mIsShowDigitTop ? point2.x - (maxWidth / 2.0f) : point2.x + ((float) (maxBitmap.getWidth() / 4)), this.this$0.mIsShowDigitTop ? (point2.y - (this.this$0.mVMinMaxHeight / 2.0f)) - this.this$0.mMinOrMaxMargin : point2.y, this.this$0.mValueMinMaxPaint);
                        Log.i("LineChart", "max value:" + max);
                        this.this$0.mMaxPainted = true;
                        maxBitmap.recycle();
                    }
                }
            }
        }

        private void checkMinMax(float min, float max, PointF location) {
            if (this.this$0.mMinCoordinate >= this.this$0.mMaxCoordinate) {
                if (!this.this$0.mMinMaxFound) {
                    this.maxDots.add(location);
                    this.this$0.mMinMaxFound = true;
                }
            } else if (location.y <= this.this$0.mMinCoordinate) {
                this.maxDots.add(location);
            } else if (location.y >= this.this$0.mMaxCoordinate) {
                this.minDots.add(new PointF(location.x, location.y + this.this$0.mVMinMaxHeight));
            }
        }

        public void calculatePartsOnScreen(float min, float max, float benchmark) {
            this.requiredPath.reset();
            this.requiredClosedPath.reset();
            int size = this.dots.size();
            PointF point;
            if (size == 1) {
                point = (PointF) this.dots.get(0);
                checkMinMax(min, max, point);
                this.requiredPath.addOval(point.x - 3.0f, point.y - 3.0f, point.x + 3.0f, point.y + 3.0f, Direction.CCW);
            } else if (size == 2) {
                checkMinMax(min, max, (PointF) this.dots.get(0));
                checkMinMax(min, max, (PointF) this.dots.get(1));
                this.requiredPath.addPath(this.this$0.createPath((PointF) this.dots.get(0), (PointF) this.dots.get(1)));
                if (this.this$0.mChartType == 2 || this.this$0.mIsShownCoverColor) {
                    this.requiredClosedPath.addPath(this.this$0.createClosedPath(new PointF(((PointF) this.dots.get(0)).x, this.this$0.mDrawStave ? max : this.this$0.mYMaxHeight), (PointF) this.dots.get(0)));
                    this.requiredClosedPath.addPath(this.this$0.createClosedPath((PointF) this.dots.get(0), (PointF) this.dots.get(1)));
                    Path path = this.requiredClosedPath;
                    LineChart lineChart = this.this$0;
                    PointF pointF = (PointF) this.dots.get(1);
                    float f = ((PointF) this.dots.get(1)).x;
                    if (!this.this$0.mDrawStave) {
                        max = this.this$0.mYMaxHeight;
                    }
                    path.addPath(lineChart.createClosedPath(pointF, new PointF(f, max)));
                }
            } else if (size > 2) {
                PointF first = (PointF) this.dots.get(0);
                checkMinMax(min, max, first);
                this.requiredPath.moveTo(first.x, first.y);
                this.requiredClosedPath.moveTo(first.x, this.this$0.mDrawStave ? max : this.this$0.mYMaxHeight);
                this.requiredClosedPath.lineTo(first.x, first.y);
                for (int j = 1; j < this.dots.size(); j++) {
                    point = (PointF) this.dots.get(j);
                    checkMinMax(min, max, point);
                    this.requiredPath.lineTo(point.x, point.y);
                    this.requiredClosedPath.lineTo(point.x, point.y);
                    if (j == this.dots.size() - 1) {
                        this.requiredClosedPath.lineTo(point.x, this.this$0.mDrawStave ? max : this.this$0.mYMaxHeight);
                        this.requiredClosedPath.close();
                    }
                }
            }
        }

        public void calcDotsOnScreen(float left, float width, float top, float height, float offset, T min, T max, SplineInterpolator interpolator) {
            this.dots.clear();
            int size = this.values.size();
            if (size == 1) {
                this.dots.add(new PointF(left + ((((Float) this.xIndices.get(0)).floatValue() * width) / this.this$0.mMaxXSize), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(0)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
            } else if (size == 2) {
                this.dots.add(new PointF(left + (((Float) this.xIndices.get(0)).floatValue() * (width / this.this$0.mMaxXSize)), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(0)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
                this.dots.add(new PointF(left + (((Float) this.xIndices.get(1)).floatValue() * (width / this.this$0.mMaxXSize)), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(1)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
            } else if (size > 2) {
                double[] x = new double[size];
                double[] y = new double[size];
                for (int i = 0; i < size; i++) {
                    x[i] = (double) (((((Float) this.xIndices.get(i)).floatValue() * width) / this.this$0.mMaxXSize) + left);
                    y[i] = min.floatValue() >= max.floatValue() ? (double) ((height / 2.0f) + top) : (double) (((top + height) - offset) - (((((Number) this.values.get(i)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue())));
                    this.dots.add(new PointF((float) x[i], (float) y[i]));
                }
            }
        }

        public void optimizeDots(float top, float height, float offset) {
            float half = top + (height / 2.0f);
            float halfmax = (height / 2.0f) - offset;
            for (PointF dot : this.dots) {
                if (dot.y < half) {
                    if (dot.y == this.this$0.mMinCoordinate) {
                        dot.y = half - (((half - dot.y) * halfmax) / (half - this.this$0.mMinCoordinate));
                        this.this$0.mMinCoordinate = dot.y;
                    } else {
                        dot.y = half - (((half - dot.y) * halfmax) / (half - this.this$0.mMinCoordinate));
                    }
                } else if (dot.y > half) {
                    if (dot.y == this.this$0.mMaxCoordinate) {
                        dot.y = (((dot.y - half) * halfmax) / (this.this$0.mMaxCoordinate - half)) + half;
                        this.this$0.mMaxCoordinate = dot.y;
                    } else {
                        dot.y = (((dot.y - half) * halfmax) / (this.this$0.mMaxCoordinate - half)) + half;
                    }
                }
            }
            this.first = (PointF) this.dots.get(0);
            this.last = (PointF) this.dots.get(this.dots.size() - 1);
        }

        public int compareTo(ValueSet another) {
            if (((Float) this.xIndices.get(this.xIndices.size() - 1)).floatValue() < ((Float) another.xIndices.get(0)).floatValue()) {
                return -1;
            }
            return ((Float) this.xIndices.get(0)).floatValue() > ((Float) another.xIndices.get(another.xIndices.size() + -1)).floatValue() ? 1 : 0;
        }
    }

    public LineChart(Context context) {
        this(context, null);
    }

    public LineChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LineChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mVMinOffset = 0.0f;
        this.mVMinMaxHeight = 0.0f;
        this.mBenchmarkCoordinate = 0.0f;
        this.mMinBenchmarkCoordinate = 0.0f;
        this.mMaxBenchmarkCoordinate = 0.0f;
        this.mStavePositions = new float[4];
        this.mShowEveryPoint = false;
        this.mValues = new ArrayList(0);
        this.mValueSetJointPaths = new ArrayList();
        this.mValueSetJointClosedPaths = new ArrayList();
        this.mBenchmarkEnabled = false;
        this.mDataProvided = false;
        this.mBenchGradient = null;
        this.mIsShowAverageLine = false;
        this.mIsShowBenchZone = false;
        this.mIsShowDigitTop = false;
        this.mIsShowDates = false;
        this.mContext = null;
        this.mDrawStave = false;
        this.mStaveLabelWidth = 0.0f;
        this.mLineWidth = 0.0f;
        this.mChartType = 0;
        this.mIsDataLoadFinished = new AtomicBoolean(false);
        this.mLoadingListener = null;
        this.mEveryPointValueHeight = 0.0f;
        this.mIsShownCoverColor = false;
        this.mCoverColors = null;
        this.mCoverRanges = null;
        this.mIsShownLineNum = false;
        this.mIsDrawMinOrMax = true;
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        Resources res = context.getResources();
        this.mVMinMaxHeight = res.getDimension(C1070R.dimen.size_linechart_minmax_font);
        float lineWidth = res.getDimension(C1070R.dimen.size_linechart_line_width);
        float averageLineWidth = res.getDimension(C1070R.dimen.average_line_width);
        this.mAverageTextPaddingLeft = res.getDimension(C1070R.dimen.average_text_padding_left);
        this.mAverageHundredTextPadding = res.getDimension(C1070R.dimen.average_hundred_text_padding_left);
        this.mMinOrMaxMargin = res.getDimension(C1070R.dimen.min_max_margin);
        this.mNoyAxisPaddingLeft = res.getDimension(C1070R.dimen.size_linechart_no_yaxis_padding_left);
        float radius = res.getDimension(C1070R.dimen.size_shadow_radius1);
        float dy = res.getDimension(C1070R.dimen.size_shadow_dy1);
        float averageTextSize = res.getDimension(C1070R.dimen.average_text_size);
        int shadowColor = res.getColor(C1070R.color.color_shadow);
        int benchmarkColor = res.getColor(C1070R.color.color_chart_benchmark_line);
        int greenColor = res.getColor(C1070R.color.color_green);
        int redColor = res.getColor(C1070R.color.color_red);
        int grayColor = res.getColor(C1070R.color.average_line_color);
        int whiteColor = res.getColor(C1070R.color.average_text_color);
        this.mAltitudeStartColor = res.getColor(C1070R.color.altitude_start_color);
        this.mAltitudeEndColor = res.getColor(C1070R.color.altitude_end_color);
        this.mGreenColor = res.getColor(C1070R.color.bench_green_color);
        this.mYellowColor = res.getColor(C1070R.color.bench_yellow_color);
        this.mRedColor = res.getColor(C1070R.color.bench_red_color);
        PathEffect effect = new DashPathEffect(new float[]{8.0f, 8.0f}, 4.0f);
        PathEffect dashPathEffect = new DashPathEffect(new float[]{3.0f, 3.0f}, 0.0f);
        this.mValueSetLineLowerPaint = createLinePaint(lineWidth, greenColor, radius, dy, shadowColor, new CornerPathEffect(5.0f));
        this.mValueSetJointPaint = createLinePaint(lineWidth, greenColor, radius, dy, shadowColor, effect);
        this.mBenchmarkPaint = createLinePaint(lineWidth, benchmarkColor, radius, dy, shadowColor, effect);
        this.mAveragePaint = createLinePaint(averageLineWidth, grayColor, radius, dy, shadowColor, dashPathEffect);
        this.mClosePathPaint = new Paint(1);
        this.mClosePathPaint.setStyle(Style.FILL);
        this.mAverageTextPaint = new Paint(1);
        this.mAverageTextPaint.setTextSize(averageTextSize);
        this.mAverageTextPaint.setColor(whiteColor);
        this.mAverageTextPaint.setShadowLayer(radius, 0.0f, dy, shadowColor);
        this.mNoDataPaint = new Paint(1);
        this.mNoDataPaint.setTextSize(this.mVMinMaxHeight);
        this.mNoDataPaint.setColor(-1);
        this.mNoDataPaint.setShadowLayer(radius, 0.0f, dy, shadowColor);
        this.mValueMinMaxPaint = new Paint(this.mNoDataPaint);
        this.mValueMinMaxPaint.setTypeface(Typeface.DEFAULT_BOLD);
        setVMinOffset(res.getDimension(C1070R.dimen.size_linechart_yoffset));
        this.mNoDataHint = res.getString(C1070R.string.no_heartrate_data_hint);
        this.mAverageText = res.getString(C1070R.string.average_text);
        this.mStaveLabelWidth = this.mXAxisLabelPaint.measureText("000");
        this.mLineWidth = res.getDimension(C1070R.dimen.size_linechart_width);
        this.mPointPaddingLeft = res.getDimension(C1070R.dimen.size_linechart_point_padding_left);
        this.mEveryPointValueHeight = res.getDimension(C1070R.dimen.pickview_unit_size);
        this.mEveryTextPaint = new Paint(1);
        this.mEveryTextPaint.setTextSize(this.mEveryPointValueHeight);
        this.mEveryTextPaint.setColor(res.getColor(C1070R.color.white));
    }

    private Paint createLinePaint(float width, int color, float radius, float dy, int shadowColor, PathEffect effect) {
        Paint p = new Paint(1);
        p.setStrokeWidth(width);
        p.setStyle(Style.STROKE);
        p.setColor(color);
        p.setPathEffect(effect);
        p.setShadowLayer(radius, 0.0f, dy, shadowColor);
        return p;
    }

    private void drawStave(Canvas canvas) {
        if (this.mDrawStave) {
            float offset;
            float rightEdge;
            if (this.mMaxOrigin == null || this.mMinOrigin == null || this.mMaxOrigin.doubleValue() - this.mMinOrigin.doubleValue() <= 3.0d || !this.mIsShownLineNum) {
                offset = (((float) ((getWidth() - getPaddingStart()) - getPaddingEnd())) - this.mLineWidth) / 2.0f;
                rightEdge = ((float) (getWidth() - getPaddingStart())) - offset;
            } else {
                int min = this.mMinOrigin.intValue();
                int max = this.mMaxOrigin.intValue();
                int stave1 = min + ((max - min) / 3);
                int stave2 = max - ((max - min) / 3);
                canvas.drawText(String.valueOf(max), (float) getPaddingStart(), this.mStavePositions[0] + (this.mXAxisLabelHeight / 2.0f), this.mXAxisLabelPaint);
                canvas.drawText(String.valueOf(stave2), (float) getPaddingStart(), this.mStavePositions[1] + (this.mXAxisLabelHeight / 2.0f), this.mXAxisLabelPaint);
                canvas.drawText(String.valueOf(stave1), (float) getPaddingStart(), this.mStavePositions[2] + (this.mXAxisLabelHeight / 2.0f), this.mXAxisLabelPaint);
                canvas.drawText(String.valueOf(min), (float) getPaddingStart(), this.mStavePositions[3] + (this.mXAxisLabelHeight / 2.0f), this.mXAxisLabelPaint);
                offset = this.mStaveLabelWidth;
                rightEdge = ((((float) getPaddingStart()) + offset) + this.mLineWidth) - ((float) (getPaddingEnd() - getPaddingStart()));
            }
            canvas.drawLine(((float) getPaddingStart()) + offset, this.mStavePositions[0], rightEdge, this.mStavePositions[0], this.mXAxisPaint);
            canvas.drawLine(((float) getPaddingStart()) + offset, this.mStavePositions[1], rightEdge, this.mStavePositions[1], this.mXAxisPaint);
            canvas.drawLine(((float) getPaddingStart()) + offset, this.mStavePositions[2], rightEdge, this.mStavePositions[2], this.mXAxisPaint);
            canvas.drawLine(((float) getPaddingStart()) + offset, this.mStavePositions[3], rightEdge, this.mStavePositions[3], this.mXAxisPaint);
        }
    }

    public void setVMinOffset(float offset) {
        this.mVMinOffset = offset;
    }

    public void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel) {
        super.setData(xLabels, xRange, dontPaintLastXLabel);
        resetParams();
    }

    public void setData(String[] xLabels, String[] yLabels, float xRange, boolean dontPaintLastXLabel) {
        super.setData(xLabels, yLabels, xRange, dontPaintLastXLabel);
        resetParams();
    }

    private void resetParams() {
        this.mParamsInitialized = false;
        this.mAnimate = false;
        invalidate();
    }

    private synchronized void calculateDrawingParams() {
        if (!this.mParamsInitialized) {
            float height;
            float left = (float) getPaddingLeft();
            float top = (float) getPaddingTop();
            float width = ((float) (getWidth() - getPaddingRight())) - left;
            if (this.mShowEveryPoint) {
                height = ((((float) (getHeight() - getPaddingBottom())) - top) - (this.mEveryPointValueHeight * 2.0f)) - this.mDateLineHeight;
            } else {
                height = ((float) (getHeight() - getPaddingBottom())) - top;
            }
            calculateXAxisParams(left, width, top, height);
            this.mStavePositions[0] = this.mVMinOffset;
            this.mStavePositions[3] = height - this.mVMinOffset;
            this.mStavePositions[1] = this.mVMinOffset + ((height - (2.0f * this.mVMinOffset)) / 3.0f);
            this.mStavePositions[2] = this.mVMinOffset + ((2.0f * (height - (2.0f * this.mVMinOffset))) / 3.0f);
            if (this.mValues.size() > 0) {
                int i;
                Collections.sort(this.mValues);
                this.mMinOrigin = ((ValueSet) this.mValues.get(0)).minOrigin();
                this.mMaxOrigin = ((ValueSet) this.mValues.get(0)).maxOrigin();
                for (ValueSet set : this.mValues) {
                    T min = set.minOrigin();
                    T max = set.maxOrigin();
                    if (this.mMinOrigin.doubleValue() > min.doubleValue()) {
                        this.mMinOrigin = min;
                    }
                    if (this.mMaxOrigin.doubleValue() < max.doubleValue()) {
                        this.mMaxOrigin = max;
                    }
                }
                SplineInterpolator interpolator = new SplineInterpolator();
                for (ValueSet set2 : this.mValues) {
                    float width2 = (!this.mDrawStave || this.mMinOrigin.doubleValue() >= this.mMaxOrigin.doubleValue() || this.mMaxOrigin.doubleValue() - this.mMinOrigin.doubleValue() <= 2.0d) ? this.mDrawStave ? ((((float) ((getWidth() - getPaddingStart()) - getPaddingEnd())) - this.mLineWidth) / 2.0f) + this.mNoyAxisPaddingLeft : 0.0f : this.mStaveLabelWidth + this.mPointPaddingLeft;
                    width2 += left;
                    float f = (!this.mDrawStave || this.mMinOrigin.doubleValue() >= this.mMaxOrigin.doubleValue()) ? 0.0f : this.mStaveLabelWidth + this.mPointPaddingLeft;
                    set2.calcDotsOnScreen(width2, width - f, top, this.mYMaxHeight, this.mVMinOffset, this.mMinOrigin, this.mMaxOrigin, interpolator);
                }
                int count = 0;
                for (i = 0; i < this.mValues.size(); i++) {
                    count += ((ValueSet) this.mValues.get(i)).dots.size();
                }
                if (count <= 1) {
                    this.mIsShowAverageLine = false;
                }
                this.mMinCoordinate = Float.MAX_VALUE;
                this.mMaxCoordinate = -3.4028235E38f;
                for (ValueSet set22 : this.mValues) {
                    float fmin = set22.minCoordinate();
                    float fmax = set22.maxCoordinate();
                    if (this.mMinCoordinate > fmin) {
                        this.mMinCoordinate = fmin;
                    }
                    if (this.mMaxCoordinate < fmax) {
                        this.mMaxCoordinate = fmax;
                    }
                }
                for (ValueSet set222 : this.mValues) {
                    set222.optimizeDots(top, this.mYMaxHeight, this.mVMinOffset);
                }
                calculateYAxis(left, (double) this.mMaxCoordinate, (double) this.mMinCoordinate);
                calculateBenchmark((float) getWidth(), this.mYMaxHeight, top, this.mVMinOffset, this.mMinOrigin, this.mMaxOrigin, this.mBenchmarkValue);
                calculateBenchmark();
                calculateAveLine();
                this.mMinMaxFound = false;
                for (ValueSet set2222 : this.mValues) {
                    set2222.calculatePartsOnScreen(this.mMinCoordinate, this.mMaxCoordinate, this.mBenchmarkCoordinate);
                }
                this.mValueSetJointPaths.clear();
                this.mValueSetJointClosedPaths.clear();
                ValueSet last = (ValueSet) this.mValues.get(0);
                for (i = 1; i < this.mValues.size(); i++) {
                    ValueSet current = (ValueSet) this.mValues.get(i);
                    this.mValueSetJointPaths.add(createPath(last.last(), current.first()));
                    this.mValueSetJointClosedPaths.add(createPath(last.last(), current.first()));
                    last = current;
                }
            } else {
                calculateBenchmark((float) getWidth(), this.mYMaxHeight, top, this.mVMinOffset, this.mMinOrigin, this.mMaxOrigin, this.mBenchmarkValue);
                this.mNoDataLocation = new PointF((((float) getWidth()) - this.mValueMinMaxPaint.measureText(this.mNoDataHint)) / 2.0f, (((this.mDrawStave ? this.mXAxisLabelHeight + this.mXAxisLabelOffset : 0.0f) + (height - this.mVMinMaxHeight)) / 2.0f) + top);
            }
            if (this.mAnimate) {
                this.x = 0.0f;
                this.y = 0.0f;
                animateY();
            } else {
                this.x = this.mMaxXSize;
                this.y = 1.0f;
            }
            this.mParamsInitialized = true;
        }
    }

    private void calculateBenchmark() {
        if (!this.mIsShowBenchZone || this.mMinBenchmarkValue == null || this.mMaxBenchmarkValue == null || this.mMinOrigin == null || this.mMaxOrigin == null || this.mAveValue == null) {
            Log.i("LineChart", "calculateBenchmark, mMinBenchmarkValue: " + this.mMinBenchmarkValue + ", mMaxBenchmarkValue: " + this.mMaxBenchmarkValue + ", mMinOrigin: " + this.mMinOrigin + ", mMaxOrigin: " + this.mMaxOrigin);
            return;
        }
        int minHeart = this.mMinOrigin.intValue();
        int maxHeart = this.mMaxOrigin.intValue();
        int minBenchValue = this.mMinBenchmarkValue.intValue();
        int maxBenchValue = this.mMaxBenchmarkValue.intValue();
        float radio = 0.0f;
        if (!(minHeart == 0 || maxHeart == 0)) {
            radio = minHeart == maxHeart ? this.mMaxCoordinate / ((float) minHeart) : (this.mMaxCoordinate - this.mMinCoordinate) / ((float) (maxHeart - minHeart));
        }
        this.mMinBenchmarkCoordinate = this.mMaxCoordinate + (((float) (minHeart - minBenchValue)) * radio);
        this.mMaxBenchmarkCoordinate = this.mMaxCoordinate + (((float) (minHeart - maxBenchValue)) * radio);
    }

    private void calculateAveLine() {
        if (!this.mIsShowAverageLine || this.mMinOrigin == null || this.mMaxOrigin == null || this.mAveValue == null) {
            Log.i("LineChart", "calculateBenchmark, mMinBenchmarkValue: " + this.mMinBenchmarkValue + ", mMaxBenchmarkValue: " + this.mMaxBenchmarkValue + ", mMinOrigin: " + this.mMinOrigin + ", mMaxOrigin: " + this.mMaxOrigin);
            return;
        }
        int aveValue = this.mAveValue.intValue();
        int minHeart = this.mMinOrigin.intValue();
        int maxHeart = this.mMaxOrigin.intValue();
        float radio = 0.0f;
        if (!(minHeart == 0 || maxHeart == 0)) {
            radio = minHeart == maxHeart ? this.mMaxCoordinate / ((float) minHeart) : (this.mMaxCoordinate - this.mMinCoordinate) / ((float) (maxHeart - minHeart));
        }
        this.mAveCoordinate = this.mMaxCoordinate + (((float) (minHeart - aveValue)) * radio);
    }

    private void calculateBenchmark(float wholeWidth, float height, float top, float offset, T min, T max, T benchmarkValue) {
        this.mPaintBenchmark = this.mBenchmarkEnabled;
        if (this.mBenchmarkEnabled) {
            if (min == null || max == null) {
                this.mPaintBenchmark = false;
            } else if (max.doubleValue() <= min.doubleValue()) {
                if (max.doubleValue() > benchmarkValue.doubleValue()) {
                    this.mBenchmarkCoordinate = top + height;
                } else if (max.doubleValue() < benchmarkValue.doubleValue()) {
                    this.mPaintBenchmark = false;
                } else {
                    this.mBenchmarkCoordinate = (height / 2.0f) + top;
                }
            } else if (max.doubleValue() >= benchmarkValue.doubleValue()) {
                this.mBenchmarkCoordinate = ((top + height) - offset) - (((benchmarkValue.floatValue() - min.floatValue()) * (height - (offset * 2.0f))) / (max.floatValue() - min.floatValue()));
            } else {
                this.mPaintBenchmark = false;
            }
        }
        if (this.mBenchmarkEnabled && this.mPaintBenchmark) {
            this.mBenchmarkPath = new Path();
            this.mBenchmarkPath.moveTo(0.0f, this.mBenchmarkCoordinate);
            this.mBenchmarkPath.lineTo(wholeWidth, this.mBenchmarkCoordinate);
        }
    }

    @TargetApi(11)
    public void animateY() {
        this.x = this.mMaxXSize;
        ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}).setDuration(1500);
        anim.addUpdateListener(new C04742());
        anim.start();
    }

    private Shader createBenchGradient() {
        if (this.mBenchGradient == null) {
            if (this.mDrawStave) {
                this.mBenchGradient = new LinearGradient(0.0f, this.mMaxCoordinate, 0.0f, this.mMinCoordinate, this.mCurveColors == null ? new int[]{this.mGreenColor, this.mYellowColor, this.mRedColor} : this.mCurveColors, this.mCurveColors == null ? HEART_BENCH_SECTION : this.mCurveRanges, TileMode.CLAMP);
            } else {
                this.mBenchGradient = new LinearGradient(0.0f, this.mMaxBenchmarkCoordinate - Math.abs((this.mMaxBenchmarkCoordinate - this.mMinBenchmarkCoordinate) * 0.14f), 0.0f, this.mMinBenchmarkCoordinate + Math.abs((this.mMaxBenchmarkCoordinate - this.mMinBenchmarkCoordinate) * 0.14f), this.mCurveColors == null ? new int[]{this.mGreenColor, this.mYellowColor, this.mRedColor, this.mRedColor, this.mYellowColor, this.mGreenColor} : this.mCurveColors, this.mCurveColors == null ? BENCH_SECTION : this.mCurveRanges, TileMode.CLAMP);
            }
        }
        return this.mBenchGradient;
    }

    public void onDraw(Canvas canvas) {
        calculateDrawingParams();
        super.onDraw(canvas);
        if (this.mChartType == 0 || this.mChartType == 1) {
            drawStave(canvas);
            drawAveragemark(canvas);
            drawBenchmark(canvas);
        }
        if (this.mCoverColors != null && this.mIsShownCoverColor) {
            this.mClosePathPaint.setShader(new LinearGradient(0.0f, this.mYMaxHeight, 0.0f, this.mMinCoordinate, this.mCoverColors, this.mCoverRanges, TileMode.CLAMP));
        } else if (this.mChartType == 2) {
            float f = 0.0f;
            this.mClosePathPaint.setShader(new LinearGradient(0.0f, this.mYMaxHeight, f, this.mMinCoordinate, new int[]{this.mAltitudeStartColor, this.mAltitudeEndColor}, new float[]{0.0f, 1.0f}, TileMode.CLAMP));
        }
        drawYValues(canvas);
        if (this.mIsDataLoadFinished.get()) {
            Log.i("LineChart", "linchart onDraw:" + this.mIsDataLoadFinished + ", finished:" + this.mIsDataLoadFinished.get());
            if (this.mLoadingListener != null) {
                this.mLoadingListener.onLoadingFinished();
            }
            this.mIsDataLoadFinished.set(false);
        }
    }

    private void drawBenchmark(Canvas canvas) {
        if (this.mBenchmarkEnabled && this.mPaintBenchmark) {
            canvas.drawPath(this.mBenchmarkPath, this.mBenchmarkPaint);
        }
    }

    private void drawAveragemark(Canvas canvas) {
        if (this.mMinOrigin == null || this.mMaxOrigin == null || !this.mIsShowAverageLine || this.mAveValue == null) {
            Log.i("LineChart", "mIsShowBenchZone: " + this.mIsShowBenchZone + ", mMinOrigin: " + this.mMinOrigin + ", mMaxOrigin: " + this.mMaxOrigin + ", mIsShowAverageLine: " + this.mIsShowAverageLine + ", mAveValue: " + this.mAveValue);
            return;
        }
        Path path = new Path();
        int averageHeart = this.mAveValue.intValue();
        StringBuffer str = new StringBuffer().append(this.mAverageText).append(averageHeart);
        path.moveTo(0.0f, this.mAveCoordinate);
        path.lineTo((float) getWidth(), this.mAveCoordinate);
        canvas.drawPath(path, this.mAveragePaint);
        canvas.drawText(str.toString(), this.mAverageTextPaddingLeft + (averageHeart > 99 ? this.mAverageHundredTextPadding : this.mAverageTextPaddingLeft), this.mAveCoordinate - (this.mVMinMaxHeight / 4.0f), this.mAverageTextPaint);
    }

    private void drawYValues(Canvas canvas) {
        if (this.mValues.size() != 0) {
            if (this.mChartType != 2) {
                this.mValueSetLineLowerPaint.setShader(createBenchGradient());
                this.mValueSetJointPaint.setShader(createBenchGradient());
            }
            this.mMaxPainted = false;
            this.mMinPainted = false;
            if (this.mChartType == 2) {
                for (ValueSet set : this.mValues) {
                    set.draw(canvas);
                }
                for (Path path : this.mValueSetJointClosedPaths) {
                    canvas.drawPath(path, this.mClosePathPaint);
                }
                return;
            }
            for (ValueSet set2 : this.mValues) {
                set2.draw(canvas);
            }
            for (Path path2 : this.mValueSetJointPaths) {
                Paint paint = (this.mBenchmarkEnabled && this.mPaintBenchmark) ? this.mBenchmarkPaint : this.mValueSetJointPaint;
                canvas.drawPath(path2, paint);
            }
        } else if (this.mDataProvided) {
            canvas.drawText(this.mNoDataHint, this.mNoDataLocation.x, this.mNoDataLocation.y, this.mNoDataPaint);
        }
    }

    private Path createPath(float startx, float starty, float endx, float endy) {
        Path path = new Path();
        path.moveTo(startx, starty);
        path.lineTo(endx, endy);
        return path;
    }

    private Path createPath(PointF start, PointF end) {
        return createPath(start.x, start.y, end.x, end.y);
    }

    private Path createClosedPath(float startx, float starty, float endx, float endy) {
        Path path = new Path();
        path.moveTo(startx, starty);
        path.lineTo(endx, endy);
        if (endy < starty) {
            path.lineTo(endx, starty);
        } else {
            path.lineTo(startx, endy);
        }
        path.close();
        return path;
    }

    private Path createClosedPath(PointF start, PointF end) {
        return createClosedPath(start.x, start.y, end.x, end.y);
    }
}
