package com.huami.watch.common.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.SweepGradient;
import android.view.animation.LinearInterpolator;
import com.huami.watch.common.widget.HmAnimatedOverlayImageView.HmAbstractOverlayAnimatorDrawable;

public class HmOverlayLoadingDrawble extends HmAbstractOverlayAnimatorDrawable {
    private float coordinateX;
    private float coordinateY;
    private Matrix f14m;
    private Paint mPaint = new Paint(1);
    private float mRadius;
    private SweepGradient mShader;
    private ValueAnimator valueAnimator;

    class C04511 implements AnimatorUpdateListener {
        C04511() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmOverlayLoadingDrawble.this.f14m.setRotate(360.0f * ((Float) animation.getAnimatedValue()).floatValue(), HmOverlayLoadingDrawble.this.coordinateX, HmOverlayLoadingDrawble.this.coordinateY);
            HmOverlayLoadingDrawble.this.mShader.setLocalMatrix(HmOverlayLoadingDrawble.this.f14m);
            HmOverlayLoadingDrawble.this.mPaint.setShader(HmOverlayLoadingDrawble.this.mShader);
            HmOverlayLoadingDrawble.this.invalidateSelf();
        }
    }

    public HmOverlayLoadingDrawble() {
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth(2.0f);
        this.valueAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.valueAnimator.setInterpolator(new LinearInterpolator());
        this.valueAnimator.setRepeatCount(-1);
        this.valueAnimator.setDuration(1000);
        this.valueAnimator.addUpdateListener(new C04511());
    }

    protected float getContentRatio() {
        return 0.8f;
    }

    public void draw(Canvas canvas) {
        if (this.valueAnimator.isRunning()) {
            canvas.drawCircle(this.coordinateX, this.coordinateY, this.mRadius, this.mPaint);
        }
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public int getOpacity() {
        return 0;
    }

    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        this.mRadius = (float) ((right - left) / 2);
        this.coordinateX = ((float) left) + this.mRadius;
        this.coordinateY = ((float) top) + this.mRadius;
        this.f14m = new Matrix();
        this.mShader = new SweepGradient(this.coordinateX, this.coordinateY, new int[]{16777215, 16777215, -1}, null);
        this.mPaint.setShader(this.mShader);
    }

    public ValueAnimator getOverlayAnmiator() {
        return this.valueAnimator;
    }
}
