package com.huami.watch.common.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberTwoSelector.SelectView;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.ui.C1070R;

public class NumberThreeSelector extends LinearLayout implements OnValueChangeListener, KeyeventConsumer {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private KeyeventProcessor keyeventProcessor;
    private NumberView mAmPm;
    private int mAmPmIndex;
    private String[] mAmorPm;
    private String[] mAmorPmSub;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;
    private String[] mHoursSub;
    private String[] mMintues;
    private String[] mMintuesSub;
    private NumberView mMinute;
    private int mMinuteIndex;
    private NumberPickerView parentPickerView;

    class C04821 implements EventCallBack {
        C04821() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP) {
                if (NumberThreeSelector.this.parentPickerView != null && NumberThreeSelector.this.parentPickerView.getCurrentIndex() == NumberThreeSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberThreeSelector.this.currentView) {
                    if (NumberThreeSelector.this.mHour != null) {
                        NumberThreeSelector.this.mHour.smoothScrollBy(-1);
                    }
                } else if (SelectView.MINUTES == NumberThreeSelector.this.currentView) {
                    if (NumberThreeSelector.this.mMinute != null) {
                        NumberThreeSelector.this.mMinute.smoothScrollBy(-1);
                    }
                } else if (SelectView.AM_OR_PM == NumberThreeSelector.this.currentView && NumberThreeSelector.this.mAmPm != null) {
                    NumberThreeSelector.this.mAmPm.smoothScrollBy(-1);
                }
                return true;
            } else if (theKey != HMKeyEvent.KEY_DOWN) {
                return theKey == HMKeyEvent.KEY_CENTER ? false : false;
            } else {
                if (NumberThreeSelector.this.parentPickerView != null && NumberThreeSelector.this.parentPickerView.getCurrentIndex() == NumberThreeSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberThreeSelector.this.currentView) {
                    if (NumberThreeSelector.this.mHour != null) {
                        NumberThreeSelector.this.mHour.smoothScrollBy(1);
                    }
                } else if (SelectView.MINUTES == NumberThreeSelector.this.currentView) {
                    if (NumberThreeSelector.this.mHour != null) {
                        NumberThreeSelector.this.mMinute.smoothScrollBy(1);
                    }
                } else if (SelectView.AM_OR_PM == NumberThreeSelector.this.currentView && NumberThreeSelector.this.mAmPm != null) {
                    NumberThreeSelector.this.mAmPm.smoothScrollBy(1);
                }
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    class C04832 implements OnTouchListener {
        C04832() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C1070R.id.picker1) {
                NumberThreeSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C1070R.id.picker2) {
                NumberThreeSelector.this.setCurrentView(SelectView.MINUTES);
            } else if (i == C1070R.id.picker_ampm) {
                NumberThreeSelector.this.setCurrentView(SelectView.AM_OR_PM);
            }
            return false;
        }
    }

    public NumberThreeSelector(@NonNull Context context) {
        this(context, null);
    }

    public NumberThreeSelector(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberThreeSelector(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.mMinuteIndex = 0;
        this.mAmPmIndex = 0;
        this.currentView = SelectView.AM_OR_PM;
        this.canAccept = true;
        this.clickListener = new C04832();
        initContent();
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04821());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public void setParentPickerView(NumberPickerView parentPickerView) {
        this.parentPickerView = parentPickerView;
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public void setData(String[] column1, String[] column2, String[] column3) {
        setData(column1, column2, column3, null, null, null);
    }

    public void setData(String[] column1, String[] column2, String[] column3, String[] column1Sub, String[] column2Sub, String[] column3Sub) {
        this.mAmorPm = column1;
        this.mHours = column2;
        this.mMintues = column3;
        this.mAmorPmSub = column1Sub;
        this.mHoursSub = column2Sub;
        this.mMintuesSub = column3Sub;
        inflateContent();
    }

    public SelectView getCurrentView() {
        return this.currentView;
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
            this.mMinute.setHasFocused(false);
            this.mAmPm.setHasFocused(false);
        } else if (currentView == SelectView.MINUTES) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(true);
            this.mAmPm.setHasFocused(false);
        } else if (currentView == SelectView.AM_OR_PM) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(false);
            this.mAmPm.setHasFocused(true);
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    private void initContent() {
        this.mAmorPm = new String[0];
        this.mHours = new String[0];
        this.mMintues = new String[0];
    }

    public void setIndex(int oneIndex, int hourIndex, int minuteIndex) {
        this.mAmPmIndex = oneIndex;
        this.mHoureIndex = hourIndex;
        this.mMinuteIndex = minuteIndex;
        if (this.mAmPm != null) {
            this.mAmPm.setPickedIndexRelativeToMin(this.mAmPmIndex);
        }
        if (this.mHour != null) {
            this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        }
        if (this.mMintues != null) {
            this.mMinute.setPickedIndexRelativeToMin(this.mMinuteIndex);
        }
    }

    private void inflateContent() {
        setCurrentView(SelectView.AM_OR_PM);
        this.mHour.setDisplayedValues(this.mHours, this.mHoursSub);
        this.mHour.setMaxValue(this.mHours.length - 1);
        this.mHour.setMinValue(0);
        this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        this.mMinute.setDisplayedValues(this.mMintues, this.mMintuesSub);
        this.mMinute.setMaxValue(this.mMintues.length - 1);
        this.mMinute.setMinValue(0);
        this.mMinute.setPickedIndexRelativeToMin(this.mMinuteIndex);
        this.mAmPm.setDisplayedValues(this.mAmorPm, this.mAmorPmSub);
        this.mAmPm.setMaxValue(this.mAmorPm.length - 1);
        this.mAmPm.setMinValue(0);
        this.mHour.setOnTouchListener(this.clickListener);
        this.mMinute.setOnTouchListener(this.clickListener);
        this.mAmPm.setOnTouchListener(this.clickListener);
        if (this.mHoursSub != null && this.mHoursSub.length > 0) {
            this.mHour.isSubtitle(true);
        }
        if (this.mMintuesSub != null && this.mMintuesSub.length > 0) {
            this.mMinute.isSubtitle(true);
        }
        if (this.mAmorPmSub != null && this.mAmorPmSub.length > 0) {
            this.mAmPm.isSubtitle(true);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mAmPm = (NumberView) findViewById(C1070R.id.picker_ampm);
        this.mHour = (NumberView) findViewById(C1070R.id.picker1);
        this.mMinute = (NumberView) findViewById(C1070R.id.picker2);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }

    public int[] getSelectIndex() {
        return new int[]{this.mAmPm.getValue(), this.mHour.getValue(), this.mMinute.getValue()};
    }

    public void setWidthColumn1(int widthColumn1) {
        if (this.mAmPm != null) {
            this.mAmPm.setWidth(widthColumn1);
        }
    }

    public void setWidthColumn2(int widthColumn2) {
        if (this.mHour != null) {
            this.mHour.setWidth(widthColumn2);
        }
    }

    public void setWidthColumn3(int widthColumn3) {
        if (this.mMinute != null) {
            this.mMinute.setWidth(widthColumn3);
        }
    }

    public boolean changeFocused() {
        if (getCurrentView() == SelectView.HOUR) {
            if (this.mMintues != null && this.mMinute.getVisibility() == 0) {
                setCurrentView(SelectView.MINUTES);
                return true;
            }
        } else if (getCurrentView() == SelectView.AM_OR_PM && this.mHour != null && this.mHour.getVisibility() == 0) {
            setCurrentView(SelectView.HOUR);
            return true;
        }
        return false;
    }

    public void setVisibilityColumn1(int visibility) {
        if (this.mAmPm != null) {
            this.mAmPm.setVisibility(visibility);
            if (visibility != 0 && getCurrentView() == SelectView.AM_OR_PM) {
                if (this.mHour != null && this.mHour.getVisibility() == 0) {
                    setCurrentView(SelectView.HOUR);
                } else if (this.mMinute != null && this.mMinute.getVisibility() == 0) {
                    setCurrentView(SelectView.MINUTES);
                }
            }
        }
    }

    public void setVisibilityColumn2(int visibility) {
        if (this.mHour != null) {
            this.mHour.setVisibility(visibility);
            if (visibility != 0 && getCurrentView() == SelectView.HOUR) {
                if (this.mAmPm != null && this.mAmPm.getVisibility() == 0) {
                    setCurrentView(SelectView.AM_OR_PM);
                } else if (this.mMinute != null && this.mMinute.getVisibility() == 0) {
                    setCurrentView(SelectView.MINUTES);
                }
            }
        }
    }

    public void setVisibilityColumn3(int visibility) {
        if (this.mMinute != null) {
            this.mMinute.setVisibility(visibility);
            if (visibility != 0 && getCurrentView() == SelectView.MINUTES) {
                if (this.mAmPm != null && this.mAmPm.getVisibility() == 0) {
                    setCurrentView(SelectView.AM_OR_PM);
                } else if (this.mHour != null && this.mHour.getVisibility() == 0) {
                    setCurrentView(SelectView.HOUR);
                }
            }
        }
    }

    public void set2OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mHour != null) {
            this.mHour.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public void set3OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mMinute != null) {
            this.mMinute.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public void set1OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mAmPm != null) {
            this.mAmPm.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public void setSelectTextSizeColumn1(int size) {
        if (this.mAmPm != null) {
            this.mAmPm.setSelectTextSize(size);
        }
    }

    public void setSelectTextSizeColumn2(int size) {
        if (this.mHour != null) {
            this.mHour.setSelectTextSize(size);
        }
    }

    public void setSelectTextSizeColumn3(int size) {
        if (this.mMinute != null) {
            this.mMinute.setSelectTextSize(size);
        }
    }

    public NumberView getHourView() {
        return this.mHour;
    }

    public NumberView getMinuteView() {
        return this.mMinute;
    }

    public NumberView getAmPmView() {
        return this.mAmPm;
    }
}
