package com.huami.watch.common.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;
import java.util.ArrayList;

public class HmTendencyCharView extends View {
    private double mCalorieBurningHeartRate;
    private Path mCalorieBurningHeartRateLine;
    private Paint mCalorieBurningHeartRatePaint;
    private int mCalorieBurningHeartrateColor;
    private Paint mCalorieBurningLinePaint;
    private double mCoordinateLeftMargin;
    private double mCoordinateRightMargin;
    private double mHeartRateBottomPadding;
    private Paint mHeartRatePaint;
    private Paint mHeartRatePointPaint;
    private double mHeartRateTopPadding;
    private double[] mHeartRateX;
    private double[] mHeartRateY;
    private int mHeight;
    private double mMaxHeartRate;
    private double mMaxHeartRateTime;
    private double mMaxTime;
    private double mMinHeartRate;
    private double mMinHeartRateTime;
    private Paint mMinMaxTextPaint;
    private float mMinMaxTextSize;
    private double mMinTime;
    private int mNormalHeartrateColor;
    private boolean mShowYAxis;
    private int mWidth;
    private int[] mXAxis;
    private Paint mXAxisLinePaint;
    private float mXAxisMargin;
    private ArrayList<Double> mXInterpolatorValue;
    private double mXRate;
    private Paint mXTextPaint;
    private float mXTextSize;
    private String mXUnit;
    private Paint mXUnitPaint;
    private Paint mYAxisLinePaint;
    private float mYAxisMargin;
    private ArrayList<Double> mYInterpolatorValue;
    private double mYRate;

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mWidth = w;
        this.mHeight = h;
    }

    private float parseXAxisToScreenPoint(double x) {
        return (float) (((this.mXRate * (x - this.mMinTime)) + ((double) this.mYAxisMargin)) + this.mCoordinateLeftMargin);
    }

    private float parseHeartRateToScreenPoint(double y) {
        if (this.mYRate == 0.0d) {
            return (((float) this.mHeight) - this.mXAxisMargin) / 2.0f;
        }
        return (float) (((((double) this.mHeight) - this.mHeartRateBottomPadding) - ((double) this.mXAxisMargin)) - (this.mYRate * (y - this.mMinHeartRate)));
    }

    protected void onDraw(Canvas canvas) {
        if (this.mMaxTime > this.mMinTime) {
            this.mXRate = ((((double) (((float) this.mWidth) - this.mYAxisMargin)) - this.mCoordinateLeftMargin) - this.mCoordinateRightMargin) / (this.mMaxTime - this.mMinTime);
        }
        if (this.mMaxHeartRate > this.mMinHeartRate) {
            this.mYRate = (((((double) this.mHeight) - this.mHeartRateTopPadding) - this.mHeartRateBottomPadding) - ((double) this.mXAxisMargin)) / (this.mMaxHeartRate - this.mMinHeartRate);
        }
        float xAxisY = ((float) this.mHeight) - this.mXAxisMargin;
        canvas.drawLine(parseXAxisToScreenPoint(0.0d), xAxisY, (float) (((double) this.mWidth) - this.mCoordinateRightMargin), xAxisY, this.mXAxisLinePaint);
        if (this.mShowYAxis) {
            canvas.drawLine(this.mYAxisMargin, xAxisY, this.mYAxisMargin, 0.0f, this.mYAxisLinePaint);
        }
        if (this.mXAxis != null) {
            for (int x : this.mXAxis) {
                float startX = parseXAxisToScreenPoint((double) x);
                float startY = ((float) this.mHeight) - this.mXAxisMargin;
                canvas.drawLine(startX, startY, startX, 0.0f, this.mXUnitPaint);
                canvas.drawText("" + x + this.mXUnit, startX, this.mXTextSize + startY, this.mXTextPaint);
            }
        }
        if (this.mHeartRateX != null && this.mHeartRateX.length > 0) {
            float heartRateLineY = parseHeartRateToScreenPoint(this.mCalorieBurningHeartRate);
            this.mCalorieBurningHeartRateLine.reset();
            this.mCalorieBurningHeartRateLine.moveTo(0.0f, heartRateLineY);
            this.mCalorieBurningHeartRateLine.lineTo((float) this.mWidth, heartRateLineY);
            canvas.drawPath(this.mCalorieBurningHeartRateLine, this.mCalorieBurningLinePaint);
            if (this.mHeartRateX.length == 1) {
                int color;
                if (this.mHeartRateY[0] > this.mCalorieBurningHeartRate) {
                    color = this.mCalorieBurningHeartrateColor;
                } else {
                    color = this.mNormalHeartrateColor;
                }
                this.mHeartRatePointPaint.setColor(color);
                canvas.drawCircle(parseXAxisToScreenPoint(this.mHeartRateX[0]), parseHeartRateToScreenPoint(this.mHeartRateY[0]), 3.0f, this.mHeartRatePointPaint);
            } else {
                for (int i = 1; i < this.mXInterpolatorValue.size(); i++) {
                    double max;
                    double startHeartRate = ((Double) this.mYInterpolatorValue.get(i - 1)).doubleValue();
                    double endHeartRate = ((Double) this.mYInterpolatorValue.get(i)).doubleValue();
                    startX = parseXAxisToScreenPoint(((Double) this.mXInterpolatorValue.get(i - 1)).doubleValue());
                    startY = parseHeartRateToScreenPoint(startHeartRate);
                    float endX = parseXAxisToScreenPoint(((Double) this.mXInterpolatorValue.get(i)).doubleValue());
                    float endY = parseHeartRateToScreenPoint(endHeartRate);
                    double min;
                    if (startHeartRate > endHeartRate) {
                        max = startHeartRate;
                        min = endHeartRate;
                    } else {
                        max = endHeartRate;
                        min = startHeartRate;
                    }
                    if (max <= this.mCalorieBurningHeartRate || max - this.mCalorieBurningHeartRate <= this.mCalorieBurningHeartRate - min) {
                        canvas.drawLine(startX, startY, endX, endY, this.mHeartRatePaint);
                    } else {
                        canvas.drawLine(startX, startY, endX, endY, this.mCalorieBurningHeartRatePaint);
                    }
                }
            }
            canvas.drawText("" + Math.round(this.mMinHeartRate), parseXAxisToScreenPoint(this.mMinHeartRateTime), parseHeartRateToScreenPoint(this.mMinHeartRate) + this.mMinMaxTextSize, this.mMinMaxTextPaint);
            canvas.drawText("" + Math.round(this.mMaxHeartRate), parseXAxisToScreenPoint(this.mMaxHeartRateTime), parseHeartRateToScreenPoint(this.mMaxHeartRate), this.mMinMaxTextPaint);
        }
    }
}
