package com.huami.watch.common.widget;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class HmClassifyBarChart extends XAxisChart {
    private int LOCATION;
    private float gap;
    private float mBarWidth;
    protected boolean mDrawGrid;
    private boolean mDrawXAxisLine;
    private int mGridEndColor;
    private PointF[] mGridLocations;
    private int mGridStartColor;
    private boolean mIsShowShadowChart;
    private boolean mIsShowYValue;
    private float mLineOffset;
    protected boolean mParamsInitialized;
    private Paint mShadowPaint;
    private RectF[] mShadowRectf;
    private List<List<RectF>> mTotalYBarRects;
    private List<List<ChartInfo>> mValues;
    private float mXAxisOffset;
    private Paint mYvaluesPaint;
    float max;

    private void calculateDrawingParams() {
        if (!this.mParamsInitialized) {
            float left = (float) getPaddingLeft();
            float top = (float) getPaddingTop();
            float width = ((float) (getWidth() - getPaddingRight())) - left;
            float height = ((float) (getHeight() - getPaddingBottom())) - top;
            calculateXAxisParams(left, width, top, height);
            calculateYAxis(left, (double) this.mYMaxHeight, (double) top);
            calculateBarChartParams(left, width, top, height);
            this.mParamsInitialized = true;
        }
    }

    protected void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel) {
        super.setData(xLabels, xRange, dontPaintLastXLabel);
    }

    private void calculateBarChartParams(float left, float width, float top, float height) {
        int i;
        this.mTotalYBarRects.clear();
        if (this.mIsShowShadowChart) {
            this.mShadowRectf = new RectF[((int) this.mMaxXSize)];
            for (i = 0; i < this.mShadowRectf.length; i++) {
                this.mShadowRectf[i] = new RectF();
                this.mShadowRectf[i].set((((float) i) * (this.gap + this.mBarWidth)) + left, top, ((((float) i) * (this.gap + this.mBarWidth)) + left) + this.mBarWidth, this.mYMaxHeight);
            }
        }
        Log.d("testdata", this.mMaxXSize + "--------");
        Log.d("testdata", "正在计算数据柱形图rectf");
        int dataIndex = 0;
        i = 0;
        while (((float) i) < this.mMaxXSize) {
            if (this.mValues != null && this.mValues.size() > 0 && dataIndex < this.mValues.size()) {
                float startBottom = this.mYMaxHeight;
                List<ChartInfo> mSingleValues = (List) this.mValues.get(dataIndex);
                int singleSize = mSingleValues.size();
                if (singleSize > 0 && i == ((ChartInfo) mSingleValues.get(0)).dayOffset) {
                    dataIndex++;
                    List<RectF> mSinegleYBarRects = new ArrayList();
                    for (int j = 0; j < singleSize; j++) {
                        float sportChartHeight = (((float) ((ChartInfo) mSingleValues.get(j)).sportTime) / this.max) * (this.mYMaxHeight - ((float) ((singleSize - 1) * 2)));
                        if (sportChartHeight < 1.0f) {
                            sportChartHeight = 1.0f;
                        }
                        RectF sRect = new RectF();
                        if (this.mMaxXSize == 1.0f) {
                            sRect.left = ((width / 2.0f) + left) - (this.mBarWidth / 2.0f);
                            sRect.right = ((width / 2.0f) + left) + (this.mBarWidth / 2.0f);
                        } else if (this.mMaxXSize > 1.0f) {
                            sRect.left = (((float) i) * (this.gap + this.mBarWidth)) + left;
                            sRect.right = ((((float) i) * (this.gap + this.mBarWidth)) + left) + this.mBarWidth;
                        }
                        sRect.bottom = startBottom;
                        if (startBottom < sportChartHeight) {
                            sRect.top = top;
                        } else {
                            sRect.top = startBottom - sportChartHeight;
                        }
                        startBottom -= 2.0f + sportChartHeight;
                        mSinegleYBarRects.add(sRect);
                        if (sRect.top == top) {
                            break;
                        }
                    }
                    this.mTotalYBarRects.add(mSinegleYBarRects);
                }
            }
            i++;
        }
    }

    private void drawYValues(Canvas canvas) {
        if (this.mIsShowShadowChart) {
            for (RectF rec : this.mShadowRectf) {
                canvas.drawRect(rec, this.mShadowPaint);
            }
        }
        Log.d("testdata", "正在画数据柱形图");
        canvas.save(2);
        for (int i = 0; i < this.mTotalYBarRects.size(); i++) {
            for (int j = 0; j < ((List) this.mTotalYBarRects.get(i)).size(); j++) {
                RectF rect = (RectF) ((List) this.mTotalYBarRects.get(i)).get(j);
                this.mYvaluesPaint.setColor(((ChartInfo) ((List) this.mValues.get(i)).get(j)).color);
                canvas.drawRect(rect, this.mYvaluesPaint);
            }
        }
        canvas.restore();
    }

    public void onDraw(Canvas canvas) {
        calculateDrawingParams();
        drawXAxis(canvas);
        drawXLabels(canvas);
        drawGrids(canvas);
        drawYAxis(canvas);
        drawYValues(canvas);
    }

    public void setData(String[] xLabels, String[] yLabels, float xRange, boolean dontPaintLastXLabel) {
        setData(xLabels, xRange, dontPaintLastXLabel);
        if (yLabels != null && yLabels.length > 0) {
            this.mYLabels = new String[yLabels.length];
            System.arraycopy(yLabels, 0, this.mYLabels, 0, yLabels.length);
        }
    }

    private void drawGrids(Canvas canvas) {
        if (this.mDrawGrid) {
            for (PointF loc : this.mGridLocations) {
                canvas.drawLine(loc.x, loc.y, loc.x, this.mYMaxHeight + loc.y, this.mGridPaint);
                Log.d("testdata", "grid走没走");
            }
        }
    }

    protected void drawXAxis(Canvas canvas) {
        if (this.mDrawXAxisLine) {
            canvas.drawLine((float) getPaddingLeft(), this.mYMaxHeight, (float) (getWidth() - getPaddingRight()), this.mYMaxHeight, this.mXAxisPaint);
        }
    }

    protected void drawYAxis(Canvas canvas) {
        if (this.mYLabels != null && this.mYLabelLocations != null && this.mYLineLocations != null) {
            int i = 0;
            while (i < this.mYLabels.length) {
                if (this.mIsShowYValue || i != 0) {
                    canvas.drawLine(this.mYLineLocations[i].x, this.mYLineLocations[i].y, this.mYLineLocations[i].x - this.mLineOffset, this.mYLineLocations[i].y, this.mGridPaint);
                    canvas.drawText(this.mYLabels[i], this.mYLabelLocations[i].x, this.mYLabelLocations[i].y, this.mXAxisLabelPaint);
                }
                i++;
            }
        }
    }

    protected void drawXLabels(Canvas canvas) {
        if (this.mDrawXAxis) {
            int i = 0;
            while (true) {
                if (i < this.mXLabels.length - (this.mDontPaintLastXLabel ? 1 : 0)) {
                    canvas.drawText(this.mXLabels[i], this.mXLabelLocations[i].x, this.mXLabelLocations[i].y, this.mXAxisLabelPaint);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    protected void setxAxisOffset(float offset) {
        this.mXAxisOffset = offset;
    }

    protected void calculateXAxisParams(float left, float width, float top, float height) {
        int labelSize = this.mXLabels.length;
        Rect rect = new Rect();
        this.mGridLocations = new PointF[labelSize];
        this.gap = (width - (this.mBarWidth * this.mMaxXSize)) / (this.mMaxXSize - 1.0f);
        int i = 0;
        while (i < labelSize) {
            switch (this.LOCATION) {
                case 1:
                    this.mGridLocations[i] = new PointF(left + (labelSize > 1 ? (((float) i) * width) / ((float) (labelSize - 1)) : 0.0f), top);
                    this.mXAxisLabelPaint.getTextBounds(this.mXLabels[i], 0, this.mXLabels[i].length(), rect);
                    this.mXLabelLocations[i] = new PointF(i == 0 ? this.mGridLocations[i].x - (((float) Math.abs(rect.right - rect.left)) / 2.0f) : (this.mGridLocations[i].x - (((float) Math.abs(rect.right - rect.left)) / 2.0f)) - 3.0f, ((top + height) - this.mXAxisLabelOffset) + this.mXAxisOffset);
                    Log.d("location", "xlabels location LEFT: " + this.mXLabelLocations[i].x);
                    break;
                case 2:
                    float x = ((((float) i) * (this.gap + this.mBarWidth)) + left) + (this.mBarWidth / 2.0f);
                    this.mGridLocations[i] = new PointF(x, top);
                    this.mXAxisLabelPaint.getTextBounds(this.mXLabels[i], 0, this.mXLabels[i].length(), rect);
                    this.mXLabelLocations[i] = new PointF((x - (((float) Math.abs(rect.right - rect.left)) / 2.0f)) - 2.0f, ((top + height) - this.mXAxisLabelOffset) + this.mXAxisOffset);
                    Log.d("location", "xlabels location CENTER: " + this.mXLabelLocations[i].x);
                    break;
                default:
                    break;
            }
            i++;
        }
        this.mYMaxHeight = height - (this.mDrawXAxis ? this.mXAxisLabelHeight + this.mXAxisLabelOffset : 0.0f);
        this.mGridPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, this.mYMaxHeight, this.mGridStartColor, this.mGridEndColor, TileMode.CLAMP));
    }
}
