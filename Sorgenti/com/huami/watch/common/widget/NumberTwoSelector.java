package com.huami.watch.common.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.ui.C1070R;

public class NumberTwoSelector extends LinearLayout implements OnValueChangeListener, KeyeventConsumer {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private KeyeventProcessor keyeventProcessor;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;
    private String[] mHoursSub;
    private String[] mMintues;
    private String[] mMintuesSub;
    private NumberView mMinute;
    private int mMinuteIndex;
    private NumberPickerView parentPickerView;
    private int widthColumn1;
    private int widthColumn2;

    class C04841 implements EventCallBack {
        C04841() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP) {
                if (NumberTwoSelector.this.parentPickerView != null && NumberTwoSelector.this.parentPickerView.getCurrentIndex() == NumberTwoSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberTwoSelector.this.currentView) {
                    if (NumberTwoSelector.this.mHour != null) {
                        NumberTwoSelector.this.mHour.smoothScrollBy(-1);
                    }
                } else if (NumberTwoSelector.this.mMinute != null) {
                    NumberTwoSelector.this.mMinute.smoothScrollBy(-1);
                }
                return true;
            } else if (theKey != HMKeyEvent.KEY_DOWN) {
                return theKey == HMKeyEvent.KEY_CENTER ? false : false;
            } else {
                if (NumberTwoSelector.this.parentPickerView != null && NumberTwoSelector.this.parentPickerView.getCurrentIndex() == NumberTwoSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberTwoSelector.this.currentView) {
                    if (NumberTwoSelector.this.mHour != null) {
                        NumberTwoSelector.this.mHour.smoothScrollBy(1);
                    }
                } else if (NumberTwoSelector.this.mHour != null) {
                    NumberTwoSelector.this.mMinute.smoothScrollBy(1);
                }
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    class C04852 implements OnTouchListener {
        C04852() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C1070R.id.picker1) {
                NumberTwoSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C1070R.id.picker2) {
                NumberTwoSelector.this.setCurrentView(SelectView.MINUTES);
            }
            return false;
        }
    }

    public enum SelectView {
        HOUR,
        MINUTES,
        AM_OR_PM
    }

    public NumberTwoSelector(@NonNull Context context) {
        this(context, null);
    }

    public NumberTwoSelector(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberTwoSelector(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.mMinuteIndex = 0;
        this.currentView = SelectView.HOUR;
        this.canAccept = true;
        this.clickListener = new C04852();
        initContent();
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04841());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public void setParentPickerView(NumberPickerView parentPickerView) {
        this.parentPickerView = parentPickerView;
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public SelectView getCurrentView() {
        return this.currentView;
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
            this.mMinute.setHasFocused(false);
        } else if (currentView == SelectView.MINUTES) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(true);
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public void setData(String[] column1, String[] column2) {
        setData(column1, column2, null, null);
    }

    public void setData(String[] column1, String[] column2, String[] columnSub1, String[] columnSub2) {
        this.mHours = column1;
        this.mMintues = column2;
        this.mHoursSub = columnSub1;
        this.mMintuesSub = columnSub2;
        inflateContent();
    }

    private void initContent() {
        this.mHours = new String[0];
        this.mMintues = new String[0];
    }

    public void setIndex(int hourIndex, int minuteIndex) {
        this.mHoureIndex = hourIndex;
        this.mMinuteIndex = minuteIndex;
        if (this.mHour != null) {
            this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        }
        if (this.mMintues != null) {
            this.mMinute.setPickedIndexRelativeToMin(this.mMinuteIndex);
        }
    }

    private void inflateContent() {
        setCurrentView(SelectView.HOUR);
        this.mHour.setDisplayedValues(this.mHours, this.mHoursSub);
        this.mHour.setMaxValue(this.mHours.length - 1);
        this.mHour.setMinValue(0);
        this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        this.mMinute.setDisplayedValues(this.mMintues, this.mMintuesSub);
        this.mMinute.setMaxValue(this.mMintues.length - 1);
        this.mMinute.setMinValue(0);
        this.mMinute.setPickedIndexRelativeToMin(this.mMinuteIndex);
        this.mHour.setOnTouchListener(this.clickListener);
        this.mMinute.setOnTouchListener(this.clickListener);
        if (this.mHoursSub != null && this.mHoursSub.length > 0) {
            this.mHour.isSubtitle(true);
        }
        if (this.mMintuesSub != null && this.mMintuesSub.length > 0) {
            this.mMinute.isSubtitle(true);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mHour = (NumberView) findViewById(C1070R.id.picker1);
        this.mMinute = (NumberView) findViewById(C1070R.id.picker2);
        this.mHour.setOnValueChangedListener(this);
        this.mMinute.setOnValueChangedListener(this);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }

    public int[] getSelectIndex() {
        return new int[]{this.mHour.getValue(), this.mMinute.getValue()};
    }

    public void setWidthColumn1(int widthColumn1) {
        this.widthColumn1 = widthColumn1;
        if (this.mHour != null) {
            this.mHour.setWidth(widthColumn1);
        }
    }

    public void setWidthColumn2(int widthColumn2) {
        this.widthColumn2 = widthColumn2;
        if (this.mMinute != null) {
            this.mMinute.setWidth(widthColumn2);
        }
    }

    public boolean changeFocused() {
        if (getCurrentView() != SelectView.HOUR || this.mMintues == null || this.mMinute.getVisibility() != 0) {
            return false;
        }
        setCurrentView(SelectView.MINUTES);
        return true;
    }

    public void setVisibilityColumn1(int visibility) {
        if (this.mHour != null) {
            this.mHour.setVisibility(visibility);
            if (visibility != 0 && getCurrentView() == SelectView.HOUR && this.mMinute != null && this.mMinute.getVisibility() == 0) {
                setCurrentView(SelectView.MINUTES);
            }
        }
    }

    public void setVisibilityColumn2(int visibility) {
        if (this.mMinute != null) {
            this.mMinute.setVisibility(visibility);
            if (visibility != 0 && getCurrentView() == SelectView.MINUTES && this.mHour != null && this.mHour.getVisibility() == 0) {
                setCurrentView(SelectView.HOUR);
            }
        }
    }

    public void set1OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mHour != null) {
            this.mHour.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public void set2OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mMinute != null) {
            this.mMinute.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public void setSelectTextSizeColumn1(int size) {
        if (this.mHour != null) {
            this.mHour.setSelectTextSize(size);
        }
    }

    public void setSelectTextSizeColumn2(int size) {
        if (this.mMinute != null) {
            this.mMinute.setSelectTextSize(size);
        }
    }

    public NumberView getHourView() {
        return this.mHour;
    }

    public NumberView getMinuteView() {
        return this.mMinute;
    }
}
