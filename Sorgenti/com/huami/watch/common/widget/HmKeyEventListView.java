package com.huami.watch.common.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.utils.Utils;

public class HmKeyEventListView extends ListView implements EventCallBack, KeyeventConsumer {
    private boolean canAccept = true;
    private int defaultBtmHeight = 100;
    private Handler handler;
    private boolean hasBottomView = true;
    private boolean hasFocused = true;
    private boolean isModeEverestReal = ModelUtil.isRealModelEverest(null);
    private OnListKeyClickListener onListKeyClickListener;
    private Paint paint;
    private KeyeventProcessor processor;
    private Runnable runnable;
    private TextView textView;
    private boolean withSelector = true;

    public interface ItemFocusedListener {
        void hasItemFocused(boolean z);
    }

    public interface OnFocusedItemCallBack {
        void focusedItem(int i);
    }

    public interface OnListKeyClickListener {
        boolean onListKeyClick(HMKeyEvent hMKeyEvent, int i, View view);

        boolean onListKeyLongOneSecondClick(HMKeyEvent hMKeyEvent, int i, View view);

        boolean onListKeyLongThreeSecondClick(HMKeyEvent hMKeyEvent, int i, View view);
    }

    public HmKeyEventListView(Context context) {
        super(context);
        init();
    }

    public HmKeyEventListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HmKeyEventListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    private void addDefaultFooter() {
        if (this.textView == null) {
            this.textView = new TextView(getContext());
            this.textView.setHeight((int) TypedValue.applyDimension(0, (float) this.defaultBtmHeight, getResources().getDisplayMetrics()));
        }
        removeFooterView(this.textView);
        addFooterView(this.textView, null, false);
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        try {
            if (this.onListKeyClickListener != null) {
                View view = getChildAt(((int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d)) - getHeaderViewsCount());
                if (this.onListKeyClickListener.onListKeyClick(theKey, getPositionForView(view), view)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            float scroll;
            if (theKey == HMKeyEvent.KEY_DOWN) {
                if (getLastVisiblePosition() < getHeaderViewsCount() || getFirstVisiblePosition() == getLastVisiblePosition()) {
                    smoothScrollBy(Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()), 600);
                } else {
                    View lastView = getChildAt(((int) Math.floor(((double) (getLastVisiblePosition() - getFirstVisiblePosition())) / 2.0d)) + 1);
                    scroll = (float) ((((double) lastView.getBottom()) - (((double) lastView.getHeight()) / 2.0d)) - (((double) ((float) ((Activity) getContext()).getWindow().getDecorView().getHeight())) / 2.0d));
                    if (scroll == 0.0f) {
                        scroll = (float) lastView.getMeasuredHeight();
                    }
                    smoothScrollBy((int) scroll, 600);
                }
                return true;
            } else if (theKey == HMKeyEvent.KEY_UP) {
                if (getFirstVisiblePosition() < getHeaderViewsCount() || getFirstVisiblePosition() == getLastVisiblePosition()) {
                    smoothScrollBy(-Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()), 600);
                } else {
                    View firstView = getChildAt(((int) Math.ceil(((double) (getLastVisiblePosition() - getFirstVisiblePosition())) / 2.0d)) - 1);
                    scroll = (float) ((((double) ((float) ((Activity) getContext()).getWindow().getDecorView().getHeight())) / 2.0d) - (((double) firstView.getBottom()) - (((double) firstView.getHeight()) / 2.0d)));
                    if (scroll == 0.0f) {
                        scroll = (float) firstView.getMeasuredHeight();
                    }
                    smoothScrollBy((int) (-scroll), 600);
                }
                return true;
            } else {
                if (theKey == HMKeyEvent.KEY_CENTER) {
                    int focused = (int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d);
                    if (this.withSelector) {
                        checkedSelector(getChildAt(focused));
                    }
                    if (!performItemClick(getChildAt(focused), getPositionForView(getChildAt(focused)), getItemIdAtPosition(getPositionForView(getChildAt(focused))))) {
                        getChildAt(focused).performClick();
                    }
                }
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void checkedSelector(final View view) {
        if (view != null && getPositionForView(view) >= getHeaderViewsCount()) {
            if (this.handler == null) {
                this.handler = new Handler(Looper.getMainLooper());
            }
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    view.setBackgroundColor(Color.parseColor("#00000000"));
                    HmKeyEventListView.this.handler.removeCallbacksAndMessages(null);
                }
            }, 50);
            view.setBackgroundColor(Color.parseColor("#4D4E53"));
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        try {
            if (this.onListKeyClickListener != null) {
                View view = getChildAt(((int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d)) - getHeaderViewsCount());
                if (this.onListKeyClickListener.onListKeyLongOneSecondClick(theKey, getPositionForView(view), view)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        try {
            if (this.onListKeyClickListener != null) {
                View view = getChildAt(((int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d)) - getHeaderViewsCount());
                if (this.onListKeyClickListener.onListKeyLongThreeSecondClick(theKey, getPositionForView(view), view)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean isHasFocused() {
        return this.hasFocused;
    }

    public void setHasFocused(boolean hasFocused) {
        this.hasFocused = hasFocused;
    }

    public void setHasBottomView(boolean hasBottomView) {
        this.hasBottomView = hasBottomView;
        if (!hasBottomView && this.textView != null) {
            removeFooterView(this.textView);
        }
    }

    public void setHasBottomView(boolean hasBottomView, int heightBottom) {
        this.defaultBtmHeight = heightBottom;
        setHasBottomView(hasBottomView);
    }

    protected void dispatchDraw(Canvas canvas) {
        if (this.hasFocused && this.isModeEverestReal) {
            try {
                int focused = (int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d);
                if (getPositionForView(getChildAt(focused)) == -1 || getPositionForView(getChildAt(focused)) > (getCount() - getFooterViewsCount()) - 1) {
                    super.dispatchDraw(canvas);
                    return;
                }
                int position = getPositionForView(getChildAt(focused));
                if (position >= getHeaderViewsCount() && position < getAdapter().getCount()) {
                    View view = getChildAt(focused);
                    if (this.paint == null) {
                        this.paint = new Paint();
                        this.paint.setColor(Color.parseColor("#2D313A"));
                        this.paint.setStyle(Style.FILL);
                    }
                    canvas.drawRect(0.0f, (float) view.getTop(), (float) getResources().getDisplayMetrics().widthPixels, (float) view.getBottom(), this.paint);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.dispatchDraw(canvas);
    }

    public void setOnItemClickListener(@Nullable OnItemClickListener listener) {
        if (this.withSelector) {
            createSelector();
        }
        super.setOnItemClickListener(listener);
    }

    void createSelector() {
        setSelectionAfterHeaderView();
        setSelector(createViewSelector());
    }

    public Drawable createViewSelector() {
        StateListDrawable selector = new StateListDrawable();
        Drawable pressDraw = new ColorDrawable(Color.parseColor("#4D4E53"));
        Drawable normalDraw = new ColorDrawable(Color.parseColor("#00000000"));
        selector.addState(new int[]{16842919}, pressDraw);
        selector.addState(new int[]{-16842919}, normalDraw);
        return selector;
    }

    public void setAdapter(ListAdapter adapter) {
        if (this.hasBottomView) {
            addDefaultFooter();
        }
        super.setAdapter(adapter);
    }

    public boolean hasFocusedItems() {
        try {
            if (getPositionForView(getChildAt((int) Math.floor(((double) ((getLastVisiblePosition() - getFirstVisiblePosition()) + 1)) / 2.0d))) >= getHeaderViewsCount()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setItemFocusedListener(final ItemFocusedListener itemFocusedListener) {
        if (itemFocusedListener != null) {
            setOnScrollListener(new OnScrollListener() {
                public void onScrollStateChanged(AbsListView absListView, int i) {
                    if (absListView != null) {
                        try {
                            if (HmKeyEventListView.this.getPositionForView(HmKeyEventListView.this.getChildAt((int) Math.floor(((double) ((HmKeyEventListView.this.getLastVisiblePosition() - HmKeyEventListView.this.getFirstVisiblePosition()) + 1)) / 2.0d))) >= HmKeyEventListView.this.getHeaderViewsCount()) {
                                itemFocusedListener.hasItemFocused(true);
                            } else {
                                itemFocusedListener.hasItemFocused(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                }
            });
        }
    }

    public void setOnFocusedItemCallBack(final OnFocusedItemCallBack onFocusedItemCallBack) {
        if (onFocusedItemCallBack != null) {
            setOnScrollListener(new OnScrollListener() {
                public void onScrollStateChanged(AbsListView absListView, int i) {
                    if (absListView != null) {
                        try {
                            onFocusedItemCallBack.focusedItem(HmKeyEventListView.this.getPositionForView(HmKeyEventListView.this.getChildAt((int) Math.floor(((double) ((HmKeyEventListView.this.getLastVisiblePosition() - HmKeyEventListView.this.getFirstVisiblePosition()) + 1)) / 2.0d))));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                }
            });
        }
    }

    public void setWithSelector(boolean withSelector) {
        this.withSelector = withSelector;
    }

    public void setOnListKeyClickListener(OnListKeyClickListener onListKeyClickListener) {
        this.onListKeyClickListener = onListKeyClickListener;
    }
}
