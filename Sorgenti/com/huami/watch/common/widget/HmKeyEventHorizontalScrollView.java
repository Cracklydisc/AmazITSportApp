package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.HorizontalScrollView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.utils.Utils;

public class HmKeyEventHorizontalScrollView extends HorizontalScrollView implements EventCallBack, KeyeventConsumer {
    private boolean canAccept = true;
    private KeyeventProcessor processor;

    public HmKeyEventHorizontalScrollView(Context context) {
        super(context);
    }

    public HmKeyEventHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmKeyEventHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        if (theKey == HMKeyEvent.KEY_DOWN) {
            smoothScrollTo(getScrollY() + Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()), 0);
            return true;
        } else if (theKey == HMKeyEvent.KEY_UP) {
            smoothScrollTo(Math.max(0, getScrollY() - Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics())), 0);
            return true;
        } else {
            if (theKey == HMKeyEvent.KEY_CENTER) {
            }
            return false;
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }
}
