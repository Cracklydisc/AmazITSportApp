package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOverlay;
import android.widget.ImageView;
import com.huami.watch.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;

public class HmAnimatedOverlayImageView extends ImageView {
    private static final HashMap<Integer, Class<? extends HmAbstractOverlayAnimatorDrawable>> sRegisterAnimDrawableTypes = new HashMap();
    private boolean isMultiple;
    private AnimatorSet mAnimators;
    private float mContentRatio;
    private final SparseArray<HmAbstractOverlayAnimatorDrawable> mDrawables;
    private ViewGroup mOverlayParent;

    public static abstract class HmAbstractOverlayAnimatorDrawable extends Drawable {
        protected abstract float getContentRatio();

        protected abstract ValueAnimator getOverlayAnmiator();
    }

    protected static void registerAnimatorDrawableTypeInternal(int featureType, Class<? extends HmAbstractOverlayAnimatorDrawable> cls) {
        sRegisterAnimDrawableTypes.put(Integer.valueOf(featureType), cls);
    }

    public HmAnimatedOverlayImageView(Context context) {
        this(context, null);
    }

    public HmAnimatedOverlayImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HmAnimatedOverlayImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isMultiple = false;
        this.mContentRatio = 0.78431374f;
        this.mDrawables = new SparseArray(5);
    }

    public void addOverlayAnimationFeature(int featureType) {
        if (!sRegisterAnimDrawableTypes.containsKey(Integer.valueOf(featureType))) {
            throw new IllegalArgumentException("No FeatureType " + featureType + " register! Please call ");
        } else if (this.mDrawables.get(featureType) == null) {
            try {
                this.mDrawables.put(featureType, (HmAbstractOverlayAnimatorDrawable) ((Class) sRegisterAnimDrawableTypes.get(Integer.valueOf(featureType))).newInstance());
            } catch (InstantiationException e) {
            } catch (IllegalAccessException e2) {
            }
        }
    }

    private View getVoverlayViewContainer() {
        if (this.mOverlayParent != null) {
            return this.mOverlayParent;
        }
        return this;
    }

    public final void playOverlayDrawableAnimation(final Runnable task, int... featureTypes) {
        if (featureTypes != null && featureTypes.length != 0) {
            final ViewOverlay viewOverlay = getVoverlayViewContainer().getOverlay();
            HmAbstractOverlayAnimatorDrawable d;
            ValueAnimator animator;
            if (featureTypes.length > 1) {
                if (this.mAnimators != null && this.mAnimators.isStarted()) {
                    this.mAnimators.cancel();
                }
                boolean setupTask = false;
                ArrayList<Animator> anims = new ArrayList(5);
                for (int i = 0; i < featureTypes.length; i++) {
                    d = (HmAbstractOverlayAnimatorDrawable) this.mDrawables.get(featureTypes[i]);
                    if (d != null) {
                        animator = d.getOverlayAnmiator();
                        animator.cancel();
                        setOverlayDrawableBounds(d);
                        viewOverlay.add(d);
                        if (!(task == null || setupTask || animator.getRepeatCount() != -1)) {
                            if (anims.size() < 1) {
                                throw new IllegalArgumentException("Can not setup the task on the animator end, the first animator will be run infinite.");
                            }
                            ((Animator) anims.get(i - 1)).addListener(new SimpleAnimatorListener() {
                                public void onAnimationComplete(Animator animator) {
                                    if (task != null) {
                                        task.run();
                                    }
                                }
                            });
                            setupTask = true;
                        }
                        anims.add(animator);
                    }
                }
                final boolean runTaskLast = !setupTask;
                this.mAnimators = new AnimatorSet();
                this.mAnimators.playSequentially(anims);
                final int[] iArr = featureTypes;
                final Runnable runnable = task;
                this.mAnimators.addListener(new SimpleAnimatorListener() {
                    public void onAnimationEnd(Animator animator) {
                        HmAnimatedOverlayImageView.this.mAnimators.removeListener(this);
                        for (int i : iArr) {
                            HmAbstractOverlayAnimatorDrawable d = (HmAbstractOverlayAnimatorDrawable) HmAnimatedOverlayImageView.this.mDrawables.get(i);
                            if (d != null) {
                                viewOverlay.remove(d);
                            }
                        }
                        HmAnimatedOverlayImageView.this.isMultiple = false;
                        super.onAnimationEnd(animator);
                    }

                    public void onAnimationComplete(Animator animator) {
                        if (runnable != null && runTaskLast) {
                            runnable.run();
                        }
                    }
                });
                this.isMultiple = true;
                this.mAnimators.start();
                return;
            }
            d = (HmAbstractOverlayAnimatorDrawable) this.mDrawables.get(featureTypes[0]);
            if (d != null) {
                animator = d.getOverlayAnmiator();
                animator.cancel();
                if (task == null || animator.getRepeatCount() != -1) {
                    setOverlayDrawableBounds(d);
                    viewOverlay.add(d);
                    animator.addListener(new SimpleAnimatorListener() {
                        public void onAnimationEnd(Animator animator) {
                            animator.removeListener(this);
                            viewOverlay.remove(d);
                            super.onAnimationEnd(animator);
                        }

                        public void onAnimationComplete(Animator animator) {
                            if (task != null) {
                                task.run();
                            }
                        }
                    });
                    animator.start();
                    return;
                }
                throw new IllegalArgumentException("Can not setup the task on the animator end, the animator will be run infinite.");
            }
        }
    }

    public void setContentRatio(float ratio) {
        this.mContentRatio = ratio;
    }

    private void setOverlayDrawableBounds(HmAbstractOverlayAnimatorDrawable d) {
        View overlayContainerView = getVoverlayViewContainer();
        Rect bounds = new Rect();
        Utils.getDescendantRectRelativeToParent(this, overlayContainerView, bounds);
        int width = bounds.width();
        float expand = ((this.mContentRatio / d.getContentRatio()) - 1.0f) * 0.5f;
        int hOffset = (int) Math.ceil((double) (((float) width) * expand));
        int vOffset = (int) Math.ceil((double) (((float) bounds.height()) * expand));
        bounds.left -= hOffset;
        bounds.top -= vOffset;
        bounds.right += hOffset;
        bounds.bottom += vOffset;
        d.setBounds(bounds);
    }

    public void setOverlayParent(ViewGroup parent) {
        this.mOverlayParent = parent;
    }
}
