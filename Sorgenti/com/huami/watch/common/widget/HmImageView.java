package com.huami.watch.common.widget;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import com.huami.watch.utils.QuadInterpolator;

public class HmImageView extends ImageView {
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private ValueAnimator mAnim;
    private AnimatorUpdateListener mAnimatorUpdateListener = new C04371();
    private boolean mClickAnimEnable = false;
    private float mScale = 1.0f;

    class C04371 implements AnimatorUpdateListener {
        C04371() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmImageView.this.mScale = ((Float) animation.getAnimatedValue()).floatValue();
            HmImageView.this.setScaleX(HmImageView.this.mScale);
            HmImageView.this.setScaleY(HmImageView.this.mScale);
        }
    }

    class C04382 implements OnTouchListener {
        final /* synthetic */ HmImageView this$0;

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case 0:
                    this.this$0.startAnimDown();
                    break;
                case 1:
                case 3:
                    this.this$0.startAnimUp();
                    break;
            }
            return false;
        }
    }

    public HmImageView(Context context) {
        super(context);
    }

    public HmImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void cancelAnim() {
        if (this.mAnim != null && this.mAnim.isStarted()) {
            this.mAnim.cancel();
        }
        this.mAnim = null;
    }

    public void startAnimDown() {
        startAnimDown(null);
    }

    public void startAnimDown(AnimatorListener listener) {
        cancelAnim();
        this.mAnim = ValueAnimator.ofFloat(new float[]{this.mScale, 0.9f});
        this.mAnim.setDuration(48);
        this.mAnim.setInterpolator(QUAD_OUT);
        this.mAnim.addUpdateListener(this.mAnimatorUpdateListener);
        if (listener != null) {
            this.mAnim.addListener(listener);
        }
        this.mAnim.start();
    }

    public void startAnimUp() {
        cancelAnim();
        this.mAnim = ValueAnimator.ofFloat(new float[]{this.mScale, 1.0f});
        this.mAnim.setDuration(200);
        this.mAnim.setInterpolator(QUAD_OUT);
        this.mAnim.addUpdateListener(this.mAnimatorUpdateListener);
        this.mAnim.start();
    }
}
