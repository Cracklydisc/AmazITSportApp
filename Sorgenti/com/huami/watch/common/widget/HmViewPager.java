package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.DecelerateInterpolator;

public class HmViewPager extends ViewPager {
    private float mDownX = Float.MIN_VALUE;
    private float mDownY = Float.MIN_VALUE;
    private boolean mEnableOverScroll = true;
    private boolean mEnableOverScrollHorizontal = false;
    private boolean mIsConfirmSlidingDirection = false;
    private boolean mIsSlidingRight = false;
    private int mLastScrollX = 0;
    private OnOverScrollListener mOnOverScrollListener;
    private ValueAnimator mOverAnim;
    private boolean scrollble = true;

    class C04631 implements AnimatorUpdateListener {
        C04631() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmViewPager.this.setScrollX(((Integer) animation.getAnimatedValue()).intValue());
        }
    }

    class C04642 extends AnimatorListenerAdapter {
        C04642() {
        }

        public void onAnimationEnd(Animator animation) {
            if (HmViewPager.this.mOnOverScrollListener != null && HmViewPager.this.mIsSlidingRight) {
                HmViewPager.this.mOnOverScrollListener.onOverScroll();
            }
        }
    }

    public interface OnOverScrollListener {
        void onOverScroll();
    }

    public HmViewPager(Context context) {
        super(context);
        initOverscrollAnim();
    }

    public HmViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        initOverscrollAnim();
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean z = false;
        if (this.scrollble || !this.mEnableOverScroll) {
            return super.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case 0:
                cancelOverscrollAnim();
                if (this.mDownX != Float.MIN_VALUE) {
                    return true;
                }
                this.mDownX = event.getX();
                this.mDownY = event.getY();
                return true;
            case 1:
            case 3:
                this.mDownX = Float.MIN_VALUE;
                this.mDownY = Float.MIN_VALUE;
                startOverscrollAnim();
                this.mIsConfirmSlidingDirection = false;
                this.mEnableOverScrollHorizontal = false;
                return true;
            case 2:
                if (!this.mIsConfirmSlidingDirection) {
                    boolean z2;
                    float x = event.getX();
                    int deltX = (int) Math.abs(x - this.mDownX);
                    int deltY = (int) Math.abs(event.getY() - this.mDownY);
                    this.mIsConfirmSlidingDirection = true;
                    if (deltX > deltY) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    this.mEnableOverScrollHorizontal = z2;
                }
                if (!this.mEnableOverScrollHorizontal) {
                    return true;
                }
                int overscrollX = (int) ((this.mDownX - event.getX()) * 0.2f);
                if (overscrollX <= 0) {
                    z = true;
                }
                this.mIsSlidingRight = z;
                setScrollX(this.mLastScrollX + overscrollX);
                return true;
            default:
                return true;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        if (this.scrollble || arg0.getAction() != 2) {
            return super.onInterceptTouchEvent(arg0);
        }
        return true;
    }

    private void initOverscrollAnim() {
        this.mOverAnim = new ValueAnimator();
        this.mOverAnim.setDuration(150);
        this.mOverAnim.setInterpolator(new DecelerateInterpolator());
        this.mOverAnim.addUpdateListener(new C04631());
        this.mOverAnim.addListener(new C04642());
    }

    private void startOverscrollAnim() {
        if (this.mOverAnim.isStarted()) {
            this.mOverAnim.cancel();
        }
        this.mDownX = Float.MIN_VALUE;
        if (getScrollX() != this.mLastScrollX) {
            this.mOverAnim.setIntValues(new int[]{getScrollX(), this.mLastScrollX});
            this.mOverAnim.start();
        }
    }

    private void cancelOverscrollAnim() {
        if (this.mOverAnim.isStarted()) {
            this.mOverAnim.cancel();
        }
    }
}
