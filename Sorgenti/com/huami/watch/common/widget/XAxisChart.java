package com.huami.watch.common.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.ui.C1070R;

public class XAxisChart extends View {
    protected boolean mAnimate;
    public float mDateLineHeight;
    private String[] mDays;
    protected boolean mDontPaintLastXLabel;
    protected boolean mDrawGrid;
    protected boolean mDrawXAxis;
    private boolean mDrawXAxisLine;
    protected Paint mEveryDateTextPaint;
    private float mEveryDateTextPaintHeight;
    private float mEveryPointTextHeight;
    protected Paint mEveryTextPaint;
    private int mGridEndColor;
    private PointF[] mGridLocations;
    protected Paint mGridPaint;
    private int mGridStartColor;
    private boolean mIsShowDates;
    private boolean mIsShowYValue;
    protected float mLineOffset;
    protected float mMaxXSize;
    private String[] mMonths;
    protected boolean mParamsInitialized;
    protected float mXAxisLabelHeight;
    protected float mXAxisLabelOffset;
    protected Paint mXAxisLabelPaint;
    private float mXAxisOffset;
    protected Paint mXAxisPaint;
    protected PointF[] mXLabelLocations;
    protected String[] mXLabels;
    protected PointF[] mYLabelLocations;
    protected String[] mYLabels;
    protected PointF[] mYLineLocations;
    protected float mYMaxHeight;
    protected float mYOffset;
    protected float f9x;
    protected float f10y;

    public XAxisChart(Context context) {
        this(context, null);
    }

    public XAxisChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public XAxisChart(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public XAxisChart(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mXLabelLocations = new PointF[0];
        this.mIsShowDates = false;
        this.mXLabels = new String[0];
        this.mDays = null;
        this.mMonths = null;
        this.mXAxisLabelHeight = 0.0f;
        this.mXAxisLabelOffset = 0.0f;
        this.mYMaxHeight = 0.0f;
        this.mYOffset = 0.0f;
        this.mMaxXSize = 0.0f;
        this.mParamsInitialized = false;
        this.mAnimate = false;
        this.mDontPaintLastXLabel = false;
        this.mDrawGrid = true;
        this.mDrawXAxis = true;
        this.mEveryPointTextHeight = 0.0f;
        this.mEveryDateTextPaintHeight = 0.0f;
        this.mDateLineHeight = 0.0f;
        this.mXAxisOffset = 0.0f;
        this.mIsShowYValue = true;
        this.mDrawXAxisLine = true;
        init(context);
    }

    protected void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel) {
        this.mMaxXSize = xRange;
        if (xLabels != null && xLabels.length > 0) {
            this.mXLabels = new String[xLabels.length];
            System.arraycopy(xLabels, 0, this.mXLabels, 0, xLabels.length);
            this.mXLabelLocations = new PointF[this.mXLabels.length];
            this.mParamsInitialized = false;
        }
        this.mDontPaintLastXLabel = dontPaintLastXLabel;
    }

    protected void setData(String[] xLabels, String[] yLabels, float xRange, boolean dontPaintLastXLabel) {
        setData(xLabels, xRange, dontPaintLastXLabel);
        if (yLabels != null && yLabels.length > 0) {
            this.mYLabels = new String[yLabels.length];
            System.arraycopy(yLabels, 0, this.mYLabels, 0, yLabels.length);
        }
    }

    private void init(Context context) {
        Resources res = context.getResources();
        float radius = res.getDimension(C1070R.dimen.size_shadow_radius1);
        float dy = res.getDimension(C1070R.dimen.size_shadow_dy1);
        Bitmap bitmap = BitmapFactory.decodeResource(res, C1070R.drawable.health_all_heart_date_line);
        this.mDateLineHeight = (float) bitmap.getWidth();
        bitmap.recycle();
        this.mXAxisLabelHeight = res.getDimension(C1070R.dimen.size_xaxis_label_font);
        this.mXAxisPaint = new Paint(1);
        this.mXAxisPaint.setStrokeWidth(1.0f);
        this.mXAxisPaint.setColor(res.getColor(C1070R.color.color_xaxis));
        this.mGridPaint = new Paint(1);
        this.mGridPaint.setStrokeWidth(1.0f);
        this.mXAxisLabelPaint = new Paint(1);
        this.mXAxisLabelPaint.setTextSize(this.mXAxisLabelHeight);
        this.mXAxisLabelPaint.setColor(res.getColor(C1070R.color.color_xaxis_label));
        this.mXAxisLabelPaint.setShadowLayer(radius, 0.0f, dy, res.getColor(C1070R.color.color_shadow));
        this.mXAxisLabelOffset = radius + dy;
        this.mEveryPointTextHeight = res.getDimension(C1070R.dimen.pickview_unit_size);
        this.mEveryTextPaint = new Paint(1);
        this.mEveryTextPaint.setTextSize(this.mEveryPointTextHeight);
        this.mEveryTextPaint.setColor(res.getColor(C1070R.color.white));
        this.mEveryDateTextPaint = new Paint(1);
        this.mEveryDateTextPaint.setTextSize(this.mEveryPointTextHeight);
        this.mEveryDateTextPaint.setColor(res.getColor(C1070R.color.date_color));
        this.mGridStartColor = res.getColor(C1070R.color.color_xaxis_start);
        this.mGridEndColor = res.getColor(C1070R.color.color_xaxis);
        this.mLineOffset = 3.0f * dy;
        this.mYOffset = res.getDimension(C1070R.dimen.size_linechart_yoffset);
    }

    protected void setVMinOffset(float offset) {
        this.mYOffset = offset;
    }

    protected void calculateXAxisParams(float left, float width, float top, float height) {
        int labelSize = this.mXLabels.length;
        Rect rect = new Rect();
        this.mGridLocations = new PointF[labelSize];
        for (int i = 0; i < labelSize; i++) {
            this.mGridLocations[i] = new PointF(left + (labelSize > 1 ? (((float) i) * width) / ((float) (labelSize - 1)) : 0.0f), top);
            this.mXAxisLabelPaint.getTextBounds(this.mXLabels[i], 0, this.mXLabels[i].length(), rect);
            this.mXLabelLocations[i] = new PointF(this.mGridLocations[i].x - (((float) Math.abs(rect.right - rect.left)) / 2.0f), ((top + height) - this.mXAxisLabelOffset) + this.mXAxisOffset);
        }
        this.mYMaxHeight = height - (this.mDrawXAxis ? this.mXAxisLabelHeight + this.mXAxisLabelOffset : 0.0f);
        this.mGridPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, this.mYMaxHeight, this.mGridStartColor, this.mGridEndColor, TileMode.CLAMP));
    }

    protected void calculateYAxis(float left, double maxY, double minY) {
        if (this.mYLabels != null && this.mYLabels.length > 0) {
            this.mYLabelLocations = new PointF[this.mYLabels.length];
            this.mYLineLocations = new PointF[this.mYLabels.length];
            for (int i = 0; i < this.mYLabels.length; i++) {
                float length;
                this.mYLabelLocations[i] = new PointF((left - this.mLineOffset) - this.mXAxisLabelPaint.measureText(this.mYLabels[i]), ((float) (((maxY - minY) * ((double) (this.mYLabels.length > 1 ? ((float) ((this.mYLabels.length - i) - 1)) / ((float) (this.mYLabels.length - 1)) : 0.0f))) + minY)) + (this.mLineOffset / 2.0f));
                PointF[] pointFArr = this.mYLineLocations;
                double d = maxY - minY;
                if (this.mYLabels.length > 1) {
                    length = ((float) ((this.mYLabels.length - i) - 1)) / ((float) (this.mYLabels.length - 1));
                } else {
                    length = 0.0f;
                }
                pointFArr[i] = new PointF(left, (float) ((d * ((double) length)) + minY));
            }
        }
    }

    protected void setXAxisTextSize(float size) {
        this.mXAxisLabelHeight = size;
        this.mXAxisLabelPaint.setTextSize(this.mXAxisLabelHeight);
    }

    public void onDraw(Canvas canvas) {
        drawXAxis(canvas);
        drawXLabels(canvas);
        drawGrids(canvas);
        drawYAxis(canvas);
    }

    private void drawGrids(Canvas canvas) {
        if (this.mDrawGrid) {
            for (PointF loc : this.mGridLocations) {
                canvas.drawLine(loc.x, loc.y, loc.x, this.mYMaxHeight + loc.y, this.mGridPaint);
            }
        }
    }

    protected void setxAxisOffset(float offset) {
        this.mXAxisOffset = offset;
    }

    protected void drawXAxis(Canvas canvas) {
        if (this.mDrawXAxis && this.mDrawXAxisLine) {
            canvas.drawLine(0.0f, this.mYMaxHeight, (float) getWidth(), this.mYMaxHeight, this.mXAxisPaint);
        }
    }

    protected void drawYAxis(Canvas canvas) {
        if (this.mYLabels != null && this.mYLabelLocations != null && this.mYLineLocations != null) {
            int i = 0;
            while (i < this.mYLabels.length) {
                if (this.mIsShowYValue || i != 0) {
                    canvas.drawLine(this.mYLineLocations[i].x, this.mYLineLocations[i].y, this.mYLineLocations[i].x - this.mLineOffset, this.mYLineLocations[i].y, this.mGridPaint);
                    canvas.drawText(this.mYLabels[i], this.mYLabelLocations[i].x, this.mYLabelLocations[i].y, this.mXAxisLabelPaint);
                }
                i++;
            }
        }
    }

    protected void drawXLabels(Canvas canvas) {
        int i;
        if (this.mIsShowDates) {
            i = 0;
            while (true) {
                if (i < this.mXLabels.length - (this.mDontPaintLastXLabel ? 1 : 0)) {
                    Bitmap minBitmap = BitmapFactory.decodeResource(getResources(), C1070R.drawable.health_all_heart_date_line);
                    canvas.drawText(this.mMonths[i], this.mXLabelLocations[i].x, (this.mXLabelLocations[i].y + ((float) minBitmap.getHeight())) + this.mEveryDateTextPaintHeight, this.mEveryDateTextPaint);
                    canvas.drawBitmap(minBitmap, this.mXLabelLocations[i].x, (this.mXLabelLocations[i].y + ((float) minBitmap.getHeight())) + (this.mEveryDateTextPaintHeight * 2.0f), this.mEveryDateTextPaint);
                    canvas.drawText(this.mDays[i], this.mXLabelLocations[i].x, ((this.mXLabelLocations[i].y + ((float) ((minBitmap.getHeight() * 3) / 2))) + (this.mEveryDateTextPaintHeight * 3.0f)) + this.mDateLineHeight, this.mEveryDateTextPaint);
                    minBitmap.recycle();
                    i++;
                } else {
                    return;
                }
            }
        } else if (this.mDrawXAxis) {
            i = 0;
            while (true) {
                if (i < this.mXLabels.length - (this.mDontPaintLastXLabel ? 1 : 0)) {
                    canvas.drawText(this.mXLabels[i], this.mXLabelLocations[i].x, this.mXLabelLocations[i].y, this.mXAxisLabelPaint);
                    i++;
                } else {
                    return;
                }
            }
        }
    }
}
