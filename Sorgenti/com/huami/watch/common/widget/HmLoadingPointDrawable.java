package com.huami.watch.common.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.view.animation.PathInterpolator;

public class HmLoadingPointDrawable extends Drawable implements Animatable, Runnable {
    private static final PathInterpolator INOUT = new PathInterpolator(0.42f, 0.0f, 0.42f, 1.01f);
    private int DURATION = 400;
    private int END_COLOR = 1090519039;
    private float RADIUS_END = 16.0f;
    private float RADIUS_START = 10.0f;
    private int START_COLOR = -1073741825;
    private boolean mAnimating = false;
    private int mFps = 30;
    private float mFractionP;
    private float mFractionR;
    private int mFrameDuration = 16;
    private Runnable mOnEnd;
    private Paint mPaint = new Paint(1);
    private boolean mPlayingBackwards = false;
    private float mRadius;
    private boolean mRunning = false;
    private float mScreenCenterX;
    private float mScreenCenterY;
    private float mStepF = ((((float) this.mFrameDuration) * 1.0f) / ((float) this.DURATION));
    private boolean mStopLoop = false;

    public HmLoadingPointDrawable() {
        this.mPaint.setStyle(Style.FILL_AND_STROKE);
        this.mPaint.setColor(this.START_COLOR);
        setFps(20);
    }

    public void start() {
        if (!isRunning()) {
            this.mAnimating = true;
            this.mStopLoop = false;
            run();
        }
    }

    public void stop() {
        this.mAnimating = false;
        reset();
        if (isRunning()) {
            unscheduleSelf(this);
        }
    }

    public boolean isRunning() {
        return this.mRunning;
    }

    protected void onBoundsChange(Rect bounds) {
        this.mScreenCenterX = (float) bounds.centerX();
        this.mScreenCenterY = (float) bounds.centerY();
    }

    public void draw(Canvas canvas) {
        canvas.drawColor(Integer.MIN_VALUE);
        canvas.drawCircle(this.mScreenCenterX, this.mScreenCenterY, this.mRadius, this.mPaint);
    }

    private void update(float input) {
        this.mRadius = this.RADIUS_START + ((this.RADIUS_END - this.RADIUS_START) * input);
        this.mPaint.setColor(evaluateColor(input, this.START_COLOR, this.END_COLOR));
    }

    private static final int evaluateColor(float fraction, int startColor, int endColor) {
        int startA = (startColor >> 24) & 255;
        int startR = (startColor >> 16) & 255;
        int startG = (startColor >> 8) & 255;
        int startB = startColor & 255;
        return ((((((int) (((float) (((endColor >> 24) & 255) - startA)) * fraction)) + startA) << 24) | ((((int) (((float) (((endColor >> 16) & 255) - startR)) * fraction)) + startR) << 16)) | ((((int) (((float) (((endColor >> 8) & 255) - startG)) * fraction)) + startG) << 8)) | (((int) (((float) ((endColor & 255) - startB)) * fraction)) + startB);
    }

    public void setAlpha(int alpha) {
        this.mPaint.setAlpha(alpha);
    }

    public void setColorFilter(ColorFilter cf) {
        this.mPaint.setColorFilter(cf);
    }

    public int getOpacity() {
        return -3;
    }

    private void setFps(int fps) {
        if (fps < 1) {
            throw new IllegalArgumentException("The value of Fps must be > 1.");
        }
        this.mFps = fps;
        updateFrameDuration();
    }

    private void updateFrameDuration() {
        this.mFrameDuration = 1000 / this.mFps;
        this.mStepF = (((float) this.mFrameDuration) * 1.0f) / ((float) this.DURATION);
    }

    public boolean setVisible(boolean visible, boolean restart) {
        boolean changed = super.setVisible(visible, restart);
        if (!visible) {
            unscheduleSelf(this);
        } else if (restart || changed) {
            nextFrame();
        }
        return changed;
    }

    public void run() {
        nextFrame();
    }

    private void reset() {
        this.mStopLoop = false;
        this.mFractionP = 0.0f;
        this.mFractionR = 0.0f;
        this.mPlayingBackwards = false;
        this.mOnEnd = null;
    }

    public void unscheduleSelf(Runnable what) {
        this.mRunning = false;
        super.unscheduleSelf(what);
    }

    private void nextFrame() {
        boolean z = false;
        this.mFractionR += this.mStepF;
        if (this.mFractionR >= 1.0f) {
            if (this.mStopLoop) {
                this.mFractionR = 0.0f;
                this.mAnimating = false;
            } else {
                this.mFractionR %= 1.0f;
            }
        }
        this.mFractionP += this.mStepF;
        if (this.mFractionP >= 1.0f) {
            if (!this.mPlayingBackwards) {
                z = true;
            }
            this.mPlayingBackwards = z;
            this.mFractionP %= 1.0f;
        }
        if (this.mPlayingBackwards) {
            update(INOUT.getInterpolation(1.0f - this.mFractionP));
        } else {
            update(INOUT.getInterpolation(this.mFractionP));
        }
        invalidateSelf();
        if (this.mAnimating) {
            this.mRunning = true;
            scheduleSelf(this, SystemClock.uptimeMillis() + ((long) this.mFrameDuration));
            return;
        }
        stop();
        if (this.mOnEnd != null) {
            this.mOnEnd.run();
            this.mOnEnd = null;
        }
    }
}
