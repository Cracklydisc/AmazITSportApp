package com.huami.watch.common.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huami.watch.common.widget.HmKeyEventScrollView.OnKeyCallBackListener;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import com.huami.watch.ui.C1070R;
import java.util.ArrayList;

public class SystemDialog extends Dialog {
    private DismissListener dismissListener;
    private KeyEventDispatcher keyEventDispatcher;

    public static class Builder implements OnKeyCallBackListener {
        private HmHaloButton buttonNegativeButton;
        private HmHaloButton buttonPositiveButton;
        private View contentView;
        private Context context;
        private boolean isPositive = true;
        private OnCancelListener mOnCancelListener;
        private OnDismissListener mOnDismissListener;
        private OnKeyListener mOnKeyListener;
        private String message;
        private int messageTextColor = -1;
        private float messageTextSize = -1.0f;
        private OnClickListener negativeButtonClickListener;
        private String negativeButtonText;
        private Drawable negativeDrawable;
        private OnClickListener positiveButtonClickListener;
        private String positiveButtonText;
        private Drawable positiveDrawable;
        private LinearLayout scrollLayout;
        private HmKeyEventScrollView scrollView;
        private boolean systemType = false;
        private int theme = -1;
        private String title;
        private int titleTextColor = -1;
        private float titleTextSize = -1.0f;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessage(int message) {
            this.message = (String) this.context.getText(message);
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = (String) this.context.getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setOnDismissListener(OnDismissListener onDismissListener) {
            this.mOnDismissListener = onDismissListener;
            return this;
        }

        public Builder setMessageTextSize(float messageTextSize) {
            this.messageTextSize = messageTextSize;
            return this;
        }

        public Builder setMessageTextColor(int messageTextColor) {
            this.messageTextColor = messageTextColor;
            return this;
        }

        public Builder setSystemType(boolean systemType) {
            this.systemType = systemType;
            return this;
        }

        public void onKeyCall(HMKeyEvent key) {
            if (key == HMKeyEvent.KEY_CENTER) {
                if (this.isPositive) {
                    if (this.buttonPositiveButton != null) {
                        this.buttonPositiveButton.performClick();
                        this.buttonPositiveButton.setPressed(true);
                    }
                } else if (this.buttonNegativeButton != null) {
                    this.buttonNegativeButton.performClick();
                    this.buttonNegativeButton.setPressed(true);
                }
            } else if (key == HMKeyEvent.KEY_DOWN) {
                if (this.scrollView != null && this.scrollView.isScrolledToBottom()) {
                    changeBtnFocusable();
                }
            } else if (key == HMKeyEvent.KEY_UP && this.scrollView != null && this.scrollView.isScrolledToTop()) {
                changeBtnFocusable();
            }
        }

        private void changeBtnFocusable() {
            if (this.buttonPositiveButton != null && this.buttonNegativeButton != null && this.negativeButtonText != null) {
                if (this.isPositive) {
                    this.buttonPositiveButton.setBackgroundResource(C1070R.drawable.system_dialog_button_bg_negative);
                    this.buttonNegativeButton.setBackgroundResource(C1070R.drawable.system_dialog_button_bg_positive);
                } else {
                    this.buttonPositiveButton.setBackgroundResource(C1070R.drawable.system_dialog_button_bg_positive);
                    this.buttonNegativeButton.setBackgroundResource(C1070R.drawable.system_dialog_button_bg_negative);
                }
                this.isPositive = !this.isPositive;
            }
        }

        public SystemDialog create() {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
            if (this.theme == -1) {
                this.theme = C1070R.style.HmDialog;
            }
            final SystemDialog dialog = new SystemDialog(this.context, this.theme);
            if (this.systemType) {
                dialog.getWindow().setType(2003);
                dialog.getWindow().requestFeature(11);
            }
            this.scrollLayout = (LinearLayout) inflater.inflate(C1070R.layout.system_dialog, null);
            this.scrollView = new HmKeyEventScrollView(this.context);
            this.scrollView.setBackgroundColor(-16777216);
            this.scrollView.setOnKeyCallBackListener(this);
            ArcScrollbarHelper.setArcScrollBarDrawable(this.scrollView);
            this.buttonPositiveButton = (HmHaloButton) this.scrollLayout.findViewById(C1070R.id.positiveButton);
            this.buttonNegativeButton = (HmHaloButton) this.scrollLayout.findViewById(C1070R.id.negativeButton);
            if (this.negativeDrawable != null) {
                this.buttonNegativeButton.setBackground(this.negativeDrawable);
            }
            if (this.positiveDrawable != null) {
                this.buttonPositiveButton.setBackground(this.positiveDrawable);
            }
            LinearLayout footLayout = (LinearLayout) this.scrollLayout.findViewById(C1070R.id.footer);
            dialog.addContentView(this.scrollLayout, new LayoutParams(-1, -1));
            this.buttonPositiveButton.setContentRatio(0.8f);
            this.buttonPositiveButton.setOverlayParent(footLayout);
            this.buttonNegativeButton.setContentRatio(0.8f);
            this.buttonNegativeButton.setOverlayParent(footLayout);
            this.buttonNegativeButton.setWithHalo(false);
            this.buttonPositiveButton.setWithHalo(false);
            if (this.titleTextSize != -1.0f) {
                ((TextView) this.scrollLayout.findViewById(C1070R.id.title)).setTextSize(this.titleTextSize);
            }
            if (this.titleTextColor != -1) {
                ((TextView) this.scrollLayout.findViewById(C1070R.id.title)).setTextColor(this.titleTextColor);
            }
            if (TextUtils.isEmpty(this.title)) {
                this.scrollLayout.findViewById(C1070R.id.title).setVisibility(8);
                this.scrollLayout.findViewById(C1070R.id.title_empty).setVisibility(0);
            } else {
                this.scrollLayout.findViewById(C1070R.id.title).setVisibility(0);
                this.scrollLayout.findViewById(C1070R.id.title_empty).setVisibility(8);
                ((TextView) this.scrollLayout.findViewById(C1070R.id.title)).setText(this.title);
            }
            if (this.positiveButtonText != null) {
                ((HmHaloButton) this.scrollLayout.findViewById(C1070R.id.positiveButton)).setText(this.positiveButtonText);
                if (this.positiveButtonClickListener != null) {
                    ((HmHaloButton) this.scrollLayout.findViewById(C1070R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.positiveButtonClickListener.onClick(dialog, -1);
                        }
                    });
                }
            } else {
                this.scrollLayout.findViewById(C1070R.id.positiveButton).setVisibility(8);
            }
            if (this.negativeButtonText != null) {
                ((HmHaloButton) this.scrollLayout.findViewById(C1070R.id.negativeButton)).setText(this.negativeButtonText);
                if (this.positiveButtonText == null) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(102, 102);
                    params.setMargins(0, 40, 0, 0);
                    footLayout.removeView(this.buttonPositiveButton);
                    footLayout.removeView(this.buttonNegativeButton);
                    footLayout.addView(this.buttonNegativeButton, params);
                }
                if (this.negativeButtonClickListener != null) {
                    ((HmHaloButton) this.scrollLayout.findViewById(C1070R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.negativeButtonClickListener.onClick(dialog, -2);
                        }
                    });
                }
                this.scrollLayout.findViewById(C1070R.id.button_empty).setVisibility(8);
            } else {
                new LinearLayout.LayoutParams(-2, -2).setMargins(0, 0, 0, 0);
                HmHaloButton button = (HmHaloButton) this.scrollLayout.findViewById(C1070R.id.positiveButton);
                ArrayList<View> list = new ArrayList();
                list.add(button);
                this.scrollLayout.findViewById(C1070R.id.footer).addChildrenForAccessibility(list);
                this.scrollLayout.findViewById(C1070R.id.negativeButton).setVisibility(8);
                this.scrollLayout.findViewById(C1070R.id.button_empty).setVisibility(0);
            }
            if (!TextUtils.isEmpty(this.message)) {
                if (this.messageTextSize != -1.0f) {
                    ((TextView) this.scrollLayout.findViewById(C1070R.id.message)).setTextSize(this.messageTextSize);
                }
                if (this.messageTextColor != -1) {
                    ((TextView) this.scrollLayout.findViewById(C1070R.id.message)).setTextColor(this.messageTextColor);
                }
                ((TextView) this.scrollLayout.findViewById(C1070R.id.message)).setText(this.message);
            } else if (this.contentView != null) {
                ((LinearLayout) this.scrollLayout.findViewById(C1070R.id.content)).removeAllViews();
                ((LinearLayout) this.scrollLayout.findViewById(C1070R.id.content)).addView(this.contentView, new LayoutParams(-1, -1));
            }
            if (this.mOnKeyListener != null) {
                dialog.setOnKeyListener(this.mOnKeyListener);
            }
            if (this.mOnDismissListener != null) {
                dialog.setOnDismissListener(this.mOnDismissListener);
            }
            if (this.mOnCancelListener != null) {
                dialog.setOnCancelListener(this.mOnCancelListener);
            }
            ((ViewGroup) this.scrollLayout.getParent()).removeAllViews();
            this.scrollView.addView(this.scrollLayout);
            dialog.setContentView(this.scrollView);
            SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(dialog.getWindow(), this.scrollLayout);
            return dialog;
        }
    }

    public interface DismissListener {
        void dismiss();
    }

    public SystemDialog(Context context, int theme) {
        super(context, theme);
    }

    public void dismiss() {
        super.dismiss();
        if (this.dismissListener != null) {
            this.dismissListener.dismiss();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        this.keyEventDispatcher.dispatchKeyevent((Dialog) this, event);
        return super.dispatchKeyEvent(event);
    }
}
