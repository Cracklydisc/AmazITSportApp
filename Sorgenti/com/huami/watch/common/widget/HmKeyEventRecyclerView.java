package com.huami.watch.common.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.utils.Utils;

public class HmKeyEventRecyclerView extends RecyclerView implements EventCallBack, KeyeventConsumer {
    private boolean canAccept = true;
    private int focusStart = 1;
    private int focused = 0;
    private int footerCount = 0;
    private boolean hasFocused = true;
    private int headerCount = 0;
    private boolean isModeEverestReal = ModelUtil.isRealModelEverest(null);
    private OnRecyclerItemKeyClickListener onRecyclerItemKeyClickListener;
    private Paint paint;
    private KeyeventProcessor processor;

    class C04421 extends OnScrollListener {
        C04421() {
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            HmKeyEventRecyclerView.this.focused = (int) Math.floor(((double) ((HmKeyEventRecyclerView.this.getLastVisiblePosition() - HmKeyEventRecyclerView.this.getFirstVisiblePosition()) + 1)) / 2.0d);
            if (HmKeyEventRecyclerView.this.focused >= HmKeyEventRecyclerView.this.focusStart || HmKeyEventRecyclerView.this.getChildAdapterPosition(HmKeyEventRecyclerView.this.getChildAt(HmKeyEventRecyclerView.this.focused)) < 0) {
                HmKeyEventRecyclerView.this.focused = HmKeyEventRecyclerView.this.focusStart;
            }
        }
    }

    public interface OnRecyclerItemKeyClickListener {
        void onItemChecked(View view, int i);

        void onTopCheckedUp();
    }

    public HmKeyEventRecyclerView(Context context) {
        super(context);
        init();
    }

    public HmKeyEventRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HmKeyEventRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setOnScrollListener(new C04421());
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    private int getLastVisiblePosition() {
        return ((LinearLayoutManager) getLayoutManager()).findLastVisibleItemPosition();
    }

    private int getFirstVisiblePosition() {
        return ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
    }

    private int getHeaderViewsCount() {
        return 0;
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        try {
            float scroll;
            if (theKey == HMKeyEvent.KEY_DOWN) {
                if (getLastVisiblePosition() < getHeaderViewsCount() || getFirstVisiblePosition() == getLastVisiblePosition()) {
                    smoothScrollBy(0, Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()));
                } else {
                    View lastView = getChildAt(((int) Math.floor(((double) (getLastVisiblePosition() - getFirstVisiblePosition())) / 2.0d)) + 1);
                    scroll = (float) ((((double) lastView.getBottom()) - (((double) lastView.getHeight()) / 2.0d)) - (((double) ((float) ((Activity) getContext()).getWindow().getDecorView().getHeight())) / 2.0d));
                    boolean toBottom = getChildAdapterPosition(getChildAt(this.focused + 1)) >= getAdapter().getItemCount() + -1;
                    if (scroll <= ((float) getChildAt(this.focused).getMeasuredHeight()) && toBottom) {
                        this.focused++;
                    }
                    smoothScrollBy(0, (int) scroll);
                }
                return true;
            } else if (theKey == HMKeyEvent.KEY_UP) {
                boolean isTopUp = false;
                if (getFirstVisiblePosition() < getHeaderViewsCount() || getFirstVisiblePosition() == getLastVisiblePosition()) {
                    smoothScrollBy(0, -Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()));
                } else {
                    boolean isLastHeightThanBottom;
                    int firstIndex = ((int) Math.ceil(((double) (getLastVisiblePosition() - getFirstVisiblePosition())) / 2.0d)) - 1;
                    View firstView = getChildAt(firstIndex);
                    float windowHeight = (float) ((Activity) getContext()).getWindow().getDecorView().getHeight();
                    scroll = (float) ((((double) windowHeight) / 2.0d) - (((double) firstView.getBottom()) - (((double) firstView.getHeight()) / 2.0d)));
                    boolean toTop = getChildAdapterPosition(getChildAt(this.focused)) > this.focusStart;
                    if (this.onRecyclerItemKeyClickListener != null) {
                        if (getChildAdapterPosition(getChildAt(0)) == 0 && getChildAt(0).getTop() == 0 && this.focused == this.focusStart) {
                            isTopUp = true;
                            isLastHeightThanBottom = getChildAt(getLastVisiblePosition()) == null && ((float) getChildAt(getLastVisiblePosition()).getBottom()) <= windowHeight;
                            if (getChildAdapterPosition(getChildAt(this.focused)) >= 0) {
                                this.focused = this.focusStart;
                            } else {
                                if ((scroll <= ((float) getChildAt(this.focused).getMeasuredHeight()) || isLastHeightThanBottom) && toTop) {
                                    this.focused--;
                                }
                            }
                            if (Math.abs(this.focused - ((getChildAdapterPosition(getChildAt(0)) != 0 ? this.focusStart : 0) + firstIndex)) > 1) {
                                scroll = 1.0f;
                            }
                            smoothScrollBy(0, (int) (-scroll));
                        }
                    }
                    isTopUp = false;
                    if (getChildAt(getLastVisiblePosition()) == null) {
                    }
                    if (getChildAdapterPosition(getChildAt(this.focused)) >= 0) {
                        this.focused--;
                    } else {
                        this.focused = this.focusStart;
                    }
                    if (getChildAdapterPosition(getChildAt(0)) != 0) {
                    }
                    if (Math.abs(this.focused - ((getChildAdapterPosition(getChildAt(0)) != 0 ? this.focusStart : 0) + firstIndex)) > 1) {
                        scroll = 1.0f;
                    }
                    smoothScrollBy(0, (int) (-scroll));
                }
                if (isTopUp) {
                    this.onRecyclerItemKeyClickListener.onTopCheckedUp();
                }
                return true;
            } else {
                if (theKey == HMKeyEvent.KEY_CENTER) {
                    if (!(getChildAt(this.focused).performClick() || this.onRecyclerItemKeyClickListener == null)) {
                        View child = getChildAt(this.focused);
                        this.onRecyclerItemKeyClickListener.onItemChecked(child, getChildAdapterPosition(child));
                    }
                }
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public OnRecyclerItemKeyClickListener getOnRecyclerItemKeyClickListener() {
        return this.onRecyclerItemKeyClickListener;
    }

    public void setOnRecyclerItemKeyClickListener(OnRecyclerItemKeyClickListener onRecyclerItemKeyClickListener) {
        this.onRecyclerItemKeyClickListener = onRecyclerItemKeyClickListener;
    }

    public void setHasFocused(boolean hasFocused) {
        this.hasFocused = hasFocused;
    }

    public void setFocusStart(int focusStart) {
        this.focusStart = focusStart;
        this.focused = focusStart;
    }

    public void setFooterCount(int footerCount) {
        this.footerCount = footerCount;
    }

    public void setHeaderCount(int headerCount) {
        this.headerCount = headerCount;
    }

    protected void dispatchDraw(Canvas canvas) {
        if (this.hasFocused && this.isModeEverestReal) {
            try {
                if (getChildCount() - 1 <= this.focusStart) {
                    this.focused = this.focusStart;
                }
                int position = getChildPosition(getChildAt(this.focused));
                if (position >= getHeaderViewsCount() && position < getAdapter().getItemCount()) {
                    View view = getChildAt(this.focused);
                    if (this.paint == null) {
                        this.paint = new Paint();
                        this.paint.setColor(Color.parseColor("#2D313A"));
                        this.paint.setStyle(Style.FILL);
                    }
                    canvas.drawRect(0.0f, (float) view.getTop(), (float) getResources().getDisplayMetrics().widthPixels, (float) view.getBottom(), this.paint);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.dispatchDraw(canvas);
    }
}
