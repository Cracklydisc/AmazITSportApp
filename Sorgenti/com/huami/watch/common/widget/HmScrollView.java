package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class HmScrollView extends ScrollView {
    public HmScrollView(Context context) {
        super(context);
    }

    public HmScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
