package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Region.Op;
import android.graphics.Shader.TileMode;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.huami.watch.utils.CubicInterpolator;
import java.util.ArrayList;
import java.util.Stack;

public class HmEcgProgressBar extends View {
    private static final PorterDuffXfermode CLEAR = new PorterDuffXfermode(Mode.CLEAR);
    private int ONE_FRAME;
    private float cos;
    private int delayOffset;
    private int mAccelerate;
    private BitmapShader mBitmapShader;
    private Canvas mCanvas;
    private Bitmap mCanvasBitmap;
    private int mDelayTime;
    private float mHalf;
    private Handler mHandler;
    private ValueAnimator mHideAnim;
    private boolean mIsCompleteMoment;
    private float mLineLengthFinal;
    private float mLineLengthMax;
    private Paint mPaint;
    private Paint mPaintBitmap;
    private Paint mPaintEraser;
    private Path mPathViewInner;
    private Path mPathViewOuter;
    private int mPerProgressDelay;
    private int mProgress;
    private AnimatorFactory mProgressAnimFactory;
    private float[] mProgressLinesLength;
    private float[] mProgressLinesPoints;
    private boolean mProgressStarted;
    private boolean mProgressStopping;
    private int mRangeMax;
    private int mRangeMin;
    private int mStrokeWidth;
    private long mTimeOutTime;
    private Runnable onAnimEndCallback;
    private Runnable onAnimTimeOutRunnable;
    private float sin;

    class C04061 extends Handler {
        C04061() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 101:
                    final int progress = HmEcgProgressBar.this.mProgress;
                    if (progress < 102) {
                        HmEcgProgressBar.this.mProgress = HmEcgProgressBar.this.mProgress + 1;
                        final ValueAnimator anim = HmEcgProgressBar.this.mProgressAnimFactory.getAnimator();
                        anim.addUpdateListener(new AnimatorUpdateListener() {
                            public void onAnimationUpdate(ValueAnimator animation) {
                                HmEcgProgressBar.this.mProgressLinesLength[progress] = ((Float) animation.getAnimatedValue()).floatValue();
                                HmEcgProgressBar.this.invalateRange(progress);
                            }
                        });
                        if (progress == 101) {
                            anim.addListener(new AnimatorListenerAdapter() {
                                public void onAnimationEnd(Animator animation) {
                                    anim.removeListener(this);
                                    HmEcgProgressBar.this.startHideProgressAnim();
                                }
                            });
                        }
                        anim.start();
                        if (HmEcgProgressBar.this.mProgressStopping) {
                            if (HmEcgProgressBar.this.mIsCompleteMoment) {
                                HmEcgProgressBar.this.delayOffset = HmEcgProgressBar.this.mPerProgressDelay;
                            } else if (HmEcgProgressBar.this.delayOffset < HmEcgProgressBar.this.mPerProgressDelay) {
                                HmEcgProgressBar.access$712(HmEcgProgressBar.this, HmEcgProgressBar.this.mAccelerate);
                            } else {
                                HmEcgProgressBar.this.delayOffset = HmEcgProgressBar.this.mPerProgressDelay;
                            }
                        } else if (HmEcgProgressBar.this.mProgress >= 72) {
                            HmEcgProgressBar.this.delayOffset = (72 - HmEcgProgressBar.this.mProgress) * HmEcgProgressBar.this.mDelayTime;
                        } else {
                            HmEcgProgressBar.this.delayOffset = 0;
                        }
                        sendEmptyMessageDelayed(101, (long) (HmEcgProgressBar.this.mPerProgressDelay - HmEcgProgressBar.this.delayOffset));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    class C04072 implements AnimatorUpdateListener {
        C04072() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmEcgProgressBar.this.mPathViewInner.rewind();
            HmEcgProgressBar.this.mPathViewInner.addCircle(HmEcgProgressBar.this.mHalf, HmEcgProgressBar.this.mHalf, HmEcgProgressBar.this.mHalf - (HmEcgProgressBar.this.mLineLengthFinal * (1.0f - ((Float) animation.getAnimatedValue()).floatValue())), Direction.CCW);
            HmEcgProgressBar.this.invalidate();
        }
    }

    class C04083 extends AnimatorListenerAdapter {
        C04083() {
        }

        public void onAnimationEnd(Animator animation) {
            HmEcgProgressBar.this.onProgressAnimatorEnd();
        }
    }

    interface AnimatorFactory {
        void clearAllAnimator();

        ValueAnimator getAnimator();

        void setInterpolator(TimeInterpolator timeInterpolator);
    }

    class C04104 implements AnimatorFactory {
        private TimeInterpolator mInterpolator = new LinearInterpolator();
        private Stack<ValueAnimator> mPoll = new Stack();
        private ArrayList<ValueAnimator> mRunning = new ArrayList();

        C04104() {
        }

        public ValueAnimator getAnimator() {
            if (!this.mPoll.isEmpty()) {
                return (ValueAnimator) this.mPoll.pop();
            }
            final ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            anim.setInterpolator(this.mInterpolator);
            anim.setDuration(800);
            anim.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animation) {
                    C04104.this.mRunning.add(anim);
                }

                public void onAnimationEnd(Animator animation) {
                    anim.removeAllUpdateListeners();
                    C04104.this.mRunning.remove(anim);
                    C04104.this.mPoll.push(anim);
                }
            });
            return anim;
        }

        public void clearAllAnimator() {
            for (int i = this.mRunning.size() - 1; i >= 0; i--) {
                ValueAnimator anim = (ValueAnimator) this.mRunning.remove(i);
                if (anim.isStarted()) {
                    anim.cancel();
                }
            }
            this.mPoll.clear();
        }

        public void setInterpolator(TimeInterpolator interpolator) {
            this.mInterpolator = interpolator;
        }
    }

    private class ProgressInterpolator implements TimeInterpolator {
        private float deltaOut;
        private float endIn;
        private TimeInterpolator interpolatorIn;
        private TimeInterpolator interpolatorOut;
        private float turning;
        private float turningLeft;

        public ProgressInterpolator(TimeInterpolator interpolatorIn, int durationIn, float endIn, TimeInterpolator interpolatorOut, int durationOut, float endOut) {
            if (interpolatorIn == null) {
                interpolatorIn = new LinearInterpolator();
            }
            this.interpolatorIn = interpolatorIn;
            if (interpolatorOut == null) {
                interpolatorOut = new LinearInterpolator();
            }
            this.interpolatorOut = interpolatorOut;
            this.turning = (((float) durationIn) * 1.0f) / ((float) (durationIn + durationOut));
            this.turningLeft = 1.0f - this.turning;
            this.endIn = endIn;
            this.deltaOut = endOut - endIn;
        }

        public float getInterpolation(float input) {
            if (input <= this.turning) {
                return this.interpolatorIn.getInterpolation(input / this.turning) * this.endIn;
            }
            return (this.interpolatorOut.getInterpolation((input - this.turning) / this.turningLeft) * this.deltaOut) + this.endIn;
        }
    }

    static /* synthetic */ int access$712(HmEcgProgressBar x0, int x1) {
        int i = x0.delayOffset + x1;
        x0.delayOffset = i;
        return i;
    }

    public HmEcgProgressBar(Context context) {
        this(context, null);
    }

    public HmEcgProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HmEcgProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public HmEcgProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mProgress = 0;
        this.delayOffset = 0;
        this.mDelayTime = 15;
        this.mAccelerate = 10;
        this.ONE_FRAME = 16;
        this.mPerProgressDelay = 125;
        this.mTimeOutTime = 19725;
        this.mIsCompleteMoment = false;
        this.mHandler = new C04061();
        this.mProgressAnimFactory = new C04104();
        this.mProgressStarted = false;
        this.mProgressStopping = false;
        this.onAnimTimeOutRunnable = null;
        float density = getResources().getDisplayMetrics().density;
        this.mLineLengthFinal = 12.0f * density;
        this.mLineLengthMax = 27.0f * density;
        this.mStrokeWidth = Math.round(2.0f * density);
        this.mRangeMin = 0;
        this.mRangeMax = 0;
        this.mProgressAnimFactory.setInterpolator(new ProgressInterpolator(new CubicInterpolator((byte) 0), 500, this.mLineLengthMax, new CubicInterpolator((byte) 1), 300, this.mLineLengthFinal));
        this.mPaintBitmap = new Paint(1);
        this.mPaintBitmap.setStyle(Style.STROKE);
        this.mPaintBitmap.setStrokeCap(Cap.ROUND);
        this.mPaintBitmap.setStrokeWidth((float) this.mStrokeWidth);
        this.mPaintBitmap.setColor(-65536);
        this.mPaintEraser = new Paint(1);
        this.mPaintEraser.setStyle(Style.STROKE);
        this.mPaintEraser.setStrokeCap(Cap.ROUND);
        this.mPaintEraser.setStrokeWidth(((float) this.mStrokeWidth) * 1.5f);
        this.mPaintEraser.setXfermode(CLEAR);
        this.mProgressLinesLength = new float[102];
        this.mProgressLinesPoints = new float[6];
        this.mCanvas = new Canvas();
        this.mPaint = new Paint(7);
        resetProgressLinesLengths();
        this.mPathViewInner = new Path();
        this.mPathViewOuter = new Path();
        setRadius(160);
    }

    public void setRadius(int radius) {
        this.mHalf = (float) radius;
        int width = radius << 1;
        if (!(this.mCanvasBitmap == null || this.mCanvasBitmap.getWidth() == width)) {
            this.mPaint.setShader(null);
            this.mBitmapShader = null;
            this.mCanvas.setBitmap(null);
            this.mCanvasBitmap.recycle();
            this.mCanvasBitmap = null;
        }
        if (this.mCanvasBitmap == null) {
            this.mCanvasBitmap = Bitmap.createBitmap(width, width, Config.ARGB_4444);
            this.mCanvas.setBitmap(this.mCanvasBitmap);
            this.mBitmapShader = new BitmapShader(this.mCanvasBitmap, TileMode.CLAMP, TileMode.CLAMP);
            this.mPaint.setShader(this.mBitmapShader);
        }
        this.mPathViewInner.rewind();
        this.mPathViewInner.addCircle(this.mHalf, this.mHalf, this.mHalf - this.mLineLengthMax, Direction.CCW);
        this.mPathViewOuter.rewind();
        this.mPathViewOuter.addCircle(this.mHalf, this.mHalf, this.mHalf, Direction.CCW);
    }

    public void setTimeOutTime(long timeOutTime, boolean momentCompleted) {
        this.mTimeOutTime = timeOutTime;
        float scale = ((float) (this.mTimeOutTime - 400)) / 19725.0f;
        this.mDelayTime = (int) (((float) this.mDelayTime) * scale);
        this.mAccelerate = (int) (((float) this.mAccelerate) * scale);
        this.mPerProgressDelay = (int) (((float) this.mPerProgressDelay) * scale);
        this.mIsCompleteMoment = momentCompleted;
        Log.i("DemoView", "delay:" + this.mDelayTime + ", acceletime:" + this.mAccelerate + ",  perProgress:" + this.mPerProgressDelay + ", scale:" + scale);
    }

    public void setLineColor(int color) {
        if (this.mPaintBitmap != null) {
            this.mPaintBitmap.setColor(color);
        }
    }

    public void setLineLength(int finalLength, int maxLength) {
        this.mLineLengthFinal = (float) finalLength;
        this.mLineLengthMax = (float) maxLength;
        this.mProgressAnimFactory.setInterpolator(new ProgressInterpolator(new CubicInterpolator((byte) 0), 500, this.mLineLengthMax, new CubicInterpolator((byte) 1), 300, this.mLineLengthFinal));
    }

    protected void onDraw(Canvas canvas) {
        updateBitmap();
        canvas.save(2);
        canvas.clipPath(this.mPathViewOuter);
        canvas.clipPath(this.mPathViewInner, Op.DIFFERENCE);
        canvas.drawPath(this.mPathViewOuter, this.mPaint);
        canvas.restore();
    }

    private void updateBitmap() {
        int i = this.mRangeMin;
        while (i < this.mRangeMax) {
            float length = this.mProgressLinesLength[i];
            if (length >= 0.0f) {
                getLine(241.5f - (3.0f * ((float) i)), length);
                this.mCanvas.drawLine(this.mProgressLinesPoints[0], this.mProgressLinesPoints[1], this.mProgressLinesPoints[4], this.mProgressLinesPoints[5], this.mPaintEraser);
                this.mCanvas.drawLine(this.mProgressLinesPoints[0], this.mProgressLinesPoints[1], this.mProgressLinesPoints[2], this.mProgressLinesPoints[3], this.mPaintBitmap);
                i++;
            } else {
                return;
            }
        }
        this.mRangeMin = 102;
        this.mRangeMax = 0;
    }

    private void clearBitmap() {
        if (this.mCanvasBitmap != null && !this.mCanvasBitmap.isRecycled()) {
            this.mCanvasBitmap.eraseColor(0);
        }
    }

    private void invalateRange(int index) {
        this.mRangeMin = Math.min(this.mRangeMin, index);
        this.mRangeMax = Math.max(this.mRangeMax, index + 1);
        invalidate();
    }

    private void resetProgressLinesLengths() {
        for (int i = 0; i < this.mProgressLinesLength.length; i++) {
            this.mProgressLinesLength[i] = -1.0f;
        }
    }

    private void getLine(float degree, float length) {
        this.sin = (float) Math.sin((double) (degree * 0.017453292f));
        this.cos = (float) Math.cos((double) (degree * 0.017453292f));
        this.mProgressLinesPoints[0] = this.mHalf + (this.cos * this.mHalf);
        this.mProgressLinesPoints[1] = this.mHalf - (this.sin * this.mHalf);
        this.mProgressLinesPoints[2] = this.mHalf + (this.cos * (this.mHalf - length));
        this.mProgressLinesPoints[3] = this.mHalf - (this.sin * (this.mHalf - length));
        this.mProgressLinesPoints[4] = this.mHalf + (this.cos * (this.mHalf - this.mLineLengthMax));
        this.mProgressLinesPoints[5] = this.mHalf - (this.sin * (this.mHalf - this.mLineLengthMax));
    }

    private void startHideProgressAnim() {
        this.mHideAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.mHideAnim.setDuration(333);
        this.mHideAnim.setInterpolator(new LinearInterpolator());
        this.mHideAnim.addUpdateListener(new C04072());
        this.mHideAnim.addListener(new C04083());
        this.mHideAnim.start();
    }

    public void startProgressAnimation(Runnable endRunnable) {
        if (!this.mProgressStarted) {
            this.onAnimEndCallback = null;
            this.onAnimTimeOutRunnable = endRunnable;
            this.mHandler.sendEmptyMessage(101);
            this.mProgressStarted = true;
        }
    }

    public void stopProgressAnimation(Runnable callback) {
        if (this.mProgressStarted) {
            this.mProgressStopping = true;
            this.mHandler.removeMessages(101);
            this.mHandler.sendEmptyMessage(101);
            this.delayOffset = 0;
            this.onAnimEndCallback = callback;
            this.onAnimTimeOutRunnable = null;
        } else if (callback != null) {
            callback.run();
        }
    }

    private void onProgressAnimatorEnd() {
        if (this.onAnimEndCallback != null) {
            this.onAnimEndCallback.run();
            this.onAnimEndCallback = null;
        }
        if (this.onAnimTimeOutRunnable != null) {
            this.onAnimTimeOutRunnable.run();
            this.onAnimTimeOutRunnable = null;
        }
        resetProgressAnimation();
    }

    public void resetProgressAnimation() {
        this.mHandler.removeMessages(101);
        this.mProgressAnimFactory.clearAllAnimator();
        this.mProgressStarted = false;
        this.mProgressStopping = false;
        resetProgressLinesLengths();
        clearBitmap();
        this.mProgress = 0;
        this.mDelayTime = 15;
        this.mAccelerate = 10;
        this.mPerProgressDelay = 125;
        this.mRangeMin = 102;
        this.mRangeMax = 0;
        this.mPathViewInner.rewind();
        this.mPathViewInner.addCircle(this.mHalf, this.mHalf, this.mHalf - this.mLineLengthMax, Direction.CCW);
        invalidate();
    }
}
