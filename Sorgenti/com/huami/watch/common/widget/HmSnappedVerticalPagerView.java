package com.huami.watch.common.widget;

import android.graphics.Rect;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class HmSnappedVerticalPagerView extends ListView {
    private static float FACTOR = 0.146467f;
    private int mActivePointerId;
    private ListAdapter mAdapter;
    private int mChildHeight;
    private int mChildWidth;
    private boolean mScrolling;
    private float mTouchY;

    class C04571 implements OnApplyWindowInsetsListener {
        public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
            return insets;
        }
    }

    class C04602 implements OnScrollListener {
        private float mStartTouchY;
        final /* synthetic */ HmSnappedVerticalPagerView this$0;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (this.this$0.mAdapter != null) {
                final ListView listView = (ListView) view;
                int position;
                int nextPosition;
                switch (scrollState) {
                    case 0:
                        if (this.this$0.mScrolling) {
                            this.this$0.mScrolling = false;
                            position = listView.getFirstVisiblePosition();
                            View item = listView.getChildAt(0);
                            if (Math.abs(item.getTop()) > Math.abs(item.getBottom())) {
                                nextPosition = position + 1;
                            } else {
                                nextPosition = position;
                            }
                            if (nextPosition >= 0 && nextPosition < this.this$0.mAdapter.getCount()) {
                                listView.post(new Runnable() {
                                    public void run() {
                                        listView.smoothScrollToPositionFromTop(nextPosition, 0, 100);
                                    }
                                });
                                return;
                            }
                            return;
                        }
                        return;
                    case 1:
                        this.this$0.mScrolling = true;
                        this.mStartTouchY = this.this$0.mTouchY;
                        return;
                    case 2:
                        if (this.this$0.mScrolling) {
                            this.this$0.mScrolling = false;
                            position = listView.getFirstVisiblePosition();
                            nextPosition = this.this$0.mTouchY > this.mStartTouchY ? position : position + 1;
                            if (nextPosition >= 0 && nextPosition < this.this$0.mAdapter.getCount()) {
                                listView.post(new Runnable() {
                                    public void run() {
                                        listView.smoothScrollToPositionFromTop(nextPosition, 0, 100);
                                    }
                                });
                                return;
                            }
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        }
    }

    public class VerticalSnapViewAdapter extends BaseAdapter {
        public VerticalSnapViewAdapter(ListAdapter adapter) {
            HmSnappedVerticalPagerView.this.mAdapter = adapter;
        }

        public int getCount() {
            if (HmSnappedVerticalPagerView.this.mAdapter == null) {
                return 0;
            }
            return HmSnappedVerticalPagerView.this.mAdapter.getCount();
        }

        public Object getItem(int position) {
            if (HmSnappedVerticalPagerView.this.mAdapter == null) {
                return null;
            }
            return HmSnappedVerticalPagerView.this.mAdapter.getItem(position);
        }

        public long getItemId(int position) {
            if (HmSnappedVerticalPagerView.this.mAdapter == null) {
                return 0;
            }
            return HmSnappedVerticalPagerView.this.mAdapter.getItemId(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (HmSnappedVerticalPagerView.this.mAdapter == null) {
                return null;
            }
            if (HmSnappedVerticalPagerView.this.mChildHeight <= 0) {
                HmSnappedVerticalPagerView.this.getWindowVisibleDisplayFrame(new Rect());
                HmSnappedVerticalPagerView.this.mChildHeight = ((parent.getBottom() - parent.getTop()) - parent.getPaddingTop()) - parent.getPaddingBottom();
                HmSnappedVerticalPagerView.this.mChildWidth = ((parent.getRight() - parent.getLeft()) - parent.getPaddingLeft()) - parent.getPaddingRight();
            }
            View view = HmSnappedVerticalPagerView.this.mAdapter.getView(position, convertView, parent);
            view.setLayoutParams(new LayoutParams(HmSnappedVerticalPagerView.this.mChildWidth, HmSnappedVerticalPagerView.this.mChildHeight));
            return view;
        }
    }

    public void setAdapter(ListAdapter adapter) {
        if (adapter != null) {
            this.mAdapter = adapter;
            super.setAdapter(new VerticalSnapViewAdapter(adapter));
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.w("zftest", "width  : " + getMeasuredWidth() + ", height : " + getMeasuredHeight());
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getActionMasked() == 0) {
            this.mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
        }
        this.mTouchY = ev.getY(ev.findPointerIndex(this.mActivePointerId));
        return super.onTouchEvent(ev);
    }
}
