package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.SweepGradient;
import android.view.animation.LinearInterpolator;

public class HmHaloRelativeLayout extends HmRelativeLayout {
    private Matrix f12m;
    private ValueAnimator mAnim;
    private boolean mEnableHalo;
    private float mHaloCenterX;
    private float mHaloCenterY;
    private float mHaloRadius;
    private SweepGradient mShader;
    private Paint paint;

    class C04331 implements AnimatorUpdateListener {
        C04331() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmHaloRelativeLayout.this.f12m.setRotate(360.0f * ((Float) animation.getAnimatedValue()).floatValue(), HmHaloRelativeLayout.this.mHaloCenterX, HmHaloRelativeLayout.this.mHaloCenterY);
            HmHaloRelativeLayout.this.mShader.setLocalMatrix(HmHaloRelativeLayout.this.f12m);
            HmHaloRelativeLayout.this.paint.setShader(HmHaloRelativeLayout.this.mShader);
            HmHaloRelativeLayout.this.invalidate();
        }
    }

    class C04342 extends AnimatorListenerAdapter {
        boolean willNotDraw = false;

        C04342() {
        }

        public void onAnimationStart(Animator animation) {
            HmHaloRelativeLayout.this.mEnableHalo = true;
            this.willNotDraw = HmHaloRelativeLayout.this.willNotDraw();
            HmHaloRelativeLayout.this.setWillNotDraw(false);
        }

        public void onAnimationEnd(Animator animation) {
            HmHaloRelativeLayout.this.mEnableHalo = false;
            HmHaloRelativeLayout.this.setWillNotDraw(this.willNotDraw);
        }
    }

    class C04353 implements AnimatorUpdateListener {
        final /* synthetic */ HmHaloRelativeLayout this$0;
        final /* synthetic */ float val$endRadius;
        final /* synthetic */ float val$startRadius;

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            this.this$0.mHaloRadius = this.val$startRadius + ((this.val$endRadius - this.val$startRadius) * input);
            this.this$0.paint.setAlpha((int) ((1.0f - input) * 163.0f));
            this.this$0.invalidate();
        }
    }

    class C04364 extends AnimatorListenerAdapter {
        final /* synthetic */ HmHaloRelativeLayout this$0;
        final /* synthetic */ boolean val$loadingAfter;
        final /* synthetic */ float val$loadingRadius;
        boolean willNotDraw;

        public void onAnimationStart(Animator animation) {
            this.this$0.mEnableHalo = true;
            this.willNotDraw = this.this$0.willNotDraw();
            this.this$0.setWillNotDraw(false);
        }

        public void onAnimationEnd(Animator animation) {
            this.this$0.mEnableHalo = false;
            this.this$0.setWillNotDraw(this.willNotDraw);
            if (this.val$loadingAfter) {
                this.this$0.startLoadingAnimation(this.val$loadingRadius);
            }
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mEnableHalo) {
            canvas.drawCircle(this.mHaloCenterX, this.mHaloCenterY, this.mHaloRadius, this.paint);
        }
    }

    private void initLoadingPaint() {
        this.paint = new Paint(1);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeWidth(2.0f);
        this.f12m = new Matrix();
        this.mShader = new SweepGradient(this.mHaloCenterX, this.mHaloCenterY, new int[]{16777215, 16777215, -1}, null);
        this.paint.setShader(this.mShader);
    }

    public void startLoadingAnimation(float loadingRadius) {
        if (!this.mEnableHalo) {
            initLoadingPaint();
            this.mHaloRadius = loadingRadius;
            this.mEnableHalo = true;
            this.mAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mAnim.setInterpolator(new LinearInterpolator());
            this.mAnim.setDuration(1000);
            this.mAnim.setRepeatCount(-1);
            this.mAnim.addUpdateListener(new C04331());
            this.mAnim.addListener(new C04342());
            this.mAnim.start();
        }
    }
}
