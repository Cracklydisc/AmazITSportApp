package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.drawable.VectorDrawable;
import android.util.ArrayMap;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;

public class HmAnimatedVectorDrawable implements AnimatorUpdateListener {
    public Method getTargetByName = null;
    private Map<Animator, String> mAnimations = new ArrayMap();
    private AnimatorSet mAnimatorSet;
    private ArrayList<Animator> mAnimators = new ArrayList();
    private VectorDrawable mVectorDrawable;

    public HmAnimatedVectorDrawable(VectorDrawable vectorDrawable) {
        this.mVectorDrawable = vectorDrawable;
        setAllowCaching();
        initTargetClassMethod();
        this.mAnimatorSet = new AnimatorSet();
    }

    public void setupAnimatorsForTarget(String name, ValueAnimator animator) {
        if (this.getTargetByName != null) {
            try {
                Object target = this.getTargetByName.invoke(this.mVectorDrawable, new Object[]{name});
                Log.d("debug", "target  :  " + target + "  animator : " + animator);
                if (target != null) {
                    animator.setTarget(target);
                    if (this.mAnimators == null) {
                        this.mAnimators = new ArrayList();
                        this.mAnimations = new ArrayMap();
                    }
                    this.mAnimators.add(animator);
                    animator.addUpdateListener(this);
                    this.mAnimations.put(animator, name);
                }
            } catch (IllegalAccessException e) {
                Log.d("debug", "initTargetClassMethod  " + e.toString());
            } catch (IllegalArgumentException e2) {
                Log.d("debug", "initTargetClassMethod  " + e2.toString());
            } catch (InvocationTargetException e3) {
                Log.d("debug", "initTargetClassMethod  " + e3.toString());
            }
        }
    }

    public void setAllowCaching() {
        try {
            Method allowCaching = this.mVectorDrawable.getClass().getDeclaredMethod("setAllowCaching", new Class[]{Boolean.TYPE});
            allowCaching.setAccessible(true);
            if (allowCaching != null) {
                allowCaching.invoke(this.mVectorDrawable, new Object[]{Boolean.valueOf(false)});
            }
        } catch (NoSuchMethodException e) {
            Log.d("debug", "setAllowCaching  " + e.toString());
        } catch (IllegalAccessException e2) {
            Log.d("debug", "setAllowCaching  " + e2.toString());
        } catch (IllegalArgumentException e3) {
            Log.d("debug", "setAllowCaching  " + e3.toString());
        } catch (InvocationTargetException e4) {
            Log.d("debug", "setAllowCaching  " + e4.toString());
        }
    }

    private void initTargetClassMethod() {
        try {
            this.getTargetByName = this.mVectorDrawable.getClass().getDeclaredMethod("getTargetByName", new Class[]{String.class});
            this.getTargetByName.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void setCurrentPlayTime(float offset) {
        int size = this.mAnimators.size();
        for (int i = 0; i < size; i++) {
            ValueAnimator valueanimator = (ValueAnimator) this.mAnimators.get(i);
            valueanimator.setCurrentPlayTime((long) Math.round(((float) valueanimator.getDuration()) * offset));
        }
    }

    public void onAnimationUpdate(ValueAnimator animation) {
        this.mVectorDrawable.invalidateSelf();
    }
}
