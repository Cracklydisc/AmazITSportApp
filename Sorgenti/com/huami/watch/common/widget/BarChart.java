package com.huami.watch.common.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.util.Log;
import java.util.List;

public class BarChart<T extends Number> extends XAxisChart {
    private int LOCATION;
    private int defalutValHeight;
    private RectF[] defaultRects;
    private float gap;
    private boolean isDefaultVal;
    private ValueAnimator mAnimY;
    private float mBarWidth;
    private RectF mClipRect;
    private BarChartColorGenerator<T> mColorGenerator;
    private int mGridEndColor;
    private PointF[] mGridLocations;
    private int mGridStartColor;
    private boolean mIsShowShadowChart;
    private T mMinY;
    private Paint mShadowPaint;
    private RectF[] mShadowRectf;
    private List<T> mValues;
    private float mXAxisOffset;
    private RectF[] mYBarRects;
    private Paint f11p;
    private float radius;

    class C03991 implements AnimatorUpdateListener {
        final /* synthetic */ BarChart this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            this.this$0.x = (float) ((Integer) animation.getAnimatedValue()).intValue();
            this.this$0.invalidate();
        }
    }

    class C04002 implements AnimatorUpdateListener {
        C04002() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            BarChart.this.y = ((Float) animation.getAnimatedValue()).floatValue();
            BarChart.this.invalidate();
        }
    }

    public interface BarChartColorGenerator<V extends Number> {
        int generateColor(int i, V v);
    }

    private void calculateDrawingParams() {
        if (!this.mParamsInitialized) {
            float left = (float) getPaddingLeft();
            float top = (float) getPaddingTop();
            float width = ((float) (getWidth() - getPaddingRight())) - left;
            calculateXAxisParams(left, width, top, ((float) (getHeight() - getPaddingBottom())) - top);
            if (this.mValues.size() > 0) {
                float max = -3.4028235E38f;
                for (Number val : this.mValues) {
                    if (max < val.floatValue()) {
                        max = val.floatValue();
                    }
                }
                if (max <= 0.0f) {
                    max = 1.0f;
                }
                if (this.mMinY != null && max < this.mMinY.floatValue()) {
                    max = this.mMinY.floatValue();
                }
                if (this.mMaxXSize == 1.0f) {
                    this.mYBarRects[0].set(((width / 2.0f) + left) - (this.mBarWidth / 2.0f), top, ((width / 2.0f) + left) + (this.mBarWidth / 2.0f), this.mYMaxHeight);
                    if (this.isDefaultVal) {
                        this.defaultRects[0].set(((width / 2.0f) + left) - (this.mBarWidth / 2.0f), top, ((width / 2.0f) + left) + (this.mBarWidth / 2.0f), this.mYMaxHeight);
                    }
                } else if (this.mMaxXSize > 1.0f) {
                    int i;
                    float gap = (width - (this.mBarWidth * this.mMaxXSize)) / (this.mMaxXSize - 1.0f);
                    for (i = 0; i < this.mValues.size(); i++) {
                        this.mYBarRects[i].set(left + (((float) i) * (this.mBarWidth + gap)), ((1.0f - (((Number) this.mValues.get(i)).floatValue() / max)) * this.mYMaxHeight) + top, ((((float) i) * (this.mBarWidth + gap)) + left) + this.mBarWidth, this.mYMaxHeight);
                    }
                    if (this.isDefaultVal) {
                        for (i = 0; i < this.mValues.size(); i++) {
                            this.defaultRects[i].set((((float) i) * (this.mBarWidth + gap)) + left, (this.mYMaxHeight + top) - ((float) this.defalutValHeight), ((((float) i) * (this.mBarWidth + gap)) + left) + this.mBarWidth, this.mYMaxHeight);
                        }
                    }
                    if (this.mIsShowShadowChart) {
                        this.mShadowRectf = new RectF[((int) this.mMaxXSize)];
                        for (i = 0; ((float) i) < this.mMaxXSize; i++) {
                            this.mShadowRectf[i] = new RectF();
                            this.mShadowRectf[i].set((((float) i) * (this.mBarWidth + gap)) + left, top, ((((float) i) * (this.mBarWidth + gap)) + left) + this.mBarWidth, this.mYMaxHeight);
                        }
                    }
                }
                this.mClipRect.set(left, top, left + width, this.mYMaxHeight);
            }
            if (this.mAnimate) {
                this.x = 0.0f;
                this.y = 0.0f;
                animateY();
            } else {
                this.x = this.mMaxXSize;
                this.y = 1.0f;
            }
            this.mParamsInitialized = true;
        }
    }

    public void animateY() {
        if (this.mAnimY != null && this.mAnimY.isStarted()) {
            this.mAnimY.cancel();
        }
        this.x = this.mMaxXSize;
        this.mAnimY = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f}).setDuration(300);
        this.mAnimY.addUpdateListener(new C04002());
        this.mAnimY.start();
    }

    public void setData(T[] yValues, boolean animate) {
        if (!compare(yValues, this.mValues)) {
            this.mValues.clear();
            int i;
            if (yValues == null || yValues.length <= 0) {
                for (RectF empty : this.mYBarRects) {
                    empty.setEmpty();
                }
            } else {
                for (T val : yValues) {
                    this.mValues.add(val);
                }
                this.mYBarRects = new RectF[this.mValues.size()];
                for (i = 0; i < this.mYBarRects.length; i++) {
                    this.mYBarRects[i] = new RectF();
                }
                if (this.isDefaultVal) {
                    this.defaultRects = new RectF[this.mValues.size()];
                    for (i = 0; i < this.defaultRects.length; i++) {
                        this.defaultRects[i] = new RectF();
                    }
                }
            }
            this.mParamsInitialized = false;
            this.mAnimate = animate;
            invalidate();
        }
    }

    public void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel) {
        setData(xLabels, xRange, dontPaintLastXLabel, null);
    }

    public void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel, T[] yValues) {
        setData(xLabels, xRange, false, yValues, false);
    }

    public void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel, T[] yValues, boolean animate) {
        super.setData(xLabels, xRange, dontPaintLastXLabel);
        setData(yValues, animate);
    }

    public void onDraw(Canvas canvas) {
        calculateDrawingParams();
        super.onDraw(canvas);
        drawYValues(canvas);
    }

    private void drawYValues(Canvas canvas) {
        int i;
        if (this.mIsShowShadowChart) {
            for (RectF rec : this.mShadowRectf) {
                canvas.drawRect(rec, this.mShadowPaint);
            }
        }
        canvas.save(2);
        canvas.clipRect(this.mClipRect);
        if (this.isDefaultVal) {
            for (i = 0; ((float) i) < Math.min((float) this.mValues.size(), Math.min(this.x, this.mMaxXSize)); i++) {
                RectF r = this.defaultRects[i];
                if (this.mColorGenerator != null) {
                    this.f11p.setColor(this.mColorGenerator.generateColor(i, (Number) this.mValues.get(i)));
                }
                canvas.drawRoundRect(r.left, ((1.0f - this.y) * r.height()) + r.top, r.right, this.radius + r.bottom, this.radius, this.radius, this.f11p);
            }
        }
        for (i = 0; ((float) i) < Math.min((float) this.mValues.size(), Math.min(this.x, this.mMaxXSize)); i++) {
            r = this.mYBarRects[i];
            if (this.mColorGenerator != null) {
                this.f11p.setColor(this.mColorGenerator.generateColor(i, (Number) this.mValues.get(i)));
            }
            canvas.drawRoundRect(r.left, ((1.0f - this.y) * r.height()) + r.top, r.right, this.radius + r.bottom, this.radius, this.radius, this.f11p);
        }
        canvas.restore();
    }

    private final boolean compare(T[] aValues, List<T> bValues) {
        if (aValues == null || aValues.length != bValues.size()) {
            return false;
        }
        for (int i = 0; i < aValues.length; i++) {
            if (aValues[i] == null) {
                if (bValues.get(i) != null) {
                    return false;
                }
            } else if (bValues.get(i) == null) {
                return false;
            } else {
                if (!aValues[i].equals(bValues.get(i))) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setXAxisTextSize(float size) {
        super.setXAxisTextSize(size);
    }

    protected void calculateXAxisParams(float left, float width, float top, float height) {
        int labelSize = this.mXLabels.length;
        Rect rect = new Rect();
        this.mGridLocations = new PointF[labelSize];
        this.gap = (width - (this.mBarWidth * this.mMaxXSize)) / (this.mMaxXSize - 1.0f);
        int i = 0;
        while (i < labelSize) {
            switch (this.LOCATION) {
                case 1:
                    this.mGridLocations[i] = new PointF(left + (labelSize > 1 ? (((float) i) * width) / ((float) (labelSize - 1)) : 0.0f), top);
                    this.mXAxisLabelPaint.getTextBounds(this.mXLabels[i], 0, this.mXLabels[i].length(), rect);
                    this.mXLabelLocations[i] = new PointF(i == 0 ? this.mGridLocations[i].x - (((float) Math.abs(rect.right - rect.left)) / 2.0f) : (this.mGridLocations[i].x - (((float) Math.abs(rect.right - rect.left)) / 2.0f)) - 3.0f, ((top + height) - this.mXAxisLabelOffset) + this.mXAxisOffset);
                    Log.d("location", "xlabels location LEFT: " + this.mXLabelLocations[i].x);
                    break;
                case 2:
                    float x = ((((float) i) * (this.gap + this.mBarWidth)) + left) + (this.mBarWidth / 2.0f);
                    this.mGridLocations[i] = new PointF(x, top);
                    this.mXAxisLabelPaint.getTextBounds(this.mXLabels[i], 0, this.mXLabels[i].length(), rect);
                    this.mXLabelLocations[i] = new PointF((x - (((float) Math.abs(rect.right - rect.left)) / 2.0f)) - 2.0f, ((top + height) - this.mXAxisLabelOffset) + this.mXAxisOffset);
                    Log.d("location", "xlabels location CENTER: " + this.mXLabelLocations[i].x);
                    break;
                default:
                    break;
            }
            i++;
        }
        this.mYMaxHeight = height - (this.mDrawXAxis ? this.mXAxisLabelHeight + this.mXAxisLabelOffset : 0.0f);
        this.mGridPaint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, this.mYMaxHeight, this.mGridStartColor, this.mGridEndColor, TileMode.CLAMP));
    }

    protected void setxAxisOffset(float offset) {
        this.mXAxisOffset = offset;
    }
}
