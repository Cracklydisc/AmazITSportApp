package com.huami.watch.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v4.widget.ScrollerCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;

public class NumberView extends View {
    private static int DEFAULT_TEXT_SIZE_SELECTED_SP_DEFAULT = 30;
    private float currX = 0.0f;
    private float currY = 0.0f;
    private float dividerY0;
    private float dividerY1;
    private float downX = 0.0f;
    private float downY = 0.0f;
    private float downYGlobal = 0.0f;
    private boolean hasFocused = false;
    private boolean isSubtitle = false;
    private int lastX;
    private int lastY;
    private String mAlterHint;
    private CharSequence[] mAlterTextArrayWithMeasureHint;
    private CharSequence[] mAlterTextArrayWithoutMeasureHint;
    private int mCurrDrawFirstItemIndex = 0;
    private int mCurrDrawFirstItemY = 0;
    private int mCurrDrawGlobalY = 0;
    private boolean mCurrentItemIndexEffect = false;
    private String[] mDisplayedSubValues;
    private String[] mDisplayedValues;
    private int mDividerColor = -695533;
    private int mDividerHeight = 2;
    private int mDividerIndex0 = 0;
    private int mDividerIndex1 = 0;
    private int mDividerMarginL = 0;
    private int mDividerMarginR = 0;
    private String mEmptyItemHint;
    private boolean mFlagMayPress = false;
    private float mFriction = 1.0f;
    private Handler mHandlerInMainThread;
    private Handler mHandlerInNewThread;
    private HandlerThread mHandlerThread;
    private boolean mHasInit = false;
    private String mHintText;
    private int mInScrollingPickedNewValue;
    private int mInScrollingPickedOldValue;
    private int mItemHeight;
    private int mItemPaddingHorizontal = 0;
    private int mItemPaddingVertical = 0;
    private int mMarginEndOfHint = 0;
    private int mMarginStartOfHint = 0;
    private int mMaxHeightOfDisplayedValues = 0;
    private int mMaxShowIndex = -1;
    private int mMaxValue = 0;
    private int mMaxWidthOfAlterArrayWithMeasureHint = 0;
    private int mMaxWidthOfAlterArrayWithoutMeasureHint = 0;
    private int mMaxWidthOfDisplayedValues = 0;
    private int mMinShowIndex = -1;
    private int mMinValue = 0;
    private int mMiniVelocityFling = 150;
    private int mNotWrapLimitYBottom;
    private int mNotWrapLimitYTop;
    private OnScrollListener mOnScrollListener;
    private OnValueChangeListener mOnValueChangeListener;
    private OnValueChangeListenerInScrolling mOnValueChangeListenerInScrolling;
    private OnValueChangeListenerRelativeToRaw mOnValueChangeListenerRaw;
    private Paint mPaintDivider = new Paint();
    private Paint mPaintHint = new Paint();
    private TextPaint mPaintText = new TextPaint();
    private boolean mPendingWrapToLinear = false;
    private int mPrevPickedIndex = 0;
    private boolean mRespondChangeInMainThread = true;
    private boolean mRespondChangeOnDetach = false;
    private int mScaledTouchSlop = 8;
    private int mScrollState = 0;
    private ScrollerCompat mScroller;
    private int mShowCount = 3;
    private boolean mShowDivider = true;
    private int mSpecModeH = 0;
    private int mSpecModeW = 0;
    private int mTextColorHint = -1;
    private int mTextColorNormal = -6710887;
    private int mTextColorSelected = -1;
    private String mTextEllipsize;
    private int mTextSizeHint = 0;
    private float mTextSizeHintCenterYOffset = 0.0f;
    private int mTextSizeNormal = 0;
    private float mTextSizeNormalCenterYOffset = 0.0f;
    private int mTextSizeSelected = 0;
    private float mTextSizeSelectedCenterYOffset = 0.0f;
    private VelocityTracker mVelocityTracker;
    private float mViewCenterX;
    private int mViewHeight;
    private int mViewWidth;
    private int mWidthOfAlterHint = 0;
    private int mWidthOfHintText = 0;
    private boolean mWrapSelectorWheel = true;
    private boolean mWrapSelectorWheelCheck = true;
    private Paint paint;

    public interface OnValueChangeListener {
        void onValueChange(NumberView numberView, int i, int i2);
    }

    class C04872 extends Handler {
        C04872() {
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 2:
                    NumberView.this.respondPickedValueChanged(msg.arg1, msg.arg2, msg.obj);
                    return;
                case 3:
                    NumberView.this.requestLayout();
                    return;
                default:
                    return;
            }
        }
    }

    public interface OnScrollListener {
        void onScrollStateChange(NumberView numberView, int i);
    }

    public interface OnValueChangeListenerInScrolling {
        void onValueChangeInScrolling(NumberView numberView, int i, int i2);
    }

    public interface OnValueChangeListenerRelativeToRaw {
        void onValueChangeRelativeToRaw(NumberView numberView, int i, int i2, String[] strArr);
    }

    public NumberView(Context context) {
        super(context);
        init(context);
    }

    public NumberView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttr(context);
        init(context);
    }

    public NumberView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(context);
        init(context);
    }

    private void initAttr(Context context) {
        this.mItemPaddingHorizontal = dp2px(context, 5.0f);
        this.mItemPaddingVertical = dp2px(context, 2.0f);
        this.mRespondChangeInMainThread = false;
        this.mShowCount = 2;
        this.mShowDivider = false;
        this.mTextSizeNormal = sp2px(context, 17.0f);
        this.mTextSizeSelected = sp2px(context, 30.0f);
        this.mWrapSelectorWheel = true;
    }

    private void init(Context context) {
        this.mScroller = ScrollerCompat.create(context);
        this.mMiniVelocityFling = ViewConfiguration.get(getContext()).getScaledMinimumFlingVelocity();
        this.mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        if (this.mTextSizeNormal == 0) {
            this.mTextSizeNormal = sp2px(context, 17.0f);
        }
        if (this.mTextSizeSelected == 0) {
            this.mTextSizeSelected = sp2px(context, 30.0f);
        }
        if (this.mTextSizeHint == 0) {
            this.mTextSizeHint = sp2px(context, 14.0f);
        }
        if (this.mMarginStartOfHint == 0) {
            this.mMarginStartOfHint = dp2px(context, 8.0f);
        }
        if (this.mMarginEndOfHint == 0) {
            this.mMarginEndOfHint = dp2px(context, 8.0f);
        }
        this.mPaintDivider.setColor(this.mDividerColor);
        this.mPaintDivider.setAntiAlias(true);
        this.mPaintDivider.setStyle(Style.STROKE);
        this.mPaintDivider.setStrokeWidth((float) this.mDividerHeight);
        this.mPaintText.setColor(this.mTextColorNormal);
        this.mPaintText.setAntiAlias(true);
        this.mPaintText.setTextAlign(Align.CENTER);
        this.mPaintHint.setColor(this.mTextColorHint);
        this.mPaintHint.setAntiAlias(true);
        this.mPaintHint.setTextAlign(Align.CENTER);
        this.mPaintHint.setTextSize((float) this.mTextSizeHint);
        if (this.mShowCount % 2 == 0) {
            this.mShowCount++;
        }
        if (this.mMinShowIndex == -1 || this.mMaxShowIndex == -1) {
            updateValueForInit();
        }
        initHandler();
    }

    private void initHandler() {
        this.mHandlerThread = new HandlerThread("HandlerThread-For-Refreshing");
        this.mHandlerThread.start();
        this.mHandlerInNewThread = new Handler(this.mHandlerThread.getLooper()) {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (NumberView.this.mScroller.isFinished()) {
                            int willPickIndex;
                            int duration = 0;
                            if (NumberView.this.mCurrDrawFirstItemY != 0) {
                                if (NumberView.this.mScrollState == 0) {
                                    NumberView.this.onScrollStateChange(1);
                                }
                                if (NumberView.this.mCurrDrawFirstItemY < (-NumberView.this.mItemHeight) / 2) {
                                    duration = (int) ((((float) (NumberView.this.mItemHeight + NumberView.this.mCurrDrawFirstItemY)) * 300.0f) / ((float) NumberView.this.mItemHeight));
                                    int i = 0;
                                    NumberView.this.mScroller.startScroll(0, NumberView.this.mCurrDrawGlobalY, i, NumberView.this.mCurrDrawFirstItemY + NumberView.this.mItemHeight, duration * 3);
                                    willPickIndex = NumberView.this.getWillPickIndexByGlobalY((NumberView.this.mCurrDrawGlobalY + NumberView.this.mItemHeight) + NumberView.this.mCurrDrawFirstItemY);
                                } else {
                                    duration = (int) ((((float) (-NumberView.this.mCurrDrawFirstItemY)) * 300.0f) / ((float) NumberView.this.mItemHeight));
                                    NumberView.this.mScroller.startScroll(0, NumberView.this.mCurrDrawGlobalY, 0, NumberView.this.mCurrDrawFirstItemY, duration * 3);
                                    willPickIndex = NumberView.this.getWillPickIndexByGlobalY(NumberView.this.mCurrDrawGlobalY + NumberView.this.mCurrDrawFirstItemY);
                                }
                                NumberView.this.postInvalidate();
                            } else {
                                NumberView.this.onScrollStateChange(0);
                                willPickIndex = NumberView.this.getWillPickIndexByGlobalY(NumberView.this.mCurrDrawGlobalY);
                            }
                            Message changeMsg = NumberView.this.getMsg(2, NumberView.this.mPrevPickedIndex, willPickIndex, msg.obj);
                            if (NumberView.this.mRespondChangeInMainThread) {
                                NumberView.this.mHandlerInMainThread.sendMessageDelayed(changeMsg, (long) (duration * 2));
                                return;
                            } else {
                                NumberView.this.mHandlerInNewThread.sendMessageDelayed(changeMsg, (long) (duration * 2));
                                return;
                            }
                        }
                        if (NumberView.this.mScrollState == 0) {
                            NumberView.this.onScrollStateChange(1);
                        }
                        NumberView.this.mHandlerInNewThread.sendMessageDelayed(NumberView.this.getMsg(1, 0, 0, msg.obj), 32);
                        return;
                    case 2:
                        NumberView.this.respondPickedValueChanged(msg.arg1, msg.arg2, msg.obj);
                        return;
                    default:
                        return;
                }
            }
        };
        this.mHandlerInMainThread = new C04872();
    }

    private void respondPickedValueChangedInScrolling(int oldVal, int newVal) {
        this.mOnValueChangeListenerInScrolling.onValueChangeInScrolling(this, oldVal, newVal);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        updateMaxWHOfDisplayedValues(false);
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mViewWidth = w;
        this.mViewHeight = h;
        this.mItemHeight = this.mViewHeight / this.mShowCount;
        this.mViewCenterX = ((float) ((this.mViewWidth + getPaddingLeft()) - getPaddingRight())) / 2.0f;
        int defaultValue = 0;
        if (getOneRecycleSize() > 1) {
            if (this.mHasInit) {
                defaultValue = getValue() - this.mMinValue;
            } else if (this.mCurrentItemIndexEffect) {
                defaultValue = this.mCurrDrawFirstItemIndex + ((this.mShowCount - 1) / 2);
            } else {
                defaultValue = 0;
            }
        }
        boolean z = this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck;
        correctPositionByDefaultValue(defaultValue, z);
        updateFontAttr();
        updateNotWrapYLimit();
        updateDividerAttr();
        this.mHasInit = true;
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mHandlerThread == null || !this.mHandlerThread.isAlive()) {
            initHandler();
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mHandlerThread.quit();
        if (this.mItemHeight != 0) {
            if (!this.mScroller.isFinished()) {
                this.mScroller.abortAnimation();
                this.mCurrDrawGlobalY = this.mScroller.getCurrY();
                calculateFirstItemParameterByGlobalY();
                if (this.mCurrDrawFirstItemY != 0) {
                    if (this.mCurrDrawFirstItemY < (-this.mItemHeight) / 2) {
                        this.mCurrDrawGlobalY = (this.mCurrDrawGlobalY + this.mItemHeight) + this.mCurrDrawFirstItemY;
                    } else {
                        this.mCurrDrawGlobalY += this.mCurrDrawFirstItemY;
                    }
                    calculateFirstItemParameterByGlobalY();
                }
                onScrollStateChange(0);
            }
            int currPickedIndex = getWillPickIndexByGlobalY(this.mCurrDrawGlobalY);
            if (currPickedIndex != this.mPrevPickedIndex && this.mRespondChangeOnDetach) {
                try {
                    if (this.mOnValueChangeListener != null) {
                        this.mOnValueChangeListener.onValueChange(this, this.mPrevPickedIndex + this.mMinValue, this.mMinValue + currPickedIndex);
                    }
                    if (this.mOnValueChangeListenerRaw != null) {
                        this.mOnValueChangeListenerRaw.onValueChangeRelativeToRaw(this, this.mPrevPickedIndex, currPickedIndex, this.mDisplayedValues);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.mPrevPickedIndex = currPickedIndex;
        }
    }

    public int getOneRecycleSize() {
        return (this.mMaxShowIndex - this.mMinShowIndex) + 1;
    }

    public void setDisplayedValues(String[] newDisplayedValues, String[] displayedSubValues) {
        boolean z = true;
        stopRefreshing();
        stopScrolling();
        if (newDisplayedValues == null) {
            throw new IllegalArgumentException("newDisplayedValues should not be null.");
        } else if ((this.mMaxValue - this.mMinValue) + 1 > newDisplayedValues.length) {
            throw new IllegalArgumentException("mMaxValue - mMinValue + 1 should not be greater than mDisplayedValues.length, now ((mMaxValue - mMinValue + 1) is " + ((this.mMaxValue - this.mMinValue) + 1) + " newDisplayedValues.length is " + newDisplayedValues.length + ", you need to set MaxValue and MinValue before setDisplayedValues(String[])");
        } else {
            updateContent(newDisplayedValues, displayedSubValues);
            updateMaxWHOfDisplayedValues(true);
            this.mPrevPickedIndex = this.mMinShowIndex + 0;
            if (!(this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck)) {
                z = false;
            }
            correctPositionByDefaultValue(0, z);
            postInvalidate();
            this.mHandlerInMainThread.sendEmptyMessage(3);
        }
    }

    public void smoothScrollToValue(int toValue) {
        smoothScrollToValue(getValue(), toValue, true);
    }

    public void smoothScrollToValue(int fromValue, int toValue, boolean needRespond) {
        boolean z;
        int deltaIndex;
        boolean z2 = true;
        int i = this.mMinValue;
        int i2 = this.mMaxValue;
        if (this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck) {
            z = true;
        } else {
            z = false;
        }
        fromValue = refineValueByLimit(fromValue, i, i2, z);
        int i3 = this.mMinValue;
        i = this.mMaxValue;
        if (!(this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck)) {
            z2 = false;
        }
        toValue = refineValueByLimit(toValue, i3, i, z2);
        if (this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck) {
            deltaIndex = toValue - fromValue;
            int halfOneRecycleSize = getOneRecycleSize() / 2;
            if (deltaIndex < (-halfOneRecycleSize) || halfOneRecycleSize < deltaIndex) {
                deltaIndex = deltaIndex > 0 ? deltaIndex - getOneRecycleSize() : deltaIndex + getOneRecycleSize();
            }
        } else {
            deltaIndex = toValue - fromValue;
        }
        setValue(fromValue);
        if (fromValue != toValue) {
            scrollByIndexSmoothly(deltaIndex, needRespond);
        }
    }

    private void respondPickedValueChanged(int oldVal, int newVal, Object respondChange) {
        onScrollStateChange(0);
        if (oldVal != newVal && (respondChange == null || !(respondChange instanceof Boolean) || ((Boolean) respondChange).booleanValue())) {
            if (this.mOnValueChangeListener != null) {
                this.mOnValueChangeListener.onValueChange(this, this.mMinValue + oldVal, this.mMinValue + newVal);
            }
            if (this.mOnValueChangeListenerRaw != null) {
                this.mOnValueChangeListenerRaw.onValueChangeRelativeToRaw(this, oldVal, newVal, this.mDisplayedValues);
            }
        }
        this.mPrevPickedIndex = newVal;
        if (this.mPendingWrapToLinear) {
            this.mPendingWrapToLinear = false;
            internalSetWrapToLinear();
        }
    }

    private void scrollByIndexSmoothly(int deltaIndex) {
        scrollByIndexSmoothly(deltaIndex, true);
    }

    private void scrollByIndexSmoothly(int deltaIndex, boolean needRespond) {
        int dy;
        int duration;
        if (!(this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck)) {
            int willPickRawIndex = getPickedIndexRelativeToRaw();
            if (willPickRawIndex + deltaIndex > this.mMaxShowIndex) {
                deltaIndex = this.mMaxShowIndex - willPickRawIndex;
            } else if (willPickRawIndex + deltaIndex < this.mMinShowIndex) {
                deltaIndex = this.mMinShowIndex - willPickRawIndex;
            }
        }
        if (this.mCurrDrawFirstItemY < (-this.mItemHeight) / 2) {
            dy = this.mItemHeight + this.mCurrDrawFirstItemY;
            duration = (int) ((((float) (this.mItemHeight + this.mCurrDrawFirstItemY)) * 300.0f) / ((float) this.mItemHeight));
            if (deltaIndex < 0) {
                duration = (-duration) - (deltaIndex * 300);
            } else {
                duration += deltaIndex * 300;
            }
        } else {
            dy = this.mCurrDrawFirstItemY;
            duration = (int) ((((float) (-this.mCurrDrawFirstItemY)) * 300.0f) / ((float) this.mItemHeight));
            if (deltaIndex < 0) {
                duration -= deltaIndex * 300;
            } else {
                duration += deltaIndex * 300;
            }
        }
        dy += this.mItemHeight * deltaIndex;
        if (duration < 300) {
            duration = 300;
        }
        if (duration > 600) {
            duration = 600;
        }
        this.mScroller.startScroll(0, this.mCurrDrawGlobalY, 0, dy, duration);
        if (needRespond) {
            this.mHandlerInNewThread.sendMessageDelayed(getMsg(1), (long) (duration / 4));
        } else {
            this.mHandlerInNewThread.sendMessageDelayed(getMsg(1, 0, 0, new Boolean(needRespond)), (long) (duration / 4));
        }
        postInvalidate();
    }

    public void setMinValue(int minValue) {
        this.mMinValue = minValue;
        this.mMinShowIndex = 0;
        updateNotWrapYLimit();
    }

    public void setMaxValue(int maxValue) {
        if (this.mDisplayedValues == null) {
            throw new NullPointerException("mDisplayedValues should not be null");
        } else if ((maxValue - this.mMinValue) + 1 > this.mDisplayedValues.length) {
            throw new IllegalArgumentException("(maxValue - mMinValue + 1) should not be greater than mDisplayedValues.length now  (maxValue - mMinValue + 1) is " + ((maxValue - this.mMinValue) + 1) + " and mDisplayedValues.length is " + this.mDisplayedValues.length);
        } else {
            this.mMaxValue = maxValue;
            this.mMaxShowIndex = (this.mMaxValue - this.mMinValue) + this.mMinShowIndex;
            setMinAndMaxShowIndex(this.mMinShowIndex, this.mMaxShowIndex);
            updateNotWrapYLimit();
        }
    }

    public void setValue(int value) {
        if (value < this.mMinValue) {
            throw new IllegalArgumentException("should not set a value less than mMinValue, value is " + value);
        } else if (value > this.mMaxValue) {
            throw new IllegalArgumentException("should not set a value greater than mMaxValue, value is " + value);
        } else {
            setPickedIndexRelativeToRaw(value - this.mMinValue);
        }
    }

    public int getValue() {
        return getPickedIndexRelativeToRaw() + this.mMinValue;
    }

    public void setPickedIndexRelativeToMin(int pickedIndexToMin) {
        if (pickedIndexToMin >= 0 && pickedIndexToMin < getOneRecycleSize()) {
            this.mPrevPickedIndex = this.mMinShowIndex + pickedIndexToMin;
            boolean z = this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck;
            correctPositionByDefaultValue(pickedIndexToMin, z);
            postInvalidate();
        }
    }

    public void setPickedIndexRelativeToRaw(int pickedIndexToRaw) {
        if (this.mMinShowIndex > -1 && this.mMinShowIndex <= pickedIndexToRaw && pickedIndexToRaw <= this.mMaxShowIndex) {
            this.mPrevPickedIndex = pickedIndexToRaw;
            int i = pickedIndexToRaw - this.mMinShowIndex;
            boolean z = this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck;
            correctPositionByDefaultValue(i, z);
            postInvalidate();
        }
    }

    public int getPickedIndexRelativeToRaw() {
        if (this.mCurrDrawFirstItemY == 0) {
            return getWillPickIndexByGlobalY(this.mCurrDrawGlobalY);
        }
        if (this.mCurrDrawFirstItemY < (-this.mItemHeight) / 2) {
            return getWillPickIndexByGlobalY((this.mCurrDrawGlobalY + this.mItemHeight) + this.mCurrDrawFirstItemY);
        }
        return getWillPickIndexByGlobalY(this.mCurrDrawGlobalY + this.mCurrDrawFirstItemY);
    }

    public void setMinAndMaxShowIndex(int minShowIndex, int maxShowIndex) {
        setMinAndMaxShowIndex(minShowIndex, maxShowIndex, true);
    }

    public void setMinAndMaxShowIndex(int minShowIndex, int maxShowIndex, boolean needRefresh) {
        if (minShowIndex > maxShowIndex) {
            throw new IllegalArgumentException("minShowIndex should be less than maxShowIndex, minShowIndex is " + minShowIndex + ", maxShowIndex is " + maxShowIndex + ".");
        } else if (this.mDisplayedValues == null) {
            throw new IllegalArgumentException("mDisplayedValues should not be null, you need to set mDisplayedValues first.");
        } else if (minShowIndex < 0) {
            throw new IllegalArgumentException("minShowIndex should not be less than 0, now minShowIndex is " + minShowIndex);
        } else if (minShowIndex > this.mDisplayedValues.length - 1) {
            throw new IllegalArgumentException("minShowIndex should not be greater than (mDisplayedValues.length - 1), now (mDisplayedValues.length - 1) is " + (this.mDisplayedValues.length - 1) + " minShowIndex is " + minShowIndex);
        } else if (maxShowIndex < 0) {
            throw new IllegalArgumentException("maxShowIndex should not be less than 0, now maxShowIndex is " + maxShowIndex);
        } else if (maxShowIndex > this.mDisplayedValues.length - 1) {
            throw new IllegalArgumentException("maxShowIndex should not be greater than (mDisplayedValues.length - 1), now (mDisplayedValues.length - 1) is " + (this.mDisplayedValues.length - 1) + " maxShowIndex is " + maxShowIndex);
        } else {
            this.mMinShowIndex = minShowIndex;
            this.mMaxShowIndex = maxShowIndex;
            if (needRefresh) {
                boolean z;
                this.mPrevPickedIndex = this.mMinShowIndex + 0;
                if (this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck) {
                    z = true;
                } else {
                    z = false;
                }
                correctPositionByDefaultValue(0, z);
                postInvalidate();
            }
        }
    }

    private void onScrollStateChange(int scrollState) {
        if (this.mScrollState != scrollState) {
            this.mScrollState = scrollState;
            if (this.mOnScrollListener != null) {
                this.mOnScrollListener.onScrollStateChange(this, scrollState);
            }
        }
    }

    public void setOnValueChangedListener(OnValueChangeListener listener) {
        this.mOnValueChangeListener = listener;
    }

    private int getWillPickIndexByGlobalY(int globalY) {
        boolean z = false;
        if (this.mItemHeight == 0) {
            return 0;
        }
        int willPickIndex = (globalY / this.mItemHeight) + (this.mShowCount / 2);
        int oneRecycleSize = getOneRecycleSize();
        if (this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck) {
            z = true;
        }
        int index = getIndexByRawIndex(willPickIndex, oneRecycleSize, z);
        if (index >= 0 && index < getOneRecycleSize()) {
            return this.mMinShowIndex + index;
        }
        throw new IllegalArgumentException("getWillPickIndexByGlobalY illegal index : " + index + " getOneRecycleSize() : " + getOneRecycleSize() + " mWrapSelectorWheel : " + this.mWrapSelectorWheel);
    }

    private int getIndexByRawIndex(int index, int size, boolean wrap) {
        if (size <= 0) {
            return 0;
        }
        if (!wrap) {
            return index;
        }
        index %= size;
        if (index < 0) {
            index += size;
        }
        return index;
    }

    private void internalSetWrapToLinear() {
        correctPositionByDefaultValue(getPickedIndexRelativeToRaw() - this.mMinShowIndex, false);
        this.mWrapSelectorWheel = false;
        postInvalidate();
    }

    private void updateDividerAttr() {
        this.mDividerIndex0 = this.mShowCount / 2;
        this.mDividerIndex1 = this.mDividerIndex0 + 1;
        this.dividerY0 = (float) ((this.mDividerIndex0 * this.mViewHeight) / this.mShowCount);
        this.dividerY1 = (float) ((this.mDividerIndex1 * this.mViewHeight) / this.mShowCount);
        if (this.mDividerMarginL < 0) {
            this.mDividerMarginL = 0;
        }
        if (this.mDividerMarginR < 0) {
            this.mDividerMarginR = 0;
        }
        if (this.mDividerMarginL + this.mDividerMarginR != 0 && getPaddingLeft() + this.mDividerMarginL >= (this.mViewWidth - getPaddingRight()) - this.mDividerMarginR) {
            int surplusMargin = (((getPaddingLeft() + this.mDividerMarginL) + getPaddingRight()) + this.mDividerMarginR) - this.mViewWidth;
            this.mDividerMarginL = (int) (((float) this.mDividerMarginL) - ((((float) surplusMargin) * ((float) this.mDividerMarginL)) / ((float) (this.mDividerMarginL + this.mDividerMarginR))));
            this.mDividerMarginR = (int) (((float) this.mDividerMarginR) - ((((float) surplusMargin) * ((float) this.mDividerMarginR)) / ((float) (this.mDividerMarginL + this.mDividerMarginR))));
        }
    }

    private void updateFontAttr() {
        if (this.mTextSizeNormal > this.mItemHeight) {
            this.mTextSizeNormal = this.mItemHeight;
        }
        if (this.mTextSizeSelected > this.mItemHeight) {
            this.mTextSizeSelected = this.mItemHeight;
        }
        if (this.mPaintHint == null) {
            throw new IllegalArgumentException("mPaintHint should not be null.");
        }
        this.mPaintHint.setTextSize((float) this.mTextSizeHint);
        this.mTextSizeHintCenterYOffset = getTextCenterYOffset(this.mPaintHint.getFontMetrics());
        this.mWidthOfHintText = getTextWidth(this.mHintText, this.mPaintHint);
        if (this.mPaintText == null) {
            throw new IllegalArgumentException("mPaintText should not be null.");
        }
        this.mPaintText.setTextSize((float) this.mTextSizeSelected);
        this.mTextSizeSelectedCenterYOffset = getTextCenterYOffset(this.mPaintText.getFontMetrics());
        this.mPaintText.setTextSize((float) this.mTextSizeNormal);
        this.mTextSizeNormalCenterYOffset = getTextCenterYOffset(this.mPaintText.getFontMetrics());
    }

    private void updateNotWrapYLimit() {
        this.mNotWrapLimitYTop = 0;
        this.mNotWrapLimitYBottom = (-this.mShowCount) * this.mItemHeight;
        if (this.mDisplayedValues != null) {
            this.mNotWrapLimitYTop = ((getOneRecycleSize() - (this.mShowCount / 2)) - 1) * this.mItemHeight;
            this.mNotWrapLimitYBottom = (-(this.mShowCount / 2)) * this.mItemHeight;
        }
    }

    private int limitY(int currDrawGlobalYPreferred) {
        if (this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck) {
            return currDrawGlobalYPreferred;
        }
        if (currDrawGlobalYPreferred < this.mNotWrapLimitYBottom) {
            currDrawGlobalYPreferred = this.mNotWrapLimitYBottom;
        } else if (currDrawGlobalYPreferred > this.mNotWrapLimitYTop) {
            currDrawGlobalYPreferred = this.mNotWrapLimitYTop;
        }
        return currDrawGlobalYPreferred;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mItemHeight == 0) {
            return true;
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(event);
        this.currY = event.getY();
        this.currX = event.getX();
        int x = (int) event.getRawX();
        int y = (int) event.getRawY();
        switch (event.getAction()) {
            case 0:
                this.lastX = x;
                this.lastY = y;
                this.mFlagMayPress = true;
                this.mHandlerInNewThread.removeMessages(1);
                stopScrolling();
                this.downY = this.currY;
                this.downYGlobal = (float) this.mCurrDrawGlobalY;
                this.downX = this.currX;
                onScrollStateChange(0);
                break;
            case 1:
                if (!this.mFlagMayPress) {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000);
                    int velocityY = (int) (velocityTracker.getYVelocity() * this.mFriction);
                    if (Math.abs(velocityY) > this.mMiniVelocityFling) {
                        this.mScroller.fling(0, this.mCurrDrawGlobalY, 0, -velocityY, Integer.MIN_VALUE, Integer.MAX_VALUE, limitY(Integer.MIN_VALUE), limitY(Integer.MAX_VALUE));
                        invalidate();
                        onScrollStateChange(2);
                    }
                    this.mHandlerInNewThread.sendMessageDelayed(getMsg(1), 0);
                    releaseVelocityTracker();
                    break;
                }
                click(event);
                break;
            case 2:
                float spanY = this.downY - this.currY;
                if (Math.abs(y - this.lastY) < Math.abs(x - this.lastX)) {
                    getParent().requestDisallowInterceptTouchEvent(false);
                } else {
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                if (!this.mFlagMayPress || ((float) (-this.mScaledTouchSlop)) >= spanY || spanY >= ((float) this.mScaledTouchSlop)) {
                    this.mFlagMayPress = false;
                    this.mCurrDrawGlobalY = limitY((int) (this.downYGlobal + spanY));
                    calculateFirstItemParameterByGlobalY();
                    invalidate();
                }
                onScrollStateChange(1);
                break;
            case 3:
                this.downYGlobal = (float) this.mCurrDrawGlobalY;
                stopScrolling();
                this.mHandlerInNewThread.sendMessageDelayed(getMsg(1), 0);
                break;
        }
        return true;
    }

    private void click(MotionEvent event) {
        float y = event.getY();
        int i = 0;
        while (i < this.mShowCount) {
            if (((float) (this.mItemHeight * i)) > y || y >= ((float) (this.mItemHeight * (i + 1)))) {
                i++;
            } else {
                clickItem(i);
                return;
            }
        }
    }

    private void clickItem(int showCountIndex) {
        if (showCountIndex >= 0 && showCountIndex < this.mShowCount) {
            scrollByIndexSmoothly(showCountIndex - (this.mShowCount / 2));
        }
    }

    private float getTextCenterYOffset(FontMetrics fontMetrics) {
        if (fontMetrics == null) {
            return 0.0f;
        }
        return Math.abs(fontMetrics.top + fontMetrics.bottom) / 2.0f;
    }

    private void correctPositionByDefaultValue(int defaultPickedIndex, boolean wrap) {
        this.mCurrDrawFirstItemIndex = defaultPickedIndex - ((this.mShowCount - 1) / 2);
        this.mCurrDrawFirstItemIndex = getIndexByRawIndex(this.mCurrDrawFirstItemIndex, getOneRecycleSize(), wrap);
        if (this.mItemHeight == 0) {
            this.mCurrentItemIndexEffect = true;
            return;
        }
        this.mCurrDrawGlobalY = this.mCurrDrawFirstItemIndex * this.mItemHeight;
        this.mInScrollingPickedOldValue = this.mCurrDrawFirstItemIndex + (this.mShowCount / 2);
        this.mInScrollingPickedOldValue %= getOneRecycleSize();
        if (this.mInScrollingPickedOldValue < 0) {
            this.mInScrollingPickedOldValue += getOneRecycleSize();
        }
        this.mInScrollingPickedNewValue = this.mInScrollingPickedOldValue;
        calculateFirstItemParameterByGlobalY();
    }

    public void computeScroll() {
        if (this.mItemHeight != 0 && this.mScroller.computeScrollOffset()) {
            this.mCurrDrawGlobalY = this.mScroller.getCurrY();
            calculateFirstItemParameterByGlobalY();
            postInvalidate();
        }
    }

    private void calculateFirstItemParameterByGlobalY() {
        this.mCurrDrawFirstItemIndex = (int) Math.floor((double) (((float) this.mCurrDrawGlobalY) / ((float) this.mItemHeight)));
        this.mCurrDrawFirstItemY = -(this.mCurrDrawGlobalY - (this.mCurrDrawFirstItemIndex * this.mItemHeight));
        if (this.mOnValueChangeListenerInScrolling != null) {
            if ((-this.mCurrDrawFirstItemY) > this.mItemHeight / 2) {
                this.mInScrollingPickedNewValue = (this.mCurrDrawFirstItemIndex + 1) + (this.mShowCount / 2);
            } else {
                this.mInScrollingPickedNewValue = this.mCurrDrawFirstItemIndex + (this.mShowCount / 2);
            }
            this.mInScrollingPickedNewValue %= getOneRecycleSize();
            if (this.mInScrollingPickedNewValue < 0) {
                this.mInScrollingPickedNewValue += getOneRecycleSize();
            }
            if (this.mInScrollingPickedOldValue != this.mInScrollingPickedNewValue) {
                respondPickedValueChangedInScrolling(this.mInScrollingPickedNewValue, this.mInScrollingPickedOldValue);
            }
            this.mInScrollingPickedOldValue = this.mInScrollingPickedNewValue;
        }
    }

    private void releaseVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.clear();
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private void updateMaxWHOfDisplayedValues(boolean needRequestLayout) {
        updateMaxWidthOfDisplayedValues();
        updateMaxHeightOfDisplayedValues();
        if (!needRequestLayout) {
            return;
        }
        if (this.mSpecModeW == Integer.MIN_VALUE || this.mSpecModeH == Integer.MIN_VALUE) {
            this.mHandlerInMainThread.sendEmptyMessage(3);
        }
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        this.mSpecModeW = specMode;
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = (getPaddingLeft() + getPaddingRight()) + Math.max(this.mMaxWidthOfAlterArrayWithMeasureHint, Math.max(this.mMaxWidthOfDisplayedValues, this.mMaxWidthOfAlterArrayWithoutMeasureHint) + ((((Math.max(this.mWidthOfHintText, this.mWidthOfAlterHint) + (Math.max(this.mWidthOfHintText, this.mWidthOfAlterHint) == 0 ? 0 : this.mMarginStartOfHint)) + (Math.max(this.mWidthOfHintText, this.mWidthOfAlterHint) == 0 ? 0 : this.mMarginEndOfHint)) + (this.mItemPaddingHorizontal * 2)) * 2));
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        this.mSpecModeH = specMode;
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = (getPaddingTop() + getPaddingBottom()) + (this.mShowCount * (this.mMaxHeightOfDisplayedValues + (this.mItemPaddingVertical * 2)));
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    public void isSubtitle(boolean isSubtitle) {
        this.isSubtitle = isSubtitle;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawContent(canvas);
        drawLine(canvas);
        drawHint(canvas);
    }

    private void drawContent(Canvas canvas) {
        float fraction = 0.0f;
        for (int i = 0; i < this.mShowCount + 1; i++) {
            int textColor;
            float textSize;
            float textSizeCenterYOffset;
            float y = (float) (this.mCurrDrawFirstItemY + (this.mItemHeight * i));
            int i2 = this.mCurrDrawFirstItemIndex + i;
            int oneRecycleSize = getOneRecycleSize();
            boolean z = this.mWrapSelectorWheel && this.mWrapSelectorWheelCheck;
            int index = getIndexByRawIndex(i2, oneRecycleSize, z);
            if (i == this.mShowCount / 2) {
                fraction = ((float) (this.mItemHeight + this.mCurrDrawFirstItemY)) / ((float) this.mItemHeight);
                textColor = getEvaluateColor(fraction, this.mTextColorNormal, this.mTextColorSelected);
                textSize = getEvaluateSize(fraction, (float) this.mTextSizeNormal, (float) this.mTextSizeSelected);
                textSizeCenterYOffset = getEvaluateSize(fraction, this.mTextSizeNormalCenterYOffset, this.mTextSizeSelectedCenterYOffset);
            } else if (i == (this.mShowCount / 2) + 1) {
                textColor = getEvaluateColor(1.0f - fraction, this.mTextColorNormal, this.mTextColorSelected);
                textSize = getEvaluateSize(1.0f - fraction, (float) this.mTextSizeNormal, (float) this.mTextSizeSelected);
                textSizeCenterYOffset = getEvaluateSize(1.0f - fraction, this.mTextSizeNormalCenterYOffset, this.mTextSizeSelectedCenterYOffset);
            } else {
                textColor = this.mTextColorNormal;
                textSize = (float) this.mTextSizeNormal;
                textSizeCenterYOffset = this.mTextSizeNormalCenterYOffset;
            }
            this.mPaintText.setColor(textColor);
            this.mPaintText.setTextSize(textSize);
            if (index >= 0 && index < getOneRecycleSize()) {
                CharSequence str = this.mDisplayedValues[this.mMinShowIndex + index];
                if (this.mTextEllipsize != null) {
                    str = TextUtils.ellipsize(str, this.mPaintText, (float) (getWidth() - (this.mItemPaddingHorizontal * 2)), getEllipsizeType());
                }
                canvas.drawText(str.toString(), this.mViewCenterX, (((float) (this.mItemHeight / 2)) + y) + textSizeCenterYOffset, this.mPaintText);
                if (!(this.mDisplayedSubValues == null || this.mDisplayedSubValues.length < this.mMinShowIndex + index || TextUtils.isEmpty(this.mDisplayedSubValues[this.mMinShowIndex + index]))) {
                    int subTextSize = (int) (textSize / 2.0f);
                    this.mPaintText.setColor(textColor);
                    this.mPaintText.setTextSize((float) subTextSize);
                    canvas.drawText(this.mDisplayedSubValues[this.mMinShowIndex + index], this.mViewCenterX, (((((float) (this.mItemHeight / 2)) + y) + textSizeCenterYOffset) + (textSize / 2.0f)) + 5.0f, this.mPaintText);
                }
            } else if (!TextUtils.isEmpty(this.mEmptyItemHint)) {
                canvas.drawText(this.mEmptyItemHint, this.mViewCenterX, (((float) (this.mItemHeight / 2)) + y) + textSizeCenterYOffset, this.mPaintText);
            }
        }
    }

    private TruncateAt getEllipsizeType() {
        String str = this.mTextEllipsize;
        Object obj = -1;
        switch (str.hashCode()) {
            case -1074341483:
                if (str.equals("middle")) {
                    obj = 1;
                    break;
                }
                break;
            case 100571:
                if (str.equals("end")) {
                    obj = 2;
                    break;
                }
                break;
            case 109757538:
                if (str.equals("start")) {
                    obj = null;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                return TruncateAt.START;
            case 1:
                return TruncateAt.MIDDLE;
            case 2:
                return TruncateAt.END;
            default:
                throw new IllegalArgumentException("Illegal text ellipsize type.");
        }
    }

    private void drawLine(Canvas canvas) {
        if (this.mShowDivider) {
            canvas.drawLine((float) (getPaddingLeft() + this.mDividerMarginL), this.dividerY0, (float) ((this.mViewWidth - getPaddingRight()) - this.mDividerMarginR), this.dividerY0, this.mPaintDivider);
            canvas.drawLine((float) (getPaddingLeft() + this.mDividerMarginL), this.dividerY1, (float) ((this.mViewWidth - getPaddingRight()) - this.mDividerMarginR), this.dividerY1, this.mPaintDivider);
        }
    }

    private void drawHint(Canvas canvas) {
        if (!TextUtils.isEmpty(this.mHintText)) {
            canvas.drawText(this.mHintText, (this.mViewCenterX + ((float) ((this.mMaxWidthOfDisplayedValues + this.mWidthOfHintText) / 2))) + ((float) this.mMarginStartOfHint), ((this.dividerY0 + this.dividerY1) / 2.0f) + this.mTextSizeHintCenterYOffset, this.mPaintHint);
        }
    }

    private void updateMaxWidthOfDisplayedValues() {
        float savedTextSize = this.mPaintText.getTextSize();
        this.mPaintText.setTextSize((float) this.mTextSizeSelected);
        this.mMaxWidthOfDisplayedValues = getMaxWidthOfTextArray(this.mDisplayedValues, this.mPaintText);
        this.mMaxWidthOfAlterArrayWithMeasureHint = getMaxWidthOfTextArray(this.mAlterTextArrayWithMeasureHint, this.mPaintText);
        this.mMaxWidthOfAlterArrayWithoutMeasureHint = getMaxWidthOfTextArray(this.mAlterTextArrayWithoutMeasureHint, this.mPaintText);
        this.mPaintText.setTextSize((float) this.mTextSizeHint);
        this.mWidthOfAlterHint = getTextWidth(this.mAlterHint, this.mPaintText);
        this.mPaintText.setTextSize(savedTextSize);
    }

    private int getMaxWidthOfTextArray(CharSequence[] array, Paint paint) {
        if (array == null) {
            return 0;
        }
        int maxWidth = 0;
        for (CharSequence item : array) {
            if (item != null) {
                maxWidth = Math.max(getTextWidth(item, paint), maxWidth);
            }
        }
        return maxWidth;
    }

    private int getTextWidth(CharSequence text, Paint paint) {
        if (TextUtils.isEmpty(text)) {
            return 0;
        }
        return (int) (paint.measureText(text.toString()) + 0.5f);
    }

    private void updateMaxHeightOfDisplayedValues() {
        float savedTextSize = this.mPaintText.getTextSize();
        this.mPaintText.setTextSize((float) this.mTextSizeSelected);
        this.mMaxHeightOfDisplayedValues = (int) (((double) (this.mPaintText.getFontMetrics().bottom - this.mPaintText.getFontMetrics().top)) + 0.5d);
        this.mPaintText.setTextSize(savedTextSize);
    }

    private void updateContent(String[] newDisplayedValues, String[] subValues) {
        this.mDisplayedValues = newDisplayedValues;
        this.mDisplayedSubValues = subValues;
        updateWrapStateByContent();
    }

    private void updateValueForInit() {
        inflateDisplayedValuesIfNull();
        updateWrapStateByContent();
        if (this.mMinShowIndex == -1) {
            this.mMinShowIndex = 0;
        }
        if (this.mMaxShowIndex == -1) {
            this.mMaxShowIndex = this.mDisplayedValues.length - 1;
        }
        setMinAndMaxShowIndex(this.mMinShowIndex, this.mMaxShowIndex, false);
    }

    private void inflateDisplayedValuesIfNull() {
        if (this.mDisplayedValues == null) {
            this.mDisplayedValues = new String[1];
            this.mDisplayedValues[0] = "0";
        }
    }

    private void updateWrapStateByContent() {
        this.mWrapSelectorWheelCheck = this.mDisplayedValues.length > this.mShowCount;
    }

    private int refineValueByLimit(int value, int minValue, int maxValue, boolean wrap) {
        if (wrap) {
            if (value > maxValue) {
                value = (((value - maxValue) % getOneRecycleSize()) + minValue) - 1;
            } else if (value < minValue) {
                value = (((value - minValue) % getOneRecycleSize()) + maxValue) + 1;
            }
            return value;
        }
        if (value > maxValue) {
            value = maxValue;
        } else if (value < minValue) {
            value = minValue;
        }
        return value;
    }

    private void stopRefreshing() {
        if (this.mHandlerInNewThread != null) {
            this.mHandlerInNewThread.removeMessages(1);
        }
    }

    public void stopScrolling() {
        if (this.mScroller != null && !this.mScroller.isFinished()) {
            this.mScroller.startScroll(0, this.mScroller.getCurrY(), 0, 0, 1);
            this.mScroller.abortAnimation();
            postInvalidate();
        }
    }

    private Message getMsg(int what) {
        return getMsg(what, 0, 0, null);
    }

    private Message getMsg(int what, int arg1, int arg2, Object obj) {
        Message msg = Message.obtain();
        msg.what = what;
        msg.arg1 = arg1;
        msg.arg2 = arg2;
        msg.obj = obj;
        return msg;
    }

    private int sp2px(Context context, float spValue) {
        return (int) ((spValue * context.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    private int dp2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    private int getEvaluateColor(float fraction, int startColor, int endColor) {
        int sA = (-16777216 & startColor) >>> 24;
        int sR = (16711680 & startColor) >>> 16;
        int sG = (65280 & startColor) >>> 8;
        int sB = (startColor & 255) >>> 0;
        return (((((int) (((float) sA) + (((float) (((-16777216 & endColor) >>> 24) - sA)) * fraction))) << 24) | (((int) (((float) sR) + (((float) (((16711680 & endColor) >>> 16) - sR)) * fraction))) << 16)) | (((int) (((float) sG) + (((float) (((65280 & endColor) >>> 8) - sG)) * fraction))) << 8)) | ((int) (((float) sB) + (((float) (((endColor & 255) >>> 0) - sB)) * fraction)));
    }

    private float getEvaluateSize(float fraction, float startSize, float endSize) {
        return ((endSize - startSize) * fraction) + startSize;
    }

    public void smoothScrollBy(int index) {
        smoothScrollToValue(getValue() + index);
    }

    public void setHasFocused(boolean hasFocused) {
        this.hasFocused = hasFocused;
        invalidate();
    }

    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        try {
            if (this.paint == null) {
                this.paint = new Paint();
            }
            if (this.hasFocused) {
                this.paint.setColor(Color.parseColor("#00FF34"));
                this.paint.setStyle(Style.FILL);
            } else {
                this.paint.setColor(Color.parseColor("#808080"));
                this.paint.setStyle(Style.FILL);
            }
            float lineHeight = TypedValue.applyDimension(0, 3.0f, getContext().getResources().getDisplayMetrics());
            float margin = TypedValue.applyDimension(0, 15.0f, getContext().getResources().getDisplayMetrics());
            float leftT = 0.0f + lineHeight;
            float topT = (float) (((((double) ((float) this.mViewHeight)) / 2.0d) - (((double) ((float) this.mTextSizeSelected)) / 2.0d)) - (((double) margin) / 2.0d));
            float rightT = ((float) getWidth()) - lineHeight;
            float topB = (float) ((((((double) ((float) this.mViewHeight)) / 2.0d) + (((double) ((float) this.mTextSizeSelected)) / 2.0d)) + (((double) margin) / 2.0d)) + ((double) (this.isSubtitle ? ((float) (this.mTextSizeNormal / 2)) + (margin / 2.0f) : 0.0f)));
            float bottomB = topB + lineHeight;
            canvas.drawRect(leftT, topT, rightT, topT + lineHeight, this.paint);
            canvas.drawRect(leftT, topB, rightT, bottomB, this.paint);
            canvas.restore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setWidth(int width) {
        LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = width;
        setLayoutParams(layoutParams);
    }

    public void setSelectTextSize(int size) {
        if (size != 0) {
            float per = ((float) size) / 30.0f;
            DEFAULT_TEXT_SIZE_SELECTED_SP_DEFAULT = size;
            this.mTextSizeNormal = sp2px(getContext(), (float) ((int) (17.0f * per)));
            this.mTextSizeSelected = sp2px(getContext(), (float) DEFAULT_TEXT_SIZE_SELECTED_SP_DEFAULT);
            postInvalidate();
        }
    }
}
