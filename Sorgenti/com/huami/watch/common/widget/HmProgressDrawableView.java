package com.huami.watch.common.widget;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

public class HmProgressDrawableView extends View {
    private boolean mDraw;
    private int mGap;
    private int mHeight;
    private Drawable mLeftD;
    private int mMinHeight;
    private float mRatio;
    private Drawable mRightD;
    private int mWidth;

    protected int getSuggestedMinimumHeight() {
        if (this.mMinHeight > 0) {
            return Math.max(this.mMinHeight, super.getSuggestedMinimumHeight());
        }
        return super.getSuggestedMinimumHeight();
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        this.mWidth = right - left;
        this.mHeight = bottom - top;
        if (changed) {
            updateRatio();
        }
    }

    private void updateRatio() {
        if (this.mDraw) {
            int w = (int) (((float) (this.mWidth - this.mGap)) * this.mRatio);
            this.mLeftD.setBounds(0, 0, w, this.mHeight);
            this.mRightD.setBounds(this.mGap + w, 0, this.mWidth, this.mHeight);
        }
    }

    protected void onDraw(Canvas canvas) {
        if (this.mDraw) {
            this.mLeftD.draw(canvas);
            this.mRightD.draw(canvas);
        }
    }
}
