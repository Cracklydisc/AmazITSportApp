package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.DisplayMetrics;
import android.util.Property;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewConfiguration;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PageListView extends RecyclerView {
    private boolean mCanClick;
    private boolean mCenterItemInPage;
    private ClickListener mClickListener;
    private boolean mGestureDirectionLocked;
    private boolean mGreedyTouchMode;
    private int mInitialOffset;
    private int mItemCountPerPage;
    private int mLastScrollChange;
    private final int[] mLocation;
    private final int mMaxFlingVelocity;
    private boolean mMaximizeSingleItem;
    private final int mMinFlingVelocity;
    private final AdapterDataObserver mObserver;
    private OnOverScrollListener mOverScrollListener;
    private boolean mPossibleVerticalSwipe;
    private final Handler mPressedHandler;
    private final Runnable mPressedRunnable;
    private View mPressedView;
    private int mPreviousCentral;
    private final Runnable mReleasedRunnable;
    private Animator mScrollAnimator;
    private final Set<ScrollListener> mScrollListeners;
    private Scroller mScroller;
    private SetScrollVerticallyProperty mSetScrollVerticallyProperty;
    private float mStartFirstTop;
    private float mStartX;
    private float mStartY;
    private int mTapPositionX;
    private int mTapPositionY;
    private final float[] mTapRegions;
    private final int mTouchSlop;

    class C04881 implements Runnable {
        final /* synthetic */ PageListView this$0;

        public void run() {
            this.this$0.mPressedView = this.this$0.getChildAt(this.this$0.findCenterViewIndex());
            this.this$0.mPressedView.setPressed(true);
        }
    }

    class C04892 implements Runnable {
        final /* synthetic */ PageListView this$0;

        public void run() {
            this.this$0.releasePressedItem();
        }
    }

    class C04913 extends AdapterDataObserver {
        final /* synthetic */ PageListView this$0;

        class C04901 implements OnLayoutChangeListener {
            C04901() {
            }

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                C04913.this.this$0.removeOnLayoutChangeListener(this);
                if (C04913.this.this$0.getChildCount() > 0) {
                    C04913.this.this$0.animateToCenter();
                }
            }
        }

        public void onChanged() {
            this.this$0.addOnLayoutChangeListener(new C04901());
        }
    }

    class C04924 extends OnScrollListener {
        final /* synthetic */ PageListView this$0;

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0 && this.this$0.getChildCount() > 0) {
                this.this$0.handleTouchUp((MotionEvent) null, newState);
            }
            for (ScrollListener listener : this.this$0.mScrollListeners) {
                listener.onScrollStateChanged(newState);
            }
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            this.this$0.onScroll(dy);
        }
    }

    class C04935 extends SimpleAnimatorListener {
        C04935() {
        }

        public void onAnimationEnd(Animator animator) {
            if (!wasCanceled()) {
                PageListView.this.mCanClick = true;
            }
        }
    }

    class C04946 extends SimpleAnimatorListener {
        final /* synthetic */ Runnable val$endAction;

        public void onAnimationEnd(Animator animator) {
            if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    public static abstract class Adapter extends android.support.v7.widget.RecyclerView.Adapter<ViewHolder> {
    }

    public interface ClickListener {
        void onClick(ViewHolder viewHolder);

        void onTopEmptyRegionClick();
    }

    private class LayoutManager extends android.support.v7.widget.RecyclerView.LayoutManager {
        private int mFirstPosition;
        private boolean mPushFirstHigher;
        private boolean mUseOldViewTop;
        private boolean mWasZoomedIn;
        final /* synthetic */ PageListView this$0;

        private void notifyChildrenAboutProximity(boolean animate) {
            if (this.this$0.mCenterItemInPage || this.this$0.mItemCountPerPage != 2) {
                int position = this.this$0.getChildViewHolder(getChildAt(this.this$0.findCenterViewIndex())).getAdapterPosition();
                if (animate && position != this.this$0.mPreviousCentral && this.this$0.getScrollState() != 1) {
                    for (ScrollListener l : this.this$0.mScrollListeners) {
                        l.onCentralPositionChanged(position, this.this$0.mPreviousCentral);
                    }
                    this.this$0.mPreviousCentral = position;
                } else if (!animate) {
                    this.this$0.mPreviousCentral = position;
                }
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLayoutChildren(android.support.v7.widget.RecyclerView.Recycler r12, android.support.v7.widget.RecyclerView.State r13) {
            /*
            r11 = this;
            r10 = 0;
            r9 = -1;
            r7 = r11.getHeight();
            r8 = r11.getPaddingBottom();
            r5 = r7 - r8;
            r7 = r11.this$0;
            r7 = r7.getCentralViewTop();
            r8 = r11.this$0;
            r8 = r8.mInitialOffset;
            r4 = r7 + r8;
            r7 = r11.mUseOldViewTop;
            if (r7 == 0) goto L_0x00b9;
        L_0x001e:
            r7 = r11.getChildCount();
            if (r7 <= 0) goto L_0x00b9;
        L_0x0024:
            r7 = r11.this$0;
            r1 = r7.findCenterViewIndex();
            r7 = r11.getChildAt(r1);
            r6 = r11.getPosition(r7);
            if (r6 != r9) goto L_0x0050;
        L_0x0034:
            r3 = 0;
            r0 = r11.getChildCount();
        L_0x0039:
            r7 = r1 + r3;
            if (r7 < r0) goto L_0x0041;
        L_0x003d:
            r7 = r1 - r3;
            if (r7 < 0) goto L_0x0050;
        L_0x0041:
            r7 = r1 + r3;
            r2 = r11.getChildAt(r7);
            if (r2 == 0) goto L_0x006d;
        L_0x0049:
            r6 = r11.getPosition(r2);
            if (r6 == r9) goto L_0x006d;
        L_0x004f:
            r1 = r1 + r3;
        L_0x0050:
            if (r6 != r9) goto L_0x0080;
        L_0x0052:
            r7 = r11.getChildAt(r10);
            r4 = r7.getTop();
            r3 = r13.getItemCount();
        L_0x005e:
            r7 = r11.mFirstPosition;
            if (r7 < r3) goto L_0x00b0;
        L_0x0062:
            r7 = r11.mFirstPosition;
            if (r7 <= 0) goto L_0x00b0;
        L_0x0066:
            r7 = r11.mFirstPosition;
            r7 = r7 + -1;
            r11.mFirstPosition = r7;
            goto L_0x005e;
        L_0x006d:
            r7 = r1 - r3;
            r2 = r11.getChildAt(r7);
            if (r2 == 0) goto L_0x007d;
        L_0x0075:
            r6 = r11.getPosition(r2);
            if (r6 == r9) goto L_0x007d;
        L_0x007b:
            r1 = r1 - r3;
            goto L_0x0050;
        L_0x007d:
            r3 = r3 + 1;
            goto L_0x0039;
        L_0x0080:
            r7 = r11.mWasZoomedIn;
            if (r7 != 0) goto L_0x008c;
        L_0x0084:
            r7 = r11.getChildAt(r1);
            r4 = r7.getTop();
        L_0x008c:
            r7 = r11.getPaddingTop();
            if (r4 <= r7) goto L_0x009e;
        L_0x0092:
            if (r6 <= 0) goto L_0x009e;
        L_0x0094:
            r6 = r6 + -1;
            r7 = r11.this$0;
            r7 = r7.getItemHeight();
            r4 = r4 - r7;
            goto L_0x008c;
        L_0x009e:
            if (r6 != 0) goto L_0x00ae;
        L_0x00a0:
            r7 = r11.this$0;
            r7 = r7.getCentralViewTop();
            if (r4 <= r7) goto L_0x00ae;
        L_0x00a8:
            r7 = r11.this$0;
            r4 = r7.getCentralViewTop();
        L_0x00ae:
            r11.mFirstPosition = r6;
        L_0x00b0:
            r11.performLayoutChildren(r12, r13, r5, r4);
            r7 = 1;
            r11.mUseOldViewTop = r7;
            r11.mPushFirstHigher = r10;
            return;
        L_0x00b9:
            r7 = r11.mPushFirstHigher;
            if (r7 == 0) goto L_0x00b0;
        L_0x00bd:
            r7 = r11.this$0;
            r7 = r7.getCentralViewTop();
            r8 = r11.this$0;
            r8 = r8.getItemHeight();
            r4 = r7 - r8;
            goto L_0x00b0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.common.widget.PageListView.LayoutManager.onLayoutChildren(android.support.v7.widget.RecyclerView$Recycler, android.support.v7.widget.RecyclerView$State):void");
        }

        private void performLayoutChildren(Recycler recycler, State state, int parentBottom, int top) {
            detachAndScrapAttachedViews(recycler);
            if (this.this$0.mMaximizeSingleItem && state.getItemCount() == 1) {
                performLayoutOneChild(recycler, parentBottom);
                this.mWasZoomedIn = true;
            } else {
                performLayoutMultipleChildren(recycler, state, parentBottom, top);
                this.mWasZoomedIn = false;
            }
            if (getChildCount() > 0) {
                notifyChildrenAboutProximity(false);
            }
        }

        private void performLayoutOneChild(Recycler recycler, int parentBottom) {
            int right = getWidth() - getPaddingRight();
            View v = recycler.getViewForPosition(getFirstPosition());
            addView(v, 0);
            measureZoomView(v);
            v.layout(getPaddingLeft(), getPaddingTop(), right, parentBottom);
        }

        private void performLayoutMultipleChildren(Recycler recycler, State state, int parentBottom, int top) {
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int count = state.getItemCount();
            int i = 0;
            while (getFirstPosition() + i < count && top < parentBottom) {
                View v = recycler.getViewForPosition(getFirstPosition() + i);
                addView(v, i);
                measureView(v);
                int bottom = top + this.this$0.getItemHeight();
                v.layout(left, top, right, bottom);
                i++;
                top = bottom;
            }
        }

        private void measureView(View v, int height) {
            LayoutParams lp = (LayoutParams) v.getLayoutParams();
            v.measure(android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), ((getPaddingLeft() + getPaddingRight()) + lp.leftMargin) + lp.rightMargin, lp.width, canScrollHorizontally()), android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), ((getPaddingTop() + getPaddingBottom()) + lp.topMargin) + lp.bottomMargin, height, canScrollVertically()));
        }

        private void measureView(View v) {
            measureView(v, getHeight() / this.this$0.mItemCountPerPage);
        }

        private void measureZoomView(View v) {
            measureView(v, getHeight());
        }

        public LayoutParams generateDefaultLayoutParams() {
            return new LayoutParams(-1, -2);
        }

        public boolean canScrollVertically() {
            return getItemCount() > this.this$0.mItemCountPerPage || !this.mWasZoomedIn;
        }

        public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
            if (getChildCount() == 0) {
                return 0;
            }
            int scrolled = 0;
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int scrollBy;
            if (dy < 0) {
                while (scrolled > dy) {
                    View parentHeight1 = getChildAt(0);
                    if (getFirstPosition() > 0) {
                        scrollBy = Math.min(scrolled - dy, Math.max(-parentHeight1.getTop(), 0));
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        if (getFirstPosition() <= 0 || scrolled <= dy) {
                            break;
                        }
                        this.mFirstPosition--;
                        View scrollBy2 = recycler.getViewForPosition(getFirstPosition());
                        addView(scrollBy2, 0);
                        measureView(scrollBy2);
                        int v1 = parentHeight1.getTop();
                        scrollBy2.layout(left, v1 - this.this$0.getItemHeight(), right, v1);
                    } else {
                        this.mPushFirstHigher = false;
                        scrollBy = Math.min(-dy, (this.this$0.mOverScrollListener != null ? getHeight() : this.this$0.getTopViewMaxTop()) - parentHeight1.getTop());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                    }
                }
            } else if (dy > 0) {
                int parentHeight = getHeight();
                while (scrolled < dy) {
                    View bottomView = getChildAt(getChildCount() - 1);
                    if (state.getItemCount() > this.mFirstPosition + getChildCount()) {
                        int scrollBy1 = -Math.min(dy - scrolled, Math.max(bottomView.getBottom() - parentHeight, 0));
                        scrolled -= scrollBy1;
                        offsetChildrenVertical(scrollBy1);
                        if (scrolled >= dy) {
                            break;
                        }
                        View v = recycler.getViewForPosition(this.mFirstPosition + getChildCount());
                        int top = getChildAt(getChildCount() - 1).getBottom();
                        addView(v);
                        measureView(v);
                        v.layout(left, top, right, top + this.this$0.getItemHeight());
                    } else {
                        scrollBy = Math.max(-dy, getHeight() - bottomView.getBottom());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        break;
                    }
                }
            }
            recycleViewsOutOfBounds(recycler);
            notifyChildrenAboutProximity(true);
            return scrolled;
        }

        public void scrollToPosition(int position) {
            this.mUseOldViewTop = false;
            if (position > 0) {
                this.mFirstPosition = position - 1;
                this.mPushFirstHigher = true;
            } else {
                this.mFirstPosition = position;
                this.mPushFirstHigher = false;
            }
            requestLayout();
        }

        public void smoothScrollToPosition(RecyclerView recyclerView, State state, int position) {
            SmoothScroller linearSmoothScroller = new SmoothScroller(recyclerView.getContext(), this);
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);
        }

        private void recycleViewsOutOfBounds(Recycler recycler) {
            int i;
            int childCount = getChildCount();
            int parentWidth = getWidth();
            int parentHeight = getHeight();
            boolean foundFirst = false;
            int first = 0;
            int last = 0;
            for (i = 0; i < childCount; i++) {
                View v = getChildAt(i);
                if (v.hasFocus() || (v.getRight() >= 0 && v.getLeft() <= parentWidth && v.getBottom() >= 0 && v.getTop() <= parentHeight)) {
                    if (!foundFirst) {
                        first = i;
                        foundFirst = true;
                    }
                    last = i;
                }
            }
            for (i = childCount - 1; i > last; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            for (i = first - 1; i >= 0; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            if (getChildCount() == 0) {
                this.mFirstPosition = 0;
            } else if (first > 0) {
                this.mPushFirstHigher = true;
                this.mFirstPosition += first;
            }
        }

        public int getFirstPosition() {
            return this.mFirstPosition;
        }

        public void onAdapterChanged(android.support.v7.widget.RecyclerView.Adapter oldAdapter, android.support.v7.widget.RecyclerView.Adapter newAdapter) {
            removeAllViews();
        }
    }

    public interface OnOverScrollListener {
        void onOverScroll();
    }

    public interface ScrollListener {
        void onCentralPositionChanged(int i, int i2);

        void onScroll(int i);

        void onScrollStateChanged(int i);
    }

    private class SetScrollVerticallyProperty extends Property<PageListView, Integer> {
        final /* synthetic */ PageListView this$0;

        public Integer get(PageListView PageListView) {
            return Integer.valueOf(this.this$0.mLastScrollChange);
        }

        public void set(PageListView PageListView, Integer value) {
            this.this$0.setScrollVertically(value.intValue());
        }
    }

    private static class SmoothScroller extends LinearSmoothScroller {
        private final LayoutManager mLayoutManager;

        public SmoothScroller(Context context, LayoutManager manager) {
            super(context);
            this.mLayoutManager = manager;
        }

        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }

        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return ((boxStart + boxEnd) / 2) - ((viewStart + viewEnd) / 2);
        }

        public PointF computeScrollVectorForPosition(int targetPosition) {
            return targetPosition < this.mLayoutManager.getFirstPosition() ? new PointF(0.0f, -1.0f) : new PointF(0.0f, 1.0f);
        }
    }

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private int mClickRegion;

        public void setClickRegion(int region) {
            if (region == 8388611 || region == 8388613) {
                this.mClickRegion = region;
                return;
            }
            throw new IllegalArgumentException("Region is not recognized:" + region);
        }
    }

    public void setAdapter(android.support.v7.widget.RecyclerView.Adapter adapter) {
        android.support.v7.widget.RecyclerView.Adapter currentAdapter = getAdapter();
        if (currentAdapter != null) {
            currentAdapter.unregisterAdapterDataObserver(this.mObserver);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.mObserver);
        }
    }

    public int getBaseline() {
        if (getChildCount() == 0) {
            return super.getBaseline();
        }
        int centerChildBaseline = getChildAt(findCenterViewIndex()).getBaseline();
        return centerChildBaseline == -1 ? super.getBaseline() : getCentralViewTop() + centerChildBaseline;
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        if (this.mGreedyTouchMode && getChildCount() > 0) {
            int action = event.getActionMasked();
            if (action == 0) {
                this.mStartX = event.getX();
                this.mStartY = event.getY();
                this.mStartFirstTop = getChildCount() > 0 ? (float) getChildAt(0).getTop() : 0.0f;
                this.mPossibleVerticalSwipe = true;
                this.mGestureDirectionLocked = false;
            } else if (action == 2 && this.mPossibleVerticalSwipe) {
                handlePossibleVerticalSwipe(event);
            }
            if (this.mPossibleVerticalSwipe && this.mGestureDirectionLocked) {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
        }
        return super.onInterceptTouchEvent(event);
    }

    private boolean handlePossibleVerticalSwipe(MotionEvent event) {
        if (this.mGestureDirectionLocked) {
            return this.mPossibleVerticalSwipe;
        }
        float deltaX = Math.abs(this.mStartX - event.getX());
        float deltaY = Math.abs(this.mStartY - event.getY());
        if ((deltaX * deltaX) + (deltaY * deltaY) > ((float) (this.mTouchSlop * this.mTouchSlop))) {
            if (deltaX > deltaY) {
                this.mPossibleVerticalSwipe = false;
            }
            this.mGestureDirectionLocked = true;
        }
        return this.mPossibleVerticalSwipe;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        int scrollState = getScrollState();
        boolean result = super.onTouchEvent(event);
        if (getChildCount() <= 0) {
            return result;
        }
        int action = event.getActionMasked();
        if (action == 0) {
            handleTouchDown(event);
            return result;
        } else if (action == 1) {
            handleTouchUp(event, scrollState);
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        } else if (action == 2) {
            if (Math.abs(this.mTapPositionX - ((int) event.getX())) >= this.mTouchSlop || Math.abs(this.mTapPositionY - ((int) event.getY())) >= this.mTouchSlop) {
                releasePressedItem();
                this.mCanClick = false;
            }
            result |= handlePossibleVerticalSwipe(event);
            getParent().requestDisallowInterceptTouchEvent(this.mPossibleVerticalSwipe);
            return result;
        } else if (action != 3) {
            return result;
        } else {
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        }
    }

    private void releasePressedItem() {
        if (this.mPressedView != null) {
            this.mPressedView.setPressed(false);
            this.mPressedView = null;
        }
        this.mPressedHandler.removeCallbacks(this.mPressedRunnable);
    }

    private void onScroll(int dy) {
        for (ScrollListener listener : this.mScrollListeners) {
            listener.onScroll(dy);
        }
    }

    private boolean isViewHit(View v, float hit) {
        v.getLocationOnScreen(this.mLocation);
        if (hit <= ((float) this.mLocation[1]) || hit >= ((float) (this.mLocation[1] + getAdjustedHeight(v)))) {
            return false;
        }
        return true;
    }

    private boolean checkForTap(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        ViewHolder holder;
        if (this.mCenterItemInPage || this.mItemCountPerPage != 2) {
            float rawY = event.getRawY();
            int index = findCenterViewIndex();
            holder = getChildViewHolder(getChildAt(index));
            computeTapRegions(this.mTapRegions);
            if (rawY > this.mTapRegions[0] && rawY < this.mTapRegions[1]) {
                if (this.mClickListener != null) {
                    this.mClickListener.onClick(holder);
                }
                return true;
            } else if (index > 0 && rawY <= this.mTapRegions[0]) {
                animateToMiddle(index - 1, index);
                return true;
            } else if (index < getChildCount() - 1 && rawY >= this.mTapRegions[1]) {
                animateToMiddle(index + 1, index);
                return true;
            } else if (index != 0 || rawY > this.mTapRegions[0] || this.mClickListener == null) {
                return false;
            } else {
                this.mClickListener.onTopEmptyRegionClick();
                return true;
            }
        }
        float hity = event.getRawY();
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            if (isViewHit(view, hity) && this.mClickListener != null) {
                holder = getChildViewHolder(view);
                holder.setClickRegion(event.getX() * 2.0f < ((float) getWidth()) ? 8388611 : 8388613);
                this.mClickListener.onClick(holder);
            }
        }
        return true;
    }

    private void animateToMiddle(int newCenterIndex, int oldCenterIndex) {
        if (newCenterIndex == oldCenterIndex) {
            throw new IllegalArgumentException("newCenterIndex must be different from oldCenterIndex");
        }
        startScrollAnimation(new ArrayList(), getCentralViewTop() - getChildAt(newCenterIndex).getTop(), 150);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration) {
        startScrollAnimation((List) animators, scroll, duration, 0);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay) {
        startScrollAnimation(animators, scroll, duration, delay, (AnimatorListener) null);
    }

    private void startScrollAnimation(int scroll, long duration, long delay, AnimatorListener listener) {
        startScrollAnimation(null, scroll, duration, delay, listener);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay, AnimatorListener listener) {
        if (this.mScrollAnimator != null) {
            this.mScrollAnimator.cancel();
        }
        this.mLastScrollChange = 0;
        ObjectAnimator scrollAnimator = ObjectAnimator.ofInt(this, this.mSetScrollVerticallyProperty, new int[]{0, -scroll});
        if (animators != null) {
            animators.add(scrollAnimator);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);
            this.mScrollAnimator = animatorSet;
        } else {
            this.mScrollAnimator = scrollAnimator;
        }
        this.mScrollAnimator.setDuration(duration);
        if (listener != null) {
            this.mScrollAnimator.addListener(listener);
        }
        if (delay > 0) {
            this.mScrollAnimator.setStartDelay(delay);
        }
        this.mScrollAnimator.start();
    }

    public boolean fling(int velocityX, int velocityY) {
        if (getChildCount() == 0) {
            return false;
        }
        int currentPosition = getChildAdapterPosition(getChildAt(findCenterViewIndex()));
        if ((currentPosition == 0 && velocityY < 0) || (currentPosition == getAdapter().getItemCount() - 1 && velocityY > 0)) {
            return super.fling(velocityX, velocityY);
        }
        if (Math.abs(velocityY) < this.mMinFlingVelocity) {
            return false;
        }
        velocityY = Math.max(Math.min(velocityY, this.mMaxFlingVelocity), -this.mMaxFlingVelocity);
        if (this.mScroller == null) {
            this.mScroller = new Scroller(getContext(), null, true);
        }
        this.mScroller.fling(0, 0, 0, velocityY, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        smoothScrollToPosition(Math.max(0, Math.min(getAdapter().getItemCount() - 1, currentPosition + (this.mScroller.getFinalY() / (getPaddingTop() + (getItemHeight() * 4))))));
        return true;
    }

    public ViewHolder getChildViewHolder(View child) {
        return (ViewHolder) super.getChildViewHolder(child);
    }

    public int findCenterViewIndex() {
        int count = getChildCount();
        int index = -1;
        int closest = Integer.MAX_VALUE;
        int centerY = getCenterYPos(this);
        if (!this.mCenterItemInPage && this.mItemCountPerPage == 2) {
            centerY = 1;
        }
        for (int i = 0; i < count; i++) {
            int distance = Math.abs(centerY - (getTop() + getCenterYPos(getChildAt(i))));
            if (distance < closest) {
                closest = distance;
                index = i;
            }
        }
        if (index != -1) {
            return index;
        }
        throw new IllegalStateException("Can't find central view.");
    }

    private int getCenterYPos(View v) {
        return (v.getTop() + v.getPaddingTop()) + (getAdjustedHeight(v) / 2);
    }

    private void handleTouchUp(MotionEvent event, int scrollState) {
        if (this.mCanClick && event != null && checkForTap(event)) {
            this.mPressedHandler.postDelayed(this.mReleasedRunnable, (long) ViewConfiguration.getTapTimeout());
        } else if (scrollState != 0) {
        } else {
            if (isOverScrolling()) {
                this.mOverScrollListener.onOverScroll();
            } else {
                animateToCenter();
            }
        }
    }

    private boolean isOverScrolling() {
        return getChildCount() > 0 && this.mStartFirstTop <= ((float) getCentralViewTop()) && getChildAt(0).getTop() >= getTopViewMaxTop() && this.mOverScrollListener != null;
    }

    private int getTopViewMaxTop() {
        if (getOverScrollMode() != 0) {
            return getAdjustedHeight() / 2;
        }
        return (this.mCenterItemInPage || this.mItemCountPerPage != 2) ? 0 : this.mInitialOffset;
    }

    public int getItemHeight() {
        return getAdjustedHeight() / this.mItemCountPerPage;
    }

    public int getCentralViewTop() {
        int offset = (int) ((((float) getAdjustedHeight()) * ((float) (this.mItemCountPerPage - 1))) / ((float) (this.mItemCountPerPage * 2)));
        if (!this.mCenterItemInPage && this.mItemCountPerPage == 2) {
            offset = 0;
        }
        return getPaddingTop() + offset;
    }

    public void animateToCenter() {
        View child = getChildAt(findCenterViewIndex());
        startScrollAnimation((getCentralViewTop() - child.getTop()) + (getChildAdapterPosition(child) <= 0 ? this.mInitialOffset : 0), 150, 0, new C04935());
    }

    private void handleTouchDown(MotionEvent event) {
        if (this.mCanClick) {
            this.mTapPositionX = (int) event.getX();
            this.mTapPositionY = (int) event.getY();
        }
    }

    private void setScrollVertically(int scroll) {
        scrollBy(0, scroll - this.mLastScrollChange);
        this.mLastScrollChange = scroll;
    }

    private int getAdjustedHeight() {
        return getAdjustedHeight(this);
    }

    private int getAdjustedHeight(View v) {
        return (v.getHeight() - v.getPaddingBottom()) - v.getPaddingTop();
    }

    private void computeTapRegions(float[] tapRegions) {
        int[] iArr = this.mLocation;
        this.mLocation[1] = 0;
        iArr[0] = 0;
        getLocationOnScreen(this.mLocation);
        int mScreenTop = this.mLocation[1];
        int height = getAdjustedHeight();
        tapRegions[0] = ((float) mScreenTop) + (((float) height) * (((float) (this.mItemCountPerPage - 1)) / ((float) (this.mItemCountPerPage * 2))));
        tapRegions[1] = tapRegions[0] + ((float) getItemHeight());
        if (!this.mCenterItemInPage && this.mItemCountPerPage == 2) {
            tapRegions[0] = (float) mScreenTop;
            tapRegions[1] = tapRegions[0] + ((float) height);
        }
    }
}
