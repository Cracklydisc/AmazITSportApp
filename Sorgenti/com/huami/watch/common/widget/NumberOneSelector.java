package com.huami.watch.common.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.ui.C1070R;

public class NumberOneSelector extends LinearLayout implements OnValueChangeListener, KeyeventConsumer {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private KeyeventProcessor keyeventProcessor;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;
    private String[] mHoursSub;
    private NumberPickerView parentPickerView;
    private int widthColumn1;

    class C04751 implements EventCallBack {
        C04751() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP) {
                if (NumberOneSelector.this.parentPickerView != null && NumberOneSelector.this.parentPickerView.getCurrentIndex() == NumberOneSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberOneSelector.this.currentView && NumberOneSelector.this.mHour != null) {
                    NumberOneSelector.this.mHour.smoothScrollBy(-1);
                }
                return true;
            } else if (theKey != HMKeyEvent.KEY_DOWN) {
                return theKey == HMKeyEvent.KEY_CENTER ? false : false;
            } else {
                if (NumberOneSelector.this.parentPickerView != null && NumberOneSelector.this.parentPickerView.getCurrentIndex() == NumberOneSelector.this.parentPickerView.getCount() - 1) {
                    return false;
                }
                if (SelectView.HOUR == NumberOneSelector.this.currentView && NumberOneSelector.this.mHour != null) {
                    NumberOneSelector.this.mHour.smoothScrollBy(1);
                }
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    class C04762 implements OnTouchListener {
        C04762() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C1070R.id.picker1) {
                NumberOneSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C1070R.id.picker2) {
                NumberOneSelector.this.setCurrentView(SelectView.MINUTES);
            }
            return false;
        }
    }

    public enum SelectView {
        HOUR,
        MINUTES,
        AM_OR_PM
    }

    public NumberOneSelector(@NonNull Context context) {
        this(context, null);
    }

    public NumberOneSelector(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberOneSelector(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.currentView = SelectView.HOUR;
        this.canAccept = true;
        this.clickListener = new C04762();
        initContent();
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04751());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public void setParentPickerView(NumberPickerView parentPickerView) {
        this.parentPickerView = parentPickerView;
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public SelectView getCurrentView() {
        return this.currentView;
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public void setData(String[] column1) {
        setData(column1, null);
    }

    public void setData(String[] column1, String[] columnSub1) {
        this.mHours = column1;
        this.mHoursSub = columnSub1;
        inflateContent();
    }

    private void initContent() {
        this.mHours = new String[0];
    }

    public void setIndex(int hourIndex) {
        this.mHoureIndex = hourIndex;
        if (this.mHour != null) {
            this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        }
    }

    private void inflateContent() {
        setCurrentView(SelectView.HOUR);
        this.mHour.setDisplayedValues(this.mHours, this.mHoursSub);
        this.mHour.setMaxValue(this.mHours.length - 1);
        this.mHour.setMinValue(0);
        this.mHour.setPickedIndexRelativeToMin(this.mHoureIndex);
        this.mHour.setOnTouchListener(this.clickListener);
        if (this.mHoursSub != null && this.mHoursSub.length > 0) {
            this.mHour.isSubtitle(true);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mHour = (NumberView) findViewById(C1070R.id.picker1);
        this.mHour.setOnValueChangedListener(this);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }

    public int[] getSelectIndex() {
        int[] result = new int[2];
        result[0] = this.mHour.getValue();
        return result;
    }

    public void setWidthColumn1(int widthColumn1) {
        this.widthColumn1 = widthColumn1;
        if (this.mHour != null) {
            this.mHour.setWidth(widthColumn1);
        }
    }

    public void setSelectTextSizeColumn1(int size) {
        if (this.mHour != null) {
            this.mHour.setSelectTextSize(size);
        }
    }

    public void setVisibilityColumn1(int visibility) {
        if (this.mHour != null) {
            this.mHour.setVisibility(visibility);
        }
    }

    public void setOnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mHour != null) {
            this.mHour.setOnValueChangedListener(onchangedLisenter);
        }
    }

    public NumberView getHourView() {
        return this.mHour;
    }
}
