package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.view.View;

public class HmUnlockView extends View {
    private int bgColor;
    private int fgColor;
    private RectF mBounds;
    private int mCenterX;
    private int mCenterY;
    private int mDegree;
    private int mLockStatus;
    private Paint mPaint;
    private int minSize;
    private Runnable onEnd;
    private Runnable onRebackRunnable;
    private float radius;
    private float strokeWidth;

    class C04611 implements AnimatorUpdateListener {
        final /* synthetic */ HmUnlockView this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            this.this$0.mDegree = ((Integer) animation.getAnimatedValue()).intValue();
            this.this$0.invalidate();
        }
    }

    class C04622 implements AnimatorListener {
        boolean isCancel;
        final /* synthetic */ HmUnlockView this$0;

        public void onAnimationStart(Animator animation) {
            this.isCancel = false;
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            if (!(this.this$0.onEnd == null || this.isCancel)) {
                this.this$0.onEnd.run();
            }
            if (this.this$0.onRebackRunnable != null && !this.isCancel) {
                this.this$0.onRebackRunnable.run();
            }
        }

        public void onAnimationCancel(Animator animation) {
            this.isCancel = true;
        }
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        this.mBounds.set(((float) this.mCenterX) - this.radius, ((float) this.mCenterY) - this.radius, ((float) this.mCenterX) + this.radius, ((float) this.mCenterY) + this.radius);
    }

    protected void onDraw(Canvas canvas) {
        this.mPaint.setColor(this.bgColor);
        canvas.drawArc(this.mBounds, -235.0f, 290.0f, false, this.mPaint);
        if (this.mDegree > 0) {
            this.mPaint.setColor(this.fgColor);
            if (this.mLockStatus == 0 || this.mLockStatus == 2) {
                canvas.drawArc(this.mBounds, -235.0f, (float) this.mDegree, false, this.mPaint);
            } else if (this.mLockStatus == 3 || this.mLockStatus == 4) {
                canvas.drawArc(this.mBounds, 55.0f, (float) (-this.mDegree), false, this.mPaint);
            }
        }
        this.mPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        canvas.drawCircle((float) this.mCenterX, (float) this.mCenterY, 160.0f + this.strokeWidth, this.mPaint);
        this.mPaint.setXfermode(null);
    }

    public int getMinimumWidth() {
        return this.minSize;
    }

    public int getMinimumHeight() {
        return this.minSize;
    }
}
