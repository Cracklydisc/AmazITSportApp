package com.huami.watch.common.widget;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import com.huami.watch.utils.QuadInterpolator;

public class HmHaloImageView extends HmAnimatedOverlayImageView {
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private float SCALE_FROM;
    private float SCALE_TO;
    private boolean clickFinished;
    private Runnable clickTask;
    private boolean mLoading;
    private OnClickListener mOnClickListener;
    private IClickStateListener mStateListener;
    private boolean onClickAfterAnimation;

    class C04251 implements Runnable {
        final /* synthetic */ HmHaloImageView this$0;

        public void run() {
            if (this.this$0.mOnClickListener != null) {
                this.this$0.mOnClickListener.onClick(this.this$0);
            }
            this.this$0.clickFinished = true;
        }
    }

    class C04262 implements OnClickListener {
        C04262() {
        }

        public void onClick(View v) {
            if (HmHaloImageView.this.mStateListener != null) {
                HmHaloImageView.this.mStateListener.enterClickState();
            }
            if (!HmHaloImageView.this.clickFinished) {
                return;
            }
            if (HmHaloImageView.this.onClickAfterAnimation) {
                if (HmHaloImageView.this.mLoading) {
                    HmHaloImageView.this.playLoadingAnimationAfterClick(HmHaloImageView.this.clickTask);
                } else {
                    HmHaloImageView.this.playClickAnimation(HmHaloImageView.this.clickTask);
                }
                HmHaloImageView.this.clickFinished = false;
                return;
            }
            if (HmHaloImageView.this.mLoading) {
                HmHaloImageView.this.playLoadingAnimationAfterClick(null);
            } else {
                HmHaloImageView.this.playClickAnimation(null);
            }
            if (HmHaloImageView.this.clickTask != null) {
                HmHaloImageView.this.clickTask.run();
            }
            HmHaloImageView.this.clickFinished = true;
        }
    }

    public interface IClickStateListener {
        void enterClickState();
    }

    static {
        HmAnimatedOverlayImageView.registerAnimatorDrawableTypeInternal(-101, HmOverlayClickHaloDrawble.class);
        HmAnimatedOverlayImageView.registerAnimatorDrawableTypeInternal(-102, HmOverlayLoadingDrawble.class);
    }

    private void initClickTask() {
        super.setOnClickListener(new C04262());
    }

    public void playClickAnimation(final Runnable callback) {
        ScaleAnimation mScaleAnim = new ScaleAnimation(this.SCALE_FROM, this.SCALE_TO, this.SCALE_FROM, this.SCALE_TO, 1, 0.5f, 1, 0.5f);
        mScaleAnim.setInterpolator(QUAD_OUT);
        mScaleAnim.setDuration(125);
        mScaleAnim.setRepeatCount(1);
        mScaleAnim.setRepeatMode(2);
        mScaleAnim.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                HmHaloImageView.this.playOverlayDrawableAnimation(callback, -101);
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(mScaleAnim);
    }

    public void playLoadingAnimationAfterClick(final Runnable callback) {
        ScaleAnimation mScaleAnim = new ScaleAnimation(this.SCALE_FROM, this.SCALE_TO, this.SCALE_FROM, this.SCALE_TO, 1, 0.5f, 1, 0.5f);
        mScaleAnim.setInterpolator(QUAD_OUT);
        mScaleAnim.setDuration(125);
        mScaleAnim.setRepeatCount(1);
        mScaleAnim.setRepeatMode(2);
        mScaleAnim.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                HmHaloImageView.this.setEnabled(false);
            }

            public void onAnimationRepeat(Animation animation) {
                HmHaloImageView.this.playOverlayDrawableAnimation(callback, -101, -102);
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(mScaleAnim);
    }

    public void setOnClickListener(OnClickListener l) {
        if (l != null) {
            if (!isClickable()) {
                setClickable(true);
            }
            initClickTask();
        } else {
            super.setOnClickListener(null);
            this.clickFinished = true;
        }
        this.mOnClickListener = l;
    }
}
