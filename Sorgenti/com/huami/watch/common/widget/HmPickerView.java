package com.huami.watch.common.widget;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.support.wearable.view.WearableListView;
import android.support.wearable.view.WearableListView.Adapter;
import android.support.wearable.view.WearableListView.ClickListener;
import android.support.wearable.view.WearableListView.OnScrollListener;
import android.support.wearable.view.WearableListView.ViewHolder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.picker.TeaserLayout;
import com.huami.watch.picker.TeaserLayout.TeaserViewInfo;
import com.huami.watch.ui.C1070R;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HmPickerView extends TeaserLayout implements KeyeventConsumer {
    private static final float DEFAULT_HINT_TEXT_SIZE = 14.0f;
    private static final float DEFAULT_TEXT_SIZE = 60.0f;
    private final int DEFAUT_MIN_ALPA;
    private final float DEFAUT_SCALE;
    private boolean canAccept;
    private int focusedLineMarginRightAndLeft;
    private boolean hasFocusedLine;
    private boolean isItemTextSingleLine;
    private KeyeventProcessor keyeventProcessor;
    private AbsBasePickerViewAdapter mAdpater;
    private IOnButtonClickListener mButtonClickListener;
    private ImageView mDefaultClickView;
    private HmAnimatedVectorDrawable mDefautAnimatedVectorDrawable;
    private FirstColumnScrollListener mFirstColumnScrollListener;
    private WearableListView mFirstListView;
    private View mFirstUnitView;
    private View mFirstView;
    private IOnSelectChangeListener mOnSelectChangeListener;
    private int[] mSelected;
    private List<View> mViewsToRemoveWhileCloseSlide;

    class C04521 implements EventCallBack {
        C04521() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP || theKey == HMKeyEvent.KEY_DOWN || theKey != HMKeyEvent.KEY_CENTER) {
                return false;
            }
            if (HmPickerView.this.getCurrentIndex() != HmPickerView.this.getCount() - 1) {
                HmPickerView.this.setCenterIndex(Math.min(HmPickerView.this.getCount(), HmPickerView.this.getCurrentIndex() + 1), true);
                return true;
            } else if (HmPickerView.this.mDefaultClickView == null) {
                return true;
            } else {
                HmPickerView.this.mDefaultClickView.performClick();
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    class C04554 implements OnClickListener {
        C04554() {
        }

        public void onClick(View view) {
            if (HmPickerView.this.mFirstColumnScrollListener == null || HmPickerView.this.mFirstColumnScrollListener.mIndex != 0) {
                HmPickerView.this.mButtonClickListener.onSelectedConfirm();
            } else {
                HmPickerView.this.mButtonClickListener.onClose();
            }
        }
    }

    public static abstract class AbsBasePickerViewAdapter {
        protected Context mContext = null;
        private ImageView mDefaultPositiveClickView;

        public abstract int getCount();

        public abstract View getPickerView(int i);

        public AbsBasePickerViewAdapter(Context context) {
            this.mContext = context;
        }

        public ImageView getDefaultPositiveClickView() {
            if (this.mDefaultPositiveClickView == null) {
                ImageView pickerPositionView = new ImageView(this.mContext);
                pickerPositionView.setImageResource(C1070R.drawable.time_picker_ok_vector);
                this.mDefaultPositiveClickView = pickerPositionView;
            }
            return this.mDefaultPositiveClickView;
        }

        public boolean hasDefaultButton() {
            return true;
        }
    }

    public static abstract class AbsPickerViewAdapter extends AbsBasePickerViewAdapter {
        private HmPickerView pickerView;

        public abstract int columnCount();

        public abstract String getDisplayString(int i, int i2);

        public abstract int getInitRow(int i);

        public abstract int rowCount(int i);

        public AbsPickerViewAdapter(Context context) {
            super(context);
        }

        private final void setPickerView(HmPickerView pickerView) {
            this.pickerView = pickerView;
            pickerView.initParams(columnCount());
        }

        public int getCount() {
            return columnCount();
        }

        public View getPickerView(int position) {
            if (this.pickerView == null) {
                return null;
            }
            TeaserViewCache cache = new TeaserViewCache();
            View content = this.pickerView.initRowContent(this, position, cache);
            content.setLayoutParams(new LayoutParams(-2, -2));
            cache.index = position;
            content.setTag(cache);
            return content;
        }

        public String getUnit(int column) {
            return "";
        }

        public float getUnitSize() {
            return this.mContext.getResources().getDimension(C1070R.dimen.pickview_unit_size);
        }

        public String getTitle() {
            return "";
        }

        public float getColumnWidth(int column) {
            return 125.0f;
        }

        public int getTextColor() {
            return -1;
        }

        public float getTextSize() {
            return HmPickerView.DEFAULT_TEXT_SIZE;
        }

        public boolean hideNegativeButton() {
            return false;
        }

        public String getNegativeButtonContent() {
            return this.mContext.getResources().getString(C1070R.string.picker_default_nagetive_text);
        }

        public float getNegativeButtonContentTextSize() {
            return this.mContext.getResources().getDimension(C1070R.dimen.picker_default_nagetive_text_size);
        }

        public int getHintTextColor() {
            return -1;
        }

        public float getHintTextSize() {
            return HmPickerView.DEFAULT_HINT_TEXT_SIZE;
        }

        public boolean showItemHint() {
            return false;
        }

        public boolean showItemBottom() {
            return false;
        }

        public String getItemBottomStringByPosition(int position) {
            return null;
        }

        public String getItemHintDisplayString() {
            return null;
        }

        public int[] getItemHintDisplayPosition() {
            return null;
        }
    }

    private static class ActivatorPassivator implements com.huami.watch.picker.TeaserLayout.ActivatorPassivator {
        public void onActivate(View view, int index) {
            view.setEnabled(true);
            view.setActivated(true);
            view.setClickable(true);
            view.setImportantForAccessibility(1);
        }

        public void onPassivate(View view, int index) {
            view.setEnabled(false);
            view.setActivated(false);
            view.setClickable(false);
            view.setImportantForAccessibility(2);
        }

        private ActivatorPassivator() {
        }
    }

    private class AdvanceForFirstItemListener implements ClickListener {
        public void onClick(ViewHolder viewholder) {
            MyViewHolder myViewHolder = (MyViewHolder) viewholder;
            int holderPosition = viewholder.getAdapterPosition() - 1;
            if (holderPosition == HmPickerView.this.mSelected[myViewHolder.mPickerViewIndex]) {
                HmPickerView.this.setCenterIndex(myViewHolder.mPickerViewIndex + 1, true);
            }
            if (holderPosition == -1) {
                HmPickerView.this.setCenterIndex(myViewHolder.mPickerViewIndex + 1, true);
            }
        }

        public void onTopEmptyRegionClick() {
        }

        private AdvanceForFirstItemListener() {
        }
    }

    private class AdvanceForItemListener implements ClickListener {
        public void onClick(ViewHolder viewholder) {
            MyViewHolder myViewHolder = (MyViewHolder) viewholder;
            if (viewholder.getAdapterPosition() == HmPickerView.this.mSelected[myViewHolder.mPickerViewIndex]) {
                HmPickerView.this.setCenterIndex(myViewHolder.mPickerViewIndex + 1, true);
            }
        }

        public void onTopEmptyRegionClick() {
        }

        private AdvanceForItemListener() {
        }
    }

    private class AutoAdvanceListener implements Listener {
        public void onCenterIndexChanged(int centerIndex) {
            if (centerIndex == 1 && HmPickerView.this.mOnSelectChangeListener != null) {
                HmPickerView.this.mOnSelectChangeListener.onChange(HmPickerView.this.getCenterIndex(), HmPickerView.this.mSelected, true);
            }
        }

        private AutoAdvanceListener() {
        }

        public void onScrollListener(List<TeaserViewInfo> infos) {
            for (TeaserViewInfo info : infos) {
                if (info.view != HmPickerView.this.mDefaultClickView) {
                    float scale = 0.6f + ((1.0f - Math.abs(info.percent)) * 0.39999998f);
                    info.view.setScaleX(scale);
                    info.view.setScaleY(scale);
                    TeaserViewCache tag = info.view.getTag();
                    if (tag instanceof TeaserViewCache) {
                        TeaserViewCache cache = tag;
                        if (cache.mUnitView != null) {
                            cache.mUnitView.setAlpha(1.0f - Math.abs(info.percent));
                        }
                        Iterator<MyViewHolder> hoIterator = cache.holders.iterator();
                        float centerItemAlpha = 163.0f + ((1.0f - Math.abs(info.percent)) * 93.0f);
                        while (hoIterator.hasNext()) {
                            MyViewHolder holder = (MyViewHolder) hoIterator.next();
                            if (holder.isCenter) {
                                holder.setAlpha(centerItemAlpha / 256.0f);
                            } else {
                                holder.setAlpha(1.0f - Math.abs(info.percent));
                            }
                        }
                    }
                } else {
                    HmPickerView.this.mDefautAnimatedVectorDrawable.setCurrentPlayTime(outInteploe((1.0f - info.percent) * 2.0f));
                }
            }
        }

        private float outInteploe(float input) {
            return input == 1.0f ? 1.0f : 1.0f + (-((float) Math.pow(2.0d, (double) (-10.0f * input))));
        }
    }

    private class ColumnScrollListener implements OnScrollListener {
        private boolean centerHasCalled;
        private int mCenterIndex;
        private int mCenterPositon;
        private int mColumn;
        private int mItemHeight;
        private int mLastSelect;
        private WearableListView mListView;
        private boolean mWithHead;

        private ColumnScrollListener(boolean withHead, int column, WearableListView listView) {
            this.mWithHead = false;
            this.mColumn = -1;
            this.mLastSelect = 0;
            this.mCenterPositon = 0;
            this.mItemHeight = 0;
            this.mCenterIndex = 0;
            this.centerHasCalled = false;
            this.mWithHead = withHead;
            this.mColumn = column;
            this.mListView = listView;
        }

        public void setCenterInfo(int centertIndex) {
            if (!this.centerHasCalled) {
                this.mCenterIndex = centertIndex;
                this.mCenterPositon = getCenterYPos(this.mListView);
                this.mItemHeight = getItemHeight();
            }
        }

        private int getCenterYPos(View view) {
            return (view.getTop() + view.getPaddingTop()) + (view.getHeight() / 2);
        }

        private int getItemHeight() {
            return ((this.mListView.getHeight() - this.mListView.getPaddingBottom()) - this.mListView.getPaddingTop()) / 3;
        }

        public void onScroll(int i) {
        }

        private float getScale(float perctent) {
            return 1.0f - (0.39999998f * perctent);
        }

        public void onAbsoluteScrollChange(int j) {
            int childCount = this.mListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = this.mListView.getChildAt(i);
                MyViewHolder holder = (MyViewHolder) this.mListView.getChildViewHolder(child);
                holder.setTextViewColor(this.mCenterIndex == holder.getLayoutPosition());
                float scale = getScale(Math.min((((float) Math.abs(getCenterYPos(child) - this.mCenterPositon)) * 1.0f) / ((float) this.mItemHeight), 1.0f));
                child.setScaleX(scale);
                child.setScaleY(scale);
            }
        }

        public void onScrollStateChanged(int i) {
            if (i == 0 && HmPickerView.this.mOnSelectChangeListener != null) {
                HmPickerView.this.mOnSelectChangeListener.onChange(HmPickerView.this.getCenterIndex(), HmPickerView.this.mSelected, true);
            }
        }

        public void onCentralPositionChanged(int i) {
            int selectRow;
            this.centerHasCalled = true;
            this.mCenterIndex = i;
            if (this.mWithHead) {
                selectRow = i - 1;
            } else {
                selectRow = i;
            }
            if (selectRow >= 0) {
                HmPickerView.this.mSelected[this.mColumn] = selectRow;
                if (!(selectRow == this.mLastSelect || HmPickerView.this.mOnSelectChangeListener == null)) {
                    HmPickerView.this.mOnSelectChangeListener.onChange(HmPickerView.this.getCenterIndex(), HmPickerView.this.mSelected, false);
                }
                this.mLastSelect = selectRow;
                this.mCenterPositon = getCenterYPos(this.mListView);
                this.mItemHeight = getItemHeight();
            }
        }
    }

    private class FirstColumnScrollListener implements OnScrollListener {
        private int mIndex;

        private FirstColumnScrollListener() {
            this.mIndex = 1;
        }

        public void onScroll(int i) {
        }

        public void onAbsoluteScrollChange(int i) {
        }

        public void onScrollStateChanged(int i) {
        }

        public void onCentralPositionChanged(int i) {
            if (this.mIndex == 0 && i > 0) {
                int index = 1;
                HmPickerView.this.mFirstUnitView.setVisibility(0);
                for (View view : HmPickerView.this.mViewsToRemoveWhileCloseSlide) {
                    if (view.getParent() == null) {
                        int index2 = index + 1;
                        HmPickerView.this.addView(view, index, new LayoutParams(-2, -1));
                        index = index2;
                    }
                }
                HmPickerView.this.requestLayout();
            } else if (this.mIndex > 0 && i == 0) {
                HmPickerView.this.mFirstUnitView.setVisibility(4);
                for (View view2 : HmPickerView.this.mViewsToRemoveWhileCloseSlide) {
                    HmPickerView.this.removeView(view2);
                }
                HmPickerView.this.requestLayout();
            }
            this.mIndex = i;
        }
    }

    public interface IOnButtonClickListener {
        void onClose();

        void onSelectedConfirm();
    }

    public interface IOnSelectChangeListener {
        void onChange(int i, int[] iArr, boolean z);
    }

    private class MyViewHolder extends ViewHolder {
        private boolean hasInital = false;
        private boolean isCenter;
        ColumnScrollListener mColumnScrollListener;
        private ViewGroup mContainer;
        boolean mHideClose = false;
        public TextView mHintTextView = null;
        private int mPickerViewIndex = 0;
        public TextView mTextView = null;
        private View mUnitView;

        public void setAlpha(float alpha) {
            if (this.mContainer != null) {
                this.mContainer.setAlpha(alpha);
            }
        }

        public MyViewHolder(View itemView, int pickerViewIndex, View unitView, ColumnScrollListener columnScrollListener, boolean hide) {
            super(itemView);
            this.mContainer = (LinearLayout) itemView;
            this.mColumnScrollListener = columnScrollListener;
            this.mUnitView = unitView;
            this.mPickerViewIndex = pickerViewIndex;
            this.mTextView = (TextView) this.mContainer.getChildAt(0);
            this.mHintTextView = (TextView) this.mContainer.getChildAt(1);
            this.mHideClose = hide;
        }

        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (isCentralItem) {
                this.mColumnScrollListener.setCenterInfo(HmPickerView.this.getCenterIndex());
            }
            if (isCentralItem && this.mPickerViewIndex == 0 && ((Integer) this.mContainer.getTag()).intValue() == 0 && !this.mHideClose) {
                this.mUnitView.setVisibility(4);
            } else if (isCentralItem) {
                this.mUnitView.setVisibility(0);
            }
            this.isCenter = isCentralItem;
        }

        public void setTextViewColor(boolean isCenter) {
            int mColor;
            if (isCenter) {
                mColor = -1;
            } else {
                mColor = Color.argb(163, 255, 255, 255);
            }
            this.mTextView.setTextColor(mColor);
            this.mHintTextView.setTextColor(mColor);
        }

        public void setHolderViewScacle(boolean isCenter) {
            if (!this.hasInital) {
                if (isCenter) {
                    this.mContainer.setScaleX(1.0f);
                    this.mContainer.setScaleY(1.0f);
                } else {
                    this.mContainer.setScaleX(1.0f);
                    this.mContainer.setScaleY(1.0f);
                }
                this.hasInital = true;
            }
        }
    }

    private static class TeaserViewCache {
        private LinkedList<MyViewHolder> holders;
        private int index;
        private View mUnitView;

        private TeaserViewCache() {
            this.holders = new LinkedList();
        }
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04521());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    private void initListIndex(int index) {
        int count = getChildCount();
        if (count >= 2) {
            int i = 0;
            while (i < count - 1) {
                View childView = getChildAt(i);
                if (childView instanceof HmLinearLayout) {
                    HmWearableListView listView = (HmWearableListView) childView.findViewById(C1070R.id.picker_listview);
                    if (listView != null) {
                        listView.setHasFocusedSelector(this.hasFocusedLine, this.focusedLineMarginRightAndLeft);
                        if (i == 0) {
                            listView.setCannotClickOnCenterKey(true);
                            listView.setInitCurrentIndex(index);
                        }
                        i++;
                    } else {
                        return;
                    }
                }
                return;
            }
        }
    }

    public void scrollToCorrectPosition() {
        if (getChildCount() >= 2) {
            final HmWearableListView listView = (HmWearableListView) getChildAt(1).findViewById(C1070R.id.picker_listview);
            if (listView != null) {
                if (this.hasFocusedLine) {
                    listView.setHasFocusedSelector(this.hasFocusedLine, this.focusedLineMarginRightAndLeft);
                }
                postDelayed(new Runnable() {
                    public void run() {
                        listView.smoothScrollToPosition(1);
                    }
                }, 150);
            }
        }
    }

    public void scrollToCorrectPosition(int pos) {
        if (getChildCount() > pos) {
            final HmWearableListView listView = (HmWearableListView) getChildAt(pos).findViewById(C1070R.id.picker_listview);
            if (listView != null) {
                if (this.hasFocusedLine) {
                    listView.setHasFocusedSelector(this.hasFocusedLine, this.focusedLineMarginRightAndLeft);
                }
                postDelayed(new Runnable() {
                    public void run() {
                        listView.smoothScrollToPosition(1);
                    }
                }, 150);
            }
        }
    }

    public void setCenterIndex(int centerIndex, boolean animate) {
        super.setCenterIndex(centerIndex, animate);
        int i = 0;
        while (i < getCount()) {
            setChildViewCanAccept(i, getCurrentIndex() == i);
            i++;
        }
    }

    public void setChildViewCanAccept(int pos, boolean canAccept) {
        if (getChildCount() > pos) {
            HmWearableListView listView = (HmWearableListView) getChildAt(pos).findViewById(C1070R.id.picker_listview);
            if (listView != null) {
                if (this.hasFocusedLine) {
                    listView.setHasFocusedSelector(this.hasFocusedLine, this.focusedLineMarginRightAndLeft);
                }
                listView.setCannotClickOnCenterKey(true);
                listView.setCanAccept(canAccept);
            }
        }
    }

    public void setHasFocusedLine(boolean hasFocusedLine) {
        this.hasFocusedLine = hasFocusedLine;
    }

    public void setHasFocusedLine(boolean hasFocusedLine, int focusedLineMarginRightAndLeft) {
        this.hasFocusedLine = hasFocusedLine;
        this.focusedLineMarginRightAndLeft = focusedLineMarginRightAndLeft;
    }

    public void setOnButtonClickListener(IOnButtonClickListener buttonClickListener) {
        this.mButtonClickListener = buttonClickListener;
        if (this.mButtonClickListener != null && this.mDefaultClickView != null) {
            this.mDefaultClickView.setOnClickListener(new C04554());
        }
    }

    public void setOnSelectChangeListener(IOnSelectChangeListener onSelectChangeListener) {
        this.mOnSelectChangeListener = onSelectChangeListener;
    }

    public HmPickerView(Context context) {
        this(context, null);
    }

    public HmPickerView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public HmPickerView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        this.DEFAUT_SCALE = 0.6f;
        this.DEFAUT_MIN_ALPA = 163;
        this.mViewsToRemoveWhileCloseSlide = new LinkedList();
        this.mSelected = null;
        this.mFirstView = null;
        this.mFirstColumnScrollListener = null;
        this.mFirstListView = null;
        this.mFirstUnitView = null;
        this.isItemTextSingleLine = false;
        this.hasFocusedLine = false;
        this.focusedLineMarginRightAndLeft = 0;
        this.canAccept = true;
        this.mOnSelectChangeListener = null;
        this.mButtonClickListener = null;
        setActivatorPassivator(new ActivatorPassivator());
    }

    public void setAdapter(AbsBasePickerViewAdapter adapter) {
        this.mDefaultClickView = null;
        this.mDefautAnimatedVectorDrawable = null;
        if (adapter == null) {
            clear();
            return;
        }
        this.mAdpater = adapter;
        boolean addOtherColumn = true;
        if (adapter instanceof AbsPickerViewAdapter) {
            AbsPickerViewAdapter absadapter = this.mAdpater;
            absadapter.setPickerView(this);
            int firstInit = absadapter.getInitRow(0);
            boolean hasClose;
            if (absadapter.hideNegativeButton()) {
                hasClose = false;
            } else {
                hasClose = true;
            }
            if (firstInit == -1 && hasClose) {
                addOtherColumn = false;
            } else {
                addOtherColumn = true;
            }
        }
        int i = 0;
        while (i < this.mAdpater.getCount()) {
            View contentView = this.mAdpater.getPickerView(i);
            if (addOtherColumn || i == 0) {
                addView(contentView, new LayoutParams(-2, -1));
            }
            i++;
        }
        if (this.mAdpater.hasDefaultButton()) {
            this.mDefaultClickView = this.mAdpater.getDefaultPositiveClickView();
            addView(this.mDefaultClickView, new LayoutParams(-2, -2, 16));
            initVectorDrawable(this.mDefaultClickView.getDrawable());
        }
        setListener(new AutoAdvanceListener());
        setCenterIndex(0, false);
        requestLayout();
        if (adapter instanceof AbsPickerViewAdapter) {
            initListIndex(((AbsPickerViewAdapter) adapter).getInitRow(0));
        }
    }

    private void initVectorDrawable(Drawable drawable) {
        if (drawable instanceof VectorDrawable) {
            this.mDefautAnimatedVectorDrawable = null;
            this.mDefautAnimatedVectorDrawable = new HmAnimatedVectorDrawable((VectorDrawable) drawable);
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("demopath", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_alpha));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("demopath", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_path));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("rightgroup", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_scaley));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("rightgroup", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_scalex));
        }
    }

    public void clear() {
        removeAllViews();
    }

    public int[] getSelected() {
        return this.mSelected;
    }

    public int getSelectedRow(int column) {
        if (column < 0 || column >= this.mSelected.length) {
            return -1;
        }
        return this.mSelected[column];
    }

    private void initParams(int columnCount) {
        this.mSelected = new int[columnCount];
    }

    private View initRowContent(AbsPickerViewAdapter adapter, int position, TeaserViewCache cache) {
        ColumnScrollListener columnScrollListener;
        int initRow;
        cache.index = position;
        int column = position;
        View childView = LayoutInflater.from(getContext()).inflate(C1070R.layout.picker_column_with_title, null);
        String unit = adapter.getUnit(column);
        TextView anchorTextView = (TextView) childView.findViewById(C1070R.id.anchor_view);
        View unitView = (TextView) childView.findViewById(C1070R.id.picker_unit);
        TextView titleView = (TextView) childView.findViewById(C1070R.id.title);
        String title = adapter.getTitle();
        if (!TextUtils.isEmpty(title)) {
            titleView.setVisibility(0);
            titleView.setText(title);
        }
        if (unit == null || unit.isEmpty()) {
            anchorTextView.setVisibility(8);
            unitView.setVisibility(8);
        } else {
            unitView.setIncludeFontPadding(false);
            unitView.setText(unit);
            unitView.setTextSize(adapter.getUnitSize());
            anchorTextView.setText(unit);
        }
        cache.mUnitView = unitView;
        WearableListView childListView = (HmWearableListView) childView.findViewById(C1070R.id.picker_listview);
        if (childListView != null && this.hasFocusedLine) {
            childListView.setHasFocusedSelector(this.hasFocusedLine, this.focusedLineMarginRightAndLeft);
        }
        ViewGroup.LayoutParams lp = childListView.getLayoutParams();
        lp.width = (int) adapter.getColumnWidth(position);
        childListView.setLayoutParams(lp);
        HmPickerView hmPickerView;
        if (adapter.hideNegativeButton() || column != 0) {
            columnScrollListener = new ColumnScrollListener(false, column, childListView);
            this.mViewsToRemoveWhileCloseSlide.add(childView);
            childListView.addOnScrollListener(columnScrollListener);
            initRow = adapter.getInitRow(column);
            childListView.scrollToPosition(initRow);
            hmPickerView = this;
            childListView.setClickListener(new AdvanceForItemListener());
            this.mSelected[column] = adapter.getInitRow(column);
        } else {
            this.mFirstView = childView;
            this.mFirstUnitView = unitView;
            this.mFirstListView = childListView;
            initRow = adapter.getInitRow(column) + 1;
            childListView.scrollToPosition(initRow);
            hmPickerView = this;
            childListView.setClickListener(new AdvanceForFirstItemListener());
            this.mSelected[column] = initRow;
            hmPickerView = this;
            this.mFirstColumnScrollListener = new FirstColumnScrollListener();
            this.mFirstColumnScrollListener.mIndex = initRow;
            childListView.addOnScrollListener(this.mFirstColumnScrollListener);
            ColumnScrollListener columnScrollListener2 = new ColumnScrollListener(true, 0, childListView);
            childListView.addOnScrollListener(columnScrollListener2);
        }
        final ColumnScrollListener hoderColumnScrollListener = columnScrollListener;
        final int tmpRow = initRow;
        final AbsPickerViewAdapter absPickerViewAdapter = adapter;
        final int i = position;
        final View view = unitView;
        final TeaserViewCache teaserViewCache = cache;
        final int i2 = column;
        childListView.setAdapter(new Adapter() {
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LinearLayout linearLayout = new LinearLayout(HmPickerView.this.getContext());
                linearLayout.setGravity(17);
                linearLayout.setOrientation(1);
                linearLayout.setEnabled(false);
                linearLayout.setClickable(false);
                linearLayout.setFocusable(false);
                linearLayout.setFocusableInTouchMode(false);
                TextView textView = new TextView(HmPickerView.this.getContext());
                textView.setIncludeFontPadding(false);
                textView.setGravity(17);
                if (HmPickerView.this.isItemTextSingleLine) {
                    textView.setSingleLine();
                }
                textView.setTextColor(absPickerViewAdapter.getTextColor());
                textView.setTextSize(absPickerViewAdapter.getTextSize());
                textView.setEnabled(false);
                textView.setClickable(false);
                textView.setFocusable(false);
                textView.setFocusableInTouchMode(false);
                textView.setIncludeFontPadding(false);
                TextView hintView = new TextView(HmPickerView.this.getContext());
                hintView.setIncludeFontPadding(false);
                hintView.setTextColor(absPickerViewAdapter.getHintTextColor());
                hintView.setTextSize(absPickerViewAdapter.getHintTextSize());
                hintView.setEnabled(false);
                hintView.setClickable(false);
                hintView.setFocusable(false);
                hintView.setFocusableInTouchMode(false);
                hintView.setIncludeFontPadding(false);
                LinearLayout.LayoutParams t_lp = new LinearLayout.LayoutParams(-2, -2);
                t_lp.weight = 8.0f;
                linearLayout.addView(textView, t_lp);
                LinearLayout.LayoutParams hint_lp = new LinearLayout.LayoutParams(-2, -2);
                hint_lp.weight = 1.0f;
                linearLayout.addView(hintView, hint_lp);
                MyViewHolder holder = new MyViewHolder(linearLayout, i, view, hoderColumnScrollListener, absPickerViewAdapter.hideNegativeButton());
                teaserViewCache.holders.add(holder);
                return holder;
            }

            public void onViewRecycled(ViewHolder holder) {
                super.onViewRecycled(holder);
            }

            public void onBindViewHolder(ViewHolder holder, int position) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                myViewHolder.mContainer.setTag(Integer.valueOf(position));
                myViewHolder.mContainer.setAlpha(1.0f);
                myViewHolder.setHolderViewScacle(position == tmpRow);
                if (absPickerViewAdapter.showItemHint()) {
                    int[] hintPos = absPickerViewAdapter.getItemHintDisplayPosition();
                    if (hintPos == null || hintPos.length != 2 || (((absPickerViewAdapter.hideNegativeButton() || hintPos[0] != position - 1) && !(absPickerViewAdapter.hideNegativeButton() && hintPos[0] == position)) || hintPos[1] != i2)) {
                        myViewHolder.mHintTextView.setVisibility(8);
                    } else {
                        myViewHolder.mHintTextView.setVisibility(0);
                    }
                } else if (absPickerViewAdapter.showItemBottom()) {
                    myViewHolder.mHintTextView.setVisibility(0);
                } else {
                    myViewHolder.mHintTextView.setVisibility(8);
                }
                if (absPickerViewAdapter.hideNegativeButton() || i2 != 0) {
                    myViewHolder.mTextView.setText(absPickerViewAdapter.getDisplayString(position, i2));
                    if (myViewHolder.mHintTextView.getVisibility() == 0 && absPickerViewAdapter.showItemHint()) {
                        myViewHolder.mHintTextView.setText(absPickerViewAdapter.getItemHintDisplayString());
                        myViewHolder.mHintTextView.setTextSize(absPickerViewAdapter.getHintTextSize());
                    }
                } else if (position == 0) {
                    myViewHolder.mTextView.setText(absPickerViewAdapter.getNegativeButtonContent());
                    myViewHolder.mTextView.setTextSize(absPickerViewAdapter.getNegativeButtonContentTextSize());
                    if (tmpRow == 0) {
                        HmPickerView.this.mFirstUnitView.setVisibility(4);
                    }
                } else {
                    myViewHolder.mTextView.setTextSize(absPickerViewAdapter.getTextSize());
                    myViewHolder.mTextView.setText(absPickerViewAdapter.getDisplayString(position - 1, i2));
                    if (myViewHolder.mHintTextView.getVisibility() == 0 && absPickerViewAdapter.showItemHint()) {
                        myViewHolder.mHintTextView.setText(absPickerViewAdapter.getItemHintDisplayString());
                        myViewHolder.mHintTextView.setTextSize(absPickerViewAdapter.getHintTextSize());
                    }
                    if (myViewHolder.mHintTextView.getVisibility() == 0 && absPickerViewAdapter.showItemBottom()) {
                        myViewHolder.mHintTextView.setText(absPickerViewAdapter.getItemBottomStringByPosition(position - 1));
                        myViewHolder.mHintTextView.setTextSize(absPickerViewAdapter.getHintTextSize());
                    }
                }
            }

            public int getItemCount() {
                int contentRowCount = absPickerViewAdapter.rowCount(i2);
                return (absPickerViewAdapter.hideNegativeButton() || i2 != 0) ? contentRowCount : contentRowCount + 1;
            }

            public int getItemViewType(int position) {
                return super.getItemViewType(position);
            }
        });
        return childView;
    }

    public boolean canScrollHorizontally(int i) {
        return super.canScrollHorizontally(i);
    }

    public void setItemTextSingleLine(boolean isItemTextSingleLine) {
        this.isItemTextSingleLine = isItemTextSingleLine;
    }
}
