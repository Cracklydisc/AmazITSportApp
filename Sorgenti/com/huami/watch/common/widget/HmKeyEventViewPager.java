package com.huami.watch.common.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;

public class HmKeyEventViewPager extends ViewPager implements EventCallBack, KeyeventConsumer {
    private boolean canAccept = true;
    private boolean isChangedForCenterKey = false;
    private KeyeventProcessor processor;

    public HmKeyEventViewPager(Context context) {
        super(context);
    }

    public HmKeyEventViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        if (this.isChangedForCenterKey) {
            if (theKey != HMKeyEvent.KEY_CENTER) {
                return false;
            }
            setCurrentItem(getCurrentItem() == getChildCount() + -1 ? 0 : getCurrentItem() + 1, true);
            return false;
        } else if (theKey == HMKeyEvent.KEY_DOWN) {
            setCurrentItem(getCurrentItem() + 1, true);
            return true;
        } else if (theKey == HMKeyEvent.KEY_UP) {
            setCurrentItem(getCurrentItem() - 1, true);
            return true;
        } else if (theKey != HMKeyEvent.KEY_CENTER) {
            return false;
        } else {
            View v = getChildAt(getCurrentItem());
            if (v == null) {
                return false;
            }
            v.performClick();
            return false;
        }
    }

    public void setChangedForCenterKey(boolean changedForCenterKey) {
        this.isChangedForCenterKey = changedForCenterKey;
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }
}
