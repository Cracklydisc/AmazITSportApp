package com.huami.watch.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.wearable.view.WearableListView;
import android.support.wearable.view.WearableListView.ClickListener;
import android.support.wearable.view.WearableListView.OnScrollListener;
import android.support.wearable.view.WearableListView.ViewHolder;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;

public class HmWearableListView extends WearableListView implements OnScrollListener, KeyeventConsumer {
    private boolean canAccept = true;
    private boolean cannotClickOnCenterKey = false;
    float deltaY = 0.0f;
    float downY = 0.0f;
    private int focusedSelectorLineMargin = 0;
    private boolean hasFocusedSelector = false;
    private boolean hasFouced = false;
    private boolean hasHeaderView = false;
    private boolean isModeEverestReal = ModelUtil.isRealModelEverest(null);
    private KeyeventProcessor keyeventProcessor;
    private int lastCenterIndex = 0;
    private Paint paint;
    private ClickListener theClickListener;

    class C04651 implements EventCallBack {
        C04651() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP) {
                boolean z;
                boolean access$000 = HmWearableListView.this.lastCenterIndex;
                if (HmWearableListView.this.hasHeaderView) {
                    z = true;
                } else {
                    z = false;
                }
                if (access$000 <= z) {
                    return false;
                }
                HmWearableListView.this.smoothScrollToPosition(HmWearableListView.access$006(HmWearableListView.this));
                return true;
            } else if (theKey == HMKeyEvent.KEY_DOWN) {
                if (HmWearableListView.this.lastCenterIndex >= HmWearableListView.this.getAdapter().getItemCount() - 1) {
                    return false;
                }
                HmWearableListView.this.smoothScrollToPosition(HmWearableListView.access$004(HmWearableListView.this));
                return true;
            } else if (theKey != HMKeyEvent.KEY_CENTER || HmWearableListView.this.cannotClickOnCenterKey) {
                return false;
            } else {
                HmWearableListView.this.lastCenterIndex = HmWearableListView.this.findCenterViewIndex();
                ViewHolder holder = HmWearableListView.this.getChildViewHolder(HmWearableListView.this.getChildAt(HmWearableListView.this.lastCenterIndex));
                if (!(holder == null || HmWearableListView.this.theClickListener == null)) {
                    HmWearableListView.this.theClickListener.onClick(holder);
                }
                if (HmWearableListView.this.getChildAt(HmWearableListView.this.lastCenterIndex) != null) {
                    HmWearableListView.this.getChildAt(HmWearableListView.this.lastCenterIndex).performClick();
                }
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    static /* synthetic */ int access$004(HmWearableListView x0) {
        int i = x0.lastCenterIndex + 1;
        x0.lastCenterIndex = i;
        return i;
    }

    static /* synthetic */ int access$006(HmWearableListView x0) {
        int i = x0.lastCenterIndex - 1;
        x0.lastCenterIndex = i;
        return i;
    }

    public HmWearableListView(Context context) {
        super(context);
        addOnScrollListener(this);
    }

    public HmWearableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        addOnScrollListener(this);
    }

    public HmWearableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addOnScrollListener(this);
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04651());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public void addView(View child, int index) {
        super.addView(child, index);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public void setCannotClickOnCenterKey(boolean cannotClickOnCenterKey) {
        this.cannotClickOnCenterKey = cannotClickOnCenterKey;
    }

    public void onScroll(int i) {
    }

    public void onAbsoluteScrollChange(int i) {
    }

    public void onCentralPositionChanged(int i) {
        this.lastCenterIndex = i;
    }

    public void setClickListener(ClickListener clickListener) {
        super.setClickListener(clickListener);
        this.theClickListener = clickListener;
    }

    public void setInitCurrentIndex(int init) {
        this.lastCenterIndex = init;
    }

    private static int getCenterYPos(View v) {
        return (v.getTop() + v.getPaddingTop()) + (getAdjustedHeight(v) / 2);
    }

    private int findCenterViewIndex() {
        int count = getChildCount();
        int index = -1;
        int closest = Integer.MAX_VALUE;
        int centerY = getCenterYPos(this);
        for (int i = 0; i < count; i++) {
            int distance = Math.abs(centerY - (getTop() + getCenterYPos(getChildAt(i))));
            if (distance < closest) {
                closest = distance;
                index = i;
            }
        }
        return index == -1 ? 0 : index;
    }

    private static int getAdjustedHeight(View v) {
        return (v.getHeight() - v.getPaddingBottom()) - v.getPaddingTop();
    }

    public void setHasFouced(boolean hasFouced) {
        this.hasFouced = hasFouced;
    }

    public void setHasFocusedSelector(boolean hasFocusedSelector, int focusedSelectorLineMargin) {
        this.hasFocusedSelector = hasFocusedSelector;
        this.focusedSelectorLineMargin = focusedSelectorLineMargin;
    }

    public void setFocusedSelectorLineMargin(int focusedSelectorLineMargin) {
        this.focusedSelectorLineMargin = focusedSelectorLineMargin;
    }

    protected void dispatchDraw(Canvas canvas) {
        try {
            if (this.hasFouced && this.isModeEverestReal) {
                View view = getChildAt(findCenterViewIndex());
                if (this.paint == null) {
                    this.paint = new Paint();
                    this.paint.setColor(Color.parseColor("#2D313A"));
                    this.paint.setStyle(Style.FILL);
                }
                canvas.drawRect(0.0f, (float) view.getTop(), (float) getResources().getDisplayMetrics().widthPixels, (float) view.getBottom(), this.paint);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (this.hasFocusedSelector) {
                if (this.paint == null) {
                    this.paint = new Paint();
                }
                this.paint.setColor(Color.parseColor("#00FF34"));
                this.paint.setStyle(Style.FILL);
                view = getChildAt(findCenterViewIndex());
                float lineHeight = TypedValue.applyDimension(0, 3.0f, getContext().getResources().getDisplayMetrics());
                float leftT = (float) this.focusedSelectorLineMargin;
                float rightT = (float) (getWidth() - this.focusedSelectorLineMargin);
                float topB = (float) view.getBottom();
                float bottomB = ((float) view.getBottom()) + lineHeight;
                Canvas canvas2 = canvas;
                canvas2.drawRect(leftT, (float) view.getTop(), rightT, ((float) view.getTop()) + lineHeight, this.paint);
                canvas.drawRect(leftT, topB, rightT, bottomB, this.paint);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.dispatchDraw(canvas);
    }

    public void setHasHeaderView(boolean hasHeaderView) {
        this.hasHeaderView = hasHeaderView;
    }

    public void smoothScrollToPosition(int position) {
        if (position == 0 && this.hasHeaderView) {
            position = 1;
        }
        super.smoothScrollToPosition(position);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.hasHeaderView) {
            if (ev.getAction() == 0) {
                this.downY = ev.getY();
            }
            this.deltaY = ev.getY() - this.downY;
            LayoutManager manager = getLayoutManager();
            int centerIndex = findCenterViewIndex();
            try {
                if (manager.getPosition(getChildAt(centerIndex)) == 1 && getChildAt(0).getTop() >= 0 && this.deltaY > 0.0f) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(HmWearableListView.class.getSimpleName(), " findCenterViewIndex:" + centerIndex + " --- getChildCount:" + getChildCount());
            }
        }
        return super.dispatchTouchEvent(ev);
    }
}
