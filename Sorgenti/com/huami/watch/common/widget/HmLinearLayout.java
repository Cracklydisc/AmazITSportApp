package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

public class HmLinearLayout extends LinearLayout {
    private Matrix f13m;
    private ValueAnimator mAnim;
    private boolean mEnableHalo = false;
    private float mHaloCenterX;
    private float mHaloCenterY;
    private float mHaloRadius;
    private SweepGradient mShader;
    private Paint paint;

    class C04431 implements AnimatorUpdateListener {
        C04431() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmLinearLayout.this.f13m.setRotate(360.0f * ((Float) animation.getAnimatedValue()).floatValue(), HmLinearLayout.this.mHaloCenterX, HmLinearLayout.this.mHaloCenterY);
            HmLinearLayout.this.mShader.setLocalMatrix(HmLinearLayout.this.f13m);
            HmLinearLayout.this.paint.setShader(HmLinearLayout.this.mShader);
            HmLinearLayout.this.invalidate();
        }
    }

    class C04442 extends AnimatorListenerAdapter {
        boolean willNotDraw = false;

        C04442() {
        }

        public void onAnimationStart(Animator animation) {
            HmLinearLayout.this.mEnableHalo = true;
            this.willNotDraw = HmLinearLayout.this.willNotDraw();
            HmLinearLayout.this.setWillNotDraw(false);
        }

        public void onAnimationEnd(Animator animation) {
            HmLinearLayout.this.mEnableHalo = false;
            HmLinearLayout.this.setWillNotDraw(this.willNotDraw);
        }
    }

    class C04453 implements AnimatorUpdateListener {
        final /* synthetic */ HmLinearLayout this$0;
        final /* synthetic */ float val$endRadius;
        final /* synthetic */ float val$startRadius;

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            this.this$0.mHaloRadius = this.val$startRadius + ((this.val$endRadius - this.val$startRadius) * input);
            this.this$0.paint.setAlpha((int) ((1.0f - input) * 163.0f));
            this.this$0.invalidate();
        }
    }

    class C04464 extends AnimatorListenerAdapter {
        final /* synthetic */ HmLinearLayout this$0;
        final /* synthetic */ boolean val$loadingAfter;
        final /* synthetic */ float val$loadingRadius;
        boolean willNotDraw;

        public void onAnimationStart(Animator animation) {
            this.this$0.mEnableHalo = true;
            this.willNotDraw = this.this$0.willNotDraw();
            this.this$0.setWillNotDraw(false);
        }

        public void onAnimationEnd(Animator animation) {
            this.this$0.mEnableHalo = false;
            this.this$0.setWillNotDraw(this.willNotDraw);
            if (this.val$loadingAfter) {
                this.this$0.startLoadingAnimation(this.val$loadingRadius);
            }
        }
    }

    public HmLinearLayout(Context context) {
        super(context);
    }

    public HmLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mEnableHalo) {
            canvas.drawCircle(this.mHaloCenterX, this.mHaloCenterY, this.mHaloRadius, this.paint);
        }
    }

    private void initLoadingPaint() {
        this.paint = new Paint(1);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeWidth(2.0f);
        this.f13m = new Matrix();
        this.mShader = new SweepGradient(this.mHaloCenterX, this.mHaloCenterY, new int[]{16777215, 16777215, -1}, null);
        this.paint.setShader(this.mShader);
    }

    public void startLoadingAnimation(float loadingRadius) {
        if (!this.mEnableHalo) {
            initLoadingPaint();
            this.mHaloRadius = loadingRadius;
            this.mEnableHalo = true;
            this.mAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mAnim.setInterpolator(new LinearInterpolator());
            this.mAnim.setDuration(1000);
            this.mAnim.setRepeatCount(-1);
            this.mAnim.addUpdateListener(new C04431());
            this.mAnim.addListener(new C04442());
            this.mAnim.start();
        }
    }
}
