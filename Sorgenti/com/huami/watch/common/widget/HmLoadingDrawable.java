package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.huami.watch.utils.CubicInterpolator;
import com.huami.watch.utils.QuadInterpolator;
import com.huami.watch.utils.QuintInterpolator;

public class HmLoadingDrawable extends Drawable implements Animatable, Runnable {
    private static final CubicInterpolator CUBIC_OUT = new CubicInterpolator((byte) 1);
    private static final QuadInterpolator QUAD_INOUT = new QuadInterpolator((byte) 2);
    private static final QuintInterpolator QUINT_IN = new QuintInterpolator((byte) 0);
    private float DELTA_RADIUS;
    private int DURATION;
    private int END_COLOR;
    private float RADIUS_END;
    private float RADIUS_START;
    private int START_COLOR;
    private boolean mAnimating;
    private float mCenterX0;
    private float mCenterX0End;
    private float mCenterX1;
    private float mCenterX1End;
    private float mCenterY;
    private float mControl0;
    private float mControl1;
    private float mControl2;
    private int mFps;
    private float mFractionP;
    private float mFractionR;
    private int mFrameDuration;
    private float mJoin;
    private float mJoinX0;
    private float mJoinX1;
    private float mJoinX2;
    private float mJoinX3;
    private float mJoinY0;
    private float mJoinY1;
    private float mJoinY2;
    private float mJoinY3;
    private RectF mLeftR;
    private Runnable mOnEnd;
    private Paint mPaint;
    private Path mPath;
    private ValueAnimator mPathAnim;
    private boolean mPlayingBackwards;
    private float mRadius;
    private RectF mRightR;
    private float mRotate;
    private ValueAnimator mRotateAnim;
    private boolean mRunning;
    private float mScreenCenterX;
    private float mScreenCenterY;
    private float mScreenHeight;
    private float mScreenWidth;
    private boolean mSever;
    private float mStepF;
    private boolean mStopLoop;

    class C04471 implements AnimatorUpdateListener {
        C04471() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmLoadingDrawable.this.mRotate = ((Float) animation.getAnimatedValue()).floatValue() * 360.0f;
        }
    }

    class C04482 implements AnimatorUpdateListener {
        C04482() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmLoadingDrawable.this.update(((Float) animation.getAnimatedValue()).floatValue());
            HmLoadingDrawable.this.invalidateSelf();
        }
    }

    class C04493 extends AnimatorListenerAdapter {
        final /* synthetic */ HmLoadingDrawable this$0;
        final /* synthetic */ Runnable val$callback;

        public void onAnimationEnd(Animator animation) {
            this.this$0.mRotateAnim.removeListener(this);
            this.this$0.mPathAnim.cancel();
            if (this.val$callback != null) {
                this.val$callback.run();
            }
        }
    }

    public HmLoadingDrawable() {
        this(60);
    }

    public HmLoadingDrawable(int fps) {
        this.START_COLOR = -16335535;
        this.END_COLOR = -11908872;
        this.DURATION = 1000;
        this.RADIUS_START = 47.0f;
        this.RADIUS_END = 12.0f;
        this.DELTA_RADIUS = (this.RADIUS_START - this.RADIUS_END) - this.RADIUS_END;
        this.mSever = false;
        this.mFps = 60;
        this.mAnimating = false;
        this.mRunning = false;
        this.mFrameDuration = 16;
        this.mStepF = (((float) this.mFrameDuration) * 1.0f) / ((float) (this.DURATION + this.DURATION));
        this.mPlayingBackwards = false;
        this.mStopLoop = false;
        this.mPath = new Path();
        this.mPaint = new Paint(1);
        this.mPaint.setStyle(Style.FILL_AND_STROKE);
        this.mPaint.setColor(this.START_COLOR);
        this.mLeftR = new RectF();
        this.mRightR = new RectF();
        setFps(fps);
        if (this.mFps == 60) {
            this.mRotateAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mRotateAnim.setInterpolator(QUAD_INOUT);
            this.mRotateAnim.setDuration((long) (this.DURATION + this.DURATION));
            this.mRotateAnim.setRepeatCount(-1);
            this.mRotateAnim.setRepeatMode(1);
            this.mRotateAnim.addUpdateListener(new C04471());
            this.mPathAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mPathAnim.setInterpolator(QUAD_INOUT);
            this.mPathAnim.setDuration((long) this.DURATION);
            this.mPathAnim.setRepeatCount(-1);
            this.mPathAnim.setRepeatMode(2);
            this.mPathAnim.addUpdateListener(new C04482());
        }
    }

    public void setStartColor(int color) {
        this.START_COLOR = color;
    }

    public void setEndColor(int color) {
        this.END_COLOR = color;
    }

    public void setRadius(float startRadius, float endRadius) {
        this.RADIUS_START = startRadius;
        this.RADIUS_END = endRadius;
        this.DELTA_RADIUS = (this.RADIUS_START - this.RADIUS_END) - this.RADIUS_END;
        this.mCenterX0End = (this.mScreenCenterX - this.RADIUS_START) + this.RADIUS_END;
        this.mCenterX1End = (this.mScreenCenterX + this.RADIUS_START) - this.RADIUS_END;
        this.mLeftR.left = this.mScreenCenterX - this.RADIUS_START;
        this.mRightR.right = this.mScreenCenterX + this.RADIUS_START;
    }

    public void start() {
        if (this.mFps == 60) {
            if (!this.mRotateAnim.isStarted()) {
                this.mRotateAnim.setRepeatCount(-1);
                this.mRotateAnim.start();
            }
            if (!this.mPathAnim.isStarted()) {
                this.mPathAnim.start();
            }
        } else if (!isRunning()) {
            this.mAnimating = true;
            this.mStopLoop = false;
            run();
        }
    }

    public void stop() {
        if (this.mFps == 60) {
            this.mRotateAnim.cancel();
            this.mPathAnim.cancel();
            return;
        }
        this.mAnimating = false;
        reset();
        if (isRunning()) {
            unscheduleSelf(this);
        }
    }

    public boolean isRunning() {
        if (this.mFps == 60) {
            return this.mPathAnim.isRunning();
        }
        return this.mRunning;
    }

    protected void onBoundsChange(Rect bounds) {
        this.mScreenWidth = (float) bounds.width();
        this.mScreenHeight = (float) bounds.height();
        this.mScreenCenterX = (float) bounds.centerX();
        this.mScreenCenterY = (float) bounds.centerY();
        this.mCenterX0 = this.mScreenCenterX;
        this.mCenterX1 = this.mScreenCenterX;
        this.mCenterY = this.mScreenCenterY;
        this.mCenterX0End = (this.mScreenCenterX - this.RADIUS_START) + this.RADIUS_END;
        this.mCenterX1End = (this.mScreenCenterX + this.RADIUS_START) - this.RADIUS_END;
        this.mLeftR.set(this.mCenterX0 - this.RADIUS_START, this.mCenterY - this.RADIUS_START, this.mCenterX0 + this.RADIUS_START, this.mCenterY + this.RADIUS_START);
        this.mRightR.set(this.mCenterX1 - this.RADIUS_START, this.mCenterY - this.RADIUS_START, this.mCenterX1 + this.RADIUS_START, this.mCenterY + this.RADIUS_START);
    }

    public void draw(Canvas canvas) {
        int saveCount = canvas.save(1);
        canvas.rotate(this.mRotate, this.mScreenCenterX, this.mScreenCenterY);
        canvas.drawPath(this.mPath, this.mPaint);
        canvas.restoreToCount(saveCount);
    }

    private void update(float input) {
        this.mCenterX0 = this.mScreenCenterX + ((this.mCenterX0End - this.mScreenCenterX) * input);
        this.mCenterX1 = this.mScreenCenterX + ((this.mCenterX1End - this.mScreenCenterX) * input);
        this.mRadius = this.RADIUS_START + ((this.RADIUS_END - this.RADIUS_START) * input);
        RectF rectF = this.mLeftR;
        float f = this.mCenterY - this.mRadius;
        this.mRightR.top = f;
        rectF.top = f;
        rectF = this.mLeftR;
        f = this.mCenterY + this.mRadius;
        this.mRightR.bottom = f;
        rectF.bottom = f;
        this.mLeftR.right = this.mCenterX0 + this.mRadius;
        this.mRightR.left = this.mCenterX1 - this.mRadius;
        this.mPaint.setColor(evaluateColor(input, this.START_COLOR, this.END_COLOR));
        this.mSever = input < 0.85f;
        float inputs;
        float f2;
        if (this.mSever) {
            inputs = QUINT_IN.getInterpolation(input / 0.85f);
            this.mJoin = this.mRadius - (this.mRadius * inputs);
            f2 = this.mScreenCenterX;
            this.mJoinX3 = f2;
            this.mJoinX2 = f2;
            this.mJoinX1 = f2;
            this.mJoinX0 = f2;
            f2 = this.mScreenCenterY - this.mJoin;
            this.mJoinY1 = f2;
            this.mJoinY0 = f2;
            f2 = this.mScreenCenterY + this.mJoin;
            this.mJoinY3 = f2;
            this.mJoinY2 = f2;
            this.mControl0 = this.mRadius * inputs;
            this.mControl1 = this.RADIUS_END * inputs;
            this.mControl2 = 0.0f;
        } else {
            inputs = CUBIC_OUT.getInterpolation((input - 0.85f) / 0.14999998f);
            this.mJoin = this.DELTA_RADIUS * inputs;
            f2 = this.mScreenCenterY;
            this.mJoinY3 = f2;
            this.mJoinY2 = f2;
            this.mJoinY1 = f2;
            this.mJoinY0 = f2;
            f2 = this.mScreenCenterX - this.mJoin;
            this.mJoinX3 = f2;
            this.mJoinX0 = f2;
            f2 = this.mScreenCenterX + this.mJoin;
            this.mJoinX2 = f2;
            this.mJoinX1 = f2;
            this.mControl0 = ((-0.5f * inputs) + 1.0f) * this.mRadius;
            if (inputs < 0.4f) {
                this.mControl1 = (this.RADIUS_END * (0.4f - inputs)) / 0.4f;
            } else {
                this.mControl1 = 0.0f;
            }
            this.mControl2 = (this.RADIUS_END * inputs) * 0.5f;
        }
        this.mPath.rewind();
        this.mPath.arcTo(this.mLeftR, 90.0f, 180.0f);
        this.mPath.cubicTo(this.mCenterX0 + this.mControl0, this.mCenterY - this.mRadius, this.mJoinX0 - this.mControl1, this.mJoinY0 - this.mControl2, this.mJoinX0, this.mJoinY0);
        if (this.mSever) {
            this.mPath.lineTo(this.mJoinX3, this.mJoinY3);
        }
        this.mPath.cubicTo(this.mJoinX3 - this.mControl1, this.mJoinY3 + this.mControl2, this.mCenterX0 + this.mControl0, this.mCenterY + this.mRadius, this.mCenterX0, this.mCenterY + this.mRadius);
        this.mPath.close();
        this.mPath.moveTo(this.mJoinX1, this.mJoinY1);
        this.mPath.cubicTo(this.mJoinX1 + this.mControl1, this.mJoinY1 - this.mControl2, this.mCenterX1 - this.mControl0, this.mCenterY - this.mRadius, this.mCenterX1, this.mCenterY - this.mRadius);
        this.mPath.arcTo(this.mRightR, 270.0f, 180.0f);
        this.mPath.cubicTo(this.mCenterX1 - this.mControl0, this.mCenterY + this.mRadius, this.mJoinX2 + this.mControl1, this.mJoinY2 + this.mControl2, this.mJoinX2, this.mJoinY2);
        this.mPath.close();
    }

    private static final int evaluateColor(float fraction, int startColor, int endColor) {
        int startA = (startColor >> 24) & 255;
        int startR = (startColor >> 16) & 255;
        int startG = (startColor >> 8) & 255;
        int startB = startColor & 255;
        return ((((((int) (((float) (((endColor >> 24) & 255) - startA)) * fraction)) + startA) << 24) | ((((int) (((float) (((endColor >> 16) & 255) - startR)) * fraction)) + startR) << 16)) | ((((int) (((float) (((endColor >> 8) & 255) - startG)) * fraction)) + startG) << 8)) | (((int) (((float) ((endColor & 255) - startB)) * fraction)) + startB);
    }

    public void setAlpha(int alpha) {
        this.mPaint.setAlpha(alpha);
    }

    public void setColorFilter(ColorFilter cf) {
        this.mPaint.setColorFilter(cf);
    }

    public int getOpacity() {
        return -3;
    }

    private void setFps(int fps) {
        if (fps < 1) {
            throw new IllegalArgumentException("The value of Fps must be > 1.");
        }
        this.mFps = fps;
        if (this.mFps > 59) {
            this.mFps = 60;
        }
        updateFrameDuration();
    }

    private void updateFrameDuration() {
        this.mFrameDuration = 1000 / this.mFps;
        this.mStepF = (((float) this.mFrameDuration) * 1.0f) / ((float) (this.DURATION + this.DURATION));
    }

    public boolean setVisible(boolean visible, boolean restart) {
        boolean changed = super.setVisible(visible, restart);
        if (!visible) {
            unscheduleSelf(this);
        } else if (restart || changed) {
            nextFrame();
        }
        return changed;
    }

    public void run() {
        nextFrame();
    }

    private void reset() {
        this.mStopLoop = false;
        this.mFractionP = 0.0f;
        this.mFractionR = 0.0f;
        this.mPlayingBackwards = false;
        this.mOnEnd = null;
    }

    public void unscheduleSelf(Runnable what) {
        this.mRunning = false;
        super.unscheduleSelf(what);
    }

    private void nextFrame() {
        boolean z = false;
        this.mFractionR += this.mStepF;
        if (this.mFractionR >= 1.0f) {
            if (this.mStopLoop) {
                this.mFractionR = 0.0f;
                this.mAnimating = false;
            } else {
                this.mFractionR %= 1.0f;
            }
        }
        this.mRotate = 360.0f * QUAD_INOUT.getInterpolation(this.mFractionR);
        this.mFractionP += this.mStepF + this.mStepF;
        if (this.mFractionP >= 1.0f) {
            if (!this.mPlayingBackwards) {
                z = true;
            }
            this.mPlayingBackwards = z;
            this.mFractionP %= 1.0f;
        }
        if (this.mPlayingBackwards) {
            update(QUAD_INOUT.getInterpolation(1.0f - this.mFractionP));
        } else {
            update(QUAD_INOUT.getInterpolation(this.mFractionP));
        }
        invalidateSelf();
        if (this.mAnimating) {
            this.mRunning = true;
            scheduleSelf(this, SystemClock.uptimeMillis() + ((long) this.mFrameDuration));
            return;
        }
        stop();
        if (this.mOnEnd != null) {
            this.mOnEnd.run();
            this.mOnEnd = null;
        }
    }
}
