package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ScrollView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.utils.Utils;

public class HmKeyEventScrollView extends ScrollView implements EventCallBack, KeyeventConsumer {
    private boolean canAccept = true;
    private boolean isScrolledToBottom = false;
    private boolean isScrolledToTop = true;
    private OnKeyCallBackListener mOnKeyCallBackListener;
    private OnScrollTopAndBottomListener mOnScrollChangedListener;
    private KeyeventProcessor processor;

    public interface OnKeyCallBackListener {
        void onKeyCall(HMKeyEvent hMKeyEvent);
    }

    public interface OnScrollTopAndBottomListener {
        void onScrolledToBottom();

        void onScrolledToTop();
    }

    public HmKeyEventScrollView(Context context) {
        super(context);
    }

    public HmKeyEventScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmKeyEventScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        if (this.mOnKeyCallBackListener != null) {
            this.mOnKeyCallBackListener.onKeyCall(theKey);
        }
        if (theKey == HMKeyEvent.KEY_DOWN) {
            smoothScrollTo(0, getScrollY() + Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics()));
            return true;
        } else if (theKey == HMKeyEvent.KEY_UP) {
            smoothScrollTo(0, Math.max(0, getScrollY() - Utils.getKeyScrollDistance(getContext().getResources().getDisplayMetrics())));
            return true;
        } else {
            if (theKey == HMKeyEvent.KEY_CENTER) {
            }
            return false;
        }
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (getScrollY() == 0) {
            this.isScrolledToTop = true;
            this.isScrolledToBottom = false;
        } else if (((getScrollY() + getHeight()) - getPaddingTop()) - getPaddingBottom() == getChildAt(0).getHeight()) {
            this.isScrolledToBottom = true;
            this.isScrolledToTop = false;
        } else {
            this.isScrolledToTop = false;
            this.isScrolledToBottom = false;
        }
        notifyScrollChangedListeners();
    }

    private void notifyScrollChangedListeners() {
        if (this.isScrolledToTop) {
            if (this.mOnScrollChangedListener != null) {
                this.mOnScrollChangedListener.onScrolledToTop();
            }
        } else if (this.isScrolledToBottom && this.mOnScrollChangedListener != null) {
            this.mOnScrollChangedListener.onScrolledToBottom();
        }
    }

    public void setOnScrollTopAndBottomListener(OnScrollTopAndBottomListener onScrollChangedListener) {
        this.mOnScrollChangedListener = onScrollChangedListener;
    }

    public void setOnKeyCallBackListener(OnKeyCallBackListener mOnKeyCallBackListener) {
        this.mOnKeyCallBackListener = mOnKeyCallBackListener;
    }

    public boolean isScrolledToTop() {
        return this.isScrolledToTop;
    }

    public boolean isScrolledToBottom() {
        if (isHeightThanScreen()) {
            return this.isScrolledToBottom;
        }
        return true;
    }

    private boolean isHeightThanScreen() {
        try {
            if (getChildAt(0).getHeight() > getResources().getDisplayMetrics().heightPixels) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }
}
