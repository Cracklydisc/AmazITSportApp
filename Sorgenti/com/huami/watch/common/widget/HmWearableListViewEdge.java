package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Property;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewConfiguration;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@TargetApi(20)
public class HmWearableListViewEdge extends RecyclerView {
    private boolean mCanClick;
    private ClickListener mClickListener;
    public int mCustomBottomViewMaxTop;
    public int mCustomTopViewMaxTop;
    private boolean mGestureDirectionLocked;
    private boolean mGreedyTouchMode;
    private int mInitialOffset;
    private int mLastScrollChange;
    private final int[] mLocation;
    private final int mMaxFlingVelocity;
    private boolean mMaximizeSingleItem;
    private final int mMinFlingVelocity;
    private final AdapterDataObserver mObserver;
    private final List<OnCentralPositionChangedListener> mOnCentralPositionChangedListeners;
    private final Set<OnScrollListener> mOnScrollListeners;
    private OnOverScrollListener mOverScrollListener;
    private boolean mPossibleVerticalSwipe;
    private final Handler mPressedHandler;
    private final Runnable mPressedRunnable;
    private View mPressedView;
    private int mPreviousCentral;
    private final Runnable mReleasedRunnable;
    private Animator mScrollAnimator;
    private Scroller mScroller;
    private SetScrollVerticallyProperty mSetScrollVerticallyProperty;
    private float mStartFirstTop;
    private float mStartX;
    private float mStartY;
    private int mTapPositionX;
    private int mTapPositionY;
    private final float[] mTapRegions;
    private final int mTouchSlop;

    class C04661 implements Runnable {
        final /* synthetic */ HmWearableListViewEdge this$0;

        public void run() {
            this.this$0.mPressedView = this.this$0.getChildAt(this.this$0.findCenterViewIndex());
            this.this$0.mPressedView.setPressed(true);
        }
    }

    class C04672 implements Runnable {
        final /* synthetic */ HmWearableListViewEdge this$0;

        public void run() {
            this.this$0.releasePressedItem();
        }
    }

    class C04693 extends AdapterDataObserver {
        final /* synthetic */ HmWearableListViewEdge this$0;

        class C04681 implements OnLayoutChangeListener {
            C04681() {
            }

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                C04693.this.this$0.removeOnLayoutChangeListener(this);
                if (C04693.this.this$0.getChildCount() > 0) {
                    C04693.this.this$0.animateToCenter();
                }
            }
        }

        public void onChanged() {
            this.this$0.addOnLayoutChangeListener(new C04681());
        }
    }

    class C04704 extends android.support.v7.widget.RecyclerView.OnScrollListener {
        final /* synthetic */ HmWearableListViewEdge this$0;

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0 && this.this$0.getChildCount() > 0) {
                this.this$0.handleTouchUp((MotionEvent) null, newState);
            }
            for (OnScrollListener listener : this.this$0.mOnScrollListeners) {
                listener.onScrollStateChanged(newState);
            }
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            this.this$0.onScroll(dy);
        }
    }

    class C04715 extends SimpleAnimatorListener {
        C04715() {
        }

        public void onAnimationEnd(Animator animator) {
            if (!wasCanceled()) {
                HmWearableListViewEdge.this.mCanClick = true;
            }
        }
    }

    class C04726 extends SimpleAnimatorListener {
        final /* synthetic */ Runnable val$endAction;

        public void onAnimationEnd(Animator animator) {
            if (this.val$endAction != null) {
                this.val$endAction.run();
            }
        }
    }

    public static abstract class Adapter extends android.support.v7.widget.RecyclerView.Adapter<ViewHolder> {
    }

    public interface ClickListener {
        void onClick(ViewHolder viewHolder);

        void onTopEmptyRegionClick();
    }

    private class LayoutManager extends android.support.v7.widget.RecyclerView.LayoutManager {
        private int mAbsoluteScroll;
        private int mFirstPosition;
        private boolean mPushFirstHigher;
        private boolean mUseOldViewTop;
        private boolean mWasZoomedIn;
        final /* synthetic */ HmWearableListViewEdge this$0;

        private int findCenterViewIndex() {
            int count = getChildCount();
            int index = -1;
            int closest = Integer.MAX_VALUE;
            int centerY = HmWearableListViewEdge.getCenterYPos(this.this$0);
            for (int i = 0; i < count; i++) {
                int distance = Math.abs(centerY - (this.this$0.getTop() + HmWearableListViewEdge.getCenterYPos(this.this$0.getLayoutManager().getChildAt(i))));
                if (distance < closest) {
                    closest = distance;
                    index = i;
                }
            }
            if (index != -1) {
                return index;
            }
            throw new IllegalStateException("Can't find central view.");
        }

        private void notifyChildrenAboutProximity(boolean animate) {
            int count = getChildCount();
            int index = findCenterViewIndex();
            int position = 0;
            while (position < count) {
                this.this$0.getChildViewHolder(getChildAt(position)).onCenterProximity(position == index, animate);
                position++;
            }
            position = this.this$0.getChildViewHolder(getChildAt(index)).getPosition();
            if (position != this.this$0.mPreviousCentral) {
                for (OnScrollListener var9 : this.this$0.mOnScrollListeners) {
                    var9.onCentralPositionChanged(position);
                }
                for (OnCentralPositionChangedListener var8 : this.this$0.mOnCentralPositionChangedListeners) {
                    var8.onCentralPositionChanged(position);
                }
                this.this$0.mPreviousCentral = position;
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLayoutChildren(android.support.v7.widget.RecyclerView.Recycler r13, android.support.v7.widget.RecyclerView.State r14) {
            /*
            r12 = this;
            r11 = 0;
            r10 = -1;
            r8 = r12.getHeight();
            r9 = r12.getPaddingBottom();
            r5 = r8 - r9;
            r8 = r12.this$0;
            r8 = r8.getCentralViewTop();
            r9 = r12.this$0;
            r9 = r9.mInitialOffset;
            r4 = r8 + r9;
            r8 = r12.mUseOldViewTop;
            if (r8 == 0) goto L_0x00c0;
        L_0x001e:
            r8 = r12.getChildCount();
            if (r8 <= 0) goto L_0x00c0;
        L_0x0024:
            r1 = r12.findCenterViewIndex();
            r8 = r12.getChildAt(r1);
            r6 = r12.getPosition(r8);
            if (r6 != r10) goto L_0x004e;
        L_0x0032:
            r3 = 0;
            r0 = r12.getChildCount();
        L_0x0037:
            r8 = r1 + r3;
            if (r8 < r0) goto L_0x003f;
        L_0x003b:
            r8 = r1 - r3;
            if (r8 < 0) goto L_0x004e;
        L_0x003f:
            r8 = r1 + r3;
            r2 = r12.getChildAt(r8);
            if (r2 == 0) goto L_0x006b;
        L_0x0047:
            r6 = r12.getPosition(r2);
            if (r6 == r10) goto L_0x006b;
        L_0x004d:
            r1 = r1 + r3;
        L_0x004e:
            if (r6 != r10) goto L_0x007e;
        L_0x0050:
            r8 = r12.getChildAt(r11);
            r4 = r8.getTop();
            r3 = r14.getItemCount();
        L_0x005c:
            r8 = r12.mFirstPosition;
            if (r8 < r3) goto L_0x00ae;
        L_0x0060:
            r8 = r12.mFirstPosition;
            if (r8 <= 0) goto L_0x00ae;
        L_0x0064:
            r8 = r12.mFirstPosition;
            r8 = r8 + -1;
            r12.mFirstPosition = r8;
            goto L_0x005c;
        L_0x006b:
            r8 = r1 - r3;
            r2 = r12.getChildAt(r8);
            if (r2 == 0) goto L_0x007b;
        L_0x0073:
            r6 = r12.getPosition(r2);
            if (r6 == r10) goto L_0x007b;
        L_0x0079:
            r1 = r1 - r3;
            goto L_0x004e;
        L_0x007b:
            r3 = r3 + 1;
            goto L_0x0037;
        L_0x007e:
            r8 = r12.mWasZoomedIn;
            if (r8 != 0) goto L_0x008a;
        L_0x0082:
            r8 = r12.getChildAt(r1);
            r4 = r8.getTop();
        L_0x008a:
            r8 = r12.getPaddingTop();
            if (r4 <= r8) goto L_0x009c;
        L_0x0090:
            if (r6 <= 0) goto L_0x009c;
        L_0x0092:
            r6 = r6 + -1;
            r8 = r12.this$0;
            r8 = r8.getItemHeight();
            r4 = r4 - r8;
            goto L_0x008a;
        L_0x009c:
            if (r6 != 0) goto L_0x00ac;
        L_0x009e:
            r8 = r12.this$0;
            r8 = r8.getCentralViewTop();
            if (r4 <= r8) goto L_0x00ac;
        L_0x00a6:
            r8 = r12.this$0;
            r4 = r8.getCentralViewTop();
        L_0x00ac:
            r12.mFirstPosition = r6;
        L_0x00ae:
            r12.performLayoutChildren(r13, r14, r5, r4);
            r8 = r12.getChildCount();
            if (r8 != 0) goto L_0x00d3;
        L_0x00b7:
            r12.setAbsoluteScroll(r11);
        L_0x00ba:
            r8 = 1;
            r12.mUseOldViewTop = r8;
            r12.mPushFirstHigher = r11;
            return;
        L_0x00c0:
            r8 = r12.mPushFirstHigher;
            if (r8 == 0) goto L_0x00ae;
        L_0x00c4:
            r8 = r12.this$0;
            r8 = r8.getCentralViewTop();
            r9 = r12.this$0;
            r9 = r9.getItemHeight();
            r4 = r8 - r9;
            goto L_0x00ae;
        L_0x00d3:
            r8 = r12.findCenterViewIndex();
            r7 = r12.getChildAt(r8);
            r8 = r7.getTop();
            r9 = r12.this$0;
            r9 = r9.getCentralViewTop();
            r8 = r8 - r9;
            r9 = r12.getPosition(r7);
            r10 = r12.this$0;
            r10 = r10.getItemHeight();
            r9 = r9 * r10;
            r8 = r8 + r9;
            r12.setAbsoluteScroll(r8);
            goto L_0x00ba;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.common.widget.HmWearableListViewEdge.LayoutManager.onLayoutChildren(android.support.v7.widget.RecyclerView$Recycler, android.support.v7.widget.RecyclerView$State):void");
        }

        private void performLayoutChildren(Recycler recycler, State state, int parentBottom, int top) {
            detachAndScrapAttachedViews(recycler);
            if (this.this$0.mMaximizeSingleItem && state.getItemCount() == 1) {
                performLayoutOneChild(recycler, parentBottom);
                this.mWasZoomedIn = true;
            } else {
                performLayoutMultipleChildren(recycler, state, parentBottom, top);
                this.mWasZoomedIn = false;
            }
            if (getChildCount() > 0) {
                notifyChildrenAboutProximity(false);
            }
        }

        private void performLayoutOneChild(Recycler recycler, int parentBottom) {
            int right = getWidth() - getPaddingRight();
            View v = recycler.getViewForPosition(getFirstPosition());
            addView(v, 0);
            measureZoomView(v);
            v.layout(getPaddingLeft(), getPaddingTop(), right, parentBottom);
        }

        private void performLayoutMultipleChildren(Recycler recycler, State state, int parentBottom, int top) {
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int count = state.getItemCount();
            int i = 0;
            while (getFirstPosition() + i < count && top < parentBottom) {
                View v = recycler.getViewForPosition(getFirstPosition() + i);
                addView(v, i);
                measureThirdView(v);
                int bottom = top + this.this$0.getItemHeight();
                v.layout(left, top, right, bottom);
                i++;
                top = bottom;
            }
        }

        private void setAbsoluteScroll(int absoluteScroll) {
            this.mAbsoluteScroll = absoluteScroll;
            for (OnScrollListener listener : this.this$0.mOnScrollListeners) {
                listener.onAbsoluteScrollChange(this.mAbsoluteScroll);
            }
        }

        private void measureView(View v, int height) {
            LayoutParams lp = (LayoutParams) v.getLayoutParams();
            v.measure(android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), ((getPaddingLeft() + getPaddingRight()) + lp.leftMargin) + lp.rightMargin, lp.width, canScrollHorizontally()), android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), ((getPaddingTop() + getPaddingBottom()) + lp.topMargin) + lp.bottomMargin, height, canScrollVertically()));
        }

        private void measureThirdView(View v) {
            measureView(v, (int) (1.0f + (((float) getHeight()) / 3.0f)));
        }

        private void measureZoomView(View v) {
            measureView(v, getHeight());
        }

        public LayoutParams generateDefaultLayoutParams() {
            return new LayoutParams(-1, -2);
        }

        public boolean canScrollVertically() {
            return (getItemCount() == 1 && this.mWasZoomedIn) ? false : true;
        }

        public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
            Log.e("renfei", " wearable scroll ");
            if (getChildCount() == 0) {
                return 0;
            }
            int scrolled = 0;
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int scrollBy;
            if (dy < 0) {
                while (scrolled > dy) {
                    View parentHeight1 = getChildAt(0);
                    if (getFirstPosition() > 0) {
                        scrollBy = Math.min(scrolled - dy, Math.max(-parentHeight1.getTop(), 0));
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        if (getFirstPosition() <= 0 || scrolled <= dy) {
                            break;
                        }
                        this.mFirstPosition--;
                        View scrollBy2 = recycler.getViewForPosition(getFirstPosition());
                        addView(scrollBy2, 0);
                        measureThirdView(scrollBy2);
                        int v1 = parentHeight1.getTop();
                        scrollBy2.layout(left, v1 - this.this$0.getItemHeight(), right, v1);
                    } else {
                        this.mPushFirstHigher = false;
                        int bottomView1 = this.this$0.mOverScrollListener != null ? getHeight() : this.this$0.getTopViewMaxTop();
                        scrollBy = Math.min(-dy, bottomView1 - parentHeight1.getTop());
                        Log.e("renfei", "bottomView1:" + bottomView1 + " top:" + parentHeight1.getTop());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                    }
                }
            } else if (dy > 0) {
                int parentHeight = getHeight();
                while (scrolled < dy) {
                    View bottomView = getChildAt(getChildCount() - 1);
                    if (state.getItemCount() > this.mFirstPosition + getChildCount()) {
                        int scrollBy1 = -Math.min(dy - scrolled, Math.max(bottomView.getBottom() - parentHeight, 0));
                        scrolled -= scrollBy1;
                        offsetChildrenVertical(scrollBy1);
                        if (scrolled >= dy) {
                            break;
                        }
                        View v = recycler.getViewForPosition(this.mFirstPosition + getChildCount());
                        int top = getChildAt(getChildCount() - 1).getBottom();
                        addView(v);
                        measureThirdView(v);
                        v.layout(left, top, right, top + this.this$0.getItemHeight());
                    } else {
                        scrollBy = Math.max(-dy, this.this$0.getBottomViewMaxTop() - bottomView.getBottom());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        break;
                    }
                }
            }
            recycleViewsOutOfBounds(recycler);
            notifyChildrenAboutProximity(true);
            setAbsoluteScroll(this.mAbsoluteScroll + scrolled);
            return scrolled;
        }

        public void scrollToPosition(int position) {
            this.mUseOldViewTop = false;
            if (position > 0) {
                this.mFirstPosition = position - 1;
                this.mPushFirstHigher = true;
            } else {
                this.mFirstPosition = position;
                this.mPushFirstHigher = false;
            }
            requestLayout();
        }

        public void smoothScrollToPosition(RecyclerView recyclerView, State state, int position) {
            SmoothScroller linearSmoothScroller = new SmoothScroller(recyclerView.getContext(), this);
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);
        }

        private void recycleViewsOutOfBounds(Recycler recycler) {
            int i;
            int childCount = getChildCount();
            int parentWidth = getWidth();
            int parentHeight = getHeight();
            boolean foundFirst = false;
            int first = 0;
            int last = 0;
            for (i = 0; i < childCount; i++) {
                View v = getChildAt(i);
                if (v.hasFocus() || (v.getRight() >= 0 && v.getLeft() <= parentWidth && v.getBottom() >= 0 && v.getTop() <= parentHeight)) {
                    if (!foundFirst) {
                        first = i;
                        foundFirst = true;
                    }
                    last = i;
                }
            }
            for (i = childCount - 1; i > last; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            for (i = first - 1; i >= 0; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            if (getChildCount() == 0) {
                this.mFirstPosition = 0;
            } else if (first > 0) {
                this.mPushFirstHigher = true;
                this.mFirstPosition += first;
            }
        }

        public int getFirstPosition() {
            return this.mFirstPosition;
        }

        public void onAdapterChanged(android.support.v7.widget.RecyclerView.Adapter oldAdapter, android.support.v7.widget.RecyclerView.Adapter newAdapter) {
            removeAllViews();
        }
    }

    public interface OnCenterProximityListener {
        void onCenterPosition(boolean z);

        void onNonCenterPosition(boolean z);
    }

    public interface OnCentralPositionChangedListener {
        void onCentralPositionChanged(int i);
    }

    public interface OnOverScrollListener {
        void onOverScroll();
    }

    public interface OnScrollListener {
        void onAbsoluteScrollChange(int i);

        void onCentralPositionChanged(int i);

        void onScroll(int i);

        void onScrollStateChanged(int i);
    }

    private class SetScrollVerticallyProperty extends Property<HmWearableListViewEdge, Integer> {
        public Integer get(HmWearableListViewEdge wearableListView) {
            return Integer.valueOf(wearableListView.mLastScrollChange);
        }

        public void set(HmWearableListViewEdge wearableListView, Integer value) {
            wearableListView.setScrollVertically(value.intValue());
        }
    }

    private static class SmoothScroller extends LinearSmoothScroller {
        private final LayoutManager mLayoutManager;

        public SmoothScroller(Context context, LayoutManager manager) {
            super(context);
            this.mLayoutManager = manager;
        }

        protected void onStart() {
            super.onStart();
        }

        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }

        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return ((boxStart + boxEnd) / 2) - ((viewStart + viewEnd) / 2);
        }

        public PointF computeScrollVectorForPosition(int targetPosition) {
            return targetPosition < this.mLayoutManager.getFirstPosition() ? new PointF(0.0f, -1.0f) : new PointF(0.0f, 1.0f);
        }
    }

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (this.itemView instanceof OnCenterProximityListener) {
                OnCenterProximityListener item = this.itemView;
                if (isCentralItem) {
                    item.onCenterPosition(animate);
                } else {
                    item.onNonCenterPosition(animate);
                }
            }
        }
    }

    public void setAdapter(android.support.v7.widget.RecyclerView.Adapter adapter) {
        android.support.v7.widget.RecyclerView.Adapter currentAdapter = getAdapter();
        if (currentAdapter != null) {
            currentAdapter.unregisterAdapterDataObserver(this.mObserver);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.mObserver);
        }
    }

    public int getBaseline() {
        if (getChildCount() == 0) {
            return super.getBaseline();
        }
        int centerChildBaseline = getChildAt(findCenterViewIndex()).getBaseline();
        return centerChildBaseline == -1 ? super.getBaseline() : getCentralViewTop() + centerChildBaseline;
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        if (this.mGreedyTouchMode && getChildCount() > 0) {
            int action = event.getActionMasked();
            if (action == 0) {
                this.mStartX = event.getX();
                this.mStartY = event.getY();
                this.mStartFirstTop = getChildCount() > 0 ? (float) getChildAt(0).getTop() : 0.0f;
                this.mPossibleVerticalSwipe = true;
                this.mGestureDirectionLocked = false;
            } else if (action == 2 && this.mPossibleVerticalSwipe) {
                handlePossibleVerticalSwipe(event);
            }
            getParent().requestDisallowInterceptTouchEvent(this.mPossibleVerticalSwipe);
        }
        return super.onInterceptTouchEvent(event);
    }

    private boolean handlePossibleVerticalSwipe(MotionEvent event) {
        if (this.mGestureDirectionLocked) {
            return this.mPossibleVerticalSwipe;
        }
        float deltaX = Math.abs(this.mStartX - event.getX());
        float deltaY = Math.abs(this.mStartY - event.getY());
        if ((deltaX * deltaX) + (deltaY * deltaY) > ((float) (this.mTouchSlop * this.mTouchSlop))) {
            if (deltaX > deltaY) {
                this.mPossibleVerticalSwipe = false;
            }
            this.mGestureDirectionLocked = true;
        }
        return this.mPossibleVerticalSwipe;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        int scrollState = getScrollState();
        boolean result = super.onTouchEvent(event);
        if (getChildCount() <= 0) {
            return result;
        }
        int action = event.getActionMasked();
        if (action == 0) {
            handleTouchDown(event);
            return result;
        } else if (action == 1) {
            handleTouchUp(event, scrollState);
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        } else if (action == 2) {
            if (Math.abs(this.mTapPositionX - ((int) event.getX())) >= this.mTouchSlop || Math.abs(this.mTapPositionY - ((int) event.getY())) >= this.mTouchSlop) {
                releasePressedItem();
                this.mCanClick = false;
            }
            result |= handlePossibleVerticalSwipe(event);
            getParent().requestDisallowInterceptTouchEvent(this.mPossibleVerticalSwipe);
            return result;
        } else if (action != 3) {
            return result;
        } else {
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        }
    }

    private void releasePressedItem() {
        if (this.mPressedView != null) {
            this.mPressedView.setPressed(false);
            this.mPressedView = null;
        }
        this.mPressedHandler.removeCallbacks(this.mPressedRunnable);
    }

    private void onScroll(int dy) {
        for (OnScrollListener listener : this.mOnScrollListeners) {
            listener.onScroll(dy);
        }
    }

    private boolean checkForTap(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        float rawY = event.getRawY();
        int index = findCenterViewIndex();
        ViewHolder holder = getChildViewHolder(getChildAt(index));
        computeTapRegions(this.mTapRegions);
        if (rawY > this.mTapRegions[0] && rawY < this.mTapRegions[1]) {
            if (this.mClickListener != null) {
                this.mClickListener.onClick(holder);
            }
            return true;
        } else if (index > 0 && rawY <= this.mTapRegions[0]) {
            animateToMiddle(index - 1, index);
            return true;
        } else if (index < getChildCount() - 1 && rawY >= this.mTapRegions[1]) {
            animateToMiddle(index + 1, index);
            return true;
        } else if (index != 0 || rawY > this.mTapRegions[0] || this.mClickListener == null) {
            return false;
        } else {
            this.mClickListener.onTopEmptyRegionClick();
            return true;
        }
    }

    private void animateToMiddle(int newCenterIndex, int oldCenterIndex) {
        if (newCenterIndex == oldCenterIndex) {
            throw new IllegalArgumentException("newCenterIndex must be different from oldCenterIndex");
        }
        startScrollAnimation(new ArrayList(), getCentralViewTop() - getChildAt(newCenterIndex).getTop(), 150);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration) {
        startScrollAnimation((List) animators, scroll, duration, 0);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay) {
        startScrollAnimation(animators, scroll, duration, delay, (AnimatorListener) null);
    }

    private void startScrollAnimation(int scroll, long duration, long delay, AnimatorListener listener) {
        startScrollAnimation((List) null, scroll, duration, delay, listener);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay, AnimatorListener listener) {
        if (this.mScrollAnimator != null) {
            this.mScrollAnimator.cancel();
        }
        this.mLastScrollChange = 0;
        ObjectAnimator scrollAnimator = ObjectAnimator.ofInt(this, this.mSetScrollVerticallyProperty, new int[]{0, -scroll});
        if (animators != null) {
            animators.add(scrollAnimator);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);
            this.mScrollAnimator = animatorSet;
        } else {
            this.mScrollAnimator = scrollAnimator;
        }
        this.mScrollAnimator.setDuration(duration);
        if (listener != null) {
            this.mScrollAnimator.addListener(listener);
        }
        if (delay > 0) {
            this.mScrollAnimator.setStartDelay(delay);
        }
        this.mScrollAnimator.start();
    }

    public boolean fling(int velocityX, int velocityY) {
        if (getChildCount() == 0) {
            return false;
        }
        int currentPosition = getChildPosition(getChildAt(findCenterViewIndex()));
        if ((currentPosition == 0 && velocityY < 0) || (currentPosition == getAdapter().getItemCount() - 1 && velocityY > 0)) {
            return super.fling(velocityX, velocityY);
        }
        if (Math.abs(velocityY) < this.mMinFlingVelocity) {
            return false;
        }
        velocityY = Math.max(Math.min(velocityY, this.mMaxFlingVelocity), -this.mMaxFlingVelocity);
        if (this.mScroller == null) {
            this.mScroller = new Scroller(getContext(), (Interpolator) null, true);
        }
        this.mScroller.fling(0, 0, 0, velocityY, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int delta = this.mScroller.getFinalY() / (getPaddingTop() + (getAdjustedHeight() / 2));
        if (delta == 0) {
            delta = velocityY > 0 ? 1 : -1;
        }
        smoothScrollToPosition(Math.max(0, Math.min(getAdapter().getItemCount() - 1, currentPosition + delta)));
        return true;
    }

    public ViewHolder getChildViewHolder(View child) {
        return (ViewHolder) super.getChildViewHolder(child);
    }

    private int findCenterViewIndex() {
        int count = getChildCount();
        int index = -1;
        int closest = Integer.MAX_VALUE;
        int centerY = getCenterYPos(this);
        for (int i = 0; i < count; i++) {
            int distance = Math.abs(centerY - (getTop() + getCenterYPos(getChildAt(i))));
            if (distance < closest) {
                closest = distance;
                index = i;
            }
        }
        if (index != -1) {
            return index;
        }
        throw new IllegalStateException("Can't find central view.");
    }

    private static int getCenterYPos(View v) {
        return (v.getTop() + v.getPaddingTop()) + (getAdjustedHeight(v) / 2);
    }

    private void handleTouchUp(MotionEvent event, int scrollState) {
        if (this.mCanClick && event != null && checkForTap(event)) {
            this.mPressedHandler.postDelayed(this.mReleasedRunnable, (long) ViewConfiguration.getTapTimeout());
        } else if (scrollState != 0) {
        } else {
            if (isOverScrolling()) {
                this.mOverScrollListener.onOverScroll();
            } else {
                animateToCenter();
            }
        }
    }

    private boolean isOverScrolling() {
        return getChildCount() > 0 && this.mStartFirstTop <= ((float) getCentralViewTop()) && getChildAt(0).getTop() >= getTopViewMaxTop() && this.mOverScrollListener != null;
    }

    private int getBottomViewMaxTop() {
        if (this.mCustomBottomViewMaxTop == -1) {
            return getHeight() / 2;
        }
        return this.mCustomBottomViewMaxTop;
    }

    private int getTopViewMaxTop() {
        if (this.mCustomTopViewMaxTop == -1) {
            return getHeight() / 2;
        }
        return this.mCustomTopViewMaxTop;
    }

    private int getItemHeight() {
        return (getAdjustedHeight() / 3) + 1;
    }

    public int getCentralViewTop() {
        return getPaddingTop() + getItemHeight();
    }

    public void animateToCenter() {
        startScrollAnimation(getCentralViewTop() - getChildAt(findCenterViewIndex()).getTop(), 150, 0, new C04715());
    }

    private void handleTouchDown(MotionEvent event) {
        if (this.mCanClick) {
            this.mTapPositionX = (int) event.getX();
            this.mTapPositionY = (int) event.getY();
            float rawY = event.getRawY();
            computeTapRegions(this.mTapRegions);
            if (rawY > this.mTapRegions[0] && rawY < this.mTapRegions[1] && (getChildAt(findCenterViewIndex()) instanceof OnCenterProximityListener)) {
                this.mPressedHandler.removeCallbacks(this.mReleasedRunnable);
                this.mPressedHandler.postDelayed(this.mPressedRunnable, (long) ViewConfiguration.getTapTimeout());
            }
        }
    }

    private void setScrollVertically(int scroll) {
        scrollBy(0, scroll - this.mLastScrollChange);
        this.mLastScrollChange = scroll;
    }

    private int getAdjustedHeight() {
        return getAdjustedHeight(this);
    }

    private static int getAdjustedHeight(View v) {
        return (v.getHeight() - v.getPaddingBottom()) - v.getPaddingTop();
    }

    private void computeTapRegions(float[] tapRegions) {
        int[] iArr = this.mLocation;
        this.mLocation[1] = 0;
        iArr[0] = 0;
        getLocationOnScreen(this.mLocation);
        int mScreenTop = this.mLocation[1];
        int height = getHeight();
        tapRegions[0] = ((float) mScreenTop) + (((float) height) * 0.33f);
        tapRegions[1] = ((float) mScreenTop) + (((float) height) * 0.66999996f);
    }
}
