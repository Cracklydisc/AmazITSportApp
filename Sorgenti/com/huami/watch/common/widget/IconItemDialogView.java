package com.huami.watch.common.widget;

public class IconItemDialogView {

    private interface DialogKeyEventListener {
    }

    public static class Builder implements DialogKeyEventListener {
    }

    private enum KeyCodes {
        KEY_DOWN,
        KEY_UP,
        KEY_CENTER
    }
}
