package com.huami.watch.common.widget;

import android.graphics.Color;
import java.io.Serializable;

public class ChartInfo implements Serializable {
    public int color = Color.parseColor("#00000000");
    public int dayOffset;
    public int sportTime;
}
