package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class HmRelativeLayout extends RelativeLayout {
    public HmRelativeLayout(Context context) {
        super(context);
    }

    public HmRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
