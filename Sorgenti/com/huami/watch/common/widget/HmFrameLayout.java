package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class HmFrameLayout extends FrameLayout {
    public HmFrameLayout(Context context) {
        super(context);
    }

    public HmFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
