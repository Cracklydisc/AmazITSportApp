package com.huami.watch.common.widget;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huami.watch.common.widget.NumberTwoSelector.SelectView;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.picker.TeaserLayout;
import com.huami.watch.picker.TeaserLayout.TeaserViewInfo;
import com.huami.watch.ui.C1070R;
import java.util.List;

public class NumberPickerView extends TeaserLayout implements KeyeventConsumer {
    private final float DEFAUT_SCALE = 0.6f;
    private boolean canAccept = true;
    private int column = 2;
    private String[] columnFir;
    private String[] columnFirSub;
    private String[] columnSec;
    private String[] columnSecSub;
    private String[] columnThr;
    private String[] columnThrSub;
    private KeyeventProcessor keyeventProcessor;
    private ActivatorPassivator mActivatorPassivator;
    private IOnButtonClickListener mButtonClickListener = null;
    private HmHaloButton mDefaultClickView;
    private HmAnimatedVectorDrawable mDefautAnimatedVectorDrawable;
    private IOnSelectChangeListener mOnSelectChangeListener = null;
    private NumberThreeSelector mTime12View;
    private NumberOneSelector mTimeOne;
    private NumberTwoSelector mTimeView;
    private int size1;
    private int size2;
    private int size3;
    private String title;
    private float titleSize;
    private int visibility1;
    private int visibility2;
    private int visibility3;
    private int widthColumn1;
    private int widthColumn2;
    private int widthColumn3;

    class C04771 implements EventCallBack {
        C04771() {
        }

        public boolean onKeyClick(HMKeyEvent theKey) {
            if (theKey == HMKeyEvent.KEY_UP || theKey == HMKeyEvent.KEY_DOWN || theKey != HMKeyEvent.KEY_CENTER) {
                return false;
            }
            if (NumberPickerView.this.getCurrentIndex() != NumberPickerView.this.getCount() - 1) {
                if (NumberPickerView.this.mTimeView != null && NumberPickerView.this.mTimeView.getCurrentView() == SelectView.HOUR && NumberPickerView.this.mTimeView.changeFocused()) {
                    return true;
                }
                if (NumberPickerView.this.mTime12View != null && NumberPickerView.this.mTime12View.getCurrentView() == SelectView.HOUR && NumberPickerView.this.mTime12View.changeFocused()) {
                    return true;
                }
                if (NumberPickerView.this.mTime12View != null && NumberPickerView.this.mTime12View.getCurrentView() == SelectView.AM_OR_PM && NumberPickerView.this.mTime12View.changeFocused()) {
                    return true;
                }
                NumberPickerView.this.setCenterIndex(Math.min(NumberPickerView.this.getCount(), NumberPickerView.this.getCurrentIndex() + 1), true);
                return true;
            } else if (NumberPickerView.this.mDefaultClickView == null) {
                return true;
            } else {
                NumberPickerView.this.mDefaultClickView.performClick();
                return true;
            }
        }

        public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
            return false;
        }
    }

    class C04782 implements OnClickListener {
        C04782() {
        }

        public void onClick(View view) {
            NumberPickerView.this.mButtonClickListener.onSelectedConfirm(NumberPickerView.this);
        }
    }

    private static class ActivatorPassivator implements com.huami.watch.picker.TeaserLayout.ActivatorPassivator {
        public void onActivate(View view, int index) {
            view.setEnabled(true);
            view.setActivated(true);
            view.setClickable(true);
            view.setImportantForAccessibility(1);
        }

        public void onPassivate(View view, int index) {
            view.setEnabled(false);
            view.setActivated(false);
            view.setClickable(false);
            view.setImportantForAccessibility(2);
        }

        private ActivatorPassivator() {
        }
    }

    private class AutoAdvanceListener implements Listener {
        public void onCenterIndexChanged(int centerIndex) {
            if (centerIndex != 1 || NumberPickerView.this.mOnSelectChangeListener != null) {
            }
        }

        private AutoAdvanceListener() {
        }

        public void onScrollListener(List<TeaserViewInfo> infos) {
            for (TeaserViewInfo info : infos) {
                if (info.view != NumberPickerView.this.mDefaultClickView) {
                    float scale = 0.6f + ((1.0f - Math.abs(info.percent)) * 0.39999998f);
                    info.view.setScaleX(scale);
                    info.view.setScaleY(scale);
                } else {
                    NumberPickerView.this.mDefautAnimatedVectorDrawable.setCurrentPlayTime(outInteploe((1.0f - info.percent) * 2.0f));
                }
            }
        }

        private float outInteploe(float input) {
            return input == 1.0f ? 1.0f : 1.0f + (-((float) Math.pow(2.0d, (double) (-10.0f * input))));
        }
    }

    public interface IOnButtonClickListener {
        void onSelectedConfirm(NumberPickerView numberPickerView);
    }

    public interface IOnSelectChangeListener {
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(new C04771());
        }
        this.keyeventProcessor.injectKeyEvent(event);
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    public void setOnButtonClickListener(IOnButtonClickListener buttonClickListener) {
        this.mButtonClickListener = buttonClickListener;
        if (this.mButtonClickListener != null && this.mDefaultClickView != null) {
            this.mDefaultClickView.setOnClickListener(new C04782());
        }
    }

    public void setOnSelectChangeListener(IOnSelectChangeListener onSelectChangeListener) {
        this.mOnSelectChangeListener = onSelectChangeListener;
    }

    public NumberPickerView(Context context, String[] columnFri, String[] columnSec, String[] columnThr) {
        super(context);
    }

    public void setPickerData(String[] columnFri, String[] columnSec, String[] columnThr) {
        if (columnSec == null && columnThr == null) {
            this.column = 1;
        } else {
            this.column = columnThr == null ? 2 : 3;
        }
        this.columnFir = columnFri;
        this.columnSec = columnSec;
        this.columnThr = columnThr;
        initContent();
        setActivatorPassivator(this.mActivatorPassivator);
    }

    public void setPickerData(String[] columnFri, String[] columnSec, String[] columnThr, String[] columnFriSub, String[] columnSecSub, String[] columnThrSub) {
        if (columnSec == null && columnThr == null) {
            this.column = 1;
        } else {
            this.column = columnThr == null ? 2 : 3;
        }
        this.columnFir = columnFri;
        this.columnSec = columnSec;
        this.columnThr = columnThr;
        this.columnFirSub = columnFriSub;
        this.columnSecSub = columnSecSub;
        this.columnThrSub = columnThrSub;
        initContent();
        setActivatorPassivator(this.mActivatorPassivator);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public NumberPickerView(Context context) {
        super(context);
    }

    private void initContent() {
        this.mActivatorPassivator = new ActivatorPassivator();
        RelativeLayout linearLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(-1, -1);
        lp.gravity = 17;
        linearLayout.setLayoutParams(lp);
        addTitle(linearLayout);
        RelativeLayout.LayoutParams dateViewParams = new RelativeLayout.LayoutParams(-1, -1);
        dateViewParams.addRule(14);
        dateViewParams.setMargins(0, (int) TypedValue.applyDimension(1, 20.0f, getContext().getResources().getDisplayMetrics()), 0, 0);
        if (this.column == 3) {
            this.mTime12View = (NumberThreeSelector) LayoutInflater.from(getContext()).inflate(C1070R.layout.time_select3, null);
            this.mTime12View.setData(this.columnFir, this.columnSec, this.columnThr, this.columnFirSub, this.columnSecSub, this.columnThrSub);
            this.mTime12View.setParentPickerView(this);
            linearLayout.addView(this.mTime12View, dateViewParams);
        } else if (this.column == 2) {
            this.mTimeView = (NumberTwoSelector) LayoutInflater.from(getContext()).inflate(C1070R.layout.time_select2, null);
            this.mTimeView.setData(this.columnFir, this.columnSec, this.columnFirSub, this.columnSecSub);
            this.mTimeView.setParentPickerView(this);
            linearLayout.addView(this.mTimeView, dateViewParams);
        } else if (this.column == 1) {
            this.mTimeOne = (NumberOneSelector) LayoutInflater.from(getContext()).inflate(C1070R.layout.time_select1, null);
            this.mTimeOne.setData(this.columnFir, this.columnFirSub);
            this.mTimeOne.setParentPickerView(this);
            linearLayout.addView(this.mTimeOne, dateViewParams);
        }
        setTextSizeSelectColumn1(this.size1);
        setTextSizeSelectColumn2(this.size2);
        setTextSizeSelectColumn3(this.size3);
        addView(linearLayout);
        initCloseButton();
        initVectorDrawable(this.mDefaultClickView.getBackground());
        addView(this.mDefaultClickView, new LayoutParams(-2, -2, 16));
        setListener(new AutoAdvanceListener());
        setCenterIndex(0, false);
        requestLayout();
    }

    private void addTitle(RelativeLayout layout) {
        if (layout != null && !TextUtils.isEmpty(this.title)) {
            TextView titleText = new TextView(getContext());
            titleText.setTextColor(Color.parseColor("#FFFFFFFF"));
            float defaultTitleSize = TypedValue.applyDimension(2, 10.0f, getContext().getResources().getDisplayMetrics());
            if (this.titleSize != 0.0f) {
                defaultTitleSize = this.titleSize;
            }
            titleText.setTextSize(defaultTitleSize);
            titleText.setGravity(17);
            titleText.setText(this.title);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-1, -2);
            lp.addRule(14);
            lp.setMargins(0, (int) TypedValue.applyDimension(1, 15.0f, getContext().getResources().getDisplayMetrics()), 0, 0);
            titleText.setLayoutParams(lp);
            if (titleText.getParent() != null && (titleText.getParent() instanceof ViewGroup)) {
                ((ViewGroup) titleText.getParent()).removeAllViews();
            }
            layout.addView(titleText);
        }
    }

    public void setCurrentIndex(int oneIndex, int hourIndex, int minuteIndex) {
        if (this.mTimeView != null) {
            this.mTimeView.setIndex(oneIndex, hourIndex);
        }
        if (this.mTime12View != null) {
            this.mTime12View.setIndex(oneIndex, hourIndex, minuteIndex);
        }
        if (this.mTimeOne != null) {
            this.mTimeOne.setIndex(oneIndex);
        }
    }

    public int[] getSelectIndex() {
        if (this.mTimeView != null) {
            return this.mTimeView.getSelectIndex();
        }
        if (this.mTime12View != null) {
            return this.mTime12View.getSelectIndex();
        }
        if (this.mTimeOne != null) {
            return this.mTimeOne.getSelectIndex();
        }
        return null;
    }

    private void initVectorDrawable(Drawable drawable) {
        if (drawable instanceof VectorDrawable) {
            this.mDefautAnimatedVectorDrawable = null;
            this.mDefautAnimatedVectorDrawable = new HmAnimatedVectorDrawable((VectorDrawable) drawable);
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("demopath", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_alpha));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("demopath", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_path));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("rightgroup", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_scaley));
            this.mDefautAnimatedVectorDrawable.setupAnimatorsForTarget("rightgroup", (ValueAnimator) AnimatorInflater.loadAnimator(getContext(), C1070R.animator.time_picker_ok_vector_scalex));
        }
    }

    private void initCloseButton() {
        if (this.mDefaultClickView == null) {
            HmHaloButton pickerPositionView = new HmHaloButton(getContext());
            pickerPositionView.setBackgroundResource(C1070R.drawable.time_picker_ok_vector);
            pickerPositionView.setWithHalo(false);
            this.mDefaultClickView = pickerPositionView;
        }
    }

    public NumberPickerView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
    }

    public NumberPickerView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public boolean canScrollHorizontally(int i) {
        return super.canScrollHorizontally(i);
    }

    public void setCenterIndex(int centerIndex, boolean animate) {
        super.setCenterIndex(centerIndex, animate);
    }

    public void setWidthColumn1(int widthColumn1) {
        this.widthColumn1 = widthColumn1;
        if (this.mTime12View != null) {
            this.mTime12View.setWidthColumn1(widthColumn1);
        }
        if (this.mTimeView != null) {
            this.mTimeView.setWidthColumn1(widthColumn1);
        }
        if (this.mTimeOne != null) {
            this.mTimeOne.setWidthColumn1(widthColumn1);
        }
    }

    public void setWidthColumn2(int widthColumn2) {
        this.widthColumn2 = widthColumn2;
        if (this.mTime12View != null) {
            this.mTime12View.setWidthColumn2(widthColumn2);
        }
        if (this.mTimeView != null) {
            this.mTimeView.setWidthColumn2(widthColumn2);
        }
    }

    public void setWidthColumn3(int widthColumn3) {
        this.widthColumn3 = widthColumn3;
        if (this.mTime12View != null) {
            this.mTime12View.setWidthColumn3(widthColumn3);
        }
    }

    public void setTextSizeSelectColumn1(int size) {
        this.size1 = size;
        if (this.mTime12View != null) {
            this.mTime12View.setSelectTextSizeColumn1(size);
        }
        if (this.mTimeView != null) {
            this.mTimeView.setSelectTextSizeColumn1(size);
        }
        if (this.mTimeOne != null) {
            this.mTimeOne.setSelectTextSizeColumn1(size);
        }
    }

    public void setTextSizeSelectColumn2(int size) {
        this.size2 = size;
        if (this.mTime12View != null) {
            this.mTime12View.setSelectTextSizeColumn2(size);
        }
        if (this.mTimeView != null) {
            this.mTimeView.setSelectTextSizeColumn2(size);
        }
    }

    public void setTextSizeSelectColumn3(int size) {
        this.size3 = size;
        if (this.mTime12View != null) {
            this.mTime12View.setSelectTextSizeColumn3(size);
        }
    }

    public void setVisibilityColumn1(final int visibility) {
        this.visibility1 = visibility;
        post(new Runnable() {
            public void run() {
                if (NumberPickerView.this.mTime12View != null) {
                    NumberPickerView.this.mTime12View.setVisibilityColumn1(visibility);
                }
                if (NumberPickerView.this.mTimeView != null) {
                    NumberPickerView.this.mTimeView.setVisibilityColumn1(visibility);
                }
                if (NumberPickerView.this.mTimeOne != null) {
                    NumberPickerView.this.mTimeOne.setVisibilityColumn1(visibility);
                }
            }
        });
    }

    public void setVisibilityColumn2(final int visibility) {
        this.visibility2 = visibility;
        post(new Runnable() {
            public void run() {
                if (NumberPickerView.this.mTime12View != null) {
                    NumberPickerView.this.mTime12View.setVisibilityColumn2(visibility);
                }
                if (NumberPickerView.this.mTimeView != null) {
                    NumberPickerView.this.mTimeView.setVisibilityColumn2(visibility);
                }
            }
        });
    }

    public void setVisibilityColumn3(final int visibility) {
        this.visibility3 = visibility;
        post(new Runnable() {
            public void run() {
                if (NumberPickerView.this.mTime12View != null) {
                    NumberPickerView.this.mTime12View.setVisibilityColumn3(visibility);
                }
            }
        });
    }

    public void set2OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mTime12View != null) {
            this.mTime12View.set2OnchangedLisenter(onchangedLisenter);
        }
        if (this.mTimeView != null) {
            this.mTimeView.set2OnchangedLisenter(onchangedLisenter);
        }
    }

    public void set3OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mTime12View != null) {
            this.mTime12View.set3OnchangedLisenter(onchangedLisenter);
        }
    }

    public void set1OnchangedLisenter(OnValueChangeListener onchangedLisenter) {
        if (this.mTime12View != null) {
            this.mTime12View.set1OnchangedLisenter(onchangedLisenter);
        }
        if (this.mTimeView != null) {
            this.mTimeView.set1OnchangedLisenter(onchangedLisenter);
        }
        if (this.mTimeOne != null) {
            this.mTimeOne.setOnchangedLisenter(onchangedLisenter);
        }
    }

    public NumberView get1ColumnView() {
        if (this.mTime12View != null) {
            return this.mTime12View.getAmPmView();
        }
        if (this.mTimeView != null) {
            return this.mTimeView.getHourView();
        }
        if (this.mTimeOne != null) {
            return this.mTimeOne.getHourView();
        }
        return null;
    }

    public NumberView get2ColumnView() {
        if (this.mTime12View != null) {
            return this.mTime12View.getHourView();
        }
        if (this.mTimeView != null) {
            return this.mTimeView.getMinuteView();
        }
        return null;
    }

    public NumberView get3ColumnView() {
        return this.mTime12View != null ? this.mTime12View.getMinuteView() : null;
    }
}
