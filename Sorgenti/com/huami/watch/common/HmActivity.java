package com.huami.watch.common;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class HmActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        View xv = ((ViewGroup) getWindow().getDecorView()).getChildAt(0);
        if (xv != null && xv.getClass().getName().endsWith("SwipeDismissLayout")) {
            xv.setFitsSystemWindows(false);
        }
    }
}
