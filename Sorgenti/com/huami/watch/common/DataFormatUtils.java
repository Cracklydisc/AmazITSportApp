package com.huami.watch.common;

import com.huami.watch.common.log.Debug;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DataFormatUtils {
    public static String parseCrossingSecondToDefaultFormattedTime(long second) {
        int h = (int) (second / 3600);
        int min = (int) ((second % 3600) / 60);
        int sec = (int) (second % 60);
        StringBuilder sb = new StringBuilder();
        if (h > 0) {
            if (h > 9) {
                sb.append(h);
            } else {
                sb.append("0").append(h);
            }
            sb.append(":");
        }
        if (min > 9) {
            sb.append(min);
        } else {
            sb.append("0").append(min);
        }
        sb.append(":");
        if (sec > 9) {
            sb.append(sec);
        } else {
            sb.append("0").append(sec);
        }
        return sb.toString();
    }

    public static String parseCrossingSecondToDefaultFormattedTimeContainHour(long second) {
        int h = (int) (second / 3600);
        int min = (int) ((second % 3600) / 60);
        int sec = (int) (second % 60);
        StringBuilder sb = new StringBuilder();
        if (h > 9) {
            sb.append(h);
        } else {
            sb.append("0").append(h);
        }
        sb.append(":");
        if (min > 9) {
            sb.append(min);
        } else {
            sb.append("0").append(min);
        }
        sb.append(":");
        if (sec > 9) {
            sb.append(sec);
        } else {
            sb.append("0").append(sec);
        }
        return sb.toString();
    }

    public static String parseMillSecondToDefaultFormattedTime(long millSec) {
        return parseSecondToDefaultFormattedTime(Math.round(((double) millSec) / 1000.0d));
    }

    public static String parseSecondToDefaultFormattedTime(long second) {
        int h = (int) (second / 3600);
        int min = (int) ((second % 3600) / 60);
        int sec = (int) (second % 60);
        StringBuilder sb = new StringBuilder();
        if (h > 9) {
            sb.append(h);
        } else {
            sb.append("0").append(h);
        }
        sb.append(":");
        if (min > 9) {
            sb.append(min);
        } else {
            sb.append("0").append(min);
        }
        sb.append(":");
        if (sec > 9) {
            sb.append(sec);
        } else {
            sb.append("0").append(sec);
        }
        return sb.toString();
    }

    public static String parseMilliSecondToFormattedTime(long timestamp, String pattern, boolean withCurrentTimeZone) {
        if (timestamp < 0) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        if (!withCurrentTimeZone) {
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        return format.format(new Date(timestamp));
    }

    public static String parseSecondPerMeterToFormattedPace(float secondPerMeter) {
        int secondPerKm = (int) (1000.0f * secondPerMeter);
        StringBuilder sb = new StringBuilder();
        if (secondPerKm >= 5999) {
            sb.append("99").append("'").append("99").append("\"");
            return sb.toString();
        }
        int minute = secondPerKm / 60;
        int second = secondPerKm % 60;
        if (minute > 9) {
            sb.append(minute);
        } else {
            sb.append("0").append(minute);
        }
        sb.append("'");
        if (second > 9) {
            sb.append(second);
        } else {
            sb.append("0").append(second);
        }
        sb.append("\"");
        return sb.toString();
    }

    public static String parseSecondPerMeterToFormatted100meterPace(float secondPerMeter) {
        int secondPerHunmeter = (int) (100.0f * secondPerMeter);
        StringBuilder sb = new StringBuilder();
        if (secondPerHunmeter >= 5999) {
            sb.append("99").append("'").append("99").append("\"");
            return sb.toString();
        }
        int minute = secondPerHunmeter / 60;
        int second = secondPerHunmeter % 60;
        if (minute > 9) {
            sb.append(minute);
        } else {
            sb.append("0").append(minute);
        }
        sb.append("'");
        if (second > 9) {
            sb.append(second);
        } else {
            sb.append("0").append(second);
        }
        sb.append("\"");
        return sb.toString();
    }

    public static String parseFormattedRealNumber(double realNumber, boolean rounding) {
        if (realNumber <= 0.0d) {
            return "0.00";
        }
        if (Double.isNaN(realNumber) || Double.isInfinite(realNumber)) {
            Debug.m5i(DataFormatUtils.class.getName(), "parseFormattedRealNumber, value is invalid:" + realNumber);
            return "0";
        }
        BigDecimal decimal = new BigDecimal(String.valueOf(realNumber));
        if (realNumber < 100.0d) {
            if (rounding) {
                return decimal.setScale(2, RoundingMode.HALF_UP).toString();
            }
            return decimal.setScale(2, 3).toString();
        } else if (realNumber >= 1000.0d) {
            return "" + ((int) realNumber);
        } else {
            if (rounding) {
                return decimal.setScale(1, RoundingMode.HALF_UP).toString();
            }
            return decimal.setScale(1, RoundingMode.FLOOR).toString();
        }
    }

    public static String parseTotalDistanceFormattedRealNumber(double realNumber, boolean rounding) {
        if (realNumber <= 0.0d) {
            return "0.00";
        }
        if (realNumber < 10000.0d) {
            if (rounding) {
                return String.format("%.2f", new Object[]{Double.valueOf(realNumber)});
            }
            return String.format("%.2f", new Object[]{Double.valueOf(Math.floor(realNumber * 100.0d) / 100.0d)});
        } else if (realNumber < 100000.0d) {
            if (rounding) {
                return String.format("%.1f", new Object[]{Double.valueOf(realNumber)});
            }
            return String.format("%.1f", new Object[]{Double.valueOf(Math.floor(realNumber * 10.0d) / 10.0d)});
        } else if (realNumber < 1000000.0d) {
            return "" + ((int) realNumber);
        } else {
            StringBuffer str = new StringBuffer();
            str.append(String.valueOf((int) realNumber).substring(0, 6)).append("..");
            return str.toString();
        }
    }
}
