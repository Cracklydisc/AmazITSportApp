package com.huami.watch.common.db;

import android.content.Context;
import android.os.Handler;
import com.huami.watch.common.ArrayUtils;
import com.huami.watch.common.log.Debug;
import java.util.List;

public class AsyncDatabaseManager<T> {
    private Dao<T> mDao = null;
    private Handler mDataManagerHandler = null;

    public AsyncDatabaseManager(Dao<T> dao, Handler handler) {
        this.mDataManagerHandler = handler;
        this.mDao = dao;
    }

    public void add(final Context context, final T t, final Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "add " + this.mDao + "|" + t);
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                boolean success = AsyncDatabaseManager.this.mDao.insert(context, t);
                if (callback != null) {
                    callback.notifyCallback(success ? 0 : -1, null);
                }
            }
        });
    }

    public void addAll(final Context context, final List<? extends T> list, final Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "addAll " + this.mDao + "|" + list);
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                boolean success = AsyncDatabaseManager.this.mDao.insertAll(context, list);
                if (callback != null) {
                    callback.notifyCallback(success ? 0 : -1, null);
                }
            }
        });
    }

    public void remove(Context context, String whereClause, String[] whereClauseArg, Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "remove " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        final Context context2 = context;
        final String str = whereClause;
        final String[] strArr = whereClauseArg;
        final Callback callback2 = callback;
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                boolean success = AsyncDatabaseManager.this.mDao.delete(context2, str, strArr);
                if (callback2 != null) {
                    callback2.notifyCallback(success ? 0 : -1, null);
                }
            }
        });
    }

    public boolean update(final Context context, final T t, final Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "update " + this.mDao + "|" + t);
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                boolean success = AsyncDatabaseManager.this.mDao.update(context, t);
                if (callback != null) {
                    callback.notifyCallback(success ? 0 : -1, null);
                }
            }
        });
        return true;
    }

    public void select(Context context, String whereClause, String[] whereClauseArg, Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "select " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        if (callback == null) {
            Debug.m6w("AsyncDatabaseManager", "callback is null");
            return;
        }
        final Context context2 = context;
        final String str = whereClause;
        final String[] strArr = whereClauseArg;
        final Callback callback2 = callback;
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                callback2.notifyCallback(0, AsyncDatabaseManager.this.mDao.select(context2, str, strArr));
            }
        });
    }

    public void selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit, Callback callback) {
        Debug.m3d("AsyncDatabaseManager", "selectAll " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        if (callback == null) {
            Debug.m6w("AsyncDatabaseManager", "callback is null");
            return;
        }
        final Context context2 = context;
        final String str = whereClause;
        final String[] strArr = whereClauseArg;
        final String str2 = orderBy;
        final String str3 = limit;
        final Callback callback2 = callback;
        this.mDataManagerHandler.post(new Runnable() {
            public void run() {
                callback2.notifyCallback(0, AsyncDatabaseManager.this.mDao.selectAll(context2, str, strArr, str2, str3));
            }
        });
    }
}
