package com.huami.watch.common.db;

import android.content.Context;
import android.database.Cursor;
import java.util.List;

public class Dao<T> {
    protected boolean insert(Context context, T t) {
        throw new IllegalStateException("insert not implements!");
    }

    protected boolean insertAll(Context context, List<? extends T> list) {
        throw new IllegalStateException("insertAll not implements!");
    }

    protected boolean update(Context context, T t) {
        throw new IllegalStateException("update not implements!");
    }

    protected T select(Context context, String whereClause, String[] whereClauseArg) {
        throw new IllegalStateException("select not implements!");
    }

    protected List<? extends T> selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        throw new IllegalStateException("selectAll not implements!");
    }

    protected boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        throw new IllegalStateException("delete not implements!");
    }

    protected Cursor selectAsCursor(String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        throw new IllegalStateException("select not implements!");
    }
}
