package com.huami.watch.common.db;

import android.os.Handler;
import com.huami.watch.common.log.Debug;

public abstract class Callback {
    private Handler mHandler = null;

    protected abstract void doCallback(int i, Object obj);

    public Callback(Handler handler) {
        this.mHandler = handler;
    }

    public void notifyCallback(int resultCode, final Object params) {
        Debug.m3d("Callback", "notifyCallback|resultCode:" + resultCode + ",message:" + params);
        if (this.mHandler == null) {
            doCallback(resultCode, params);
        } else {
            this.mHandler.post(new Runnable() {
                public void run() {
                    Callback.this.doCallback(0, params);
                }
            });
        }
    }
}
