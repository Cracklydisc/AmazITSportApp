package com.huami.watch.common.db;

import android.content.Context;
import android.database.Cursor;
import com.huami.watch.common.ArrayUtils;
import com.huami.watch.common.log.Debug;
import java.util.List;

public class SyncDatabaseManager<T> {
    private Dao<T> mDao = null;

    public SyncDatabaseManager(Dao<T> dao) {
        this.mDao = dao;
    }

    public boolean add(Context context, T t) {
        Debug.m3d("SyncDatabaseManager", "add " + this.mDao + "|" + t);
        return this.mDao.insert(context, t);
    }

    public boolean addAll(Context context, List<? extends T> list) {
        Debug.m3d("SyncDatabaseManager", "addAll " + this.mDao + "|" + list);
        return this.mDao.insertAll(context, list);
    }

    public boolean remove(Context context, String whereClause, String[] whereClauseArg) {
        Debug.m3d("SyncDatabaseManager", "remove " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        return this.mDao.delete(context, whereClause, whereClauseArg);
    }

    public boolean update(Context context, T t) {
        Debug.m3d("SyncDatabaseManager", "update : " + t);
        return this.mDao.update(context, t);
    }

    public T select(Context context, String whereClause, String[] whereClauseArg) {
        Debug.m3d("SyncDatabaseManager", "select " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        return this.mDao.select(context, whereClause, whereClauseArg);
    }

    public List<? extends T> selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        Debug.m3d("SyncDatabaseManager", "selectAll " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        return this.mDao.selectAll(context, whereClause, whereClauseArg, orderBy, limit);
    }

    public Cursor selectAsCursor(String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        Debug.m3d("SyncDatabaseManager", "select " + this.mDao + "|whereClause:" + whereClause + "|args:" + ArrayUtils.arrayToString(whereClauseArg));
        return this.mDao.selectAsCursor(whereClause, whereClauseArg, orderBy, limit);
    }
}
