package com.huami.watch.common;

public class ArrayUtils {
    public static String arrayToString(Object[] arr) {
        if (arr == null) {
            return null;
        }
        if (arr.length == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object o : arr) {
            sb.append(o.toString()).append(",");
        }
        sb.append("]");
        return sb.toString();
    }
}
