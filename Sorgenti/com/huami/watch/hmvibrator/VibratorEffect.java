package com.huami.watch.hmvibrator;

public final class VibratorEffect {
    public static final long[] VIBE_PATTERN_CALL_1 = new long[]{0, 800, 100, 120, 180, 120, 180, 120, 180};
    public static final long[] VIBE_PATTERN_CALL_2 = new long[]{0, 1000, 500};
    public static final long[] VIBE_PATTERN_CALL_3 = new long[]{0, 800, 100, 150, 750, 800, 1000};
    public static final long[] VIBE_PATTERN_FAIL_1 = new long[]{0, 300, 100, 300, 100, 300};
    public static final long[] VIBE_PATTERN_FAIL_2 = new long[]{0, 400, 100, 400};
    public static final long[] VIBE_PATTERN_FAIL_3 = new long[]{0, 200, 100, 200, 100, 200, 100, 200, 100, 200, 100, 200, 100, 200, 100, 200, 100};
    public static final long[] VIBE_PATTERN_HEATBEAT_QUICK = new long[]{0, 40, 120, 30, 460, 40, 120, 30, 460, 40, 120, 30, 460};
    public static final long[] VIBE_PATTERN_HEATBEAT_SLOW = new long[]{0, 60, 150, 35, 800, 60, 150, 35, 800, 60, 150, 35, 800};
    public static final long[] VIBE_PATTERN_MESSAGE_1 = new long[]{0, 400};
    public static final long[] VIBE_PATTERN_MESSAGE_2 = new long[]{0, 110, 70, 60, 120, 400};
    public static final long[] VIBE_PATTERN_SUCCESS = new long[]{0, 300, 200, 120};
}
