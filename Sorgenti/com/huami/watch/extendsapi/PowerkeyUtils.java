package com.huami.watch.extendsapi;

import android.os.SystemProperties;
import android.util.Log;

public class PowerkeyUtils {
    public static void disablePowerKey(boolean disable) {
        int disableCount = 0;
        try {
            disableCount = Integer.valueOf(SystemProperties.get("sys.disable.launcherhome")).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (disable) {
            disableCount++;
        } else {
            disableCount--;
        }
        if (disableCount < 0) {
            Log.w("PowerkeyUtils", "enable power key before disable!!!");
            new Throwable("PowerkeyUtils").printStackTrace();
            return;
        }
        SystemProperties.set("sys.disable.launcherhome", String.valueOf(disableCount));
        Log.i("PowerkeyUtils", "set disable power, count:" + disableCount);
        new Throwable("PowerkeyUtils").printStackTrace();
    }
}
