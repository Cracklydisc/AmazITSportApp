package com.huami.watch.extendsapi;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.android.internal.widget.SwipeDismissLayout;

public class SwipeDismissUtil {
    public static void requestSwipeDismissAlphaBackground(Activity activity, ViewGroup viewGroup) {
        if (activity != null) {
            View decorView = activity.getWindow().getDecorView();
            if (decorView instanceof ViewGroup) {
                View swipeDismissLayout = ((ViewGroup) decorView).getChildAt(0);
                if (swipeDismissLayout instanceof SwipeDismissLayout) {
                    ((SwipeDismissLayout) swipeDismissLayout).setSwipeTopContainer(viewGroup);
                }
            }
        }
    }

    public static void requestSwipeDismissAlphaBackgroud(Window window, ViewGroup viewGroup) {
        if (window != null) {
            View decorView = window.getDecorView();
            if (decorView instanceof ViewGroup) {
                View swipeDismissLayout = ((ViewGroup) decorView).getChildAt(0);
                if (swipeDismissLayout instanceof SwipeDismissLayout) {
                    ((SwipeDismissLayout) swipeDismissLayout).setSwipeTopContainer(viewGroup);
                }
            }
        }
    }
}
