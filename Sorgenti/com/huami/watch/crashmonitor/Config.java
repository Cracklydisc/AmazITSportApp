package com.huami.watch.crashmonitor;

import com.huami.watch.transport.httpsupport.transporter.http.URLCompleter.URLSeat;

public class Config {
    public static String getBaseServer() {
        String baseServer = "https://watch.mi-ae.cn";
        if (baseServer.endsWith("/")) {
            return baseServer;
        }
        return baseServer + "/";
    }

    public static String getUrlDropBoxCrash() {
        return getBaseServer() + "users/" + URLSeat.USER_ID + "/mobileAppActivities?activityType=CRASH_INFO";
    }
}
