package com.huami.watch.crashmonitor;

import android.content.Context;
import android.os.DropBoxManager;
import android.os.DropBoxManager.Entry;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import com.huami.watch.transport.httpsupport.cacher.DataCacher;
import com.huami.watch.transport.httpsupport.control.service.HttpTransportManager;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SystemMonitor {
    private static JSONObject BUFF_DATA;
    public static final String HM_HOST_CRASH = Config.getUrlDropBoxCrash();
    private static String JSON_KEY_CRASH_ARRAY = "chain";
    private static String JSON_KEY_CRASH_NODE = "node";
    private static final Object JSON_LOCK = new Object();
    private static WeakReference<Class> sClassType = null;
    private static WeakReference<Method> sGetMethod;
    private static TaskManager sTaskManager = new TaskManager("hm-sys-crash-monitor");

    private static void buildProto() {
        synchronized (JSON_LOCK) {
            if (BUFF_DATA != null) {
                return;
            }
            BUFF_DATA = new JSONObject();
            try {
                BUFF_DATA.put(JSON_KEY_CRASH_ARRAY, new JSONArray());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized void inject(Context ctx, String tag, long stamp) {
        synchronized (SystemMonitor.class) {
            if (ctx != null) {
                if (!TextUtils.isEmpty(tag)) {
                    if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                        Log.i("WH-CRASH", "====注入 crash =====>");
                    }
                    if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                        SolidLogger.getInstance().with("WH-CRASH", "====注入 错误 =====>");
                    }
                    Entry entry = ((DropBoxManager) ctx.getSystemService("dropbox")).getNextEntry(tag, stamp - 1);
                    if (entry != null) {
                        String message = entry.getText(1000);
                        if (TextUtils.isEmpty(message)) {
                            if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                                Log.i("WH-CRASH", "====message 是空的长度 忽略这次注入=====>");
                            }
                            SolidLogger.getInstance().with("WH-CRASH", "====message 是空的长度 忽略这次注入=====>");
                        } else {
                            buildProto();
                            synchronized (JSON_LOCK) {
                                JSONArray node = BUFF_DATA.optJSONArray(JSON_KEY_CRASH_ARRAY);
                                if (node != null) {
                                    JSONObject pin = new JSONObject();
                                    try {
                                        pin.put(JSON_KEY_CRASH_NODE, message);
                                        node.put(pin);
                                    } catch (JSONException e) {
                                    }
                                    performOncePreUpload(ctx, chunckMsgBuffer(ctx, node, false));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static void performOncePreUpload(final Context context, final DataItem item) {
        if (item != null) {
            sTaskManager.next(new Task(RunningStatus.WORK_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    boolean conned = false;
                    item.setState(1);
                    DataCacher.getInstance(context).save(item);
                    if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                        Log.i("WH-CRASH", "====[生成一个item]=====>" + item.getIdentifier());
                    }
                    SolidLogger.getInstance().with("WH-CRASH", "====[生成一个item]=====>" + item.getIdentifier());
                    if (System.getInt(context.getContentResolver(), "com.huami.watch.extra.DEVICE_CONNECTION_STATUS", 0) > 0 || System.getInt(context.getContentResolver(), "com.huami.watch.extra.conn.ctrl", 0) > 0) {
                        conned = true;
                    }
                    if (conned) {
                        List<DataItem> listForWatting = DataCacher.getInstance(context).query(1);
                        if (listForWatting != null) {
                            if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                                Log.i("WH-CRASH", "====[目前缓存的总个数]===== " + listForWatting.size());
                            }
                            SolidLogger.getInstance().with("WH-CRASH", "====[目前缓存的总个数]===== " + listForWatting.size());
                            if (listForWatting.size() > 1) {
                                if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                                    Log.i("WH-CRASH", "====[触发上传缓存]=====");
                                }
                                SolidLogger.getInstance().with("WH-CRASH", "====[触发上传缓存]=====");
                                HttpTransportManager.getInstance(context).tryProcessCachedRequestWhilePhoneAvalible(context);
                            }
                        }
                    } else {
                        if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                            Log.i("WH-CRASH", "====[不触发上传缓存]===== 因为'手表未连接'");
                        }
                        SolidLogger.getInstance().with("WH-CRASH", "====[不触发上传缓存]===== 因为'手表未连接'");
                    }
                    return null;
                }
            }).execute();
        }
    }

    public static synchronized void forceUpload(Context ctx) {
        synchronized (SystemMonitor.class) {
            if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                Log.i("WH-CRASH", "=======强制上传crash log =====>");
            }
            SolidLogger.getInstance().with("WH-CRASH", "=======强制上传crash log =====>");
            buildProto();
            performOncePreUpload(ctx, chunckMsgBuffer(ctx, null, true));
        }
    }

    private static DataItem chunckMsgBuffer(Context ctx, JSONArray node, boolean forceDesired) {
        if ((node == null || node.length() <= 3) && !forceDesired) {
            return null;
        }
        DataItem item = new DataItem();
        item.addFlags(128);
        item.addFlags(2);
        item.addFlags(16);
        item.addFlags(4096);
        item.setOwner(ctx.getPackageName());
        item.setUrl(HM_HOST_CRASH);
        item.setState(1);
        item.setAction("crash_pump");
        JSONObject obj = new JSONObject();
        try {
            ensureMethod();
            obj.put("details", "");
            obj.put("mobileDeviceId", ((Method) sGetMethod.get()).invoke(null, new Object[]{"ro.sn.serial_numbers"}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        item.addExtraPair("extra_slot_frame", obj.toString());
        item.addExtraPair("extra_slot_point", "details");
        item.setData(new String(BUFF_DATA.toString()));
        item.setMethod("post");
        item.setIdentifier(DataUtils.generateId());
        BUFF_DATA = null;
        return item;
    }

    private static void ensureMethod() {
        try {
            if (sClassType == null || sGetMethod == null || sClassType.get() == null || sGetMethod.get() == null) {
                sClassType = new WeakReference(Class.forName("android.os.SystemProperties"));
                sGetMethod = new WeakReference(((Class) sClassType.get()).getDeclaredMethod("get", new Class[]{String.class}));
                if (!((Method) sGetMethod.get()).isAccessible()) {
                    ((Method) sGetMethod.get()).setAccessible(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
