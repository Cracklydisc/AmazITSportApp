package com.huami.watch.crashmonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.transport.httpsupport.GlobalDefine;
import java.util.HashSet;

public class DropboxListener extends BroadcastReceiver {
    private static final HashSet<String> MONITOR_TAGS = new HashSet();

    class C05031 extends Task {
        final /* synthetic */ Context val$context;
        final /* synthetic */ Intent val$intent;

        public TaskOperation onExecute(TaskOperation taskOperation) {
            String action = this.val$intent.getAction();
            if (TextUtils.equals(action, "android.intent.action.DROPBOX_ENTRY_ADDED")) {
                String tag = this.val$intent.getStringExtra("tag");
                if (DropboxListener.MONITOR_TAGS.contains(tag)) {
                    SystemMonitor.inject(this.val$context, tag, this.val$intent.getLongExtra("time", 0));
                } else {
                    if (GlobalDefine.DEBUG_CRASH_MONITOR) {
                        Log.i("WH-CRASH", "=======tag 错误=====> " + tag);
                    }
                    SolidLogger.getInstance().with("WH-CRASH", "=======tag 错误 =====>" + tag);
                }
            } else if (TextUtils.equals(action, "com.huami.watch.crashmonitor.UPLOAD_CRASH")) {
                SystemMonitor.forceUpload(this.val$context);
            }
            return null;
        }
    }

    static {
        MONITOR_TAGS.add("system_app_crash");
        MONITOR_TAGS.add("data_app_crash");
    }

    public void onReceive(Context context, Intent intent) {
        SolidLogger.withContext(context);
        if (GlobalDefine.DEBUG_CRASH_MONITOR) {
            Log.i("WH-CRASH", "=======收到dropbox 消息=====>");
        }
        SolidLogger.getInstance().with("WH-CRASH", "=======收到dropbox 消息 =====>");
        if (GlobalDefine.DEBUG_CRASH_MONITOR) {
            Log.i("WH-CRASH", "=======CRASH 当前no care=====>");
        }
        SolidLogger.getInstance().with("WH-CRASH", "=======CRASH 当前no care=====>");
    }
}
