package com.huami.watch.widgetsupport;

import android.os.Bundle;

public interface LocalWidgetClient {
    void onReceiveDataFromLocalWidget(int i, Bundle bundle);

    void removeLocalWidget();

    void setLocalWidget(LocalWidgetView localWidgetView);
}
