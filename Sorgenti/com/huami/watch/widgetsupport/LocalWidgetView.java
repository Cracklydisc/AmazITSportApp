package com.huami.watch.widgetsupport;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import clc.sliteplugin.flowboard.ISpringBoardHostStub;

public interface LocalWidgetView {
    View getView(Context context);

    void onActive(Bundle bundle);

    void onBindHost(ISpringBoardHostStub iSpringBoardHostStub);

    void onDestroy();

    void onInactive(Bundle bundle);

    void onPause();

    void onPostDisableByHost();

    void onPostEnableByHost();

    void onPreDisableByHost();

    void onPreEnableByHost();

    void onResume();

    void onStop();
}
