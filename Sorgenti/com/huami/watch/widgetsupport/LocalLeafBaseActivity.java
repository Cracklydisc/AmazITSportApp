package com.huami.watch.widgetsupport;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import clc.sliteplugin.flowboard.IPluginable;
import clc.sliteplugin.flowboard.ISpringBoardHostStub;

public abstract class LocalLeafBaseActivity extends Activity {
    private FrameLayout mContainer;
    private Handler mUIHandler;
    private LocalWidgetClient mWidgetClient;
    private LocalWidgetView mWidgetView;
    private Handler mWorkHandler;
    private HandlerThread mWorkThread;

    private class ISpringBoardHostStubImpl implements ISpringBoardHostStub {
        private ISpringBoardHostStubImpl() {
        }

        public byte postDataToProvider(IPluginable iPluginable, final int i, final Bundle bundle) {
            runTaskOnUI(null, new Runnable() {
                public void run() {
                    if (LocalLeafBaseActivity.this.mWidgetClient != null) {
                        LocalLeafBaseActivity.this.mWidgetClient.onReceiveDataFromLocalWidget(i, bundle);
                    }
                }
            });
            return (byte) 100;
        }

        public void saveWidgetData(IPluginable iPluginable, String s, Object o) {
            Log.w("LocalLeafBaseActivity", "local saveWidgetData do nothing.");
        }

        public Object getWidgetData(IPluginable iPluginable, String s, Class<?> cls) {
            Log.w("LocalLeafBaseActivity", "local getWidgetData return null.");
            return null;
        }

        public void runTaskOnUI(IPluginable iPluginable, Runnable runnable) {
            LocalLeafBaseActivity.this.mUIHandler.post(runnable);
        }

        public void runTaskOnWorkThread(IPluginable iPluginable, Runnable runnable) {
            if (LocalLeafBaseActivity.this.mWorkHandler != null) {
                LocalLeafBaseActivity.this.mWorkHandler.post(runnable);
            } else {
                Log.e("LocalLeafBaseActivity", "mWorkHandler is null");
            }
        }

        public byte postDataToHost(IPluginable iPluginable, String s, Bundle bundle) {
            Log.w("LocalLeafBaseActivity", "local postDataToHost do nothing.");
            return (byte) 0;
        }

        public int getHostVerCode() {
            PackageInfo info = LocalLeafBaseActivity.this.getPackageInfo(LocalLeafBaseActivity.this.getApplicationContext());
            if (info != null) {
                return info.versionCode;
            }
            return 0;
        }

        public String getHostVerName() {
            PackageInfo info = LocalLeafBaseActivity.this.getPackageInfo(LocalLeafBaseActivity.this.getApplicationContext());
            if (info != null) {
                return info.versionName;
            }
            return null;
        }

        public Window getHostWindow() {
            return LocalLeafBaseActivity.this.getWindow();
        }

        public Window getContainerWindow(IPluginable iPluginable) {
            return LocalLeafBaseActivity.this.getWindow();
        }
    }

    public abstract LocalWidgetClient getWidgetClient();

    public abstract LocalWidgetView getWidgetView();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mWorkThread = new HandlerThread("local-work");
        this.mWorkThread.start();
        this.mWorkHandler = new Handler(this.mWorkThread.getLooper());
        this.mUIHandler = new Handler(getMainLooper());
        this.mWidgetClient = getWidgetClient();
        this.mWidgetView = getWidgetView();
        this.mWidgetClient.setLocalWidget(this.mWidgetView);
        this.mWidgetView.onBindHost(new ISpringBoardHostStubImpl());
        slotView();
        if (this.mWidgetView != null) {
            this.mWidgetView.onPreEnableByHost();
            this.mWidgetView.onPostEnableByHost();
            this.mWidgetView.onActive(savedInstanceState);
        }
    }

    private void slotView() {
        if (this.mContainer == null) {
            this.mContainer = new FrameLayout(this);
            setContentView(this.mContainer);
        }
        this.mContainer.removeAllViews();
        this.mContainer.addView(this.mWidgetView.getView(getApplicationContext()), new LayoutParams(-1, -1));
        this.mContainer.requestLayout();
    }

    protected void onResume() {
        super.onResume();
        if (this.mWidgetView != null) {
            this.mWidgetView.onResume();
        }
    }

    protected void onPause() {
        super.onPause();
        if (this.mWidgetView != null) {
            this.mWidgetView.onPause();
        }
    }

    protected void onStop() {
        super.onStop();
        if (this.mWidgetView != null) {
            this.mWidgetView.onInactive(new Bundle());
            this.mWidgetView.onStop();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mWidgetClient != null) {
            this.mWidgetClient.removeLocalWidget();
        }
        if (this.mWidgetView != null) {
            this.mWidgetView.onDestroy();
            this.mWidgetView.onInactive(new Bundle());
            this.mWidgetView.onPreDisableByHost();
            this.mWidgetView.onPostDisableByHost();
        }
        if (this.mWorkThread != null && this.mWorkThread.isAlive()) {
            this.mWorkThread.quit();
        }
    }

    private PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 16384);
        } catch (Exception e) {
            e.printStackTrace();
            return pi;
        }
    }
}
