package com.huami.watch.notification.stream;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.lang.ref.WeakReference;
import java.util.List;

public class StreamItemManager {
    public static final boolean DEBUG = Config.isDebug();
    private final ConnectionCallbacks mConnectionCallbacks;
    private int mConnectionState;
    private final Context mContext;
    private Handler mHandler;
    private final ArrayMap<OnStreamItemUpdatedListener, StreamItemChangedListenerRec> mListeners;
    private IStreamItemManagerService mServiceBinder;
    private IStreamItemManagerServiceCallbacks mServiceCallbacks;
    private StreamItemManagerServiceConnection mServiceConnection;
    private final ArrayMap<StreamItemId, StreamItemMutationRec> mStreamItemMutationCallbackMap;
    private boolean mSubscribedToStreamItemUpdate;

    public static abstract class OnStreamItemUpdatedListener {
        public abstract void onStreamItemAdded(StreamItem streamItem);

        public abstract void onStreamItemRemoved(StreamItem streamItem);

        public abstract void onStreamItemUpdated(StreamItem streamItem);

        public abstract void onStreamLoaded(List<StreamItem> list);

        public void onStreamItemContentUpdated(StreamItemId streamId, Bundle contentText) {
        }

        public void onStreamItemContentUpdateFailed(String packageName, int notificationId) {
        }
    }

    public static class ConnectionCallbacks {
        public void onConnected(Bundle connectionHints) {
        }

        public void onConnectionSuspended(int cause) {
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
        }
    }

    public interface OnMutationResultCallback {
        void onMutationResult(MutationResult mutationResult);
    }

    class C09321 implements Runnable {
        final /* synthetic */ StreamItemManager this$0;
        final /* synthetic */ ServiceConnection val$thisConnection;

        public void run() {
            if (this.val$thisConnection == this.this$0.mServiceConnection) {
                this.this$0.forceCloseConnection();
                this.this$0.mConnectionCallbacks.onConnectionFailed(new ConnectionResult(1, null));
            }
        }
    }

    private static class ServiceCallbacks extends Stub {
        private WeakReference<StreamItemManager> mStreamItemManager;

        public ServiceCallbacks(StreamItemManager streamItemManager) {
            this.mStreamItemManager = new WeakReference(streamItemManager);
        }

        public void onConnected(Bundle connectionHints) throws RemoteException {
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onServiceConnected(this, connectionHints);
            }
        }

        public void onConnectionFailed(ConnectionResult result) throws RemoteException {
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onConnectionFailed(this, result);
            }
        }

        public void onStreamItemsLoaded(List<StreamItem> list) throws RemoteException {
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemsLoaded(this, list);
            }
        }

        public void onStreamItemAdded(StreamItem item) throws RemoteException {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onStreamItemAdded(item=" + item + ")", new Object[0]);
            }
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemAdded(this, item);
            }
        }

        public void onStreamItemUpdated(StreamItem item) throws RemoteException {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onStreamItemUpdated(item=" + item + ")", new Object[0]);
            }
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemUpdated(this, item);
            }
        }

        public void onStreamItemContentUpdated(String tag, int id, Bundle content, String packageName) throws RemoteException {
            StreamItemId streamId = new StreamItemId(packageName, id, tag);
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onStreamItemContentUpdated(notification id =" + streamId + ")", new Object[0]);
            }
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemContentUpdated(this, streamId, content);
            }
        }

        public void onStreamItemContentUpdateFailed(String packageName, int notificationId) {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onStreamItemContentUpdateFailed(notificationId=" + notificationId + ")", new Object[0]);
            }
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemContentUpdateFailed(packageName, notificationId);
            }
        }

        public void onStreamItemRemoved(StreamItem item) throws RemoteException {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onStreamItemRemoved(itemId=" + item + ")", new Object[0]);
            }
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemRemoved(this, item);
            }
        }

        public void onStreamItemMutationResult(MutationResult result) throws RemoteException {
            StreamItemManager streamManager = (StreamItemManager) this.mStreamItemManager.get();
            if (streamManager != null) {
                streamManager.onStreamItemMutationResult(this, result);
            }
        }
    }

    private class StreamItemChangedListenerRec {
        public Handler handler;
        public OnStreamItemUpdatedListener listener;

        public StreamItemChangedListenerRec(OnStreamItemUpdatedListener _listener, Handler _handler) {
            this.listener = _listener;
            this.handler = _handler;
        }
    }

    private class StreamItemManagerServiceConnection implements ServiceConnection {
        final /* synthetic */ StreamItemManager this$0;

        public void onServiceConnected(ComponentName name, IBinder service) {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "StreamItemManagerServiceConnection.onServiceConnected(name=" + name + " binder=" + service, new Object[0]);
            }
            if (isCurrent()) {
                this.this$0.mServiceBinder = IStreamItemManagerService.Stub.asInterface(service);
                this.this$0.mServiceCallbacks = new ServiceCallbacks(this.this$0);
                this.this$0.mConnectionState = 1;
                try {
                    if (StreamItemManager.DEBUG) {
                        Log.m26d("StreamItemManager", "binding to service...", new Object[0]);
                    }
                    this.this$0.mServiceBinder.connect(this.this$0.mContext.getPackageName(), this.this$0.mServiceCallbacks);
                } catch (RemoteException e) {
                    Log.m32w("StreamItemManager", "RemoteException when connecting to stream manager service.", new Object[0]);
                }
            } else if (StreamItemManager.DEBUG) {
                Log.m32w("StreamItemManager", "Not current service connection in onServiceConnected(). ignore it.", new Object[0]);
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            if (StreamItemManager.DEBUG) {
                Log.m26d("StreamItemManager", "onServiceDisconnected(componentName=" + name + " )", new Object[0]);
            }
            if (isCurrent()) {
                this.this$0.mServiceBinder = null;
                this.this$0.mServiceCallbacks = null;
                this.this$0.mConnectionState = 3;
                this.this$0.mConnectionCallbacks.onConnectionSuspended(1);
            } else if (StreamItemManager.DEBUG) {
                Log.m32w("StreamItemManager", "Not current service connection in onServiceDisconnected. ignore it.", new Object[0]);
            }
        }

        private boolean isCurrent() {
            return this.this$0.mServiceConnection == this;
        }
    }

    private class StreamItemMutationRec {
        public StreamItemId itemId;
        public OnMutationResultCallback resultCallback;
    }

    private void forceCloseConnection() {
        if (this.mServiceConnection != null) {
            this.mContext.unbindService(this.mServiceConnection);
        }
        this.mConnectionState = 0;
        this.mServiceConnection = null;
        this.mServiceBinder = null;
        this.mServiceCallbacks = null;
        this.mSubscribedToStreamItemUpdate = false;
        this.mListeners.clear();
        this.mStreamItemMutationCallbackMap.clear();
    }

    public void addOnStreamItemUpdatedListener(@NonNull OnStreamItemUpdatedListener listener, @Nullable Handler handler) {
        if (listener == null) {
            throw new IllegalArgumentException("listener can not be null.");
        } else if (!isConnected()) {
            throw new IllegalStateException("addOnStreamItemUpdatedListener() is called when not connected to the service (connectionState=" + getStateString(this.mConnectionState) + ")");
        } else if (this.mListeners.containsKey(listener)) {
            throw new IllegalArgumentException("listener already registered.");
        } else {
            if (handler == null) {
                handler = new Handler();
            }
            this.mListeners.put(listener, new StreamItemChangedListenerRec(listener, handler));
            if (!this.mSubscribedToStreamItemUpdate) {
                subcribeToItemUpdates();
            }
        }
    }

    public boolean isConnected() {
        return this.mConnectionState == 2;
    }

    private void sendMutationCallback(StreamItemId itemId, OnMutationResultCallback callback, MutationResult mutationResult) {
        callback.onMutationResult(mutationResult);
        this.mStreamItemMutationCallbackMap.remove(itemId);
    }

    private static String getStateString(int connectionState) {
        switch (connectionState) {
            case 0:
                return "CONNECT_STATE_DISCONNECTED";
            case 1:
                return "CONNECT_STATE_CONNECTING";
            case 2:
                return "CONNECT_STATE_CONNECTED";
            case 3:
                return "CONNECT_STATE_SUSPENDED";
            default:
                return "UNKNOWN State:" + connectionState;
        }
    }

    private void onStreamItemRemoved(ServiceCallbacks serviceCallbacks, final StreamItem item) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemRemoved. mListeners size = " + StreamItemManager.this.mListeners.size(), new Object[0]);
                }
                for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                    Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                    if (h != null) {
                        h.post(new Runnable() {
                            public void run() {
                                listener.onStreamItemRemoved(item);
                            }
                        });
                    } else {
                        listener.onStreamItemRemoved(item);
                    }
                }
            }
        });
    }

    private void onStreamItemUpdated(ServiceCallbacks serviceCallbacks, final StreamItem item) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemUpdated. mListeners size = " + StreamItemManager.this.mListeners.size(), new Object[0]);
                }
                for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                    Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                    if (h != null) {
                        h.post(new Runnable() {
                            public void run() {
                                listener.onStreamItemUpdated(item);
                            }
                        });
                    } else {
                        listener.onStreamItemUpdated(item);
                    }
                }
            }
        });
    }

    private void onStreamItemContentUpdated(ServiceCallbacks serviceCallbacks, final StreamItemId streamId, final Bundle content) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemContentUpdated. mListeners size = " + StreamItemManager.this.mListeners.size(), new Object[0]);
                }
                for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                    Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                    if (h != null) {
                        h.post(new Runnable() {
                            public void run() {
                                listener.onStreamItemContentUpdated(streamId, content);
                            }
                        });
                    } else {
                        listener.onStreamItemContentUpdated(streamId, content);
                    }
                }
            }
        });
    }

    private void onStreamItemContentUpdateFailed(final String packageName, final int id) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemContentUpdateFailed. mListeners size = " + StreamItemManager.this.mListeners.size(), new Object[0]);
                    Log.m26d("StreamItemManager", "packageName:" + packageName + " id:" + id, new Object[0]);
                }
                for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                    Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                    if (h != null) {
                        h.post(new Runnable() {
                            public void run() {
                                listener.onStreamItemContentUpdateFailed(packageName, id);
                            }
                        });
                    } else {
                        listener.onStreamItemContentUpdateFailed(packageName, id);
                    }
                }
            }
        });
    }

    private void onStreamItemAdded(ServiceCallbacks serviceCallbacks, final StreamItem item) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemAdded. mListeners size = " + StreamItemManager.this.mListeners.size(), new Object[0]);
                }
                for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                    Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                    if (h != null) {
                        h.post(new Runnable() {
                            public void run() {
                                listener.onStreamItemAdded(item);
                            }
                        });
                    } else {
                        listener.onStreamItemAdded(item);
                    }
                }
            }
        });
    }

    private void onStreamItemMutationResult(final ServiceCallbacks serviceCallbacks, final MutationResult result) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemMutationResult(result=" + result + ").", new Object[0]);
                }
                if (StreamItemManager.this.isCurrent(serviceCallbacks)) {
                    StreamItemMutationRec rec = (StreamItemMutationRec) StreamItemManager.this.mStreamItemMutationCallbackMap.get(result.getItemId());
                    if (rec != null) {
                        StreamItemManager.this.sendMutationCallback(rec.itemId, rec.resultCallback, result);
                    } else if (StreamItemManager.DEBUG) {
                        Log.m32w("StreamItemManager", "Can't find any mutation callback to match result:" + result, new Object[0]);
                    }
                } else if (StreamItemManager.DEBUG) {
                    Log.m32w("StreamItemManager", "Not the same callback onStreamItemMutationResult(). Ignore it.", new Object[0]);
                }
            }
        });
    }

    private void onStreamItemsLoaded(final ServiceCallbacks serviceCallbacks, final List<StreamItem> list) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m26d("StreamItemManager", "onStreamItemsLoaded.", new Object[0]);
                }
                if (StreamItemManager.this.isCurrent(serviceCallbacks)) {
                    for (final OnStreamItemUpdatedListener listener : StreamItemManager.this.mListeners.keySet()) {
                        Handler h = ((StreamItemChangedListenerRec) StreamItemManager.this.mListeners.get(listener)).handler;
                        if (h != null) {
                            h.post(new Runnable() {
                                public void run() {
                                    listener.onStreamLoaded(list);
                                }
                            });
                        } else {
                            listener.onStreamLoaded(list);
                        }
                    }
                } else if (StreamItemManager.DEBUG) {
                    Log.m32w("StreamItemManager", "Not the same callback onStreamItemsLoaded(). Ignore it.", new Object[0]);
                }
            }
        });
    }

    private void onConnectionFailed(final ServiceCallbacks serviceCallbacks, final ConnectionResult result) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.DEBUG) {
                    Log.m28e("StreamItemManager", "Failed to connect to stream manager service.", new Object[0]);
                }
                if (StreamItemManager.this.isCurrent(serviceCallbacks)) {
                    if (StreamItemManager.this.mConnectionState == 1) {
                        StreamItemManager.this.forceCloseConnection();
                        StreamItemManager.this.mConnectionCallbacks.onConnectionFailed(result);
                    } else if (StreamItemManager.DEBUG) {
                        Log.m32w("StreamItemManager", "onConnect from service while mState=" + StreamItemManager.getStateString(StreamItemManager.this.mConnectionState) + "... ignoring", new Object[0]);
                    }
                } else if (StreamItemManager.DEBUG) {
                    Log.m32w("StreamItemManager", "Not the same callback onConnectionFailed(). Ignore it.", new Object[0]);
                }
            }
        });
    }

    private void onServiceConnected(final ServiceCallbacks serviceCallbacks, final Bundle connectionHints) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (StreamItemManager.this.isCurrent(serviceCallbacks)) {
                    if (StreamItemManager.this.mConnectionState == 1) {
                        StreamItemManager.this.mConnectionState = 2;
                        if (StreamItemManager.DEBUG) {
                            Log.m26d("StreamItemManager", "ServiceCallbacks.onServiceConnected()", new Object[0]);
                        }
                        StreamItemManager.this.mConnectionCallbacks.onConnected(connectionHints);
                        if (!StreamItemManager.this.mSubscribedToStreamItemUpdate) {
                            StreamItemManager.this.subcribeToItemUpdates();
                        }
                    } else if (StreamItemManager.DEBUG) {
                        Log.m32w("StreamItemManager", "onConnect from service while mState=" + StreamItemManager.getStateString(StreamItemManager.this.mConnectionState) + "... ignoring", new Object[0]);
                    }
                } else if (StreamItemManager.DEBUG) {
                    Log.m32w("StreamItemManager", "Not the same callback onServiceConnected(). Ignore it.", new Object[0]);
                }
            }
        });
    }

    private void subcribeToItemUpdates() {
        try {
            this.mServiceBinder.subscribe(this.mServiceCallbacks);
            this.mSubscribedToStreamItemUpdate = true;
        } catch (RemoteException e) {
            Log.m28e("StreamItemManager", "Can't subscribe to stream item changes.", new Object[0]);
        }
    }

    private boolean isCurrent(IStreamItemManagerServiceCallbacks callbacks) {
        if (this.mServiceCallbacks == callbacks) {
            return true;
        }
        if (this.mConnectionState != 0) {
        }
        return false;
    }
}
