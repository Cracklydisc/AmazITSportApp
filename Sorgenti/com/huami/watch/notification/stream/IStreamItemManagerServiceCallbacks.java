package com.huami.watch.notification.stream;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IStreamItemManagerServiceCallbacks extends IInterface {

    public static abstract class Stub extends Binder implements IStreamItemManagerServiceCallbacks {

        private static class Proxy implements IStreamItemManagerServiceCallbacks {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onConnected(Bundle connectionHints) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (connectionHints != null) {
                        _data.writeInt(1);
                        connectionHints.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onConnectionFailed(ConnectionResult result) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (result != null) {
                        _data.writeInt(1);
                        result.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(2, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemsLoaded(List<StreamItem> list) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    _data.writeTypedList(list);
                    this.mRemote.transact(3, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemAdded(StreamItem item) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (item != null) {
                        _data.writeInt(1);
                        item.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(4, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemUpdated(StreamItem item) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (item != null) {
                        _data.writeInt(1);
                        item.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(5, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemContentUpdated(String tag, int id, Bundle content, String packageName) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    _data.writeString(tag);
                    _data.writeInt(id);
                    if (content != null) {
                        _data.writeInt(1);
                        content.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(packageName);
                    this.mRemote.transact(6, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemContentUpdateFailed(String packageName, int notificationId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    _data.writeString(packageName);
                    _data.writeInt(notificationId);
                    this.mRemote.transact(7, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemRemoved(StreamItem item) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (item != null) {
                        _data.writeInt(1);
                        item.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(8, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemMutationResult(MutationResult result) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (result != null) {
                        _data.writeInt(1);
                        result.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(9, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
        }

        public static IStreamItemManagerServiceCallbacks asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
            if (iin == null || !(iin instanceof IStreamItemManagerServiceCallbacks)) {
                return new Proxy(obj);
            }
            return (IStreamItemManagerServiceCallbacks) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            StreamItem _arg0;
            switch (code) {
                case 1:
                    Bundle _arg02;
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg02 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    onConnected(_arg02);
                    return true;
                case 2:
                    ConnectionResult _arg03;
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg03 = (ConnectionResult) ConnectionResult.CREATOR.createFromParcel(data);
                    } else {
                        _arg03 = null;
                    }
                    onConnectionFailed(_arg03);
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    onStreamItemsLoaded(data.createTypedArrayList(StreamItem.CREATOR));
                    return true;
                case 4:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg0 = (StreamItem) StreamItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onStreamItemAdded(_arg0);
                    return true;
                case 5:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg0 = (StreamItem) StreamItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onStreamItemUpdated(_arg0);
                    return true;
                case 6:
                    Bundle _arg2;
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    String _arg04 = data.readString();
                    int _arg1 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg2 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg2 = null;
                    }
                    onStreamItemContentUpdated(_arg04, _arg1, _arg2, data.readString());
                    return true;
                case 7:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    onStreamItemContentUpdateFailed(data.readString(), data.readInt());
                    return true;
                case 8:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg0 = (StreamItem) StreamItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onStreamItemRemoved(_arg0);
                    return true;
                case 9:
                    MutationResult _arg05;
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    if (data.readInt() != 0) {
                        _arg05 = (MutationResult) MutationResult.CREATOR.createFromParcel(data);
                    } else {
                        _arg05 = null;
                    }
                    onStreamItemMutationResult(_arg05);
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onConnected(Bundle bundle) throws RemoteException;

    void onConnectionFailed(ConnectionResult connectionResult) throws RemoteException;

    void onStreamItemAdded(StreamItem streamItem) throws RemoteException;

    void onStreamItemContentUpdateFailed(String str, int i) throws RemoteException;

    void onStreamItemContentUpdated(String str, int i, Bundle bundle, String str2) throws RemoteException;

    void onStreamItemMutationResult(MutationResult mutationResult) throws RemoteException;

    void onStreamItemRemoved(StreamItem streamItem) throws RemoteException;

    void onStreamItemUpdated(StreamItem streamItem) throws RemoteException;

    void onStreamItemsLoaded(List<StreamItem> list) throws RemoteException;
}
