package com.huami.watch.notification.stream;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.Objects;

public class StreamItemId implements Parcelable {
    public static final Creator<StreamItemId> CREATOR = new C09311();
    private final String mCreatorPackageName;
    private String mIOSPkg;
    private String mIOSTitle;
    private byte[] mIOSUid;
    private boolean mIsIOS;
    private final int mNotificationId;
    private boolean mSupportCombin;
    private final String mTag;

    static class C09311 implements Creator<StreamItemId> {
        C09311() {
        }

        public StreamItemId createFromParcel(Parcel in) {
            return new StreamItemId(in);
        }

        public StreamItemId[] newArray(int size) {
            return new StreamItemId[size];
        }
    }

    public StreamItemId(@NonNull String creatorPackageName, @NonNull int notificationId, @Nullable String tag) {
        this.mCreatorPackageName = (String) Objects.requireNonNull(creatorPackageName, "creatorPackageName can't be null.");
        this.mNotificationId = ((Integer) Objects.requireNonNull(Integer.valueOf(notificationId), "notificationId can't be null.")).intValue();
        this.mTag = tag;
        this.mIOSPkg = "";
        this.mIOSUid = new byte[0];
    }

    @NonNull
    public String getCreatorPackageName() {
        return this.mCreatorPackageName;
    }

    @NonNull
    public int getNotificationId() {
        return this.mNotificationId;
    }

    @Nullable
    public String getTag() {
        return this.mTag;
    }

    public String getIOSPackage() {
        return this.mIOSPkg;
    }

    public byte[] getIOSUID() {
        return this.mIOSUid;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StreamItemId{");
        sb.append("mCreatorPackageName=").append(this.mCreatorPackageName);
        sb.append(", mNotificationId=").append(this.mNotificationId);
        sb.append(", mTag=").append(this.mTag);
        sb.append(", mIOSPkg=").append(this.mIOSPkg);
        sb.append(", mIOSUid=").append(Arrays.toString(this.mIOSUid));
        sb.append(", mIsIOS=").append(this.mIsIOS);
        sb.append('}');
        return sb.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mCreatorPackageName);
        dest.writeInt(this.mNotificationId);
        dest.writeString(this.mTag);
        dest.writeString(this.mIOSPkg);
        dest.writeByteArray(this.mIOSUid);
        dest.writeInt(this.mIsIOS ? 1 : 0);
    }

    private StreamItemId(Parcel in) {
        this.mCreatorPackageName = in.readString();
        this.mNotificationId = in.readInt();
        this.mTag = in.readString();
        this.mIOSPkg = in.readString();
        this.mIOSUid = in.createByteArray();
        this.mIsIOS = in.readInt() != 0;
    }

    public int hashCode() {
        if (!this.mIsIOS) {
            return ((((Objects.hashCode(this.mCreatorPackageName) + 527) * 31) + Objects.hashCode(Integer.valueOf(this.mNotificationId))) * 31) + Objects.hashCode(this.mTag);
        }
        if (this.mSupportCombin) {
            return ((Objects.hashCode(this.mIOSPkg) + 527) * 31) + Objects.hashCode(this.mIOSTitle);
        }
        return Arrays.hashCode(this.mIOSUid) + 527;
    }

    public boolean equals(Object o) {
        if (!(o instanceof StreamItemId)) {
            return false;
        }
        StreamItemId that = (StreamItemId) o;
        if (that == this) {
            return true;
        }
        if (this.mIsIOS) {
            if (!this.mSupportCombin) {
                return Arrays.equals(this.mIOSUid, that.getIOSUID());
            }
            if (Objects.equals(this.mIOSPkg, that.getIOSPackage()) && Objects.equals(this.mIOSTitle, that.getmIOSTitle())) {
                return true;
            }
            return false;
        } else if (Objects.equals(this.mCreatorPackageName, that.getCreatorPackageName()) && Objects.equals(Integer.valueOf(this.mNotificationId), Integer.valueOf(that.getNotificationId())) && Objects.equals(this.mTag, that.getTag())) {
            return true;
        } else {
            return false;
        }
    }

    public String getmIOSTitle() {
        return this.mIOSTitle;
    }

    public boolean getIOSSupportCombin() {
        return this.mSupportCombin;
    }
}
