package com.huami.watch.notification.stream;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStreamItemManagerService extends IInterface {

    public static abstract class Stub extends Binder implements IStreamItemManagerService {

        private static class Proxy implements IStreamItemManagerService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void connect(String packageName, IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    _data.writeString(packageName);
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void disconnect(IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(2, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void subscribe(IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(3, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void unsubscribe(IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(4, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void addOrReplaceStreamItem(StreamItem streamItem, IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (streamItem != null) {
                        _data.writeInt(1);
                        streamItem.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(5, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void removeStreamItem(StreamItem item, IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (item != null) {
                        _data.writeInt(1);
                        item.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(6, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void updateNotification(String tag, int id, Bundle content, String packageName) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    _data.writeString(tag);
                    _data.writeInt(id);
                    if (content != null) {
                        _data.writeInt(1);
                        content.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(packageName);
                    this.mRemote.transact(7, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void updateNotificationFailed(String packageName, int id) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.IStreamItemManagerService");
                    _data.writeString(packageName);
                    _data.writeInt(id);
                    this.mRemote.transact(8, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.notification.stream.IStreamItemManagerService");
        }

        public static IStreamItemManagerService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
            if (iin == null || !(iin instanceof IStreamItemManagerService)) {
                return new Proxy(obj);
            }
            return (IStreamItemManagerService) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            StreamItem _arg0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    connect(data.readString(), com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    disconnect(com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    subscribe(com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 4:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    unsubscribe(com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 5:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (data.readInt() != 0) {
                        _arg0 = (StreamItem) StreamItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    addOrReplaceStreamItem(_arg0, com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 6:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    if (data.readInt() != 0) {
                        _arg0 = (StreamItem) StreamItem.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    removeStreamItem(_arg0, com.huami.watch.notification.stream.IStreamItemManagerServiceCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 7:
                    Bundle _arg2;
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    String _arg02 = data.readString();
                    int _arg1 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg2 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg2 = null;
                    }
                    updateNotification(_arg02, _arg1, _arg2, data.readString());
                    return true;
                case 8:
                    data.enforceInterface("com.huami.watch.notification.stream.IStreamItemManagerService");
                    updateNotificationFailed(data.readString(), data.readInt());
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.notification.stream.IStreamItemManagerService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void addOrReplaceStreamItem(StreamItem streamItem, IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void connect(String str, IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void disconnect(IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void removeStreamItem(StreamItem streamItem, IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void subscribe(IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void unsubscribe(IStreamItemManagerServiceCallbacks iStreamItemManagerServiceCallbacks) throws RemoteException;

    void updateNotification(String str, int i, Bundle bundle, String str2) throws RemoteException;

    void updateNotificationFailed(String str, int i) throws RemoteException;
}
