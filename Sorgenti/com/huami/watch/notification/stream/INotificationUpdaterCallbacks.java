package com.huami.watch.notification.stream;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INotificationUpdaterCallbacks extends IInterface {

    public static abstract class Stub extends Binder implements INotificationUpdaterCallbacks {

        private static class Proxy implements INotificationUpdaterCallbacks {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void onConnected(Bundle connectionHints) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    if (connectionHints != null) {
                        _data.writeInt(1);
                        connectionHints.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onConnectionFailed(ConnectionResult result) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    if (result != null) {
                        _data.writeInt(1);
                        result.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(2, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void onStreamItemContentUpdateFailed(String packageName, int notificationId) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    _data.writeString(packageName);
                    _data.writeInt(notificationId);
                    this.mRemote.transact(3, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
        }

        public static INotificationUpdaterCallbacks asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
            if (iin == null || !(iin instanceof INotificationUpdaterCallbacks)) {
                return new Proxy(obj);
            }
            return (INotificationUpdaterCallbacks) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    Bundle _arg0;
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    if (data.readInt() != 0) {
                        _arg0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    onConnected(_arg0);
                    return true;
                case 2:
                    ConnectionResult _arg02;
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    if (data.readInt() != 0) {
                        _arg02 = (ConnectionResult) ConnectionResult.CREATOR.createFromParcel(data);
                    } else {
                        _arg02 = null;
                    }
                    onConnectionFailed(_arg02);
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    onStreamItemContentUpdateFailed(data.readString(), data.readInt());
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.notification.stream.INotificationUpdaterCallbacks");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void onConnected(Bundle bundle) throws RemoteException;

    void onConnectionFailed(ConnectionResult connectionResult) throws RemoteException;

    void onStreamItemContentUpdateFailed(String str, int i) throws RemoteException;
}
