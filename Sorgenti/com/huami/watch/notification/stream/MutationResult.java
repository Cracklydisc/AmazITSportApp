package com.huami.watch.notification.stream;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class MutationResult implements Parcelable {
    public static final Creator<MutationResult> CREATOR = new C09291();
    private final int mCode;
    private final String mDescription;
    private final StreamItemId mItemId;
    private final int mType;

    static class C09291 implements Creator<MutationResult> {
        C09291() {
        }

        public MutationResult createFromParcel(Parcel in) {
            return new MutationResult(in);
        }

        public MutationResult[] newArray(int size) {
            return new MutationResult[size];
        }
    }

    public MutationResult(@NonNull StreamItemId itemId, @NonNull int code, @NonNull int type, @Nullable String description) {
        this.mItemId = itemId;
        this.mCode = code;
        this.mType = type;
        this.mDescription = description;
    }

    @NonNull
    public StreamItemId getItemId() {
        return this.mItemId;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("MutationResult{");
        sb.append("mItemId=").append(this.mItemId);
        sb.append(", mCode=").append(this.mCode);
        sb.append(", mType=").append(this.mType);
        sb.append(", mDescription=").append(this.mDescription);
        sb.append('}');
        return sb.toString();
    }

    private MutationResult(Parcel in) {
        this.mItemId = (StreamItemId) in.readParcelable(StreamItemId.class.getClassLoader());
        this.mCode = in.readInt();
        this.mType = in.readInt();
        this.mDescription = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mItemId, flags);
        dest.writeInt(this.mCode);
        dest.writeInt(this.mType);
        dest.writeString(this.mDescription);
    }
}
