package com.huami.watch.notification.stream;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import com.huami.watch.notification.data.NotificationData;
import com.huami.watch.notification.data.StatusBarNotificationData;

public class StreamItem implements Parcelable {
    public static final Creator<StreamItem> CREATOR = new C09301();
    private final StreamItemId mId;
    private final boolean mIsIOS;
    private final boolean mIsLocal;
    private final StatusBarNotificationData mNotificationData;
    private String mNotificationGroupKey;
    private String mNotificationKey;

    static class C09301 implements Creator<StreamItem> {
        C09301() {
        }

        public StreamItem createFromParcel(Parcel in) {
            return new StreamItem(in);
        }

        public StreamItem[] newArray(int size) {
            return new StreamItem[size];
        }
    }

    public StreamItem(@NonNull StreamItem item, boolean withoutNotiData) {
        this.mId = item.getId();
        if (!withoutNotiData) {
            this.mNotificationData = item.getStatusBarNotification();
        } else if (item.mIsLocal || item.getStatusBarNotification() == null || item.getStatusBarNotification().notification == null) {
            this.mNotificationData = null;
        } else {
            StatusBarNotificationData emptyData = StatusBarNotificationData.empty();
            emptyData.notification = NotificationData.empty();
            emptyData.notification.key = item.getStatusBarNotification().notification.key;
            emptyData.notification.isLocal = false;
            this.mNotificationData = emptyData;
        }
        if (item.getStatusBarNotification() != null) {
            this.mNotificationKey = item.getStatusBarNotification().key;
            this.mNotificationGroupKey = item.getStatusBarNotification().groupKey;
        } else {
            this.mNotificationKey = item.mNotificationKey;
            this.mNotificationGroupKey = item.mNotificationGroupKey;
        }
        this.mIsLocal = item.mIsLocal;
        this.mIsIOS = item.mIsIOS;
    }

    private StreamItem(Parcel in) {
        boolean z = true;
        this.mId = (StreamItemId) in.readParcelable(StreamItemId.class.getClassLoader());
        this.mNotificationData = (StatusBarNotificationData) in.readParcelable(StatusBarNotificationData.class.getClassLoader());
        this.mIsLocal = in.readInt() != 0;
        this.mNotificationKey = in.readString();
        this.mNotificationGroupKey = in.readString();
        if (in.readInt() == 0) {
            z = false;
        }
        this.mIsIOS = z;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i = 1;
        dest.writeParcelable(this.mId, flags);
        dest.writeParcelable(this.mNotificationData, flags);
        dest.writeInt(this.mIsLocal ? 1 : 0);
        dest.writeString(this.mNotificationKey);
        dest.writeString(this.mNotificationGroupKey);
        if (!this.mIsIOS) {
            i = 0;
        }
        dest.writeInt(i);
    }

    @NonNull
    public StreamItemId getId() {
        return this.mId;
    }

    public StatusBarNotificationData getStatusBarNotification() {
        return this.mNotificationData;
    }

    public NotificationData getNotification() {
        if (this.mNotificationData == null) {
            return null;
        }
        return this.mNotificationData.notification;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("StreamItem{");
        sb.append("mNd=").append(this.mNotificationData);
        sb.append(", mIsLocal=").append(this.mIsLocal);
        sb.append(", mIsIOS=").append(this.mIsIOS);
        sb.append('}');
        return super.toString() + "\n" + sb.toString();
    }
}
