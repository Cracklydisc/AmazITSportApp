package com.huami.watch.notification.stream;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface INotificationUpdater extends IInterface {

    public static abstract class Stub extends Binder implements INotificationUpdater {

        private static class Proxy implements INotificationUpdater {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void connect(String packageName, INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdater");
                    _data.writeString(packageName);
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void disconnect(INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdater");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(2, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void subscribe(INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdater");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(3, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void unsubscribe(INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
                IBinder iBinder = null;
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdater");
                    if (serviceCallbacks != null) {
                        iBinder = serviceCallbacks.asBinder();
                    }
                    _data.writeStrongBinder(iBinder);
                    this.mRemote.transact(4, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void updateNotification(String tag, int id, Bundle content, String packageName) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.notification.stream.INotificationUpdater");
                    _data.writeString(tag);
                    _data.writeInt(id);
                    if (content != null) {
                        _data.writeInt(1);
                        content.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeString(packageName);
                    this.mRemote.transact(5, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.notification.stream.INotificationUpdater");
        }

        public static INotificationUpdater asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.notification.stream.INotificationUpdater");
            if (iin == null || !(iin instanceof INotificationUpdater)) {
                return new Proxy(obj);
            }
            return (INotificationUpdater) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdater");
                    connect(data.readString(), com.huami.watch.notification.stream.INotificationUpdaterCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 2:
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdater");
                    disconnect(com.huami.watch.notification.stream.INotificationUpdaterCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 3:
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdater");
                    subscribe(com.huami.watch.notification.stream.INotificationUpdaterCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 4:
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdater");
                    unsubscribe(com.huami.watch.notification.stream.INotificationUpdaterCallbacks.Stub.asInterface(data.readStrongBinder()));
                    return true;
                case 5:
                    Bundle _arg2;
                    data.enforceInterface("com.huami.watch.notification.stream.INotificationUpdater");
                    String _arg0 = data.readString();
                    int _arg1 = data.readInt();
                    if (data.readInt() != 0) {
                        _arg2 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    } else {
                        _arg2 = null;
                    }
                    updateNotification(_arg0, _arg1, _arg2, data.readString());
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.notification.stream.INotificationUpdater");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void connect(String str, INotificationUpdaterCallbacks iNotificationUpdaterCallbacks) throws RemoteException;

    void disconnect(INotificationUpdaterCallbacks iNotificationUpdaterCallbacks) throws RemoteException;

    void subscribe(INotificationUpdaterCallbacks iNotificationUpdaterCallbacks) throws RemoteException;

    void unsubscribe(INotificationUpdaterCallbacks iNotificationUpdaterCallbacks) throws RemoteException;

    void updateNotification(String str, int i, Bundle bundle, String str2) throws RemoteException;
}
