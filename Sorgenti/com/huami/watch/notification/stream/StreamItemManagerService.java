package com.huami.watch.notification.stream;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.ArrayMap;
import com.huami.watch.notification.stream.IStreamItemManagerService.Stub;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.util.ArrayList;
import java.util.Map.Entry;

public class StreamItemManagerService extends Service {
    private static final ArrayList<String> CLIENT_WHITELIST = new ArrayList();
    public static final boolean DEBUG = Config.isDebug();
    private ServiceBinder mBinder;
    private final ArrayMap<IBinder, ConnectionRecord> mConnections = new ArrayMap();
    private final Handler mHandler = new Handler();
    private final ArrayMap<StreamItemId, StreamItem> mStreamItemsMap = new ArrayMap();
    private ServiceUpdateBinder mUpdateBinder;
    private final ArrayMap<IBinder, UpdateConnectionRecord> mUpdateConnections = new ArrayMap();

    private class ConnectionRecord {
        IStreamItemManagerServiceCallbacks callbacks;
        boolean hasSubscribed;
        String pkg;

        private ConnectionRecord() {
        }
    }

    private class ServiceBinder extends Stub {
        private ServiceBinder() {
        }

        public void connect(final String packageName, final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            int uid = Binder.getCallingUid();
            if (StreamItemManagerService.this.isAuthorizedPackage(packageName, uid)) {
                StreamItemManagerService.this.mHandler.post(new Runnable() {
                    public void run() {
                        IBinder b = serviceCallbacks.asBinder();
                        StreamItemManagerService.this.mConnections.remove(b);
                        ConnectionRecord connection = new ConnectionRecord();
                        connection.pkg = packageName;
                        connection.callbacks = serviceCallbacks;
                        connection.hasSubscribed = false;
                        try {
                            StreamItemManagerService.this.mConnections.put(b, connection);
                            serviceCallbacks.onConnected(new Bundle());
                            b.linkToDeath(new callbackDiedHandler(b), 0);
                        } catch (RemoteException e) {
                            Log.m32w("StreamItemService", "Calling onConnect() failed. Dropping client. pkg=" + packageName, new Object[0]);
                            StreamItemManagerService.this.mConnections.remove(b);
                        }
                    }
                });
                return;
            }
            throw new IllegalArgumentException("Package/uid mismatch: uid=" + uid + " package=" + packageName);
        }

        public void disconnect(final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    ConnectionRecord old = (ConnectionRecord) StreamItemManagerService.this.mConnections.remove(serviceCallbacks.asBinder());
                    if (old != null && StreamItemManagerService.DEBUG) {
                        Log.m26d("StreamItemService", "Removed ConnectionRecord : " + old, new Object[0]);
                    }
                }
            });
        }

        public void subscribe(final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    ConnectionRecord connection = (ConnectionRecord) StreamItemManagerService.this.mConnections.get(serviceCallbacks.asBinder());
                    if (connection != null) {
                        connection.hasSubscribed = true;
                        try {
                            serviceCallbacks.onStreamItemsLoaded(new ArrayList(StreamItemManagerService.this.mStreamItemsMap.values()));
                        } catch (RemoteException e) {
                            Log.m32w("StreamItemService", "Can't invoke client callback onStreamItemsLoaded(). Probably binder died?", new Object[0]);
                        }
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "subscribe for callback that isn't connected.", new Object[0]);
                    }
                }
            });
        }

        public void unsubscribe(final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    ConnectionRecord connection = (ConnectionRecord) StreamItemManagerService.this.mConnections.get(serviceCallbacks.asBinder());
                    if (connection != null) {
                        connection.hasSubscribed = false;
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "unsubscribe for callback that isn't subscribed.", new Object[0]);
                    }
                }
            });
        }

        public void addOrReplaceStreamItem(final StreamItem streamItem, final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    if (((ConnectionRecord) StreamItemManagerService.this.mConnections.get(serviceCallbacks.asBinder())) != null) {
                        NewNotificationUtils.amendChatStreamItem(streamItem);
                        StreamItem oldStreamItem = (StreamItem) StreamItemManagerService.this.mStreamItemsMap.get(streamItem.getId());
                        if (oldStreamItem != null) {
                            boolean mIsIOS = streamItem.getStatusBarNotification().isIOS;
                            String pkg = mIsIOS ? streamItem.getStatusBarNotification().iOSPkg : streamItem.getStatusBarNotification().pkg;
                            if (mIsIOS) {
                                if (streamItem.getId().getIOSSupportCombin()) {
                                    NewNotificationUtils.updateChatMsg(streamItem, oldStreamItem);
                                }
                            } else if (NewNotificationUtils.checkChatNotification(pkg)) {
                                NewNotificationUtils.updateChatMsg(streamItem, oldStreamItem);
                            }
                            StreamItemManagerService.this.mStreamItemsMap.put(streamItem.getId(), streamItem);
                            try {
                                for (Entry<IBinder, ConnectionRecord> map : StreamItemManagerService.this.mConnections.entrySet()) {
                                    ((ConnectionRecord) map.getValue()).callbacks.onStreamItemUpdated(streamItem);
                                }
                                serviceCallbacks.onStreamItemMutationResult(new MutationResult(streamItem.getId(), 0, 2, ""));
                                return;
                            } catch (RemoteException e) {
                                Log.m31w("StreamItemService", "Can't call client callback onStreamItemUpdated(). Probably binder died?", e, new Object[0]);
                                return;
                            }
                        }
                        StreamItemManagerService.this.mStreamItemsMap.put(streamItem.getId(), streamItem);
                        try {
                            for (Entry<IBinder, ConnectionRecord> map2 : StreamItemManagerService.this.mConnections.entrySet()) {
                                ((ConnectionRecord) map2.getValue()).callbacks.onStreamItemAdded(streamItem);
                            }
                            serviceCallbacks.onStreamItemMutationResult(new MutationResult(streamItem.getId(), 0, 1, ""));
                        } catch (RemoteException e2) {
                            Log.m31w("StreamItemService", "Can't call client callback onStreamItemAdded(). Probably binder died?", e2, new Object[0]);
                        }
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "addOrReplaceStreamItem for callback that isn't connected.", new Object[0]);
                    }
                }
            });
        }

        public void removeStreamItem(final StreamItem item, final IStreamItemManagerServiceCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    RemoteException e;
                    if (((ConnectionRecord) StreamItemManagerService.this.mConnections.get(serviceCallbacks.asBinder())) == null) {
                        if (StreamItemManagerService.DEBUG) {
                            Log.m32w("StreamItemService", "removeStreamItem for callback that isn't connected.", new Object[0]);
                        }
                    } else if (item != null) {
                        StreamItem originItem = (StreamItem) StreamItemManagerService.this.mStreamItemsMap.remove(item.getId());
                        if (originItem == null) {
                            Log.m26d("StreamItemService", item + " has been removed", new Object[0]);
                            return;
                        }
                        try {
                            StreamItem originItem2 = new StreamItem(originItem, true);
                            try {
                                for (Entry<IBinder, ConnectionRecord> map : StreamItemManagerService.this.mConnections.entrySet()) {
                                    ((ConnectionRecord) map.getValue()).callbacks.onStreamItemRemoved(originItem2);
                                }
                                serviceCallbacks.onStreamItemMutationResult(new MutationResult(item.getId(), 0, 2, ""));
                            } catch (RemoteException e2) {
                                e = e2;
                                originItem = originItem2;
                                Log.m31w("StreamItemService", "Can't call client callback onStreamItemRemoved(). Probably binder died?", e, new Object[0]);
                            }
                        } catch (RemoteException e3) {
                            e = e3;
                            Log.m31w("StreamItemService", "Can't call client callback onStreamItemRemoved(). Probably binder died?", e, new Object[0]);
                        }
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "The stream item to be removed doesn't exist. itemId=" + item.getId(), new Object[0]);
                    }
                }
            });
        }

        public void updateNotification(String tag, int id, Bundle content, String packageName) throws RemoteException {
        }

        public void updateNotificationFailed(String packageName, int notificationId) throws RemoteException {
            if (!TextUtils.isEmpty(packageName)) {
                for (Entry<IBinder, UpdateConnectionRecord> map : StreamItemManagerService.this.mUpdateConnections.entrySet()) {
                    try {
                        UpdateConnectionRecord record = (UpdateConnectionRecord) map.getValue();
                        if (packageName.equals(record.pkg) && record.hasSubscribed) {
                            record.callbacks.onStreamItemContentUpdateFailed(packageName, notificationId);
                            return;
                        } else if (packageName.equals(record.pkg)) {
                            return;
                        }
                    } catch (RemoteException e) {
                        Log.m31w("StreamItemService", "Can't call client callback onStreamItemRemoved(). Probably binder died?", e, new Object[0]);
                    }
                }
            }
        }
    }

    private class ServiceUpdateBinder extends INotificationUpdater.Stub {
        private ServiceUpdateBinder() {
        }

        public void updateNotification(String tag, int id, Bundle content, String packageName) throws RemoteException {
            for (Entry<IBinder, ConnectionRecord> map : StreamItemManagerService.this.mConnections.entrySet()) {
                ((ConnectionRecord) map.getValue()).callbacks.onStreamItemContentUpdated(tag, id, content, packageName);
            }
        }

        public void connect(final String packageName, final INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    IBinder b = serviceCallbacks.asBinder();
                    StreamItemManagerService.this.mUpdateConnections.remove(b);
                    UpdateConnectionRecord connection = new UpdateConnectionRecord();
                    connection.pkg = packageName;
                    connection.callbacks = serviceCallbacks;
                    connection.hasSubscribed = false;
                    try {
                        StreamItemManagerService.this.mUpdateConnections.put(b, connection);
                        serviceCallbacks.onConnected(new Bundle());
                        b.linkToDeath(new UpdateContentDiedHandler(b), 0);
                    } catch (RemoteException e) {
                        Log.m32w("StreamItemService", "Calling onConnect() failed. Dropping client. pkg=" + packageName, new Object[0]);
                        StreamItemManagerService.this.mConnections.remove(b);
                    }
                }
            });
        }

        public void disconnect(final INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    UpdateConnectionRecord old = (UpdateConnectionRecord) StreamItemManagerService.this.mUpdateConnections.remove(serviceCallbacks.asBinder());
                    if (old != null && StreamItemManagerService.DEBUG) {
                        Log.m26d("StreamItemService", "Removed mUpdateConnections : " + old, new Object[0]);
                    }
                }
            });
        }

        public void subscribe(final INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    UpdateConnectionRecord connection = (UpdateConnectionRecord) StreamItemManagerService.this.mUpdateConnections.get(serviceCallbacks.asBinder());
                    if (connection != null) {
                        connection.hasSubscribed = true;
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "subscribe for callback that isn't connected.", new Object[0]);
                    }
                }
            });
        }

        public void unsubscribe(final INotificationUpdaterCallbacks serviceCallbacks) throws RemoteException {
            StreamItemManagerService.this.mHandler.post(new Runnable() {
                public void run() {
                    UpdateConnectionRecord connection = (UpdateConnectionRecord) StreamItemManagerService.this.mUpdateConnections.get(serviceCallbacks.asBinder());
                    if (connection != null) {
                        connection.hasSubscribed = false;
                    } else if (StreamItemManagerService.DEBUG) {
                        Log.m32w("StreamItemService", "unsubscribe for callback that isn't subscribed.", new Object[0]);
                    }
                }
            });
        }
    }

    private class UpdateConnectionRecord {
        INotificationUpdaterCallbacks callbacks;
        boolean hasSubscribed;
        String pkg;

        private UpdateConnectionRecord() {
        }
    }

    private class UpdateContentDiedHandler implements DeathRecipient {
        private IBinder mClient;

        public UpdateContentDiedHandler(IBinder b) {
            this.mClient = b;
        }

        public void binderDied() {
            if (StreamItemManagerService.this.mConnections != null) {
                StreamItemManagerService.this.mUpdateConnections.remove(this.mClient);
                Log.m28e("StreamItemService", "client died, remove the connection record", new Object[0]);
            }
        }
    }

    private class callbackDiedHandler implements DeathRecipient {
        private IBinder mClient;

        public callbackDiedHandler(IBinder b) {
            this.mClient = b;
        }

        public void binderDied() {
            if (StreamItemManagerService.this.mConnections != null) {
                StreamItemManagerService.this.mConnections.remove(this.mClient);
                Log.m28e("StreamItemService", "client died, remove the connection record", new Object[0]);
            }
        }
    }

    static {
        CLIENT_WHITELIST.add("com.ingenic.iwds.device");
        CLIENT_WHITELIST.add("com.huami.watch.launcher");
    }

    public void onCreate() {
        super.onCreate();
        this.mBinder = new ServiceBinder();
    }

    public IBinder onBind(Intent intent) {
        if (intent == null || !"com.huami.watch.notification.stream.UpdateService".equals(intent.getAction())) {
            android.util.Log.d("StreamItemService", "onBind return binder is ServiceBinder");
            return this.mBinder;
        }
        android.util.Log.d("StreamItemService", "onBind return binder is ServiceUpdateBinder");
        this.mUpdateBinder = new ServiceUpdateBinder();
        return this.mUpdateBinder;
    }

    private boolean isAuthorizedPackage(String packageName, int uid) {
        return true;
    }
}
