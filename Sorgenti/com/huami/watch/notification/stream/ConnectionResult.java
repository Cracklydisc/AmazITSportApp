package com.huami.watch.notification.stream;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ConnectionResult implements Parcelable {
    public static final Creator<ConnectionResult> CREATOR = new C09281();
    private PendingIntent mPendingIntent;
    private int mStatusCode;

    static class C09281 implements Creator<ConnectionResult> {
        C09281() {
        }

        public ConnectionResult createFromParcel(Parcel in) {
            return new ConnectionResult(in);
        }

        public ConnectionResult[] newArray(int size) {
            return new ConnectionResult[size];
        }
    }

    public ConnectionResult(int statusCode, PendingIntent pendingIntent) {
        this.mStatusCode = statusCode;
        this.mPendingIntent = pendingIntent;
    }

    private ConnectionResult(Parcel in) {
        this.mStatusCode = in.readInt();
        this.mPendingIntent = (PendingIntent) in.readParcelable(PendingIntent.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mStatusCode);
        dest.writeParcelable(this.mPendingIntent, flags);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ConnectionResult{");
        sb.append("mStatusCode=").append(this.mStatusCode);
        sb.append(", mPendingIntent=").append(this.mPendingIntent);
        sb.append('}');
        return sb.toString();
    }
}
