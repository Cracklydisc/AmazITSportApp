package com.huami.watch.notification.stream;

import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.huami.watch.notification.data.NotificationData;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewNotificationUtils {
    private static String[] supportChatApp = new String[]{"com.tencent.mm", "com.android.mms", "com.tencent.mm.hmhb"};

    public static void amendChatStreamItem(StreamItem streamItem) {
        String pkg;
        Object name = null;
        if (streamItem.getStatusBarNotification().isIOS) {
            pkg = streamItem.getStatusBarNotification().iOSPkg;
        } else {
            pkg = streamItem.getStatusBarNotification().pkg;
        }
        if (checkChatNotification(pkg)) {
            name = changeSingleOrMultiMsgForm(streamItem);
        }
        JSONObject newStreamObj = new JSONObject();
        Object newStreamArray = new JSONArray();
        try {
            newStreamObj.put("MsgArray", newStreamArray);
            JSONObject contentObj = new JSONObject();
            contentObj.put("time", Long.valueOf(streamItem.getNotification().when));
            contentObj.put("data", streamItem.getNotification().text);
            if (!TextUtils.isEmpty(name)) {
                contentObj.put("name", name);
            }
            newStreamArray.add(contentObj);
            streamItem.getNotification().text = newStreamObj.toString();
        } catch (JSONException e) {
            Log.w("NewNotification", "Can't amendChatStreamItem", e);
        }
    }

    public static boolean checkChatNotification(String pkg) {
        if (TextUtils.isEmpty(pkg) || supportChatApp == null || supportChatApp.length <= 0) {
            return false;
        }
        for (String supportPkg : supportChatApp) {
            if (supportPkg.equals(pkg)) {
                return true;
            }
        }
        return false;
    }

    private static String changeSingleOrMultiMsgForm(StreamItem streamItem) {
        Matcher matcher = Pattern.compile(":").matcher(streamItem.getNotification().text);
        int startIndex = -1;
        int endIndex = -1;
        if (matcher.find()) {
            startIndex = matcher.start();
            endIndex = matcher.end();
        }
        boolean isSingleMsg = true;
        String name = null;
        if (startIndex != -1 && endIndex != -1) {
            name = streamItem.getNotification().text.substring(0, startIndex);
            if (!TextUtils.isEmpty(name)) {
                streamItem.getNotification().text = streamItem.getNotification().text.substring(endIndex + 1, streamItem.getNotification().text.length());
                isSingleMsg = name.equals(streamItem.getNotification().title);
            }
        } else if (startIndex == -1 && endIndex == -1) {
            matcher = Pattern.compile("\".+\"").matcher(streamItem.getNotification().text);
            if (matcher.find()) {
                startIndex = matcher.start();
                endIndex = matcher.end();
            }
            if (!(startIndex == -1 || endIndex == -1)) {
                name = streamItem.getNotification().text.substring(startIndex + 1, endIndex - 1);
                if (!TextUtils.isEmpty(name)) {
                    isSingleMsg = name.equals(streamItem.getNotification().title);
                }
            }
        }
        return isSingleMsg ? null : name;
    }

    public static void updateChatMsg(StreamItem newStreamItem, StreamItem oldStreamItem) {
        NotificationData newNotification = newStreamItem.getNotification();
        NotificationData oldNotification = oldStreamItem.getNotification();
        try {
            String newTime = "-1";
            JSONObject newChat = null;
            int index = -1;
            JSONArray newChatArray = JSON.parseObject(newNotification.text).getJSONArray("MsgArray");
            if (newChatArray != null && newChatArray.size() > 0) {
                newChat = (JSONObject) newChatArray.get(0);
                newTime = newChat.getString("time");
            }
            JSONObject oldObject = JSON.parseObject(oldNotification.text);
            JSONArray oldChatArray = oldObject.getJSONArray("MsgArray");
            if (oldChatArray != null && oldChatArray.size() > 0) {
                if (oldChatArray.size() == 99) {
                    oldChatArray.remove(98);
                }
                for (int i = 0; i < oldChatArray.size(); i++) {
                    if (Long.parseLong(newTime) >= Long.parseLong(((JSONObject) oldChatArray.get(i)).getString("time"))) {
                        index = i;
                        break;
                    }
                }
                if (index == -1) {
                    oldChatArray.add(newChat);
                } else if (filerRepeatedMsg(newChat, oldChatArray)) {
                    oldChatArray.add(index, newChat);
                } else {
                    oldChatArray.set(index, newChat);
                }
                newNotification.text = oldObject.toString();
            }
        } catch (JSONException e) {
            Log.w("NewNotification", "Can't updateChatMsg", e);
        }
    }

    private static boolean filerRepeatedMsg(JSONObject newObject, JSONArray oldChatArray) {
        if (newObject == null) {
            return false;
        }
        String newData = newObject.getString("data");
        String oldData = ((JSONObject) oldChatArray.get(oldChatArray.size() - 1)).getString("data");
        if (TextUtils.isEmpty(newData)) {
            return false;
        }
        if (TextUtils.isEmpty(oldData)) {
            return true;
        }
        Pattern zhPattern = Pattern.compile("\\d+个联系人发来\\d+条消息");
        Matcher newZhMatcher = zhPattern.matcher(newData);
        Matcher oldZhMatcher = zhPattern.matcher(oldData);
        Pattern engPattern = Pattern.compile("\\d+\\sfriend\\(s\\)\\ssent\\syou\\s\\d+\\smessage\\(s\\)");
        Matcher newEngMatcher = engPattern.matcher(newData);
        Matcher oldEngMatcher = engPattern.matcher(oldData);
        if ((newZhMatcher.find() || newEngMatcher.find()) && (oldZhMatcher.find() || oldEngMatcher.find())) {
            return false;
        }
        return true;
    }
}
