package com.huami.watch.notification;

import android.os.Bundle;
import com.huami.watch.notification.stream.ConnectionResult;
import com.huami.watch.notification.stream.MutationResult;
import com.huami.watch.notification.stream.StreamItem;
import com.huami.watch.notification.stream.StreamItemId;
import com.huami.watch.notification.stream.StreamItemManager;
import com.huami.watch.notification.stream.StreamItemManager.ConnectionCallbacks;
import com.huami.watch.notification.stream.StreamItemManager.OnMutationResultCallback;
import com.huami.watch.notification.stream.StreamItemManager.OnStreamItemUpdatedListener;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.util.List;

public class NotificationPoster {
    public static final boolean DEBUG = Config.isDebug();
    private StreamItemManager mStreamItemManager;

    class C09191 extends ConnectionCallbacks {
        final /* synthetic */ NotificationPoster this$0;

        class C09181 extends OnStreamItemUpdatedListener {
            C09181() {
            }

            public void onStreamLoaded(List<StreamItem> list) {
            }

            public void onStreamItemAdded(StreamItem item) {
            }

            public void onStreamItemUpdated(StreamItem item) {
            }

            public void onStreamItemContentUpdateFailed(String packageName, int notificationId) {
            }

            public void onStreamItemRemoved(StreamItem item) {
            }

            public void onStreamItemContentUpdated(StreamItemId streamId, Bundle content) {
            }
        }

        public void onConnected(Bundle connectionHints) {
            super.onConnected(connectionHints);
            Log.m29i("Noti-Manager", "StreamItemManager onConnected().", new Object[0]);
            this.this$0.mStreamItemManager.addOnStreamItemUpdatedListener(new C09181(), null);
        }

        public void onConnectionSuspended(int cause) {
            super.onConnectionSuspended(cause);
            if (NotificationPoster.DEBUG) {
                Log.m29i("Noti-Manager", "StreamItemManager onConnectionSuspended(cause=" + cause + " )", new Object[0]);
            }
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            super.onConnectionFailed(connectionResult);
            if (NotificationPoster.DEBUG) {
                Log.m29i("Noti-Manager", "StreamItemManager onConnectionFailed(connectionResult=" + connectionResult + ").", new Object[0]);
            }
        }
    }

    class C09202 implements OnMutationResultCallback {
        public void onMutationResult(MutationResult result) {
            if (NotificationPoster.DEBUG) {
                Log.m26d("Noti-Manager", "StreamItemManager onMutationResult(result=" + result + ")", new Object[0]);
            }
        }
    }

    private NotificationPoster() {
    }
}
