package com.huami.watch.notification.data;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public final class Utils {

    public static class BitmapExtender {
        public boolean canRecycle = true;
    }

    private Utils() {
    }

    public static Bitmap readBmp(Parcel parcel) {
        byte[] bytes = parcel.createByteArray();
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static void writeBmp(Parcel parcel, Bitmap bmp) {
        parcel.writeByteArray(bmp == null ? null : bmpToBytes(bmp));
    }

    public static byte[] bmpToBytes(Bitmap bmp) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.PNG, 100, out);
        return out.toByteArray();
    }

    public static <T extends Parcelable> T[] readParcelableArray(Parcel parcel, Class<? extends T[]> newType) {
        Parcelable[] array = parcel.readParcelableArray(newType.getClassLoader());
        if (array == null) {
            return null;
        }
        return (Parcelable[]) Arrays.copyOf(array, array.length, newType);
    }
}
