package com.huami.watch.notification.data;

import android.app.Notification;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;
import java.util.Arrays;

public class NotificationData implements Parcelable {
    public static final Creator<NotificationData> CREATOR = new C09211();
    public static final boolean DEBUG = Config.isDebug();
    public ActionData[] actions;
    public String contentInfo;
    public int flags;
    public String iOSPkg;
    public byte[] iOSUid;
    public boolean isIOS;
    public boolean isLocal;
    public NotificationKeyData key;
    public Bitmap largeIcon;
    private boolean mCanRecycleBitmap;
    public Notification originalNotification;
    public int priority;
    public Bitmap smallIcon;
    public StyleData style;
    public String text;
    public String title;
    public WearableExtrasData wearableExtras;
    public long when;

    static class C09211 implements Creator<NotificationData> {
        C09211() {
        }

        public NotificationData createFromParcel(Parcel parcel) {
            return new NotificationData(parcel);
        }

        public NotificationData[] newArray(int size) {
            return new NotificationData[size];
        }
    }

    public static class ActionData implements Parcelable {
        public static final Creator<ActionData> CREATOR = new C09221();
        public Bitmap icon;
        int index;
        boolean isWearable;
        private boolean mCanRecycleBitmap;
        NotificationData noti;
        public RemoteInputData[] remoteInputs;
        public String title;

        static class C09221 implements Creator<ActionData> {
            C09221() {
            }

            public ActionData createFromParcel(Parcel parcel) {
                return new ActionData(parcel);
            }

            public ActionData[] newArray(int size) {
                return new ActionData[size];
            }
        }

        private ActionData() {
            this.mCanRecycleBitmap = true;
        }

        public ActionData(Parcel parcel) {
            this.mCanRecycleBitmap = true;
            this.mCanRecycleBitmap = true;
            this.icon = Utils.readBmp(parcel);
            this.title = parcel.readString();
            this.remoteInputs = (RemoteInputData[]) Utils.readParcelableArray(parcel, RemoteInputData[].class);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int flags) {
            Utils.writeBmp(parcel, this.icon);
            parcel.writeString(this.title);
            parcel.writeParcelableArray(this.remoteInputs, 0);
        }
    }

    public static class RemoteInputKitkat {
    }

    public static class StyleData implements Parcelable {
        public static final Creator<StyleData> CREATOR = new C09231();
        public String bigContentTitle;
        public Bitmap bigPicture;
        public String bigText;
        public int style;
        public String styleName;
        public String summaryText;
        public String[] textLines;

        static class C09231 implements Creator<StyleData> {
            C09231() {
            }

            public StyleData createFromParcel(Parcel parcel) {
                return new StyleData(parcel);
            }

            public StyleData[] newArray(int size) {
                return new StyleData[size];
            }
        }

        private StyleData() {
        }

        private StyleData(Parcel parcel) {
            this.style = parcel.readInt();
            this.styleName = parcel.readString();
            this.textLines = parcel.createStringArray();
            this.bigContentTitle = parcel.readString();
            this.bigText = parcel.readString();
            this.summaryText = parcel.readString();
            this.bigPicture = Utils.readBmp(parcel);
            if (NotificationData.DEBUG) {
                Log.m26d("Noti-Data", "Read Style : " + this.style, new Object[0]);
                Log.m26d("Noti-Data", "Read StyleName : " + this.styleName, new Object[0]);
            }
        }

        public void writeToParcel(Parcel parcel, int flags) {
            parcel.writeInt(this.style);
            parcel.writeString(this.styleName);
            parcel.writeStringArray(this.textLines);
            parcel.writeString(this.bigContentTitle);
            parcel.writeString(this.bigText);
            parcel.writeString(this.summaryText);
            Utils.writeBmp(parcel, this.bigPicture);
        }

        public int describeContents() {
            return 0;
        }
    }

    public static class WearableExtrasData implements Parcelable {
        public static final Creator<WearableExtrasData> CREATOR = new C09241();
        public ActionData[] actions;
        public Bitmap background;
        public NotificationData[] pages;

        static class C09241 implements Creator<WearableExtrasData> {
            C09241() {
            }

            public WearableExtrasData createFromParcel(Parcel parcel) {
                return new WearableExtrasData(parcel);
            }

            public WearableExtrasData[] newArray(int size) {
                return new WearableExtrasData[size];
            }
        }

        private WearableExtrasData() {
        }

        public WearableExtrasData(Parcel parcel) {
            this.background = Utils.readBmp(parcel);
            this.pages = (NotificationData[]) Utils.readParcelableArray(parcel, NotificationData[].class);
            this.actions = (ActionData[]) Utils.readParcelableArray(parcel, ActionData[].class);
            if (NotificationData.DEBUG) {
                if (this.background != null) {
                    Log.m26d("Noti-Data.WExtras", "Read Background : " + this.background, new Object[0]);
                }
                if (this.pages != null && this.pages.length > 0) {
                    Log.m26d("Noti-Data.WExtras", "Read Pages : " + Arrays.toString(this.pages), new Object[0]);
                }
                if (this.actions != null && this.actions.length > 0) {
                    Log.m26d("Noti-Data.WExtras", "Read Action : " + Arrays.toString(this.actions), new Object[0]);
                }
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int flags) {
            Utils.writeBmp(parcel, this.background);
            parcel.writeParcelableArray(this.pages, 0);
            parcel.writeParcelableArray(this.actions, 0);
        }
    }

    private NotificationData() {
        this.mCanRecycleBitmap = true;
    }

    public NotificationData(Parcel parcel) {
        boolean z;
        this.mCanRecycleBitmap = true;
        this.mCanRecycleBitmap = true;
        this.smallIcon = Utils.readBmp(parcel);
        this.largeIcon = Utils.readBmp(parcel);
        this.title = parcel.readString();
        this.text = parcel.readString();
        this.contentInfo = parcel.readString();
        if (DEBUG) {
            this.style = (StyleData) parcel.readParcelable(StyleData.class.getClassLoader());
            this.actions = (ActionData[]) Utils.readParcelableArray(parcel, ActionData[].class);
            prepareActions(this.actions, this, false);
            this.wearableExtras = (WearableExtrasData) parcel.readParcelable(WearableExtrasData.class.getClassLoader());
        } else {
            this.style = (StyleData) parcel.readParcelable(StyleData.class.getClassLoader());
            this.actions = (ActionData[]) Utils.readParcelableArray(parcel, ActionData[].class);
            prepareActions(this.actions, this, false);
            this.wearableExtras = (WearableExtrasData) parcel.readParcelable(WearableExtrasData.class.getClassLoader());
        }
        if (this.wearableExtras != null) {
            prepareActions(this.wearableExtras.actions, this, true);
        }
        this.priority = parcel.readInt();
        this.flags = parcel.readInt();
        this.when = parcel.readLong();
        if (DEBUG) {
            Log.m26d("Noti-Data", "Read When : " + this.when, new Object[0]);
        }
        this.originalNotification = (Notification) parcel.readParcelable(Notification.class.getClassLoader());
        this.iOSPkg = parcel.readString();
        this.iOSUid = parcel.createByteArray();
        if (parcel.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.isIOS = z;
    }

    private static void prepareActions(ActionData[] actions, NotificationData noti, boolean isWearable) {
        if (actions != null && actions.length > 0) {
            int index = 0;
            for (ActionData action : actions) {
                action.noti = noti;
                action.index = index;
                action.isWearable = isWearable;
                if (action.remoteInputs != null && action.remoteInputs.length > 0) {
                    for (RemoteInputData remoteInput : action.remoteInputs) {
                        remoteInput.action = action;
                    }
                }
                index++;
            }
        }
    }

    public static NotificationData empty() {
        return new NotificationData();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        int i;
        Utils.writeBmp(parcel, this.smallIcon);
        Utils.writeBmp(parcel, this.largeIcon);
        parcel.writeString(this.title);
        parcel.writeString(this.text);
        parcel.writeString(this.contentInfo);
        parcel.writeParcelable(this.style, 0);
        parcel.writeParcelableArray(this.actions, 0);
        parcel.writeParcelable(this.wearableExtras, 0);
        parcel.writeInt(this.priority);
        parcel.writeInt(this.flags);
        parcel.writeLong(this.when);
        parcel.writeParcelable(this.isLocal ? this.originalNotification : null, 0);
        parcel.writeString(this.iOSPkg);
        parcel.writeByteArray(this.iOSUid);
        if (this.isIOS) {
            i = 1;
        } else {
            i = 0;
        }
        parcel.writeInt(i);
    }
}
