package com.huami.watch.notification.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.notification.data.NotificationData.ActionData;
import com.huami.watch.util.Config;

public class RemoteInputData implements Parcelable {
    public static final Creator<RemoteInputData> CREATOR = new C09261();
    public static final boolean DEBUG = Config.isDebug();
    ActionData action;
    public String[] choices;
    public String label;
    public String resultKey;

    static class C09261 implements Creator<RemoteInputData> {
        C09261() {
        }

        public RemoteInputData createFromParcel(Parcel parcel) {
            return new RemoteInputData(parcel);
        }

        public RemoteInputData[] newArray(int size) {
            return new RemoteInputData[size];
        }
    }

    private RemoteInputData() {
    }

    public RemoteInputData(Parcel parcel) {
        this.resultKey = parcel.readString();
        this.label = parcel.readString();
        this.choices = parcel.createStringArray();
        if (!DEBUG) {
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(this.resultKey);
        parcel.writeString(this.label);
        parcel.writeStringArray(this.choices);
    }
}
