package com.huami.watch.notification.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StatusBarNotificationData implements Parcelable {
    public static final Creator<StatusBarNotificationData> CREATOR = new C09271();
    public String groupKey;
    public String iOSPkg;
    public byte[] iOSUid;
    public int id;
    public boolean isIOS;
    public String key;
    public NotificationData notification;
    public String pkg;
    public String tag;
    public String targetPkg;

    static class C09271 implements Creator<StatusBarNotificationData> {
        C09271() {
        }

        public StatusBarNotificationData createFromParcel(Parcel parcel) {
            return new StatusBarNotificationData(parcel);
        }

        public StatusBarNotificationData[] newArray(int size) {
            return new StatusBarNotificationData[size];
        }
    }

    private StatusBarNotificationData() {
    }

    private StatusBarNotificationData(Parcel parcel) {
        this.pkg = parcel.readString();
        this.id = parcel.readInt();
        this.tag = parcel.readString();
        this.key = parcel.readString();
        this.groupKey = parcel.readString();
        this.notification = (NotificationData) parcel.readParcelable(NotificationData.class.getClassLoader());
        this.iOSPkg = parcel.readString();
        this.iOSUid = parcel.createByteArray();
        this.isIOS = parcel.readInt() != 0;
        if (this.notification != null) {
            this.notification.isIOS = this.isIOS;
        }
        this.targetPkg = parcel.readString();
        if (this.notification != null) {
            this.notification.key = NotificationKeyData.from(this.pkg, this.id, this.tag, this.key, this.targetPkg);
            this.notification.isLocal = false;
        }
    }

    public static StatusBarNotificationData empty() {
        return new StatusBarNotificationData();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        int i = 0;
        parcel.writeString(this.pkg);
        parcel.writeInt(this.id);
        parcel.writeString(this.tag);
        parcel.writeString(this.key);
        parcel.writeString(this.groupKey);
        parcel.writeParcelable(this.notification, 0);
        parcel.writeString(this.iOSPkg);
        parcel.writeByteArray(this.iOSUid);
        if (this.isIOS) {
            i = 1;
        }
        parcel.writeInt(i);
        parcel.writeString(this.targetPkg);
    }

    public String toString() {
        return new StringBuilder(super.toString()).append("<").append("Pkg : ").append(this.pkg).append(", ").append("Id : ").append(this.id).append(", ").append("Tag : ").append(this.tag).append(", ").append("Key : ").append(this.key).append(", ").append("GroupKey : ").append(this.groupKey).append(", ").append("TargetPkg : ").append(this.targetPkg).append(">").toString();
    }
}
