package com.huami.watch.notification.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class NotificationKeyData implements Parcelable {
    public static final Creator<NotificationKeyData> CREATOR = new C09251();
    public int id;
    public String key;
    public String pkg;
    public String tag;
    public String targetPkg;

    static class C09251 implements Creator<NotificationKeyData> {
        C09251() {
        }

        public NotificationKeyData createFromParcel(Parcel parcel) {
            return new NotificationKeyData(parcel);
        }

        public NotificationKeyData[] newArray(int size) {
            return new NotificationKeyData[size];
        }
    }

    private NotificationKeyData() {
    }

    private NotificationKeyData(Parcel parcel) {
        this.pkg = parcel.readString();
        this.id = parcel.readInt();
        this.tag = parcel.readString();
        this.key = parcel.readString();
        this.targetPkg = parcel.readString();
    }

    public static NotificationKeyData from(String pkg, int id, String tag, String key, String targetPkg) {
        NotificationKeyData data = new NotificationKeyData();
        data.pkg = pkg;
        data.id = id;
        data.tag = tag;
        if (!TextUtils.isEmpty(key)) {
            data.key = key;
        }
        data.targetPkg = targetPkg;
        return data;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(this.pkg);
        parcel.writeInt(this.id);
        parcel.writeString(this.tag);
        parcel.writeString(this.key);
        parcel.writeString(this.targetPkg);
    }

    public String toString() {
        return new StringBuilder(super.toString()).append("<").append("Pkg : ").append(this.pkg).append(", ").append("Id : ").append(this.id).append(", ").append("Tag : ").append(this.tag).append(", ").append("Key : ").append(this.key).append(", ").append("TargetPkg : ").append(this.targetPkg).append(">").toString();
    }
}
