package com.huami.watch.notification;

import android.content.Context;
import android.provider.Settings.System;
import com.huami.watch.notification.data.NotificationKeyData;
import com.huami.watch.transport.DataTransportResult;
import com.huami.watch.transport.Transporter.DataSendResultCallback;
import com.huami.watch.util.Config;
import com.huami.watch.util.Log;

public final class NotificationHandler {
    public static final boolean DEBUG = Config.isDebug();

    public interface Handler {
    }

    private static class DefaultHandler implements Handler {
        private DefaultHandler() {
        }
    }

    private static class DefaultActionHandler extends DefaultHandler {

        class C09151 implements DataSendResultCallback {
            public void onResultBack(DataTransportResult result) {
                if (!NotificationHandler.DEBUG) {
                }
            }
        }

        class C09162 implements DataSendResultCallback {
            public void onResultBack(DataTransportResult result) {
                if (!NotificationHandler.DEBUG) {
                }
            }
        }
    }

    private static class DefaultIOSNotificationHandler extends DefaultHandler {
    }

    private static class DefaultLocalActionHandler extends DefaultHandler {
    }

    private static class DefaultLocalNotificationHandler extends DefaultHandler {
    }

    private static class DefaultNotificationHandler extends DefaultHandler {
        NotificationKeyData key;

        class C09171 implements DataSendResultCallback {
            final /* synthetic */ DefaultNotificationHandler this$0;
            final /* synthetic */ String val$finalAction;
            final /* synthetic */ Context val$finalContext;

            public void onResultBack(DataTransportResult result) {
                if (result.getResultCode() != 0 && "block".equals(this.val$finalAction)) {
                    NotificationHandler.addToPendingBlockList(this.val$finalContext, this.this$0.key.pkg);
                }
            }
        }
    }

    public static final class Phone {
    }

    private NotificationHandler() {
    }

    private static void addToPendingBlockList(Context context, String pkgName) {
        String key = "com.huami.watch.notification.PendingBlockList";
        String exist = System.getString(context.getContentResolver(), key);
        if (exist == null) {
            exist = "";
        }
        if (!exist.contains(pkgName)) {
            exist = exist + pkgName + ",";
            if (DEBUG) {
                Log.m26d("Noti-Handler", "Save PendingBlockList : " + pkgName, new Object[0]);
            }
            System.putString(context.getContentResolver(), key, exist);
        } else if (DEBUG) {
            Log.m32w("Noti-Handler", "PendingBlockList Already Contains : " + pkgName, new Object[0]);
        }
    }
}
