package com.huami.watch.notification.update;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;
import com.huami.watch.notification.stream.ConnectionResult;
import com.huami.watch.notification.stream.INotificationUpdater;
import com.huami.watch.notification.stream.INotificationUpdaterCallbacks.Stub;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class NotificationUpdater {
    private boolean mConnecting;
    private Callback mConnectionCallbacks;
    private Context mContext;
    private boolean mDisConnectFailed;
    private SparseArray<UpdateNotificationFaildCallback> mFailedCallback;
    private int mFailedNotificationId;
    private String mFailedNotificationPackageName;
    private boolean mIsConnected;
    private OnContentUpdatedListener mListener;
    private HashMap<Integer, Bundle> mMayMissNotificationsContent;
    private NotificationUpdaterManager mUpdateContentManager;

    class C09581 extends OnContentUpdatedListener {
        final /* synthetic */ NotificationUpdater this$0;

        public void onStreamItemContentUpdateFailed(String packageName, int notificationId) {
            Log.w("NotificationUpdater", "notification update failed of id:" + notificationId + " packageName:" + packageName);
            this.this$0.mFailedNotificationId = notificationId;
            this.this$0.mFailedNotificationPackageName = packageName;
            if (this.this$0.mFailedCallback != null) {
                UpdateNotificationFaildCallback callback = (UpdateNotificationFaildCallback) this.this$0.mFailedCallback.get(notificationId);
                if (callback != null) {
                    callback.onUpdateNotificationFaild(this.this$0.mFailedNotificationId);
                }
            }
        }
    }

    private class Callback extends ConnectionCallbacks {
        final /* synthetic */ NotificationUpdater this$0;

        public void onConnected(Bundle connectionHints) {
            super.onConnected(connectionHints);
            if (this.this$0.mUpdateContentManager.isConnected()) {
                try {
                    this.this$0.mUpdateContentManager.addUpdatedListener(this.this$0.mListener, null);
                    Log.d("NotificationUpdater", "addOnStreamItemUpdatedListener :" + this.this$0.mListener);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                Log.d("NotificationUpdater", "onConnected and addOnStreamItemUpdated listener");
                this.this$0.mConnecting = false;
                this.this$0.mIsConnected = true;
                this.this$0.handleMayMissNotificationUpdate();
                if (this.this$0.mDisConnectFailed) {
                    this.this$0.mDisConnectFailed = false;
                    Log.w("NotificationUpdater", "disconnect from onConnected due to mDisConnectFailed");
                    this.this$0.mUpdateContentManager.disconnect();
                }
            }
        }

        public void onConnectionSuspended(int cause) {
            super.onConnectionSuspended(cause);
            Log.w("NotificationUpdater", "onConnectionSuspended");
            this.this$0.mIsConnected = false;
            this.this$0.mConnecting = false;
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            super.onConnectionFailed(connectionResult);
            Log.w("NotificationUpdater", "onConnectionFailed");
            this.this$0.mIsConnected = false;
            this.this$0.mConnecting = false;
            for (Integer notificationId : this.this$0.mMayMissNotificationsContent.keySet()) {
                ((UpdateNotificationFaildCallback) this.this$0.mFailedCallback.get(notificationId.intValue())).onUpdateNotificationFaild(notificationId.intValue());
            }
        }
    }

    private static class NotificationUpdaterManager {
        private ConnectionCallbacks mConnectionCallbacks;
        private int mConnectionState;
        private Context mContext;
        private Handler mHandler;
        private final List<OnContentUpdatedListener> mListeners;
        private INotificationUpdater mServiceBinder;
        private UpdateServiceCallback mServiceCallbacks;
        private UpdaterServiceConnection mServiceConnection;
        private boolean mSubscribedToStreamItemUpdate;

        private static abstract class OnContentUpdatedListener {
            public abstract void onStreamItemContentUpdateFailed(String str, int i);

            private OnContentUpdatedListener() {
            }
        }

        public static class ConnectionCallbacks {
            public void onConnected(Bundle connectionHints) {
            }

            public void onConnectionSuspended(int cause) {
            }

            public void onConnectionFailed(ConnectionResult connectionResult) {
            }
        }

        private class BinderDeathRecipient implements DeathRecipient {
            private BinderDeathRecipient() {
            }

            public void binderDied() {
                Log.d("NotificationUpdater", "binderDied");
                NotificationUpdaterManager.this.mServiceBinder = null;
                NotificationUpdaterManager.this.mServiceCallbacks = null;
                NotificationUpdaterManager.this.mConnectionState = 3;
                NotificationUpdaterManager.this.mConnectionCallbacks.onConnectionSuspended(2);
            }
        }

        private class UpdateServiceCallback extends Stub {
            private WeakReference<NotificationUpdaterManager> mUpdaterManager;

            public UpdateServiceCallback(NotificationUpdaterManager updaterManager) {
                this.mUpdaterManager = new WeakReference(updaterManager);
            }

            public void onConnected(Bundle connectionHints) throws RemoteException {
                NotificationUpdaterManager updaterManager = (NotificationUpdaterManager) this.mUpdaterManager.get();
                if (updaterManager != null) {
                    updaterManager.onConnected(this, connectionHints);
                }
            }

            public void onConnectionFailed(ConnectionResult result) throws RemoteException {
                NotificationUpdaterManager updaterManager = (NotificationUpdaterManager) this.mUpdaterManager.get();
                if (updaterManager != null) {
                    updaterManager.onConnectionFailed(this, result);
                }
            }

            public void onStreamItemContentUpdateFailed(String packageName, int notificationId) throws RemoteException {
                NotificationUpdaterManager updaterManager = (NotificationUpdaterManager) this.mUpdaterManager.get();
                if (updaterManager != null) {
                    updaterManager.onStreamItemContentUpdateFailed(packageName, notificationId);
                }
            }
        }

        private class UpdaterServiceConnection implements ServiceConnection {
            private UpdaterServiceConnection() {
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d("NotificationUpdater", "StreamItemManagerServiceConnection.onServiceConnected(name=" + name + " binder=" + service);
                if (isCurrent()) {
                    NotificationUpdaterManager.this.mServiceBinder = INotificationUpdater.Stub.asInterface(service);
                    try {
                        service.linkToDeath(new BinderDeathRecipient(), 0);
                        NotificationUpdaterManager.this.mServiceCallbacks = new UpdateServiceCallback(NotificationUpdaterManager.this);
                        NotificationUpdaterManager.this.mConnectionState = 1;
                        try {
                            Log.d("NotificationUpdater", "binding to service...");
                            NotificationUpdaterManager.this.mServiceBinder.connect(NotificationUpdaterManager.this.mContext.getPackageName(), NotificationUpdaterManager.this.mServiceCallbacks);
                            return;
                        } catch (RemoteException e) {
                            Log.w("NotificationUpdater", "RemoteException when connecting to stream manager service.");
                            return;
                        }
                    } catch (RemoteException e2) {
                        NotificationUpdaterManager.this.mServiceBinder = null;
                        NotificationUpdaterManager.this.mServiceCallbacks = null;
                        NotificationUpdaterManager.this.mConnectionState = 0;
                        NotificationUpdaterManager.this.mConnectionCallbacks.onConnectionSuspended(1);
                        Log.d("NotificationUpdater", "binding service has died");
                        return;
                    }
                }
                Log.w("NotificationUpdater", "Not current service connection in onServiceConnected(). ignore it.");
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("NotificationUpdater", "onServiceDisconnected(componentName=" + name + " )");
                if (isCurrent()) {
                    NotificationUpdaterManager.this.mServiceBinder = null;
                    NotificationUpdaterManager.this.mServiceCallbacks = null;
                    NotificationUpdaterManager.this.mConnectionState = 3;
                    NotificationUpdaterManager.this.mConnectionCallbacks.onConnectionSuspended(1);
                    return;
                }
                Log.w("NotificationUpdater", "Not current service connection in onServiceDisconnected. ignore it.");
            }

            private boolean isCurrent() {
                return NotificationUpdaterManager.this.mServiceConnection == this;
            }
        }

        private NotificationUpdaterManager(Context context, ConnectionCallbacks connectionCallbacks) {
            this.mConnectionState = 0;
            this.mHandler = new Handler();
            this.mSubscribedToStreamItemUpdate = false;
            this.mListeners = new ArrayList();
            this.mContext = context;
            this.mConnectionCallbacks = connectionCallbacks;
        }

        public void connect() {
            if (this.mConnectionState != 0) {
                throw new IllegalStateException("calling connect() while not disconnected. mConnectionState=" + getStateString(this.mConnectionState));
            } else if (this.mConnectionState != 1 && this.mConnectionState != 2) {
                if (this.mServiceConnection != null) {
                    throw new RuntimeException("mServiceConnection should be null. Instead it is " + this.mServiceConnection);
                } else if (this.mServiceBinder != null) {
                    throw new RuntimeException("mServiceBinder should be null. Instead it is " + this.mServiceBinder);
                } else if (this.mServiceCallbacks != null) {
                    throw new RuntimeException("mServiceCallbacks should be null. Instead it is " + this.mServiceCallbacks);
                } else {
                    this.mConnectionState = 1;
                    Intent intent = createIntentFromAction(this.mContext, "com.huami.watch.notification.stream.UpdateService");
                    final ServiceConnection thisConnection = new UpdaterServiceConnection();
                    this.mServiceConnection = thisConnection;
                    boolean bound = false;
                    try {
                        bound = this.mContext.bindService(intent, this.mServiceConnection, 1);
                    } catch (Exception ex) {
                        Log.e("NotificationUpdater", "Failed binding to StreamItemManagerService.", ex);
                    }
                    if (!bound) {
                        this.mHandler.post(new Runnable() {
                            public void run() {
                                if (thisConnection == NotificationUpdaterManager.this.mServiceConnection) {
                                    NotificationUpdaterManager.this.forceCloseConnection();
                                    NotificationUpdaterManager.this.mConnectionCallbacks.onConnectionFailed(new ConnectionResult(1, null));
                                }
                            }
                        });
                    }
                    Log.d("NotificationUpdater", "connect to StreamItemManagerService.");
                }
            }
        }

        public void disconnect() {
            if (this.mServiceCallbacks != null) {
                try {
                    this.mServiceBinder.disconnect(this.mServiceCallbacks);
                } catch (RemoteException e) {
                    Log.w("NotificationUpdater", "RemoteException during connect.");
                }
            }
            forceCloseConnection();
            Log.d("NotificationUpdater", "disconnect...");
        }

        public boolean isConnected() {
            return this.mConnectionState == 2;
        }

        private void forceCloseConnection() {
            if (this.mServiceConnection != null) {
                this.mContext.unbindService(this.mServiceConnection);
            }
            this.mConnectionState = 0;
            this.mServiceConnection = null;
            this.mServiceBinder = null;
            this.mServiceCallbacks = null;
        }

        public void updateNotification(int id, Bundle bundle) {
            updateNotificationWithTag(null, id, bundle);
        }

        public void updateNotificationWithTag(String tag, int id, Bundle notificationContent) {
            try {
                this.mServiceBinder.updateNotification(tag, id, notificationContent, this.mContext.getPackageName());
                Log.d("NotificationUpdater", "the StreamItemManager:" + this);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void addUpdatedListener(@NonNull OnContentUpdatedListener listener, @Nullable Handler handler) {
            if (listener == null) {
                throw new IllegalArgumentException("listener can not be null.");
            } else if (!isConnected()) {
                throw new IllegalStateException("addOnStreamItemUpdatedListener() is called when not connected to the service (connectionState=" + getStateString(this.mConnectionState) + ")");
            } else if (this.mListeners.contains(listener)) {
                throw new IllegalArgumentException("listener already registered.");
            } else {
                if (handler == null) {
                    handler = new Handler();
                }
                this.mListeners.add(listener);
                if (!this.mSubscribedToStreamItemUpdate) {
                    subcribeToItemUpdates();
                }
            }
        }

        private void subcribeToItemUpdates() {
            try {
                this.mServiceBinder.subscribe(this.mServiceCallbacks);
                this.mSubscribedToStreamItemUpdate = true;
            } catch (RemoteException e) {
                Log.e("NotificationUpdater", "Can't subscribe to stream item changes.");
            }
        }

        public void onConnected(final UpdateServiceCallback serviceCallbacks, final Bundle connectionHints) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (!NotificationUpdaterManager.this.isCurrent(serviceCallbacks)) {
                        Log.w("NotificationUpdater", "Not the same callback onServiceConnected(). Ignore it.");
                    } else if (NotificationUpdaterManager.this.mConnectionState != 1) {
                        Log.w("NotificationUpdater", "onConnect from service while mState=" + NotificationUpdaterManager.getStateString(NotificationUpdaterManager.this.mConnectionState) + "... ignoring");
                    } else {
                        NotificationUpdaterManager.this.mConnectionState = 2;
                        Log.d("NotificationUpdater", "ServiceCallbacks.onServiceConnected()");
                        NotificationUpdaterManager.this.mConnectionCallbacks.onConnected(connectionHints);
                    }
                }
            });
        }

        public void onConnectionFailed(final UpdateServiceCallback serviceCallbacks, final ConnectionResult result) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    Log.e("NotificationUpdater", "Failed to connect to stream manager service.");
                    if (!NotificationUpdaterManager.this.isCurrent(serviceCallbacks)) {
                        Log.w("NotificationUpdater", "Not the same callback onConnectionFailed(). Ignore it.");
                    } else if (NotificationUpdaterManager.this.mConnectionState != 1) {
                        Log.w("NotificationUpdater", "onConnect from service while mState=" + NotificationUpdaterManager.getStateString(NotificationUpdaterManager.this.mConnectionState) + "... ignoring");
                    } else {
                        NotificationUpdaterManager.this.forceCloseConnection();
                        NotificationUpdaterManager.this.mConnectionCallbacks.onConnectionFailed(result);
                    }
                }
            });
        }

        public void onStreamItemContentUpdateFailed(final String packageName, final int notificationId) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    Log.d("NotificationUpdater", "onStreamItemContentUpdateFailed. mListeners size = " + NotificationUpdaterManager.this.mListeners.size());
                    Log.d("NotificationUpdater", "packageName:" + packageName + " id:" + notificationId);
                    for (OnContentUpdatedListener listener : NotificationUpdaterManager.this.mListeners) {
                        listener.onStreamItemContentUpdateFailed(packageName, notificationId);
                    }
                }
            });
        }

        private boolean isCurrent(UpdateServiceCallback callbacks) {
            if (this.mServiceCallbacks == callbacks) {
                return true;
            }
            if (this.mConnectionState != 0) {
            }
            return false;
        }

        private Intent createIntentFromAction(Context context, String action) {
            Intent implicitIntent = new Intent(action);
            List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentServices(implicitIntent, 0);
            String className = null;
            String packageName = null;
            if (resolveInfoList == null || resolveInfoList.size() == 0) {
                return null;
            }
            Iterator i$ = resolveInfoList.iterator();
            if (i$.hasNext()) {
                ResolveInfo resolveInfo = (ResolveInfo) i$.next();
                packageName = resolveInfo.serviceInfo.packageName;
                className = resolveInfo.serviceInfo.name;
            }
            if (packageName == null || className == null) {
                return null;
            }
            ComponentName component = new ComponentName(packageName, className);
            Intent explicitIntent = new Intent(implicitIntent);
            explicitIntent.setComponent(component);
            return explicitIntent;
        }

        private static String getStateString(int connectionState) {
            switch (connectionState) {
                case 0:
                    return "CONNECT_STATE_DISCONNECTED";
                case 1:
                    return "CONNECT_STATE_CONNECTING";
                case 2:
                    return "CONNECT_STATE_CONNECTED";
                case 3:
                    return "CONNECT_STATE_SUSPENDED";
                default:
                    return "UNKNOWN State:" + connectionState;
            }
        }
    }

    public interface UpdateNotificationFaildCallback {
        void onUpdateNotificationFaild(int i);
    }

    private void initConnection() {
        if (this.mContext == null) {
            throw new IllegalArgumentException("context can't be null.");
        }
        this.mUpdateContentManager = new NotificationUpdaterManager(this.mContext, this.mConnectionCallbacks);
        this.mUpdateContentManager.connect();
        Log.d("NotificationUpdater", "mStreamItemManager.connect() ");
        this.mConnecting = true;
    }

    public void updateNotificationContent(int notificationId, Bundle content) {
        Log.d("NotificationUpdater", "updateNotificationContent Notification id:" + notificationId + " content:" + content + " mIsConnected:" + this.mIsConnected + " mConnecting:" + this.mConnecting);
        if (this.mIsConnected) {
            this.mUpdateContentManager.updateNotification(notificationId, content);
            synchronized (this.mMayMissNotificationsContent) {
                this.mMayMissNotificationsContent.remove(Integer.valueOf(notificationId));
            }
        } else {
            synchronized (this.mMayMissNotificationsContent) {
                this.mMayMissNotificationsContent.put(Integer.valueOf(notificationId), content);
            }
            if (!this.mConnecting) {
                initConnection();
            }
        }
        this.mFailedNotificationId = -1;
        this.mFailedNotificationPackageName = null;
    }

    private void handleMayMissNotificationUpdate() {
        Log.d("NotificationUpdater", "handleMayMissNotificationUpdate");
        synchronized (this.mMayMissNotificationsContent) {
            for (Integer notificationId : this.mMayMissNotificationsContent.keySet()) {
                updateNotificationContent(notificationId.intValue(), (Bundle) this.mMayMissNotificationsContent.get(notificationId));
            }
            this.mMayMissNotificationsContent.clear();
        }
    }
}
