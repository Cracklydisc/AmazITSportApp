package com.huami.watch.utils;

import android.view.animation.Interpolator;

public class BackInterpolator implements Interpolator {
    byte _mode;
    protected float param_s;

    public float getInterpolation(float input) {
        float s;
        switch (this._mode) {
            case (byte) 0:
                s = this.param_s;
                return (input * input) * (((s + 1.0f) * input) - s);
            case (byte) 1:
                s = this.param_s;
                input -= 1.0f;
                return ((input * input) * (((s + 1.0f) * input) + s)) + 1.0f;
            case (byte) 2:
                s = this.param_s;
                input *= 2.0f;
                if (input < 1.0f) {
                    s *= 1.525f;
                    return ((input * input) * (((s + 1.0f) * input) - s)) * 0.5f;
                }
                input -= 2.0f;
                s *= 1.525f;
                return (((input * input) * (((s + 1.0f) * input) + s)) + 2.0f) * 0.5f;
            default:
                return input;
        }
    }
}
