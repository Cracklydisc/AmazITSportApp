package com.huami.watch.utils;

import android.view.animation.Interpolator;

public class MoveBackInterpolator implements Interpolator {
    boolean custom;
    Interpolator in;
    float max;
    Interpolator out;

    public float getInterpolation(float input) {
        if (!this.custom) {
            return (float) Math.cos((((double) input) * 3.141592653589793d) + 4.71238898038469d);
        }
        if (input < this.max) {
            if (this.in != null) {
                return this.in.getInterpolation(input / this.max);
            }
            return input / this.max;
        } else if (this.out != null) {
            return 1.0f - this.out.getInterpolation((input - this.max) / (1.0f - this.max));
        } else {
            return (1.0f - input) / (1.0f - this.max);
        }
    }
}
