package com.huami.watch.utils;

import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import java.util.ArrayList;

public class Utils {
    public static float getDescendantRectRelativeToParent(View descendant, View root, Rect r) {
        int[] mTmpXY = new int[]{0, 0};
        float scale = getDescendantCoordRelativeToParent(descendant, root, mTmpXY);
        r.set(mTmpXY[0], mTmpXY[1], (int) (((float) mTmpXY[0]) + (((float) descendant.getMeasuredWidth()) * scale)), (int) (((float) mTmpXY[1]) + (((float) descendant.getMeasuredHeight()) * scale)));
        return scale;
    }

    private static float getDescendantCoordRelativeToParent(View descendant, View parentView, int[] coord) {
        return getDescendantCoordRelativeToParent(descendant, parentView, coord, false);
    }

    public static float getDescendantCoordRelativeToParent(View descendant, View parentView, int[] coord, boolean includeRootScroll) {
        ArrayList<View> ancestorChain = new ArrayList();
        float[] pt = new float[]{(float) coord[0], (float) coord[1]};
        View v = descendant;
        while (v != parentView && v != null) {
            ancestorChain.add(v);
            v = (View) v.getParent();
        }
        float scale = 1.0f;
        int count = ancestorChain.size();
        for (int i = 0; i < count; i++) {
            View v0 = (View) ancestorChain.get(i);
            if (v0 != descendant || includeRootScroll) {
                pt[0] = pt[0] - ((float) v0.getScrollX());
                pt[1] = pt[1] - ((float) v0.getScrollY());
            }
            v0.getMatrix().mapPoints(pt);
            pt[0] = pt[0] + ((float) v0.getLeft());
            pt[1] = pt[1] + ((float) v0.getTop());
            scale *= v0.getScaleX();
        }
        coord[0] = Math.round(pt[0]);
        coord[1] = Math.round(pt[1]);
        return scale;
    }

    public static int getKeyScrollDistance(DisplayMetrics displayMetrics) {
        int i = 0;
        try {
            i = Math.round(TypedValue.applyDimension(0, 200.0f, displayMetrics));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }
}
