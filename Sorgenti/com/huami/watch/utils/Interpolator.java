package com.huami.watch.utils;

import com.huami.watch.math3.analysis.interpolation.SplineInterpolator;
import com.huami.watch.math3.analysis.polynomials.PolynomialSplineFunction;

public class Interpolator {
    private PolynomialSplineFunction mFunction = null;
    private SplineInterpolator mInterpolator = null;
    private double mLastX = 0.0d;
    private double mLastY = 0.0d;
}
