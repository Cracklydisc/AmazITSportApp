package com.huami.watch.utils;

import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;

public class ClickScaleAnimation {
    private static final QuadInterpolator QUAD_IN = new QuadInterpolator((byte) 0);
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    WeakReference<View> mAnimationView;
    private AnimatorSet mAnims;
    AnimatorListener mListener;

    public ClickScaleAnimation(View view, AnimatorListener listener) {
        this.mAnimationView = new WeakReference(view);
        this.mListener = listener;
    }

    public void startClickAnimation() {
        final View view = (View) this.mAnimationView.get();
        if (view == null) {
            Log.e("ClickAnimation", "can't get view ref...");
            return;
        }
        if (this.mAnims != null && this.mAnims.isStarted()) {
            this.mAnims.cancel();
        }
        this.mAnims = new AnimatorSet();
        ValueAnimator mScaleIn = ValueAnimator.ofFloat(new float[]{1.0f, 0.9f});
        mScaleIn.setDuration(208);
        mScaleIn.setInterpolator(QUAD_OUT);
        mScaleIn.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float input = ((Float) animation.getAnimatedValue()).floatValue();
                view.setScaleX(input);
                view.setScaleY(input);
            }
        });
        ValueAnimator mScaleOut = ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        mScaleOut.setDuration(208);
        mScaleOut.setInterpolator(QUAD_IN);
        mScaleOut.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float scale = 1.0f - (0.1f * ((Float) animation.getAnimatedValue()).floatValue());
                view.setScaleX(scale);
                view.setScaleY(scale);
            }
        });
        mScaleOut.addListener(this.mListener);
        this.mAnims.play(mScaleIn);
        this.mAnims.play(mScaleOut).after(mScaleIn);
        this.mAnims.start();
    }
}
