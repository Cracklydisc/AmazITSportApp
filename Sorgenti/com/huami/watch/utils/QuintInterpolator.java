package com.huami.watch.utils;

import android.view.animation.Interpolator;

public class QuintInterpolator implements Interpolator {
    byte _mode = (byte) 0;

    public QuintInterpolator(byte mode) {
        if (mode <= (byte) -1 || mode >= (byte) 3) {
            throw new IllegalArgumentException("The mode must be 0, 1 or 2. See the doc");
        }
        this._mode = mode;
    }

    public float getInterpolation(float input) {
        switch (this._mode) {
            case (byte) 0:
                return (((input * input) * input) * input) * input;
            case (byte) 1:
                input -= 1.0f;
                return ((((input * input) * input) * input) * input) + 1.0f;
            case (byte) 2:
                input *= 2.0f;
                if (input < 1.0f) {
                    return ((((0.5f * input) * input) * input) * input) * input;
                }
                input -= 2.0f;
                return (((((input * input) * input) * input) * input) + 2.0f) * 0.5f;
            default:
                return input;
        }
    }
}
