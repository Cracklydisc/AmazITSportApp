package com.huami.watch.utils;

import android.content.Context;
import com.huami.watch.ui.C1070R;

public final class LayoutUtils {
    private LayoutUtils() {
    }

    public static int getItemHeight(Context context) {
        return context.getResources().getDimensionPixelSize(C1070R.dimen.settings_item_height);
    }

    public static int getIconTextMargin(Context context) {
        return context.getResources().getDimensionPixelSize(C1070R.dimen.setting_icon_txt_margin);
    }
}
