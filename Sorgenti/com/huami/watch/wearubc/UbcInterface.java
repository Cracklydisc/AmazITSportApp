package com.huami.watch.wearubc;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class UbcInterface {
    private static boolean DEBUG = GlobalConfig.DEBUG;
    public static String EVENT_TEST1_ID1_LAUGH = "xx0001";
    public static String EVENT_TEST1_ID2_CRY = "xx0002";
    public static String EVENT_TEST1_ID3_YEAL = "xx0003";
    private static String TAG = "UbcInterface";
    private static String sAppId = null;
    private static String sAppVer = null;
    private static Context sContext;
    private static Handler sHandler;
    private static Looper sLooper;
    private static UbcDataManager sUbcDataManager;

    public static class MsgParams {
        String mAppId;
        String mAppVer;
        String mCategery;
        String mEventId;
        Map<String, String> mParams;

        public MsgParams(String eventId, String appVser, String appId, Map<String, String> parmas) {
            this(eventId, appVser, appId, parmas, null);
        }

        public MsgParams(String eventId, String appVer, String appId, Map<String, String> params, String categery) {
            this.mEventId = eventId;
            this.mParams = params;
            this.mCategery = null;
            this.mAppVer = appVer;
            this.mAppId = appId;
        }
    }

    private static class UbcHandler extends Handler {
        public UbcHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (UbcInterface.DEBUG) {
                Log.d(UbcInterface.TAG, " handle message:" + msg);
            }
            switch (msg.what) {
                case 1:
                    UbcInterface.recordEvent((MsgParams) msg.obj);
                    return;
                case 2:
                    UbcInterface.releaseDelayed();
                    return;
                default:
                    Log.w(UbcInterface.TAG, "Unknow message :" + msg);
                    return;
            }
        }
    }

    private static JSONObject paramsToJson(MsgParams msgParams) {
        if (msgParams == null) {
            Log.e(TAG, "msgParams is null");
        }
        JSONObject json = new JSONObject();
        try {
            json.put("eid", UbcDataManager.WATCH_EVENT_PREFIX + msgParams.mAppId + msgParams.mEventId);
            json.put("ver", msgParams.mAppVer);
            json.put("stmp", System.currentTimeMillis());
            JSONObject parmsJson = null;
            if (msgParams.mParams != null) {
                parmsJson = new JSONObject(msgParams.mParams);
            }
            String str = "pms";
            if (parmsJson == null) {
                parmsJson = null;
            }
            json.put(str, parmsJson);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void recordEvent(MsgParams msgParams) {
        if (msgParams == null) {
            Log.w(TAG, "null msg params");
            return;
        }
        JSONObject json = paramsToJson(msgParams);
        if (DEBUG) {
            Log.d(TAG, "Json put in database:" + json);
        }
        sUbcDataManager.putEvent(Base64.encodeToString(json.toString().getBytes(), 0));
    }

    public static synchronized void initialize(Context context, String appId, String appVer) {
        synchronized (UbcInterface.class) {
            initialize(context, appId, appVer, null);
        }
    }

    public static synchronized void initialize(Context context, String appId, String appVer, Looper looper) {
        synchronized (UbcInterface.class) {
            if (context == null) {
                throw new IllegalArgumentException("context should not be null");
            } else if (appId == null) {
                throw new IllegalArgumentException("must have appId");
            } else if (appVer == null) {
                throw new IllegalArgumentException("must have appVer");
            } else {
                if (sLooper == null || sContext == null || sHandler == null || sAppId == null || sAppVer == null) {
                    sContext = context.getApplicationContext();
                    if (sContext == null) {
                        sContext = context;
                    }
                    sUbcDataManager = new UbcDataManager(sContext);
                    if (looper != null) {
                        sLooper = looper;
                    } else {
                        sLooper = Looper.getMainLooper();
                    }
                    if (sLooper == null) {
                        throw new IllegalStateException("no looper prepared in this thread");
                    }
                    sHandler = new UbcHandler(sLooper);
                    if (sAppId == null) {
                        sAppId = appId;
                    } else {
                        Log.w(TAG, "try to overwrite appid :" + sAppId + " to:" + sAppId);
                    }
                    if (sAppVer == null) {
                        sAppVer = appVer;
                    } else {
                        Log.w(TAG, "try to overwrite appVersion: " + sAppVer + " to:" + sAppVer);
                    }
                } else {
                    Log.d(TAG, "already initialized~~");
                }
            }
        }
    }

    private static void checkHandler() {
        if (sHandler == null) {
            throw new IllegalStateException("Must call initialize first!");
        }
    }

    public static void recordCountEvent(String id) {
        recordCountEvent(id, null, null);
    }

    public static void recordCountEvent(String id, String category, Map<String, String> params) {
        Log.d("UBC_TEST_TAG", "recordCountEvent eventId:" + UbcDataManager.WATCH_EVENT_PREFIX + sAppId + id);
        checkHandler();
        Map<String, String> p = params;
        if (p == null) {
            p = new HashMap();
        }
        p.put("event_count", "1");
        sHandler.obtainMessage(1, new MsgParams(id, sAppVer, sAppId, p)).sendToTarget();
    }

    public static void recordPropertyEvent(String id, String... properties) {
        if (properties == null || properties.length == 0) {
            Log.e(TAG, "properties is null of event:" + id);
        }
        Log.d("UBC_TEST_TAG", "recordPropertyEvent eventId:" + UbcDataManager.WATCH_EVENT_PREFIX + sAppId + id);
        for (String property : properties) {
            Log.d("UBC_TEST_TAG", "properties:" + property);
        }
        checkHandler();
        Map<String, String> p = new HashMap();
        String propertyValue = "";
        for (int i = 0; i < properties.length; i++) {
            propertyValue = propertyValue + properties[i];
            if (i != properties.length - 1) {
                propertyValue = propertyValue + "|";
            }
        }
        if (DEBUG) {
            Log.d(TAG, "propertyValues:" + propertyValue);
        }
        p.put("event_property", propertyValue);
        p.put("event_count", "1");
        sHandler.obtainMessage(1, new MsgParams(id, sAppVer, sAppId, p)).sendToTarget();
    }

    private static void releaseDelayed() {
    }
}
