package com.huami.watch.wearubc;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.provider.BaseColumns;
import android.util.Base64;
import android.util.Log;
import com.huami.watch.wearubc.IUbcService.Stub;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UbcDataManager {
    private static boolean DEBUG = GlobalConfig.DEBUG;
    public static String WATCH_EVENT_PREFIX = "watch";
    private static ArrayList<ContentValues> sUbcCaches = new ArrayList(0);
    private ArrayList<String> mCacheBeforeConnectedWithService = new ArrayList();
    private Context mContext;
    private IUbcService mService;
    private ServiceConnection mServiceConnection = new C10731();
    private int mServiceConnectionState = 0;
    private ServiceHandler mServiceHandler;

    class C10731 implements ServiceConnection {
        C10731() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            UbcDataManager.this.mService = Stub.asInterface(service);
            UbcDataManager.this.mServiceHandler.obtainMessage(1).sendToTarget();
        }

        public void onServiceDisconnected(ComponentName name) {
            UbcDataManager.this.mServiceHandler.obtainMessage(2).sendToTarget();
        }
    }

    class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    UbcDataManager.this.handleServiceConnectState(1);
                    return;
                case 2:
                    UbcDataManager.this.handleServiceConnectState(0);
                    return;
                case 3:
                    UbcDataManager.this.putEventInternal((String) msg.obj);
                    return;
                case 4:
                    UbcDataManager.this.releaseInternal();
                    return;
                case 5:
                    UbcDataManager.this.handleServiceConnectState(2);
                    return;
                default:
                    Log.w("UbcDataManager", "unknow message");
                    return;
            }
        }
    }

    public static final class UbcSettingsTable implements BaseColumns {
    }

    public static final class UbcTableData implements BaseColumns {
        public static final Uri CONTENT_URI_DATA = Uri.parse("content://com.huami.watch.wearubc.UbcContentProvider/ubc_info");

        public static boolean putEventItem(IUbcService service, String jsonData, String packageName) {
            try {
                ContentValues values = new ContentValues();
                values.put("_data", jsonData);
                values.put("_app", packageName);
                values.put("_sync_state", Integer.valueOf(3));
                values.put("_sync_prior", Integer.valueOf(-1));
                service.insert(values);
                return true;
            } catch (RemoteException e) {
                Log.w("UbcTableData", "Can't set key " + jsonData + e);
                return false;
            }
        }
    }

    private void handleServiceConnectState(int state) {
        if (DEBUG) {
            Log.d("UbcDataManager", "handleServiceConnectState " + state);
        }
        this.mServiceConnectionState = state;
        switch (state) {
            case 0:
                if (DEBUG) {
                    Log.w("UbcDataManager", "testCach Service disconnected");
                    return;
                }
                return;
            case 1:
                if (this.mCacheBeforeConnectedWithService.size() > 0) {
                    if (DEBUG) {
                        Log.d("UbcDataManager", "testCach Service connected put cached event first");
                    }
                    Iterator i$ = this.mCacheBeforeConnectedWithService.iterator();
                    while (i$.hasNext()) {
                        String event = (String) i$.next();
                        if (DEBUG) {
                            Log.d("UbcDataManager", "testCach putCatchEvent:" + event);
                        }
                        putEventInternal(event);
                    }
                    this.mCacheBeforeConnectedWithService.clear();
                    return;
                }
                return;
            case 2:
                if (DEBUG) {
                    Log.d("UbcDataManager", "testCach Service is connecting");
                    return;
                }
                return;
            default:
                Log.w("UbcDataManager", "unknow state:" + state);
                return;
        }
    }

    public UbcDataManager(Context context) {
        this.mContext = context;
        Looper looper = Looper.myLooper();
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        this.mServiceHandler = new ServiceHandler(looper);
        initService();
    }

    private void initService() {
        Intent service = createIntentFromAction(this.mContext, "android.intent.action.UBC_SERVICE");
        if (service == null) {
            Log.w("UbcDataManager", "init service failed, is service been uninstalled?");
            return;
        }
        this.mContext.bindService(service, this.mServiceConnection, 1);
        this.mServiceHandler.obtainMessage(5).sendToTarget();
    }

    private Intent createIntentFromAction(Context context, String action) {
        Intent implicitIntent = new Intent(action);
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentServices(implicitIntent, 0);
        String className = null;
        String packageName = null;
        if (resolveInfoList == null || resolveInfoList.size() == 0) {
            return null;
        }
        Iterator i$ = resolveInfoList.iterator();
        if (i$.hasNext()) {
            ResolveInfo resolveInfo = (ResolveInfo) i$.next();
            packageName = resolveInfo.serviceInfo.packageName;
            className = resolveInfo.serviceInfo.name;
        }
        if (packageName == null || className == null) {
            return null;
        }
        ComponentName component = new ComponentName(packageName, className);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    private void putEventInternal(String jsonEvent) {
        if (this.mServiceConnectionState == 1) {
            if (DEBUG) {
                Log.d("UbcDataManager", "testCach putEventInternal jsonEvent:" + new String(Base64.decode(jsonEvent, 0)));
            }
            if (this.mService != null) {
                if (!UbcTableData.putEventItem(this.mService, jsonEvent, this.mContext.getPackageName())) {
                    Log.w("UbcDataManager", "testCatch putEvent failed :" + jsonEvent);
                    this.mCacheBeforeConnectedWithService.add(jsonEvent);
                    return;
                } else if (DEBUG) {
                    Log.d("UbcDataManager", "testCatch event: " + jsonEvent + " record success");
                    return;
                } else {
                    return;
                }
            } else if (DEBUG) {
                Log.d("UbcDataManager", "testCach mService is null event lost:" + new String(Base64.decode(jsonEvent, 0)));
                return;
            } else {
                return;
            }
        }
        Log.w("UbcDataManager", "testCach putEvent " + new String(Base64.decode(jsonEvent, 0)) + " in cache when service not connected");
        this.mCacheBeforeConnectedWithService.add(jsonEvent);
        if (this.mServiceConnectionState == 0) {
            if (DEBUG) {
                Log.d("UbcDataManager", "try to rebind");
            }
            initService();
        }
    }

    public void putEvent(String jsonEvent) {
        this.mServiceHandler.obtainMessage(3, jsonEvent).sendToTarget();
    }

    private void releaseInternal() {
        if (this.mServiceConnectionState == 1) {
            this.mContext.unbindService(this.mServiceConnection);
            this.mServiceConnectionState = 0;
            if (DEBUG) {
                Log.d("UbcDataManager", "service unbind");
            }
        }
        if (DEBUG) {
            Log.d("UbcDataManager", "UbcDataManager released");
        }
    }
}
