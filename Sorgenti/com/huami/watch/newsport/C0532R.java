package com.huami.watch.newsport;

public final class C0532R {

    public static final class anim {
    }

    public static final class animator {
    }

    public static final class array {
    }

    public static final class attr {
    }

    public static final class color {
    }

    public static final class dimen {
    }

    public static final class drawable {
    }

    public static final class id {
    }

    public static final class integer {
    }

    public static final class interpolator {
    }

    public static final class layout {
    }

    public static final class mipmap {
    }

    public static final class string {
    }

    public static final class style {
    }

    public static final class styleable {
        public static final int[] BoxInsetLayout_Layout = new int[]{C0532R.attr.hm_layout_box, C0532R.attr.layout_box};
        public static final int[] CircledImageView = new int[]{16843033, C0532R.attr.circle_color, C0532R.attr.circle_radius, C0532R.attr.circle_radius_pressed, C0532R.attr.circle_border_width, C0532R.attr.circle_border_color, C0532R.attr.circle_padding, C0532R.attr.shadow_width};
        public static final int[] DelayedConfirmationView = new int[]{C0532R.attr.update_interval};
        public static final int[] DotsPageIndicator = new int[]{C0532R.attr.dotSpacing, C0532R.attr.dotRadius, C0532R.attr.dotRadiusSelected, C0532R.attr.dotColor, C0532R.attr.dotColorSelected, C0532R.attr.dotFadeWhenIdle, C0532R.attr.dotFadeOutDelay, C0532R.attr.dotFadeOutDuration, C0532R.attr.dotFadeInDuration, C0532R.attr.dotShadowColor, C0532R.attr.dotShadowRadius, C0532R.attr.dotShadowDx, C0532R.attr.dotShadowDy};
        public static final int[] HillViewData = new int[]{C0532R.attr.radius, C0532R.attr.rect_length, C0532R.attr.hill_even_color, C0532R.attr.hill_up_color, C0532R.attr.hill_down_color, C0532R.attr.hill_text_size};
        public static final int[] PageListView = new int[]{C0532R.attr.centerItemInPage, C0532R.attr.itemCountPerPage};
        public static final int[] RealDataDisplay = new int[]{C0532R.attr.curve_height, C0532R.attr.curve_color, C0532R.attr.text_color, C0532R.attr.textsize, C0532R.attr.curve_padding_bottom, C0532R.attr.curve_padding_top};
        public static final int[] RecyclerView = new int[]{16842948, C0532R.attr.layoutManager, C0532R.attr.spanCount, C0532R.attr.reverseLayout, C0532R.attr.stackFromEnd};
        public static final int[] WatchViewStub = new int[]{C0532R.attr.rectLayout, C0532R.attr.roundLayout};
        public static final int[] WearableHeaderTextView = new int[]{C0532R.attr.circular_layout_gravity, C0532R.attr.circular_text_size};
    }
}
