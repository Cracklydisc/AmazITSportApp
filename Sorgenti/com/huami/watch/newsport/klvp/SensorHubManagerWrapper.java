package com.huami.watch.newsport.klvp;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.historydata.utils.DataTypeChangeHelper;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.sensor.HmGpsLocation;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnConfigFinishListener;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportConfig;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SensorHubManagerWrapper {
    public static SensorHubManagerWrapper instance = null;
    private Context mContext = null;
    private KlvpDataListener mKlvpDataListener = null;
    private Object mLock = new Object();
    private List<Callback> mOnceUpdateCallback = new ArrayList();
    private HmSensorHubConfigManager mSensorHubConfigManager = null;
    private Callback mSportDataUpdateCallback = null;
    private Callback mSportLapCallback = null;
    private WeakReference<OutdoorSportSnapshot> mSportSnapshot = null;
    private int mSportType = -1;
    private Runnable mStartBunchModeRunnable = new C05751();
    private Runnable mStopBunchModeRunnable = new C05762();
    private Handler mThreadHandler = null;
    private Runnable mTimeOutRequestDataRunnable = new C05839();

    class C05751 implements Runnable {
        C05751() {
        }

        public void run() {
            Debug.m6w("SensorHubManagerWrapper", "msg start bunch mode gps");
            SensorHubManagerWrapper.this.mSensorHubConfigManager.startBatchModeGPS();
        }
    }

    class C05762 implements Runnable {
        C05762() {
        }

        public void run() {
            Debug.m6w("SensorHubManagerWrapper", "msg stop bunch mode gps");
            SensorHubManagerWrapper.this.mSensorHubConfigManager.stopBatchModeGPS();
        }
    }

    class C05839 implements Runnable {
        C05839() {
        }

        public void run() {
            Debug.m5i("SensorHubManagerWrapper", "request data time out, mOnceUpdateCallback:" + SensorHubManagerWrapper.this.mOnceUpdateCallback);
            SensorHubManagerWrapper.this.notifyOnceRequestCallback(-1, null);
        }
    }

    private static class KlvpDataListener implements OnKlvpDataListener {
        private WeakReference<SensorHubManagerWrapper> mSensorHubManagerWrapperWeakReference = null;

        public KlvpDataListener(SensorHubManagerWrapper sensorHubManagerWrapper) {
            this.mSensorHubManagerWrapperWeakReference = new WeakReference(sensorHubManagerWrapper);
        }

        public void onHealthDataReady(int i, float v) {
        }

        public void onSportDataReady(SportStatistics sportData) {
            SensorHubManagerWrapper sensorHubManagerWrapper = (SensorHubManagerWrapper) this.mSensorHubManagerWrapperWeakReference.get();
            if (sensorHubManagerWrapper == null) {
                Debug.m3d("SensorHubManagerWrapper", "sensor hub manager wrapper is null!");
                return;
            }
            Callback callback = sensorHubManagerWrapper.mSportDataUpdateCallback;
            if (callback == null) {
                Debug.m3d("SensorHubManagerWrapper", "callback is null while on sport data ready!");
                sensorHubManagerWrapper.notifyOnceRequestCallback(0, null);
                return;
            }
            OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) sensorHubManagerWrapper.mSportSnapshot.get();
            if (snapshot == null) {
                Debug.m3d("SensorHubManagerWrapper", "snapshot is null while on sport data ready!");
                sensorHubManagerWrapper.notifyOnceRequestCallback(0, null);
            } else if (sportData == null) {
                Debug.m6w("SensorHubManagerWrapper", "sport data is null");
                callback.notifyCallback(-1, null);
                sensorHubManagerWrapper.notifyOnceRequestCallback(0, null);
            } else {
                StringBuffer sb;
                Debug.m6w("SensorHubManagerWrapper", "receive klvp gps status:" + sportData.getMCurSportGpsStatus());
                if (sportData.mRealtimeAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport realtime avg data is null");
                } else {
                    byte[] heartByte = DataTypeChangeHelper.intTo4byte(sportData.mRealtimeAvg.getMHeartRate());
                    snapshot.setRealTimeSpeed(sportData.mRealtimeAvg.getMSpeed());
                    snapshot.setRealTimePace(sportData.mRealtimeAvg.getMPace());
                    snapshot.setHeartRate((float) (heartByte[3] & 255));
                    snapshot.setHeartQuantity(heartByte[2] & 31);
                    snapshot.setHeartRange((heartByte[2] & 255) >> 5);
                    snapshot.setRealTimeStepFreq(sportData.mRealtimeAvg.getMCadence());
                    snapshot.setVerticalVelocity(sportData.mRealtimeAvg.getMVerticalSpeed());
                    if (sportData.getMCurSportGpsStatus() == 4 || sportData.getMCurSportGpsStatus() == 1 || sportData.getMCurSportGpsStatus() == 16) {
                        snapshot.setAbsoluteAtitude(-20000.0f);
                    } else {
                        snapshot.setAbsoluteAtitude(sportData.mRealtimeAvg.getMAltitude());
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m6w("SensorHubManagerWrapper", "sport realtime heart: " + (heartByte[3] & 255) + ", heart quantity:" + (heartByte[2] & 31));
                    }
                }
                if (sportData.mSportAcm == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport acm data is null");
                } else {
                    snapshot.setDistance(sportData.mSportAcm.getMDistance());
                    if (Global.DEBUG_TRAINING_MODE) {
                        TrainingInfo info = TrainingInfoManager.getInstance(Global.getApplicationContext()).getTrainingInfo();
                        if (!(info.getTrainStatus() == 1 || info.getTrainInfoId() == -1 || TrainingInfoManager.getInstance(Global.getApplicationContext()).changeTrainTypeToSportType(info.getTrainType()) != sensorHubManagerWrapper.mSportType)) {
                            snapshot.setDistance(info.getDistance() + sportData.mSportAcm.getMDistance());
                        }
                    }
                    snapshot.setTotalTime(((long) sportData.mSportAcm.getMTimer()) * 1000);
                    snapshot.setCalorie(sportData.mSportAcm.getMSportCalories() * 1000.0f);
                    snapshot.setStepCount(sportData.mSportAcm.getMStep());
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m5i("SensorHubManagerWrapper", "gps status:" + sportData.getMCurSportGpsStatus());
                    }
                    snapshot.setClimbUp((float) Math.round(sportData.mSportAcm.getMAscendMeter()));
                    snapshot.setClimbDown((float) Math.round(Math.abs(sportData.mSportAcm.getMDescendMeter())));
                }
                if (sportData.mSportAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport avg data is null");
                } else {
                    snapshot.setTotalPace(sportData.mSportAvg.getMPace());
                    snapshot.setTotalSpeed(sportData.mSportAvg.getMSpeed());
                    snapshot.setStepFreq(sportData.mSportAvg.getMCadence());
                    snapshot.setOffsetAltitude(sportData.mSportAvg.getMAltitude());
                }
                if (sportData.mSportKiloAcm == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport kilo avg data is null");
                } else {
                    snapshot.setLastKMCostTime((long) (sportData.mSportKiloAcm.getMTimer() * 1000.0f));
                }
                if (sportData.mSportKiloAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport kilo avg data is null");
                } else {
                    snapshot.setLastKMPace(sportData.mSportKiloAvg.getMPace());
                }
                snapshot.setLastKilometer(sportData.getMSportKiloNum());
                if (sportData.mRtSportLapAcm == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport lap rt acm data is null");
                } else {
                    snapshot.setIndividualDistance(sportData.mRtSportLapAcm.getMDistance());
                    snapshot.setIndividaulCostTime((long) (sportData.mRtSportLapAcm.getMTimer() * 1000.0f));
                }
                if (sportData.mRtSportLapAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport lap rt avg data is null");
                } else {
                    snapshot.setIndividualPace(sportData.mRtSportLapAvg.getMPace());
                }
                if (sportData.mSportLapAcm == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport lap acm data is null");
                } else {
                    snapshot.setPreviousLapsDistance(sportData.mSportAcm.getMDistance());
                    snapshot.setPreviousLapsCostTime((long) (sportData.mSportLapAcm.getMTimer() * 1000.0f));
                    snapshot.setRtCurLapMaxDownhillElveLoss(sportData.mSportLapAcm.getMAscendMeter());
                }
                if (sportData.mSportMaxAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport max avg data is null");
                } else if (Float.compare(sportData.mSportMaxAvg.getMAltitude(), -99999.0f) != 0) {
                    snapshot.setHighAltitude(sportData.mSportMaxAvg.getMAltitude());
                }
                if (sportData.mSportMinAvg == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport min avg data is null");
                } else if (Float.compare(sportData.mSportMinAvg.getMAltitude(), 99999.0f) != 0) {
                    snapshot.setLowAltitude(sportData.mSportMinAvg.getMAltitude());
                }
                if (sportData.mRiddingInfo == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport riding info data is null");
                } else {
                    snapshot.setClimbdisascend((float) Math.round(sportData.mRiddingInfo.getMDistance3DAscend()));
                    snapshot.setClimbdisDescend((float) Math.round(sportData.mRiddingInfo.getMDistance3DDescend()));
                    snapshot.setAscendTime((long) sportData.mRiddingInfo.getMTimeAscend());
                    snapshot.setDescendTime((long) sportData.mRiddingInfo.getMTimeDescend());
                }
                if (sportData.mSwimInfo == null) {
                    Debug.m6w("SensorHubManagerWrapper", "swim info data is null");
                } else {
                    snapshot.setSwolfPerFixedMeters(sportData.mSwimInfo.getMSwolfPerFixedMeters());
                    snapshot.setTotalStrokes(sportData.mSwimInfo.getMTotalStrokes());
                    snapshot.setTotalTrips(sportData.mSwimInfo.getMTotalTrips());
                    snapshot.setRtDistancePerStroke(sportData.mSwimInfo.getMRtDistancePerStroke());
                    snapshot.setRtStrokeSpeed(sportData.mSwimInfo.getMRtStrokeSpeed());
                    snapshot.setAvgStrokeSpeed(sportData.mSwimInfo.getMAvgStrokeSpeed());
                    snapshot.setMaxStrokeSpeed(sportData.mSwimInfo.getMMaxStrokeSpeed());
                    snapshot.setAvgDistancePerStroke(sportData.mSwimInfo.getMAvgDistancePerStroke());
                    snapshot.setSwimStyle(sportData.mSwimInfo.getMSwimStyle());
                }
                if (sportData.mSportThaInfo == null) {
                    Debug.m6w("SensorHubManagerWrapper", "mSportThaInfo info data is null");
                } else {
                    SportThaInfo sportThaInfo = snapshot.getSportThaInfo();
                    if (sportThaInfo == null) {
                        sportThaInfo = new SportThaInfo();
                    }
                    snapshot.setmETEdailyPerformance(sportData.mSportEteInfo.getETEdailyPerformance());
                    sportThaInfo.setCurrnetDayTrainLoad(sportData.mSportEteInfo.getETEtrainingLoadPeak());
                    sportThaInfo.setVo2MaxTread(sportData.mSportThaInfo.getVo2MaxTrend());
                    sportThaInfo.setTrainingLoadTrend(sportData.mSportThaInfo.getTrainingLoadTrend());
                    sportThaInfo.setFitnessClass(sportData.mSportThaInfo.getFitnessClass());
                    sportThaInfo.setFitnessLevelIncrease(sportData.mSportThaInfo.getFitnessLevelIncrease());
                    sportThaInfo.setWtlStatus(sportData.mSportThaInfo.getWtlStatus());
                    if (sportData.mSportThaInfo.getWtlSum() > 65535) {
                        sportThaInfo.setWtlSum(-1);
                    } else {
                        sportThaInfo.setWtlSum(sportData.mSportThaInfo.getWtlSum());
                    }
                    sportThaInfo.setWtlSumOptimalMax(sportData.mSportThaInfo.getWtlSumOptimalMax());
                    sportThaInfo.setWtlSumOptimalMin(sportData.mSportThaInfo.getWtlSumOptimalMin());
                    sportThaInfo.setWtlSumOverreaching(sportData.mSportThaInfo.getWtlSumOverreaching());
                    snapshot.setSportThaInfo(sportThaInfo);
                    sb = new StringBuffer();
                    sb.append("getVo2MaxTrend:" + sportData.mSportThaInfo.getVo2MaxTrend() + ",");
                    sb.append("getTrainingLoadTrend:" + sportData.mSportThaInfo.getTrainingLoadTrend() + ",");
                    sb.append("getFitnessClass:" + sportData.mSportThaInfo.getFitnessClass() + ",");
                    sb.append("getFitnessLevelIncrease:" + sportData.mSportThaInfo.getFitnessLevelIncrease() + ",");
                    sb.append("getWtlStatus:" + sportData.mSportThaInfo.getWtlStatus() + ",");
                    sb.append("getWtlSum:" + sportData.mSportThaInfo.getWtlSum() + ",");
                    sb.append("getWtlSumOptimalMax:" + sportData.mSportThaInfo.getWtlSumOptimalMax() + ",");
                    sb.append("getWtlSumOptimalMin:" + sportData.mSportThaInfo.getWtlSumOptimalMin() + ",");
                    sb.append("getWtlSumOverreaching:" + sportData.mSportThaInfo.getWtlSumOverreaching());
                    Log.i("SensorHubManagerWrapper", " mSportThaInfo " + sb.toString());
                }
                if (sportData.mSportEteInfo == null) {
                    Debug.m6w("SensorHubManagerWrapper", "mSportEteInfo info data is null");
                } else {
                    float vo2max = (float) (((double) (((float) sportData.mSportEteInfo.getETEmaximalMET()) / 65535.0f)) * 3.5d);
                    int recoveryTime = sportData.mSportEteInfo.getETEresourceRecovery();
                    int te = sportData.mSportEteInfo.getETEtrainingEffect();
                    float trainLoad = ((float) sportData.mSportEteInfo.getETEtrainingLoadPeak()) / 65535.0f;
                    snapshot.setmSportEteVo2Max(vo2max);
                    snapshot.setmSportEteTrainingEffect(te);
                    snapshot.setmSportEteResourceRecovery(recoveryTime);
                    snapshot.setmSportEteTraingLoadPeak(trainLoad);
                    sb = new StringBuffer();
                    sb.append("getETEmaximalMET:" + sportData.mSportEteInfo.getETEmaximalMET() + ",");
                    sb.append("getETEresourceRecovery:" + sportData.mSportEteInfo.getETEresourceRecovery() + ",");
                    sb.append("getETEtrainingEffect:" + sportData.mSportEteInfo.getETEtrainingEffect() + ",");
                    sb.append("getETEtrainingLoadPeak:" + sportData.mSportEteInfo.getETEtrainingLoadPeak() + ",");
                    sportData.mSportEteInfo.getETEdailyPerformance();
                    sportData.mSportEteInfo.getETEphraseNumber();
                    Log.i("SensorHubManagerWrapper", " mSportEteInfo " + sb.toString());
                }
                if (sportData.mSportSetStat == null) {
                    Debug.m6w("SensorHubManagerWrapper", "sport set info data is null");
                } else {
                    snapshot.setSportSetTotalTime(sportData.mSportSetStat.getMSportSetTotalTime() * 1000);
                    snapshot.setSportSetTotalDistance(sportData.mSportSetStat.getMSportSetTotalDistance());
                    snapshot.setSportSetTotalCalories(sportData.mSportSetStat.getMSportSetTotalCalories() * 1000.0f);
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m6w("SensorHubManagerWrapper", "sport set info data:" + snapshot.getSportSetTotalTime() + ", " + snapshot.getSportSetTotalDistance() + "," + snapshot.getsSportSetTotalCalories());
                    }
                }
                if (sportData.mGPSLonLatPoint != null) {
                    Long[] lonLat = new Long[sportData.mGPSLonLatPoint.length];
                    for (int i = 0; i < lonLat.length; i++) {
                        lonLat[i] = Long.valueOf(sportData.mGPSLonLatPoint[i]);
                    }
                    snapshot.setCurGPSLonLatPoint(lonLat);
                }
                if (snapshot.getSportType() == 11) {
                    snapshot.setDownHillNum(sportData.getMSportLapNum());
                    if (sportData.mSportMaxAvg != null) {
                        snapshot.setDownhillMaxSpeed(sportData.mSportMaxAvg.getMSpeed());
                    }
                    if (sportData.mRtSportLapAcm != null) {
                        snapshot.setSingleDownhillDescend(sportData.mRtSportLapAcm.getMDistance());
                        snapshot.setSingleDownhillElveLoss(sportData.mRtSportLapAcm.getMDescendMeter());
                    }
                    if (sportData.mSportLapAvg != null) {
                        snapshot.setDownhillAvgSpeed(sportData.mSportLapAvg.getMSpeed());
                        snapshot.setRtCurLapAvgPace(sportData.mSportLapAvg.getMPace());
                    }
                }
                if (snapshot.getSportType() == 17 && sportData.mTennisInfo != null) {
                    int[] tennicInfos = sportData.mTennisInfo.mSwingCnt;
                    Log.i("SensorHubManagerWrapper", " tennicInfos:" + Utils.dump(tennicInfos));
                    if (tennicInfos != null && tennicInfos.length >= 4) {
                        snapshot.setmStrokes(tennicInfos[0]);
                        snapshot.setmForeHand(tennicInfos[1]);
                        snapshot.setmBackHand(tennicInfos[2]);
                        snapshot.setmServe(tennicInfos[3]);
                    }
                }
                callback.notifyCallback(0, snapshot);
                if (sensorHubManagerWrapper.mSportLapCallback != null) {
                    sensorHubManagerWrapper.mSportLapCallback.notifyCallback(0, snapshot);
                    sensorHubManagerWrapper.mSportLapCallback = null;
                }
                if (sensorHubManagerWrapper.mOnceUpdateCallback != null) {
                    Global.getGlobalWorkHandler().removeCallbacks(sensorHubManagerWrapper.mTimeOutRequestDataRunnable);
                    sensorHubManagerWrapper.notifyOnceRequestCallback(0, null);
                }
            }
        }
    }

    private SensorHubManagerWrapper(Context context) {
        this.mContext = context;
        this.mSensorHubConfigManager = HmSensorHubConfigManager.getHmSensorHubConfigManager(context);
        this.mThreadHandler = Global.getGlobalWorkHandler();
    }

    public static synchronized SensorHubManagerWrapper getInstance(Context context) {
        SensorHubManagerWrapper sensorHubManagerWrapper;
        synchronized (SensorHubManagerWrapper.class) {
            if (instance == null) {
                instance = new SensorHubManagerWrapper(context.getApplicationContext());
            }
            sensorHubManagerWrapper = instance;
        }
        return sensorHubManagerWrapper;
    }

    public void setSportSnapshot(OutdoorSportSnapshot sportSnapshot) {
        this.mSportSnapshot = new WeakReference(sportSnapshot);
    }

    public void setSportDataUpdateCallback(Callback sportDataUpdateCallback) {
        this.mSportDataUpdateCallback = sportDataUpdateCallback;
    }

    public void setLapCallback(Callback lapCallback) {
        this.mSportLapCallback = lapCallback;
    }

    public void setSportType(int sportType) {
        this.mSportType = sportType;
    }

    public void startChangeItemTimer(int sportType, long timeStamps) {
        if (this.mSensorHubConfigManager == null) {
            Debug.m5i("SensorHubManagerWrapper", "sIntance is null");
        } else {
            this.mSensorHubConfigManager.startChangeItemTimer(parseSportTypeToSensorHubSportType(sportType), timeStamps);
        }
    }

    public void stopChangeItemTimer(long timeStamps) {
        if (this.mSensorHubConfigManager == null) {
            Debug.m5i("SensorHubManagerWrapper", "sIntance is null");
        } else {
            this.mSensorHubConfigManager.stopChangeItemTimer(timeStamps);
        }
    }

    public List<HmGpsLocation> getAllGpsLocation() {
        if (this.mSensorHubConfigManager != null) {
            return this.mSensorHubConfigManager.getAllGpsLocation();
        }
        Debug.m5i("SensorHubManagerWrapper", "sIntance is null");
        return null;
    }

    public byte[] getHeartHistoryData() {
        if (this.mSensorHubConfigManager != null) {
            return this.mSensorHubConfigManager.getHeartHistoryData();
        }
        Debug.m5i("SensorHubManagerWrapper", "sIntance is null");
        return null;
    }

    public void setSLPTUpdateAt1Hz(boolean is1Hz) {
        this.mSensorHubConfigManager.setSLPTUpdateAt1Hz(is1Hz);
    }

    public void startBunchModeGPS() {
        Debug.m5i("SensorHubManagerWrapper", "start bunch mode gps");
        this.mThreadHandler.post(this.mStartBunchModeRunnable);
    }

    public void stopBunchModeGPS() {
        Debug.m5i("SensorHubManagerWrapper", "stop bunch mode gps");
        this.mThreadHandler.post(this.mStopBunchModeRunnable);
    }

    public boolean isGpsAvailable() {
        Debug.m5i("SensorHubManagerWrapper", "isGpsAvailable");
        if (this.mSensorHubConfigManager != null) {
            return this.mSensorHubConfigManager.getGpsState();
        }
        throw new IllegalArgumentException("mSensorHubConfigManager is null");
    }

    public int startSport(int sportType, long timestamp, int lastIndex, Bundle bundle, Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "start sport|timestamp:" + timestamp + "|sport type:" + sportType);
        if (this.mKlvpDataListener == null) {
            this.mKlvpDataListener = new KlvpDataListener(this);
        }
        int index = lastIndex + 1;
        final Callback callback2 = callback;
        this.mSensorHubConfigManager.startSport(((index << 24) | parseSportTypeToSensorHubSportType(sportType)) | ((((int) (timestamp / 1000)) & 255) << 16), timestamp, bundle, new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call start sport finish|i=" + i + "|s=" + s);
                callback2.notifyCallback(i, s);
            }
        });
        return index;
    }

    private static int parseSportTypeToSensorHubSportType(int sportType) {
        switch (sportType) {
            case 1001:
                return 1;
            case 1009:
                return 9;
            case 1015:
                return 15;
            default:
                return sportType;
        }
    }

    public void startMixedSport(long startTime, final Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "startMixedSport, stop|startTime:" + startTime + "|sport type:" + 2001);
        this.mSensorHubConfigManager.startMixedSport(new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call start sport finish|i=" + i + "|s=" + s);
                callback.notifyCallback(i, s);
            }
        }, startTime);
    }

    public void stopMixedSport(long startTime, final Callback callback, Bundle bundle) {
        Debug.m5i("SensorHubManagerWrapper", "stopMixedSport, stop|startTime:" + startTime + "|sport type:" + 2001);
        this.mSensorHubConfigManager.stopMixedSport(new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call stop sport finish|i=" + i + "|s=" + s);
                callback.notifyCallback(i, s);
            }
        }, startTime, bundle);
    }

    public void pauseSport(int sportType, long timestamp, Bundle bundle, final Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "pause stop|timestamp:" + timestamp + "|sport type:" + sportType);
        this.mSensorHubConfigManager.pauseSport(parseSportTypeToSensorHubSportType(sportType), timestamp, bundle, new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call pause sport finish|i=" + i + "|s=" + s);
                if (callback != null) {
                    callback.notifyCallback(i, s);
                }
            }
        });
    }

    public void continueSport(int sportType, long timestamp, Bundle bundle, final Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "continue sport|timestamp:" + timestamp + "|sport type:" + sportType);
        this.mSensorHubConfigManager.continueSport(parseSportTypeToSensorHubSportType(sportType), timestamp, bundle, new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call continue sport finish|i=" + i + "|s=" + s);
                if (callback != null) {
                    callback.notifyCallback(i, s);
                }
            }
        });
    }

    public void stopSport(int sportType, long timestamp, Callback callback) {
        stopSport(sportType, timestamp, new Bundle(), callback);
    }

    public void stopSport(int sportType, long timestamp, Bundle bundle, final Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "stop sport|timestamp:" + timestamp + "|sport type:" + sportType);
        this.mSensorHubConfigManager.stopSport(parseSportTypeToSensorHubSportType(sportType), bundle, new OnConfigFinishListener() {
            public void onFinish(int i, String s) {
                Debug.m5i("SensorHubManagerWrapper", "call stop sport finish|i=" + i + "|s=" + s);
                if (callback != null) {
                    callback.notifyCallback(i, s);
                }
            }
        });
    }

    public void notifySensorUpdateSportData(Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "notifySensorUpdateSportData|timestamp:" + System.currentTimeMillis() + "|notify type:" + 98);
        writeSportChannel(new byte[]{(byte) 9, (byte) 2}, callback);
    }

    public void notifySensorOpenHeartRate(Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "notifySensorOpenHeartRate|timestamp:" + System.currentTimeMillis() + "|notify type:" + 99);
        writeSportChannel(new byte[]{(byte) 9, (byte) 0}, callback);
    }

    public void notifyIndoorModifiedDistanceResult2SensorHub(int distance, Callback callback) {
        LogUtil.m9i(true, "SensorHubManagerWrapper", "notifyIndoorModifiedDistanceResult2SensorHub, distance:" + distance);
        byte[] disBytes = DataTypeChangeHelper.intTo4byteForReverse(distance);
        byte[] bytes = new byte[(disBytes.length + 1)];
        bytes[0] = (byte) 8;
        System.arraycopy(disBytes, 0, bytes, 1, disBytes.length);
        writeSportChannel(bytes, callback);
    }

    private void writeSportChannel(byte[] cmds, Callback callback) {
        LogUtil.m9i(true, "SensorHubManagerWrapper", "writeSportChannel");
        if (cmds == null) {
            LogUtil.m9i(true, "SensorHubManagerWrapper", "writeSportChannel failed");
            return;
        }
        this.mSensorHubConfigManager.writeSportChannel(cmds, cmds.length);
        if (callback != null) {
            callback.notifyCallback(0, null);
        }
    }

    public void notifySensorCloseHeartRate(Callback callback) {
        Debug.m5i("SensorHubManagerWrapper", "notifySensorCloseHeartRate|timestamp:" + System.currentTimeMillis() + "|notify type:" + 99);
        writeSportChannel(new byte[]{(byte) 9, (byte) 1}, callback);
    }

    public void setEnable(int wakeupSource, boolean enable, Callback callback, float... params) {
        Debug.m5i("SensorHubManagerWrapper", "set enable|wakeupSource:" + wakeupSource + "|enable:" + enable + "|params:" + Arrays.toString(params));
        this.mSensorHubConfigManager.setEnable(wakeupSource, enable, null, params);
        if (callback != null) {
            callback.notifyCallback(0, null);
        }
    }

    public void setSportConfigData(SportConfig sportConfig) {
        if (this.mSensorHubConfigManager != null) {
            this.mSensorHubConfigManager.setKlvpConfig(sportConfig, null);
        }
    }

    public void requestRealtimeData(OnKlvpDataListener listener) {
        if (this.mSensorHubConfigManager != null) {
            this.mSensorHubConfigManager.requestRealtimeData(2, listener);
        }
    }

    public void updateOutdoorSportStatus(OutdoorSportSnapshot status, Callback callback) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SensorHubManagerWrapper", "updateOutdoorSportStatus|lastStatus:" + status);
        }
        if (status == null) {
            throw new IllegalArgumentException("last status should not be null");
        }
        synchronized (this.mLock) {
            if (callback != null) {
                this.mOnceUpdateCallback.add(callback);
            }
        }
        this.mSensorHubConfigManager.requestRealtimeData(2, this.mKlvpDataListener);
        if (this.mOnceUpdateCallback != null) {
            Global.getGlobalWorkHandler().postDelayed(this.mTimeOutRequestDataRunnable, 10000);
        }
    }

    private void notifyOnceRequestCallback(int resultCode, Object params) {
        synchronized (this.mLock) {
            if (this.mOnceUpdateCallback != null) {
                for (Callback callback : this.mOnceUpdateCallback) {
                    callback.notifyCallback(resultCode, params);
                }
            }
            this.mOnceUpdateCallback.clear();
        }
    }

    public void clearup() {
        this.mKlvpDataListener = null;
        this.mSportLapCallback = null;
        synchronized (this.mLock) {
            if (this.mOnceUpdateCallback != null) {
                this.mOnceUpdateCallback.clear();
            }
        }
        this.mSportDataUpdateCallback = null;
        this.mSportSnapshot = null;
    }
}
