package com.huami.watch.newsport.route.action;

import android.graphics.Bitmap;
import com.huami.watch.newsport.gps.callback.IPointProvider;

public interface ITrailBitmapProvider {

    public interface INavigationOritationListener {
        float getOritationAngle();
    }

    public static final class Point {
        public final int mColor;
        public final long mId;
        public final float mX;
        public final float mY;

        public String toString() {
            return "Point{mId=" + this.mId + ", mX=" + this.mX + ", mY=" + this.mY + ", mColor=" + this.mColor + '}';
        }
    }

    void drawOnBitmap(Bitmap bitmap);

    void drawOritationBitmap(Bitmap bitmap);

    int getMaxScale();

    int getMinScale();

    int getScale();

    boolean isInitedNavigationData();

    boolean refreshBitmap(boolean z);

    void setOritationListener(INavigationOritationListener iNavigationOritationListener);

    void setScale(int i);

    void start(IPointProvider iPointProvider);

    void stop();
}
