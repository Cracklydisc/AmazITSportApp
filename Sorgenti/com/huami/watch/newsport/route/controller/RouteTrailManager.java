package com.huami.watch.newsport.route.controller;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.SystemProperties;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.gps.callback.IPointProvider;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.route.action.ITrailBitmapProvider;
import com.huami.watch.newsport.route.action.ITrailBitmapProvider.INavigationOritationListener;
import com.huami.watch.newsport.route.model.Degree;
import com.huami.watch.newsport.route.model.RoutePoint;
import com.huami.watch.newsport.route.test.TestNavigationParser;
import com.huami.watch.newsport.route.utils.CoordinatesHelper;
import com.huami.watch.newsport.route.utils.NavigationParser;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RouteTrailManager implements ITrailBitmapProvider {
    private static final int[] BG_WHITE_ROUTE_SCALE_RES = new int[]{C0532R.drawable.bg_white_sport_map_bg1, C0532R.drawable.bg_white_sport_map_bg2, C0532R.drawable.bg_white_sport_map_bg3, C0532R.drawable.bg_white_sport_map_bg4};
    private static int DEFAULT_LINE_COLOR = -16719355;
    private static final float[] ROUTE_END_POINT_SCALE = new float[]{1.0f, 0.9f, 0.8f, 0.7f};
    private static final int[] ROUTE_NAVIGATION_PAINT_WIDTH = new int[]{9, 7, 5, 5};
    public static final int[] ROUTE_SCALE_RANGE = new int[]{50, 200, 2000, 5000};
    private static final int[] ROUTE_SCALE_RES = new int[]{C0532R.drawable.sport_map_bg1, C0532R.drawable.sport_map_bg2, C0532R.drawable.sport_map_bg3, C0532R.drawable.sport_map_bg4};
    private static final String TAG = RouteTrailManager.class.getName();
    private int mBgType = 0;
    private double mCenterlat;
    private double mCenterlng;
    private int mDrawAreaHeight = 196;
    private int mDrawAreaWidth = 196;
    private Bitmap mEndBitmap = null;
    private RoutePoint mEndPoint;
    private boolean mFirstinComingPoint = false;
    private Bitmap mFlagBgBitmap = null;
    private Canvas mFlagCanvas = null;
    private Bitmap mFlagIconBitmap = null;
    private Paint mFlagTextPaint;
    private int mHeight = 196;
    private List<FlagNavigationPoint> mInRangeFlagRoutePoints = new ArrayList();
    private boolean mIsInitNavigationSuccessful = false;
    private Waypoint mLastNavigationWayPoint = null;
    private RoutePoint mLastShownRoutePoints = null;
    private Object mLock = new Object();
    private Matrix mMatrix = null;
    private Bitmap mNaviEndBitmap = null;
    private Bitmap mNaviStartBitmap = null;
    private Bitmap mNavigationBitmap = null;
    private Canvas mNavigationCanvas = null;
    private double mNavigationCenterlat;
    private double mNavigationCenterlng;
    private int mNavigationDatasLength = 0;
    private List<Waypoint> mNavigationFlagWayPoints = new ArrayList();
    private List<TrackSegment> mNavigationLocationDatas;
    private volatile float mNavigationOffsetXRadio = 0.0f;
    private volatile float mNavigationOffsetYRadio = 0.0f;
    private Paint mNavigationPaint = null;
    private Bitmap mOritationBitmap = null;
    private Canvas mOritationCanvas = null;
    private INavigationOritationListener mOritationListener;
    private RoutePoint mPenulTimatePoint;
    private IPointProvider mPointProvider;
    private Degree[] mRangeDegrees;
    private Canvas mRealTimeCanvas;
    private Paint mRealTimePaint;
    private Bitmap mRealTimeRouteBitmap;
    private int mScale = 0;
    private Bitmap mScaleBitmap;
    private Canvas mScaleCanvas;
    private Paint mScalePaint;
    private int[] mScaleRes = ROUTE_SCALE_RES;
    private int mSportType = -1;
    private Bitmap mStartBitmap = null;
    private RoutePoint mStartPoint;
    private Bitmap mTempNavigationBitmap = null;
    private Canvas mTempNavigationCanvas = null;
    private Canvas mTempRealTimeCanvas;
    private Bitmap mTempRealTimeRouteBitmap;
    private int mWidth = 196;
    private float mXOffset = 0.0f;
    private float mYOffset = 0.0f;

    private static class FlagNavigationPoint {
        private String name;
        private float f19x;
        private float f20y;

        private FlagNavigationPoint() {
        }
    }

    public RouteTrailManager(int width, int height, Bitmap startBitmap, Bitmap endBitmap, Bitmap startNaviBitmap, Bitmap endNaviBitmap, int sportType, int bgType) {
        this.mWidth = (int) (Math.sqrt(2.0d) * ((double) Math.max(width, height)));
        this.mHeight = (int) (Math.sqrt(2.0d) * ((double) Math.max(width, height)));
        this.mSportType = sportType;
        this.mBgType = bgType;
        this.mDrawAreaWidth = this.mWidth + 0;
        this.mDrawAreaHeight = this.mHeight + 0;
        this.mStartBitmap = Bitmap.createBitmap(startBitmap);
        this.mEndBitmap = Bitmap.createBitmap(endBitmap);
        this.mNaviStartBitmap = Bitmap.createBitmap(startNaviBitmap);
        this.mNaviEndBitmap = Bitmap.createBitmap(endNaviBitmap);
        this.mFlagBgBitmap = BitmapFactory.decodeResource(Global.getApplicationContext().getResources(), C0532R.drawable.sport_map_fun_cp_bg).copy(Config.ARGB_8888, true);
        this.mFlagIconBitmap = BitmapFactory.decodeResource(Global.getApplicationContext().getResources(), C0532R.drawable.sport_map_fun_icon_cp_dot).copy(Config.ARGB_8888, true);
        this.mMatrix = new Matrix();
        this.mNavigationBitmap = Bitmap.createBitmap(this.mWidth * 2, this.mHeight * 2, Config.ARGB_8888);
        this.mNavigationCanvas = new Canvas(this.mNavigationBitmap);
        this.mTempNavigationBitmap = Bitmap.createBitmap(this.mWidth * 2, this.mHeight * 2, Config.ARGB_8888);
        this.mTempNavigationCanvas = new Canvas(this.mTempNavigationBitmap);
        this.mNavigationPaint = new Paint(7);
        this.mNavigationPaint.setStrokeWidth(6.0f);
        if (this.mBgType == 1) {
            this.mNavigationPaint.setColor(-16711936);
        } else {
            this.mNavigationPaint.setColor(Color.parseColor("#FF686480"));
        }
        this.mNavigationPaint.setStyle(Style.STROKE);
        this.mNavigationPaint.setStrokeCap(Cap.ROUND);
        this.mFlagTextPaint = new Paint(1);
        if (this.mBgType == 1) {
            this.mFlagTextPaint.setColor(-16777216);
            this.mScaleRes = BG_WHITE_ROUTE_SCALE_RES;
        } else {
            this.mFlagTextPaint.setColor(-1);
        }
        this.mFlagTextPaint.setStyle(Style.STROKE);
        this.mFlagTextPaint.setTextSize(12.0f);
        this.mFlagTextPaint.setStrokeWidth(1.0f);
        this.mFlagTextPaint.setTextAlign(Align.CENTER);
        this.mFlagCanvas = new Canvas(this.mFlagBgBitmap);
        this.mScaleBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        this.mScalePaint = new Paint(1);
        if (this.mBgType == 1) {
            this.mScalePaint.setColor(-16777216);
        } else {
            this.mScalePaint.setColor(-1);
        }
        this.mScalePaint.setStyle(Style.FILL);
        this.mScalePaint.setStrokeWidth(2.0f);
        this.mScalePaint.setTextSize(14.0f);
        this.mScalePaint.setTextAlign(Align.CENTER);
        this.mScaleCanvas = new Canvas(this.mScaleBitmap);
        reScaleBitmap();
    }

    public int getScale() {
        return this.mScale;
    }

    public int getMaxScale() {
        return 0;
    }

    public int getMinScale() {
        return 3;
    }

    public boolean isInitedNavigationData() {
        return this.mIsInitNavigationSuccessful;
    }

    public void setScale(int index) {
        Debug.m5i(TAG, "setScale, now Scale:" + index);
        if (index >= 0 && index <= 3) {
            SystemProperties.set("sys.sport.android.scale", String.valueOf(ROUTE_SCALE_RANGE[index]));
            this.mScale = index;
            reScaleBitmap();
            reCalculateNavigationRange(true);
            refreshBitmap(true);
            drawOritationBitmap();
        }
    }

    private float getAngle() {
        if (this.mOritationListener != null) {
            return this.mOritationListener.getOritationAngle();
        }
        if (this.mEndPoint == null || this.mPenulTimatePoint == null) {
            return 0.0f;
        }
        double endX = (double) this.mEndPoint.f21x;
        double width = endX - ((double) this.mPenulTimatePoint.f21x);
        double height = ((double) this.mPenulTimatePoint.f22y) - ((double) this.mEndPoint.f22y);
        float grad = (float) Math.sqrt((width * width) + (height * height));
        if (grad == 0.0f) {
            return 0.0f;
        }
        float degrees = (float) Math.toDegrees((double) ((float) Math.asin((double) ((float) (width / ((double) grad))))));
        if (height > 0.0d) {
            return degrees;
        }
        return 180.0f - degrees;
    }

    private void updatePenulTimatePoint(RoutePoint lastPoint, RoutePoint currentPoint) {
        if (currentPoint == null) {
            LogUtil.m8e(true, TAG, "update penultimate point failed. current point is null");
        } else if (lastPoint == null || Float.compare(lastPoint.f21x, currentPoint.f21x) != 0 || Float.compare(lastPoint.f22y, currentPoint.f22y) != 0) {
            this.mPenulTimatePoint = lastPoint;
            this.mEndPoint = currentPoint;
        }
    }

    public void start(IPointProvider provider) {
        this.mPointProvider = provider;
        this.mRealTimeRouteBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.ARGB_8888);
        this.mRealTimeCanvas = new Canvas(this.mRealTimeRouteBitmap);
        this.mTempRealTimeRouteBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.ARGB_8888);
        this.mTempRealTimeCanvas = new Canvas(this.mTempRealTimeRouteBitmap);
        this.mRealTimePaint = new Paint(7);
        this.mRealTimePaint.setStrokeWidth(4.0f);
        if (this.mBgType == 1) {
            this.mRealTimePaint.setColor(-16777216);
        } else {
            this.mRealTimePaint.setColor(DEFAULT_LINE_COLOR);
        }
        this.mRealTimePaint.setStyle(Style.STROKE);
        this.mRealTimePaint.setStrokeCap(Cap.ROUND);
        this.mOritationBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.ARGB_8888);
        this.mOritationCanvas = new Canvas(this.mOritationBitmap);
    }

    private void initNavigationDatasIfNeed() {
        if (this.mNavigationLocationDatas == null) {
            long startTime = System.currentTimeMillis();
            Debug.m5i(TAG, "initNavigationDatasIfNeed, start");
            if (Global.DEBUG_ROUTE) {
                this.mNavigationLocationDatas = TestNavigationParser.getInstance().getNavigationLocationData();
            } else {
                this.mNavigationLocationDatas = NavigationParser.getTrackInfo(this.mNavigationFlagWayPoints, this.mSportType);
            }
            if (this.mNavigationLocationDatas != null) {
                Debug.m5i(TAG, "start, navigation data size:" + this.mNavigationLocationDatas.size());
            }
            if (!(this.mNavigationLocationDatas == null || this.mNavigationLocationDatas.isEmpty())) {
                List<Waypoint> firstPoints = ((TrackSegment) this.mNavigationLocationDatas.get(0)).getWaypoints();
                if (!(firstPoints == null || firstPoints.isEmpty())) {
                    setCenterLocation(((Waypoint) firstPoints.get(0)).getLongitude(), ((Waypoint) firstPoints.get(0)).getLatitude());
                }
                for (TrackSegment trackSegment : this.mNavigationLocationDatas) {
                    this.mNavigationDatasLength += trackSegment.getWaypoints().size();
                }
                for (int i = this.mNavigationLocationDatas.size() - 1; i >= 0; i--) {
                    List<Waypoint> waypoints = ((TrackSegment) this.mNavigationLocationDatas.get(i)).getWaypoints();
                    if (waypoints != null && !waypoints.isEmpty()) {
                        this.mLastNavigationWayPoint = (Waypoint) waypoints.get(waypoints.size() - 1);
                        break;
                    }
                }
                if (!(!Global.DEBUG_ROUTE || firstPoints == null || this.mLastNavigationWayPoint == null)) {
                    Waypoint first = (Waypoint) firstPoints.get(0);
                    double disByHand = CoordinatesHelper.getDistance(first.getLatitude(), first.getLongitude(), this.mLastNavigationWayPoint.getLatitude(), this.mLastNavigationWayPoint.getLongitude());
                    float[] result = new float[2];
                    Location.distanceBetween(first.getLatitude(), first.getLongitude(), this.mLastNavigationWayPoint.getLatitude(), this.mLastNavigationWayPoint.getLongitude(), result);
                    Debug.m5i(TAG, "dis by hand:" + disByHand + ", dis by android:(" + result[0] + "," + result[1] + "), " + Math.sqrt((double) ((result[0] * result[0]) + (result[1] * result[1]))));
                }
            }
            this.mIsInitNavigationSuccessful = true;
            Debug.m5i(TAG, "initNavigationDatasIfNeed, end,  cost time:" + (System.currentTimeMillis() - startTime));
        }
    }

    public void drawOnBitmap(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled() && this.mTempRealTimeRouteBitmap != null && !this.mTempRealTimeRouteBitmap.isRecycled()) {
            Canvas canvas = new Canvas(bitmap);
            canvas.drawColor(0, Mode.CLEAR);
            if (Global.DEBUG_ROUTE) {
                canvas.drawRect(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight(), this.mNavigationPaint);
            }
            canvas.drawBitmap(this.mScaleBitmap, 0.0f, 0.0f, this.mScalePaint);
            float angle = getAngle();
            synchronized (this.mLock) {
                if (this.mTempNavigationBitmap != null) {
                    int count = canvas.saveLayer(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight(), this.mNavigationPaint);
                    this.mMatrix.reset();
                    this.mMatrix.setRotate(-angle);
                    float tranlateX = (((float) this.mTempNavigationBitmap.getWidth()) * this.mNavigationOffsetXRadio) - ((float) (this.mTempNavigationBitmap.getWidth() / 2));
                    float tranlateY = (((float) this.mTempNavigationBitmap.getHeight()) * this.mNavigationOffsetYRadio) - ((float) (this.mTempNavigationBitmap.getHeight() / 2));
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m5i(TAG, "drawOnBitmap, tranlateX:" + (((float) (this.mWidth * 2)) * this.mNavigationOffsetXRadio) + ", " + (((float) (this.mHeight * 2)) * this.mNavigationOffsetYRadio) + "width:" + this.mWidth + ", height:" + this.mHeight);
                    }
                    this.mMatrix.preTranslate(tranlateX, tranlateY);
                    this.mMatrix.postTranslate(((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
                    canvas.concat(this.mMatrix);
                    canvas.drawBitmap(this.mTempNavigationBitmap, 0.0f, 0.0f, this.mNavigationPaint);
                    canvas.restoreToCount(count);
                } else {
                    Debug.m5i(TAG, "adjustNavigationPosition, navigation canvas is null");
                }
                canvas.save(1);
                this.mMatrix.reset();
                this.mMatrix.setRotate(-angle);
                this.mMatrix.preTranslate((float) ((-this.mTempRealTimeRouteBitmap.getWidth()) / 2), (float) ((-this.mTempRealTimeRouteBitmap.getHeight()) / 2));
                this.mMatrix.postTranslate(((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
                canvas.concat(this.mMatrix);
                canvas.drawBitmap(this.mTempRealTimeRouteBitmap, 0.0f, 0.0f, this.mRealTimePaint);
                canvas.drawBitmap(this.mOritationBitmap, 0.0f, 0.0f, this.mNavigationPaint);
                canvas.clipRect(0, 0, this.mTempRealTimeRouteBitmap.getWidth(), this.mTempRealTimeRouteBitmap.getHeight());
                canvas.restore();
            }
        }
    }

    private void reFreshDisplayPoints() {
        if (this.mPointProvider == null) {
            Debug.m5i(TAG, "reFreshDisplayPoints, point provider is null");
            return;
        }
        List<SportLocationData> allRecordLocationDatas = new ArrayList();
        this.mPointProvider.getAllGpsPoints(allRecordLocationDatas);
        Degree[] disDegrees;
        if (allRecordLocationDatas == null || allRecordLocationDatas.isEmpty()) {
            Debug.m5i(TAG, "reFreshDisplayPoints, record location is null");
            this.mRealTimeCanvas.drawColor(0, Mode.CLEAR);
            drawFlagNavigation(this.mInRangeFlagRoutePoints, getAngle());
            if (this.mLastNavigationWayPoint != null) {
                disDegrees = CoordinatesHelper.getDegreeCoordinates(new Degree(this.mCenterlng, this.mCenterlat), CoordinatesHelper.getDistance((double) ROUTE_SCALE_RANGE[getScale()], this.mDrawAreaWidth, this.mDrawAreaHeight));
                if (CoordinatesHelper.isInArea(this.mLastNavigationWayPoint.getLatitude(), this.mLastNavigationWayPoint.getLongitude(), disDegrees[0].getLantitude(), disDegrees[3].getLantitude(), disDegrees[0].getLongitude(), disDegrees[3].getLongitude())) {
                    double lngDiff = Math.abs(disDegrees[0].getLongitude() - disDegrees[3].getLongitude());
                    this.mLastShownRoutePoints = new RoutePoint((float) (((((double) this.mDrawAreaWidth) * (this.mLastNavigationWayPoint.getLongitude() - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset)), (float) (((((double) this.mDrawAreaHeight) * (disDegrees[0].getLantitude() - this.mLastNavigationWayPoint.getLatitude())) / Math.abs(disDegrees[0].getLantitude() - disDegrees[3].getLantitude())) + ((double) this.mYOffset)), -1);
                    return;
                }
                this.mLastShownRoutePoints = null;
                return;
            }
            return;
        }
        if (this.mFirstinComingPoint) {
            reCalculateNavigationRange(true);
            this.mFirstinComingPoint = false;
        }
        SportLocationData lastLocation = (SportLocationData) allRecordLocationDatas.get(allRecordLocationDatas.size() - 1);
        setCenterLocation((double) lastLocation.mLongitude, (double) lastLocation.mLatitude);
        disDegrees = CoordinatesHelper.getDegreeCoordinates(new Degree((double) lastLocation.mLongitude, (double) lastLocation.mLatitude), CoordinatesHelper.getDistance((double) ROUTE_SCALE_RANGE[getScale()], this.mDrawAreaWidth, this.mDrawAreaHeight));
        if (disDegrees == null || disDegrees.length != 4) {
            throw new IllegalArgumentException("err scale range adbout lat and lng");
        }
        Debug.m5i(TAG, "reFreshDisplayPoints, left top:(" + disDegrees[0].getLongitude() + "," + disDegrees[0].getLantitude() + ")" + "left bottom:(" + disDegrees[1].getLongitude() + "," + disDegrees[1].getLantitude() + ")" + "right top:(" + disDegrees[2].getLongitude() + "," + disDegrees[2].getLantitude() + ")" + "right bottom:(" + disDegrees[3].getLongitude() + "," + disDegrees[3].getLantitude() + ")");
        lngDiff = Math.abs(disDegrees[0].getLongitude() - disDegrees[3].getLongitude());
        double latDiff = Math.abs(disDegrees[0].getLantitude() - disDegrees[3].getLantitude());
        List<RoutePoint> routePoints = new ArrayList();
        int index = 0;
        boolean isInRange = false;
        long startTime = System.currentTimeMillis();
        Debug.m5i(TAG, "reFreshDisplayPoints, start");
        if (this.mLastNavigationWayPoint != null) {
            if (CoordinatesHelper.isInArea(this.mLastNavigationWayPoint.getLatitude(), this.mLastNavigationWayPoint.getLongitude(), disDegrees[0].getLantitude(), disDegrees[3].getLantitude(), disDegrees[0].getLongitude(), disDegrees[3].getLongitude())) {
                this.mLastShownRoutePoints = new RoutePoint((float) (((((double) this.mDrawAreaWidth) * (this.mLastNavigationWayPoint.getLongitude() - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset)), (float) (((((double) this.mDrawAreaHeight) * (disDegrees[0].getLantitude() - this.mLastNavigationWayPoint.getLatitude())) / latDiff) + ((double) this.mYOffset)), -1);
            } else {
                this.mLastShownRoutePoints = null;
            }
        }
        for (SportLocationData locationData : allRecordLocationDatas) {
            if (Global.DEBUG_ROUTE) {
                Debug.m5i(TAG, "reFreshDisplayPoints, location(" + locationData.mLongitude + "," + locationData.mLatitude + ")");
            }
            if (CoordinatesHelper.isInArea((double) locationData.mLatitude, (double) locationData.mLongitude, disDegrees[0].getLantitude(), disDegrees[3].getLantitude(), disDegrees[0].getLongitude(), disDegrees[3].getLongitude())) {
                if (Global.DEBUG_ROUTE) {
                    Debug.m5i(TAG, "reFreshDisplayPoints, location info " + index + "(" + locationData.mLongitude + "," + locationData.mLatitude + ")");
                }
                if (!isInRange && index > 0) {
                    SportLocationData lastData = (SportLocationData) allRecordLocationDatas.get(index - 1);
                    routePoints.add(new RoutePoint((float) (((((double) this.mDrawAreaWidth) * (((double) lastData.mLongitude) - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset)), (float) (((((double) this.mDrawAreaHeight) * (disDegrees[0].getLantitude() - ((double) lastData.mLatitude))) / latDiff) + ((double) this.mYOffset)), index - 1));
                }
                isInRange = true;
                routePoints.add(new RoutePoint((float) (((((double) this.mDrawAreaWidth) * Math.abs(((double) locationData.mLongitude) - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset)), (float) (((((double) this.mDrawAreaHeight) * Math.abs(((double) locationData.mLatitude) - disDegrees[0].getLantitude())) / latDiff) + ((double) this.mYOffset)), index));
            } else {
                if (isInRange && index < allRecordLocationDatas.size() - 1) {
                    SportLocationData nextData = (SportLocationData) allRecordLocationDatas.get(index + 1);
                    routePoints.add(new RoutePoint((float) (((((double) this.mDrawAreaWidth) * (((double) nextData.mLongitude) - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset)), (float) (((((double) this.mDrawAreaHeight) * (disDegrees[0].getLantitude() - ((double) nextData.mLatitude))) / latDiff) + ((double) this.mYOffset)), index + 1));
                }
                isInRange = false;
            }
            index++;
        }
        if (!(this.mNavigationFlagWayPoints == null || this.mInRangeFlagRoutePoints == null)) {
            this.mInRangeFlagRoutePoints.clear();
            for (Waypoint waypoint : this.mNavigationFlagWayPoints) {
                if (CoordinatesHelper.isInArea(waypoint.getLatitude(), waypoint.getLongitude(), disDegrees[0].getLantitude(), disDegrees[3].getLantitude(), disDegrees[0].getLongitude(), disDegrees[3].getLongitude())) {
                    float xCoordinate = (float) (((((double) this.mDrawAreaWidth) * Math.abs(waypoint.getLongitude() - disDegrees[0].getLongitude())) / lngDiff) + ((double) this.mXOffset));
                    float yCoordinate = (float) (((((double) this.mDrawAreaHeight) * Math.abs(waypoint.getLatitude() - disDegrees[0].getLantitude())) / latDiff) + ((double) this.mYOffset));
                    Debug.m5i(TAG, "reFreshDisplayPoints, flag in range （" + waypoint.getLongitude() + "," + waypoint.getLatitude() + ")");
                    FlagNavigationPoint flagNavigationPoint = new FlagNavigationPoint();
                    flagNavigationPoint.f19x = xCoordinate;
                    flagNavigationPoint.f20y = yCoordinate;
                    flagNavigationPoint.name = waypoint.getName();
                    this.mInRangeFlagRoutePoints.add(flagNavigationPoint);
                }
            }
        }
        Debug.m5i(TAG, "reFreshDisplayPoints, end,  cost time:" + (System.currentTimeMillis() - startTime));
        if (routePoints.isEmpty()) {
            this.mStartPoint = null;
        } else {
            RoutePoint lastPoint;
            if (Global.DEBUG_ROUTE) {
                for (RoutePoint point : routePoints) {
                    Debug.m5i(TAG, "reFreshDisplayPoints, location info " + point.index + "(" + point.f21x + "," + point.f22y + ")");
                }
            }
            RoutePoint firstPoint = (RoutePoint) routePoints.get(0);
            if (firstPoint.index == 0) {
                this.mStartPoint = firstPoint;
            } else {
                this.mStartPoint = null;
            }
            RoutePoint endPoint = (RoutePoint) routePoints.get(routePoints.size() - 1);
            if (routePoints.size() > 1) {
                lastPoint = (RoutePoint) routePoints.get(routePoints.size() - 2);
                if (lastPoint.index != endPoint.index - 1) {
                    lastPoint = endPoint;
                }
            } else {
                lastPoint = endPoint;
            }
            updatePenulTimatePoint(lastPoint, endPoint);
        }
        drawTrace(routePoints, this.mRealTimeCanvas, this.mRealTimePaint, true);
        copyNavigationBitmap2Temp(this.mRealTimeRouteBitmap, true);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean refreshBitmap(boolean r8) {
        /*
        r7 = this;
        r1 = 1;
        r2 = r7.mPointProvider;
        if (r2 != 0) goto L_0x0007;
    L_0x0005:
        r1 = 0;
    L_0x0006:
        return r1;
    L_0x0007:
        r7.initNavigationDatasIfNeed();
        r2 = r7.mPointProvider;
        monitor-enter(r2);
        r3 = r7.mPointProvider;	 Catch:{ all -> 0x0031 }
        r0 = r3.getUpdatePointsStatus(r8);	 Catch:{ all -> 0x0031 }
        r3 = 1;
        r4 = TAG;	 Catch:{ all -> 0x0031 }
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0031 }
        r5.<init>();	 Catch:{ all -> 0x0031 }
        r6 = "update type : ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x0031 }
        r5 = r5.append(r0);	 Catch:{ all -> 0x0031 }
        r5 = r5.toString();	 Catch:{ all -> 0x0031 }
        com.huami.watch.newsport.utils.LogUtil.m7d(r3, r4, r5);	 Catch:{ all -> 0x0031 }
        switch(r0) {
            case 1: goto L_0x0039;
            case 2: goto L_0x0034;
            case 3: goto L_0x003d;
            default: goto L_0x002f;
        };	 Catch:{ all -> 0x0031 }
    L_0x002f:
        monitor-exit(r2);	 Catch:{ all -> 0x0031 }
        goto L_0x0006;
    L_0x0031:
        r1 = move-exception;
        monitor-exit(r2);	 Catch:{ all -> 0x0031 }
        throw r1;
    L_0x0034:
        r7.reFreshDisplayPoints();	 Catch:{ all -> 0x0031 }
    L_0x0037:
        monitor-exit(r2);	 Catch:{ all -> 0x0031 }
        goto L_0x0006;
    L_0x0039:
        r7.reFreshDisplayPoints();	 Catch:{ all -> 0x0031 }
        goto L_0x0037;
    L_0x003d:
        r7.reFreshDisplayPoints();	 Catch:{ all -> 0x0031 }
        goto L_0x0037;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.route.controller.RouteTrailManager.refreshBitmap(boolean):boolean");
    }

    private void drawStartPoint(Canvas canvas, RoutePoint startPoint, Bitmap startBitmap, Paint paint) {
        if (startBitmap == null) {
            Debug.m6w(TAG, "start bitmap is null");
        } else if (startPoint == null || startBitmap.isRecycled()) {
            Debug.m6w(TAG, "start point is null or is recycled");
        } else {
            canvas.drawBitmap(startBitmap, startPoint.f21x - ((float) (startBitmap.getScaledWidth(canvas) / 2)), startPoint.f22y - ((float) (startBitmap.getScaledHeight(canvas) / 2)), paint);
        }
    }

    private void drawEndPoint(Canvas canvas, float angle, RoutePoint endPoint, Bitmap endBitmap, Paint paint, boolean isRealTime) {
        if (endBitmap == null || endBitmap.isRecycled()) {
            Debug.m6w(TAG, "end bitmap is null or is recycled");
        } else if (endPoint == null) {
            Debug.m6w(TAG, "end point is null");
        } else {
            LogUtil.m7d(Global.DEBUG_LEVEL_3, TAG, "draw angle : " + angle + ". from " + this.mPenulTimatePoint + " to " + endPoint);
            canvas.save();
            int endWidth = endBitmap.getScaledWidth(canvas);
            int endHeight = endBitmap.getScaledHeight(canvas);
            this.mMatrix.reset();
            this.mMatrix.postRotate(angle, (float) (endWidth / 2), (float) (endHeight / 2));
            this.mMatrix.postTranslate(endPoint.f21x - ((float) (endWidth / 2)), endPoint.f22y - ((float) (endHeight / 2)));
            canvas.drawBitmap(endBitmap, this.mMatrix, paint);
            canvas.restore();
        }
    }

    private void drawFlagNavigation(List<FlagNavigationPoint> flagNavigationPoints, float angle) {
        if (flagNavigationPoints != null && !flagNavigationPoints.isEmpty()) {
            for (FlagNavigationPoint flagNavigationPoint : flagNavigationPoints) {
                drawFlagContent(flagNavigationPoint.name);
                this.mOritationCanvas.save();
                this.mOritationCanvas.rotate(angle, flagNavigationPoint.f19x, flagNavigationPoint.f20y);
                this.mOritationCanvas.drawBitmap(this.mFlagIconBitmap, flagNavigationPoint.f19x - ((float) (this.mFlagIconBitmap.getWidth() / 2)), flagNavigationPoint.f20y - ((float) (this.mFlagIconBitmap.getHeight() / 2)), this.mRealTimePaint);
                this.mOritationCanvas.drawBitmap(this.mFlagBgBitmap, flagNavigationPoint.f19x, flagNavigationPoint.f20y - ((float) (this.mFlagBgBitmap.getHeight() / 2)), this.mRealTimePaint);
                this.mOritationCanvas.restore();
            }
        }
    }

    private void drawFlagContent(String name) {
        FontMetricsInt fontMetrics = this.mFlagTextPaint.getFontMetricsInt();
        this.mFlagCanvas.drawText(name, (float) (this.mFlagBgBitmap.getWidth() / 2), (float) (((this.mFlagBgBitmap.getHeight() - fontMetrics.bottom) - fontMetrics.top) / 2), this.mFlagTextPaint);
    }

    public void stop() {
        if (!(this.mRealTimeRouteBitmap == null || this.mRealTimeRouteBitmap.isRecycled())) {
            this.mRealTimeRouteBitmap.recycle();
        }
        if (!(this.mTempRealTimeRouteBitmap == null || this.mTempRealTimeRouteBitmap.isRecycled())) {
            this.mTempRealTimeRouteBitmap.recycle();
        }
        if (!(this.mStartBitmap == null || this.mStartBitmap.isRecycled())) {
            this.mStartBitmap.recycle();
        }
        if (!(this.mEndBitmap == null || this.mEndBitmap.isRecycled())) {
            this.mEndBitmap.recycle();
        }
        if (!(this.mNavigationBitmap == null || this.mNavigationBitmap.isRecycled())) {
            this.mNavigationBitmap.recycle();
        }
        if (!(this.mTempNavigationBitmap == null || this.mTempNavigationBitmap.isRecycled())) {
            this.mTempNavigationBitmap.recycle();
        }
        if (!(this.mNaviStartBitmap == null || this.mNaviStartBitmap.isRecycled())) {
            this.mNaviStartBitmap.recycle();
        }
        if (!(this.mNaviEndBitmap == null || this.mNaviEndBitmap.isRecycled())) {
            this.mNaviEndBitmap.recycle();
        }
        if (!(this.mFlagBgBitmap == null || this.mFlagBgBitmap.isRecycled())) {
            this.mFlagBgBitmap.recycle();
        }
        if (!(this.mFlagIconBitmap == null || this.mFlagIconBitmap.isRecycled())) {
            this.mFlagIconBitmap.recycle();
        }
        if (this.mNavigationLocationDatas != null) {
            this.mNavigationLocationDatas.clear();
        }
        if (!(this.mScaleBitmap == null || this.mScaleBitmap.isRecycled())) {
            this.mScaleBitmap.recycle();
        }
        if (!(this.mOritationBitmap == null || this.mOritationBitmap.isRecycled())) {
            this.mOritationBitmap.recycle();
        }
        if (this.mNavigationFlagWayPoints != null) {
            this.mNavigationFlagWayPoints.clear();
        }
        this.mNavigationFlagWayPoints = null;
        this.mNavigationLocationDatas = null;
        this.mNavigationBitmap = null;
        this.mTempNavigationBitmap = null;
        this.mRealTimeRouteBitmap = null;
        this.mTempRealTimeRouteBitmap = null;
        this.mStartBitmap = null;
        this.mEndBitmap = null;
        this.mNaviStartBitmap = null;
        this.mNaviEndBitmap = null;
        this.mFlagBgBitmap = null;
        this.mFlagIconBitmap = null;
        this.mScaleBitmap = null;
        this.mOritationBitmap = null;
        this.mIsInitNavigationSuccessful = false;
    }

    private void drawTrace(List<RoutePoint> routePoints, Canvas canvas, Paint paint, boolean realtimeRoute) {
        if (canvas == null || routePoints == null) {
            Debug.m5i(TAG, "drawTrace, drawTrace canvas is null");
            return;
        }
        canvas.drawColor(0, Mode.CLEAR);
        if (Global.DEBUG_ROUTE) {
            if (realtimeRoute) {
                canvas.drawRect(0.0f, 0.0f, (float) this.mRealTimeRouteBitmap.getWidth(), (float) this.mRealTimeRouteBitmap.getHeight(), this.mRealTimePaint);
            } else {
                canvas.drawRect(0.0f, 0.0f, (float) this.mNavigationBitmap.getWidth(), (float) this.mNavigationBitmap.getHeight(), this.mNavigationPaint);
            }
        }
        if (realtimeRoute) {
            this.mRealTimePaint.setStrokeWidth((float) ROUTE_NAVIGATION_PAINT_WIDTH[getScale()]);
        } else {
            this.mNavigationPaint.setStrokeWidth((float) ROUTE_NAVIGATION_PAINT_WIDTH[getScale()]);
        }
        long startTime = System.currentTimeMillis();
        Debug.m5i(TAG, "drawTrace, start");
        if (!routePoints.isEmpty()) {
            RoutePoint lastPoint = (RoutePoint) routePoints.get(0);
            canvas.drawPoint(lastPoint.f21x, lastPoint.f22y, paint);
            if (lastPoint.index == 0) {
                if (realtimeRoute) {
                    drawStartPoint(this.mRealTimeCanvas, lastPoint, this.mStartBitmap, this.mRealTimePaint);
                } else {
                    drawStartPoint(this.mNavigationCanvas, lastPoint, this.mNaviStartBitmap, this.mNavigationPaint);
                }
            }
            for (int i = 1; i < routePoints.size(); i++) {
                RoutePoint curPoint = (RoutePoint) routePoints.get(i);
                if (curPoint.index == lastPoint.index + 1) {
                    canvas.drawLine(lastPoint.f21x, lastPoint.f22y, curPoint.f21x, curPoint.f22y, paint);
                } else {
                    canvas.drawPoint(curPoint.f21x, curPoint.f22y, paint);
                }
                lastPoint = curPoint;
            }
            if (realtimeRoute) {
                this.mEndPoint = lastPoint;
            }
        }
        Debug.m5i(TAG, "drawTrace, end,  cost time:" + (System.currentTimeMillis() - startTime));
    }

    private void reCalculateNavigationRange(boolean forceCal) {
        if (this.mRangeDegrees == null) {
            drawTrace(loadNavigationLocation(), this.mNavigationCanvas, this.mNavigationPaint, false);
            copyNavigationBitmap2Temp(this.mNavigationBitmap, false);
            adjustNavigationPosition();
            return;
        }
        if (!isInRange(this.mRangeDegrees, CoordinatesHelper.getDegreeCoordinates(new Degree(this.mCenterlng, this.mCenterlat), CoordinatesHelper.getDistance((double) ROUTE_SCALE_RANGE[getScale()], this.mWidth, this.mHeight))) || forceCal) {
            drawTrace(loadNavigationLocation(), this.mNavigationCanvas, this.mNavigationPaint, false);
            copyNavigationBitmap2Temp(this.mNavigationBitmap, false);
            adjustNavigationPosition();
            return;
        }
        adjustNavigationPosition();
    }

    private void copyNavigationBitmap2Temp(Bitmap bitmap, boolean isRTRoute) {
        if (bitmap == null || bitmap.isRecycled()) {
            LogUtil.m9i(true, TAG, "copyBitmap2Temp failed" + bitmap);
            return;
        }
        synchronized (this.mLock) {
            if (isRTRoute) {
                this.mTempRealTimeCanvas.drawColor(0, Mode.CLEAR);
                this.mTempRealTimeCanvas.drawBitmap(bitmap, 0.0f, 0.0f, this.mRealTimePaint);
            } else {
                this.mTempNavigationCanvas.drawColor(0, Mode.CLEAR);
                this.mTempNavigationCanvas.drawBitmap(bitmap, 0.0f, 0.0f, this.mNavigationPaint);
            }
        }
    }

    private void calNavigationRange() {
        Degree[] degrees = CoordinatesHelper.getDegreeCoordinates(new Degree(this.mCenterlng, this.mCenterlat), CoordinatesHelper.getDistance((double) ROUTE_SCALE_RANGE[getScale()], this.mWidth, this.mHeight));
        if (degrees != null && degrees.length == 4) {
            this.mRangeDegrees = new Degree[4];
            this.mRangeDegrees[0] = new Degree();
            this.mRangeDegrees[0].setLongitude(degrees[0].getLongitude() - (((degrees[3].getLongitude() - degrees[0].getLongitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[0].setLantitude(degrees[0].getLantitude() - (((degrees[3].getLantitude() - degrees[0].getLantitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[1] = new Degree();
            this.mRangeDegrees[1].setLongitude(degrees[0].getLongitude() - (((degrees[3].getLongitude() - degrees[0].getLongitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[1].setLantitude(degrees[3].getLantitude() + (((degrees[3].getLantitude() - degrees[0].getLantitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[2] = new Degree();
            this.mRangeDegrees[2].setLongitude(degrees[3].getLongitude() + (((degrees[3].getLongitude() - degrees[0].getLongitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[2].setLantitude(degrees[0].getLantitude() - (((degrees[3].getLantitude() - degrees[0].getLantitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[3] = new Degree();
            this.mRangeDegrees[3].setLongitude(degrees[3].getLongitude() + (((degrees[3].getLongitude() - degrees[0].getLongitude()) * 1.0d) / 2.0d));
            this.mRangeDegrees[3].setLantitude(degrees[3].getLantitude() + (((degrees[3].getLantitude() - degrees[0].getLantitude()) * 1.0d) / 2.0d));
            Debug.m5i(TAG, "navigation degree left_top, x:" + this.mRangeDegrees[0].getLongitude() + ", y:" + this.mRangeDegrees[0].getLantitude() + "  degree right_bottom, x:" + this.mRangeDegrees[3].getLongitude() + ", y:" + this.mRangeDegrees[3].getLantitude());
        }
    }

    private List<RoutePoint> loadNavigationLocation() {
        if (this.mNavigationLocationDatas == null || this.mNavigationLocationDatas.isEmpty()) {
            return null;
        }
        calNavigationRange();
        this.mNavigationCenterlng = this.mCenterlng;
        this.mNavigationCenterlat = this.mCenterlat;
        double lngDiff = Math.abs(this.mRangeDegrees[0].getLongitude() - this.mRangeDegrees[3].getLongitude());
        double latDiff = Math.abs(this.mRangeDegrees[0].getLantitude() - this.mRangeDegrees[3].getLantitude());
        int index = 0;
        List<RoutePoint> routePoints = new ArrayList();
        long startTime = System.currentTimeMillis();
        Debug.m5i(TAG, "loadNavigationLocation, start");
        for (TrackSegment segment : this.mNavigationLocationDatas) {
            Iterator i$ = segment.getWaypoints().iterator();
            while (i$.hasNext()) {
                Waypoint waypoint = (Waypoint) i$.next();
                if (CoordinatesHelper.isInArea(waypoint.getLatitude(), waypoint.getLongitude(), this.mRangeDegrees[0].getLantitude(), this.mRangeDegrees[3].getLantitude(), this.mRangeDegrees[0].getLongitude(), this.mRangeDegrees[3].getLongitude())) {
                    if (Global.DEBUG_ROUTE) {
                        Debug.m5i(TAG, "reCalculateNavigationRange, location info " + index + "(" + waypoint.getLongitude() + "," + waypoint.getLatitude() + ")");
                    }
                    routePoints.add(new RoutePoint((float) ((((double) (this.mWidth * 2)) * Math.abs(waypoint.getLongitude() - this.mRangeDegrees[0].getLongitude())) / lngDiff), (float) ((((double) (this.mHeight * 2)) * Math.abs(waypoint.getLatitude() - this.mRangeDegrees[0].getLantitude())) / latDiff), index));
                }
                index++;
            }
            index++;
        }
        if (Global.DEBUG_ROUTE && !routePoints.isEmpty()) {
            for (RoutePoint point : routePoints) {
                Debug.m5i(TAG, "point " + point.toString());
            }
            RoutePoint first = (RoutePoint) routePoints.get(0);
            RoutePoint last = (RoutePoint) routePoints.get(routePoints.size() - 1);
            float width = Math.abs(first.f21x - last.f21x);
            float height = Math.abs(first.f22y - last.f22y);
            Debug.m5i(TAG, "first_last, distance: " + Math.sqrt((double) ((width * width) + (height * height))));
        }
        Debug.m5i(TAG, "loadNavigationLocation, end  cost time:" + (System.currentTimeMillis() - startTime));
        return routePoints;
    }

    private boolean isInRange(Degree[] navigationDegree, Degree[] realTimeDegress) {
        if (navigationDegree == null || realTimeDegress == null || navigationDegree.length != 4 || realTimeDegress.length != 4) {
            return false;
        }
        for (Degree rlDegree : realTimeDegress) {
            if (!CoordinatesHelper.isInArea(rlDegree.getLantitude(), rlDegree.getLongitude(), navigationDegree[0].getLantitude(), navigationDegree[3].getLantitude(), navigationDegree[0].getLongitude(), navigationDegree[3].getLongitude())) {
                return false;
            }
        }
        return true;
    }

    private void setCenterLocation(double lng, double lat) {
        this.mCenterlng = lng;
        this.mCenterlat = lat;
        Debug.m5i(TAG, "setCenterLocation,  navigation(" + this.mNavigationCenterlng + ", " + this.mNavigationCenterlat + ")   realtime(" + this.mCenterlng + ", " + this.mCenterlat + ")");
        reCalculateNavigationRange(false);
    }

    private void adjustNavigationPosition() {
        if (this.mRangeDegrees == null) {
            Debug.m5i(TAG, "adjustNavigationPosition, range degree is null");
            return;
        }
        double lngRange = Math.abs(this.mRangeDegrees[0].getLongitude() - this.mRangeDegrees[3].getLongitude());
        double latRange = Math.abs(this.mRangeDegrees[0].getLantitude() - this.mRangeDegrees[3].getLantitude());
        double lngOffset = this.mCenterlng - this.mNavigationCenterlng;
        double latOffset = this.mCenterlat - this.mNavigationCenterlat;
        double lngRadio = lngOffset / lngRange;
        double latRadio = latOffset / latRange;
        if (Math.abs(lngRadio) > 1.0d || Math.abs(latRadio) > 1.0d) {
            Debug.m5i(TAG, "adjustNavigationPosition, navigation radio is beyond 1");
        }
        this.mNavigationOffsetXRadio = (float) (-lngRadio);
        this.mNavigationOffsetYRadio = (float) latRadio;
        Debug.m5i(TAG, "adjustNavigationPosition, xRadio:" + this.mNavigationOffsetXRadio + ", yRadio:" + this.mNavigationOffsetYRadio + ",,, lngrange:" + lngRange + ", latRange:" + latRange + ", lngOffset:" + lngOffset + ", latOffset:" + latOffset + ", centerX:" + this.mCenterlng + ", centerlat:" + this.mCenterlat + ", na lng:" + this.mNavigationCenterlng + ", na lat:" + this.mNavigationCenterlat);
    }

    private void reScaleBitmap() {
        String title;
        this.mScaleCanvas.drawColor(0, Mode.CLEAR);
        Bitmap scaleBitmap = ((BitmapDrawable) Global.getApplicationContext().getResources().getDrawable(this.mScaleRes[this.mScale])).getBitmap();
        this.mScaleCanvas.drawBitmap(scaleBitmap, (float) ((this.mScaleBitmap.getWidth() - scaleBitmap.getWidth()) / 2), (float) ((this.mScaleBitmap.getHeight() - scaleBitmap.getHeight()) / 2), this.mScalePaint);
        this.mScaleCanvas.drawLine(291.5f, 141.0f, 291.5f, 135.0f, this.mScalePaint);
        this.mScaleCanvas.drawLine(291.5f, 141.0f, 261.5f, 141.0f, this.mScalePaint);
        this.mScaleCanvas.drawLine(261.5f, 141.0f, 261.5f, 135.0f, this.mScalePaint);
        if (ROUTE_SCALE_RANGE[this.mScale] < 1000) {
            if (UnitConvertUtils.isImperial()) {
                title = Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) ROUTE_SCALE_RANGE[this.mScale], this.mSportType)) + "ft";
            } else {
                title = ROUTE_SCALE_RANGE[this.mScale] + "m";
            }
        } else if (UnitConvertUtils.isImperial()) {
            title = String.format("%.1f", new Object[]{Double.valueOf(UnitConvertUtils.convertDistanceToMileOrKm((double) (((float) ROUTE_SCALE_RANGE[this.mScale]) / 1000.0f)))}) + "mi";
        } else if (ROUTE_SCALE_RANGE[this.mScale] % 1000 == 0) {
            title = (ROUTE_SCALE_RANGE[this.mScale] / 1000) + "km";
        } else {
            title = String.format("%.1f", new Object[]{Float.valueOf(((float) ROUTE_SCALE_RANGE[this.mScale]) / 1000.0f)}) + "km";
        }
        float length = this.mScalePaint.measureText(title);
        FontMetricsInt fontMetrics = this.mScalePaint.getFontMetricsInt();
        this.mScaleCanvas.drawText(title, 261.5f + (length / 2.0f), (float) ((int) (118.0f + (((14.0f - ((float) fontMetrics.bottom)) - ((float) fontMetrics.top)) / 2.0f))), this.mScalePaint);
    }

    public void setOritationListener(INavigationOritationListener listener) {
        this.mOritationListener = listener;
    }

    private void drawOritationBitmap() {
        drawOritationBitmap(null);
    }

    public void drawOritationBitmap(Bitmap bitmap) {
        if (this.mOritationBitmap == null || this.mOritationCanvas == null) {
            Debug.m5i(TAG, "drawOritationBitmap, bitmap or canvas is null, bitmap:" + this.mOritationBitmap + ", canvas:" + this.mOritationCanvas);
            return;
        }
        this.mOritationCanvas.drawColor(0, Mode.CLEAR);
        drawFlagNavigation(this.mInRangeFlagRoutePoints, getAngle());
        if (this.mLastShownRoutePoints != null) {
            drawEndPoint(this.mOritationCanvas, getAngle(), this.mLastShownRoutePoints, this.mNaviEndBitmap, this.mNavigationPaint, false);
        }
        if (this.mEndPoint != null) {
            float scale = ROUTE_END_POINT_SCALE[this.mScale];
            this.mMatrix.reset();
            this.mMatrix.postScale(scale, scale);
            Bitmap scaleBitmap = Bitmap.createBitmap(this.mEndBitmap, 0, 0, this.mEndBitmap.getWidth(), this.mEndBitmap.getHeight(), this.mMatrix, true);
            drawEndPoint(this.mOritationCanvas, getAngle(), this.mEndPoint, scaleBitmap, this.mRealTimePaint, true);
            if (!(scaleBitmap == null || scaleBitmap == this.mEndBitmap || scaleBitmap.isRecycled())) {
                scaleBitmap.recycle();
            }
        }
        if (bitmap != null) {
            drawOnBitmap(bitmap);
        }
    }
}
