package com.huami.watch.newsport.route.controller;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class CompassManager {
    private static boolean isRegistered = false;
    private static boolean isRegisteringSensor = false;
    private static boolean isUnRegisteringSensor = false;
    private static CompassManager sCompassManager;
    private int mAccuracy = -1;
    private float mLastDirection = 0.0f;
    private SensorEventListener mListener;
    private Sensor mOrientationSensor;
    private float[] mOrientationValues;
    private SensorManager mSensorManager;

    public interface OnSensorChangeListener {
        void onChange(float f);
    }

    private CompassManager() {
    }

    public static CompassManager getManager() {
        if (sCompassManager == null) {
            sCompassManager = new CompassManager();
        }
        return sCompassManager;
    }

    public float getRotateAngle() {
        return this.mLastDirection;
    }

    public void initSensor(Context context, final OnSensorChangeListener changeListener) {
        if (this.mSensorManager == null) {
            this.mSensorManager = (SensorManager) context.getSystemService("sensor");
            this.mOrientationSensor = this.mSensorManager.getDefaultSensor(3);
        }
        this.mOrientationValues = new float[3];
        if (this.mListener == null) {
            this.mListener = new SensorEventListener() {
                public void onSensorChanged(SensorEvent event) {
                    if (event.sensor.getType() == 3) {
                        CompassManager.this.mOrientationValues[0] = event.values[0];
                        CompassManager.this.mOrientationValues[1] = event.values[1];
                        CompassManager.this.mOrientationValues[2] = event.values[2];
                        if (Math.abs(CompassManager.this.mOrientationValues[0] - CompassManager.this.mLastDirection) >= 1.0f) {
                            if (changeListener != null) {
                                changeListener.onChange(CompassManager.this.mOrientationValues[0]);
                            }
                            CompassManager.this.mLastDirection = CompassManager.this.mOrientationValues[0];
                        }
                    }
                }

                public void onAccuracyChanged(Sensor sensor, int accuracy) {
                    CompassManager.this.mAccuracy = accuracy;
                    Log.d("CompassManager", "current accuracy is : " + accuracy);
                }
            };
        }
    }

    public void registerListener() {
        if (!isRegisteringSensor && !isRegistered) {
            isRegisteringSensor = true;
            Log.d("CompassManager", " start registerListener...");
            this.mSensorManager.registerListener(this.mListener, this.mOrientationSensor, 1);
            isRegisteringSensor = false;
            isRegistered = true;
        }
    }

    public void unRegisterListener() {
        if (!isUnRegisteringSensor && isRegistered) {
            isUnRegisteringSensor = true;
            if (!(this.mSensorManager == null || this.mListener == null)) {
                Log.d("CompassManager", "start UNregisterListener...");
                this.mSensorManager.unregisterListener(this.mListener);
            }
            isUnRegisteringSensor = false;
            isRegistered = false;
        }
    }

    public void destroy() {
        unRegisterListener();
        this.mListener = null;
    }
}
