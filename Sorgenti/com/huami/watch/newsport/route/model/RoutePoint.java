package com.huami.watch.newsport.route.model;

public class RoutePoint {
    public int index;
    public float f21x;
    public float f22y;

    public RoutePoint(float x, float y, int index) {
        this.f21x = x;
        this.f22y = y;
        this.index = index;
    }

    public String toString() {
        return "RoutePoint{x=" + this.f21x + ", y=" + this.f22y + ", index=" + this.index + '}';
    }
}
