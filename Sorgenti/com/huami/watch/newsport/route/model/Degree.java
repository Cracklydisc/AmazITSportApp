package com.huami.watch.newsport.route.model;

public class Degree {
    private double lantitude;
    private double longitude;

    public Degree(double longitude, double lantitude) {
        this.longitude = longitude;
        this.lantitude = lantitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLantitude() {
        return this.lantitude;
    }

    public void setLantitude(double lantitude) {
        this.lantitude = lantitude;
    }

    public String toString() {
        return "Degree{longitude=" + this.longitude + ", lantitude=" + this.lantitude + '}';
    }
}
