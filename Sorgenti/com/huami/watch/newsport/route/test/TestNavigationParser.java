package com.huami.watch.newsport.route.test;

import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.utils.LogUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestNavigationParser {
    private static final String TAG = TestNavigationParser.class.getName();
    private static TestNavigationParser sInstance = null;
    private final String DATA_PATH = (SportApplication.getInstance().getFilesDir() + "/loc.txt");
    private int INIT_OFFSET = 0;
    private int STEP = 1;
    private int mIndex = 0;
    private List<TrackSegment> mRouteCoordinates = new ArrayList();

    public static TestNavigationParser getInstance() {
        if (sInstance == null) {
            sInstance = new TestNavigationParser();
        }
        return sInstance;
    }

    private TestNavigationParser() {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.hs.gpxparser.modal.TrackSegment> getNavigationLocationData() {
        /*
        r14 = this;
        r0 = java.lang.System.currentTimeMillis();
        r8 = TAG;
        r9 = "getNavigationLocationData, start";
        com.huami.watch.common.log.Debug.m5i(r8, r9);
        r6 = new com.hs.gpxparser.modal.TrackSegment;
        r6.<init>();
        r7 = new java.util.ArrayList;
        r7.<init>();
        r4 = new java.io.BufferedReader;	 Catch:{ FileNotFoundException -> 0x008f }
        r8 = new java.io.InputStreamReader;	 Catch:{ FileNotFoundException -> 0x008f }
        r9 = new java.io.FileInputStream;	 Catch:{ FileNotFoundException -> 0x008f }
        r10 = new java.io.File;	 Catch:{ FileNotFoundException -> 0x008f }
        r11 = r14.DATA_PATH;	 Catch:{ FileNotFoundException -> 0x008f }
        r10.<init>(r11);	 Catch:{ FileNotFoundException -> 0x008f }
        r9.<init>(r10);	 Catch:{ FileNotFoundException -> 0x008f }
        r8.<init>(r9);	 Catch:{ FileNotFoundException -> 0x008f }
        r4.<init>(r8);	 Catch:{ FileNotFoundException -> 0x008f }
        r14.parseHead(r4);	 Catch:{ FileNotFoundException -> 0x008f }
        r8 = 0;
        r14.mIndex = r8;	 Catch:{ IOException -> 0x0046 }
        r8 = r14.mRouteCoordinates;	 Catch:{ IOException -> 0x0046 }
        r8.clear();	 Catch:{ IOException -> 0x0046 }
    L_0x0036:
        r3 = r4.readLine();	 Catch:{ IOException -> 0x0046 }
        if (r3 == 0) goto L_0x0086;
    L_0x003c:
        r5 = r14.parse(r3);	 Catch:{ IOException -> 0x0046 }
        if (r5 == 0) goto L_0x0036;
    L_0x0042:
        r7.add(r5);	 Catch:{ IOException -> 0x0046 }
        goto L_0x0036;
    L_0x0046:
        r2 = move-exception;
        r2.printStackTrace();	 Catch:{ all -> 0x0099 }
        r4.close();	 Catch:{ IOException -> 0x0094 }
    L_0x004d:
        r6.setWaypoints(r7);
        r8 = r14.mRouteCoordinates;
        r8.add(r6);
        r8 = TAG;
        r9 = new java.lang.StringBuilder;
        r9.<init>();
        r10 = "getNavigationLocationData, end size:";
        r9 = r9.append(r10);
        r10 = r7.size();
        r9 = r9.append(r10);
        r10 = ", time cost:";
        r9 = r9.append(r10);
        r10 = java.lang.System.currentTimeMillis();
        r10 = r10 - r0;
        r12 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r10 = r10 / r12;
        r9 = r9.append(r10);
        r9 = r9.toString();
        com.huami.watch.common.log.Debug.m5i(r8, r9);
        r8 = r14.mRouteCoordinates;
        return r8;
    L_0x0086:
        r4.close();	 Catch:{ IOException -> 0x008a }
        goto L_0x004d;
    L_0x008a:
        r2 = move-exception;
        r2.printStackTrace();	 Catch:{ FileNotFoundException -> 0x008f }
        goto L_0x004d;
    L_0x008f:
        r2 = move-exception;
        r2.printStackTrace();
        goto L_0x004d;
    L_0x0094:
        r2 = move-exception;
        r2.printStackTrace();	 Catch:{ FileNotFoundException -> 0x008f }
        goto L_0x004d;
    L_0x0099:
        r8 = move-exception;
        r4.close();	 Catch:{ IOException -> 0x009e }
    L_0x009d:
        throw r8;	 Catch:{ FileNotFoundException -> 0x008f }
    L_0x009e:
        r2 = move-exception;
        r2.printStackTrace();	 Catch:{ FileNotFoundException -> 0x008f }
        goto L_0x009d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.route.test.TestNavigationParser.getNavigationLocationData():java.util.List<com.hs.gpxparser.modal.TrackSegment>");
    }

    private void parseHead(BufferedReader reader) {
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null && !line.isEmpty()) {
                    String[] strs = line.split("=");
                    if (strs.length == 2) {
                        if ("offset".equals(strs[0].trim())) {
                            this.INIT_OFFSET = Integer.parseInt(strs[1].trim());
                        } else if ("step".equals(strs[0].trim())) {
                            this.STEP = Integer.parseInt(strs[1].trim());
                            return;
                        }
                    }
                } else {
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private Waypoint parse(String line) {
        String[] strs = line.split("\\|");
        if (strs.length < 10) {
            LogUtil.m9i(true, TAG, "invalid format : " + line);
            return null;
        }
        int pointIndex = Integer.parseInt(strs[0]);
        int algoType = Integer.parseInt(strs[1]);
        float course = Float.parseFloat(strs[2]);
        long timeStamps = Long.parseLong(strs[3]);
        float latitude = Float.parseFloat(strs[4]);
        float longtitude = Float.parseFloat(strs[5]);
        float speed = Float.parseFloat(strs[6]);
        float altitude = Float.parseFloat(strs[7]);
        float accuracy = Float.parseFloat(strs[8]);
        int bar = Integer.parseInt(strs[9]);
        return new Waypoint((double) latitude, (double) longtitude);
    }
}
