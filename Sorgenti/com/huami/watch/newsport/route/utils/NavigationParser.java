package com.huami.watch.newsport.route.utils;

import android.util.Log;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Route;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.hs.gpxparser.utils.SDCardGPSUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class NavigationParser {
    private static final String TAG = NavigationParser.class.getName();

    public static List<TrackSegment> getTrackInfo(List<Waypoint> flagWayPoints, int mSportType) {
        long startTime = System.currentTimeMillis();
        Debug.m5i(TAG, "getTrackInfo, start");
        String[] gpxRouteInfo = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(Global.getApplicationContext(), mSportType);
        if (gpxRouteInfo == null || gpxRouteInfo.length < 2) {
            Debug.m5i(TAG, "getTrackInfo, gpx route is null or length less than 2: " + gpxRouteInfo);
            return null;
        }
        Debug.m5i(TAG, "route name:" + gpxRouteInfo[0] + ", file name:" + gpxRouteInfo[1]);
        if (gpxRouteInfo[0] == null || gpxRouteInfo[1] == null) {
            return null;
        }
        GPX gpxDatas = SDCardGPSUtils.getWayPointsFromFileName(gpxRouteInfo[1]);
        if (gpxDatas == null) {
            Debug.m5i(TAG, "getTrackInfo, gpx data is null");
            return null;
        }
        if (flagWayPoints == null) {
            flagWayPoints = new ArrayList();
        }
        flagWayPoints.clear();
        flagWayPoints.addAll(gpxDatas.getWaypoints());
        HashSet<Track> trackHashSet = gpxDatas.getTracks();
        List<TrackSegment> segmentList = new ArrayList();
        if (trackHashSet == null || trackHashSet.isEmpty()) {
            Log.i(TAG, " hasNotTackData and has RouteData ");
            if (gpxDatas != null && gpxDatas.getRoutes().size() > 0) {
                HashSet<Route> routeHashSet = gpxDatas.getRoutes();
                if (routeHashSet != null && routeHashSet.size() > 0) {
                    Route route = (Route) routeHashSet.iterator().next();
                    if (route != null) {
                        TrackSegment trackSegment = new TrackSegment();
                        ArrayList<Waypoint> waypointList = route.getRoutePoints();
                        trackSegment.setWaypoints(waypointList);
                        Log.i(TAG, " NavigationParse WayPaintSize:" + waypointList.size());
                        segmentList.add(trackSegment);
                    }
                }
            }
        } else {
            Iterator<Track> trackIterator = trackHashSet.iterator();
            while (trackIterator.hasNext()) {
                segmentList.addAll(((Track) trackIterator.next()).getTrackSegments());
            }
        }
        int size = 0;
        for (TrackSegment segment : segmentList) {
            List<Waypoint> waypoints = segment.getWaypoints();
            if (segment != null) {
                size += waypoints.size();
            }
        }
        Debug.m5i(TAG, "getTrackInfo, end, size:" + size + ", cost time:" + (System.currentTimeMillis() - startTime));
        return segmentList;
    }
}
