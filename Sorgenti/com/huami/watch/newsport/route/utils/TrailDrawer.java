package com.huami.watch.newsport.route.utils;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TrailDrawer {
    private static final float[] ROUTE_END_POINT_SCALE = new float[]{1.0f, 0.9f, 0.8f, 0.7f};
    private static final int[] ROUTE_NAVIGATION_PAINT_WIDTH = new int[]{9, 7, 5, 5};
    private static final int[] ROUTE_SCALE_RANGE = new int[]{50, 200, 2000, 5000};
    private static final int[] ROUTE_SCALE_RES = new int[]{C0532R.drawable.sport_map_bg1, C0532R.drawable.sport_map_bg2, C0532R.drawable.sport_map_bg3, C0532R.drawable.sport_map_bg4};
    private Config mBitmapConfig = Config.ARGB_8888;
    private boolean mColorfulPath = false;
    private Context mContext = null;
    private int mDefaultLineColor = -65536;
    private float mDistancePerLat = -1.0f;
    private float mDistancePerLng = -1.0f;
    private int mDrawAreaHeight = 166;
    private int mDrawAreaWidth = 166;
    private float mDrawPathHeight = 0.0f;
    private float mDrawPathWidth = 0.0f;
    private Bitmap mEndBitmap = null;
    private List<Bitmap> mEndFlagBitmapList = new ArrayList();
    ArgbEvaluator mEvaluator = new ArgbEvaluator();
    private Point mFirstPoint = null;
    private int mHeight = 0;
    private float mHeightLatRatio = 0.0f;
    private Point mLastPoint = null;
    private List<TrailLocations> mLocationDatas = new LinkedList();
    private Matrix mMatrix = null;
    private float mMaxLat = -1800.0f;
    private float mMaxLng = -1800.0f;
    private int mMaxSpeed = -1;
    private float mMinLat = 1800.0f;
    private float mMinLng = 1800.0f;
    private int mMinSpeed = Integer.MAX_VALUE;
    private float mPathLatDistance = -1.0f;
    private float mPathLngDistance = -1.0f;
    private Paint mPausedTrailPaint = null;
    private Point mPreLastPoint = null;
    private float mRatio = -1.0f;
    private Bitmap mScaleBitmap;
    private Canvas mScaleCanvas;
    private Paint mScalePaint;
    private float mSpeedSpan = 0.0f;
    private Paint mSportTrailPaint = null;
    private Bitmap mStartBitmap = null;
    private Bitmap mTrailBitmap = null;
    private List<TrailPath> mTrailPathList = new ArrayList();
    private Bitmap mWholeTrailBitmap = null;
    private Canvas mWholeTrailCanvas = null;
    private Paint mWholeTrailPaint = null;
    private int mWidth = 0;
    private float mWidthLngRatio = 0.0f;
    private float mXOffset = 0.0f;
    private float mYOffset = 0.0f;

    public static final class Point {
        public final int mColor;
        public final float mX;
        public final float mY;

        public Point(float x, float y, int color) {
            this.mX = x;
            this.mY = y;
            this.mColor = color;
        }

        public String toString() {
            return "Point{mX=" + this.mX + ", mY=" + this.mY + ", mColor=" + this.mColor + '}';
        }
    }

    public static final class TrailPath {
        public int[] mColor;
        public Point mEndPoint;
        public Point mStartPoint;
        public Path mTrailPth;

        public TrailPath(Path trailPath, int[] color) {
            this.mTrailPth = trailPath;
            this.mColor = color;
        }

        public String toString() {
            return "TrailPath{mTrailPth=" + this.mTrailPth + ", mColors=" + this.mColor + '}';
        }
    }

    public TrailDrawer(Context context, int bitmapWidth, int bitmapHeight, boolean colorfulPath) {
        this.mWidth = bitmapWidth;
        this.mHeight = bitmapHeight;
        this.mDrawAreaWidth = this.mWidth - 30;
        this.mDrawAreaHeight = (this.mHeight - 30) - 30;
        this.mColorfulPath = colorfulPath;
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        this.mSportTrailPaint = new Paint(5);
        this.mSportTrailPaint.setColor(-65536);
        this.mSportTrailPaint.setStyle(Style.FILL_AND_STROKE);
        this.mSportTrailPaint.setStrokeWidth(5.0f);
        this.mSportTrailPaint.setStrokeCap(Cap.ROUND);
        this.mPausedTrailPaint = new Paint(5);
        this.mPausedTrailPaint.setPathEffect(new DashPathEffect(new float[]{1.0f, 2.0f, 3.0f, 4.0f}, 1.0f));
        this.mPausedTrailPaint.setColor(-1442840576);
        this.mPausedTrailPaint.setStrokeWidth(4.0f);
        this.mWholeTrailPaint = new Paint(5);
        this.mMatrix = new Matrix();
        this.mFirstPoint = null;
        this.mLastPoint = null;
        this.mPreLastPoint = null;
        this.mScaleBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, Config.ARGB_8888);
        this.mScalePaint = new Paint(1);
        this.mScalePaint.setColor(-1);
        this.mScalePaint.setStyle(Style.FILL);
        this.mScalePaint.setStrokeWidth(2.0f);
        this.mScalePaint.setTextSize(25.0f);
        this.mScalePaint.setTextAlign(Align.CENTER);
        this.mScaleCanvas = new Canvas(this.mScaleBitmap);
    }

    private void initBitmapIfNotExist() {
        if (this.mWidth <= 0 || this.mHeight <= 0) {
            throw new IllegalArgumentException("width or height should not less than zero. width:" + this.mWidth + ", height:" + this.mHeight);
        }
        synchronized (this) {
            if (this.mTrailBitmap == null) {
                this.mTrailBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapConfig);
            }
            if (this.mWholeTrailBitmap == null) {
                this.mWholeTrailBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapConfig);
                this.mWholeTrailCanvas = new Canvas(this.mWholeTrailBitmap);
            }
        }
    }

    public synchronized void clearState() {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "clear state");
        }
        this.mMinLng = 1800.0f;
        this.mMaxLng = -1800.0f;
        this.mMinLat = 1800.0f;
        this.mMaxLat = -1800.0f;
        this.mLastPoint = null;
        this.mPreLastPoint = null;
        this.mFirstPoint = null;
        this.mPathLngDistance = -1.0f;
        this.mPathLatDistance = -1.0f;
        this.mRatio = -1.0f;
        this.mLocationDatas.clear();
    }

    public synchronized void addLocationDatasToBitmapOfZhufeng(List<TrailLocations> trailLocationsList) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "add line data " + trailLocationsList);
        }
        initBitmapIfNotExist();
        if (!(trailLocationsList == null || trailLocationsList.isEmpty())) {
            this.mLocationDatas.addAll(trailLocationsList);
            List<SportLocationData> locationDatas = new ArrayList();
            for (TrailLocations trailLocations : trailLocationsList) {
                locationDatas.addAll(trailLocations.locationDatas);
            }
            boolean checkResult = checkAndUpdateMaxMinLatLngOfZhufeng(this.mLocationDatas);
            initDraw(locationDatas);
            Point startPoint;
            int i;
            Point endPoint;
            if (checkResult) {
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "change ratio");
                }
                this.mTrailPathList = convertToPointOfZhufeng(this.mLocationDatas);
                if (this.mTrailPathList != null) {
                    for (TrailPath trailPath : this.mTrailPathList) {
                        startPoint = trailPath.mStartPoint;
                        if (startPoint != null) {
                            this.mFirstPoint = startPoint;
                            break;
                        }
                    }
                    for (i = this.mTrailPathList.size() - 1; i >= 0; i--) {
                        endPoint = ((TrailPath) this.mTrailPathList.get(i)).mEndPoint;
                        if (endPoint != null) {
                            this.mLastPoint = endPoint;
                            break;
                        }
                    }
                }
            } else {
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "keep ratio " + this.mRatio);
                }
                this.mTrailPathList = convertToPointOfZhufeng(this.mLocationDatas);
                if (this.mTrailPathList != null) {
                    for (TrailPath trailPath2 : this.mTrailPathList) {
                        startPoint = trailPath2.mStartPoint;
                        if (startPoint != null) {
                            this.mFirstPoint = startPoint;
                        }
                    }
                    for (i = this.mTrailPathList.size() - 1; i >= 0; i--) {
                        endPoint = ((TrailPath) this.mTrailPathList.get(i)).mEndPoint;
                        if (endPoint != null) {
                            this.mLastPoint = endPoint;
                        }
                    }
                }
            }
            synchronized (this) {
                if (this.mTrailBitmap != null) {
                    drawOnBitmap(this.mTrailPathList, this.mTrailBitmap);
                }
            }
        }
    }

    public float getRate() {
        return this.mRatio;
    }

    private void drawOnBitmap(List<TrailPath> trailPaths, Bitmap bitmap) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "draw on bitmap|points:" + trailPaths);
        }
        if (this.mDrawAreaWidth > 0 && this.mDrawAreaHeight > 0) {
            Canvas canvas = new Canvas(bitmap);
            canvas.drawColor(0, Mode.CLEAR);
            for (int i = 0; i < trailPaths.size(); i++) {
                TrailPath tp = (TrailPath) trailPaths.get(i);
                Point nextStartPoint = tp.mStartPoint;
                this.mSportTrailPaint.setStrokeCap(Cap.ROUND);
                this.mSportTrailPaint.setStyle(Style.STROKE);
                this.mSportTrailPaint.setStrokeWidth(6.0f);
                this.mSportTrailPaint.setColor(tp.mColor[1]);
                canvas.drawPath(tp.mTrailPth, this.mSportTrailPaint);
                this.mSportTrailPaint.setStyle(Style.STROKE);
                this.mSportTrailPaint.setStrokeWidth(4.0f);
                this.mSportTrailPaint.setColor(tp.mColor[0]);
                canvas.drawPath(tp.mTrailPth, this.mSportTrailPaint);
                Bitmap endBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), tp.mColor[2]);
                if (tp.mStartPoint != null) {
                    canvas.drawBitmap(this.mStartBitmap, tp.mStartPoint.mX - ((float) (this.mStartBitmap.getScaledWidth(this.mWholeTrailCanvas) / 2)), tp.mStartPoint.mY - ((float) (this.mStartBitmap.getScaledHeight(this.mWholeTrailCanvas) / 2)), this.mSportTrailPaint);
                }
                if (tp.mEndPoint != null) {
                    canvas.drawBitmap(endBitmap, tp.mEndPoint.mX - ((float) (endBitmap.getScaledWidth(this.mWholeTrailCanvas) / 2)), tp.mEndPoint.mY - ((float) (endBitmap.getScaledHeight(this.mWholeTrailCanvas) / 2)), this.mSportTrailPaint);
                }
                this.mEndFlagBitmapList.add(endBitmap);
                Point lastPoint = tp.mEndPoint;
            }
        }
    }

    public Point getFirstPoint() {
        return this.mFirstPoint;
    }

    public Point getLastPoint() {
        return this.mLastPoint;
    }

    public void startDraw() {
        clearState();
    }

    public synchronized void finishDraw() {
        Debug.m5i("TrailDrawer", "finishDraw");
        if (this.mTrailBitmap != null) {
            this.mTrailBitmap.recycle();
            this.mTrailBitmap = null;
        }
        if (this.mWholeTrailBitmap != null) {
            this.mWholeTrailBitmap.recycle();
            this.mWholeTrailBitmap = null;
            this.mWholeTrailCanvas = null;
        }
        if (this.mStartBitmap != null) {
            this.mStartBitmap.recycle();
            this.mStartBitmap = null;
        }
        if (this.mEndBitmap != null) {
            this.mEndBitmap.recycle();
            this.mEndBitmap = null;
        }
        if (this.mEndFlagBitmapList != null) {
            for (Bitmap bitmap : this.mEndFlagBitmapList) {
                if (!(bitmap == null || bitmap.isRecycled())) {
                    bitmap.recycle();
                }
            }
        }
        clearState();
    }

    public void setStart(int startRes) {
        this.mStartBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), startRes);
    }

    public void setEnd(int endRes) {
        this.mEndBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), endRes);
    }

    public synchronized Bitmap newTrailBitmap() {
        Bitmap bitmap;
        initBitmapIfNotExist();
        this.mWholeTrailCanvas.drawColor(0, Mode.CLEAR);
        this.mWholeTrailCanvas.drawBitmap(this.mTrailBitmap, 0.0f, 0.0f, this.mWholeTrailPaint);
        Point firstPoint = getFirstPoint();
        Point lastPoint = getLastPoint();
        if (firstPoint == null || firstPoint == lastPoint || this.mTrailBitmap == null || Float.isInfinite(this.mRatio)) {
            if (this.mEndBitmap != null) {
                this.mWholeTrailCanvas.drawBitmap(this.mEndBitmap, (float) ((this.mWidth - this.mEndBitmap.getScaledWidth(this.mWholeTrailCanvas)) / 2), (float) ((this.mHeight - this.mEndBitmap.getScaledHeight(this.mWholeTrailCanvas)) / 2), this.mWholeTrailPaint);
            }
            bitmap = this.mWholeTrailBitmap;
        } else {
            if (this.mEndBitmap != null) {
                this.mWholeTrailCanvas.drawBitmap(this.mEndBitmap, this.mLastPoint.mX - ((float) (this.mEndBitmap.getScaledWidth(this.mWholeTrailCanvas) / 2)), this.mLastPoint.mY - ((float) (this.mEndBitmap.getScaledHeight(this.mWholeTrailCanvas) / 2)), this.mWholeTrailPaint);
            }
            Bitmap scaleBitmap = ((BitmapDrawable) Global.getApplicationContext().getResources().getDrawable(ROUTE_SCALE_RES[(int) getRate()])).getBitmap();
            this.mWholeTrailCanvas.drawBitmap(scaleBitmap, (float) ((this.mScaleBitmap.getWidth() - scaleBitmap.getWidth()) / 2), (float) ((this.mScaleBitmap.getHeight() - scaleBitmap.getHeight()) / 2), this.mScalePaint);
            bitmap = this.mWholeTrailBitmap;
        }
        return bitmap;
    }

    private void initDistancePerLatLng(float lat, float lng) {
        if (this.mDistancePerLat < 0.0f || this.mDistancePerLng < 0.0f) {
            float[] w = new float[1];
            Location.distanceBetween((double) lat, (double) lng, (double) (lat + 1.0f), (double) lng, w);
            this.mDistancePerLat = w[0];
            Location.distanceBetween((double) lat, (double) lng, (double) lat, (double) (lng + 1.0f), w);
            this.mDistancePerLng = w[0];
        }
    }

    private void initDraw(List<SportLocationData> latLngs) {
        if (latLngs != null && !latLngs.isEmpty() && this.mWidth > 0 && this.mHeight > 0) {
            initDistancePerLatLng(((SportLocationData) latLngs.get(0)).mLatitude, ((SportLocationData) latLngs.get(0)).mLongitude);
            this.mPathLngDistance = this.mDistancePerLng * (this.mMaxLng - this.mMinLng);
            this.mPathLatDistance = this.mDistancePerLat * (this.mMaxLat - this.mMinLat);
            if (Math.max(this.mPathLngDistance, this.mPathLatDistance) < ((float) Math.min(this.mDrawAreaWidth, this.mDrawAreaHeight)) / 2.0f) {
                this.mRatio = 2.0f;
            } else if (this.mPathLatDistance != 0.0f && this.mPathLngDistance == 0.0f) {
                this.mRatio = ((float) this.mDrawAreaHeight) / this.mPathLatDistance;
            } else if (this.mPathLatDistance == 0.0f && this.mPathLngDistance != 0.0f) {
                this.mRatio = ((float) this.mDrawAreaWidth) / this.mPathLngDistance;
            } else if (this.mPathLngDistance / this.mPathLatDistance > ((float) this.mDrawAreaWidth) / ((float) this.mDrawAreaHeight)) {
                this.mRatio = ((float) this.mDrawAreaWidth) / this.mPathLngDistance;
            } else {
                this.mRatio = ((float) this.mDrawAreaHeight) / this.mPathLatDistance;
            }
            this.mWidthLngRatio = this.mDistancePerLng * this.mRatio;
            this.mHeightLatRatio = this.mDistancePerLat * this.mRatio;
            this.mDrawPathWidth = (this.mMaxLng - this.mMinLng) * this.mWidthLngRatio;
            this.mDrawPathHeight = (this.mMaxLat - this.mMinLat) * this.mHeightLatRatio;
            this.mXOffset = (((float) this.mWidth) - this.mDrawPathWidth) / 2.0f;
            this.mYOffset = (((float) this.mHeight) - this.mDrawPathHeight) / 2.0f;
        }
    }

    private List<TrailPath> convertToPointOfZhufeng(TrailLocations trailLocation) {
        if (trailLocation == null || trailLocation.locationDatas == null) {
            return null;
        }
        List<TrailPath> trailPathList = new ArrayList();
        Path path = new Path();
        boolean isFirst = true;
        float x = -1.0f;
        float y = -1.0f;
        Point startPoint = null;
        for (SportLocationData point : trailLocation.locationDatas) {
            x = ((point.mLongitude - this.mMinLng) * this.mWidthLngRatio) + this.mXOffset;
            y = ((this.mMaxLat - point.mLatitude) * this.mHeightLatRatio) + this.mYOffset;
            if (isFirst) {
                startPoint = new Point(x, y, this.mDefaultLineColor);
                path.moveTo(x, y);
                isFirst = false;
            } else if ((point.mPointType & 2) != 0) {
                trailPathList.add(new TrailPath(path, trailLocation.mColors));
                path = new Path();
                path.moveTo(x, y);
            } else {
                path.lineTo(x, y);
            }
        }
        TrailPath trailPath = new TrailPath(path, trailLocation.mColors);
        trailPath.mStartPoint = startPoint;
        trailPath.mEndPoint = new Point(x, y, -65536);
        trailPathList.add(trailPath);
        return trailPathList;
    }

    private List<TrailPath> convertToPointOfZhufeng(List<TrailLocations> trailLocationsList) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "convert to point|points:" + trailLocationsList + "|distance/lng:" + this.mDistancePerLng + "|distance/lat:" + this.mDistancePerLat + "|mRatio:" + this.mRatio);
        }
        if (trailLocationsList == null || trailLocationsList.isEmpty()) {
            return null;
        }
        List<TrailPath> trailPathList = new ArrayList();
        for (TrailLocations trailLocation : trailLocationsList) {
            List<TrailPath> childPaths = convertToPointOfZhufeng(trailLocation);
            if (childPaths != null) {
                trailPathList.addAll(childPaths);
            }
        }
        return trailPathList;
    }

    private boolean checkAndUpdateMaxMinLatLngOfZhufeng(List<TrailLocations> trailLocationsList) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "check and update " + trailLocationsList);
        }
        boolean result = false;
        for (TrailLocations trailLocations : trailLocationsList) {
            List<? extends SportLocationData> locationDatas = trailLocations.locationDatas;
            if (!(locationDatas == null || locationDatas.isEmpty())) {
                for (SportLocationData locationData : locationDatas) {
                    if (this.mMinLat > locationData.mLatitude) {
                        this.mMinLat = locationData.mLatitude;
                        result = true;
                    }
                    if (this.mMinLng > locationData.mLongitude) {
                        this.mMinLng = locationData.mLongitude;
                        result = true;
                    }
                    if (this.mMaxLat < locationData.mLatitude) {
                        this.mMaxLat = locationData.mLatitude;
                        result = true;
                    }
                    if (this.mMaxLng < locationData.mLongitude) {
                        this.mMaxLng = locationData.mLongitude;
                        result = true;
                    }
                    if (((float) this.mMinSpeed) > locationData.mSpeed) {
                        this.mMinSpeed = (int) SportDataFilterUtils.parseSpeed(locationData.mSpeed);
                    }
                    if (((float) this.mMaxSpeed) < locationData.mSpeed) {
                        this.mMaxSpeed = (int) SportDataFilterUtils.parseSpeed(locationData.mSpeed);
                    }
                }
            }
        }
        this.mSpeedSpan = (float) (this.mMaxSpeed - this.mMinSpeed);
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "check result " + result);
        }
        return result;
    }
}
