package com.huami.watch.newsport.route.utils;

import android.location.Location;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.route.model.Degree;
import com.huami.watch.newsport.utils.NumeriConversionUtils;

public class CoordinatesHelper {
    private static final String TAG = CoordinatesHelper.class.getName();

    private static double radians(double d) {
        return (3.141592653589793d * d) / 180.0d;
    }

    private static double degrees(double d) {
        return 57.29577951308232d * d;
    }

    public static double getDistance(Degree Degree1, Degree Degree2) {
        double radLat1 = radians(Degree1.getLantitude());
        double radLat2 = radians(Degree2.getLantitude());
        return (double) (Math.round(10000.0d * ((2.0d * Math.asin(Math.sqrt(Math.pow(Math.sin((radLat1 - radLat2) / 2.0d), 2.0d) + ((Math.cos(radLat1) * Math.cos(radLat2)) * Math.pow(Math.sin((radians(Degree1.getLongitude()) - radians(Degree2.getLongitude())) / 2.0d), 2.0d))))) * 6378137.0d)) / 10000);
    }

    public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
        double radLat1 = radians(lat1);
        double radLat2 = radians(lat2);
        return (double) (Math.round(10000.0d * ((2.0d * Math.asin(Math.sqrt(Math.pow(Math.sin((radLat1 - radLat2) / 2.0d), 2.0d) + ((Math.cos(radLat1) * Math.cos(radLat2)) * Math.pow(Math.sin(radians(lng1 - lng2) / 2.0d), 2.0d))))) * 6378137.0d)) / 10000);
    }

    public static Degree[] getDegreeCoordinates(Degree Degree1, double distance) {
        double dlng = 0.0d;
        if (!(Double.isInfinite(Degree1.getLantitude()) || Double.isInfinite(Degree1.getLongitude()) || Double.isNaN(Degree1.getLantitude()) || Double.isNaN(Degree1.getLongitude()))) {
            dlng = 2.0d * Math.asin(Math.sin(distance / 1.2756274E7d) / Math.cos((Degree1.getLantitude() * 3.141592653589793d) / 180.0d));
        }
        dlng = degrees(dlng);
        double dlat = degrees(distance / 6378137.0d);
        Debug.m5i(TAG, "getDegreeCoordinates, degree:" + Degree1.toString() + ", dis:" + distance + ",  dlng:" + dlng + ", dlat:" + dlat);
        double firstLng = NumeriConversionUtils.getDoubleValue(String.valueOf(Degree1.getLongitude() + dlng), 6);
        double secondLng = NumeriConversionUtils.getDoubleValue(String.valueOf(Degree1.getLongitude() - dlng), 6);
        double firstLat = NumeriConversionUtils.getDoubleValue(String.valueOf(Degree1.getLantitude() + dlat), 6);
        double secondLat = NumeriConversionUtils.getDoubleValue(String.valueOf(Degree1.getLantitude() - dlat), 6);
        double minLng = Math.min(firstLng, secondLng);
        double maxLng = Math.max(firstLng, secondLng);
        double minLat = Math.min(firstLat, secondLat);
        double maxLat = Math.max(firstLat, secondLat);
        degrees = new Degree[4];
        degrees[0].setLongitude(minLng);
        degrees[0].setLantitude(maxLat);
        degrees[1] = new Degree();
        degrees[1].setLongitude(minLng);
        degrees[1].setLantitude(minLat);
        degrees[2] = new Degree();
        degrees[2].setLongitude(maxLng);
        degrees[2].setLantitude(maxLat);
        degrees[3] = new Degree();
        degrees[3].setLongitude(maxLng);
        degrees[3].setLantitude(minLat);
        if (Global.DEBUG_ROUTE) {
            float[] results = new float[2];
            Location.distanceBetween(minLat, minLng, minLat, maxLng, results);
            Debug.m5i(TAG, "GetDegreeCoordinates, distance lng range: (" + results[0] + "," + results[1] + ")" + ", " + getDistance(degrees[1], degrees[3]));
            Location.distanceBetween(minLat, minLng, maxLat, minLng, results);
            Debug.m5i(TAG, "GetDegreeCoordinates, distance lat range: (" + results[0] + "," + results[1] + ")" + ", " + getDistance(degrees[0], degrees[1]));
        }
        return degrees;
    }

    public static boolean isInArea(double latitue, double longitude, double areaLatitude1, double areaLatitude2, double areaLongitude1, double areaLongitude2) {
        if (!isInRange(latitue, areaLatitude1, areaLatitude2)) {
            return false;
        }
        if (areaLongitude1 * areaLongitude2 > 0.0d) {
            if (isInRange(longitude, areaLongitude1, areaLongitude2)) {
                return true;
            }
            return false;
        } else if (Math.abs(areaLongitude1) + Math.abs(areaLongitude2) >= 180.0d) {
            double left = Math.max(areaLongitude1, areaLongitude2);
            double right = Math.min(areaLongitude1, areaLongitude2);
            if (isInRange(longitude, left, 180.0d) || isInRange(longitude, 0.0d, right)) {
                return true;
            }
            return false;
        } else if (isInRange(longitude, areaLongitude1, areaLongitude2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isInRange(double point, double left, double right) {
        if (point < Math.min(left, right) || point > Math.max(left, right)) {
            return false;
        }
        return true;
    }

    public static double getDistance(double scale, int width, int height) {
        double distance = (((double) Math.max(width, height)) * (scale / 30.0d)) / 2.0d;
        Debug.m5i(TAG, "getDistance, distance:" + distance);
        return distance;
    }
}
