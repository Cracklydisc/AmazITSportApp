package com.huami.watch.newsport.route.utils;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.Shader.TileMode;
import android.location.Location;
import com.hs.gpxparser.modal.Waypoint;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import java.util.LinkedList;
import java.util.List;

public class TrailWayPointDrawer {
    private Config mBitmapConfig = Config.ARGB_8888;
    private boolean mColorfulPath = false;
    private Context mContext = null;
    private int mDefaultLineColor = -65536;
    private float mDistancePerLat = -1.0f;
    private float mDistancePerLng = -1.0f;
    private int mDrawAreaHeight = 166;
    private int mDrawAreaWidth = 166;
    private float mDrawPathHeight = 0.0f;
    private float mDrawPathWidth = 0.0f;
    private Bitmap mEndBitmap = null;
    ArgbEvaluator mEvaluator = new ArgbEvaluator();
    private Point mFirstPoint = null;
    private int mHeight = 0;
    private float mHeightLatRatio = 0.0f;
    private Point mLastPoint = null;
    private List<Waypoint> mLocationDatas = new LinkedList();
    private Matrix mMatrix = null;
    private float mMaxLat = -1800.0f;
    private float mMaxLng = -1800.0f;
    private int mMaxSpeed = -1;
    private float mMinLat = 1800.0f;
    private float mMinLng = 1800.0f;
    private int mMinSpeed = Integer.MAX_VALUE;
    private float mPathLatDistance = -1.0f;
    private float mPathLngDistance = -1.0f;
    private Paint mPausedTrailPaint = null;
    private List<Point> mPoints;
    private Point mPreLastPoint = null;
    private float mRatio = -1.0f;
    private LinearGradient mShader;
    private float mSpeedSpan = 0.0f;
    private Paint mSportTrailPaint = null;
    private Bitmap mStartBitmap = null;
    private Bitmap mTrailBitmap = null;
    private Bitmap mWholeTrailBitmap = null;
    private Canvas mWholeTrailCanvas = null;
    private Paint mWholeTrailPaint = null;
    private int mWidth = 0;
    private float mWidthLngRatio = 0.0f;
    private float mXOffset = 0.0f;
    private float mYOffset = 0.0f;

    public static final class Point {
        public final int mColor;
        public final float mX;
        public final float mY;

        public Point(float x, float y, int color) {
            this.mX = x;
            this.mY = y;
            this.mColor = color;
        }

        public String toString() {
            return "Point{mX=" + this.mX + ", mY=" + this.mY + ", mColor=" + this.mColor + '}';
        }
    }

    public TrailWayPointDrawer(Context context, int bitmapWidth, int bitmapHeight, boolean colorfulPath) {
        this.mWidth = bitmapWidth;
        this.mHeight = bitmapHeight;
        this.mDrawAreaWidth = this.mWidth - 30;
        this.mDrawAreaHeight = this.mHeight - 30;
        this.mColorfulPath = colorfulPath;
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        this.mSportTrailPaint = new Paint(5);
        this.mSportTrailPaint.setColor(-65536);
        this.mSportTrailPaint.setStyle(Style.STROKE);
        this.mSportTrailPaint.setStrokeWidth(4.0f);
        this.mSportTrailPaint.setStrokeCap(Cap.ROUND);
        this.mPausedTrailPaint = new Paint(5);
        this.mPausedTrailPaint.setPathEffect(new DashPathEffect(new float[]{1.0f, 2.0f, 3.0f, 4.0f}, 1.0f));
        this.mPausedTrailPaint.setColor(-1442840576);
        this.mPausedTrailPaint.setStrokeWidth(4.0f);
        this.mWholeTrailPaint = new Paint(5);
        this.mMatrix = new Matrix();
        this.mFirstPoint = null;
        this.mLastPoint = null;
        this.mPreLastPoint = null;
    }

    private void initBitmapIfNotExist() {
        if (this.mWidth <= 0 || this.mHeight <= 0) {
            throw new IllegalArgumentException("width or height should not less than zero. width:" + this.mWidth + ", height:" + this.mHeight);
        }
        synchronized (this) {
            if (this.mTrailBitmap == null) {
                this.mTrailBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapConfig);
            }
            if (this.mWholeTrailBitmap == null) {
                this.mWholeTrailBitmap = Bitmap.createBitmap(this.mWidth, this.mHeight, this.mBitmapConfig);
                this.mWholeTrailCanvas = new Canvas(this.mWholeTrailBitmap);
            }
        }
    }

    public synchronized void clearState() {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "clear state");
        }
        this.mMinLng = 1800.0f;
        this.mMaxLng = -1800.0f;
        this.mMinLat = 1800.0f;
        this.mMaxLat = -1800.0f;
        this.mLastPoint = null;
        this.mPreLastPoint = null;
        this.mFirstPoint = null;
        this.mPathLngDistance = -1.0f;
        this.mPathLatDistance = -1.0f;
        this.mRatio = -1.0f;
        this.mLocationDatas.clear();
    }

    public synchronized void addLocationDatasToBitmap(List<? extends Waypoint> locationDatas) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "add line data " + locationDatas);
        }
        initBitmapIfNotExist();
        if (!(locationDatas == null || locationDatas.isEmpty())) {
            List<Point> points;
            this.mLocationDatas.addAll(locationDatas);
            boolean checkResult = checkAndUpdateMaxMinLatLng(locationDatas);
            initDraw(this.mLocationDatas);
            if (checkResult) {
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "change ratio");
                }
                points = convertToPoint(this.mLocationDatas);
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "points : " + points);
                }
                if (points == null) {
                    throw new IllegalStateException("points should not be null");
                }
                this.mFirstPoint = (Point) points.get(0);
                if (points.size() <= 1) {
                    this.mPreLastPoint = this.mLastPoint;
                } else {
                    this.mPreLastPoint = (Point) points.get(points.size() - 2);
                }
                this.mLastPoint = (Point) points.get(points.size() - 1);
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "draw area width : " + this.mDrawAreaWidth + ", draw area height : " + this.mDrawAreaHeight);
                }
            } else {
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "keep ratio " + this.mRatio);
                }
                points = convertToPoint(locationDatas);
                if (Global.DEBUG_VIEW) {
                    Debug.m3d("TrailDrawer", "points : " + points);
                }
                if (!(points == null || points.isEmpty())) {
                    points.add(0, this.mLastPoint);
                    if (points.size() <= 1) {
                        this.mPreLastPoint = this.mLastPoint;
                    } else {
                        this.mPreLastPoint = (Point) points.get(points.size() - 2);
                    }
                    this.mLastPoint = (Point) points.get(points.size() - 1);
                }
            }
            synchronized (this) {
                this.mPoints = points;
                if (this.mTrailBitmap != null) {
                    drawOnBitmap(points, this.mTrailBitmap, checkResult);
                }
            }
        }
    }

    private void drawOnBitmap(List<Point> points, Bitmap bitmap, boolean needClear) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "draw on bitmap|points:" + points + "|need clear:" + needClear);
        }
        if (this.mDrawAreaWidth > 0 && this.mDrawAreaHeight > 0) {
            Canvas canvas = new Canvas(bitmap);
            if (needClear) {
                canvas.drawColor(0, Mode.CLEAR);
            }
            boolean isFirst = true;
            float lastX = 0.0f;
            float lastY = 0.0f;
            int lastColor = 0;
            for (int i = 0; i < points.size(); i++) {
                Point p = (Point) points.get(i);
                if (isFirst) {
                    isFirst = false;
                } else {
                    this.mShader = new LinearGradient(lastX, lastY, p.mX, p.mY, lastColor, p.mColor, TileMode.CLAMP);
                    this.mSportTrailPaint.setShader(this.mShader);
                    canvas.drawLine(lastX, lastY, p.mX, p.mY, this.mSportTrailPaint);
                }
                lastX = p.mX;
                lastY = p.mY;
                lastColor = p.mColor;
            }
        }
    }

    public Point getFirstPoint() {
        return this.mFirstPoint;
    }

    public Point getPreLastPoint() {
        return this.mPreLastPoint;
    }

    public Point getLastPoint() {
        return this.mLastPoint;
    }

    private float getAngle(float startX, float startY, float endX, float endY) {
        float width = endX - startX;
        float height = startY - endY;
        float degrees = (float) Math.toDegrees((double) ((float) Math.asin((double) (height / ((float) Math.sqrt((double) ((width * width) + (height * height))))))));
        if (width > 0.0f) {
            return degrees;
        }
        return 180.0f - degrees;
    }

    public void startDraw() {
        clearState();
    }

    public void setStart(int startRes) {
        this.mStartBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), startRes);
    }

    public void setEnd(int endRes) {
        this.mEndBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), endRes);
    }

    public synchronized Bitmap newTrailBitmap() {
        Bitmap bitmap;
        initBitmapIfNotExist();
        this.mWholeTrailCanvas.drawColor(0, Mode.CLEAR);
        this.mWholeTrailCanvas.drawBitmap(this.mTrailBitmap, 0.0f, 0.0f, this.mWholeTrailPaint);
        Point firstPoint = getFirstPoint();
        Point lastPoint = getLastPoint();
        Point preLastPoint = getPreLastPoint();
        if (firstPoint == null || firstPoint == lastPoint || this.mTrailBitmap == null || Float.isInfinite(this.mRatio)) {
            if (this.mEndBitmap != null) {
                this.mWholeTrailCanvas.drawBitmap(this.mEndBitmap, (float) ((this.mWidth - this.mEndBitmap.getScaledWidth(this.mWholeTrailCanvas)) / 2), (float) ((this.mHeight - this.mEndBitmap.getScaledHeight(this.mWholeTrailCanvas)) / 2), this.mWholeTrailPaint);
            }
            bitmap = this.mWholeTrailBitmap;
        } else {
            if (this.mStartBitmap != null) {
                float firstX = firstPoint.mX;
                float firstY = firstPoint.mY;
                this.mWholeTrailCanvas.drawBitmap(this.mStartBitmap, firstX - ((float) (this.mStartBitmap.getScaledWidth(this.mWholeTrailCanvas) / 2)), firstY - ((float) (this.mStartBitmap.getScaledHeight(this.mWholeTrailCanvas) / 2)), this.mWholeTrailPaint);
            }
            if (this.mEndBitmap != null) {
                float lastX = preLastPoint.mX;
                float lastY = preLastPoint.mY;
                float endX = lastPoint.mX;
                float endY = lastPoint.mY;
                float angle = 90.0f - getAngle(lastX, lastY, endX, endY);
                this.mMatrix.reset();
                this.mMatrix.postTranslate(endX - ((float) (this.mEndBitmap.getScaledWidth(this.mWholeTrailCanvas) / 2)), endY - ((float) (this.mEndBitmap.getScaledHeight(this.mWholeTrailCanvas) / 2)));
                this.mWholeTrailCanvas.drawBitmap(this.mEndBitmap, this.mMatrix, this.mWholeTrailPaint);
            }
            bitmap = this.mWholeTrailBitmap;
        }
        return bitmap;
    }

    private void initDistancePerLatLng(float lat, float lng) {
        if (this.mDistancePerLat < 0.0f || this.mDistancePerLng < 0.0f) {
            float[] w = new float[1];
            Location.distanceBetween((double) lat, (double) lng, (double) (lat + 1.0f), (double) lng, w);
            this.mDistancePerLat = w[0];
            Location.distanceBetween((double) lat, (double) lng, (double) lat, (double) (lng + 1.0f), w);
            this.mDistancePerLng = w[0];
        }
    }

    private void initDraw(List<Waypoint> latLngs) {
        if (latLngs != null && !latLngs.isEmpty() && this.mWidth > 0 && this.mHeight > 0) {
            initDistancePerLatLng((float) ((Waypoint) latLngs.get(0)).getLatitude(), (float) ((Waypoint) latLngs.get(0)).getLongitude());
            this.mPathLngDistance = this.mDistancePerLng * (this.mMaxLng - this.mMinLng);
            this.mPathLatDistance = this.mDistancePerLat * (this.mMaxLat - this.mMinLat);
            if (Math.max(this.mPathLngDistance, this.mPathLatDistance) < ((float) Math.min(this.mDrawAreaWidth, this.mDrawAreaHeight)) / 2.0f) {
                this.mRatio = 2.0f;
            } else if (this.mPathLatDistance != 0.0f && this.mPathLngDistance == 0.0f) {
                this.mRatio = ((float) this.mDrawAreaHeight) / this.mPathLatDistance;
            } else if (this.mPathLatDistance == 0.0f && this.mPathLngDistance != 0.0f) {
                this.mRatio = ((float) this.mDrawAreaWidth) / this.mPathLngDistance;
            } else if (this.mPathLngDistance / this.mPathLatDistance > ((float) this.mDrawAreaWidth) / ((float) this.mDrawAreaHeight)) {
                this.mRatio = ((float) this.mDrawAreaWidth) / this.mPathLngDistance;
            } else {
                this.mRatio = ((float) this.mDrawAreaHeight) / this.mPathLatDistance;
            }
            this.mWidthLngRatio = this.mDistancePerLng * this.mRatio;
            this.mHeightLatRatio = this.mDistancePerLat * this.mRatio;
            this.mDrawPathWidth = (this.mMaxLng - this.mMinLng) * this.mWidthLngRatio;
            this.mDrawPathHeight = (this.mMaxLat - this.mMinLat) * this.mHeightLatRatio;
            this.mXOffset = (((float) this.mWidth) - this.mDrawPathWidth) / 2.0f;
            this.mYOffset = (((float) this.mHeight) - this.mDrawPathHeight) / 2.0f;
        }
    }

    private List<Point> convertToPoint(List<? extends Waypoint> gpsPoint) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "convert to point|points:" + gpsPoint + "|distance/lng:" + this.mDistancePerLng + "|distance/lat:" + this.mDistancePerLat + "|mRatio:" + this.mRatio);
        }
        if (gpsPoint == null || gpsPoint.isEmpty()) {
            return null;
        }
        List<Point> result = new LinkedList();
        for (Waypoint point : gpsPoint) {
            result.add(new Point((float) (((point.getLongitude() - ((double) this.mMinLng)) * ((double) this.mWidthLngRatio)) + ((double) this.mXOffset)), (float) (((((double) this.mMaxLat) - point.getLatitude()) * ((double) this.mHeightLatRatio)) + ((double) this.mYOffset)), -65536));
        }
        return result;
    }

    private boolean checkAndUpdateMaxMinLatLng(List<? extends Waypoint> locationDatas) {
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "check and update " + locationDatas);
        }
        boolean result = false;
        for (Waypoint locationData : locationDatas) {
            if (((double) this.mMinLat) > locationData.getLatitude()) {
                this.mMinLat = (float) locationData.getLatitude();
                result = true;
            }
            if (((double) this.mMinLng) > locationData.getLongitude()) {
                this.mMinLng = (float) locationData.getLongitude();
                result = true;
            }
            if (((double) this.mMaxLat) < locationData.getLatitude()) {
                this.mMaxLat = (float) locationData.getLatitude();
                result = true;
            }
            if (((double) this.mMaxLng) < locationData.getLongitude()) {
                this.mMaxLng = (float) locationData.getLongitude();
                result = true;
            }
        }
        this.mSpeedSpan = (float) (this.mMaxSpeed - this.mMinSpeed);
        if (Global.DEBUG_VIEW) {
            Debug.m3d("TrailDrawer", "check result " + result);
        }
        return result;
    }
}
