package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TimeHeartCurveInfo extends CurveInfo {
    List<HeartRate> mAvailableHeartRates = null;
    List<CurvePoint> mHeartRatePoints = null;
    private int mMaxIndex = -1;
    private int mMaxValue = -1;
    private int mMinIndex = -1;
    private int mMinValue = -1;
    private float mRange = 0.0f;
    private SportSummary mSportSummary = null;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeHeartCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            this.mSportSummary = getSportSummary(this.mTrackId);
            List<? extends HeartRate> heartRateList = getAllHeartRateList(startTime, endTime, this.mSportSummary);
            this.mTotalTime = (long) this.mSportSummary.getSportDuration();
            if (heartRateList == null || heartRateList.isEmpty()) {
                Debug.m5i("CurveInfo", "heart list is null");
                return null;
            }
            HeartRate rate;
            this.mAvailableHeartRates = new LinkedList();
            for (int i = 0; i < heartRateList.size(); i++) {
                rate = (HeartRate) heartRateList.get(i);
                if (rate != null && rate.getHeartQuality() <= 1) {
                    this.mAvailableHeartRates.add(rate);
                }
            }
            this.mHeartRatePoints = createHeartRatePointsInfo(this.mAvailableHeartRates, startTime, endTime);
            if (Global.DEBUG_LEVEL_3) {
                for (HeartRate rate2 : this.mAvailableHeartRates) {
                    Debug.m5i("test_heart", "duration:" + ((rate2.getTimestamp() - startTime) / 1000));
                }
            }
            CurvePointsInfo info = new CurvePointsInfo();
            info.mMaxValue = (float) this.mMaxValue;
            info.mMinValue = (float) this.mMinValue;
            info.mCurvePoints.addAll(this.mHeartRatePoints);
            optimizeCurveData(info, this.mMinIndex, this.mMaxIndex);
            configCordinateinfo(info, this.mRange);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateDisInfo(List heartRateList, long trackId, float totalDistance) {
        return super.getXCordinateDisInfo(heartRateList, trackId, totalDistance);
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (this.mMaxValue == -1 || this.mMinValue == -1) {
            return new String[0];
        }
        if (this.mMaxValue - this.mMinValue < 30) {
            return new String[]{String.valueOf(this.mMaxValue), String.valueOf(this.mMinValue)};
        }
        yAxis = new String[4];
        int range = (this.mMaxValue - this.mMinValue) / 3;
        yAxis[3] = String.valueOf(this.mMinValue);
        yAxis[2] = String.valueOf(this.mMinValue + range);
        yAxis[1] = String.valueOf(this.mMinValue + (range * 2));
        yAxis[0] = String.valueOf(this.mMaxValue);
        return yAxis;
    }

    public float getAveValue() {
        if (this.mSportSummary != null) {
            return (float) ((OutdoorSportSummary) this.mSportSummary).getAvgHeartRate();
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.no_heartrate_data_hint);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }

    private List<CurvePoint> createHeartRatePointsInfo(List<? extends HeartRate> heartRates, long startTime, long endTime) {
        long timeSpan = endTime - startTime;
        if (timeSpan < 9000) {
            return new ArrayList();
        }
        int minutesOnePoint = ((int) (timeSpan / 9600000)) + 1;
        List<CurvePoint> heartPoints = new LinkedList();
        float pointIndex = 0.0f;
        float avgTotal = 0.0f;
        float avgNum = 0.0f;
        float maxIndex = 0.0f;
        float minIndex = 0.0f;
        float maxHeart = 0.0f;
        float minHeart = Float.MAX_VALUE;
        long minTime = 0;
        long maxTime = 0;
        int i = 0;
        while (i < heartRates.size()) {
            HeartRate heartRate = (HeartRate) heartRates.get(i);
            float avg;
            if (timeSpan < 60000) {
                avgTotal += (float) heartRate.getHeartRate();
                avgNum += 1.0f;
                if (i == heartRates.size() - 1) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    pointIndex += 1.0f;
                }
            } else {
                if (((float) heartRate.getHeartRate()) >= maxHeart) {
                    maxIndex = pointIndex;
                    maxTime = heartRate.getTimestamp() - startTime;
                    maxHeart = (float) heartRate.getHeartRate();
                }
                if (((float) heartRate.getHeartRate()) <= minHeart) {
                    minIndex = pointIndex;
                    minTime = heartRate.getTimestamp() - startTime;
                    minHeart = (float) heartRate.getHeartRate();
                }
                if ((heartRate.getHeartRate() > 0 && ((float) (heartRate.getTimestamp() - startTime)) <= (60000.0f * pointIndex) * ((float) minutesOnePoint)) || pointIndex == 0.0f) {
                    avgTotal += (float) heartRate.getHeartRate();
                    avgNum += 1.0f;
                }
                if (((float) (heartRate.getTimestamp() - startTime)) > (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += 1.0f;
                    i--;
                } else if (((float) (heartRate.getTimestamp() - startTime)) >= (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    if (((float) (heartRate.getTimestamp() - startTime)) > (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                        i--;
                    }
                    pointIndex += 1.0f;
                } else if (i == heartRates.size() - 1 && timeSpan > 60000) {
                    float scale = ((float) ((long) (((float) (heartRate.getTimestamp() - startTime)) - (((pointIndex - 1.0f) * 60000.0f) * ((float) minutesOnePoint))))) / ((float) (60000 * minutesOnePoint));
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint((pointIndex - 1.0f) + scale, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += scale;
                }
            }
            i++;
        }
        if (timeSpan > 60000) {
            if (!(minHeart == Float.MAX_VALUE || minTime == 0)) {
                CurvePoint hr = new CurvePoint(((float) minTime) / ((float) (60000 * minutesOnePoint)), minHeart);
                if (minIndex < ((float) (heartPoints.size() - 1))) {
                    heartPoints.remove((CurvePoint) heartPoints.get((int) minIndex));
                }
                heartPoints.add((int) minIndex, hr);
                this.mMinIndex = (int) minIndex;
            }
            if (!(maxHeart == 0.0f || maxTime == 0)) {
                CurvePoint curvePoint = new CurvePoint(((float) maxTime) / ((float) (60000 * minutesOnePoint)), maxHeart);
                CurvePoint maxPoint = (CurvePoint) heartPoints.get((int) maxIndex);
                if (minIndex != maxIndex) {
                    if (maxIndex < ((float) (heartPoints.size() - 1))) {
                        heartPoints.remove(maxPoint);
                    }
                    heartPoints.add((int) maxIndex, curvePoint);
                    this.mMaxIndex = (int) maxIndex;
                } else if (maxTime < minTime) {
                    heartPoints.add((int) maxIndex, curvePoint);
                    this.mMaxIndex = (int) maxIndex;
                } else {
                    heartPoints.add(((int) maxIndex) + 1, curvePoint);
                    this.mMaxIndex = ((int) maxIndex) + 1;
                }
            }
            this.mMaxValue = (int) maxHeart;
            this.mMinValue = (int) minHeart;
            this.mMaxValue = this.mMaxValue % 10 == 0 ? this.mMaxValue : (this.mMaxValue + 10) - (this.mMaxValue % 10);
            this.mMinValue = this.mMinValue % 10 == 0 ? this.mMinValue : (this.mMinValue / 10) * 10;
        }
        pointIndex = (((float) timeSpan) / ((float) (60000 * minutesOnePoint))) + 1.0f;
        if (pointIndex >= 2.0f) {
            this.mRange = pointIndex - 1.0f;
        } else {
            this.mRange = 1.0f;
        }
        return heartPoints;
    }
}
