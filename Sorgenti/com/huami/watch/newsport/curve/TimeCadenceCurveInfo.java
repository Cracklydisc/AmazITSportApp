package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import java.util.List;

public class TimeCadenceCurveInfo extends CurveInfo {
    List<? extends CyclingDetail> mCadenceList = null;
    private int mMaxCadence = 0;
    private int mMaxIndex = -1;
    private int mMinCadence = Integer.MAX_VALUE;
    private int mMinIndex = -1;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeCadenceCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mCadenceList = getCadenceData(this.mTrackId, summary);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mCadenceList == null || this.mCadenceList.isEmpty()) {
                return null;
            }
            CurvePointsInfo info = getXCordinateTimeInfo(this.mCadenceList, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, -1);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateTimeInfo(List cadenceList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (cadenceList != null) {
            int i = 0;
            for (Object cadence : cadenceList) {
                long time = ((CyclingDetail) cadence).getCurTime();
                int curCadence = ((CyclingDetail) cadence).getCurCadence();
                if (curCadence > this.mMaxCadence) {
                    this.mMaxCadence = curCadence;
                    this.mMaxIndex = i;
                }
                if (curCadence < this.mMinCadence) {
                    this.mMinCadence = curCadence;
                    this.mMinIndex = i;
                }
                info.mCurvePoints.add(new CurvePoint((float) time, (float) curCadence));
                i++;
            }
            this.mMaxCadence = optimizeData(this.mMaxCadence, true);
            this.mMinCadence = optimizeData(this.mMinCadence, false);
            info.mMaxValue = (float) this.mMaxCadence;
            info.mMinValue = (float) this.mMinCadence;
        }
        return info;
    }

    private int optimizeData(int data, boolean isUp) {
        if (isUp) {
            return (data + 10) - (data % 10);
        }
        return data - (data % 10);
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare((float) this.mMaxCadence, 0.0f) == 0 || Float.compare((float) this.mMinCadence, Float.MAX_VALUE) == 0) {
            return null;
        }
        int range = this.mMaxCadence - this.mMinCadence;
        if (range < 30) {
            return new String[]{"" + String.valueOf(this.mMaxCadence), "" + String.valueOf(this.mMinCadence)};
        }
        double standardY = (double) (range / 3);
        return new String[]{String.valueOf(this.mMinCadence), String.valueOf((int) (((double) this.mMinCadence) + standardY)), String.valueOf((int) (((double) this.mMinCadence) + (2.0d * standardY))), String.valueOf(this.mMaxCadence)};
    }

    public float getAveValue() {
        if (this.sportSummary != null) {
            return ((OutdoorSportSummary) this.sportSummary).getAveCadence();
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_cadence_data);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
