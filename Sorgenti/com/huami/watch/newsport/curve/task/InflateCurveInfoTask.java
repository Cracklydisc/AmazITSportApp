package com.huami.watch.newsport.curve.task;

import android.content.Context;
import android.os.AsyncTask;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.ui.view.NewSportChart;
import com.huami.watch.newsport.ui.view.NewSportChart.ILineChartLoadingListener;
import java.util.ArrayList;
import java.util.List;

public class InflateCurveInfoTask extends AsyncTask<Object, Void, CurvePointsInfo> implements ILineChartLoadingListener {
    private Context mContext = null;
    private CurveInfoPointFactory mCurvePointFactory;
    private NewSportChart<Float> mCurveView;
    private boolean mIsNoData = false;
    private boolean mIsShownAveLine = false;
    private IInflateCurveInfoListener mListener = null;
    private XCoordinateType xType;
    private YCoordinateType yType;

    class C05631 implements Runnable {
        C05631() {
        }

        public void run() {
            InflateCurveInfoTask.this.mListener.onLoadFinished(InflateCurveInfoTask.this.xType, InflateCurveInfoTask.this.yType);
        }
    }

    public interface IInflateCurveInfoListener {
        void onLoadFinished(XCoordinateType xCoordinateType, YCoordinateType yCoordinateType);
    }

    public InflateCurveInfoTask(Context context, NewSportChart curveView, XCoordinateType xCoordinateType, YCoordinateType yCoordinateType, boolean isNeedAveLine, IInflateCurveInfoListener listener, int sportType) {
        this.mContext = context;
        this.mCurveView = curveView;
        this.xType = xCoordinateType;
        this.yType = yCoordinateType;
        this.mCurvePointFactory = new CurveInfoPointFactory(context, this.xType, this.yType, sportType);
        this.mIsShownAveLine = isNeedAveLine;
        this.mListener = listener;
        this.mCurveView.setLineChartListener(this);
    }

    public InflateCurveInfoTask setLocationDatas(List<? extends SportLocationData> sportLocationDataList) {
        if (this.mCurvePointFactory != null) {
            this.mCurvePointFactory.setLocationList(sportLocationDataList);
        }
        return this;
    }

    public InflateCurveInfoTask setHeartRateDatas(List<? extends HeartRate> heartRates) {
        if (this.mCurvePointFactory != null) {
            this.mCurvePointFactory.setHeartRateDatas(heartRates);
        }
        return this;
    }

    protected CurvePointsInfo doInBackground(Object... params) {
        return this.mCurvePointFactory.getCurvePointInfo(params);
    }

    protected void onPostExecute(CurvePointsInfo info) {
        super.onPostExecute(info);
        if (info == null) {
            this.mIsNoData = true;
            this.mCurveView.setNoData();
            if (this.mCurvePointFactory.getNoDataHint() != null) {
                this.mCurveView.setNoDataHint(this.mCurvePointFactory.getNoDataHint());
            }
            if (this.xType != XCoordinateType.DISTANCE || this.mCurvePointFactory.getXRange() <= 0.0f) {
                float benchTextSize = this.mContext.getResources().getDimension(C0532R.dimen.bench_xaxis_text_size);
                this.mCurveView.setXAxis(this.mContext.getResources().getDimension(C0532R.dimen.xaxis_offset), benchTextSize);
                this.mCurveView.setData(this.mCurvePointFactory.getXTimeAxis(), this.mCurvePointFactory.getXRange(), false);
                return;
            }
            this.mCurveView.setData(this.mCurvePointFactory.getXDisAxis(), this.mCurvePointFactory.getXRange(), false);
            return;
        }
        this.mCurveView.setIsMaxOrMinValue(true);
        this.mCurveView.setMaxOrMinValue(Float.valueOf(info.getMinValue()), Float.valueOf(info.getMaxValue()));
        this.mCurveView.setData(info.getAxisSplit(), info.getYxisSplit(), info.getRange(), false);
        String[] displayStr = this.mCurvePointFactory.getMinOrMaxValueDisplay();
        if (displayStr != null && displayStr.length == 2) {
            this.mCurveView.setMaxOrMinDisplayStr(displayStr[0], displayStr[1]);
        }
        List<Float> xValues = new ArrayList();
        List<Float> yValues = new ArrayList();
        List<CurvePoint> mPoints = info.getCurvePoints();
        if (mPoints == null || mPoints.isEmpty()) {
            this.mIsNoData = true;
            this.mCurveView.setNoData();
            if (this.mCurvePointFactory.getNoDataHint() != null) {
                this.mCurveView.setNoDataHint(this.mCurvePointFactory.getNoDataHint());
            }
            this.mCurveView.setData(this.mCurvePointFactory.getXTimeAxis(), this.mCurvePointFactory.getXRange(), false);
        } else {
            float[] x;
            Float[] y;
            for (CurvePoint point : mPoints) {
                if (this.yType != YCoordinateType.HEART_RATE && this.yType != YCoordinateType.STROKE && this.yType != YCoordinateType.PACE) {
                    xValues.add(Float.valueOf(point.getX()));
                    yValues.add(Float.valueOf(point.getCurveValue()));
                } else if (point.getCurveValue() > 0.0f) {
                    xValues.add(Float.valueOf(point.getX()));
                    yValues.add(Float.valueOf(point.getCurveValue()));
                } else if (!xValues.isEmpty()) {
                    x = new float[xValues.size()];
                    y = new Float[yValues.size()];
                    for (int j = 0; j < xValues.size(); j++) {
                        x[j] = ((Float) xValues.get(j)).floatValue();
                        y[j] = (Float) yValues.get(j);
                    }
                    this.mCurveView.addData(x, y);
                    xValues.clear();
                    yValues.clear();
                }
            }
            if (!(xValues.isEmpty() || yValues.isEmpty())) {
                x = new float[xValues.size()];
                y = new Float[yValues.size()];
                for (int i = 0; i < xValues.size(); i++) {
                    x[i] = ((Float) xValues.get(i)).floatValue();
                    y[i] = (Float) yValues.get(i);
                }
                this.mCurveView.addData(x, y);
            }
        }
        if (this.mIsShownAveLine && this.mCurvePointFactory != null && this.mCurvePointFactory.getCurveAveValue() != -1.0f && this.mCurvePointFactory.getCurveAveValue() > info.mMinValue && this.mCurvePointFactory.getCurveAveValue() < info.mMaxValue) {
            this.mCurveView.setAveValue(Float.valueOf(this.mCurvePointFactory.getCurveAveValue()));
            this.mCurveView.setIsShownAveLine(true);
        }
        if (this.xType == XCoordinateType.TIME && this.mContext != null) {
            benchTextSize = this.mContext.getResources().getDimension(C0532R.dimen.bench_xaxis_text_size);
            this.mCurveView.setXAxis(this.mContext.getResources().getDimension(C0532R.dimen.xaxis_offset), benchTextSize);
        }
    }

    public void onLoadingFinished() {
        if (!this.mIsNoData && this.mListener != null) {
            Global.getGlobalUIHandler().postDelayed(new C05631(), 400);
        }
    }
}
