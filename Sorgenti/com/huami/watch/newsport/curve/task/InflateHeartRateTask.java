package com.huami.watch.newsport.curve.task;

import android.content.Context;
import android.os.AsyncTask;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.curve.controller.HeartRatePointFactory;
import com.huami.watch.newsport.curve.controller.HeartRatePointFactory.HeartRatePoint;
import com.huami.watch.newsport.curve.controller.HeartRatePointFactory.HeartRatePointsInfo;
import com.huami.watch.newsport.ui.view.NewSportChart;
import com.huami.watch.newsport.ui.view.NewSportChart.ILineChartLoadingListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class InflateHeartRateTask extends AsyncTask<Long, Void, HeartRatePointsInfo> implements ILineChartLoadingListener {
    private int mAvgHeartRate;
    private WeakReference<Context> mContextRef;
    private NewSportChart<Integer> mHeartRateView;
    private IInflateListener mListener;
    private HeartRatePointFactory mPointFactory;
    private SyncDatabaseManager<HeartRate> mSyncDatabaseManager;

    class C05641 implements Runnable {
        C05641() {
        }

        public void run() {
            InflateHeartRateTask.this.mListener.onLoadFinished();
        }
    }

    public interface IInflateListener {
        void onLoadFinished();
    }

    protected HeartRatePointsInfo doInBackground(Long... params) {
        if (params == null || params.length < 2) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (this.mContextRef == null) {
            return null;
        } else {
            Context context = (Context) this.mContextRef.get();
            if (context == null) {
                return null;
            }
            HeartRatePointsInfo info = this.mPointFactory.createHeartRatePoint(context, this.mSyncDatabaseManager.selectAll(context, "time>=? AND time<=? AND heart_quality<=?", new String[]{"" + params[0], "" + params[1], "1"}, "time ASC", null), params[0].longValue(), params[1].longValue());
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("InflateHeartRateTask", "heart rate info " + info);
            }
            int arraySize = info.getHeartRatePoints().size();
            double[] x = new double[arraySize];
            double[] y = new double[arraySize];
            for (int i = 0; i < arraySize; i++) {
                HeartRatePoint p = (HeartRatePoint) info.getHeartRatePoints().get(i);
                x[i] = (double) p.getX();
                y[i] = (double) p.getHeartRateValue();
            }
            return info;
        }
    }

    protected void onPostExecute(HeartRatePointsInfo info) {
        int i;
        this.mHeartRateView.setData(info.getAxisSplit(), info.getRange(), true);
        if (info.getHeartRatePoints() == null || info.getHeartRatePoints().isEmpty()) {
            this.mHeartRateView.setNoData();
        }
        List<HeartRatePoint> heartRatePoints = info.getHeartRatePoints();
        List<Float> xValues = new ArrayList();
        List<Integer> yValues = new ArrayList();
        boolean hasDataAdded = false;
        int mTotalHeart = 0;
        for (HeartRatePoint point : heartRatePoints) {
            float[] x;
            Integer[] y;
            if (point.getHeartRateValue() > 0.0f) {
                xValues.add(Float.valueOf(point.getX()));
                yValues.add(Integer.valueOf((int) point.getHeartRateValue()));
                mTotalHeart = ((int) (((float) mTotalHeart) + point.getHeartRateValue())) + 1;
            } else if (!xValues.isEmpty()) {
                x = new float[xValues.size()];
                y = new Integer[yValues.size()];
                for (i = 0; i < xValues.size(); i++) {
                    x[i] = ((Float) xValues.get(i)).floatValue();
                    y[i] = (Integer) yValues.get(i);
                }
                this.mHeartRateView.addData(x, y);
                hasDataAdded = true;
                xValues.clear();
                yValues.clear();
            }
        }
        if (!(xValues.isEmpty() || yValues.isEmpty())) {
            x = new float[xValues.size()];
            y = new Integer[yValues.size()];
            for (i = 0; i < xValues.size(); i++) {
                x[i] = ((Float) xValues.get(i)).floatValue();
                y[i] = (Integer) yValues.get(i);
            }
            this.mHeartRateView.addData(x, y);
            hasDataAdded = true;
        }
        if (this.mAvgHeartRate > 0) {
            this.mHeartRateView.setAveValue(Integer.valueOf(this.mAvgHeartRate));
        } else if (null != null) {
            this.mHeartRateView.setAveValue(Integer.valueOf(mTotalHeart / 0));
        }
        if (!hasDataAdded) {
            this.mHeartRateView.setNoData();
        }
    }

    public void onLoadingFinished() {
        if (this.mListener != null) {
            Global.getGlobalUIHandler().postDelayed(new C05641(), 400);
        }
    }
}
