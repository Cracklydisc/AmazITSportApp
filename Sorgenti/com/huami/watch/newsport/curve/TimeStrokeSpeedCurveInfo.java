package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import java.util.List;

public class TimeStrokeSpeedCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    private int mMaxIndex = -1;
    private int mMaxSrokeSpeed = 0;
    private int mMinIndex = -1;
    private int mMinSrokeSpeed = Integer.MAX_VALUE;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeStrokeSpeedCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, summary);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mHeartRateList == null || this.mHeartRateList.isEmpty()) {
                return null;
            }
            CurvePointsInfo info = getXCordinateTimeInfo(this.mHeartRateList, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, -1);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateTimeInfo(List heartList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (heartList != null) {
            int i = 0;
            for (Object heart : heartList) {
                long time = ((HeartRate) heart).getTimestamp() - this.sportSummary.getTrackId();
                int strokeSpeed = (int) (((HeartRate) heart).getStrokeSpeed() * 60.0f);
                if (strokeSpeed < 10) {
                    info.mCurvePoints.add(new CurvePoint((float) time, 0.0f));
                    i++;
                } else {
                    if (strokeSpeed > this.mMaxSrokeSpeed) {
                        this.mMaxSrokeSpeed = strokeSpeed;
                        this.mMaxIndex = i;
                    }
                    if (strokeSpeed < this.mMinSrokeSpeed) {
                        this.mMinSrokeSpeed = strokeSpeed;
                        this.mMinIndex = i;
                    }
                    info.mCurvePoints.add(new CurvePoint((float) time, (float) strokeSpeed));
                    i++;
                }
            }
            this.mMaxSrokeSpeed = optimizeSpeed(this.mMaxSrokeSpeed, true);
            this.mMinSrokeSpeed = optimizeSpeed(this.mMinSrokeSpeed, false);
            info.mMaxValue = (float) this.mMaxSrokeSpeed;
            info.mMinValue = (float) this.mMinSrokeSpeed;
        }
        return info;
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare((float) this.mMaxSrokeSpeed, 0.0f) == 0 || Float.compare((float) this.mMinSrokeSpeed, Float.MAX_VALUE) == 0) {
            return null;
        }
        int range = this.mMaxSrokeSpeed - this.mMinSrokeSpeed;
        if (range < 30) {
            return new String[]{"" + this.mMaxSrokeSpeed, "" + this.mMinSrokeSpeed};
        }
        double standardY = (double) (range / 3);
        return new String[]{String.valueOf(this.mMinSrokeSpeed), String.valueOf((int) (((double) this.mMinSrokeSpeed) + standardY)), String.valueOf((int) (((double) this.mMinSrokeSpeed) + (2.0d * standardY))), String.valueOf(this.mMaxSrokeSpeed)};
    }

    private int optimizeSpeed(int speed, boolean isUp) {
        if (isUp) {
            return (speed + 10) - (speed % 10);
        }
        return speed - (speed % 10);
    }

    public float getAveValue() {
        if (this.sportSummary != null) {
            return ((OutdoorSportSummary) this.sportSummary).getAvgStrokeSpeed() * 60.0f;
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_stroke_speed_data);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
