package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.PauseInfo;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class CurveInfo<T> {
    private List<? extends HeartRate> heartRateDatas;
    private List<? extends SportLocationData> locationList;
    protected WeakReference<Context> mContextRef;
    private SyncDatabaseManager<CyclingDetail> mSyncCadenceSummaryManager = null;
    private SyncDatabaseManager<HeartRate> mSyncHeartManager = null;
    private SyncDatabaseManager<SportLocationData> mSyncLocationManager = null;
    private SyncDatabaseManager<SportSummary> mSyncSportSummaryManager = null;
    protected int mVersion = 12;
    protected SportSummary sportSummary;

    public static final class CurveInfoUtils {
        public static void optimizeHeartRateWithoutPauseTime(SportSummary sportSummary, List<? extends HeartRate> heartRates) {
            if (sportSummary != null && heartRates != null) {
                List<PauseInfo> pauseInfos = sportSummary.getPauseInfos();
                if (pauseInfos != null && !pauseInfos.isEmpty()) {
                    Debug.m5i("CurveInfo", "&&&&&& optimize heart start &&&&&&&&&");
                    int count = 0;
                    for (HeartRate rate : heartRates) {
                        long time = rate.getTimestamp();
                        long pauseTime = 0;
                        for (PauseInfo pauseInfo : pauseInfos) {
                            long pauseEndTime = pauseInfo.getEnd();
                            long pauseStartTime = pauseInfo.getStart();
                            if (time < pauseEndTime) {
                                if (time <= pauseStartTime) {
                                    break;
                                }
                                if (Global.DEBUG_LEVEL_3) {
                                    Debug.m5i("CurveInfo", "time is between pause start and end time:(" + pauseStartTime + ", " + pauseEndTime + ")" + ", time:" + time);
                                }
                                count++;
                                pauseTime += time - pauseStartTime;
                            } else {
                                pauseTime += pauseEndTime - pauseStartTime;
                            }
                        }
                        rate.setTimestamp(time - pauseTime);
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        for (HeartRate rate2 : heartRates) {
                            Debug.m5i("TAG", "rate heart:" + rate2.toString());
                        }
                    }
                    Debug.m5i("CurveInfo", "&&&&&& optimize heart end &&&&&&&&&, count:" + count);
                }
            }
        }

        public static void optimizeLocationWithoutPauseTime(SportSummary sportSummary, List<? extends SportLocationData> locationDatas) {
            if (sportSummary != null && locationDatas != null) {
                List<PauseInfo> pauseInfos = sportSummary.getPauseInfos();
                if (pauseInfos != null && !pauseInfos.isEmpty()) {
                    Debug.m5i("CurveInfo", "&&&&&& optimize location start &&&&&&&&&");
                    int count = 0;
                    for (SportLocationData locationData : locationDatas) {
                        long time = locationData.mTimestamp + sportSummary.getTrackId();
                        long pauseTime = 0;
                        for (PauseInfo pauseInfo : pauseInfos) {
                            long pauseEndTime = pauseInfo.getEnd();
                            long pauseStartTime = pauseInfo.getStart();
                            if (time < pauseEndTime) {
                                if (time <= pauseStartTime) {
                                    break;
                                }
                                if (Global.DEBUG_LEVEL_3) {
                                    Debug.m5i("CurveInfo", "time is between pause start and end time:(" + pauseStartTime + ", " + pauseEndTime + ")" + ", time:" + time);
                                }
                                count++;
                                pauseTime += time - pauseStartTime;
                            } else {
                                pauseTime += pauseEndTime - pauseStartTime;
                            }
                        }
                        locationData.mTimestamp = (time - pauseTime) - sportSummary.getTrackId();
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        for (SportLocationData locationData2 : locationDatas) {
                            Debug.m5i("TAG", "locationData:" + locationData2.toString());
                        }
                    }
                    Debug.m5i("CurveInfo", "&&&&&& optimize location end &&&&&&&&&, count:" + count);
                }
            }
        }

        public static void optimizeCadenceWithoutPauseTime(SportSummary sportSummary, List<? extends CyclingDetail> cyclingDetails) {
            if (sportSummary != null && cyclingDetails != null && !cyclingDetails.isEmpty()) {
                List<PauseInfo> pauseInfos = sportSummary.getPauseInfos();
                if (pauseInfos != null && !pauseInfos.isEmpty()) {
                    Debug.m5i("CurveInfo", "&&&&&& optimize CyclingDetail start &&&&&&&&&");
                    int count = 0;
                    for (CyclingDetail detail : cyclingDetails) {
                        long time = detail.getCurTime();
                        long pauseTime = 0;
                        for (PauseInfo pauseInfo : pauseInfos) {
                            long pauseEndTime = pauseInfo.getEnd();
                            long pauseStartTime = pauseInfo.getStart();
                            if (time < pauseEndTime) {
                                if (time <= pauseStartTime) {
                                    break;
                                }
                                if (Global.DEBUG_LEVEL_3) {
                                    Debug.m5i("CurveInfo", "time is between pause start and end time:(" + pauseStartTime + ", " + pauseEndTime + ")" + ", time:" + time);
                                }
                                count++;
                                pauseTime += time - pauseStartTime;
                            } else {
                                pauseTime += pauseEndTime - pauseStartTime;
                            }
                        }
                        detail.setCurTime(time - pauseTime);
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        for (CyclingDetail detail2 : cyclingDetails) {
                            Debug.m5i("TAG", "rate heart:" + detail2.toString());
                        }
                    }
                    Debug.m5i("CurveInfo", "&&&&&& optimize CyclingDetail end &&&&&&&&&, count:" + count);
                }
            }
        }
    }

    public static class CurvePoint {
        private float mCurveValue = -1.0f;
        private float mX = -1.0f;

        public CurvePoint(float x, float curveValue) {
            this.mX = x;
            this.mCurveValue = curveValue;
        }

        public String toString() {
            return "CurvePoint{mX=" + this.mX + ", mCurveValue=" + this.mCurveValue + '}';
        }

        public float getX() {
            return this.mX;
        }

        public float getCurveValue() {
            return this.mCurveValue;
        }

        public void setCurveValue(float curveValue) {
            this.mCurveValue = curveValue;
        }
    }

    public static class CurvePointsInfo {
        public String[] mAxisSplit = null;
        public List<CurvePoint> mCurvePoints = new LinkedList();
        public float mMaxValue = -1.0f;
        public float mMinValue = -1.0f;
        public float mRange = 1.0f;
        public String mUnit = null;
        public String[] mYxisSplit = null;

        public String toString() {
            return "CurvePointsInfo{mCurvePoints=" + this.mCurvePoints + ", mUnit='" + this.mUnit + '\'' + ", mAxisSplit=" + Arrays.toString(this.mAxisSplit) + ", mYxisSplit=" + Arrays.toString(this.mYxisSplit) + ", mRange=" + this.mRange + ", mMaxValue=" + this.mMaxValue + ", mMinValue=" + this.mMinValue + '}';
        }

        public float getMaxValue() {
            return this.mMaxValue;
        }

        public float getMinValue() {
            return this.mMinValue;
        }

        public List<CurvePoint> getCurvePoints() {
            return this.mCurvePoints;
        }

        public String[] getAxisSplit() {
            return this.mAxisSplit;
        }

        public float getRange() {
            return this.mRange;
        }

        public String[] getYxisSplit() {
            return this.mYxisSplit;
        }
    }

    public abstract int addDataToCurveInfo(CurvePointsInfo curvePointsInfo, int i, long j, long j2, int i2, float f);

    public abstract float getAveValue();

    public abstract CurvePointsInfo getCurvePoints(Object... objArr);

    public abstract String getNoDataHint();

    public abstract String[] getXAxis();

    public abstract String[] getYAxis();

    public CurveInfo(Context context) {
        this.mContextRef = new WeakReference(context);
        this.mSyncHeartManager = new SyncDatabaseManager(HeartRateDao.getInstance(context.getApplicationContext()));
        this.mSyncLocationManager = new SyncDatabaseManager(LocationDataDao.getInstance(context.getApplicationContext()));
        this.mSyncSportSummaryManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context.getApplicationContext()));
        this.mSyncCadenceSummaryManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(context.getApplicationContext()));
    }

    public String[] getMinOrMaxValueDisplay() {
        return null;
    }

    protected boolean isContextAvaiable() {
        if (this.mContextRef == null || ((Context) this.mContextRef.get()) == null) {
            return false;
        }
        return true;
    }

    protected void optimizeCurveData(CurvePointsInfo info) {
        List<CurvePoint> points = info.getCurvePoints();
        if (points != null && !points.isEmpty() && points.size() >= 300) {
            float unitValue = ((float) points.size()) / 300.0f;
            List<CurvePoint> optimizedPoints = new LinkedList();
            optimizedPoints.add(points.get(0));
            for (int i = 1; i <= 300; i++) {
                int index = ((int) (((float) i) * unitValue)) - 1;
                if (index != 0) {
                    optimizedPoints.add(points.get(index));
                }
            }
            info.mCurvePoints = optimizedPoints;
        }
    }

    protected void optimizeCurveData(CurvePointsInfo info, int maxIndex, int minIndex) {
        List<CurvePoint> points = info.getCurvePoints();
        if (points != null && !points.isEmpty() && points.size() >= 300) {
            float unitValue = ((float) points.size()) / 300.0f;
            List<CurvePoint> optimizedPoints = new LinkedList();
            int lastIndex = -1;
            for (int i = 1; i <= 300; i++) {
                int index = ((int) (((float) i) * unitValue)) - 1;
                if (index > minIndex && lastIndex < minIndex) {
                    optimizedPoints.add(points.get(minIndex));
                }
                if (index > maxIndex && lastIndex < maxIndex) {
                    optimizedPoints.add(points.get(maxIndex));
                }
                optimizedPoints.add(points.get(index));
                lastIndex = index;
            }
            info.mCurvePoints = optimizedPoints;
        }
    }

    protected void configCordinateinfo(CurvePointsInfo info, float xRange) {
        info.mAxisSplit = getXAxis();
        info.mYxisSplit = getYAxis();
        info.mRange = xRange;
    }

    protected List<? extends HeartRate> getAllHeartRateList(long startTime, long endTime, SportSummary sportSummary) {
        if (this.heartRateDatas != null) {
            if (this.heartRateDatas.size() >= 2) {
                HeartRate lastHeart = (HeartRate) this.heartRateDatas.get(this.heartRateDatas.size() - 1);
                if (((long) ((HeartRate) this.heartRateDatas.get(0)).getHeartRate()) >= startTime && lastHeart.getTimestamp() <= endTime) {
                    return this.heartRateDatas;
                }
            }
            List<? extends HeartRate> heartRateList = new ArrayList();
            for (HeartRate heartRate : this.heartRateDatas) {
                if (heartRate.getTimestamp() <= endTime && heartRate.getTimestamp() >= startTime) {
                    heartRateList.add(heartRate);
                }
            }
            return heartRateList;
        }
        this.heartRateDatas = this.mSyncHeartManager.selectAll((Context) this.mContextRef.get(), "time>=? AND time<=?", new String[]{"" + sportSummary.getStartTime(), "" + sportSummary.getEndTime()}, "time ASC", null);
        CurveInfoUtils.optimizeHeartRateWithoutPauseTime(sportSummary, this.heartRateDatas);
        return this.heartRateDatas;
    }

    protected List<? extends SportLocationData> getLocationData(long trackId) {
        if (this.locationList != null) {
            return this.locationList;
        }
        this.locationList = this.mSyncLocationManager.selectAll((Context) this.mContextRef.get(), "track_id=?", new String[]{"" + trackId}, "timestamp ASC", null);
        return this.locationList;
    }

    protected SportSummary getSportSummary(long trackId) {
        if (this.sportSummary != null) {
            return this.sportSummary;
        }
        this.sportSummary = (SportSummary) this.mSyncSportSummaryManager.select((Context) this.mContextRef.get(), "track_id=?", new String[]{"" + trackId});
        updateSummaryVersion(this.sportSummary);
        return this.sportSummary;
    }

    protected List<? extends CyclingDetail> getCadenceData(long trackId, SportSummary summary) {
        List<? extends CyclingDetail> detailList = this.mSyncCadenceSummaryManager.selectAll((Context) this.mContextRef.get(), "track_id>=?", new String[]{"" + trackId}, null, null);
        CurveInfoUtils.optimizeCadenceWithoutPauseTime(summary, detailList);
        return detailList;
    }

    protected CurvePointsInfo getXCordinateDisInfo(List<? extends HeartRate> heartRateList, long trackId, float totalDistance) {
        CurvePointsInfo info = new CurvePointsInfo();
        float standard = totalDistance / 5.0f;
        float dis = 0.0f;
        int index = 0;
        int locationIndex = 0;
        long lastEndTimeStamp = ((HeartRate) heartRateList.get(0)).getTimestamp();
        long lastStartTimeStamp = trackId;
        int i = 0;
        while (i < heartRateList.size()) {
            HeartRate rate = (HeartRate) heartRateList.get(i);
            float disDiff = rate.getDistance() / 1000.0f;
            dis += disDiff;
            if (dis > ((float) (index + 1)) * standard) {
                Debug.m5i("CurveInfo", "dis: " + dis);
                dis -= disDiff;
                index++;
                i--;
                locationIndex = addDataToCurveInfo(info, locationIndex, lastStartTimeStamp, lastEndTimeStamp - lastStartTimeStamp, index, standard);
                lastStartTimeStamp = lastEndTimeStamp;
            } else {
                lastEndTimeStamp = rate.getTimestamp();
            }
            i++;
        }
        addDataToCurveInfo(info, locationIndex, lastStartTimeStamp, lastEndTimeStamp - lastStartTimeStamp, index + 1, standard);
        return info;
    }

    public void setLocationList(List<? extends SportLocationData> locationList) {
        this.locationList = locationList;
    }

    public void setHeartRateDatas(List<? extends HeartRate> heartRateDatas) {
        this.heartRateDatas = heartRateDatas;
    }

    private void updateSummaryVersion(SportSummary sportSummary) {
        if (sportSummary != null) {
            this.mVersion = ((OutdoorSportSummary) sportSummary).getVersion();
        }
    }

    public String[] getXDisAxis() {
        if (this.sportSummary == null) {
            return null;
        }
        double totalDis = UnitConvertUtils.convertDistanceToMileOrKm((double) (this.sportSummary.getDistance() / 1000.0f));
        if (totalDis < 0.5d) {
            return new String[]{"0", DataFormatUtils.parseFormattedRealNumber(totalDis, false)};
        }
        double standardX = totalDis / 5.0d;
        String[] xAxis = new String[6];
        xAxis[0] = "0";
        for (int i = 1; i < 6; i++) {
            xAxis[i] = DataFormatUtils.parseFormattedRealNumber(((double) i) * standardX, false);
        }
        return xAxis;
    }

    public float getXDisRange() {
        if (this.sportSummary != null) {
            return this.sportSummary.getDistance();
        }
        return 0.0f;
    }
}
