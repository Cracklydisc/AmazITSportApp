package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class DistanceAltitudeCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    private boolean mIsInitAltitude = false;
    private boolean mIsInitLastDis = false;
    List<? extends SportLocationData> mLocationDatas = null;
    private float mMaxAltitude = -20000.0f;
    private float mMinAltitude = Float.MAX_VALUE;
    SportSummary mSummary = null;
    private SyncDatabaseManager<HeartRate> mSyncHeartManager = null;
    private SyncDatabaseManager<SportLocationData> mSyncLocationManager = null;
    private SyncDatabaseManager<SportSummary> mSyncSportSummaryManager = null;
    private float mTotalDistance = 0.0f;
    private long mTrackId = -1;

    public DistanceAltitudeCurveInfo(Context context) {
        super(context);
        this.mSyncHeartManager = new SyncDatabaseManager(HeartRateDao.getInstance(context.getApplicationContext()));
        this.mSyncLocationManager = new SyncDatabaseManager(LocationDataDao.getInstance(context.getApplicationContext()));
        this.mSyncSportSummaryManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context.getApplicationContext()));
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            this.mLocationDatas = getLocationData(this.mTrackId);
            this.mSummary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, this.mSummary);
            if (this.mLocationDatas == null || this.mHeartRateList == null || this.mLocationDatas.isEmpty() || this.mHeartRateList.isEmpty()) {
                return null;
            }
            this.mTotalDistance = this.mSummary.getDistance() / 1000.0f;
            CurvePointsInfo info = getXCordinateDisInfo(this.mHeartRateList, this.mTrackId, this.mTotalDistance);
            optimizeCurveData(info);
            configCordinateinfo(info, this.mTotalDistance);
            return info;
        }
    }

    public String[] getXAxis() {
        if (this.mTotalDistance < 0.5f) {
            return new String[]{"0", DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(String.valueOf(this.mTotalDistance))), false)};
        }
        float standardX = this.mTotalDistance / 5.0f;
        String[] xAxis = new String[6];
        xAxis[0] = "0";
        for (int i = 1; i < 6; i++) {
            xAxis[i] = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm((double) (((float) i) * standardX)), false);
        }
        return xAxis;
    }

    public String[] getYAxis() {
        if (Float.compare(this.mMinAltitude, Float.MAX_VALUE) == 0 || Float.compare(this.mMaxAltitude, Float.MIN_VALUE) == 0 || Float.compare(this.mMaxAltitude, -20000.0f) == 0 || Float.compare(this.mMinAltitude, -20000.0f) == 0) {
            return null;
        }
        if (Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) this.mMaxAltitude, this.mSummary.getSportType())) - Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) this.mMinAltitude, this.mSummary.getSportType())) < 5) {
            return new String[]{String.valueOf((int) UnitConvertUtils.convertDistanceToFtOrMeter((double) this.mMinAltitude, this.mSummary.getSportType())), String.valueOf((int) UnitConvertUtils.convertDistanceToFtOrMeter((double) this.mMaxAltitude, this.mSummary.getSportType()))};
        }
        float standardY = (this.mMaxAltitude - this.mMinAltitude) / 5.0f;
        String[] yAxis = new String[6];
        for (int i = 0; i < 6; i++) {
            yAxis[i] = String.valueOf((int) UnitConvertUtils.convertDistanceToFtOrMeter((double) ((((float) i) * standardY) + this.mMinAltitude), this.mSummary.getSportType()));
        }
        return yAxis;
    }

    public float getAveValue() {
        return 0.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.no_altitude_hint);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        for (int j = locationIndex; j < this.mLocationDatas.size(); j++) {
            SportLocationData locationData = (SportLocationData) this.mLocationDatas.get(j);
            long time = locationData.mTimestamp;
            float altitude = locationData.mAltitude;
            if (Float.compare(altitude, -20000.0f) != 0) {
                if (altitude > this.mMaxAltitude) {
                    this.mMaxAltitude = altitude;
                }
                if (altitude < this.mMinAltitude) {
                    this.mMinAltitude = altitude;
                }
                long timeDiff = time - (lastStartTimeStamp - this.mTrackId);
                if ((timeDiff > totalTimestamp && index < 5) || timeDiff < 0) {
                    Debug.m5i("CurveInfo", "tiDiff:" + timeDiff);
                    break;
                }
                float disPoint = (((float) (index - 1)) * standard) + ((((float) timeDiff) / ((float) totalTimestamp)) * standard);
                if (disPoint > this.mTotalDistance) {
                    if (this.mIsInitLastDis) {
                        break;
                    }
                    disPoint = this.mTotalDistance;
                    this.mIsInitAltitude = true;
                }
                if (!this.mIsInitAltitude) {
                    info.mCurvePoints.add(new CurvePoint(0.0f, (float) UnitConvertUtils.convertDistanceToFtOrMeter((double) altitude, this.mSummary.getSportType())));
                    this.mIsInitAltitude = true;
                }
                info.mCurvePoints.add(new CurvePoint(disPoint, (float) UnitConvertUtils.convertDistanceToFtOrMeter((double) altitude, this.mSummary.getSportType())));
                locationIndex++;
            } else {
                locationIndex++;
            }
        }
        return locationIndex;
    }
}
