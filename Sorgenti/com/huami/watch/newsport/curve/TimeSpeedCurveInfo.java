package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class TimeSpeedCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    List<? extends SportLocationData> mLocationDatas = null;
    private int mMaxIndex = -1;
    private float mMaxSpeed = 0.0f;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeSpeedCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, summary);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mHeartRateList == null || this.mHeartRateList.isEmpty()) {
                return null;
            }
            CurvePointsInfo info = getXCordinateTimeInfo(this.mHeartRateList, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, -1);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateTimeInfo(List heartList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (heartList != null) {
            int i = 0;
            for (Object heart : heartList) {
                long time = ((HeartRate) heart).getTimestamp() - this.sportSummary.getTrackId();
                float speed = Float.compare(((HeartRate) heart).getPace(), 0.0f) == 0 ? 0.0f : 1.0f / ((HeartRate) heart).getPace();
                if (speed > this.mMaxSpeed) {
                    this.mMaxSpeed = speed;
                    this.mMaxIndex = i;
                }
                info.mCurvePoints.add(new CurvePoint((float) time, speed));
                i++;
            }
        }
        info.mMaxValue = this.mMaxSpeed;
        info.mMinValue = 0.0f;
        return info;
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare(this.mMaxSpeed, 0.0f) == 0) {
            return null;
        }
        double maxSpeed = UnitConvertUtils.convertSpeedToKmPerHour((double) this.mMaxSpeed);
        if (this.mMaxSpeed < 0.5f) {
            String[] yAxis = new String[2];
            yAxis[1] = "0";
            yAxis[0] = String.format("%.2f", new Object[]{Double.valueOf(maxSpeed)});
            return yAxis;
        }
        double standardY = maxSpeed / 3.0d;
        yAxis = new String[4];
        yAxis[2] = String.format("%.2f", new Object[]{Double.valueOf(standardY)});
        yAxis[1] = String.format("%.2f", new Object[]{Double.valueOf(2.0d * standardY)});
        yAxis[0] = DataFormatUtils.parseFormattedRealNumber(maxSpeed, true);
        return yAxis;
    }

    public float getAveValue() {
        if (this.sportSummary != null) {
            return ((OutdoorSportSummary) this.sportSummary).getTotalSpeed();
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_speed_data);
    }

    public String[] getMinOrMaxValueDisplay() {
        display = new String[2];
        display[0] = String.format("%.2f", new Object[]{Double.valueOf(UnitConvertUtils.convertSpeedToKmPerHour((double) this.mMaxSpeed))});
        display[1] = String.format("%.2f", new Object[]{Double.valueOf(UnitConvertUtils.convertSpeedToKmPerHour(0.0d))});
        return display;
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
