package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import java.util.List;

public class TimePaceCurveInfo extends CurveInfo {
    private float mDefaultPace = 5.999f;
    List<? extends HeartRate> mHeartRateList = null;
    private int mMaxIndex = -1;
    private float mMaxPace = 0.0f;
    private String mMaxPaceStr;
    private int mMinIndex = -1;
    private float mMinPace = Float.MAX_VALUE;
    private String mMinPaceStr;
    private int mSportType = -1;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimePaceCurveInfo(Context context, int sportType) {
        super(context);
        this.mSportType = sportType;
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, summary);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mHeartRateList == null || this.mHeartRateList.isEmpty()) {
                return null;
            }
            CurvePointsInfo info = getXCordinateTimeInfo(this.mHeartRateList, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, this.mMinIndex);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateTimeInfo(List heartList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (heartList != null) {
            int i = 0;
            for (Object heart : heartList) {
                long time = ((HeartRate) heart).getTimestamp() - this.sportSummary.getTrackId();
                float pace = ((HeartRate) heart).getPace();
                float tempPace = pace;
                if (SportType.isSwimMode(this.mSportType)) {
                    tempPace = SportDataFilterUtils.parsePacePer100Meter(((HeartRate) heart).getPace(), ((OutdoorSportSummary) this.sportSummary).getUnit());
                } else {
                    tempPace = SportDataFilterUtils.parsePace(((HeartRate) heart).getPace());
                }
                if (Float.compare(tempPace, 0.0f) == 0 || isFilter(tempPace)) {
                    info.mCurvePoints.add(new CurvePoint((float) time, 0.0f));
                    i++;
                } else {
                    if (Float.compare(pace, 0.0f) == 0) {
                        pace = this.mDefaultPace;
                    }
                    if (pace > this.mMaxPace) {
                        this.mMaxPace = pace;
                        this.mMaxIndex = i;
                    }
                    if (pace < this.mMinPace) {
                        this.mMinPace = pace;
                        this.mMinIndex = i;
                    }
                    if (SportType.isSwimMode(this.mSportType)) {
                        pace = SportDataFilterUtils.parsePacePer100Meter(((HeartRate) heart).getPace(), ((OutdoorSportSummary) this.sportSummary).getUnit());
                    } else {
                        pace = SportDataFilterUtils.parsePace(((HeartRate) heart).getPace());
                    }
                    info.mCurvePoints.add(new CurvePoint((float) time, pace));
                    i++;
                }
            }
            if (SportType.isSwimMode(this.mSportType)) {
                this.mMaxPaceStr = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(this.mMaxPace, ((OutdoorSportSummary) this.sportSummary).getUnit()));
                this.mMinPaceStr = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(this.mMinPace, ((OutdoorSportSummary) this.sportSummary).getUnit()));
            } else {
                this.mMaxPaceStr = DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(this.mMaxPace));
                this.mMinPaceStr = DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(this.mMinPace));
            }
            this.mMaxPace = optimizePace(this.mMaxPace, true);
            this.mMinPace = optimizePace(this.mMinPace, false);
            if (info.mCurvePoints != null) {
                for (CurvePoint point : info.mCurvePoints) {
                    if (point.getCurveValue() > 0.0f) {
                        point.setCurveValue(this.mMaxPace - (point.getCurveValue() - this.mMinPace));
                    }
                }
            }
            info.mMaxValue = this.mMaxPace;
            info.mMinValue = this.mMinPace;
        }
        return info;
    }

    private boolean isFilter(float pace) {
        int unit;
        if (this.mSportType == 14 || this.mSportType == 15) {
            unit = 100;
        } else {
            unit = 1000;
        }
        if (((int) (((float) unit) * pace)) / 60 >= 15) {
            return true;
        }
        return false;
    }

    private float optimizePace(float pace, boolean isUp) {
        int unit;
        if (SportType.isSwimMode(this.mSportType)) {
            unit = 100;
        } else {
            unit = 1000;
        }
        int sport_unit = ((OutdoorSportSummary) this.sportSummary).getUnit();
        if (SportType.isSwimMode(this.mSportType)) {
            pace = SportDataFilterUtils.parsePacePer100Meter(pace, sport_unit);
        } else {
            pace = SportDataFilterUtils.parsePace(pace);
        }
        int secondPer100 = (int) (((float) unit) * pace);
        int min = secondPer100 / 60;
        if (secondPer100 % 60 == 0) {
            return pace;
        }
        if (isUp) {
            pace = (((float) (min + 1)) * 60.0f) / ((float) unit);
        } else {
            pace = (((float) min) * 60.0f) / ((float) unit);
        }
        return pace;
    }

    private boolean isShownCompleteYAxis() {
        int unit;
        if (SportType.isSwimMode(this.mSportType)) {
            unit = 100;
        } else {
            unit = 1000;
        }
        if (Math.abs((((int) (this.mMaxPace * ((float) unit))) / 60) - (((int) (this.mMinPace * ((float) unit))) / 60)) >= 3) {
            return true;
        }
        return false;
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare(this.mMinPace, Float.MAX_VALUE) == 0 || Float.compare(this.mMaxPace, 0.0f) == 0) {
            return null;
        }
        String[] yAxis;
        if (isShownCompleteYAxis()) {
            yAxis = new String[4];
            float range = (this.mMaxPace - this.mMinPace) / 3.0f;
            if (SportType.isSwimMode(this.mSportType)) {
                yAxis[3] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(this.mMaxPace);
                yAxis[2] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(this.mMinPace + (range * 2.0f));
                float value = this.mMinPace + range;
                yAxis[1] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(value);
                yAxis[0] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(value);
                return yAxis;
            }
            yAxis[3] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMaxPace);
            yAxis[2] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMinPace + (range * 2.0f));
            yAxis[1] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMinPace + range);
            yAxis[0] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMinPace);
            return yAxis;
        }
        yAxis = new String[2];
        if (SportType.isSwimMode(this.mSportType)) {
            yAxis[1] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(this.mMaxPace);
            yAxis[0] = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(this.mMinPace);
            return yAxis;
        }
        yAxis[1] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMaxPace);
        yAxis[0] = DataFormatUtils.parseSecondPerMeterToFormattedPace(this.mMinPace);
        return yAxis;
    }

    public float getAveValue() {
        if (this.sportSummary == null || Float.compare(this.mMinPace, Float.MAX_VALUE) == 0 || Float.compare(this.mMaxPace, 0.0f) == 0) {
            return -1.0f;
        }
        return this.mMaxPace - (((OutdoorSportSummary) this.sportSummary).getTotalPace() - this.mMinPace);
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_pace_data);
    }

    public String[] getMinOrMaxValueDisplay() {
        return new String[]{this.mMinPaceStr, this.mMaxPaceStr};
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
