package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class DistanceSpeedCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    private boolean mIsInitLastDis = false;
    List<? extends SportLocationData> mLocationDatas = null;
    private float mMaxSpeed = 0.0f;
    private SyncDatabaseManager<HeartRate> mSyncHeartManager = null;
    private SyncDatabaseManager<SportLocationData> mSyncLocationManager = null;
    private SyncDatabaseManager<SportSummary> mSyncSportSummaryManager = null;
    private float mTotalDistance = 0.0f;
    private long mTrackId = -1;

    public DistanceSpeedCurveInfo(Context context) {
        super(context);
        this.mSyncHeartManager = new SyncDatabaseManager(HeartRateDao.getInstance(context.getApplicationContext()));
        this.mSyncLocationManager = new SyncDatabaseManager(LocationDataDao.getInstance(context.getApplicationContext()));
        this.mSyncSportSummaryManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context.getApplicationContext()));
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, summary);
            this.mLocationDatas = getLocationData(this.mTrackId);
            if (this.mLocationDatas == null || this.mLocationDatas.isEmpty() || this.mHeartRateList == null || this.mHeartRateList.isEmpty()) {
                return null;
            }
            this.mTotalDistance = summary.getDistance() / 1000.0f;
            CurvePointsInfo info = getXCordinateDisInfo(this.mHeartRateList, this.mTrackId, this.mTotalDistance);
            optimizeCurveData(info);
            configCordinateinfo(info, this.mTotalDistance);
            return info;
        }
    }

    public String[] getXAxis() {
        if (this.mTotalDistance < 0.5f) {
            return new String[]{"0", DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(String.valueOf(this.mTotalDistance))), false)};
        }
        float standardX = this.mTotalDistance / 5.0f;
        String[] xAxis = new String[6];
        xAxis[0] = "0";
        for (int i = 1; i < 6; i++) {
            xAxis[i] = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm((double) (((float) i) * standardX)), false);
        }
        return xAxis;
    }

    public String[] getYAxis() {
        if (Float.compare(this.mMaxSpeed, 0.0f) == 0) {
            return null;
        }
        double maxSpeed = UnitConvertUtils.convertSpeedToKmPerHour((double) this.mMaxSpeed);
        if (this.mMaxSpeed < 0.5f) {
            String[] yAxis = new String[2];
            yAxis[0] = "0";
            yAxis[1] = String.format("%.1f", new Object[]{Double.valueOf(maxSpeed)});
            return yAxis;
        }
        double standardY = maxSpeed / 5.0d;
        yAxis = new String[6];
        yAxis[0] = "0";
        for (int i = 1; i < 6; i++) {
            yAxis[i] = String.format("%.1f", new Object[]{Double.valueOf(((double) i) * standardY)});
        }
        return yAxis;
    }

    public float getAveValue() {
        return 0.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_speed_data);
    }

    protected CurvePointsInfo getXCordinateDisInfo(List heartRateList, long trackId, float totalDistance) {
        if (this.mVersion >= 14) {
        }
        return super.getXCordinateDisInfo(heartRateList, trackId, totalDistance);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        if (locationIndex == 0) {
            info.mCurvePoints.add(new CurvePoint(0.0f, 0.0f));
        }
        for (int j = locationIndex; j < this.mLocationDatas.size(); j++) {
            SportLocationData locationData = (SportLocationData) this.mLocationDatas.get(j);
            long time = locationData.mTimestamp;
            float speed = locationData.mSpeed;
            if (speed > this.mMaxSpeed) {
                this.mMaxSpeed = speed;
            }
            long timeDiff = time - (lastStartTimeStamp - this.mTrackId);
            if ((timeDiff > totalTimestamp && index < 5) || timeDiff < 0) {
                Debug.m5i("CurveInfo", "tiDiff:" + timeDiff);
                break;
            }
            float disPoint = (((float) (index - 1)) * standard) + ((((float) timeDiff) / ((float) totalTimestamp)) * standard);
            if (disPoint > this.mTotalDistance) {
                if (this.mIsInitLastDis) {
                    break;
                }
                disPoint = this.mTotalDistance;
                this.mIsInitLastDis = true;
            }
            info.mCurvePoints.add(new CurvePoint(disPoint, (float) UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(speed))));
            locationIndex++;
        }
        return locationIndex;
    }
}
