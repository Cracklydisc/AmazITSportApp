package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurveInfoUtils;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class TimeAltitudeCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    List<? extends SportLocationData> mLocationDatas = null;
    private int mMaxAltitude = -20000;
    private int mMaxIndex = -1;
    private int mMinAltitude = Integer.MAX_VALUE;
    private int mMinIndex = -1;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeAltitudeCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            this.mLocationDatas = getLocationData(this.mTrackId);
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mLocationDatas == null || this.mLocationDatas.isEmpty()) {
                return null;
            }
            CurveInfoUtils.optimizeLocationWithoutPauseTime(summary, this.mLocationDatas);
            CurvePointsInfo info = getXCordinateTimeInfo(this.mLocationDatas, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, this.mMinIndex);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    private CurvePointsInfo getXCordinateTimeInfo(List locationList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (locationList != null) {
            int i = 0;
            for (Object locationData : locationList) {
                long time = ((SportLocationData) locationData).mTimestamp;
                int altitude = (int) ((SportLocationData) locationData).mAltitude;
                if (Float.compare((float) altitude, -20000.0f) != 0) {
                    if (altitude > this.mMaxAltitude) {
                        this.mMaxAltitude = altitude;
                        this.mMaxIndex = i;
                    }
                    if (altitude < this.mMinAltitude) {
                        this.mMinAltitude = altitude;
                        this.mMinIndex = i;
                    }
                    info.mCurvePoints.add(new CurvePoint((float) time, (float) altitude));
                    i++;
                }
            }
        }
        float range = (float) (this.mMaxAltitude - this.mMinAltitude);
        if (range > 0.0f) {
            if (range > 30.0f && range <= 1000.0f) {
                if (this.mMaxAltitude % 10 != 0) {
                    this.mMaxAltitude = (this.mMaxAltitude + 10) - (this.mMaxAltitude % 10);
                }
                if (this.mMinAltitude % 10 != 0) {
                    this.mMinAltitude -= this.mMinAltitude % 10;
                }
            } else if (range > 1000.0f) {
                if (this.mMaxAltitude % 100 != 0) {
                    this.mMaxAltitude = (this.mMaxAltitude + 100) - (this.mMaxAltitude % 100);
                }
                if (this.mMinAltitude % 100 != 0) {
                    this.mMinAltitude -= this.mMinAltitude % 100;
                }
            }
        }
        info.mMaxValue = (float) this.mMaxAltitude;
        info.mMinValue = (float) this.mMinAltitude;
        return info;
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare((float) this.mMinAltitude, Float.MAX_VALUE) == 0 || Float.compare((float) this.mMaxAltitude, Float.MIN_VALUE) == 0 || Float.compare((float) this.mMaxAltitude, -20000.0f) == 0 || Float.compare((float) this.mMinAltitude, -20000.0f) == 0) {
            return null;
        }
        String[] yAxis = new String[4];
        int maxAltitude = (int) Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) ((OutdoorSportSummary) getSportSummary(this.mTrackId)).getHighestAltitude(), getSportSummary(this.mTrackId).getSportType()));
        int minAltitude = (int) Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) ((OutdoorSportSummary) getSportSummary(this.mTrackId)).getLowestAltitude(), getSportSummary(this.mTrackId).getSportType()));
        LogUtil.m9i(true, "CurveInfo", "maxAltitude:" + this.mMaxAltitude + ", minAltitude:" + this.mMinAltitude);
        if (maxAltitude - minAltitude < 3) {
            return new String[]{String.valueOf(minAltitude), String.valueOf(maxAltitude)};
        }
        float range = ((float) (maxAltitude - minAltitude)) / 3.0f;
        if (range <= 30.0f || range > 1000.0f) {
            if (range > 1000.0f) {
            }
        }
        yAxis[3] = String.valueOf(minAltitude);
        yAxis[2] = String.valueOf((int) (((float) minAltitude) + range));
        yAxis[1] = String.valueOf((int) (((float) minAltitude) + (2.0f * range)));
        yAxis[0] = String.valueOf(maxAltitude);
        return yAxis;
    }

    public float getAveValue() {
        if (this.sportSummary == null) {
            return -1.0f;
        }
        int highAltitude = Math.round(((OutdoorSportSummary) this.sportSummary).getHighestAltitude());
        int lowAltitude = Math.round(((OutdoorSportSummary) this.sportSummary).getLowestAltitude());
        if (highAltitude <= -20000 || lowAltitude <= -20000) {
            return -1.0f;
        }
        return (float) Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) (((float) (lowAltitude + highAltitude)) / 2.0f), getSportSummary(this.mTrackId).getSportType()));
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.no_altitude_hint);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
