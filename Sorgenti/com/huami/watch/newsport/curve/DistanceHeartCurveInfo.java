package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.LinkedList;
import java.util.List;

public class DistanceHeartCurveInfo extends CurveInfo {
    List<HeartRate> mAvailableHeartRates = null;
    private int mCurvePintMaxIndex = -1;
    private int mCurvePointMinIndex = -1;
    List<CurvePoint> mHeartRatePoints = null;
    private int mMaxIndex = -1;
    private int mMinIndex = -1;
    private SportSummary mSportSummary = null;
    private float mTotalDistance = 0.0f;
    private long mTrackId = -1;

    public DistanceHeartCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            this.mSportSummary = getSportSummary(this.mTrackId);
            List<? extends HeartRate> heartRateList = getAllHeartRateList(startTime, endTime, this.mSportSummary);
            if (heartRateList == null || heartRateList.isEmpty()) {
                Debug.m5i("HmDistanceHeartCurveInfo", "heart list is null");
                return null;
            }
            this.mAvailableHeartRates = new LinkedList();
            for (int i = 0; i < heartRateList.size(); i++) {
                HeartRate rate = (HeartRate) heartRateList.get(i);
                if (rate != null && rate.getHeartQuality() <= 1) {
                    this.mAvailableHeartRates.add(rate);
                }
            }
            this.mHeartRatePoints = createHeartRatePointsInfo(this.mAvailableHeartRates, startTime, endTime);
            this.mTotalDistance = this.mSportSummary.getDistance() / 1000.0f;
            CurvePointsInfo info = getXCordinateDisInfo(heartRateList, this.mTrackId, this.mTotalDistance);
            optimizeCurveData(info, this.mCurvePintMaxIndex, this.mCurvePointMinIndex);
            configCordinateinfo(info, this.mTotalDistance);
            return info;
        }
    }

    public String[] getXAxis() {
        if (this.mTotalDistance < 0.5f) {
            return new String[]{"0", DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(String.valueOf(this.mTotalDistance))), false)};
        }
        float standardX = this.mTotalDistance / 5.0f;
        String[] xAxis = new String[6];
        xAxis[0] = "0";
        for (int i = 1; i < 6; i++) {
            xAxis[i] = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm((double) (((float) i) * standardX)), false);
        }
        return xAxis;
    }

    public String[] getYAxis() {
        return null;
    }

    public float getAveValue() {
        if (this.mSportSummary != null) {
            return (float) ((OutdoorSportSummary) this.mSportSummary).getAvgHeartRate();
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.no_heartrate_data_hint);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        for (int j = locationIndex; j < this.mHeartRatePoints.size(); j++) {
            CurvePoint heartData = (CurvePoint) this.mHeartRatePoints.get(j);
            long time = (long) (heartData.getX() * 60000.0f);
            float heartValue = heartData.getCurveValue();
            long timeDiff = time - (lastStartTimeStamp - this.mTrackId);
            if ((timeDiff > totalTimestamp && index < 5) || timeDiff < 0) {
                break;
            }
            float disPoint = (((float) (index - 1)) * standard) + ((((float) timeDiff) / ((float) totalTimestamp)) * standard);
            if (disPoint > this.mTotalDistance) {
                disPoint = this.mTotalDistance;
            }
            info.mCurvePoints.add(new CurvePoint(disPoint, heartValue));
            if (j == this.mMinIndex) {
                this.mCurvePointMinIndex = info.mCurvePoints.size() - 1;
            }
            if (j == this.mMaxIndex) {
                this.mCurvePintMaxIndex = info.mCurvePoints.size() - 1;
            }
            locationIndex++;
        }
        return locationIndex;
    }

    private List<CurvePoint> createHeartRatePointsInfo(List<? extends HeartRate> heartRates, long startTime, long endTime) {
        long timeSpan = endTime - startTime;
        List<CurvePoint> heartPoints = new LinkedList();
        float pointIndex = 0.0f;
        float avgTotal = 0.0f;
        float avgNum = 0.0f;
        float maxIndex = 0.0f;
        float minIndex = 0.0f;
        float maxHeart = 0.0f;
        float minHeart = Float.MAX_VALUE;
        long minTime = 0;
        long maxTime = 0;
        int i = 0;
        while (i < heartRates.size()) {
            HeartRate heartRate = (HeartRate) heartRates.get(i);
            float avg;
            if (timeSpan < 60000) {
                avgTotal += (float) heartRate.getHeartRate();
                avgNum += 1.0f;
                if (i == heartRates.size() - 1) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    pointIndex += 1.0f;
                }
            } else {
                if (((float) heartRate.getHeartRate()) >= maxHeart) {
                    maxIndex = pointIndex;
                    maxTime = heartRate.getTimestamp() - startTime;
                    maxHeart = (float) heartRate.getHeartRate();
                }
                if (((float) heartRate.getHeartRate()) <= minHeart) {
                    minIndex = pointIndex;
                    minTime = heartRate.getTimestamp() - startTime;
                    minHeart = (float) heartRate.getHeartRate();
                }
                if ((heartRate.getHeartRate() > 0 && ((float) (heartRate.getTimestamp() - startTime)) <= 60000.0f * pointIndex) || pointIndex == 0.0f) {
                    avgTotal += (float) heartRate.getHeartRate();
                    avgNum += 1.0f;
                }
                if (((float) (heartRate.getTimestamp() - startTime)) > 60000.0f * pointIndex) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += 1.0f;
                    i--;
                } else if (((float) (heartRate.getTimestamp() - startTime)) >= 60000.0f * pointIndex) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    if (((float) (heartRate.getTimestamp() - startTime)) > 60000.0f * pointIndex) {
                        i--;
                    }
                    pointIndex += 1.0f;
                } else if (i == heartRates.size() - 1 && timeSpan > 60000) {
                    float scale = ((float) ((long) (((float) (heartRate.getTimestamp() - startTime)) - ((pointIndex - 1.0f) * 60000.0f)))) / 60000.0f;
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    heartPoints.add(new CurvePoint((pointIndex - 1.0f) + scale, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += scale;
                }
            }
            i++;
        }
        if (timeSpan > 60000) {
            if (!(minHeart == Float.MAX_VALUE || minTime == 0)) {
                CurvePoint hr = new CurvePoint(((float) minTime) / 60000.0f, minHeart);
                if (minIndex < ((float) (heartPoints.size() - 1))) {
                    heartPoints.remove((CurvePoint) heartPoints.get((int) minIndex));
                }
                heartPoints.add((int) minIndex, hr);
                this.mMinIndex = (int) minIndex;
            }
            if (!(maxHeart == 0.0f || maxTime == 0)) {
                CurvePoint curvePoint = new CurvePoint(((float) maxTime) / 60000.0f, maxHeart);
                CurvePoint maxPoint = (CurvePoint) heartPoints.get((int) maxIndex);
                if (minIndex != maxIndex) {
                    if (maxIndex < ((float) (heartPoints.size() - 1))) {
                        heartPoints.remove(maxPoint);
                    }
                    heartPoints.add((int) maxIndex, curvePoint);
                    this.mMaxIndex = (int) maxIndex;
                } else if (maxTime < minTime) {
                    heartPoints.add((int) maxIndex, curvePoint);
                    this.mMaxIndex = (int) maxIndex;
                } else {
                    heartPoints.add(((int) maxIndex) + 1, curvePoint);
                    this.mMaxIndex = ((int) maxIndex) + 1;
                }
            }
        }
        return heartPoints;
    }
}
