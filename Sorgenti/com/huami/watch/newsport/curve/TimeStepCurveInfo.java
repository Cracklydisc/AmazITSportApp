package com.huami.watch.newsport.curve;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.CurveInfo.CurvePoint;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class TimeStepCurveInfo extends CurveInfo {
    List<? extends HeartRate> mHeartRateList = null;
    private int mMaxIndex = -1;
    private int mMaxStepFreq = 0;
    private int mMinIndex = -1;
    private int mMinStepFreq = Integer.MAX_VALUE;
    private long mTotalTime = 0;
    private long mTrackId = -1;

    public TimeStepCurveInfo(Context context) {
        super(context);
    }

    public CurvePointsInfo getCurvePoints(Object... params) {
        if (params == null || params.length < 3) {
            throw new IllegalArgumentException("param should be set to start time and end time");
        } else if (!isContextAvaiable()) {
            return null;
        } else {
            this.mTrackId = ((Long) params[0]).longValue();
            long startTime = ((Long) params[1]).longValue();
            long endTime = ((Long) params[2]).longValue();
            SportSummary summary = getSportSummary(this.mTrackId);
            this.mHeartRateList = getAllHeartRateList(startTime, endTime, this.sportSummary);
            this.mTotalTime = (long) summary.getSportDuration();
            if (this.mHeartRateList == null || this.mHeartRateList.isEmpty()) {
                return null;
            }
            CurvePointsInfo info = getXCordinateTimeInfo(this.mHeartRateList, this.mTrackId, this.mTotalTime);
            optimizeCurveData(info, this.mMaxIndex, -1);
            configCordinateinfo(info, (float) this.mTotalTime);
            return info;
        }
    }

    protected CurvePointsInfo getXCordinateTimeInfo(List heartList, long trackId, long totalTime) {
        CurvePointsInfo info = new CurvePointsInfo();
        if (heartList != null) {
            int i = 0;
            for (Object heart : heartList) {
                long time = ((HeartRate) heart).getTimestamp() - this.sportSummary.getTrackId();
                int stepFreq = UnitConvertUtils.convertStepFreqToTimesPerMin((double) ((HeartRate) heart).getStepFreq());
                if (stepFreq > this.mMaxStepFreq) {
                    this.mMaxStepFreq = stepFreq;
                    this.mMaxIndex = i;
                }
                if (stepFreq < this.mMinStepFreq) {
                    this.mMinStepFreq = stepFreq;
                    this.mMinIndex = i;
                }
                info.mCurvePoints.add(new CurvePoint((float) time, (float) stepFreq));
                i++;
            }
        }
        if (this.mMaxStepFreq % 10 != 0) {
            this.mMaxStepFreq = (this.mMaxStepFreq + 10) - (this.mMaxStepFreq % 10);
        }
        if (this.mMinStepFreq % 10 != 0) {
            this.mMinStepFreq -= this.mMinStepFreq % 10;
        }
        info.mMinValue = (float) this.mMinStepFreq;
        info.mMaxValue = (float) this.mMaxStepFreq;
        return info;
    }

    public String[] getXAxis() {
        if (this.mTotalTime < 5000) {
            return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
        }
        long timeSpan = this.mTotalTime / 3;
        return new String[]{"0", DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(2 * timeSpan), DataFormatUtils.parseMillSecondToDefaultFormattedTime(this.mTotalTime)};
    }

    public String[] getYAxis() {
        if (Float.compare((float) this.mMaxStepFreq, 0.0f) == 0 || Float.compare((float) this.mMinStepFreq, Float.MAX_VALUE) == 0) {
            return null;
        }
        int range = this.mMaxStepFreq - this.mMinStepFreq;
        if (range < 30) {
            return new String[]{"" + minStepFreq, "" + maxStepFreq};
        }
        double standardY = (double) (range / 3);
        return new String[]{String.valueOf(minStepFreq), String.valueOf((int) (((double) minStepFreq) + standardY)), String.valueOf((int) (((double) minStepFreq) + (2.0d * standardY))), String.valueOf(maxStepFreq)};
    }

    public float getAveValue() {
        if (this.sportSummary != null) {
            return (float) UnitConvertUtils.convertStepFreqToTimesPerMin((double) ((OutdoorSportSummary) this.sportSummary).getStepFreq());
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        if (this.mContextRef == null || this.mContextRef.get() == null) {
            return null;
        }
        return ((Context) this.mContextRef.get()).getString(C0532R.string.not_step_freq_data);
    }

    public int addDataToCurveInfo(CurvePointsInfo info, int locationIndex, long lastStartTimeStamp, long totalTimestamp, int index, float standard) {
        return 0;
    }
}
