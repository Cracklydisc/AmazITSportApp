package com.huami.watch.newsport.curve.controller;

import android.content.Context;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.curve.CurveInfo;
import com.huami.watch.newsport.curve.CurveInfo.CurvePointsInfo;
import com.huami.watch.newsport.curve.DistanceAltitudeCurveInfo;
import com.huami.watch.newsport.curve.DistanceHeartCurveInfo;
import com.huami.watch.newsport.curve.DistanceSpeedCurveInfo;
import com.huami.watch.newsport.curve.TimeAltitudeCurveInfo;
import com.huami.watch.newsport.curve.TimeCadenceCurveInfo;
import com.huami.watch.newsport.curve.TimeHeartCurveInfo;
import com.huami.watch.newsport.curve.TimePaceCurveInfo;
import com.huami.watch.newsport.curve.TimeSpeedCurveInfo;
import com.huami.watch.newsport.curve.TimeStepCurveInfo;
import com.huami.watch.newsport.curve.TimeStrokeSpeedCurveInfo;
import com.huami.watch.newsport.gps.model.SportLocationData;
import java.util.List;

public class CurveInfoPointFactory {
    private Context mContext;
    private CurveInfo mCurveInfo;
    private int mSportType = -1;
    private XCoordinateType xType;
    private YCoordinateType yType;

    public enum XCoordinateType {
        TIME(0),
        DISTANCE(1);
        
        private int index;

        private XCoordinateType(int index) {
            this.index = index;
        }
    }

    public enum YCoordinateType {
        SPEED(0),
        HEART_RATE(1),
        ALTITUDE(2),
        PACE(3),
        STEPFERQ(4),
        STROKE(5),
        CADENCE(6);
        
        private int index;

        private YCoordinateType(int index) {
            this.index = index;
        }
    }

    public CurveInfoPointFactory(Context context, XCoordinateType xCoordinateType, YCoordinateType yCoordinateType, int sportType) {
        this.mContext = context;
        this.mSportType = sportType;
        this.xType = xCoordinateType;
        this.yType = yCoordinateType;
        initCurveInfo();
    }

    private void initCurveInfo() {
        switch (this.xType) {
            case DISTANCE:
                if (this.yType == YCoordinateType.SPEED) {
                    this.mCurveInfo = new DistanceSpeedCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.ALTITUDE) {
                    this.mCurveInfo = new DistanceAltitudeCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.HEART_RATE) {
                    this.mCurveInfo = new DistanceHeartCurveInfo(this.mContext);
                    return;
                } else {
                    return;
                }
            case TIME:
                if (this.yType == YCoordinateType.SPEED) {
                    this.mCurveInfo = new TimeSpeedCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.ALTITUDE) {
                    this.mCurveInfo = new TimeAltitudeCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.HEART_RATE) {
                    this.mCurveInfo = new TimeHeartCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.PACE) {
                    this.mCurveInfo = new TimePaceCurveInfo(this.mContext, this.mSportType);
                    return;
                } else if (this.yType == YCoordinateType.STEPFERQ) {
                    this.mCurveInfo = new TimeStepCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.STROKE) {
                    this.mCurveInfo = new TimeStrokeSpeedCurveInfo(this.mContext);
                    return;
                } else if (this.yType == YCoordinateType.CADENCE) {
                    this.mCurveInfo = new TimeCadenceCurveInfo(this.mContext);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public CurvePointsInfo getCurvePointInfo(Object... params) {
        if (this.mCurveInfo != null) {
            return this.mCurveInfo.getCurvePoints(params);
        }
        return null;
    }

    public String[] getMinOrMaxValueDisplay() {
        if (this.mCurveInfo != null) {
            return this.mCurveInfo.getMinOrMaxValueDisplay();
        }
        return null;
    }

    public float getCurveAveValue() {
        if (this.mCurveInfo != null) {
            return this.mCurveInfo.getAveValue();
        }
        return -1.0f;
    }

    public String getNoDataHint() {
        return this.mCurveInfo.getNoDataHint();
    }

    public void setLocationList(List<? extends SportLocationData> locationList) {
        if (this.mCurveInfo != null) {
            this.mCurveInfo.setLocationList(locationList);
        }
    }

    public void setHeartRateDatas(List<? extends HeartRate> heartRateDatas) {
        if (this.mCurveInfo != null) {
            this.mCurveInfo.setHeartRateDatas(heartRateDatas);
        }
    }

    public String[] getXDisAxis() {
        if (this.mCurveInfo != null) {
            return this.mCurveInfo.getXDisAxis();
        }
        return null;
    }

    public String[] getXTimeAxis() {
        if (this.mCurveInfo != null) {
            return this.mCurveInfo.getXAxis();
        }
        return null;
    }

    public float getXRange() {
        if (this.mCurveInfo != null) {
            this.mCurveInfo.getXDisRange();
        }
        return 0.0f;
    }
}
