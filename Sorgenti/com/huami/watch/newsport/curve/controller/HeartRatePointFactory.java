package com.huami.watch.newsport.curve.controller;

import android.content.Context;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class HeartRatePointFactory {

    public static class HeartRatePoint {
        private float mHeartRateValue = -1.0f;
        private float mX = -1.0f;

        public HeartRatePoint(float x, float heartRateValue) {
            this.mX = x;
            this.mHeartRateValue = heartRateValue;
        }

        public String toString() {
            return "HeartRatePoint{mX=" + this.mX + ", mHeartRateValue=" + this.mHeartRateValue + '}';
        }

        public float getX() {
            return this.mX;
        }

        public float getHeartRateValue() {
            return this.mHeartRateValue;
        }
    }

    public static class HeartRatePointsInfo {
        private String[] mAxisSplit = null;
        private List<HeartRatePoint> mHeartRatePoints = new LinkedList();
        private float mRange = 1.0f;
        private String mUnit = null;

        public String toString() {
            return "HeartRatePointsInfo{mHeartRatePoints=" + this.mHeartRatePoints + ", mUnit='" + this.mUnit + '\'' + ", mAxisSplit=" + Arrays.toString(this.mAxisSplit) + ", mRange=" + this.mRange + '}';
        }

        public List<HeartRatePoint> getHeartRatePoints() {
            return this.mHeartRatePoints;
        }

        public String[] getAxisSplit() {
            return this.mAxisSplit;
        }

        public float getRange() {
            return this.mRange;
        }
    }

    public HeartRatePointsInfo createHeartRatePoint(Context context, List<? extends HeartRate> heartRates, long startTime, long endTime) {
        return createHeartRatePointsInfo(context, heartRates, startTime, endTime);
    }

    private HeartRatePointsInfo createHeartRatePointsInfo(Context context, List<? extends HeartRate> heartRates, long startTime, long endTime) {
        long timeSpan = endTime - startTime;
        if (timeSpan < 9000) {
            return new HeartRatePointsInfo();
        }
        int minutesOnePoint = ((int) (timeSpan / 9600000)) + 1;
        HeartRatePointsInfo info = new HeartRatePointsInfo();
        info.mHeartRatePoints = new LinkedList();
        float pointIndex = 0.0f;
        float avgTotal = 0.0f;
        float avgNum = 0.0f;
        float maxIndex = 0.0f;
        float minIndex = 0.0f;
        float maxHeart = 0.0f;
        float minHeart = Float.MAX_VALUE;
        long minTime = 0;
        long maxTime = 0;
        int i = 0;
        while (i < heartRates.size()) {
            HeartRate heartRate = (HeartRate) heartRates.get(i);
            float avg;
            if (timeSpan < 60000) {
                avgTotal += (float) heartRate.getHeartRate();
                avgNum += 1.0f;
                if (i == heartRates.size() - 1) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    info.mHeartRatePoints.add(new HeartRatePoint(pointIndex, avg));
                    pointIndex += 1.0f;
                }
            } else {
                if (((float) heartRate.getHeartRate()) >= maxHeart) {
                    maxIndex = pointIndex;
                    maxTime = heartRate.getTimestamp() - startTime;
                    maxHeart = (float) heartRate.getHeartRate();
                }
                if (((float) heartRate.getHeartRate()) <= minHeart) {
                    minIndex = pointIndex;
                    minTime = heartRate.getTimestamp() - startTime;
                    minHeart = (float) heartRate.getHeartRate();
                }
                if ((heartRate.getHeartRate() > 0 && ((float) (heartRate.getTimestamp() - startTime)) <= (60000.0f * pointIndex) * ((float) minutesOnePoint)) || pointIndex == 0.0f) {
                    avgTotal += (float) heartRate.getHeartRate();
                    avgNum += 1.0f;
                }
                if (((float) (heartRate.getTimestamp() - startTime)) > (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    info.mHeartRatePoints.add(new HeartRatePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += 1.0f;
                    i--;
                } else if (((float) (heartRate.getTimestamp() - startTime)) >= (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    info.mHeartRatePoints.add(new HeartRatePoint(pointIndex, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    if (((float) (heartRate.getTimestamp() - startTime)) > (60000.0f * pointIndex) * ((float) minutesOnePoint)) {
                        i--;
                    }
                    pointIndex += 1.0f;
                } else if (i == heartRates.size() - 1 && timeSpan > 60000) {
                    long time = (long) (((float) (heartRate.getTimestamp() - startTime)) - (((pointIndex - 1.0f) * 60000.0f) * ((float) minutesOnePoint)));
                    Debug.m5i("HeartRatePointFactory", "the last time span:" + time);
                    float scale = ((float) time) / ((float) (60000 * minutesOnePoint));
                    avg = 0.0f;
                    if (avgNum != 0.0f) {
                        avg = avgTotal / avgNum;
                    }
                    Debug.m5i("HeartRatePointFactory", "the last avg heart rate: " + avg);
                    info.mHeartRatePoints.add(new HeartRatePoint((pointIndex - 1.0f) + scale, avg));
                    avgNum = 0.0f;
                    avgTotal = 0.0f;
                    pointIndex += scale;
                }
            }
            i++;
        }
        if (timeSpan > 60000) {
            HeartRatePoint heartRatePoint;
            if (!(minHeart == Float.MAX_VALUE || minTime == 0)) {
                heartRatePoint = new HeartRatePoint(((float) minTime) / ((float) (60000 * minutesOnePoint)), minHeart);
                if (minIndex < ((float) (info.mHeartRatePoints.size() - 1))) {
                    info.mHeartRatePoints.remove((HeartRatePoint) info.mHeartRatePoints.get((int) minIndex));
                }
                info.mHeartRatePoints.add((int) minIndex, heartRatePoint);
            }
            if (!(maxHeart == 0.0f || maxTime == 0)) {
                heartRatePoint = new HeartRatePoint(((float) maxTime) / ((float) (60000 * minutesOnePoint)), maxHeart);
                HeartRatePoint maxPoint = (HeartRatePoint) info.mHeartRatePoints.get((int) maxIndex);
                if (minIndex != maxIndex) {
                    if (maxIndex < ((float) (info.mHeartRatePoints.size() - 1))) {
                        info.mHeartRatePoints.remove(maxPoint);
                    }
                    info.mHeartRatePoints.add((int) maxIndex, heartRatePoint);
                } else if (maxTime < minTime) {
                    info.mHeartRatePoints.add((int) maxIndex, heartRatePoint);
                } else {
                    info.mHeartRatePoints.add(((int) maxIndex) + 1, heartRatePoint);
                }
            }
        }
        pointIndex = (((float) timeSpan) / ((float) (60000 * minutesOnePoint))) + 1.0f;
        if (pointIndex >= 2.0f) {
            info.mRange = pointIndex - 1.0f;
        } else {
            info.mRange = 1.0f;
        }
        info.mAxisSplit = new String[4];
        String timeFormat = context.getString(C0532R.string.heartrate_x_axis_time_format);
        info.mAxisSplit[0] = "0";
        info.mAxisSplit[1] = DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan / 3);
        info.mAxisSplit[2] = DataFormatUtils.parseMillSecondToDefaultFormattedTime((2 * timeSpan) / 3);
        info.mAxisSplit[3] = DataFormatUtils.parseMillSecondToDefaultFormattedTime(timeSpan);
        return info;
    }
}
