package com.huami.watch.newsport.train.controller;

import android.content.Context;
import android.content.Intent;
import android.telecom.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.OutdoorSportDelegate;
import com.huami.watch.newsport.train.model.TrainConfig;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainSnapShot;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.OnDismissListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class SportTrainManager {
    private static final String TAG = SportTrainManager.class.getName();
    private List<TrainUnit> mCompletedUnitList = new ArrayList();
    private Context mContext = null;
    private TrainUnit mCurrentTrainUnit = null;
    private int mDisTrainStatus = 0;
    private boolean mIsResetTrainUnit = false;
    private boolean mIsSingleTargetType = true;
    private long mLapPausedTime = 0;
    private int mLastLapStepCount = 0;
    private OutdoorSportDelegate mOutDelegate = null;
    private ITrainUnitAction mTrainCallback = null;
    private TrainProgram mTrainProgram = null;
    private List<RunningInfoPerLap> mTrainUnitLapList = new ArrayList();

    public interface ITrainUnitAction {
        void configTrainTimeUnit(TrainUnit trainUnit);
    }

    class C06691 implements OnDismissListener {
        C06691() {
        }

        public void onDismiss() {
        }
    }

    public static final class TrainUtils {
        public static int[] getSportOrderByTrainUnit(TrainTargetType targetType, TrainRemindType remindType, boolean isSingleTarget) {
            LogUtil.m9i(true, SportTrainManager.TAG, "getSportOrderByTrainUnit, targetType:" + targetType + ", remindType:" + remindType + ", isSingleTarget:" + isSingleTarget);
            List<Integer> order = new ArrayList();
            if (targetType == TrainTargetType.DIATANCE) {
                order.add(Integer.valueOf(250));
            } else if (targetType == TrainTargetType.TIME) {
                order.add(Integer.valueOf(253));
            } else if (targetType == TrainTargetType.KEYPRESS) {
                order.add(Integer.valueOf(254));
            }
            if (remindType == TrainRemindType.HEART) {
                order.add(Integer.valueOf(5));
            } else {
                order.add(Integer.valueOf(3));
            }
            if (targetType == TrainTargetType.DIATANCE) {
                if (isSingleTarget) {
                    order.add(Integer.valueOf(251));
                }
                order.add(Integer.valueOf(1));
                if (remindType == TrainRemindType.HEART) {
                    order.add(Integer.valueOf(3));
                } else {
                    order.add(Integer.valueOf(5));
                }
            } else if (targetType == TrainTargetType.TIME) {
                if (isSingleTarget) {
                    order.add(Integer.valueOf(252));
                }
                order.add(Integer.valueOf(2));
                if (remindType == TrainRemindType.HEART) {
                    order.add(Integer.valueOf(3));
                } else {
                    order.add(Integer.valueOf(5));
                }
            } else if (targetType == TrainTargetType.KEYPRESS) {
                order.add(Integer.valueOf(1));
                if (remindType == TrainRemindType.HEART) {
                    order.add(Integer.valueOf(3));
                } else {
                    order.add(Integer.valueOf(5));
                }
            }
            if (order == null || order.size() <= 0) {
                return null;
            }
            int[] orderArray = new int[order.size()];
            for (int i = 0; i < order.size(); i++) {
                orderArray[i] = ((Integer) order.get(i)).intValue();
            }
            return orderArray;
        }
    }

    public SportTrainManager(Context context, OutdoorSportDelegate outdoorSportDelegate) {
        this.mContext = context;
        this.mOutDelegate = outdoorSportDelegate;
    }

    public boolean isDistanceTrainUnitNearlyCompleted() {
        return false;
    }

    public void setDisTrainStatus(int disTrainStatus) {
        this.mDisTrainStatus = disTrainStatus;
    }

    public void setTrainCallback(ITrainUnitAction action) {
        this.mTrainCallback = action;
    }

    public TrainUnit getCurentTrainUnit() {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "getCurentTrainUnit, " + this.mCurrentTrainUnit);
        return this.mCurrentTrainUnit;
    }

    public void addTrainProgram(TrainProgram trainProgram) {
        this.mTrainProgram = trainProgram;
        this.mIsSingleTargetType = this.mTrainProgram.isOnlySingleTrainType();
        if (this.mTrainProgram != null) {
            LogUtil.m9i(true, TAG, "addTrainProgram, trainProgram:" + this.mTrainProgram.toString());
        }
    }

    public boolean configNextUnitCmd(BaseConfig config, OutdoorSportSnapshot snapshot, long startTime) {
        if (this.mTrainProgram == null || this.mTrainProgram.getTrainUnitList().isEmpty()) {
            LogUtil.m9i(true, TAG, "configNextUnitCmd, empty");
            return false;
        }
        this.mCurrentTrainUnit = (TrainUnit) this.mTrainProgram.getTrainUnitList().remove(0);
        this.mCurrentTrainUnit.setItemOrders(getCorrentUnitOrder());
        this.mCurrentTrainUnit.setStartDistance((int) snapshot.getDistance());
        this.mCurrentTrainUnit.setStartTimeStamp(startTime);
        LogUtil.m9i(true, TAG, "configNextUnitCmd, " + this.mCurrentTrainUnit);
        this.mCompletedUnitList.add(this.mCurrentTrainUnit);
        TrainSnapShot info = snapshot.getTrainSnapShot();
        info.setCurUnitDis(0);
        info.setCurUnitTime(0);
        info.setCurTargetDis(snapshot.getTrimedDistance() + this.mCurrentTrainUnit.getTargetDistance());
        info.setCurTargetCostTime((((long) snapshot.getCurrentSecond()) * 1000) + this.mCurrentTrainUnit.getTargetTime());
        notifySlptNextTrainUnitStart(snapshot);
        Map<TrainRemindType, TrainConfig> remindMap = new HashMap();
        for (TrainConfig trainConfig : this.mCurrentTrainUnit.getTrainConfigList()) {
            remindMap.put(trainConfig.getTrainRemindType(), trainConfig);
        }
        SportConfig sportConfig = new SportConfig();
        sportConfig.setMIsSportLapMeterAlertEnabled(false);
        if (UnitConvertUtils.isImperial()) {
            sportConfig.setMIsSporKiloMeterAlertEnabled(false);
            sportConfig.setMSportKiloMeter((float) (UnitConvertUtils.convertDistanceFromMileOrKmtokm(1.0d) * 1000.0d));
        } else {
            sportConfig.setMIsSporKiloMeterAlertEnabled(false);
            sportConfig.setMSportKiloMeter(1000.0f);
        }
        sportConfig.setMIsSportAutoPauseEnabled(false);
        sportConfig.setMSportAutoPauseSpeedTh(0.0f);
        TrainConfig paceConfig = (TrainConfig) remindMap.get(TrainRemindType.PACE);
        TrainConfig heartConfig = (TrainConfig) remindMap.get(TrainRemindType.HEART);
        if (paceConfig == null) {
            sportConfig.setMSportTargetPace(0.0f);
            sportConfig.setMIsSportTargetPaceAlertEnabled(false);
        } else {
            sportConfig.setMSportFastTargetPace(paceConfig.getMaxPace());
            sportConfig.setMSportTargetPace(paceConfig.getMinPace());
            sportConfig.setMIsSportTargetPaceAlertEnabled(true);
        }
        if (heartConfig == null) {
            sportConfig.setMSportHrZoneLower(0.0f);
            sportConfig.setMSportHrZoneUpper(0.0f);
            sportConfig.setMIsSportHrZoneAlertEnabled(false);
        } else {
            sportConfig.setMSportHrZoneLower((float) heartConfig.getMinHeart());
            sportConfig.setMSportHrZoneUpper((float) heartConfig.getMaxHeart());
            sportConfig.setMIsSportHrZoneAlertEnabled(true);
        }
        switch (this.mCurrentTrainUnit.getUnitType()) {
            case DIATANCE:
                this.mDisTrainStatus = 0;
                LogUtil.m9i(true, TAG, "distance: " + this.mCurrentTrainUnit.getTargetDistance() + ", " + snapshot.getTrimedDistance());
                sportConfig.setMSportTargetDistance((float) (this.mCurrentTrainUnit.getTargetDistance() + snapshot.getTrimedDistance()));
                sportConfig.setMIsSportTargetDistanceAlertEnabled(true);
                break;
            case TIME:
                if (this.mTrainCallback != null) {
                    this.mTrainCallback.configTrainTimeUnit(this.mCurrentTrainUnit);
                }
                sportConfig.setMSportTargetDistance((float) this.mCurrentTrainUnit.getTargetDistance());
                sportConfig.setMIsSportTargetDistanceAlertEnabled(false);
                break;
            case KEYPRESS:
                sportConfig.setMSportTargetDistance((float) this.mCurrentTrainUnit.getTargetDistance());
                sportConfig.setMIsSportTargetDistanceAlertEnabled(false);
                break;
        }
        boolean enableHeart = config.isRemindSafeHeartRate();
        float configHeart = config.getSafeHeartRateHigh();
        int safeHeart = UserInfoManager.getDefaultSafeHeart(this.mContext);
        if (configHeart < 0.0f) {
            configHeart = (float) safeHeart;
        }
        sportConfig.setMSportSafeHeartRate((int) configHeart);
        sportConfig.setMIsSafeHeartRateAlertEnabled(enableHeart);
        sportConfig.setMSportTargetCalories(0.0f);
        sportConfig.setMIsSportTargetCaloriesAlertEnabled(false);
        sportConfig.setMSportTargetTimer(0.0f);
        sportConfig.setMIsSportTargetTimerAlertEnabled(false);
        Log.i(TAG, " teValueInt:" + 0, new Object[0]);
        sportConfig.setMSportTargetTrainingEffect(0);
        sportConfig.setMIs3DDistanceEnabled(config.is3DSportModel());
        sportConfig.setMIsMeasureHrInCrossCountryRun(config.isMeasureHeart());
        sportConfig.setMSwimPoolLen(0);
        SensorHubManagerWrapper.getInstance(this.mContext).setSportConfigData(sportConfig);
        SensorHubManagerWrapper.getInstance(this.mContext).setSLPTUpdateAt1Hz(true);
        return true;
    }

    public void refreshTrainDataPerSec(OutdoorSportSnapshot snapshot) {
        TrainSnapShot info = snapshot.getTrainSnapShot();
        if (!this.mCompletedUnitList.isEmpty()) {
            int currentDis = (int) (((float) this.mCurrentTrainUnit.getTargetDistance()) - (((float) info.getCurTargetDis()) - snapshot.getDistance()));
            long currentTime = this.mCurrentTrainUnit.getTargetTime() - (info.getCurTargetCostTime() - (((long) snapshot.getCurrentSecond()) * 1000));
            if (currentDis < 0) {
                LogUtil.m9i(true, TAG, "dis is below 0, " + currentDis);
                currentDis = 0;
            }
            if (currentTime < 0) {
                LogUtil.m9i(true, TAG, "time is below 0, " + currentTime);
                currentTime = 0;
            }
            info.setCurUnitDis(currentDis);
            info.setCurUnitTime(currentTime);
        }
    }

    public void computeCurTrainInfo(TrainSnapShot trainSnapShot) {
        if (this.mTrainProgram != null && !this.mTrainProgram.getTrainUnitList().isEmpty()) {
            int totalTargetDis = 0;
            long totalTargetTime = 0;
            for (TrainUnit unit : this.mTrainProgram.getTrainUnitList()) {
                totalTargetDis += unit.getTargetDistance();
                totalTargetTime += unit.getTargetTime();
            }
            trainSnapShot.setTotalTargetDistance(totalTargetDis);
            trainSnapShot.setTotalTargetCostTime(totalTargetTime);
        }
    }

    public void updateTrainLapPauseInfo(long addedPauseTime) {
        this.mLapPausedTime += addedPauseTime;
    }

    public List<RunningInfoPerLap> getTrainLapInfos() {
        return this.mTrainUnitLapList;
    }

    public RunningInfoPerLap addTrainLapInfo(OutdoorSportSnapshot snapshot) {
        Debug.m5i(TAG, "addTrainLapInfo, " + snapshot.toString());
        long totalPreTime = 0;
        int totalPreDis = 0;
        int totalPreCalorie = 0;
        float totalPreClimbDis = 0.0f;
        float totalPreClimbUp = 0.0f;
        float totalPreClimbDown = 0.0f;
        long totalRunTime = 0;
        for (RunningInfoPerLap lap : this.mTrainUnitLapList) {
            totalPreDis = (int) (((float) totalPreDis) + lap.getDistance());
            totalPreCalorie += lap.getLapCalories();
            totalPreClimbDis += lap.getClimbDistance();
            totalPreClimbUp += lap.getClimbUp();
            totalPreClimbDown += lap.getClimbDown();
            totalRunTime += lap.getCostTime();
        }
        if (!this.mTrainUnitLapList.isEmpty()) {
            totalPreTime = ((RunningInfoPerLap) this.mTrainUnitLapList.get(this.mTrainUnitLapList.size() - 1)).getTotalCostTime();
        }
        RunningInfoPerLap lap2 = new RunningInfoPerLap();
        lap2.setDistance((float) ((((int) (snapshot.getDistance() - ((float) totalPreDis))) / 10) * 10));
        long lapCostTime = (this.mOutDelegate.getCurrentTimeMillisOnRuntime() - this.mOutDelegate.getTrackId()) - totalPreTime;
        if (lapCostTime >= this.mLapPausedTime) {
            lapCostTime -= this.mLapPausedTime;
        } else {
            Debug.m5i(TAG, "addManualLapInfo, lapCostTime:" + lapCostTime + ", pausedTime:" + this.mLapPausedTime);
        }
        lap2.setCostTime(lapCostTime);
        long totalCostTime = this.mOutDelegate.getCurrentTimeMillisOnRuntime() - this.mOutDelegate.getTrackId();
        if (totalCostTime < totalRunTime + lapCostTime) {
            totalCostTime = totalRunTime + lapCostTime;
        }
        lap2.setTotalCostTime(totalCostTime);
        lap2.setPace(Float.compare(0.0f, lap2.getDistance()) == 0 ? 0.0d : (double) ((((float) lap2.getCostTime()) / 1000.0f) / lap2.getDistance()));
        lap2.setLapNum(this.mTrainUnitLapList.size());
        lap2.setLapCalories((int) (snapshot.getCalorie() - ((float) totalPreCalorie)));
        lap2.setClimbDistance(snapshot.getClimbdisascend() - totalPreClimbDis);
        lap2.setClimbUp(snapshot.getClimbUp() - totalPreClimbUp);
        lap2.setClimbDown(snapshot.getClimbDown() - totalPreClimbDown);
        if (lap2.getCostTime() != 0) {
            lap2.setLapStepFreq((float) ((int) (((long) (snapshot.getStepCount() - this.mLastLapStepCount)) / (lap2.getCostTime() / 1000))));
        }
        lap2.setTrackId(this.mOutDelegate.getTrackId());
        Long[] lngLat = snapshot.getCurGPSLonLatPoint();
        if (lngLat != null && lngLat.length >= 3) {
            float lng = ((float) lngLat[1].longValue()) / 3000000.0f;
            float lat = ((float) lngLat[2].longValue()) / 3000000.0f;
            Debug.m5i(TAG, "addManualLapInfo, lng:" + lng + ", lat:" + lat);
            lap2.setLongitude(lng);
            lap2.setLatitude(lat);
        }
        lap2.setLapType(2);
        this.mTrainUnitLapList.add(lap2);
        this.mLapPausedTime = 0;
        this.mLastLapStepCount = snapshot.getStepCount();
        return lap2;
    }

    public String getCurSLPTUnitInfo(OutdoorSportSnapshot snapshot) {
        return configSLPTUnitInfo(snapshot);
    }

    public boolean reSetItemOrderAfterTrainCompleted(OutdoorSportSnapshot snapshot) {
        if (this.mCurrentTrainUnit == null || this.mIsResetTrainUnit) {
            return false;
        }
        this.mIsResetTrainUnit = true;
        int[] orders = this.mCurrentTrainUnit.getItemOrders();
        if (orders == null || orders.length <= 0) {
            return false;
        }
        switch (this.mCurrentTrainUnit.getUnitType()) {
            case DIATANCE:
            case KEYPRESS:
                orders[0] = 2;
                break;
            case TIME:
                orders[0] = 1;
                break;
        }
        this.mCurrentTrainUnit.setIntervalMode(1);
        notifySlptNextTrainUnitStart(snapshot);
        return true;
    }

    public TrainUnit getNextTrainUnit() {
        if (this.mTrainProgram.getTrainUnitList() == null || this.mTrainProgram.getTrainUnitList().size() <= 0) {
            return null;
        }
        return (TrainUnit) this.mTrainProgram.getTrainUnitList().get(0);
    }

    public void showNextTrainUnitDialog(TrainUnit unit) {
        if (!this.mIsResetTrainUnit) {
            View view;
            PromptWindow promptWindow = new PromptWindow(this.mContext);
            if (unit != null) {
                view = LayoutInflater.from(this.mContext).inflate(C0532R.layout.interval_train_tips_layout, null, false);
                String valueStr = null;
                String valueUnitStr = null;
                TrainTargetType type = unit.getUnitType();
                if (type == TrainTargetType.TIME) {
                    valueStr = ((int) (unit.getTargetTime() / 60000)) + " ";
                    valueUnitStr = this.mContext.getString(C0532R.string.sport_target_remind_time_min);
                } else if (type == TrainTargetType.DIATANCE) {
                    valueStr = String.format("%.2f", new Object[]{Double.valueOf(UnitConvertUtils.convertDistanceToMileOrKm((double) (((float) unit.getTargetDistance()) / 1000.0f)))}) + " ";
                    valueUnitStr = this.mContext.getString(UnitConvertUtils.isImperial() ? C0532R.string.km_imperial : C0532R.string.km_metric);
                } else if (type == TrainTargetType.KEYPRESS) {
                    valueStr = "";
                    valueUnitStr = this.mContext.getString(C0532R.string.skiing_remind_click_finsh_this_train_unit);
                }
                ((NumberTextView) view.findViewById(C0532R.id.interval_run_value)).setText(valueStr);
                ((TextView) view.findViewById(C0532R.id.interval_run_unit)).setText(valueUnitStr);
            } else {
                view = LayoutInflater.from(this.mContext).inflate(C0532R.layout.interval_train_tips_complete_layout, null, false);
            }
            promptWindow.setContentView(view);
            promptWindow.setOnDismissListener(new C06691());
            promptWindow.showWithVibrator(new long[]{0, 800}, -1, VibratorUtil.getMostStrongAttributes());
        }
    }

    private void notifySlptNextTrainUnitStart(OutdoorSportSnapshot snapshot) {
        if (this.mCurrentTrainUnit == null || snapshot == null) {
            LogUtil.m9i(true, TAG, "notifySlptNextTrainUnitStart failed, unit:" + this.mCurrentTrainUnit + ", snap:" + snapshot);
            return;
        }
        LogUtil.m9i(true, TAG, "notifySlptNextTrainUnitStart");
        Intent intent = new Intent("com.huami.watchface.SlptWatchService.new_train_unit");
        intent.putExtra("clockperiod", 1);
        intent.putExtra("train_unit", configSLPTUnitInfo(snapshot));
        this.mContext.sendBroadcast(intent);
    }

    private String configSLPTUnitInfo(OutdoorSportSnapshot snapshot) {
        JSONObject jsonObject = new JSONObject();
        if (jsonObject == null) {
            LogUtil.m9i(true, TAG, "notifySlptNextTrainUnitStart, current unit can not get json");
            return "";
        }
        TrainSnapShot trainSnapShot = snapshot.getTrainSnapShot();
        try {
            jsonObject.put("curtargetDis", this.mCurrentTrainUnit.getTargetDistance());
            jsonObject.put("curtargetTime", this.mCurrentTrainUnit.getTargetTime() / 1000);
            jsonObject.put("totaltargetDis", trainSnapShot.getTotalTargetDistance());
            jsonObject.put("totaltargetTime", trainSnapShot.getTotalTargetCostTime() / 1000);
            jsonObject.put("startDis", this.mCurrentTrainUnit.getStartDistance());
            jsonObject.put("startTime", this.mCurrentTrainUnit.getStartTimeStamp() / 1000);
            jsonObject.put("interval_train_mode", this.mCurrentTrainUnit.getIntervalMode());
            int[] orders = this.mCurrentTrainUnit.getItemOrders();
            StringBuilder builder = new StringBuilder();
            if (orders != null) {
                for (int i = 0; i < orders.length; i++) {
                    builder.append(orders[i]);
                    if (i != orders.length - 1) {
                        builder.append(",");
                    }
                }
            }
            jsonObject.put("data_item", builder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtil.m9i(true, TAG, "configSLPTUnitInfo," + jsonObject);
        return jsonObject.toString();
    }

    private int[] getCorrentUnitOrder() {
        if (this.mCurrentTrainUnit == null) {
            return null;
        }
        TrainTargetType unitType = this.mCurrentTrainUnit.getUnitType();
        List<TrainConfig> configList = this.mCurrentTrainUnit.getTrainConfigList();
        TrainRemindType remindType = TrainRemindType.NONE;
        if (configList != null && configList.size() > 0) {
            remindType = ((TrainConfig) configList.get(0)).getTrainRemindType();
        }
        return TrainUtils.getSportOrderByTrainUnit(unitType, remindType, this.mIsSingleTargetType);
    }
}
