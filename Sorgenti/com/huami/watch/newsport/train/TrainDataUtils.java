package com.huami.watch.newsport.train;

import android.content.Context;
import android.util.Log;
import com.huami.watch.newsport.customtrain.utils.TrainSpUtils;
import com.huami.watch.newsport.train.model.TrainConfig;
import com.huami.watch.newsport.train.model.TrainGroup;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.xmlparser.utils.FileUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TrainDataUtils {
    private static final String TAG = TrainDataUtils.class.getSimpleName();

    public static TrainProgram getTrainProgramByResult(Context mContext) {
        String filename = TrainSpUtils.getCustomIntervttentTrainFileName(mContext);
        Log.i(TAG, "getTrainProgramByResult: filename:" + filename);
        String result = getTrainTemplateFromJsonFile(mContext, filename);
        Log.i(TAG, "getTrainProgramByResult: result:" + result);
        if (result == null || result.length() == 0) {
            return null;
        }
        TrainProgram trainProgram = getTrainProgramTemplate(result);
        Log.i(TAG, " trainProgram: is null ?:" + (trainProgram == null));
        if (trainProgram == null) {
            return trainProgram;
        }
        Log.i(TAG, " trainProgramSize:" + trainProgram.getTrainUnitList().size());
        return trainProgram;
    }

    public static TrainProgram getTrainProgramTemplate(String jsonStr) {
        try {
            JSONArray groupList = (JSONArray) new JSONObject(jsonStr).opt("list");
            List<TrainGroup> trainGroupList1 = new ArrayList();
            TrainProgram trainProgram = new TrainProgram();
            Log.i(TAG, "groupListSize:" + groupList.length());
            for (int index = 0; index < groupList.length(); index++) {
                JSONObject currentJsonObject = (JSONObject) groupList.get(index);
                TrainGroup currentTrainGroup = new TrainGroup();
                currentTrainGroup.setRepeatCounts(currentJsonObject.optInt("mRepeatCounts"));
                JSONArray trainUnitList = (JSONArray) currentJsonObject.opt("mTrainUnitList");
                List<TrainUnit> trainUnits = new ArrayList();
                for (int i = 0; i < trainUnitList.length(); i++) {
                    TrainUnit currentTrainUnit = new TrainUnit();
                    JSONObject currentUnitObject = (JSONObject) trainUnitList.get(i);
                    currentTrainUnit.setTargetDistance(currentUnitObject.optInt("mTargetDistance"));
                    currentTrainUnit.setTargetTime(currentUnitObject.optLong("mTargetTime"));
                    currentTrainUnit.setUnitType(TrainTargetType.valueOf(currentUnitObject.optString("mUnitType")));
                    List<TrainConfig> trainConfigList = getTrainConfigByParsrJson((JSONArray) currentUnitObject.opt("mTrainConfigList"));
                    if (trainConfigList != null && trainConfigList.size() > 0) {
                        currentTrainUnit.addTrainConfigList(trainConfigList);
                    }
                    trainUnits.add(currentTrainUnit);
                }
                currentTrainGroup.setTrainUnitList(trainUnits);
                trainProgram.addTrainGroup(currentTrainGroup);
            }
            return trainProgram;
        } catch (JSONException e) {
            Log.i(TAG, " jsonException ");
            e.printStackTrace();
            return null;
        }
    }

    public static String getTrainTemplateFromJsonFile(Context mContext, String filename) {
        String filePath = FileUtils.getIntervalTrainFilePath(mContext, filename);
        try {
            StringBuffer buffer = new StringBuffer("");
            InputStream inputStream = mContext.getAssets().open(filePath);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                String str = bufferedReader.readLine();
                if (str != null) {
                    buffer.append(str);
                } else {
                    inputStream.close();
                    return buffer.toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<TrainConfig> getTrainConfigByParsrJson(JSONArray jsonArray) {
        JSONException e;
        List<TrainConfig> trainConfigList = null;
        if (jsonArray != null && jsonArray.length() > 0) {
            trainConfigList = new ArrayList();
            int i = 0;
            TrainConfig currnetConfig = null;
            while (i < jsonArray.length() - 1) {
                TrainConfig currnetConfig2;
                try {
                    JSONObject currentObject = (JSONObject) jsonArray.get(i);
                    if (currentObject.optString("mTrainRemindType") == null || currentObject.optString("mTrainRemindType").length() > 0) {
                        currnetConfig2 = new TrainConfig(TrainRemindType.valueOf(currentObject.optString("mTrainRemindType")));
                    } else {
                        currnetConfig2 = new TrainConfig(TrainRemindType.valueOf(currentObject.optString("mTrainRemindType")));
                    }
                    try {
                        currnetConfig2.setMaxHeart(currentObject.optInt("mMaxHeart"));
                        currnetConfig2.setMaxPace((float) currentObject.optDouble("mMaxPace"));
                        currnetConfig2.setMinHeart(currentObject.optInt("mMinHeart"));
                        currnetConfig2.setMinPace((float) currentObject.optDouble("mMinPace"));
                        trainConfigList.add(currnetConfig2);
                        i++;
                        currnetConfig = currnetConfig2;
                    } catch (JSONException e2) {
                        e = e2;
                    }
                } catch (JSONException e3) {
                    e = e3;
                    currnetConfig2 = currnetConfig;
                }
            }
        }
        return trainConfigList;
        e.printStackTrace();
        return trainConfigList;
    }
}
