package com.huami.watch.newsport.train.model;

public enum TrainRemindType {
    NONE(0),
    PACE(1),
    HEART(2);
    
    private int mType;

    private TrainRemindType(int type) {
        this.mType = type;
    }

    public int getType() {
        return this.mType;
    }
}
