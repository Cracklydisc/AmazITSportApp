package com.huami.watch.newsport.train.model;

import com.huami.watch.newsport.utils.LogUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrainUnit {
    private int mIntervalMode = 0;
    private int[] mItemOrders = null;
    private int mStartDistance = 0;
    private long mStartTimeStamp = 0;
    private int mTargetDistance = 0;
    private long mTargetTime = 0;
    private List<TrainConfig> mTrainConfigList = new ArrayList();
    private TrainTargetType mUnitType = TrainTargetType.DIATANCE;

    public int getIntervalMode() {
        return this.mIntervalMode;
    }

    public void setIntervalMode(int intervalMode) {
        this.mIntervalMode = intervalMode;
    }

    public int getStartDistance() {
        return this.mStartDistance;
    }

    public void setStartDistance(int startDistance) {
        this.mStartDistance = startDistance;
    }

    public long getStartTimeStamp() {
        return this.mStartTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.mStartTimeStamp = startTimeStamp;
    }

    public int[] getItemOrders() {
        return this.mItemOrders;
    }

    public void setItemOrders(int[] itemOrders) {
        this.mItemOrders = itemOrders;
    }

    public long getTargetTime() {
        return this.mTargetTime;
    }

    public void setTargetTime(long targetTime) {
        this.mTargetTime = targetTime;
    }

    public int getTargetDistance() {
        return this.mTargetDistance;
    }

    public void setTargetDistance(int targetDistance) {
        this.mTargetDistance = targetDistance;
    }

    public void addTrainConfig(TrainConfig config) {
        if (config != null) {
            LogUtil.m9i(true, "KeyEventDispatcher", "addTrainConfig, " + config.toString());
            this.mTrainConfigList.add(config);
        }
    }

    public void addTrainConfigList(List<TrainConfig> configList) {
        if (configList != null && configList.size() > 0) {
            this.mTrainConfigList.addAll(configList);
        }
    }

    public List<TrainConfig> getTrainConfigList() {
        return this.mTrainConfigList;
    }

    public void setUnitType(TrainTargetType unitType) {
        this.mUnitType = unitType;
    }

    public TrainTargetType getUnitType() {
        return this.mUnitType;
    }

    public String toString() {
        return "TrainUnit{mUnitType=" + this.mUnitType + ", mTargetTime=" + this.mTargetTime + ", mTargetDistance=" + this.mTargetDistance + ", mStartDistance=" + this.mStartDistance + ", mStartTimeStamp=" + this.mStartTimeStamp + ", mItemOrders=" + Arrays.toString(this.mItemOrders) + ", mTrainConfigList=" + this.mTrainConfigList + '}';
    }
}
