package com.huami.watch.newsport.train.model;

import java.util.ArrayList;
import java.util.List;

public class TrainGroup {
    private int mRepeatCounts = 1;
    private List<TrainUnit> mTrainUnitList = new ArrayList();

    public List<TrainUnit> getTrainUnitList() {
        return this.mTrainUnitList;
    }

    public void setTrainUnitList(List<TrainUnit> trainUnitList) {
        this.mTrainUnitList = trainUnitList;
    }

    public int getRepeatCounts() {
        return this.mRepeatCounts;
    }

    public void setRepeatCounts(int repeatCounts) {
        this.mRepeatCounts = repeatCounts;
    }

    public String toString() {
        return "TrainGroup{mTrainUnitList=" + this.mTrainUnitList + ", mRepeatCounts=" + this.mRepeatCounts + '}';
    }
}
