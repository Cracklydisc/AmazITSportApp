package com.huami.watch.newsport.train.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.common.JSONAble;
import org.json.JSONException;
import org.json.JSONObject;

public class TrainSnapShot implements Parcelable, JSONAble {
    public static final Creator<TrainSnapShot> CREATOR = new C06711();
    private long mCurTargetCostTime = 0;
    private int mCurTargetDis = 0;
    private int mCurUnitDis = 0;
    private long mCurUnitTime = 0;
    private long mTotalTargetCostTime = 0;
    private int mTotalTargetDistance = 0;
    private TrainTargetType mTrainTargetType = TrainTargetType.DIATANCE;

    static class C06711 implements Creator<TrainSnapShot> {
        C06711() {
        }

        public TrainSnapShot createFromParcel(Parcel in) {
            return new TrainSnapShot(in);
        }

        public TrainSnapShot[] newArray(int size) {
            return new TrainSnapShot[size];
        }
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    protected TrainSnapShot(Parcel in) {
        this.mTotalTargetDistance = in.readInt();
        this.mCurTargetDis = in.readInt();
        this.mCurUnitDis = in.readInt();
        this.mTotalTargetCostTime = in.readLong();
        this.mCurTargetCostTime = in.readLong();
        this.mCurUnitTime = in.readLong();
        initTrainTargetType(in.readInt());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mTotalTargetDistance);
        dest.writeInt(this.mCurTargetDis);
        dest.writeInt(this.mCurUnitDis);
        dest.writeLong(this.mTotalTargetCostTime);
        dest.writeLong(this.mCurTargetCostTime);
        dest.writeLong(this.mCurUnitTime);
        dest.writeInt(this.mTrainTargetType.getType());
    }

    public int getTotalTargetDistance() {
        return this.mTotalTargetDistance;
    }

    public void setTotalTargetDistance(int totalTargetDistance) {
        this.mTotalTargetDistance = totalTargetDistance;
    }

    public int getCurTargetDis() {
        return this.mCurTargetDis;
    }

    public void setCurTargetDis(int curTargetDis) {
        this.mCurTargetDis = curTargetDis;
    }

    public int getCurUnitDis() {
        return this.mCurUnitDis;
    }

    public void setCurUnitDis(int curUnitDis) {
        this.mCurUnitDis = curUnitDis;
    }

    public long getTotalTargetCostTime() {
        return this.mTotalTargetCostTime;
    }

    public void setTotalTargetCostTime(long totalTargetCostTime) {
        this.mTotalTargetCostTime = totalTargetCostTime;
    }

    public long getCurTargetCostTime() {
        return this.mCurTargetCostTime;
    }

    public void setCurTargetCostTime(long curTargetCostTime) {
        this.mCurTargetCostTime = curTargetCostTime;
    }

    public long getCurUnitTime() {
        return this.mCurUnitTime;
    }

    public void setCurUnitTime(long curUnitTime) {
        this.mCurUnitTime = curUnitTime;
    }

    public String toString() {
        return "TrainSnapShot{mTrainTargetType=" + this.mTrainTargetType + ", mTotalTargetDistance=" + this.mTotalTargetDistance + ", mCurTargetDis=" + this.mCurTargetDis + ", mCurUnitDis=" + this.mCurUnitDis + ", mTotalTargetCostTime=" + this.mTotalTargetCostTime + ", mCurTargetCostTime=" + this.mCurTargetCostTime + ", mCurUnitTime=" + this.mCurUnitTime + '}';
    }

    public String toJSON() {
        try {
            JSONObject jsonObject = new JSONObject();
            initJSONFromObject(jsonObject);
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("train_snap_shot_total_dis", this.mTotalTargetDistance);
        jsonObject.put("train_snap_shot_cur_tar_dis", this.mCurTargetDis);
        jsonObject.put("train_snap_shot_cur_unit_dis", this.mCurUnitDis);
        jsonObject.put("train_snap_shot_total_time", this.mTotalTargetCostTime);
        jsonObject.put("train_snap_shot_total_tar_time", this.mCurTargetCostTime);
        jsonObject.put("train_snap_shot_total_unit_time", this.mCurUnitTime);
        jsonObject.put("train_snap_shot_remind_train_type", this.mTrainTargetType.getType());
    }

    private void initTrainTargetType(int type) {
        switch (type) {
            case 0:
                this.mTrainTargetType = TrainTargetType.DIATANCE;
                return;
            case 1:
                this.mTrainTargetType = TrainTargetType.TIME;
                return;
            case 2:
                this.mTrainTargetType = TrainTargetType.KEYPRESS;
                return;
            default:
                return;
        }
    }
}
