package com.huami.watch.newsport.train.model;

public class TrainingInfo {
    private float distance = -1.0f;
    private int maxTime = 0;
    private int minTime = 0;
    private int rateHigh = -1;
    private int rateLow = -1;
    private long trainInfoId = -1;
    private int trainStatus = -1;
    private int trainType = -1;

    public long getTrainInfoId() {
        return this.trainInfoId;
    }

    public void setTrainInfoId(long trainInfoId) {
        this.trainInfoId = trainInfoId;
    }

    public float getDistance() {
        return this.distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getRateHigh() {
        return this.rateHigh;
    }

    public void setRateHigh(int rateHigh) {
        this.rateHigh = rateHigh;
    }

    public void setRateLow(int rateLow) {
        this.rateLow = rateLow;
    }

    public int getTrainStatus() {
        return this.trainStatus;
    }

    public void setTrainStatus(int trainStatus) {
        this.trainStatus = trainStatus;
    }

    public void setTrainType(int trainType) {
        this.trainType = trainType;
    }

    public int getTrainType() {
        return this.trainType;
    }

    public int getMaxTime() {
        return this.maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public int getMinTime() {
        return this.minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public String toString() {
        return "TrainingInfo{trainInfoId=" + this.trainInfoId + ", distance=" + this.distance + ", rateHigh=" + this.rateHigh + ", rateLow=" + this.rateLow + ", trainStatus=" + this.trainStatus + ", trainType=" + this.trainType + ", minTime=" + this.minTime + ", maxTime=" + this.maxTime + '}';
    }
}
