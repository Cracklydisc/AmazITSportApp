package com.huami.watch.newsport.train.model;

public class TrainConfig {
    private int mMaxHeart = -1;
    private float mMaxPace = -1.0f;
    private int mMinHeart = -1;
    private float mMinPace = -1.0f;
    private TrainRemindType mTrainRemindType = TrainRemindType.NONE;

    public TrainConfig(TrainRemindType trainRemindType) {
        this.mTrainRemindType = trainRemindType;
    }

    public int getMinHeart() {
        return this.mMinHeart;
    }

    public void setMinHeart(int minHeart) {
        this.mMinHeart = minHeart;
    }

    public int getMaxHeart() {
        return this.mMaxHeart;
    }

    public void setMaxHeart(int maxHeart) {
        this.mMaxHeart = maxHeart;
    }

    public float getMinPace() {
        return this.mMinPace;
    }

    public void setMinPace(float minPace) {
        this.mMinPace = minPace;
    }

    public void setMaxPace(float maxPace) {
        this.mMaxPace = maxPace;
    }

    public float getMaxPace() {
        return this.mMaxPace;
    }

    public TrainRemindType getTrainRemindType() {
        return this.mTrainRemindType;
    }

    public String toString() {
        return "TrainConfig{mTrainRemindType=" + this.mTrainRemindType + ", mMinHeart=" + this.mMinHeart + ", mMaxHeart=" + this.mMaxHeart + ", mMaxPace=" + this.mMaxPace + ", mMinPace=" + this.mMinPace + '}';
    }
}
