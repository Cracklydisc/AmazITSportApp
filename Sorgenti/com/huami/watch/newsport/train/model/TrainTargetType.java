package com.huami.watch.newsport.train.model;

public enum TrainTargetType {
    DIATANCE(0),
    TIME(1),
    KEYPRESS(2);
    
    private int mType;

    private TrainTargetType(int type) {
        this.mType = type;
    }

    public int getType() {
        return this.mType;
    }
}
