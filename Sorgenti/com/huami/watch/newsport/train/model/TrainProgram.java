package com.huami.watch.newsport.train.model;

import java.util.ArrayList;
import java.util.List;

public class TrainProgram {
    private int mLastDistanceSize = 0;
    private int mLastTimeSize = 0;
    private int mTotalDistance = 0;
    private int mTotalTime = 0;
    private List<TrainUnit> mTrainUnitList = new ArrayList();

    public List<TrainUnit> getTrainUnitList() {
        return this.mTrainUnitList;
    }

    public void addTrainGroup(TrainGroup group) {
        if (checkIsValidGroup(group)) {
            for (int i = 0; i < group.getRepeatCounts(); i++) {
                this.mTrainUnitList.addAll(group.getTrainUnitList());
            }
        }
    }

    public boolean isOnlySingleTrainType() {
        if (this.mTrainUnitList.size() > 0) {
            TrainUnit lastUnit = (TrainUnit) this.mTrainUnitList.get(0);
            for (TrainUnit unit : this.mTrainUnitList) {
                if (unit.getUnitType() != lastUnit.getUnitType()) {
                    return false;
                }
                lastUnit = unit;
            }
        }
        return true;
    }

    private boolean checkIsValidGroup(TrainGroup group) {
        if (group == null || group.getTrainUnitList().isEmpty() || group.getRepeatCounts() <= 0) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "TrainProgram{mTrainUnitList=" + this.mTrainUnitList + ", mTotalDistance=" + this.mTotalDistance + ", mTotalTime=" + this.mTotalTime + ", mLastTimeSize=" + this.mLastTimeSize + ", mLastDistanceSize=" + this.mLastDistanceSize + '}';
    }
}
