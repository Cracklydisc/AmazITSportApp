package com.huami.watch.newsport.train;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.train.model.TrainingInfo;

public class TrainingInfoManager {
    private static final String TAG = TrainingInfoManager.class.getName();
    private static final Uri TRAINING_RESLUT_URI = Uri.parse("content://com.huami.watch.train.ui.provider.dayTrainRecordProvider/finishTodayTrainTask");
    private static final Uri TRAINING_URI_FOR_SPORT = Uri.parse("content://com.huami.watch.train.ui.provider.dayTrainRecordProvider/getTodayTrainTask");
    private static TrainingInfoManager sInstance;
    private Context mContext = null;

    public static TrainingInfoManager getInstance(Context context) {
        synchronized (TrainingInfoManager.class) {
            if (sInstance == null) {
                sInstance = new TrainingInfoManager(context.getApplicationContext());
            }
        }
        return sInstance;
    }

    private TrainingInfoManager(Context context) {
        this.mContext = context;
    }

    public TrainingInfo getTrainingInfo() {
        if (this.mContext == null) {
            Debug.m5i(TAG, " context is null");
        }
        TrainingInfo info = new TrainingInfo();
        Cursor cursor = null;
        try {
            cursor = this.mContext.getContentResolver().query(TRAINING_URI_FOR_SPORT, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                info.setTrainInfoId(cursor.getLong(cursor.getColumnIndex("_id")));
                info.setDistance(cursor.getFloat(cursor.getColumnIndex("DISTANCE")) * 1000.0f);
                info.setRateLow(cursor.getInt(cursor.getColumnIndex("RATE_START")));
                info.setRateHigh(cursor.getInt(cursor.getColumnIndex("RATE_END")));
                info.setTrainStatus(cursor.getInt(cursor.getColumnIndex("DAY_TRAIN_STATUS")));
                info.setTrainType(cursor.getInt(cursor.getColumnIndex("SPORT_TYPE")));
                info.setMinTime((cursor.getInt(cursor.getColumnIndex("TRAIN_TIME_MIN")) * 60) * 1000);
                info.setMaxTime((cursor.getInt(cursor.getColumnIndex("TRAIN_TIME_MAX")) * 60) * 1000);
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
        Debug.m5i(TAG, "train info:" + info.toString());
        return info;
    }

    public void updateTodayTaskStatus(Long todayTaskId) {
        if (this.mContext == null) {
            Debug.m5i(TAG, "updateTodayTaskStatus, context is null");
            return;
        }
        ContentResolver resolver = this.mContext.getContentResolver();
        Uri resultUri = ContentUris.withAppendedId(TRAINING_RESLUT_URI, todayTaskId.longValue());
        Debug.m5i(TAG, "updateTodayTaskStatus, resultUri:" + resultUri.toString());
        Debug.m5i(TAG, "updateTodayTaskStatus, resultInt:" + resolver.update(resultUri, new ContentValues(), null, null));
    }

    public static boolean isInTrainingMode(int mode) {
        return mode == 2;
    }

    public int changeTrainTypeToSportType(int trainType) {
        Debug.m5i(TAG, "training type:" + trainType);
        if (trainType == 100) {
            return 1;
        }
        if (trainType == 300) {
            return 9;
        }
        if (trainType == 200) {
            return 14;
        }
        return -1;
    }

    public boolean isCompleteTrainInfo(TrainingInfo trainInfo, SportSummary sportSummary) {
        int sportType = sportSummary.getSportType();
        int trainType = changeTrainTypeToSportType(trainInfo.getTrainType());
        if (sportType == 15 && trainInfo.getTrainType() == 200) {
            trainType = sportType;
        }
        if (trainType == sportType) {
            if (trainType == 9 || trainType == 14 || trainType == 15) {
                if (Global.DEBUG_TRAINING_MODE && sportSummary.getSportDuration() >= 180000) {
                    sportSummary.setSportDuration(1800000);
                }
                if (sportSummary.getSportDuration() >= trainInfo.getMinTime()) {
                    return true;
                }
            } else if (((float) sportSummary.getTrimedDistance()) >= trainInfo.getDistance()) {
                return true;
            }
        }
        return false;
    }
}
