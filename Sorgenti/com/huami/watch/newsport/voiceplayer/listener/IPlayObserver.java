package com.huami.watch.newsport.voiceplayer.listener;

import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;

public interface IPlayObserver {
    void onCompletion(CommandId commandId);

    boolean onError(int i, CommandId commandId);

    void onPrepared(CommandId commandId);
}
