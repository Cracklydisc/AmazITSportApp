package com.huami.watch.newsport.voiceplayer.listener;

import android.media.MediaPlayer;
import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;

public interface IPlayInnerListener {
    void onCompletion(CommandId commandId);

    boolean onError(int i, CommandId commandId);

    void onPrepared(CommandId commandId, MediaPlayer mediaPlayer);
}
