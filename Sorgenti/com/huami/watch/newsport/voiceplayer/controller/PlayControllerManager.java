package com.huami.watch.newsport.voiceplayer.controller;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.voiceplayer.action.GPSTTSPlayer;
import com.huami.watch.newsport.voiceplayer.model.VoiceInfo;
import java.util.Locale;

public class PlayControllerManager {
    private static PlayControllerManager sInstance;
    private Context mAppContext = null;
    private long mContinueLowSpeedTime = -1;
    private long mContinueMaxHRTime = -1;
    private int mIntervalVoiceSequence = 0;
    private boolean mIsNoticeEnabled = false;
    private boolean mIsNoticeHighHR = false;
    private boolean mIsNoticePaceEnabled = false;
    private boolean mIsSwitchOn = true;
    private boolean mIsTargetRunDone = false;
    private int mMaxHR = Integer.MAX_VALUE;
    private int mNoticeLowPace = 0;
    private GPSTTSPlayer mPlayer = null;
    private boolean mTargetRunModeHalfDone = false;

    public static synchronized PlayControllerManager getInstance(Context context) {
        PlayControllerManager playControllerManager;
        synchronized (PlayControllerManager.class) {
            synchronized (PlayControllerManager.class) {
                if (sInstance == null) {
                    sInstance = new PlayControllerManager(context);
                }
                playControllerManager = sInstance;
            }
        }
        return playControllerManager;
    }

    private PlayControllerManager(Context context) {
        this.mAppContext = context;
        this.mPlayer = new GPSTTSPlayer(context);
        String lang = Locale.getDefault().getLanguage();
        if (!(TextUtils.isEmpty(lang) || Locale.CHINA.getLanguage().equals(lang) || Locale.CHINESE.getLanguage().equals(lang))) {
            this.mIsSwitchOn = false;
        }
        Debug.m5i("HmVoice", "PlayController ,on?=" + this.mIsSwitchOn + ",lang:" + lang);
    }

    private boolean playHighHRIfNeed(Object... param) {
        int curHR = ((Integer) param[0]).intValue();
        this.mPlayer.playMaxHRWarnning(curHR);
        Debug.m5i("HmVoice", "playHighHRIfNeed curHeartRate?:" + curHR);
        return true;
    }

    private void playHeartRegion(Object... param) {
        this.mPlayer.playHeartRegionSpeed(((Integer) param[0]).intValue(), ((Integer) param[1]).intValue());
    }

    private synchronized void playSixMiniuteDailyPerformanceStatus(Object... params) {
        if (params.length != 0) {
            this.mPlayer.playSixMinutePerformanceStatus(((Integer) params[0]).intValue());
        }
    }

    private synchronized void playRealTimeGuideVoice(Object... params) {
        if (params.length != 0) {
            this.mPlayer.playRealTimeGuideVoice(params[0], ((Integer) params[1]).intValue(), ((Integer) params[2]).intValue(), ((Integer) params[3]).intValue());
        }
    }

    public synchronized boolean playRemindVoiceIfNeed(int wakeupsource, boolean isShow, Object... params) {
        boolean z;
        Log.i("HmVoice", " wakeUoSource :" + wakeupsource);
        if (!isShow || UnitConvertUtils.isImperial() || UnitConvertUtils.isOverSeaVersion() || !SportDataManager.getInstance().isPlayVoice()) {
            Debug.m5i("HmVoice", "playRemindVoiceIfNeed, not needed, isShow:" + isShow + ", isImperial:" + UnitConvertUtils.isImperial() + ", config is_play_voice " + SportDataManager.getInstance().isPlayVoice());
            z = false;
        } else {
            switch (wakeupsource) {
                case 1:
                    playKm(wakeupsource, params);
                    break;
                case 2:
                    int dis = DataManager.getInstance().getSportConfig(this.mAppContext, SportDataManager.getInstance().getLastSportType()).getTargetDistance() / 1000;
                    break;
                case 5:
                    playHighHRIfNeed(params);
                    break;
                case 6:
                    playLowSpeed(params);
                    break;
                case 12:
                    Log.i("HmVoice", " remind_heart_region ");
                    playHeartRegion(params);
                    break;
                case 15:
                    playRealTimeGuideVoice(params);
                    break;
                case 16:
                    playSixMiniuteDailyPerformanceStatus(params);
                    break;
                case 18:
                    playHightSpeed(params);
                    break;
            }
            z = true;
        }
        return z;
    }

    private boolean playLowSpeed(Object... params) {
        if (params == null || params.length < 1) {
            Debug.m5i("HmVoice", "playLowSpeed, params is err");
            return false;
        }
        this.mPlayer.playLowPace(((Float) params[0]).floatValue());
        return true;
    }

    private boolean playHightSpeed(Object... params) {
        if (params == null || params.length < 1) {
            Debug.m5i("HmVoice", "playLowSpeed, params is err");
            return false;
        }
        this.mPlayer.playHightPace(((Float) params[0]).floatValue());
        return true;
    }

    private boolean playKm(int type, Object... params) {
        Exception e;
        if (params == null || params.length < 3) {
            Debug.m5i("HmVoice", "playKm, params is err");
            return false;
        }
        int sportType = SportDataManager.getInstance().getLastSportType();
        if (sportType == -1) {
            Debug.m5i("HmVoice", "sport type is invalid");
            return false;
        }
        VoiceInfo voiceInfo = null;
        switch (sportType) {
            case 1:
            case 6:
            case 9:
                try {
                    float kmDistance = ((Float) params[0]).floatValue();
                    float kmAvePace = ((Float) params[1]).floatValue();
                    float kmAveSpeed = ((Float) params[2]).floatValue();
                    float kmTime = kmAvePace * 1000.0f;
                    VoiceInfo voiceInfo2 = new VoiceInfo(sportType);
                    try {
                        voiceInfo2.mDistance = kmDistance;
                        voiceInfo2.mPace = kmAvePace;
                        voiceInfo2.mSpeed = kmAveSpeed;
                        voiceInfo2.mKmUsedTime = kmTime;
                        voiceInfo = voiceInfo2;
                    } catch (Exception e2) {
                        e = e2;
                        voiceInfo = voiceInfo2;
                        e.printStackTrace();
                        if (voiceInfo != null) {
                            this.mPlayer.playKilometer(type, voiceInfo);
                        }
                        return true;
                    }
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    if (voiceInfo != null) {
                        this.mPlayer.playKilometer(type, voiceInfo);
                    }
                    return true;
                }
        }
        if (voiceInfo != null) {
            this.mPlayer.playKilometer(type, voiceInfo);
        }
        return true;
    }
}
