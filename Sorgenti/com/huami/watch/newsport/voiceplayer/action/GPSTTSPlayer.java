package com.huami.watch.newsport.voiceplayer.action;

import android.content.Context;
import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;
import com.huami.watch.newsport.voiceplayer.model.VoiceInfo;
import java.util.ArrayList;
import java.util.List;

public class GPSTTSPlayer {
    private List<CommandId> mCommandsToPlay;
    private TTSPlayerAssistor mMediaAssistor;
    private SportMediaPlayer mPlayer;

    public GPSTTSPlayer(Context context) {
        this.mMediaAssistor = null;
        this.mPlayer = null;
        this.mCommandsToPlay = null;
        this.mMediaAssistor = new TTSPlayerAssistor();
        this.mPlayer = new SportMediaPlayer(context);
        this.mCommandsToPlay = new ArrayList(1);
    }

    public void playKilometer(int type, VoiceInfo info) {
        if (info == null) {
            throw new IllegalArgumentException();
        }
        playVoice(1, this.mMediaAssistor.convertKilometerInfoToSpeech(type, info));
    }

    private void playVoice(int type, List<String> data) {
        PlayCommand command = new PlayCommand(type);
        command.addData(data);
        this.mPlayer.play(command);
    }

    public void playLowPace(float pace) {
        if (this.mPlayer != null) {
            playVoice(13, this.mMediaAssistor.convertPaceInfoToSpeech(pace));
        }
    }

    public void playHightPace(float pace) {
        if (this.mPlayer != null) {
            playVoice(17, this.mMediaAssistor.convertHightPaceInfoToSpeech(pace));
        }
    }

    public void playMaxHRWarnning(int maxHr) {
        if (maxHr > 0) {
            playVoice(8, this.mMediaAssistor.getHRWarnSpeech(maxHr));
        }
    }

    public void playRealTimeGuideVoice(String guideTitle, int phraseNumber, int paramOne, int paramSecond) {
        playVoice(15, this.mMediaAssistor.getRealTimeGuideSpeech(guideTitle, phraseNumber, paramOne, paramSecond));
    }

    public void playHeartRegionSpeed(int currentHeartRate, int currentStatus) {
        playVoice(16, this.mMediaAssistor.getHeartRegionString(currentHeartRate, currentStatus));
    }

    public void playSixMinutePerformanceStatus(int performanceValue) {
        playVoice(14, this.mMediaAssistor.getSixPerformanmceValueSpeech(performanceValue));
    }
}
