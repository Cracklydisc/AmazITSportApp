package com.huami.watch.newsport.voiceplayer.action;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;
import com.huami.watch.newsport.voiceplayer.listener.IPlayInnerListener;

final class MediaPlayListener implements OnCompletionListener, OnErrorListener {
    private AudioManager mAudioManager = null;
    private CommandId mId = null;
    private IPlayInnerListener mListener = null;

    public MediaPlayListener(Context context, CommandId cmdId, IPlayInnerListener playListener) {
        this.mId = cmdId;
        this.mListener = playListener;
        this.mAudioManager = (AudioManager) context.getSystemService("audio");
    }

    public void onCompletion(MediaPlayer mp) {
        forceRelease(mp);
        if (this.mListener != null) {
            this.mListener.onCompletion(this.mId);
        }
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Debug.m5i("HmVoice", "PlayListener onError what: " + what + ",extra: " + extra);
        forceRelease(mp);
        if (this.mListener != null) {
            this.mListener.onError(what, this.mId);
        }
        return true;
    }

    private void forceRelease(MediaPlayer mp) {
        if (mp != null) {
            mp.stop();
            mp.release();
        }
        if (this.mAudioManager != null) {
            this.mAudioManager.abandonAudioFocus(null);
        }
    }
}
