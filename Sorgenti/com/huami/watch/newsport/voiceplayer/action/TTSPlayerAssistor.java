package com.huami.watch.newsport.voiceplayer.action;

import android.util.Log;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.voiceplayer.model.VoiceInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class TTSPlayerAssistor {
    private static final HashMap<String, String> mSoundMap = new HashMap();

    public List<String> convertPaceInfoToSpeech(float pace) {
        float curPace = SportDataFilterUtils.parsePace(pace);
        List<String> resultList = new ArrayList();
        resultList.add(mSoundMap.get("slow_speed"));
        resultList.add(mSoundMap.get("current"));
        resultList.add(mSoundMap.get("peisu"));
        int secondPerKm = (int) (1000.0f * curPace);
        int minute = secondPerKm / 60;
        int second = secondPerKm % 60;
        if (minute > 0) {
            resultList.add(mSoundMap.get(String.valueOf(minute)));
            resultList.add(mSoundMap.get("minute"));
        }
        resultList.add(mSoundMap.get(String.valueOf(second)));
        resultList.add(mSoundMap.get("second"));
        return resultList;
    }

    public List<String> convertHightPaceInfoToSpeech(float pace) {
        float curPace = SportDataFilterUtils.parsePace(pace);
        List<String> resultList = new ArrayList();
        resultList.add(mSoundMap.get("run_faster"));
        resultList.add(mSoundMap.get("current"));
        resultList.add(mSoundMap.get("peisu"));
        int secondPerKm = (int) (1000.0f * curPace);
        int minute = secondPerKm / 60;
        int second = secondPerKm % 60;
        if (minute > 0) {
            resultList.add(mSoundMap.get(String.valueOf(minute)));
            resultList.add(mSoundMap.get("minute"));
        }
        resultList.add(mSoundMap.get(String.valueOf(second)));
        resultList.add(mSoundMap.get("second"));
        return resultList;
    }

    List<String> convertKilometerInfoToSpeech(int type, VoiceInfo info) {
        List<String> resultList = new ArrayList();
        if (info == null) {
            Debug.m5i("HmVoice", "convertKilometerInfoToSpeech, voice info is null");
        } else if (info.mSportType == -1 || info.mSportType == 7 || info.mSportType == 10) {
            Debug.m5i("HmVoice", "sport type is invalid: " + info.mSportType);
        } else if (type == 1) {
            if (info.mSportType == 1) {
                resultList.add(mSoundMap.get("already_run"));
            }
            if (info.mSportType == 6) {
                resultList.add(mSoundMap.get("already_walk"));
            }
            if (info.mSportType == 9) {
                resultList.add(mSoundMap.get("already_ride"));
            }
            convertIntegerAndDecimal(resultList, String.valueOf((int) info.mDistance));
            resultList.add(mSoundMap.get("kilometer"));
            if (info.mSportType == 1) {
                resultList.add(mSoundMap.get("recent_kilo_pace"));
                resultList.addAll(convertPaceInfoToSpeech(info.mPace));
            }
            if (info.mSportType == 6) {
                resultList.add(mSoundMap.get("recent_kilo_time"));
                resultList.addAll(convertTimeInfoToSpeech(info));
            }
            if (info.mSportType == 9) {
                resultList.add(mSoundMap.get("recent_kilo_time"));
                resultList.addAll(convertTimeInfoToSpeech(info));
            }
        }
        return resultList;
    }

    public List<String> convertTimeInfoToSpeech(VoiceInfo info) {
        List<String> resultList = new ArrayList();
        String timeString = String.valueOf((int) info.mKmUsedTime);
        if (info.mKmUsedTime > 0.0f) {
            resultList.addAll(convertTimeToText(timeString));
        } else {
            resultList.add(mSoundMap.get("0"));
            resultList.add(mSoundMap.get("second"));
        }
        return resultList;
    }

    List<String> getCurrentHRSpeech(int hr) {
        List<String> resultList = new ArrayList();
        resultList.add(mSoundMap.get("aleary_out_safe_geart"));
        resultList.add(mSoundMap.get("current"));
        resultList.addAll(convertIntegerToText(String.valueOf(hr)));
        resultList.add(mSoundMap.get("times_per_minute"));
        return resultList;
    }

    List<String> getHRWarnSpeech(int hr) {
        List<String> resultList = new ArrayList();
        resultList.addAll(getCurrentHRSpeech(hr));
        return resultList;
    }

    List<String> getHeartRegionString(int currentHeartRate, int currentStatus) {
        List<String> resultList = new ArrayList();
        if (currentStatus == 1) {
            resultList.add(mSoundMap.get("heart_rate_too_hight"));
            resultList.add(mSoundMap.get("current"));
            resultList.addAll(convertIntegerToText(String.valueOf(currentHeartRate)));
            resultList.add(mSoundMap.get("times_per_minute"));
        } else if (currentStatus == 2) {
            resultList.add(mSoundMap.get("heart_rate_too_lower"));
            resultList.add(mSoundMap.get("current"));
            resultList.addAll(convertIntegerToText(String.valueOf(currentHeartRate)));
            resultList.add(mSoundMap.get("times_per_minute"));
        }
        return resultList;
    }

    List<String> getSixPerformanmceValueSpeech(int performanceValue) {
        List<String> sixPerformanceList = new ArrayList();
        sixPerformanceList.add(mSoundMap.get("performance_status"));
        if (performanceValue > 0) {
            sixPerformanceList.add(mSoundMap.get("positive"));
        } else {
            sixPerformanceList.add(mSoundMap.get("negative"));
        }
        sixPerformanceList.add(mSoundMap.get(String.valueOf(Math.abs(performanceValue))));
        String status = getVoideFileByPerformanceValue(performanceValue);
        if (status != null) {
            sixPerformanceList.add(status);
        }
        return sixPerformanceList;
    }

    List<String> getRealTimeGuideSpeech(String guideTitle, int phraseNumber, int paramOne, int paramSecond) {
        List<String> realTimeGuideList = new ArrayList();
        Log.i("HmVoice", " getRealTimeGuideSpeech:" + "phraseNumber:" + phraseNumber + ",paramOne:" + paramOne + ",paramSecond:" + paramSecond);
        switch (phraseNumber) {
            case 1:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_1"));
                break;
            case 2:
                realTimeGuideList.add(mSoundMap.get("run_again"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("minute"));
                realTimeGuideList.add(mSoundMap.get("minutes"));
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_2_after"));
                break;
            case 3:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_3"));
                break;
            case 6:
                realTimeGuideList.add(mSoundMap.get("run_again"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("minute"));
                realTimeGuideList.add(mSoundMap.get("minutes"));
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_6_after"));
                break;
            case 7:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_3"));
                break;
            case 10:
                realTimeGuideList.add(mSoundMap.get("run_again"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("minute"));
                realTimeGuideList.add(mSoundMap.get("minutes"));
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_10_after"));
                break;
            case 11:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_11"));
                break;
            case 14:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_14"));
                break;
            case 22:
                realTimeGuideList.add(mSoundMap.get("predict"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("minute"));
                realTimeGuideList.add(mSoundMap.get("minutes_after_finish_target"));
                break;
            case 25:
                realTimeGuideList.add(mSoundMap.get("keep_heart_rate_at"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("to"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramSecond)));
                realTimeGuideList.add(mSoundMap.get("times_per_minute"));
                break;
            case 26:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_26"));
                break;
            case 27:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_27"));
                break;
            case 28:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_28"));
                realTimeGuideList.add(mSoundMap.get(String.valueOf(paramOne)));
                break;
            case 30:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_30"));
                break;
            case 31:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_31"));
                break;
            case 32:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_32"));
                break;
            case 33:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_33"));
                break;
            case 34:
                realTimeGuideList.add(mSoundMap.get("keep_heart_rate_at"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramOne)));
                realTimeGuideList.add(mSoundMap.get("to"));
                realTimeGuideList.addAll(convertIntegerToText(String.valueOf(paramSecond)));
                realTimeGuideList.add(mSoundMap.get("times_per_minute"));
                break;
            case 35:
                realTimeGuideList.add(mSoundMap.get("real_time_guide_desc_35"));
                break;
        }
        return realTimeGuideList;
    }

    private String getVoideFileByPerformanceValue(int performanceValue) {
        if (performanceValue >= -20 && performanceValue < -10) {
            return (String) mSoundMap.get("fatigue");
        }
        if (performanceValue >= -10 && performanceValue < -5) {
            return (String) mSoundMap.get("fatigue");
        }
        if (performanceValue >= -5 && performanceValue < 5) {
            return (String) mSoundMap.get("general");
        }
        if (performanceValue >= 5 && performanceValue < 10) {
            return (String) mSoundMap.get("good");
        }
        if (performanceValue < 10 || performanceValue >= 20) {
            return null;
        }
        return (String) mSoundMap.get("good");
    }

    private List<String> convertDecimalToText(String paramString) {
        char[] arrayOfChar = paramString.toCharArray();
        List<String> resultList = new ArrayList();
        for (char item : arrayOfChar) {
            resultList.add(mSoundMap.get(String.valueOf(item)));
        }
        return resultList;
    }

    private void convertIntegerAndDecimal(List<String> resultList, String data) {
        if (data.indexOf(".") > 0) {
            String[] arrayOfString = data.split("\\.");
            if (arrayOfString.length == 2) {
                String integerString = arrayOfString[0];
                String decimalString = arrayOfString[1];
                resultList.addAll(convertIntegerToText(integerString));
                resultList.add(mSoundMap.get("dot"));
                resultList.addAll(convertDecimalToText(decimalString));
                return;
            }
            return;
        }
        resultList.addAll(convertIntegerToText(data));
    }

    private List<String> convertIntegerToText(String data) {
        List<String> numberList = new ArrayList();
        int k = Integer.parseInt(data);
        if (k == 0) {
            numberList.add(mSoundMap.get("0"));
        } else {
            int ten;
            int ge;
            if (k / 100 > 0) {
                int tenThousandK = k / 10000;
                int thousandK = (k % 10000) / 1000;
                int hundred = (k % 1000) / 100;
                ten = ((k % 1000) % 100) / 10;
                ge = ((k % 1000) % 100) % 10;
                if (tenThousandK > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(tenThousandK)));
                    numberList.add(mSoundMap.get("10000"));
                }
                if (thousandK > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(thousandK)));
                    numberList.add(mSoundMap.get("1000"));
                } else if (tenThousandK > 0 && !(thousandK == 0 && hundred == 0 && ge == 0)) {
                    numberList.add(mSoundMap.get("0"));
                }
                if (hundred > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(hundred)));
                    numberList.add(mSoundMap.get("hundred"));
                } else if (thousandK > 0 && !(hundred == 0 && ge == 0)) {
                    numberList.add(mSoundMap.get("0"));
                }
                if (ten > 0) {
                    if (ten == 1) {
                        numberList.add(mSoundMap.get("h_" + String.valueOf((ten * 10) + ge)));
                    } else {
                        numberList.add(mSoundMap.get(String.valueOf(ten * 10)));
                    }
                } else if (hundred > 0 && ge != 0) {
                    numberList.add(mSoundMap.get("0"));
                }
                if (ge > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(ge)));
                }
            } else {
                ten = ((k % 1000) % 100) / 10;
                ge = ((k % 1000) % 100) % 10;
                if (ten > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(ten * 10)));
                }
                if (ge > 0) {
                    numberList.add(mSoundMap.get(String.valueOf(ge)));
                }
            }
        }
        return numberList;
    }

    private List<String> convertTimeToText(String data) {
        List<String> numberList = new ArrayList();
        int k = Integer.parseInt(data);
        int hour = k / 3600;
        if (hour > 0) {
            int hundred = (hour % 1000) / 100;
            int ten = (hour % 100) / 10;
            int ge = hour % 10;
            if (hundred > 0) {
                numberList.add(mSoundMap.get(String.valueOf(hundred)));
                numberList.add(mSoundMap.get("hundred"));
                if (ten > 0) {
                    if (ten == 1) {
                        numberList.add(mSoundMap.get("h_" + String.valueOf((ten * 10) + ge)));
                    } else {
                        numberList.add(mSoundMap.get(String.valueOf((ten * 10) + ge)));
                    }
                } else if (ge > 0) {
                    numberList.add(mSoundMap.get("0"));
                    numberList.add(mSoundMap.get(String.valueOf(ge)));
                }
            } else {
                numberList.add(mSoundMap.get(String.valueOf(hour)));
            }
            numberList.add(mSoundMap.get("hour"));
        }
        int minute = (k % 3600) / 60;
        if (minute > 0) {
            numberList.add(mSoundMap.get(String.valueOf(minute)));
            numberList.add(mSoundMap.get("minute"));
        }
        numberList.add(mSoundMap.get(String.valueOf((k % 3600) % 60)));
        numberList.add(mSoundMap.get("second"));
        return numberList;
    }

    static {
        for (int i = 0; i <= 100; i++) {
            mSoundMap.put(String.valueOf(i), "voice/" + i + ".mp3");
        }
        mSoundMap.put("1000", "voice/1000.mp3");
        mSoundMap.put("10000", "voice/10000.mp3");
        mSoundMap.put("already_run", "voice/already_run.mp3");
        mSoundMap.put("already_walk", "voice/already_walk.mp3");
        mSoundMap.put("h_10", "voice/h_10.mp3");
        mSoundMap.put("h_11", "voice/h_11.mp3");
        mSoundMap.put("h_12", "voice/h_12.mp3");
        mSoundMap.put("h_13", "voice/h_13.mp3");
        mSoundMap.put("h_14", "voice/h_14.mp3");
        mSoundMap.put("h_15", "voice/h_15.mp3");
        mSoundMap.put("h_16", "voice/h_16.mp3");
        mSoundMap.put("h_17", "voice/h_17.mp3");
        mSoundMap.put("h_18", "voice/h_18.mp3");
        mSoundMap.put("h_19", "voice/h_19.mp3");
        mSoundMap.put("second", "voice/second.mp3");
        mSoundMap.put("minute", "voice/minute.mp3");
        mSoundMap.put("hour", "voice/hour.mp3");
        mSoundMap.put("hundred", "voice/hundred.mp3");
        mSoundMap.put("kilometer", "voice/kilometer.mp3");
        mSoundMap.put("recent_kilo_pace", "voice/recent_kilo_pace.mp3");
        mSoundMap.put("recent_kilo_time", "voice/recent_kilo_time.mp3");
        mSoundMap.put("time", "voice/time.mp3");
        mSoundMap.put("performance_status", "voice/performance_status.mp3");
        mSoundMap.put("negative", "voice/negative.mp3");
        mSoundMap.put("positive", "voice/positive.mp3");
        mSoundMap.put("general", "voice/general.mp3");
        mSoundMap.put("good", "voice/good.mp3");
        mSoundMap.put("excellent", "voice/excellent.mp3");
        mSoundMap.put("fatigue", "voice/fatigue.mp3");
        mSoundMap.put("overwork_fatigue", "voice/overwork_fatigue.mp3");
        mSoundMap.put("you_are_very_good", "voice/you_are_very_good.mp3");
        mSoundMap.put("keep_this_speed", "voice/keep_this_speed.mp3");
        mSoundMap.put("run_faster", "voice/run_faster.mp3");
        mSoundMap.put("slow_speed", "voice/slow_speed.mp3");
        mSoundMap.put("already_ride", "voice/already_ride.mp3");
        mSoundMap.put("heart_rate", "voice/heart_rate.mp3");
        mSoundMap.put("lower", "voice/lower.mp3");
        mSoundMap.put("current_heart", "voice/current_heart.mp3");
        mSoundMap.put("heart_rata_too_high", "voice/heart_rata_too_high.mp3");
        mSoundMap.put("aleary_out_safe_geart", "voice/aleary_out_safe_geart.mp3");
        mSoundMap.put("current", "voice/current.mp3");
        mSoundMap.put("times_per_minute", "voice/times_per_minute.mp3");
        mSoundMap.put("this_time", "voice/this_time.mp3");
        mSoundMap.put("iron_three_results", "voice/iron_three_results.mp3");
        mSoundMap.put("change_project", "voice/change_projec.mp3");
        mSoundMap.put("time_cost", "voice/time_cost.mp3");
        mSoundMap.put("this_time_total_swim", "voice/this_time_total_swim.mp3");
        mSoundMap.put("trip", "voice/trip.mp3");
        mSoundMap.put("total_water", "voice/total_water.mp3");
        mSoundMap.put("single_time", "voice/single_time.mp3");
        mSoundMap.put("water", "voice/water.mp3");
        mSoundMap.put("distance", "voice/distance.mp3");
        mSoundMap.put("swolf_efficiency_index", "voice/swolf_efficiency_index.mp3");
        mSoundMap.put("rate", "voice/rate.mp3");
        mSoundMap.put("peisu", "voice/peisu.mp3");
        mSoundMap.put("max", "voice/max.mp3");
        mSoundMap.put("heart_rate_too_hight", "voice/heart_rate_too_hight.mp3");
        mSoundMap.put("heart_rate_too_lower", "voice/heart_rate_too_lower.mp3");
        mSoundMap.put("heart_rata_too_high_please_be_careful", "voice/heart_rata_too_high_please_be_careful.mp3");
        mSoundMap.put("real_time_guide_desc_1", "voice/real_time_guide_desc_1.mp3");
        mSoundMap.put("real_time_guide_desc_3", "voice/real_time_guide_desc_3.mp3");
        mSoundMap.put("real_time_guide_desc_6_after", "voice/real_time_guide_desc_6_after.mp3");
        mSoundMap.put("real_time_guide_desc_10_after", "voice/real_time_guide_desc_10_after.mp3");
        mSoundMap.put("real_time_guide_desc_11", "voice/real_time_guide_desc_11.mp3");
        mSoundMap.put("real_time_guide_desc_14", "voice/real_time_guide_desc_14.mp3");
        mSoundMap.put("real_time_guide_desc_26", "voice/real_time_guide_desc_26.mp3");
        mSoundMap.put("real_time_guide_desc_27", "voice/real_time_guide_desc_27.mp3");
        mSoundMap.put("real_time_guide_desc_28", "voice/real_time_guide_desc_28.mp3");
        mSoundMap.put("real_time_guide_desc_30", "voice/real_time_guide_desc_30.mp3");
        mSoundMap.put("real_time_guide_desc_31", "voice/real_time_guide_desc_31.mp3");
        mSoundMap.put("real_time_guide_desc_32", "voice/real_time_guide_desc_32.mp3");
        mSoundMap.put("real_time_guide_desc_33", "voice/real_time_guide_desc_33.mp3");
        mSoundMap.put("real_time_guide_desc_35", "voice/real_time_guide_desc_35.mp3");
        mSoundMap.put("real_time_guide_desc_2_after", "voice/real_time_guide_desc_2_after.mp3");
        mSoundMap.put("run_again", "voice/run_again.mp3");
        mSoundMap.put("minutes", "voice/minutes.mp3");
        mSoundMap.put("predict", "voice/predict.mp3");
        mSoundMap.put("minutes_after_finish_target", "voice/minutes_after_finish_target.mp3");
        mSoundMap.put("keep_heart_rate_at", "voice/keep_heart_rate_at.mp3");
        mSoundMap.put("to", "voice/to.mp3");
        mSoundMap.put("realguide_finish_percent", "voice/realguide_finish_percent.mp3");
    }
}
