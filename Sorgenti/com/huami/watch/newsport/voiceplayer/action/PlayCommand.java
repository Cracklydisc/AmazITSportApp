package com.huami.watch.newsport.voiceplayer.action;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class PlayCommand {
    private List<String> mDataList;
    private CommandId mId;

    public static final class CommandId {
        String mId = "";
        int mUserCmdId = 0;

        public CommandId(String id, int userCmdId) {
            this.mId = id;
            this.mUserCmdId = userCmdId;
        }

        public boolean equals(Object object) {
            if (object == null || !(object instanceof CommandId) || TextUtils.isEmpty(this.mId)) {
                return false;
            }
            CommandId command = (CommandId) object;
            if (this.mId.equals(command.mId) && command.mUserCmdId == this.mUserCmdId) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "UUID:" + this.mId + ",userCmdId:" + this.mUserCmdId;
        }
    }

    public PlayCommand() {
        this(0, new ArrayList());
    }

    public PlayCommand(int userCmdId) {
        this(userCmdId, new ArrayList());
    }

    public PlayCommand(int userCmdId, List<String> fileAbsolutePaths) {
        this.mId = null;
        this.mDataList = null;
        this.mId = new CommandId(UUID.randomUUID().toString(), userCmdId);
        this.mDataList = fileAbsolutePaths;
    }

    public void addData(List<String> fileAbsolutePaths) {
        if (fileAbsolutePaths == null) {
            throw new IllegalArgumentException();
        } else if (this.mDataList == null) {
            throw new IllegalStateException();
        } else {
            this.mDataList.addAll(fileAbsolutePaths);
        }
    }

    public boolean equals(Object object) {
        if (object == null || !(object instanceof PlayCommand) || this.mId == null) {
            return false;
        }
        return this.mId.equals(((PlayCommand) object).mId);
    }

    public List<String> getDataList() {
        return this.mDataList;
    }

    public CommandId getId() {
        return this.mId;
    }
}
