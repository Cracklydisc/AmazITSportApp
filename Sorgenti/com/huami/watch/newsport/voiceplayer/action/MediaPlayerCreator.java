package com.huami.watch.newsport.voiceplayer.action;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;
import com.huami.watch.newsport.voiceplayer.listener.IPlayInnerListener;
import com.huami.watch.newsport.voiceplayer.model.MediaCommand;
import java.io.File;
import java.util.List;

final class MediaPlayerCreator {

    static class C08831 implements OnPreparedListener {
        C08831() {
        }

        public void onPrepared(MediaPlayer mp) {
            mp.start();
        }
    }

    public static MediaPlayer play(Context context, List<String> dataList, CommandId cmdId, IPlayInnerListener listener) {
        if (context == null || cmdId == null || dataList == null || dataList.size() <= 0) {
            return null;
        }
        File outputFile = MediaFileOp.mergeSoundFile(context, dataList);
        if (outputFile == null) {
            Debug.m5i("HmVoice", "playMedia failed to create file cmdId: " + cmdId);
            return null;
        }
        String srcFilePath = outputFile.getAbsolutePath();
        MediaCommand command = new MediaCommand();
        command.mContext = context;
        command.mUri = Uri.parse(srcFilePath);
        command.mLooping = false;
        command.mStreamType = 3;
        command.mVolume = 1.0f;
        return play(command, cmdId, listener);
    }

    private static MediaPlayer play(MediaCommand cmd, CommandId cmdId, IPlayInnerListener playListener) {
        if (isBleConnected()) {
            try {
                MediaPlayer player = new MediaPlayer();
                player.setAudioStreamType(cmd.mStreamType);
                player.setDataSource(cmd.mContext, cmd.mUri);
                player.setLooping(cmd.mLooping);
                player.setVolume(cmd.mVolume, cmd.mVolume);
                player.prepare();
                AudioManager audioManager = (AudioManager) cmd.mContext.getSystemService("audio");
                if (!(cmd.mUri == null || cmd.mUri.getEncodedPath() == null || cmd.mUri.getEncodedPath().length() <= 0)) {
                    if (cmd.mLooping) {
                        audioManager.requestAudioFocus(null, cmd.mStreamType, 1);
                    } else {
                        audioManager.requestAudioFocus(null, cmd.mStreamType, 3);
                    }
                }
                MediaPlayListener listener = new MediaPlayListener(cmd.mContext, cmdId, playListener);
                player.setOnCompletionListener(listener);
                player.setOnErrorListener(listener);
                player.setOnPreparedListener(new C08831());
                return player;
            } catch (Exception e) {
                Debug.m5i("HmVoice", e.getMessage());
                return null;
            }
        }
        if (playListener != null) {
            playListener.onError(0, cmdId);
        }
        return null;
    }

    private static boolean isBleConnected() {
        boolean isConnectedBluetooth = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2;
        Debug.m5i("HmVoice", "isConnectBLE: " + isConnectedBluetooth);
        return isConnectedBluetooth;
    }

    private MediaPlayerCreator() {
    }
}
