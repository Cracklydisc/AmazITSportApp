package com.huami.watch.newsport.voiceplayer.action;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.huami.watch.newsport.voiceplayer.listener.IPlayInnerListener;

final class MediaPlayerTask {
    private Context mContext = null;
    private IPlayInnerListener mIPlayTaskListener = null;
    private volatile TaskHandler mTaskHandler;

    private final class TaskHandler extends Handler {
        public TaskHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            MediaPlayerTask.this.onHandleIntent(msg);
        }
    }

    public MediaPlayerTask(Context context, IPlayInnerListener listener) {
        this.mContext = context;
        this.mIPlayTaskListener = listener;
        HandlerThread thread = new HandlerThread("play_task", -16);
        thread.start();
        this.mTaskHandler = new TaskHandler(thread.getLooper());
    }

    public void sendCommand(PlayCommand command) {
        if (command == null) {
            throw new IllegalArgumentException();
        }
        Message msg = this.mTaskHandler.obtainMessage();
        msg.what = 1;
        msg.obj = command;
        this.mTaskHandler.sendMessage(msg);
    }

    private void onHandleIntent(Message msg) {
        switch (msg.what) {
            case 1:
                if (msg.obj instanceof PlayCommand) {
                    PlayCommand command = msg.obj;
                    this.mIPlayTaskListener.onPrepared(command.getId(), MediaPlayerCreator.play(this.mContext, command.getDataList(), command.getId(), this.mIPlayTaskListener));
                    return;
                }
                return;
            case 2:
                this.mTaskHandler.removeCallbacksAndMessages(null);
                this.mTaskHandler.getLooper().quitSafely();
                this.mTaskHandler = null;
                return;
            default:
                return;
        }
    }
}
