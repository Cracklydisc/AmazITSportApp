package com.huami.watch.newsport.voiceplayer.action;

import android.content.Context;
import android.media.MediaPlayer;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.voiceplayer.action.PlayCommand.CommandId;
import com.huami.watch.newsport.voiceplayer.listener.IPlayInnerListener;
import com.huami.watch.newsport.voiceplayer.listener.IPlayObserver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public final class SportMediaPlayer {
    private ConcurrentHashMap<CommandId, PlayCommand> mCachedMsgMap;
    private ConcurrentHashMap<CommandId, MediaPlayer> mCachedPlayerMap;
    private CommandId mCurCommandId;
    private MediaPlayerTask mPlayTask;
    private List<IPlayObserver> mPlayerObservers;
    private final IPlayInnerListener mTaskListener;

    private final class PlayTaskListener implements IPlayInnerListener {
        private PlayTaskListener() {
        }

        public void onCompletion(CommandId commandId) {
            Debug.m5i("HmVoice", "onCompletion curCmd:" + commandId);
            SportMediaPlayer.this.updateCache(commandId);
            SportMediaPlayer.this.playNext();
            for (IPlayObserver observer : SportMediaPlayer.this.mPlayerObservers) {
                observer.onCompletion(commandId);
            }
        }

        public boolean onError(int what, CommandId commandId) {
            Debug.m5i("HmVoice", "onError curCmd:" + commandId);
            SportMediaPlayer.this.updateCache(commandId);
            SportMediaPlayer.this.playNext();
            for (IPlayObserver observer : SportMediaPlayer.this.mPlayerObservers) {
                observer.onError(what, commandId);
            }
            return true;
        }

        public void onPrepared(CommandId commandId, MediaPlayer player) {
            Debug.m5i("HmVoice", "onPrepared curCmd:" + commandId + ",player:" + player);
            if (player == null) {
                SportMediaPlayer.this.updateCache(commandId);
                SportMediaPlayer.this.playNext();
                return;
            }
            SportMediaPlayer.this.mCurCommandId = commandId;
            SportMediaPlayer.this.mCachedPlayerMap.put(commandId, player);
            for (IPlayObserver observer : SportMediaPlayer.this.mPlayerObservers) {
                observer.onPrepared(commandId);
            }
        }
    }

    public SportMediaPlayer(Context context) {
        this.mTaskListener = new PlayTaskListener();
        this.mPlayTask = null;
        this.mCachedMsgMap = null;
        this.mCachedPlayerMap = null;
        this.mPlayerObservers = null;
        this.mCurCommandId = null;
        this.mCachedMsgMap = new ConcurrentHashMap();
        this.mCachedPlayerMap = new ConcurrentHashMap();
        this.mPlayerObservers = new ArrayList();
        this.mPlayTask = new MediaPlayerTask(context, this.mTaskListener);
    }

    public boolean play(PlayCommand command) {
        if (command == null) {
            throw new IllegalArgumentException();
        }
        execute(command);
        return true;
    }

    private void execute(PlayCommand command) {
        this.mCachedMsgMap.put(command.getId(), command);
        if (this.mCachedMsgMap.size() == 1) {
            this.mPlayTask.sendCommand(command);
        } else {
            Debug.m5i("HmVoice", "Execute delayed to play:" + command.getId() + ",cachedSize = " + this.mCachedMsgMap.size());
        }
    }

    private boolean playNext() {
        if (this.mCachedMsgMap.size() <= 0) {
            return false;
        }
        Iterator<PlayCommand> iterator = this.mCachedMsgMap.values().iterator();
        if (iterator.hasNext()) {
            PlayCommand command = (PlayCommand) iterator.next();
            if (command != null) {
                this.mPlayTask.sendCommand(command);
            }
            return true;
        }
        Debug.m5i("HmVoice", "Delayed cachedMsgMap !hasNext");
        return false;
    }

    private void updateCache(CommandId commandId) {
        if (commandId != null) {
            this.mCachedMsgMap.remove(commandId);
            this.mCachedPlayerMap.remove(commandId);
        }
    }
}
