package com.huami.watch.newsport.voiceplayer.action;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import com.huami.watch.common.log.Debug;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class MediaFileOp {
    public static File mergeSoundFile(Context context, List<String> toneList) {
        IOException e;
        Throwable th;
        if (context == null || toneList == null || toneList.isEmpty()) {
            Debug.m4e("HmVoice", "mergeSoundFlie failed");
            return null;
        }
        String destFilePath = context.getFilesDir() + File.separator + "sound.mp3";
        File destFile = new File(destFilePath);
        if (destFile.exists()) {
            destFile.delete();
        }
        List<InputStream> inputStreamList = new ArrayList();
        AssetManager manager = context.getResources().getAssets();
        SequenceInputStream sequenceInputStream = null;
        FileOutputStream outputStream = null;
        try {
            FileOutputStream outputStream2;
            for (String name : toneList) {
                if (!TextUtils.isEmpty(name)) {
                    inputStreamList.add(manager.open(name));
                }
            }
            SequenceInputStream sequenceInputStream2 = new SequenceInputStream(Collections.enumeration(inputStreamList));
            try {
                outputStream2 = new FileOutputStream(destFilePath);
            } catch (IOException e2) {
                e = e2;
                sequenceInputStream = sequenceInputStream2;
                try {
                    e.printStackTrace();
                    Debug.m4e("HmVoice", e.getMessage());
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                            return null;
                        }
                    }
                    if (sequenceInputStream != null) {
                        sequenceInputStream.close();
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e32) {
                            e32.printStackTrace();
                            throw th;
                        }
                    }
                    if (sequenceInputStream != null) {
                        sequenceInputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                sequenceInputStream = sequenceInputStream2;
                if (outputStream != null) {
                    outputStream.close();
                }
                if (sequenceInputStream != null) {
                    sequenceInputStream.close();
                }
                throw th;
            }
            try {
                byte[] arraybyte = new byte[4096];
                while (true) {
                    int len = sequenceInputStream2.read(arraybyte);
                    if (len <= 0) {
                        break;
                    }
                    outputStream2.write(arraybyte, 0, len);
                    outputStream2.flush();
                }
                File file = new File(destFilePath);
                if (outputStream2 != null) {
                    try {
                        outputStream2.close();
                    } catch (IOException e322) {
                        e322.printStackTrace();
                        return file;
                    }
                }
                if (sequenceInputStream2 == null) {
                    return file;
                }
                sequenceInputStream2.close();
                return file;
            } catch (IOException e4) {
                e322 = e4;
                outputStream = outputStream2;
                sequenceInputStream = sequenceInputStream2;
                e322.printStackTrace();
                Debug.m4e("HmVoice", e322.getMessage());
                if (outputStream != null) {
                    outputStream.close();
                }
                if (sequenceInputStream != null) {
                    sequenceInputStream.close();
                }
                return null;
            } catch (Throwable th4) {
                th = th4;
                outputStream = outputStream2;
                sequenceInputStream = sequenceInputStream2;
                if (outputStream != null) {
                    outputStream.close();
                }
                if (sequenceInputStream != null) {
                    sequenceInputStream.close();
                }
                throw th;
            }
        } catch (IOException e5) {
            e322 = e5;
            e322.printStackTrace();
            Debug.m4e("HmVoice", e322.getMessage());
            if (outputStream != null) {
                outputStream.close();
            }
            if (sequenceInputStream != null) {
                sequenceInputStream.close();
            }
            return null;
        }
    }

    private MediaFileOp() {
    }
}
