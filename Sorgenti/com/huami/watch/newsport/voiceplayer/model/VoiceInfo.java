package com.huami.watch.newsport.voiceplayer.model;

public class VoiceInfo {
    public float mDistance;
    public float mKmUsedTime;
    public float mPace;
    public float mSpeed;
    public int mSportType = -1;

    public VoiceInfo(int sportType) {
        this.mSportType = sportType;
    }

    public String toString() {
        return String.valueOf(this.mDistance);
    }
}
