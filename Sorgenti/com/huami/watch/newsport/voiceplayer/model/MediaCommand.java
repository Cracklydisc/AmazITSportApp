package com.huami.watch.newsport.voiceplayer.model;

import android.content.Context;
import android.net.Uri;

public class MediaCommand {
    public Context mContext;
    public boolean mLooping;
    public int mStreamType;
    public Uri mUri;
    public float mVolume;
}
