package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class PausePendingState implements IState {
    public int state() {
        return 4;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() == 2 || state.state() == 5 || state.state() == 3) {
            owner.setState(state);
            return true;
        }
        Debug.m4e("PausePendingState", "pause pending state cannot change to " + state);
        return false;
    }
}
