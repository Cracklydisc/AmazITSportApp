package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class ContinuePendingState implements IState {
    public int state() {
        return 5;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() == 1 || state.state() == 4) {
            owner.setState(state);
            return true;
        }
        Debug.m4e("ContinuePendingState", "continue pending state cannot change to " + state);
        return false;
    }
}
