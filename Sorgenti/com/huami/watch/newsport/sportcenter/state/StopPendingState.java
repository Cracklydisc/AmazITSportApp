package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class StopPendingState implements IState {
    public int state() {
        return 6;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() != 0) {
            Debug.m4e("StopPendingState", "stop pending state cannot change to " + state);
            return false;
        }
        owner.setState(state);
        return true;
    }
}
