package com.huami.watch.newsport.sportcenter.state;

public interface IState {

    public interface StateOwner {
        void setState(IState iState);
    }

    boolean changeStateIfIllegal(StateOwner stateOwner, IState iState);

    int state();
}
