package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class PausedState implements IState {
    public int state() {
        return 2;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() == 6 || state.state() == 5 || state.state() == 1) {
            owner.setState(state);
            return true;
        }
        Debug.m4e("PausedState", "paused state cannot change to " + state);
        return false;
    }
}
