package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class RunningState implements IState {
    public int state() {
        return 1;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() == 6 || state.state() == 4 || state.state() == 2) {
            owner.setState(state);
            return true;
        }
        Debug.m4e("RunningState", "running state cannot change to " + state);
        return false;
    }
}
