package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class StartPendingState implements IState {
    public int state() {
        return 3;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() == 1 || state.state() == 4 || state.state() == 6) {
            owner.setState(state);
            return true;
        }
        Debug.m4e("StartPendingState", "start pending state cannot change to " + state);
        return false;
    }
}
