package com.huami.watch.newsport.sportcenter.state;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;

public class StopState implements IState {
    public int state() {
        return 0;
    }

    public boolean changeStateIfIllegal(StateOwner owner, IState state) {
        if (state.state() != 3) {
            Debug.m4e("StopState", "stopped state cannot change to " + state);
            return false;
        }
        owner.setState(state);
        return true;
    }
}
