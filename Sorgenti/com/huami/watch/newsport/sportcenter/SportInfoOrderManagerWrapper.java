package com.huami.watch.newsport.sportcenter;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.customtrain.model.CustomIntervalTrain;
import com.huami.watch.newsport.sportcenter.action.ISportInfoOrderManager;
import com.huami.watch.newsport.sportcenter.action.ISportInfoOrderManager.SportInfo;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.controller.SportTrainManager.TrainUtils;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.utils.DateUtils;
import com.huami.watch.newsport.utils.DumpUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.SportDataItemInfo;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class SportInfoOrderManagerWrapper implements ISportInfoOrderManager {
    private static String mPauseHeartHint = null;
    private CustomIntervalTrain customIntervalTrain = null;
    private Context mContext;
    private volatile int mCurrnetCalence = -1;
    private boolean mIsMixedSport = false;
    private int mPosition = -1;
    private int[] mSportDataItemInfos;
    private int mSportType = -1;
    private TrainUnit trainUnit;

    public SportInfoOrderManagerWrapper(Context context, int sportType, boolean isMixedSport, int intervailTrainType, int intervalRemindType, boolean isOnlySingleTrainType) {
        this.mIsMixedSport = isMixedSport;
        this.mContext = context;
        this.mSportType = sportType;
        int[] order = DataManager.getInstance().getSportItemOrder(context, sportType);
        if (order == null) {
            switch (sportType) {
                case 1:
                    LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " intervailTrainType:" + intervailTrainType);
                    switch (intervailTrainType) {
                        case -1:
                            this.mSportDataItemInfos = SportDataItemInfo.mRunningInfoOrder;
                            break;
                        case 0:
                            this.mSportDataItemInfos = TrainUtils.getSportOrderByTrainUnit(TrainTargetType.values()[intervailTrainType], TrainRemindType.values()[intervalRemindType], isOnlySingleTrainType);
                            break;
                        case 1:
                            this.mSportDataItemInfos = TrainUtils.getSportOrderByTrainUnit(TrainTargetType.values()[intervailTrainType], TrainRemindType.values()[intervalRemindType], isOnlySingleTrainType);
                            break;
                        case 2:
                            this.mSportDataItemInfos = TrainUtils.getSportOrderByTrainUnit(TrainTargetType.values()[intervailTrainType], TrainRemindType.values()[intervalRemindType], isOnlySingleTrainType);
                            break;
                        default:
                            break;
                    }
                case 6:
                    this.mSportDataItemInfos = SportDataItemInfo.mWalkingInfoOrder;
                    break;
                case 7:
                    this.mSportDataItemInfos = SportDataItemInfo.mCrossingInfoOrder;
                    break;
                case 8:
                    this.mSportDataItemInfos = SportDataItemInfo.mIndoorRunningInfoOrder;
                    break;
                case 9:
                    this.mSportDataItemInfos = SportDataItemInfo.mOutdoorRidingInfoOrder;
                    break;
                case 10:
                    this.mSportDataItemInfos = SportDataItemInfo.mIndoorRidingInfoOrder;
                    break;
                case 11:
                    this.mSportDataItemInfos = SportDataItemInfo.mSkiingInfoOrder;
                    break;
                case 12:
                    this.mSportDataItemInfos = SportDataItemInfo.mEppiticalInfoOrder;
                    break;
                case 13:
                    this.mSportDataItemInfos = SportDataItemInfo.mMountaineerInfoOrder;
                    break;
                case 14:
                    this.mSportDataItemInfos = SportDataItemInfo.mSwimingInfoOrder;
                    break;
                case 15:
                    this.mSportDataItemInfos = SportDataItemInfo.mOpenWaterSwimingInfoOrder;
                    break;
                case 17:
                    this.mSportDataItemInfos = SportDataItemInfo.mTennisInfoOrder;
                    break;
                case 18:
                    this.mSportDataItemInfos = SportDataItemInfo.mSoccerInfoOrder;
                    break;
                case 1001:
                    this.mSportDataItemInfos = SportDataItemInfo.mMixedRunningInfoOrder;
                    break;
                case 1009:
                    this.mSportDataItemInfos = SportDataItemInfo.mMixedRidingInfoOrder;
                    break;
                case 1015:
                    this.mSportDataItemInfos = SportDataItemInfo.mMixedOpenSwimInfoOrder;
                    break;
            }
        }
        List<Integer> tempList;
        int i;
        int[] tempOrder;
        if (SportType.isSwimMode(this.mSportType)) {
            tempList = new ArrayList();
            for (i = 0; i < order.length; i++) {
                if (5 != order[i]) {
                    tempList.add(Integer.valueOf(order[i]));
                }
            }
            tempOrder = new int[tempList.size()];
            for (i = 0; i < tempOrder.length; i++) {
                tempOrder[i] = ((Integer) tempList.get(i)).intValue();
            }
            order = tempOrder;
        } else if (this.mSportType == 9 || this.mSportType == 10) {
            tempList = new ArrayList();
            for (i = 0; i < order.length; i++) {
                if (15 != order[i]) {
                    tempList.add(Integer.valueOf(order[i]));
                }
            }
            tempOrder = new int[tempList.size()];
            for (i = 0; i < tempOrder.length; i++) {
                tempOrder[i] = ((Integer) tempList.get(i)).intValue();
            }
            order = tempOrder;
        }
        if (this.mSportType != 1 || intervailTrainType == -1) {
            this.mSportDataItemInfos = order;
        } else {
            this.mSportDataItemInfos = TrainUtils.getSportOrderByTrainUnit(TrainTargetType.values()[intervailTrainType], TrainRemindType.values()[intervalRemindType], isOnlySingleTrainType);
            LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " mSportDataItemInfo: trainType:" + intervailTrainType + ",remindType:" + intervalRemindType + ",isOnlySingle:" + isOnlySingleTrainType + "," + DumpUtils.println(this.mSportDataItemInfos));
        }
        LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " mSportDataItemInfos:" + DumpUtils.println(this.mSportDataItemInfos));
        this.mSportType = sportType;
        mPauseHeartHint = context.getString(C0532R.string.sport_pause_heart_shown);
    }

    public int getSportItemSize() {
        if (this.mSportDataItemInfos != null) {
            return this.mSportDataItemInfos.length;
        }
        return 0;
    }

    public SportInfo getSportInfo(int position, OutdoorSportSnapshot sportStatus) {
        if (this.mSportDataItemInfos == null || sportStatus == null) {
            Debug.m6w("SportInfoOrderManagerWrapper", "crossing info not initial");
            return null;
        }
        if (position >= this.mSportDataItemInfos.length || position < 0) {
            Debug.m6w("SportInfoOrderManagerWrapper", "invalid position " + position + ". current data info length " + this.mSportDataItemInfos.length);
            return null;
        }
        SportInfo sportInfo = new SportInfo();
        sportInfo.itemValueRemind = -1;
        sportInfo.dataItemType = this.mSportDataItemInfos[position];
        int i;
        switch (this.mSportDataItemInfos[position]) {
            case 1:
                sportInfo.mValue = DataFormatUtils.parseCrossingSecondToDefaultFormattedTimeContainHour((long) sportStatus.getCurrentSecond());
                sportInfo.mUnitRes = C0532R.string.running_cost_time_desc;
                sportInfo.mValueTextSize = 22;
                sportInfo.mMillValue = String.format("%02d", new Object[]{Integer.valueOf(sportStatus.getCurrentMillSec())});
                return sportInfo;
            case 2:
                sportInfo.mValue = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportStatus.getDistance()), false);
                if (this.mSportType == 14 || this.mSportType == 15 || this.mSportType == 1015) {
                    sportInfo.mUnitRes = C0532R.string.detail_lap_dis;
                } else {
                    sportInfo.mUnitRes = C0532R.string.running_distance_desc;
                }
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 3:
                if (isValidNormalPace(sportStatus.getRealTimePace())) {
                    sportInfo.mValue = parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(sportStatus.getRealTimePace()));
                } else {
                    sportInfo.mValue = mPauseHeartHint;
                }
                sportInfo.mUnitRes = C0532R.string.running_pace_desc;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 4:
                if (isValidNormalPace(sportStatus.getTotalPace())) {
                    sportInfo.mValue = parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(sportStatus.getTotalPace()));
                } else {
                    sportInfo.mValue = mPauseHeartHint;
                }
                sportInfo.mUnitRes = C0532R.string.running_avg_pace_desc;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 5:
                if (sportStatus.getCurrentStatus() == (byte) 2 || sportStatus.getHeartQuantity() > 1) {
                    sportInfo.mValue = mPauseHeartHint;
                    sportInfo.mHeartRange = -1;
                } else {
                    sportInfo.mValue = String.format("%.0f", new Object[]{Float.valueOf(SportDataFilterUtils.parseHeartRate(sportStatus.getHeartRate()))});
                    sportInfo.mHeartRange = sportStatus.getHeartRange();
                }
                sportInfo.mUnitRes = C0532R.string.running_heartrate_desc;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 6:
                sportInfo.mValue = String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) sportStatus.getCalorie()))});
                sportInfo.mUnitRes = C0532R.string.running_calorie_desc;
                sportInfo.mValueTextSize = 22;
                return sportInfo;
            case 7:
                float absoluteAltitude = sportStatus.getAbsoluteAtitude();
                if (absoluteAltitude == -20000.0f) {
                    sportInfo.mValue = mPauseHeartHint;
                } else {
                    sportInfo.mValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) absoluteAltitude, this.mSportType)));
                }
                sportInfo.mUnitRes = C0532R.string.route_cur_altitude;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 8:
                float climbUp = sportStatus.getClimbUp();
                if (climbUp < 0.0f) {
                    sportInfo.mValue = String.valueOf("0");
                } else {
                    sportInfo.mValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbUp, this.mSportType)));
                }
                sportInfo.mUnitRes = C0532R.string.sport_main_climb_al;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 9:
                float climbDown = sportStatus.getClimbDown();
                if (climbDown < 0.0f) {
                    sportInfo.mValue = String.valueOf("0");
                } else {
                    sportInfo.mValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbDown, this.mSportType)));
                }
                sportInfo.mUnitRes = C0532R.string.sport_main_descend_al;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 10:
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(sportStatus.getRealTimeSpeed())), true);
                sportInfo.mUnitRes = C0532R.string.running_speed_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 11:
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(sportStatus.getTotalSpeed())), true);
                sportInfo.mUnitRes = C0532R.string.running_avg_speed_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 12:
                sportInfo.mValue = String.valueOf(UnitConvertUtils.convertStepFreqToTimesPerMin((double) SportDataFilterUtils.parseStepFreq(sportStatus.getRealTimeStepFreq())));
                sportInfo.mUnitRes = C0532R.string.running_step_freq_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 13:
                int climbDiscend = (int) sportStatus.getClimbdisascend();
                if (climbDiscend < 0) {
                    sportInfo.mValue = String.valueOf("0");
                } else {
                    sportInfo.mValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbDiscend, this.mSportType)));
                }
                sportInfo.mUnitRes = C0532R.string.sport_main_climb_dis;
                if (position == 0) {
                    i = 22;
                } else {
                    i = 22;
                }
                sportInfo.mValueTextSize = i;
                return sportInfo;
            case 14:
                sportInfo.mValue = String.valueOf(sportStatus.getStepCount());
                sportInfo.mUnitRes = C0532R.string.walking_step_count;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 15:
                if (sportStatus.getCadence() < 0.0f) {
                    sportInfo.mValue = mPauseHeartHint;
                } else {
                    sportInfo.mValue = String.valueOf((int) sportStatus.getCadence());
                }
                sportInfo.mUnitRes = C0532R.string.outdoor_cadence;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 16:
                sportInfo.mValue = String.valueOf(sportStatus.getTotalTrips());
                sportInfo.mUnitRes = C0532R.string.rt_swim_trips;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 17:
                sportInfo.mValue = String.valueOf(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistance2YDOrMeter((double) sportStatus.getRtDistancePerStroke(), sportStatus.getUnit()), false));
                sportInfo.mUnitRes = C0532R.string.swim_dps;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 18:
                sportInfo.mValue = String.valueOf(Math.round(sportStatus.getRtStrokeSpeed() * 60.0f));
                sportInfo.mUnitRes = C0532R.string.swim_stroke_speed;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 19:
                sportInfo.mValue = String.valueOf(Math.round(sportStatus.getAvgStrokeSpeed() * 60.0f));
                sportInfo.mUnitRes = C0532R.string.swim_avg_stroke_speed;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 20:
                if (isValidSwimPace(sportStatus.getRealTimePace(), sportStatus.getUnit())) {
                    sportInfo.mValue = getPacePer100Meter(sportStatus.getRealTimePace(), sportStatus.getUnit());
                } else {
                    sportInfo.mValue = mPauseHeartHint;
                }
                sportInfo.mUnitRes = C0532R.string.running_pace_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 21:
                if (isValidSwimPace(sportStatus.getTotalPace(), sportStatus.getUnit())) {
                    sportInfo.mValue = getPacePer100Meter(sportStatus.getTotalPace(), sportStatus.getUnit());
                } else {
                    sportInfo.mValue = mPauseHeartHint;
                }
                sportInfo.mUnitRes = C0532R.string.running_avg_pace_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 22:
                sportInfo.mValue = "" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) sportStatus.getDistance()), sportStatus.getUnit()));
                if (this.mSportType == 14 || this.mSportType == 15 || this.mSportType == 1015) {
                    sportInfo.mUnitRes = C0532R.string.detail_lap_dis;
                } else {
                    sportInfo.mUnitRes = C0532R.string.running_distance_desc;
                }
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 23:
                sportInfo.mValue = String.format("%.1f", new Object[]{Float.valueOf(((float) sportStatus.getmSportEteTrainingEffect()) / 10.0f)});
                sportInfo.mUnitRes = C0532R.string.sport_data_info_te_unit;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 24:
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertSpeedToMeterPerMin((double) SportDataFilterUtils.parseSpeed(Math.abs(sportStatus.getVerticalVelocity()))), true);
                sportInfo.mUnitRes = C0532R.string.crossing_vertical_speed;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                if (sportStatus.getVerticalVelocity() == 0.0f) {
                    sportInfo.mImgRes = 0;
                    return sportInfo;
                }
                sportInfo.mImgRes = sportStatus.getVerticalVelocity() > 0.0f ? 1 : 2;
                return sportInfo;
            case 25:
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(sportStatus.getDownhillAvgSpeed())), true);
                sportInfo.mUnitRes = C0532R.string.running_avg_speed_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 26:
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(sportStatus.getDownhillMaxSpeed())), true);
                sportInfo.mImgRes = 3;
                sportInfo.mUnitRes = C0532R.string.skiing_downhill_max_speed;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 27:
                sportInfo.mValue = String.valueOf(sportStatus.getDownHillNum());
                sportInfo.mUnitRes = C0532R.string.skiing_downhill_num;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 28:
                sportInfo.mValue = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportStatus.getSingleDownhillDescend()), false);
                sportInfo.mImgRes = 3;
                sportInfo.mUnitRes = C0532R.string.skiing_downhill_single_descend;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 29:
                float skiDescend = sportStatus.getClimbdisDescend();
                if (skiDescend < 0.0f) {
                    sportInfo.mValue = String.valueOf("0");
                } else {
                    sportInfo.mValue = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) skiDescend), false);
                }
                sportInfo.mUnitRes = C0532R.string.skiing_rt_downhill_desecnd;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 30:
                sportInfo.mValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) sportStatus.getSingleDownhillElveLoss(), this.mSportType)));
                sportInfo.mImgRes = 3;
                sportInfo.mUnitRes = C0532R.string.skiing_downhill_elev_loss;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 31:
                sportInfo.mValue = String.valueOf(sportStatus.getmStrokes());
                sportInfo.mUnitRes = C0532R.string.sport_tennis_strokes;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            case 250:
                LogUtil.m9i(true, "SportInfoOrderManagerWrapper", "dataItemSurplusDistance:" + sportStatus.getTrainSnapShot().toJSON());
                this.trainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
                sportInfo.needRun = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(Float.toString(((float) this.trainUnit.getTargetDistance()) / 1000.0f))), false);
                sportInfo.mValue = parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(Float.toString(((float) (this.trainUnit.getTargetDistance() - sportStatus.getTrainSnapShot().getCurUnitDis())) / 1000.0f))), RoundingMode.UP);
                sportInfo.mUnitRes = C0532R.string.data_item_surplus_distance;
                return sportInfo;
            case 251:
                this.trainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
                LogUtil.m9i(true, "SportInfoOrderManagerWrapper", "dataItemTrainTotalDistance:" + sportStatus.getTrainSnapShot().toJSON());
                StringBuilder distanceBuffer = new StringBuilder();
                distanceBuffer.append(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm(NumeriConversionUtils.getDoubleValue(Float.toString(sportStatus.getDistance()))), false));
                distanceBuffer.append("/");
                distanceBuffer.append(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm(NumeriConversionUtils.getDoubleValue(Integer.toString(sportStatus.getTrainSnapShot().getTotalTargetDistance()))), false));
                sportInfo.mValue = distanceBuffer.toString();
                sportInfo.mUnitRes = C0532R.string.data_item_train_total_mileage;
                return sportInfo;
            case 252:
                this.trainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
                LogUtil.m9i(true, "SportInfoOrderManagerWrapper", "dataItemTrainTotlaTime:" + sportStatus.getTrainSnapShot().toJSON());
                String usedMinute = DateUtils.getTimeFormatDefaultOnlyMinute((long) sportStatus.getCurrentSecond());
                String usedSecond = DateUtils.getTimeFormatDefaultOnlySecond((long) sportStatus.getCurrentSecond());
                String totalMinute = String.valueOf((int) ((sportStatus.getTrainSnapShot().getTotalTargetCostTime() / 1000) / 60));
                sportInfo.interValUsedMinute = usedMinute;
                sportInfo.interValUsedSecond = usedSecond;
                sportInfo.interValTotalTimes = totalMinute;
                sportInfo.intervalTotalTimeStr = getSpannableString(this.mContext, usedMinute, usedSecond, totalMinute);
                sportInfo.itemValueRemind = C0532R.string.time_picker_minute;
                sportInfo.mUnitRes = C0532R.string.data_item_train_total_times;
                return sportInfo;
            case 253:
                this.trainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
                sportInfo.needRun = DataFormatUtils.parseCrossingSecondToDefaultFormattedTime(this.trainUnit.getTargetTime() / 1000);
                LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " dataItemSurplusTime:" + sportStatus.getTrainSnapShot().toJSON() + ", " + sportInfo.needRun);
                long remainTime = this.trainUnit.getTargetTime() - sportStatus.getTrainSnapShot().getCurUnitTime();
                if (remainTime < 0) {
                    remainTime = 0;
                }
                sportInfo.mValue = DataFormatUtils.parseCrossingSecondToDefaultFormattedTime(remainTime / 1000);
                sportInfo.mUnitRes = C0532R.string.data_item_surplus_time;
                return sportInfo;
            case 254:
                sportInfo.mValue = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportStatus.getDistance()), false);
                sportInfo.mUnitRes = C0532R.string.running_distance_desc;
                sportInfo.mValueTextSize = position == 0 ? 22 : 22;
                return sportInfo;
            default:
                return sportInfo;
        }
    }

    private SpannableString getSpannableString(Context mcontext, String userMinutes, String usedSecond, String totalTime) {
        String result = String.format(mcontext.getResources().getString(C0532R.string.test_html_txt), new Object[]{userMinutes, usedSecond, totalTime});
        Log.i("SportInfoOrderManagerWrapper", "getSpannableString:" + result);
        SpannableString msp = new SpannableString(result);
        if (isZh(this.mContext)) {
            msp.setSpan(new RelativeSizeSpan(1.0f), 0, 2, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 2, 3, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 3, 4, 33);
            msp.setSpan(new RelativeSizeSpan(1.0f), 4, 6, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 6, 7, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 7, 8, 33);
            msp.setSpan(new RelativeSizeSpan(1.0f), 8, 9, 33);
            msp.setSpan(new RelativeSizeSpan(1.0f), 9, result.length() - 1, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), result.length() - 1, result.length(), 33);
        } else {
            msp.setSpan(new RelativeSizeSpan(1.0f), 0, 2, 33);
            msp.setSpan(new TypefaceSpan("RobotoCondensed-Regular"), 2, 5, 17);
            msp.setSpan(new ForegroundColorSpan(Color.parseColor("#ff00ff")), 2, 5, 33);
            msp.setSpan(new AbsoluteSizeSpan(18), 2, 5, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 5, 6, 33);
            msp.setSpan(new AbsoluteSizeSpan(18), 5, 6, 33);
            msp.setSpan(new RelativeSizeSpan(1.0f), 6, 8, 33);
            msp.setSpan(new TypefaceSpan("RobotoCondensed-Regular"), 8, 11, 17);
            msp.setSpan(new ForegroundColorSpan(Color.parseColor("#ff00ff")), 8, 11, 33);
            msp.setSpan(new AbsoluteSizeSpan(18), 8, 11, 33);
            msp.setSpan(new RelativeSizeSpan(0.2f), 11, 12, 33);
            msp.setSpan(new AbsoluteSizeSpan(18), 11, 12, 33);
            msp.setSpan(new RelativeSizeSpan(1.0f), 12, result.length() - 3, 33);
            msp.setSpan(new TypefaceSpan("RobotoCondensed-Regular"), result.length() - 3, result.length(), 17);
            msp.setSpan(new ForegroundColorSpan(Color.parseColor("#ff00ff")), result.length() - 3, result.length(), 33);
            msp.setSpan(new AbsoluteSizeSpan(18), result.length() - 3, result.length(), 33);
        }
        return msp;
    }

    private boolean isZh(Context mContext) {
        if (mContext.getResources().getConfiguration().locale.getLanguage().endsWith("zh")) {
            return true;
        }
        return false;
    }

    public int getTimePickerPosition() {
        if (this.mSportDataItemInfos != null && this.mSportDataItemInfos.length > 0) {
            for (int i = 0; i < this.mSportDataItemInfos.length; i++) {
                if (this.mSportDataItemInfos[i] == 1) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean isShowMillSec() {
        return true;
    }

    public void refreshSportDataItem() {
        if (SportDataManager.getInstance().getCurrentTrainUnit() == null) {
            LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " SportDataItem is null");
            return;
        }
        this.mSportDataItemInfos = SportDataManager.getInstance().getCurrentTrainUnit().getItemOrders();
        LogUtil.m9i(true, "SportInfoOrderManagerWrapper", " SportDataItem:" + DumpUtils.println(this.mSportDataItemInfos));
    }

    private boolean isValidNormalPace(float pace) {
        if (Float.compare(pace, 0.0f) == 0) {
            return false;
        }
        int standardSec = 1800;
        if (this.mSportType == 6) {
            standardSec = 3600;
        }
        if (((int) (1000.0f * pace)) <= standardSec) {
            return true;
        }
        return false;
    }

    private boolean isValidSwimPace(float pace, int unit) {
        if (Float.compare(pace, 0.0f) != 0 && ((int) (UnitConvertUtils.convertpace2MeterOrYD((double) pace, unit) * 100.0d)) <= 600) {
            return true;
        }
        return false;
    }

    private String getPacePer100Meter(float pace, int unit) {
        Debug.m5i("SportInfoOrderManagerWrapper", "getPacePer100Meter, pace:" + pace);
        return parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(pace, unit));
    }

    public static String parseSecondPerMeterToFormattedPace(float secondPerMeter) {
        int secondPerKm = Math.round(1000.0f * secondPerMeter);
        StringBuilder sb = new StringBuilder();
        if (secondPerKm >= 5999) {
            sb.append("99").append("'").append("99").append("\"");
            return sb.toString();
        }
        int second = secondPerKm % 60;
        sb.append(secondPerKm / 60);
        sb.append("'");
        if (second > 9) {
            sb.append(second);
        } else {
            sb.append("0").append(second);
        }
        sb.append("\"");
        return sb.toString();
    }

    public static String parseSecondPerMeterToFormatted100meterPace(float secondPerMeter) {
        int secondPerHunmeter = Math.round(100.0f * secondPerMeter);
        StringBuilder sb = new StringBuilder();
        if (secondPerHunmeter >= 5999) {
            sb.append("99").append("'").append("99").append("\"");
            return sb.toString();
        }
        int second = secondPerHunmeter % 60;
        sb.append(secondPerHunmeter / 60);
        sb.append("'");
        if (second > 9) {
            sb.append(second);
        } else {
            sb.append("0").append(second);
        }
        sb.append("\"");
        return sb.toString();
    }

    public static String parseFormattedRealNumber(double realNumber, boolean rounding) {
        if (rounding) {
            return parseFormattedRealNumber(realNumber, RoundingMode.HALF_UP);
        }
        return parseFormattedRealNumber(realNumber, RoundingMode.FLOOR);
    }

    public static String parseFormattedRealNumber(double realNumber, RoundingMode mode) {
        if (realNumber <= 0.0d) {
            return "0.00";
        }
        if (Double.isNaN(realNumber) || Double.isInfinite(realNumber)) {
            Debug.m5i(DataFormatUtils.class.getName(), "parseFormattedRealNumber, value is invalid:" + realNumber);
            return "0.00";
        }
        BigDecimal decimal = new BigDecimal(String.valueOf(realNumber));
        if (realNumber < 10.0d) {
            return decimal.setScale(2, mode).toString();
        }
        if (realNumber < 100.0d) {
            return decimal.setScale(1, mode).toString();
        }
        return "" + ((int) realNumber);
    }
}
