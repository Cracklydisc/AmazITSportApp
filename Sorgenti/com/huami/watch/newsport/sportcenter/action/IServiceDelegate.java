package com.huami.watch.newsport.sportcenter.action;

import com.huami.watch.common.db.Callback;

public interface IServiceDelegate {
    boolean childSportStart(Callback callback);

    boolean childSportStop(Callback callback, boolean z);

    boolean continu(Callback callback);

    long getTrackId();

    boolean pause(Callback callback);

    boolean start(Callback callback);

    boolean stop(Callback callback, boolean z);
}
