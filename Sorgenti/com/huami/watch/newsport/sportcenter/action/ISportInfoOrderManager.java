package com.huami.watch.newsport.sportcenter.action;

import android.text.SpannableString;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;

public interface ISportInfoOrderManager {

    public static class SportInfo {
        public int dataItemType = -1;
        public String interValTotalTimes = "";
        public String interValUsedMinute = "";
        public String interValUsedSecond = "";
        public SpannableString intervalTotalTimeStr = null;
        public int itemValueRemind = -1;
        public int mHeartRange = -1;
        public int mImgRes = 0;
        public String mMillValue = null;
        public int mUnitRes = 0;
        public String mValue = null;
        public int mValueTextSize = 0;
        public String needRun = null;

        public int getHeartRange() {
            return this.mHeartRange;
        }

        public int getUnitRes() {
            return this.mUnitRes;
        }

        public String getValue() {
            return this.mValue;
        }

        public String getMillValue() {
            return this.mMillValue;
        }

        public int getImgRes() {
            return this.mImgRes;
        }

        public String getNeedRun() {
            return this.needRun;
        }

        public int getDataItemType() {
            return this.dataItemType;
        }

        public String getInterValUsedMinute() {
            return this.interValUsedMinute;
        }

        public String getInterValUsedSecond() {
            return this.interValUsedSecond;
        }

        public String getInterValTotalTimes() {
            return this.interValTotalTimes;
        }

        public String toString() {
            return "SportInfo{mValue='" + this.mValue + '\'' + ", mMillValue='" + this.mMillValue + '\'' + ", mUnitRes=" + this.mUnitRes + ", mValueTextSize=" + this.mValueTextSize + ", mImgRes=" + this.mImgRes + ", mHeartRange=" + this.mHeartRange + ", needRun=" + this.needRun + ", itemValueRemind=" + this.itemValueRemind + ", dataItemType=" + this.dataItemType + '}';
        }
    }

    SportInfo getSportInfo(int i, OutdoorSportSnapshot outdoorSportSnapshot);

    int getSportItemSize();

    int getTimePickerPosition();

    boolean isShowMillSec();
}
