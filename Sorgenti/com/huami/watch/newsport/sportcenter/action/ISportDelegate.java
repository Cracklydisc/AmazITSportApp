package com.huami.watch.newsport.sportcenter.action;

import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportOptions;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.sportcenter.model.CurLapInfo;
import com.huami.watch.newsport.train.model.TrainUnit;
import java.util.List;

public interface ISportDelegate extends IServiceDelegate {

    public interface IDelegateDataListener {
        void doNextTrainUnitStart();

        void doWholeTrainUnitCompleted();

        void onManualOrAutoLapStart(boolean z);

        void onMillUpdate(int i);

        void onRouteRunTrackOffset(int i);

        void onSingleTrainUnitCompleted();

        void onSingleTrainUnitNearlyCompleted();

        void onSportAutoPaused();

        void onSportAutoResume();

        void onSportStatusUpdate(ISportDelegate iSportDelegate, SportSnapshot sportSnapshot);

        void onStateUpdateErr();
    }

    void addAutoLapsInfo(RunningInfoPerLap runningInfoPerLap);

    void addCadenceSummaryData(CyclingDataSummary cyclingDataSummary);

    void doRecordOneLap();

    List<RunningInfoPerLap> getAutoLapsInfo();

    int getAutoPauseStatusInScreenOff();

    CurLapInfo getCurLapInfo();

    int getCurSportType();

    long getCurrentTimeMillisOnRuntime();

    TrainUnit getCurrentTrainUnit();

    List<HeartRate> getHeartRatesInfo();

    int getNextSportType();

    SportSnapshot getRecentSportStatus();

    SportOptions getSportOptions();

    SportSummary getSportSummary();

    boolean hasNextSport();

    boolean isPlayVoice();

    boolean isValidSportWhenEndSport();

    void notifyHalfSecStart();

    void notifySportDisplayStatus(boolean z);

    void notifyStartNextCustomTrainUnit();

    void saveCadenceDetailList(List<CyclingDetail> list);

    void setCurrnetCaldengceValue(int i);

    void setIsGiveupRecord(boolean z);

    void setSportDataListener(IDelegateDataListener iDelegateDataListener);

    void shutDown();
}
