package com.huami.watch.newsport.sportcenter.action;

import android.os.Parcelable;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;

public interface ISportDataManagerCallback {
    void changeStateErr();

    void doEvent(int i, Parcelable... parcelableArr);

    void doNextTrainUnitStart();

    void doSingleTrainUnitCompleted();

    void doSingleTrainUnitNearlyCompleted();

    void doWholeTrainUnitCompleted();

    void newOutdoorMillSecUpdate(int i);

    void newOutdoorSportData(OutdoorSportSnapshot outdoorSportSnapshot);

    void remindUserRouteOffset(int i);
}
