package com.huami.watch.newsport.sportcenter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.telecom.Log;
import android.util.SparseArray;
import android.widget.Toast;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Route;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.hs.gpxparser.utils.LogUtils;
import com.hs.gpxparser.utils.SDCardGPSUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.PauseInfo;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportOptions;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.db.SportDatabaseControlCenter;
import com.huami.watch.newsport.gps.callback.IPointProvider;
import com.huami.watch.newsport.gps.controller.LocationPool;
import com.huami.watch.newsport.gps.controller.LocationPool.ILocationPoolListener;
import com.huami.watch.newsport.gps.controller.PointProviderManager;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.gps.utils.SportPointTypeParser;
import com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager;
import com.huami.watch.newsport.historydata.controller.GPSHistoryDataManager;
import com.huami.watch.newsport.historydata.controller.HeartHistoryDataManager;
import com.huami.watch.newsport.historydata.listener.IHistoryDataListener;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.reminder.controller.ReminderManager.IReminderListener;
import com.huami.watch.newsport.sportcenter.action.ISportDelegate;
import com.huami.watch.newsport.sportcenter.action.ISportDelegate.IDelegateDataListener;
import com.huami.watch.newsport.sportcenter.action.ISportHeartListener;
import com.huami.watch.newsport.sportcenter.action.ISportLocationListener;
import com.huami.watch.newsport.sportcenter.model.CurLapInfo;
import com.huami.watch.newsport.sportcenter.state.ContinuePendingState;
import com.huami.watch.newsport.sportcenter.state.IState;
import com.huami.watch.newsport.sportcenter.state.IState.StateOwner;
import com.huami.watch.newsport.sportcenter.state.PausePendingState;
import com.huami.watch.newsport.sportcenter.state.PausedState;
import com.huami.watch.newsport.sportcenter.state.RunningState;
import com.huami.watch.newsport.sportcenter.state.StartPendingState;
import com.huami.watch.newsport.sportcenter.state.StopPendingState;
import com.huami.watch.newsport.sportcenter.state.StopState;
import com.huami.watch.newsport.syncservice.DetailInfoSaver;
import com.huami.watch.newsport.syncservice.UploadCacheManager;
import com.huami.watch.newsport.timeticker.action.ISportTicker;
import com.huami.watch.newsport.timeticker.action.ITickerRemind;
import com.huami.watch.newsport.timeticker.controller.TickerManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.controller.SportTrainManager;
import com.huami.watch.newsport.train.controller.SportTrainManager.ITrainUnitAction;
import com.huami.watch.newsport.train.model.TrainSnapShot;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.utils.DateUtils;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.PowerUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.WearHabitDefaultValHelper;
import com.huami.watch.newsport.wakeup.controller.WakeupManagerFactory;
import com.huami.watch.newsport.wakeup.obsever.AbsWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.AbsWakeupManager.IWakeupListener;
import com.huami.watch.newsport.wakeup.obsever.SleepTimeWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.TimeCostWakeUpManager;
import com.huami.watch.newsport.xmlparser.bean.TERemindStage;
import com.huami.watch.newsport.xmlparser.utils.SAXUtils;
import com.huami.watch.sensor.HmGpsLocation;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensorhub.SensorHubProtos.SportConfig;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class OutdoorSportDelegate implements ILocationPoolListener, IReminderListener, ISportDelegate, ISportHeartListener, ISportLocationListener, StateOwner, ITickerRemind, ITrainUnitAction {
    private Handler mAlgoThreadHandler = null;
    private List<HeartRate> mAllHeartRateList = new ArrayList();
    private List<Integer> mAutoSportStatusQuene = new LinkedList();
    private List<HeartRate> mAvaiableHeartRateList = new ArrayList();
    private long mBaseElapsedTime = SystemClock.elapsedRealtime();
    private long mBaseTime = System.currentTimeMillis();
    List<CyclingDataSummary> mCadenceSummaryList = new ArrayList();
    private Context mContext = null;
    private IState mContinuePendingState = new ContinuePendingState();
    private CurLapInfo mCurLapInfo = null;
    private long mCurrentSportTrackId = -1;
    private int mCurrentSportType = -1;
    private IState mCurrentState = this.mStopState;
    private OutdoorSportSnapshot mCurrentStatus = null;
    private int mCurrnetCaldenceValue = -1;
    private List<DailyPerformanceInfo> mDailyPerformanceInfoList = new LinkedList();
    private DataManager mDataManager = null;
    private List<PauseInfo> mDynamicPauseRecords = new LinkedList();
    private AbsHistoryDataManager mHeartRateManager = null;
    private long mHeartRateTotalPausedTime = 0;
    private boolean mIsAvailableMixedSport = false;
    private boolean mIsForeground = true;
    private boolean mIsGiveupRecord = false;
    private boolean mIsImperial = false;
    private boolean mIsInitLap = false;
    private boolean mIsIntervalTrainMode = false;
    private boolean mIsStopped = false;
    private int mLapPausedTime = 0;
    private int mLastKilometer = 0;
    private long mLastLapEndTime = System.currentTimeMillis();
    private int mLastLapStepCount = 0;
    private int mLastLapStrokes = 0;
    private int mLastLapTotalDistance = 0;
    private long mLastPausedTimestamp = -1;
    private int mLastStartSportIndex = -1;
    private OutdoorSportSnapshot mLastStatus = null;
    private PauseInfo mLatestPausedTime = null;
    private IDelegateDataListener mListener = null;
    private SportLocationData mLocationData = null;
    private AbsHistoryDataManager mLocationManager = null;
    private LocationPool mLocationPool = null;
    private Object mLock = new Object();
    private int mMaxHeartRate = Integer.MIN_VALUE;
    private long mMaxTimestamp = -1;
    private int mMinHeartRate = Integer.MAX_VALUE;
    private OutdoorSportSummary mOutdoorSummary = null;
    private IState mPausePendingState = new PausePendingState();
    private IState mPausedState = new PausedState();
    private int mPausedTime = 0;
    private IPointProvider mPointProvider = PointProviderManager.getInstance().getPointProvider();
    private List<RunningInfoPerLap> mPreviousAutoLapsInfo = new ArrayList();
    private List<RunningInfoPerLap> mPreviousManualLapsInfo = new ArrayList();
    private List<Long> mRecordTrackIdArray = new LinkedList();
    private ReminderManager mReminderManager = null;
    private List<RunningInfoPerKM> mRunningInfoPerKMs = new LinkedList();
    private IState mRunningState = new RunningState();
    private int mSaveFlags = 0;
    private SensorHubManagerWrapper mSensorHubManagerWrapper = null;
    private int mSingleHeartCount = 0;
    private int mSingleHeartSum = 0;
    private SleepTimeWakeupManager mSleepTimeWakeupWakeupManager = null;
    private BaseConfig mSportConfig = null;
    private SportDatabaseControlCenter mSportDBControlCenter;
    private final IHistoryDataListener<HeartRate> mSportHeartRateListener = new IHistoryDataListener<HeartRate>() {
        public void onHistoryDataReady(List<HeartRate> data) {
            OutdoorSportDelegate.this.onSportHeartReady(data);
            if (OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId) != null) {
                OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId).saveHistoryHeartData(data, false);
            }
        }
    };
    private List<Integer> mSportList = new ArrayList();
    private final IHistoryDataListener<HmGpsLocation> mSportLocationListener = new IHistoryDataListener<HmGpsLocation>() {
        public void onHistoryDataReady(List<HmGpsLocation> data) {
            OutdoorSportDelegate.this.onSportLocationReady(data);
        }
    };
    private SportOptions mSportOptions = null;
    private SportPointTypeParser mSportPointTypeParser = new SportPointTypeParser();
    private List<SportSummary> mSportSummaryList = new ArrayList();
    private ISportTicker mSportTickerManager = null;
    private IState mStartPendingState = new StartPendingState();
    private long mStartTime = -1;
    private int mStepCount = 0;
    private float mStepStride = 0.0f;
    private IState mStopPendingState = new StopPendingState();
    private IState mStopState = new StopState();
    private volatile byte mStopStep = (byte) 0;
    private long mStopTime = -1;
    private SparseArray<TERemindStage> mTeRemindSettings = null;
    private long mTickerStartTime = -1;
    private TimeCostWakeUpManager mTimeCostWakeupManager = null;
    private float mTodayDistance = 0.0f;
    private float mTotalDistance = 0.0f;
    private int mTotalHeartCount = 0;
    private int mTotalHeartSum = 0;
    private OutdoorSportSummary mTotalSportSummary = null;
    private long mTotalTime = 0;
    private long mTrackId = -1;
    private SportTrainManager mTrainManager = null;
    private UploadCacheManager mUploadCacheManager = null;
    private final IWakeupListener mWakeupListener = new IWakeupListener() {
        public void wakeup(AbsWakeupManager source, Object... params) {
            Debug.m5i("OutdoorSportDelegate", "wakeup by source : " + source + ", type: " + source.getWakeupSourceType());
            if (source.getWakeupSourceType() == 3) {
                if (params == null || params.length == 0) {
                    Debug.m6w("OutdoorSportDelegate", "params is null or empty");
                }
            } else if (source.getWakeupSourceType() == 7) {
                if (params == null || params.length == 0) {
                    Debug.m6w("OutdoorSportDelegate", "auto pause params is null or empty");
                    return;
                }
                final int pauseStatus = ((Integer) params[0]).intValue();
                Global.getGlobalWorkHandler().post(new Runnable() {
                    public void run() {
                        OutdoorSportDelegate.this.autoPause(pauseStatus);
                    }
                });
            } else if (source.getWakeupSourceType() == 10) {
                if (params == null || params.length == 0) {
                    Debug.m6w("OutdoorSportDelegate", "params is null or empty");
                    return;
                }
                int routeStatus = ((Integer) params[0]).intValue();
                Log.i("OutdoorSportDelegate", "-- route　offset to remind user routeStatus:" + routeStatus, new Object[0]);
                OutdoorSportDelegate.this.remindUserRouteOffset(routeStatus);
            } else if (source.getWakeupSourceType() == 13) {
                if (OutdoorSportDelegate.this.mPreviousAutoLapsInfo != null && OutdoorSportDelegate.this.mPreviousAutoLapsInfo.size() > 0) {
                    RunningInfoPerLap lastLap = (RunningInfoPerLap) OutdoorSportDelegate.this.mPreviousAutoLapsInfo.get(OutdoorSportDelegate.this.mPreviousAutoLapsInfo.size() - 1);
                    OutdoorSportDelegate.this.mCurLapInfo = new CurLapInfo();
                    OutdoorSportDelegate.this.mCurLapInfo.setFlag(1);
                    OutdoorSportDelegate.this.mCurLapInfo.setNumber(OutdoorSportDelegate.this.mPreviousAutoLapsInfo.size());
                    OutdoorSportDelegate.this.mCurLapInfo.setRunningTime(lastLap.getCostTime());
                    if (OutdoorSportDelegate.this.mListener != null) {
                        OutdoorSportDelegate.this.mListener.onManualOrAutoLapStart(false);
                    }
                }
            } else if (source.getWakeupSourceType() == 2) {
                OutdoorSportDelegate.this.notifyTrainUnitFinished();
            } else if (source.getWakeupSourceType() == 8) {
                OutdoorSportDelegate.this.notifyTrainUnitFinished();
            } else if (source.getWakeupSourceType() == 17) {
                OutdoorSportDelegate.this.notifyTrainUnitInAdvance();
            }
        }
    };
    private WakeupManagerFactory mWakeupManagerFactory = WakeupManagerFactory.getInstance();

    class C06328 extends Callback {
        C06328() {
        }

        protected void doCallback(int resultCode, Object params) {
            LogUtil.m9i(true, "OutdoorSportDelegate", "saveNecessaryDataWhenSportStopped, delete sport_summary finished");
        }
    }

    static /* synthetic */ float access$3916(OutdoorSportDelegate x0, float x1) {
        float f = x0.mTotalDistance + x1;
        x0.mTotalDistance = f;
        return f;
    }

    static /* synthetic */ byte access$4176(OutdoorSportDelegate x0, int x1) {
        byte b = (byte) (x0.mStopStep | x1);
        x0.mStopStep = b;
        return b;
    }

    public OutdoorSportDelegate(Context context, SportOptions options) {
        this.mContext = context;
        this.mSportDBControlCenter = new SportDatabaseControlCenter(context);
        this.mSensorHubManagerWrapper = SensorHubManagerWrapper.getInstance(context);
        this.mReminderManager = ReminderManager.getInstance();
        this.mReminderManager.initReminder(options, this.mContext);
        this.mReminderManager.setReminderListener(this);
        this.mDataManager = DataManager.getInstance();
        this.mSportOptions = options;
        this.mSportConfig = DataManager.getInstance().getSportConfig(this.mContext, options.getSportType());
        if (this.mSportOptions.getSportType() == 15 || SportType.isMixedSport(this.mSportOptions.getSportType())) {
            this.mSportConfig.setTargetSwimLength(100);
            DataManager.getInstance().setSportConfig(this.mContext, this.mSportConfig);
        }
        this.mUploadCacheManager = UploadCacheManager.getInstance(this.mContext);
        initTrainManager();
        if (this.mSportConfig.getTargetTEStage() < 0) {
            this.mSportConfig.setIsRemindTE(false);
            DataManager.getInstance().setSportConfig(context, this.mSportConfig);
        }
        if (SportType.isSwimMode(this.mSportOptions.getSportType())) {
            this.mSportConfig.setIsRemindSafeHeartRate(false);
            this.mSportConfig.setIsRemindCalBurnHeartRate(false);
            if (this.mSportConfig.isRemindPlayVoice()) {
                this.mSportConfig.setIsRemindPlayVoice(false);
                DataManager.getInstance().setSportConfig(context, this.mSportConfig);
            }
        }
        if (!SportType.isMixedSport(this.mSportOptions.getSportType())) {
            this.mSportList.add(Integer.valueOf(this.mSportOptions.getSportType()));
        } else if (this.mSportConfig instanceof MixedBaseConfig) {
            this.mSportList.clear();
            this.mSportList.addAll(((MixedBaseConfig) this.mSportConfig).getChildSports());
        }
        this.mLocationPool = new LocationPool(this);
        this.mAlgoThreadHandler = Global.getGlobalWorkHandler();
        this.mSportTickerManager = TickerManager.getInstance();
        this.mRecordTrackIdArray = new LinkedList();
        this.mRecordTrackIdArray.clear();
        this.mIsStopped = false;
        this.mTeRemindSettings = SAXUtils.getTERemindSettingsFromXml(context);
        this.mIsImperial = UnitConvertUtils.isImperial();
    }

    private void initTrainManager() {
        if (this.mSportOptions.getIntervalType() == 1) {
            this.mTrainManager = new SportTrainManager(this.mContext, this);
            this.mTrainManager.addTrainProgram(this.mSportOptions.getTrainProgram());
            this.mTrainManager.setTrainCallback(this);
            this.mIsIntervalTrainMode = true;
        }
    }

    private void initTimeObserver() {
        this.mTimeCostWakeupManager = (TimeCostWakeUpManager) this.mWakeupManagerFactory.getTimeCostWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig);
        this.mSleepTimeWakeupWakeupManager = (SleepTimeWakeupManager) this.mWakeupManagerFactory.getTimerArrivalWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig);
        if (this.mTimeCostWakeupManager != null && !this.mIsIntervalTrainMode && !this.mSportConfig.isRemindTE() && this.mSportConfig.isRemindTargetTimeCost()) {
            this.mTimeCostWakeupManager.setIsSportStarted(true);
            this.mTimeCostWakeupManager.startTimeCostAlarm((getCurrentTimeMillisOnRuntime() - this.mTrackId) / 1000);
        }
    }

    private void autoPause(int pauseStatus) {
        if (this.mCurrentState == this.mStopPendingState || this.mCurrentState == this.mStopState) {
            LogUtil.m9i(true, "OutdoorSportDelegate", "autoPause failed, cur state is stopped");
            return;
        }
        if (pauseStatus == 1) {
            doPause(true);
            if (this.mListener != null) {
                this.mListener.onSportAutoPaused();
            }
        } else if (pauseStatus == 2) {
            doContinue(true);
            if (this.mListener != null) {
                this.mListener.onSportAutoResume();
            }
        }
        if (this.mIsForeground) {
            clearAutoPauseStatus();
        } else {
            addAutoPauseStatus(pauseStatus);
        }
    }

    private void remindUserRouteOffset(int routeStatus) {
        if (this.mListener != null) {
            this.mListener.onRouteRunTrackOffset(routeStatus);
        }
    }

    private void initHistoryDataManager() {
        if (!Global.DEBUG_GPS) {
            this.mLocationManager = new GPSHistoryDataManager(this.mContext, this.mCurrentSportTrackId, this.mCurrentSportType);
            this.mHeartRateManager = new HeartHistoryDataManager(this.mContext, this.mCurrentSportTrackId, this.mCurrentSportType, this.mLastStartSportIndex + 1, this);
            ((HeartHistoryDataManager) this.mHeartRateManager).setCurrentStatus(this.mCurrentStatus);
        } else if (!Global.AUTO_GPS) {
        }
    }

    private void registerWakeupManagers() {
        this.mWakeupManagerFactory.getAutoPauseWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceRepeatWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceRepeatMileWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getExtNormalHeartrateWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getLowPaceWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getTimeCostWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getGPSStatusChangeWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getTimerArrivalWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getRouteOffsetWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getWearLoosWakupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getCalorieWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getLapWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDailyPromanceSixMinuteWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getRealTimeGuideWakeUpManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig).registerWakeupListener(this.mWakeupListener);
        initTimeObserver();
    }

    private void unregisterWakeupManagers() {
        this.mWakeupManagerFactory.getAutoPauseWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceRepeatWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceRepeatMileWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDistanceWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getExtNormalHeartrateWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getLowPaceWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getTimeCostWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getGPSStatusChangeWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getTimerArrivalWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getRouteOffsetWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getWearLoosWakupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getCalorieWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getLapWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getDailyPromanceSixMinuteWakeupManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.getRealTimeGuideWakeUpManager(this.mContext, this.mSportOptions.getSportType()).unregisterWakeupListener(this.mWakeupListener);
        this.mWakeupManagerFactory.clearWakeupManager();
    }

    public SportOptions getSportOptions() {
        return this.mSportOptions;
    }

    public int getCurSportType() {
        return this.mCurrentSportType;
    }

    public synchronized void setState(IState state) {
        Debug.m3d("OutdoorSportDelegate", "setState : " + state);
        this.mCurrentState = state;
    }

    public void setSportDataListener(IDelegateDataListener listener) {
        this.mListener = listener;
    }

    public SportSnapshot getRecentSportStatus() {
        return this.mCurrentStatus;
    }

    public void addAutoLapsInfo(RunningInfoPerLap infoPerIndividual) {
        if (infoPerIndividual == null) {
            Debug.m5i("OutdoorSportDelegate", "auto lap is null");
            return;
        }
        infoPerIndividual.setTrackId(this.mTrackId);
        if (this.mPreviousAutoLapsInfo != null) {
            this.mPreviousAutoLapsInfo.add(infoPerIndividual);
            Debug.m5i("OutdoorSportDelegate", "per lap info:" + infoPerIndividual.toString());
        }
    }

    public List<RunningInfoPerLap> getAutoLapsInfo() {
        return this.mPreviousAutoLapsInfo;
    }

    public void notifySportDisplayStatus(boolean isForeground) {
        Debug.m5i("OutdoorSportDelegate", "notifySportDisplayStatus, isForeground:" + isForeground);
        this.mIsForeground = isForeground;
        if (this.mSleepTimeWakeupWakeupManager != null) {
            this.mSleepTimeWakeupWakeupManager.notifySportDisplayStatus(isForeground);
        }
        if (this.mSportTickerManager != null) {
            this.mSportTickerManager.notifySportDisplayStatus(isForeground);
        }
    }

    public synchronized void addAutoPauseStatus(int pauseStatus) {
        if (this.mAutoSportStatusQuene == null) {
            Debug.m5i("OutdoorSportDelegate", "Error, auto sport quene is null");
        } else {
            this.mAutoSportStatusQuene.clear();
            this.mAutoSportStatusQuene.add(Integer.valueOf(pauseStatus));
            Debug.m5i("OutdoorSportDelegate", "auto add pauseStatus:" + pauseStatus);
        }
    }

    public synchronized void clearAutoPauseStatus() {
        if (!(this.mAutoSportStatusQuene == null || this.mAutoSportStatusQuene.isEmpty())) {
            this.mAutoSportStatusQuene.clear();
        }
        Debug.m5i("OutdoorSportDelegate", "clearAutoPauseStatus");
    }

    public synchronized int getAutoPauseStatusInScreenOff() {
        int i = 0;
        synchronized (this) {
            if (this.mAutoSportStatusQuene != null && this.mAutoSportStatusQuene.size() == 1) {
                i = ((Integer) this.mAutoSportStatusQuene.get(0)).intValue();
                this.mAutoSportStatusQuene.clear();
            }
        }
        return i;
    }

    public SportSummary getSportSummary() {
        return this.mTotalSportSummary;
    }

    public int getNextSportType() {
        if (this.mSportList == null || this.mSportList.size() <= 0) {
            return -1;
        }
        return ((Integer) this.mSportList.get(0)).intValue();
    }

    private void initCurSportType() {
        this.mCurrentSportType = ((Integer) this.mSportList.remove(0)).intValue();
        Debug.m5i("OutdoorSportDelegate", "cur sport:" + this.mCurrentSportType);
    }

    private void initSingleSport() {
        this.mIsInitLap = false;
        this.mPausedTime = 0;
        this.mLapPausedTime = 0;
        this.mLastLapTotalDistance = 0;
        this.mSingleHeartCount = 0;
        this.mMaxHeartRate = Integer.MIN_VALUE;
        this.mMinHeartRate = Integer.MAX_VALUE;
        this.mSingleHeartSum = 0;
        this.mHeartRateTotalPausedTime = 0;
        this.mMaxTimestamp = -1;
        this.mLastKilometer = 0;
        this.mStepCount = 0;
        this.mStepStride = 0.0f;
        this.mTotalTime = 0;
        this.mStopStep = (byte) 0;
        this.mRunningInfoPerKMs.clear();
        this.mPreviousManualLapsInfo.clear();
        this.mSaveFlags = 0;
    }

    public boolean start(final Callback callback) {
        Debug.m3d("OutdoorSportDelegate", "start");
        this.mTrackId = getCurrentTimeMillisOnRuntime();
        this.mTrackId -= this.mTrackId % 1000;
        long tickerStartTime = SystemClock.elapsedRealtime();
        this.mTickerStartTime = tickerStartTime;
        this.mTickerStartTime -= tickerStartTime % 1000;
        if (SportType.isMixedSport(this.mSportOptions.getSportType())) {
            this.mSensorHubManagerWrapper.startMixedSport(this.mTrackId, new Callback() {

                class C06191 extends Callback {
                    C06191() {
                    }

                    protected void doCallback(int resultCode, Object params) {
                        OutdoorSportDelegate.this.mTotalSportSummary = OutdoorSportSummary.createOutdoorSportSummary(OutdoorSportDelegate.this.mSportOptions.getSportType());
                        if (OutdoorSportDelegate.this.mTotalSportSummary == null) {
                            throw new IllegalArgumentException("Illegal sport type : " + OutdoorSportDelegate.this.mSportOptions.getSportType());
                        }
                        OutdoorSportDelegate.this.mTotalSportSummary.setTrackId(OutdoorSportDelegate.this.mTrackId);
                        OutdoorSportDelegate.this.mTotalSportSummary.setStartTime(OutdoorSportDelegate.this.mCurrentSportTrackId);
                        float disPerLap = OutdoorSportDelegate.this.mSportConfig.getDistanceAutoLap();
                        if (disPerLap < 0.0f) {
                            disPerLap = 1000.0f;
                        }
                        OutdoorSportDelegate.this.mTotalSportSummary.setDisPerLap(disPerLap);
                        OutdoorSportDelegate.this.mTotalSportSummary.setCurrentStatus(1);
                        OutdoorSportDelegate.this.mSportDBControlCenter.asyncAddSportSummary(OutdoorSportDelegate.this.mTotalSportSummary, null);
                        if (OutdoorSportDelegate.this.mSportOptions.getSportType() != 8) {
                            OutdoorSportDelegate.this.mUploadCacheManager.addSportInfo(OutdoorSportDelegate.this.mTrackId);
                        }
                        OutdoorSportDelegate.this.updateTodayAndTotalDistance();
                        if (resultCode != 0) {
                            Debug.m6w("OutdoorSportDelegate", "error occur while call sensorhub start sport. error code : " + resultCode + ". message : " + params);
                            if (callback != null) {
                                callback.notifyCallback(-1, null);
                            }
                        } else if (callback != null) {
                            callback.notifyCallback(0, null);
                        }
                    }
                }

                protected void doCallback(int resultCode, Object params) {
                    if (resultCode != 0) {
                        Debug.m6w("OutdoorSportDelegate", "error occur while call sensorhub start sport. error code : " + resultCode + ". message : " + params);
                        if (callback != null) {
                            callback.notifyCallback(-1, null);
                            return;
                        }
                        return;
                    }
                    OutdoorSportDelegate.this.childSportStart(new C06191());
                }
            });
        } else {
            childSportStart(new Callback() {
                protected void doCallback(int resultCode, Object params) {
                    OutdoorSportDelegate.this.mTotalSportSummary = OutdoorSportSummary.createOutdoorSportSummary(OutdoorSportDelegate.this.mCurrentSportType);
                    if (OutdoorSportDelegate.this.mTotalSportSummary == null) {
                        throw new IllegalArgumentException("Illegal sport type : " + OutdoorSportDelegate.this.mSportOptions.getSportType());
                    }
                    OutdoorSportDelegate.this.mTotalSportSummary.setTrackId(OutdoorSportDelegate.this.mTrackId);
                    OutdoorSportDelegate.this.mTotalSportSummary.setStartTime(OutdoorSportDelegate.this.mCurrentSportTrackId);
                    float disPerLap = OutdoorSportDelegate.this.mSportConfig.getDistanceAutoLap();
                    if (disPerLap < 0.0f) {
                        disPerLap = 1000.0f;
                    }
                    OutdoorSportDelegate.this.mTotalSportSummary.setDisPerLap(disPerLap);
                    OutdoorSportDelegate.this.mTotalSportSummary.setCurrentStatus(1);
                    OutdoorSportDelegate.this.mSportDBControlCenter.asyncAddSportSummary(OutdoorSportDelegate.this.mTotalSportSummary, null);
                    if (OutdoorSportDelegate.this.mSportOptions.getSportType() != 8) {
                        OutdoorSportDelegate.this.mUploadCacheManager.addSportInfo(OutdoorSportDelegate.this.mTrackId);
                    }
                    OutdoorSportDelegate.this.updateTodayAndTotalDistance();
                    if (resultCode != 0) {
                        Debug.m6w("OutdoorSportDelegate", "error occur while call sensorhub start sport. error code : " + resultCode + ". message : " + params);
                        if (callback != null) {
                            callback.notifyCallback(-1, null);
                        }
                    } else if (callback != null) {
                        callback.notifyCallback(0, null);
                    }
                }
            });
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean childSportStart(final com.huami.watch.common.db.Callback r9) {
        /*
        r8 = this;
        monitor-enter(r8);
        r0 = r8.mCurrentState;	 Catch:{ all -> 0x00b5 }
        r1 = r8.mStartPendingState;	 Catch:{ all -> 0x00b5 }
        r0 = r0.changeStateIfIllegal(r8, r1);	 Catch:{ all -> 0x00b5 }
        if (r0 != 0) goto L_0x0047;
    L_0x000b:
        r0 = "OutdoorSportDelegate";
        r1 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00b5 }
        r1.<init>();	 Catch:{ all -> 0x00b5 }
        r4 = "change state failed. from : ";
        r1 = r1.append(r4);	 Catch:{ all -> 0x00b5 }
        r4 = r8.mCurrentState;	 Catch:{ all -> 0x00b5 }
        r4 = r4.getClass();	 Catch:{ all -> 0x00b5 }
        r4 = r4.getSimpleName();	 Catch:{ all -> 0x00b5 }
        r1 = r1.append(r4);	 Catch:{ all -> 0x00b5 }
        r4 = " to ";
        r1 = r1.append(r4);	 Catch:{ all -> 0x00b5 }
        r4 = r8.mStartPendingState;	 Catch:{ all -> 0x00b5 }
        r4 = r4.getClass();	 Catch:{ all -> 0x00b5 }
        r4 = r4.getSimpleName();	 Catch:{ all -> 0x00b5 }
        r1 = r1.append(r4);	 Catch:{ all -> 0x00b5 }
        r1 = r1.toString();	 Catch:{ all -> 0x00b5 }
        com.huami.watch.common.log.Debug.m6w(r0, r1);	 Catch:{ all -> 0x00b5 }
        r8.onStateErr();	 Catch:{ all -> 0x00b5 }
        r0 = 0;
        monitor-exit(r8);	 Catch:{ all -> 0x00b5 }
    L_0x0046:
        return r0;
    L_0x0047:
        monitor-exit(r8);	 Catch:{ all -> 0x00b5 }
        r0 = r8.mPointProvider;
        r0.reset();
        r8.initCurSportType();
        r2 = -1;
        r0 = r8.mSportOptions;
        r0 = r0.getSportType();
        r0 = com.huami.watch.newsport.common.model.SportType.isMixedSport(r0);
        if (r0 != 0) goto L_0x00b8;
    L_0x005e:
        r0 = r8.mTrackId;
        r8.mCurrentSportTrackId = r0;
        r2 = r8.mCurrentSportTrackId;
    L_0x0064:
        r0 = "OutdoorSportDelegate";
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r4 = "current sport id:";
        r1 = r1.append(r4);
        r6 = r8.mCurrentSportTrackId;
        r1 = r1.append(r6);
        r1 = r1.toString();
        com.huami.watch.common.log.Debug.m5i(r0, r1);
        r0 = r8.mSportOptions;
        r0 = r0.getSportType();
        r8.setRouteDataToSensorhub(r0);
        r8.initCurrentStatus();
        r0 = r8.mCurrentSportType;
        r0 = com.huami.watch.newsport.common.model.OutdoorSportSummary.createOutdoorSportSummary(r0);
        r8.mOutdoorSummary = r0;
        r0 = r8.mOutdoorSummary;
        if (r0 != 0) goto L_0x00d0;
    L_0x0096:
        r0 = new java.lang.IllegalArgumentException;
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r4 = "Illegal sport type : ";
        r1 = r1.append(r4);
        r4 = r8.mSportOptions;
        r4 = r4.getSportType();
        r1 = r1.append(r4);
        r1 = r1.toString();
        r0.<init>(r1);
        throw r0;
    L_0x00b5:
        r0 = move-exception;
        monitor-exit(r8);	 Catch:{ all -> 0x00b5 }
        throw r0;
    L_0x00b8:
        r0 = r8.getCurrentTimeMillisOnRuntime();
        r8.mCurrentSportTrackId = r0;
        r2 = r8.mCurrentSportTrackId;
        r0 = r8.mCurrentSportTrackId;
        r6 = r8.mTrackId;
        r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1));
        if (r0 != 0) goto L_0x0064;
    L_0x00c8:
        r0 = r8.mCurrentSportTrackId;
        r6 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r0 = r0 + r6;
        r8.mCurrentSportTrackId = r0;
        goto L_0x0064;
    L_0x00d0:
        r0 = com.huami.watch.newsport.common.manager.DataManager.getInstance();
        r0 = r0.getLastSportIndex();
        r8.mLastStartSportIndex = r0;
        r0 = r8.mWakeupManagerFactory;
        r0.clearWakeupManager();
        r8.registerWakeupManagers();
        r0 = r8.mSportConfig;
        r8.checkHeartRegionValue(r0);
        r0 = r8.mSportConfig;
        r8.notifyAlgorithmConfig(r0);
        r0 = r8.mSportOptions;
        r0 = r0.getSportType();
        r1 = 8;
        if (r0 == r1) goto L_0x00fd;
    L_0x00f6:
        r0 = r8.mUploadCacheManager;
        r6 = r8.mCurrentSportTrackId;
        r0.addSportInfo(r6);
    L_0x00fd:
        r5 = new android.os.Bundle;
        r5.<init>();
        r0 = "sport_mode";
        r1 = r8.mSportOptions;
        r1 = r1.getIntervalType();
        r5.putInt(r0, r1);
        r0 = r8.mSensorHubManagerWrapper;
        r1 = r8.mCurrentSportType;
        r4 = r8.mLastStartSportIndex;
        r6 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$3;
        r7 = r8.mAlgoThreadHandler;
        r6.<init>(r7, r9);
        r0.startSport(r1, r2, r4, r5, r6);
        r0 = 1;
        goto L_0x0046;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.sportcenter.OutdoorSportDelegate.childSportStart(com.huami.watch.common.db.Callback):boolean");
    }

    private void checkHeartRegionValue(BaseConfig config) {
        if (config.getmHeartRegionValueMin() > config.getmHeartRegionValueMax()) {
            int userAge = UserInfoManager.getUserAge(this.mContext);
            int min = ((220 - userAge) * 50) / 100;
            int max = ((220 - userAge) * 60) / 100;
            switch (config.getmHeartRegionType()) {
                case 0:
                    min = ((220 - userAge) * 50) / 100;
                    max = ((220 - userAge) * 60) / 100;
                    break;
                case 1:
                    min = ((220 - userAge) * 60) / 100;
                    max = ((220 - userAge) * 70) / 100;
                    break;
                case 2:
                    min = ((220 - userAge) * 70) / 100;
                    max = ((220 - userAge) * 80) / 100;
                    break;
                case 3:
                    min = ((220 - userAge) * 80) / 100;
                    max = ((220 - userAge) * 90) / 100;
                    break;
                case 4:
                    min = ((220 - userAge) * 90) / 100;
                    max = ((220 - userAge) * 100) / 100;
                    break;
            }
            config.setmHeartRegionValueMin(min);
            config.setmHeartRegionValueMax(max);
        }
    }

    private void setRouteDataToSensorhub(final int sportType) {
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                String currentSelectFileName = null;
                String[] contents = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(OutdoorSportDelegate.this.mContext, sportType);
                if (contents.length == 2) {
                    String currentSelectdName = contents[0];
                    currentSelectFileName = contents[1];
                }
                if (currentSelectFileName != null) {
                    GPX gpx = SDCardGPSUtils.getWayPointsFromFileName(currentSelectFileName);
                    List<Waypoint> waypointList = null;
                    if (gpx == null || gpx.getTracks().size() <= 0) {
                        OutdoorSportDelegate.this.clearKernelGpsDataCache();
                        return;
                    }
                    Iterator<Track> trackIterator = gpx.getTracks().iterator();
                    while (trackIterator.hasNext()) {
                        waypointList = ((TrackSegment) ((Track) trackIterator.next()).getTrackSegments().get(0)).getWaypoints();
                    }
                    if (gpx.getTracks() == null || gpx.getTracks().size() == 0) {
                        HashSet<Route> routeHashSet = gpx.getRoutes();
                        if (routeHashSet != null && routeHashSet.size() > 0) {
                            Route route = (Route) routeHashSet.iterator().next();
                            if (route != null) {
                                waypointList = route.getRoutePoints();
                            }
                        }
                    }
                    byte[] listObjects = SDCardGPSUtils.getGPSToKernelData(OutdoorSportDelegate.this.mContext, waypointList);
                    if (listObjects != null && listObjects.length > 0) {
                        LogUtils.print("OutdoorSportDelegate", " gpx data to kernel　has data ");
                        HmSensorHubConfigManager.getHmSensorHubConfigManager(OutdoorSportDelegate.this.mContext).syncGpxTrailData(listObjects, listObjects.length);
                        return;
                    }
                    return;
                }
                OutdoorSportDelegate.this.clearKernelGpsDataCache();
            }
        });
    }

    private void clearKernelGpsDataCache() {
        LogUtils.print("OutdoorSportDelegate", " gpx data to kernel　has no data , clear hub cache  ");
        byte[] clearGPSDData = SDCardGPSUtils.getClearGPSToKernelData();
        HmSensorHubConfigManager.getHmSensorHubConfigManager(this.mContext).syncGpxTrailData(clearGPSDData, clearGPSDData.length);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean pause(final com.huami.watch.common.db.Callback r8) {
        /*
        r7 = this;
        r0 = "OutdoorSportDelegate";
        r1 = "pause";
        com.huami.watch.common.log.Debug.m3d(r0, r1);
        monitor-enter(r7);
        r0 = r7.mCurrentState;	 Catch:{ all -> 0x007b }
        r1 = r7.mPausePendingState;	 Catch:{ all -> 0x007b }
        r0 = r0.changeStateIfIllegal(r7, r1);	 Catch:{ all -> 0x007b }
        if (r0 != 0) goto L_0x003e;
    L_0x0012:
        r0 = "OutdoorSportDelegate";
        r1 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007b }
        r1.<init>();	 Catch:{ all -> 0x007b }
        r2 = "cannot change state from ";
        r1 = r1.append(r2);	 Catch:{ all -> 0x007b }
        r2 = r7.mCurrentState;	 Catch:{ all -> 0x007b }
        r1 = r1.append(r2);	 Catch:{ all -> 0x007b }
        r2 = " to ";
        r1 = r1.append(r2);	 Catch:{ all -> 0x007b }
        r2 = r7.mPausePendingState;	 Catch:{ all -> 0x007b }
        r1 = r1.append(r2);	 Catch:{ all -> 0x007b }
        r1 = r1.toString();	 Catch:{ all -> 0x007b }
        com.huami.watch.common.log.Debug.m6w(r0, r1);	 Catch:{ all -> 0x007b }
        r7.onStateErr();	 Catch:{ all -> 0x007b }
        r0 = 0;
        monitor-exit(r7);	 Catch:{ all -> 0x007b }
    L_0x003d:
        return r0;
    L_0x003e:
        monitor-exit(r7);	 Catch:{ all -> 0x007b }
        r7.doPausePending();
        r4 = new android.os.Bundle;
        r4.<init>();
        r0 = "sport_mode";
        r1 = r7.mSportOptions;
        r1 = r1.getIntervalType();
        r4.putInt(r0, r1);
        r0 = r7.mIsIntervalTrainMode;
        if (r0 == 0) goto L_0x0067;
    L_0x0056:
        r0 = r7.mTrainManager;
        if (r0 == 0) goto L_0x0067;
    L_0x005a:
        r0 = "train_unit";
        r1 = r7.mTrainManager;
        r2 = r7.mCurrentStatus;
        r1 = r1.getCurSLPTUnitInfo(r2);
        r4.putString(r0, r1);
    L_0x0067:
        r0 = r7.mSensorHubManagerWrapper;
        r1 = r7.mCurrentSportType;
        r2 = r7.getCurrentTimeMillisOnRuntime();
        r5 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$5;
        r6 = r7.mAlgoThreadHandler;
        r5.<init>(r6, r8);
        r0.pauseSport(r1, r2, r4, r5);
        r0 = 1;
        goto L_0x003d;
    L_0x007b:
        r0 = move-exception;
        monitor-exit(r7);	 Catch:{ all -> 0x007b }
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.sportcenter.OutdoorSportDelegate.pause(com.huami.watch.common.db.Callback):boolean");
    }

    private void doPausePending() {
        Debug.m5i("OutdoorSportDelegate", "doPausePending");
        if (this.mTimeCostWakeupManager != null) {
            this.mTimeCostWakeupManager.setIsSportStarted(false);
            this.mTimeCostWakeupManager.stopTimeCostAlarm();
        }
        this.mCurrentStatus.setCurrentStatus((byte) 2);
        this.mLastPausedTimestamp = getCurrentTimeMillisOnRuntime();
        this.mSportTickerManager.pauseTicker();
        this.mCurrentStatus.setCurrentPausedSecond(0);
        this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean doPause(boolean r4) {
        /*
        r3 = this;
        if (r4 == 0) goto L_0x0005;
    L_0x0002:
        r3.doPausePending();
    L_0x0005:
        monitor-enter(r3);
        if (r4 == 0) goto L_0x0010;
    L_0x0008:
        r0 = r3.mPausedState;	 Catch:{ all -> 0x0056 }
        r3.setState(r0);	 Catch:{ all -> 0x0056 }
    L_0x000d:
        monitor-exit(r3);	 Catch:{ all -> 0x0056 }
        r0 = 1;
    L_0x000f:
        return r0;
    L_0x0010:
        r0 = r3.mCurrentState;	 Catch:{ all -> 0x0056 }
        r1 = r3.mPausedState;	 Catch:{ all -> 0x0056 }
        r0 = r0.changeStateIfIllegal(r3, r1);	 Catch:{ all -> 0x0056 }
        if (r0 != 0) goto L_0x000d;
    L_0x001a:
        r0 = "OutdoorSportDelegate";
        r1 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0056 }
        r1.<init>();	 Catch:{ all -> 0x0056 }
        r2 = "change state failed. from : ";
        r1 = r1.append(r2);	 Catch:{ all -> 0x0056 }
        r2 = r3.mCurrentState;	 Catch:{ all -> 0x0056 }
        r2 = r2.getClass();	 Catch:{ all -> 0x0056 }
        r2 = r2.getSimpleName();	 Catch:{ all -> 0x0056 }
        r1 = r1.append(r2);	 Catch:{ all -> 0x0056 }
        r2 = " to ";
        r1 = r1.append(r2);	 Catch:{ all -> 0x0056 }
        r2 = r3.mPausedState;	 Catch:{ all -> 0x0056 }
        r2 = r2.getClass();	 Catch:{ all -> 0x0056 }
        r2 = r2.getSimpleName();	 Catch:{ all -> 0x0056 }
        r1 = r1.append(r2);	 Catch:{ all -> 0x0056 }
        r1 = r1.toString();	 Catch:{ all -> 0x0056 }
        com.huami.watch.common.log.Debug.m6w(r0, r1);	 Catch:{ all -> 0x0056 }
        r3.onStateErr();	 Catch:{ all -> 0x0056 }
        r0 = 0;
        monitor-exit(r3);	 Catch:{ all -> 0x0056 }
        goto L_0x000f;
    L_0x0056:
        r0 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0056 }
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.sportcenter.OutdoorSportDelegate.doPause(boolean):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean continu(final com.huami.watch.common.db.Callback r8) {
        /*
        r7 = this;
        r0 = 0;
        r1 = "OutdoorSportDelegate";
        r2 = "continue";
        com.huami.watch.common.log.Debug.m3d(r1, r2);
        monitor-enter(r7);
        r1 = r7.mCurrentState;	 Catch:{ all -> 0x008b }
        r2 = r7.mContinuePendingState;	 Catch:{ all -> 0x008b }
        r1 = r1.changeStateIfIllegal(r7, r2);	 Catch:{ all -> 0x008b }
        if (r1 != 0) goto L_0x004e;
    L_0x0013:
        r1 = "OutdoorSportDelegate";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x008b }
        r2.<init>();	 Catch:{ all -> 0x008b }
        r3 = "change state failed. from : ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x008b }
        r3 = r7.mCurrentState;	 Catch:{ all -> 0x008b }
        r3 = r3.getClass();	 Catch:{ all -> 0x008b }
        r3 = r3.getSimpleName();	 Catch:{ all -> 0x008b }
        r2 = r2.append(r3);	 Catch:{ all -> 0x008b }
        r3 = " to ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x008b }
        r3 = r7.mContinuePendingState;	 Catch:{ all -> 0x008b }
        r3 = r3.getClass();	 Catch:{ all -> 0x008b }
        r3 = r3.getSimpleName();	 Catch:{ all -> 0x008b }
        r2 = r2.append(r3);	 Catch:{ all -> 0x008b }
        r2 = r2.toString();	 Catch:{ all -> 0x008b }
        com.huami.watch.common.log.Debug.m6w(r1, r2);	 Catch:{ all -> 0x008b }
        r7.onStateErr();	 Catch:{ all -> 0x008b }
        monitor-exit(r7);	 Catch:{ all -> 0x008b }
    L_0x004d:
        return r0;
    L_0x004e:
        monitor-exit(r7);	 Catch:{ all -> 0x008b }
        r7.doContinuePending(r0);
        r4 = new android.os.Bundle;
        r4.<init>();
        r0 = "sport_mode";
        r1 = r7.mSportOptions;
        r1 = r1.getIntervalType();
        r4.putInt(r0, r1);
        r0 = r7.mIsIntervalTrainMode;
        if (r0 == 0) goto L_0x0077;
    L_0x0066:
        r0 = r7.mTrainManager;
        if (r0 == 0) goto L_0x0077;
    L_0x006a:
        r0 = "train_unit";
        r1 = r7.mTrainManager;
        r2 = r7.mCurrentStatus;
        r1 = r1.getCurSLPTUnitInfo(r2);
        r4.putString(r0, r1);
    L_0x0077:
        r0 = r7.mSensorHubManagerWrapper;
        r1 = r7.mCurrentSportType;
        r2 = r7.getCurrentTimeMillisOnRuntime();
        r5 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$6;
        r6 = r7.mAlgoThreadHandler;
        r5.<init>(r6, r8);
        r0.continueSport(r1, r2, r4, r5);
        r0 = 1;
        goto L_0x004d;
    L_0x008b:
        r0 = move-exception;
        monitor-exit(r7);	 Catch:{ all -> 0x008b }
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.sportcenter.OutdoorSportDelegate.continu(com.huami.watch.common.db.Callback):boolean");
    }

    private void doContinuePending(boolean auto) {
        Debug.m5i("OutdoorSportDelegate", "doContinuePending, auto:" + auto);
        updatePauseInfo(auto);
        if (this.mTimeCostWakeupManager != null) {
            this.mTimeCostWakeupManager.setIsSportStarted(true);
            if (this.mIsIntervalTrainMode) {
                TrainTargetType type = this.mTrainManager.getCurentTrainUnit().getUnitType();
                TrainSnapShot trainSnapShot = this.mCurrentStatus.getTrainSnapShot();
                if (type == TrainTargetType.TIME) {
                    this.mTimeCostWakeupManager.startTimeCostAlarm(getTotalRunningTime(true), true, (int) (trainSnapShot.getCurTargetCostTime() / 1000), false);
                }
            } else {
                this.mTimeCostWakeupManager.startTimeCostAlarm(getTotalRunningTime(true));
            }
        }
        this.mCurrentStatus.setCurrentStatus((byte) 1);
        this.mCurrentStatus.setCurrentPausedSecond(0);
        this.mSportTickerManager.continueTicker();
    }

    private boolean doContinue(boolean auto) {
        boolean z = true;
        if (auto) {
            doContinuePending(true);
        }
        synchronized (this) {
            if (auto) {
                setState(this.mRunningState);
            } else if (!this.mCurrentState.changeStateIfIllegal(this, this.mRunningState)) {
                Debug.m6w("OutdoorSportDelegate", "change state failed. from : " + this.mCurrentState.getClass().getSimpleName() + " to " + this.mRunningState.getClass().getSimpleName());
                onStateErr();
                z = false;
            }
        }
        return z;
    }

    public boolean stop(final Callback callback, boolean forcely) {
        childSportStop(new Callback() {
            protected void doCallback(int code, Object params) {
                Debug.m5i("test_update", "request the newest data");
                OutdoorSportDelegate.this.saveNecessaryDataWhenSportStopped(callback);
            }
        }, forcely);
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void saveNecessaryDataWhenSportStopped(com.huami.watch.common.db.Callback r27) {
        /*
        r26 = this;
        r0 = r26;
        r0 = r0.mLock;
        r19 = r0;
        monitor-enter(r19);
        r18 = "OutdoorSportDelegate";
        r20 = "saveNecessaryDataWhenSportStopped";
        r0 = r18;
        r1 = r20;
        com.huami.watch.common.log.Debug.m5i(r0, r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mCadenceSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 == 0) goto L_0x0023;
    L_0x001a:
        r0 = r26;
        r0 = r0.mCadenceSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18.clear();	 Catch:{ all -> 0x00f0 }
    L_0x0023:
        r0 = r26;
        r0 = r0.mReminderManager;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18.resetReminder();	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportTickerManager;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 == 0) goto L_0x003d;
    L_0x0034:
        r0 = r26;
        r0 = r0.mSportTickerManager;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18.stopTicker();	 Catch:{ all -> 0x00f0 }
    L_0x003d:
        r18 = 0;
        r0 = r18;
        r1 = r26;
        r1.mSportTickerManager = r0;	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.size();	 Catch:{ all -> 0x00f0 }
        r20 = 1;
        r0 = r18;
        r1 = r20;
        if (r0 >= r1) goto L_0x012e;
    L_0x0057:
        r18 = "OutdoorSportDelegate";
        r20 = "singSportStop sport summary is less than 1";
        r0 = r18;
        r1 = r20;
        com.huami.watch.common.log.Debug.m5i(r0, r1);	 Catch:{ all -> 0x00f0 }
        if (r27 == 0) goto L_0x00f3;
    L_0x0064:
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r24 = r0;
        r24 = r24.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r22 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$8;	 Catch:{ all -> 0x00f0 }
        r0 = r22;
        r1 = r26;
        r0.<init>();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r3 = r22;
        r0.asyncDeleteSportSummary(r1, r2, r3);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportOptions;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getSportType();	 Catch:{ all -> 0x00f0 }
        r18 = com.huami.watch.newsport.common.model.SportType.isMixedSport(r18);	 Catch:{ all -> 0x00f0 }
        if (r18 == 0) goto L_0x00e2;
    L_0x00b9:
        r0 = r26;
        r0 = r0.mSensorHubManagerWrapper;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mTrackId;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r22 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$9;	 Catch:{ all -> 0x00f0 }
        r0 = r22;
        r1 = r26;
        r2 = r27;
        r0.<init>(r2);	 Catch:{ all -> 0x00f0 }
        r23 = new android.os.Bundle;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r3 = r22;
        r4 = r23;
        r0.stopMixedSport(r1, r3, r4);	 Catch:{ all -> 0x00f0 }
    L_0x00e0:
        monitor-exit(r19);	 Catch:{ all -> 0x00f0 }
    L_0x00e1:
        return;
    L_0x00e2:
        r18 = 0;
        r20 = 0;
        r0 = r27;
        r1 = r18;
        r2 = r20;
        r0.notifyCallback(r1, r2);	 Catch:{ all -> 0x00f0 }
        goto L_0x00e0;
    L_0x00f0:
        r18 = move-exception;
        monitor-exit(r19);	 Catch:{ all -> 0x00f0 }
        throw r18;
    L_0x00f3:
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r24 = r0;
        r24 = r24.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.deleteSportSummary(r1, r2);	 Catch:{ all -> 0x00f0 }
        goto L_0x00e0;
    L_0x012e:
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.size();	 Catch:{ all -> 0x00f0 }
        r20 = 1;
        r0 = r18;
        r1 = r20;
        if (r0 != r1) goto L_0x029f;
    L_0x0140:
        r0 = r26;
        r0 = r0.mSportOptions;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getSportType();	 Catch:{ all -> 0x00f0 }
        r18 = com.huami.watch.newsport.common.model.SportType.isMixedSport(r18);	 Catch:{ all -> 0x00f0 }
        if (r18 != 0) goto L_0x029f;
    L_0x0150:
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = 0;
        r0 = r18;
        r1 = r20;
        r18 = r0.get(r1);	 Catch:{ all -> 0x00f0 }
        r18 = (com.huami.watch.newsport.common.model.OutdoorSportSummary) r18;	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r26;
        r1.mTotalSportSummary = r0;	 Catch:{ all -> 0x00f0 }
    L_0x0168:
        r0 = r26;
        r0 = r0.mIsStopped;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 != 0) goto L_0x01a4;
    L_0x0170:
        r0 = r26;
        r0 = r0.mDataManager;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mContext;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.addSummaryToStatistic(r1, r2);	 Catch:{ all -> 0x00f0 }
        r18 = com.huami.watch.newsport.common.manager.DataManager.getInstance();	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mContext;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r26;
        r0 = r0.mTodayDistance;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.setTodayDistance(r1, r2);	 Catch:{ all -> 0x00f0 }
    L_0x01a4:
        r0 = r26;
        r0 = r0.mSportOptions;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getSportType();	 Catch:{ all -> 0x00f0 }
        r20 = 8;
        r0 = r18;
        r1 = r20;
        if (r0 != r1) goto L_0x0572;
    L_0x01b6:
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = 8;
        r0 = r18;
        r1 = r20;
        r0.setCurrentStatus(r1);	 Catch:{ all -> 0x00f0 }
    L_0x01c5:
        if (r27 == 0) goto L_0x05be;
    L_0x01c7:
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r21 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$12;	 Catch:{ all -> 0x00f0 }
        r0 = r21;
        r1 = r26;
        r0.<init>();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.asyncUpdateSportSummary(r1, r2);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportOptions;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getSportType();	 Catch:{ all -> 0x00f0 }
        r18 = com.huami.watch.newsport.common.model.SportType.isMixedSport(r18);	 Catch:{ all -> 0x00f0 }
        if (r18 == 0) goto L_0x0583;
    L_0x01f5:
        r6 = new android.os.Bundle;	 Catch:{ all -> 0x00f0 }
        r6.<init>();	 Catch:{ all -> 0x00f0 }
        r18 = "today_distance";
        r0 = r26;
        r0 = r0.mTodayDistance;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r18;
        r1 = r20;
        r6.putFloat(r0, r1);	 Catch:{ all -> 0x00f0 }
        r18 = "total_distance";
        r0 = r26;
        r0 = r0.mTotalDistance;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r21 = 1148846080; // 0x447a0000 float:1000.0 double:5.676053805E-315;
        r20 = r20 / r21;
        r20 = java.lang.String.valueOf(r20);	 Catch:{ all -> 0x00f0 }
        r20 = com.huami.watch.newsport.utils.NumeriConversionUtils.getDoubleValue(r20);	 Catch:{ all -> 0x00f0 }
        r22 = 0;
        r20 = com.huami.watch.common.DataFormatUtils.parseTotalDistanceFormattedRealNumber(r20, r22);	 Catch:{ all -> 0x00f0 }
        r20 = java.lang.Float.parseFloat(r20);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r6.putFloat(r0, r1);	 Catch:{ all -> 0x00f0 }
        r18 = "OutdoorSportDelegate";
        r20 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r20.<init>();	 Catch:{ all -> 0x00f0 }
        r21 = "mixed slpt stop intent today dis:";
        r20 = r20.append(r21);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTodayDistance;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ all -> 0x00f0 }
        r21 = ", total dis:";
        r20 = r20.append(r21);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalDistance;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ all -> 0x00f0 }
        r20 = r20.toString();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        com.huami.watch.common.log.Debug.m5i(r0, r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSensorHubManagerWrapper;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mTrackId;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r22 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$13;	 Catch:{ all -> 0x00f0 }
        r0 = r22;
        r1 = r26;
        r2 = r27;
        r0.<init>(r2);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r3 = r22;
        r0.stopMixedSport(r1, r3, r6);	 Catch:{ all -> 0x00f0 }
    L_0x0280:
        r18 = com.huami.watch.newsport.Global.getGlobalHeartHandler();	 Catch:{ all -> 0x00f0 }
        r20 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$14;	 Catch:{ all -> 0x00f0 }
        r0 = r20;
        r1 = r26;
        r0.<init>();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r0.post(r1);	 Catch:{ all -> 0x00f0 }
    L_0x0294:
        r18 = 1;
        r0 = r18;
        r1 = r26;
        r1.mIsStopped = r0;	 Catch:{ all -> 0x00f0 }
        monitor-exit(r19);	 Catch:{ all -> 0x00f0 }
        goto L_0x00e1;
    L_0x029f:
        r0 = r26;
        r0 = r0.mIsAvailableMixedSport;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 == 0) goto L_0x0440;
    L_0x02a7:
        r0 = r26;
        r0 = r0.mIsGiveupRecord;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 != 0) goto L_0x0440;
    L_0x02af:
        r10 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r11 = 2147483647; // 0x7fffffff float:NaN double:1.060997895E-314;
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18.clearChildTrackIdsAndTypes();	 Catch:{ all -> 0x00f0 }
        r16 = 0;
        r17 = 0;
        r8 = 0;
        r15 = 0;
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r9 = r18.iterator();	 Catch:{ all -> 0x00f0 }
    L_0x02cd:
        r18 = r9.hasNext();	 Catch:{ all -> 0x00f0 }
        if (r18 == 0) goto L_0x035d;
    L_0x02d3:
        r14 = r9.next();	 Catch:{ all -> 0x00f0 }
        r14 = (com.huami.watch.newsport.common.model.SportSummary) r14;	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = r14.getTrackId();	 Catch:{ all -> 0x00f0 }
        r22 = r14.getSportType();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r3 = r22;
        r0.addChildSportSummary(r1, r3);	 Catch:{ all -> 0x00f0 }
        r18 = r14.getSportType();	 Catch:{ all -> 0x00f0 }
        r20 = 1015; // 0x3f7 float:1.422E-42 double:5.015E-321;
        r0 = r18;
        r1 = r20;
        if (r0 != r1) goto L_0x0351;
    L_0x02fc:
        r18 = r14.getDistance();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r0 = (int) r0;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r0 = (float) r0;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r16 = r16 + r18;
    L_0x030c:
        r18 = r14.getTotalPausedTime();	 Catch:{ all -> 0x00f0 }
        r17 = r17 + r18;
        r0 = r14;
        r0 = (com.huami.watch.newsport.common.model.OutdoorSportSummary) r0;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getMaxHeartRate();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r10 = java.lang.Math.max(r10, r0);	 Catch:{ all -> 0x00f0 }
        r0 = r14;
        r0 = (com.huami.watch.newsport.common.model.OutdoorSportSummary) r0;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getMinHeartRate();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r11 = java.lang.Math.min(r11, r0);	 Catch:{ all -> 0x00f0 }
        r20 = r14.getEndTime();	 Catch:{ all -> 0x00f0 }
        r22 = r14.getStartTime();	 Catch:{ all -> 0x00f0 }
        r20 = r20 - r22;
        r18 = r14.getTotalPausedTime();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r0 = (long) r0;	 Catch:{ all -> 0x00f0 }
        r22 = r0;
        r20 = r20 - r22;
        r0 = r20;
        r12 = (int) r0;	 Catch:{ all -> 0x00f0 }
        r8 = r8 + r12;
        r18 = r14.getCalorie();	 Catch:{ all -> 0x00f0 }
        r15 = r15 + r18;
        goto L_0x02cd;
    L_0x0351:
        r18 = r14.getTrimedDistance();	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r0 = (float) r0;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r16 = r16 + r18;
        goto L_0x030c;
    L_0x035d:
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r1 = r16;
        r0.setDistance(r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r0.setCalorie(r15);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r21 = r21.size();	 Catch:{ all -> 0x00f0 }
        r21 = r21 + -1;
        r0 = r18;
        r1 = r21;
        r18 = r0.get(r1);	 Catch:{ all -> 0x00f0 }
        r18 = (com.huami.watch.newsport.common.model.SportSummary) r18;	 Catch:{ all -> 0x00f0 }
        r22 = r18.getEndTime();	 Catch:{ all -> 0x00f0 }
        r0 = r20;
        r1 = r22;
        r0.setEndTime(r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r1 = r17;
        r0.setTotalPausedTime(r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r0.setSportDuration(r8);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r0.setMaxHeartRate(r10);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r0.setMinHeartRate(r11);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r26;
        r0 = r0.mTotalHeartCount;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        if (r18 != 0) goto L_0x0431;
    L_0x03de:
        r18 = -1;
    L_0x03e0:
        r0 = r20;
        r1 = r18;
        r0.setAvgHeartRate(r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getDistance();	 Catch:{ all -> 0x00f0 }
        r20 = 1148846080; // 0x447a0000 float:1000.0 double:5.676053805E-315;
        r18 = r18 / r20;
        r18 = java.lang.String.valueOf(r18);	 Catch:{ all -> 0x00f0 }
        r20 = com.huami.watch.newsport.utils.NumeriConversionUtils.getDoubleValue(r18);	 Catch:{ all -> 0x00f0 }
        r18 = 0;
        r0 = r20;
        r2 = r18;
        r18 = com.huami.watch.common.DataFormatUtils.parseFormattedRealNumber(r0, r2);	 Catch:{ all -> 0x00f0 }
        r7 = com.huami.watch.newsport.utils.NumeriConversionUtils.convertStringToFloat(r18);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTodayDistance;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r18 = com.huami.watch.newsport.utils.NumeriConversionUtils.getAddResult(r0, r7);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r26;
        r1.mTodayDistance = r0;	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalDistance;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r18;
        r18 = com.huami.watch.newsport.utils.NumeriConversionUtils.getAddResult(r0, r7);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r26;
        r1.mTotalDistance = r0;	 Catch:{ all -> 0x00f0 }
        goto L_0x0168;
    L_0x0431:
        r0 = r26;
        r0 = r0.mTotalHeartSum;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mTotalHeartCount;	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r18 = r18 / r21;
        goto L_0x03e0;
    L_0x0440:
        if (r27 == 0) goto L_0x04df;
    L_0x0442:
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r24 = r0;
        r24 = r24.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r22 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$10;	 Catch:{ all -> 0x00f0 }
        r0 = r22;
        r1 = r26;
        r2 = r27;
        r0.<init>(r2);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r3 = r22;
        r0.asyncDeleteSportSummary(r1, r2, r3);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r9 = r18.iterator();	 Catch:{ all -> 0x00f0 }
    L_0x0493:
        r18 = r9.hasNext();	 Catch:{ all -> 0x00f0 }
        if (r18 == 0) goto L_0x056f;
    L_0x0499:
        r13 = r9.next();	 Catch:{ all -> 0x00f0 }
        r13 = (com.huami.watch.newsport.common.model.SportSummary) r13;	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r24 = r13.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r22 = new com.huami.watch.newsport.sportcenter.OutdoorSportDelegate$11;	 Catch:{ all -> 0x00f0 }
        r0 = r22;
        r1 = r26;
        r0.<init>(r13);	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r3 = r22;
        r0.asyncDeleteSportSummary(r1, r2, r3);	 Catch:{ all -> 0x00f0 }
        goto L_0x0493;
    L_0x04df:
        r18 = "OutdoorSportDelegate";
        r20 = "saveNecessaryDataWhenSportStopped, callback is null delete summary";
        r0 = r18;
        r1 = r20;
        com.huami.watch.common.log.Debug.m5i(r0, r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r24 = r0;
        r24 = r24.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.deleteSportSummary(r1, r2);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportSummaryList;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r9 = r18.iterator();	 Catch:{ all -> 0x00f0 }
    L_0x052e:
        r18 = r9.hasNext();	 Catch:{ all -> 0x00f0 }
        if (r18 == 0) goto L_0x056f;
    L_0x0534:
        r13 = r9.next();	 Catch:{ all -> 0x00f0 }
        r13 = (com.huami.watch.newsport.common.model.SportSummary) r13;	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = "track_id=?";
        r21 = 1;
        r0 = r21;
        r0 = new java.lang.String[r0];	 Catch:{ all -> 0x00f0 }
        r21 = r0;
        r22 = 0;
        r23 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f0 }
        r23.<init>();	 Catch:{ all -> 0x00f0 }
        r24 = "";
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r24 = r13.getTrackId();	 Catch:{ all -> 0x00f0 }
        r23 = r23.append(r24);	 Catch:{ all -> 0x00f0 }
        r23 = r23.toString();	 Catch:{ all -> 0x00f0 }
        r21[r22] = r23;	 Catch:{ all -> 0x00f0 }
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.deleteSportSummary(r1, r2);	 Catch:{ all -> 0x00f0 }
        goto L_0x052e;
    L_0x056f:
        monitor-exit(r19);	 Catch:{ all -> 0x00f0 }
        goto L_0x00e1;
    L_0x0572:
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r20 = 2;
        r0 = r18;
        r1 = r20;
        r0.setCurrentStatus(r1);	 Catch:{ all -> 0x00f0 }
        goto L_0x01c5;
    L_0x0583:
        r18 = 0;
        r0 = r26;
        r0 = r0.mCurrentStatus;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r27;
        r1 = r18;
        r2 = r20;
        r0.notifyCallback(r1, r2);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mContext;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r1 = r18;
        r0.keepSportRecordCount(r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mContext;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r18 = r18.getContentResolver();	 Catch:{ all -> 0x00f0 }
        r20 = "content://com.huami.watch.sport.provider/sport_summary_data_info";
        r20 = android.net.Uri.parse(r20);	 Catch:{ all -> 0x00f0 }
        r21 = 0;
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.notifyChange(r1, r2);	 Catch:{ all -> 0x00f0 }
        goto L_0x0280;
    L_0x05be:
        r18 = "OutdoorSportDelegate";
        r20 = "saveNecessaryDataWhenSportStopped, callback is null update summary";
        r0 = r18;
        r1 = r20;
        com.huami.watch.common.log.Debug.m5i(r0, r1);	 Catch:{ all -> 0x00f0 }
        r0 = r26;
        r0 = r0.mSportDBControlCenter;	 Catch:{ all -> 0x00f0 }
        r18 = r0;
        r0 = r26;
        r0 = r0.mTotalSportSummary;	 Catch:{ all -> 0x00f0 }
        r20 = r0;
        r0 = r18;
        r1 = r20;
        r0.updateSportSummary(r1);	 Catch:{ all -> 0x00f0 }
        goto L_0x0294;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.sportcenter.OutdoorSportDelegate.saveNecessaryDataWhenSportStopped(com.huami.watch.common.db.Callback):void");
    }

    private void doFinish(OutdoorSportSnapshot status, List<RunningInfoPerKM> runningInfoPerKMs, Callback callback) {
        synchronized (this.mLock) {
            Debug.m3d("OutdoorSportDelegate", "doFinish|status:" + status + "|running info per km : " + runningInfoPerKMs);
            if (status != null) {
                if (!this.mRecordTrackIdArray.contains(Long.valueOf(this.mOutdoorSummary.getTrackId()))) {
                    checkIfLastAutoLapComplete();
                    checkIfLastSkiingLapComplete();
                    float maxDescend = computeLapMaxAltitudeDescend(this.mPreviousAutoLapsInfo);
                    Global.getGlobalHeartHandler().post(new Runnable() {
                        public void run() {
                            List<RunningInfoPerLap> laps = OutdoorSportDelegate.this.saveLapInfo2db();
                            if (OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId) != null) {
                                OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId).saveLapData(laps);
                            }
                        }
                    });
                    this.mOutdoorSummary = status.initSummary(this.mOutdoorSummary);
                    if (SportType.isChildSport(this.mOutdoorSummary.getSportType())) {
                        this.mOutdoorSummary.setCalorie((float) (Math.round(UnitConvertUtils.convertCalorieToKilocalorie((double) this.mOutdoorSummary.getCalorie())) * 1000));
                    }
                    if (this.mLastPausedTimestamp != -1) {
                        this.mOutdoorSummary.setEndTime((this.mLastPausedTimestamp / 1000) * 1000);
                    } else {
                        this.mOutdoorSummary.setEndTime((getCurrentTimeMillisOnRuntime() / 1000) * 1000);
                    }
                    if (this.mSportOptions.getSportType() == 8) {
                        this.mOutdoorSummary.setCurrentStatus(8);
                    } else {
                        this.mOutdoorSummary.setCurrentStatus(2);
                    }
                    this.mOutdoorSummary.setTotalPausedTime((this.mPausedTime / 1000) * 1000);
                    this.mOutdoorSummary.setSportDuration((int) ((this.mOutdoorSummary.getEndTime() - this.mOutdoorSummary.getStartTime()) - ((long) this.mOutdoorSummary.getTotalPausedTime())));
                    this.mOutdoorSummary.setAvgStride(this.mStepCount == 0 ? -1.0f : this.mStepStride / ((float) this.mStepCount));
                    this.mOutdoorSummary.setAvgHeartRate(this.mSingleHeartCount == 0 ? 0 : this.mSingleHeartSum / this.mSingleHeartCount);
                    this.mOutdoorSummary.setMaxHeartRate(this.mMaxHeartRate != Integer.MIN_VALUE ? this.mMaxHeartRate : -1);
                    this.mOutdoorSummary.setMinHeartRate(this.mMinHeartRate != Integer.MAX_VALUE ? this.mMinHeartRate : -1);
                    this.mOutdoorSummary.setDownhillMaxAltitudeDescend(maxDescend);
                    this.mTotalHeartCount += this.mSingleHeartCount;
                    this.mTotalHeartSum += this.mSingleHeartSum;
                    if (this.mLocationData == null || Float.isNaN(this.mLocationData.mLatitude) || Float.isNaN(this.mLocationData.mLongitude) || Float.isInfinite(this.mLocationData.mLatitude) || Float.isInfinite(this.mLocationData.mLongitude)) {
                        LogUtil.m9i(true, "OutdoorSportDelegate", "location data is null, cannot init location data in summary");
                    } else {
                        this.mOutdoorSummary.setLatitude((double) this.mLocationData.mLatitude);
                        this.mOutdoorSummary.setLongitude((double) this.mLocationData.mLongitude);
                    }
                    if (!SportType.isSportTypeNeedDis(this.mSportOptions.getSportType())) {
                        this.mOutdoorSummary.setDistance(0.0f);
                    } else if (this.mSportOptions.getSportType() == 11) {
                        this.mOutdoorSummary.setDistance(this.mOutdoorSummary.getClimbdisDescend());
                    }
                    long totalTime = (long) this.mOutdoorSummary.getSportDuration();
                    float dis = this.mOutdoorSummary.getDistance();
                    if (this.mSportOptions.getSportType() != 11) {
                        float f;
                        float totalPace = dis == 0.0f ? 0.0f : ((float) (totalTime / 1000)) / dis;
                        this.mOutdoorSummary.setPace(totalPace);
                        this.mOutdoorSummary.setTotalPace(totalPace);
                        this.mOutdoorSummary.setSpeed(totalPace == 0.0f ? 0.0f : 1.0f / totalPace);
                        this.mOutdoorSummary.setTotalSpeed(totalPace == 0.0f ? 0.0f : 1.0f / totalPace);
                        OutdoorSportSummary outdoorSportSummary = this.mOutdoorSummary;
                        if (this.mOutdoorSummary.getBestPace() <= 0.0f) {
                            f = 0.0f;
                        } else {
                            f = 1.0f / this.mOutdoorSummary.getBestPace();
                        }
                        outdoorSportSummary.setMaxSpeed(f);
                    }
                    if (SportType.isMixedSport(this.mSportOptions.getSportType())) {
                        this.mOutdoorSummary.setParentTrackId(this.mTrackId);
                    }
                    if (this.mOutdoorSummary.getMaxStepFreq() < this.mOutdoorSummary.getStepFreq()) {
                        Debug.m5i("OutdoorSportDelegate", "***********err step freq*************");
                        this.mOutdoorSummary.setMaxStepFreq(this.mOutdoorSummary.getStepFreq() + 2.0f);
                    }
                    Debug.m3d("OutdoorSportDelegate", "sport summary : " + this.mOutdoorSummary);
                    if (!UnitConvertUtils.isHuangheMode()) {
                        this.mOutdoorSummary.setTE(status.getmSportEteTrainingEffect());
                    }
                    if (SportType.isMixedSport(this.mSportOptions.getSportType()) && SportType.isValidWhenEndSport(this.mCurrentSportType, status)) {
                        this.mIsAvailableMixedSport = true;
                    }
                    if (!SportType.isValidWhenEndSport(this.mCurrentSportType, status) || this.mIsGiveupRecord) {
                        Debug.m3d("OutdoorSportDelegate", "distance is less than minimum distance. current distance " + this.mOutdoorSummary.getDistance());
                        SportDatabaseControlCenter sportDatabaseControlCenter;
                        String[] strArr;
                        if (callback != null) {
                            sportDatabaseControlCenter = this.mSportDBControlCenter;
                            strArr = new String[1];
                            strArr[0] = "" + this.mOutdoorSummary.getTrackId();
                            sportDatabaseControlCenter.asyncDeleteSportSummary("track_id=?", strArr, new Callback() {
                                protected void doCallback(int resultCode, Object params) {
                                    LogUtil.m9i(true, "OutdoorSportDelegate", "delete_SportSummary:" + resultCode + ", params:" + params);
                                }
                            });
                            callback.notifyCallback(0, null);
                        } else {
                            sportDatabaseControlCenter = this.mSportDBControlCenter;
                            strArr = new String[1];
                            strArr[0] = "" + this.mOutdoorSummary.getTrackId();
                            sportDatabaseControlCenter.deleteSportSummary("track_id=?", strArr);
                        }
                    } else {
                        Debug.m3d("OutdoorSportDelegate", "running info per kilometer " + this.mRunningInfoPerKMs);
                        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(this.mContext);
                        BaseConfig sportBaseConfig = DataManager.getInstance().getSportConfig(this.mContext, this.mCurrentSportType);
                        Log.i("OutdoorSportDelegate", " currnetSportType:" + this.mSportOptions.getSportType(), new Object[0]);
                        if (SportType.isSuportHasHeartRataSports(this.mCurrentSportType) || ((this.mSportOptions.getSportType() == 7 || this.mSportOptions.getSportType() == 13) && sportBaseConfig.isMeasureHeart())) {
                            Log.i("OutdoorSportDelegate", " updateRecoveryTime ", new Object[0]);
                            config.setRecoveryTime(status.getmSportEteResourceRecovery());
                            config.setSportFinishTime(System.currentTimeMillis());
                        }
                        if (SportType.isSupportFirstBeatData(this.mSportOptions.getSportType()) || this.mCurrentSportType == 1001) {
                            Vo2maxDayInfo vo2maxDayInfo = new Vo2maxDayInfo();
                            vo2maxDayInfo.setDayId(FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis()));
                            vo2maxDayInfo.setUpdateTime(System.currentTimeMillis());
                            Log.i("OutdoorSportDelegate", " save First Beat config:" + config.toString(), new Object[0]);
                            if (getCurSportType() == 1 || this.mCurrentSportType == 1001) {
                                config.setLastRunVo2max(config.getCurrnetRunVo2max());
                                config.setCurrnetRunVo2max(status.getmSportEteVo2Max());
                                vo2maxDayInfo.setVo2maxRun(status.getmSportEteVo2Max());
                            } else if (getCurSportType() == 6) {
                                config.setLastWalkingVo2max(config.getCurrnetWalkingVo2max());
                                config.setCurrnetWalkingVo2max(status.getmSportEteVo2Max());
                                vo2maxDayInfo.setVo2MaxWalking(status.getmSportEteVo2Max());
                            }
                            final Vo2maxDayInfo vo2maxDayInfo2 = vo2maxDayInfo;
                            Global.getGlobalHeartHandler().post(new Runnable() {
                                public void run() {
                                    OutdoorSportDelegate.this.mSportDBControlCenter.addVo2MaxDayInfo(vo2maxDayInfo2);
                                }
                            });
                        }
                        DataManager.getInstance().setFirstBeatConfig(this.mContext, config);
                        Global.getGlobalHeartHandler().post(new Runnable() {
                            public void run() {
                                OutdoorSportDelegate.this.saveRunningInfoPerKm2db();
                                if (OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId) != null) {
                                    OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId).savePerKmOrMiData(OutdoorSportDelegate.this.mRunningInfoPerKMs, true);
                                }
                            }
                        });
                        if (1 == getCurSportType()) {
                            boolean isNotFullKmSave = true;
                            double finalDis = UnitConvertUtils.convertDistanceToMileOrKm((double) (this.mOutdoorSummary.getDistance() / 1000.0f), !this.mIsImperial);
                            if (this.mDailyPerformanceInfoList != null && this.mDailyPerformanceInfoList.size() > 1) {
                                isNotFullKmSave = finalDis != ((DailyPerformanceInfo) this.mDailyPerformanceInfoList.get(this.mDailyPerformanceInfoList.size() + -1)).getCurrnetKilos();
                            }
                            if (isNotFullKmSave && (1000.0d * finalDis) % 1000.0d >= 10.0d) {
                                DailyPerformanceInfo dailyPerformanceInfo = new DailyPerformanceInfo();
                                dailyPerformanceInfo.setTeValue(status.getmSportEteTrainingEffect());
                                dailyPerformanceInfo.setDailyPorpermence((float) status.getmETEdailyPerformance());
                                dailyPerformanceInfo.setmTrackId(this.mCurrentSportTrackId);
                                dailyPerformanceInfo.setSaveDataType(1);
                                dailyPerformanceInfo.setCreateTime(System.currentTimeMillis());
                                dailyPerformanceInfo.setCurrnetKilos((float) finalDis);
                                this.mDailyPerformanceInfoList.add(dailyPerformanceInfo);
                            }
                            if (finalDis >= 1.0d) {
                                Global.getGlobalHeartHandler().post(new Runnable() {
                                    public void run() {
                                        OutdoorSportDelegate.this.saveDailyPerformance2db();
                                        if (OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId) != null) {
                                            OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId).saveDailyPerformanceData(OutdoorSportDelegate.this.mDailyPerformanceInfoList, true);
                                        }
                                    }
                                });
                            }
                        }
                        if (SportType.isSuportHasHeartRataSports(this.mSportOptions.getSportType()) || ((this.mSportOptions.getSportType() == 7 || this.mSportOptions.getSportType() == 13) && sportBaseConfig.isMeasureHeart())) {
                            Log.i("OutdoorSportDelegate", " updateSportThaInfo  ", new Object[0]);
                            SportThaInfo sportThaInfo = status.getSportThaInfo();
                            if (sportThaInfo != null) {
                                sportThaInfo.setDayId(FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis()));
                                sportThaInfo.setUpdateTime(System.currentTimeMillis());
                                sportThaInfo.setDateStr(FirstBeatDataUtils.getDayTimeFormat(System.currentTimeMillis()));
                                if (getCurSportType() == 1) {
                                    sportThaInfo.setSportVo2Max(status.getmSportEteVo2Max());
                                    sportThaInfo.setRideVo2max(-1.0f);
                                } else if (getCurSportType() == 9) {
                                    sportThaInfo.setRideVo2max(status.getmSportEteVo2Max());
                                    sportThaInfo.setSportVo2Max(-1.0f);
                                }
                                Log.i("OutdoorSportDelegate", " insert sport tha info:" + sportThaInfo.toString(), new Object[0]);
                                final SportThaInfo sportThaInfo2 = sportThaInfo;
                                Global.getGlobalHeartHandler().post(new Runnable() {
                                    public void run() {
                                        OutdoorSportDelegate.this.mSportDBControlCenter.insertOrUpdateThaInfo(sportThaInfo2);
                                    }
                                });
                            }
                        }
                        this.mSportSummaryList.add(this.mOutdoorSummary.copy());
                        if (callback == null) {
                            Debug.m5i("OutdoorSportDelegate", "doFinish, callback is null");
                            if (SportType.isMixedSport(this.mSportOptions.getSportType())) {
                                this.mSportDBControlCenter.updateSportSummary(this.mOutdoorSummary);
                            }
                        } else if (SportType.isMixedSport(this.mSportOptions.getSportType())) {
                            final Callback callback2 = callback;
                            this.mSportDBControlCenter.asyncUpdateSportSummary(this.mOutdoorSummary, new Callback() {
                                protected void doCallback(int code, Object params) {
                                    if (code != 0) {
                                        Debug.m6w("OutdoorSportDelegate", "error occur while call sensorhub continue sport. error code : " + code + ". params : " + params);
                                        if (callback2 != null) {
                                            callback2.notifyCallback(-1, null);
                                            return;
                                        }
                                        return;
                                    }
                                    callback2.notifyCallback(0, null);
                                }
                            });
                        } else {
                            callback.notifyCallback(0, null);
                        }
                    }
                    this.mRecordTrackIdArray.add(Long.valueOf(this.mOutdoorSummary.getTrackId()));
                    return;
                }
            }
            Debug.m6w("OutdoorSportDelegate", "status is null while doFinish :" + this.mRecordTrackIdArray.contains(Long.valueOf(this.mOutdoorSummary.getTrackId())));
        }
    }

    private void saveDailyPerformance2db() {
        synchronized (this.mLock) {
            if ((this.mSaveFlags & 4) != 0) {
                return;
            }
            this.mSportDBControlCenter.addDailyPerpormanceInfoList(this.mDailyPerformanceInfoList);
            this.mSaveFlags |= 4;
        }
    }

    private void saveRunningInfoPerKm2db() {
        synchronized (this.mLock) {
            if ((this.mSaveFlags & 1) != 0) {
                return;
            }
            this.mSportDBControlCenter.addKmInfs(this.mRunningInfoPerKMs);
            this.mSaveFlags |= 1;
        }
    }

    private List<RunningInfoPerLap> saveLapInfo2db() {
        List<RunningInfoPerLap> laps = new ArrayList();
        synchronized (this.mLock) {
            if ((this.mSaveFlags & 2) != 0) {
            } else {
                if (!(this.mPreviousAutoLapsInfo == null || this.mPreviousAutoLapsInfo.isEmpty())) {
                    computeLapMaxPace(this.mPreviousAutoLapsInfo);
                    laps.addAll(this.mPreviousAutoLapsInfo);
                }
                if (this.mPreviousManualLapsInfo != null && this.mPreviousManualLapsInfo.size() > 0) {
                    computeLapMaxPace(this.mPreviousManualLapsInfo);
                    laps.addAll(this.mPreviousManualLapsInfo);
                }
                this.mPreviousManualLapsInfo.clear();
                this.mPreviousAutoLapsInfo.clear();
                if (this.mTrainManager != null) {
                    List<RunningInfoPerLap> runningInfoPerLaps = this.mTrainManager.getTrainLapInfos();
                    computeLapMaxPace(runningInfoPerLaps);
                    laps.addAll(runningInfoPerLaps);
                }
                this.mSportDBControlCenter.addLaps(laps);
                if (this.mAllHeartRateList != null) {
                    this.mAllHeartRateList.clear();
                }
                this.mSaveFlags |= 2;
            }
        }
        return laps;
    }

    private float computeLapMaxAltitudeDescend(List<RunningInfoPerLap> lapList) {
        if (lapList == null || lapList.isEmpty()) {
            return -1.0f;
        }
        float maxDescend = -1.0f;
        for (RunningInfoPerLap lap : lapList) {
            if (lap.getLapType() == 3 && maxDescend < lap.getClimbDown()) {
                maxDescend = lap.getClimbDown();
            }
        }
        return maxDescend;
    }

    private void computeLapMaxPace(List<RunningInfoPerLap> lapList) {
        long lastAutoLapTime = 0;
        for (RunningInfoPerLap autoLap : lapList) {
            autoLap.setMaxPace((double) getLapMaxPace(this.mCurrentSportTrackId + lastAutoLapTime, autoLap.getTotalCostTime() + this.mCurrentSportTrackId));
            lastAutoLapTime = autoLap.getTotalCostTime();
        }
        addAveCadencePerLap(lapList);
    }

    private boolean isLastAutoPauseIsAvailable() {
        float preDis = 0.0f;
        for (RunningInfoPerLap lap : this.mPreviousAutoLapsInfo) {
            preDis += lap.getDistance();
        }
        Debug.m5i("OutdoorSportDelegate", "isLastAutoPauseIsAvailable, preDis: " + preDis + ", now dis:" + this.mCurrentStatus.getDistance());
        float remainDis = this.mCurrentStatus.getDistance() - preDis;
        if ((!SportType.isSwimMode(this.mCurrentSportType) || remainDis < 1.0f) && remainDis < ((float) Global.MINIMUM_DISTANCE_METER)) {
            return false;
        }
        return true;
    }

    private void checkIfLastAutoLapComplete() {
        Debug.m5i("OutdoorSportDelegate", "checkIfLastAutoLapComplete, " + this.mCurrentStatus.toString());
        if (!this.mSportConfig.isOpenAutoLapRecord() || this.mIsIntervalTrainMode || !SportType.isNeedAutoLap(this.mSportOptions.getSportType())) {
            Debug.m5i("OutdoorSportDelegate", "checkIfLastAutoLapComplete, not open lap record, intervalmode:" + this.mIsIntervalTrainMode);
        } else if (isLastAutoPauseIsAvailable()) {
            saveLastAutoLapInfo();
        }
    }

    private void checkIfLastSkiingLapComplete() {
        Debug.m5i("OutdoorSportDelegate", "checkIfLastAutoLapComplete, " + this.mCurrentStatus.toString() + ", lap size:" + this.mPreviousAutoLapsInfo.size());
        if (this.mCurrentStatus.getDownHillNum() > this.mPreviousAutoLapsInfo.size() && this.mSportOptions.getSportType() == 11) {
            saveLastAutoLapInfo();
        }
    }

    private void saveLastAutoLapInfo() {
        long totalPreTime = 0;
        int totalPreDis = 0;
        int totalPreCalorie = 0;
        float totalPreClimbDis = 0.0f;
        float totalPreClimbUp = 0.0f;
        float totalPreClimbDown = 0.0f;
        int totalPreStepCount = 0;
        int totalPreLapStrokes = 0;
        long totalRunTime = 0;
        int lastNum = -1;
        for (RunningInfoPerLap lap : this.mPreviousAutoLapsInfo) {
            totalPreDis = (int) (((float) totalPreDis) + lap.getDistance());
            totalPreCalorie += lap.getLapCalories();
            totalPreClimbDis += lap.getClimbDistance() >= 0.0f ? lap.getClimbDistance() : 0.0f;
            totalPreClimbUp += lap.getClimbUp() >= 0.0f ? lap.getClimbUp() : 0.0f;
            totalPreClimbDown += lap.getClimbDown() >= 0.0f ? lap.getClimbDown() : 0.0f;
            totalPreStepCount = (int) (((float) totalPreStepCount) + (lap.getLapStepFreq() * ((float) (lap.getCostTime() / 1000))));
            totalPreLapStrokes += lap.getLapStrokes();
            totalRunTime += lap.getCostTime();
        }
        if (!this.mPreviousAutoLapsInfo.isEmpty()) {
            totalPreTime = ((RunningInfoPerLap) this.mPreviousAutoLapsInfo.get(this.mPreviousAutoLapsInfo.size() - 1)).getTotalCostTime();
            lastNum = ((RunningInfoPerLap) this.mPreviousAutoLapsInfo.get(this.mPreviousAutoLapsInfo.size() - 1)).getLapNum();
        }
        if (this.mSportConfig.getSportType() != 11) {
            float distance = this.mSportConfig.getDistanceAutoLap();
            if (this.mSportConfig.getUnit() != 0) {
                distance = (float) Math.round(UnitConvertUtils.convertYDOrMeter2Meter((double) distance, this.mSportConfig.getUnit()));
            } else if (SportType.isSwimMode(this.mCurrentSportType)) {
                distance = (float) UnitConvertUtils.convertDistanceToMeter((double) distance, this.mCurrentSportType);
            } else {
                distance = ((float) UnitConvertUtils.convertDistanceFromMileOrKmtokm((double) (distance / 1000.0f))) * 1000.0f;
            }
            totalPreDis = (int) (((float) (lastNum + 1)) * distance);
        }
        RunningInfoPerLap lap2 = new RunningInfoPerLap();
        lap2.setTrackId(this.mCurrentSportTrackId);
        if (this.mSportOptions.getSportType() == 11) {
            lap2.setLapType(3);
        } else {
            lap2.setLapType(1);
        }
        lap2.setLapNum(lastNum + 1);
        if (this.mSportConfig.getSportType() == 11) {
            lap2.setDistance((float) ((int) (this.mCurrentStatus.getClimbdisDescend() - ((float) totalPreDis))));
        } else {
            int dis = (int) (this.mCurrentStatus.getDistance() - ((float) totalPreDis));
            if (!SportType.isSwimMode(this.mSportOptions.getSportType())) {
                dis = (dis / 10) * 10;
            }
            lap2.setDistance((float) dis);
        }
        Long[] lngLat = this.mCurrentStatus.getCurGPSLonLatPoint();
        if (lngLat != null && lngLat.length >= 3) {
            float lng = ((float) lngLat[1].longValue()) / 3000000.0f;
            float lat = ((float) lngLat[2].longValue()) / 3000000.0f;
            Debug.m5i("OutdoorSportDelegate", "checkIfSwimAutoLapComplete, lng:" + lng + ", lat:" + lat);
            lap2.setLongitude(lng);
            lap2.setLatitude(lat);
        }
        long lapCostTime = (getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId) - totalPreTime;
        long pauseTime = 0;
        List<PauseInfo> pauseInfos = this.mOutdoorSummary.getPauseInfos();
        if (pauseInfos != null) {
            for (int i = pauseInfos.size() - 1; i >= 0; i--) {
                PauseInfo info = (PauseInfo) pauseInfos.get(i);
                long startTime = info.getStart();
                long endTime = info.getEnd();
                if (this.mCurrentSportTrackId + totalPreTime >= endTime) {
                    break;
                }
                if (startTime > this.mCurrentSportTrackId + totalPreTime && endTime < getCurrentTimeMillisOnRuntime()) {
                    pauseTime += endTime - startTime;
                }
            }
        }
        pauseTime += (long) (this.mCurrentStatus.getCurrentPausedSecond() * 1000);
        Debug.m5i("OutdoorSportDelegate", "pretime:" + totalPreTime + ", current:" + (getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId) + "pauseTime:" + pauseTime);
        if (lapCostTime >= pauseTime) {
            lapCostTime -= pauseTime;
        } else {
            Debug.m5i("OutdoorSportDelegate", "checkIfSwimAutoLapComplete, lapCostTime:" + lapCostTime + ", pausedTime:" + pauseTime);
        }
        lap2.setCostTime(lapCostTime);
        long totalCostTime = (getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId) - ((long) (this.mCurrentStatus.getCurrentPausedSecond() * 1000));
        if (totalCostTime < totalRunTime + lapCostTime) {
            totalCostTime = totalRunTime + lapCostTime;
        }
        lap2.setTotalCostTime(totalCostTime);
        if (this.mSportOptions.getSportType() == 11) {
            lap2.setPace((double) this.mCurrentStatus.getRtCurLapAvgPace());
            lap2.setMaxiMumFall(this.mCurrentStatus.getRtCurLapMaxDownhillElveLoss());
        } else {
            lap2.setPace(Float.compare(0.0f, lap2.getDistance()) == 0 ? 0.0d : (double) ((((float) lap2.getCostTime()) / 1000.0f) / lap2.getDistance()));
        }
        lap2.setLapCalories((int) (this.mCurrentStatus.getCalorie() - ((float) totalPreCalorie)));
        lap2.setClimbDistance(this.mCurrentStatus.getClimbdisascend() - totalPreClimbDis);
        lap2.setClimbUp(this.mCurrentStatus.getClimbUp() - totalPreClimbUp);
        lap2.setClimbDown(this.mCurrentStatus.getClimbDown() - totalPreClimbDown);
        if (lap2.getCostTime() != 0) {
            lap2.setLapStepFreq((float) ((int) (((long) (this.mCurrentStatus.getStepCount() - totalPreStepCount)) / (lap2.getCostTime() / 1000))));
        }
        if (SportType.isSwimMode(this.mSportOptions.getSportType())) {
            lap2.setLapStrokes(this.mCurrentStatus.getTotalStrokes() - totalPreLapStrokes);
            int costTime = (int) (lap2.getCostTime() / 1000);
            lap2.setLapStrokeSpeed(costTime == 0 ? 0.0f : ((float) lap2.getLapStrokes()) / ((float) costTime));
            if (Float.compare(0.0f, lap2.getDistance()) != 0) {
                int swimLength = (int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter((double) this.mSportConfig.getTargetSwimLength(), this.mSportConfig.getUnit()));
                if (this.mSportConfig.getSportType() == 15) {
                    swimLength = (int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter(100.0d, this.mSportConfig.getUnit()));
                }
                lap2.setLapSwolf((int) (((float) ((lap2.getLapStrokes() + costTime) * swimLength)) / lap2.getDistance()));
            }
        }
        this.mPreviousAutoLapsInfo.add(lap2);
    }

    public boolean childSportStop(final Callback callback, boolean forcely) {
        if (this.mSportTickerManager != null) {
            this.mSportTickerManager.stopTicker();
            this.mSportTickerManager.setSportTickerListener(null);
        }
        Debug.m3d("OutdoorSportDelegate", "childSportStop, update final data request");
        Debug.m3d("OutdoorSportDelegate", "user stop sport begin to start, currentTime: " + DataFormatUtils.parseMilliSecondToFormattedTime(getCurrentTimeMillisOnRuntime(), "yy-MM-dd HH:mm:ss", true) + ", current state is: " + this.mCurrentState.state());
        if (this.mCurrentState == this.mStopPendingState || this.mCurrentState == this.mStopState) {
            Debug.m5i("OutdoorSportDelegate", "sport is stopping or stopped, refuse this request:" + this.mCurrentState);
        } else {
            this.mStopTime = getCurrentTimeMillisOnRuntime();
            this.mStopTime -= this.mStopTime % 1000;
            this.mTickerStartTime = -1;
            setState(this.mStopPendingState);
            this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, new Callback() {
                protected void doCallback(int i, Object o) {
                    Debug.m3d("OutdoorSportDelegate", "-------Step1 sensorhub stop sport start-------");
                    if (OutdoorSportDelegate.this.mCurrentStatus == null) {
                        Debug.m5i("OutdoorSportDelegate", "current status is null, err");
                        new Bundle().putInt("sport_mode", OutdoorSportDelegate.this.mSportOptions.getIntervalType());
                        OutdoorSportDelegate.this.mSensorHubManagerWrapper.stopSport(OutdoorSportDelegate.this.mCurrentSportType, OutdoorSportDelegate.this.mStopTime, new Callback(OutdoorSportDelegate.this.mAlgoThreadHandler) {
                            protected void doCallback(int code, Object params) {
                                Debug.m6w("OutdoorSportDelegate", "stop sport success : " + code + ". params : " + params);
                                if (code != 0) {
                                    if (callback != null) {
                                        callback.notifyCallback(-1, null);
                                    }
                                } else if (callback != null) {
                                    callback.notifyCallback(0, null);
                                }
                            }
                        });
                        return;
                    }
                    if (!SportType.isValidWhenEndSport(OutdoorSportDelegate.this.mCurrentSportType, OutdoorSportDelegate.this.mCurrentStatus) || SportType.isMixedSport(OutdoorSportDelegate.this.mSportOptions.getSportType()) || OutdoorSportDelegate.this.mIsGiveupRecord || !SportType.isSportTypeNeedDis(OutdoorSportDelegate.this.mSportOptions.getSportType())) {
                        Debug.m5i("OutdoorSportDelegate", "the dis is:" + OutdoorSportDelegate.this.mCurrentStatus.getDistance());
                    } else {
                        OutdoorSportDelegate.this.mTodayDistance = NumeriConversionUtils.getAddResult(OutdoorSportDelegate.this.mTodayDistance, NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber((double) (OutdoorSportDelegate.this.mCurrentStatus.getDistance() / 1000.0f), false)));
                        OutdoorSportDelegate.access$3916(OutdoorSportDelegate.this, (float) OutdoorSportDelegate.this.mCurrentStatus.getTrimedDistance());
                        Debug.m5i("OutdoorSportDelegate", "valid sport, today dis:" + OutdoorSportDelegate.this.mTodayDistance + ", total dis:" + OutdoorSportDelegate.this.mTotalDistance);
                    }
                    if (!DateUtils.isSameDate(OutdoorSportDelegate.this.mCurrentSportTrackId)) {
                        Debug.m5i("OutdoorSportDelegate", "the sport time beyond a whole day");
                        OutdoorSportDelegate.this.mTodayDistance = 0.0f;
                    }
                    Bundle bundle = new Bundle();
                    if (!(SportType.isMixedSport(OutdoorSportDelegate.this.mSportOptions.getSportType()) || OutdoorSportDelegate.this.mIsGiveupRecord)) {
                        bundle.putFloat("today_distance", OutdoorSportDelegate.this.mTodayDistance);
                        bundle.putFloat("total_distance", Float.parseFloat(DataFormatUtils.parseTotalDistanceFormattedRealNumber((double) (OutdoorSportDelegate.this.mTotalDistance / 1000.0f), false)));
                        Debug.m5i("OutdoorSportDelegate", "slpt stop intent today dis:" + OutdoorSportDelegate.this.mTodayDistance + ", total dis:" + OutdoorSportDelegate.this.mTotalDistance);
                    }
                    bundle.putInt("sport_mode", OutdoorSportDelegate.this.mSportOptions.getIntervalType());
                    OutdoorSportDelegate.this.mSensorHubManagerWrapper.stopSport(OutdoorSportDelegate.this.mCurrentSportType, OutdoorSportDelegate.this.mStopTime, bundle, new Callback(OutdoorSportDelegate.this.mAlgoThreadHandler) {

                        class C06231 extends Callback {
                            C06231() {
                            }

                            protected void doCallback(int i, Object o) {
                                OutdoorSportDelegate.this.doNecessaryWorkAfterStopSport(callback);
                            }
                        }

                        protected void doCallback(int code, Object params) {
                            Debug.m3d("OutdoorSportDelegate", "-------Step1 sensorhub stop sport stop-------");
                            if (code != 0) {
                                Debug.m6w("OutdoorSportDelegate", "error occur while call sensorhub stop sport. error code : " + code + ". params : " + params);
                                if (callback != null) {
                                    callback.notifyCallback(-1, null);
                                }
                            } else if (OutdoorSportDelegate.this.mSportOptions.getSportType() == 14 || OutdoorSportDelegate.this.mSportOptions.getSportType() == 11) {
                                OutdoorSportDelegate.this.mSensorHubManagerWrapper.updateOutdoorSportStatus(OutdoorSportDelegate.this.mCurrentStatus, new C06231());
                            } else {
                                OutdoorSportDelegate.this.doNecessaryWorkAfterStopSport(callback);
                            }
                        }
                    });
                }
            });
        }
        return true;
    }

    private void doNecessaryWorkAfterStopSport(final Callback callback) {
        unregisterWakeupManagers();
        if (this.mTimeCostWakeupManager != null) {
            this.mTimeCostWakeupManager.setIsSportStarted(false);
        }
        final List<HeartRate> heartRateList = this.mHeartRateManager.stopListen();
        onSportHeartReady(heartRateList);
        Global.getGlobalHeartHandler().post(new Runnable() {
            public void run() {
                Debug.m3d("OutdoorSportDelegate", "-------Step2 stop listen gps heartrate start-------");
                if (SportType.isSportTypeNeedGps(OutdoorSportDelegate.this.mCurrentSportType)) {
                    OutdoorSportDelegate.this.onSportLocationReady(OutdoorSportDelegate.this.mLocationManager.stopListen());
                }
                if (OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId) != null) {
                    OutdoorSportDelegate.this.getUploadSaver(OutdoorSportDelegate.this.mCurrentSportTrackId).saveHistoryHeartData(heartRateList, true);
                }
                OutdoorSportDelegate.this.mLocationManager.setHistoryDataListener(null);
                OutdoorSportDelegate.this.mHeartRateManager.setHistoryDataListener(null);
                Debug.m3d("OutdoorSportDelegate", "-------Step2 stop listen gps heartrate stop-------");
            }
        });
        this.mStopStep = (byte) (this.mStopStep | 2);
        notifyCallbackStopSportIfNotStop(callback, this.mCurrentSportTrackId);
        this.mCurrentStatus.setCurrentStatus((byte) 3);
        Debug.m3d("OutdoorSportDelegate", "-------Step3 save data to databse start-------");
        doFinish(this.mCurrentStatus, this.mRunningInfoPerKMs, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                Debug.m3d("OutdoorSportDelegate", "-------Step3 save data to databse stop-------");
                Debug.m3d("OutdoorSportDelegate", "user stop sport stop complete, currentTime: " + DataFormatUtils.parseMilliSecondToFormattedTime(OutdoorSportDelegate.this.getCurrentTimeMillisOnRuntime(), "yy-MM-dd HH:mm:ss", true) + ", current state is: " + OutdoorSportDelegate.this.mCurrentState.state());
                OutdoorSportDelegate.this.mSensorHubManagerWrapper.clearup();
                OutdoorSportDelegate.access$4176(OutdoorSportDelegate.this, 1);
                OutdoorSportDelegate.this.notifyCallbackStopSportIfNotStop(callback, OutdoorSportDelegate.this.mCurrentSportTrackId);
            }
        });
    }

    private void notifyCallbackStopSportIfNotStop(Callback callback, final long trackId) {
        synchronized (this.mLock) {
            if (!(this.mCurrentState == this.mStopState || (this.mStopStep & 2) == 0 || (this.mStopStep & 1) == 0)) {
                Debug.m5i("OutdoorSportDelegate", "notifyCallbackStopSportIfNotStop");
                setState(this.mStopState);
                final OutdoorSportSummary sportSummary = this.mOutdoorSummary;
                Global.getGlobalHeartHandler().post(new Runnable() {
                    public void run() {
                        OutdoorSportDelegate.this.mLocationPool.commit(trackId);
                        if (OutdoorSportDelegate.this.mCurrentStatus != null) {
                            float offset = OutdoorSportDelegate.this.mCurrentStatus.getOffsetAltitude();
                            OutdoorSportDelegate.this.mSportDBControlCenter.updateLocationAltitude(trackId, offset);
                            if (OutdoorSportDelegate.this.getUploadSaver(trackId) != null) {
                                OutdoorSportDelegate.this.getUploadSaver(trackId).updateAltitudeOffset(offset);
                            }
                        }
                        if (OutdoorSportDelegate.this.getUploadSaver(trackId) != null) {
                            OutdoorSportDelegate.this.getUploadSaver(trackId).savePauseInfoData(sportSummary.getPauseInfos(), true);
                            OutdoorSportDelegate.this.getUploadSaver(trackId).saveSportDataInRun(sportSummary);
                        }
                        LogUtil.m9i(true, "OutdoorSportDelegate", "location complete saved");
                    }
                });
                this.mAvaiableHeartRateList.clear();
                if (callback != null) {
                    callback.notifyCallback(0, this.mCurrentStatus);
                }
            }
        }
    }

    private long getTotalRunningTime(boolean containsCurSport) {
        long runtime = 0;
        for (SportSummary summary : this.mSportSummaryList) {
            runtime += ((long) summary.getSportDuration()) / 1000;
        }
        if (containsCurSport) {
            return runtime + ((long) this.mCurrentStatus.getCurrentSecond());
        }
        return runtime;
    }

    private void onStateErr() {
        if (this.mListener != null) {
            this.mListener.onStateUpdateErr();
        }
    }

    private void updateTodayAndTotalDistance() {
        if (Float.compare(this.mTotalDistance, 0.0f) == 0 && Float.compare(this.mTodayDistance, 0.0f) == 0) {
            long startTime = DateUtils.getDayStartTime(this.mTrackId);
            long endTime = DateUtils.getDayendTime(this.mTrackId);
            this.mSportDBControlCenter.asyncgetAllSportSummary("track_id BETWEEN ? and ?", new String[]{"" + startTime, "" + endTime}, null, null, new Callback() {
                protected void doCallback(int resultCode, Object params) {
                    List<SportSummary> sportSummaries = (List) params;
                    if (!(sportSummaries == null || sportSummaries.isEmpty())) {
                        for (SportSummary sportSummary : sportSummaries) {
                            if (!(sportSummary.getCurrentStatus() == 7 || sportSummary.getCurrentStatus() == 1 || sportSummary.getCurrentStatus() == 0 || SportType.isChildSport(sportSummary.getSportType()))) {
                                OutdoorSportDelegate.this.mTodayDistance = NumeriConversionUtils.getAddResult(OutdoorSportDelegate.this.mTodayDistance, NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber((double) (sportSummary.getDistance() / 1000.0f), false)));
                            }
                        }
                    }
                    Debug.m3d("OutdoorSportDelegate", "today distance:" + OutdoorSportDelegate.this.mTodayDistance);
                }
            });
            SportStatistic sportStatistics = DataManager.getInstance().getSportStatistic(this.mContext);
            if (sportStatistics != null) {
                this.mTotalDistance += (float) sportStatistics.getTotalDistance();
            }
            Debug.m5i("OutdoorSportDelegate", "total dis:" + this.mTotalDistance);
        }
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    private void configAlgorithmConfig(boolean enableRepeatDis, float repeatDis, boolean enableAutoPause, float autoSpeed, boolean enablePace, float pace, boolean enableTotalDis, float totalDis, boolean enableHeart, float configHeart, boolean enableCalorie, float calorie, boolean enableHeartRegionRemind, int minHeartRemind, int maxHeartRemind, boolean isRemindTe, boolean isRemindRealTimeGuide, float teValue, boolean isRemindTargetTimeCost, long targetTimeCost, int tennicClapHand, int remindkm) {
        Debug.m5i("OutdoorSportDelegate", "enableRepeatDis:" + enableRepeatDis + ", repeatDis:" + repeatDis + "enableAutoPause:" + enableAutoPause + "enablePace:" + enablePace + ", pace:" + pace + "enableTotalDis:" + enableTotalDis + ", totalDis:" + totalDis + "enableHeart:" + enableHeart + ", configHeart:" + configHeart + "enableCalorie:" + enableCalorie + ", calorie:" + calorie + "enableHeartRegionRemind:" + enableHeartRegionRemind + ", minHeartRemind:" + minHeartRemind + "maxHeartRemind:" + maxHeartRemind + ", autoSpeed:" + autoSpeed + ", remindkm:" + remindkm);
        if (this.mSensorHubManagerWrapper == null) {
            Debug.m5i("OutdoorSportDelegate", "sensor wrapper is null");
            return;
        }
        int teValueInt;
        int safeHeart = UserInfoManager.getDefaultSafeHeart(this.mContext);
        SportConfig sportConfig = new SportConfig();
        if (this.mSportConfig.getUnit() != 0) {
            sportConfig.setMSportLapMeter((float) ((int) UnitConvertUtils.convertYDOrMeter2Meter((double) repeatDis, this.mSportConfig.getUnit())));
        } else if (SportType.isSwimMode(this.mCurrentSportType)) {
            sportConfig.setMSportLapMeter((float) ((int) UnitConvertUtils.convertDistanceToMeter((double) repeatDis, this.mCurrentSportType)));
        } else {
            sportConfig.setMSportLapMeter(((float) UnitConvertUtils.convertDistanceFromMileOrKmtokm((double) (repeatDis / 1000.0f))) * 1000.0f);
        }
        sportConfig.setMIsSportLapMeterAlertEnabled(enableRepeatDis);
        if (!this.mSportConfig.isRemindPerKM()) {
            remindkm = 0;
        }
        if (UnitConvertUtils.isImperial()) {
            sportConfig.setMIsSporKiloMeterAlertEnabled(false);
            sportConfig.setMSportKiloMeter(((float) remindkm) * 1000.0f);
        } else {
            sportConfig.setMIsSporKiloMeterAlertEnabled(true);
            sportConfig.setMSportKiloMeter(((float) remindkm) * 1000.0f);
        }
        sportConfig.setMIsSportAutoPauseEnabled(enableAutoPause);
        if (this.mSportOptions.getSportType() == 1 || this.mSportOptions.getSportType() == 9) {
            sportConfig.setMSportAutoPauseSpeedTh(autoSpeed);
        } else {
            sportConfig.setMSportAutoPauseSpeedTh(0.0f);
        }
        if (this.mSportOptions.getSportType() == 11) {
            sportConfig.setMSportFastTargetPace(pace);
            sportConfig.setMIsSportTargetPaceAlertEnabled(false);
            sportConfig.setMIsSportFastTargetPaceAlertEnabled(enablePace);
        } else {
            sportConfig.setMSportTargetPace(pace);
            sportConfig.setMIsSportTargetPaceAlertEnabled(enablePace);
        }
        if (UnitConvertUtils.isHuangheMode()) {
            isRemindTe = false;
        }
        if (!isRemindTe || this.mSportOptions.isTranningMode()) {
            sportConfig.setMSportTargetDistance(totalDis);
            sportConfig.setMIsSportTargetDistanceAlertEnabled(enableTotalDis);
        } else {
            sportConfig.setMSportTargetDistance(0.0f);
            sportConfig.setMIsSportTargetDistanceAlertEnabled(false);
        }
        if (configHeart < 0.0f) {
            configHeart = (float) safeHeart;
        }
        sportConfig.setMSportSafeHeartRate((int) configHeart);
        sportConfig.setMIsSafeHeartRateAlertEnabled(enableHeart);
        if (isRemindTe) {
            sportConfig.setMSportTargetCalories(calorie);
            sportConfig.setMIsSportTargetCaloriesAlertEnabled(false);
        } else {
            sportConfig.setMSportTargetCalories(calorie);
            sportConfig.setMIsSportTargetCaloriesAlertEnabled(enableCalorie);
        }
        sportConfig.setMSportHrZoneLower((float) minHeartRemind);
        sportConfig.setMSportHrZoneUpper((float) maxHeartRemind);
        sportConfig.setMIsSportHrZoneAlertEnabled(enableHeartRegionRemind);
        if (isRemindTe) {
            teValueInt = (int) (10.0f * teValue);
            sportConfig.setMSportTargetTimer((float) ((TERemindStage) this.mTeRemindSettings.get(this.mSportConfig.getTargetTEStage())).getTimeCost());
            sportConfig.setMIsSportTargetTimerAlertEnabled(false);
        } else {
            teValueInt = 0;
            sportConfig.setMSportTargetTimer((float) targetTimeCost);
            sportConfig.setMIsSportTargetTimerAlertEnabled(false);
        }
        Log.i("OutdoorSportDelegate", " teValueInt:" + teValueInt, new Object[0]);
        if (isRemindRealTimeGuide) {
            sportConfig.setMSportTargetTrainingEffect(teValueInt + 128);
        } else {
            sportConfig.setMSportTargetTrainingEffect(teValueInt);
        }
        Log.i("OutdoorSportDelegate", "---new Data set sensor method---", new Object[0]);
        sportConfig.setMIs3DDistanceEnabled(this.mSportConfig.is3DSportModel());
        if (SportType.isSwimMode(this.mCurrentSportType)) {
            sportConfig.setMIsMeasureHrInCrossCountryRun(false);
        } else if (this.mSportOptions.getSportType() == 11) {
            sportConfig.setMIsMeasureHrInCrossCountryRun(true);
        } else {
            sportConfig.setMIsMeasureHrInCrossCountryRun(this.mSportConfig.isMeasureHeart());
        }
        if (this.mSportOptions != null && this.mSportOptions.getSportType() == 8) {
            sportConfig.mStepModel = DataManager.getInstance().getStepLength(this.mContext);
        }
        if (this.mSportOptions.getSportType() != 14) {
            sportConfig.setMSwimPoolLen(0);
        } else if (this.mSportConfig.getUnit() == 0) {
            sportConfig.setMSwimPoolLen((int) Math.round(UnitConvertUtils.convertDistanceToMeter((double) this.mSportConfig.getTargetSwimLength(), this.mCurrentSportType)));
        } else {
            sportConfig.setMSwimPoolLen((int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter((double) this.mSportConfig.getTargetSwimLength(), this.mSportConfig.getUnit())));
        }
        if (this.mSportOptions.getSportType() != 17) {
            switch (WearHabitDefaultValHelper.getWearHabitDefVal(this.mContext)) {
                case 0:
                    Log.i("OutdoorSportDelegate", "wearhand normal left handers", new Object[0]);
                    sportConfig.setMUserIsLeftHander(true);
                    break;
                case 1:
                    Log.i("OutdoorSportDelegate", "wearhand normal right handers", new Object[0]);
                    sportConfig.setMUserIsLeftHander(false);
                    break;
                default:
                    break;
            }
        }
        switch (tennicClapHand) {
            case 0:
                Log.i("OutdoorSportDelegate", "wearhand tennic left handers", new Object[0]);
                sportConfig.setMUserIsLeftHander(true);
                break;
            case 1:
                Log.i("OutdoorSportDelegate", "wearhand tennic right handers", new Object[0]);
                sportConfig.setMUserIsLeftHander(false);
                break;
        }
        this.mSensorHubManagerWrapper.setSportConfigData(sportConfig);
    }

    private void notifyAlgorithmConfig(BaseConfig config) {
        Debug.m3d("OutdoorSportDelegate", "notify algorithm config : " + config);
        if (this.mIsIntervalTrainMode) {
            this.mTrainManager.configNextUnitCmd(config, this.mCurrentStatus, 0);
        } else if (this.mSportOptions.isTranningMode()) {
            isAutoPause = config.isAutoPause();
            autoSpeed = config.getSportType() != 9 ? Float.compare(config.getAutoPausePace(), 0.0f) == 0 ? 0.0f : 1.0f / config.getAutoPausePace() : config.getAutoPausePace();
            TrainingInfo info = TrainingInfoManager.getInstance(this.mContext).getTrainingInfo();
            Debug.m5i("OutdoorSportDelegate", "train info:" + info.toString() + "isRemind: " + (info.getRateHigh() != 0));
            if (info.getTrainStatus() == 0) {
                boolean z;
                int defaultHeart = UserInfoManager.getDefaultSafeHeart(this.mContext);
                float convertPaceFromMileOrKmTokm = ((float) UnitConvertUtils.convertPaceFromMileOrKmTokm((double) (config.getTargetPace() * 1000.0f))) / 1000.0f;
                r10 = info.getDistance();
                if (info.getRateHigh() != 0) {
                    z = true;
                } else {
                    z = false;
                }
                configAlgorithmConfig(true, 1000.0f, isAutoPause, autoSpeed, false, convertPaceFromMileOrKmTokm, true, r10, z, (((float) info.getRateHigh()) / 100.0f) * ((float) defaultHeart), config.isRemindCalorie(), (float) config.getTargetCalorie(), config.ismHeartRegionRemind(), config.getmHeartRegionValueMin(), config.getmHeartRegionValueMax(), config.isRemindTE(), config.isRealTimeGuide(), config.getTargetTE(), config.isRemindTargetTimeCost(), config.getTargetTimeCost() / 1000, config.getTennicClapThaHands(), config.getRemindKm());
            }
        } else {
            boolean isRemindTargetDis = config.isRemindTargetDistance();
            if (SportType.isSwimMode(this.mCurrentSportType)) {
                if (this.mSportConfig.getUnit() != 0) {
                    r10 = (float) ((int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter((double) (config.getTargetSwimLength() * config.getTargetTrips()), this.mSportConfig.getUnit())));
                } else if (this.mSportOptions.getSportType() == 14) {
                    r10 = (float) (config.getTargetSwimLength() * config.getTargetTrips());
                } else {
                    r10 = (float) config.getTargetDistance();
                }
                isRemindTargetDis = config.isRemindTargetTrips();
            } else {
                r10 = (float) (UnitConvertUtils.convertDistanceFromMileOrKmtokm((double) (((float) config.getTargetDistance()) / 1000.0f)) * 1000.0d);
            }
            boolean isRemindRegionRemind = config.ismHeartRegionRemind();
            isAutoPause = config.isAutoPause();
            if (config.getSportType() != 9) {
                autoSpeed = (float) UnitConvertUtils.convertSpeedToMeter((double) (Float.compare(config.getAutoPausePace(), 0.0f) == 0 ? 0.0f : 1.0f / config.getAutoPausePace()));
            } else {
                autoSpeed = ((float) UnitConvertUtils.convertPaceFromMileOrKmTokm((double) (1000.0f * config.getAutoPausePace()))) / 1000.0f;
            }
            if (SportType.isMixedSport(config.getSportType())) {
                if (this.mCurrentSportType != 1001) {
                    isRemindRegionRemind = false;
                }
                if (this.mCurrentSportType == 1015) {
                    isAutoPause = false;
                }
            }
            configAlgorithmConfig(config.isOpenAutoLapRecord(), config.getDistanceAutoLap(), isAutoPause, autoSpeed, config.isRemindTargetPace(), ((float) UnitConvertUtils.convertPaceFromMileOrKmTokm((double) (config.getTargetPace() * 1000.0f))) / 1000.0f, isRemindTargetDis, r10, config.isRemindSafeHeartRate(), config.getSafeHeartRateHigh(), config.isRemindCalorie(), (float) config.getTargetCalorie(), isRemindRegionRemind, config.getmHeartRegionValueMin(), config.getmHeartRegionValueMax(), config.isRemindTE(), config.isRealTimeGuide(), config.getTargetTE(), config.isRemindTargetTimeCost(), config.getTargetTimeCost() / 1000, config.getTennicClapThaHands(), config.getRemindKm());
            if (this.mSportOptions.getSportType() == 7) {
                this.mSensorHubManagerWrapper.setSLPTUpdateAt1Hz(false);
            } else {
                this.mSensorHubManagerWrapper.setSLPTUpdateAt1Hz(this.mSportConfig.isSportSecondBrush());
            }
        }
    }

    public long getCurrentTimeMillisOnRuntime() {
        return (((this.mBaseTime + SystemClock.elapsedRealtime()) - this.mBaseElapsedTime) / 1000) * 1000;
    }

    public void setIsGiveupRecord(boolean isGiveup) {
        Debug.m5i("OutdoorSportDelegate", "setIsGiveupRecord:" + isGiveup);
        this.mIsGiveupRecord = isGiveup;
    }

    public void addKmInfo(int km, float pace, float speed, boolean isImperial, int teValue, float dailyPorpermence, long[] lngLat, long curTime) {
        LogUtil.m10w(true, "OutdoorSportDelegate", "add km info " + km + ", " + pace + ", " + speed + ", isImperial:" + isImperial);
        if (km <= this.mLastKilometer) {
            LogUtil.m10w(true, "OutdoorSportDelegate", "km : " + km + " should not less than last km : " + this.mLastKilometer);
            return;
        }
        int costTime = (int) ((((double) pace) * UnitConvertUtils.convertDistanceFromMileOrKmtokm(1.0d, isImperial)) * 1000000.0d);
        RunningInfoPerKM runningInfoPerKM = new RunningInfoPerKM();
        runningInfoPerKM.setTrackId(this.mCurrentSportTrackId);
        runningInfoPerKM.setCostTime((long) costTime);
        runningInfoPerKM.setSpeed((double) speed);
        runningInfoPerKM.setPace((double) pace);
        runningInfoPerKM.setTeValue(teValue);
        runningInfoPerKM.setDailyPorpermence(dailyPorpermence);
        runningInfoPerKM.setTotalCostTime(curTime - this.mCurrentSportTrackId);
        if (lngLat != null && lngLat.length >= 3) {
            float lng = ((float) lngLat[1]) / 3000000.0f;
            float lat = ((float) lngLat[2]) / 3000000.0f;
            Debug.m5i("OutdoorSportDelegate", "addKmInfo, lng:" + lng + ", lat:" + lat);
            runningInfoPerKM.setLongitude(lng);
            runningInfoPerKM.setLatitude(lat);
        }
        if (isImperial) {
            runningInfoPerKM.setUnit(1);
        }
        DailyPerformanceInfo dailyPerformanceInfo = new DailyPerformanceInfo();
        dailyPerformanceInfo.setTeValue(teValue);
        dailyPerformanceInfo.setDailyPorpermence(dailyPorpermence);
        dailyPerformanceInfo.setmTrackId(this.mCurrentSportTrackId);
        dailyPerformanceInfo.setSaveDataType(0);
        dailyPerformanceInfo.setCreateTime(curTime);
        dailyPerformanceInfo.setCurrnetKilos((float) km);
        this.mRunningInfoPerKMs.add(runningInfoPerKM);
        this.mDailyPerformanceInfoList.add(dailyPerformanceInfo);
        this.mLastKilometer = km;
    }

    private void updatePauseInfo(boolean auto) {
        if (this.mLastPausedTimestamp > 0) {
            long currentTime = getCurrentTimeMillisOnRuntime();
            if (currentTime - this.mLastPausedTimestamp < 1000) {
                Debug.m5i("OutdoorSportDelegate", "pause time is below 1s, ignore:" + ((currentTime - this.mLastPausedTimestamp) / 1000));
                this.mLastPausedTimestamp = -1;
                return;
            }
            PauseInfo pauseInfo = new PauseInfo(this.mLastPausedTimestamp, currentTime, auto ? (byte) 1 : (byte) 2);
            synchronized (this.mLock) {
                this.mDynamicPauseRecords.add(pauseInfo);
            }
            this.mOutdoorSummary.addPauseInfo(pauseInfo);
            this.mPausedTime = (int) (((long) this.mPausedTime) + (currentTime - this.mLastPausedTimestamp));
            this.mLapPausedTime = (int) (((long) this.mLapPausedTime) + (currentTime - this.mLastPausedTimestamp));
            if (this.mTrainManager != null) {
                this.mTrainManager.updateTrainLapPauseInfo(currentTime - this.mLastPausedTimestamp);
            }
            this.mLastPausedTimestamp = -1;
            return;
        }
        Debug.m6w("OutdoorSportDelegate", "update pause info failed. last pause time " + this.mLastPausedTimestamp);
    }

    private void keepSportRecordCount(Context context) {
        Debug.m3d("OutdoorSportDelegate", "keep sport record count");
        List<? extends SportSummary> summaries = this.mSportDBControlCenter.getSportSummaryList(null, null, "start_time DESC", null);
        if (summaries == null || summaries.size() < 30) {
            Debug.m6w("OutdoorSportDelegate", "summary is null or empty while check sport record count");
            return;
        }
        Debug.m3d("OutdoorSportDelegate", "sport summary to check : " + summaries.size());
        int i = 0;
        List<SportSummary> summariesToDelete = new LinkedList();
        for (SportSummary summary : summaries) {
            if (!(summary.getCurrentStatus() == 0 || summary.getCurrentStatus() == 1)) {
                int i2 = i + 1;
                if (i >= 30) {
                    summariesToDelete.add(summary);
                }
                i = i2;
            }
        }
        Debug.m3d("OutdoorSportDelegate", "sport summary to delete : " + summariesToDelete);
        if (!summariesToDelete.isEmpty()) {
            this.mSportDBControlCenter.deleteSportSummary("track_id<=?", new String[]{"" + ((SportSummary) summariesToDelete.get(0)).getTrackId()});
            RecordGraphManager.getInstance(Global.getApplicationContext()).deleteCache(firstSummary.getTrackId());
        }
    }

    public List<HeartRate> getHeartRatesInfo() {
        return this.mAvaiableHeartRateList;
    }

    public CurLapInfo getCurLapInfo() {
        if (this.mCurLapInfo != null) {
            this.mCurLapInfo.setTotalTime(getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId);
        }
        return this.mCurLapInfo;
    }

    public void doRecordOneLap() {
        Debug.m5i("OutdoorSportDelegate", "doRecordOneLap");
        if (this.mCurrentState.state() != 2 && this.mPreviousManualLapsInfo != null) {
            if (!this.mIsInitLap) {
                this.mIsInitLap = true;
                this.mLapPausedTime = 0;
                this.mLastLapEndTime = this.mTrackId;
            }
            this.mSensorHubManagerWrapper.setLapCallback(new Callback() {
                protected void doCallback(int code, Object params) {
                    if (code != 0) {
                        Debug.m5i("OutdoorSportDelegate", "doRecordOneLap, not success");
                        return;
                    }
                    OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) params;
                    if (OutdoorSportDelegate.this.isAvailableLapInfo(snapshot)) {
                        RunningInfoPerLap lastLap = OutdoorSportDelegate.this.addManualLapInfo(snapshot);
                        OutdoorSportDelegate.this.mLastLapEndTime = OutdoorSportDelegate.this.getCurrentTimeMillisOnRuntime();
                        OutdoorSportDelegate.this.mCurLapInfo = new CurLapInfo();
                        OutdoorSportDelegate.this.mCurLapInfo.setFlag(0);
                        OutdoorSportDelegate.this.mCurLapInfo.setNumber(OutdoorSportDelegate.this.mPreviousManualLapsInfo.size());
                        OutdoorSportDelegate.this.mCurLapInfo.setRunningTime(lastLap.getCostTime());
                        OutdoorSportDelegate.this.mListener.onManualOrAutoLapStart(true);
                        return;
                    }
                    Toast.makeText(OutdoorSportDelegate.this.mContext, OutdoorSportDelegate.this.mContext.getString(C0532R.string.lap_add_failed), 0).show();
                    OutdoorSportDelegate.this.mListener.onManualOrAutoLapStart(true);
                }
            });
            this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, null);
        }
    }

    private boolean isAvailableLapInfo(OutdoorSportSnapshot sportSnapshot) {
        return true;
    }

    private RunningInfoPerLap addManualLapInfo(OutdoorSportSnapshot snapshot) {
        double d;
        Debug.m5i("OutdoorSportDelegate", "addManualLapInfo, " + snapshot.toString());
        long totalPreTime = 0;
        int totalPreDis = 0;
        int totalPreCalorie = 0;
        float totalPreClimbDis = 0.0f;
        float totalPreClimbUp = 0.0f;
        float totalPreClimbDown = 0.0f;
        long totalRunTime = 0;
        for (RunningInfoPerLap lap : this.mPreviousManualLapsInfo) {
            totalPreDis = (int) (((float) totalPreDis) + lap.getDistance());
            totalPreCalorie += lap.getLapCalories();
            totalPreClimbDis += lap.getClimbDistance();
            totalPreClimbUp += lap.getClimbUp();
            totalPreClimbDown += lap.getClimbDown();
            totalRunTime += lap.getCostTime();
        }
        if (!this.mPreviousManualLapsInfo.isEmpty()) {
            totalPreTime = ((RunningInfoPerLap) this.mPreviousManualLapsInfo.get(this.mPreviousManualLapsInfo.size() - 1)).getTotalCostTime();
        }
        RunningInfoPerLap lap2 = new RunningInfoPerLap();
        if (this.mSportOptions.getSportType() == 11) {
            lap2.setDistance((float) ((snapshot.getClimbdisDescend() > 0.0f ? (int) snapshot.getClimbdisDescend() : 0) - totalPreDis));
        } else {
            int dis = (int) (snapshot.getDistance() - ((float) totalPreDis));
            if (!SportType.isSwimMode(this.mSportOptions.getSportType())) {
                dis = (dis / 10) * 10;
            }
            lap2.setDistance((float) dis);
        }
        long lapCostTime = (getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId) - totalPreTime;
        if (lapCostTime >= ((long) this.mLapPausedTime)) {
            lapCostTime -= (long) this.mLapPausedTime;
        } else {
            Debug.m5i("OutdoorSportDelegate", "addManualLapInfo, lapCostTime:" + lapCostTime + ", pausedTime:" + this.mLapPausedTime);
        }
        lap2.setCostTime(lapCostTime);
        long totalCostTime = getCurrentTimeMillisOnRuntime() - this.mCurrentSportTrackId;
        if (totalCostTime < totalRunTime + lapCostTime) {
            totalCostTime = totalRunTime + lapCostTime;
        }
        lap2.setTotalCostTime(totalCostTime);
        if (Float.compare(0.0f, lap2.getDistance()) == 0) {
            d = 0.0d;
        } else {
            d = (double) ((((float) lap2.getCostTime()) / 1000.0f) / lap2.getDistance());
        }
        lap2.setPace(d);
        lap2.setLapNum(this.mPreviousManualLapsInfo.size());
        lap2.setLapCalories((int) (snapshot.getCalorie() - ((float) totalPreCalorie)));
        lap2.setClimbDistance(snapshot.getClimbdisascend() - totalPreClimbDis);
        lap2.setClimbUp(snapshot.getClimbUp() - totalPreClimbUp);
        lap2.setClimbDown(snapshot.getClimbDown() - totalPreClimbDown);
        if (lap2.getCostTime() != 0) {
            lap2.setLapStepFreq((float) ((int) (((long) (snapshot.getStepCount() - this.mLastLapStepCount)) / (lap2.getCostTime() / 1000))));
        }
        lap2.setTrackId(this.mCurrentSportTrackId);
        if (SportType.isSwimMode(this.mSportOptions.getSportType())) {
            lap2.setLapStrokes(snapshot.getTotalStrokes() - this.mLastLapStrokes);
            int costTime = (int) (lap2.getCostTime() / 1000);
            lap2.setLapStrokeSpeed(costTime == 0 ? 0.0f : ((float) lap2.getLapStrokes()) / ((float) costTime));
            if (Float.compare(0.0f, lap2.getDistance()) != 0) {
                int swimLength = (int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter((double) this.mSportConfig.getTargetSwimLength(), this.mSportConfig.getUnit()));
                if (this.mSportConfig.getSportType() == 15) {
                    swimLength = (int) Math.round(UnitConvertUtils.convertYDOrMeter2Meter(100.0d, this.mSportConfig.getUnit()));
                }
                lap2.setLapSwolf((int) (((float) ((lap2.getLapStrokes() + costTime) * swimLength)) / lap2.getDistance()));
            }
        }
        Long[] lngLat = snapshot.getCurGPSLonLatPoint();
        if (lngLat != null && lngLat.length >= 3) {
            float lng = ((float) lngLat[1].longValue()) / 3000000.0f;
            float lat = ((float) lngLat[2].longValue()) / 3000000.0f;
            Debug.m5i("OutdoorSportDelegate", "addManualLapInfo, lng:" + lng + ", lat:" + lat);
            lap2.setLongitude(lng);
            lap2.setLatitude(lat);
        }
        this.mPreviousManualLapsInfo.add(lap2);
        this.mLapPausedTime = 0;
        this.mLastLapTotalDistance = (int) snapshot.getDistance();
        this.mLastLapEndTime = getCurrentTimeMillisOnRuntime();
        this.mLastLapStepCount = snapshot.getStepCount();
        this.mLastLapStrokes = snapshot.getTotalStrokes();
        return lap2;
    }

    public void onSportHeartReady(List<HeartRate> heartRates) {
        Debug.m3d("OutdoorSportDelegate", "onSportHeartReady : " + heartRates);
        if (heartRates != null && !heartRates.isEmpty()) {
            synchronized (this.mLock) {
                Iterator<PauseInfo> pauseTimeIterator = this.mDynamicPauseRecords.iterator();
                for (HeartRate heartRate : heartRates) {
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m3d("OutdoorSportDelegate", "heart rate : " + heartRate);
                        Debug.m3d("OutdoorSportDelegate", "paused time : " + this.mDynamicPauseRecords);
                    }
                    while (pauseTimeIterator.hasNext()) {
                        PauseInfo nextPaused = (PauseInfo) pauseTimeIterator.next();
                        if (nextPaused.getEnd() <= heartRate.getTimestamp()) {
                            pauseTimeIterator.remove();
                            this.mLatestPausedTime = nextPaused;
                            this.mHeartRateTotalPausedTime += this.mLatestPausedTime.getEnd() - this.mLatestPausedTime.getStart();
                        }
                    }
                    heartRate.setTrackId(this.mCurrentSportTrackId);
                    if (heartRate.getHeartRate() > 0 && heartRate.getHeartQuality() <= 1) {
                        this.mSingleHeartSum += heartRate.getHeartRate();
                        this.mSingleHeartCount++;
                        if (heartRate.getHeartRate() > this.mMaxHeartRate) {
                            this.mMaxHeartRate = heartRate.getHeartRate();
                        }
                        if (heartRate.getHeartRate() < this.mMinHeartRate) {
                            this.mMinHeartRate = heartRate.getHeartRate();
                        }
                    }
                    int stepCount = heartRate.getStepDiff();
                    if (stepCount != 0) {
                        this.mStepCount += stepCount;
                        this.mStepStride += heartRate.getDistance();
                    }
                    heartRate.setRunTime((int) ((heartRate.getTimestamp() - this.mHeartRateTotalPausedTime) - this.mStartTime));
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m3d("OutdoorSportDelegate", "heart rate after modify : " + heartRate);
                    }
                    if (getLastHeartRateTime() != heartRate.getTimestamp()) {
                        this.mAvaiableHeartRateList.add(heartRate);
                        reSizeAvailableHeartRateList();
                    }
                }
            }
            this.mAllHeartRateList.addAll(heartRates);
            onHeartRateConfirm(heartRates);
        }
    }

    private void reSizeAvailableHeartRateList() {
        if (this.mAvaiableHeartRateList != null && !this.mAvaiableHeartRateList.isEmpty()) {
            long lastTimestamp = ((HeartRate) this.mAvaiableHeartRateList.get(this.mAvaiableHeartRateList.size() - 1)).getTimestamp();
            ListIterator<HeartRate> iterator = this.mAvaiableHeartRateList.listIterator();
            while (iterator.hasNext() && ((int) ((lastTimestamp - ((HeartRate) iterator.next()).getTimestamp()) / 1000)) > 1320) {
                iterator.remove();
            }
        }
    }

    private long getLastHeartRateTime() {
        if (this.mAvaiableHeartRateList == null || this.mAvaiableHeartRateList.isEmpty()) {
            return -1;
        }
        return ((HeartRate) this.mAvaiableHeartRateList.get(this.mAvaiableHeartRateList.size() - 1)).getTimestamp();
    }

    private void onHeartRateConfirm(List<HeartRate> heartRates) {
        Debug.m3d("OutdoorSportDelegate", "onHeartRateConfirm");
        if (heartRates == null) {
            Debug.m5i("OutdoorSportDelegate", "heartrate is null");
        } else if (heartRates.isEmpty()) {
            Debug.m5i("OutdoorSportDelegate", "heartrate is empty");
        } else {
            this.mSportDBControlCenter.asyncAddHeartData(heartRates, null);
        }
    }

    private void sportLocationReady(List<HmGpsLocation> locations) {
        for (HmGpsLocation loc : locations) {
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "GPS_Collection", "" + loc.getTimestamp() + " " + loc.getLongitude() + " " + loc.getLatitude() + " " + loc.getPointType());
            SportPointTypeParser sportPointTypeParser = this.mSportPointTypeParser;
            int pointType = SportPointTypeParser.parsePointType(loc);
            SportLocationData data = new SportLocationData();
            data.mPointIndex = loc.getGloIndex();
            data.mTrackId = this.mCurrentSportTrackId;
            data.mAlgoPointType = loc.getPointType();
            data.mCourse = loc.getCourse() / 1000.0f;
            data.mPointType = pointType;
            data.mTimestamp = loc.getTimestamp();
            data.mLatitude = (float) loc.getLatitude();
            data.mLongitude = (float) loc.getLongitude();
            data.mSpeed = loc.getSpeed();
            data.mAltitude = (float) loc.getAltitude();
            data.mGPSAccuracy = (float) loc.getAccuracy();
            data.mBar = loc.getBar();
            this.mLocationData = data;
            if (this.mCurrentState.state() != this.mStopState.state()) {
                this.mPointProvider.addNewPoint(data);
            }
            if (loc.getTimestamp() < this.mMaxTimestamp) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m5i("OutdoorSportDelegate", "loc " + loc + " is an optimized point, max timestamp : " + this.mMaxTimestamp);
                }
                if (Global.IGNORE_OPTMIZE_GPS) {
                    Debug.m5i("OutdoorSportDelegate", "ignore gps point " + loc);
                } else if ((pointType & 4096) != 0) {
                    this.mLocationPool.changeIfExist(data);
                } else if ((pointType & 32768) != 0) {
                    Debug.m6w("OutdoorSportDelegate", "invalid point " + pointType + ". loc : " + loc + ", current max time : " + this.mMaxTimestamp);
                    this.mLocationPool.deleteIfExist(data);
                } else {
                    Debug.m6w("OutdoorSportDelegate", "illegal point type " + pointType + ". loc : " + loc + ", current max time : " + this.mMaxTimestamp);
                }
            } else if ((pointType & 32768) == 0) {
                LogUtil.m7d(Global.DEBUG_LEVEL_3, "GPS_ORIGIN", "" + loc.getTimestamp() + " " + loc.getLongitude() + " " + loc.getLatitude() + " " + loc.getPointType());
                if (this.mMaxTimestamp < 0) {
                    LogUtil.m9i(true, "OutdoorSportDelegate", "first timestamp : " + loc.getTimestamp());
                }
                this.mMaxTimestamp = loc.getTimestamp();
                this.mLocationPool.add(data, this.mCurrentSportTrackId);
            }
        }
    }

    public void onSportLocationReady(List<HmGpsLocation> locations) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("OutdoorSportDelegate", "onSportLocationReady : " + locations);
        }
        if (locations != null && !locations.isEmpty()) {
            Debug.m5i("OutdoorSportDelegate", "onSportLocationReady, " + locations.size());
            sportLocationReady(locations);
        }
    }

    public void onTicker(int second) {
        if (!Global.DEBUG_GPS && this.mCurrentState.state() == this.mRunningState.state()) {
            this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, null);
        }
        this.mCurrentStatus.setCurrentSecond(second);
        if (this.mCurrnetCaldenceValue >= 0) {
            this.mCurrentStatus.setCadence((float) this.mCurrnetCaldenceValue);
        }
        long mixedTime = getCurrentTimeMillisOnRuntime() - this.mTrackId;
        if (mixedTime < ((long) second) * 1000) {
            mixedTime = ((long) second) * 1000;
        }
        this.mCurrentStatus.setMixedTotalTime(mixedTime);
        this.mLastStatus = OutdoorSportSnapshot.copyOf(this.mCurrentStatus);
        if (this.mListener == null) {
            Debug.m6w("OutdoorSportDelegate", "listener is null while onTicker");
            return;
        }
        if (this.mTrainManager != null) {
            this.mTrainManager.refreshTrainDataPerSec(this.mCurrentStatus);
        }
        this.mListener.onSportStatusUpdate(this, this.mCurrentStatus);
    }

    public void onPauseTicker(int sec) {
        this.mCurrentStatus.setCurrentPausedSecond(sec);
        if (this.mListener != null) {
            this.mListener.onSportStatusUpdate(this, this.mCurrentStatus);
        }
    }

    public void onMillTicker(int millSec) {
        this.mCurrentStatus.setCurrentMillSec(millSec);
        this.mListener.onMillUpdate(millSec);
    }

    public OutdoorSportSnapshot getSportStatus() {
        return this.mCurrentStatus;
    }

    public void shutDown() {
        Debug.m3d("OutdoorSportDelegate", "shut down");
        if (this.mSportOptions != null && SportType.isSportTypeNeedGps(this.mCurrentSportType)) {
            onSportLocationReady(this.mLocationManager.stopListen());
        }
        this.mStopStep = (byte) (this.mStopStep | 2);
        if (!SportType.isValidWhenEndSport(this.mCurrentSportType, this.mCurrentStatus) || SportType.isMixedSport(this.mSportOptions.getSportType()) || this.mIsGiveupRecord) {
            Debug.m5i("OutdoorSportDelegate", "the dis is:" + this.mCurrentStatus.getDistance());
        } else {
            Debug.m5i("OutdoorSportDelegate", "valid sport, today dis:" + this.mTodayDistance + ", total dis:" + this.mTotalDistance);
        }
        if (!DateUtils.isSameDate(this.mCurrentSportTrackId)) {
            Debug.m5i("OutdoorSportDelegate", "the sport time beyond a whole day");
            this.mTodayDistance = 0.0f;
        }
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport heart rate start============================");
        onSportHeartReady(this.mHeartRateManager.stopListen());
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport heart rate end============================");
        doFinish(this.mCurrentStatus, null, null);
        if (this.mOutdoorSummary != null) {
            this.mSportDBControlCenter.updateSportSummary(this.mOutdoorSummary);
        }
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport km info start============================");
        saveRunningInfoPerKm2db();
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport km info end============================");
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport lap info start============================");
        saveLapInfo2db();
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport lap info end============================");
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport daily performance start============================");
        saveDailyPerformance2db();
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport daily performance end============================");
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport location start============================");
        this.mLocationPool.commit(this.mCurrentSportTrackId);
        if (this.mCurrentStatus != null) {
            this.mSportDBControlCenter.updateLocationAltitude(this.mCurrentSportTrackId, this.mCurrentStatus.getOffsetAltitude());
        }
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport location end============================");
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport summary start============================");
        saveNecessaryDataWhenSportStopped(null);
        Debug.m3d("OutdoorSportDelegate", "=================================shut down save sport summary end============================");
    }

    public void setCurrnetCaldengceValue(int value) {
        this.mCurrnetCaldenceValue = value;
    }

    public void saveCadenceDetailList(List<CyclingDetail> list) {
        if (this.mSportDBControlCenter != null) {
            this.mSportDBControlCenter.addCadenceDetailList(list);
            if (getUploadSaver(this.mCurrentSportTrackId) != null) {
                getUploadSaver(this.mCurrentSportTrackId).saveCadenceData(list, false);
            }
        }
    }

    public void addCadenceSummaryData(CyclingDataSummary dataSummary) {
        if (this.mCadenceSummaryList != null) {
            this.mCadenceSummaryList.add(dataSummary);
            Debug.m5i("OutdoorSportDelegate", "addCadenceSummaryData, " + dataSummary.toString());
        }
    }

    private void addAveCadencePerLap(List<RunningInfoPerLap> lapList) {
        if (this.mCadenceSummaryList != null && !this.mCadenceSummaryList.isEmpty()) {
            int lastAveCadence = 0;
            int lastCircle = 0;
            long lastTime = 0;
            for (RunningInfoPerLap lap : lapList) {
                long endTime = lap.getTotalCostTime();
                long startTime = endTime - lap.getCostTime();
                int i = 0;
                while (i < this.mCadenceSummaryList.size()) {
                    CyclingDataSummary summary = (CyclingDataSummary) this.mCadenceSummaryList.get(i);
                    long suTime = summary.getEndTime() - summary.getStartTime();
                    if (suTime >= startTime && (suTime > endTime || i == this.mCadenceSummaryList.size() - 1)) {
                        if (suTime == lastTime) {
                            lap.setLapCadence(lastAveCadence);
                        } else {
                            int aveCadence = (int) (((long) ((summary.getTotalCircle() - lastCircle) * 60)) / ((suTime - lastTime) / 60000));
                            lap.setLapCadence(aveCadence);
                            lastAveCadence = aveCadence;
                        }
                        lastCircle = summary.getTotalCircle();
                        lastTime = suTime;
                    } else {
                        i++;
                    }
                }
            }
        }
    }

    public boolean hasNextSport() {
        return this.mSportList.size() > 0;
    }

    public boolean isValidSportWhenEndSport() {
        if (SportType.isMixedSport(this.mSportOptions.getSportType()) ? this.mIsAvailableMixedSport || SportType.isValidWhenEndSport(this.mCurrentSportType, this.mCurrentStatus) : SportType.isValidWhenEndSport(this.mSportOptions.getSportType(), this.mCurrentStatus)) {
            if (!this.mIsGiveupRecord) {
                return true;
            }
        }
        return false;
    }

    public boolean isPlayVoice() {
        if (this.mSportConfig != null) {
            return this.mSportConfig.isRemindPlayVoice();
        }
        return false;
    }

    public TrainUnit getCurrentTrainUnit() {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, "OutdoorSportDelegate", "getCurrentTrainUnit, manager " + this.mTrainManager);
        if (this.mTrainManager != null) {
            return this.mTrainManager.getCurentTrainUnit();
        }
        return null;
    }

    public void notifyStartNextCustomTrainUnit() {
        if (this.mTrainManager != null) {
            TrainUnit nextUnit = this.mTrainManager.getNextTrainUnit();
            this.mTrainManager.showNextTrainUnitDialog(nextUnit);
            if (nextUnit == null && !this.mTrainManager.reSetItemOrderAfterTrainCompleted(this.mCurrentStatus)) {
                return;
            }
        }
        if (this.mTrainManager != null) {
            this.mSensorHubManagerWrapper.setLapCallback(new Callback() {
                protected void doCallback(int code, Object params) {
                    if (code != 0) {
                        Debug.m5i("OutdoorSportDelegate", "doRecordOneLap, not success");
                        return;
                    }
                    OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) params;
                    OutdoorSportDelegate.this.mTrainManager.addTrainLapInfo(snapshot);
                    boolean isSuccess = OutdoorSportDelegate.this.mTrainManager.configNextUnitCmd(OutdoorSportDelegate.this.mSportConfig, snapshot, snapshot.getTotalTime());
                    LogUtil.m9i(true, "OutdoorSportDelegate", "notifyStartNextCustomTrainUnit, " + isSuccess);
                    if (isSuccess) {
                        OutdoorSportDelegate.this.mListener.doNextTrainUnitStart();
                    } else {
                        OutdoorSportDelegate.this.mListener.doWholeTrainUnitCompleted();
                    }
                }
            });
            this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, null);
        }
    }

    public void notifyHalfSecStart() {
        this.mCurrentStatus.setSecHalfStartTime(getCurrentTimeMillisOnRuntime() - this.mTrackId);
    }

    private void notifyTrainUnitInAdvance() {
        if (this.mIsIntervalTrainMode) {
            PowerUtils.wakeupForMilliSecond(this.mContext, 5000);
            this.mSensorHubManagerWrapper.updateOutdoorSportStatus(this.mCurrentStatus, new Callback() {
                protected void doCallback(int i, Object o) {
                    if (OutdoorSportDelegate.this.mTrainManager != null) {
                        TrainTargetType trainTargetType = OutdoorSportDelegate.this.mTrainManager.getCurentTrainUnit().getUnitType();
                        if (!OutdoorSportDelegate.this.isIntervalTrainInRange(trainTargetType)) {
                            OutdoorSportDelegate.this.mTrainManager.setDisTrainStatus(1);
                            OutdoorSportDelegate.this.notifyTrainUnitFinished();
                        } else if (trainTargetType == TrainTargetType.DIATANCE) {
                            if (OutdoorSportDelegate.this.mTrainManager == null) {
                                return;
                            }
                            if (OutdoorSportDelegate.this.mTrainManager.isDistanceTrainUnitNearlyCompleted()) {
                                if (OutdoorSportDelegate.this.mListener != null) {
                                    LogUtil.m9i(true, "OutdoorSportDelegate", "notifyTrainUnitInAdvance");
                                    OutdoorSportDelegate.this.mListener.onSingleTrainUnitNearlyCompleted();
                                }
                                OutdoorSportDelegate.this.mSensorHubManagerWrapper.setEnable(2, true, null, (float) OutdoorSportDelegate.this.mCurrentStatus.getTrainSnapShot().getCurTargetDis());
                                OutdoorSportDelegate.this.mTrainManager.setDisTrainStatus(1);
                                return;
                            }
                            OutdoorSportDelegate.this.notifyTrainUnitFinished();
                        } else if (trainTargetType == TrainTargetType.TIME) {
                            if (OutdoorSportDelegate.this.mListener != null) {
                                LogUtil.m9i(true, "OutdoorSportDelegate", "notifyTrainUnitInAdvance");
                                OutdoorSportDelegate.this.mListener.onSingleTrainUnitNearlyCompleted();
                            }
                            int timeCost = (int) (OutdoorSportDelegate.this.mCurrentStatus.getTrainSnapShot().getCurTargetCostTime() / 1000);
                            LogUtil.m9i(true, "OutdoorSportDelegate", "notifyTrainUnitInAdvance, now:" + OutdoorSportDelegate.this.getTotalRunningTime(true) + ", target:" + timeCost);
                            OutdoorSportDelegate.this.mTimeCostWakeupManager.startTimeCostAlarm(OutdoorSportDelegate.this.getTotalRunningTime(true), true, timeCost, false);
                        } else {
                            LogUtil.m9i(true, "OutdoorSportDelegate", "notifyTrainUnitInAdvance, not available train " + trainTargetType);
                        }
                    }
                }
            });
        }
    }

    private boolean isIntervalTrainInRange(TrainTargetType type) {
        if (type == TrainTargetType.DIATANCE) {
            if (((double) this.mCurrentStatus.getDistance()) >= ((double) this.mCurrentStatus.getTrainSnapShot().getCurTargetDis())) {
                return false;
            }
        } else if (type == TrainTargetType.TIME) {
            if (this.mCurrentStatus.getTrainSnapShot().getCurTargetCostTime() / 1000 <= getTotalRunningTime(true)) {
                return false;
            }
        }
        return true;
    }

    private void notifyTrainUnitFinished() {
        if (this.mIsIntervalTrainMode) {
            LogUtil.m9i(true, "OutdoorSportDelegate", "notifyTrainUnitFinished");
            if (this.mListener != null) {
                this.mListener.onSingleTrainUnitCompleted();
                notifyStartNextCustomTrainUnit();
            }
        }
    }

    private float getLapMaxPace(long startTime, long endTime) {
        if (this.mAllHeartRateList == null || this.mAllHeartRateList.isEmpty()) {
            return 0.0f;
        }
        float pace = Float.MAX_VALUE;
        long now = System.currentTimeMillis();
        Debug.m5i("OutdoorSportDelegate", "getLapMaxPace, --------start");
        for (int i = 0; i < this.mAllHeartRateList.size(); i++) {
            HeartRate rate = (HeartRate) this.mAllHeartRateList.get(i);
            if (rate.getTimestamp() >= startTime) {
                if (rate.getTimestamp() > endTime) {
                    break;
                } else if (Float.compare(rate.getPace(), 0.0f) != 0 && rate.getPace() < pace) {
                    pace = rate.getPace();
                }
            }
        }
        if (Float.compare(pace, Float.MAX_VALUE) == 0) {
            pace = 0.0f;
        }
        Debug.m5i("OutdoorSportDelegate", "getLapMaxPace, --------end, pace:" + pace + ", cost time:" + (System.currentTimeMillis() - now));
        return pace;
    }

    private void initCurrentStatus() {
        Debug.m5i("OutdoorSportDelegate", "initCurrentStatus");
        this.mCurrentStatus = OutdoorSportSnapshot.createSportStatus(this.mSportOptions.getSportType());
        this.mCurrentStatus.setUnit(this.mSportConfig.getUnit());
        this.mCurrentStatus.setIntervalType(this.mSportOptions.getIntervalType());
        this.mSensorHubManagerWrapper.setSportSnapshot(this.mCurrentStatus);
        this.mSensorHubManagerWrapper.setSportType(this.mSportOptions.getSportType());
        this.mSensorHubManagerWrapper.setSportDataUpdateCallback(new Callback() {
            protected void doCallback(int code, Object params) {
                Debug.m5i("OutdoorSportDelegate", "code: " + code);
            }
        });
        this.mLastStatus = OutdoorSportSnapshot.copyOf(this.mCurrentStatus);
        this.mBaseTime = this.mCurrentSportTrackId;
        this.mBaseElapsedTime = SystemClock.elapsedRealtime();
        if (this.mCurrentStatus == null) {
            Debug.m6w("OutdoorSportDelegate", "cannot create sport status, sport type may invalid : " + this.mSportOptions.getSportType());
            return;
        }
        this.mCurrentStatus.setTrackId(this.mCurrentSportTrackId);
        this.mCurrentStatus.setStartTime(this.mCurrentSportTrackId);
        this.mCurrentStatus.setCurrentStatus((byte) 1);
        if (this.mSportTickerManager != null) {
            this.mSportTickerManager.stopTicker();
        }
        this.mSportTickerManager.setSportTickerListener(this);
        this.mSportTickerManager.startTicker(this.mTickerStartTime);
        if (this.mTrainManager != null) {
            this.mTrainManager.computeCurTrainInfo(this.mCurrentStatus.getTrainSnapShot());
        }
    }

    public void configTrainTimeUnit(TrainUnit curUnit) {
        if (this.mTimeCostWakeupManager == null) {
            this.mTimeCostWakeupManager = (TimeCostWakeUpManager) this.mWakeupManagerFactory.getTimeCostWakeupManager(this.mContext, this.mSportOptions.getSportType()).setConfig(this.mSportConfig);
        }
        this.mTimeCostWakeupManager.setIsSportStarted(true);
        long nowTime = getTotalRunningTime(true);
        int targetTime = (int) ((curUnit.getTargetTime() / 1000) + nowTime);
        LogUtil.m9i(true, "OutdoorSportDelegate", "configTrainTimeUnit, now:" + nowTime + ", target:" + targetTime);
        this.mTimeCostWakeupManager.startTimeCostAlarm(nowTime, this.mIsIntervalTrainMode, targetTime, false);
    }

    private DetailInfoSaver getUploadSaver(long trackId) {
        return this.mUploadCacheManager.getSingleSportSaver(trackId);
    }

    public void onFinalLocationConfirm(List<SportLocationData> locationDatas, long trackId) {
        LogUtil.m9i(true, "OutdoorSportDelegate", "onFinalLocationConfirm:" + this.mCurrentSportTrackId + ", " + trackId);
        if (getUploadSaver(trackId) != null) {
            getUploadSaver(trackId).saveLocationData(locationDatas, false);
        }
    }
}
