package com.huami.watch.newsport.sportcenter.utils;

import android.os.SystemProperties;
import com.huami.watch.common.log.Debug;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AGPSManager {
    private static final String TAG = AGPSManager.class.getName();
    private static AGPSManager sInstance = null;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public static synchronized AGPSManager getInstance() {
        AGPSManager aGPSManager;
        synchronized (AGPSManager.class) {
            if (sInstance == null) {
                sInstance = new AGPSManager();
            }
            aGPSManager = sInstance;
        }
        return aGPSManager;
    }

    public boolean checkAgpsExpired() {
        String agpsStr = SystemProperties.get("persist.sys.gps.agps");
        Debug.m5i(TAG, "agps info: " + agpsStr);
        if (agpsStr == null || agpsStr.length() <= 0) {
            Debug.m5i(TAG, "the agps is not sync");
            return true;
        } else if (agpsStr.contains("T") && agpsStr.contains("Z") && agpsStr.contains(",")) {
            try {
                String syncTime = agpsStr.substring(0, agpsStr.indexOf("Z")).replace("T", " ");
                int expiredTime = Integer.parseInt(agpsStr.substring(agpsStr.indexOf(",") + 1));
                sdf.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                Date syncDate = sdf.parse(syncTime);
                Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
                calendar.setTime(syncDate);
                calendar.set(11, 0);
                calendar.set(12, 0);
                calendar.set(13, 0);
                long syncTimeMill = calendar.getTimeInMillis();
                Calendar nowCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
                nowCalendar.set(11, 0);
                nowCalendar.set(12, 0);
                nowCalendar.set(13, 0);
                float alreadySyncDays = ((float) (nowCalendar.getTimeInMillis() - syncTimeMill)) / 8.64E7f;
                Debug.m5i(TAG, "agps sync time, year:" + calendar.get(1) + ", month: " + calendar.get(2) + ", day:" + calendar.get(5) + ", now time, year:" + nowCalendar.get(1) + ", month: " + nowCalendar.get(2) + ", day:" + nowCalendar.get(5) + ", syncDays: " + alreadySyncDays);
                if (alreadySyncDays >= ((float) expiredTime)) {
                    return true;
                }
                return false;
            } catch (ParseException e) {
                e.printStackTrace();
                Debug.m5i(TAG, "the sync time parse err");
                return true;
            } catch (IndexOutOfBoundsException e2) {
                e2.printStackTrace();
                Debug.m5i(TAG, "the Index out of bound");
                return true;
            } catch (NumberFormatException e3) {
                e3.printStackTrace();
                Debug.m5i(TAG, "the expired time parse err");
                return true;
            }
        } else {
            Debug.m5i(TAG, "the agps format is wrong");
            return true;
        }
    }
}
