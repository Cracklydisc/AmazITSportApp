package com.huami.watch.newsport.sportcenter;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcelable;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportOptions.Builder;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.action.ISportDataManagerCallback;
import com.huami.watch.newsport.sportcenter.action.ISportDelegate;
import com.huami.watch.newsport.sportcenter.action.ISportDelegate.IDelegateDataListener;
import com.huami.watch.newsport.sportcenter.model.CurLapInfo;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.widget.client.SportWidgetClient;
import java.util.List;

public class SportsDataManagerService extends Service {
    private SportDataManagerServiceBinder mBinder = new SportDataManagerServiceBinder();
    private SensorHubManagerWrapper mSensorHubManagerWrapper = null;
    private SportWidgetClient mWidgetClient = null;

    public class SportDataManagerServiceBinder extends Binder implements IDelegateDataListener {
        private int intervalType = 0;
        private int mGPSHolderCount = 0;
        private ISportDataManagerCallback mGPSStatusCallback = null;
        private int mGpsStatus = 2;
        private boolean mIsForeground = true;
        private boolean mIsGpsOpened = false;
        private boolean mIsTrainingMode = false;
        private int mServiceStatus = 0;
        private ISportDataManagerCallback mSportDataCallback = null;
        private ISportDelegate mSportDelegate = null;
        private int mSportType = -1;
        private TrainProgram mTrainProgram = null;

        class C06341 extends Callback {
            C06341() {
            }

            protected void doCallback(int resultCode, Object params) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m3d("SportsDataManagerService", "start callback. result(0 is success) : " + resultCode);
                }
                if (resultCode == 0) {
                    SportDataManagerServiceBinder.this.mServiceStatus = 1;
                    if (SportDataManagerServiceBinder.this.mSportDelegate != null) {
                        SportsDataManagerService.this.mWidgetClient.sendSportStarted(SportDataManagerServiceBinder.this.mSportDelegate.getSportOptions().getSportType());
                    } else {
                        Debug.m6w("SportsDataManagerService", "send sport started failed because delegate is null");
                    }
                    SportDataManagerServiceBinder.this.notifyEvent(2, new Parcelable[0]);
                    return;
                }
                Debug.m6w("SportsDataManagerService", "start failed");
            }
        }

        class C06352 extends Callback {
            C06352() {
            }

            protected void doCallback(int resultCode, Object params) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m3d("SportsDataManagerService", "start callback. result(0 is success) : " + resultCode);
                }
                if (resultCode == 0) {
                    if (SportDataManagerServiceBinder.this.mSportDelegate != null) {
                        SportsDataManagerService.this.mWidgetClient.sendSportStarted(SportDataManagerServiceBinder.this.mSportDelegate.getSportOptions().getSportType());
                    } else {
                        Debug.m6w("SportsDataManagerService", "send sport started failed because delegate is null");
                    }
                    SportDataManagerServiceBinder.this.mServiceStatus = 1;
                    SportDataManagerServiceBinder.this.notifyEvent(2, new Parcelable[0]);
                    return;
                }
                Debug.m6w("SportsDataManagerService", "start failed");
            }
        }

        class C06363 extends Callback {
            C06363() {
            }

            protected void doCallback(int resultCode, Object params) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m3d("SportsDataManagerService", "stop child callback. result(0 is success) : " + resultCode);
                }
                if (resultCode == 0) {
                    if (SportDataManagerServiceBinder.this.mSportDelegate != null) {
                        SportsDataManagerService.this.mWidgetClient.sendSportStopped(SportDataManagerServiceBinder.this.mSportDelegate.getSportOptions().getSportType());
                    } else {
                        Debug.m6w("SportsDataManagerService", "send sport started failed because delegate is null");
                    }
                    SportDataManagerServiceBinder.this.mServiceStatus = 0;
                    SportDataManagerServiceBinder.this.notifyEvent(6, new Parcelable[0]);
                    return;
                }
                Debug.m6w("SportsDataManagerService", "stop child failed");
            }
        }

        class C06385 extends Callback {
            C06385() {
            }

            protected void doCallback(int resultCode, Object params) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m3d("SportsDataManagerService", "stop callback. result(0 is success) : " + resultCode);
                }
                if (resultCode == 0) {
                    SportDataManagerServiceBinder.this.mServiceStatus = 0;
                    SportDataManagerServiceBinder.this.mSportType = -1;
                    if (SportDataManagerServiceBinder.this.mSportDelegate != null) {
                        SportsDataManagerService.this.mWidgetClient.sendSportStopped(SportDataManagerServiceBinder.this.mSportDelegate.getSportOptions().getSportType());
                    } else {
                        Debug.m6w("SportsDataManagerService", "send sport stopped failed because delegate is null");
                    }
                    OutdoorSportSnapshot status = (OutdoorSportSnapshot) params;
                    SportDataManagerServiceBinder.this.notifyEvent(4, status);
                    return;
                }
                Debug.m6w("SportsDataManagerService", "stop sport failed");
            }
        }

        public long startSport(int sportType) {
            Debug.m3d("SportsDataManagerService", "start sport|sport type : " + sportType + "|delegate : " + this.mSportDelegate);
            if (this.mSportDelegate != null) {
                Debug.m6w("SportsDataManagerService", "sport has not been stopped");
            }
            this.mSportDelegate = new OutdoorSportDelegate(SportsDataManagerService.this, new Builder().setSportType(sportType).setGpsStatus(this.mGpsStatus).setTrainProgram(this.mTrainProgram).setIntervalType(this.intervalType).setIsTrainningMode(this.mIsTrainingMode).build());
            this.mSportDelegate.setSportDataListener(this);
            this.mSportType = sportType;
            if (this.mSportDelegate.start(new C06341())) {
                return this.mSportDelegate.getTrackId();
            }
            return -1;
        }

        public boolean startChildSport() {
            return this.mSportDelegate.childSportStart(new C06352());
        }

        public boolean stopChildSport() {
            return this.mSportDelegate.childSportStop(new C06363(), false);
        }

        public int getNextChildSportType() {
            if (SportType.isSportTypeValid(this.mSportType)) {
                return this.mSportDelegate.getNextSportType();
            }
            Debug.m5i("SportsDataManagerService", "getNextSportType, invalid sport type:" + this.mSportType);
            return -1;
        }

        public boolean hasNextChildSport() {
            return this.mSportDelegate.hasNextSport();
        }

        public void setIsTrainingMode(boolean isTrainingMode) {
            this.mIsTrainingMode = isTrainingMode;
        }

        public void setIsInterValTrainMode(int interValTrainMode) {
            this.intervalType = interValTrainMode;
        }

        public void setGpsStatus(int gpsStatus) {
            this.mGpsStatus = gpsStatus;
        }

        public void notifySportForeground() {
            this.mIsForeground = true;
            if (this.mSportDelegate != null) {
                this.mSportDelegate.notifySportDisplayStatus(true);
            }
        }

        public void notifySportBackground() {
            this.mIsForeground = false;
            if (this.mSportDelegate != null) {
                this.mSportDelegate.notifySportDisplayStatus(false);
            }
        }

        public void shutDown() {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.shutDown();
            }
        }

        public int getAutoPauseStatusInScreenOff() {
            if (this.mSportDelegate != null) {
                return this.mSportDelegate.getAutoPauseStatusInScreenOff();
            }
            return 0;
        }

        public synchronized void startGPS() {
            Debug.m3d("SportsDataManagerService", "start gps, current gps holder count " + this.mGPSHolderCount);
            if (!this.mIsGpsOpened) {
                if (SportsDataManagerService.this.mSensorHubManagerWrapper == null) {
                    SportsDataManagerService.this.mSensorHubManagerWrapper = SensorHubManagerWrapper.getInstance(SportsDataManagerService.this);
                }
                SportsDataManagerService.this.mSensorHubManagerWrapper.startBunchModeGPS();
                this.mIsGpsOpened = true;
            }
        }

        public synchronized void stopGPS() {
            Debug.m3d("SportsDataManagerService", "stop gps, current gps holder count : " + this.mGPSHolderCount);
            if (this.mIsGpsOpened) {
                if (SportsDataManagerService.this.mSensorHubManagerWrapper != null) {
                    SportsDataManagerService.this.mSensorHubManagerWrapper.stopBunchModeGPS();
                }
                this.mIsGpsOpened = false;
            }
        }

        public boolean pauseSport() {
            Debug.m3d("SportsDataManagerService", "pause sport|delegate : " + this.mSportDelegate);
            final ISportDelegate delegate = this.mSportDelegate;
            if (delegate != null) {
                return delegate.pause(new Callback() {
                    protected void doCallback(int resultCode, Object params) {
                        if (Global.DEBUG_LEVEL_3) {
                            Debug.m3d("SportsDataManagerService", "pause callback. result(0 is success) : " + resultCode);
                        }
                        if (resultCode == 0) {
                            SportDataManagerServiceBinder.this.mServiceStatus = 2;
                            if (delegate != null) {
                                SportsDataManagerService.this.mWidgetClient.sendSportPaused(delegate.getSportOptions().getSportType());
                            } else {
                                Debug.m6w("SportsDataManagerService", "send sport paused failed because delegate is null");
                            }
                            SportDataManagerServiceBinder.this.notifyEvent(3, new Parcelable[0]);
                            return;
                        }
                        Debug.m6w("SportsDataManagerService", "pause failed");
                    }
                });
            }
            Debug.m6w("SportsDataManagerService", "sport delegate is null while pauseSport");
            return false;
        }

        public boolean stopSport(boolean force) {
            Debug.m3d("SportsDataManagerService", "stop sport|delegate : " + this.mSportDelegate);
            if (this.mSportDelegate != null) {
                return this.mSportDelegate.stop(new C06385(), force);
            }
            Debug.m6w("SportsDataManagerService", "sport delegate is null while stopSport");
            notifyEvent(4, (Parcelable) null);
            return false;
        }

        public boolean continueSport() {
            Debug.m3d("SportsDataManagerService", "continue sport|delegate : " + this.mSportDelegate);
            final ISportDelegate delegate = this.mSportDelegate;
            if (delegate != null) {
                return delegate.continu(new Callback() {
                    protected void doCallback(int resultCode, Object params) {
                        if (Global.DEBUG_LEVEL_3) {
                            Debug.m3d("SportsDataManagerService", "continue callback. result(0 is success) : " + resultCode);
                        }
                        if (resultCode == 0) {
                            if (delegate != null) {
                                SportsDataManagerService.this.mWidgetClient.sendSportContinued(delegate.getSportOptions().getSportType());
                            } else {
                                Debug.m6w("SportsDataManagerService", "send sport started failed because delegate is null");
                            }
                            SportDataManagerServiceBinder.this.notifyEvent(5, new Parcelable[0]);
                            return;
                        }
                        Debug.m6w("SportsDataManagerService", "continue sport failed");
                    }
                });
            }
            Debug.m6w("SportsDataManagerService", "sport delegate is null while continueSport");
            return false;
        }

        public synchronized void addAutoLapsInfo(RunningInfoPerLap info) {
            if (this.mSportDelegate == null) {
                Debug.m6w("SportsDataManagerService", "delegate is null, sport may not started. set last lap info failed");
            } else {
                this.mSportDelegate.addAutoLapsInfo(info);
            }
        }

        public synchronized List<RunningInfoPerLap> getAutoLapsInfo() {
            List<RunningInfoPerLap> list;
            if (this.mSportDelegate == null) {
                Debug.m6w("SportsDataManagerService", "delegate is null, sport may not started. get last lap info failed");
                list = null;
            } else {
                list = this.mSportDelegate.getAutoLapsInfo();
            }
            return list;
        }

        public synchronized int getCurrentSportType() {
            int i;
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "get current sport type");
            }
            if (this.mSportDelegate == null) {
                Debug.m6w("SportsDataManagerService", "delegate is null, sport may not started.");
                i = -1;
            } else {
                i = this.mSportDelegate.getCurSportType();
            }
            return i;
        }

        public OutdoorSportSnapshot getLastSportSnapshot() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "getLastSportSnapshot");
            }
            if (this.mSportDelegate != null) {
                return (OutdoorSportSnapshot) this.mSportDelegate.getRecentSportStatus();
            }
            Debug.m6w("SportsDataManagerService", "delegate is null, sport may not started.");
            return null;
        }

        public void notifyNewOutdoorSportStatus(OutdoorSportSnapshot status) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "notifyNewOutdoorSportStatus|status:" + status);
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", "call back not exist while notify sport status : " + status);
            } else {
                callback.newOutdoorSportData(status);
            }
        }

        public void setSportDataCallback(ISportDataManagerCallback callback) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "set sport data call back");
            }
            this.mSportDataCallback = callback;
        }

        public void setCurrentCaldenceValue(int caldenceValue) {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.setCurrnetCaldengceValue(caldenceValue);
            }
        }

        public void saveCadenceDetailList(List<CyclingDetail> list) {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.saveCadenceDetailList(list);
            }
        }

        public void addCadenceSummaryData(CyclingDataSummary dataSummary) {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.addCadenceSummaryData(dataSummary);
            }
        }

        public void notifyEvent(int eventType, Parcelable... params) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "notifyEvent|eventType:" + eventType);
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", "call back not exist while notifyEvent|event type : " + eventType);
            } else {
                callback.doEvent(eventType, params);
            }
        }

        public void onSportStatusUpdate(ISportDelegate delegate, SportSnapshot status) {
            notifyNewOutdoorSportStatus((OutdoorSportSnapshot) status);
        }

        public void onSportAutoPaused() {
            Debug.m6w("SportsDataManagerService", "onSportAutoPaused");
            this.mServiceStatus = 2;
            if (this.mSportDelegate != null) {
                SportsDataManagerService.this.mWidgetClient.sendSportPaused(this.mSportDelegate.getSportOptions().getSportType());
            } else {
                Debug.m6w("SportsDataManagerService", "send sport paused failed because delegate is null");
            }
            notifyEvent(7, new Parcelable[0]);
        }

        public void onSportAutoResume() {
            this.mServiceStatus = 1;
            if (this.mSportDelegate != null) {
                SportsDataManagerService.this.mWidgetClient.sendSportContinued(this.mSportDelegate.getSportOptions().getSportType());
            } else {
                Debug.m6w("SportsDataManagerService", "send sport paused failed because delegate is null");
            }
            notifyEvent(8, new Parcelable[0]);
        }

        public void onMillUpdate(int millSec) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "notifyNewOutdoorSportMillSecUpdate|millSec:" + millSec);
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", "call back not exist while notify sport mill sec");
            } else {
                callback.newOutdoorMillSecUpdate(millSec);
            }
        }

        public void onStateUpdateErr() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "onStateUpdateErr");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", "call back not exist while notify sport mill sec");
            } else {
                callback.changeStateErr();
            }
        }

        public void onRouteRunTrackOffset(int routeStatus) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "　onRouteRunTrackOffset  ");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", " route run track offset ");
            } else {
                callback.remindUserRouteOffset(routeStatus);
            }
        }

        public void onManualOrAutoLapStart(boolean manual) {
            notifyEvent(manual ? 11 : 12, new Parcelable[0]);
        }

        public void onSingleTrainUnitCompleted() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "　onSingleTrainUnitCompleted  ");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", " callback is null");
            } else {
                callback.doSingleTrainUnitCompleted();
            }
        }

        public void onSingleTrainUnitNearlyCompleted() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "　onSingleTrainUnitNearlyCompleted  ");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", " callback is null");
            } else {
                callback.doSingleTrainUnitNearlyCompleted();
            }
        }

        public void doNextTrainUnitStart() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "　doNextTrainUnitStart  ");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", " callback is null");
            } else {
                callback.doNextTrainUnitStart();
            }
        }

        public void doWholeTrainUnitCompleted() {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportsDataManagerService", "　doWholeTrainUnitCompleted  ");
            }
            ISportDataManagerCallback callback = this.mSportDataCallback;
            if (callback == null) {
                Debug.m6w("SportsDataManagerService", " callback is null");
            } else {
                callback.doWholeTrainUnitCompleted();
            }
        }

        public SportSummary getSportSummry() {
            if (this.mSportDelegate == null) {
                return null;
            }
            return this.mSportDelegate.getSportSummary();
        }

        public List<HeartRate> getHeartRatesInfo() {
            if (this.mSportDelegate == null) {
                return null;
            }
            return this.mSportDelegate.getHeartRatesInfo();
        }

        public void doRecordOneLap() {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.doRecordOneLap();
            }
        }

        public CurLapInfo getCurLapInfo() {
            if (this.mSportDelegate == null) {
                return null;
            }
            return this.mSportDelegate.getCurLapInfo();
        }

        public boolean isValidSportWhenEndSport() {
            if (this.mSportDelegate == null) {
                return false;
            }
            return this.mSportDelegate.isValidSportWhenEndSport();
        }

        public void clearup() {
            this.mSportDelegate = null;
        }

        public long getCurrentTimeMillisOnRuntime() {
            if (this.mSportDelegate == null) {
                return -1;
            }
            return this.mSportDelegate.getCurrentTimeMillisOnRuntime();
        }

        public void setIsGiveupRecord(boolean isGiveup) {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.setIsGiveupRecord(isGiveup);
            }
        }

        public boolean isPlayVoice() {
            if (this.mSportDelegate != null) {
                return this.mSportDelegate.isPlayVoice();
            }
            return false;
        }

        public void addTrainProgram(TrainProgram trainProgram) {
            this.mTrainProgram = trainProgram;
        }

        public boolean isCustomTrainMode() {
            if (this.mTrainProgram != null) {
                return true;
            }
            return false;
        }

        public TrainUnit getCurrentTrainUnit() {
            LogUtil.m9i(Global.DEBUG_LEVEL_3, "SportsDataManagerService", "getCurrentTrainUnit");
            if (this.mSportDelegate != null) {
                return this.mSportDelegate.getCurrentTrainUnit();
            }
            return null;
        }

        public void notifyStartNextCustomTrainUnit() {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.notifyStartNextCustomTrainUnit();
            }
        }

        public boolean isIntervalTrain() {
            return this.intervalType == 1;
        }

        public void notifyHalfSecStart() {
            if (this.mSportDelegate != null) {
                this.mSportDelegate.notifyHalfSecStart();
            }
        }
    }

    public void onCreate() {
        this.mWidgetClient = SportApplication.getInstance().mWidgetClient;
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportsDataManagerService", "onCreate");
        }
        super.onCreate();
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportsDataManagerService", "onCreate finish");
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 2;
    }

    public void onDestroy() {
        Debug.m3d("SportsDataManagerService", "onDestroy");
        super.onDestroy();
        if (this.mBinder != null) {
            this.mBinder.clearup();
        }
        this.mBinder = null;
    }

    public IBinder onBind(Intent intent) {
        Debug.m3d("SportsDataManagerService", "onBind:" + intent + ":" + this);
        return this.mBinder;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Debug.m3d("SportsDataManagerService", "onRebind : " + this);
    }

    public boolean onUnbind(Intent intent) {
        Debug.m3d("SportsDataManagerService", "onUnbind:" + intent + ":" + this);
        return false;
    }

    public void onLowMemory() {
        super.onLowMemory();
        Debug.m6w("SportsDataManagerService", "on low memory. VM Heap:" + Runtime.getRuntime().totalMemory() + ",Allocated:" + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) + ",Native allocated:" + android.os.Debug.getNativeHeapAllocatedSize());
    }
}
