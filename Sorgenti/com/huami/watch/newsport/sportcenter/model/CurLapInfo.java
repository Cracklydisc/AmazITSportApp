package com.huami.watch.newsport.sportcenter.model;

public class CurLapInfo {
    public int mFlag = 0;
    public int mNumber = 0;
    public long mRunningTime = -1;
    public long mTotalTime = -1;

    public int getFlag() {
        return this.mFlag;
    }

    public void setFlag(int flag) {
        this.mFlag = flag;
    }

    public int getNumber() {
        return this.mNumber;
    }

    public void setNumber(int number) {
        this.mNumber = number;
    }

    public long getRunningTime() {
        return this.mRunningTime;
    }

    public void setRunningTime(long runningTime) {
        this.mRunningTime = runningTime;
    }

    public long getTotalTime() {
        return this.mTotalTime;
    }

    public void setTotalTime(long totalTime) {
        this.mTotalTime = totalTime;
    }

    public String toString() {
        return "CurLapInfo{mNumber=" + this.mNumber + ", mRunningTime=" + this.mRunningTime + ", mTotalTime=" + this.mTotalTime + ", mFlag=" + this.mFlag + '}';
    }
}
