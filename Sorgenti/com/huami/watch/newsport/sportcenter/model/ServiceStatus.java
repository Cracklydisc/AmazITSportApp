package com.huami.watch.newsport.sportcenter.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ServiceStatus implements Parcelable {
    public static final Creator<ServiceStatus> CREATOR = new C06421();
    private long mRealSportTimestamp = -1;
    private int mServerStatus = 0;
    private int mSportType = -1;

    static class C06421 implements Creator<ServiceStatus> {
        C06421() {
        }

        public ServiceStatus createFromParcel(Parcel source) {
            ServiceStatus status = new ServiceStatus();
            status.mRealSportTimestamp = source.readLong();
            status.mServerStatus = source.readInt();
            status.mSportType = source.readInt();
            return status;
        }

        public ServiceStatus[] newArray(int size) {
            return new ServiceStatus[size];
        }
    }

    public static class ServerStatus {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mRealSportTimestamp);
        dest.writeInt(this.mServerStatus);
        dest.writeInt(this.mSportType);
    }
}
