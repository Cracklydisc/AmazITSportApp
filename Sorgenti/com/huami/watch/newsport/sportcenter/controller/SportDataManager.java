package com.huami.watch.newsport.sportcenter.controller;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Parcelable;
import android.view.KeyEvent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.sportcenter.SportsDataManagerService;
import com.huami.watch.newsport.sportcenter.SportsDataManagerService.SportDataManagerServiceBinder;
import com.huami.watch.newsport.sportcenter.action.ISportDataManagerCallback;
import com.huami.watch.newsport.sportcenter.model.CurLapInfo;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import java.util.LinkedList;
import java.util.List;

public class SportDataManager implements ISportDataManagerCallback {
    private static SportDataManager gInstance = null;
    private static final Class mTargetService = SportsDataManagerService.class;
    private DeathRecipient mDeathRecipient = new C06401();
    private IDialogKeyEventListener mDialogEventListener = null;
    private boolean mIsBinding = false;
    private boolean mIsForeground = true;
    private boolean mIsStopped = false;
    private SportDataManagerServiceBinder mService = null;
    private ServiceConnection mSportConnection = new C06412();
    private ISportConnectionListener mSportConnectionListener = null;
    private List<ISportDataListener> mSportDataListeners = new LinkedList();
    private int mSportStatus = 0;
    private int mSportType = -1;
    private long mTrackId = -1;

    class C06401 implements DeathRecipient {
        C06401() {
        }

        public void binderDied() {
            Debug.m5i("SportDataManager", "binder died");
            if (SportDataManager.this.mSportConnection != null) {
                SportDataManager.this.unbindService(Global.getApplicationContext());
                if (SportDataManager.this.mService != null) {
                    SportDataManager.this.mService.unlinkToDeath(this, 0);
                }
                SportDataManager.this.mService = null;
                SportDataManager.this.mIsBinding = false;
                SportDataManager.this.bindService(Global.getApplicationContext());
            }
        }
    }

    class C06412 implements ServiceConnection {
        C06412() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            Debug.m3d("SportDataManager", "onServiceConnected:" + this);
            SportDataManager.this.mService = (SportDataManagerServiceBinder) service;
            if (SportDataManager.this.mService == null) {
                Debug.m6w("SportDataManager", "service is null");
                throw new IllegalArgumentException("service should not be null");
            }
            SportDataManager.this.mService.setSportDataCallback(SportDataManager.this);
            if (SportDataManager.this.mSportConnectionListener == null) {
                Debug.m6w("SportDataManager", "sport connection listener is null while connect");
                return;
            }
            if (SportDataManager.this.mIsForeground) {
                SportDataManager.this.mService.notifySportForeground();
            } else {
                SportDataManager.this.mService.notifySportBackground();
            }
            SportDataManager.this.mSportConnectionListener.onServiceConnected();
            SportDataManager.this.mIsBinding = false;
            SportDataManager.this.mService.linkToDeath(SportDataManager.this.mDeathRecipient, 0);
        }

        public void onServiceDisconnected(ComponentName name) {
            Debug.m3d("SportDataManager", "onServiceDisconnected:" + this);
            SportDataManager.this.mService.setSportDataCallback(null);
            if (SportDataManager.this.mSportConnectionListener == null) {
                Debug.m6w("SportDataManager", "sport connection listener is null while disconnect");
            } else {
                SportDataManager.this.mSportConnectionListener.onServiceDisconnected();
            }
            SportDataManager.this.mService = null;
        }
    }

    public interface ISportConnectionListener {
        void onServiceConnected();

        void onServiceDisconnected();
    }

    public interface ISportDataListener {
        void doChildSportStopped(SportSnapshot sportSnapshot);

        void doManualOrAutoLapStart(boolean z);

        void doNextTrainUnitStart();

        void doRemindUserRouteOffset(int i);

        void doSingleTrainUnitCompleted();

        void doSingleTrainUnitNearlyCompleted();

        void doSportAutoPaused();

        void doSportAutoResumed();

        void doSportContinued();

        void doSportDataReady(SportSnapshot sportSnapshot);

        void doSportMillSecUpdate(int i);

        void doSportPaused();

        void doSportStarted();

        void doSportStateErr();

        void doSportStopped(SportSnapshot sportSnapshot);

        void doWholeTrainUnitCompleted();
    }

    private SportDataManager() {
    }

    public static synchronized SportDataManager getInstance() {
        SportDataManager sportDataManager;
        synchronized (SportDataManager.class) {
            if (gInstance == null) {
                gInstance = new SportDataManager();
            }
            sportDataManager = gInstance;
        }
        return sportDataManager;
    }

    public void setSportConnectionListener(ISportConnectionListener sportConnectionListener) {
        this.mSportConnectionListener = sportConnectionListener;
    }

    public void doEvent(int eventType, Parcelable... params) {
        Debug.m5i("SportDataManager", "doEvent : " + eventType);
        switch (eventType) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 11:
            case 12:
                doSportStatusChanged(eventType, params);
                return;
            default:
                return;
        }
    }

    public void addAutoLapsInfo(RunningInfoPerLap perLap) {
        if (this.mService != null) {
            this.mService.addAutoLapsInfo(perLap);
        }
    }

    public long getSportTrackId() {
        return this.mTrackId;
    }

    public void newOutdoorSportData(OutdoorSportSnapshot status) {
        doNewSportStatus(status.getSportType(), status);
    }

    private void doNewSportStatus(int sportType, OutdoorSportSnapshot status) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doNewSportStatus|getSportType:" + sportType + ",status:" + status);
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
        } else if (status == null) {
            Debug.m5i("SportDataManager", "status is null");
        } else {
            for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
                l.doSportDataReady(status);
            }
        }
    }

    public void newOutdoorMillSecUpdate(int millSec) {
        doNewSportMillUpdate(millSec);
    }

    private void doNewSportMillUpdate(int millSec) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doNewSportMillUpdate|MillSec:" + millSec);
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doSportMillSecUpdate(millSec);
        }
    }

    public void changeStateErr() {
        dochangeStateErr();
    }

    private void dochangeStateErr() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "dochangeStateErr");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doSportStateErr();
        }
    }

    public void remindUserRouteOffset(int routeStatus) {
        doRemindUserRouteOffset(routeStatus);
    }

    public void doSingleTrainUnitNearlyCompleted() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doSingleTrainUnitCompleted");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doSingleTrainUnitNearlyCompleted();
        }
    }

    public void doSingleTrainUnitCompleted() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doSingleTrainUnitCompleted");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doSingleTrainUnitCompleted();
        }
    }

    public void doNextTrainUnitStart() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doNextTrainUnitStart");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doNextTrainUnitStart();
        }
    }

    public void doWholeTrainUnitCompleted() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "doNextTrainUnitStart");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doWholeTrainUnitCompleted();
        }
    }

    private void doRemindUserRouteOffset(int routeStatus) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportDataManager", "dochangeStateErr");
        }
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "sport data listener is empty");
            return;
        }
        for (ISportDataListener l : new LinkedList(this.mSportDataListeners)) {
            l.doRemindUserRouteOffset(routeStatus);
        }
    }

    public int getSportStatus() {
        return this.mSportStatus;
    }

    private void doSportStatusChanged(int eventType, Parcelable... params) {
        SportSnapshot status = null;
        if (this.mSportDataListeners.isEmpty()) {
            Debug.m5i("SportDataManager", "mSportDataListener is empty while do sport status changed. event type " + eventType);
        }
        List<ISportDataListener> tmp = new LinkedList(this.mSportDataListeners);
        switch (eventType) {
            case 2:
                this.mSportStatus = 1;
                for (ISportDataListener l : tmp) {
                    l.doSportStarted();
                }
                return;
            case 3:
                this.mSportStatus = 2;
                for (ISportDataListener l2 : tmp) {
                    l2.doSportPaused();
                }
                return;
            case 4:
                this.mSportStatus = 0;
                this.mSportType = -1;
                if (!(params == null || params.length == 0)) {
                    status = (SportSnapshot) params[0];
                }
                for (ISportDataListener l22 : tmp) {
                    l22.doSportStopped(status);
                }
                return;
            case 5:
                this.mSportStatus = 1;
                for (ISportDataListener l222 : tmp) {
                    l222.doSportContinued();
                }
                return;
            case 6:
                this.mSportStatus = 0;
                SportSnapshot childSportStatus = (params == null || params.length == 0) ? null : (SportSnapshot) params[0];
                for (ISportDataListener l2222 : tmp) {
                    l2222.doChildSportStopped(childSportStatus);
                }
                return;
            case 7:
                this.mSportStatus = 2;
                if (this.mIsForeground) {
                    for (ISportDataListener l22222 : tmp) {
                        l22222.doSportAutoPaused();
                    }
                    return;
                }
                Debug.m5i("SportDataManager", "auto pause is refused to refresh view because of background");
                return;
            case 8:
                this.mSportStatus = 1;
                if (this.mIsForeground) {
                    for (ISportDataListener l222222 : tmp) {
                        l222222.doSportAutoResumed();
                    }
                    return;
                }
                Debug.m5i("SportDataManager", "auto resume is refused to refresh view because of background");
                return;
            case 11:
                for (ISportDataListener l2222222 : tmp) {
                    l2222222.doManualOrAutoLapStart(true);
                }
                return;
            case 12:
                for (ISportDataListener l22222222 : tmp) {
                    l22222222.doManualOrAutoLapStart(false);
                }
                return;
            default:
                return;
        }
    }

    public void setIsTrainingMode(boolean isTrainingMode) {
        Debug.m3d("SportDataManager", "setIsTrainingMode, isTrainingMode: " + isTrainingMode);
        if (this.mService != null) {
            this.mService.setIsTrainingMode(isTrainingMode);
        }
    }

    public void setIsInterValTrainMode(int intervalModel) {
        Debug.m3d("SportDataManager", "setIsTrainingMode, isTrainingMode: " + intervalModel);
        if (this.mService != null) {
            this.mService.setIsInterValTrainMode(intervalModel);
        }
    }

    public synchronized void setGpsStatus(int gpsStatus) {
        Debug.m3d("SportDataManager", "setGpsStatus : " + gpsStatus + ", service: " + this.mService);
        if (this.mService != null) {
            this.mService.setGpsStatus(gpsStatus);
        }
    }

    public int getNextChildSportType() {
        if (!SportType.isSportTypeValid(this.mSportType)) {
            return -1;
        }
        if (SportType.isMixedSport(this.mSportType)) {
            return this.mService.getNextChildSportType();
        }
        return this.mSportType;
    }

    public synchronized long startSport(int sportType) {
        Debug.m3d("SportDataManager", "start sport|sport type : " + sportType);
        this.mIsStopped = false;
        this.mSportType = sportType;
        int nextSportType = sportType;
        if (SportType.isMixedSport(this.mSportType)) {
            nextSportType = ((Integer) ((MixedBaseConfig) DataManager.getInstance().getSportConfig(Global.getApplicationContext(), sportType)).getChildSports().get(0)).intValue();
        }
        if (SportType.isSportTypeNeedGps(nextSportType)) {
            this.mService.startGPS();
        }
        this.mTrackId = this.mService.startSport(sportType);
        if (this.mTrackId >= 0) {
            this.mSportStatus = 3;
        }
        return this.mTrackId;
    }

    public void startNextChildSport() {
        this.mIsStopped = false;
        int nextSportType = this.mService.getNextChildSportType();
        if (SportType.isSportTypeValid(nextSportType)) {
            if (SportType.isSportTypeNeedGps(nextSportType)) {
                this.mService.startGPS();
            }
            boolean isSuccess = this.mService.startChildSport();
            Debug.m5i("SportDataManager", "startNextChildSport, isSuccess:" + isSuccess);
            if (isSuccess) {
                this.mSportStatus = 3;
                return;
            }
            return;
        }
        throw new IllegalArgumentException("startNextChildSport, err sport type:" + nextSportType);
    }

    public int getLastSportType() {
        return this.mSportType;
    }

    public List<RunningInfoPerLap> getAutoLapsInfo() {
        if (this.mService != null) {
            return this.mService.getAutoLapsInfo();
        }
        Debug.m5i("SportDataManager", "getLastLapInfo, service is null");
        return null;
    }

    public synchronized boolean pauseSport() {
        boolean result;
        Debug.m3d("SportDataManager", "pause sport");
        result = this.mService.pauseSport();
        if (result) {
            this.mSportStatus = 4;
        }
        return result;
    }

    public void notifySportForeground() {
        Debug.m3d("SportDataManager", "notifySportForeground");
        this.mIsForeground = true;
        if (this.mService != null) {
            this.mService.notifySportForeground();
        }
    }

    public void notifySportBackground() {
        Debug.m3d("SportDataManager", "notifySportBackground");
        this.mIsForeground = false;
        if (this.mService != null) {
            this.mService.notifySportBackground();
        }
    }

    public int getAutoPauseStatusInScreenOff() {
        if (this.mService != null) {
            return this.mService.getAutoPauseStatusInScreenOff();
        }
        return 0;
    }

    public synchronized boolean stopSport(boolean forcely) {
        boolean z = true;
        synchronized (this) {
            Debug.m3d("SportDataManager", "stop sport : forcely = " + forcely);
            if (!this.mIsStopped) {
                this.mIsStopped = true;
                if (SportType.isMixedSport(this.mSportType) && this.mService.hasNextChildSport() && !forcely) {
                    Debug.m5i("SportDataManager", "stop child sport");
                    z = this.mService.stopChildSport();
                } else {
                    Debug.m5i("SportDataManager", "stop the whole sport");
                    z = this.mService.stopSport(forcely);
                }
                if (z) {
                    this.mService.stopGPS();
                    this.mSportStatus = 6;
                }
            }
        }
        return z;
    }

    public synchronized boolean continueSport() {
        boolean result;
        Debug.m3d("SportDataManager", "continue sport");
        result = this.mService.continueSport();
        if (result) {
            this.mSportStatus = 5;
        }
        return result;
    }

    public void clearSportDataListener() {
        Debug.m3d("SportDataManager", "clearSportDataListener");
        if (this.mSportDataListeners != null) {
            this.mSportDataListeners.clear();
        }
    }

    public void registerSportDataListener(ISportDataListener listener) {
        Debug.m3d("SportDataManager", "register sport data listener " + listener);
        if (!this.mSportDataListeners.contains(listener)) {
            this.mSportDataListeners.add(listener);
        }
    }

    public void unregisterSportDataListener(ISportDataListener listener) {
        Debug.m3d("SportDataManager", "unregister sport data listener " + listener);
        if (this.mSportDataListeners.contains(listener)) {
            this.mSportDataListeners.remove(listener);
        }
    }

    public SportSnapshot getSportSnapshot() {
        if (this.mService == null) {
            return null;
        }
        return this.mService.getLastSportSnapshot();
    }

    public void bindService(Context context) {
        Debug.m3d("SportDataManager", "bindService:" + this.mSportConnection);
        this.mIsBinding = true;
        context.bindService(new Intent(context, mTargetService), this.mSportConnection, 1);
    }

    public void unbindService(Context context) {
        Debug.m3d("SportDataManager", "unbindService:" + this.mSportConnection);
        try {
            this.mService.unlinkToDeath(this.mDeathRecipient, 0);
            this.mService.setSportDataCallback(null);
            context.unbindService(this.mSportConnection);
            this.mService = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startService(Context context) {
        Debug.m3d("SportDataManager", "start service :" + context);
        context.startService(new Intent(context, mTargetService));
    }

    public void stopService(Context context) {
        Debug.m3d("SportDataManager", "stop service :" + context);
        context.stopService(new Intent(context, mTargetService));
    }

    public SportSummary getSportSummry() {
        Debug.m3d("SportDataManager", "getSportSummry");
        if (this.mService == null) {
            return null;
        }
        return this.mService.getSportSummry();
    }

    public void shutDown() {
        Debug.m3d("SportDataManager", "shut down");
        if (this.mService != null) {
            this.mService.shutDown();
        }
    }

    public List<HeartRate> getHeartRatesInfo() {
        if (this.mService == null) {
            return null;
        }
        return this.mService.getHeartRatesInfo();
    }

    public void doRecordOneLap() {
        if (this.mService != null) {
            this.mService.doRecordOneLap();
        }
    }

    public CurLapInfo getCurLapInfo() {
        if (this.mService == null) {
            return null;
        }
        return this.mService.getCurLapInfo();
    }

    public void setDialogKeyEventListener(IDialogKeyEventListener keyEventListener) {
        this.mDialogEventListener = keyEventListener;
    }

    public void notifyDialogKeyEvent(KeyEvent event) {
        if (this.mDialogEventListener != null) {
            this.mDialogEventListener.dispatchDialogKeyEvent(event);
        }
    }

    public void notifyHalfSecStart() {
        if (this.mService != null) {
            this.mService.notifyHalfSecStart();
        }
    }

    public void setCurrnetCaldence(int currnetCaldenceValue) {
        if (this.mService != null) {
            this.mService.setCurrentCaldenceValue(currnetCaldenceValue);
        }
    }

    public void saveCadenceDetailList(List<CyclingDetail> list) {
        if (this.mService != null) {
            this.mService.saveCadenceDetailList(list);
        }
    }

    public void addCadenceSummaryData(CyclingDataSummary dataSummary) {
        if (this.mService != null) {
            this.mService.addCadenceSummaryData(dataSummary);
        }
    }

    public boolean isValidSportWhenEndSport() {
        if (this.mService != null) {
            return this.mService.isValidSportWhenEndSport();
        }
        return false;
    }

    public int getCurentSportType() {
        if (this.mService != null) {
            return this.mService.getCurrentSportType();
        }
        return -1;
    }

    public long getCurrentTimeMillisOnRuntime() {
        if (this.mService == null) {
            return -1;
        }
        return this.mService.getCurrentTimeMillisOnRuntime();
    }

    public void setIsGiveupRecord(boolean isGiveup) {
        if (this.mService != null) {
            this.mService.setIsGiveupRecord(isGiveup);
        }
    }

    public boolean isPlayVoice() {
        if (this.mService != null) {
            return this.mService.isPlayVoice();
        }
        return false;
    }

    public void addTrainProgram(TrainProgram trainProgram) {
        if (this.mService != null) {
            this.mService.addTrainProgram(trainProgram);
        }
    }

    public boolean isCustomTrainMode() {
        if (this.mService != null) {
            return this.mService.isCustomTrainMode();
        }
        return false;
    }

    public TrainUnit getCurrentTrainUnit() {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, "SportDataManager", "getCurrentTrainUnit");
        if (this.mService != null) {
            return this.mService.getCurrentTrainUnit();
        }
        return null;
    }

    public void notifyStartNextCustomTrainUnit() {
        if (this.mService != null) {
            this.mService.notifyStartNextCustomTrainUnit();
        }
    }

    public boolean isIntervalTrain() {
        if (this.mService != null) {
            return this.mService.isIntervalTrain();
        }
        return false;
    }
}
