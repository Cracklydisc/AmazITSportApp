package com.huami.watch.newsport.medal;

import android.content.Context;
import com.huami.watch.common.log.Debug;

public class ContinuityMedal extends Medal {
    private MedalContainer mContainer = null;
    private int mContinuetyDay = 0;

    public ContinuityMedal(int id, int continuetyDay) {
        super(id);
        this.mContinuetyDay = continuetyDay;
    }

    boolean check(Context context, long trackId) {
        if (this.mIsActive) {
            return false;
        }
        this.mContainer = MedalContainer.getInstance(context);
        int count = this.mContainer.getContinuousDay().getContinusDays();
        Debug.m5i("ContinuityMedal", "get continuity medal because count is " + count + ", target day count is " + this.mContinuetyDay);
        if (count >= this.mContinuetyDay) {
            return true;
        }
        return false;
    }
}
