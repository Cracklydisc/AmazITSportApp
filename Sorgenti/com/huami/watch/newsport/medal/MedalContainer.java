package com.huami.watch.newsport.medal;

import android.content.Context;
import android.util.SparseBooleanArray;
import com.huami.watch.common.log.Debug;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.newsport.medal.impl.First10KmMedal;
import com.huami.watch.newsport.medal.impl.First5KmMedal;
import com.huami.watch.newsport.medal.impl.FirstSportMedal;
import com.huami.watch.newsport.medal.impl.HalfMarathonMedal;
import com.huami.watch.newsport.medal.impl.MarathonMedal;
import com.huami.watch.newsport.medal.impl.SevenContinutyMedal;
import com.huami.watch.newsport.medal.impl.ThirtyConitnutyMedal;
import com.huami.watch.newsport.medal.impl.Total1000KmMedal;
import com.huami.watch.newsport.medal.impl.Total300KmMedal;
import com.huami.watch.newsport.medal.impl.Total50KmMedal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class MedalContainer {
    private static MedalContainer gInstance = null;
    private SparseBooleanArray mActiveMedalArray = null;
    private ContinuousDays mContinuousDay = null;
    private List<Medal> mMedals = null;

    private MedalContainer(Context context) {
    }

    public static synchronized MedalContainer getInstance(Context context) {
        MedalContainer medalContainer;
        synchronized (MedalContainer.class) {
            if (gInstance == null) {
                gInstance = new MedalContainer(context);
            }
            medalContainer = gInstance;
        }
        return medalContainer;
    }

    public synchronized ContinuousDays getContinuousDay() {
        if (this.mContinuousDay == null) {
            this.mContinuousDay = new ContinuousDays();
        }
        return this.mContinuousDay;
    }

    public final List<Medal> getMedals(Context context) {
        if (this.mMedals == null || this.mMedals.isEmpty()) {
            this.mMedals = new LinkedList();
            this.mMedals.add(new First5KmMedal());
            this.mMedals.add(new First10KmMedal());
            this.mMedals.add(new FirstSportMedal());
            this.mMedals.add(new Total50KmMedal());
            this.mMedals.add(new Total300KmMedal());
            this.mMedals.add(new Total1000KmMedal());
            this.mMedals.add(new HalfMarathonMedal());
            this.mMedals.add(new MarathonMedal());
            this.mMedals.add(new SevenContinutyMedal());
            this.mMedals.add(new ThirtyConitnutyMedal());
        }
        if (this.mActiveMedalArray == null) {
            int[] activeMedalIds = getActiveMedalIds(context);
            this.mActiveMedalArray = new SparseBooleanArray();
            for (int medalId : activeMedalIds) {
                this.mActiveMedalArray.put(medalId, true);
            }
        }
        for (Medal medal : this.mMedals) {
            medal.setIsActive(this.mActiveMedalArray.get(medal.getMedalId(), false));
            medal.initIconId();
        }
        return this.mMedals;
    }

    private int[] getActiveMedalIds(Context context) {
        String medalIds = WatchSettings.get(context.getContentResolver(), "huami.watch.sport.medal");
        Debug.m3d("MedalContainer", "get active medals " + medalIds);
        if (medalIds == null || medalIds.isEmpty()) {
            return new int[0];
        }
        String[] medalList = medalIds.split(";");
        int length = medalList.length;
        String[] medalStrs = medalList[0].split(",");
        int[] result = new int[medalStrs.length];
        for (int i = 0; i < medalStrs.length; i++) {
            String medalStr = medalStrs[i];
            int medalId = -1;
            try {
                medalId = Integer.parseInt(medalStr);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            if (medalId == -1) {
                Debug.m6w("MedalContainer", "parse medal failed " + medalStr + ", medal list : " + medalIds);
            }
            result[i] = medalId;
        }
        if (length <= 1) {
            return result;
        }
        String[] reachDays = medalList[1].split(",");
        try {
            getContinuousDay().setLastSportTime(Long.parseLong(reachDays[0]));
            getContinuousDay().setContinusDays(Integer.parseInt(reachDays[1]));
            return result;
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return result;
        }
    }

    private void putActiveMedals(Context context, int[] activeMedalId) {
        Debug.m3d("MedalContainer", "put active medals " + Arrays.toString(activeMedalId));
        if (activeMedalId == null || activeMedalId.length == 0) {
            Debug.m6w("MedalContainer", "put active medals is null or empty " + activeMedalId);
            WatchSettings.put(context.getContentResolver(), "huami.watch.sport.medal", "");
            return;
        }
        StringBuilder result = new StringBuilder();
        result.append(activeMedalId[0]);
        for (int i = 1; i < activeMedalId.length; i++) {
            result.append(",").append(activeMedalId[i]);
        }
        result.append(";").append(getContinuousDay().getLastSportTime()).append(",").append(getContinuousDay().getContinusDays());
        WatchSettings.put(context.getContentResolver(), "huami.watch.sport.medal", result.toString());
    }

    public void makeActive(Context context, int medalId) {
        Debug.m3d("MedalContainer", "make medal " + medalId + " active.");
        if (this.mActiveMedalArray == null) {
            Debug.m6w("MedalContainer", "medal active record is null.");
            this.mActiveMedalArray = new SparseBooleanArray();
            for (int id : getActiveMedalIds(context)) {
                this.mActiveMedalArray.put(id, true);
            }
        }
        this.mActiveMedalArray.put(medalId, true);
        new Medal(medalId).setIsActive(true);
        int[] activeMedalIds = new int[this.mActiveMedalArray.size()];
        for (int i = 0; i < this.mActiveMedalArray.size(); i++) {
            activeMedalIds[i] = this.mActiveMedalArray.keyAt(i);
        }
        putActiveMedals(context, activeMedalIds);
    }

    public synchronized void checkContinuousSports(Context context, long trackId) {
        int dayDiff = (int) ((getFormatDate(trackId) - getFormatDate(getContinuousDay().getLastSportTime())) / 86400000);
        int count = getContinuousDay().getContinusDays();
        if (dayDiff == 1) {
            count++;
        } else if (dayDiff > 1 || (dayDiff < 1 && count == 0)) {
            count = 1;
        }
        Debug.m5i("MedalContainer", "checkContinuousSports, daydiff: " + dayDiff + ", count: " + count + ", curDate:" + trackId + ", lastDate:" + getContinuousDay().getLastSportTime());
        updateSportDays(context, trackId, count);
    }

    private long getFormatDate(long sportTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sportTime);
        calendar.set(11, 0);
        calendar.set(13, 0);
        calendar.set(12, 0);
        calendar.set(14, 0);
        return calendar.getTimeInMillis();
    }

    private void updateSportDays(Context context, long sportTime, int continusDay) {
        getContinuousDay().setLastSportTime(sportTime);
        getContinuousDay().setContinusDays(continusDay);
        if (this.mActiveMedalArray == null) {
            Debug.m6w("MedalContainer", "medal active record is null.");
            this.mActiveMedalArray = new SparseBooleanArray();
            for (int id : getActiveMedalIds(context)) {
                this.mActiveMedalArray.put(id, true);
            }
        }
        int[] activeMedalIds = new int[this.mActiveMedalArray.size()];
        for (int i = 0; i < this.mActiveMedalArray.size(); i++) {
            activeMedalIds[i] = this.mActiveMedalArray.keyAt(i);
        }
        putActiveMedals(context, activeMedalIds);
    }
}
