package com.huami.watch.newsport.medal;

import android.content.Context;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportStatistic;

public class TotalTargetDistanceMedal extends Medal {
    private DataManager mDataManager = DataManager.getInstance();
    private float mTotalDistance = 0.0f;

    public TotalTargetDistanceMedal(int id, float totalDistance) {
        super(id);
        this.mTotalDistance = totalDistance;
    }

    boolean check(Context context, long trackId) {
        if (this.mIsActive) {
            return false;
        }
        SportStatistic statistic = this.mDataManager.getSportStatistic(context);
        if (statistic == null) {
            return false;
        }
        Debug.m5i("TotalTargetDistanceMedal", "get target total distance medal because distance is " + statistic.getTotalDistance() + ", target total distance is " + this.mTotalDistance);
        if (((float) statistic.getTotalDistance()) >= this.mTotalDistance) {
            return true;
        }
        return false;
    }
}
