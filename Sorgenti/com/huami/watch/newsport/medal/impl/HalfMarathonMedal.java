package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TargetDistanceSportMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class HalfMarathonMedal extends TargetDistanceSportMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public HalfMarathonMedal() {
        super(20001, 21097.5f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_06_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_06_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_half_marathon;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_06_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_06_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_half_marathon;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_06_mi : C0532R.drawable.sport_medal_06_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_06_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_half_marathon;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_06 : C0532R.drawable.sport_medal_06_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_06_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_half_marathon;
    }
}
