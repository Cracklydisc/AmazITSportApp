package com.huami.watch.newsport.medal.impl;

import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.ContinuityMedal;

public class SevenContinutyMedal extends ContinuityMedal {
    public SevenContinutyMedal() {
        super(40001, 7);
        this.mIconId = C0532R.drawable.sport_medal_04_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_04_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_continuous_7_days_sport;
    }

    public void initIconId() {
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_04 : C0532R.drawable.sport_medal_04_grey;
    }
}
