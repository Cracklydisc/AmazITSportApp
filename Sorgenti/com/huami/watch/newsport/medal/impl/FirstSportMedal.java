package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TargetDistanceSportMedal;

public class FirstSportMedal extends TargetDistanceSportMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public FirstSportMedal() {
        super(10001, 0.0f);
        this.mIconId = C0532R.drawable.sport_medal_01_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_01_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_first_sport;
    }

    public void initIconId() {
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_01 : C0532R.drawable.sport_medal_01_grey;
    }
}
