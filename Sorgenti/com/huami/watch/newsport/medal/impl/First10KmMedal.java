package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TargetDistanceSportMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class First10KmMedal extends TargetDistanceSportMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public First10KmMedal() {
        super(10003, 10000.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_03_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_03_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_one_10_mi;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_03_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_03_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_one_10_km;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_03_mi : C0532R.drawable.sport_medal_03_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_03_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_one_10_mi;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_03 : C0532R.drawable.sport_medal_03_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_03_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_one_10_km;
    }
}
