package com.huami.watch.newsport.medal.impl;

import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.ContinuityMedal;

public class ThirtyConitnutyMedal extends ContinuityMedal {
    public ThirtyConitnutyMedal() {
        super(40002, 30);
        this.mIconId = C0532R.drawable.sport_medal_05_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_05_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_continuous_30_days_sport;
    }

    public void initIconId() {
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_05 : C0532R.drawable.sport_medal_05_grey;
    }
}
