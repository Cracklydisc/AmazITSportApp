package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TotalTargetDistanceMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class Total1000KmMedal extends TotalTargetDistanceMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public Total1000KmMedal() {
        super(30003, 1000000.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_10_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_10_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_1000_mi;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_10_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_10_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_1000_km;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_10_mi : C0532R.drawable.sport_medal_10_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_10_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_1000_mi;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_10 : C0532R.drawable.sport_medal_10_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_10_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_1000_km;
    }
}
