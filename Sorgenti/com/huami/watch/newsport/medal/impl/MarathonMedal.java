package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TargetDistanceSportMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class MarathonMedal extends TargetDistanceSportMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public MarathonMedal() {
        super(20002, 42195.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_07_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_07_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_marathon;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_07_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_07_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_marathon;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_07_mi : C0532R.drawable.sport_medal_07_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_07_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_marathon;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_07 : C0532R.drawable.sport_medal_07_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_07_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_marathon;
    }
}
