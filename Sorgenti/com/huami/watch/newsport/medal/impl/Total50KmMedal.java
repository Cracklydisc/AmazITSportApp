package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TotalTargetDistanceMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class Total50KmMedal extends TotalTargetDistanceMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public Total50KmMedal() {
        super(30001, 50000.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_08_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_08_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_50_mi;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_08_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_08_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_50_km;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_08_mi : C0532R.drawable.sport_medal_08_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_08_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_50_mi;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_08 : C0532R.drawable.sport_medal_08_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_08_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_50_km;
    }
}
