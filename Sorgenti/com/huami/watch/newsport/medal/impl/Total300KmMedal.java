package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TotalTargetDistanceMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class Total300KmMedal extends TotalTargetDistanceMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public Total300KmMedal() {
        super(30002, 300000.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_09_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_09_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_300_mi;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_09_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_09_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_300_km;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_09_mi : C0532R.drawable.sport_medal_09_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_09_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_total_300_mi;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_09 : C0532R.drawable.sport_medal_09_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_09_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_total_300_km;
    }
}
