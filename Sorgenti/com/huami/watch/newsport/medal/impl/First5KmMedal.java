package com.huami.watch.newsport.medal.impl;

import android.os.Parcelable.Creator;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.TargetDistanceSportMedal;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class First5KmMedal extends TargetDistanceSportMedal {
    public static final Creator<Medal> CREATOR = Medal.CREATOR;

    public First5KmMedal() {
        super(10002, 5000.0f);
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = C0532R.drawable.sport_medal_02_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_02_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_one_5_mi;
            return;
        }
        this.mIconId = C0532R.drawable.sport_medal_02_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_02_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_one_5_km;
    }

    public void initIconId() {
        if (UnitConvertUtils.isImperial()) {
            this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_02_mi : C0532R.drawable.sport_medal_02_grey_mi;
            this.mBackgroundId = C0532R.drawable.sport_medal_02_bg;
            this.mDescResId = C0532R.string.sport_medal_desc_one_5_mi;
            return;
        }
        this.mIconId = this.mIsActive ? C0532R.drawable.sport_medal_02 : C0532R.drawable.sport_medal_02_grey;
        this.mBackgroundId = C0532R.drawable.sport_medal_02_bg;
        this.mDescResId = C0532R.string.sport_medal_desc_one_5_km;
    }
}
