package com.huami.watch.newsport.medal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ContinuousDays implements Parcelable {
    public static final Creator<ContinuousDays> CREATOR = new C05841();
    private int mContinusDays = 0;
    private long mLastSportTime = 0;

    static class C05841 implements Creator<ContinuousDays> {
        C05841() {
        }

        public ContinuousDays createFromParcel(Parcel in) {
            ContinuousDays continuousDays = new ContinuousDays();
            continuousDays.setLastSportTime(in.readLong());
            continuousDays.setContinusDays(in.readInt());
            return continuousDays;
        }

        public ContinuousDays[] newArray(int size) {
            return new ContinuousDays[size];
        }
    }

    public void setContinusDays(int continusDays) {
        this.mContinusDays = continusDays;
    }

    public int getContinusDays() {
        return this.mContinusDays;
    }

    public void setLastSportTime(long lastSportTime) {
        this.mLastSportTime = lastSportTime;
    }

    public long getLastSportTime() {
        return this.mLastSportTime;
    }

    protected ContinuousDays() {
    }

    public String toString() {
        return "ContinusDay{mLastSportTime=" + this.mLastSportTime + ", mContinusDays='" + this.mContinusDays + '}';
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mLastSportTime);
        dest.writeInt(this.mContinusDays);
    }
}
