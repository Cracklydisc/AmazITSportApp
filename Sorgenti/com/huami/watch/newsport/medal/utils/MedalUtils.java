package com.huami.watch.newsport.medal.utils;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.medal.Medal;
import com.huami.watch.newsport.medal.MedalChecker;
import com.huami.watch.newsport.medal.MedalChecker.ICheckResultListener;
import com.huami.watch.newsport.medal.MedalContainer;
import java.util.List;

public class MedalUtils {
    private static final String TAG = MedalUtils.class.getName();

    public static void checkMedalActive(long trackId) {
        final MedalContainer medalContainer = MedalContainer.getInstance(Global.getApplicationContext());
        MedalChecker medalChecker = MedalChecker.getInstance();
        List<Medal> medals = medalContainer.getMedals(Global.getApplicationContext());
        medalContainer.checkContinuousSports(Global.getApplicationContext(), trackId);
        medalChecker.check(medals, new ICheckResultListener() {
            public void onMedalReceive(Medal medal) {
                Debug.m5i(MedalUtils.TAG, "medal receive : " + medal);
                medalContainer.makeActive(Global.getApplicationContext(), medal.getMedalId());
            }

            public void onMedalResult(List<Medal> list) {
            }
        }, trackId);
    }
}
