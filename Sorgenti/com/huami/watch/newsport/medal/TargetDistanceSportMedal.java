package com.huami.watch.newsport.medal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import java.util.List;

public class TargetDistanceSportMedal extends Medal {
    public static final Creator<Medal> CREATOR = new C05871();
    private float distance = 0.0f;
    private DataManager mDataManager = DataManager.getInstance();
    private SyncDatabaseManager<SportSummary> mStatusAsyncDatabaseManager = null;

    static class C05871 implements Creator<Medal> {
        C05871() {
        }

        public Medal createFromParcel(Parcel parcel) {
            Medal medal = new Medal();
            medal.initFromParcel(parcel);
            return medal;
        }

        public Medal[] newArray(int i) {
            return new Medal[i];
        }
    }

    public TargetDistanceSportMedal(int id, float distance) {
        super(id);
        this.distance = distance;
    }

    private void initDatabaseManager(Context context) {
        this.mStatusAsyncDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context));
    }

    boolean check(Context context, long trackId) {
        if (this.mIsActive) {
            return false;
        }
        if (this.mStatusAsyncDatabaseManager == null) {
            initDatabaseManager(context);
        }
        List<? extends SportSummary> sportSummaries = this.mStatusAsyncDatabaseManager.selectAll(context, null, null, "start_time DESC", "1");
        if (sportSummaries == null || sportSummaries.isEmpty()) {
            return false;
        }
        OutdoorSportSummary status = null;
        for (SportSummary summary : sportSummaries) {
            if (summary.getTrackId() == trackId) {
                status = (OutdoorSportSummary) summary;
                break;
            }
        }
        if (status == null) {
            return false;
        }
        Debug.m5i("TargetDistanceSportMedal", "get target distance medal because distance is " + status.getDistance() + ", target distance is " + this.distance);
        return status.getDistance() >= this.distance;
    }
}
