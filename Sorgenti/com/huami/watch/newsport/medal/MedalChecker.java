package com.huami.watch.newsport.medal;

import android.os.AsyncTask;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MedalChecker {
    private static final MedalChecker instance = new MedalChecker();
    private static ExecutorService mService = Executors.newSingleThreadExecutor();

    public interface ICheckResultListener {
        void onMedalReceive(Medal medal);

        void onMedalResult(List<Medal> list);
    }

    private MedalChecker() {
    }

    public static synchronized MedalChecker getInstance() {
        MedalChecker medalChecker;
        synchronized (MedalChecker.class) {
            medalChecker = instance;
        }
        return medalChecker;
    }

    public void check(List<Medal> medalList, ICheckResultListener listener, long trackId) {
        if (listener == null) {
            Debug.m6w("MedalChecker", "listener is null while check medal");
            return;
        }
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("MedalChecker", "check medal list " + medalList);
        }
        if (medalList != null) {
            final List<Medal> list = medalList;
            final long j = trackId;
            final ICheckResultListener iCheckResultListener = listener;
            new AsyncTask<Void, Medal, List<Medal>>() {
                protected List<Medal> doInBackground(Void... voids) {
                    List<Medal> result = new LinkedList();
                    for (Medal m : list) {
                        if (m.check(Global.getApplicationContext(), j)) {
                            result.add(m);
                            publishProgress(new Medal[]{m});
                        }
                    }
                    return result;
                }

                protected void onProgressUpdate(Medal... values) {
                    if (values != null && values.length >= 1) {
                        iCheckResultListener.onMedalReceive(values[0]);
                    }
                }

                protected void onPostExecute(List<Medal> medals) {
                    if (medals != null && medals.size() >= 1) {
                        iCheckResultListener.onMedalResult(medals);
                    }
                }
            }.executeOnExecutor(mService, new Void[0]);
        }
    }
}
