package com.huami.watch.newsport.medal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Comparator;

public class Medal implements Parcelable {
    public static final Creator<Medal> CREATOR = new C05851();
    protected int mBackgroundId = 0;
    protected int mDescResId = 0;
    protected int mIconId = 0;
    protected boolean mIsActive = false;
    protected int mMedalId = 0;
    protected String mName = null;
    protected int priority = 0;

    static class C05851 implements Creator<Medal> {
        C05851() {
        }

        public Medal createFromParcel(Parcel parcel) {
            Medal medal = new Medal();
            medal.initFromParcel(parcel);
            return medal;
        }

        public Medal[] newArray(int i) {
            return new Medal[i];
        }
    }

    public static final class MedalComparator implements Comparator<Medal> {
        public int compare(Medal medal1, Medal medal2) {
            if (medal1.isActive() && !medal2.isActive()) {
                return -1;
            }
            if (medal1.isActive() || !medal2.isActive()) {
                return 0;
            }
            return 1;
        }
    }

    Medal(int id) {
        this.mMedalId = id;
    }

    public String toString() {
        return "Medal{mIsActive=" + this.mIsActive + ", mName='" + this.mName + '\'' + ", mDescResId=" + this.mDescResId + ", mIconId=" + this.mIconId + ", mBackgroundId=" + this.mBackgroundId + ", mMedalId=" + this.mMedalId + ", priority=" + this.priority + '}';
    }

    protected void initFromParcel(Parcel parcel) {
        boolean z = true;
        this.mMedalId = parcel.readInt();
        if (parcel.readByte() != (byte) 1) {
            z = false;
        }
        setIsActive(z);
        setName(parcel.readString());
        setDescResId(parcel.readInt());
        setIconId(parcel.readInt());
        setBackgroundId(parcel.readInt());
    }

    public void setBackgroundId(int backgroundId) {
        this.mBackgroundId = backgroundId;
    }

    public int getMedalId() {
        return this.mMedalId;
    }

    public void setIconId(int iconId) {
        this.mIconId = iconId;
    }

    public void initIconId() {
    }

    public boolean isActive() {
        return this.mIsActive;
    }

    public void setIsActive(boolean isActive) {
        this.mIsActive = isActive;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setDescResId(int descResId) {
        this.mDescResId = descResId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mMedalId);
        parcel.writeByte((byte) (this.mIsActive ? 1 : 0));
        parcel.writeString(this.mName);
        parcel.writeInt(this.mDescResId);
        parcel.writeInt(this.mIconId);
        parcel.writeInt(this.mBackgroundId);
    }

    boolean check(Context context, long trackId) {
        return false;
    }
}
