package com.huami.watch.newsport;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

public class Global {
    public static boolean AUTO_GPS = false;
    public static boolean CATCH_EXCEPTION = false;
    public static boolean DEBUG_DATA = false;
    public static boolean DEBUG_GPS = false;
    public static final boolean DEBUG_GPS_TOAST = Log.isLoggable("sport_gps_toast", 2);
    public static final boolean DEBUG_LEVEL_3 = Log.isLoggable("sport_log", 2);
    public static final boolean DEBUG_NEW_UPLOAD = Log.isLoggable("sport_new_upload", 2);
    public static boolean DEBUG_ROUTE = false;
    public static final boolean DEBUG_TRAINING_MODE = Log.isLoggable("sport_train_log", 2);
    public static final boolean DEBUG_VIEW = DEBUG_LEVEL_3;
    public static boolean DEFAULT_MUSIC = false;
    public static boolean IGNORE_OPTMIZE_GPS = false;
    public static int MINIMUM_DISTANCE_METER = 10;
    public static boolean SCREENON_WHEN_WAKEUP = false;
    public static boolean USE_FRAMEWORK_GPS = false;
    private static Context sApplicationContext = null;
    private static Handler sCloudHandler;
    private static HandlerThread sCloudThread = new HandlerThread("global-cloud-thread");
    private static HandlerThread sHandlerThread = new HandlerThread("global-work-thread");
    private static Handler sHeartHandler;
    private static HandlerThread sHeartHandlerThread = new HandlerThread("global-heart-thread");
    private static Handler sRouteHandler;
    private static HandlerThread sRouteThread = new HandlerThread("global-route-thread");
    private static Handler sUIHandler = new Handler(Looper.getMainLooper());
    private static Handler sWorkHandler;

    public static class WidgetConst {
    }

    static {
        sWorkHandler = null;
        sHeartHandler = null;
        sCloudHandler = null;
        sRouteHandler = null;
        sHandlerThread.start();
        sHandlerThread.setPriority(10);
        sWorkHandler = new Handler(sHandlerThread.getLooper());
        sHeartHandlerThread.start();
        sHeartHandler = new Handler(sHeartHandlerThread.getLooper());
        sCloudThread.start();
        sCloudThread.setPriority(10);
        sCloudHandler = new Handler(sCloudThread.getLooper());
        sRouteThread.start();
        sRouteHandler = new Handler(sRouteThread.getLooper());
    }

    public static void init(Context context) {
        sApplicationContext = context.getApplicationContext();
    }

    public static Handler getGlobalHeartHandler() {
        return sHeartHandler;
    }

    public static Handler getGlobalWorkHandler() {
        return sWorkHandler;
    }

    public static Handler getGlobalCloudHandler() {
        return sCloudHandler;
    }

    public static Handler getGlobalRouteHandler() {
        return sRouteHandler;
    }

    public static Handler getGlobalUIHandler() {
        return sUIHandler;
    }

    public static Context getApplicationContext() {
        return sApplicationContext;
    }
}
