package com.huami.watch.newsport.gpxexport;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import clc.utils.taskmanager.Task;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.TaskManager;
import clc.utils.taskmanager.TaskOperation;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmLoadingDrawable;
import com.huami.watch.common.widget.SystemDialog;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.gpxexport.SportGPXSaver.OnGpxSaveListener;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ExportGPXActivity extends BaseActivity implements EventCallBack, OnGpxSaveListener {
    private SportGPXSaver gpxSaver;
    private SystemDialog hintCancelDlg;
    private HmLoadingDrawable loadingDrawable;
    private String name;
    private ViewFrame nodeOfResult;
    private ViewFrame nodeOfRolling;
    private TaskManager tm = new TaskManager("gpx-export-activity");
    private String trackId;
    private ViewSwitcher viewSwitcher;

    class C05692 implements OnClickListener {

        class C05671 implements DialogInterface.OnClickListener {
            C05671() {
            }

            public void onClick(DialogInterface dialog, int which) {
                ExportGPXActivity.this.handleCancel();
                ExportGPXActivity.this.hintCancelDlg.dismiss();
            }
        }

        class C05682 implements DialogInterface.OnClickListener {
            C05682() {
            }

            public void onClick(DialogInterface dialog, int which) {
                ExportGPXActivity.this.hintCancelDlg.dismiss();
            }
        }

        C05692() {
        }

        public void onClick(View v) {
            if (ExportGPXActivity.this.hintCancelDlg != null) {
                ExportGPXActivity.this.hintCancelDlg.dismiss();
                ExportGPXActivity.this.hintCancelDlg = null;
            }
            ExportGPXActivity.this.hintCancelDlg = new Builder(ExportGPXActivity.this).setMessage((int) C0532R.string.sport_dlg_msg_hint_cancel).setNegativeButton((int) C0532R.string.dlg_btn_l_msg, new C05682()).setPositiveButton(ExportGPXActivity.this.getResources().getString(C0532R.string.dlg_btn_r_msg), new C05671()).create();
            ExportGPXActivity.this.hintCancelDlg.show();
        }
    }

    class C05703 implements OnClickListener {
        C05703() {
        }

        public void onClick(View v) {
            ExportGPXActivity.this.finish();
        }
    }

    class ViewFrame {
        View root;
        private List<View> views;

        ViewFrame() {
        }

        public void add(View v) {
            if (this.views == null) {
                this.views = new ArrayList();
            }
            this.views.add(v);
        }

        public View get(int position) {
            return (View) this.views.get(position);
        }
    }

    class ViewSwitcher {
        ViewSwitcher() {
        }

        private void switchToStateInternal(int newState) {
            switch (newState) {
                case 0:
                    ExportGPXActivity.this.nodeOfResult.root.setVisibility(8);
                    ExportGPXActivity.this.loadingDrawable.start();
                    ExportGPXActivity.this.nodeOfRolling.root.setVisibility(0);
                    return;
                case 1:
                    ExportGPXActivity.this.nodeOfResult.root.setVisibility(8);
                    ((TextView) ExportGPXActivity.this.nodeOfRolling.get(1)).setText(C0532R.string.hint_cancelling);
                    ExportGPXActivity.this.nodeOfRolling.get(2).setVisibility(4);
                    ExportGPXActivity.this.nodeOfRolling.root.setVisibility(0);
                    return;
                case 2:
                    ExportGPXActivity.this.loadingDrawable.stop();
                    ExportGPXActivity.this.nodeOfRolling.root.setVisibility(8);
                    ExportGPXActivity.this.nodeOfResult.root.setVisibility(0);
                    ((TextView) ExportGPXActivity.this.nodeOfResult.get(0)).setText(C0532R.string.hint_success_exported_title);
                    ((TextView) ExportGPXActivity.this.nodeOfResult.get(1)).setText(C0532R.string.hint_where);
                    return;
                case 3:
                    ExportGPXActivity.this.loadingDrawable.stop();
                    ExportGPXActivity.this.nodeOfRolling.root.setVisibility(8);
                    ExportGPXActivity.this.nodeOfResult.root.setVisibility(0);
                    ((TextView) ExportGPXActivity.this.nodeOfResult.get(0)).setText(C0532R.string.hint_failed_exported_title);
                    ((TextView) ExportGPXActivity.this.nodeOfResult.get(1)).setText(C0532R.string.hint_failed);
                    return;
                default:
                    return;
            }
        }

        void switchToState(final int newState) {
            ExportGPXActivity.this.tm.next(new Task(RunningStatus.UI_THREAD) {
                public TaskOperation onExecute(TaskOperation taskOperation) {
                    ViewSwitcher.this.switchToStateInternal(newState);
                    return null;
                }
            }).execute();
        }
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            if (this.nodeOfResult.root.getVisibility() == 0) {
                if (((HmHaloButton) findViewById(C0532R.id.result_confirm)).getVisibility() == 0) {
                    ((HmHaloButton) findViewById(C0532R.id.result_confirm)).performClick();
                }
            } else if (this.nodeOfRolling.root.getVisibility() == 0 && ((HmHaloButton) findViewById(C0532R.id.btn_cancel)).getVisibility() == 0) {
                ((HmHaloButton) findViewById(C0532R.id.btn_cancel)).performClick();
            }
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        this.trackId = intent.getStringExtra("trackid");
        this.name = intent.getStringExtra("name");
        setKeyEventListener(this);
        initView();
        this.tm.next(new Task(RunningStatus.WORK_THREAD) {
            public TaskOperation onExecute(TaskOperation taskOperation) {
                ExportGPXActivity.this.rolling();
                return null;
            }
        }).execute();
    }

    private void initView() {
        setContentView(C0532R.layout.activity_export_gpx);
        this.nodeOfRolling = new ViewFrame();
        this.nodeOfRolling.root = findViewById(C0532R.id.node_rolling);
        ImageView iv = (ImageView) findViewById(C0532R.id.iv_loading);
        if (this.loadingDrawable == null) {
            this.loadingDrawable = new HmLoadingDrawable(30);
        }
        iv.setImageDrawable(this.loadingDrawable);
        TextView tv = (TextView) findViewById(C0532R.id.tv_hint);
        HmHaloButton cancel = (HmHaloButton) findViewById(C0532R.id.btn_cancel);
        cancel.setWithHalo(false);
        cancel.setOnClickListener(new C05692());
        this.nodeOfRolling.add(iv);
        this.nodeOfRolling.add(tv);
        this.nodeOfRolling.add(cancel);
        this.nodeOfResult = new ViewFrame();
        this.nodeOfResult.root = findViewById(C0532R.id.node_result);
        TextView title = (TextView) findViewById(C0532R.id.title_result);
        TextView msg = (TextView) findViewById(C0532R.id.msg_result);
        HmHaloButton confirm = (HmHaloButton) findViewById(C0532R.id.result_confirm);
        if (!UnitConvertUtils.isZh()) {
            ((LayoutParams) confirm.getLayoutParams()).topMargin = 20;
            ((LayoutParams) cancel.getLayoutParams()).topMargin = 20;
            ((LayoutParams) iv.getLayoutParams()).topMargin = 30;
        }
        confirm.setWithHalo(false);
        confirm.setOnClickListener(new C05703());
        this.nodeOfResult.add(title);
        this.nodeOfResult.add(msg);
        this.nodeOfResult.add(confirm);
        this.viewSwitcher = new ViewSwitcher();
        this.viewSwitcher.switchToState(0);
    }

    private void handleCancel() {
        if (this.gpxSaver == null) {
            finish();
            return;
        }
        this.viewSwitcher.switchToState(1);
        this.gpxSaver.cancel();
    }

    private void rolling() {
        this.gpxSaver = new SportGPXSaver(this, this.trackId, this.name);
        this.gpxSaver.setOnSaveFinishListener(this);
        this.gpxSaver.rolling();
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.gpxSaver != null) {
            this.gpxSaver.cancel();
            this.gpxSaver = null;
        }
    }

    public void onSaveFinished(String trackId, File f, int result) {
        Log.i("gpx", " exported : " + trackId + " , to file : " + f + " , res: " + result);
        if (this.viewSwitcher == null) {
            Log.i("gpx", "No view switcher.");
            return;
        }
        switch (result) {
            case -1:
                Log.i("gpx", "Finished for result cancelled !");
                finish();
                break;
            case 0:
                this.viewSwitcher.switchToState(2);
                break;
            default:
                this.viewSwitcher.switchToState(3);
                break;
        }
        this.gpxSaver = null;
    }

    public void finish() {
        clearBeforeFinish();
        super.finish();
    }

    private void clearBeforeFinish() {
    }
}
