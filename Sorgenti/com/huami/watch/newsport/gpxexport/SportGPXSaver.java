package com.huami.watch.newsport.gpxexport;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.gpxhandle.core.GPXSaver;
import com.huami.watch.gpxhandle.core.GPXSaver.GPXSaverCallbacks;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import java.io.File;

public class SportGPXSaver implements GPXSaverCallbacks {
    private Cursor dataPool;
    private String fileName;
    private Cursor heartPool;
    private OnGpxSaveListener f16l;
    private int lastHeartRate = 0;
    private GPXSaver saver;
    private Context theContext;
    private String trackId;

    public interface OnGpxSaveListener {
        void onSaveFinished(String str, File file, int i);
    }

    public void setOnSaveFinishListener(OnGpxSaveListener l) {
        this.f16l = l;
    }

    public SportGPXSaver(Context context, String trackId, String fileName) {
        this.theContext = context;
        this.trackId = trackId;
        this.fileName = fileName;
    }

    public boolean hasMorePoints() {
        return !this.dataPool.isAfterLast();
    }

    public Location nextPoint() {
        int heartV;
        Cursor c = this.dataPool;
        Location l = new Location("amazfit watch");
        l.setLongitude((double) c.getFloat(c.getColumnIndex("longitude")));
        l.setLatitude((double) c.getFloat(c.getColumnIndex("latitude")));
        l.setAltitude((double) c.getFloat(c.getColumnIndex("altitude")));
        long gpxPointTimePointer = c.getLong(c.getColumnIndex("timestamp"));
        l.setTime(gpxPointTimePointer);
        if (hasMorePoints()) {
            c.moveToNext();
        }
        if (this.heartPool.isAfterLast()) {
            heartV = this.lastHeartRate;
        } else {
            if (this.heartPool.getLong(this.heartPool.getColumnIndex("run_time")) < gpxPointTimePointer && !this.heartPool.isAfterLast()) {
                this.heartPool.moveToNext();
            }
            if (this.heartPool.isAfterLast()) {
                heartV = this.lastHeartRate;
            } else {
                heartV = this.heartPool.getInt(this.heartPool.getColumnIndex("rate"));
                this.lastHeartRate = heartV;
            }
        }
        Bundle b = new Bundle();
        b.putInt("heartrate", heartV);
        l.setExtras(b);
        return l;
    }

    public Object onFetchData() {
        Cursor c = new SyncDatabaseManager(LocationDataDao.getInstance(this.theContext)).selectAsCursor("track_id=?", new String[]{this.trackId}, "timestamp ASC", null);
        this.dataPool = c;
        this.heartPool = new SyncDatabaseManager(HeartRateDao.getInstance(this.theContext)).selectAsCursor("track_id=?", new String[]{this.trackId}, "time ASC", null);
        return c;
    }

    public void onPreHandleData() {
        this.dataPool.moveToFirst();
        this.heartPool.moveToFirst();
    }

    public void onSaveFinish(String trackId, File f, int resultCode) {
        Log.i("gpx", "rolling : " + f.getAbsolutePath() + " , done with : " + resultCode);
        if (this.f16l != null) {
            this.f16l.onSaveFinished(trackId, f, resultCode);
        }
    }

    public void onPostHandleData() {
        this.dataPool.close();
        this.heartPool.close();
    }

    public synchronized void rolling() {
        this.saver = new GPXSaver(this.theContext, this.trackId, this.fileName, this);
        this.saver.startSave();
    }

    public synchronized void cancel() {
        if (this.saver != null) {
            this.saver.cancelSave();
        }
    }

    public long onGetSportTime() {
        return Long.valueOf(this.trackId).longValue();
    }
}
