package com.huami.watch.newsport.xmlparser.bean;

public class TERemindStage {
    public String mDesc = null;
    public int mId = -1;
    public int mTe = 0;
    public int mTimeCost = 0;

    public int getId() {
        return this.mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getTe() {
        return this.mTe;
    }

    public void setTe(int te) {
        this.mTe = te;
    }

    public int getTimeCost() {
        return this.mTimeCost;
    }

    public void setTimeCost(int timeCost) {
        this.mTimeCost = timeCost;
    }

    public String getDesc() {
        return this.mDesc;
    }

    public void setDesc(String desc) {
        this.mDesc = desc;
    }
}
