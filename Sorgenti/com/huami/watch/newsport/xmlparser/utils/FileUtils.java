package com.huami.watch.newsport.xmlparser.utils;

import android.content.Context;
import android.util.Log;
import com.android.internal.util.XmlUtils;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportStatistic;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();

    public static String getFilePathByCategoryType(Context context, String modelName, int categoryType) {
        StringBuilder sb = new StringBuilder();
        sb.append(modelName + "_");
        sb.append(getLanguageType(context) == 0 ? "zh/" : "en/");
        sb.append("sport_effect.xml");
        return sb.toString();
    }

    public static int getLanguageType(Context mContext) {
        String result = mContext.getResources().getConfiguration().locale.getCountry();
        if (result.equals("CN")) {
            return 0;
        }
        if ((result.equals("UK") | result.equals("US")) != 0) {
            return 1;
        }
        return 0;
    }

    public static String getIntervalTrainFilePath(Context context, String fileName) {
        StringBuilder sb = new StringBuilder();
        sb.append("interval_train/");
        sb.append(fileName);
        return sb.toString();
    }

    public static String[] getFileIntervalTrain(Context mContext) {
        File file = new File("/data/data/" + mContext.getPackageName() + "/databases/" + "interval_run" + File.separator);
        if (!file.exists()) {
            file.mkdir();
        }
        String[] filePaths = file.list();
        Log.i(TAG, " filePaths length  " + filePaths.length);
        for (String curretPath : filePaths) {
            Log.i(TAG, " filePath:" + curretPath);
        }
        return filePaths;
    }

    public static void clearIntervalRunFiles(Context mContext) {
        File file = new File("/data/data/" + mContext.getPackageName() + "/databases/" + "interval_run" + File.separator);
        if (!file.exists()) {
            file.mkdir();
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File currentFile : listFiles) {
                currentFile.delete();
            }
        }
    }

    public static void copyShareFileFromSdCard(Context mContext) {
        XmlPullParserException var11;
        JSONException var13;
        IOException e;
        String filePath = "/sdcard/share.xml";
        File file = new File(filePath);
        if (file != null && file.exists()) {
            try {
                BufferedInputStream str = new BufferedInputStream(new FileInputStream(filePath), 16384);
                try {
                    Object object = XmlUtils.readMapXml(str).get("sport_statistic");
                    if (object != null) {
                        Log.i(TAG, " resultObject:" + object.toString());
                        SportStatistic sportStatistic = new SportStatistic();
                        sportStatistic.initObjectFromJSON(new JSONObject(object.toString()));
                        DataManager.getInstance().addStatistic(mContext, sportStatistic);
                        File newFile = new File(filePath);
                        if (newFile != null && newFile.exists()) {
                            newFile.delete();
                        }
                    } else {
                        Log.i(TAG, " resultObject: is null ");
                    }
                    BufferedInputStream bufferedInputStream = str;
                } catch (XmlPullParserException e2) {
                    var11 = e2;
                    bufferedInputStream = str;
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", var11);
                } catch (FileNotFoundException e3) {
                    var12 = e3;
                    bufferedInputStream = str;
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", var12);
                } catch (JSONException e4) {
                    var13 = e4;
                    bufferedInputStream = str;
                    Log.w("SharedPreferencesImpl", "getSharedPreferences", var13);
                } catch (IOException e5) {
                    e = e5;
                    bufferedInputStream = str;
                    e.printStackTrace();
                } catch (Throwable th) {
                    th = th;
                    bufferedInputStream = str;
                    throw th;
                }
            } catch (XmlPullParserException e6) {
                var11 = e6;
                Log.w("SharedPreferencesImpl", "getSharedPreferences", var11);
            } catch (FileNotFoundException e7) {
                FileNotFoundException var12;
                var12 = e7;
                Log.w("SharedPreferencesImpl", "getSharedPreferences", var12);
            } catch (JSONException e8) {
                var13 = e8;
                Log.w("SharedPreferencesImpl", "getSharedPreferences", var13);
            } catch (IOException e9) {
                e = e9;
                e.printStackTrace();
            } catch (Throwable th2) {
                Throwable th3;
                th3 = th2;
                throw th3;
            }
        }
    }
}
