package com.huami.watch.newsport.xmlparser.utils;

import android.content.Context;
import android.util.SparseArray;
import com.huami.watch.newsport.xmlparser.XmlTeHandler;
import com.huami.watch.newsport.xmlparser.bean.TERemindStage;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SAXUtils {
    public static SparseArray<TERemindStage> getTERemindSettingsFromXml(Context context) {
        try {
            InputStream inputStream = context.getAssets().open(FileUtils.getFilePathByCategoryType(context, "te", -1));
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            XmlTeHandler defaultHandler = new XmlTeHandler();
            parser.parse(inputStream, defaultHandler);
            inputStream.close();
            return defaultHandler.getTESettings();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }
}
