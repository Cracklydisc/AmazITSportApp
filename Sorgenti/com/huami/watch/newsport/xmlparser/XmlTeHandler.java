package com.huami.watch.newsport.xmlparser;

import android.util.SparseArray;
import com.huami.watch.newsport.xmlparser.bean.TERemindStage;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlTeHandler extends DefaultHandler {
    private TERemindStage mCurrentTE;
    private SparseArray<TERemindStage> mTESettings = null;
    private String tagName = null;

    public SparseArray<TERemindStage> getTESettings() {
        return this.mTESettings;
    }

    public void startDocument() throws SAXException {
        super.startDocument();
        this.mTESettings = new SparseArray();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("effect")) {
            this.mCurrentTE = new TERemindStage();
            this.mCurrentTE.setId(Integer.parseInt(attributes.getValue("id")));
        }
        this.tagName = qName;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.tagName != null) {
            String data = new String(ch, start, length);
            if (this.tagName.equals("te")) {
                this.mCurrentTE.setTe(Integer.parseInt(data));
            } else if (this.tagName.equals("time")) {
                this.mCurrentTE.setTimeCost(Integer.parseInt(data));
            } else if (this.tagName.equals("desc")) {
                this.mCurrentTE.setDesc(data);
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equals("effect")) {
            this.mTESettings.put(this.mCurrentTE.getId(), this.mCurrentTE);
            this.mCurrentTE = null;
        }
        this.tagName = null;
    }

    public void endDocument() throws SAXException {
        super.endDocument();
    }
}
