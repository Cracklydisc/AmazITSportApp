package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;

public class AutoPauseWakeupManager extends AbsWakeupManager {
    public AutoPauseWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 7;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.algorithm.sport_state_change", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.algorithm.sport_state_change", this);
    }

    public void onWakeup(Intent intent) {
        String sportState = intent.getStringExtra("SPORT_STATE");
        Debug.m5i("AutoPauseWakeupManager", "sport state: " + sportState);
        if ("sport_auto_pause".equals(sportState)) {
            notifyWakeup(Integer.valueOf(1));
        } else if ("sport_auto_resume".equals(sportState)) {
            notifyWakeup(Integer.valueOf(2));
        }
    }
}
