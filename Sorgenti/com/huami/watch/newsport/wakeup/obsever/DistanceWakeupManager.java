package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;

public class DistanceWakeupManager extends AbsWakeupManager {
    private static final String TAG = DistanceWakeupManager.class.getName();

    public DistanceWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 2;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.gps.total_distance", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.gps.total_distance", this);
    }

    public void onWakeup(Intent intent) {
        Debug.m5i(TAG, "DistanceWakeupManager onWakeup");
        notifyWakeup(new Object[0]);
        if (!SportDataManager.getInstance().isCustomTrainMode()) {
            ReminderManager.getInstance().remind(3, new Object[0]);
        }
    }
}
