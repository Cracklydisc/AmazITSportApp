package com.huami.watch.newsport.wakeup.obsever;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import java.util.Calendar;

public class TimeCostWakeUpManager extends AbsWakeupManager {
    private AlarmManager mAlarmManager = null;
    private Context mContext = null;
    private boolean mIsInAdvancedTrain = false;
    private boolean mIsSelfTrainMode = false;
    private boolean mIsSportStarted = false;
    private TimeCostAlramReceiver mReceiver = null;

    public class TimeCostAlramReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Debug.m5i("TimeCostWakeUpManager", "onReceive, time: " + System.currentTimeMillis());
            TimeCostWakeUpManager.this.onWakeup(intent);
        }
    }

    public TimeCostWakeUpManager(Context context, int sportType) {
        super(context, sportType);
        this.mContext = context;
        this.mAlarmManager = (AlarmManager) context.getSystemService("alarm");
        this.mReceiver = new TimeCostAlramReceiver();
    }

    private void registerTimeCostReceiver() {
        if (this.mContext != null && this.mReceiver != null) {
            this.mContext.registerReceiver(this.mReceiver, new IntentFilter("com.huami.watch.action.TIME_WAKEUP"));
        }
    }

    private void unRegisterTimeCostReceiver() {
        if (this.mContext != null && this.mReceiver != null) {
            this.mContext.unregisterReceiver(this.mReceiver);
        }
    }

    public int getWakeupSourceType() {
        if (this.mIsInAdvancedTrain) {
            return 17;
        }
        return 8;
    }

    public void startTimeCostAlarm(long second, boolean isSelfTrain, int targetTime, boolean isInAdvancedTrain) {
        if (this.mConfig == null) {
            this.mConfig = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType);
        }
        this.mIsSelfTrainMode = isSelfTrain;
        this.mIsInAdvancedTrain = isInAdvancedTrain;
        Debug.m5i("TimeCostWakeUpManager", "startTimeCostAlarm: " + this.mConfig + ", " + this.mIsSportStarted + ", isSelfTrainMode:" + this.mIsSelfTrainMode);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Debug.m5i("TimeCostWakeUpManager", "startTimeCostAlarm, startTime: " + calendar.getTimeInMillis());
        if (this.mIsSportStarted) {
            int targetSecond;
            if (ReminderManager.getInstance().isTrainningMode() && (ReminderManager.getInstance().getSportType() == 9 || ReminderManager.getInstance().getSportType() == 14)) {
                if (Global.DEBUG_TRAINING_MODE) {
                    targetSecond = 10;
                } else {
                    targetSecond = TrainingInfoManager.getInstance(this.mContext).getTrainingInfo().getMaxTime() / 1000;
                }
            } else if ((this.mConfig != null && this.mConfig.isRemindTargetTimeCost()) || isSelfTrain) {
                if (isSelfTrain) {
                    targetSecond = targetTime;
                } else {
                    targetSecond = (int) (this.mConfig.getTargetTimeCost() / 1000);
                }
            } else {
                return;
            }
            if (((long) targetSecond) <= second) {
                stopTimeCostAlarm();
                return;
            }
            int remindSecond = (int) (((long) targetSecond) - second);
            calendar.add(13, remindSecond);
            Debug.m5i("TimeCostWakeUpManager", "startTimeCostAlarm: " + targetSecond + ", now: " + second + ", remindSecond: " + remindSecond + ", endTime: " + calendar.getTimeInMillis());
            this.mAlarmManager.setExact(0, calendar.getTimeInMillis(), PendingIntent.getBroadcast(this.mContext, 0, new Intent("com.huami.watch.action.TIME_WAKEUP"), 0));
        }
    }

    public void startTimeCostAlarm(long second) {
        startTimeCostAlarm(second, false, 0, false);
    }

    public void stopTimeCostAlarm() {
        Debug.m5i("TimeCostWakeUpManager", "stopTimeCostAlarm");
        this.mAlarmManager.cancel(PendingIntent.getBroadcast(this.mContext, 0, new Intent("com.huami.watch.action.TIME_WAKEUP"), 0));
    }

    public void start() {
        super.start();
        startTimeCostAlarm(0);
        registerTimeCostReceiver();
    }

    public void stop() {
        super.stop();
        unRegisterTimeCostReceiver();
        stopTimeCostAlarm();
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        if (!this.mIsSelfTrainMode) {
            ReminderManager.getInstance().remind(2, new Object[0]);
        }
        Debug.m5i("TimeCostWakeUpManager", "onWakeUp");
    }

    public void setIsSportStarted(boolean isSportStarted) {
        this.mIsSportStarted = isSportStarted;
    }
}
