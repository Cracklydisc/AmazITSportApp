package com.huami.watch.newsport.wakeup.obsever;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.huami.watch.common.log.Debug;
import java.util.Calendar;

public class SleepTimeWakeupManager extends AbsWakeupManager {
    private static final String TAG = SleepTimeWakeupManager.class.getName();
    private AlarmManager mAlarmManager = null;
    private Context mContext = null;
    private TimerArrivalAlramReceiver mReceiver = null;
    private WakeLock mWakeLock = null;

    public class TimerArrivalAlramReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Debug.m5i(SleepTimeWakeupManager.TAG, "TimerArrivalAlramReceiver, onReceive, time: " + System.currentTimeMillis());
            SleepTimeWakeupManager.this.onWakeup(intent);
            SleepTimeWakeupManager.this.mWakeLock.acquire(10000);
        }
    }

    public SleepTimeWakeupManager(Context context, int sportType) {
        super(context, sportType);
        this.mContext = context;
        this.mAlarmManager = (AlarmManager) context.getSystemService("alarm");
        this.mReceiver = new TimerArrivalAlramReceiver();
        this.mWakeLock = ((PowerManager) this.mContext.getSystemService("power")).newWakeLock(1, "timer_arrival");
    }

    public int getWakeupSourceType() {
        return 9;
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        startTimeArrivalAlarm();
        Debug.m5i(TAG, "onWakeUp");
    }

    private void registerTimeCostReceiver() {
        if (this.mContext != null && this.mReceiver != null) {
            this.mContext.registerReceiver(this.mReceiver, new IntentFilter("com.huami.watch.action.SPORT_TIMER_ARRIVAL"));
        }
    }

    private void unRegisterTimeCostReceiver() {
        if (this.mContext != null && this.mReceiver != null) {
            this.mContext.unregisterReceiver(this.mReceiver);
        }
    }

    private void startTimeArrivalAlarm() {
        stopTimeArrivalAlarm();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Debug.m5i(TAG, "startTimeArrivalAlarm, startTime: " + calendar.getTimeInMillis());
        calendar.add(13, 3600);
        Debug.m5i(TAG, "startTimeArrivalAlarm, endTime: " + calendar.getTimeInMillis());
        this.mAlarmManager.setExact(0, calendar.getTimeInMillis(), PendingIntent.getBroadcast(this.mContext, 0, new Intent("com.huami.watch.action.SPORT_TIMER_ARRIVAL"), 0));
    }

    public void stopTimeArrivalAlarm() {
        Debug.m5i(TAG, "stopTimeArrivalAlarm");
        this.mAlarmManager.cancel(PendingIntent.getBroadcast(this.mContext, 0, new Intent("com.huami.watch.action.SPORT_TIMER_ARRIVAL"), 0));
    }

    public void start() {
        super.start();
        registerTimeCostReceiver();
    }

    public void stop() {
        super.stop();
        unRegisterTimeCostReceiver();
        stopTimeArrivalAlarm();
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    public void notifySportDisplayStatus(boolean isForeground) {
        if (isForeground) {
            stopTimeArrivalAlarm();
        } else {
            startTimeArrivalAlarm();
        }
    }
}
