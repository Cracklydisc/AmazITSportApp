package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class RouteOffsetWakeUpManager extends AbsWakeupManager {
    private static final String TAG = RouteOffsetWakeUpManager.class.getSimpleName();

    public RouteOffsetWakeUpManager(Context context, int sportType) {
        super(context, sportType);
    }

    public void start() {
        super.start();
        Log.i(TAG, "---route offset start -----  ");
        this.mWakeupSource.registerWakeupManager("sensorhub.trial.digression", this);
        this.mWakeupSource.registerWakeupManager("sensorhub.trial.back", this);
        this.mWakeupSource.registerWakeupManager("sensorhub.trial.off.far.away", this);
    }

    public void stop() {
        super.stop();
        Log.i(TAG, "---route offset stop -----  ");
        this.mWakeupSource.unregisterWakeupManager("sensorhub.trial.digression", this);
        this.mWakeupSource.unregisterWakeupManager("sensorhub.trial.back", this);
        this.mWakeupSource.unregisterWakeupManager("sensorhub.trial.off.far.away", this);
    }

    public int getWakeupSourceType() {
        return 10;
    }

    public void onWakeup(Intent intent) {
        Log.i(TAG, "---route offset on Wake up ---");
        String intentReuslt = intent.getStringExtra("WAKEUP_SOURCE");
        if ("sensorhub.trial.digression".equalsIgnoreCase(intentReuslt)) {
            Log.i(TAG, "---route offset on Wake up ---");
            ReminderManager.getInstance().remind(7, Integer.valueOf(7));
            notifyWakeup(Integer.valueOf(7));
        } else if ("sensorhub.trial.back".equalsIgnoreCase(intentReuslt)) {
            Log.i(TAG, "---route back on Wake up ---");
            ReminderManager.getInstance().remind(8, Integer.valueOf(8));
            notifyWakeup(Integer.valueOf(8));
        } else if ("sensorhub.trial.off.far.away".equalsIgnoreCase(intentReuslt)) {
            Log.i(TAG, "---route trial off far away  ---");
            notifyWakeup(Integer.valueOf(9));
        }
    }
}
