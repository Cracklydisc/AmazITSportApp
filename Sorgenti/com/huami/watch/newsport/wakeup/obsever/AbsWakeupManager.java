package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.wakeup.receiver.WakeupSource;
import java.util.LinkedList;
import java.util.List;

public abstract class AbsWakeupManager {
    protected BaseConfig mConfig = null;
    protected Context mContext = null;
    protected int mSportType = -1;
    private List<IWakeupListener> mWakeupListeners = new LinkedList();
    protected WakeupSource mWakeupSource = null;

    public interface IWakeupListener {
        void wakeup(AbsWakeupManager absWakeupManager, Object... objArr);
    }

    public abstract int getWakeupSourceType();

    public abstract void onWakeup(Intent intent);

    public AbsWakeupManager(Context context, int sportType) {
        this.mContext = context;
        this.mWakeupSource = WakeupSource.getInstance(this.mContext);
        this.mSportType = sportType;
        if (!SportType.isSportTypeValid(this.mSportType)) {
            throw new IllegalArgumentException("err sport type:" + this.mSportType);
        }
    }

    public AbsWakeupManager setConfig(BaseConfig config) {
        this.mConfig = config;
        return this;
    }

    public void registerWakeupListener(IWakeupListener listener) {
        if (!this.mWakeupListeners.contains(listener)) {
            if (this.mWakeupListeners.isEmpty()) {
                start();
            }
            this.mWakeupListeners.add(listener);
        }
    }

    public void unregisterWakeupListener(IWakeupListener listener) {
        if (this.mWakeupListeners.contains(listener)) {
            if (this.mWakeupListeners.size() == 1) {
                stop();
            }
            this.mWakeupListeners.remove(listener);
        }
    }

    public void start() {
        Debug.m3d("AbsWakeupManager", "start wake up manager : " + getClass().getSimpleName());
    }

    public void stop() {
        Debug.m3d("AbsWakeupManager", "stop wake up manager : " + getClass().getSimpleName());
    }

    protected void notifyWakeup(Object... params) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("AbsWakeupManager", "notifyWakeup|param : " + params);
        }
        if (this.mWakeupListeners.size() == 0) {
            Debug.m6w("AbsWakeupManager", "wakeup listen is empty");
            return;
        }
        for (IWakeupListener l : new LinkedList(this.mWakeupListeners)) {
            if (l != null) {
                l.wakeup(this, params);
            }
        }
    }
}
