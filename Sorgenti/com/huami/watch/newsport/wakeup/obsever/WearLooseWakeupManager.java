package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class WearLooseWakeupManager extends AbsWakeupManager {
    public static final String TAG = WearLooseWakeupManager.class.getName();

    public WearLooseWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.not.wear.tight", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.not.wear.tight", this);
    }

    public int getWakeupSourceType() {
        return 11;
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        Debug.m3d(TAG, "wear loose remind");
        ReminderManager.getInstance().remind(10, new Object[0]);
    }
}
