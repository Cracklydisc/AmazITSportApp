package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class LowSpeedWakeupManager extends AbsWakeupManager {
    public LowSpeedWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 6;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.algorithm.pace.minimum", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.algorithm.pace.minimum", this);
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        float pace = 0.0f;
        try {
            pace = Float.parseFloat(intent.getStringExtra("PACE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Debug.m3d("HmLowSpeedWakeupManager", "low speed: " + pace);
        ReminderManager.getInstance().remind(4, Float.valueOf(pace));
    }
}
