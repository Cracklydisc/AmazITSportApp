package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class ExtNormalHeartrateWakeupManager extends AbsWakeupManager {
    public ExtNormalHeartrateWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 5;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.heatrate.exceed_target_zone", this);
        this.mWakeupSource.registerWakeupManager("sensorhub.hearrate.reach.ratezone", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.heatrate.exceed_target_zone", this);
        this.mWakeupSource.unregisterWakeupManager("sensorhub.hearrate.reach.ratezone", this);
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        String intentReuslt = intent.getStringExtra("WAKEUP_SOURCE");
        int heartRate;
        if ("sensorhub.heatrate.exceed_target_zone".equalsIgnoreCase(intentReuslt)) {
            heartRate = 0;
            try {
                heartRate = Integer.parseInt(intent.getStringExtra("HEART_RATE_UP")) & 255;
            } catch (Exception e) {
                e.printStackTrace();
            }
            Debug.m3d("ExtNormalHeartrateWakeupManager", "Ext Normal Heartrate: " + heartRate);
            ReminderManager.getInstance().remind(1, Integer.valueOf(heartRate));
        } else if ("sensorhub.hearrate.reach.ratezone".equalsIgnoreCase(intentReuslt)) {
            heartRate = 0;
            try {
                heartRate = Integer.parseInt(intent.getStringExtra("HEART_RATE_UP")) & 255;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            Debug.m3d("ExtNormalHeartrateWakeupManager", "Ext Normal Heartrate: " + heartRate);
            ReminderManager.getInstance().remind(12, Integer.valueOf(heartRate));
        }
    }
}
