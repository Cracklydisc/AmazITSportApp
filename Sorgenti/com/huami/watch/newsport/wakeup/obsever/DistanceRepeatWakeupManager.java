package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import java.util.Arrays;

public class DistanceRepeatWakeupManager extends AbsWakeupManager {
    private boolean mIsPlayVoice = true;
    private boolean mShowAlertWindow = false;

    public DistanceRepeatWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 1;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.new.kilometer", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.new.kilometer", this);
    }

    private void initConfig() {
        if (this.mConfig == null) {
            this.mConfig = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType);
        }
        if (this.mConfig.getSportType() == 18 && this.mConfig.isRemindPerKM()) {
            this.mConfig.setIsRemindPerKM(false);
            DataManager.getInstance().setSportConfig(this.mContext, this.mConfig);
        }
        this.mShowAlertWindow = this.mConfig.isRemindPerKM();
        this.mIsPlayVoice = this.mConfig.isRemindPlayVoice();
        LogUtil.m9i(true, "DistanceRepeatWakeupManager", "set show alert window " + this.mShowAlertWindow + ", isPlayVoice:" + this.mIsPlayVoice);
    }

    public void onWakeup(Intent intent) {
        initConfig();
        boolean isImperial = UnitConvertUtils.isImperial();
        boolean isOutdoorRiding = false;
        if (this.mConfig.getSportType() == 9 || this.mConfig.getSportType() == 7) {
            isOutdoorRiding = true;
        }
        float kmDistance = 0.0f;
        float kmAvePace = 0.0f;
        float kmAveSpeed = 0.0f;
        float dailyFloatValue = (float) DailyPerformenceChatView.invalidPointValue;
        int teValue = 0;
        String kmDistanceStr = intent.getStringExtra("REPEAT_DISTANCE");
        String kmAvePaceStr = intent.getStringExtra("LAST_KM_PACE");
        String kmAveSpeedStr = intent.getStringExtra("LAST_KM_SPEED");
        String teValueStr = intent.getStringExtra("trainning_effect");
        String dailyPerformenceStr = intent.getStringExtra("daily_performance");
        LogUtil.m9i(true, "DistanceRepeatWakeupManager", "wake up. distance : " + kmDistanceStr + ", pace : " + kmAvePaceStr + ", speed : " + kmAveSpeedStr + ", isImperial:" + isImperial + ", teValue:" + teValue + ",dailyFLoat:" + dailyFloatValue + ",teValueStr:" + teValueStr + ", lnglat:" + Arrays.toString(intent.getLongArrayExtra("lonlat_point")));
        try {
            kmDistance = Float.parseFloat(kmDistanceStr) / 1000.0f;
            kmAvePace = Float.parseFloat(kmAvePaceStr);
            kmAveSpeed = Float.parseFloat(kmAveSpeedStr);
            dailyFloatValue = Float.parseFloat(dailyPerformenceStr);
            teValue = Integer.parseInt(teValueStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Debug.m3d("DistanceRepeatWakeupManager", "on distance repeat wake up. distance : " + kmDistanceStr + ", ave pace: " + kmAvePace + ", ave speed: " + kmAveSpeed);
        if (!(!this.mShowAlertWindow || isImperial || SportDataManager.getInstance().getCurentSportType() == 1015)) {
            if (!isOutdoorRiding) {
                ReminderManager.getInstance().remind(0, Float.valueOf(kmDistance), Float.valueOf(kmAvePace), Float.valueOf(kmAveSpeed), Boolean.valueOf(false), Integer.valueOf(teValue), Float.valueOf(dailyFloatValue));
            } else if (Math.round(kmDistance) % this.mConfig.getRemindKm() == 0) {
                ReminderManager.getInstance().remind(0, Float.valueOf(kmDistance), Float.valueOf(kmAvePace), Float.valueOf(kmAveSpeed), Boolean.valueOf(false), Integer.valueOf(teValue), Float.valueOf(dailyFloatValue));
            }
        }
        if (this.mIsPlayVoice) {
            if (!isOutdoorRiding) {
                PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(1, true, Float.valueOf(kmDistance), Float.valueOf(kmAvePace), Float.valueOf(kmAveSpeed));
            } else if (Math.round(kmDistance) % this.mConfig.getRemindKm() == 0) {
                PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(1, true, Float.valueOf(kmDistance), Float.valueOf(kmAvePace), Float.valueOf(kmAveSpeed));
            }
        }
        notifyWakeup(Float.valueOf(kmDistance));
    }
}
