package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class CalorieWakeupManager extends AbsWakeupManager {
    public CalorieWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 12;
    }

    public void onWakeup(Intent intent) {
        ReminderManager.getInstance().remind(11, new Object[0]);
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.calories.target", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.calories.target", this);
    }
}
