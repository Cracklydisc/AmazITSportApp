package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.utils.Constants.GPSUtils;
import com.huami.watch.newsport.utils.PowerUtils;
import com.huami.watch.newsport.utils.VibratorUtil;

public class GPSStatusChangeWakeupManager extends AbsWakeupManager {
    private String VALUE_KEY_GPS_STATS = "GPS_STATE";

    public GPSStatusChangeWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public int getWakeupSourceType() {
        return 3;
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.gps.state_change", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.gps.state_change", this);
    }

    public void onWakeup(Intent intent) {
        if (Global.SCREENON_WHEN_WAKEUP) {
            PowerUtils.wakeupForMilliSecond(this.mContext, 7000);
        }
        String gpsStatusText = intent.getStringExtra(this.VALUE_KEY_GPS_STATS);
        Debug.m5i("GPSStatusChangeWakeupManager", "onWakeup, gpsStatus: " + gpsStatusText);
        int gpsStatus = 2;
        if ("1".equals(gpsStatusText)) {
            gpsStatus = 1;
            ReminderManager.getInstance().remind(5, Boolean.valueOf(true));
        } else {
            if (GPSUtils.isGPSRouteAvailable(this.mSportType)) {
                ReminderManager.getInstance().remind(5, Boolean.valueOf(false));
            }
            VibratorUtil.vibrateOnceWithStrongAttributes(this.mContext, new long[]{0, 500, 200, 500});
        }
        notifyWakeup(Integer.valueOf(gpsStatus));
    }
}
