package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.PowerUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager.SwimInfo;
import java.util.Arrays;
import java.util.List;

public class LapWakeupManager extends AbsWakeupManager {
    private static final int[] AUTO_LAP_ARRAY = new int[]{1, 9, 8, 6, 14, 7, 13, 11};
    private static final String TAG = LapWakeupManager.class.getName();

    public LapWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.gps.repeat_distance", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.gps.repeat_distance", this);
    }

    public int getWakeupSourceType() {
        return 13;
    }

    private boolean isValidAutoLapRemind(int sportType) {
        Debug.m5i(TAG, "isValidAutoLapRemind, sporType:" + sportType);
        for (int type : AUTO_LAP_ARRAY) {
            if (type == sportType) {
                return true;
            }
        }
        return false;
    }

    public void onWakeup(Intent intent) {
        if (this.mConfig == null) {
            this.mConfig = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType);
        }
        if (isValidAutoLapRemind(this.mSportType)) {
            float distancePerLap = 0.0f;
            if (this.mConfig != null) {
                if (this.mConfig.isOpenAutoLapRecord() || this.mSportType == 11) {
                    distancePerLap = this.mConfig.getDistanceAutoLap();
                } else {
                    Debug.m5i(TAG, "LapWakeupManager, not open");
                    return;
                }
            }
            if (Float.compare(distancePerLap, 0.0f) != 0 || this.mSportType == 11) {
                List<RunningInfoPerLap> preLaps = SportDataManager.getInstance().getAutoLapsInfo();
                if (preLaps == null) {
                    Debug.m5i(TAG, "the lap infos is null");
                    return;
                }
                int i;
                String lapDistanceStr = intent.getStringExtra("REPEAT_DISTANCE");
                String lapAvePaceStr = intent.getStringExtra("LAP_PACE");
                String penNumStr = intent.getStringExtra("LAP_NUMBER");
                String timeStr = intent.getStringExtra("LAP_TIME");
                String calorieStr = intent.getStringExtra("total_calories");
                String cadenceStr = intent.getStringExtra("cadence_per_lap");
                String climbDistanceStr = intent.getStringExtra("climb_distance");
                String climb3DDescendStr = intent.getStringExtra("ramp_meter");
                String climbUpStr = intent.getStringExtra("ascend_meter");
                String climbDownStr = intent.getStringExtra("descend_meter");
                SwimInfo swimInfo = (SwimInfo) intent.getParcelableExtra("swim_info");
                long[] lnglat = intent.getLongArrayExtra("lonlat_point");
                String maximumFallStr = intent.getStringExtra("lap_max_fall");
                Debug.m3d(TAG, "on distance repeat wake up. distance : " + lapDistanceStr + ", ave pace: " + lapAvePaceStr + ", penNumStr:" + penNumStr + ", timeStr:" + timeStr + ", calorieStr:" + calorieStr + ", cadenceStr:" + cadenceStr + ", climbDistanceStr:" + climbDistanceStr + ", swim: " + (swimInfo != null ? swimInfo.toString() : "null") + ", lnglat:" + Arrays.toString(lnglat) + ", climb3DDescendStr:" + climb3DDescendStr + ", maximumFallStr:" + maximumFallStr);
                RunningInfoPerLap info = new RunningInfoPerLap();
                if (lnglat != null && lnglat.length >= 3) {
                    float lat = ((float) lnglat[2]) / 3000000.0f;
                    info.setLongitude(((float) lnglat[1]) / 3000000.0f);
                    info.setLatitude(lat);
                }
                if (lapDistanceStr != null) {
                    if (this.mSportType != 11) {
                        double distance = (double) this.mConfig.getDistanceAutoLap();
                        if (this.mConfig.getUnit() != 0) {
                            info.setDistance((float) Math.round(UnitConvertUtils.convertYDOrMeter2Meter(distance, this.mConfig.getUnit())));
                        } else if (SportType.isSwimMode(this.mSportType)) {
                            info.setDistance((float) UnitConvertUtils.convertDistanceToMeter(distance, this.mSportType));
                        } else {
                            info.setDistance((float) Math.ceil(UnitConvertUtils.convertDistanceFromMileOrKmtokm(distance / 1000.0d) * 1000.0d));
                        }
                    } else if (climb3DDescendStr != null) {
                        float distance2 = Float.parseFloat(climb3DDescendStr);
                        float previousDis = 0.0f;
                        for (i = 0; i < preLaps.size(); i++) {
                            previousDis += ((RunningInfoPerLap) preLaps.get(i)).getDistance();
                        }
                        info.setDistance(distance2 - previousDis);
                    }
                }
                if (timeStr != null) {
                    Debug.m5i(TAG, "auto lap total time:" + SportDataManager.getInstance().getCurrentTimeMillisOnRuntime());
                    long totalTime = (long) (Float.parseFloat(timeStr) * 1000.0f);
                    long preCostTime = 0;
                    for (RunningInfoPerLap lap : preLaps) {
                        preCostTime += lap.getCostTime();
                    }
                    long totalCostTime = SportDataManager.getInstance().getCurrentTimeMillisOnRuntime() - SportDataManager.getInstance().getSportTrackId();
                    if (totalCostTime < totalTime) {
                        totalCostTime = totalTime;
                    }
                    info.setTotalCostTime(totalCostTime);
                    info.setCostTime(totalTime - preCostTime);
                }
                if (lapAvePaceStr != null) {
                    if (this.mSportType == 11) {
                        info.setPace((double) Float.parseFloat(lapAvePaceStr));
                    } else {
                        float lapPace = ((float) (info.getCostTime() / 1000)) / info.getDistance();
                        info.setPace((double) lapPace);
                        Debug.m3d(TAG, "LapWakeupManager, lapPace: " + lapPace);
                    }
                }
                if (penNumStr != null) {
                    info.setLapNum(Integer.parseInt(penNumStr) - 1);
                }
                if (calorieStr != null) {
                    float totalCalorie = Float.parseFloat(calorieStr);
                    float previousCal = 0.0f;
                    for (i = 0; i < preLaps.size(); i++) {
                        previousCal += (float) ((RunningInfoPerLap) preLaps.get(i)).getLapCalories();
                    }
                    info.setLapCalories((int) ((1000.0f * totalCalorie) - previousCal));
                }
                if (cadenceStr != null) {
                    info.setLapStepFreq(Float.parseFloat(cadenceStr));
                }
                if (climbUpStr != null) {
                    float totalClimbUp = Float.parseFloat(climbUpStr);
                    float preClimb = 0.0f;
                    for (i = 0; i < preLaps.size(); i++) {
                        preClimb += ((RunningInfoPerLap) preLaps.get(i)).getClimbUp();
                    }
                    info.setClimbUp(totalClimbUp - preClimb);
                }
                if (climbDownStr != null) {
                    float totalClimbDown = Float.parseFloat(climbDownStr);
                    float preClimbDown = 0.0f;
                    for (i = 0; i < preLaps.size(); i++) {
                        preClimbDown += ((RunningInfoPerLap) preLaps.get(i)).getClimbDown();
                    }
                    info.setClimbDown(totalClimbDown - preClimbDown);
                }
                if (climbDistanceStr != null) {
                    float totalClimbDis = Float.parseFloat(climbDistanceStr);
                    float preClimbDis = 0.0f;
                    for (i = 0; i < preLaps.size(); i++) {
                        preClimbDis += ((RunningInfoPerLap) preLaps.get(i)).getClimbDistance();
                    }
                    info.setClimbDistance(totalClimbDis - preClimbDis);
                }
                if (swimInfo != null) {
                    info.setLapStrokes(swimInfo.mLapStrokes);
                    info.setLapStrokeSpeed(swimInfo.mLapStrokeSpeed);
                    info.setLapSwolf(swimInfo.mLapSwolf);
                }
                if (maximumFallStr != null && this.mSportType == 11) {
                    info.setMaxiMumFall(Float.parseFloat(maximumFallStr));
                }
                if (this.mSportType == 11) {
                    info.setLapType(3);
                } else {
                    info.setLapType(1);
                }
                SportDataManager.getInstance().addAutoLapsInfo(info);
                if (this.mConfig.isRemindAutoLap()) {
                    notifyWakeup(new Object[0]);
                    PowerUtils.wakeupForMilliSecond(this.mContext, 6000);
                    return;
                }
                return;
            }
            Debug.m5i(TAG, "LapWakeupManager, the per lap distance should not be 0");
            return;
        }
        Debug.m5i(TAG, "is not valid sport auto lap type");
    }
}
