package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;

public class DailyPromanceSixMinuteWakeupManager extends AbsWakeupManager {
    private static final String TAG = DailyPromanceSixMinuteWakeupManager.class.getSimpleName();

    public DailyPromanceSixMinuteWakeupManager(Context context, int sportType) {
        super(context, sportType);
    }

    public void start() {
        super.start();
        this.mWakeupSource.registerWakeupManager("sensorhub.ete.daily.performance.info", this);
    }

    public void stop() {
        super.stop();
        this.mWakeupSource.unregisterWakeupManager("sensorhub.ete.daily.performance.info", this);
    }

    public int getWakeupSourceType() {
        return 16;
    }

    public void onWakeup(Intent intent) {
        notifyWakeup(new Object[0]);
        Log.i(TAG, " onWakeup ");
        if (intent != null) {
            Log.i(TAG, " dailyProformanceValueInt: " + intent.getIntExtra("daily_performance", DailyPerformenceChatView.invalidPointValue));
            ReminderManager.getInstance().remind(16, Integer.valueOf(dailyProformanceValue));
        }
    }
}
