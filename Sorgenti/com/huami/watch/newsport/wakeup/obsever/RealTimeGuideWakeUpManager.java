package com.huami.watch.newsport.wakeup.obsever;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.huami.watch.newsport.reminder.controller.ReminderManager;

public class RealTimeGuideWakeUpManager extends AbsWakeupManager {
    private static final String TAG = RealTimeGuideWakeUpManager.class.getSimpleName();

    public RealTimeGuideWakeUpManager(Context context, int sportType) {
        super(context, sportType);
    }

    public void start() {
        super.start();
        Log.i(TAG, "---   start -----  ");
        this.mWakeupSource.registerWakeupManager("sensorhub.ete.rt.phrase.num.info", this);
    }

    public void stop() {
        super.stop();
        Log.i(TAG, "---   stop -----  ");
        this.mWakeupSource.unregisterWakeupManager("sensorhub.ete.rt.phrase.num.info", this);
    }

    public int getWakeupSourceType() {
        return 15;
    }

    public void onWakeup(Intent intent) {
        Log.i(TAG, "---   onWakeup -----  ");
        if (intent != null) {
            notifyWakeup(new Object[0]);
            int realTimeGuideValue = intent.getIntExtra("phrase_number_info", 0);
            Log.i(TAG, " realTimeGuideValue:" + realTimeGuideValue + ",phVariableValue:" + intent.getIntExtra("phrase_variable", 0));
            ReminderManager.getInstance().remind(15, Integer.valueOf(realTimeGuideValue), Integer.valueOf(phVariableValue));
        }
    }
}
