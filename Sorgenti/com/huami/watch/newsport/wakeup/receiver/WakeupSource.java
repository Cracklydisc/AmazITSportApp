package com.huami.watch.newsport.wakeup.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.utils.PowerUtils;
import com.huami.watch.newsport.wakeup.obsever.AbsWakeupManager;
import java.util.Map;
import java.util.TreeMap;

public class WakeupSource {
    private static WakeupSource gInstance = null;
    private WakeupSourceBroadcastReceiver mBroadcastReceiver = null;
    private Context mContext = null;
    private Map<String, AbsWakeupManager> mWakeupManagerMap = new TreeMap();

    private class WakeupSourceBroadcastReceiver extends BroadcastReceiver {
        private WakeupSourceBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (Global.SCREENON_WHEN_WAKEUP) {
                PowerUtils.wakeupForMilliSecond(context, 7000);
            }
            String wakeupSource = intent.getStringExtra("WAKEUP_SOURCE");
            Debug.m5i("WakeupSource", "receive wakeup broadcast : " + wakeupSource);
            if (wakeupSource == null) {
                Debug.m6w("WakeupSource", "wake up source should not be null");
                return;
            }
            AbsWakeupManager wakeupManager = (AbsWakeupManager) WakeupSource.this.mWakeupManagerMap.get(wakeupSource);
            if (wakeupManager == null) {
                Debug.m6w("WakeupSource", "wake up source : " + wakeupSource + " has not been registered");
            } else {
                wakeupManager.onWakeup(intent);
            }
        }
    }

    private WakeupSource(Context context) {
        this.mContext = context;
        this.mBroadcastReceiver = new WakeupSourceBroadcastReceiver();
    }

    public static synchronized WakeupSource getInstance(Context context) {
        WakeupSource wakeupSource;
        synchronized (WakeupSource.class) {
            if (gInstance == null) {
                gInstance = new WakeupSource(context.getApplicationContext());
            }
            wakeupSource = gInstance;
        }
        return wakeupSource;
    }

    public void registerWakeupManager(String wakeupSource, AbsWakeupManager wakeupManager) {
        Debug.m5i("WakeupSource", "register wake up manager : map : " + this.mWakeupManagerMap);
        if (this.mWakeupManagerMap.isEmpty()) {
            start();
        }
        this.mWakeupManagerMap.put(wakeupSource, wakeupManager);
        Debug.m5i("WakeupSource", "register wake up manager end : map : " + this.mWakeupManagerMap);
    }

    public void unregisterWakeupManager(String wakeupSource, AbsWakeupManager wakeupManager) {
        Debug.m5i("WakeupSource", "unregister wake up manager : map : " + this.mWakeupManagerMap);
        if (((AbsWakeupManager) this.mWakeupManagerMap.get(wakeupSource)) == wakeupManager) {
            this.mWakeupManagerMap.remove(wakeupSource);
        }
        if (this.mWakeupManagerMap.isEmpty()) {
            stop();
        }
    }

    public void start() {
        Debug.m5i("WakeupSource", "wake up source is started");
        this.mContext.registerReceiver(this.mBroadcastReceiver, new IntentFilter("com.huami.watch.action.SENSOR_WAKEUP"));
    }

    public void stop() {
        Debug.m5i("WakeupSource", "wake up source is stopped");
        this.mContext.unregisterReceiver(this.mBroadcastReceiver);
    }
}
