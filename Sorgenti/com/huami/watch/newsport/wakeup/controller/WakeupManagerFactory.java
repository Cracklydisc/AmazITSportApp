package com.huami.watch.newsport.wakeup.controller;

import android.content.Context;
import com.huami.watch.newsport.wakeup.obsever.AbsWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.AutoPauseWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.CalorieWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.DailyPromanceSixMinuteWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.DistanceRepeatMileWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.DistanceRepeatWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.DistanceWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.ExtNormalHeartrateWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.GPSStatusChangeWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.LapWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.LowSpeedWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.RealTimeGuideWakeUpManager;
import com.huami.watch.newsport.wakeup.obsever.RouteOffsetWakeUpManager;
import com.huami.watch.newsport.wakeup.obsever.SleepTimeWakeupManager;
import com.huami.watch.newsport.wakeup.obsever.TimeCostWakeUpManager;
import com.huami.watch.newsport.wakeup.obsever.WearLooseWakeupManager;

public class WakeupManagerFactory {
    private static WakeupManagerFactory gInstance = null;
    private AbsWakeupManager mAutoPauseWakeupManager = null;
    private AbsWakeupManager mCalorieWakeupManager = null;
    private AbsWakeupManager mDailyPromanceSixMinuteWakeupManager = null;
    private AbsWakeupManager mDistanceRepeatMileWakeupManager = null;
    private AbsWakeupManager mDistanceRepeatWakeupManager = null;
    private AbsWakeupManager mDistanceWakeupManager = null;
    private AbsWakeupManager mExtNormalHeartrateWakeupManager = null;
    private AbsWakeupManager mGPSStatusChangeWakeupManager = null;
    private AbsWakeupManager mLapWakeupManager = null;
    private AbsWakeupManager mLowSpeedWakeupManager = null;
    private AbsWakeupManager mRealTimeGuideWakeUpManager = null;
    private AbsWakeupManager mRouteOffsetWakeupManager = null;
    private AbsWakeupManager mTimeCostWakeupManager = null;
    private AbsWakeupManager mTimerArrivalWakeupManager = null;
    private AbsWakeupManager mWearLooseWakeupManager = null;

    private WakeupManagerFactory() {
    }

    public static synchronized WakeupManagerFactory getInstance() {
        WakeupManagerFactory wakeupManagerFactory;
        synchronized (WakeupManagerFactory.class) {
            if (gInstance == null) {
                gInstance = new WakeupManagerFactory();
            }
            wakeupManagerFactory = gInstance;
        }
        return wakeupManagerFactory;
    }

    public AbsWakeupManager getAutoPauseWakeupManager(Context context, int sportType) {
        if (this.mAutoPauseWakeupManager == null) {
            this.mAutoPauseWakeupManager = new AutoPauseWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mAutoPauseWakeupManager;
    }

    public AbsWakeupManager getDistanceRepeatWakeupManager(Context context, int sportType) {
        if (this.mDistanceRepeatWakeupManager == null) {
            this.mDistanceRepeatWakeupManager = new DistanceRepeatWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mDistanceRepeatWakeupManager;
    }

    public AbsWakeupManager getDistanceRepeatMileWakeupManager(Context context, int sportType) {
        if (this.mDistanceRepeatMileWakeupManager == null) {
            this.mDistanceRepeatMileWakeupManager = new DistanceRepeatMileWakeupManager(context, sportType);
        }
        return this.mDistanceRepeatMileWakeupManager;
    }

    public AbsWakeupManager getDistanceWakeupManager(Context context, int sportType) {
        if (this.mDistanceWakeupManager == null) {
            this.mDistanceWakeupManager = new DistanceWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mDistanceWakeupManager;
    }

    public AbsWakeupManager getExtNormalHeartrateWakeupManager(Context context, int sportType) {
        if (this.mExtNormalHeartrateWakeupManager == null) {
            this.mExtNormalHeartrateWakeupManager = new ExtNormalHeartrateWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mExtNormalHeartrateWakeupManager;
    }

    public AbsWakeupManager getGPSStatusChangeWakeupManager(Context context, int sportType) {
        if (this.mGPSStatusChangeWakeupManager == null) {
            this.mGPSStatusChangeWakeupManager = new GPSStatusChangeWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mGPSStatusChangeWakeupManager;
    }

    public AbsWakeupManager getLowPaceWakeupManager(Context context, int sportType) {
        if (this.mLowSpeedWakeupManager == null) {
            this.mLowSpeedWakeupManager = new LowSpeedWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mLowSpeedWakeupManager;
    }

    public AbsWakeupManager getTimeCostWakeupManager(Context context, int sportType) {
        if (this.mTimeCostWakeupManager == null) {
            this.mTimeCostWakeupManager = new TimeCostWakeUpManager(context.getApplicationContext(), sportType);
        }
        return this.mTimeCostWakeupManager;
    }

    public AbsWakeupManager getTimerArrivalWakeupManager(Context context, int sportType) {
        if (this.mTimerArrivalWakeupManager == null) {
            this.mTimerArrivalWakeupManager = new SleepTimeWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mTimerArrivalWakeupManager;
    }

    public AbsWakeupManager getRouteOffsetWakeupManager(Context context, int sportType) {
        if (this.mRouteOffsetWakeupManager == null) {
            this.mRouteOffsetWakeupManager = new RouteOffsetWakeUpManager(context.getApplicationContext(), sportType);
        }
        return this.mRouteOffsetWakeupManager;
    }

    public AbsWakeupManager getWearLoosWakupManager(Context context, int sportType) {
        if (this.mWearLooseWakeupManager == null) {
            this.mWearLooseWakeupManager = new WearLooseWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mWearLooseWakeupManager;
    }

    public AbsWakeupManager getCalorieWakeupManager(Context context, int sportType) {
        if (this.mCalorieWakeupManager == null) {
            this.mCalorieWakeupManager = new CalorieWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mCalorieWakeupManager;
    }

    public AbsWakeupManager getLapWakeupManager(Context context, int sportType) {
        if (this.mLapWakeupManager == null) {
            this.mLapWakeupManager = new LapWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mLapWakeupManager;
    }

    public AbsWakeupManager getDailyPromanceSixMinuteWakeupManager(Context context, int sportType) {
        if (this.mDailyPromanceSixMinuteWakeupManager == null) {
            this.mDailyPromanceSixMinuteWakeupManager = new DailyPromanceSixMinuteWakeupManager(context.getApplicationContext(), sportType);
        }
        return this.mDailyPromanceSixMinuteWakeupManager;
    }

    public AbsWakeupManager getRealTimeGuideWakeUpManager(Context context, int sportType) {
        if (this.mRealTimeGuideWakeUpManager == null) {
            this.mRealTimeGuideWakeUpManager = new RealTimeGuideWakeUpManager(context.getApplicationContext(), sportType);
        }
        return this.mRealTimeGuideWakeUpManager;
    }

    public void clearWakeupManager() {
        this.mAutoPauseWakeupManager = null;
        this.mDistanceRepeatWakeupManager = null;
        this.mDistanceRepeatMileWakeupManager = null;
        this.mDistanceWakeupManager = null;
        this.mExtNormalHeartrateWakeupManager = null;
        this.mGPSStatusChangeWakeupManager = null;
        this.mLowSpeedWakeupManager = null;
        this.mTimeCostWakeupManager = null;
        this.mTimerArrivalWakeupManager = null;
        this.mRouteOffsetWakeupManager = null;
        this.mWearLooseWakeupManager = null;
        this.mCalorieWakeupManager = null;
        this.mLapWakeupManager = null;
        this.mDailyPromanceSixMinuteWakeupManager = null;
        this.mRealTimeGuideWakeUpManager = null;
    }
}
