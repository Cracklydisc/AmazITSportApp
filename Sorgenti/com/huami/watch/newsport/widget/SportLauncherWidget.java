package com.huami.watch.newsport.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import clc.sliteplugin.flowboard.AbstractPlugin;
import clc.sliteplugin.flowboard.ISpringBoardHostStub;
import clc.sliteplugin.flowboard.KeyDef;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmHaloButtonWidget;
import com.huami.watch.common.widget.HmHaloImageViewWidget;
import com.huami.watch.common.widget.HmLoadingDrawable;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.SportContentProvider;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import java.util.ArrayList;
import java.util.List;

public class SportLauncherWidget extends AbstractPlugin {
    private static final String TAG = SportLauncherWidget.class.getName();
    private boolean isClickStart = false;
    private Context mContext = null;
    private View mFirstContainer = null;
    private TextView mFirstTitle = null;
    private ImageView mFirstView = null;
    private ISpringBoardHostStub mHost = null;
    private int[] mLastSportArray = new int[]{-1, -1};
    private HmLoadingDrawable mLoadingDrawable;
    private View mLoadingView = null;
    private Button mMoreBtn = null;
    private TextView mRecoveryTime = null;
    private View mRootView = null;
    private View mSecContainer = null;
    private TextView mSecondTitle = null;
    private ImageView mSecondView = null;
    private TextView mThirdTitle = null;

    class C08961 implements OnClickListener {
        C08961() {
        }

        public void onClick(View v) {
            SportLauncherWidget.this.startSportList();
        }
    }

    class C08972 implements OnClickListener {
        C08972() {
        }

        public void onClick(View v) {
            SportLauncherWidget.this.startSportList();
        }
    }

    class C08983 implements OnClickListener {
        C08983() {
        }

        public void onClick(View v) {
            ((HmHaloImageViewWidget) SportLauncherWidget.this.mFirstView).playClickAnimation(null);
            int sportType = SportLauncherWidget.this.mLastSportArray[0];
            if (SportType.isSportTypeValid(sportType)) {
                SportLauncherWidget.this.startSport(sportType);
            }
        }
    }

    class C08994 implements OnClickListener {
        C08994() {
        }

        public void onClick(View v) {
            ((HmHaloImageViewWidget) SportLauncherWidget.this.mSecondView).playClickAnimation(null);
            int sportType = SportLauncherWidget.this.mLastSportArray[1];
            if (SportType.isSportTypeValid(sportType)) {
                SportLauncherWidget.this.startSport(sportType);
            }
        }
    }

    class C09005 implements Runnable {
        C09005() {
        }

        public void run() {
            SportLauncherWidget.this.showLoading(true);
        }
    }

    class C09026 implements Runnable {
        C09026() {
        }

        public void run() {
            if (SportLauncherWidget.this.mContext != null) {
                final List<SportSummary> sportSummaries = new ArrayList();
                try {
                    Cursor cursor = SportLauncherWidget.this.mContext.getContentResolver().query(SportContentProvider.CONTENT_URI_SPORT_INFO, null, "current_status  not in (?,?,?,?)  and  type  not in (?,?,?) ", new String[]{String.valueOf(0), String.valueOf(1), String.valueOf(7), String.valueOf(9), String.valueOf(1015), String.valueOf(1009), String.valueOf(1001)}, "start_time DESC");
                    while (cursor != null && cursor.moveToNext()) {
                        String content = cursor.getString(cursor.getColumnIndex("content"));
                        int currentStatus = cursor.getInt(cursor.getColumnIndex("current_status"));
                        SportSummary summary = OutdoorSportSummary.createFromJSON(content);
                        if (summary != null) {
                            summary.setCurrentStatus(currentStatus);
                            sportSummaries.add(summary);
                        }
                    }
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Debug.m5i(SportLauncherWidget.TAG, "err found in query sport summary, " + e.getLocalizedMessage());
                }
                SportLauncherWidget.this.mHost.runTaskOnUI(SportLauncherWidget.this, new Runnable() {
                    public void run() {
                        SportLauncherWidget.this.updateView(sportSummaries);
                        SportLauncherWidget.this.showLoading(false);
                    }
                });
                SportLauncherWidget.this.mHost.postDataToProvider(SportLauncherWidget.this, 8199, null);
            }
        }
    }

    class C09037 implements Runnable {
        C09037() {
        }

        public void run() {
            Debug.m5i(SportLauncherWidget.TAG, "doReceiveRecoveryTime, bundle is null");
            SportLauncherWidget.this.mRecoveryTime.setVisibility(8);
        }
    }

    class C09048 implements Runnable {
        C09048() {
        }

        public void run() {
            SportLauncherWidget.this.mRecoveryTime.setVisibility(8);
        }
    }

    public void onBindHost(ISpringBoardHostStub iSpringBoardHostStub) {
        this.mHost = iSpringBoardHostStub;
    }

    public View getView(Context context) {
        this.mContext = context;
        this.mRootView = LayoutInflater.from(context).inflate(C0532R.layout.widget_launcher_new_view, null);
        this.mFirstContainer = this.mRootView.findViewById(C0532R.id.first_container);
        this.mSecContainer = this.mRootView.findViewById(C0532R.id.second_container);
        this.mRecoveryTime = (TextView) this.mRootView.findViewById(C0532R.id.recovery_time);
        this.mMoreBtn = (Button) this.mRootView.findViewById(C0532R.id.more_btn);
        ((HmHaloButtonWidget) this.mMoreBtn).setWithHalo(false);
        this.mFirstView = (ImageView) this.mRootView.findViewById(C0532R.id.first_img);
        this.mFirstTitle = (TextView) this.mRootView.findViewById(C0532R.id.first_title);
        this.mSecondView = (ImageView) this.mRootView.findViewById(C0532R.id.second_img);
        this.mSecondTitle = (TextView) this.mRootView.findViewById(C0532R.id.second_title);
        this.mThirdTitle = (TextView) this.mRootView.findViewById(C0532R.id.hide_title);
        this.mLoadingView = this.mRootView.findViewById(C0532R.id.loading_view);
        initLoadingView();
        ((HmHaloImageViewWidget) this.mFirstView).setContentRatio(1.1f);
        ((HmHaloImageViewWidget) this.mSecondView).setContentRatio(1.1f);
        ((HmHaloImageViewWidget) this.mFirstView).setOverlayParent((ViewGroup) this.mFirstContainer);
        ((HmHaloImageViewWidget) this.mSecondView).setOverlayParent((ViewGroup) this.mSecContainer);
        this.mMoreBtn.setOnClickListener(new C08961());
        this.mRootView.setOnClickListener(new C08972());
        this.mFirstContainer.setOnClickListener(new C08983());
        this.mSecContainer.setOnClickListener(new C08994());
        this.mHost.runTaskOnUI(this, new C09005());
        return this.mRootView;
    }

    public void onActive(Bundle saveInstanceData) {
        super.onActive(saveInstanceData);
        Debug.m5i(TAG, "onActive");
        this.isClickStart = false;
        this.mHost.runTaskOnWorkThread(this, new C09026());
    }

    private void updateView(List<SportSummary> sportSummaryList) {
        if (sportSummaryList == null || sportSummaryList.isEmpty()) {
            int firImgRes = getImgRes(1);
            int firtitleRes = getTitleRes(1);
            int secImgRes = getImgRes(6);
            int sectitleRes = getTitleRes(6);
            String firstTitle = refreshView(0, firImgRes, firtitleRes);
            String secondTitle = refreshView(1, secImgRes, sectitleRes);
            this.mThirdTitle.setText(firstTitle);
            this.mLastSportArray[0] = 1;
            this.mLastSportArray[1] = 6;
            return;
        }
        int firstSportType = -1;
        int secSportType = -1;
        if (sportSummaryList.size() > 0) {
            firstSportType = ((SportSummary) sportSummaryList.get(0)).getSportType();
        }
        if (sportSummaryList.size() > 1) {
            for (int i = 1; i < sportSummaryList.size(); i++) {
                int type = ((SportSummary) sportSummaryList.get(i)).getSportType();
                if (type != firstSportType) {
                    secSportType = type;
                    break;
                }
            }
        }
        if (secSportType == -1) {
            if (firstSportType == 1) {
                secSportType = 6;
            } else {
                secSportType = 1;
            }
        }
        firImgRes = getImgRes(firstSportType);
        firtitleRes = getTitleRes(firstSportType);
        secImgRes = getImgRes(secSportType);
        sectitleRes = getTitleRes(secSportType);
        firstTitle = refreshView(0, firImgRes, firtitleRes);
        secondTitle = refreshView(1, secImgRes, sectitleRes);
        if (firstTitle == null || secondTitle == null) {
            this.mThirdTitle.setText(null);
        } else {
            String title = "";
            TextPaint textPaint = this.mThirdTitle.getPaint();
            if (firstTitle != null && textPaint.measureText(firstTitle) > textPaint.measureText(title)) {
                title = firstTitle;
            }
            if (secondTitle != null && textPaint.measureText(secondTitle) > textPaint.measureText(title)) {
                title = secondTitle;
            }
            this.mThirdTitle.setText(title);
            if (!(firstTitle == null || secondTitle == null)) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m5i(TAG, "firstTitle:" + firstTitle.length() + ", secTitle:" + secondTitle.length() + ", width:" + this.mThirdTitle.getWidth() + ", paint width:" + this.mThirdTitle.getPaint().measureText(this.mThirdTitle.getText().toString()) + ",  " + this.mThirdTitle.getText());
                } else {
                    Debug.m5i(TAG, "firstTitle:" + firstTitle.length() + ", secTitle:" + secondTitle.length() + ", width:" + this.mThirdTitle.getWidth() + ", paint width:" + this.mThirdTitle.getPaint().measureText(this.mThirdTitle.getText().toString()) + ",  " + this.mThirdTitle.getText());
                }
                LayoutParams lp;
                if (textPaint.measureText(firstTitle) > textPaint.measureText(secondTitle)) {
                    lp = this.mSecondTitle.getLayoutParams();
                    lp.width = (int) this.mThirdTitle.getPaint().measureText(this.mThirdTitle.getText().toString());
                    this.mSecondTitle.setLayoutParams(lp);
                    this.mSecondTitle.setText(secondTitle);
                    this.mFirstTitle.setLayoutParams(lp);
                } else {
                    lp = this.mFirstTitle.getLayoutParams();
                    lp.width = (int) this.mThirdTitle.getPaint().measureText(this.mThirdTitle.getText().toString());
                    this.mFirstTitle.setLayoutParams(lp);
                    this.mFirstTitle.setText(firstTitle);
                    this.mSecondTitle.setLayoutParams(lp);
                }
            }
        }
        this.mLastSportArray[0] = firstSportType;
        this.mLastSportArray[1] = secSportType;
    }

    private String refreshView(int pos, int imgRes, int titleRes) {
        if (imgRes >= 0) {
            if (pos == 0) {
                this.mFirstView.setImageDrawable(this.mContext.getDrawable(imgRes));
                this.mFirstTitle.setText(this.mContext.getString(titleRes));
            } else if (pos == 1) {
                this.mSecondView.setImageDrawable(this.mContext.getDrawable(imgRes));
                this.mSecondTitle.setText(this.mContext.getString(titleRes));
            }
            return this.mContext.getString(titleRes);
        } else if (pos == 0) {
            this.mFirstView.setImageDrawable(null);
            this.mFirstTitle.setText("");
            return null;
        } else if (pos != 1) {
            return null;
        } else {
            this.mSecondView.setImageDrawable(null);
            this.mSecondTitle.setText("");
            return null;
        }
    }

    private int getImgRes(int sportType) {
        switch (sportType) {
            case 1:
                return C0532R.drawable.sport_menu_running;
            case 6:
                return C0532R.drawable.sport_menu_walk;
            case 7:
                return C0532R.drawable.sport_menu_trail_running;
            case 8:
                return C0532R.drawable.sport_menu_treadmill;
            case 9:
                return C0532R.drawable.sport_menu_outdoor_riding;
            case 10:
                return C0532R.drawable.sport_menu_indoor_riding;
            case 11:
                return C0532R.drawable.sport_menu_skiing;
            case 12:
                return C0532R.drawable.sport_menu_ellipticals;
            case 13:
                return C0532R.drawable.sport_menu_climb_mountain;
            case 14:
                return C0532R.drawable.sport_menu_indoor_swimming;
            case 15:
                return C0532R.drawable.sport_menu_outdoor_swimming;
            case 17:
                return C0532R.drawable.sport_menu_tennis;
            case 18:
                return C0532R.drawable.sport_menu_football;
            case 2001:
                return C0532R.drawable.sport_menu_triathlon;
            case 2002:
                return C0532R.drawable.sport_menu_complex_sport;
            default:
                return -1;
        }
    }

    private int getTitleRes(int sportType) {
        switch (sportType) {
            case 1:
                return C0532R.string.sport_widget_title_run;
            case 6:
                return C0532R.string.sport_widget_title_walk;
            case 7:
                return C0532R.string.sport_widget_title_cross;
            case 8:
                return C0532R.string.sport_widget_title_indoor_run;
            case 9:
                return C0532R.string.sport_widget_title_outdoor_riding;
            case 10:
                return C0532R.string.sport_widget_title_indoor_riding;
            case 11:
                return C0532R.string.sport_widget_title_skiing;
            case 12:
                return C0532R.string.sport_widget_title_elliptical;
            case 13:
                return C0532R.string.sport_widget_title_mountaineer;
            case 14:
                return C0532R.string.sport_widget_title_indoor_swim;
            case 15:
                return C0532R.string.sport_widget_title_open_water_swim;
            case 17:
                return C0532R.string.sport_widget_title_tennis;
            case 18:
                return C0532R.string.sport_widget_title_soccer;
            case 2001:
                return C0532R.string.sport_widget_title_triathlon;
            case 2002:
                return C0532R.string.sport_widget_title_compound;
            default:
                return -1;
        }
    }

    private void startSportList() {
        Debug.m5i(TAG, "click to start sport list, isClickStart:" + this.isClickStart);
        if (!this.isClickStart) {
            if (this.mHost != null) {
                this.mHost.postDataToHost(this, "widget_lock_launcher", null);
            }
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.huami.watch.launcher", "com.huami.watch.launcher.faces.lonelyleaf.LeafStageActivity"));
            intent.putExtra("pkg", "com.huami.watch.newsport");
            intent.putExtra("cls", "com.huami.watch.newsport.widget.SportWidget");
            intent.addFlags(268435456);
            this.mContext.startActivity(intent);
            this.isClickStart = true;
        }
    }

    private void startSport(int sportType) {
        Debug.m5i(TAG, "click to start sport, isClickStart:" + this.isClickStart);
        if (!this.isClickStart) {
            if (this.mHost != null) {
                this.mHost.postDataToHost(this, "widget_lock_launcher", null);
            }
            Intent sportIntent = new Intent("com.huami.watch.sport.action.SPORT_GPS_SEARCH");
            sportIntent.setPackage("com.huami.watch.newsport");
            sportIntent.putExtra("sport_type", sportType);
            sportIntent.addFlags(268435456);
            this.mContext.startActivity(sportIntent);
            this.isClickStart = true;
        }
    }

    public void onInactive(Bundle saveInstanceData) {
        super.onInactive(saveInstanceData);
        Debug.m5i(TAG, "onInactive");
        this.isClickStart = false;
        int[] iArr = this.mLastSportArray;
        this.mLastSportArray[1] = -1;
        iArr[0] = -1;
    }

    public void onReceiveDataFromProvider(int reqId, Bundle data) {
        Debug.m5i(TAG, "onReceiveDataFromProvider, receive:" + reqId);
        super.onReceiveDataFromProvider(reqId, data);
        if (reqId == 8199) {
            doReceiveRecoveryTime(data);
        }
    }

    private void doReceiveRecoveryTime(Bundle bundle) {
        if (bundle == null) {
            this.mHost.runTaskOnUI(this, new C09037());
            return;
        }
        final long restTime = bundle.getLong("com.huami.watch.sport.recovery_remain_time", 0);
        Debug.m5i(TAG, "doReceiveRecoveryTime, restTime:" + restTime);
        if (restTime <= 0) {
            this.mHost.runTaskOnUI(this, new C09048());
        } else {
            this.mHost.runTaskOnUI(this, new Runnable() {
                public void run() {
                    if (Math.round(((float) restTime) / 3600000.0f) <= 0) {
                        SportLauncherWidget.this.mRecoveryTime.setVisibility(8);
                        return;
                    }
                    SportLauncherWidget.this.mRecoveryTime.setVisibility(0);
                    SportLauncherWidget.this.mRecoveryTime.setText(Html.fromHtml(SportLauncherWidget.this.mContext.getString(C0532R.string.remain_time_recovery, new Object[]{String.valueOf(h)})));
                }
            });
        }
    }

    private void initLoadingView() {
        this.mLoadingDrawable = new HmLoadingDrawable(30);
        this.mLoadingDrawable.setStartColor(-1);
        this.mLoadingDrawable.setEndColor(-7829368);
        this.mLoadingDrawable.setRadius(20.0f, 5.0f);
        this.mLoadingView.setBackground(this.mLoadingDrawable);
    }

    private void showLoading(boolean isShow) {
        if (isShow) {
            this.mLoadingView.setVisibility(0);
            this.mLoadingDrawable.start();
            return;
        }
        if (this.mLoadingDrawable.isRunning()) {
            this.mLoadingDrawable.stop();
        }
        this.mLoadingView.setVisibility(4);
    }

    public boolean onKeyClick(KeyDef key) {
        if (key != KeyDef.TOP) {
            if (key == KeyDef.MIDDLE) {
                if (this.mRootView != null) {
                    this.mRootView.performClick();
                }
            } else if (key == KeyDef.BOTTOM) {
            }
        }
        return super.onKeyClick(key);
    }
}
