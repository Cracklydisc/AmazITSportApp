package com.huami.watch.newsport.widget.client;

import android.content.Context;
import android.os.Bundle;
import clc.sliteplugin.flowboard.SpringBoardWidgetClient;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;

public class SportWidgetClient extends SpringBoardWidgetClient {
    public SportWidgetClient(Context context, Class<?> clazz) {
        super(context, clazz);
    }

    protected void onReceiveDataFromWidget(int requestId, Bundle data) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportWidgetClient", "receive data from host. request id " + requestId + ", data " + data);
        }
        if (requestId == 8199) {
            sendRecoveryTime2Client();
        } else if (requestId != 4097) {
            Debug.m3d("SportWidgetClient", "unknown request id from host. request id " + requestId);
        } else {
            sendSportStatusData(SportDataManager.getInstance().getSportStatus(), SportDataManager.getInstance().getLastSportType());
        }
    }

    private void sendRecoveryTime2Client() {
        FirstBeatConfig firstBeatConfig = DataManager.getInstance().getFirstBeatConfig(Global.getApplicationContext());
        long recoveryTime = -1;
        if (firstBeatConfig != null) {
            long remainTime = System.currentTimeMillis() - firstBeatConfig.getSportFinishTime();
            recoveryTime = ((long) (firstBeatConfig.getRecoveryTime() * 60000)) - remainTime;
            Debug.m5i("SportWidgetClient", "sendRecoveryTime2Client, last finish time:" + firstBeatConfig.getSportFinishTime() + ", remainTime:" + remainTime + ", recoveryTime:" + recoveryTime);
        }
        if (recoveryTime > 349200000) {
            recoveryTime = -1;
        }
        Bundle bundle = new Bundle();
        bundle.putLong("com.huami.watch.sport.recovery_remain_time", recoveryTime);
        bundle.putLong("com.huami.watch.sport.recovery_total_time", (long) (firstBeatConfig.getRecoveryTime() * 60000));
        sendDataToWidget(8199, bundle);
    }

    public void sendSportStatusData(int sportStatus, int sportType) {
        int currentStatus;
        if (sportStatus == 2) {
            currentStatus = 3;
        } else if (sportStatus == 1) {
            currentStatus = 2;
        } else {
            currentStatus = 1;
        }
        int type = 0;
        if (sportType == 1) {
            type = 1;
        } else if (sportType == 6) {
            type = 2;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("com.huami.watch.sport.status", currentStatus);
        bundle.putInt("com.huami.watch.sport.type", type);
        sendDataToWidget(8193, bundle);
    }

    public void sendSportStarted(int sportType) {
        sendDataToWidget(8194, createBundle(sportType));
    }

    private Bundle createBundle(int sportType) {
        int type = 0;
        if (sportType == 1) {
            type = 1;
        } else if (sportType == 6) {
            type = 2;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("com.huami.watch.sport.type", type);
        return bundle;
    }

    public void sendSportPaused(int sportType) {
        sendDataToWidget(8195, createBundle(sportType));
    }

    public void sendSportContinued(int sportType) {
        sendDataToWidget(8196, createBundle(sportType));
    }

    public void sendSportStopped(int sportType) {
        sendDataToWidget(8197, createBundle(sportType));
    }
}
