package com.huami.watch.newsport.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import clc.sliteplugin.flowboard.AbstractPlugin;
import clc.sliteplugin.flowboard.ISpringBoardHostStub;
import clc.sliteplugin.flowboard.KeyDef;
import clc.sliteplugin.flowboard.KeyDef.Stage;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.battery.BatteryManager;
import com.huami.watch.newsport.battery.BatteryManager.IBatteryChangedListener;
import com.huami.watch.newsport.common.SportContentProvider;
import com.huami.watch.newsport.common.model.SortInfo;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter.ISportItemClickListener;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.SportMainSettingFace;
import com.huami.watch.newsport.ui.view.SportSettingWheelList;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.wearubc.UbcInterface;
import java.util.HashMap;
import java.util.Map;

public class SportWidget extends AbstractPlugin implements IBatteryChangedListener, ISportItemClickListener {
    public static final int[] BATTERY_RES = new int[]{C0532R.drawable.widget_battery_00, C0532R.drawable.widget_battery_01, C0532R.drawable.widget_battery_02, C0532R.drawable.widget_battery_03, C0532R.drawable.widget_battery_04, C0532R.drawable.widget_battery_05, C0532R.drawable.widget_battery_06, C0532R.drawable.widget_battery_07, C0532R.drawable.widget_battery_08, C0532R.drawable.widget_battery_09, C0532R.drawable.widget_battery_10, C0532R.drawable.widget_battery_11, C0532R.drawable.widget_battery_12, C0532R.drawable.widget_battery_13};
    private static final String TAG = SportWidget.class.getName();
    private SportLauncherItemAdapter mAdapter = null;
    private ImageView mBatteryImg;
    private BatteryManager mBatteryManager = null;
    private TextView mBatteryPercent = null;
    private Context mContext = null;
    private NumberTextView mContinueDisView;
    private NumberTextView mContinueTimeView;
    private TextView mDisUnit = null;
    private boolean mFirstActive = true;
    private BroadcastReceiver mHomeKeyEventReceiver = new C09137();
    private ISpringBoardHostStub mHost = null;
    private LayoutInflater mInflater = null;
    private boolean mIsActive = false;
    public boolean mIsMetric = true;
    private SportMainSettingFace mSportMainSettingFace = null;
    private int mSportType = -1;
    private TextView mSuggestTimeView = null;
    private UIUpdateHandler mUIUpdateHandler = null;
    private SportSettingWheelList mWheelList = null;

    class C09071 implements Runnable {
        C09071() {
        }

        public void run() {
            Context context = SportWidget.this.mHost.getHostWindow().getContext();
            if (context == null || context.getApplicationContext() == null) {
                Debug.m5i(SportWidget.TAG, "the context is null when query sport sort info");
                return;
            }
            try {
                Cursor cursor = context.getContentResolver().query(SportContentProvider.CONTENT_URI_SPORT_SORT_INFO, null, null, null, null);
                final Map<Integer, SortInfo> sortInfoMap = new HashMap();
                while (cursor != null && cursor.moveToNext()) {
                    SortInfo sortInfo = new SortInfo();
                    sortInfo.setType(cursor.getInt(cursor.getColumnIndex("sport_sort_type")));
                    sortInfo.setCount(cursor.getInt(cursor.getColumnIndex("sport_sort_count")));
                    sortInfo.setTime(cursor.getLong(cursor.getColumnIndex("sport_sort_last_time")));
                    sortInfoMap.put(Integer.valueOf(sortInfo.getType()), sortInfo);
                }
                if (cursor != null) {
                    cursor.close();
                }
                SportWidget.this.mHost.runTaskOnUI(SportWidget.this, new Runnable() {
                    public void run() {
                        int index = SportWidget.this.mAdapter.refreshSportSortInfo(sortInfoMap);
                        if (index != -1) {
                            SportWidget.this.mWheelList.scroll((float) (index - SportWidget.this.mWheelList.getCurrentItem()), 100);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C09082 implements Runnable {
        C09082() {
        }

        public void run() {
            SportWidget.this.updateTitles(-1, false);
        }
    }

    class C09093 implements Runnable {
        C09093() {
        }

        public void run() {
            SportWidget.this.scrollToTop();
        }
    }

    class C09104 implements Runnable {
        C09104() {
        }

        public void run() {
            SportWidget.this.mSuggestTimeView.setVisibility(4);
        }
    }

    class C09115 implements Runnable {
        C09115() {
        }

        public void run() {
            SportWidget.this.mSuggestTimeView.setVisibility(4);
        }
    }

    class C09137 extends BroadcastReceiver {
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";
        String SYSTEM_REASON = "reason";

        C09137() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && TextUtils.equals(intent.getStringExtra(this.SYSTEM_REASON), this.SYSTEM_HOME_KEY)) {
                Debug.m5i("test_home", "SYSTEM_HOME_KEY");
                SportWidget.this.mFirstActive = true;
            }
        }
    }

    private static final class UIUpdateHandler extends Handler {
        SportWidget sportRef;

        public UIUpdateHandler(SportWidget f) {
            this.sportRef = f;
        }

        public void handleMessage(Message message) {
            boolean isStarted = true;
            if (this.sportRef == null) {
                Log.i(SportWidget.TAG, "The SportLauncherView object is null");
                return;
            }
            switch (message.what) {
                case 0:
                    this.sportRef.updateViews(message.arg2, message.obj);
                    return;
                case 1:
                    Bundle sportBundle = message.obj;
                    int sportType = sportBundle.getInt("com.huami.watch.sport.type", 1);
                    if (sportBundle.getInt("com.huami.watch.sport.status", 1) == 1) {
                        isStarted = false;
                    }
                    this.sportRef.updateTitles(sportType, isStarted);
                    return;
                case 2:
                    this.sportRef.mHost.postDataToProvider(this.sportRef, 8199, null);
                    this.sportRef.mUIUpdateHandler.sendEmptyMessageDelayed(2, 360000);
                    return;
                default:
                    return;
            }
        }
    }

    public void onBindHost(ISpringBoardHostStub iSpringBoardHostStub) {
        this.mHost = iSpringBoardHostStub;
    }

    public View getView(Context context) {
        Debug.m5i(TAG, "getView");
        this.mFirstActive = true;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mUIUpdateHandler = new UIUpdateHandler(this);
        UbcInterface.initialize(context, "01", String.valueOf(this.mHost.getHostVerCode()));
        ViewGroup container = (ViewGroup) this.mInflater.inflate(C0532R.layout.activity_launcher_view, null);
        this.mDisUnit = (TextView) container.findViewById(C0532R.id.dis_unit);
        this.mSportMainSettingFace = (SportMainSettingFace) container.findViewById(C0532R.id.main_list);
        this.mContinueDisView = (NumberTextView) container.findViewById(C0532R.id.battery_distance_time);
        this.mContinueTimeView = (NumberTextView) container.findViewById(C0532R.id.battery_continue_time);
        this.mBatteryPercent = (TextView) container.findViewById(C0532R.id.battery_percent);
        this.mBatteryImg = (ImageView) container.findViewById(C0532R.id.battery_icon);
        this.mSuggestTimeView = (TextView) container.findViewById(C0532R.id.suggest_reset_time);
        this.mBatteryManager = new BatteryManager(this.mContext);
        this.mSportMainSettingFace.setDismissAllow(true);
        int marginTop = context.getResources().getDimensionPixelSize(C0532R.dimen.settings_item_layout_drawing_top);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        this.mWheelList = new SportSettingWheelList(this.mSportMainSettingFace, new RectF(0.0f, 0.0f, (float) dm.widthPixels, (float) dm.widthPixels));
        this.mWheelList.setMarginTop(marginTop);
        this.mWheelList.getXContext().setParentNeedTouchEvent(true);
        this.mAdapter = new SportLauncherItemAdapter(this.mSportMainSettingFace);
        this.mAdapter.setSportItemClickListener(this);
        this.mWheelList.setViewAdapter(this.mAdapter);
        this.mWheelList.standup(false, true);
        this.mSportMainSettingFace.addDrawingItem(this.mWheelList);
        this.mSportMainSettingFace.postInvalidate();
        return container;
    }

    public void onActive(Bundle saveInstanceData) {
        boolean z = true;
        super.onActive(saveInstanceData);
        Debug.m6w(TAG, "onActive");
        this.mIsActive = true;
        if (this.mHost != null) {
            if (this.mWheelList != null) {
                this.mWheelList.showScrollBar();
            }
            this.mHost.postDataToProvider(this, 4097, null);
            this.mBatteryManager.setBatterListener(this);
            this.mBatteryManager.registerBatteryReceiver();
            if (getMeasurement(this.mContext) != 0) {
                z = false;
            }
            this.mIsMetric = z;
            if (this.mIsMetric) {
                this.mDisUnit.setText(this.mContext.getString(C0532R.string.battery_continue_distance_unit));
            } else {
                this.mDisUnit.setText(this.mContext.getString(C0532R.string.battery_continue_imperial_distance_unit));
            }
            registerHomeKeyReceiver();
            if (this.mFirstActive) {
                this.mFirstActive = false;
                this.mHost.runTaskOnWorkThread(this, new C09071());
            }
        }
    }

    public void onInactive(Bundle saveInstanceData) {
        super.onInactive(saveInstanceData);
        Debug.m6w(TAG, "onInActive");
        this.mIsActive = false;
        this.mBatteryManager.unRegisterBatteryReceiver();
        this.mBatteryManager.setBatterListener(null);
        this.mUIUpdateHandler.removeMessages(2);
        unRegisterHomeKeyReceiver();
    }

    public void onProviderOnline() {
        super.onProviderOnline();
        Debug.m5i(TAG, "sport, onProviderOffline");
        if (this.mHost != null) {
            this.mHost.runTaskOnUI(this, new C09082());
        }
    }

    public void onProviderOffline() {
        super.onProviderOffline();
        Debug.m5i(TAG, "sport, onProviderOnline");
    }

    public void onReceiveDataFromProvider(int i, Bundle bundle) {
        if (this.mContext != null) {
            Debug.m5i(TAG, "sport, onReceiveDataFromProvider: " + i);
            if (i == 8193) {
                this.mUIUpdateHandler.obtainMessage(1, bundle).sendToTarget();
            } else if (i == 8198) {
                Log.i(TAG, "sport,  scoll to top : " + i);
                if (this.mHost != null) {
                    this.mHost.runTaskOnUI(this, new C09093());
                }
            } else if (i == 8199) {
                doReceiveRecoveryTime(bundle);
            } else {
                this.mUIUpdateHandler.obtainMessage(0, 0, i, bundle).sendToTarget();
            }
        }
    }

    private void doReceiveRecoveryTime(Bundle bundle) {
        if (bundle == null) {
            this.mHost.runTaskOnUI(this, new C09104());
            return;
        }
        final long restTime = bundle.getLong("com.huami.watch.sport.recovery_remain_time", 0);
        if (restTime <= 0) {
            this.mHost.runTaskOnUI(this, new C09115());
        } else {
            this.mHost.runTaskOnUI(this, new Runnable() {
                public void run() {
                    int h = Math.round(((float) restTime) / 3600000.0f);
                    SportWidget.this.mSuggestTimeView.setText(SportWidget.this.mContext.getString(C0532R.string.suggest_reset_time, new Object[]{String.valueOf(h)}));
                }
            });
        }
    }

    public void onSportClicked(int sportType) {
        if (this.mIsActive) {
            this.mSportType = sportType;
            Intent sportIntent = new Intent("com.huami.watch.sport.action.SPORT_GPS_SEARCH");
            sportIntent.setPackage("com.huami.watch.newsport");
            sportIntent.putExtra("sport_type", this.mSportType);
            sportIntent.addFlags(805339136);
            this.mContext.startActivity(sportIntent);
        }
    }

    public void onBatteryChanged(int level, int continueHour, float continueDis, int status) {
        Debug.m5i(TAG, "level:" + level + ", hour:" + continueHour + ", dis:" + continueDis + ", status:" + status);
        if (status == 2) {
            this.mBatteryImg.setImageResource(C0532R.drawable.widget_battery_charging);
        } else {
            updateBattery(level);
        }
        if (this.mIsMetric) {
            this.mDisUnit.setText(this.mContext.getString(C0532R.string.battery_continue_distance_unit));
        } else {
            this.mDisUnit.setText(this.mContext.getString(C0532R.string.battery_continue_imperial_distance_unit));
        }
        this.mBatteryPercent.setText(this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(level)}));
        this.mContinueDisView.setText(String.valueOf(getFormatNumber((float) UnitConvertUtils.convertDistanceToMileOrKm((double) continueDis, this.mIsMetric))));
        this.mContinueTimeView.setText(String.valueOf(continueHour));
    }

    public void doBatteryChangedRequest() {
    }

    public void updateBattery(int level) {
        int pos = 13;
        if (this.mBatteryImg != null) {
            if (level / 7 <= 13) {
                pos = level / 7;
            }
            this.mBatteryImg.setImageResource(BATTERY_RES[pos]);
        }
    }

    private void updateViews(int state, Bundle bundle) {
        int sportType = bundle.getInt("com.huami.watch.sport.type");
        boolean isStarted = false;
        if (state == 8194) {
            isStarted = true;
        } else if (state == 8195) {
            isStarted = true;
        } else if (state == 8196) {
            isStarted = true;
        } else if (state == 8197) {
            isStarted = false;
        }
        updateTitles(sportType, isStarted);
    }

    private void updateTitles(int sportType, boolean isStarted) {
        Debug.m5i(TAG, "sport, updateTitles: " + sportType + ", isStarted: " + isStarted);
        this.mAdapter.refreshViewStatus(sportType, isStarted);
    }

    private void scrollToTop() {
        if (this.mWheelList != null) {
            this.mWheelList.resetListPosition();
        }
    }

    private String getFormatNumber(float f) {
        if (f >= 100.0f) {
            return String.valueOf((int) f);
        }
        if (f >= 10.0f) {
            return String.format("%.1f", new Object[]{Float.valueOf(f)});
        }
        return String.format("%.2f", new Object[]{Float.valueOf(f)});
    }

    public boolean onKeyClick(KeyDef key) {
        return super.onKeyClick(key);
    }

    public boolean onKeyLongClick(KeyDef key, Stage seg) {
        return super.onKeyLongClick(key, seg);
    }

    public boolean onKeyLongClickTimeOut(KeyDef key, Stage seg) {
        return super.onKeyLongClickTimeOut(key, seg);
    }

    private void registerHomeKeyReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        this.mContext.registerReceiver(this.mHomeKeyEventReceiver, filter);
    }

    private void unRegisterHomeKeyReceiver() {
        try {
            this.mContext.unregisterReceiver(this.mHomeKeyEventReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getWidgetIcon(Context context) {
        return BitmapFactory.decodeResource(context.getResources(), C0532R.drawable.ic_launcher);
    }

    public String getWidgetTitle(Context context) {
        return context.getResources().getString(C0532R.string.sport_app_name);
    }

    public static final int getMeasurement(Context c) {
        int m = 0;
        try {
            m = Secure.getInt(c.getContentResolver(), "measurement");
            LogUtil.m9i(true, TAG, "measure unit:" + m);
            return m;
        } catch (SettingNotFoundException e) {
            return m;
        }
    }
}
