package com.huami.watch.newsport.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import clc.sliteplugin.flowboard.AbstractPlugin;
import clc.sliteplugin.flowboard.ISpringBoardHostStub;
import clc.sliteplugin.flowboard.KeyDef;
import clc.sliteplugin.flowboard.KeyDef.Stage;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLoadingDrawable;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.SportContentProvider;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.view.ArcPercentWidgetDrawable;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.utils.ClickScaleAnimation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class SportHistoryWidget extends AbstractPlugin {
    private static final String TAG = SportHistoryWidget.class.getName();
    public final String KEY_MEASUREMENT = "measurement";
    public final int VALUE_IMPERIAL = 1;
    public final int VALUE_METRIC = 0;
    private AtomicBoolean activeState = new AtomicBoolean(false);
    private boolean isClickedStart = false;
    private Context mContext = null;
    private TextView mCostTimeView = null;
    private View mHistoryRecorderContainer = null;
    private ISpringBoardHostStub mHost = null;
    private boolean mIsInit = false;
    private boolean mIsUpdatingSportSummary = false;
    private HmLoadingDrawable mLoadingDrawable;
    private View mLoadingView = null;
    private View mNoHistoryContainer = null;
    private ArcPercentWidgetDrawable mPercentDrawable = null;
    private View mRecoveryContainer = null;
    private TextView mRecoveryTimeView = null;
    private HmImageView mSportImgView = null;
    private View mSportLineView = null;
    private TextView mSportSingleCostTimeView = null;
    private TextView mSportSingleDisTitleView = null;
    private TextView mSportSingleDisView = null;
    private TextView mSportSingleTimeTitleView = null;
    private SportSummary mSportSummary = null;
    private TextView mSportTypeTitle = null;
    private View root = null;

    class C08881 implements Runnable {

        class C08861 implements Comparator<SportSummary> {
            C08861() {
            }

            public int compare(SportSummary lhs, SportSummary rhs) {
                if (lhs.getStartTime() > rhs.getStartTime()) {
                    return -1;
                }
                if (lhs.getStartTime() < rhs.getStartTime()) {
                    return 1;
                }
                return 0;
            }
        }

        class C08872 implements Runnable {
            C08872() {
            }

            public void run() {
                SportHistoryWidget.this.showLoading(false);
            }
        }

        C08881() {
        }

        public void run() {
            if (SportHistoryWidget.this.mContext != null) {
                List<SportSummary> sportSummaries = new ArrayList();
                Cursor cursor = SportHistoryWidget.this.mContext.getContentResolver().query(SportContentProvider.CONTENT_URI_SPORT_INFO, null, null, null, null);
                while (cursor != null && cursor.moveToNext()) {
                    if (SportHistoryWidget.this.activeState.get()) {
                        try {
                            String content = cursor.getString(cursor.getColumnIndex("content"));
                            int currentStatus = cursor.getInt(cursor.getColumnIndex("current_status"));
                            SportSummary summary = OutdoorSportSummary.createFromJSON(content);
                            if (summary != null) {
                                summary.setCurrentStatus(currentStatus);
                                sportSummaries.add(summary);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Debug.m5i(SportHistoryWidget.TAG, "err found in query sport summary, " + e.getLocalizedMessage());
                        }
                    } else {
                        Log.i(SportHistoryWidget.TAG, "updateData run: break for active stage changed.");
                        if (cursor != null) {
                            try {
                                cursor.close();
                                return;
                            } catch (Throwable th) {
                                return;
                            }
                        }
                        return;
                    }
                }
                cursor.close();
                List<SportSummary> sportSummariesAvailable = new LinkedList();
                if (sportSummaries != null) {
                    for (SportSummary sportSummary : sportSummaries) {
                        if (sportSummary.getCurrentStatus() == 0 || sportSummary.getCurrentStatus() == 1 || sportSummary.getCurrentStatus() == 7 || sportSummary.getCurrentStatus() == 9 || SportType.isChildSport(sportSummary.getSportType())) {
                            Debug.m6w(SportHistoryWidget.TAG, "sport summary is not finished : " + sportSummary);
                        } else {
                            sportSummariesAvailable.add(sportSummary);
                        }
                    }
                    Collections.sort(sportSummariesAvailable, new C08861());
                }
                if (sportSummariesAvailable == null || sportSummariesAvailable.size() <= 0) {
                    SportHistoryWidget.this.mSportSummary = null;
                } else {
                    SportHistoryWidget.this.mSportSummary = (SportSummary) sportSummariesAvailable.get(0);
                }
                SportHistoryWidget.this.updateView();
                SportHistoryWidget.this.mHost.runTaskOnUI(SportHistoryWidget.this, new C08872());
            }
        }
    }

    class C08892 implements Runnable {
        C08892() {
        }

        public void run() {
            if (SportHistoryWidget.this.mIsInit) {
                if (SportHistoryWidget.this.mSportSummary != null) {
                    SportHistoryWidget.this.mNoHistoryContainer.setVisibility(4);
                    SportHistoryWidget.this.mHistoryRecorderContainer.setVisibility(0);
                    SportHistoryWidget.this.mCostTimeView.setText(SportHistoryWidget.this.getStartTimeString(SportHistoryWidget.this.mContext, (OutdoorSportSummary) SportHistoryWidget.this.mSportSummary));
                    int resourceId = -1;
                    int titleResId = -1;
                    int lineColorResId = -1;
                    if (SportHistoryWidget.this.mSportSummary.getSportType() == 1) {
                        resourceId = C0532R.drawable.sport_history_icon_running;
                        titleResId = C0532R.string.sport_widget_title_run;
                        lineColorResId = C0532R.color.history_line_color_run;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 6) {
                        resourceId = C0532R.drawable.sport_history_icon_walk;
                        titleResId = C0532R.string.sport_widget_title_walk;
                        lineColorResId = C0532R.color.history_line_color_walk;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 7) {
                        resourceId = C0532R.drawable.sport_history_icon_trail_running;
                        titleResId = C0532R.string.sport_widget_title_cross;
                        lineColorResId = C0532R.color.history_line_color_cross;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 8) {
                        resourceId = C0532R.drawable.sport_history_icon_treadmill;
                        titleResId = C0532R.string.sport_widget_title_indoor_run;
                        lineColorResId = C0532R.color.history_line_color_indoor_run;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 9) {
                        resourceId = C0532R.drawable.sport_history_icon_outdoor_riding;
                        titleResId = C0532R.string.sport_widget_title_outdoor_riding;
                        lineColorResId = C0532R.color.history_line_color_outdoor_riding;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 10) {
                        resourceId = C0532R.drawable.sport_history_icon_indoor_riding;
                        titleResId = C0532R.string.sport_widget_title_indoor_riding;
                        lineColorResId = C0532R.color.history_line_color_indoor_riding;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 12) {
                        resourceId = C0532R.drawable.sport_history_icon_ellipticals;
                        titleResId = C0532R.string.sport_widget_title_elliptical;
                        lineColorResId = C0532R.color.history_line_color_elliptical_machine;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 2001) {
                        resourceId = C0532R.drawable.sport_history_icon_triathlon;
                        titleResId = C0532R.string.sport_widget_title_triathlon;
                        lineColorResId = C0532R.color.history_line_color_triathlon;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 14) {
                        resourceId = C0532R.drawable.sport_history_icon_indoor_swimming;
                        titleResId = C0532R.string.sport_widget_title_indoor_swim;
                        lineColorResId = C0532R.color.history_line_color_swimming;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 15) {
                        resourceId = C0532R.drawable.sport_history_icon_outdoor_swimming;
                        titleResId = C0532R.string.sport_widget_title_open_water_swim;
                        lineColorResId = C0532R.color.history_line_color_open_water_swim;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 13) {
                        resourceId = C0532R.drawable.sport_history_icon_climb_mountain;
                        titleResId = C0532R.string.sport_widget_title_mountaineer;
                        lineColorResId = C0532R.color.history_line_color_mountaineer;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 11) {
                        resourceId = C0532R.drawable.sport_history_icon_skiing;
                        titleResId = C0532R.string.sport_widget_title_skiing;
                        lineColorResId = C0532R.color.history_line_color_swimming;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 17) {
                        resourceId = C0532R.drawable.sport_history_icon_tennis;
                        titleResId = C0532R.string.sport_widget_title_tennis;
                        lineColorResId = C0532R.color.history_line_color_swimming;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 18) {
                        resourceId = C0532R.drawable.sport_history_icon_football;
                        titleResId = C0532R.string.sport_widget_title_soccer;
                        lineColorResId = C0532R.color.history_line_color_swimming;
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 2002) {
                        resourceId = C0532R.drawable.sport_history_icon_complex_sport;
                        titleResId = C0532R.string.sport_widget_title_compound;
                        lineColorResId = C0532R.color.history_line_color_triathlon;
                    }
                    SportHistoryWidget.this.mSportImgView.setImageDrawable(SportHistoryWidget.this.mContext.getDrawable(resourceId));
                    SportHistoryWidget.this.mSportTypeTitle.setText(SportHistoryWidget.this.mContext.getString(titleResId));
                    SportHistoryWidget.this.mSportLineView.setBackgroundColor(SportHistoryWidget.this.mContext.getResources().getColor(lineColorResId));
                    if (SportType.isSportTypeNeedDis(SportHistoryWidget.this.mSportSummary.getSportType())) {
                        if (SportType.isSwimMode(SportHistoryWidget.this.mSportSummary.getSportType())) {
                            SportHistoryWidget.this.mSportSingleDisTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.detail_lap_dis));
                            if (((OutdoorSportSummary) SportHistoryWidget.this.mSportSummary).getUnit() == 0) {
                                SportHistoryWidget.this.mSportSingleDisView.setText("" + ((int) UnitConvertUtils.convertDistanceToFtOrMeter((double) SportHistoryWidget.this.mSportSummary.getDistance(), true)));
                            } else {
                                SportHistoryWidget.this.mSportSingleDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) SportHistoryWidget.this.mSportSummary.getDistance()), ((OutdoorSportSummary) SportHistoryWidget.this.mSportSummary).getUnit())));
                            }
                        } else {
                            SportHistoryWidget.this.mSportSingleDisTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.running_distance_desc));
                            if (SportHistoryWidget.this.mSportSummary.getSportType() == 11) {
                                SportHistoryWidget.this.mSportSingleDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) ((OutdoorSportSummary) SportHistoryWidget.this.mSportSummary).getClimbdisDescend(), SportHistoryWidget.this.isMetric()), false));
                            } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 17) {
                                SportHistoryWidget.this.mSportSingleDisTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.sport_tennis_strokes));
                                SportHistoryWidget.this.mSportSingleDisView.setText(String.valueOf(((OutdoorSportSummary) SportHistoryWidget.this.mSportSummary).getmStrokes()));
                            } else {
                                SportHistoryWidget.this.mSportSingleDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) SportHistoryWidget.this.mSportSummary.getDistance(), SportHistoryWidget.this.isMetric()), false));
                            }
                        }
                    } else if (SportHistoryWidget.this.mSportSummary.getSportType() == 17) {
                        SportHistoryWidget.this.mSportSingleDisTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.sport_tennis_strokes));
                        SportHistoryWidget.this.mSportSingleDisView.setText(String.valueOf(((OutdoorSportSummary) SportHistoryWidget.this.mSportSummary).getmStrokes()));
                    } else {
                        SportHistoryWidget.this.mSportSingleDisTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.running_calorie_desc));
                        SportHistoryWidget.this.mSportSingleDisView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) SportHistoryWidget.this.mSportSummary.getCalorie()))}));
                    }
                    SportHistoryWidget.this.mSportSingleTimeTitleView.setText(SportHistoryWidget.this.mContext.getString(C0532R.string.running_cost_time_desc));
                    SportHistoryWidget.this.mSportSingleCostTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime((long) SportHistoryWidget.this.mSportSummary.getTrimSportDuration()));
                    LogUtils.print(SportHistoryWidget.TAG, "mRightContainer:" + DataFormatUtils.parseMillSecondToDefaultFormattedTime((long) SportHistoryWidget.this.mSportSummary.getTrimSportDuration()));
                } else {
                    SportHistoryWidget.this.mCostTimeView.setText("");
                    SportHistoryWidget.this.mSportImgView.setImageDrawable(null);
                    SportHistoryWidget.this.mSportSingleTimeTitleView.setText("");
                    SportHistoryWidget.this.mSportSingleCostTimeView.setText("");
                    SportHistoryWidget.this.mSportSingleDisTitleView.setText("");
                    SportHistoryWidget.this.mSportSingleDisView.setText("");
                    SportHistoryWidget.this.mRecoveryContainer.setVisibility(4);
                    SportHistoryWidget.this.mNoHistoryContainer.setVisibility(0);
                    SportHistoryWidget.this.mHistoryRecorderContainer.setVisibility(4);
                }
                SportHistoryWidget.this.mIsUpdatingSportSummary = false;
            }
        }
    }

    class C08913 implements OnClickListener {

        class C08901 extends AnimatorListenerAdapter {
            C08901() {
            }

            public void onAnimationEnd(Animator animation) {
                SportHistoryWidget.this.startHistoryView();
            }
        }

        C08913() {
        }

        public void onClick(View v) {
            if (SportHistoryWidget.this.mHost != null) {
                SportHistoryWidget.this.mHost.postDataToHost(SportHistoryWidget.this, "widget_lock_launcher", null);
            }
            new ClickScaleAnimation(v, new C08901()).startClickAnimation();
        }
    }

    class C08924 implements Runnable {
        C08924() {
        }

        public void run() {
            SportHistoryWidget.this.showLoading(true);
        }
    }

    class C08935 implements Runnable {
        C08935() {
        }

        public void run() {
            SportHistoryWidget.this.mRecoveryContainer.setVisibility(4);
        }
    }

    class C08946 implements Runnable {
        C08946() {
        }

        public void run() {
            SportHistoryWidget.this.mRecoveryContainer.setVisibility(4);
        }
    }

    public void onBindHost(ISpringBoardHostStub iSpringBoardHostStub) {
        this.mHost = iSpringBoardHostStub;
    }

    public void onInactive(Bundle saveInstanceData) {
        super.onInactive(saveInstanceData);
        this.activeState.set(false);
        this.isClickedStart = false;
    }

    public void onActive(Bundle saveInstanceData) {
        super.onActive(saveInstanceData);
        this.activeState.set(true);
        this.isClickedStart = false;
        updateData();
    }

    private void updateData() {
        this.mIsUpdatingSportSummary = true;
        this.mHost.runTaskOnWorkThread(this, new C08881());
    }

    private void updateView() {
        this.mHost.runTaskOnUI(this, new C08892());
    }

    public View getView(Context context) {
        this.mContext = context;
        this.root = LayoutInflater.from(context).inflate(C0532R.layout.widget_sport_history, null);
        this.mHistoryRecorderContainer = this.root.findViewById(C0532R.id.record_container);
        this.mNoHistoryContainer = this.root.findViewById(C0532R.id.no_history_container);
        this.mCostTimeView = (TextView) this.root.findViewById(C0532R.id.date_title);
        this.mSportImgView = (HmImageView) this.root.findViewById(C0532R.id.sport_type_img);
        this.mSportLineView = this.root.findViewById(C0532R.id.sport_line);
        this.mSportTypeTitle = (TextView) this.root.findViewById(C0532R.id.sport_type);
        this.mSportSingleTimeTitleView = (TextView) this.root.findViewById(C0532R.id.time_title);
        this.mSportSingleDisTitleView = (TextView) this.root.findViewById(C0532R.id.dis_title);
        this.mSportSingleDisView = (TextView) this.root.findViewById(C0532R.id.dis_value);
        this.mSportSingleCostTimeView = (TextView) this.root.findViewById(C0532R.id.time_value);
        this.mRecoveryContainer = this.root.findViewById(C0532R.id.recovery_container);
        this.mRecoveryTimeView = (TextView) this.root.findViewById(C0532R.id.remain_recovery_time);
        this.mPercentDrawable = new ArcPercentWidgetDrawable(context.getResources().getDimension(C0532R.dimen.widget_size_arc_base_width), context.getResources().getColor(C0532R.color.widget_color_widget), context.getResources().getDimension(C0532R.dimen.widget_size_arc_width), context.getResources().getColor(C0532R.color.widget_color_arc), 0.0f, 0.0f, 0.0f, 0, 0.0f, context.getResources().getColor(C0532R.color.widget_color_arc));
        this.mPercentDrawable.setAngleRange(-235.0f, 290.0f);
        this.root.setOnClickListener(new C08913());
        this.mIsInit = true;
        this.mLoadingView = this.root.findViewById(C0532R.id.loading_view);
        initLoadingView();
        this.mHost.runTaskOnUI(this, new C08924());
        return this.root;
    }

    private void startHistoryView() {
        Debug.m5i(TAG, "startHistoryView");
        if (!this.isClickedStart) {
            if (this.mHost != null) {
                this.mHost.postDataToHost(this, "widget_lock_launcher", null);
            }
            this.isClickedStart = true;
            Intent intent = new Intent("com.huami.watch.sport.action.SPORT_HISTORY");
            intent.setPackage("com.huami.watch.newsport");
            intent.addFlags(268468224);
            this.mContext.startActivity(intent);
        }
    }

    public String getStartTimeString(Context mActivity, OutdoorSportSummary sportSummary) {
        if (DateFormat.is24HourFormat(mActivity)) {
            return DataFormatUtils.parseMilliSecondToFormattedTime(sportSummary.getStartTime(), "MM/dd HH:mm", true);
        }
        String amOrPm;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sportSummary.getStartTime());
        if (calendar.get(9) == 0) {
            amOrPm = mActivity.getString(C0532R.string.date_am);
        } else {
            amOrPm = mActivity.getString(C0532R.string.date_pm);
        }
        return DataFormatUtils.parseMilliSecondToFormattedTime(sportSummary.getStartTime(), "MM/dd " + amOrPm + " hh:mm", true);
    }

    public void onReceiveDataFromProvider(int reqId, Bundle data) {
        super.onReceiveDataFromProvider(reqId, data);
        if (reqId == 8199) {
            doReceiveRecoveryTime(data);
        }
    }

    private void doReceiveRecoveryTime(Bundle bundle) {
        if (bundle == null) {
            this.mHost.runTaskOnUI(this, new C08935());
            return;
        }
        final long totalTime = bundle.getLong("com.huami.watch.sport.recovery_total_time", 0);
        final long restTime = bundle.getLong("com.huami.watch.sport.recovery_remain_time", 0);
        if (restTime <= 0) {
            this.mHost.runTaskOnUI(this, new C08946());
        } else {
            this.mHost.runTaskOnUI(this, new Runnable() {
                public void run() {
                    int h = (int) ((restTime + 1800000) / 3600000);
                    if (h >= 0) {
                        SportHistoryWidget.this.mRecoveryContainer.setVisibility(0);
                        SportHistoryWidget.this.mRecoveryTimeView.setText("" + h);
                        SportHistoryWidget.this.mPercentDrawable.setPercent(((float) restTime) / ((float) totalTime));
                    }
                }
            });
        }
    }

    public boolean isMetric() {
        Context context = this.mHost.getHostWindow().getContext();
        if (context == null || context.getApplicationContext() == null) {
            Debug.m5i(TAG, "isMetric, context is null");
            return true;
        }
        int m = 0;
        try {
            m = Secure.getInt(context.getContentResolver(), "measurement");
        } catch (SettingNotFoundException e) {
        }
        Debug.m5i(TAG, "isMetric, m:" + m);
        if (m != 0) {
            return false;
        }
        return true;
    }

    private void initLoadingView() {
        this.mLoadingDrawable = new HmLoadingDrawable(30);
        this.mLoadingDrawable.setStartColor(-1);
        this.mLoadingDrawable.setEndColor(-7829368);
        this.mLoadingDrawable.setRadius(20.0f, 5.0f);
        this.mLoadingView.setBackground(this.mLoadingDrawable);
    }

    private void showLoading(boolean isShow) {
        if (isShow) {
            this.mLoadingView.setVisibility(0);
            this.mLoadingDrawable.start();
            return;
        }
        if (this.mLoadingDrawable.isRunning()) {
            this.mLoadingDrawable.stop();
        }
        this.mLoadingView.setVisibility(4);
    }

    public Bitmap getWidgetIcon(Context context) {
        return ((BitmapDrawable) this.mContext.getResources().getDrawable(C0532R.drawable.lan_applist_icon_sport_record)).getBitmap();
    }

    public String getWidgetTitle(Context context) {
        return this.mContext.getResources().getString(C0532R.string.history_title);
    }

    public Intent getWidgetIntent() {
        return null;
    }

    public boolean onKeyClick(KeyDef key) {
        if (key != KeyDef.TOP) {
            if (key == KeyDef.MIDDLE) {
                if (this.root != null) {
                    this.root.performClick();
                }
            } else if (key == KeyDef.BOTTOM) {
            }
        }
        return super.onKeyClick(key);
    }

    public boolean onKeyLongClick(KeyDef key, Stage seg) {
        return super.onKeyLongClick(key, seg);
    }

    public boolean onKeyLongClickTimeOut(KeyDef key, Stage seg) {
        return super.onKeyLongClickTimeOut(key, seg);
    }
}
