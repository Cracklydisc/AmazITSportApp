package com.huami.watch.newsport;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.newsport.ui.fragps.SportTargetRemindFragment;
import com.huami.watch.newsport.ui.fragps.SportTargetRemindFragment.ITargetRecommandListener;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager;

public class SportTargetRecommandActivity extends BaseActivity implements EventCallBack, ITargetRecommandListener {
    private static final String TAG = SportTargetRecommandActivity.class.getName();
    private SportTargetRemindFragment mFragment = null;
    private int mSportType = -1;

    class C05371 implements OnDismissListener {
        C05371() {
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
            DataManager.getInstance().saveBindDeviceState(SportTargetRecommandActivity.this, false);
            SportTargetRecommandActivity.this.onStartBtnClicked();
        }
    }

    class C05382 implements OnClickListener {
        C05382() {
        }

        public void onClick(DialogInterface dialog, int which) {
            SportTargetRecommandActivity.this.startBindDeviceActivity();
        }
    }

    class C05393 implements OnClickListener {
        C05393() {
        }

        public void onClick(DialogInterface dialog, int which) {
            DataManager.getInstance().saveBindDeviceState(SportTargetRecommandActivity.this, false);
            SportTargetRecommandActivity.this.onStartBtnClicked();
        }
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_widget_setting_layout);
            this.mFragment = (SportTargetRemindFragment) BaseFragment.newInstance(this.mSportType, 2);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(C0532R.id.container, this.mFragment);
            transaction.commitAllowingStateLoss();
            setKeyEventListener(this);
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    public void onStartBtnClicked() {
        HmSensorHubConfigManager.getHmSensorHubConfigManager(this).setIsUseThaWorkoutSet(true);
        if (!UnitConvertUtils.isOverSeaVersion()) {
            DataManager.getInstance();
            if (DataManager.isDeviceExperience(this) && DataManager.getInstance().isShownBindDeviceTips(this)) {
                showExperienceMode();
                return;
            }
        }
        Intent intent = new Intent();
        intent.putExtra("is_start_sport", true);
        setResult(8705, intent);
        finish();
    }

    private void showExperienceMode() {
        new Builder(this).setMessage((int) C0532R.string.experience_title).setMessageTextSize(16.0f).setMessageTextColor(-1).setNegativeButton(getString(C0532R.string.decision_no), new C05393()).setPositiveButton(getString(C0532R.string.decision_yes), new C05382()).setOnDismissListener(new C05371()).create().show();
    }

    private void startBindDeviceActivity() {
        try {
            Intent bindIntent = new Intent("com.huami.mobile.watchsettings.action.init.personalinfo");
            bindIntent.setPackage("com.huami.mobile.watchsettings");
            bindIntent.addCategory("android.intent.category.DEFAULT");
            startActivityForResult(bindIntent, 103);
            overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
        } catch (ActivityNotFoundException e) {
            Debug.m3d(TAG, "bluetooth activity not found");
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 103) {
            DataManager.getInstance().saveBindDeviceState(this, false);
            onStartBtnClicked();
        }
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            PointUtils.recordEventProperty("0007", this.mSportType);
            onStartBtnClicked();
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }
}
