package com.huami.watch.newsport.common.model.snapshot;

import com.huami.watch.common.JSONAble;

public abstract class SportSnapshot implements JSONAble {
    protected float mCalorie = 0.0f;
    protected int mCurrentMillSec = 0;
    protected int mCurrentPausedSecond = 0;
    protected int mCurrentSecond = 0;
    protected byte mCurrentStatus = (byte) 3;
    protected int mHeartQuantity = Integer.MAX_VALUE;
    protected int mHeartRange = -1;
    protected float mHeartRate = 0.0f;
    protected byte mPointStatus = (byte) 1;
    protected int mSportType = -1;
    protected long mStartTime = -1;
    protected long mTimestamp = -1;
    protected long mTotalTime = 0;
    protected long mTrackId = -1;

    public static final class CurrentStatusType {
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public static final class PointType {
    }

    public void setHeartRange(int heartRange) {
        this.mHeartRange = heartRange;
    }

    public int getHeartRange() {
        return this.mHeartRange;
    }

    public void setHeartQuantity(int heartQuantity) {
        this.mHeartQuantity = heartQuantity;
    }

    public int getHeartQuantity() {
        return this.mHeartQuantity;
    }

    public byte getCurrentStatus() {
        return this.mCurrentStatus;
    }

    public void setCurrentStatus(byte currentStatus) {
        this.mCurrentStatus = currentStatus;
    }

    public int getCurrentPausedSecond() {
        return this.mCurrentPausedSecond;
    }

    public void setCurrentPausedSecond(int currentPausedSecond) {
        this.mCurrentPausedSecond = currentPausedSecond;
    }

    public String toString() {
        return "SportSnapshot{mTrackId=" + this.mTrackId + ", mTimestamp=" + this.mTimestamp + ", mTotalTime=" + this.mTotalTime + ", mStartTime=" + this.mStartTime + ", mCalorie=" + this.mCalorie + ", mHeartRate=" + this.mHeartRate + ", mSportType=" + this.mSportType + ", mPointStatus=" + this.mPointStatus + ", mCurrentStatus=" + this.mCurrentStatus + ", mCurrentSecond=" + this.mCurrentSecond + ", mCurrentPausedSecond=" + this.mCurrentPausedSecond + ", mCurrentMillSec=" + this.mCurrentMillSec + ", mHeartRange=" + this.mHeartRange + '}';
    }

    public int getCurrentSecond() {
        return this.mCurrentSecond;
    }

    public void setCurrentSecond(int currentSecond) {
        this.mCurrentSecond = currentSecond;
    }

    public int getCurrentMillSec() {
        return this.mCurrentMillSec;
    }

    public void setCurrentMillSec(int millSec) {
        this.mCurrentMillSec = millSec;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public long getTotalTime() {
        return this.mTotalTime;
    }

    public void setTotalTime(long totalTime) {
        this.mTotalTime = totalTime;
    }

    public void setStartTime(long startTime) {
        this.mStartTime = startTime;
    }

    public float getCalorie() {
        return this.mCalorie;
    }

    public void setCalorie(float calorie) {
        this.mCalorie = calorie;
    }

    public float getHeartRate() {
        return this.mHeartRate;
    }

    public void setHeartRate(float heartRate) {
        this.mHeartRate = heartRate;
    }

    public int getSportType() {
        return this.mSportType;
    }
}
