package com.huami.watch.newsport.common.model.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.train.model.TrainSnapShot;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;

public class OutdoorSportSnapshot extends SportSnapshot implements Parcelable {
    public static Creator<OutdoorSportSnapshot> CREATOR = new C05611();
    protected int intervalType;
    protected float mAbsoluteAtitude;
    protected float mAltitude;
    protected long mAscendTime;
    protected float mAveCadence;
    private float mAvgDistancePerStroke;
    private float mAvgStrokeSpeed;
    private int mBackHand;
    protected float mBestPace;
    protected float mCadence;
    protected float mClimbDown;
    protected float mClimbUp;
    protected float mClimbdisDescend;
    protected float mClimbdisascend;
    protected Long[] mCurGPSLonLatPoint;
    protected long mDescendTime;
    private int mDeviceBrand;
    private int mDeviceType;
    protected float mDistance;
    private int mDownHillNum;
    private float mDownhillAvgSpeed;
    private float mDownhillMaxSpeed;
    private int mETEdailyPerformance;
    private int mForeHand;
    protected float mHighAltitude;
    protected long mIndividaulCostTime;
    protected float mIndividualDistance;
    protected float mIndividualPace;
    protected boolean mIsRemindGPS;
    protected long mLastKMCostTime;
    protected float mLastKMPace;
    protected int mLastKilometer;
    protected double mLatitude;
    protected double mLongtitude;
    protected float mLowAltitude;
    protected float mMaxCadence;
    protected float mMaxStepFreq;
    private float mMaxStrokeSpeed;
    protected float mMinPace;
    protected long mMixedTotalTime;
    private float mOffsetAltitude;
    protected long mPreviousLapsCostTime;
    protected float mPreviousLapsDistance;
    protected float mRealTimePace;
    protected float mRealTimeSpeed;
    protected float mRealTimeStepFreq;
    protected float mRtCurLapAvgPace;
    protected float mRtCurLapMaxDownhillElveLoss;
    private float mRtDistancePerStroke;
    private float mRtStrokeSpeed;
    protected long mSecHalfStartTime;
    private int mServe;
    private float mSingleDownhillDescend;
    private float mSingleDownhillElveLoss;
    private int mSportEteResourceRecovery;
    private float mSportEteTraingLoadPeak;
    private int mSportEteTrainingEffect;
    private float mSportEteVo2Max;
    private float mSportSetTotalCalories;
    private float mSportSetTotalDistance;
    private long mSportSetTotalTime;
    private int mSportThaTrainLoadTrend;
    private int mSportVo2MaxTread;
    protected int mStepCount;
    protected float mStepFreq;
    private int mStrokes;
    private int mSwimStyle;
    private int mSwolfPerFixedMeters;
    protected float mTotalPace;
    protected float mTotalSpeed;
    private int mTotalStrokes;
    private int mTotalTrips;
    private TrainSnapShot mTrainSnapShot;
    private int mUnit;
    protected float mVerticalVelocity;
    private SportThaInfo sportThaInfo;

    static class C05611 implements Creator<OutdoorSportSnapshot> {
        C05611() {
        }

        public OutdoorSportSnapshot createFromParcel(Parcel source) {
            OutdoorSportSnapshot status = new OutdoorSportSnapshot();
            status.mLatitude = (double) source.readFloat();
            status.mLongtitude = (double) source.readFloat();
            status.mAltitude = source.readFloat();
            status.mRealTimePace = source.readFloat();
            status.mRealTimeSpeed = source.readFloat();
            status.mTotalPace = source.readFloat();
            status.mTotalSpeed = source.readFloat();
            status.mDistance = source.readFloat();
            status.mRealTimeStepFreq = source.readFloat();
            status.mStepCount = source.readInt();
            status.mTrackId = source.readLong();
            status.mTimestamp = source.readLong();
            status.mTotalTime = source.readLong();
            status.mStartTime = source.readLong();
            status.mCalorie = source.readFloat();
            status.mHeartRate = source.readFloat();
            status.mSportType = source.readInt();
            status.mPointStatus = source.readByte();
            status.mCurrentSecond = source.readInt();
            status.mCurrentPausedSecond = source.readInt();
            status.mCurrentStatus = source.readByte();
            status.mStepFreq = source.readFloat();
            status.mCurrentMillSec = source.readInt();
            status.mVerticalVelocity = source.readFloat();
            status.mBestPace = source.readFloat();
            status.mMinPace = source.readFloat();
            status.mMaxStepFreq = source.readFloat();
            status.mAbsoluteAtitude = source.readFloat();
            status.mClimbUp = source.readFloat();
            status.mClimbDown = source.readFloat();
            status.mHighAltitude = source.readFloat();
            status.mLowAltitude = source.readFloat();
            status.mIndividualDistance = source.readFloat();
            status.mIndividaulCostTime = source.readLong();
            status.mIndividualPace = source.readFloat();
            status.mPreviousLapsCostTime = source.readLong();
            status.mPreviousLapsDistance = source.readFloat();
            status.mClimbdisascend = source.readFloat();
            status.mCadence = source.readFloat();
            status.mMaxCadence = source.readFloat();
            status.mAveCadence = source.readFloat();
            status.mDeviceType = source.readInt();
            status.mDeviceBrand = source.readInt();
            status.mClimbdisDescend = source.readFloat();
            status.mAscendTime = source.readLong();
            status.mDescendTime = source.readLong();
            status.mMixedTotalTime = source.readLong();
            status.mSwolfPerFixedMeters = source.readInt();
            status.mTotalStrokes = source.readInt();
            status.mTotalTrips = source.readInt();
            status.mRtDistancePerStroke = source.readFloat();
            status.mRtStrokeSpeed = source.readFloat();
            status.mAvgStrokeSpeed = source.readFloat();
            status.mMaxStrokeSpeed = source.readFloat();
            status.mAvgDistancePerStroke = source.readFloat();
            status.mSportSetTotalTime = source.readLong();
            status.mSportSetTotalDistance = source.readFloat();
            status.mSportSetTotalCalories = source.readFloat();
            status.mSportEteTraingLoadPeak = (float) source.readInt();
            status.mSportEteTrainingEffect = source.readInt();
            status.mSportEteVo2Max = source.readFloat();
            status.mSportEteResourceRecovery = source.readInt();
            status.mSportVo2MaxTread = source.readInt();
            status.mSportThaTrainLoadTrend = source.readInt();
            status.mSwimStyle = source.readInt();
            status.mCurGPSLonLatPoint = (Long[]) source.readArray(Long.class.getClassLoader());
            status.mUnit = source.readInt();
            status.mTrainSnapShot = (TrainSnapShot) source.readParcelable(TrainSnapShot.class.getClassLoader());
            status.intervalType = source.readInt();
            status.mOffsetAltitude = source.readFloat();
            status.mDownHillNum = source.readInt();
            status.mSingleDownhillDescend = source.readFloat();
            status.mSingleDownhillElveLoss = source.readFloat();
            status.mDownhillAvgSpeed = source.readFloat();
            status.mDownhillMaxSpeed = source.readFloat();
            status.mStrokes = source.readInt();
            status.mForeHand = source.readInt();
            status.mBackHand = source.readInt();
            status.mServe = source.readInt();
            status.mRtCurLapMaxDownhillElveLoss = source.readFloat();
            status.mRtCurLapAvgPace = source.readFloat();
            status.mSecHalfStartTime = source.readLong();
            return status;
        }

        public OutdoorSportSnapshot[] newArray(int size) {
            return new OutdoorSportSnapshot[0];
        }
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public long getSecHalfStartTime() {
        return this.mSecHalfStartTime;
    }

    public void setSecHalfStartTime(long secHalfStartTime) {
        this.mSecHalfStartTime = secHalfStartTime;
    }

    public float getRtCurLapMaxDownhillElveLoss() {
        return this.mRtCurLapMaxDownhillElveLoss;
    }

    public void setRtCurLapMaxDownhillElveLoss(float rtCurLapMaxDownhillElveLoss) {
        this.mRtCurLapMaxDownhillElveLoss = rtCurLapMaxDownhillElveLoss;
    }

    public float getRtCurLapAvgPace() {
        return this.mRtCurLapAvgPace;
    }

    public void setRtCurLapAvgPace(float rtCurLapAvgPace) {
        this.mRtCurLapAvgPace = rtCurLapAvgPace;
    }

    public int getmStrokes() {
        return this.mStrokes;
    }

    public void setmStrokes(int strokes) {
        this.mStrokes = strokes;
    }

    public void setmForeHand(int mForeHand) {
        this.mForeHand = mForeHand;
    }

    public void setmBackHand(int mBackHand) {
        this.mBackHand = mBackHand;
    }

    public void setmServe(int mServe) {
        this.mServe = mServe;
    }

    public int getDownHillNum() {
        return this.mDownHillNum;
    }

    public void setDownHillNum(int downHillNum) {
        this.mDownHillNum = downHillNum;
    }

    public float getSingleDownhillDescend() {
        return this.mSingleDownhillDescend;
    }

    public void setSingleDownhillDescend(float singleDownhillDescend) {
        this.mSingleDownhillDescend = singleDownhillDescend;
    }

    public float getDownhillAvgSpeed() {
        return this.mDownhillAvgSpeed;
    }

    public void setDownhillAvgSpeed(float downhillAvgSpeed) {
        this.mDownhillAvgSpeed = downhillAvgSpeed;
    }

    public float getSingleDownhillElveLoss() {
        return this.mSingleDownhillElveLoss;
    }

    public void setSingleDownhillElveLoss(float singleDownhillElveLoss) {
        this.mSingleDownhillElveLoss = singleDownhillElveLoss;
    }

    public float getDownhillMaxSpeed() {
        return this.mDownhillMaxSpeed;
    }

    public void setDownhillMaxSpeed(float downhillMaxSpeed) {
        this.mDownhillMaxSpeed = downhillMaxSpeed;
    }

    public void setOffsetAltitude(float offset) {
        this.mOffsetAltitude = offset;
    }

    public float getOffsetAltitude() {
        return this.mOffsetAltitude;
    }

    public TrainSnapShot getTrainSnapShot() {
        return this.mTrainSnapShot;
    }

    public int getUnit() {
        return this.mUnit;
    }

    public void setUnit(int unit) {
        this.mUnit = unit;
    }

    public void setIntervalType(int intervalType) {
        this.intervalType = intervalType;
    }

    private OutdoorSportSnapshot() {
        this.mLatitude = 1000.0d;
        this.mLongtitude = 1000.0d;
        this.mAltitude = 0.0f;
        this.mRealTimePace = 0.0f;
        this.mRealTimeSpeed = 0.0f;
        this.mTotalPace = 0.0f;
        this.mTotalSpeed = 0.0f;
        this.mDistance = 0.0f;
        this.mRealTimeStepFreq = 0.0f;
        this.mStepCount = 0;
        this.mLastKilometer = 0;
        this.mLastKMPace = 0.0f;
        this.mLastKMCostTime = 0;
        this.mStepFreq = 0.0f;
        this.mVerticalVelocity = 0.0f;
        this.mBestPace = -1.0f;
        this.mMinPace = -1.0f;
        this.mMaxStepFreq = 0.0f;
        this.mAbsoluteAtitude = -20000.0f;
        this.mClimbUp = -1.0f;
        this.mClimbDown = -1.0f;
        this.mHighAltitude = -20000.0f;
        this.mLowAltitude = -20000.0f;
        this.mIndividualDistance = 0.0f;
        this.mIndividaulCostTime = 0;
        this.mIndividualPace = 0.0f;
        this.mPreviousLapsCostTime = 0;
        this.mPreviousLapsDistance = 0.0f;
        this.mClimbdisascend = -1.0f;
        this.mClimbdisDescend = -1.0f;
        this.mAscendTime = -1;
        this.mDescendTime = -1;
        this.mCadence = -1.0f;
        this.mMaxCadence = -1.0f;
        this.mAveCadence = -1.0f;
        this.mMixedTotalTime = 0;
        this.intervalType = 0;
        this.mDeviceType = -1;
        this.mDeviceBrand = -1;
        this.mSwolfPerFixedMeters = 0;
        this.mTotalStrokes = 0;
        this.mTotalTrips = 0;
        this.mRtDistancePerStroke = 0.0f;
        this.mRtStrokeSpeed = 0.0f;
        this.mAvgStrokeSpeed = 0.0f;
        this.mMaxStrokeSpeed = 0.0f;
        this.mAvgDistancePerStroke = 0.0f;
        this.mSportSetTotalTime = 0;
        this.mSportSetTotalDistance = 0.0f;
        this.mSportSetTotalCalories = 0.0f;
        this.mSportEteTraingLoadPeak = 0.0f;
        this.mSportEteTrainingEffect = 0;
        this.mSportEteVo2Max = 0.0f;
        this.mSportEteResourceRecovery = 0;
        this.mSportVo2MaxTread = 0;
        this.mSportThaTrainLoadTrend = 0;
        this.mETEdailyPerformance = DailyPerformenceChatView.invalidPointValue;
        this.mSwimStyle = 0;
        this.mUnit = 0;
        this.mTrainSnapShot = new TrainSnapShot();
        this.mOffsetAltitude = 0.0f;
        this.mDownHillNum = 0;
        this.mSingleDownhillDescend = 0.0f;
        this.mDownhillAvgSpeed = 0.0f;
        this.mSingleDownhillElveLoss = 0.0f;
        this.mDownhillMaxSpeed = 0.0f;
        this.mRtCurLapMaxDownhillElveLoss = 0.0f;
        this.mRtCurLapAvgPace = 0.0f;
        this.mSecHalfStartTime = -1;
        this.mIsRemindGPS = false;
    }

    public static OutdoorSportSnapshot copyOf(OutdoorSportSnapshot status) {
        OutdoorSportSnapshot copy = new OutdoorSportSnapshot();
        copy.mLatitude = status.mLatitude;
        copy.mLongtitude = status.mLongtitude;
        copy.mAltitude = status.mAltitude;
        copy.mRealTimePace = status.mRealTimePace;
        copy.mRealTimeSpeed = status.mRealTimeSpeed;
        copy.mTotalPace = status.mTotalPace;
        copy.mTotalSpeed = status.mTotalSpeed;
        copy.mDistance = status.mDistance;
        copy.mRealTimeStepFreq = status.mRealTimeStepFreq;
        copy.mStepCount = status.mStepCount;
        copy.mTrackId = status.mTrackId;
        copy.mTimestamp = status.mTimestamp;
        copy.mTotalTime = status.mTotalTime;
        copy.mStartTime = status.mStartTime;
        copy.mCalorie = status.mCalorie;
        copy.mHeartRate = status.mHeartRate;
        copy.mSportType = status.mSportType;
        copy.mPointStatus = status.mPointStatus;
        copy.mCurrentSecond = status.mCurrentSecond;
        copy.mCurrentPausedSecond = status.mCurrentPausedSecond;
        copy.mStepFreq = status.mStepFreq;
        copy.mCurrentMillSec = status.mCurrentMillSec;
        copy.mVerticalVelocity = status.mVerticalVelocity;
        copy.mBestPace = status.mBestPace;
        copy.mMinPace = status.mMinPace;
        copy.mMaxStepFreq = status.mMaxStepFreq;
        copy.mAbsoluteAtitude = status.mAbsoluteAtitude;
        copy.mClimbUp = status.mClimbUp;
        copy.mClimbDown = status.mClimbDown;
        copy.mHighAltitude = status.mHighAltitude;
        copy.mLowAltitude = status.mLowAltitude;
        copy.mIndividualDistance = status.mIndividualDistance;
        copy.mIndividaulCostTime = status.mIndividaulCostTime;
        copy.mIndividualPace = status.mIndividualPace;
        copy.mPreviousLapsCostTime = status.mPreviousLapsCostTime;
        copy.mPreviousLapsDistance = status.mPreviousLapsDistance;
        copy.mClimbdisascend = status.mClimbdisascend;
        copy.mCadence = status.mCadence;
        copy.mMaxCadence = status.mMaxCadence;
        copy.mAveCadence = status.mAveCadence;
        copy.mDeviceType = status.mDeviceType;
        copy.mDeviceBrand = status.mDeviceBrand;
        copy.mClimbdisDescend = status.mClimbdisDescend;
        copy.mAscendTime = status.mAscendTime;
        copy.mDescendTime = status.mDescendTime;
        copy.mMixedTotalTime = status.mMixedTotalTime;
        copy.mSwolfPerFixedMeters = status.mSwolfPerFixedMeters;
        copy.mTotalStrokes = status.mTotalStrokes;
        copy.mTotalTrips = status.mTotalTrips;
        copy.mRtDistancePerStroke = status.mRtDistancePerStroke;
        copy.mRtStrokeSpeed = status.mRtStrokeSpeed;
        copy.mAvgStrokeSpeed = status.mAvgStrokeSpeed;
        copy.mMaxStrokeSpeed = status.mMaxStrokeSpeed;
        copy.mAvgDistancePerStroke = status.mAvgDistancePerStroke;
        copy.mSportSetTotalTime = status.mSportSetTotalTime;
        copy.mSportSetTotalDistance = status.mSportSetTotalDistance;
        copy.mSportSetTotalCalories = status.mSportSetTotalCalories;
        copy.mSportEteTraingLoadPeak = status.mSportEteTraingLoadPeak;
        copy.mSportEteTrainingEffect = status.mSportEteTrainingEffect;
        copy.mSportEteVo2Max = status.mSportEteVo2Max;
        copy.mSportEteResourceRecovery = status.mSportEteResourceRecovery;
        copy.mSportVo2MaxTread = status.mSportVo2MaxTread;
        copy.mSportThaTrainLoadTrend = status.mSportThaTrainLoadTrend;
        copy.mSwimStyle = status.mSwimStyle;
        copy.mETEdailyPerformance = status.mETEdailyPerformance;
        copy.mCurGPSLonLatPoint = status.mCurGPSLonLatPoint;
        copy.mUnit = status.mUnit;
        copy.mTrainSnapShot = status.mTrainSnapShot;
        copy.intervalType = status.intervalType;
        copy.mOffsetAltitude = status.mOffsetAltitude;
        copy.mDownHillNum = status.mDownHillNum;
        copy.mSingleDownhillElveLoss = status.mSingleDownhillElveLoss;
        copy.mSingleDownhillDescend = status.mSingleDownhillDescend;
        copy.mDownhillAvgSpeed = status.mDownhillAvgSpeed;
        copy.mDownhillMaxSpeed = status.mDownhillMaxSpeed;
        copy.mStrokes = status.mStrokes;
        copy.mForeHand = status.mForeHand;
        copy.mBackHand = status.mBackHand;
        copy.mServe = status.mServe;
        copy.mRtCurLapMaxDownhillElveLoss = status.mRtCurLapMaxDownhillElveLoss;
        copy.mRtCurLapAvgPace = status.mRtCurLapAvgPace;
        copy.mSecHalfStartTime = status.mSecHalfStartTime;
        return copy;
    }

    public static OutdoorSportSnapshot createSportStatus(int sportType) {
        if (SportType.isSportTypeValid(sportType)) {
            OutdoorSportSnapshot status = new OutdoorSportSnapshot();
            status.mSportType = sportType;
            return status;
        }
        Debug.m6w("OutdoorSportSnapshot", "invalid sport status");
        return null;
    }

    public Long[] getCurGPSLonLatPoint() {
        return this.mCurGPSLonLatPoint;
    }

    public void setCurGPSLonLatPoint(Long[] curGPSLonLatPoint) {
        this.mCurGPSLonLatPoint = curGPSLonLatPoint;
    }

    public void setSwimStyle(int swimStyle) {
        this.mSwimStyle = swimStyle;
    }

    public long getSportSetTotalTime() {
        return this.mSportSetTotalTime;
    }

    public void setSportSetTotalTime(long sportSetTotalTime) {
        this.mSportSetTotalTime = sportSetTotalTime;
    }

    public float getSportSetTotalDistance() {
        return this.mSportSetTotalDistance;
    }

    public void setSportSetTotalDistance(float sportSetTotalDistance) {
        this.mSportSetTotalDistance = sportSetTotalDistance;
    }

    public float getsSportSetTotalCalories() {
        return this.mSportSetTotalCalories;
    }

    public void setSportSetTotalCalories(float sportSetTotalCalories) {
        this.mSportSetTotalCalories = sportSetTotalCalories;
    }

    public void setSwolfPerFixedMeters(int swolfPerFixedMeters) {
        this.mSwolfPerFixedMeters = swolfPerFixedMeters;
    }

    public int getTotalStrokes() {
        return this.mTotalStrokes;
    }

    public void setTotalStrokes(int totalStrokes) {
        this.mTotalStrokes = totalStrokes;
    }

    public int getTotalTrips() {
        return this.mTotalTrips;
    }

    public void setTotalTrips(int totalTrips) {
        this.mTotalTrips = totalTrips;
    }

    public float getRtDistancePerStroke() {
        return this.mRtDistancePerStroke;
    }

    public void setRtDistancePerStroke(float rtDistancePerStroke) {
        this.mRtDistancePerStroke = rtDistancePerStroke;
    }

    public float getRtStrokeSpeed() {
        return this.mRtStrokeSpeed;
    }

    public void setAvgDistancePerStroke(float avgDistancePerStroke) {
        this.mAvgDistancePerStroke = avgDistancePerStroke;
    }

    public void setRtStrokeSpeed(float rtStrokeSpeed) {
        this.mRtStrokeSpeed = rtStrokeSpeed;
    }

    public float getAvgStrokeSpeed() {
        return this.mAvgStrokeSpeed;
    }

    public void setAvgStrokeSpeed(float avgStrokeSpeed) {
        this.mAvgStrokeSpeed = avgStrokeSpeed;
    }

    public void setMaxStrokeSpeed(float maxStrokeSpeed) {
        this.mMaxStrokeSpeed = maxStrokeSpeed;
    }

    public long getMixedTotalTime() {
        return this.mMixedTotalTime;
    }

    public void setMixedTotalTime(long mixedTotalTime) {
        this.mMixedTotalTime = mixedTotalTime;
    }

    public void setLastKMCostTime(long lastKMCostTime) {
        this.mLastKMCostTime = lastKMCostTime;
    }

    public void setLastKilometer(int lastKilometer) {
        this.mLastKilometer = lastKilometer;
    }

    public void setLastKMPace(float lastKMPace) {
        this.mLastKMPace = lastKMPace;
    }

    public void setStepFreq(float stepFreq) {
        this.mStepFreq = stepFreq;
    }

    public float getRealTimeStepFreq() {
        return this.mRealTimeStepFreq;
    }

    public void setRealTimeStepFreq(float realTimeStepFreq) {
        this.mRealTimeStepFreq = realTimeStepFreq;
    }

    public int getStepCount() {
        return this.mStepCount;
    }

    public void setStepCount(int stepCount) {
        this.mStepCount = stepCount;
    }

    public float getRealTimePace() {
        return this.mRealTimePace;
    }

    public void setRealTimePace(float realTimePace) {
        this.mRealTimePace = realTimePace;
    }

    public float getRealTimeSpeed() {
        return this.mRealTimeSpeed;
    }

    public void setRealTimeSpeed(float realTimeSpeed) {
        this.mRealTimeSpeed = realTimeSpeed;
    }

    public float getTotalPace() {
        return this.mTotalPace;
    }

    public void setTotalPace(float totalPace) {
        this.mTotalPace = totalPace;
    }

    public float getTotalSpeed() {
        return this.mTotalSpeed;
    }

    public void setTotalSpeed(float totalSpeed) {
        this.mTotalSpeed = totalSpeed;
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
    }

    public int getTrimedDistance() {
        return (((int) this.mDistance) / 10) * 10;
    }

    public float getVerticalVelocity() {
        return this.mVerticalVelocity;
    }

    public void setVerticalVelocity(float verticalVelocity) {
        this.mVerticalVelocity = verticalVelocity;
    }

    public float getBestPace() {
        return this.mBestPace;
    }

    public void setBestPace(float bestPace) {
        this.mBestPace = bestPace;
    }

    public float getMinPace() {
        return this.mMinPace;
    }

    public void setMinPace(float minPace) {
        this.mMinPace = minPace;
    }

    public float getMaxStepFreq() {
        return this.mMaxStepFreq;
    }

    public void setMaxStepFreq(float maxStepFreq) {
        this.mMaxStepFreq = maxStepFreq;
    }

    public float getAbsoluteAtitude() {
        return this.mAbsoluteAtitude;
    }

    public void setAbsoluteAtitude(float absoluteAtitude) {
        this.mAbsoluteAtitude = absoluteAtitude;
    }

    public float getClimbUp() {
        return this.mClimbUp;
    }

    public void setClimbUp(float climbUp) {
        this.mClimbUp = climbUp;
    }

    public float getClimbDown() {
        return this.mClimbDown;
    }

    public void setClimbDown(float climbDown) {
        this.mClimbDown = climbDown;
    }

    public void setHighAltitude(float highAltitude) {
        this.mHighAltitude = highAltitude;
    }

    public void setLowAltitude(float lowAltitude) {
        this.mLowAltitude = lowAltitude;
    }

    public void setIndividualDistance(float individualDistance) {
        this.mIndividualDistance = individualDistance;
    }

    public void setIndividaulCostTime(long individaulCostTime) {
        this.mIndividaulCostTime = individaulCostTime;
    }

    public void setIndividualPace(float individualPace) {
        this.mIndividualPace = individualPace;
    }

    public void setPreviousLapsCostTime(long previousLapsCostTime) {
        this.mPreviousLapsCostTime = previousLapsCostTime;
    }

    public void setPreviousLapsDistance(float previousLapsDistance) {
        this.mPreviousLapsDistance = previousLapsDistance;
    }

    public float getClimbdisascend() {
        return this.mClimbdisascend;
    }

    public void setClimbdisascend(float climbdisascend) {
        this.mClimbdisascend = climbdisascend;
    }

    public float getClimbdisDescend() {
        return this.mClimbdisDescend;
    }

    public void setClimbdisDescend(float climbdisDescend) {
        this.mClimbdisDescend = climbdisDescend;
    }

    public void setAscendTime(long ascendTime) {
        this.mAscendTime = ascendTime;
    }

    public void setDescendTime(long descendTime) {
        this.mDescendTime = descendTime;
    }

    public float getCadence() {
        return this.mCadence;
    }

    public void setCadence(float cadence) {
        this.mCadence = cadence;
    }

    public void setMaxCadence(float maxCadence) {
        this.mMaxCadence = maxCadence;
    }

    public void setAveCadence(float aveCadence) {
        this.mAveCadence = aveCadence;
    }

    public void setDeviceType(int deviceType) {
        this.mDeviceType = deviceType;
    }

    public void setDeviceBrand(int deviceBrand) {
        this.mDeviceBrand = deviceBrand;
    }

    public void setmSportEteTraingLoadPeak(float mSportEteTraingLoadPeak) {
        this.mSportEteTraingLoadPeak = mSportEteTraingLoadPeak;
    }

    public int getmSportEteTrainingEffect() {
        return this.mSportEteTrainingEffect;
    }

    public void setmSportEteTrainingEffect(int mSportEteTrainingEffect) {
        this.mSportEteTrainingEffect = mSportEteTrainingEffect;
    }

    public float getmSportEteVo2Max() {
        return this.mSportEteVo2Max;
    }

    public void setmSportEteVo2Max(float mSportEteVo2Max) {
        this.mSportEteVo2Max = mSportEteVo2Max;
    }

    public int getmSportEteResourceRecovery() {
        return this.mSportEteResourceRecovery;
    }

    public void setmSportEteResourceRecovery(int mSportEteResourceRecovery) {
        this.mSportEteResourceRecovery = mSportEteResourceRecovery;
    }

    public SportThaInfo getSportThaInfo() {
        return this.sportThaInfo;
    }

    public void setSportThaInfo(SportThaInfo sportThaInfo) {
        this.sportThaInfo = sportThaInfo;
    }

    public int getmETEdailyPerformance() {
        return this.mETEdailyPerformance;
    }

    public void setmETEdailyPerformance(int mETEdailyPerformance) {
        this.mETEdailyPerformance = mETEdailyPerformance;
    }

    public OutdoorSportSummary initSummary(OutdoorSportSummary summary) {
        float f = 0.0f;
        if (SportType.isSwimMode(this.mSportType)) {
            summary.setDistance(this.mDistance);
        } else {
            summary.setDistance((float) getTrimedDistance());
        }
        summary.setCalorie(this.mCalorie);
        summary.setStartTime(this.mStartTime);
        summary.setCalorie(this.mCalorie);
        summary.setTrackId(this.mTrackId);
        summary.setLongitude(this.mLongtitude);
        summary.setLatitude(this.mLatitude);
        summary.setStepFreq(this.mStepFreq);
        if (this.mSportType == 11) {
            summary.setMaxSpeed(this.mDownhillMaxSpeed);
            summary.setBestPace(Float.compare(this.mDownhillMaxSpeed, 0.0f) == 0 ? 0.0f : 1.0f / this.mDownhillMaxSpeed);
            if (Float.compare(this.mDownhillAvgSpeed, 0.0f) != 0) {
                f = 1.0f / this.mDownhillAvgSpeed;
            }
            summary.setPace(f);
            summary.setTotalPace(summary.getPace());
            summary.setSpeed(this.mDownhillAvgSpeed);
            summary.setTotalSpeed(this.mDownhillAvgSpeed);
        } else {
            summary.setPace(this.mTotalPace);
            summary.setSpeed(this.mTotalSpeed);
            summary.setBestPace(this.mBestPace);
        }
        summary.setMinPace(this.mMinPace);
        summary.setMaxStepFreq(this.mMaxStepFreq);
        summary.setClimbUp(this.mClimbUp);
        summary.setClimbDown(this.mClimbDown);
        summary.setStepCount(this.mStepCount);
        if (Float.compare(this.mHighAltitude, -20000.0f) != 0) {
            summary.setHighestAltitude(this.mHighAltitude + this.mOffsetAltitude);
        }
        if (Float.compare(this.mLowAltitude, -20000.0f) != 0) {
            summary.setLowestAltitude(this.mLowAltitude + this.mOffsetAltitude);
        }
        summary.setClimbdisascend(this.mClimbdisascend);
        summary.setMaxCadence(this.mMaxCadence);
        summary.setAveCadence(this.mAveCadence);
        summary.setDeviceType(this.mDeviceType);
        summary.setDeviceBrand(this.mDeviceBrand);
        summary.setClimbdisDescend(this.mClimbdisDescend);
        summary.setAscendTime(this.mAscendTime);
        summary.setDescendTime(this.mDescendTime);
        summary.setSwolfPerFixedMeters(this.mSwolfPerFixedMeters);
        summary.setTotalStrokes(this.mTotalStrokes);
        summary.setTotalTrips(this.mTotalTrips);
        summary.setAvgDistancePerStroke(this.mAvgDistancePerStroke);
        summary.setAvgStrokeSpeed(this.mAvgStrokeSpeed);
        summary.setMaxStrokeSpeed(this.mMaxStrokeSpeed);
        summary.setmSwimStyle(this.mSwimStyle);
        summary.setUnit(this.mUnit);
        summary.setIntervalType(this.intervalType);
        summary.setDownHillNum(this.mDownHillNum);
        summary.setmStrokes(this.mStrokes);
        summary.setmForeHand(this.mForeHand);
        summary.setmBackHand(this.mBackHand);
        summary.setmServe(this.mServe);
        summary.setSecHalfStartTime(this.mSecHalfStartTime);
        return summary;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongtitude);
        dest.writeFloat(this.mAltitude);
        dest.writeFloat(this.mRealTimePace);
        dest.writeFloat(this.mRealTimeSpeed);
        dest.writeFloat(this.mTotalPace);
        dest.writeFloat(this.mTotalSpeed);
        dest.writeFloat(this.mDistance);
        dest.writeFloat(this.mRealTimeStepFreq);
        dest.writeInt(this.mStepCount);
        dest.writeLong(this.mTrackId);
        dest.writeLong(this.mTimestamp);
        dest.writeLong(this.mTotalTime);
        dest.writeLong(this.mStartTime);
        dest.writeFloat(this.mCalorie);
        dest.writeFloat(this.mHeartRate);
        dest.writeInt(this.mSportType);
        dest.writeByte(this.mPointStatus);
        dest.writeInt(this.mCurrentSecond);
        dest.writeInt(this.mCurrentPausedSecond);
        dest.writeByte(this.mCurrentStatus);
        dest.writeFloat(this.mStepFreq);
        dest.writeInt(this.mCurrentMillSec);
        dest.writeFloat(this.mVerticalVelocity);
        dest.writeFloat(this.mBestPace);
        dest.writeFloat(this.mMinPace);
        dest.writeFloat(this.mMaxStepFreq);
        dest.writeFloat(this.mAbsoluteAtitude);
        dest.writeFloat(this.mClimbUp);
        dest.writeFloat(this.mClimbDown);
        dest.writeFloat(this.mHighAltitude);
        dest.writeFloat(this.mLowAltitude);
        dest.writeFloat(this.mIndividualDistance);
        dest.writeLong(this.mIndividaulCostTime);
        dest.writeFloat(this.mIndividualPace);
        dest.writeLong(this.mPreviousLapsCostTime);
        dest.writeFloat(this.mPreviousLapsDistance);
        dest.writeFloat(this.mClimbdisascend);
        dest.writeFloat(this.mCadence);
        dest.writeFloat(this.mMaxCadence);
        dest.writeFloat(this.mAveCadence);
        dest.writeInt(this.mDeviceType);
        dest.writeInt(this.mDeviceBrand);
        dest.writeFloat(this.mClimbdisDescend);
        dest.writeLong(this.mAscendTime);
        dest.writeLong(this.mDescendTime);
        dest.writeLong(this.mMixedTotalTime);
        dest.writeInt(this.mSwolfPerFixedMeters);
        dest.writeInt(this.mTotalStrokes);
        dest.writeInt(this.mTotalTrips);
        dest.writeFloat(this.mRtDistancePerStroke);
        dest.writeFloat(this.mRtStrokeSpeed);
        dest.writeFloat(this.mAvgStrokeSpeed);
        dest.writeFloat(this.mMaxStrokeSpeed);
        dest.writeFloat(this.mAvgDistancePerStroke);
        dest.writeLong(this.mSportSetTotalTime);
        dest.writeFloat(this.mSportSetTotalDistance);
        dest.writeFloat(this.mSportSetTotalCalories);
        dest.writeFloat(this.mSportEteTraingLoadPeak);
        dest.writeInt(this.mSportEteTrainingEffect);
        dest.writeFloat(this.mSportEteVo2Max);
        dest.writeInt(this.mSportEteResourceRecovery);
        dest.writeInt(this.mSportVo2MaxTread);
        dest.writeInt(this.mSportThaTrainLoadTrend);
        dest.writeInt(this.mSwimStyle);
        dest.writeArray(this.mCurGPSLonLatPoint);
        dest.writeInt(this.mUnit);
        dest.writeParcelable(this.mTrainSnapShot, 0);
        dest.writeInt(this.intervalType);
        dest.writeFloat(this.mOffsetAltitude);
        dest.writeInt(this.mDownHillNum);
        dest.writeFloat(this.mSingleDownhillDescend);
        dest.writeFloat(this.mSingleDownhillElveLoss);
        dest.writeFloat(this.mDownhillAvgSpeed);
        dest.writeFloat(this.mDownhillMaxSpeed);
        dest.writeInt(this.mStrokes);
        dest.writeInt(this.mForeHand);
        dest.writeInt(this.mBackHand);
        dest.writeInt(this.mServe);
        dest.writeFloat(this.mRtCurLapMaxDownhillElveLoss);
        dest.writeFloat(this.mRtCurLapAvgPace);
        dest.writeLong(this.mSecHalfStartTime);
    }

    public String toString() {
        return super.toString() + "OutdoorSportSnapshot{" + "mLatitude=" + this.mLatitude + ", mLongtitude=" + this.mLongtitude + ", mAltitude=" + this.mAltitude + ", mRealTimePace=" + this.mRealTimePace + ", mRealTimeSpeed=" + this.mRealTimeSpeed + ", mTotalPace=" + this.mTotalPace + ", mTotalSpeed=" + this.mTotalSpeed + ", mDistance=" + this.mDistance + ", mRealTimeStepFreq=" + this.mRealTimeStepFreq + ", mStepCount=" + this.mStepCount + ", mLastKilometer=" + this.mLastKilometer + ", mLastKMPace=" + this.mLastKMPace + ", mLastKMCostTime=" + this.mLastKMCostTime + ", mStepFreq=" + this.mStepFreq + ", mVerticalVelocity=" + this.mVerticalVelocity + ", mBestPace=" + this.mBestPace + ", mMinPace=" + this.mMinPace + ", mMaxStepFreq=" + this.mMaxStepFreq + ", mAbsoluteAtitude=" + this.mAbsoluteAtitude + ", mClimbUp=" + this.mClimbUp + ", mClimbDown=" + this.mClimbDown + ", mHighAltitude=" + this.mHighAltitude + ", mLowAltitude=" + this.mLowAltitude + ", mIndividualDistance=" + this.mIndividualDistance + ", mIndividaulCostTime=" + this.mIndividaulCostTime + ", mIndividualPace=" + this.mIndividualPace + ", mPreviousLapsCostTime=" + this.mPreviousLapsCostTime + ", mPreviousLapsDistance=" + this.mPreviousLapsDistance + ", mClimbdisascend=" + this.mClimbdisascend + ", mClimbdisDescend=" + this.mClimbdisDescend + ", mAscendTime=" + this.mAscendTime + ", mDescendTime=" + this.mDescendTime + ", mCadence=" + this.mCadence + ", mMaxCadence=" + this.mMaxCadence + ", mAveCadence=" + this.mAveCadence + ", mDeviceType=" + this.mDeviceType + ", mDeviceBrand=" + this.mDeviceBrand + ", mIsRemindGPS=" + this.mIsRemindGPS + ", mMixedTotalTime=" + this.mMixedTotalTime + ", mSwolfPerFixedMeters=" + this.mSwolfPerFixedMeters + ", mTotalStrokes=" + this.mTotalStrokes + ", mTotalTrips=" + this.mTotalTrips + ", mRtDistancePerStroke=" + this.mRtDistancePerStroke + ", mRtStrokeSpeed=" + this.mRtStrokeSpeed + ", mAvgStrokeSpeed=" + this.mAvgStrokeSpeed + ", mMaxStrokeSpeed=" + this.mMaxStrokeSpeed + ", mAvgDistancePerStroke=" + this.mAvgDistancePerStroke + ", mSportSetTotalTime=" + this.mSportSetTotalTime + ", mSportSetTotalDistance=" + this.mSportSetTotalDistance + ", mSportSetTotalCalories=" + this.mSportSetTotalCalories + ", mSportEteTraingLoadPeak=" + this.mSportEteTraingLoadPeak + ", mSportEteTrainingEffect=" + this.mSportEteTrainingEffect + ", mSportEteVo2Max=" + this.mSportEteVo2Max + ", mSportEteResourceRecovery=" + this.mSportEteResourceRecovery + ", mSportVo2MaxTread=" + this.mSportVo2MaxTread + ", mSportThaTrainLoadTrend=" + this.mSportThaTrainLoadTrend + ", mSwimStyle=" + this.mSwimStyle + ", mUnit=" + this.mUnit + ", mTrainSnapShot=" + this.mTrainSnapShot.toString() + ", mIntervalType=" + this.intervalType + ", mOffsetAltitude=" + this.mOffsetAltitude + ", mDownHillNum=" + this.mDownHillNum + ", mSingleDownhillDescend=" + this.mSingleDownhillDescend + ", mSingleDownhillElveLoss=" + this.mSingleDownhillElveLoss + ", mDownhillAvgSpeed=" + this.mDownhillAvgSpeed + ", mDownhillMaxSpeed=" + this.mDownhillMaxSpeed + ", mStrokes=" + this.mStrokes + ", mForeHand=" + this.mForeHand + ", mBackHand=" + this.mBackHand + ", mServe=" + this.mServe + ", mRtCurLapMaxDownhillElveLoss=" + this.mRtCurLapMaxDownhillElveLoss + ", mRtCurLapAvgPace=" + this.mRtCurLapAvgPace + ", mSecHalfStartTime=" + this.mSecHalfStartTime + '}';
    }
}
