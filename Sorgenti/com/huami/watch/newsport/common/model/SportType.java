package com.huami.watch.newsport.common.model;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class SportType {
    private static final List<Integer> mAvailableSports = new ArrayList();
    private static final List<Integer> mNotNeedDisSports = new ArrayList();
    private static final List<Integer> mNotNeedGpsSports = new ArrayList();
    private static final List<Integer> mSaveFirstBeatData = new ArrayList();
    private static final List<Integer> mSuporrtHasHeartRataSports = new ArrayList();

    static {
        mAvailableSports.add(Integer.valueOf(1));
        mAvailableSports.add(Integer.valueOf(6));
        mAvailableSports.add(Integer.valueOf(7));
        mAvailableSports.add(Integer.valueOf(8));
        mAvailableSports.add(Integer.valueOf(9));
        mAvailableSports.add(Integer.valueOf(10));
        mAvailableSports.add(Integer.valueOf(12));
        mAvailableSports.add(Integer.valueOf(14));
        mAvailableSports.add(Integer.valueOf(2001));
        mAvailableSports.add(Integer.valueOf(15));
        mAvailableSports.add(Integer.valueOf(1001));
        mAvailableSports.add(Integer.valueOf(1009));
        mAvailableSports.add(Integer.valueOf(1015));
        mAvailableSports.add(Integer.valueOf(13));
        mAvailableSports.add(Integer.valueOf(11));
        mAvailableSports.add(Integer.valueOf(17));
        mAvailableSports.add(Integer.valueOf(18));
        mAvailableSports.add(Integer.valueOf(2002));
        mNotNeedGpsSports.add(Integer.valueOf(10));
        mNotNeedGpsSports.add(Integer.valueOf(8));
        mNotNeedGpsSports.add(Integer.valueOf(12));
        mNotNeedGpsSports.add(Integer.valueOf(14));
        mNotNeedGpsSports.add(Integer.valueOf(17));
        mNotNeedDisSports.add(Integer.valueOf(10));
        mNotNeedDisSports.add(Integer.valueOf(12));
        mNotNeedDisSports.add(Integer.valueOf(17));
        mSaveFirstBeatData.add(Integer.valueOf(1));
        mSaveFirstBeatData.add(Integer.valueOf(6));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(1));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(6));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(8));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(9));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(10));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(12));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(1009));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(1001));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(17));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(18));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(2001));
        mSuporrtHasHeartRataSports.add(Integer.valueOf(2002));
    }

    public static boolean isSportTypeValid(int sportType) {
        if (mAvailableSports.contains(Integer.valueOf(sportType))) {
            return true;
        }
        Debug.m5i("SportType", "sport type is not valid, sportType:" + sportType);
        return false;
    }

    public static boolean isSupportFirstBeatData(int sportType) {
        if (mSaveFirstBeatData.contains(Integer.valueOf(sportType))) {
            return true;
        }
        Debug.m5i("SportType", "sport type is not valid, sportType:" + sportType);
        return false;
    }

    public static boolean isSportTypeNeedGps(int sportType) {
        Debug.m5i("SportType", "isSportTypeNeedGps, sportType:" + sportType);
        if (mNotNeedGpsSports.contains(Integer.valueOf(sportType))) {
            return false;
        }
        return true;
    }

    public static boolean isSportTypeNeedDis(int sportType) {
        Debug.m5i("SportType", "isSportTypeNeedDis, sportType:" + sportType);
        if (mNotNeedDisSports.contains(Integer.valueOf(sportType))) {
            return false;
        }
        return true;
    }

    public static boolean isSuportHasHeartRataSports(int sportType) {
        Debug.m5i("SportType", "isSuporrtRecoveryTime, sportType:" + sportType);
        if (mSuporrtHasHeartRataSports.contains(Integer.valueOf(sportType))) {
            return true;
        }
        return false;
    }

    public static boolean isMixedSport(int sportType) {
        if (sportType == 2001 || sportType == 2002) {
            return true;
        }
        return false;
    }

    public static boolean isChildSport(int sportType) {
        if (sportType == 1015 || sportType == 1009 || sportType == 1001) {
            return true;
        }
        return false;
    }

    public static boolean isMeterStandard(int sportType) {
        if (sportType == 14 || sportType == 15) {
            return true;
        }
        return false;
    }

    public static boolean isNeedSportTargetRecommand(int sportType) {
        if (sportType == 1 || sportType == 2002) {
            return true;
        }
        return false;
    }

    public static boolean isValidWhenEndSport(int sportType, OutdoorSportSnapshot sportSnapshot) {
        if (sportSnapshot == null) {
            Debug.m5i("sport_is_valid", "invalid sport, snapshot is null");
            return false;
        } else if (sportType == 11) {
            if (sportSnapshot.getDownHillNum() > 0) {
                return true;
            }
            return false;
        } else if (sportType == 14) {
            if (sportSnapshot.getTotalTrips() > 0) {
                return true;
            }
            return false;
        } else if (sportType == 15 || sportType == 1015) {
            if (sportSnapshot.getDistance() >= 25.0f) {
                return true;
            }
            return false;
        } else if (sportType == 17) {
            if (sportSnapshot.getmStrokes() > 0) {
                return true;
            }
            return false;
        } else if (isSportTypeNeedDis(sportType)) {
            if (((double) sportSnapshot.getDistance()) >= UnitConvertUtils.convertDistanceFromMileOrKmtokm((double) (((float) Global.MINIMUM_DISTANCE_METER) / 1000.0f)) * 1000.0d) {
                return true;
            }
            return false;
        } else if (((double) Math.round(UnitConvertUtils.convertCalorieToKilocalorie((double) sportSnapshot.getCalorie()))) >= 1.0d) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSwimMode(int sportType) {
        if (sportType == 15 || sportType == 14 || sportType == 1015) {
            return true;
        }
        return false;
    }

    public static boolean isNeedAutoLap(int sportType, boolean isIntervalTrain) {
        if (sportType == 11 || isIntervalTrain) {
            return false;
        }
        return true;
    }

    public static boolean isNeedAutoLap(int sportType) {
        return isNeedAutoLap(sportType, false);
    }

    public static boolean isNeedManualLap(int sportType) {
        if (sportType == 17 || sportType == 18) {
            return false;
        }
        return true;
    }

    public static boolean isSupportImperial(int sportType) {
        if (isSwimMode(sportType)) {
            return false;
        }
        return true;
    }
}
