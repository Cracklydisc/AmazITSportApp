package com.huami.watch.newsport.common.model;

import com.huami.watch.common.JSONAble;
import com.huami.watch.common.JSONAble.JSONCreator;

public class RunningInfoPerKM implements JSONAble {
    public static JSONCreator<RunningInfoPerKM> JSON_CREATOR = new C05571();
    private float dailyPorpermence = 127.0f;
    private long mCostTime = 0;
    private float mLatitude = 10000.0f;
    private float mLongitude = 10000.0f;
    private double mPace = 0.0d;
    private double mSpeed = 0.0d;
    private long mTotalCostTime = 0;
    private long mTrackId = -1;
    private int mUnit = 0;
    private int teValue = 0;

    static class C05571 implements JSONCreator<RunningInfoPerKM> {
        C05571() {
        }
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public int getUnit() {
        return this.mUnit;
    }

    public void setUnit(int unit) {
        this.mUnit = unit;
    }

    public long getTotalCostTime() {
        return this.mTotalCostTime;
    }

    public void setTotalCostTime(long totalCostTime) {
        this.mTotalCostTime = totalCostTime;
    }

    public float getLongitude() {
        return this.mLongitude;
    }

    public void setLongitude(float longitude) {
        this.mLongitude = longitude;
    }

    public float getLatitude() {
        return this.mLatitude;
    }

    public void setLatitude(float latitude) {
        this.mLatitude = latitude;
    }

    public int getTeValue() {
        return this.teValue;
    }

    public void setTeValue(int teValue) {
        this.teValue = teValue;
    }

    public float getDailyPorpermence() {
        return this.dailyPorpermence;
    }

    public void setDailyPorpermence(float dailyPorpermence) {
        this.dailyPorpermence = dailyPorpermence;
    }

    public String toString() {
        return "RunningInfoPerKM{mTrackId=" + this.mTrackId + ", mSpeed=" + this.mSpeed + ", mPace=" + this.mPace + ", mCostTime=" + this.mCostTime + ", mTotalCostTime=" + this.mTotalCostTime + ", mLatitude=" + this.mLatitude + ", mLongitude=" + this.mLongitude + ", mUnit=" + this.mUnit + ", teValue=" + this.teValue + ", dailyPorpermence=" + this.dailyPorpermence + '}';
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public double getSpeed() {
        return this.mSpeed;
    }

    public void setSpeed(double speed) {
        this.mSpeed = speed;
    }

    public double getPace() {
        return this.mPace;
    }

    public void setPace(double pace) {
        this.mPace = pace;
    }

    public long getCostTime() {
        return this.mCostTime;
    }

    public void setCostTime(long costTime) {
        this.mCostTime = costTime;
    }
}
