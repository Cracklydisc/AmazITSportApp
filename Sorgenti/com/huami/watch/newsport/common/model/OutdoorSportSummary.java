package com.huami.watch.newsport.common.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.huami.watch.common.log.Debug;
import org.json.JSONException;
import org.json.JSONObject;

public class OutdoorSportSummary extends SportSummary {
    public static Creator<OutdoorSportSummary> CREATOR = new C05551();
    private int intervalType;
    private long mAscendTime;
    private float mAveCadence;
    private float mAvgDistancePerStroke;
    private int mAvgHeartRate;
    private float mAvgStride;
    private float mAvgStrokeSpeed;
    private int mBackHand;
    private float mBestPace;
    private float mClimbDown;
    private float mClimbUp;
    private float mClimbdisDescend;
    private float mClimbdisascend;
    private long mDescendTime;
    private int mDeviceBrand;
    private int mDeviceType;
    private float mDisPerLap;
    private int mDownHillNum;
    private float mDownhillMaxAltitudeDescend;
    private int mForeHand;
    private float mHighestAltitude;
    private double mLatitude;
    private double mLongitude;
    private float mLowestAltitude;
    private float mMaxCadence;
    private int mMaxHeartRate;
    private float mMaxSpeed;
    private float mMaxStepFreq;
    private float mMaxStrokeSpeed;
    private int mMinHeartRate;
    private float mMinPace;
    private float mPace;
    private long mSecHalfStartTime;
    private int mServe;
    private float mSpeed;
    private int mStepCount;
    private float mStepFreq;
    private int mStrokes;
    private int mSwimPoolLength;
    private int mSwimStyle;
    private int mSwolfPerFixedMeters;
    private int mTE;
    private float mTotalPace;
    private float mTotalSpeed;
    private int mTotalStrokes;
    private int mTotalTrips;
    private int mUnit;
    private int mVersion;

    static class C05551 implements Creator<OutdoorSportSummary> {
        C05551() {
        }

        public OutdoorSportSummary createFromParcel(Parcel source) {
            OutdoorSportSummary summary = new OutdoorSportSummary();
            summary.mCurrentStatus = source.readInt();
            summary.mSyncCount = source.readInt();
            summary.mSpeed = source.readFloat();
            summary.mSportType = source.readInt();
            summary.mTrackId = source.readLong();
            summary.mCalorie = source.readFloat();
            summary.mDistance = source.readFloat();
            summary.mEndTime = source.readLong();
            summary.mStartTime = source.readLong();
            summary.mSportDuration = source.readInt();
            summary.mPace = source.readFloat();
            summary.mTotalPausedTime = source.readInt();
            summary.mLatitude = source.readDouble();
            summary.mLongitude = source.readDouble();
            summary.mStepFreq = source.readFloat();
            summary.mAvgHeartRate = source.readInt();
            summary.mMaxHeartRate = source.readInt();
            summary.mMinHeartRate = source.readInt();
            summary.mTotalPace = source.readFloat();
            summary.mBestPace = source.readFloat();
            summary.mMinPace = source.readFloat();
            summary.mTotalSpeed = source.readFloat();
            summary.mMaxSpeed = source.readFloat();
            summary.mMaxStepFreq = source.readFloat();
            summary.mClimbUp = source.readFloat();
            summary.mClimbDown = source.readFloat();
            summary.mHighestAltitude = source.readFloat();
            summary.mLowestAltitude = source.readFloat();
            summary.mDisPerLap = source.readFloat();
            summary.mStepCount = source.readInt();
            summary.mAvgStride = source.readFloat();
            summary.mClimbdisascend = source.readFloat();
            summary.mMaxCadence = source.readFloat();
            summary.mAveCadence = source.readFloat();
            summary.mDeviceType = source.readInt();
            summary.mDeviceBrand = source.readInt();
            summary.mVersion = source.readInt();
            summary.mClimbdisDescend = source.readFloat();
            summary.mAscendTime = source.readLong();
            summary.mDescendTime = source.readLong();
            source.readList(summary.mChildTrackIds, Long.class.getClassLoader());
            source.readList(summary.mChildSportTypes, Integer.class.getClassLoader());
            summary.mSwolfPerFixedMeters = source.readInt();
            summary.mTotalStrokes = source.readInt();
            summary.mTotalTrips = source.readInt();
            summary.mAvgStrokeSpeed = source.readFloat();
            summary.mMaxStrokeSpeed = source.readFloat();
            summary.mAvgDistancePerStroke = source.readFloat();
            summary.mSwimPoolLength = source.readInt();
            summary.mTE = source.readInt();
            summary.mSwimStyle = source.readInt();
            summary.mParentTrackId = source.readLong();
            source.readList(summary.mPauseInfos, PauseInfo.class.getClassLoader());
            summary.mUnit = source.readInt();
            summary.intervalType = source.readInt();
            summary.mDownHillNum = source.readInt();
            summary.mDownhillMaxAltitudeDescend = source.readFloat();
            summary.mStrokes = source.readInt();
            summary.mForeHand = source.readInt();
            summary.mBackHand = source.readInt();
            summary.mServe = source.readInt();
            summary.mSecHalfStartTime = source.readLong();
            return summary;
        }

        public OutdoorSportSummary[] newArray(int size) {
            return new OutdoorSportSummary[size];
        }
    }

    private static final class JSONKeys {
        private JSONKeys() {
        }
    }

    public float getDownhillMaxAltitudeDescend() {
        return this.mDownhillMaxAltitudeDescend;
    }

    public void setDownhillMaxAltitudeDescend(float downhillMaxAltitudeDescend) {
        this.mDownhillMaxAltitudeDescend = downhillMaxAltitudeDescend;
    }

    public long getSecHalfStartTime() {
        return this.mSecHalfStartTime;
    }

    public void setSecHalfStartTime(long secHalfStartTime) {
        this.mSecHalfStartTime = secHalfStartTime;
    }

    public int getmStrokes() {
        return this.mStrokes;
    }

    public void setmStrokes(int mStrokes) {
        this.mStrokes = mStrokes;
    }

    public int getmForeHand() {
        return this.mForeHand;
    }

    public void setmForeHand(int mForeHand) {
        this.mForeHand = mForeHand;
    }

    public int getmBackHand() {
        return this.mBackHand;
    }

    public void setmBackHand(int mBackHand) {
        this.mBackHand = mBackHand;
    }

    public int getmServe() {
        return this.mServe;
    }

    public void setmServe(int mServe) {
        this.mServe = mServe;
    }

    public int getDownHillNum() {
        return this.mDownHillNum;
    }

    public void setDownHillNum(int downHillNum) {
        this.mDownHillNum = downHillNum;
    }

    public int getUnit() {
        return this.mUnit;
    }

    public void setUnit(int unit) {
        this.mUnit = unit;
    }

    public int getSwimStyle() {
        return this.mSwimStyle;
    }

    public void setmSwimStyle(int swimStyle) {
        this.mSwimStyle = swimStyle;
    }

    public int getTE() {
        return this.mTE;
    }

    public void setTE(int tE) {
        this.mTE = tE;
    }

    public int getSwimPoolLength() {
        return this.mSwimPoolLength;
    }

    public void setSwimPoolLength(int swimPoolLength) {
        this.mSwimPoolLength = swimPoolLength;
    }

    public int getSwolfPerFixedMeters() {
        return this.mSwolfPerFixedMeters;
    }

    public void setSwolfPerFixedMeters(int swolfPerFixedMeters) {
        this.mSwolfPerFixedMeters = swolfPerFixedMeters;
    }

    public int getTotalStrokes() {
        return this.mTotalStrokes;
    }

    public void setTotalStrokes(int totalStrokes) {
        this.mTotalStrokes = totalStrokes;
    }

    public int getTotalTrips() {
        return this.mTotalTrips;
    }

    public void setTotalTrips(int totalTrips) {
        this.mTotalTrips = totalTrips;
    }

    public float getAvgDistancePerStroke() {
        return this.mAvgDistancePerStroke;
    }

    public void setAvgDistancePerStroke(float avgDistancePerStroke) {
        this.mAvgDistancePerStroke = avgDistancePerStroke;
    }

    public float getAvgStrokeSpeed() {
        return this.mAvgStrokeSpeed;
    }

    public void setAvgStrokeSpeed(float avgStrokeSpeed) {
        this.mAvgStrokeSpeed = avgStrokeSpeed;
    }

    public float getMaxStrokeSpeed() {
        return this.mMaxStrokeSpeed;
    }

    public void setMaxStrokeSpeed(float maxStrokeSpeed) {
        this.mMaxStrokeSpeed = maxStrokeSpeed;
    }

    public int getVersion() {
        return this.mVersion;
    }

    public void setVersion(int version) {
        this.mVersion = version;
    }

    public float getClimbdisascend() {
        return this.mClimbdisascend;
    }

    public void setClimbdisascend(float climbdisascend) {
        this.mClimbdisascend = climbdisascend;
    }

    public float getMaxCadence() {
        return this.mMaxCadence;
    }

    public void setMaxCadence(float maxCadence) {
        this.mMaxCadence = maxCadence;
    }

    public float getAveCadence() {
        return this.mAveCadence;
    }

    public void setAveCadence(float aveCadence) {
        this.mAveCadence = aveCadence;
    }

    public float getAvgStride() {
        return this.mAvgStride;
    }

    public void setAvgStride(float avgStride) {
        this.mAvgStride = avgStride;
    }

    public float getClimbdisDescend() {
        return this.mClimbdisDescend;
    }

    public void setClimbdisDescend(float climbdisDescend) {
        this.mClimbdisDescend = climbdisDescend;
    }

    public long getAscendTime() {
        return this.mAscendTime;
    }

    public void setAscendTime(long ascendTime) {
        this.mAscendTime = ascendTime;
    }

    public long getDescendTime() {
        return this.mDescendTime;
    }

    public void setDescendTime(long descendTime) {
        this.mDescendTime = descendTime;
    }

    public boolean isLocationValid() {
        return this.mLongitude >= -180.0d && this.mLongitude <= 180.0d && this.mLatitude >= -90.0d && this.mLatitude <= 90.0d;
    }

    public int getStepCount() {
        return this.mStepCount;
    }

    public void setStepCount(int stepCount) {
        this.mStepCount = stepCount;
    }

    public int getIntervalType() {
        return this.intervalType;
    }

    public void setIntervalType(int intervalType) {
        this.intervalType = intervalType;
    }

    private OutdoorSportSummary() {
        this.mSpeed = 0.0f;
        this.mPace = 0.0f;
        this.mStepFreq = 0.0f;
        this.mLongitude = 10000.0d;
        this.mLatitude = 10000.0d;
        this.mAvgHeartRate = -1;
        this.mMaxHeartRate = -1;
        this.mMinHeartRate = -1;
        this.mTotalPace = 0.0f;
        this.mBestPace = 0.0f;
        this.mMinPace = 0.0f;
        this.mTotalSpeed = 0.0f;
        this.mMaxSpeed = 0.0f;
        this.mMaxStepFreq = 0.0f;
        this.mClimbUp = -1.0f;
        this.mClimbDown = -1.0f;
        this.mHighestAltitude = -20000.0f;
        this.mLowestAltitude = -20000.0f;
        this.mDisPerLap = 0.0f;
        this.mClimbdisascend = -1.0f;
        this.mClimbdisDescend = -1.0f;
        this.mAscendTime = -1;
        this.mDescendTime = -1;
        this.mMaxCadence = -1.0f;
        this.mAveCadence = -1.0f;
        this.mDeviceType = -1;
        this.mDeviceBrand = -1;
        this.mVersion = 12;
        this.mSwolfPerFixedMeters = -1;
        this.mTotalStrokes = -1;
        this.mTotalTrips = -1;
        this.mAvgStrokeSpeed = -1.0f;
        this.mMaxStrokeSpeed = -1.0f;
        this.mAvgDistancePerStroke = -1.0f;
        this.mSwimPoolLength = -1;
        this.mTE = -1;
        this.mSwimStyle = -1;
        this.mUnit = 0;
        this.intervalType = 0;
        this.mDownHillNum = 0;
        this.mDownhillMaxAltitudeDescend = -1.0f;
        this.mStrokes = 0;
        this.mForeHand = 0;
        this.mBackHand = 0;
        this.mServe = 0;
        this.mSecHalfStartTime = -1;
        this.mAvgStride = -1.0f;
        this.mStepCount = 0;
    }

    public static OutdoorSportSummary createOutdoorSportSummary(int sportType) {
        if (SportType.isSportTypeValid(sportType)) {
            OutdoorSportSummary summary = new OutdoorSportSummary();
            summary.mSportType = sportType;
            summary.mVersion = 15;
            return summary;
        }
        Debug.m6w("OutdoorSportSummary", "invalid sport type : " + sportType);
        return null;
    }

    public static OutdoorSportSummary createFromJSON(String jsonStr) {
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            OutdoorSportSummary summary = new OutdoorSportSummary();
            summary.initObjectFromJSON(jsonObject);
            return summary;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getAvgHeartRate() {
        return this.mAvgHeartRate;
    }

    public void setAvgHeartRate(int avgHeartRate) {
        this.mAvgHeartRate = avgHeartRate;
    }

    public int getMaxHeartRate() {
        return this.mMaxHeartRate;
    }

    public void setMaxHeartRate(int maxHeartRate) {
        this.mMaxHeartRate = maxHeartRate;
    }

    public int getMinHeartRate() {
        return this.mMinHeartRate;
    }

    public void setMinHeartRate(int minHeartRate) {
        this.mMinHeartRate = minHeartRate;
    }

    public float getStepFreq() {
        return this.mStepFreq;
    }

    public void setStepFreq(float stepFreq) {
        this.mStepFreq = stepFreq;
    }

    public double getLongitude() {
        return this.mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    public double getLatitude() {
        return this.mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public float getDistancePerLap() {
        return this.mDisPerLap;
    }

    public void setDisPerLap(float disPerLap) {
        this.mDisPerLap = disPerLap;
    }

    public String toString() {
        return super.toString() + "OutdoorSportSummary{" + "mSpeed=" + this.mSpeed + ", mPace=" + this.mPace + ", mStepFreq=" + this.mStepFreq + ", mLongitude=" + this.mLongitude + ", mLatitude=" + this.mLatitude + ", mAvgHeartRate=" + this.mAvgHeartRate + ", mMaxHeartRate=" + this.mMaxHeartRate + ", mMinHeartRate=" + this.mMinHeartRate + ", mTotalPace=" + this.mTotalPace + ", mBestPace=" + this.mBestPace + ", mMinpace=" + this.mMinPace + ", mTotalSpeed=" + this.mTotalSpeed + ", mMaxSpeed=" + this.mMaxSpeed + ", mMaxStepFreq=" + this.mMaxStepFreq + ", mClimbUp=" + this.mClimbUp + ", mClimbDown=" + this.mClimbDown + ", mHighestAltitude=" + this.mHighestAltitude + ", mLowestAltitude=" + this.mLowestAltitude + ", mDisPerLap=" + this.mDisPerLap + ", mClimbdisascend=" + this.mClimbdisascend + ", mClimbdisDescend=" + this.mClimbdisDescend + ", mAscendTime=" + this.mAscendTime + ", mDescendTime=" + this.mDescendTime + ", mMaxCadence=" + this.mMaxCadence + ", mAveCadence=" + this.mAveCadence + ", mDeviceType=" + this.mDeviceType + ", mDeviceBrand=" + this.mDeviceBrand + ", mVersion=" + this.mVersion + ", mAvgStride=" + this.mAvgStride + ", mStepCount=" + this.mStepCount + ", mSwolfPerFixedMeters=" + this.mSwolfPerFixedMeters + ", mTotalStrokes=" + this.mTotalStrokes + ", mTotalTrips=" + this.mTotalTrips + ", mAvgStrokeSpeed=" + this.mAvgStrokeSpeed + ", mMaxStrokeSpeed=" + this.mMaxStrokeSpeed + ", mAvgDistancePerStroke=" + this.mAvgDistancePerStroke + ", mSwimPoolLength=" + this.mSwimPoolLength + ", mTE=" + this.mTE + ",mSwimStyle=" + this.mSwimStyle + ",mUnit=" + this.mUnit + ",mInterValType=" + this.intervalType + ",mDownHillNum=" + this.mDownHillNum + ",mDownhillMaxAltitudeDescend=" + this.mDownhillMaxAltitudeDescend + ",mStrokes=" + this.mStrokes + ",mForeHand=" + this.mForeHand + ",mBackHand=" + this.mBackHand + ",mServe=" + this.mServe + ",mSecHalfStartTime=" + this.mSecHalfStartTime + '}';
    }

    public float getPace() {
        return this.mPace;
    }

    public void setPace(float pace) {
        this.mPace = pace;
    }

    public float getSpeed() {
        return this.mSpeed;
    }

    public void setSpeed(float speed) {
        this.mSpeed = speed;
    }

    public float getTotalPace() {
        return this.mTotalPace;
    }

    public void setTotalPace(float totalPace) {
        this.mTotalPace = totalPace;
    }

    public float getMinPace() {
        return this.mMinPace;
    }

    public void setMinPace(float minPace) {
        this.mMinPace = minPace;
    }

    public float getBestPace() {
        return this.mBestPace;
    }

    public void setBestPace(float bestPace) {
        this.mBestPace = bestPace;
    }

    public void setTotalSpeed(float totalSpeed) {
        this.mTotalSpeed = totalSpeed;
    }

    public float getTotalSpeed() {
        return this.mTotalSpeed;
    }

    public float getMaxSpeed() {
        return this.mMaxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.mMaxSpeed = maxSpeed;
    }

    public float getMaxStepFreq() {
        return this.mMaxStepFreq;
    }

    public void setMaxStepFreq(float maxStepFreq) {
        this.mMaxStepFreq = maxStepFreq;
    }

    public float getClimbUp() {
        return this.mClimbUp;
    }

    public void setClimbUp(float climbUp) {
        this.mClimbUp = climbUp;
    }

    public float getClimbDown() {
        return this.mClimbDown;
    }

    public void setClimbDown(float climbDown) {
        this.mClimbDown = climbDown;
    }

    public float getHighestAltitude() {
        return this.mHighestAltitude;
    }

    public void setHighestAltitude(float highestAltitude) {
        this.mHighestAltitude = highestAltitude;
    }

    public float getLowestAltitude() {
        return this.mLowestAltitude;
    }

    public void setLowestAltitude(float lowestAltitude) {
        this.mLowestAltitude = lowestAltitude;
    }

    public int getDeviceType() {
        return this.mDeviceType;
    }

    public void setDeviceType(int deviceType) {
        this.mDeviceType = deviceType;
    }

    public int getDeviceBrand() {
        return this.mDeviceBrand;
    }

    public void setDeviceBrand(int deviceBrand) {
        this.mDeviceBrand = deviceBrand;
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        super.initJSONFromObject(jsonObject);
        jsonObject.put("speed", (double) this.mSpeed);
        jsonObject.put("pace", (double) this.mPace);
        jsonObject.put("latitude", this.mLatitude);
        jsonObject.put("longitude", this.mLongitude);
        jsonObject.put("step_freq", (double) this.mStepFreq);
        jsonObject.put("avg_heart_rate", this.mAvgHeartRate);
        jsonObject.put("max_heart_rate", this.mMaxHeartRate);
        jsonObject.put("min_heart_rate", this.mMinHeartRate);
        jsonObject.put("total_pace", (double) this.mTotalPace);
        jsonObject.put("best_pace", (double) this.mBestPace);
        jsonObject.put("min_pace", (double) this.mMinPace);
        jsonObject.put("total_speed", (double) this.mTotalSpeed);
        jsonObject.put("max_speed", (double) this.mMaxSpeed);
        jsonObject.put("max_step_freq", (double) this.mMaxStepFreq);
        jsonObject.put("climb_up", (double) this.mClimbUp);
        jsonObject.put("climb_down", (double) this.mClimbDown);
        jsonObject.put("high_altitude", (double) this.mHighestAltitude);
        jsonObject.put("low_altitude", (double) this.mLowestAltitude);
        jsonObject.put("km_per_individual", (double) this.mDisPerLap);
        jsonObject.put("step_count", this.mStepCount);
        jsonObject.put("avg_stride", (double) this.mAvgStride);
        jsonObject.put("total_climb_disacend", (double) this.mClimbdisascend);
        jsonObject.put("max_cadence", (double) this.mMaxCadence);
        jsonObject.put("ave_cadence", (double) this.mAveCadence);
        jsonObject.put("device_type", this.mDeviceType);
        jsonObject.put("device_source", this.mDeviceBrand);
        jsonObject.put("summary_version", this.mVersion);
        jsonObject.put("descend_distance", (double) this.mClimbdisDescend);
        jsonObject.put("ascend_time", this.mAscendTime);
        jsonObject.put("descend_time", this.mDescendTime);
        jsonObject.put("swolf", this.mSwolfPerFixedMeters);
        jsonObject.put("total_strokes", this.mTotalStrokes);
        jsonObject.put("total_trips", this.mTotalTrips);
        jsonObject.put("avg_stoke_speed", (double) this.mAvgStrokeSpeed);
        jsonObject.put("max_stoke_speed", (double) this.mMaxStrokeSpeed);
        if (Float.isNaN(this.mAvgDistancePerStroke)) {
            Debug.m5i("OutdoorSportSummary", "initJSONFromObject, mAvgDistancePerStroke is nan");
            this.mAvgDistancePerStroke = 0.0f;
        }
        jsonObject.put("avg_dis_per_stroke", (double) this.mAvgDistancePerStroke);
        jsonObject.put("swim_pool_length", this.mSwimPoolLength);
        jsonObject.put("te_value", this.mTE);
        jsonObject.put("swim_style", this.mSwimStyle);
        jsonObject.put("unit", this.mUnit);
        jsonObject.put("interval_type", this.intervalType);
        jsonObject.put("downhill_num", this.mDownHillNum);
        jsonObject.put("downhill_max_altitude_descend", (double) this.mDownhillMaxAltitudeDescend);
        jsonObject.put("strokes", this.mStrokes);
        jsonObject.put("fore_hand", this.mForeHand);
        jsonObject.put("back_hand", this.mBackHand);
        jsonObject.put("serve", this.mServe);
        jsonObject.put("sec_half_start_time", this.mSecHalfStartTime);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        super.initObjectFromJSON(jsonObject);
        this.mSpeed = (float) jsonObject.optDouble("speed");
        this.mPace = (float) jsonObject.optDouble("pace");
        this.mLatitude = jsonObject.optDouble("latitude");
        this.mLongitude = jsonObject.optDouble("longitude");
        this.mStepFreq = (float) jsonObject.optDouble("step_freq");
        this.mAvgHeartRate = jsonObject.optInt("avg_heart_rate");
        this.mMaxHeartRate = jsonObject.optInt("max_heart_rate");
        this.mMinHeartRate = jsonObject.optInt("min_heart_rate");
        this.mTotalPace = (float) jsonObject.optDouble("total_pace", 0.0d);
        this.mBestPace = (float) jsonObject.optDouble("best_pace", 0.0d);
        this.mMinPace = (float) jsonObject.optDouble("min_pace", 0.0d);
        this.mTotalSpeed = (float) jsonObject.optDouble("total_speed", 0.0d);
        this.mMaxSpeed = (float) jsonObject.optDouble("max_speed", 0.0d);
        this.mMaxStepFreq = (float) jsonObject.optDouble("max_step_freq", 0.0d);
        this.mClimbUp = (float) jsonObject.optDouble("climb_up", 0.0d);
        this.mClimbDown = (float) jsonObject.optDouble("climb_down", 0.0d);
        this.mHighestAltitude = (float) jsonObject.optDouble("high_altitude", -20000.0d);
        this.mLowestAltitude = (float) jsonObject.optDouble("low_altitude", -20000.0d);
        this.mDisPerLap = (float) jsonObject.optDouble("km_per_individual", 0.0d);
        this.mAvgStride = (float) jsonObject.optDouble("avg_stride", -1.0d);
        this.mStepCount = jsonObject.optInt("step_count", 0);
        this.mClimbdisascend = (float) jsonObject.optDouble("total_climb_disacend", -1.0d);
        this.mMaxCadence = (float) jsonObject.optDouble("max_cadence", 0.0d);
        this.mAveCadence = (float) jsonObject.optDouble("ave_cadence", 0.0d);
        this.mDeviceType = jsonObject.optInt("device_type", -1);
        this.mDeviceBrand = jsonObject.optInt("device_source", -1);
        this.mVersion = jsonObject.optInt("summary_version", 12);
        this.mClimbdisDescend = (float) jsonObject.optDouble("descend_distance", 0.0d);
        this.mAscendTime = jsonObject.optLong("ascend_time", 0);
        this.mDescendTime = jsonObject.optLong("descend_time", 0);
        this.mSwolfPerFixedMeters = jsonObject.optInt("swolf", 0);
        this.mTotalStrokes = jsonObject.optInt("total_strokes", 0);
        this.mTotalTrips = jsonObject.optInt("total_trips", 0);
        this.mAvgStrokeSpeed = (float) jsonObject.optDouble("avg_stoke_speed", 0.0d);
        this.mMaxStrokeSpeed = (float) jsonObject.optDouble("max_stoke_speed", 0.0d);
        this.mAvgDistancePerStroke = (float) jsonObject.optDouble("avg_dis_per_stroke", 0.0d);
        this.mSwimPoolLength = jsonObject.optInt("swim_pool_length", 25);
        this.mTE = jsonObject.optInt("te_value", 0);
        this.mSwimStyle = jsonObject.optInt("swim_style", 0);
        this.mUnit = jsonObject.optInt("unit", 0);
        this.intervalType = jsonObject.optInt("interval_type", 0);
        this.mDownHillNum = jsonObject.optInt("downhill_num", 0);
        this.mDownhillMaxAltitudeDescend = (float) jsonObject.optDouble("downhill_max_altitude_descend", -1.0d);
        this.mStrokes = jsonObject.optInt("strokes", 0);
        this.mForeHand = jsonObject.optInt("fore_hand", 0);
        this.mBackHand = jsonObject.optInt("back_hand", 0);
        this.mServe = jsonObject.optInt("serve", 0);
        this.mSecHalfStartTime = jsonObject.optLong("sec_half_start_time", -1);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mCurrentStatus);
        dest.writeInt(this.mSyncCount);
        dest.writeFloat(this.mSpeed);
        dest.writeInt(this.mSportType);
        dest.writeLong(this.mTrackId);
        dest.writeFloat(this.mCalorie);
        dest.writeFloat(this.mDistance);
        dest.writeLong(this.mEndTime);
        dest.writeLong(this.mStartTime);
        dest.writeInt(this.mSportDuration);
        dest.writeFloat(this.mPace);
        dest.writeInt(this.mTotalPausedTime);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeFloat(this.mStepFreq);
        dest.writeInt(this.mAvgHeartRate);
        dest.writeInt(this.mMaxHeartRate);
        dest.writeInt(this.mMinHeartRate);
        dest.writeFloat(this.mTotalPace);
        dest.writeFloat(this.mBestPace);
        dest.writeFloat(this.mMinPace);
        dest.writeFloat(this.mTotalSpeed);
        dest.writeFloat(this.mMaxSpeed);
        dest.writeFloat(this.mMaxStepFreq);
        dest.writeFloat(this.mClimbUp);
        dest.writeFloat(this.mClimbDown);
        dest.writeFloat(this.mHighestAltitude);
        dest.writeFloat(this.mLowestAltitude);
        dest.writeFloat(this.mDisPerLap);
        dest.writeInt(this.mStepCount);
        dest.writeFloat(this.mAvgStride);
        dest.writeFloat(this.mClimbdisascend);
        dest.writeFloat(this.mMaxCadence);
        dest.writeFloat(this.mAveCadence);
        dest.writeInt(this.mDeviceType);
        dest.writeInt(this.mDeviceBrand);
        dest.writeInt(this.mVersion);
        dest.writeFloat(this.mClimbdisDescend);
        dest.writeLong(this.mAscendTime);
        dest.writeLong(this.mDescendTime);
        dest.writeList(this.mChildTrackIds);
        dest.writeList(this.mChildSportTypes);
        dest.writeInt(this.mSwolfPerFixedMeters);
        dest.writeInt(this.mTotalStrokes);
        dest.writeInt(this.mTotalTrips);
        dest.writeFloat(this.mAvgStrokeSpeed);
        dest.writeFloat(this.mMaxStrokeSpeed);
        dest.writeFloat(this.mAvgDistancePerStroke);
        dest.writeInt(this.mSwimPoolLength);
        dest.writeInt(this.mTE);
        dest.writeInt(this.mSwimStyle);
        dest.writeLong(this.mParentTrackId);
        dest.writeList(this.mPauseInfos);
        dest.writeInt(this.mUnit);
        dest.writeInt(this.intervalType);
        dest.writeInt(this.mDownHillNum);
        dest.writeFloat(this.mDownhillMaxAltitudeDescend);
        dest.writeInt(this.mStrokes);
        dest.writeInt(this.mForeHand);
        dest.writeInt(this.mBackHand);
        dest.writeInt(this.mServe);
        dest.writeLong(this.mSecHalfStartTime);
    }

    public OutdoorSportSummary copy() {
        OutdoorSportSummary summary = new OutdoorSportSummary();
        JSONObject jsonObject = new JSONObject();
        try {
            initJSONFromObject(jsonObject);
            summary.initObjectFromJSON(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Debug.m5i("OutdoorSportSummary", "summary:" + summary.toString());
        return summary;
    }
}
