package com.huami.watch.newsport.common.model;

public class HeartRate {
    private int mAltitude = 0;
    private float mDistanceDiff = 0.0f;
    private int mHeartQuality = 0;
    private int mHeartRange = 0;
    private int mHeartRate = 0;
    private float mPace = 0.0f;
    private int mRunTime = 0;
    private int mStepDiff = 0;
    private float mStepFreq = 0.0f;
    private int mStride = 0;
    private float mStrokeSpeed = 0.0f;
    private long mTimestamp = -1;
    private long mTrackId = -1;

    public static final class HeartRangeRegion {
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public int getRunTime() {
        return this.mRunTime;
    }

    public void setRunTime(int runTime) {
        this.mRunTime = runTime;
    }

    public float getDistance() {
        return this.mDistanceDiff;
    }

    public void setDistance(float distance) {
        this.mDistanceDiff = distance;
    }

    public float getStrokeSpeed() {
        return this.mStrokeSpeed;
    }

    public void setStrokeSpeed(float strokeSpeed) {
        this.mStrokeSpeed = strokeSpeed;
    }

    public String toString() {
        return "HeartRate{mTimestamp=" + this.mTimestamp + ", mHeartRate=" + this.mHeartRate + ", mStepFreq=" + this.mStepFreq + ", mAltitude=" + this.mAltitude + ", mPace=" + this.mPace + ", mHeartQuality=" + this.mHeartQuality + ", mHeartRange=" + this.mHeartRange + ", mStepDiff=" + this.mStepDiff + ", mStride=" + this.mStride + ", mRunTime=" + this.mRunTime + ", mTrackId=" + this.mTrackId + ", mDistance=" + this.mDistanceDiff + ", mStrokeSpeed=" + this.mStrokeSpeed + '}';
    }

    public int getHeartRange() {
        return this.mHeartRange;
    }

    public void setHeartRange(int heartRange) {
        this.mHeartRange = heartRange;
    }

    public int getStepDiff() {
        return this.mStepDiff;
    }

    public void setStepDiff(int stepDiff) {
        this.mStepDiff = stepDiff;
    }

    public int getStride() {
        return this.mStride;
    }

    public void setStride(int stride) {
        this.mStride = stride;
    }

    public long getTimestamp() {
        return this.mTimestamp;
    }

    public void setTimestamp(long timestamp) {
        this.mTimestamp = timestamp;
    }

    public int getHeartRate() {
        return this.mHeartRate;
    }

    public void setHeartRate(int heartRate) {
        this.mHeartRate = heartRate;
    }

    public float getStepFreq() {
        return this.mStepFreq;
    }

    public void setStepFreq(float stepFreq) {
        this.mStepFreq = stepFreq;
    }

    public int getAltitude() {
        return this.mAltitude;
    }

    public void setAltitude(int altitude) {
        this.mAltitude = altitude;
    }

    public float getPace() {
        return this.mPace;
    }

    public void setPace(float pace) {
        this.mPace = pace;
    }

    public int getHeartQuality() {
        return this.mHeartQuality;
    }

    public void setHeartQuality(int quality) {
        this.mHeartQuality = quality;
    }
}
