package com.huami.watch.newsport.common.model;

public class RequestInfo {
    private String mAction = null;
    private String mIdentifier = null;
    private int mRequestStat = 11;
    private long mTrackId = -1;

    private RequestInfo() {
    }

    public static RequestInfo createRequest(long trackId, String identifier, String action) {
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.mTrackId = trackId;
        requestInfo.mIdentifier = identifier;
        requestInfo.mAction = action;
        requestInfo.mRequestStat = 11;
        return requestInfo;
    }

    public static RequestInfo createRequest(String identifier, String action) {
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.mIdentifier = identifier;
        requestInfo.mAction = action;
        return requestInfo;
    }

    public String getAction() {
        return this.mAction;
    }

    public void setAction(String action) {
        this.mAction = action;
    }

    public String toString() {
        return "RequestInfo{mTrackId=" + this.mTrackId + ", mIdentifier='" + this.mIdentifier + '\'' + ", mRequestStat=" + this.mRequestStat + '}';
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public String getIdentifier() {
        return this.mIdentifier;
    }

    public int getRequestStat() {
        return this.mRequestStat;
    }

    public void setRequestStat(int requestStat) {
        this.mRequestStat = requestStat;
    }
}
