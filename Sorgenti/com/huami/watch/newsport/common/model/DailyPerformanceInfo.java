package com.huami.watch.newsport.common.model;

import com.huami.watch.common.JSONAble;

public class DailyPerformanceInfo implements JSONAble {
    private static final String TAG = DailyPerformanceInfo.class.getSimpleName();
    private long createTime;
    private float currnetKilos = 0.0f;
    private float dailyPorpermence = 127.0f;
    private long mTrackId = -1;
    private int saveDataType = 0;
    private int teValue = 0;

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public long getmTrackId() {
        return this.mTrackId;
    }

    public void setmTrackId(long mTrackId) {
        this.mTrackId = mTrackId;
    }

    public int getTeValue() {
        return this.teValue;
    }

    public void setTeValue(int teValue) {
        this.teValue = teValue;
    }

    public float getDailyPorpermence() {
        return this.dailyPorpermence;
    }

    public void setDailyPorpermence(float dailyPorpermence) {
        this.dailyPorpermence = dailyPorpermence;
    }

    public int getSaveDataType() {
        return this.saveDataType;
    }

    public void setSaveDataType(int saveDataType) {
        this.saveDataType = saveDataType;
    }

    public long getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public double getCurrnetKilos() {
        return (double) this.currnetKilos;
    }

    public void setCurrnetKilos(float currnetKilos) {
        this.currnetKilos = currnetKilos;
    }

    public String toString() {
        return "DailyPerformanceInfo{mTrackId=" + this.mTrackId + ", teValue=" + this.teValue + ", dailyPorpermence=" + this.dailyPorpermence + ", saveDataType=" + this.saveDataType + ", createTime=" + this.createTime + ", currnetKilos=" + this.currnetKilos + '}';
    }
}
