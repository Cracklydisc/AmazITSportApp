package com.huami.watch.newsport.common.model;

import com.huami.watch.common.JSONAble;
import org.json.JSONException;
import org.json.JSONObject;

public class SportStatistic implements JSONAble {
    private int mTotalCalorie = 0;
    private int mTotalCount = 0;
    private int mTotalDistance = 0;

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public String toString() {
        return "SportStatistic{mTotalDistance=" + this.mTotalDistance + ", mTotalCount=" + this.mTotalCount + ", mTotalCalorie=" + this.mTotalCalorie + '}';
    }

    public int getTotalDistance() {
        return this.mTotalDistance;
    }

    public void setTotalDistance(int totalDistance) {
        this.mTotalDistance = totalDistance;
    }

    public int getTotalCount() {
        return this.mTotalCount;
    }

    public void setTotalCount(int totalCount) {
        this.mTotalCount = totalCount;
    }

    public int getTotalCalorie() {
        return this.mTotalCalorie;
    }

    public void setTotalCalorie(int totalCalorie) {
        this.mTotalCalorie = totalCalorie;
    }

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            initJSONFromObject(jsonObject);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("total_distance", this.mTotalDistance);
        jsonObject.put("total_count", this.mTotalCount);
        jsonObject.put("total_calorie", this.mTotalCalorie);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.mTotalDistance = jsonObject.getInt("total_distance");
        this.mTotalCount = jsonObject.getInt("total_count");
        this.mTotalCalorie = jsonObject.getInt("total_calorie");
    }
}
