package com.huami.watch.newsport.common.model;

import com.huami.watch.common.JSONAble;
import com.huami.watch.common.JSONAble.JSONCreator;

public class RunningInfoPerLap implements JSONAble {
    public static JSONCreator<RunningInfoPerLap> JSON_CREATOR = new C05581();
    private float mAbsoluteAltitude = -20000.0f;
    private float mAvgHeart = -1.0f;
    private double mAvgPace = 0.0d;
    private float mClimbDistance = -1.0f;
    private float mClimbDown = -20000.0f;
    private float mClimbUp = -20000.0f;
    private float mDistance = 0.0f;
    private int mLapCadence = -1;
    private int mLapCalories = 0;
    private int mLapNum = 0;
    private float mLapStepFreq = 0.0f;
    private float mLapStrokeSpeed = -1.0f;
    private int mLapStrokes = -1;
    private int mLapSwolf = -1;
    private int mLapType = 0;
    private float mLatitude = 10000.0f;
    private float mLongitude = 10000.0f;
    private double mMaxPace = 0.0d;
    private float mMaxiMumFall = 0.0f;
    private long mPerLapCostTime = 0;
    private long mTotalCostTime = 0;
    private long mTrackId = -1;

    static class C05581 implements JSONCreator<RunningInfoPerLap> {
        C05581() {
        }
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public void setMaxiMumFall(float maxiMumFall) {
        this.mMaxiMumFall = maxiMumFall;
    }

    public int getLapType() {
        return this.mLapType;
    }

    public void setLapType(int lapType) {
        this.mLapType = lapType;
    }

    public double getMaxPace() {
        return this.mMaxPace;
    }

    public void setMaxPace(double maxPace) {
        this.mMaxPace = maxPace;
    }

    public float getClimbDistance() {
        return this.mClimbDistance;
    }

    public void setClimbDistance(float climbDistance) {
        this.mClimbDistance = climbDistance;
    }

    public int getLapStrokes() {
        return this.mLapStrokes;
    }

    public void setLapStrokes(int lapStrokes) {
        this.mLapStrokes = lapStrokes;
    }

    public float getLapStrokeSpeed() {
        return this.mLapStrokeSpeed;
    }

    public void setLapStrokeSpeed(float lapStrokeSpeed) {
        this.mLapStrokeSpeed = lapStrokeSpeed;
    }

    public int getLapSwolf() {
        return this.mLapSwolf;
    }

    public void setLapSwolf(int lapSwolf) {
        this.mLapSwolf = lapSwolf;
    }

    public int getLapCalories() {
        return this.mLapCalories;
    }

    public void setLapCalories(int lapCalories) {
        this.mLapCalories = lapCalories;
    }

    public float getLapStepFreq() {
        return this.mLapStepFreq;
    }

    public void setLapStepFreq(float lapStepFreq) {
        this.mLapStepFreq = lapStepFreq;
    }

    public int getLapCadence() {
        return this.mLapCadence;
    }

    public void setLapCadence(int lapCadence) {
        this.mLapCadence = lapCadence;
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public double getPace() {
        return this.mAvgPace;
    }

    public void setPace(double pace) {
        this.mAvgPace = pace;
    }

    public long getCostTime() {
        return this.mPerLapCostTime;
    }

    public void setCostTime(long costTime) {
        this.mPerLapCostTime = costTime;
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
    }

    public int getLapNum() {
        return this.mLapNum;
    }

    public void setLapNum(int lapNum) {
        this.mLapNum = lapNum;
    }

    public void setLongitude(float longitude) {
        this.mLongitude = longitude;
    }

    public float getLongitude() {
        return this.mLongitude;
    }

    public void setLatitude(float latitude) {
        this.mLatitude = latitude;
    }

    public float getLatitude() {
        return this.mLatitude;
    }

    public void setAvgHeart(float avgHeart) {
        this.mAvgHeart = avgHeart;
    }

    public float getAvgHeart() {
        return this.mAvgHeart;
    }

    public void setTotalCostTime(long totalCostTime) {
        this.mTotalCostTime = totalCostTime;
    }

    public long getTotalCostTime() {
        return this.mTotalCostTime;
    }

    public void setAbsoluteAltitude(float absoluteAltitude) {
        this.mAbsoluteAltitude = absoluteAltitude;
    }

    public float getAbsoluteAltitude() {
        return this.mAbsoluteAltitude;
    }

    public void setClimbUp(float climbUp) {
        this.mClimbUp = climbUp;
    }

    public float getClimbUp() {
        return this.mClimbUp;
    }

    public void setClimbDown(float climbDown) {
        this.mClimbDown = climbDown;
    }

    public float getClimbDown() {
        return this.mClimbDown;
    }

    public String toString() {
        return "RunningInfoPerLap{mTrackId=" + this.mTrackId + ", mDistance=" + this.mDistance + ", mAvgPace=" + this.mAvgPace + ", mMaxPace=" + this.mMaxPace + ", mClimbDistance=" + this.mClimbDistance + ", mLapStrokes=" + this.mLapStrokes + ", mLapStrokeSpeed=" + this.mLapStrokeSpeed + ", mLapSwolf=" + this.mLapSwolf + ", mLapCalories=" + this.mLapCalories + ", mLapStepFreq=" + this.mLapStepFreq + ", mLapCadence=" + this.mLapCadence + ", mLapType=" + this.mLapType + ", mPerLapCostTime=" + this.mPerLapCostTime + ", mLapNum=" + this.mLapNum + ", mLatitude=" + this.mLatitude + ", mLongitude=" + this.mLongitude + ", mAvgHeart=" + this.mAvgHeart + ", mTotalCostTime=" + this.mTotalCostTime + ", mAbsoluteAltitude=" + this.mAbsoluteAltitude + ", mClimbUp=" + this.mClimbUp + ", mClimbDown=" + this.mClimbDown + ", mMaxiMumFall=" + this.mMaxiMumFall + '}';
    }
}
