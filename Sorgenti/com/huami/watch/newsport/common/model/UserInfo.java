package com.huami.watch.newsport.common.model;

public class UserInfo {
    private int mGender = -1;
    private int mHeight = -1;
    private int mMonth = -1;
    private float mWeight = -1.0f;
    private int mYear = -1;

    public String toString() {
        return "UserInfo{mGender=" + this.mGender + ", mYear=" + this.mYear + ", mMonth=" + this.mMonth + ", mHeight=" + this.mHeight + ", mWeight=" + this.mWeight + '}';
    }

    public int getGender() {
        return this.mGender;
    }

    public void setGender(int gender) {
        this.mGender = gender;
    }

    public int getYear() {
        return this.mYear;
    }

    public void setYear(int year) {
        this.mYear = year;
    }

    public void setMonth(int month) {
        this.mMonth = month;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void setHeight(int height) {
        this.mHeight = height;
    }

    public float getWeight() {
        return this.mWeight;
    }

    public void setWeight(float weight) {
        this.mWeight = weight;
    }
}
