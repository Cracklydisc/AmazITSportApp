package com.huami.watch.newsport.common.model;

import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class SportThaInfo implements Serializable {
    private long createTime;
    private int currnetDayTrainLoad;
    private String dateStr;
    private long dayId;
    private int fitnessClass;
    private int fitnessLevelIncrease;
    private float rideVo2max;
    private float sportVo2Max;
    private int trainingLoadTrend;
    private long updateTime;
    private int uploadStatus = 0;
    private int vo2MaxTread;
    private int wtlStatus;
    private int wtlSum;
    private int wtlSumOptimalMax;
    private int wtlSumOptimalMin;
    private int wtlSumOverreaching;

    public long getDayId() {
        return this.dayId;
    }

    public void setDayId(long dayId) {
        this.dayId = dayId;
    }

    public int getWtlSum() {
        return this.wtlSum;
    }

    public void setWtlSum(int wtlSum) {
        this.wtlSum = wtlSum;
    }

    public int getWtlSumOptimalMax() {
        return this.wtlSumOptimalMax;
    }

    public void setWtlSumOptimalMax(int wtlSumOptimalMax) {
        this.wtlSumOptimalMax = wtlSumOptimalMax;
    }

    public int getWtlSumOptimalMin() {
        return this.wtlSumOptimalMin;
    }

    public void setWtlSumOptimalMin(int wtlSumOptimalMin) {
        this.wtlSumOptimalMin = wtlSumOptimalMin;
    }

    public int getWtlSumOverreaching() {
        return this.wtlSumOverreaching;
    }

    public void setWtlSumOverreaching(int wtlSumOverreaching) {
        this.wtlSumOverreaching = wtlSumOverreaching;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public String getDateStr() {
        return this.dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public int getVo2MaxTread() {
        return this.vo2MaxTread;
    }

    public void setVo2MaxTread(int vo2MaxTread) {
        this.vo2MaxTread = vo2MaxTread;
    }

    public int getTrainingLoadTrend() {
        return this.trainingLoadTrend;
    }

    public void setTrainingLoadTrend(int trainingLoadTrend) {
        this.trainingLoadTrend = trainingLoadTrend;
    }

    public int getWtlStatus() {
        return this.wtlStatus;
    }

    public void setWtlStatus(int wtlStatus) {
        this.wtlStatus = wtlStatus;
    }

    public int getFitnessClass() {
        return this.fitnessClass;
    }

    public void setFitnessClass(int fitnessClass) {
        this.fitnessClass = fitnessClass;
    }

    public int getFitnessLevelIncrease() {
        return this.fitnessLevelIncrease;
    }

    public void setFitnessLevelIncrease(int fitnessLevelIncrease) {
        this.fitnessLevelIncrease = fitnessLevelIncrease;
    }

    public int getCurrnetDayTrainLoad() {
        return this.currnetDayTrainLoad;
    }

    public void setCurrnetDayTrainLoad(int currnetDayTrainLoad) {
        this.currnetDayTrainLoad = currnetDayTrainLoad;
    }

    public float getSportVo2Max() {
        return this.sportVo2Max;
    }

    public void setSportVo2Max(float sportVo2Max) {
        this.sportVo2Max = sportVo2Max;
    }

    public float getRideVo2max() {
        return this.rideVo2max;
    }

    public void setRideVo2max(float rideVo2max) {
        this.rideVo2max = rideVo2max;
    }

    public int getUploadStatus() {
        return this.uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String toString() {
        return "SportThaInfo{dayId=" + this.dayId + ", wtlSum=" + this.wtlSum + ", wtlSumOptimalMax=" + this.wtlSumOptimalMax + ", wtlSumOptimalMin=" + this.wtlSumOptimalMin + ", wtlSumOverreaching=" + this.wtlSumOverreaching + ", vo2MaxTread=" + this.vo2MaxTread + ", trainingLoadTrend=" + this.trainingLoadTrend + ", wtlStatus=" + this.wtlStatus + ", fitnessClass=" + this.fitnessClass + ", fitnessLevelIncrease=" + this.fitnessLevelIncrease + ", createTime=" + this.createTime + ", updateTime=" + this.updateTime + ", dateStr='" + this.dateStr + '\'' + ", currnetDayTrainLoad=" + this.currnetDayTrainLoad + ", sportVo2Max=" + this.sportVo2Max + ", rideVo2max=" + this.rideVo2max + ", uploadStatus=" + this.uploadStatus + '}';
    }

    public String modelToJson() {
        return modelToJsonObject().toString();
    }

    public JSONObject modelToJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dayId", FirstBeatDataUtils.getDayTimeFormat(getDayId()));
            jsonObject.put("wtlSum", getWtlSum());
            jsonObject.put("wtlSumOptimalMax", getWtlSumOptimalMax());
            jsonObject.put("wtlSumOptimalMin", getWtlSumOptimalMin());
            jsonObject.put("wtlSumOverreaching", getWtlSumOverreaching());
            jsonObject.put("updateTime", getUpdateTime());
            jsonObject.put("currnetDayTrainLoad", getCurrnetDayTrainLoad());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public void initModelWithJsonObject(JSONObject jsonObject) {
        this.wtlSum = jsonObject.optInt("wtlSum");
        this.wtlSumOptimalMax = jsonObject.optInt("wtlSumOptimalMax", 0);
        this.wtlSumOptimalMin = jsonObject.optInt("wtlSumOptimalMin", 0);
        this.wtlSumOverreaching = jsonObject.optInt("wtlSumOverreaching", 0);
        this.updateTime = jsonObject.optLong("updateTime");
        this.dateStr = jsonObject.optString("dateStr");
        this.currnetDayTrainLoad = jsonObject.optInt("currnetDayTrainLoad", 0);
        this.dayId = FirstBeatDataUtils.getDayIdByDataStr(jsonObject.optString("dayId"));
    }
}
