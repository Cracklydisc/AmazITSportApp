package com.huami.watch.newsport.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.common.JSONAble;
import org.json.JSONException;
import org.json.JSONObject;

public class PauseInfo implements Parcelable, JSONAble {
    public static final Creator<PauseInfo> CREATOR = new C05561();
    private long mEnd = -1;
    private byte mPauseType = (byte) 2;
    private long mStart = -1;

    static class C05561 implements Creator<PauseInfo> {
        C05561() {
        }

        public PauseInfo createFromParcel(Parcel in) {
            return new PauseInfo(in);
        }

        public PauseInfo[] newArray(int size) {
            return new PauseInfo[size];
        }
    }

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    PauseInfo() {
    }

    public PauseInfo(long start, long end, byte pauseType) {
        this.mStart = start;
        this.mEnd = end;
        this.mPauseType = pauseType;
    }

    protected PauseInfo(Parcel in) {
        this.mStart = in.readLong();
        this.mEnd = in.readLong();
        this.mPauseType = in.readByte();
    }

    public long getStart() {
        return this.mStart;
    }

    public long getEnd() {
        return this.mEnd;
    }

    public byte getPauseType() {
        return this.mPauseType;
    }

    public String toString() {
        return "PauseInfo{mStart=" + this.mStart + ", mEnd=" + this.mEnd + ", mPauseType=" + this.mPauseType + '}';
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("start_time", this.mStart);
        jsonObject.put("end_time", this.mEnd);
        jsonObject.put("pause_type", this.mPauseType);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.mStart = jsonObject.getLong("start_time");
        this.mEnd = jsonObject.getLong("end_time");
        this.mPauseType = (byte) jsonObject.getInt("pause_type");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mStart);
        parcel.writeLong(this.mEnd);
        parcel.writeByte(this.mPauseType);
    }
}
