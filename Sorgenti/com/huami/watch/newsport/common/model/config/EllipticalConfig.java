package com.huami.watch.newsport.common.model.config;

public class EllipticalConfig extends BaseConfig {
    public EllipticalConfig() {
        this.mIsRemindPerKM = false;
    }

    public String getKey() {
        return "12";
    }

    public int getSportType() {
        return 12;
    }
}
