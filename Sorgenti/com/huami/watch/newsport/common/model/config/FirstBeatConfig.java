package com.huami.watch.newsport.common.model.config;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class FirstBeatConfig implements Serializable {
    private float currnetRunVo2max = -1.0f;
    private float currnetWalkingVo2max = -1.0f;
    private float lastRunVo2max = -1.0f;
    private float lastWalkingVo2max = -1.0f;
    private int recoveryTime;
    private long sportFinishTime;

    private static final class ConfigKey {
        private ConfigKey() {
        }
    }

    public static FirstBeatConfig newFirstBeatConfig() {
        return new FirstBeatConfig();
    }

    public String toString() {
        return "FirstBeatConfig{currnetRunVo2max=" + this.currnetRunVo2max + ", currnetWalkingVo2max=" + this.currnetWalkingVo2max + ", recoveryTime=" + this.recoveryTime + ", sportFinishTime=" + this.sportFinishTime + ", lastRunVo2max=" + this.lastRunVo2max + ", lastWalkingVo2max=" + this.lastWalkingVo2max + '}';
    }

    public float getCurrnetRunVo2max() {
        return this.currnetRunVo2max;
    }

    public void setCurrnetRunVo2max(float currnetRunVo2max) {
        this.currnetRunVo2max = currnetRunVo2max;
    }

    public float getCurrnetWalkingVo2max() {
        return this.currnetWalkingVo2max;
    }

    public void setCurrnetWalkingVo2max(float currnetWalkingVo2max) {
        this.currnetWalkingVo2max = currnetWalkingVo2max;
    }

    public float getLastRunVo2max() {
        return this.lastRunVo2max;
    }

    public void setLastRunVo2max(float lastRunVo2max) {
        this.lastRunVo2max = lastRunVo2max;
    }

    public void setLastWalkingVo2max(float lastWalkingVo2max) {
        this.lastWalkingVo2max = lastWalkingVo2max;
    }

    public int getRecoveryTime() {
        return this.recoveryTime;
    }

    public void setRecoveryTime(int recoveryTime) {
        this.recoveryTime = recoveryTime;
    }

    public long getSportFinishTime() {
        return this.sportFinishTime;
    }

    public void setSportFinishTime(long sportFinishTime) {
        this.sportFinishTime = sportFinishTime;
    }

    public String toJSON() {
        JSONObject object = new JSONObject();
        try {
            initJSONFromObject(object);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("currnet_run_vo2max", (double) this.currnetRunVo2max);
        jsonObject.put("current_walking_vo2max", (double) this.currnetWalkingVo2max);
        jsonObject.put("last_run_vo2max", (double) this.lastRunVo2max);
        jsonObject.put("last_walking_vo2max", (double) this.lastWalkingVo2max);
        jsonObject.put("recovery_time", this.recoveryTime);
        jsonObject.put("sport_finish_time", this.sportFinishTime);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.currnetRunVo2max = (float) jsonObject.optDouble("currnet_run_vo2max", -1.0d);
        this.currnetWalkingVo2max = (float) jsonObject.optDouble("current_walking_vo2max", -1.0d);
        this.recoveryTime = jsonObject.optInt("recovery_time", 0);
        this.sportFinishTime = jsonObject.optLong("sport_finish_time", 0);
        this.lastRunVo2max = (float) jsonObject.optDouble("last_run_vo2max", -1.0d);
        this.lastWalkingVo2max = (float) jsonObject.optDouble("last_walking_vo2max", -1.0d);
    }
}
