package com.huami.watch.newsport.common.model.config;

public class SoccerConfig extends BaseConfig {
    public SoccerConfig() {
        this.mIsOpenAutoLapRecord = false;
        this.mIsRemindPerKM = false;
    }

    public String getKey() {
        return String.valueOf(18);
    }

    public int getSportType() {
        return 18;
    }
}
