package com.huami.watch.newsport.common.model.config;

public class MountaineerConfig extends BaseConfig {
    public MountaineerConfig() {
        this.mIs3DSportModel = false;
        this.mIsRemindPerKM = false;
        this.mRealGraphShownType = 2;
    }

    public String getKey() {
        return "13";
    }

    public int getSportType() {
        return 13;
    }
}
