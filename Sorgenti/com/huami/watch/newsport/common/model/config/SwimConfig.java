package com.huami.watch.newsport.common.model.config;

public class SwimConfig extends BaseConfig {
    public SwimConfig() {
        this.mIsRemindPlayVoice = false;
    }

    public String getKey() {
        return "14";
    }

    public int getSportType() {
        return 14;
    }
}
