package com.huami.watch.newsport.common.model.config;

public class IndoorRidingConfig extends BaseConfig {
    public IndoorRidingConfig() {
        this.mIsRemindPerKM = false;
    }

    public String getKey() {
        return "10";
    }

    public int getSportType() {
        return 10;
    }
}
