package com.huami.watch.newsport.common.model.config;

import java.util.List;

public abstract class MixedBaseConfig extends BaseConfig {

    public static final class ConfigKey {
    }

    public abstract List<Integer> getChildSports();

    public abstract void setChildSports(List<Integer> list);

    public String getKey() {
        return null;
    }

    public int getSportType() {
        return 0;
    }
}
