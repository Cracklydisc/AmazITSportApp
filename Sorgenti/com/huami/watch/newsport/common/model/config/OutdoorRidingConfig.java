package com.huami.watch.newsport.common.model.config;

public class OutdoorRidingConfig extends BaseConfig {
    public OutdoorRidingConfig() {
        this.mIs3DSportModel = false;
        this.mRemindKm = 5;
    }

    public String getKey() {
        return "9";
    }

    public int getSportType() {
        return 9;
    }
}
