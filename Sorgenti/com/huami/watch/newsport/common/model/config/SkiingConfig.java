package com.huami.watch.newsport.common.model.config;

public class SkiingConfig extends BaseConfig {
    public SkiingConfig() {
        this.mIsRemindPerKM = false;
        this.mIsRemindPlayVoice = false;
        this.mIsRealTimeGuide = false;
        this.mIsOpenAutoLapRecord = true;
        this.mRealGraphShownType = 4;
        this.mIs3DSportModel = true;
    }

    public String getKey() {
        return "11";
    }

    public int getSportType() {
        return 11;
    }
}
