package com.huami.watch.newsport.common.model.config;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.huami.watch.common.JSONAble;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.SportType;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseConfig implements Parcelable, JSONAble {
    public static final Creator<BaseConfig> CREATOR = new C05601();
    private static int customMaxHeartRate = 0;
    private static int customMinHeartRate = 0;
    private int currentHeartRegionType = 0;
    private float mAutoPausePace = 0.0f;
    private int mBgType = 0;
    private float mCalBurnHeartRate = -1.0f;
    protected List<Integer> mChildSports = new ArrayList();
    private int mDefaultDataShown = 6;
    private float mDistanceAutoLap = 1000.0f;
    protected boolean mHeartRegionRemind = false;
    private int mHeartRegionType = 0;
    private int mHeartRegionValueMax = 0;
    private int mHeartRegionValueMin = 0;
    protected boolean mIs3DSportModel = false;
    protected boolean mIsAutoPause = false;
    protected boolean mIsMeasureHeart = true;
    protected boolean mIsOpenAutoLapRecord = false;
    protected boolean mIsRealTimeGuide = true;
    protected boolean mIsRemindAutoLap = false;
    protected boolean mIsRemindCalBurnHeartRate = false;
    protected boolean mIsRemindCalorie = false;
    protected boolean mIsRemindGPSStatusChange = false;
    protected boolean mIsRemindPerKM = true;
    protected boolean mIsRemindPlayVoice = true;
    protected boolean mIsRemindSafeHeartRate = true;
    protected boolean mIsRemindTE = false;
    protected boolean mIsRemindTargetDistance = false;
    protected boolean mIsRemindTargetPace = false;
    protected boolean mIsRemindTargetTimeCost = false;
    protected boolean mIsRemindTargetTrips = false;
    protected boolean mIsSportSecondBrush = true;
    private int mMonthTargetCostTime = 1200000;
    private int mMonthTargetDistance = 20000;
    protected int mRealGraphShownType = 0;
    protected int mRemindKm = 1;
    private float mSafeHeartRateHigh = -1.0f;
    private float mSafeHeartRateLow = 120.0f;
    private int mTargetCalorie = -1;
    private int mTargetDistance = -1;
    private float mTargetPace = -1.0f;
    protected int mTargetSwimLength = 25;
    private float mTargetTE = 0.0f;
    private int mTargetTEStage = -1;
    private long mTargetTimeCost = -1;
    private int mTargetTrips = 1;
    private int mUnit = 0;
    private int tennicClapThaHands = -1;

    static class C05601 implements Creator<BaseConfig> {
        C05601() {
        }

        public BaseConfig createFromParcel(Parcel source) {
            boolean z;
            boolean z2 = true;
            BaseConfig config = BaseConfig.newSportItemConfig(source.readInt());
            config.mIsAutoPause = source.readByte() == (byte) 1;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindCalBurnHeartRate = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindGPSStatusChange = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindPerKM = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindSafeHeartRate = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindTargetDistance = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindTargetPace = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindTargetTimeCost = z;
            config.mCalBurnHeartRate = source.readFloat();
            config.mTargetTimeCost = source.readLong();
            config.mTargetDistance = source.readInt();
            config.mTargetPace = source.readFloat();
            config.mSafeHeartRateLow = source.readFloat();
            config.mSafeHeartRateHigh = source.readFloat();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsSportSecondBrush = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindPlayVoice = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindCalorie = z;
            config.mTargetCalorie = source.readInt();
            config.mDefaultDataShown = source.readInt();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindAutoLap = z;
            config.mDistanceAutoLap = source.readFloat();
            config.mRealGraphShownType = source.readInt();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIs3DSportModel = z;
            config.mMonthTargetDistance = source.readInt();
            config.mMonthTargetCostTime = source.readInt();
            config.mTargetTrips = source.readInt();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mHeartRegionRemind = z;
            config.mHeartRegionValueMin = source.readInt();
            config.mHeartRegionValueMax = source.readInt();
            config.currentHeartRegionType = source.readInt();
            BaseConfig.customMinHeartRate = source.readInt();
            BaseConfig.customMaxHeartRate = source.readInt();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsOpenAutoLapRecord = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindTE = z;
            config.mTargetTE = source.readFloat();
            config.mTargetSwimLength = source.readInt();
            config.mHeartRegionType = source.readInt();
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsMeasureHeart = z;
            if (source.readByte() == (byte) 1) {
                z = true;
            } else {
                z = false;
            }
            config.mIsRemindTargetTrips = z;
            if (source.readByte() != (byte) 1) {
                z2 = false;
            }
            config.mIsRealTimeGuide = z2;
            config.mAutoPausePace = source.readFloat();
            config.mBgType = source.readInt();
            config.mTargetTEStage = source.readInt();
            config.mUnit = source.readInt();
            config.mRemindKm = source.readInt();
            config.tennicClapThaHands = source.readInt();
            config.mChildSports = source.readArrayList(Integer.class.getClassLoader());
            return config;
        }

        public BaseConfig[] newArray(int i) {
            return new BaseConfig[i];
        }
    }

    private static final class ConfigKey {
        private ConfigKey() {
        }
    }

    public abstract String getKey();

    public abstract int getSportType();

    public int getTargetTEStage() {
        return this.mTargetTEStage;
    }

    public void setTargetTEStage(int targetTEStage) {
        this.mTargetTEStage = targetTEStage;
    }

    public static BaseConfig newSportItemConfig(int sportType) {
        if (!SportType.isSportTypeValid(sportType)) {
            return null;
        }
        switch (sportType) {
            case 1:
                return new RunningConfig();
            case 6:
                return new WalkingConfig();
            case 7:
                return new CrossingConfig();
            case 8:
                return new IndoorRunConfig();
            case 9:
                return new OutdoorRidingConfig();
            case 10:
                return new IndoorRidingConfig();
            case 11:
                return new SkiingConfig();
            case 12:
                return new EllipticalConfig();
            case 13:
                return new MountaineerConfig();
            case 14:
                return new SwimConfig();
            case 15:
                return new OpenSwimConfig();
            case 17:
                return new TennisConfig();
            case 18:
                return new SoccerConfig();
            case 2001:
                return new TriathlonConfig();
            case 2002:
                return new CompoundConfig();
            default:
                return null;
        }
    }

    protected BaseConfig() {
    }

    public String toString() {
        return "BaseConfig{mIsRemindPerKM=" + this.mIsRemindPerKM + ", mIsRemindCalBurnHeartRate=" + this.mIsRemindCalBurnHeartRate + ", mIsRemindSafeHeartRate=" + this.mIsRemindSafeHeartRate + ", mIsAutoPause=" + this.mIsAutoPause + ", mIsRemindTargetTimeCost=" + this.mIsRemindTargetTimeCost + ", mIsRemindTargetDistance=" + this.mIsRemindTargetDistance + ", mIsRemindTargetPace=" + this.mIsRemindTargetPace + ", mIsRemindPlayVoice=" + this.mIsRemindPlayVoice + ", mIsRemindGPSStatusChange=" + this.mIsRemindGPSStatusChange + ", mIsSportSecondBrush=" + this.mIsSportSecondBrush + ", mIsRemindCalorie=" + this.mIsRemindCalorie + ", mIsRemindAutoLap=" + this.mIsRemindAutoLap + ", mTargetTimeCost=" + this.mTargetTimeCost + ", mTargetDistance=" + this.mTargetDistance + ", mTargetPace=" + this.mTargetPace + ", mSafeHeartRateLow=" + this.mSafeHeartRateLow + ", mSafeHeartRateHigh=" + this.mSafeHeartRateHigh + ", mCalBurnHeartRate=" + this.mCalBurnHeartRate + ", mTargetCalorie=" + this.mTargetCalorie + ", mDefaultDataShown=" + this.mDefaultDataShown + ", mRealGraphShownType=" + this.mRealGraphShownType + ", mDistanceAutoLap=" + this.mDistanceAutoLap + ", mIs3DSportModel=" + this.mIs3DSportModel + ", mMonthTargetCostTime=" + this.mMonthTargetCostTime + ", mMonthTargetDistance=" + this.mMonthTargetDistance + ", mTargetTrips=" + this.mTargetTrips + ", mHeartRegionRemind=" + this.mHeartRegionRemind + ", mHeartRegionValueMin=" + this.mHeartRegionValueMin + ", mHeartRegionValueMax=" + this.mHeartRegionValueMax + ", currentHeartRegionType=" + this.currentHeartRegionType + ", customMinHeartRate=" + customMinHeartRate + ", customMaxHeartRate=" + customMaxHeartRate + ", mIsOpenAutoLapRecord=" + this.mIsOpenAutoLapRecord + ", mIsRemindTE=" + this.mIsRemindTE + ", mTargetTE=" + this.mTargetTE + ", mTargetSwimLength=" + this.mTargetSwimLength + ", mIsMeasureHeart=" + this.mIsMeasureHeart + ", mIsRemindTargetTrips=" + this.mIsRemindTargetTrips + ", mIsRealTimeGuide=" + this.mIsRealTimeGuide + ", mAutoPausePace=" + this.mAutoPausePace + ", mBgType=" + this.mBgType + ", mTargetTEStage=" + this.mTargetTEStage + ", mHeartRegionType:" + this.mHeartRegionType + ", mUnit:" + this.mUnit + ", mRemindKm:" + this.mRemindKm + '}';
    }

    public int getRemindKm() {
        return this.mRemindKm;
    }

    public void setRemindKm(int remindKm) {
        this.mRemindKm = remindKm;
    }

    public int getUnit() {
        return this.mUnit;
    }

    public void setUnit(int unit) {
        this.mUnit = unit;
    }

    public void setBgType(int bgType) {
        this.mBgType = bgType;
    }

    public int getBgType() {
        return this.mBgType;
    }

    public float getAutoPausePace() {
        return this.mAutoPausePace;
    }

    public void setAutoPausePace(float autoPausePace) {
        this.mAutoPausePace = autoPausePace;
    }

    public void setIsRealTimeGuide(boolean isRealTimeGuide) {
        this.mIsRealTimeGuide = isRealTimeGuide;
    }

    public boolean isRealTimeGuide() {
        return this.mIsRealTimeGuide;
    }

    public boolean isRemindTargetTrips() {
        return this.mIsRemindTargetTrips;
    }

    public void setIsRemindTargetTrips(boolean isRemindTargetTrips) {
        this.mIsRemindTargetTrips = isRemindTargetTrips;
    }

    public boolean isMeasureHeart() {
        return this.mIsMeasureHeart;
    }

    public void setIsMeasureHeart(boolean isMeasureHeart) {
        this.mIsMeasureHeart = isMeasureHeart;
    }

    public void setTargetSwimLength(int targetSwimLength) {
        this.mTargetSwimLength = targetSwimLength;
    }

    public int getTargetSwimLength() {
        return this.mTargetSwimLength;
    }

    public float getTargetTE() {
        return this.mTargetTE;
    }

    public void setTargetTE(float targetTE) {
        this.mTargetTE = targetTE;
    }

    public boolean isRemindTE() {
        return this.mIsRemindTE;
    }

    public void setIsRemindTE(boolean isRemindTE) {
        this.mIsRemindTE = isRemindTE;
    }

    public boolean isOpenAutoLapRecord() {
        return this.mIsOpenAutoLapRecord;
    }

    public void setIsOpenAutoLapRecord(boolean isOpenAutoLapRecord) {
        this.mIsOpenAutoLapRecord = isOpenAutoLapRecord;
    }

    @Deprecated
    public int getCurrentHeartRegionType() {
        return this.currentHeartRegionType;
    }

    @Deprecated
    public void setCurrentHeartRegionType(int currentHeartRegionType) {
        this.currentHeartRegionType = currentHeartRegionType;
    }

    public int getTargetTrips() {
        return this.mTargetTrips;
    }

    public void setTargetTrips(int targetTrips) {
        this.mTargetTrips = targetTrips;
    }

    public int getMonthTargetCostTime() {
        return this.mMonthTargetCostTime;
    }

    public int getMonthTargetDistance() {
        return this.mMonthTargetDistance;
    }

    public void setMonthTargetCostTime(int monthTargetCostTime) {
        this.mMonthTargetCostTime = monthTargetCostTime;
    }

    public void setMonthTargetDistance(int monthTargetDistance) {
        this.mMonthTargetDistance = monthTargetDistance;
    }

    public boolean is3DSportModel() {
        return this.mIs3DSportModel;
    }

    public void setIs3DSportModel(boolean is3DSportModel) {
        this.mIs3DSportModel = is3DSportModel;
    }

    public int getRealGraphShownType() {
        return this.mRealGraphShownType;
    }

    public void setRealGraphShownType(int realGraphShownType) {
        this.mRealGraphShownType = realGraphShownType;
    }

    public void setIsRemindAutoLap(boolean isRemindAutoLap) {
        this.mIsRemindAutoLap = isRemindAutoLap;
    }

    public boolean isRemindAutoLap() {
        return this.mIsRemindAutoLap;
    }

    public float getDistanceAutoLap() {
        return this.mDistanceAutoLap;
    }

    public void setDistanceAutoLap(float distanceAutoLap) {
        this.mDistanceAutoLap = distanceAutoLap;
    }

    public void setDefaultDataShown(int defaultDataShown) {
        this.mDefaultDataShown = defaultDataShown;
    }

    public int getDefaultDataShown() {
        return this.mDefaultDataShown;
    }

    public boolean isRemindCalorie() {
        return this.mIsRemindCalorie;
    }

    public void setIsRemindCalorie(boolean isRemindCalorie) {
        this.mIsRemindCalorie = isRemindCalorie;
    }

    public boolean isRemindPlayVoice() {
        return this.mIsRemindPlayVoice;
    }

    public void setIsRemindPlayVoice(boolean isRemindPlayVoice) {
        this.mIsRemindPlayVoice = isRemindPlayVoice;
    }

    public void setTargetCalorie(int targetCalorie) {
        this.mTargetCalorie = targetCalorie;
    }

    public int getTargetCalorie() {
        return this.mTargetCalorie;
    }

    public float getSafeHeartRateHigh() {
        return this.mSafeHeartRateHigh;
    }

    public void setSafeHeartRateHigh(float safeHeartRateHigh) {
        this.mSafeHeartRateHigh = safeHeartRateHigh;
    }

    public boolean isRemindTargetTimeCost() {
        return this.mIsRemindTargetTimeCost;
    }

    public void setIsRemindTargetTimeCost(boolean isRemindTargetTimeCost) {
        this.mIsRemindTargetTimeCost = isRemindTargetTimeCost;
    }

    public boolean isRemindTargetDistance() {
        return this.mIsRemindTargetDistance;
    }

    public void setIsRemindTargetDistance(boolean isRemindTargetDistance) {
        this.mIsRemindTargetDistance = isRemindTargetDistance;
    }

    public boolean isRemindTargetPace() {
        return this.mIsRemindTargetPace;
    }

    public void setIsRemindTargetPace(boolean isRemindTargetPace) {
        this.mIsRemindTargetPace = isRemindTargetPace;
    }

    public boolean isRemindPerKM() {
        return this.mIsRemindPerKM;
    }

    public void setIsRemindPerKM(boolean isRemindPerKM) {
        this.mIsRemindPerKM = isRemindPerKM;
    }

    public void setIsRemindCalBurnHeartRate(boolean isRemindCalBurnHeartRate) {
        this.mIsRemindCalBurnHeartRate = isRemindCalBurnHeartRate;
    }

    public boolean isRemindSafeHeartRate() {
        return this.mIsRemindSafeHeartRate;
    }

    public void setIsRemindSafeHeartRate(boolean isRemindSafeHeartRate) {
        this.mIsRemindSafeHeartRate = isRemindSafeHeartRate;
    }

    public boolean isAutoPause() {
        return this.mIsAutoPause;
    }

    public void setIsAutoPause(boolean isAutoPause) {
        this.mIsAutoPause = isAutoPause;
    }

    public long getTargetTimeCost() {
        return this.mTargetTimeCost;
    }

    public void setTargetTimeCost(long targetTimeCost) {
        this.mTargetTimeCost = targetTimeCost;
    }

    public int getTargetDistance() {
        return this.mTargetDistance;
    }

    public void setTargetDistance(int targetDistance) {
        this.mTargetDistance = targetDistance;
    }

    public float getTargetPace() {
        return this.mTargetPace;
    }

    public void setTargetPace(float targetPace) {
        this.mTargetPace = targetPace;
    }

    public boolean isSportSecondBrush() {
        return this.mIsSportSecondBrush;
    }

    public boolean ismHeartRegionRemind() {
        return this.mHeartRegionRemind;
    }

    public void setmHeartRegionRemind(boolean mHeartRegionRemind) {
        this.mHeartRegionRemind = mHeartRegionRemind;
    }

    public int getmHeartRegionValueMin() {
        return this.mHeartRegionValueMin;
    }

    public void setmHeartRegionValueMin(int mHeartRegionValueMin) {
        this.mHeartRegionValueMin = mHeartRegionValueMin;
    }

    public int getmHeartRegionValueMax() {
        return this.mHeartRegionValueMax;
    }

    public void setmHeartRegionValueMax(int mHeartRegionValueMax) {
        this.mHeartRegionValueMax = mHeartRegionValueMax;
    }

    public static int getCustomMinHeartRate() {
        return customMinHeartRate;
    }

    public static void setCustomMinHeartRate(int customMinHeartRate) {
        customMinHeartRate = customMinHeartRate;
    }

    public static int getCustomMaxHeartRate() {
        return customMaxHeartRate;
    }

    public static void setCustomMaxHeartRate(int customMaxHeartRate) {
        customMaxHeartRate = customMaxHeartRate;
    }

    public int getmHeartRegionType() {
        return this.mHeartRegionType;
    }

    public void setmHeartRegionType(int mHeartRegionType) {
        this.mHeartRegionType = mHeartRegionType;
    }

    public int getTennicClapThaHands() {
        return this.tennicClapThaHands;
    }

    public void setTennicClapThaHands(int tennicClapThaHands) {
        this.tennicClapThaHands = tennicClapThaHands;
    }

    public String toJSON() {
        JSONObject object = new JSONObject();
        try {
            initJSONFromObject(object);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        jsonObject.put("v2_is_auto_pause", this.mIsAutoPause);
        jsonObject.put("iv2_s_remind_cal_burn_heartrate", this.mIsRemindCalBurnHeartRate);
        jsonObject.put("v2_is_remind_per_km", this.mIsRemindPerKM);
        jsonObject.put("v2_is_remind_safe_heartrate", this.mIsRemindSafeHeartRate);
        jsonObject.put("v2_irtd", this.mIsRemindTargetDistance);
        jsonObject.put("v2_irtp", this.mIsRemindTargetPace);
        jsonObject.put("v2_irttc", this.mIsRemindTargetTimeCost);
        jsonObject.put("v2_is_remind_calorie", this.mIsRemindCalorie);
        jsonObject.put("v2_target_calorie_cost", this.mTargetCalorie);
        jsonObject.put("v2_heart_rate_burn_heartrate", (double) this.mCalBurnHeartRate);
        jsonObject.put("v2_target_time_cost", this.mTargetTimeCost);
        jsonObject.put("v2_target_distance", this.mTargetDistance);
        jsonObject.put("v2_target_pace", (double) this.mTargetPace);
        jsonObject.put("v2_safe_heartrate_low", (double) this.mSafeHeartRateLow);
        jsonObject.put("v2_safe_heartrate_high", (double) this.mSafeHeartRateHigh);
        jsonObject.put("v2_sport_second_brush", this.mIsSportSecondBrush);
        jsonObject.put("v2_sport_play_voice", this.mIsRemindPlayVoice);
        jsonObject.put("v2_sport_data_source_count", this.mDefaultDataShown);
        jsonObject.put("v2_is_remind_auto_lap", this.mIsRemindAutoLap);
        jsonObject.put("v2_sport_auto_lap_dis", (double) this.mDistanceAutoLap);
        jsonObject.put("v2_sport_real_graph_type", this.mRealGraphShownType);
        jsonObject.put("v2_sport_3d_sport_is_on", this.mIs3DSportModel);
        jsonObject.put("v2_sport_month_target_distance", this.mMonthTargetDistance);
        jsonObject.put("v2_sport_month_target_cost_time", this.mMonthTargetCostTime);
        jsonObject.put("v2_sport_target_trips", this.mTargetTrips);
        jsonObject.put("v2_heart_region_remind", this.mHeartRegionRemind);
        jsonObject.put("v2_heart_region_value_min", this.mHeartRegionValueMin);
        jsonObject.put("v2_heart_region_value_max", this.mHeartRegionValueMax);
        jsonObject.put("v2_current_heart_region_type", this.currentHeartRegionType);
        jsonObject.put("v2_custom_min_heart_region", customMinHeartRate);
        jsonObject.put("v2_custom_max_heart_region", customMaxHeartRate);
        jsonObject.put("v2_sport_target_is_open_auto_lap", this.mIsOpenAutoLapRecord);
        jsonObject.put("v2_sport_remind_te", this.mIsRemindTE);
        jsonObject.put("v2_sport_target_te", (double) this.mTargetTE);
        jsonObject.put("v2_sport_target_swim_length", this.mTargetSwimLength);
        jsonObject.put("heart_region_content", this.mHeartRegionType);
        jsonObject.put("v2_sport_measure_heart", this.mIsMeasureHeart);
        jsonObject.put("v2_sport_remind_target_trips", this.mIsRemindTargetTrips);
        jsonObject.put("v2_sport_remind_rt_guide", this.mIsRealTimeGuide);
        jsonObject.put("v2_sport_remind_auto_pause_pace", (double) this.mAutoPausePace);
        jsonObject.put("v2_sport_bg_type", this.mBgType);
        jsonObject.put("v2_te_stage", this.mTargetTEStage);
        jsonObject.put("v2_sport_unit", this.mUnit);
        jsonObject.put("v2_sport_remind_km", this.mRemindKm);
        jsonObject.put("tennic_clap_hands", this.tennicClapThaHands);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        this.mIsAutoPause = jsonObject.optBoolean("v2_is_auto_pause", this.mIsAutoPause);
        this.mIsRemindCalBurnHeartRate = jsonObject.optBoolean("iv2_s_remind_cal_burn_heartrate", this.mIsRemindCalBurnHeartRate);
        this.mIsRemindPerKM = jsonObject.optBoolean("v2_is_remind_per_km", this.mIsRemindPerKM);
        this.mIsRemindSafeHeartRate = jsonObject.optBoolean("v2_is_remind_safe_heartrate", this.mIsRemindSafeHeartRate);
        this.mIsRemindTargetDistance = jsonObject.optBoolean("v2_irtd", this.mIsRemindTargetDistance);
        this.mIsRemindTargetPace = jsonObject.optBoolean("v2_irtp", this.mIsRemindTargetPace);
        this.mIsRemindTargetTimeCost = jsonObject.optBoolean("v2_irttc", this.mIsRemindTargetTimeCost);
        this.mCalBurnHeartRate = (float) jsonObject.getDouble("v2_heart_rate_burn_heartrate");
        this.mTargetTimeCost = jsonObject.getLong("v2_target_time_cost");
        this.mTargetDistance = jsonObject.getInt("v2_target_distance");
        this.mTargetPace = (float) jsonObject.getDouble("v2_target_pace");
        this.mSafeHeartRateLow = (float) jsonObject.getDouble("v2_safe_heartrate_low");
        this.mSafeHeartRateHigh = (float) jsonObject.getDouble("v2_safe_heartrate_high");
        this.mIsSportSecondBrush = jsonObject.optBoolean("v2_sport_second_brush", this.mIsSportSecondBrush);
        this.mIsRemindPlayVoice = jsonObject.optBoolean("v2_sport_play_voice", this.mIsRemindPlayVoice);
        this.mIsRemindCalorie = jsonObject.optBoolean("v2_is_remind_calorie", this.mIsRemindCalorie);
        this.mTargetCalorie = jsonObject.getInt("v2_target_calorie_cost");
        this.mDefaultDataShown = jsonObject.optInt("v2_sport_data_source_count", 4);
        if (this.mSafeHeartRateHigh < 97.0f || this.mSafeHeartRateHigh > 202.0f) {
            this.mSafeHeartRateHigh = (float) UserInfoManager.getDefaultSafeHeart(Global.getApplicationContext());
        }
        this.mIsRemindAutoLap = jsonObject.optBoolean("v2_is_remind_auto_lap", this.mIsRemindAutoLap);
        this.mDistanceAutoLap = (float) jsonObject.optDouble("v2_sport_auto_lap_dis", 1000.0d);
        this.mRealGraphShownType = jsonObject.optInt("v2_sport_real_graph_type", this.mRealGraphShownType);
        this.mIs3DSportModel = jsonObject.optBoolean("v2_sport_3d_sport_is_on", this.mIs3DSportModel);
        this.mMonthTargetDistance = jsonObject.optInt("v2_sport_month_target_distance", 20000);
        this.mMonthTargetCostTime = jsonObject.optInt("v2_sport_month_target_cost_time", 1200000);
        this.mTargetTrips = jsonObject.optInt("v2_sport_target_trips", 1);
        this.mHeartRegionRemind = jsonObject.getBoolean("v2_heart_region_remind");
        this.mHeartRegionValueMin = jsonObject.getInt("v2_heart_region_value_min");
        this.mHeartRegionValueMax = jsonObject.getInt("v2_heart_region_value_max");
        this.currentHeartRegionType = jsonObject.getInt("v2_current_heart_region_type");
        customMinHeartRate = jsonObject.getInt("v2_custom_min_heart_region");
        customMaxHeartRate = jsonObject.getInt("v2_custom_max_heart_region");
        this.mIsOpenAutoLapRecord = jsonObject.optBoolean("v2_sport_target_is_open_auto_lap", this.mIsOpenAutoLapRecord);
        this.mIsRemindTE = jsonObject.optBoolean("v2_sport_remind_te", this.mIsRemindTE);
        this.mTargetTE = (float) jsonObject.optDouble("v2_sport_target_te", 0.0d);
        this.mTargetSwimLength = jsonObject.optInt("v2_sport_target_swim_length", 25);
        this.mHeartRegionType = jsonObject.optInt("heart_region_content", 0);
        this.mIsMeasureHeart = jsonObject.optBoolean("v2_sport_measure_heart", true);
        this.mIsRemindTargetTrips = jsonObject.optBoolean("v2_sport_remind_target_trips", false);
        this.mIsRealTimeGuide = jsonObject.optBoolean("v2_sport_remind_rt_guide", this.mIsRealTimeGuide);
        this.mAutoPausePace = (float) jsonObject.optDouble("v2_sport_remind_auto_pause_pace", 0.0d);
        this.mBgType = jsonObject.optInt("v2_sport_bg_type", 0);
        this.mTargetTEStage = jsonObject.optInt("v2_te_stage", 0);
        this.mUnit = jsonObject.optInt("v2_sport_unit", 0);
        this.mRemindKm = jsonObject.optInt("v2_sport_remind_km", this.mRemindKm);
        this.tennicClapThaHands = jsonObject.optInt("tennic_clap_hands", -1);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        byte b;
        byte b2 = (byte) 1;
        dest.writeInt(getSportType());
        dest.writeByte(this.mIsAutoPause ? (byte) 1 : (byte) 0);
        if (this.mIsRemindCalBurnHeartRate) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindGPSStatusChange) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindPerKM) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindSafeHeartRate) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindTargetDistance) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindTargetPace) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindTargetTimeCost) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeFloat(this.mCalBurnHeartRate);
        dest.writeLong(this.mTargetTimeCost);
        dest.writeInt(this.mTargetDistance);
        dest.writeFloat(this.mTargetPace);
        dest.writeFloat(this.mSafeHeartRateLow);
        dest.writeFloat(this.mSafeHeartRateHigh);
        if (this.mIsSportSecondBrush) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindPlayVoice) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindCalorie) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeInt(this.mTargetCalorie);
        dest.writeInt(this.mDefaultDataShown);
        if (this.mIsRemindAutoLap) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeFloat(this.mDistanceAutoLap);
        dest.writeInt(this.mRealGraphShownType);
        if (this.mIs3DSportModel) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeInt(this.mMonthTargetDistance);
        dest.writeInt(this.mMonthTargetCostTime);
        dest.writeInt(this.mTargetTrips);
        if (this.mHeartRegionRemind) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeInt(this.mHeartRegionValueMin);
        dest.writeInt(this.mHeartRegionValueMax);
        dest.writeInt(this.currentHeartRegionType);
        dest.writeInt(customMinHeartRate);
        dest.writeInt(customMaxHeartRate);
        if (this.mIsOpenAutoLapRecord) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindTE) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeFloat(this.mTargetTE);
        dest.writeInt(this.mTargetSwimLength);
        dest.writeInt(this.mHeartRegionType);
        if (this.mIsMeasureHeart) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (this.mIsRemindTargetTrips) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        if (!this.mIsRealTimeGuide) {
            b2 = (byte) 0;
        }
        dest.writeByte(b2);
        dest.writeFloat(this.mAutoPausePace);
        dest.writeInt(this.mBgType);
        dest.writeInt(this.mTargetTEStage);
        dest.writeInt(this.mUnit);
        dest.writeInt(this.mRemindKm);
        dest.writeInt(this.tennicClapThaHands);
        dest.writeList(this.mChildSports);
    }
}
