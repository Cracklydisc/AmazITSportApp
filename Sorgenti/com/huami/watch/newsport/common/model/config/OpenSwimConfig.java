package com.huami.watch.newsport.common.model.config;

public class OpenSwimConfig extends BaseConfig {
    public OpenSwimConfig() {
        this.mIsRemindPerKM = false;
        this.mIsRemindPlayVoice = false;
        this.mTargetSwimLength = 100;
    }

    public String getKey() {
        return "15";
    }

    public int getSportType() {
        return 15;
    }
}
