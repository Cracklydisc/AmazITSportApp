package com.huami.watch.newsport.common.model.config;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CompoundConfig extends MixedBaseConfig {
    private static final List<Integer> DEFAULT_CHILD_SPORTS = new LinkedList();

    static {
        DEFAULT_CHILD_SPORTS.add(Integer.valueOf(1009));
        DEFAULT_CHILD_SPORTS.add(Integer.valueOf(1001));
    }

    public CompoundConfig() {
        this.mTargetSwimLength = 100;
    }

    public String getKey() {
        return "2002";
    }

    public int getSportType() {
        return 2002;
    }

    public void setChildSports(List<Integer> childSports) {
        if (this.mChildSports == null) {
            this.mChildSports = new ArrayList();
        }
        this.mChildSports.clear();
        this.mChildSports.addAll(childSports);
    }

    public List<Integer> getChildSports() {
        List<Integer> childSports = new LinkedList();
        if (this.mChildSports.size() < 1) {
            childSports.addAll(DEFAULT_CHILD_SPORTS);
        } else {
            childSports.addAll(this.mChildSports);
        }
        return childSports;
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        super.initJSONFromObject(jsonObject);
        JSONArray jsonArray = new JSONArray();
        for (Integer childType : getChildSports()) {
            jsonArray.put(childType);
        }
        jsonObject.put("key_child_sports", jsonArray);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        super.initObjectFromJSON(jsonObject);
        JSONArray jsonArray = jsonObject.optJSONArray("key_child_sports");
        this.mChildSports.clear();
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                this.mChildSports.add(Integer.valueOf(jsonArray.optInt(i)));
            }
            return;
        }
        this.mChildSports.addAll(DEFAULT_CHILD_SPORTS);
    }
}
