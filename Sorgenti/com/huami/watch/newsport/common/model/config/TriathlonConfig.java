package com.huami.watch.newsport.common.model.config;

import java.util.LinkedList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class TriathlonConfig extends MixedBaseConfig {
    private static final List<Integer> DEFAULT_CHILD_SPORTS = new LinkedList();

    static {
        DEFAULT_CHILD_SPORTS.add(Integer.valueOf(1015));
        DEFAULT_CHILD_SPORTS.add(Integer.valueOf(1009));
        DEFAULT_CHILD_SPORTS.add(Integer.valueOf(1001));
    }

    public TriathlonConfig() {
        this.mTargetSwimLength = 100;
    }

    public String getKey() {
        return "2001";
    }

    public int getSportType() {
        return 2001;
    }

    public void setChildSports(List<Integer> list) {
    }

    public List<Integer> getChildSports() {
        List<Integer> childSports = new LinkedList();
        if (this.mChildSports.size() < 1) {
            childSports.addAll(DEFAULT_CHILD_SPORTS);
        } else {
            childSports.addAll(this.mChildSports);
        }
        return childSports;
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        super.initJSONFromObject(jsonObject);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        super.initObjectFromJSON(jsonObject);
    }
}
