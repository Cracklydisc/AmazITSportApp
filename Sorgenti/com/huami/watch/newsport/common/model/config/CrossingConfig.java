package com.huami.watch.newsport.common.model.config;

public class CrossingConfig extends BaseConfig {
    public CrossingConfig() {
        this.mIs3DSportModel = false;
    }

    public String getKey() {
        return "7";
    }

    public int getSportType() {
        return 7;
    }
}
