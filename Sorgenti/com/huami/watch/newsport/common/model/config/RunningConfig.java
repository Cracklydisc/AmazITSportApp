package com.huami.watch.newsport.common.model.config;

import com.huami.watch.newsport.utils.UnitConvertUtils;

public class RunningConfig extends BaseConfig {
    public RunningConfig() {
        if (UnitConvertUtils.isHuangheMode()) {
            this.mIsRealTimeGuide = false;
        }
    }

    public String getKey() {
        return "1";
    }

    public int getSportType() {
        return 1;
    }
}
