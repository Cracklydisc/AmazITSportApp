package com.huami.watch.newsport.common.model.config;

public class TennisConfig extends BaseConfig {
    public TennisConfig() {
        this.mIsOpenAutoLapRecord = false;
    }

    public String getKey() {
        return String.valueOf(17);
    }

    public int getSportType() {
        return 17;
    }
}
