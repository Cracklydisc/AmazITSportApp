package com.huami.watch.newsport.common.model;

import com.huami.watch.newsport.train.model.TrainProgram;

public class SportOptions {
    private int intervalType = 0;
    private int mGpsStatus = 2;
    private boolean mIsTrainningMode = false;
    private int mSessionId = -1;
    private int mSportType = -1;
    private TrainProgram mTrainProgram = null;
    private boolean mUseGPS = false;

    public static class Builder {
        private SportOptions mSportOptions = new SportOptions();

        public Builder setSportType(int sportType) {
            this.mSportOptions.mSportType = sportType;
            return this;
        }

        public Builder setGpsStatus(int gpsStatus) {
            this.mSportOptions.mGpsStatus = gpsStatus;
            return this;
        }

        public Builder setIsTrainningMode(boolean isTrainningMode) {
            this.mSportOptions.mIsTrainningMode = isTrainningMode;
            return this;
        }

        public Builder setTrainProgram(TrainProgram trainProgram) {
            this.mSportOptions.setTrainProgram(trainProgram);
            return this;
        }

        public Builder setIntervalType(int intervalType) {
            this.mSportOptions.setIntervalType(intervalType);
            return this;
        }

        public SportOptions build() {
            return this.mSportOptions;
        }
    }

    public String toString() {
        return "SportOptions{mSportType=" + this.mSportType + ", mUseGPS=" + this.mUseGPS + ", mIsTrainningMode=" + this.mIsTrainningMode + ", mSessionId=" + this.mSessionId + ", mGpsStatus=" + this.mGpsStatus + '}';
    }

    public int getGpsStatus() {
        return this.mGpsStatus;
    }

    public int getSportType() {
        return this.mSportType;
    }

    public int getIntervalType() {
        return this.intervalType;
    }

    public void setIntervalType(int intervalType) {
        this.intervalType = intervalType;
    }

    public boolean isTranningMode() {
        return this.mIsTrainningMode;
    }

    public void setTrainProgram(TrainProgram trainProgram) {
        this.mTrainProgram = trainProgram;
    }

    public TrainProgram getTrainProgram() {
        return this.mTrainProgram;
    }
}
