package com.huami.watch.newsport.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class SortInfo implements Parcelable {
    public static final Creator<SortInfo> CREATOR = new C05591();
    private int count;
    private long time;
    private int type;

    static class C05591 implements Creator<SortInfo> {
        C05591() {
        }

        public SortInfo createFromParcel(Parcel in) {
            return new SortInfo(in);
        }

        public SortInfo[] newArray(int size) {
            return new SortInfo[size];
        }
    }

    protected SortInfo(Parcel in) {
        this.type = in.readInt();
        this.count = in.readInt();
        this.time = in.readLong();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeInt(this.count);
        dest.writeLong(this.time);
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String toString() {
        return "SortInfo{type=" + this.type + ", count=" + this.count + ", time=" + this.time + '}';
    }
}
