package com.huami.watch.newsport.common.model;

import android.os.Parcelable;
import com.huami.watch.common.JSONAble;
import com.huami.watch.common.log.Debug;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class SportSummary implements Parcelable, JSONAble {
    protected float mCalorie = 0.0f;
    protected List<Integer> mChildSportTypes = new LinkedList();
    protected List<Long> mChildTrackIds = new LinkedList();
    protected int mCurrentStatus = 0;
    protected float mDistance = 0.0f;
    protected long mEndTime = -1;
    protected long mParentTrackId = -1;
    protected List<PauseInfo> mPauseInfos = new LinkedList();
    protected int mSportDuration = 0;
    protected int mSportType = -1;
    protected long mStartTime = -1;
    protected int mSyncCount = 0;
    protected int mTotalPausedTime = 0;
    protected long mTrackId = -1;

    private static class JSONKeys {
        private JSONKeys() {
        }
    }

    public static final class SportFlag {
    }

    public static final class SportStatus {
    }

    public long getParentTrackId() {
        return this.mParentTrackId;
    }

    public void setParentTrackId(long parentTrackId) {
        this.mParentTrackId = parentTrackId;
    }

    public List<Long> getChildSportSummrys() {
        return this.mChildTrackIds;
    }

    public List<Integer> getChildTypes() {
        return this.mChildSportTypes;
    }

    public void clearChildTrackIdsAndTypes() {
        this.mChildTrackIds.clear();
        this.mChildSportTypes.clear();
    }

    public void addChildSportSummary(long summaryTrackId, int sportType) {
        this.mChildTrackIds.add(Long.valueOf(summaryTrackId));
        this.mChildSportTypes.add(Integer.valueOf(sportType));
    }

    public List<PauseInfo> getPauseInfos() {
        return this.mPauseInfos;
    }

    public int getTrimedDistance() {
        return (((int) this.mDistance) / 10) * 10;
    }

    public void addPauseInfo(PauseInfo pauseInfo) {
        this.mPauseInfos.add(pauseInfo);
    }

    public int getSyncCount() {
        return this.mSyncCount;
    }

    public void addSyncCount() {
        this.mSyncCount++;
    }

    public int getTotalPausedTime() {
        return this.mTotalPausedTime;
    }

    public void setTotalPausedTime(int totalPausedTime) {
        this.mTotalPausedTime = totalPausedTime;
    }

    public int getSportDuration() {
        return this.mSportDuration;
    }

    public void setSportDuration(int sportDuration) {
        this.mSportDuration = sportDuration;
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
    }

    public int getCurrentStatus() {
        return this.mCurrentStatus;
    }

    public void setCurrentStatus(int currentStatus) {
        this.mCurrentStatus = currentStatus;
    }

    public int getSportType() {
        return this.mSportType;
    }

    public void setSportType(int sportType) {
        this.mSportType = sportType;
    }

    public float getCalorie() {
        return this.mCalorie;
    }

    public void setCalorie(float calorie) {
        this.mCalorie = calorie;
    }

    public long getEndTime() {
        return this.mEndTime;
    }

    public long getTrimEndTime() {
        return (this.mEndTime / 1000) * 1000;
    }

    public long getTrimStartTime() {
        return (this.mStartTime / 1000) * 1000;
    }

    public int getTrimSportDuration() {
        return (int) ((((long) this.mSportDuration) / 1000) * 1000);
    }

    public void setEndTime(long endTime) {
        this.mEndTime = endTime;
    }

    public long getStartTime() {
        return this.mStartTime;
    }

    public void setStartTime(long startTime) {
        this.mStartTime = startTime;
    }

    public long getTrackId() {
        return this.mTrackId;
    }

    public void setTrackId(long trackId) {
        this.mTrackId = trackId;
    }

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            initJSONFromObject(jsonObject);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String parseChildListToJson() {
        try {
            JSONArray childListJsonArray = new JSONArray();
            for (int i = 0; i < this.mChildTrackIds.size(); i++) {
                long id = ((Long) this.mChildTrackIds.get(i)).longValue();
                int type = ((Integer) this.mChildSportTypes.get(i)).intValue();
                JSONObject childJSON = new JSONObject();
                childJSON.put("trackid", id / 1000);
                childJSON.put("type", type);
                childListJsonArray.put(i, childJSON);
            }
            return childListJsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void parseStrToChildList(String childListStr) {
        try {
            JSONArray childListJSONArr = new JSONArray(childListStr);
            if (childListJSONArr != null) {
                for (int i = 0; i < childListJSONArr.length(); i++) {
                    JSONObject object = childListJSONArr.getJSONObject(i);
                    long trackId = object.getLong("trackid") * 1000;
                    int type = object.getInt("type");
                    if (!this.mChildTrackIds.contains(Long.valueOf(trackId))) {
                        this.mChildTrackIds.add(Long.valueOf(trackId));
                        this.mChildSportTypes.add(Integer.valueOf(type));
                    }
                }
                return;
            }
            Debug.m6w("SportSummary", "child summarys info is null");
        } catch (JSONException e) {
            e.printStackTrace();
            Debug.m5i("test_summary", "parse err:" + e.getLocalizedMessage());
        }
    }

    public void initJSONFromObject(JSONObject jsonObject) throws JSONException {
        int i;
        jsonObject.put("track_id", this.mTrackId);
        jsonObject.put("calorie", (double) this.mCalorie);
        jsonObject.put("end_time", this.mEndTime);
        jsonObject.put("start_time", this.mStartTime);
        jsonObject.put("distance", (double) this.mDistance);
        jsonObject.put("sport_type", this.mSportType);
        jsonObject.put("duration", this.mSportDuration);
        jsonObject.put("paused_time", this.mTotalPausedTime);
        jsonObject.put("update_count", this.mSyncCount);
        JSONArray childListJsonArray = new JSONArray();
        for (i = 0; i < this.mChildTrackIds.size(); i++) {
            long id = ((Long) this.mChildTrackIds.get(i)).longValue();
            int type = ((Integer) this.mChildSportTypes.get(i)).intValue();
            JSONObject childJSON = new JSONObject();
            childJSON.put("trackid", id);
            childJSON.put("type", type);
            childListJsonArray.put(i, childJSON);
        }
        jsonObject.put("child_list", childListJsonArray);
        Debug.m5i("SportSummary", "childjson:" + childListJsonArray);
        i = 0;
        JSONArray pauseInfos = new JSONArray();
        for (PauseInfo p : this.mPauseInfos) {
            JSONObject pauseInfoJson = new JSONObject();
            p.initJSONFromObject(pauseInfoJson);
            int i2 = i + 1;
            pauseInfos.put(i, pauseInfoJson);
            i = i2;
        }
        jsonObject.put("pause_info", pauseInfos);
        jsonObject.put("parent_trackid", this.mParentTrackId);
    }

    public void initObjectFromJSON(JSONObject jsonObject) throws JSONException {
        int i;
        this.mTrackId = jsonObject.optLong("track_id");
        this.mCalorie = (float) jsonObject.optDouble("calorie");
        this.mStartTime = jsonObject.optLong("start_time");
        this.mEndTime = jsonObject.optLong("end_time");
        this.mDistance = (float) jsonObject.optDouble("distance");
        this.mSportType = jsonObject.optInt("sport_type");
        this.mSportDuration = jsonObject.optInt("duration");
        this.mTotalPausedTime = jsonObject.optInt("paused_time");
        this.mSyncCount = jsonObject.optInt("update_count");
        JSONArray childListJSONArr = jsonObject.optJSONArray("child_list");
        this.mChildSportTypes.clear();
        this.mChildTrackIds.clear();
        if (childListJSONArr != null) {
            for (i = 0; i < childListJSONArr.length(); i++) {
                JSONObject object = childListJSONArr.getJSONObject(i);
                long trackId = object.getLong("trackid");
                int type = object.getInt("type");
                if (!this.mChildTrackIds.contains(Long.valueOf(trackId))) {
                    this.mChildTrackIds.add(Long.valueOf(trackId));
                    this.mChildSportTypes.add(Integer.valueOf(type));
                }
            }
        } else {
            Debug.m6w("SportSummary", "child summarys info is null");
        }
        JSONArray pauseInfo = jsonObject.optJSONArray("pause_info");
        if (pauseInfo != null) {
            for (i = 0; i < pauseInfo.length(); i++) {
                try {
                    JSONObject pauseInfoJson = pauseInfo.getJSONObject(i);
                    PauseInfo p = new PauseInfo();
                    p.initObjectFromJSON(pauseInfoJson);
                    this.mPauseInfos.add(p);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Debug.m6w("SportSummary", "pause info is null");
        }
        this.mParentTrackId = jsonObject.optLong("parent_trackid", -1);
    }

    public String toString() {
        return "SportSummary{mCalorie=" + this.mCalorie + ", mEndTime=" + this.mEndTime + ", mStartTime=" + this.mStartTime + ", mTrackId=" + this.mTrackId + ", mDistance=" + this.mDistance + ", mSportType=" + this.mSportType + ", mCurrentStatus=" + this.mCurrentStatus + ", mSportDuration=" + this.mSportDuration + ", mTotalPausedTime=" + this.mTotalPausedTime + ", mSyncCount=" + this.mSyncCount + ", mParentTrackId=" + this.mParentTrackId + ", mPauseInfos=" + this.mPauseInfos + '}';
    }
}
