package com.huami.watch.newsport.common.model;

public class RealTimeGuidanceModel {
    private String guideDesc;
    private String guideTitle;
    private int id;
    private int phraseNumber;

    public void setPhraseNumber(int phraseNumber) {
        this.phraseNumber = phraseNumber;
    }

    public String getGuideTitle() {
        return this.guideTitle;
    }

    public void setGuideTitle(String guideTitle) {
        this.guideTitle = guideTitle;
    }

    public String getGuideDesc() {
        return this.guideDesc;
    }

    public void setGuideDesc(String guideDesc) {
        this.guideDesc = guideDesc;
    }

    public String toString() {
        return "RealTimeGuidanceModel{id=" + this.id + ", phraseNumber=" + this.phraseNumber + ", guideTitle='" + this.guideTitle + '\'' + ", guideDesc='" + this.guideDesc + '\'' + '}';
    }
}
