package com.huami.watch.newsport.common.model;

import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class Vo2maxDayInfo implements Serializable {
    private long dayId;
    private long updateTime;
    private int uploadStatus;
    private float vo2MaxWalking = -1.0f;
    private float vo2maxRun = -1.0f;

    public long getDayId() {
        return this.dayId;
    }

    public void setDayId(long dayId) {
        this.dayId = dayId;
    }

    public float getVo2maxRun() {
        return this.vo2maxRun;
    }

    public void setVo2maxRun(float vo2maxRun) {
        this.vo2maxRun = vo2maxRun;
    }

    public float getVo2MaxWalking() {
        return this.vo2MaxWalking;
    }

    public void setVo2MaxWalking(float vo2MaxWalking) {
        this.vo2MaxWalking = vo2MaxWalking;
    }

    public long getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getUploadStatus() {
        return this.uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public String toString() {
        return "Vo2maxDayInfo{dayId=" + this.dayId + ", vo2maxRun=" + this.vo2maxRun + ", vo2MaxWalking=" + this.vo2MaxWalking + ", updateTime=" + this.updateTime + ", uploadStatus=" + this.uploadStatus + '}';
    }

    public String modelToJson() {
        return modelToJsonObject().toString();
    }

    public JSONObject modelToJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dayId", FirstBeatDataUtils.getDayTimeFormat(getDayId()));
            jsonObject.put("vo2_max_run", (double) getVo2maxRun());
            jsonObject.put("vo2_max_walking", (double) getVo2MaxWalking());
            jsonObject.put("update_time", getUpdateTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public void initModelWithJsonObject(JSONObject jsonObject) {
        this.dayId = FirstBeatDataUtils.getDayIdByDataStr(jsonObject.optString("dayId"));
        this.vo2maxRun = (float) jsonObject.optDouble("vo2_max_run", -1.0d);
        this.vo2MaxWalking = (float) jsonObject.optDouble("vo2_max_walking", -1.0d);
        this.updateTime = jsonObject.optLong("update_time", 0);
    }
}
