package com.huami.watch.newsport.common.model;

import android.text.TextUtils;

public class StepConfig {
    String learnArray;
    String stepArray;

    public StepConfig(String learnArray, String stepArray) {
        this.learnArray = learnArray;
        this.stepArray = stepArray;
    }

    public String getStepArray() {
        return this.stepArray;
    }

    public void setStepArray(String stepArray) {
        this.stepArray = stepArray;
    }

    public String getLearnArray() {
        return this.learnArray;
    }

    public void setLearnArray(String learnArray) {
        this.learnArray = learnArray;
    }

    public boolean isEmpty() {
        if (TextUtils.isEmpty(this.learnArray) && TextUtils.isEmpty(this.stepArray)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "StepConfig{learnArray='" + this.learnArray + '\'' + ", stepArray='" + this.stepArray + '\'' + '}';
    }
}
