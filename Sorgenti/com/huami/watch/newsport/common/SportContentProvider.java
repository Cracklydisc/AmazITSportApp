package com.huami.watch.newsport.common;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.database.MatrixCursor.RowBuilder;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SortInfo;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.SportDataHelper;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import java.util.Calendar;

public class SportContentProvider extends ContentProvider {
    public static final Uri CONTENT_URI_ONE_DAY = Uri.parse("content://com.huami.watch.sport.provider/today_dis");
    public static final Uri CONTENT_URI_SPORT_HEART_COUNT_INFO = Uri.parse("content://com.huami.watch.sport.provider/sport_heart_count");
    public static final Uri CONTENT_URI_SPORT_INFO = Uri.parse("content://com.huami.watch.sport.provider/sport_summary_data_info");
    public static final Uri CONTENT_URI_SPORT_SORT_INFO = Uri.parse("content://com.huami.watch.sport.provider/sport_sort_info");
    public static final Uri CONTENT_URI_SPORT_STATISTICS_INFO = Uri.parse("content://com.huami.watch.sport.provider/sport_statistics");
    public static final Uri CONTENT_URI_TOTAL = Uri.parse("content://com.huami.watch.sport.provider/total_dis");
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1);
    private SportDataHelper mDb = null;

    class C05541 implements Runnable {
        C05541() {
        }

        public void run() {
            SportContentProvider.this.updateTodayDistance();
            SportStatistic sportStatistic = DataManager.getInstance().getSportStatistic(SportContentProvider.this.getContext());
            DataManager.getInstance().setTotalDistance(SportContentProvider.this.getContext(), Float.parseFloat(DataFormatUtils.parseTotalDistanceFormattedRealNumber(((double) sportStatistic.getTotalDistance()) / 1000.0d, false)));
            Debug.m3d("HmSportProvider", "total distance:" + sportStatistic.getTotalDistance());
        }
    }

    static {
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "today_dis", 1);
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "total_dis", 2);
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "sport_summary_data_info", 3);
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "sport_heart_count", 4);
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "sport_statistics", 5);
        URI_MATCHER.addURI("com.huami.watch.sport.provider", "sport_sort_info", 6);
    }

    public boolean onCreate() {
        this.mDb = SportDataHelper.getInstance(getContext());
        updateSportData();
        return true;
    }

    private void updateSportData() {
        Global.getGlobalWorkHandler().post(new C05541());
    }

    private void updateTodayDistance() {
        long currentTime = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTime);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        String startTime = String.valueOf(calendar.getTimeInMillis());
        calendar.add(6, 1);
        String endTime = String.valueOf(calendar.getTimeInMillis());
        float todayDis = 0.0f;
        for (SportSummary summary : SportSummaryDao.getInstance(getContext()).selectAll(getContext(), "track_id BETWEEN ? and ?", new String[]{startTime, endTime}, null, null)) {
            if (!(summary.getCurrentStatus() == 7 || summary.getCurrentStatus() == 1 || summary.getCurrentStatus() == 0 || SportType.isChildSport(summary.getSportType()))) {
                todayDis = NumeriConversionUtils.getAddResult(todayDis, NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber(((double) summary.getDistance()) / 1000.0d, false)));
            }
        }
        Debug.m3d("HmSportProvider", "today distance:" + todayDis);
        DataManager.getInstance().setTodayDistance(getContext(), todayDis);
    }

    @Nullable
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase sqLiteDatabase = this.mDb.getReadableDatabase();
        MatrixCursor matrixCursor;
        switch (URI_MATCHER.match(uri)) {
            case 3:
                return sqLiteDatabase.query("sport_summary", projection, selection, selectionArgs, null, null, sortOrder);
            case 4:
                matrixCursor = new MatrixCursor(new String[]{"count"});
                long count = DatabaseUtils.queryNumEntries(sqLiteDatabase, "heart_rate", selection, selectionArgs);
                Debug.m5i("HmSportProvider", "heart count:" + count);
                matrixCursor.newRow().add(Long.valueOf(count));
                return matrixCursor;
            case 5:
                SportStatistic statistic = DataManager.getInstance().getSportStatistic(getContext());
                matrixCursor = new MatrixCursor(new String[]{"total_dis", "total_count", "total_cal"});
                RowBuilder statisticsBuilder = matrixCursor.newRow();
                statisticsBuilder.add(Integer.valueOf(statistic.getTotalDistance()));
                statisticsBuilder.add(Integer.valueOf(statistic.getTotalCount()));
                statisticsBuilder.add(Integer.valueOf(statistic.getTotalCalorie()));
                return matrixCursor;
            case 6:
                matrixCursor = new MatrixCursor(new String[]{"sport_sort_type", "sport_sort_count", "sport_sort_last_time"});
                for (int sportType : SportLauncherItemAdapter.SPORT_TYPE_ARRAYS) {
                    SortInfo sortInfo = DataManager.getInstance().getSpecificSportNum(sportType);
                    RowBuilder sortBuilder = matrixCursor.newRow();
                    sortBuilder.add(Integer.valueOf(sortInfo.getType()));
                    sortBuilder.add(Integer.valueOf(sortInfo.getCount()));
                    sortBuilder.add(Long.valueOf(sortInfo.getTime()));
                }
                return matrixCursor;
            default:
                return null;
        }
    }

    @Nullable
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
