package com.huami.watch.newsport.common.manager;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.UserInfo;
import java.util.Calendar;

public class UserInfoManager {
    private static final Uri USER_INFO_URI = Uri.parse("content://com.huami.watch.setup.usersettings");
    private static UserInfoManager sInstance = null;
    private UserInfo mUserInfo = null;

    private UserInfoManager() {
    }

    public static UserInfoManager getInstance() {
        synchronized (UserInfoManager.class) {
            if (sInstance == null) {
                sInstance = new UserInfoManager();
            }
        }
        return sInstance;
    }

    public UserInfo getUserInfo(Context context) {
        if (this.mUserInfo == null) {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(USER_INFO_URI, null, null, null, null);
                UserInfo userInfo = new UserInfo();
                if (cursor != null && cursor.moveToFirst()) {
                    userInfo.setGender(cursor.getInt(cursor.getColumnIndex("gender")));
                    userInfo.setYear(cursor.getInt(cursor.getColumnIndex("year")));
                    userInfo.setMonth(cursor.getInt(cursor.getColumnIndex("month")));
                    userInfo.setWeight(cursor.getFloat(cursor.getColumnIndex("weight")));
                    userInfo.setHeight(cursor.getInt(cursor.getColumnIndex("height")));
                }
                this.mUserInfo = userInfo;
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (this.mUserInfo != null) {
            Debug.m5i("HmUserInfoManager", "user info:" + this.mUserInfo.toString());
        }
        return this.mUserInfo;
    }

    public static int getDefaultSafeHeart(Context context) {
        UserInfo info = getInstance().getUserInfo(context);
        if (info == null) {
            Debug.m5i("HmUserInfoManager", "User info is null");
            return 180;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(1) - info.getYear();
        int safeHeart = 220 - year;
        if (safeHeart < 97 || safeHeart > 202) {
            return 180;
        }
        return 220 - year;
    }

    public static int getUserAge(Context context) {
        UserInfo info = getInstance().getUserInfo(context);
        if (info == null) {
            Debug.m5i("HmUserInfoManager", "User info is null");
            return 18;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(1) - info.getYear();
        if (year >= 220) {
            return 18;
        }
        return year;
    }

    public static int getUserAgeByUserInfo(UserInfo info) {
        if (info == null || info.getYear() < 0) {
            Debug.m5i("HmUserInfoManager", "User info is null");
            return 18;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(1) - info.getYear();
        if (year >= 220) {
            return 18;
        }
        return year;
    }
}
