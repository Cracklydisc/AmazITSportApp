package com.huami.watch.newsport.common.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SortInfo;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.StepConfig;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.huangheproduction.model.HuangHeSportInfoOrderManagerWrapper;
import com.huami.watch.newsport.huangheproduction.model.HuangHeSportType;
import com.huami.watch.newsport.utils.DateUtils;
import com.huami.watch.newsport.utils.IntertelRunDataParse;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.ParseUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;

public class DataManager {
    private static final String TAG = DataManager.class.getName();
    private static DataManager sInstance = null;

    public static DataManager getInstance() {
        synchronized (DataManager.class) {
            if (sInstance == null) {
                sInstance = new DataManager();
            }
        }
        return sInstance;
    }

    private DataManager() {
    }

    public void setSportConfig(Context context, BaseConfig config) {
        Debug.m5i(TAG, "set config " + config);
        if (config != null) {
            if (config.getSportType() == 11 && !config.isMeasureHeart()) {
                config.setIsMeasureHeart(true);
            }
            String configJson = WatchSettings.get(context.getContentResolver(), "huami.watch.sport.v2_config_all");
            JSONObject jsonObject;
            if (configJson == null || configJson.isEmpty()) {
                jsonObject = new JSONObject();
                try {
                    jsonObject.put(config.getKey(), new JSONObject(config.toJSON()));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                WatchSettings.put(context.getContentResolver(), "huami.watch.sport.v2_config_all", jsonObject.toString());
                return;
            }
            try {
                jsonObject = new JSONObject(configJson);
                jsonObject.put(config.getKey(), new JSONObject(config.toJSON()));
                WatchSettings.put(context.getContentResolver(), "huami.watch.sport.v2_config_all", jsonObject.toString());
            } catch (JSONException e) {
                Debug.m6w(TAG, "json error exist when parse : " + configJson + ". error : " + e.getLocalizedMessage());
                WatchSettings.put(context.getContentResolver(), "huami.watch.sport.v2_config_all", "");
                Debug.m3d(TAG, "get config : " + config.toString());
            }
        }
    }

    public BaseConfig getSportConfig(Context context, int sportType) {
        if (SportType.isSportTypeValid(sportType)) {
            BaseConfig config = BaseConfig.newSportItemConfig(sportType);
            parseSportConfigObjFromJson(context, "huami.watch.sport.v2_config_all", config);
            return config;
        }
        Debug.m5i(TAG, "getSportItemConfig, invalid sport type:" + sportType);
        return null;
    }

    private void parseSportConfigObjFromJson(Context context, String key, BaseConfig config) {
        String configJson = WatchSettings.get(context.getContentResolver(), key);
        if (configJson != null && config != null) {
            try {
                Debug.m6w(TAG, "parseSportConfigObjFromJson, key: " + config.getKey());
                JSONObject sportJson = new JSONObject(configJson).optJSONObject(config.getKey());
                if (sportJson != null) {
                    config.initObjectFromJSON(sportJson);
                }
            } catch (JSONException e) {
                Debug.m6w(TAG, "json error exist when parse : " + configJson + ". error : " + e.getLocalizedMessage());
            }
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d(TAG, "get config : " + config.toString());
            }
        }
    }

    public SportStatistic getSportStatistic(Context context) {
        String statisticStr = getDefaultPerferences(context).getString("sport_statistic", null);
        SportStatistic statistic = new SportStatistic();
        if (statisticStr != null) {
            try {
                statistic.initObjectFromJSON(new JSONObject(statisticStr));
            } catch (JSONException e) {
                e.printStackTrace();
                Debug.m6w(TAG, "json error exist when parse : " + statisticStr + ". error : " + e.getLocalizedMessage());
            }
        }
        Debug.m3d(TAG, "get sport statistic : " + statistic);
        return statistic;
    }

    public void addStatistic(Context context, SportStatistic statistic) {
        Debug.m3d(TAG, "set statistic : " + statistic);
        if (statistic == null) {
            Debug.m6w(TAG, "statistic is null while update statistic");
        } else {
            updateSportStatistic(context, statistic.getTotalCount(), statistic.getTotalDistance(), statistic.getTotalCalorie());
        }
    }

    private void updateSportStatistic(Context context, int totalCount, int distance, int cal) {
        Debug.m5i(TAG, "updateSportStatistic, totalCount:" + totalCount + ", distance:" + distance + ", cal:" + cal);
        SharedPreferences preferences = getDefaultPerferences(context);
        String statisticJSONStr = preferences.getString("sport_statistic", null);
        SportStatistic statistic = new SportStatistic();
        if (statisticJSONStr != null) {
            try {
                statistic.initObjectFromJSON(new JSONObject(statisticJSONStr));
            } catch (JSONException e) {
                Debug.m6w(TAG, "json error exist when parse : " + statisticJSONStr + ". error : " + e.getLocalizedMessage());
            }
        }
        statistic.setTotalCount(statistic.getTotalCount() + totalCount);
        statistic.setTotalDistance(statistic.getTotalDistance() + distance);
        statistic.setTotalCalorie(statistic.getTotalCalorie() + cal);
        if (statistic.getTotalCount() <= 0) {
            statistic.setTotalCount(0);
            statistic.setTotalDistance(0);
            statistic.setTotalCalorie(0);
        }
        Editor editor = preferences.edit();
        editor.putString("sport_statistic", statistic.toJSON());
        editor.apply();
        setTotalDistance(context, NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseTotalDistanceFormattedRealNumber(NumeriConversionUtils.getDoubleValue(String.valueOf(statistic.getTotalDistance())) / 1000.0d, false)));
    }

    public void deleteSummaryFromStatistic(Context context, SportSummary sportSummary) {
        Debug.m3d(TAG, "delete summary from statistic : " + sportSummary);
        updateSportStatistic(context, -1, -sportSummary.getTrimedDistance(), (int) (-sportSummary.getCalorie()));
        updateTodayDistance(context, sportSummary.getDistance(), sportSummary.getTrackId());
    }

    public void addSummaryToStatistic(Context context, SportSummary sportSummary) {
        Debug.m3d(TAG, "add summary to statistic : " + sportSummary);
        updateSportStatistic(context, 1, sportSummary.getTrimedDistance(), (int) sportSummary.getCalorie());
    }

    public void reModifySummaryToStatAndMileDay(Context context, int reDis, long trackId) {
        updateSportStatistic(context, 0, reDis, 0);
        updateTodayDistance(context, (float) (-reDis), trackId);
    }

    public void updateTodayDistance(Context context, float deleteDistance, long trackId) {
        if (context == null) {
            Debug.m3d(TAG, "update today distance error, context is null");
            return;
        }
        double deleteDis = NumeriConversionUtils.getDoubleValue(String.valueOf(deleteDistance / 1000.0f));
        if (DateUtils.isSameDate(trackId)) {
            float deleteValue;
            float todayDis = 0.0f;
            try {
                todayDis = Secure.getFloat(context.getContentResolver(), "sport_today_distance");
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
                Debug.m3d(TAG, "settings not found");
            }
            Debug.m5i(TAG, "today dis:" + todayDis);
            if (deleteDis < 0.0d) {
                deleteValue = -NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber(-deleteDis, false));
            } else {
                deleteValue = NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber(deleteDis, false));
            }
            todayDis = NumeriConversionUtils.getSubtractResult(todayDis, deleteValue);
            Debug.m5i(TAG, "dele Dis: " + deleteDis + ", after today dis:" + todayDis);
            if (todayDis < 0.0f) {
                todayDis = 0.0f;
            }
            setTodayDistance(context, todayDis);
            return;
        }
        Debug.m5i(TAG, "delete not today sport summary");
    }

    public void setTodayDistance(Context context, float todayDistance) {
        if (context == null) {
            Debug.m3d(TAG, "setTodayDis error, context is null");
            return;
        }
        Debug.m3d(TAG, "setTodayDistance, todayDistance:" + todayDistance);
        Secure.putFloat(context.getContentResolver(), "sport_today_distance", todayDistance);
    }

    public void setTotalDistance(Context context, float totalDistance) {
        if (context == null) {
            Debug.m3d(TAG, "setTotalDistance error, context is null");
            return;
        }
        Debug.m3d(TAG, "setTotalDistance, totalDistance:" + totalDistance);
        Secure.putFloat(context.getContentResolver(), "sport_total_distance", totalDistance);
    }

    private SharedPreferences getDefaultPerferences(Context context) {
        return getPreferences(context, "base_sport_info");
    }

    private SharedPreferences getCachePerferences(Context context) {
        return getPreferences(context, "cache_sport_graph_record");
    }

    private SharedPreferences getFirstBeatConfigSP(Context mContext) {
        return getPreferences(mContext, "first_beat_newest_data");
    }

    private SharedPreferences getPreferences(Context context, String modelName) {
        return context.getApplicationContext().getSharedPreferences(modelName, 0);
    }

    public int[] getSportItemOrder(Context context, int sportType) {
        if (!UnitConvertUtils.isHuangheMode() || System.getInt(context.getContentResolver(), "sport_item_order_version" + sportType, 0) > 0) {
            int[] order = ParseUtils.parseStringToIntArray(System.getString(context.getContentResolver(), "sport_item_order" + sportType));
            Debug.m3d(TAG, "get sport item order_2 " + Arrays.toString(order) + " for sport type " + sportType);
            return order;
        }
        int huangheSportType = HuangHeSportType.transferSportTypeFromZhuFeng2Huanghe(sportType);
        if (System.getInt(context.getContentResolver(), "sport_item_order_version" + huangheSportType, 0) == 1) {
            LogUtil.m9i(true, TAG, "get sport item order, the setting has been covered by new order");
            return null;
        }
        int[] transferOrders = HuangHeSportInfoOrderManagerWrapper.getTransferSportOrder(sportType, ParseUtils.parseStringToIntArray(System.getString(context.getContentResolver(), "sport_item_order" + huangheSportType)));
        Debug.m3d(TAG, "get sport item order_0 " + Arrays.toString(transferOrders) + " for sport type " + sportType + ", " + huangheSportType);
        return transferOrders;
    }

    public boolean isShownBindDeviceTips(Context context) {
        return getDefaultPerferences(context).getBoolean("first_show_bind_devices_tips", true);
    }

    public void saveBindDeviceState(Context context, boolean isShown) {
        Editor editor = getDefaultPerferences(context).edit();
        editor.putBoolean("first_show_bind_devices_tips", isShown);
        editor.apply();
    }

    public void setStepLearningMatrix(Context context, Float[] learningMatrix) {
        if (learningMatrix != null && learningMatrix.length >= 1) {
            Editor editor = getDefaultPerferences(context).edit();
            String matrix = ParseUtils.parseNumberArrayToString(Arrays.asList(learningMatrix), ",");
            Debug.m5i(TAG, "setStepLearningMatrix: " + Arrays.toString(learningMatrix));
            editor.putString("key_sport_learning_matrix", matrix);
            editor.apply();
        }
    }

    public float[] getStepLearningMatrix(Context context) {
        return ParseUtils.parseStringToFloatArray(getDefaultPerferences(context).getString("key_sport_learning_matrix", ""));
    }

    public void setStepLength(Context context, Integer[] learningMatrix) {
        if (learningMatrix != null && learningMatrix.length >= 1) {
            Editor editor = getDefaultPerferences(context).edit();
            String matrix = ParseUtils.parseNumberArrayToString(Arrays.asList(learningMatrix), ",");
            Debug.m5i(TAG, "setStepLength: " + Arrays.toString(learningMatrix));
            editor.putString("key_sport_step_length", matrix);
            editor.apply();
        }
    }

    public int[] getStepLength(Context context) {
        int[] matrixs = ParseUtils.parseStringToIntArray(getDefaultPerferences(context).getString("key_sport_step_length", ""));
        Debug.m5i(TAG, "getStepLength: " + Arrays.toString(matrixs));
        return matrixs;
    }

    public void updateStepConfig(Context context, StepConfig stepConfig) {
        Editor editor = getDefaultPerferences(context).edit();
        Debug.m5i(TAG, "updateStepConfig, setStepLength: " + stepConfig.getStepArray());
        Debug.m5i(TAG, "updateStepConfig, setStepLearningMatrix: " + stepConfig.getLearnArray());
        editor.putString("key_sport_step_length", stepConfig.getStepArray());
        editor.putString("key_sport_learning_matrix", stepConfig.getLearnArray());
        editor.apply();
    }

    public StepConfig getStepConfig(Context context) {
        SharedPreferences sp = getDefaultPerferences(context);
        return new StepConfig(sp.getString("key_sport_learning_matrix", ""), sp.getString("key_sport_step_length", ""));
    }

    public boolean isFirstDownloadHistory(Context context) {
        return getDefaultPerferences(context).getBoolean("first_download_history", true);
    }

    public void setIsFirstDownloadHistory(Context context, boolean isFirst) {
        Editor editor = getDefaultPerferences(context).edit();
        editor.putBoolean("first_download_history", isFirst);
        editor.commit();
    }

    public static boolean isDeviceExperience(Context context) {
        boolean isDeviceExperience = false;
        try {
            isDeviceExperience = Secure.getInt(context.getContentResolver(), "device_experience") == 1;
        } catch (Exception e) {
            Debug.m3d(TAG, "Device is Experience : Setting Not Found, " + e.getLocalizedMessage());
        }
        Debug.m3d(TAG, "Device is Experience : " + isDeviceExperience);
        return isDeviceExperience;
    }

    public void saveSportRecord(String key) {
        Editor editor = getCachePerferences(Global.getApplicationContext()).edit();
        editor.putBoolean(key, true);
        editor.commit();
    }

    public boolean isSportRecordAvaiable(String key) {
        return getCachePerferences(Global.getApplicationContext()).getBoolean(key, false);
    }

    public void recordDownloadSportRecord(int num) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putInt("down_load_sport_record_count", num);
        editor.commit();
    }

    public int getDownloadSportRecord() {
        return getDefaultPerferences(Global.getApplicationContext()).getInt("down_load_sport_record_count", 0);
    }

    public void recordCompleteDownloadHistory() {
        int num = getDownloadNums() + 1;
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putInt("complete_down_load_sport_record_count", num);
        editor.commit();
    }

    public int getDownloadNums() {
        return getDefaultPerferences(Global.getApplicationContext()).getInt("complete_down_load_sport_record_count", 0);
    }

    public void updateSpecificSportNum(int sportType, long timeStamps) {
        SharedPreferences sp = getPreferences(Global.getApplicationContext(), "specific_sport");
        int count = sp.getInt("key_sport_" + sportType, 0) + 1;
        Editor editor = sp.edit();
        editor.putInt("key_sport_" + sportType, count);
        editor.putLong("key_specefic_sport_time_stamps_" + sportType, timeStamps);
        editor.apply();
    }

    public SortInfo getSpecificSportNum(int sportType) {
        SharedPreferences sp = getPreferences(Global.getApplicationContext(), "specific_sport");
        int count = sp.getInt("key_sport_" + sportType, 0);
        long time = sp.getLong("key_specefic_sport_time_stamps_" + sportType, 0);
        SortInfo sortInfo = new SortInfo();
        sortInfo.setType(sportType);
        sortInfo.setCount(count);
        sortInfo.setTime(time);
        return sortInfo;
    }

    public void updateSportRecoveryTime(long startTime, long recoveryTime) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putLong("key_recovery_start_time", startTime);
        editor.putLong("key_recovery_time", recoveryTime);
        editor.apply();
    }

    public boolean isFirstOpenSwim() {
        return getDefaultPerferences(Global.getApplicationContext()).getBoolean("pref_key_first_open_swim", true);
    }

    public boolean isFirstTennicClapHand() {
        return getDefaultPerferences(Global.getApplicationContext()).getBoolean("pref_key_tennic_clap_hand", true);
    }

    public void setIsFirstTennicClapHand(boolean isFirst) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("pref_key_tennic_clap_hand", isFirst);
        editor.apply();
    }

    public long getLastSportThaInfoUploadTime() {
        return getDefaultPerferences(Global.getApplicationContext()).getLong("last_upload_sport_tha_time_stamp", 0);
    }

    public void setLastSportThaInfoUploadTime(long time) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putLong("last_upload_sport_tha_time_stamp", time);
        editor.apply();
    }

    public long getLastVo2maxUploadTime() {
        return getDefaultPerferences(Global.getApplicationContext()).getLong("last_upload_vo2max_time_stamp", 0);
    }

    public void setLastVo2maxUploadTime(long time) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putLong("last_upload_vo2max_time_stamp", time);
        editor.apply();
    }

    public void setIsFirstOpenSwim(boolean isFirst) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("pref_key_first_open_swim", isFirst);
        editor.apply();
    }

    public int getLastSwimLength() {
        return getDefaultPerferences(Global.getApplicationContext()).getInt("pref_key_last_swim_length", 0);
    }

    public void setLastSwimLength(int length) {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putInt("pref_key_last_swim_length", length);
        editor.apply();
    }

    public void setFirstBeatConfig(Context context, FirstBeatConfig config) {
        saveFirstBeatConfigToSP(context, config);
    }

    public FirstBeatConfig getFirstBeatConfig(Context context) {
        return getFirstBeatConfigFromSP(context);
    }

    private void saveFirstBeatConfigToSP(Context mContext, FirstBeatConfig firstBeatConfig) {
        if (firstBeatConfig != null) {
            Log.i(TAG, " saveFirstConfigToSp:" + firstBeatConfig.toString());
            Editor editor = getFirstBeatConfigSP(Global.getApplicationContext()).edit();
            editor.putString("pref_key_first_beat_config", firstBeatConfig.toJSON());
            editor.apply();
        }
    }

    private FirstBeatConfig getFirstBeatConfigFromSP(Context mContext) {
        String result = getFirstBeatConfigSP(Global.getApplicationContext()).getString("pref_key_first_beat_config", null);
        FirstBeatConfig firstBeatConfig = FirstBeatConfig.newFirstBeatConfig();
        if (!(result == null || TextUtils.isEmpty(result))) {
            try {
                firstBeatConfig.initObjectFromJSON(new JSONObject(result));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.i(TAG, " getFirstBeatConfigFromSP:" + firstBeatConfig.toString());
        return firstBeatConfig;
    }

    public int getLastSportIndex() {
        int index = getDefaultPerferences(Global.getApplicationContext()).getInt("last_sport_index", 1);
        if (index > 100) {
            return 1;
        }
        return index;
    }

    public void setLastSportIndex(int index) {
        if (index > 100) {
            index = 1;
        }
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putInt("last_sport_index", index);
        editor.apply();
    }

    public boolean getIsNotFirstDayEndReceiver() {
        return getDefaultPerferences(Global.getApplicationContext()).getBoolean("is_first_time_day_end", false);
    }

    public void setIsNotFirstDayEndReceiver() {
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("is_first_time_day_end", true);
        editor.apply();
    }

    public void setIsInGPSearchingUI(boolean isInGPS) {
        LogUtil.m9i(true, TAG, "setIsInGPSearchingUI, " + isInGPS);
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("in_gps_ui", isInGPS);
        editor.apply();
    }

    public boolean isInGPSearchingUI() {
        boolean isInGPS = getDefaultPerferences(Global.getApplicationContext()).getBoolean("in_gps_ui", false);
        LogUtil.m9i(true, TAG, "isInGPSearchingUI, " + isInGPS);
        return isInGPS;
    }

    public boolean isFirstShowIntervalTrainTips() {
        boolean isShowTips = getDefaultPerferences(Global.getApplicationContext()).getBoolean("first_show_interval_train_tips", true);
        LogUtil.m9i(true, TAG, "isFirstShowIntervalTrainTips, " + isShowTips);
        return isShowTips;
    }

    public void setIsFirstShowIntervalTrainTips(boolean isShowTips) {
        LogUtil.m9i(true, TAG, "setIsFirstShowIntervalTrainTips, " + isShowTips);
        Editor editor = getDefaultPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("first_show_interval_train_tips", isShowTips);
        editor.apply();
    }

    private SharedPreferences getUploadPerferences(Context context) {
        return getPreferences(context, "upload_session");
    }

    public void setUploadSportSessionId(long trackId, String sessionId) {
        LogUtil.m9i(true, TAG, "setUploadSportSessionId, trackId:" + trackId + ", sessionId:" + sessionId);
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.putString("session_" + trackId, sessionId);
        editor.apply();
    }

    public String getUploadSportSessionId(long trackId) {
        LogUtil.m9i(true, TAG, "getUploadSportSessionId, trackId:" + trackId);
        return getUploadPerferences(Global.getApplicationContext()).getString("session_" + trackId, "");
    }

    public void setIsUploadDataTempCached(long trackId, boolean isCached) {
        setIsUploadDataTempCached(String.valueOf(trackId), isCached);
    }

    public void setIsUploadDataTempCached(String trackId, boolean isCached) {
        LogUtil.m9i(true, TAG, "setIsUploadDataTempCached, trackId:" + trackId + ", isCached:" + isCached);
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("temp_cache_" + trackId, isCached);
        editor.apply();
    }

    public boolean isUploadSportTempCached(long trackId) {
        LogUtil.m9i(true, TAG, "isUploadSportCached, trackId:" + trackId);
        return getUploadPerferences(Global.getApplicationContext()).getBoolean("temp_cache_" + trackId, false);
    }

    public void setIsUploadDataCached(long trackId, boolean isCached) {
        setIsUploadDataCached(String.valueOf(trackId), isCached);
    }

    public void setIsUploadDataCached(String trackId, boolean isCached) {
        LogUtil.m9i(true, TAG, "setIsUploadDataCached, trackId:" + trackId + ", isCached:" + isCached);
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("cache_" + trackId, isCached);
        editor.apply();
    }

    public void clearUploadCached(long trackId) {
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.remove("cache_" + trackId);
        editor.remove("temp_cache_" + trackId);
        editor.remove("cache_detail_" + trackId);
        editor.apply();
    }

    public void clearUploadSp() {
        LogUtil.m9i(true, TAG, "clearUploadSp");
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.clear();
        editor.apply();
    }

    public boolean isUploadSportCached(long trackId) {
        LogUtil.m9i(true, TAG, "isUploadSportCached, trackId:" + trackId);
        return getUploadPerferences(Global.getApplicationContext()).getBoolean("cache_" + trackId, false);
    }

    public void setIsUploadDetailDataCached(long trackId, boolean isCached) {
        setIsUploadDetailDataCached(String.valueOf(trackId), isCached);
    }

    public void setIsUploadDetailDataCached(String trackId, boolean isCached) {
        LogUtil.m9i(true, TAG, "setIsUploadDetailDataCached, trackId:" + trackId + ", isCached:" + isCached);
        Editor editor = getUploadPerferences(Global.getApplicationContext()).edit();
        editor.putBoolean("cache_detail_" + trackId, isCached);
        editor.apply();
    }

    public boolean isUploadDetailSportCached(long trackId) {
        LogUtil.m9i(true, TAG, "isUploadDetailSportCached, trackId:" + trackId);
        return getUploadPerferences(Global.getApplicationContext()).getBoolean("cache_detail_" + trackId, false);
    }

    public void getIntervalRunData() {
        String configJson = WatchSettings.get(Global.getApplicationContext().getContentResolver(), "huami.watch.sport.intervalrun");
        Log.i(TAG, "getIntervalRunData configJson:" + configJson);
        if (configJson != null) {
            IntertelRunDataParse.getInstance().productTrainProgramFile(Global.getApplicationContext(), configJson);
        } else {
            Log.i(TAG, "getIntervalRunData Result:" + configJson);
        }
    }

    public boolean isNeedClearSportDataCache(int version) {
        if (getCachePerferences(Global.getApplicationContext()).getInt("cache_version", 1) != version) {
            return true;
        }
        return false;
    }

    public void setSportDataCacheVersion(int version) {
        Editor editor = getCachePerferences(Global.getApplicationContext()).edit();
        editor.putInt("cache_version", version);
        editor.apply();
    }
}
