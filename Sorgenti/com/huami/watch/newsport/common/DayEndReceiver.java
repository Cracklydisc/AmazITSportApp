package com.huami.watch.newsport.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Secure;
import android.util.Log;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;

public class DayEndReceiver extends BroadcastReceiver {
    private static final String TAG = DayEndReceiver.class.getName();

    class C05521 implements Runnable {

        class C05511 implements OnKlvpDataListener {
            C05511() {
            }

            public void onHealthDataReady(int i, float v) {
            }

            public void onSportDataReady(SportStatistics sportData) {
                if (sportData != null && sportData.mSportEteInfo != null) {
                    StringBuffer sb = new StringBuffer();
                    sb.append("getVo2MaxTrend:" + sportData.mSportThaInfo.getVo2MaxTrend() + ",");
                    sb.append("getTrainingLoadTrend:" + sportData.mSportThaInfo.getTrainingLoadTrend() + ",");
                    sb.append("getFitnessClass:" + sportData.mSportThaInfo.getFitnessClass() + ",");
                    sb.append("getFitnessLevelIncrease:" + sportData.mSportThaInfo.getFitnessLevelIncrease() + ",");
                    sb.append("getWtlStatus:" + sportData.mSportThaInfo.getWtlStatus() + ",");
                    sb.append("getWtlSum:" + sportData.mSportThaInfo.getWtlSum() + ",");
                    sb.append("getWtlSumOptimalMax:" + sportData.mSportThaInfo.getWtlSumOptimalMax() + ",");
                    sb.append("getWtlSumOptimalMin:" + sportData.mSportThaInfo.getWtlSumOptimalMin() + ",");
                    sb.append("getWtlSumOverreaching:" + sportData.mSportThaInfo.getWtlSumOverreaching());
                    Log.i(DayEndReceiver.TAG, " mSportThaInfo " + sb.toString());
                    SportThaInfo sportThaInfo = new SportThaInfo();
                    sportThaInfo.setCurrnetDayTrainLoad(sportData.mSportEteInfo.getETEtrainingLoadPeak());
                    sportThaInfo.setVo2MaxTread(sportData.mSportThaInfo.getVo2MaxTrend());
                    sportThaInfo.setTrainingLoadTrend(sportData.mSportThaInfo.getTrainingLoadTrend());
                    sportThaInfo.setFitnessClass(sportData.mSportThaInfo.getFitnessClass());
                    sportThaInfo.setFitnessLevelIncrease(sportData.mSportThaInfo.getFitnessLevelIncrease());
                    sportThaInfo.setWtlStatus(sportData.mSportThaInfo.getWtlStatus());
                    sportThaInfo.setWtlSum(sportData.mSportThaInfo.getWtlSum());
                    sportThaInfo.setWtlSumOptimalMax(sportData.mSportThaInfo.getWtlSumOptimalMax());
                    sportThaInfo.setWtlSumOptimalMin(sportData.mSportThaInfo.getWtlSumOptimalMin());
                    sportThaInfo.setWtlSumOverreaching(sportData.mSportThaInfo.getWtlSumOverreaching());
                    sportThaInfo.setDayId(FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis()));
                    sportThaInfo.setUpdateTime(System.currentTimeMillis());
                    sportThaInfo.setDateStr(FirstBeatDataUtils.getDayTimeFormat(System.currentTimeMillis()));
                    Log.i(DayEndReceiver.TAG, "sportThaInfo dayEnd:" + sportThaInfo.toString());
                    Log.i(DayEndReceiver.TAG, "insertOrUpdateThaInfo : " + new SyncDatabaseManager(SportThaInfoDao.getmInstance(Global.getApplicationContext())).add(Global.getApplicationContext(), sportThaInfo));
                } else if (sportData == null) {
                    Log.i(DayEndReceiver.TAG, " updateTrainLoadData sportData is null ");
                } else if (sportData.mSportEteInfo == null) {
                    Log.i(DayEndReceiver.TAG, " updateTrainLoadData sportData.mSportEteInfo is null ");
                }
            }
        }

        C05521() {
        }

        public void run() {
            boolean isNotFirstStatus = DataManager.getInstance().getIsNotFirstDayEndReceiver();
            Log.i(DayEndReceiver.TAG, " isNotFirstStatus:" + isNotFirstStatus);
            if (isNotFirstStatus) {
                HmSensorHubConfigManager.getHmSensorHubConfigManager(Global.getApplicationContext()).requestRealtimeData(2, new C05511());
            } else {
                DataManager.getInstance().setIsNotFirstDayEndReceiver();
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String wakeupSource = intent.getStringExtra("WAKEUP_SOURCE");
        Log.i(TAG, "receive date change action, wakup source: " + wakeupSource);
        if ("sensorhub.reach.day.end".equals(wakeupSource)) {
            setTodayDistance(context, 0.0f);
            updateTrainLoadData();
        }
    }

    public void setTodayDistance(Context context, float todayDistance) {
        Secure.putFloat(context.getContentResolver(), "sport_today_distance", todayDistance);
    }

    public void updateTrainLoadData() {
        Log.i(TAG, " updateTrainLoadData ");
        Global.getGlobalWorkHandler().post(new C05521());
    }
}
