package com.huami.watch.newsport.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.huami.watch.newsport.syncservice.UploadUnsyncDataService;

public class BootCompleteReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("HmSportBootComplete", "receive action:" + action);
        if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            UploadUnsyncDataService.startActionUploadUnsyncDataWithoutFinishBroadcast(context);
        }
    }
}
