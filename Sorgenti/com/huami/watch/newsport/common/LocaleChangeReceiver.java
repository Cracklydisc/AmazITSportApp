package com.huami.watch.newsport.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.utils.LogUtil;

public class LocaleChangeReceiver extends BroadcastReceiver {
    private static final String TAG = LocaleChangeReceiver.class.getName();

    class C05531 implements Runnable {
        C05531() {
        }

        public void run() {
            RecordGraphManager.getInstance(Global.getApplicationContext()).clearCache();
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.LOCALE_CHANGED")) {
            LogUtil.m9i(true, TAG, "LocaleChangeReceiver, language changed");
            Global.getGlobalWorkHandler().post(new C05531());
        }
    }
}
