package com.huami.watch.newsport.stepmodel;

import android.content.Context;
import com.huami.watch.common.log.Debug;

public class StepModelManager {
    private static final String TAG = StepModelManager.class.getName();
    private static StepModelManager sInstace = null;
    private NativeDataProvider mProvider = null;

    public static StepModelManager getInstance(Context context) {
        synchronized (StepModelManager.class) {
            if (sInstace == null) {
                sInstace = new StepModelManager(context);
            }
        }
        return sInstace;
    }

    private StepModelManager(Context context) {
        this.mProvider = new NativeDataProvider(context);
    }

    public void init(int sportType, int gender, int age, int height, float weight) {
        if (this.mProvider == null) {
            Debug.m5i(TAG, "registerCallbackIndoorLearningArg err, the provider is null");
        } else {
            this.mProvider.init(sportType, gender, age, height, weight);
        }
    }

    public void registerCallbackIndoorLearningArg(IndoorLearningArgCallback callback) {
        Debug.m5i(TAG, "registerCallbackIndoorLearningArg");
        if (this.mProvider == null) {
            Debug.m5i(TAG, "registerCallbackIndoorLearningArg err, the provider is null");
        } else {
            this.mProvider.registerCallbackIndoorLearningArg(callback);
        }
    }

    public void unRegisterCallbackIndoorLearningArg(IndoorLearningArgCallback callback) {
        Debug.m5i(TAG, "unRegisterCallbackIndoorLearningArg");
        if (this.mProvider == null) {
            Debug.m5i(TAG, "unRegisterCallbackIndoorLearningArg err, the provider is null");
        } else {
            this.mProvider.unRegisterCallbackIndoorLearningArg(callback);
        }
    }

    public void receiveIndoorResultToLearn(long[] millisecond, short[] stepfreq, int size, float[] learningMatrix, int matrixSize, int userInputDis) {
        Debug.m5i(TAG, "receiveIndoorResultToLearn");
        if (this.mProvider == null) {
            Debug.m5i(TAG, "receiveIndoorResultToLearn err, the provider is null");
        } else {
            this.mProvider.receiveIndoorResultToLearn(millisecond, stepfreq, size, learningMatrix, matrixSize, userInputDis);
        }
    }

    public void destroy() {
        Debug.m5i(TAG, "destroy");
        if (this.mProvider == null) {
            Debug.m5i(TAG, "destroy err, the provider is null");
            return;
        }
        this.mProvider.destroy();
        sInstace = null;
    }
}
