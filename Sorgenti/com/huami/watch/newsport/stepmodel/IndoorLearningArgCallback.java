package com.huami.watch.newsport.stepmodel;

public interface IndoorLearningArgCallback {
    void onLearningResult(float[] fArr, int i, int[] iArr, int i2);
}
