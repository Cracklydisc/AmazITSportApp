package com.huami.watch.newsport.stepmodel;

import android.content.Context;
import android.util.Log;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import java.lang.ref.WeakReference;

public class NativeDataProvider {
    static final String TAG = "NDP";
    int mNativeContext = 0;

    native int native_finalize(int i);

    native int native_init(Object obj, int i, int i2, int i3, int i4, float f);

    native int native_receiveIndoorResultToLearn(long[] jArr, short[] sArr, int i, float[] fArr, int i2, int i3, int i4);

    native int native_receiveLearningStepLMatrix(int[] iArr, int i, int i2);

    native int native_register_callback_indoorLearningArg(IndoorLearningArgCallback indoorLearningArgCallback, int i);

    native int native_unregister_callback_indoorLearningArg(IndoorLearningArgCallback indoorLearningArgCallback, int i);

    static {
        Log.i(TAG, "loadLibrary");
        System.loadLibrary("everrest-run");
    }

    public NativeDataProvider(Context context) {
        UserInfo info = UserInfoManager.getInstance().getUserInfo(context.getApplicationContext());
        init(7, info.getGender(), UserInfoManager.getUserAge(context.getApplicationContext()), info.getHeight(), info.getWeight());
    }

    public void destroy() {
        native_finalize(this.mNativeContext);
        Log.i(TAG, "destroy mNativeContext:" + this.mNativeContext);
    }

    public void init(int sportType, int gender, int age, int height, float weight) {
        this.mNativeContext = native_init(new WeakReference(this), sportType, gender, age, height, weight);
        if (this.mNativeContext == 0) {
            throw new IllegalStateException("native ptr is 0");
        }
        Log.i(TAG, "init mNativeContext:" + this.mNativeContext);
    }

    public void registerCallbackIndoorLearningArg(IndoorLearningArgCallback callback) {
        native_register_callback_indoorLearningArg(callback, this.mNativeContext);
        Log.i(TAG, "registerCallbackIndoorLearningArg" + this.mNativeContext);
    }

    public void unRegisterCallbackIndoorLearningArg(IndoorLearningArgCallback callback) {
        native_unregister_callback_indoorLearningArg(callback, this.mNativeContext);
        Log.i(TAG, "registerCallbackIndoorLearningArg" + this.mNativeContext);
    }

    public void receiveIndoorResultToLearn(long[] millisecond, short[] stepfreq, int size, float[] learningMatrix, int matrixSize, int userInputDis) {
        native_receiveIndoorResultToLearn(millisecond, stepfreq, size, learningMatrix, matrixSize, userInputDis, this.mNativeContext);
    }

    public void receiveLearningStepLMatrix(int[] stepLArray, int arraySize) {
        native_receiveLearningStepLMatrix(stepLArray, arraySize, this.mNativeContext);
    }
}
