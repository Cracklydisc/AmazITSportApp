package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class TargetDistanceReminder implements IReminder {
    private static final long[] VIBRATE_PATTERN = new long[]{0, 800, 200, 500, 200, 500};
    private Context mContext = null;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private int mTargetKilometer = -1;
    private TextView mTitleView = null;
    private Vibrator mVibrator = null;

    class C06101 implements IDialogKeyEventListener {
        C06101() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public TargetDistanceReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView;
        int imgRes;
        if (!ReminderManager.getInstance().isTrainningMode() || ReminderManager.getInstance().getSportType() == 9 || ReminderManager.getInstance().getSportType() == 14) {
            remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_distance_target, null);
            imgRes = C0532R.drawable.sport_alert_target_trainning;
        } else {
            remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_distance_target_training, null);
            imgRes = C0532R.drawable.training_notify_completed_trainning;
        }
        ((ImageView) remindRootView.findViewById(C0532R.id.remind_icon)).setImageResource(imgRes);
        this.mTitleView = (TextView) remindRootView.findViewById(C0532R.id.remind_type_text);
        remindRootView.findViewById(C0532R.id.remind_type_text_tint).setVisibility(0);
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06101());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        String titleText;
        PointUtils.recordEventProperty("0058", this.mSportType);
        this.mTargetKilometer = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).getTargetDistance() / 1000;
        initPromptViewIfNeeded();
        if (!ReminderManager.getInstance().isTrainningMode() || ReminderManager.getInstance().getSportType() == 9) {
            if (this.mSportType == 14) {
                titleText = this.mContext.getString(C0532R.string.swim_trips_complete, new Object[]{Integer.valueOf(config.getTargetTrips())});
            } else if (UnitConvertUtils.isImperial()) {
                titleText = String.format(this.mContext.getString(C0532R.string.km_imperial_remind_target_distance_title_format), new Object[]{Integer.valueOf(this.mTargetKilometer)});
            } else {
                titleText = String.format(this.mContext.getString(C0532R.string.km_metric_remind_target_distance_title_format), new Object[]{Integer.valueOf(this.mTargetKilometer)});
            }
        } else if (this.mSportType == 14) {
            titleText = this.mContext.getString(C0532R.string.swim_trips_complete, new Object[]{Integer.valueOf(config.getTargetTrips())});
        } else {
            double targetKm = UnitConvertUtils.convertDistanceToKm((double) TrainingInfoManager.getInstance(this.mContext).getTrainingInfo().getDistance());
            if (UnitConvertUtils.isImperial()) {
                titleText = String.format(this.mContext.getString(C0532R.string.km_imperial_remind_target_distance_training_title_format), new Object[]{String.valueOf(NumeriConversionUtils.getDoubleValue(String.valueOf(targetKm), 1))});
            } else {
                titleText = String.format(this.mContext.getString(C0532R.string.km_metric_remind_target_distance_training_title_format), new Object[]{String.valueOf(NumeriConversionUtils.getDoubleValue(String.valueOf(targetKm), 1))});
            }
        }
        this.mTitleView.setText(titleText);
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(2, true, new Object[0]);
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
