package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.ui.view.WearNotTightReminderView;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class WearLooseReminder implements IReminder {
    private static final long[] TIME_SPAN_VIBRATE = new long[]{0, 500, 200, 500};
    private Context mContext = null;
    PromptWindow mPromptWindow;
    private int mSportType = -1;
    private Vibrator mVibrator = null;

    class C06121 implements IDialogKeyEventListener {
        C06121() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public WearLooseReminder(Context context, int sportType) {
        this.mContext = context;
        this.mSportType = sportType;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        this.mPromptWindow = new PromptWindow(this.mContext);
        this.mPromptWindow.setAutoDismissDelay(8000);
        ViewGroup remindRootView = new WearNotTightReminderView(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.showWithVibrator(TIME_SPAN_VIBRATE, -1, VibratorUtil.getMostStrongAttributes());
        this.mPromptWindow.setDialogKeyListener(new C06121());
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
