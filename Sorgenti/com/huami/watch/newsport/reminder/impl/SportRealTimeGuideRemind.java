package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.RealTimeGuidanceModel;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.db.RealTimeGuideDBManager;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.ResourceUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class SportRealTimeGuideRemind implements IReminder {
    private static final String TAG = SportRealTimeGuideRemind.class.getSimpleName();
    private static final long[] VIBRATE_PATTERN = new long[]{0, 800};
    private int mBgType = 0;
    private BaseConfig mConfig = null;
    private Context mContext;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private Vibrator mVibrator;
    private RealTimeGuidanceModel model;
    private int paramOne;
    private int paramSecond;
    private int phraseNumber;
    private RealTimeGuideDBManager realTimeGuideDBManager;
    private SQLiteDatabase sqLiteDatabase;

    class C06081 implements IDialogKeyEventListener {
        C06081() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public SportRealTimeGuideRemind(Context context, int sportType) {
        LogUtils.print(TAG, "SportRealTimeGuideRemind");
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
        this.realTimeGuideDBManager = new RealTimeGuideDBManager(context.getApplicationContext());
        this.mConfig = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType);
        this.mBgType = BackgroundUtils.getCustomBgType(context);
    }

    private boolean initPromptViewIfNeeded() {
        LogUtils.print(TAG, "initPromptViewIfNeeded");
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.remind_sport_real_time_guide, null);
        HmImageView icon = (HmImageView) remindRootView.findViewById(C0532R.id.remind_icon);
        HmTextView realTimeGuideTitleView = (HmTextView) remindRootView.findViewById(C0532R.id.real_time_guide_title);
        HmTextView realTimeGuideDescView = (HmTextView) remindRootView.findViewById(C0532R.id.real_time_guide_desc);
        if (this.mBgType == 1) {
            remindRootView.findViewById(C0532R.id.real_guide_container).setBackgroundResource(C0532R.drawable.bg_white_custom);
            realTimeGuideTitleView.setTextColor(-16777216);
            realTimeGuideDescView.setTextColor(-16777216);
        }
        this.sqLiteDatabase = this.realTimeGuideDBManager.initDBManager(this.mContext.getPackageName());
        this.model = this.realTimeGuideDBManager.selectItemByPhraseNumber(this.sqLiteDatabase, this.phraseNumber);
        if (this.model == null) {
            return false;
        }
        Log.i(TAG, " realTimeGuideModel:" + this.model.toString());
        if (this.model.getGuideTitle() != null) {
            if (this.mBgType == 1) {
                icon.setBackground(this.mContext.getResources().getDrawable(ResourceUtils.getDrawableId(this.mContext, "bg_white_icon_" + this.model.getGuideTitle())));
            } else {
                icon.setBackground(this.mContext.getResources().getDrawable(ResourceUtils.getDrawableId(this.mContext, "icon_" + this.model.getGuideTitle())));
            }
        }
        if (this.model.getGuideTitle() != null) {
            String title = this.mContext.getResources().getString(ResourceUtils.getStringId(this.mContext, this.model.getGuideTitle()));
            realTimeGuideTitleView.setText(this.mContext.getResources().getString(ResourceUtils.getStringId(this.mContext, "real_time_guide_title_" + this.phraseNumber)));
            if (this.model.getGuideTitle().equalsIgnoreCase("real_time_keep_speed")) {
                PointUtils.recordEventProperty("0071", this.mSportType);
            } else if (this.model.getGuideTitle().equalsIgnoreCase("real_time_run_faster")) {
                PointUtils.recordEventProperty("0072", this.mSportType);
            } else if (this.model.getGuideTitle().equalsIgnoreCase("real_time_run_slower")) {
                PointUtils.recordEventProperty("0073", this.mSportType);
            } else if (this.model.getGuideTitle().equalsIgnoreCase("real_time_target_finish")) {
                PointUtils.recordEventProperty("0074", this.mSportType);
            } else if (this.model.getGuideTitle().equalsIgnoreCase("real_time_every_good")) {
                PointUtils.recordEventProperty("0075", this.mSportType);
            }
        }
        if (this.model.getGuideDesc() != null) {
            realTimeGuideDescView.setVisibility(0);
            realTimeGuideDescView.setText(String.format(this.mContext.getResources().getString(ResourceUtils.getStringId(this.mContext, this.model.getGuideDesc())), new Object[]{String.valueOf(this.paramOne), String.valueOf(this.paramSecond)}));
        } else {
            realTimeGuideDescView.setVisibility(8);
        }
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06081());
        return true;
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        LogUtils.print(TAG, "remind");
        if (params == null) {
            Debug.m6w(TAG, "params is null or empty");
            return;
        }
        if (sportStatus == null) {
            Debug.m5i(TAG, "sportStatus is null");
        } else {
            this.phraseNumber = ((Integer) params[0]).intValue();
            decodeVariableValue(((Integer) params[1]).intValue());
        }
        Log.i(TAG, "realTimeGuideValue:" + this.phraseNumber);
        if (initPromptViewIfNeeded()) {
            if (this.mConfig.isRemindTE()) {
                PointUtils.recordEventProperty("0069", this.mSportType);
            } else {
                PointUtils.recordEventProperty("0070", this.mSportType);
            }
            this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
            PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(15, true, this.model.getGuideTitle(), Integer.valueOf(this.phraseNumber), Integer.valueOf(this.paramOne), Integer.valueOf(this.paramSecond));
            return;
        }
        Log.i(TAG, "realTimeGuideValue, failed");
    }

    private void decodeVariableValue(int value) {
        byte[] number = intToBytes4(value);
        byte[] afterNumber = new byte[]{number[0], number[1]};
        this.paramOne = byte2Toint(new byte[]{number[2], number[3]});
        this.paramSecond = byte2Toint(afterNumber);
        Log.i(TAG, "value:" + value + " ,paramOne:" + this.paramOne + ",paramSecond:" + this.paramSecond);
    }

    public static byte[] intToBytes4(int value) {
        return new byte[]{(byte) ((value >> 24) & 255), (byte) ((value >> 16) & 255), (byte) ((value >> 8) & 255), (byte) (value & 255)};
    }

    public static int byte2Toint(byte[] ary) {
        return ((ary[0] << 8) & 65280) | (ary[1] & 255);
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
