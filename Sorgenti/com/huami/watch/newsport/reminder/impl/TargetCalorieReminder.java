package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class TargetCalorieReminder implements IReminder {
    private static final String TAG = TargetCalorieReminder.class.getName();
    private static final long[] VIBRATE_PATTERN = new long[]{0, 200, 200, 200, 200, 500};
    private int mCalorie = 0;
    private Context mContext;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private HmTextView mTitleView = null;
    private Vibrator mVibrator;

    class C06091 implements IDialogKeyEventListener {
        C06091() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public TargetCalorieReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_calorie, null);
        this.mTitleView = (HmTextView) remindRootView.findViewById(C0532R.id.alert_value);
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        PointUtils.recordEventProperty("0059", this.mSportType);
        Debug.m5i(TAG, "remind");
        initPromptViewIfNeeded();
        this.mCalorie = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).getTargetCalorie();
        this.mTitleView.setText("" + this.mCalorie);
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        this.mPromptWindow.setDialogKeyListener(new C06091());
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
