package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class SafeHeartRateReminder implements IReminder {
    private static final long[] VIBRATE_PATTERN = new long[]{0, 500, 200, 500, 200, 500, 200, 500};
    private int f18i = 0;
    private Context mContext = null;
    private int mHeartRate;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private Vibrator mVibrator = null;

    class C06071 implements IDialogKeyEventListener {
        C06071() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public SafeHeartRateReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_templete, null);
        ((ImageView) remindRootView.findViewById(C0532R.id.remind_icon)).setImageResource(C0532R.drawable.sport_notify_heart_remind);
        ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.remind_heart_rate_too_high));
        ((TextView) remindRootView.findViewById(C0532R.id.alert_unit)).setText(this.mContext.getString(C0532R.string.remind_heart_rate_value_after));
        ((TextView) remindRootView.findViewById(C0532R.id.alert_value)).setText(String.valueOf(this.mHeartRate));
        remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_heart_bg));
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06071());
    }

    public final void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        if (params == null || params.length == 0) {
            Debug.m6w("SafeHeartRateReminder", "params is null or empty");
            return;
        }
        if (sportStatus == null) {
            Debug.m5i("SafeHeartRateReminder", "sportStatus is null");
        } else {
            this.mHeartRate = ((Integer) params[0]).intValue();
        }
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(5, true, Integer.valueOf(this.mHeartRate));
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
