package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class GPSStatusReminder implements IReminder {
    private static final long[] TIME_SPAN_VIBRATE = new long[]{0, 800};
    private Context mContext = null;
    PromptWindow mPromptWindow;
    private int mSportType = -1;
    private Vibrator mVibrator = null;

    class C06011 implements IDialogKeyEventListener {
        C06011() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public GPSStatusReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        boolean isGpsAvaiable = false;
        if (params != null && params.length > 0) {
            isGpsAvaiable = ((Boolean) params[0]).booleanValue();
        }
        this.mPromptWindow = new PromptWindow(this.mContext);
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_gps, null);
        if (isGpsAvaiable) {
            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(C0532R.string.gps_notify_success);
            remindRootView.findViewById(C0532R.id.remind_icon).setBackgroundResource(C0532R.drawable.sport_notify_process_good);
            remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_time_bg));
        } else {
            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(C0532R.string.gps_notify_loss);
            remindRootView.findViewById(C0532R.id.remind_icon).setBackgroundResource(C0532R.drawable.sport_notify_signal_lost);
            remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_heart_bg));
        }
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.showWithVibrator(TIME_SPAN_VIBRATE, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(3, true, new Object[0]);
        this.mPromptWindow.setDialogKeyListener(new C06011());
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
