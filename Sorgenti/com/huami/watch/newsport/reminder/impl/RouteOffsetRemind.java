package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class RouteOffsetRemind implements IReminder {
    private static final long[] VIBRATE_PATTERN = new long[]{0, 500, 200, 500};
    private Context mContext = null;
    private PromptWindow mPromptWindow = null;
    private int mRouteType = -1;
    private int mSportType = -1;
    private int mTargetKilometer = -1;
    private TextView mTitleView = null;
    private Vibrator mVibrator = null;

    class C06041 implements OnClickListener {
        C06041() {
        }

        public void onClick(View v) {
            RouteOffsetRemind.this.mPromptWindow.dismiss();
        }
    }

    class C06052 implements Runnable {
        C06052() {
        }

        public void run() {
            RouteOffsetRemind.this.mPromptWindow.dismiss();
        }
    }

    class C06063 implements IDialogKeyEventListener {
        C06063() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public RouteOffsetRemind(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_route_offset, null);
        ImageView icon = (ImageView) remindRootView.findViewById(C0532R.id.icon_notify_complete);
        int iconId = -1;
        String message = null;
        switch (this.mRouteType) {
            case 7:
                iconId = C0532R.drawable.sport_alert_departure_trajectory;
                message = this.mContext.getString(C0532R.string.gpx_offset_route);
                break;
            case 8:
                iconId = C0532R.drawable.sport_alert_back_trajectory;
                message = this.mContext.getString(C0532R.string.gpx_offset_route);
                break;
        }
        icon.setBackgroundResource(iconId);
        ((TextView) remindRootView.findViewById(C0532R.id.look_to_route)).setOnClickListener(new C06041());
        if (message != null) {
            ((TextView) remindRootView.findViewById(C0532R.id.route_status_remind)).setText(message);
        } else {
            ((TextView) remindRootView.findViewById(C0532R.id.route_status_remind)).setText("");
        }
        if (this.mRouteType == 8) {
            ((TextView) remindRootView.findViewById(C0532R.id.route_status_remind)).setText(this.mContext.getString(C0532R.string.gpx_back_route));
            remindRootView.findViewById(C0532R.id.line_separate).setVisibility(4);
            remindRootView.findViewById(C0532R.id.look_to_route).setVisibility(4);
            Global.getGlobalUIHandler().postDelayed(new C06052(), 2000);
        } else {
            ((TextView) remindRootView.findViewById(C0532R.id.route_status_remind)).setText(this.mContext.getString(C0532R.string.gpx_offset_route));
            remindRootView.findViewById(C0532R.id.line_separate).setVisibility(0);
            remindRootView.findViewById(C0532R.id.look_to_route).setVisibility(0);
        }
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06063());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        if (params != null && params.length >= 1) {
            this.mRouteType = ((Integer) params[0]).intValue();
            initPromptViewIfNeeded();
            this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        }
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
