package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class DistanceRepeatReminder implements IReminder {
    private static final long[] TIME_SPAN_DISTANCE_REPEAT_REMIND = new long[]{0, 800};
    private float dailyFloatValue = 0.0f;
    private HmLinearLayout hasDailyPerpormenceContainer = null;
    private boolean isImperial = false;
    private int kmDistance = 0;
    private int mBgType = 0;
    private Context mContext = null;
    private String mCurrentPace = null;
    private float mCurrentPaceFloat = 0.0f;
    private int mKilometer;
    private PromptWindow mPromptWindow = null;
    private int mSportType = 1;
    private TextView mTitleView = null;
    private Vibrator mVibrator = null;
    private HmLinearLayout noDailyPerformenceContainer = null;
    private int teValue = 0;
    private long totalTime = -1;

    class C06001 implements IDialogKeyEventListener {
        C06001() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public DistanceRepeatReminder(Context context, int SportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = SportType;
        this.mBgType = BackgroundUtils.getCustomBgType(context);
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_distance_kilometer_newremind, null);
        this.noDailyPerformenceContainer = (HmLinearLayout) remindRootView.findViewById(C0532R.id.ll_last_use_time);
        this.hasDailyPerpormenceContainer = (HmLinearLayout) remindRootView.findViewById(C0532R.id.ll_first_beat_contaner);
        if (this.mBgType == 1) {
            remindRootView.findViewById(C0532R.id.dis_repeat_container).setBackgroundColor(-1);
            ((TextView) remindRootView.findViewById(C0532R.id.kilometer_nums)).setTextColor(this.mContext.getResources().getColor(C0532R.color.bg_white_red_bg));
            ((TextView) remindRootView.findViewById(C0532R.id.status_remind_title)).setTextColor(this.mContext.getResources().getColor(C0532R.color.bg_white_red_bg));
            ((TextView) remindRootView.findViewById(C0532R.id.last_kilo_use_time)).setTextColor(-16777216);
        }
        Log.i("DistanceRepeatReminder", " mSportType:" + this.mSportType + ",dailyFloatValue:" + this.dailyFloatValue);
        Log.i("DistanceRepeatReminder", " noDailyPerformenceContainer , mCurrentPaceFloat:" + this.mCurrentPaceFloat + ", isImperial:" + this.isImperial);
        this.hasDailyPerpormenceContainer.setVisibility(8);
        this.noDailyPerformenceContainer.setVisibility(0);
        ((HmTextView) remindRootView.findViewById(C0532R.id.kilometer_nums)).setText(this.mKilometer + this.mContext.getResources().getString(this.isImperial ? C0532R.string.km_imperial : C0532R.string.km_metric));
        ((HmTextView) remindRootView.findViewById(C0532R.id.last_kilo_use_time)).setText(DataFormatUtils.parseCrossingSecondToDefaultFormattedTimeContainHour((long) ((((double) this.mCurrentPaceFloat) * UnitConvertUtils.convertDistanceFromMileOrKmtokm(1.0d, this.isImperial)) * 1000.0d)));
        ((TextView) remindRootView.findViewById(C0532R.id.status_remind_title)).setText(this.mContext.getResources().getString(this.isImperial ? C0532R.string.last_mi_times : C0532R.string.last_kilo_meters_times));
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06001());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        if (params == null || params.length < 6) {
            Debug.m6w("DistanceRepeatReminder", "params is null or empty");
            return;
        }
        SportSnapshot sportSnapshot = SportDataManager.getInstance().getSportSnapshot();
        if (sportSnapshot != null) {
            this.totalTime = sportSnapshot.getTotalTime();
        }
        this.isImperial = ((Boolean) params[3]).booleanValue();
        this.teValue = ((Integer) params[4]).intValue();
        this.dailyFloatValue = ((Float) params[5]).floatValue();
        this.kmDistance = (int) ((Float) params[0]).floatValue();
        this.mCurrentPaceFloat = ((Float) params[1]).floatValue();
        Log.i("DistanceRepeatReminder", " teValue:" + this.teValue + " ,DailyPropermentceValue:" + this.dailyFloatValue);
        this.mKilometer = (int) Math.round(UnitConvertUtils.convertDistanceToMileOrKm((double) ((Float) params[0]).floatValue()));
        if (DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).isRemindTargetDistance()) {
            int targetDistance = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).getTargetDistance() / 1000;
            Debug.m5i("DistanceRepeatReminder", "target distance is opened, tarDis: " + targetDistance + ", curdis: " + this.mKilometer + ", dis: " + sportStatus.getDistance());
            if (this.mKilometer == targetDistance) {
                return;
            }
        }
        if (sportStatus == null) {
            Debug.m5i("DistanceRepeatReminder", "sportStatus is null");
        } else {
            this.mSportType = sportStatus.getSportType();
            if (this.mSportType == 6 || this.mSportType == 9) {
                this.mCurrentPace = DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(((Float) params[2]).floatValue())), false);
            } else {
                this.mCurrentPace = DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(((Float) params[1]).floatValue()));
            }
        }
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(TIME_SPAN_DISTANCE_REPEAT_REMIND, -1, VibratorUtil.getMostStrongAttributes());
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
