package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.widget.TextView;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;

public class TargetIndividualReminder implements IReminder {
    private static final long[] VIBRATE_PATTERN = new long[]{0, 200, 200, 200, 200, 500};
    private Context mContext = null;
    private long mCostTime;
    private int mIndex = 0;
    private float mPace;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private TextView mTitleView = null;
    private Vibrator mVibrator = null;

    public TargetIndividualReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        if (params == null || params.length == 0) {
            Debug.m6w("TargetIndividualReminder", "params is null or empty");
            return;
        }
        if (sportStatus == null) {
            Debug.m5i("TargetIndividualReminder", "sportStatus is null");
        } else {
            this.mIndex = ((Integer) params[0]).intValue();
            this.mCostTime = ((Long) params[1]).longValue();
            this.mPace = ((Float) params[2]).floatValue();
        }
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
