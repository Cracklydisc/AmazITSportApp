package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class DailyPropermanceSixMinuteReminder implements IReminder {
    private static final String TAG = DailyPropermanceSixMinuteReminder.class.getSimpleName();
    private static final long[] TIME_SPAN_DISTANCE_REPEAT_REMIND = new long[]{0, 800};
    private int dailPropermanceValue;
    private Context mContext = null;
    private PromptWindow mPromptWindow = null;
    private int mSportType = 1;
    private Vibrator mVibrator = null;
    private NumberTextView staminaLevelView;
    private HmTextView staminaStatusView;

    class C05981 implements IDialogKeyEventListener {
        C05981() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public DailyPropermanceSixMinuteReminder(Context context, int SportType) {
        Log.i(TAG, "DailyPropermanceSixMinuteReminder");
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = SportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_six_daily_propermance_remind, null);
        this.staminaLevelView = (NumberTextView) remindRootView.findViewById(C0532R.id.stamina_level);
        this.staminaStatusView = (HmTextView) remindRootView.findViewById(C0532R.id.stamina_status);
        this.staminaLevelView.setText(String.valueOf(this.dailPropermanceValue > 0 ? "+" + this.dailPropermanceValue : Integer.valueOf(this.dailPropermanceValue)));
        this.staminaStatusView.setText(FirstBeatDataUtils.getRealTimePerPormanceLevel(this.mContext, (float) this.dailPropermanceValue));
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C05981());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        if (params == null) {
            Debug.m6w(TAG, "params is null or empty");
            return;
        }
        PointUtils.recordEventProperty("0076", this.mSportType);
        this.dailPropermanceValue = ((Integer) params[0]).intValue();
        Log.i(TAG, " currnet dailyPropermanceValue:" + this.dailPropermanceValue);
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(TIME_SPAN_DISTANCE_REPEAT_REMIND, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(16, true, Integer.valueOf(this.dailPropermanceValue));
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
