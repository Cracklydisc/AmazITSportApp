package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.reminder.controller.ReminderManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class TargetTimeCostReminder implements IReminder {
    private static final long[] VIBRATE_PATTERN = new long[]{0, 800, 200, 500, 200, 500};
    private Context mContext = null;
    private HmTextView mHourView = null;
    private HmTextView mMinView = null;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private int mTargetSecond = -1;
    private TextView mTitleView = null;
    private Vibrator mVibrator = null;

    class C06111 implements IDialogKeyEventListener {
        C06111() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public TargetTimeCostReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView;
        if (ReminderManager.getInstance().isTrainningMode() && (ReminderManager.getInstance().getSportType() == 9 || ReminderManager.getInstance().getSportType() == 14)) {
            remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.remind_custom_trainning_complete_view, null);
            ((ImageView) remindRootView.findViewById(C0532R.id.remind_icon)).setImageResource(C0532R.drawable.training_notify_completed_trainning);
            this.mTitleView = (TextView) remindRootView.findViewById(C0532R.id.remind_type_text);
            remindRootView.findViewById(C0532R.id.remind_type_text_tint).setVisibility(0);
            this.mPromptWindow = new PromptWindow(this.mContext);
            SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
            this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        } else {
            remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_time_cost, null);
            ((HmTextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.remind_target_time_title));
            remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_time_bg));
            this.mHourView = (HmTextView) remindRootView.findViewById(C0532R.id.alert_value);
            this.mMinView = (HmTextView) remindRootView.findViewById(C0532R.id.alert_value1);
            this.mPromptWindow = new PromptWindow(this.mContext);
            SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
            this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        }
        this.mPromptWindow.setDialogKeyListener(new C06111());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        int minute;
        PointUtils.recordEventProperty("0057", this.mSportType);
        initPromptViewIfNeeded();
        String titleText = null;
        if (ReminderManager.getInstance().isTrainningMode() && (ReminderManager.getInstance().getSportType() == 9 || ReminderManager.getInstance().getSportType() == 14)) {
            this.mTargetSecond = TrainingInfoManager.getInstance(this.mContext).getTrainingInfo().getMaxTime() / 1000;
            minute = (this.mTargetSecond / 60) % 60;
            if (ReminderManager.getInstance().getSportType() == 9) {
                titleText = String.format(this.mContext.getString(C0532R.string.remind_target_time_training_title_format), new Object[]{Integer.valueOf(minute)});
            } else if (ReminderManager.getInstance().getSportType() == 14) {
                titleText = String.format(this.mContext.getString(C0532R.string.remind_target_time_training_title_swim_format), new Object[]{Integer.valueOf(minute)});
            }
        } else {
            this.mTargetSecond = ((int) DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).getTargetTimeCost()) / 1000;
        }
        int hour = this.mTargetSecond / 3600;
        minute = (this.mTargetSecond / 60) % 60;
        if (this.mTitleView != null) {
            this.mTitleView.setText(titleText);
        } else {
            this.mHourView.setText(String.format("%02d", new Object[]{Integer.valueOf(hour)}));
            this.mMinView.setText(String.format("%02d", new Object[]{Integer.valueOf(minute)}));
        }
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(8, true, Long.valueOf(DataManager.getInstance().getSportConfig(this.mContext, this.mSportType).getTargetTimeCost()));
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
