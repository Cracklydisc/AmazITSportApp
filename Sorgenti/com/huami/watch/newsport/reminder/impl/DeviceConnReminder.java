package com.huami.watch.newsport.reminder.impl;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.bleconn.BleConnManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class DeviceConnReminder {
    private static final String TAG = DeviceConnReminder.class.getName();
    private static final long[] VIBRATE_PATTERN_DEVICE_IN = new long[]{0, 800};
    private static final long[] VIBRATE_PATTERN_DEVICE_OUT = new long[]{0, 500, 200, 500};
    private BluetoothAdapter mBluetoothApapter;
    private Context mContext;
    private PromptWindow mPromptWindow = null;

    class C05991 implements IDialogKeyEventListener {
        C05991() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public DeviceConnReminder(Context context) {
        this.mContext = context;
    }

    private void initPromptViewIfNeeded(boolean isConn) {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_device, null);
        if (isConn) {
            remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_time_bg));
        } else {
            remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_heart_bg));
        }
        int deviceRes = isHRDeviceConnect() ? C0532R.drawable.sport_notify_device_hr : C0532R.drawable.sport_notify_device_hr_lost;
        int earphoneRes = isBtConnect() ? C0532R.drawable.sport_notify_device_earphone : C0532R.drawable.sport_notify_device_earphone_lost;
        if (isConn) {
            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.device_in));
        } else {
            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.device_out));
        }
        ((ImageView) remindRootView.findViewById(C0532R.id.remind_device_icon)).setBackgroundResource(deviceRes);
        ((ImageView) remindRootView.findViewById(C0532R.id.remind_earear_icon)).setBackgroundResource(earphoneRes);
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C05991());
    }

    public void notifyDeviceConnStatus(boolean isConn) {
        initPromptViewIfNeeded(isConn);
        if (isConn) {
            this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN_DEVICE_IN, -1, VibratorUtil.getMostStrongAttributes());
        } else {
            this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN_DEVICE_OUT, -1, VibratorUtil.getMostStrongAttributes());
        }
    }

    private boolean isBtConnect() {
        if (this.mBluetoothApapter == null) {
            this.mBluetoothApapter = BluetoothAdapter.getDefaultAdapter();
        }
        return this.mBluetoothApapter.getProfileConnectionState(2) == 2;
    }

    private boolean isHRDeviceConnect() {
        int curState = BleConnManager.getInstance().getDeviceConnState(0, 1);
        LogUtil.m9i(true, TAG, "isHRDeviceConnect, curState:" + curState);
        if (curState == 2) {
            return true;
        }
        return false;
    }

    public void reset() {
        if (this.mPromptWindow != null) {
            try {
                this.mPromptWindow.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
