package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.model.TrainConfig;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;
import java.util.List;

public class PaceReminder implements IReminder {
    private static final long[] TIME_SPAN_PACE_REMIND = new long[]{0, 500, 200, 500};
    private int currnetPaceType = 0;
    private Context mContext = null;
    private float mCurrentPace = 0.0f;
    private String mCurrentPaceStr = null;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private Vibrator mVibrator = null;

    class C06031 implements IDialogKeyEventListener {
        C06031() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public PaceReminder(Context context, int sportType) {
        this.mContext = context;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        this.mSportType = sportType;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_pace, null);
        this.currnetPaceType = 0;
        ((TextView) remindRootView.findViewById(C0532R.id.alert_value)).setText(this.mCurrentPaceStr);
        if (this.mSportType == 11) {
            ((TextView) remindRootView.findViewById(C0532R.id.alert_unit)).setText(this.mContext.getString(UnitConvertUtils.isImperial() ? C0532R.string.km_imperial_speed_unit : C0532R.string.km_metric_speed_unit));
            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.speed_too_low_remind_title));
        } else {
            ((TextView) remindRootView.findViewById(C0532R.id.alert_unit)).setText(this.mContext.getString(UnitConvertUtils.isImperial() ? C0532R.string.running_pace_unit : C0532R.string.km_metric_pace_unit));
            if (SportDataManager.getInstance().isIntervalTrain()) {
                TrainUnit trainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
                LogUtil.m9i(true, "PaceReminder", "trainunit:" + trainUnit);
                if (trainUnit != null) {
                    List<TrainConfig> configList = trainUnit.getTrainConfigList();
                    if (!(configList == null || configList.isEmpty())) {
                        if (this.mCurrentPace < ((TrainConfig) configList.get(0)).getMaxPace()) {
                            this.currnetPaceType = 1;
                            ((TextView) remindRootView.findViewById(C0532R.id.remind_type_text)).setText(this.mContext.getString(C0532R.string.remind_pace_too_fast));
                        }
                    }
                }
            }
        }
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06031());
    }

    public void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        float speed = 0.0f;
        PointUtils.recordEventProperty("0061", this.mSportType);
        if (sportStatus == null || params == null || params.length == 0) {
            Debug.m5i("PaceReminder", "sportStatus is null, param: " + params);
        } else {
            this.mCurrentPace = ((Float) params[0]).floatValue();
            if (this.mSportType == 11) {
                if (Float.compare(this.mCurrentPace, 0.0f) != 0) {
                    speed = 1.0f / this.mCurrentPace;
                }
                this.mCurrentPaceStr = String.valueOf(Math.round(3.6f * speed));
            } else {
                this.mCurrentPaceStr = DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(this.mCurrentPace));
            }
        }
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(TIME_SPAN_PACE_REMIND, -1, VibratorUtil.getMostStrongAttributes());
        switch (this.currnetPaceType) {
            case 0:
                PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(6, true, Float.valueOf(SportDataFilterUtils.parsePace(((Float) params[0]).floatValue())));
                return;
            case 1:
                PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(18, true, Float.valueOf(SportDataFilterUtils.parsePace(((Float) params[0]).floatValue())));
                return;
            default:
                return;
        }
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
