package com.huami.watch.newsport.reminder.impl;

import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.voiceplayer.controller.PlayControllerManager;
import com.huami.watch.prompt.PromptWindow;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.prompt.PromptWindow.LayoutParams;

public class HeartRegionRemind implements IReminder {
    private static final String TAG = HeartRegionRemind.class.getSimpleName();
    private static final long[] VIBRATE_PATTERN = new long[]{0, 500, 200, 500, 200, 500, 200, 500};
    private BaseConfig config = null;
    private int currnetStatus = 0;
    private float fontTextSize = 15.0f;
    private int f17i = 0;
    private Context mContext = null;
    private int mHeartRate;
    private PromptWindow mPromptWindow = null;
    private int mSportType = -1;
    private Vibrator mVibrator = null;

    class C06021 implements IDialogKeyEventListener {
        C06021() {
        }

        public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
            SportDataManager.getInstance().notifyDialogKeyEvent(event);
        }
    }

    public HeartRegionRemind(Context context, int sportType) {
        this.mContext = context;
        this.mSportType = sportType;
        this.mVibrator = (Vibrator) this.mContext.getSystemService("vibrator");
        LogUtils.print(TAG, "HeartRegionRemind");
        this.config = DataManager.getInstance().getSportConfig(this.mContext, this.mSportType);
        this.currnetStatus = 0;
    }

    private void initPromptViewIfNeeded() {
        ViewGroup remindRootView = (ViewGroup) LayoutInflater.from(this.mContext).inflate(C0532R.layout.alert_templete, null);
        ((ImageView) remindRootView.findViewById(C0532R.id.remind_icon)).setImageResource(C0532R.drawable.sport_notify_heart_remind);
        TextView overHeartRegionView = (TextView) remindRootView.findViewById(C0532R.id.remind_type_text);
        ((TextView) remindRootView.findViewById(C0532R.id.alert_unit)).setText(this.mContext.getString(C0532R.string.remind_heart_rate_value_after));
        if (this.mHeartRate > this.config.getmHeartRegionValueMax()) {
            LogUtils.print(TAG, " heart rata too high ");
            this.currnetStatus = 1;
            overHeartRegionView.setText(this.mContext.getString(C0532R.string.remind_heart_rate_region_over));
        } else {
            LogUtils.print(TAG, " heart rata too less ");
            this.currnetStatus = 2;
            overHeartRegionView.setText(this.mContext.getString(C0532R.string.remind_heart_rate_region_less));
        }
        ((TextView) remindRootView.findViewById(C0532R.id.alert_value)).setText(String.valueOf(this.mHeartRate));
        remindRootView.findViewById(C0532R.id.alert_main_container).setBackgroundColor(this.mContext.getResources().getColor(C0532R.color.alert_heart_bg));
        this.mPromptWindow = new PromptWindow(this.mContext);
        SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(this.mPromptWindow.getWindow(), remindRootView);
        this.mPromptWindow.setContentView(remindRootView, new LayoutParams(-1, -1));
        this.mPromptWindow.setDialogKeyListener(new C06021());
    }

    public final void remind(OutdoorSportSnapshot sportStatus, Object... params) {
        LogUtils.print(TAG, "HeartRegionRemind");
        if (SportType.isMixedSport(this.mSportType) && SportDataManager.getInstance().getCurentSportType() != 1001) {
            return;
        }
        if (params == null || params.length == 0) {
            Debug.m6w(TAG, "params is null or empty");
            return;
        }
        if (sportStatus == null) {
            Debug.m5i(TAG, "sportStatus is null");
        } else {
            this.mHeartRate = ((Integer) params[0]).intValue();
        }
        initPromptViewIfNeeded();
        this.mPromptWindow.showWithVibrator(VIBRATE_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
        PlayControllerManager.getInstance(this.mContext).playRemindVoiceIfNeed(12, true, Integer.valueOf(this.mHeartRate), Integer.valueOf(this.currnetStatus));
    }

    public void reset() {
        if (this.mPromptWindow != null && this.mPromptWindow.isShowing()) {
            this.mPromptWindow.dismiss();
        }
    }
}
