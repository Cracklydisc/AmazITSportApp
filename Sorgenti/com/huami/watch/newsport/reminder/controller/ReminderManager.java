package com.huami.watch.newsport.reminder.controller;

import android.content.Context;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.SportOptions;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.reminder.action.IReminder;
import com.huami.watch.newsport.reminder.impl.DailyPropermanceSixMinuteReminder;
import com.huami.watch.newsport.reminder.impl.DistanceRepeatReminder;
import com.huami.watch.newsport.reminder.impl.GPSStatusReminder;
import com.huami.watch.newsport.reminder.impl.HeartRegionRemind;
import com.huami.watch.newsport.reminder.impl.PaceReminder;
import com.huami.watch.newsport.reminder.impl.RouteOffsetRemind;
import com.huami.watch.newsport.reminder.impl.SafeHeartRateReminder;
import com.huami.watch.newsport.reminder.impl.SportRealTimeGuideRemind;
import com.huami.watch.newsport.reminder.impl.TargetCalorieReminder;
import com.huami.watch.newsport.reminder.impl.TargetDistanceReminder;
import com.huami.watch.newsport.reminder.impl.TargetIndividualReminder;
import com.huami.watch.newsport.reminder.impl.TargetTimeCostReminder;
import com.huami.watch.newsport.reminder.impl.WearLooseReminder;
import com.huami.watch.newsport.utils.Constants.GPSUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ReminderManager {
    private static final int[] COMPOUND_REMIND_CONFIG = new int[]{5, 0, 1, 10, 12};
    private static final int[] CROSSING_REMIND_CONFIG = new int[]{8, 9, 7, 5, 1, 0, 10, 11, 2, 3};
    private static final int[] ELLIPTICAL_MACHINE_REMIND_CONFIG = new int[]{2, 11, 1, 10};
    private static final int[] INDOOR_RIDING_REMIND_CONFIG = new int[]{2, 1, 10, 11};
    private static final int[] INDOOR_RUN_REMIND_CONFIG = new int[]{0, 3, 4, 2, 1, 10, 11, 12};
    private static final int[] MOUNTAINEER_REMIND_CONFIG = new int[]{8, 9, 7, 5, 1, 10};
    private static final int[] OPEN_SWIM_REMIND_CONFIG = new int[]{5, 2, 11, 3, 1, 10};
    private static final int[] OUTDOOR_RIDING_REMIND_CONFIG = new int[]{8, 9, 7, 0, 3, 2, 11, 5, 1, 10};
    private static final Map<Integer, int[]> REMIND_SPORT_MAP = new HashMap();
    private static final int[] RUUNING_REMIND_CONFIG = new int[]{8, 9, 7, 0, 3, 4, 2, 5, 1, 11, 10, 16, 15, 12};
    private static final int[] SKIING_REMIND_CONFIG = new int[]{8, 9, 7, 5, 1, 10, 4};
    private static final int[] SOCCER_REMIND_CONFIG = new int[]{8, 9, 7, 0, 3, 4, 2, 5, 1, 11, 10, 16, 15, 12};
    private static final int[] SWIM_MACHINE_REMIND_CONFIG = new int[]{2, 11, 1, 10, 3};
    private static final int[] TENNIS_REMIND_CONFIG = new int[]{8, 9, 7, 0, 3, 4, 2, 5, 1, 11, 10, 16, 15, 12};
    private static final int[] TRIATHLON_REMIND_CONFIG = new int[]{5, 0, 1, 10, 12};
    private static final int[] WALKING_REMIND_CONFIG = new int[]{8, 9, 7, 0, 3, 2, 11, 5, 1, 10};
    private static ReminderManager mReminderManager = null;
    private boolean mIsTrainingMode = false;
    private IReminderListener mListener = null;
    private Map<Integer, IReminder> mReminders = new TreeMap();
    private int mSportType = -1;

    public interface IReminderListener {
        OutdoorSportSnapshot getSportStatus();
    }

    static {
        REMIND_SPORT_MAP.put(Integer.valueOf(1), RUUNING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(6), WALKING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(7), CROSSING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(8), INDOOR_RUN_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(9), OUTDOOR_RIDING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(10), INDOOR_RIDING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(12), ELLIPTICAL_MACHINE_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(2001), TRIATHLON_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(14), SWIM_MACHINE_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(15), OPEN_SWIM_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(13), MOUNTAINEER_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(11), SKIING_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(17), TENNIS_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(18), SOCCER_REMIND_CONFIG);
        REMIND_SPORT_MAP.put(Integer.valueOf(2002), COMPOUND_REMIND_CONFIG);
    }

    public static synchronized ReminderManager getInstance() {
        ReminderManager reminderManager;
        synchronized (ReminderManager.class) {
            if (mReminderManager == null) {
                mReminderManager = new ReminderManager();
            }
            reminderManager = mReminderManager;
        }
        return reminderManager;
    }

    public boolean isTrainningMode() {
        return this.mIsTrainingMode;
    }

    public int getSportType() {
        return this.mSportType;
    }

    public void initReminder(SportOptions options, Context context) {
        context = context.getApplicationContext();
        int sportType = options.getSportType();
        int gpsStatus = options.getGpsStatus();
        this.mIsTrainingMode = options.isTranningMode();
        Debug.m5i("SportReminderManager", "initReminder, type: " + sportType);
        if (this.mReminders.isEmpty() || this.mSportType != sportType) {
            resetReminder();
            this.mSportType = sportType;
            if (this.mIsTrainingMode) {
                this.mReminders.put(Integer.valueOf(3), new TargetDistanceReminder(context, sportType));
                this.mReminders.put(Integer.valueOf(1), new SafeHeartRateReminder(context, sportType));
                if (this.mSportType == 9 || this.mSportType == 14) {
                    this.mReminders.put(Integer.valueOf(2), new TargetTimeCostReminder(context, sportType));
                    return;
                }
                return;
            }
            for (int type : (int[]) REMIND_SPORT_MAP.get(Integer.valueOf(sportType))) {
                this.mReminders.put(Integer.valueOf(type), getCorrespodingReminder(context, type));
            }
            if (gpsStatus == 1 && !GPSUtils.isGPSRouteAvailable(sportType)) {
                this.mReminders.remove(Integer.valueOf(5));
                return;
            }
            return;
        }
        Debug.m5i("SportReminderManager", "initReminder, reminder is not empty");
    }

    public void resetReminder() {
        Debug.m5i("SportReminderManager", "clearReminder");
        for (Integer intValue : this.mReminders.keySet()) {
            int type = intValue.intValue();
            IReminder reminder = (IReminder) this.mReminders.get(Integer.valueOf(type));
            if (reminder != null) {
                reminder.reset();
            } else {
                Debug.m5i("SportReminderManager", "resetReminder, reminder type:" + type);
            }
        }
        this.mReminders.clear();
        this.mListener = null;
    }

    public void remind(int remindType, Object... params) {
        Debug.m3d("SportReminderManager", "remind, remindType: " + remindType);
        IReminder reminder = (IReminder) this.mReminders.get(Integer.valueOf(remindType));
        if (reminder == null) {
            Debug.m3d("SportReminderManager", "remind, there is no this reminder type, remindType:" + remindType);
        } else if (this.mListener == null) {
            Debug.m3d("SportReminderManager", "remind, listener is null");
        } else {
            reminder.remind(this.mListener.getSportStatus(), params);
            if ((reminder instanceof GPSStatusReminder) && !GPSUtils.isGPSRouteAvailable(this.mSportType)) {
                this.mReminders.remove(Integer.valueOf(5));
            }
        }
    }

    public void setReminderListener(IReminderListener listener) {
        this.mListener = listener;
    }

    private IReminder getCorrespodingReminder(Context context, int type) {
        switch (type) {
            case 0:
                return new DistanceRepeatReminder(context, this.mSportType);
            case 1:
                return new SafeHeartRateReminder(context, this.mSportType);
            case 2:
                return new TargetTimeCostReminder(context, this.mSportType);
            case 3:
                return new TargetDistanceReminder(context, this.mSportType);
            case 4:
                return new PaceReminder(context, this.mSportType);
            case 5:
                return new GPSStatusReminder(context, this.mSportType);
            case 6:
                return new TargetIndividualReminder(context, this.mSportType);
            case 7:
            case 8:
            case 9:
                return new RouteOffsetRemind(context, this.mSportType);
            case 10:
                return new WearLooseReminder(context, this.mSportType);
            case 11:
                return new TargetCalorieReminder(context, this.mSportType);
            case 12:
                return new HeartRegionRemind(context, this.mSportType);
            case 15:
                return new SportRealTimeGuideRemind(context, this.mSportType);
            case 16:
                return new DailyPropermanceSixMinuteReminder(context, this.mSportType);
            default:
                return null;
        }
    }
}
