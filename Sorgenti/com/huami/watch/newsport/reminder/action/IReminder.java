package com.huami.watch.newsport.reminder.action;

import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;

public interface IReminder {
    void remind(OutdoorSportSnapshot outdoorSportSnapshot, Object... objArr);

    void reset();
}
