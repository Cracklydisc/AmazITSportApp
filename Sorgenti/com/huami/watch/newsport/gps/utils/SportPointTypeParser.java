package com.huami.watch.newsport.gps.utils;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.sensor.HmGpsLocation;
import java.util.Locale;

public class SportPointTypeParser {
    public static int parsePointType(HmGpsLocation location) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportPointTypeParser", String.format(Locale.US, "parse point lat %f, lng %f, type %08X", new Object[]{Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()), Integer.valueOf(location.getPointType())}));
        }
        return parseAlgoPointType(location.getPointType());
    }

    public static int parseAlgoPointType(int pointType) {
        if ((pointType & 1) == 0) {
            return 32768;
        }
        int result = 1;
        if ((pointType & 256) != 0) {
            result = 1 | 2;
        }
        if ((pointType & 512) != 0) {
            result = (result | 2) | 4;
        }
        if ((32768 & pointType) != 0) {
            return result | 4096;
        }
        return result;
    }
}
