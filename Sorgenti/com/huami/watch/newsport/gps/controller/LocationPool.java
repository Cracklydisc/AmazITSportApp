package com.huami.watch.newsport.gps.controller;

import android.util.LongSparseArray;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.LinkedList;
import java.util.List;

public class LocationPool {
    private LongSparseArray<SportLocationData> mCacheMap;
    private int mCacheSize;
    private List<SportLocationData> mCached;
    private ILocationPoolListener mListener;
    private List<SportLocationData> mPool;
    private int mPoolSize;
    private SyncDatabaseManager<SportLocationData> mSyncDatabaseManager;

    public interface ILocationPoolListener {
        void onFinalLocationConfirm(List<SportLocationData> list, long j);
    }

    public LocationPool(ILocationPoolListener listener) {
        this.mCached = new LinkedList();
        this.mPool = new LinkedList();
        this.mCacheMap = new LongSparseArray();
        this.mPoolSize = 200;
        this.mCacheSize = 100;
        this.mSyncDatabaseManager = null;
        this.mListener = null;
        this.mSyncDatabaseManager = new SyncDatabaseManager(LocationDataDao.getInstance(SportApplication.getInstance()));
        this.mListener = listener;
    }

    public synchronized boolean changeIfExist(SportLocationData locationData) {
        boolean z;
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "change is exist " + locationData + ". current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
        SportLocationData cachedLocation = (SportLocationData) this.mCacheMap.get(locationData.mTimestamp);
        if (cachedLocation != null) {
            cachedLocation.copy(locationData);
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized boolean deleteIfExist(SportLocationData locationData) {
        boolean z;
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "delete is exist " + locationData + ". current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
        SportLocationData cachedLocation = (SportLocationData) this.mCacheMap.get(locationData.mTimestamp);
        if (cachedLocation != null) {
            this.mCacheMap.remove(locationData.mTimestamp);
            this.mCached.remove(cachedLocation);
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized void add(SportLocationData locationData, long trackId) {
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "add " + locationData + ". current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
        this.mCached.add(locationData);
        this.mCacheMap.append(locationData.mTimestamp, locationData);
        while (this.mCached.size() > this.mCacheSize) {
            SportLocationData fixedLocation = (SportLocationData) this.mCached.remove(0);
            this.mCacheMap.remove(fixedLocation.mTimestamp);
            this.mPool.add(fixedLocation);
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "fix location " + fixedLocation);
        }
        if (this.mPool.size() > this.mPoolSize) {
            addLocations(new LinkedList(this.mPool), trackId);
            this.mPool.clear();
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "clear pool");
        }
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "current cache " + this.mCached.size() + ". cached map " + this.mCacheMap.size() + ". pool " + this.mPool.size());
    }

    public synchronized void commit(long trackId) {
        List<SportLocationData> locationDatas = new LinkedList();
        locationDatas.addAll(this.mPool);
        locationDatas.addAll(this.mCached);
        addLocations(locationDatas, trackId);
        this.mPool.clear();
        this.mCached.clear();
        this.mCacheMap.clear();
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "LocationPool", "commit " + locationDatas.size());
    }

    public void addLocations(List<SportLocationData> locationDatas, long trackId) {
        this.mSyncDatabaseManager.addAll(SportApplication.getInstance(), locationDatas);
        if (this.mListener != null) {
            this.mListener.onFinalLocationConfirm(locationDatas, trackId);
        }
    }
}
