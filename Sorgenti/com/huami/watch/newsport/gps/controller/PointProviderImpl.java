package com.huami.watch.newsport.gps.controller;

import android.os.SystemClock;
import android.util.LongSparseArray;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.gps.callback.IPointProvider;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class PointProviderImpl implements IPointProvider {
    private List<SportLocationData> mFixedPointList = new LinkedList();
    private long mLatestTimestamp = -1;
    private boolean mNewAdded = false;
    private boolean mNewUpdated = false;
    private List<SportLocationData> mRecentAddedPointList = new LinkedList();
    private LongSparseArray<SportLocationData> mRecentAddedPointMap = new LongSparseArray();
    private volatile int mSize = 0;

    public synchronized int getUpdatePointsStatus(boolean forceDraw) {
        int i;
        if (!forceDraw) {
            if (!(this.mNewUpdated && this.mNewAdded)) {
                if (this.mNewAdded) {
                    i = 2;
                } else if (this.mNewUpdated) {
                    i = 3;
                } else {
                    i = 0;
                }
            }
        }
        i = 1;
        return i;
    }

    private void removeRecentPoints(List<SportLocationData> pointToRemove) {
        while (this.mRecentAddedPointList.size() > 100) {
            SportLocationData locationData = (SportLocationData) this.mRecentAddedPointList.remove(0);
            if (locationData != null) {
                this.mRecentAddedPointMap.remove(locationData.mTimestamp);
                pointToRemove.add(locationData);
            }
        }
    }

    private void clearFlags() {
        this.mNewUpdated = false;
        this.mNewAdded = false;
    }

    private void resizePoints(List<SportLocationData> locationDatas) {
        LogUtil.m7d(true, "PointProviderImpl", "resize fixed points : " + locationDatas.size());
        Iterator<SportLocationData> iterator = locationDatas.iterator();
        boolean isFilter = false;
        while (iterator.hasNext()) {
            iterator.next();
            if (isFilter) {
                iterator.remove();
                isFilter = false;
            } else {
                isFilter = true;
            }
        }
        LogUtil.m7d(true, "PointProviderImpl", "size of fixed points after resize : " + locationDatas.size());
    }

    public synchronized void addNewPoint(SportLocationData locationData) {
        long startTime = SystemClock.elapsedRealtime();
        LogUtil.m7d(Global.DEBUG_LEVEL_3, "PointProviderImpl", "add new point " + locationData);
        if (this.mRecentAddedPointList.isEmpty() || this.mLatestTimestamp < locationData.mTimestamp) {
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "PointProviderImpl", "add location. latest timestamp " + this.mLatestTimestamp);
            this.mRecentAddedPointList.add(locationData);
            this.mRecentAddedPointMap.append(locationData.mTimestamp, locationData);
            this.mLatestTimestamp = locationData.mTimestamp;
            this.mNewAdded = true;
            this.mSize++;
            LogUtil.m7d(Global.DEBUG_LEVEL_3, "PointProviderImpl", "add new point cost : " + (SystemClock.elapsedRealtime() - startTime) + "(mm)");
        } else {
            SportLocationData loc = (SportLocationData) this.mRecentAddedPointMap.get(locationData.mTimestamp);
            if (loc != null) {
                if ((locationData.mPointType & 32768) != 0) {
                    LogUtil.m7d(Global.DEBUG_LEVEL_3, "PointProviderImpl", "point type is invalid, remove " + locationData);
                    this.mRecentAddedPointList.remove(loc);
                    this.mRecentAddedPointMap.remove(loc.mTimestamp);
                    this.mSize--;
                    this.mNewUpdated = true;
                } else if (!(loc.mLongitude == locationData.mLongitude && loc.mLatitude == locationData.mLatitude)) {
                    LogUtil.m7d(Global.DEBUG_LEVEL_3, "PointProviderImpl", "update point " + locationData);
                    loc.copy(locationData);
                    this.mNewUpdated = true;
                }
            }
            if (!this.mNewUpdated) {
                Debug.m6w("PointProviderImpl", "cannot find location data " + locationData + " in list " + this.mRecentAddedPointList);
            }
            LogUtil.m7d(true, "PointProviderImpl", "update post point cost : " + (SystemClock.elapsedRealtime() - startTime) + "(mm)");
        }
    }

    public void getAllGpsPoints(List<SportLocationData> allRecordLocationDatas) {
        if (allRecordLocationDatas != null) {
            allRecordLocationDatas.clear();
            List<SportLocationData> pointsToFix = new LinkedList();
            removeRecentPoints(pointsToFix);
            this.mFixedPointList.addAll(pointsToFix);
            if (this.mFixedPointList.size() > 1000) {
                resizePoints(this.mFixedPointList);
            }
            if (this.mFixedPointList != null) {
                allRecordLocationDatas.addAll(this.mFixedPointList);
            }
            if (this.mRecentAddedPointList != null) {
                allRecordLocationDatas.addAll(this.mRecentAddedPointList);
            }
            clearFlags();
        }
    }

    public synchronized void reset() {
        clearFlags();
        this.mFixedPointList.clear();
        this.mRecentAddedPointList.clear();
        this.mRecentAddedPointMap.clear();
    }

    public synchronized int pointSize() {
        return this.mSize;
    }
}
