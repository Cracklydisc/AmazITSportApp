package com.huami.watch.newsport.gps.controller;

import com.huami.watch.newsport.gps.callback.IPointProvider;

public class PointProviderManager {
    private static PointProviderManager sInstance = null;
    private IPointProvider mPointProvider;

    private PointProviderManager() {
        this.mPointProvider = null;
        this.mPointProvider = new PointProviderImpl();
    }

    public static synchronized PointProviderManager getInstance() {
        PointProviderManager pointProviderManager;
        synchronized (PointProviderManager.class) {
            if (sInstance == null) {
                sInstance = new PointProviderManager();
            }
            pointProviderManager = sInstance;
        }
        return pointProviderManager;
    }

    public IPointProvider getPointProvider() {
        return this.mPointProvider;
    }
}
