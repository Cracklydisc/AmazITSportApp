package com.huami.watch.newsport.gps.model;

public class SportLocationData {
    public int mAlgoPointType = 0;
    public float mAltitude = 0.0f;
    public int mBar = 0;
    public float mCourse = 0.0f;
    public float mGPSAccuracy = 0.0f;
    public boolean mIsAutoPause = false;
    public float mLatitude = 0.0f;
    public long mLocationId = -1;
    public float mLongitude = 0.0f;
    public int mPointIndex = -1;
    public int mPointType = 0;
    public float mSpeed = 0.0f;
    public long mTimestamp = -1;
    public long mTrackId = -1;

    public void copy(SportLocationData locationData) {
        this.mTrackId = locationData.mTrackId;
        this.mLocationId = locationData.mLocationId;
        this.mTimestamp = locationData.mTimestamp;
        this.mLongitude = locationData.mLongitude;
        this.mLatitude = locationData.mLatitude;
        this.mAltitude = locationData.mAltitude;
        this.mGPSAccuracy = locationData.mGPSAccuracy;
        this.mPointType = locationData.mPointType;
        this.mSpeed = locationData.mSpeed;
        this.mIsAutoPause = locationData.mIsAutoPause;
        this.mPointIndex = locationData.mPointIndex;
        this.mBar = locationData.mBar;
        this.mAlgoPointType = locationData.mAlgoPointType;
        this.mCourse = locationData.mCourse;
    }

    public String toString() {
        return "SportLocationData{mTrackId=" + this.mTrackId + ", mLocationId=" + this.mLocationId + ", mTimestamp=" + this.mTimestamp + ", mLongitude=" + this.mLongitude + ", mLatitude=" + this.mLatitude + ", mAltitude=" + this.mAltitude + ", mGPSAccuracy=" + this.mGPSAccuracy + ", mPointType=" + this.mPointType + ", mSpeed=" + this.mSpeed + ", mIsAutoPause=" + this.mIsAutoPause + ", mPointIndex=" + this.mPointIndex + ", mBar=" + this.mBar + ", mCourse=" + this.mCourse + ", mAlgoPointType=" + this.mAlgoPointType + '}';
    }
}
