package com.huami.watch.newsport.gps.callback;

import com.huami.watch.newsport.gps.model.SportLocationData;
import java.util.List;

public interface IPointProvider {
    void addNewPoint(SportLocationData sportLocationData);

    void getAllGpsPoints(List<SportLocationData> list);

    int getUpdatePointsStatus(boolean z);

    int pointSize();

    void reset();
}
