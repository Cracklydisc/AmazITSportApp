package com.huami.watch.newsport.historydata.controller;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.historydata.listener.IHistoryDataListener;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import java.lang.ref.WeakReference;
import java.util.List;

public abstract class AbsHistoryDataManager<T> {
    protected static final String TAG = AbsHistoryDataManager.class.getName();
    protected WeakReference<Context> mContextRef = null;
    protected OutdoorSportSnapshot mCurrentStatus;
    protected IHistoryDataListener<T> mHistoryDataListener = null;
    private HandlerThread mHistoryHanderThread;
    protected boolean mIsStop = true;
    protected MyHandler mMyHandler;
    protected SensorHubManagerWrapper mSensorHubConfigManager = null;
    protected int mSportType = -1;
    protected long mStartTimestamp = 0;

    class C05721 implements Runnable {
        C05721() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r3 = this;
            r1 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;
            if (r1 == 0) goto L_0x000b;
        L_0x0004:
            r1 = com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.TAG;
            r2 = "onTicker";
            com.huami.watch.common.log.Debug.m5i(r1, r2);
        L_0x000b:
            r2 = com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.this;
            monitor-enter(r2);
            r1 = com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.this;	 Catch:{ all -> 0x002d }
            r0 = r1.getHistoryDataInfo();	 Catch:{ all -> 0x002d }
            if (r0 == 0) goto L_0x001c;
        L_0x0016:
            r1 = r0.size();	 Catch:{ all -> 0x002d }
            if (r1 != 0) goto L_0x001e;
        L_0x001c:
            monitor-exit(r2);	 Catch:{ all -> 0x002d }
        L_0x001d:
            return;
        L_0x001e:
            r1 = com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.this;	 Catch:{ all -> 0x002d }
            r1 = r1.mHistoryDataListener;	 Catch:{ all -> 0x002d }
            if (r1 == 0) goto L_0x002b;
        L_0x0024:
            r1 = com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.this;	 Catch:{ all -> 0x002d }
            r1 = r1.mHistoryDataListener;	 Catch:{ all -> 0x002d }
            r1.onHistoryDataReady(r0);	 Catch:{ all -> 0x002d }
        L_0x002b:
            monitor-exit(r2);	 Catch:{ all -> 0x002d }
            goto L_0x001d;
        L_0x002d:
            r1 = move-exception;
            monitor-exit(r2);	 Catch:{ all -> 0x002d }
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.historydata.controller.AbsHistoryDataManager.1.run():void");
        }
    }

    private static class MyHandler extends Handler {
        private WeakReference<AbsHistoryDataManager> mHistoryDataWeakRef = null;

        public MyHandler(Looper looper, AbsHistoryDataManager historyDataWeakRef) {
            super(looper);
            this.mHistoryDataWeakRef = new WeakReference(historyDataWeakRef);
        }

        public void handleMessage(Message msg) {
            AbsHistoryDataManager historyDataMamager = (AbsHistoryDataManager) this.mHistoryDataWeakRef.get();
            if (historyDataMamager != null) {
                switch (msg.what) {
                    case 1:
                        if (historyDataMamager.mIsStop) {
                            removeCallbacksAndMessages(null);
                            return;
                        }
                        historyDataMamager.onTicker();
                        sendEmptyMessageDelayed(1, historyDataMamager.getIntervalTime());
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public abstract List<T> getHistoryDataInfo();

    public abstract long getIntervalTime();

    public abstract String getThreadName();

    public AbsHistoryDataManager(Context context, long startTime, int sportType) {
        this.mSensorHubConfigManager = SensorHubManagerWrapper.getInstance(context);
        this.mContextRef = new WeakReference(context);
        this.mHistoryHanderThread = new HandlerThread(getThreadName());
        this.mStartTimestamp = startTime;
        this.mSportType = sportType;
    }

    public void setHistoryDataListener(IHistoryDataListener listener) {
        this.mHistoryDataListener = listener;
    }

    public void startListen() {
        Debug.m3d(TAG, "start listen heart rate");
        this.mHistoryHanderThread.start();
        this.mMyHandler = new MyHandler(this.mHistoryHanderThread.getLooper(), this);
        this.mIsStop = false;
        this.mMyHandler.obtainMessage(1).sendToTarget();
    }

    public synchronized List<T> stopListen() {
        Debug.m3d(TAG, "stop listen heart rate");
        if (this.mMyHandler == null) {
            Debug.m3d(TAG, "the handler is null, please make sure it is started");
        } else {
            this.mIsStop = true;
            this.mMyHandler.removeCallbacksAndMessages(null);
            this.mHistoryHanderThread.quitSafely();
            this.mMyHandler = null;
            this.mHistoryHanderThread = null;
            List<T> historyDataInfos = getHistoryDataInfo();
            if (this.mHistoryDataListener != null) {
                this.mHistoryDataListener.onHistoryDataReady(historyDataInfos);
            }
        }
        return null;
    }

    private void onTicker() {
        if (((Context) this.mContextRef.get()) != null) {
            Global.getGlobalHeartHandler().post(new C05721());
        }
    }
}
