package com.huami.watch.newsport.historydata.controller;

import android.content.Context;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.historydata.utils.DataTypeChangeHelper;
import com.huami.watch.newsport.sportcenter.OutdoorSportDelegate;
import com.huami.watch.newsport.sportcenter.action.ISportDelegate;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HeartHistoryDataManager extends AbsHistoryDataManager<HeartRate> {
    private final int BYTES_OF_BYTE = 1;
    private final int BYTES_OF_DOUBLE = 8;
    private final int BYTES_OF_FLOAT = 4;
    private final int BYTES_OF_INT = 4;
    private final int BYTES_OF_LONG = 8;
    private final int BYTES_OF_SHORT = 2;
    private OutdoorSportDelegate mDelegate = null;
    public int mID = 0;
    Map<Integer, KiloInfo> mKmPerInfoMap = new LinkedHashMap();
    private int mPosition = 0;

    private class KiloInfo {
        private float dailyPorpermence;
        private boolean isImperial;
        private int mKm;
        private long mLatitude;
        private long mLongitude;
        private float mPace;
        private float mSpeed;
        private long mTotalCostTime;
        private int teValue;

        private KiloInfo() {
            this.mKm = 0;
            this.mSpeed = 0.0f;
            this.mPace = 0.0f;
            this.mTotalCostTime = 0;
            this.mLatitude = 10000;
            this.mLongitude = 10000;
            this.teValue = 0;
            this.dailyPorpermence = 127.0f;
            this.isImperial = false;
        }

        public boolean isImperial() {
            return this.isImperial;
        }

        public void setImperial(boolean imperial) {
            this.isImperial = imperial;
        }

        public int getmKm() {
            return this.mKm;
        }

        public void setmKm(int mKm) {
            this.mKm = mKm;
        }

        public float getmSpeed() {
            return this.mSpeed;
        }

        public void setmSpeed(float mSpeed) {
            this.mSpeed = mSpeed;
        }

        public float getmPace() {
            return this.mPace;
        }

        public void setmPace(float mPace) {
            this.mPace = mPace;
        }

        public long getmTotalCostTime() {
            return this.mTotalCostTime;
        }

        public void setmTotalCostTime(long mTotalCostTime) {
            this.mTotalCostTime = mTotalCostTime;
        }

        public long getmLatitude() {
            return this.mLatitude;
        }

        public void setmLatitude(long mLatitude) {
            this.mLatitude = mLatitude;
        }

        public long getmLongitude() {
            return this.mLongitude;
        }

        public void setmLongitude(long mLongitude) {
            this.mLongitude = mLongitude;
        }

        public void setTeValue(int teValue) {
            this.teValue = teValue;
        }

        public void setDailyPorpermence(float dailyPorpermence) {
            this.dailyPorpermence = dailyPorpermence;
        }

        public String toString() {
            return "KiloInfo{mKm=" + this.mKm + ", mSpeed=" + this.mSpeed + ", mPace=" + this.mPace + ", mTotalCostTime=" + this.mTotalCostTime + ", mLatitude=" + this.mLatitude + ", mLongitude=" + this.mLongitude + ", teValue=" + this.teValue + ", dailyPorpermence=" + this.dailyPorpermence + ", isImperial=" + this.isImperial + '}';
        }
    }

    public HeartHistoryDataManager(Context context, long startTime, int sportType, int startIndex, ISportDelegate sportDelegate) {
        super(context, startTime, sportType);
        this.mID = startIndex;
        this.mDelegate = (OutdoorSportDelegate) sportDelegate;
        LogUtil.m9i(true, TAG, "curId:" + this.mID);
    }

    public void startListen() {
        Debug.m5i(TAG, "HeartHistoryDataManager start listen");
        super.startListen();
    }

    public List<HeartRate> stopListen() {
        Debug.m5i(TAG, "HeartHistoryDataManager stop listen");
        List<HeartRate> rates = super.stopListen();
        addKmInfo();
        return rates;
    }

    public void setCurrentStatus(SportSnapshot snapshot) {
        this.mCurrentStatus = (OutdoorSportSnapshot) snapshot;
    }

    public List<HeartRate> getHistoryDataInfo() {
        List<HeartRate> ret = new LinkedList();
        while (true) {
            List<HeartRate> heartRates = getHeartRateInfo();
            if (heartRates == null || heartRates.isEmpty()) {
                return ret;
            }
            LogUtil.m7d(Global.DEBUG_LEVEL_3, TAG, "get heart rate size " + heartRates.size());
            ret.addAll(heartRates);
        }
        return ret;
    }

    public long getIntervalTime() {
        return 3000;
    }

    public String getThreadName() {
        return "history-heart-thread";
    }

    private synchronized List<HeartRate> getHeartRateInfo() {
        List<HeartRate> list;
        Debug.m5i(TAG, "parseHeartRateInfo, START");
        byte[] rateList = this.mSensorHubConfigManager.getHeartHistoryData();
        Debug.m5i(TAG, "parseHeartRateInfo, list: " + rateList + ", startTime" + this.mStartTimestamp);
        if (rateList == null || rateList.length == 0) {
            list = null;
        } else {
            this.mPosition = 0;
            int size = rateList.length / 16;
            list = new ArrayList(size);
            float bestPace = 0.0f;
            float minPace = 0.0f;
            float maxStepFreq = -1.0f;
            for (int i = 0; i < size; i++) {
                HeartRate item = new HeartRate();
                if (isPerKmInfo(rateList, this.mPosition)) {
                    this.mPosition += 16;
                } else {
                    if (isCurSportHeartData(rateList, this.mPosition)) {
                        item.setTimestamp((1000 * DataTypeChangeHelper.unsigned3BytesToLong(rateList, this.mPosition)) + this.mStartTimestamp);
                        this.mPosition += 4;
                        float stepFreq = (float) DataTypeChangeHelper.unsigned2ByteToInt(rateList, this.mPosition);
                        if (SportType.isSwimMode(this.mSportType)) {
                            item.setStrokeSpeed(stepFreq / 60.0f);
                        } else {
                            item.setStepFreq(stepFreq / 60.0f);
                        }
                        this.mPosition += 2;
                        item.setAltitude(DataTypeChangeHelper.signed2ByteToInt(rateList, this.mPosition));
                        this.mPosition += 2;
                        item.setPace(((float) DataTypeChangeHelper.unsigned2ByteToInt(rateList, this.mPosition)) / 1000.0f);
                        this.mPosition += 2;
                        item.setHeartRate(rateList[this.mPosition] & 255);
                        this.mPosition++;
                        item.setHeartQuality(rateList[this.mPosition] & 31);
                        item.setHeartRange((rateList[this.mPosition] & 255) >> 5);
                        this.mPosition++;
                        int i2 = this.mPosition;
                        this.mPosition = i2 + 1;
                        item.setStride(rateList[i2] & 255);
                        i2 = this.mPosition;
                        this.mPosition = i2 + 1;
                        item.setStepDiff(rateList[i2] & 255);
                        this.mPosition++;
                        item.setDistance((float) (rateList[this.mPosition] & 255));
                        this.mPosition++;
                        list.add(item);
                        if (Global.DEBUG_LEVEL_3) {
                            Debug.m3d(TAG, "data: " + item.toString() + ", " + ((item.getTimestamp() - this.mStartTimestamp) / 1000));
                        }
                        if ((Math.abs(bestPace) > 1.0E-6f && item.getPace() < bestPace && Math.abs(item.getPace()) > 1.0E-6f) || Math.abs(bestPace) <= 1.0E-6f) {
                            bestPace = item.getPace();
                        }
                        if ((Math.abs(minPace) > 1.0E-6f && item.getPace() > minPace && Math.abs(item.getPace()) > 1.0E-6f) || Math.abs(minPace) <= 1.0E-6f) {
                            minPace = item.getPace();
                        }
                        if (item.getStepFreq() > maxStepFreq) {
                            maxStepFreq = item.getStepFreq();
                        }
                    } else {
                        this.mPosition += 16;
                    }
                }
            }
            if (this.mCurrentStatus == null) {
                Debug.m3d(TAG, "current status is null");
            } else {
                float currentBestPace = this.mCurrentStatus.getBestPace();
                if (currentBestPace < 0.0f || Math.abs(currentBestPace) <= 1.0E-6f) {
                    currentBestPace = bestPace;
                } else if (currentBestPace > bestPace && Math.abs(bestPace) > 1.0E-6f) {
                    currentBestPace = bestPace;
                }
                this.mCurrentStatus.setBestPace(currentBestPace);
                float currentMinPace = this.mCurrentStatus.getMinPace();
                if (currentMinPace < 0.0f || Math.abs(currentMinPace) <= 1.0E-6f) {
                    currentMinPace = minPace;
                } else if (currentMinPace < minPace && Math.abs(minPace) > 1.0E-6f) {
                    currentMinPace = minPace;
                }
                this.mCurrentStatus.setMinPace(currentMinPace);
                if (this.mCurrentStatus.getMaxStepFreq() < maxStepFreq) {
                    this.mCurrentStatus.setMaxStepFreq(maxStepFreq);
                }
            }
            Debug.m5i(TAG, "parseHeartRateInfo, end");
        }
        return list;
    }

    private boolean isCurSportHeartData(byte[] rateBytes, int startPosition) {
        if (rateBytes == null || rateBytes.length < startPosition + 4) {
            LogUtil.m9i(true, TAG, "isCurSportHeartData, wrong bytes length return 0");
            return false;
        }
        byte highTimestamps = rateBytes[startPosition + 3];
        if (highTimestamps == (byte) 0 || highTimestamps == this.mID) {
            return true;
        }
        LogUtil.m9i(true, TAG, "isCurSportHeartData, not, high byte:" + highTimestamps + ", cur id:" + this.mID);
        return false;
    }

    private void addKmInfo() {
        for (Entry<Integer, KiloInfo> entry : this.mKmPerInfoMap.entrySet()) {
            LogUtil.m9i(true, TAG, "key:" + entry.getKey() + ", value:" + ((KiloInfo) entry.getValue()).toString());
            KiloInfo curKilo = (KiloInfo) entry.getValue();
            if (this.mDelegate != null) {
                this.mDelegate.addKmInfo(curKilo.getmKm(), curKilo.getmPace(), curKilo.getmSpeed(), curKilo.isImperial(), curKilo.teValue, curKilo.dailyPorpermence, new long[]{0, curKilo.getmLongitude(), curKilo.getmLatitude()}, curKilo.getmTotalCostTime());
            }
        }
    }

    private boolean isPerKmInfo(byte[] rateBytes, int startPosition) {
        if (rateBytes == null || rateBytes.length < startPosition + 4) {
            LogUtil.m9i(true, TAG, "isPerKmInfo, wrong bytes length return 0");
            return false;
        }
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "3:" + rateBytes[startPosition + 3] + ", 2:" + rateBytes[startPosition + 2] + ", 0:" + startPosition);
        if ((rateBytes[startPosition + 3] & 255) != 240 || (rateBytes[startPosition + 2] & 255) != 240) {
            return false;
        }
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "is PerKmInfo");
        int km = Math.round(((float) DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 4)) / 1000.0f);
        KiloInfo runningInfoPerKM = (KiloInfo) this.mKmPerInfoMap.get(Integer.valueOf(km));
        if (runningInfoPerKM == null) {
            HeartHistoryDataManager heartHistoryDataManager = this;
            runningInfoPerKM = new KiloInfo();
            runningInfoPerKM.setmKm(km);
            this.mKmPerInfoMap.put(Integer.valueOf(km), runningInfoPerKM);
        }
        if (rateBytes[startPosition] == (byte) 1) {
            LogUtil.m9i(true, TAG, "km:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 4) + ", pace:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 8) + ", lastAveSpeed:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 12));
            float avePace = ((float) DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 8)) / 1000.0f;
            runningInfoPerKM.setmSpeed(((float) DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 12)) / 1000.0f);
            runningInfoPerKM.setmPace(avePace);
        } else if (rateBytes[startPosition] == (byte) 2) {
            LogUtil.m9i(true, TAG, "km:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 4) + ", Te:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 8) + ", dailyPerformance:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 12));
            float dailyPerformance = ((float) DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 12)) / 10.0f;
            runningInfoPerKM.setTeValue(DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 8));
            runningInfoPerKM.setDailyPorpermence(dailyPerformance);
        } else if (rateBytes[startPosition] == (byte) 3) {
            LogUtil.m9i(true, TAG, "km:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 4) + ", lng:" + DataTypeChangeHelper.unsigned4BytesToLong(rateBytes, startPosition + 8) + ", lat:" + DataTypeChangeHelper.unsigned4BytesToLong(rateBytes, startPosition + 12));
            long lng = DataTypeChangeHelper.unsigned4BytesToLong(rateBytes, startPosition + 8);
            runningInfoPerKM.setmLatitude(DataTypeChangeHelper.unsigned4BytesToLong(rateBytes, startPosition + 12));
            runningInfoPerKM.setmLongitude(lng);
        } else {
            LogUtil.m9i(true, TAG, "km:" + DataTypeChangeHelper.unsigned4BytesToInt(rateBytes, startPosition + 4) + ", time_stamp:" + DataTypeChangeHelper.unsigned3BytesToLong(rateBytes, startPosition + 8));
            long sec = DataTypeChangeHelper.unsigned3BytesToLong(rateBytes, startPosition + 8);
            boolean isImperial = (rateBytes[startPosition + 11] & 255) == 1;
            runningInfoPerKM.setmTotalCostTime((1000 * sec) + this.mStartTimestamp);
            runningInfoPerKM.setImperial(isImperial);
        }
        return true;
    }
}
