package com.huami.watch.newsport.historydata.controller;

import android.content.Context;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.sensor.HmGpsLocation;
import java.util.List;

public class GPSHistoryDataManager extends AbsHistoryDataManager<HmGpsLocation> {
    public GPSHistoryDataManager(Context context, long startTime, int sportType) {
        super(context, startTime, sportType);
    }

    public List<HmGpsLocation> getHistoryDataInfo() {
        List<HmGpsLocation> locations = this.mSensorHubConfigManager.getAllGpsLocation();
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "get location from sensor : " + locations);
        }
        return locations;
    }

    public void startListen() {
        Debug.m5i(TAG, "GPSHistoryDataManager start listen");
        super.startListen();
    }

    public List<HmGpsLocation> stopListen() {
        Debug.m5i(TAG, "GPSHistoryDataManager stop listen");
        return super.stopListen();
    }

    public long getIntervalTime() {
        return 2000;
    }

    public String getThreadName() {
        return "history-gps-thread";
    }
}
