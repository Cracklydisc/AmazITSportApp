package com.huami.watch.newsport.historydata.utils;

import android.util.Log;
import com.huami.watch.newsport.Global;

public class DataTypeChangeHelper {
    public static long unsigned4BytesToLong(byte[] bytes, int startIndex) {
        if (bytes == null || bytes.length < startIndex + 4) {
            Log.w("DataTypeChangeHelper", "wrong bytes length return 0");
            return 0;
        }
        if (Global.DEBUG_LEVEL_3) {
            for (int i = 0; i < bytes.length; i++) {
                Log.d("DataTypeChangeHelper", "Byte " + i + " value:" + bytes[i]);
            }
        }
        return ((((((long) bytes[startIndex + 3]) & 255) << 24) | ((((long) bytes[startIndex + 2]) & 255) << 16)) | ((((long) bytes[startIndex + 1]) & 255) << 8)) | ((((long) bytes[startIndex]) & 255) << null);
    }

    public static long unsigned3BytesToLong(byte[] bytes, int startIndex) {
        if (bytes == null || bytes.length < startIndex + 4) {
            Log.w("DataTypeChangeHelper", "wrong bytes length return 0");
            return 0;
        }
        if (Global.DEBUG_LEVEL_3) {
            for (int i = 0; i < bytes.length; i++) {
                Log.d("DataTypeChangeHelper", "Byte " + i + " value:" + bytes[i]);
            }
        }
        return (((((long) bytes[startIndex + 2]) & 255) << 16) | ((((long) bytes[startIndex + 1]) & 255) << 8)) | ((((long) bytes[startIndex]) & 255) << null);
    }

    public static int unsigned4BytesToInt(byte[] bytes, int startIndex) {
        if (bytes != null && bytes.length >= startIndex + 4) {
            return ((((bytes[startIndex + 3] & 255) << 24) | ((bytes[startIndex + 2] & 255) << 16)) | ((bytes[startIndex + 1] & 255) << 8)) | ((bytes[startIndex] & 255) << 0);
        }
        Log.w("DataTypeChangeHelper", "wrong bytes length return 0");
        return 0;
    }

    public static int unsigned2ByteToInt(byte[] bytes, int startIndex) {
        if (bytes != null && bytes.length >= startIndex + 2) {
            return ((bytes[startIndex + 1] & 255) << 8) | ((bytes[startIndex] & 255) << 0);
        }
        Log.w("DataTypeChangeHelper", "wrong bytes length return 0");
        return 0;
    }

    public static int signed2ByteToInt(byte[] bytes, int startIndex) {
        if (bytes != null && bytes.length >= startIndex + 2) {
            return (bytes[startIndex + 1] << 8) | (bytes[startIndex] & 255);
        }
        Log.w("DataTypeChangeHelper", "wrong bytes length return 0");
        return 0;
    }

    public static byte[] intTo4byte(int num) {
        return new byte[]{(byte) ((num >> 24) & 255), (byte) ((num >> 16) & 255), (byte) ((num >> 8) & 255), (byte) (num & 255)};
    }

    public static byte[] intTo4byteForReverse(int num) {
        return new byte[]{(byte) ((num >> 24) & 255), (byte) ((num >> 16) & 255), (byte) ((num >> 8) & 255), (byte) (num & 255)};
    }
}
