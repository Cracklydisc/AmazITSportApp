package com.huami.watch.newsport.historydata.listener;

import java.util.List;

public interface IHistoryDataListener<T> {
    void onHistoryDataReady(List<T> list);
}
