package com.huami.watch.newsport;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.huangheproduction.dbtransfer.HuangHeDBTransferManager;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.syncservice.SyncManager;
import com.huami.watch.newsport.syncservice.UploadCacheManager;
import com.huami.watch.newsport.syncservice.WifiSyncClient;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.TPUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.widget.SportHistoryWidget;
import com.huami.watch.newsport.widget.SportLauncherWidget;
import com.huami.watch.newsport.widget.SportWidget;
import com.huami.watch.newsport.widget.client.SportWidgetClient;
import com.huami.watch.newsport.xmlparser.utils.FileUtils;
import com.huami.watch.wearubc.UbcInterface;
import java.lang.Thread.UncaughtExceptionHandler;

public class SportApplication extends Application {
    public static boolean mIsMetric = true;
    public static boolean mIsUseNewUploadMethod = false;
    private static SportApplication sInstance = null;
    final int FEATURE_NOT_SUPPORT = 0;
    final int FEATURE_SUPPORT = 1;
    final String KEY_FEATURE_SUPPORT_NEW_DATASYNC_IOS = "feature_support_new_datasync_ios";
    private SportWidgetClient mHistoryWidgetClient = null;
    private ContentObserver mMesurementObserver = new ContentObserver(null) {
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Debug.m5i("SportApplication", "onchange");
            SportApplication.mIsMetric = SportApplication.getMeasurement(SportApplication.this) == 0;
            RecordGraphManager.getInstance(SportApplication.getInstance()).clearCache();
        }
    };
    private SportWidgetClient mSportStartClient = null;
    private ContentObserver mUploadObserver = new ContentObserver(null) {
        public void onChange(boolean selfChange) {
            boolean z = false;
            super.onChange(selfChange);
            Debug.m5i("SportApplication", "uploadObserver, changed");
            if (Secure.getInt(SportApplication.this.getContentResolver(), "feature_support_new_datasync_ios", 0) != 0) {
                z = true;
            }
            SportApplication.mIsUseNewUploadMethod = z;
            if (UnitConvertUtils.isUseNewUploadMethod(SportApplication.this)) {
                SyncManager.getInstance().initNewUploadClient();
            }
        }
    };
    public SportWidgetClient mWidgetClient = null;

    class C05331 implements UncaughtExceptionHandler {
        C05331() {
        }

        public void uncaughtException(Thread thread, Throwable throwable) {
            Debug.printException("ERR", throwable);
        }
    }

    class C05342 implements Runnable {
        C05342() {
        }

        public void run() {
            if (UnitConvertUtils.isHuangheMode()) {
                HuangHeDBTransferManager.getInstance(SportApplication.this).beginDBTransfer();
                FileUtils.copyShareFileFromSdCard(SportApplication.this);
            }
        }
    }

    public static final SportApplication getInstance() {
        return sInstance;
    }

    public void onCreate() {
        boolean z = true;
        super.onCreate();
        UbcInterface.initialize(this, "21", PointUtils.getApplicationVersion(this));
        sInstance = this;
        this.mWidgetClient = new SportWidgetClient(this, SportWidget.class);
        WifiSyncClient.newInstance(this);
        this.mHistoryWidgetClient = new SportWidgetClient(this, SportHistoryWidget.class);
        this.mSportStartClient = new SportWidgetClient(this, SportLauncherWidget.class);
        Debug.init(this);
        Debug.setTag("HmSport");
        if (Global.CATCH_EXCEPTION) {
            Thread.setDefaultUncaughtExceptionHandler(new C05331());
        }
        Global.init(this);
        if (DataManager.getInstance().isNeedClearSportDataCache(4)) {
            LogUtil.m9i(true, "SportApplication", "is need clear sport data cache");
            UploadCacheManager.getInstance(this).clearCache();
            DataManager.getInstance().setSportDataCacheVersion(4);
            LogUtil.m9i(true, "SportApplication", "clear success");
        }
        LogUtil.m9i(true, "SportApplication", "DEBUG_NEW_UPLOAD:" + Global.DEBUG_NEW_UPLOAD);
        mIsUseNewUploadMethod = Secure.getInt(getContentResolver(), "feature_support_new_datasync_ios", 0) != 0;
        getContentResolver().registerContentObserver(Secure.getUriFor("feature_support_new_datasync_ios"), false, this.mUploadObserver);
        if (UnitConvertUtils.isUseNewUploadMethod(this)) {
            SyncManager.getInstance().changeNotModifyData2NotSync();
            SyncManager.getInstance().triggerUploadSportData();
        } else {
            SyncManager.getInstance().startUploadNotModifyAndSyncSportDatas();
        }
        if (getMeasurement(this) != 0) {
            z = false;
        }
        mIsMetric = z;
        getContentResolver().registerContentObserver(Secure.getUriFor("measurement"), false, this.mMesurementObserver);
        Debug.m5i("SportApplication", "application created.");
        if (DataManager.getInstance().isInGPSearchingUI()) {
            notifySlptLeaveGps();
        }
        TPUtils.notifyKernelBgColor(0);
        Global.getGlobalHeartHandler().post(new C05342());
    }

    public static final int getMeasurement(Context c) {
        int m = 0;
        try {
            m = Secure.getInt(c.getContentResolver(), "measurement");
        } catch (SettingNotFoundException e) {
        }
        return m;
    }

    private void notifySlptLeaveGps() {
        Debug.m5i("SportApplication", "notifySlptLeaveGps");
        Intent intent = new Intent("com.huami.watchface.SlptWatchService.leave_gps");
        intent.putExtra("is_start_sport", false);
        sendBroadcast(intent);
        DataManager.getInstance().setIsInGPSearchingUI(false);
    }
}
