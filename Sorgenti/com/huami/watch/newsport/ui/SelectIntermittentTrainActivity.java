package com.huami.watch.newsport.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.support.wearable.view.WearableListView.Adapter;
import android.support.wearable.view.WearableListView.OnScrollListener;
import android.support.wearable.view.WearableListView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmRelativeLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.common.widget.HmWearableListView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.customtrain.utils.TrainSpUtils;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.train.model.TrainConfig;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.utils.IntertelRunDataParse;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.xmlparser.utils.FileUtils;
import java.util.List;

public class SelectIntermittentTrainActivity extends BaseActivity implements EventCallBack {
    private static final String TAG = SelectIntermittentTrainActivity.class.getSimpleName();
    boolean currentGPSState = false;
    private String[] files = null;
    private HmImageView goNextBtn;
    private GpsStatusReceiver gpsStatusReceiver;
    private HmTextView gpsStatusView;
    private HmRelativeLayout intervalRunDataContainer;
    private HmTextView intervalRunTitleView;
    private ImageView mGpsIcon;
    private AnimationDrawable mSearchAnimationDrawable = null;
    private int mSportType = -1;
    private ColumnScrollListener mWearColumnScrollListener;
    private HmRelativeLayout noDataContainer;
    private WearAdapter wearAdapter;
    private HmWearableListView wearableListView;

    class C06941 implements OnClickListener {
        C06941() {
        }

        public void onClick(View v) {
            if (SelectIntermittentTrainActivity.this.files != null && SelectIntermittentTrainActivity.this.files.length != 0) {
                Log.i(SelectIntermittentTrainActivity.TAG, " mCenterIndex:" + SelectIntermittentTrainActivity.this.mWearColumnScrollListener.mCenterIndex);
                final int position = SelectIntermittentTrainActivity.this.mWearColumnScrollListener.mCenterIndex;
                Global.getGlobalWorkHandler().post(new Runnable() {
                    public void run() {
                        TrainSpUtils.setCustomIntervttentTrain(SelectIntermittentTrainActivity.this, SelectIntermittentTrainActivity.this.files[position]);
                        final TrainProgram trainProgram = IntertelRunDataParse.getInstance().parsetJson(SelectIntermittentTrainActivity.this);
                        if (UnitConvertUtils.isHuangheMode()) {
                            for (TrainUnit trainUnit : trainProgram.getTrainUnitList()) {
                                if (TrainTargetType.KEYPRESS.getType() == trainUnit.getUnitType().getType()) {
                                    Log.i(SelectIntermittentTrainActivity.TAG, " currentIntervalType is Not Support KeyPress");
                                    return;
                                }
                            }
                        }
                        final int firstTrainUnit = ((TrainUnit) trainProgram.getTrainUnitList().get(0)).getUnitType().getType();
                        List<TrainConfig> trainConfigList = ((TrainUnit) trainProgram.getTrainUnitList().get(0)).getTrainConfigList();
                        int firstRemindType = TrainRemindType.NONE.getType();
                        if (!(trainConfigList == null || trainConfigList.size() <= 0 || trainConfigList.get(0) == null)) {
                            firstRemindType = ((TrainConfig) trainConfigList.get(0)).getTrainRemindType().getType();
                        }
                        final int finalFirstRemindType = firstRemindType;
                        Global.getGlobalUIHandler().post(new Runnable() {
                            public void run() {
                                Intent intent = new Intent();
                                intent.putExtra("is_start_sport", true);
                                intent.putExtra("sport_type", SelectIntermittentTrainActivity.this.mSportType);
                                intent.putExtra("sport_gps_status", 2);
                                intent.putExtra("entrance_type", -1);
                                intent.putExtra("intermittent_train_type", firstTrainUnit);
                                intent.putExtra("intermittent_remind_type", finalFirstRemindType);
                                intent.putExtra("only_single_train_type", trainProgram.isOnlySingleTrainType());
                                SelectIntermittentTrainActivity.this.setResult(-1, intent);
                                SelectIntermittentTrainActivity.this.finish();
                            }
                        });
                    }
                });
            }
        }
    }

    class C06972 implements Runnable {

        class C06951 implements Runnable {
            C06951() {
            }

            public void run() {
                SelectIntermittentTrainActivity.this.intervalRunTitleView.setText(SelectIntermittentTrainActivity.this.getResources().getString(C0532R.string.run_train_intermittent_select_train));
                SelectIntermittentTrainActivity.this.intervalRunDataContainer.setVisibility(8);
                SelectIntermittentTrainActivity.this.noDataContainer.setVisibility(0);
            }
        }

        class C06962 implements Runnable {
            C06962() {
            }

            public void run() {
                SelectIntermittentTrainActivity.this.intervalRunTitleView.setText(SelectIntermittentTrainActivity.this.getResources().getString(C0532R.string.run_train_intermittent));
                SelectIntermittentTrainActivity.this.intervalRunDataContainer.setVisibility(0);
                SelectIntermittentTrainActivity.this.noDataContainer.setVisibility(8);
                SelectIntermittentTrainActivity.this.wearAdapter = new WearAdapter(SelectIntermittentTrainActivity.this, SelectIntermittentTrainActivity.this.files);
                SelectIntermittentTrainActivity.this.wearableListView.setAdapter(SelectIntermittentTrainActivity.this.wearAdapter);
                SelectIntermittentTrainActivity.this.wearAdapter.notifyDataSetChanged();
            }
        }

        C06972() {
        }

        public void run() {
            FileUtils.clearIntervalRunFiles(SelectIntermittentTrainActivity.this);
            DataManager.getInstance().getIntervalRunData();
            SelectIntermittentTrainActivity.this.files = FileUtils.getFileIntervalTrain(SelectIntermittentTrainActivity.this);
            if (SelectIntermittentTrainActivity.this.files == null || SelectIntermittentTrainActivity.this.files.length == 0) {
                Global.getGlobalUIHandler().post(new C06951());
            } else {
                Global.getGlobalUIHandler().post(new C06962());
            }
        }
    }

    private class ColumnScrollListener implements OnScrollListener {
        private boolean centerHasCalled;
        private int mCenterIndex;
        private int mCenterPositon;
        private int mItemHeight;
        private WearableListView mListView;

        private ColumnScrollListener(WearableListView listView) {
            this.mCenterPositon = 0;
            this.mItemHeight = 0;
            this.mCenterIndex = 0;
            this.centerHasCalled = false;
            this.mListView = listView;
        }

        public void setCenterInfo(int centertIndex) {
            if (!this.centerHasCalled) {
                this.mCenterIndex = centertIndex;
                this.mCenterPositon = getCenterYPos(this.mListView);
                this.mItemHeight = getItemHeight();
            }
        }

        private int getCenterYPos(View view) {
            return (view.getTop() + view.getPaddingTop()) + (((view.getHeight() - view.getPaddingTop()) - view.getPaddingBottom()) / 2);
        }

        private int getItemHeight() {
            return ((this.mListView.getHeight() - this.mListView.getPaddingBottom()) - this.mListView.getPaddingTop()) / 3;
        }

        public void onScroll(int i) {
        }

        private float getScale(float perctent) {
            return 1.0f - (0.39999998f * perctent);
        }

        public void onAbsoluteScrollChange(int j) {
            Log.i(SelectIntermittentTrainActivity.TAG, "onAbsoluteScrollChange Index:" + j);
            int childCount = this.mListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = this.mListView.getChildAt(i);
                MyViewHolder holder = (MyViewHolder) this.mListView.getChildViewHolder(child);
                holder.setTextViewColor(this.mCenterIndex == holder.getLayoutPosition());
                float scale = getScale(Math.min((((float) Math.abs(getCenterYPos(child) - this.mCenterPositon)) * 1.0f) / ((float) this.mItemHeight), 1.0f));
                child.setScaleX(scale);
                child.setScaleY(scale);
            }
        }

        public void onScrollStateChanged(int i) {
            Log.i(SelectIntermittentTrainActivity.TAG, "onScrollStateChanged Index:" + i);
        }

        public void onCentralPositionChanged(int i) {
            Log.i(SelectIntermittentTrainActivity.TAG, "onCentralPositionChanged Index:" + i);
            this.centerHasCalled = true;
            this.mCenterIndex = i;
            this.mCenterPositon = getCenterYPos(this.mListView);
            this.mItemHeight = getItemHeight();
        }
    }

    private class GpsStatusReceiver extends BroadcastReceiver {
        private GpsStatusReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.location.PROVIDERS_CHANGED")) {
                Log.i(SelectIntermittentTrainActivity.TAG, " currnetGPSStatus:" + SelectIntermittentTrainActivity.this.currentGPSState);
                SelectIntermittentTrainActivity.this.updateGpsStatus(SelectIntermittentTrainActivity.this.getGPSState(SelectIntermittentTrainActivity.this));
                if (SelectIntermittentTrainActivity.this.getGPSState(SelectIntermittentTrainActivity.this)) {
                    SelectIntermittentTrainActivity.this.mGpsIcon.clearAnimation();
                    SelectIntermittentTrainActivity.this.mSearchAnimationDrawable.stop();
                    SelectIntermittentTrainActivity.this.mSearchAnimationDrawable.selectDrawable(0);
                    SelectIntermittentTrainActivity.this.mGpsIcon.setBackgroundResource(C0532R.drawable.sport_gps_ready);
                }
            }
        }
    }

    private class MyViewHolder extends ViewHolder {
        private final int DEFAUT_MIN_ALPA = 163;
        private ColumnScrollListener columnScrollListener;
        private boolean hasInital = false;
        private boolean isCenter;
        private ViewGroup mContainer;
        private TextView mTextView;

        public MyViewHolder(View itemView, ColumnScrollListener mWearColumnScrollListener) {
            super(itemView);
            this.mContainer = (ViewGroup) itemView;
            this.columnScrollListener = mWearColumnScrollListener;
            if (this.mContainer.getChildAt(0) instanceof TextView) {
                this.mTextView = (TextView) this.mContainer.getChildAt(0);
            }
        }

        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (isCentralItem) {
                this.columnScrollListener.setCenterInfo(this.columnScrollListener.mCenterIndex);
            }
            this.isCenter = isCentralItem;
        }

        public void setTextViewColor(boolean isCenter) {
            int mColor;
            if (isCenter) {
                mColor = -1;
            } else {
                mColor = Color.argb(163, 255, 255, 255);
            }
            if (this.mTextView != null) {
                this.mTextView.setTextColor(mColor);
            }
        }

        public void setHolderViewScacle(boolean isCenter) {
            if (isCenter) {
                this.mContainer.setScaleX(0.8f);
                this.mContainer.setScaleY(0.8f);
            } else {
                this.mContainer.setScaleX(0.6f);
                this.mContainer.setScaleY(0.6f);
            }
            this.hasInital = true;
        }
    }

    private class WearAdapter extends Adapter {
        private String[] intervalTrainFiles;
        private LayoutInflater mInflater;

        public WearAdapter(Context mcontext, String[] files) {
            this.intervalTrainFiles = files;
            this.mInflater = LayoutInflater.from(mcontext);
        }

        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new MyViewHolder(this.mInflater.inflate(C0532R.layout.wear_item_interval, null), SelectIntermittentTrainActivity.this.mWearColumnScrollListener);
        }

        public void onBindViewHolder(ViewHolder viewHolder, int position) {
            boolean z = true;
            MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
            if (myViewHolder.mTextView != null) {
                myViewHolder.mTextView.setText(this.intervalTrainFiles[position].replace(".json", ""));
            }
            myViewHolder.mContainer.setTag(Integer.valueOf(position));
            myViewHolder.mContainer.setAlpha(1.0f);
            Log.i(SelectIntermittentTrainActivity.TAG, " currentScrollIndex:" + SelectIntermittentTrainActivity.this.mWearColumnScrollListener.mCenterIndex);
            myViewHolder.setTextViewColor(position == SelectIntermittentTrainActivity.this.mWearColumnScrollListener.mCenterIndex);
            if (position != SelectIntermittentTrainActivity.this.mWearColumnScrollListener.mCenterIndex) {
                z = false;
            }
            myViewHolder.setHolderViewScacle(z);
            myViewHolder.itemView.setTag(Integer.valueOf(position));
        }

        public int getItemCount() {
            return this.intervalTrainFiles.length;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("widget_settings_type", -1);
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_interval_go);
            this.intervalRunDataContainer = (HmRelativeLayout) findViewById(C0532R.id.interval_run_data_container);
            this.noDataContainer = (HmRelativeLayout) findViewById(C0532R.id.no_data_contrainer);
            this.intervalRunTitleView = (HmTextView) findViewById(C0532R.id.interval_run_title);
            this.mGpsIcon = (ImageView) findViewById(C0532R.id.waiting_gps_icon);
            this.mGpsIcon.setBackgroundResource(C0532R.drawable.waiting_gps_search);
            this.mSearchAnimationDrawable = (AnimationDrawable) this.mGpsIcon.getBackground();
            this.mGpsIcon.setBackgroundResource(C0532R.drawable.waiting_gps_search);
            this.wearableListView = (HmWearableListView) findViewById(C0532R.id.interval_sport_list);
            this.goNextBtn = (HmImageView) findViewById(C0532R.id.go_next_btn);
            this.gpsStatusView = (HmTextView) findViewById(C0532R.id.gps_status);
            this.gpsStatusView.setText(getResources().getString(C0532R.string.gps_searching));
            LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(this, C0532R.anim.layout_gps_list_item_anim));
            lac.setDelay(0.6f);
            lac.setOrder(0);
            this.wearableListView.setLayoutAnimation(lac);
            this.wearableListView.setFocusable(true);
            this.mWearColumnScrollListener = new ColumnScrollListener(this.wearableListView);
            this.wearableListView.addOnScrollListener(this.mWearColumnScrollListener);
            this.intervalRunTitleView.setText(getResources().getString(C0532R.string.run_train_intermittent_select_train));
            loadDataSet();
            setKeyEventListener(this);
            this.goNextBtn.setOnClickListener(new C06941());
            registerGPSStatus(this);
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    protected void onResume() {
        super.onResume();
        updateGpsStatus(getGPSState(this));
    }

    private void loadDataSet() {
        Global.getGlobalWorkHandler().post(new C06972());
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER && this.goNextBtn != null) {
            this.goNextBtn.performClick();
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public void registerGPSStatus(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.location.PROVIDERS_CHANGED");
        this.gpsStatusReceiver = new GpsStatusReceiver();
        context.registerReceiver(this.gpsStatusReceiver, filter);
    }

    private void unRegisternGPSStatus(Context context) {
        if (this.gpsStatusReceiver != null) {
            try {
                context.unregisterReceiver(this.gpsStatusReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean getGPSState(Context context) {
        return SensorHubManagerWrapper.getInstance(context).isGpsAvailable();
    }

    private void updateGpsStatus(boolean isAvailable) {
        this.goNextBtn.setBackgroundResource(isAvailable ? C0532R.drawable.sport_star_btn_ready_m : C0532R.drawable.sport_star_btn_search_m);
        this.gpsStatusView.setText(isAvailable ? getString(C0532R.string.gps_search_success) : getString(C0532R.string.gps_searching));
        if (isAvailable) {
            this.mGpsIcon.clearAnimation();
            this.mSearchAnimationDrawable.stop();
            this.mSearchAnimationDrawable.selectDrawable(0);
            this.mGpsIcon.setBackgroundResource(C0532R.drawable.sport_gps_ready);
            return;
        }
        this.mSearchAnimationDrawable.selectDrawable(0);
        this.mSearchAnimationDrawable.start();
    }

    protected void onStop() {
        super.onStop();
        unRegisternGPSStatus(this);
        this.currentGPSState = false;
        this.mGpsIcon.clearAnimation();
        this.mSearchAnimationDrawable.stop();
        this.mSearchAnimationDrawable.selectDrawable(0);
        this.mGpsIcon.setBackgroundResource(C0532R.drawable.sport_gps_ready);
    }
}
