package com.huami.watch.newsport.ui.fragps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.ConfigMenuAdapter;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;

public class SettingFragment extends BaseFragment {
    private static final String TAG = SettingFragment.class.getSimpleName();
    private BaseConfigMenuAdapter mAdapter = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        HmMenuView view = new HmMenuView(getActivity());
        this.mAdapter = new ConfigMenuAdapter(getActivity(), null, this.mSportType);
        view.setSimpleMenuAdapter(this.mAdapter);
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.mAdapter != null) {
            this.mAdapter.onDataChanged();
        }
    }
}
