package com.huami.watch.newsport.ui.fragps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.SelectIntermittentTrainActivity;
import com.huami.watch.newsport.ui.adapter.SelectIntermittentTrainAdapter;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;

public class SelectIntermittentTrainFragment extends BaseFragment {
    private static final String TAG = SelectIntermittentTrainActivity.class.getSimpleName();
    private SelectIntermittentTrainAdapter selectIntermittentTrainAdapter;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("fragment_sport_type")) {
            this.mSportType = getArguments().getInt("fragment_sport_type");
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        HmMenuView view = new HmMenuView(getActivity(), null, C0532R.string.run_train_title);
        this.selectIntermittentTrainAdapter = new SelectIntermittentTrainAdapter(getActivity(), null, this.mSportType);
        view.setSimpleMenuAdapter(this.selectIntermittentTrainAdapter);
        return view;
    }
}
