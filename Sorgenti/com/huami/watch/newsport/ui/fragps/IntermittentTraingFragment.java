package com.huami.watch.newsport.ui.fragps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.ui.adapter.IntermittentTrainAdapter;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;

public class IntermittentTraingFragment extends BaseFragment {
    private static final String TAG = IntermittentTraingFragment.class.getSimpleName();
    private IntermittentTrainAdapter intermittentTrainAdapter;
    private int mSportType = -1;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("fragment_sport_type")) {
            this.mSportType = getArguments().getInt("fragment_sport_type");
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        HmMenuView view = new HmMenuView(getActivity(), null, C0532R.string.run_train_title);
        loadSportThaInfo(view);
        return view;
    }

    private void loadSportThaInfo(final HmMenuView view) {
        Global.getGlobalWorkHandler().post(new Runnable() {

            class C07701 implements OnKlvpDataListener {
                C07701() {
                }

                public void onHealthDataReady(int i, float v) {
                }

                public void onSportDataReady(SportStatistics sportStatistics) {
                    Utils.printlnSportStatistics(sportStatistics);
                    if (sportStatistics == null || sportStatistics.mSportThaWorkout == null) {
                        IntermittentTraingFragment.this.initAdapter(view, false);
                        return;
                    }
                    int teValue = sportStatistics.mSportThaWorkout.getTrainingEffect();
                    int duration = sportStatistics.mSportThaWorkout.getDuration();
                    int distance = sportStatistics.mSportThaWorkout.getDistance();
                    Log.i(IntermittentTraingFragment.TAG, "teValue:" + teValue + ",duration:" + duration + ",distance:" + distance + ",phrase:" + sportStatistics.mSportThaWorkout.getPhrase());
                    if (teValue == 0 || duration == 0 || distance == 0) {
                        IntermittentTraingFragment.this.initAdapter(view, false);
                    } else {
                        IntermittentTraingFragment.this.initAdapter(view, true);
                    }
                }
            }

            public void run() {
                if (!IntermittentTraingFragment.this.isDetached() && IntermittentTraingFragment.this.isAdded()) {
                    HmSensorHubConfigManager.getHmSensorHubConfigManager(IntermittentTraingFragment.this.getActivity()).requestRealtimeData(2, new C07701());
                }
            }
        });
    }

    private void initAdapter(final HmMenuView view, final boolean isSupportRecommend) {
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (!IntermittentTraingFragment.this.isDetached() && IntermittentTraingFragment.this.isAdded()) {
                    Log.i(IntermittentTraingFragment.TAG, " intermittentTrainAdapter Status: isSupportRecommend:" + isSupportRecommend);
                    IntermittentTraingFragment.this.intermittentTrainAdapter = new IntermittentTrainAdapter(IntermittentTraingFragment.this.getActivity(), null, IntermittentTraingFragment.this.mSportType, isSupportRecommend);
                    view.setSimpleMenuAdapter(IntermittentTraingFragment.this.intermittentTrainAdapter);
                }
            }
        });
    }
}
