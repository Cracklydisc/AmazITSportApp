package com.huami.watch.newsport.ui.fragps;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.view.HmSportLayout;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnDismissedListener;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnSwipeProgressChangedListener;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class GPStatusFragment extends Fragment implements OnClickListener {
    private HmHaloButton mConfirmBtn = null;
    private HmTextView mContentView = null;
    private HmImageView mFailedImg = null;
    private int mFailedType = 0;
    private IGPSearchFailedListener mListener = null;
    private HmSportLayout mRootView = null;
    private HmTextView mTitleView = null;

    public interface IGPSearchFailedListener {
        void onGPSearchBtnClick(int i);
    }

    class C07681 implements OnSwipeProgressChangedListener {
        C07681() {
        }

        public void onSwipeProgressChanged(HmSportLayout view, float progress, float deltx) {
            if (deltx >= 0.0f && GPStatusFragment.this.mRootView.getVisibility() == 0) {
                GPStatusFragment.this.mRootView.setAlpha(1.0f - progress);
            }
        }

        public void onSwipeCancelled(HmSportLayout var1) {
        }
    }

    class C07692 implements OnDismissedListener {
        C07692() {
        }

        public void onDismissed(HmSportLayout var1) {
            if (GPStatusFragment.this.mListener != null) {
                GPStatusFragment.this.mListener.onGPSearchBtnClick(GPStatusFragment.this.mFailedType);
            }
            GPStatusFragment.this.mRootView.setVisibility(8);
        }
    }

    public static synchronized GPStatusFragment newInstance() {
        GPStatusFragment fragment;
        synchronized (GPStatusFragment.class) {
            fragment = new GPStatusFragment();
        }
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IGPSearchFailedListener) {
            this.mListener = (IGPSearchFailedListener) activity;
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_gps_failed_layout, container, false);
        this.mRootView = (HmSportLayout) root.findViewById(C0532R.id.root_view);
        this.mTitleView = (HmTextView) root.findViewById(C0532R.id.title);
        this.mContentView = (HmTextView) root.findViewById(C0532R.id.content);
        if (!UnitConvertUtils.isZh()) {
            LayoutParams params = (LayoutParams) this.mContentView.getLayoutParams();
            params.leftMargin = 20;
            params.rightMargin = 20;
        }
        this.mFailedImg = (HmImageView) root.findViewById(C0532R.id.failed_img);
        this.mConfirmBtn = (HmHaloButton) root.findViewById(C0532R.id.click_btn);
        this.mConfirmBtn.setWithHalo(false);
        this.mConfirmBtn.setOnClickListener(this);
        updateView();
        this.mRootView.setAppOnSwipeProgressChangedListener(new C07681());
        this.mRootView.setOnDismissedListener(new C07692());
        this.mRootView.setVisibility(0);
        return root;
    }

    public void setFailedType(int type) {
        this.mFailedType = type;
        updateView();
    }

    public int getGPStatusType() {
        return this.mFailedType;
    }

    private void updateView() {
        if (isDetached() || !isAdded()) {
            LogUtil.m9i(true, "KeyEventDispatcher", "updateView failed, isDetached:" + isDetached() + ", isAdded:" + isAdded());
        } else if (this.mConfirmBtn != null && this.mContentView != null) {
            this.mRootView.setAlpha(1.0f);
            this.mRootView.setVisibility(0);
            switch (this.mFailedType) {
                case 0:
                    changeBtnLayoutParams((int) TypedValue.applyDimension(1, 24.0f, getResources().getDisplayMetrics()));
                    this.mTitleView.setVisibility(0);
                    this.mFailedImg.setBackgroundResource(C0532R.drawable.sport_gps_img_lost);
                    this.mTitleView.setText(C0532R.string.gps_search_failed_title);
                    this.mContentView.setText(C0532R.string.gps_search_failed_content);
                    this.mConfirmBtn.setText(C0532R.string.confirm);
                    return;
                case 1:
                    changeBtnLayoutParams((int) TypedValue.applyDimension(1, 24.0f, getResources().getDisplayMetrics()));
                    this.mFailedImg.setBackgroundResource(C0532R.drawable.sport_gps_img_expiration);
                    this.mTitleView.setVisibility(8);
                    this.mContentView.setText(C0532R.string.gps_search_timeout_content);
                    this.mConfirmBtn.setText(C0532R.string.confirm);
                    return;
                case 2:
                    changeBtnLayoutParams((int) TypedValue.applyDimension(1, 24.0f, getResources().getDisplayMetrics()));
                    this.mTitleView.setVisibility(0);
                    this.mFailedImg.setBackgroundResource(C0532R.drawable.sport_gps_img_ready);
                    this.mTitleView.setText(C0532R.string.gps_search_success_title);
                    this.mContentView.setText(C0532R.string.gps_search_success_content);
                    this.mConfirmBtn.setText(C0532R.string.confirm);
                    return;
                case 3:
                    changeBtnLayoutParams((int) TypedValue.applyDimension(1, 24.0f, getResources().getDisplayMetrics()));
                    this.mFailedImg.setBackgroundResource(C0532R.drawable.sport_gps_img_lost);
                    this.mTitleView.setVisibility(8);
                    this.mContentView.setText(C0532R.string.gps_search_time_out_content);
                    this.mConfirmBtn.setText(C0532R.string.confirm);
                    return;
                case 4:
                    changeBtnLayoutParams((int) TypedValue.applyDimension(1, 10.0f, getResources().getDisplayMetrics()));
                    this.mTitleView.setVisibility(0);
                    this.mFailedImg.setBackgroundResource(C0532R.drawable.sport_notify_icon_gps);
                    this.mTitleView.setText(getString(C0532R.string.gps_close_notification_title));
                    this.mContentView.setText(C0532R.string.gps_close_notification_content);
                    this.mConfirmBtn.setText(C0532R.string.confirm);
                    return;
                default:
                    return;
            }
        }
    }

    private void changeBtnLayoutParams(int marginbottom) {
        if (this.mConfirmBtn != null) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) this.mConfirmBtn.getLayoutParams();
            params.bottomMargin = marginbottom;
            this.mConfirmBtn.setLayoutParams(params);
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onClick(View v) {
        if (v.getId() == C0532R.id.click_btn && this.mListener != null) {
            this.mListener.onGPSearchBtnClick(this.mFailedType);
        }
    }
}
