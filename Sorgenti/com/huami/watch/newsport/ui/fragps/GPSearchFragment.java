package com.huami.watch.newsport.ui.fragps;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.wearable.view.WearableListView;
import android.support.wearable.view.WearableListView.Adapter;
import android.support.wearable.view.WearableListView.ClickListener;
import android.support.wearable.view.WearableListView.OnScrollListener;
import android.support.wearable.view.WearableListView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmEcgProgressBar;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.historydata.utils.DataTypeChangeHelper;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.sportcenter.utils.AGPSManager;
import com.huami.watch.newsport.timeticker.action.BaseTicker;
import com.huami.watch.newsport.timeticker.listener.ITickerListener;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.PowerUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.List;

public class GPSearchFragment extends BaseFragment implements OnClickListener {
    private static final long[] GPS_AVAILABLE_VIBRATOR_TIME_PATTERN = new long[]{0, 500};
    public static final String TAG = GPSearchFragment.class.getName();
    private final int DEFAUT_MIN_ALPA = 163;
    private volatile boolean isShowSportGuide = true;
    private AlarmManager mAlarmManager = null;
    private int mCurrentStatus = 2;
    private View mDownFlagView = null;
    private boolean mFirstSearchGps = true;
    private long mGPSearchTime = -1;
    private View mGpsContainer = null;
    private ImageView mGpsIcon;
    private final BroadcastReceiver mGpsReceiver = new C07679();
    private UIHandler mHandler = null;
    private ImageView mHeartImg = null;
    private TextView mHeartValueView = null;
    private SensorHubManagerWrapper mHmSensorHubConfigManager = null;
    private boolean mIsAgpsExpiredShown = false;
    private boolean mIsClosedGps = false;
    private boolean mIsFirstCreated = true;
    private boolean mIsStopRequestHeartData = true;
    private boolean mIsTimeout = false;
    private OnKlvpDataListener mKlvpDataListener = new OnKlvpDataListener() {
        public void onHealthDataReady(int i, float v) {
        }

        public void onSportDataReady(SportStatistics sportData) {
            if (sportData == null) {
                Debug.m6w(GPSearchFragment.TAG, "sport data is null");
                return;
            }
            byte[] heartByte = DataTypeChangeHelper.intTo4byte(sportData.mRealtimeAvg.getMHeartRate());
            int heartRate = heartByte[3] & 255;
            int heartQuantity = heartByte[2] & 31;
            LogUtil.m9i(true, GPSearchFragment.TAG, "test heart rate " + heartRate + ", heartQuantity " + heartQuantity);
            GPSearchFragment.this.updateHeartValue(heartQuantity, heartRate);
        }
    };
    private IGPSearchListener mListener;
    private View mMainContainer = null;
    private HmEcgProgressBar mProgressView = null;
    private final BroadcastReceiver mReceiver = new C07668();
    private AnimationDrawable mSearchAnimationDrawable = null;
    private boolean mSearchGPSSuccess = false;
    private SensorHubManagerWrapper mSensorHubManagerWrapper = null;
    private BaseTicker mTicker = null;
    private ITickerListener mTimeListener = new ITickerListener() {
        public void onTicker(int millSecond) {
            GPSearchFragment.this.getSensorManager().requestRealtimeData(GPSearchFragment.this.mKlvpDataListener);
        }
    };
    private HmTextView mTitleView;
    private Vibrator mVibrator = null;
    private HmTextView mWaitingGpsTextView;
    private WakeLock mWakeLock = null;
    private ColumnScrollListener mWearColumnScrollListener = null;
    private WearableListView mWearListView = null;
    private WearAdapter wearAdapter;

    public interface IGPSearchListener {
        BaseConfig getSportConfig();

        boolean isSportStart();

        void onAgpsExpired();

        void onFirstExperienceStartSport();

        void onGPSClosedNotificationShow();

        void onGpsChanged(int i);

        void onGpsFoundSuccess();

        void onGpsTimeOut(boolean z);

        void onItemClick(int i, boolean z);

        void startSport(int i);
    }

    class C07591 implements Runnable {

        class C07581 implements ClickListener {
            C07581() {
            }

            public void onClick(ViewHolder viewHolder) {
                Log.i(GPSearchFragment.TAG, " onItemClickPos:" + viewHolder.getLayoutPosition());
                if (GPSearchFragment.this.mListener != null) {
                    GPSearchFragment.this.mListener.onItemClick(viewHolder.getLayoutPosition(), SportType.isNeedSportTargetRecommand(GPSearchFragment.this.mSportType));
                }
            }

            public void onTopEmptyRegionClick() {
            }
        }

        C07591() {
        }

        public void run() {
            if (!GPSearchFragment.this.isDetached() && GPSearchFragment.this.isAdded()) {
                LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(GPSearchFragment.this.getActivity(), C0532R.anim.layout_gps_list_item_anim));
                lac.setDelay(0.6f);
                lac.setOrder(0);
                GPSearchFragment.this.mWearListView.setLayoutAnimation(lac);
                GPSearchFragment.this.mWearColumnScrollListener = new ColumnScrollListener(GPSearchFragment.this.mWearListView);
                GPSearchFragment.this.mWearListView.addOnScrollListener(GPSearchFragment.this.mWearColumnScrollListener);
                GPSearchFragment.this.mWearListView.setAdapter(GPSearchFragment.this.wearAdapter);
                GPSearchFragment.this.mWearListView.setClickListener(new C07581());
            }
        }
    }

    class C07602 implements Runnable {
        C07602() {
        }

        public void run() {
            if (!GPSearchFragment.this.isDetached() && GPSearchFragment.this.isAdded()) {
                GPSearchFragment.this.mDownFlagView.setVisibility(0);
                GPSearchFragment.this.mDownFlagView.startAnimation(AnimationUtils.loadAnimation(GPSearchFragment.this.getActivity(), C0532R.anim.alpha_in));
            }
        }
    }

    class C07613 implements Runnable {
        C07613() {
        }

        public void run() {
            Debug.m5i(GPSearchFragment.TAG, "gps available, release wake lock");
            GPSearchFragment.this.releaseWakeLock();
            ((PowerManager) GPSearchFragment.this.getActivity().getSystemService("power")).userActivity(SystemClock.uptimeMillis(), 0, 1);
        }
    }

    class C07624 implements Runnable {
        C07624() {
        }

        public void run() {
            GPSearchFragment.this.mHandler.sendEmptyMessage(0);
        }
    }

    class C07635 implements Runnable {
        C07635() {
        }

        public void run() {
            if (!GPSearchFragment.this.isDetached() && GPSearchFragment.this.isAdded()) {
                Debug.m5i(GPSearchFragment.TAG, "----------------start gps-----------------");
                GPSearchFragment.this.getSensorManager().startBunchModeGPS();
                GPSearchFragment.this.registeGPSReceiver();
            }
        }
    }

    class C07646 implements Runnable {
        C07646() {
        }

        public void run() {
            Debug.m5i(GPSearchFragment.TAG, "----------------stop gps-----------------");
            GPSearchFragment.this.getSensorManager().stopBunchModeGPS();
            GPSearchFragment.this.unRegisteGPSReceiver();
        }
    }

    class C07668 extends BroadcastReceiver {
        C07668() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("sensorhub.gps.state_change".equals(intent.getStringExtra("WAKEUP_SOURCE"))) {
                String gpsStatusText = intent.getStringExtra("GPS_STATE");
                Debug.m6w(GPSearchFragment.TAG, "onReceive, gpsStatusText: " + gpsStatusText);
                if ("1".equals(gpsStatusText)) {
                    GPSearchFragment.this.onGpsStatusChanged(1);
                } else {
                    GPSearchFragment.this.onGpsStatusChanged(2);
                }
            }
        }
    }

    class C07679 extends BroadcastReceiver {
        C07679() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Debug.m5i(GPSearchFragment.TAG, "mGpsReceiver, intent is null");
                return;
            }
            String action = intent.getAction();
            Debug.m5i(GPSearchFragment.TAG, "mGpsReceiver, receive gps close intent: " + action);
            if ("com.huami.watch.sport.action.CLOSE_GPS".equals(action)) {
                GPSearchFragment.this.onSearchGPSFailed(true);
                PowerUtils.wakeupForMilliSecond(GPSearchFragment.this.getActivity(), 2000);
            } else if ("com.huami.watch.sport.action.CLOSE_HEART".equals(action)) {
                SensorHubManagerWrapper.getInstance(GPSearchFragment.this.getActivity()).notifySensorCloseHeartRate(null);
            } else if ("com.huami.watch.sport.action.CLOSE_GPS_NOTIFY".equals(action)) {
                if (GPSearchFragment.this.mListener != null) {
                    GPSearchFragment.this.mListener.onGPSClosedNotificationShow();
                }
                PowerUtils.wakeupForMilliSecond(GPSearchFragment.this.getActivity(), 2000);
            }
        }
    }

    private class ColumnScrollListener implements OnScrollListener {
        private boolean centerHasCalled;
        private int mCenterIndex;
        private int mCenterPositon;
        private int mItemHeight;
        private WearableListView mListView;

        private ColumnScrollListener(WearableListView listView) {
            this.mCenterPositon = 0;
            this.mItemHeight = 0;
            this.mCenterIndex = 0;
            this.centerHasCalled = false;
            this.mListView = listView;
        }

        public void setCenterInfo(int centertIndex) {
            if (!this.centerHasCalled) {
                this.mCenterIndex = centertIndex;
                this.mCenterPositon = getCenterYPos(this.mListView);
                this.mItemHeight = getItemHeight();
            }
        }

        private int getCenterYPos(View view) {
            return (view.getTop() + view.getPaddingTop()) + (((view.getHeight() - view.getPaddingTop()) - view.getPaddingBottom()) / 2);
        }

        private int getItemHeight() {
            return ((this.mListView.getHeight() - this.mListView.getPaddingBottom()) - this.mListView.getPaddingTop()) / 3;
        }

        public void onScroll(int i) {
        }

        private float getScale(float perctent) {
            return 1.0f - (0.39999998f * perctent);
        }

        public void onAbsoluteScrollChange(int j) {
            int childCount = this.mListView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = this.mListView.getChildAt(i);
                MyViewHolder holder = (MyViewHolder) this.mListView.getChildViewHolder(child);
                holder.setTextViewColor(this.mCenterIndex == holder.getLayoutPosition());
                float scale = getScale(Math.min((((float) Math.abs(getCenterYPos(child) - this.mCenterPositon)) * 1.0f) / ((float) this.mItemHeight), 1.0f));
                child.setScaleX(scale);
                child.setScaleY(scale);
            }
        }

        public void onScrollStateChanged(int i) {
        }

        public void onCentralPositionChanged(int i) {
            this.centerHasCalled = true;
            this.mCenterIndex = i;
            this.mCenterPositon = getCenterYPos(this.mListView);
            this.mItemHeight = getItemHeight();
            if (GPSearchFragment.this.wearAdapter != null && GPSearchFragment.this.mDownFlagView != null) {
                if (i == 0) {
                    GPSearchFragment.this.mDownFlagView.setVisibility(0);
                    return;
                }
                GPSearchFragment.this.mDownFlagView.clearAnimation();
                GPSearchFragment.this.mDownFlagView.setVisibility(8);
            }
        }
    }

    private class MyViewHolder extends ViewHolder {
        private boolean hasInital = false;
        private boolean isCenter;
        ColumnScrollListener mColumnScrollListener;
        private ViewGroup mContainer;
        private ImageView mImageView = null;
        public TextView mTextView = null;
        public TextView mTipsView = null;

        public MyViewHolder(View itemView, ColumnScrollListener columnScrollListener) {
            super(itemView);
            this.mContainer = (ViewGroup) itemView;
            this.mColumnScrollListener = columnScrollListener;
            if (this.mContainer.getChildAt(0) instanceof TextView) {
                this.mTextView = (TextView) this.mContainer.getChildAt(0);
                this.mTipsView = (TextView) this.mContainer.getChildAt(1);
            } else if (this.mContainer.getChildAt(0) instanceof RelativeLayout) {
                if (!SportType.isSwimMode(GPSearchFragment.this.mSportType)) {
                    itemView.findViewById(C0532R.id.heart_container).setVisibility(0);
                    GPSearchFragment.this.mHeartImg = (ImageView) itemView.findViewById(C0532R.id.heart_img);
                    GPSearchFragment.this.mHeartValueView = (TextView) itemView.findViewById(C0532R.id.heart_value);
                }
                this.mImageView = (ImageView) itemView.findViewById(C0532R.id.start_btn);
                ImageView imageView = this.mImageView;
                int i = (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) ? C0532R.drawable.sport_star_btn_ready : C0532R.drawable.sport_star_btn_search;
                imageView.setBackgroundResource(i);
                if (GPSearchFragment.this.mHeartImg != null && GPSearchFragment.this.mHeartValueView != null) {
                    imageView = GPSearchFragment.this.mHeartImg;
                    i = (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) ? C0532R.drawable.sport_star_icon_hr_black : C0532R.drawable.sport_star_icon_hr_red;
                    imageView.setBackgroundResource(i);
                    TextView access$1600 = GPSearchFragment.this.mHeartValueView;
                    if (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) {
                        i = -16777216;
                    } else {
                        i = -1;
                    }
                    access$1600.setTextColor(i);
                }
            }
        }

        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (isCentralItem) {
                this.mColumnScrollListener.setCenterInfo(this.mColumnScrollListener.mCenterIndex);
            }
            this.isCenter = isCentralItem;
        }

        public void setTextViewColor(boolean isCenter) {
            int mColor;
            float mAlpha;
            if (isCenter) {
                mColor = -1;
                mAlpha = 1.0f;
            } else {
                mColor = Color.argb(163, 255, 255, 255);
                mAlpha = 0.64f;
            }
            if (this.mTextView != null) {
                this.mTextView.setTextColor(mColor);
            }
            if (this.mTipsView != null) {
                this.mTipsView.setTextColor(mColor);
            }
            if (this.mImageView != null) {
                this.mImageView.setAlpha(mAlpha);
            }
        }

        public void setHolderViewScacle(boolean isCenter) {
            if (isCenter) {
                this.mContainer.setScaleX(1.0f);
                this.mContainer.setScaleY(1.0f);
            } else {
                this.mContainer.setScaleX(0.6f);
                this.mContainer.setScaleY(0.6f);
            }
            this.hasInital = true;
        }
    }

    private static final class UIHandler extends Handler {
        WeakReference<GPSearchFragment> waitRefs;

        public UIHandler(GPSearchFragment waitingGPSFragment) {
            this.waitRefs = new WeakReference(waitingGPSFragment);
        }

        public void handleMessage(Message msg) {
            GPSearchFragment fragment = (GPSearchFragment) this.waitRefs.get();
            if (fragment == null) {
                Debug.m6w(GPSearchFragment.TAG, "wait fragment is null");
            } else {
                fragment.onSearchGPSFailed(false);
            }
        }
    }

    private class WearAdapter extends Adapter {
        private final LayoutInflater mInflater;

        private WearAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
            if (getItemViewType(position) == 0) {
                return new MyViewHolder(this.mInflater.inflate(C0532R.layout.image_header, null), GPSearchFragment.this.mWearColumnScrollListener);
            }
            return new MyViewHolder(this.mInflater.inflate(C0532R.layout.wear_item, null), GPSearchFragment.this.mWearColumnScrollListener);
        }

        public void onBindViewHolder(ViewHolder holder, int position) {
            boolean z;
            boolean z2 = true;
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            if (getItemViewType(position) == 0) {
                ImageView access$1800;
                int i;
                if (myViewHolder.mImageView != null) {
                    access$1800 = myViewHolder.mImageView;
                    i = (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) ? C0532R.drawable.sport_star_btn_ready : C0532R.drawable.sport_star_btn_search;
                    access$1800.setBackgroundResource(i);
                }
                if (!(GPSearchFragment.this.mHeartImg == null || GPSearchFragment.this.mHeartValueView == null)) {
                    access$1800 = GPSearchFragment.this.mHeartImg;
                    i = (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) ? C0532R.drawable.sport_star_icon_hr_black : C0532R.drawable.sport_star_icon_hr_red;
                    access$1800.setBackgroundResource(i);
                    TextView access$1600 = GPSearchFragment.this.mHeartValueView;
                    if (GPSearchFragment.this.mCurrentStatus == 1 || !SportType.isSportTypeNeedGps(GPSearchFragment.this.mSportType)) {
                        i = -16777216;
                    } else {
                        i = -1;
                    }
                    access$1600.setTextColor(i);
                }
            } else {
                if (myViewHolder.mTextView != null) {
                    myViewHolder.mTextView.setText(getCorrespondingStr(position));
                }
                if (GPSearchFragment.this.mSportType == 2002) {
                    myViewHolder.mTextView.setTextSize(0, 32.0f);
                    myViewHolder.mTipsView.setTextSize(0, 21.0f);
                    if (position == 1 && GPSearchFragment.this.mListener != null) {
                        BaseConfig config = GPSearchFragment.this.mListener.getSportConfig();
                        if (config != null && (config instanceof MixedBaseConfig)) {
                            List<Integer> childList = ((MixedBaseConfig) config).getChildSports();
                            StringBuilder builder = new StringBuilder();
                            for (Integer child : childList) {
                                if (child.intValue() == 1015) {
                                    builder.append(GPSearchFragment.this.getString(C0532R.string.sport_child_swim));
                                    builder.append("+");
                                } else if (child.intValue() == 1009) {
                                    builder.append(GPSearchFragment.this.getString(C0532R.string.sport_child_riding));
                                    builder.append("+");
                                } else if (child.intValue() == 1001) {
                                    builder.append(GPSearchFragment.this.getString(C0532R.string.sport_child_running));
                                    builder.append("+");
                                }
                            }
                            int lastIndex = builder.lastIndexOf("+");
                            if (lastIndex != -1) {
                                builder.delete(lastIndex, builder.length());
                            }
                            myViewHolder.mTipsView.setVisibility(0);
                            myViewHolder.mTipsView.setText(builder.toString());
                        }
                    }
                }
            }
            myViewHolder.mContainer.setTag(Integer.valueOf(position));
            myViewHolder.mContainer.setAlpha(1.0f);
            if (position == GPSearchFragment.this.mWearColumnScrollListener.mCenterIndex) {
                z = true;
            } else {
                z = false;
            }
            myViewHolder.setTextViewColor(z);
            if (position != GPSearchFragment.this.mWearColumnScrollListener.mCenterIndex) {
                z2 = false;
            }
            myViewHolder.setHolderViewScacle(z2);
            holder.itemView.setTag(Integer.valueOf(position));
        }

        private String getCorrespondingStr(int position) {
            if (position == 1) {
                if (!SportType.isNeedSportTargetRecommand(GPSearchFragment.this.mSportType)) {
                    return GPSearchFragment.this.getActivity().getString(C0532R.string.sport_widget_title_setting);
                }
                if (GPSearchFragment.this.mSportType == 2002) {
                    return GPSearchFragment.this.getString(C0532R.string.sport_select_compound);
                }
                return GPSearchFragment.this.getActivity().getString(C0532R.string.sport_run_train_title);
            } else if (position == 2) {
                return GPSearchFragment.this.getActivity().getString(C0532R.string.sport_widget_title_setting);
            } else {
                return "";
            }
        }

        public int getItemCount() {
            if (SportType.isNeedSportTargetRecommand(GPSearchFragment.this.mSportType) || GPSearchFragment.this.mSportType == 2002) {
                return 3;
            }
            return 2;
        }

        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            }
            return 1;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(C0532R.layout.fragment_gps_search, container, false);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(getActivity(), (ViewGroup) root);
        this.mTitleView = (HmTextView) root.findViewById(C0532R.id.sport_type_title);
        this.mGpsContainer = root.findViewById(C0532R.id.waiting_gps_container);
        this.mProgressView = (HmEcgProgressBar) root.findViewById(C0532R.id.wait_gps_progress_view);
        this.mWearListView = (WearableListView) root.findViewById(C0532R.id.sport_wear_list_view);
        this.mWearListView.setFocusable(false);
        this.mGpsIcon = (ImageView) root.findViewById(C0532R.id.waiting_gps_icon);
        this.mWaitingGpsTextView = (HmTextView) root.findViewById(C0532R.id.waiting_gps);
        this.mDownFlagView = root.findViewById(C0532R.id.down_flag);
        this.mMainContainer = root.findViewById(C0532R.id.main_container);
        this.mHandler = new UIHandler(this);
        this.mGpsIcon.setBackgroundResource(C0532R.drawable.waiting_gps_search);
        this.mSearchAnimationDrawable = (AnimationDrawable) this.mGpsIcon.getBackground();
        initTitle();
        initProgressView();
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            setGPSStatus(2);
        }
        initGPS();
        this.wearAdapter = new WearAdapter(getActivity());
        Global.getGlobalUIHandler().postDelayed(new C07591(), 500);
        Global.getGlobalUIHandler().postDelayed(new C07602(), 1000);
        return root;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mListener = (IGPSearchListener) activity;
        this.mVibrator = (Vibrator) activity.getSystemService("vibrator");
        this.mHmSensorHubConfigManager = SensorHubManagerWrapper.getInstance(Global.getApplicationContext());
        scheduleHeartClosed();
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            scheduleGpsClosed();
            schedulGPSNotification();
        }
        registerGpsCloseReceiver();
        notifySlptInGps();
        if (!SportType.isSwimMode(this.mSportType)) {
            startHeartMeasure();
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mWakeLock = ((PowerManager) getActivity().getSystemService("power")).newWakeLock(10, "wait_gps_wake_lock");
    }

    public void onResume() {
        super.onResume();
        if (this.mIsFirstCreated && SportType.isSportTypeNeedGps(this.mSportType)) {
            this.mIsAgpsExpiredShown = AGPSManager.getInstance().checkAgpsExpired();
            if (!this.mIsAgpsExpiredShown) {
                beginToSearchGps();
            } else if (this.mListener != null) {
                this.mListener.onAgpsExpired();
            }
            this.mIsFirstCreated = false;
        }
    }

    private void setGPSStatus(int gpsStatus) {
        Debug.m3d(TAG, "setGpsStatus: " + gpsStatus + ", current gps: " + this.mCurrentStatus);
        if (!isAdded() || isDetached()) {
            Debug.m5i(TAG, "the fragment is not attached");
            return;
        }
        setSlptGPStatus(gpsStatus);
        this.mMainContainer.setVisibility(0);
        switch (gpsStatus) {
            case 1:
                if (this.mHandler != null) {
                    this.mHandler.removeCallbacksAndMessages(null);
                    this.mProgressView.setVisibility(8);
                    this.mGpsIcon.clearAnimation();
                    this.mSearchAnimationDrawable.stop();
                    this.mSearchAnimationDrawable.selectDrawable(0);
                    this.mGpsIcon.setBackgroundResource(C0532R.drawable.sport_gps_ready);
                    this.mWaitingGpsTextView.setText(C0532R.string.gps_search_success);
                    if (this.mVibrator != null) {
                        Debug.m3d(TAG, "GPS_STATUS_AVAILABLE, vibrate");
                        this.mVibrator.vibrate(GPS_AVAILABLE_VIBRATOR_TIME_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
                    }
                    this.mHandler.postDelayed(new C07613(), 14000);
                    if (this.mListener != null) {
                        this.mListener.onGpsFoundSuccess();
                        break;
                    }
                }
                Debug.m5i(TAG, "handler is null");
                return;
                break;
            case 2:
                if (!(this.mCurrentStatus == gpsStatus || this.mIsClosedGps)) {
                    resetGpsSearchTimeout();
                }
                this.mProgressView.setVisibility(0);
                this.mWaitingGpsTextView.setText(C0532R.string.gps_searching);
                this.mSearchAnimationDrawable.stop();
                this.mSearchAnimationDrawable.selectDrawable(0);
                this.mSearchAnimationDrawable.start();
                break;
        }
        boolean isSame;
        if (this.mCurrentStatus == gpsStatus) {
            isSame = true;
        } else {
            isSame = false;
        }
        this.mCurrentStatus = gpsStatus;
        this.mListener.onGpsChanged(this.mCurrentStatus);
        if (this.mWearListView != null && this.mWearListView.getAdapter() != null && !isSame) {
            this.mWearListView.getAdapter().notifyItemChanged(0);
        }
    }

    private void initProgressView() {
        if (this.mProgressView != null) {
            int finalLength = getResources().getDimensionPixelSize(C0532R.dimen.gps_search_line_final_length);
            int maxLength = getResources().getDimensionPixelSize(C0532R.dimen.gps_search_line_max_length);
            this.mProgressView.setLineColor(getResources().getColor(C0532R.color.gps_search_color));
            this.mProgressView.setLineLength(finalLength, maxLength);
        }
    }

    private void beginToSearchGps() {
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            this.mMainContainer.setVisibility(0);
            Debug.m3d(TAG, "onResume, listener:" + this.mListener + ", isTimeout: " + this.mIsTimeout);
            if (this.mListener != null && !this.mIsTimeout && this.mHmSensorHubConfigManager != null) {
                int gpsState;
                if (this.mHmSensorHubConfigManager.isGpsAvailable()) {
                    gpsState = 1;
                } else {
                    gpsState = 2;
                }
                Debug.m3d(TAG, "onResume, gps state: " + gpsState);
                setGPSStatus(gpsState);
                if (gpsState == 2) {
                    resetGpsSearchTimeout();
                }
            }
        }
    }

    private void resetGpsSearchTimeout() {
        Debug.m6w(TAG, "resetGpsSearchTimeout");
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            if (AGPSManager.getInstance().checkAgpsExpired()) {
                acquireWakeLock(180000);
            } else {
                acquireWakeLock(180000);
            }
            if (this.mHandler != null) {
                setSlptGPStatus(3);
                this.mHandler.removeCallbacksAndMessages(null);
                this.mProgressView.resetProgressAnimation();
                this.mProgressView.setTimeOutTime(180000, true);
                this.mProgressView.startProgressAnimation(new C07624());
            }
        }
    }

    private void acquireWakeLock(long duration) {
        Debug.m5i(TAG, "acquireWakeLock");
        if (this.mWakeLock != null) {
            this.mWakeLock.acquire(14000 + duration);
        }
    }

    private void releaseWakeLock() {
        Debug.m5i(TAG, "releaseWakeLock");
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    public void onDetach() {
        super.onDetach();
        if (!SportType.isSwimMode(this.mSportType)) {
            stopHeartMeasure();
        }
        notifySlptLeaveGps();
        releaseWakeLock();
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
        }
        Debug.m3d(TAG, "onDetach, listener:" + this.mListener + ", isStartedSport: " + this.mListener.isSportStart());
        if (!this.mListener.isSportStart()) {
            stopToListenGPS();
        }
        if (this.mGpsIcon != null) {
            this.mGpsIcon.clearAnimation();
        }
        if (this.mSearchAnimationDrawable != null) {
            this.mSearchAnimationDrawable.stop();
        }
        if (this.mProgressView != null) {
            this.mProgressView.resetProgressAnimation();
        }
        this.mHandler = null;
        this.mListener = null;
        this.mHmSensorHubConfigManager = null;
        cancelSendGpsClosedNotificationIntent();
        cancelSendGpsClosedIntent();
        cancelSendHeartClosedIntent();
        unRegisterGpsCloseReceiver();
        unRegisteGPSReceiver();
        setSlptGPStatus(2);
    }

    private void initGPS() {
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            onGpsStatusChanged(getSensorManager().isGpsAvailable() ? 1 : 2);
            startToListenGPS();
            return;
        }
        this.mGpsContainer.setVisibility(4);
    }

    private void initTitle() {
        String title = null;
        switch (this.mSportType) {
            case 1:
                title = getString(C0532R.string.sport_widget_title_run);
                break;
            case 6:
                title = getString(C0532R.string.sport_widget_title_walk);
                break;
            case 7:
                title = getString(C0532R.string.sport_widget_title_cross);
                break;
            case 8:
                title = getString(C0532R.string.sport_widget_title_indoor_run);
                break;
            case 9:
                title = getString(C0532R.string.sport_widget_title_outdoor_riding);
                break;
            case 10:
                title = getString(C0532R.string.sport_widget_title_indoor_riding);
                break;
            case 11:
                title = getString(C0532R.string.sport_widget_title_skiing);
                break;
            case 12:
                title = getString(C0532R.string.sport_widget_title_elliptical);
                break;
            case 13:
                title = getString(C0532R.string.sport_widget_title_mountaineer);
                break;
            case 14:
                title = getString(C0532R.string.sport_widget_title_indoor_swim);
                break;
            case 15:
                title = getString(C0532R.string.sport_widget_title_open_water_swim);
                break;
            case 17:
                title = getString(C0532R.string.sport_widget_title_tennis);
                break;
            case 18:
                title = getString(C0532R.string.sport_widget_title_soccer);
                break;
            case 2001:
                title = getString(C0532R.string.sport_widget_title_triathlon);
                break;
            case 2002:
                title = getString(C0532R.string.sport_widget_title_compound);
                break;
        }
        this.mTitleView.setText(title);
    }

    private void startToListenGPS() {
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            if (this.mFirstSearchGps) {
                this.mFirstSearchGps = false;
                this.mGPSearchTime = System.currentTimeMillis();
            }
            Global.getGlobalWorkHandler().post(new C07635());
        }
    }

    private void stopToListenGPS() {
        Global.getGlobalWorkHandler().post(new C07646());
    }

    private SensorHubManagerWrapper getSensorManager() {
        if (this.mSensorHubManagerWrapper == null) {
            this.mSensorHubManagerWrapper = SensorHubManagerWrapper.getInstance(Global.getApplicationContext());
        }
        return this.mSensorHubManagerWrapper;
    }

    private void registeGPSReceiver() {
        Debug.m6w(TAG, "-----registeGPSReceiver-----");
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watch.action.SENSOR_WAKEUP");
        getActivity().registerReceiver(this.mReceiver, filter);
    }

    private void unRegisteGPSReceiver() {
        Debug.m6w(TAG, "-----unRegisteGPSReceiver-----");
        if (this.mReceiver != null) {
            try {
                getActivity().unregisterReceiver(this.mReceiver);
            } catch (Exception e) {
                Debug.m6w(TAG, "the receiver not registered");
            }
        }
    }

    private void onGpsStatusChanged(final int gpsStatus) {
        if (!isDetached() && isAdded() && this.mHandler != null && this.mProgressView != null) {
            Debug.m3d(TAG, "onStatusChange: " + gpsStatus + ", mIsTimeout:" + this.mIsTimeout + ", current gps status: " + this.mCurrentStatus);
            if (this.mCurrentStatus != gpsStatus) {
                if (gpsStatus == 1) {
                    this.mHandler.removeCallbacksAndMessages(null);
                    this.mProgressView.stopProgressAnimation(new Runnable() {
                        public void run() {
                            GPSearchFragment.this.setGPSStatus(gpsStatus);
                        }
                    });
                } else if (gpsStatus == 2) {
                    setGPSStatus(gpsStatus);
                }
                PowerUtils.wakeupForMilliSecond(getActivity(), 2000);
            }
            if (gpsStatus == 1 && !this.mSearchGPSSuccess) {
                int costTime = (int) (System.currentTimeMillis() - this.mGPSearchTime);
                this.mSearchGPSSuccess = true;
            }
        }
    }

    private synchronized void onSearchGPSFailed(boolean isCloseGps) {
        if (SportType.isSportTypeNeedGps(this.mSportType)) {
            Debug.m6w(TAG, "onSearchGPSFailed, isCloseGps:" + isCloseGps);
            if (!isAdded() || isDetached() || this.mListener.isSportStart()) {
                Debug.m6w(TAG, "onSearchGPSFailed, fragment is not attached");
            } else {
                if (this.mVibrator != null) {
                    this.mVibrator.vibrate(GPS_AVAILABLE_VIBRATOR_TIME_PATTERN, -1, VibratorUtil.getMostStrongAttributes());
                }
                setSlptGPStatus(2);
                this.mGpsIcon.clearAnimation();
                this.mIsTimeout = true;
                this.mSearchAnimationDrawable.stop();
                this.mSearchAnimationDrawable.selectDrawable(0);
                if (this.mListener != null && isCloseGps) {
                    this.mIsClosedGps = true;
                    stopToListenGPS();
                    setGPSStatus(2);
                    setSlptGPStatus(4);
                }
                if (this.mListener != null) {
                    this.mListener.onGpsTimeOut(isCloseGps);
                }
            }
        }
    }

    private void scheduleHeartClosed() {
        Debug.m5i(TAG, "scheduleHeartClosed");
        scheduleOneTimeTash(1200000, "com.huami.watch.sport.action.CLOSE_HEART");
    }

    private void scheduleGpsClosed() {
        Debug.m5i(TAG, "scheduleGpsClosed");
        scheduleOneTimeTash(1200000, "com.huami.watch.sport.action.CLOSE_GPS");
    }

    private void schedulGPSNotification() {
        Debug.m5i(TAG, "schedulGPSNotification");
        scheduleOneTimeTash(1140000, "com.huami.watch.sport.action.CLOSE_GPS_NOTIFY");
    }

    private void scheduleOneTimeTash(int mill, String action) {
        Debug.m5i(TAG, "scheduleOneTimeTash");
        if (this.mAlarmManager == null) {
            this.mAlarmManager = (AlarmManager) getActivity().getSystemService("alarm");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(14, mill);
        this.mAlarmManager.setExact(0, calendar.getTimeInMillis(), PendingIntent.getBroadcast(getActivity(), 0, new Intent(action), 0));
    }

    private void registerGpsCloseReceiver() {
        Debug.m5i(TAG, "registerGpsReceiver");
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.huami.watch.sport.action.CLOSE_GPS_NOTIFY");
            filter.addAction("com.huami.watch.sport.action.CLOSE_GPS");
            filter.addAction("com.huami.watch.sport.action.CLOSE_HEART");
            getActivity().registerReceiver(this.mGpsReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unRegisterGpsCloseReceiver() {
        Debug.m5i(TAG, "unRegisterGpsReceiver");
        try {
            getActivity().unregisterReceiver(this.mGpsReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelSendHeartClosedIntent() {
        Debug.m5i(TAG, "cancelSendHeartClosedIntent");
        cancelTimeTaskIntent("com.huami.watch.sport.action.CLOSE_HEART");
    }

    private void cancelSendGpsClosedIntent() {
        Debug.m5i(TAG, "cancelSendGpsClosedIntent");
        cancelTimeTaskIntent("com.huami.watch.sport.action.CLOSE_GPS");
    }

    private void cancelSendGpsClosedNotificationIntent() {
        Debug.m5i(TAG, "cancelSendGpsClosedNotificationIntent");
        cancelTimeTaskIntent("com.huami.watch.sport.action.CLOSE_GPS_NOTIFY");
    }

    private void cancelTimeTaskIntent(String action) {
        if (this.mAlarmManager != null) {
            Debug.m5i(TAG, "cancelTimeTaskIntent, action:" + action);
            this.mAlarmManager.cancel(PendingIntent.getBroadcast(getActivity(), 0, new Intent(action), 0));
        }
    }

    public void onClick(View view) {
        int id = view.getId();
    }

    public void handleBtnEvent() {
        if (!isDetached() && isAdded()) {
            Debug.m6w(TAG, "wait gps click, isTimeout: " + this.mIsTimeout);
            if (!UnitConvertUtils.isOverSeaVersion()) {
                DataManager.getInstance();
                if (DataManager.isDeviceExperience(getActivity()) && DataManager.getInstance().isShownBindDeviceTips(getActivity())) {
                    if (this.mListener != null) {
                        this.mListener.onFirstExperienceStartSport();
                        return;
                    }
                    return;
                }
            }
            if (this.mIsAgpsExpiredShown && this.mCurrentStatus != 1) {
                beginToSearchGps();
                this.mIsAgpsExpiredShown = false;
            } else if (this.mIsTimeout && this.mCurrentStatus == 2) {
                this.mGpsIcon.setBackground(this.mSearchAnimationDrawable);
                setGPSStatus(2);
                startToListenGPS();
                resetGpsSearchTimeout();
                this.mIsTimeout = false;
                this.mIsClosedGps = false;
            } else {
                if (this.mHandler != null) {
                    this.mHandler.removeCallbacksAndMessages(null);
                }
                this.mListener.startSport(this.mCurrentStatus);
                PointUtils.recordEventProperty("0001", this.mSportType);
            }
        }
    }

    private void notifySlptInGps() {
        if (SportDataManager.getInstance().getSportStatus() != 0) {
            LogUtil.m9i(true, TAG, "notifySlptInGps, sport status is :" + SportDataManager.getInstance().getSportStatus());
            return;
        }
        Debug.m5i(TAG, "notifySlptInGps, sport:" + this.mSportType);
        Intent intent = new Intent("com.huami.watchface.SlptWatchService.enter_gps");
        intent.putExtra("sport_type", this.mSportType);
        getActivity().sendBroadcast(intent);
        DataManager.getInstance().setIsInGPSearchingUI(true);
    }

    private void notifySlptLeaveGps() {
        if (SportDataManager.getInstance().getSportStatus() != 0) {
            LogUtil.m9i(true, TAG, "notifySlptLeaveGps, sport status is :" + SportDataManager.getInstance().getSportStatus());
            return;
        }
        Debug.m5i(TAG, "notifySlptLeaveGps, sport:" + this.mSportType);
        Intent intent = new Intent("com.huami.watchface.SlptWatchService.leave_gps");
        intent.putExtra("is_start_sport", this.mListener.isSportStart());
        getActivity().sendBroadcast(intent);
        setSlptGPStatus(2);
        DataManager.getInstance().setIsInGPSearchingUI(false);
    }

    private void setSlptGPStatus(int gpsStatus) {
        Debug.m5i(TAG, "setSlptGPStatus, " + gpsStatus);
        try {
            SystemProperties.set("sys.watchface.gps.status", String.valueOf(gpsStatus));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateHeartValue(final int quantity, final int heart) {
        if (this.mHeartImg != null && this.mHeartValueView != null) {
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    if (!GPSearchFragment.this.isDetached() && GPSearchFragment.this.isAdded()) {
                        if (quantity <= 1) {
                            GPSearchFragment.this.mHeartValueView.setText("" + heart);
                        } else {
                            GPSearchFragment.this.mHeartValueView.setText(GPSearchFragment.this.getString(C0532R.string.sport_pause_heart_shown));
                        }
                    }
                }
            });
        }
    }

    public void refreshCompoundView() {
        this.wearAdapter.notifyDataSetChanged();
    }

    private void startHeartMeasure() {
        if (this.mIsStopRequestHeartData) {
            if (this.mTicker == null) {
                this.mTicker = new BaseTicker();
            }
            this.mTicker.setIntervalTime(1000);
            this.mTicker.startTicker();
            this.mTicker.setTickerListener(this.mTimeListener);
            this.mIsStopRequestHeartData = false;
        }
    }

    private void stopHeartMeasure() {
        if (!this.mIsStopRequestHeartData) {
            this.mIsStopRequestHeartData = false;
            this.mTicker.stopTicker();
            this.mTicker.setTickerListener(null);
            this.mTicker = null;
        }
    }
}
