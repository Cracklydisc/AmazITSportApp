package com.huami.watch.newsport.ui.fragps;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import com.huami.watch.sensorhub.SensorHubProtos.SportThaWorkout;
import java.text.DecimalFormat;

public class SportTargetRemindFragment extends BaseFragment implements OnClickListener {
    private static final String TAG = SportTargetRemindFragment.class.getSimpleName();
    private volatile int distance;
    private volatile int duration;
    private int index = 0;
    private NumberTextView mDisView;
    private HmTextView mDistanceUnitView = null;
    private HmTextView mGpsStatusView = null;
    private ITargetRecommandListener mListener;
    private final BroadcastReceiver mReceiver = new C07773();
    private HmTextView mSportTargetStatus;
    private View mStartBtn;
    private NumberTextView mTeView;
    private NumberTextView mTimeView;
    private HmTextView mTitleView;
    private volatile int phraseLevel = -1;
    private SportThaWorkout sportThaWorkout = null;
    private volatile int trainEffect;

    public interface ITargetRecommandListener {
        void onStartBtnClicked();
    }

    class C07731 implements OnClickListener {
        C07731() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(SportTargetRemindFragment.this.getActivity(), 3);
        }
    }

    class C07762 implements Runnable {

        class C07751 implements OnKlvpDataListener {
            C07751() {
            }

            public void onHealthDataReady(int i, float v) {
            }

            public void onSportDataReady(SportStatistics sportStatistics) {
                Utils.printlnSportStatistics(sportStatistics);
                if (sportStatistics != null && sportStatistics.mSportThaWorkout != null) {
                    final int teValue = sportStatistics.mSportThaWorkout.getTrainingEffect();
                    final int duration = sportStatistics.mSportThaWorkout.getDuration();
                    final int distance = sportStatistics.mSportThaWorkout.getDistance();
                    final int phrase = sportStatistics.mSportThaWorkout.getPhrase();
                    SportTargetRemindFragment.this.setThaWorkOutData(phrase, duration, distance, teValue);
                    Global.getGlobalUIHandler().post(new Runnable() {
                        public void run() {
                            Log.i(SportTargetRemindFragment.TAG, " getSensorHubData update ui ");
                            if (!SportTargetRemindFragment.this.isDetached() && SportTargetRemindFragment.this.isAdded()) {
                                SportTargetRemindFragment.this.mSportTargetStatus.setText(SportTargetRemindFragment.this.getString(C0532R.string.sport_tartget_tips, SportTargetRemindFragment.this.getSportTargetStatus(phrase)));
                                SportTargetRemindFragment.this.mTeView.setText(new DecimalFormat("######0.0").format((double) (((float) teValue) / 10.0f)) + " ");
                                SportTargetRemindFragment.this.mTimeView.setText(String.valueOf(duration) + " ");
                                if (UnitConvertUtils.isImperial()) {
                                    SportTargetRemindFragment.this.mDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm((double) distance), false));
                                } else {
                                    SportTargetRemindFragment.this.mDisView.setText(String.valueOf(distance) + " ");
                                }
                                if (UnitConvertUtils.isImperial()) {
                                    SportTargetRemindFragment.this.mDistanceUnitView.setText(SportTargetRemindFragment.this.getResources().getString(C0532R.string.km_imperial));
                                } else {
                                    SportTargetRemindFragment.this.mDistanceUnitView.setText(SportTargetRemindFragment.this.getResources().getString(C0532R.string.km_metric));
                                }
                            }
                        }
                    });
                }
            }
        }

        C07762() {
        }

        public void run() {
            HmSensorHubConfigManager.getHmSensorHubConfigManager(SportTargetRemindFragment.this.getActivity()).requestRealtimeData(2, new C07751());
        }
    }

    class C07773 extends BroadcastReceiver {
        C07773() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("sensorhub.gps.state_change".equals(intent.getStringExtra("WAKEUP_SOURCE"))) {
                String gpsStatusText = intent.getStringExtra("GPS_STATE");
                Debug.m6w(SportTargetRemindFragment.TAG, "onReceive, gpsStatusText: " + gpsStatusText);
                if ("1".equals(gpsStatusText)) {
                    SportTargetRemindFragment.this.updateGpsStatus(true);
                } else {
                    SportTargetRemindFragment.this.updateGpsStatus(false);
                }
            }
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey("tha_work_out_phrase")) {
                this.phraseLevel = getArguments().getInt("tha_work_out_phrase");
            }
            if (getArguments().containsKey("tha_work_out_duration")) {
                this.duration = getArguments().getInt("tha_work_out_duration");
            }
            if (getArguments().containsKey("tha_work_out_diatance")) {
                this.distance = getArguments().getInt("tha_work_out_diatance");
            }
            if (getArguments().containsKey("tha_work_out_training_effect")) {
                this.trainEffect = getArguments().getInt("tha_work_out_training_effect");
            }
        }
    }

    public void setThaWorkOutData(int phraseLevel, int duration, int distance, int trainEffect) {
        this.phraseLevel = phraseLevel;
        this.duration = duration;
        this.distance = distance;
        this.trainEffect = trainEffect;
        Log.i(TAG, " setThaWorkOutData update ui: [phraseLevel:" + phraseLevel + ",duration:" + duration + ",distance:" + distance + ",trainEffect:" + trainEffect);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_sport_target_remind, container, false);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(getActivity(), (ViewGroup) root);
        this.mTitleView = (HmTextView) root.findViewById(C0532R.id.sport_type_title);
        this.mStartBtn = root.findViewById(C0532R.id.go_next_btn);
        this.mTeView = (NumberTextView) root.findViewById(C0532R.id.te_value);
        this.mTimeView = (NumberTextView) root.findViewById(C0532R.id.time_value);
        this.mDisView = (NumberTextView) root.findViewById(C0532R.id.dis_value);
        this.mGpsStatusView = (HmTextView) root.findViewById(C0532R.id.gps_status);
        this.mSportTargetStatus = (HmTextView) root.findViewById(C0532R.id.sport_target_status);
        this.mDistanceUnitView = (HmTextView) root.findViewById(C0532R.id.distance_unit);
        this.mStartBtn.setOnClickListener(this);
        getSensorHubData();
        this.mTitleView.setOnClickListener(new C07731());
        registeGPSReceiver();
        updateGpsStatus(SensorHubManagerWrapper.getInstance(getActivity()).isGpsAvailable());
        return root;
    }

    public void onResume() {
        super.onResume();
        Log.i(TAG, " onResume ");
        this.mSportTargetStatus.setText(getString(C0532R.string.sport_tartget_tips, getSportTargetStatus(this.phraseLevel)));
        this.mTeView.setText(new DecimalFormat("######0.0").format((double) (((float) this.trainEffect) / 10.0f)) + " ");
        this.mTimeView.setText(String.valueOf(this.duration) + " ");
        if (UnitConvertUtils.isImperial()) {
            this.mDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm((double) this.distance), false));
        } else {
            this.mDisView.setText(String.valueOf(this.distance) + " ");
        }
        if (UnitConvertUtils.isImperial()) {
            this.mDistanceUnitView.setText(getResources().getString(C0532R.string.km_imperial));
        } else {
            this.mDistanceUnitView.setText(getResources().getString(C0532R.string.km_metric));
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mListener = (ITargetRecommandListener) activity;
    }

    public void onDetach() {
        super.onDetach();
        unRegisteGPSReceiver();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case C0532R.id.go_next_btn:
                if (this.mListener != null) {
                    PointUtils.recordEventProperty("0007", this.mSportType);
                    this.mListener.onStartBtnClicked();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private String getSportTargetStatus(int phrase) {
        String targetStatus = "";
        if (getActivity() == null || getActivity().getResources() == null) {
            return targetStatus;
        }
        switch (phrase) {
            case 0:
                return getActivity().getResources().getString(C0532R.string.sport_target_remind_status_rest_or_light);
            case 1:
                return getActivity().getResources().getString(C0532R.string.sport_target_remind_status_maintaining);
            case 2:
                return getActivity().getResources().getString(C0532R.string.sport_target_remind_status_improving);
            case 3:
                return getActivity().getResources().getString(C0532R.string.sport_target_remind_status_count);
            default:
                return targetStatus;
        }
    }

    private void getSensorHubData() {
        Log.i(TAG, " before phraseLevel:" + this.phraseLevel + ",duration:" + this.duration + ",distance:" + this.distance + ",teValue:" + this.trainEffect);
        if (this.phraseLevel < 0 || this.duration <= 0 || this.distance <= 0 || this.trainEffect == 0) {
            Global.getGlobalWorkHandler().post(new C07762());
        } else {
            Global.getGlobalWorkHandler().post(new C07762());
        }
    }

    private void registeGPSReceiver() {
        Debug.m6w(TAG, "-----registeGPSReceiver-----");
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watch.action.SENSOR_WAKEUP");
        getActivity().registerReceiver(this.mReceiver, filter);
    }

    private void unRegisteGPSReceiver() {
        Debug.m6w(TAG, "-----unRegisteGPSReceiver-----");
        if (this.mReceiver != null) {
            try {
                getActivity().unregisterReceiver(this.mReceiver);
            } catch (Exception e) {
                Debug.m6w(TAG, "the receiver not registered");
            }
        }
    }

    public void updateGpsStatus(boolean isAvailable) {
        this.mStartBtn.setBackgroundResource(isAvailable ? C0532R.drawable.sport_star_btn_ready_m : C0532R.drawable.sport_star_btn_search_m);
        this.mGpsStatusView.setText(isAvailable ? getString(C0532R.string.gps_search_success) : getString(C0532R.string.gps_searching));
    }
}
