package com.huami.watch.newsport.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;

public class IntermittentTrainingActivity extends BaseActivity {
    private BaseFragment mIntermittentTrainingFragment;
    private int mSportType = -1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("widget_settings_type", -1);
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_widget_setting_layout);
            this.mIntermittentTrainingFragment = BaseFragment.newInstance(this.mSportType, 3);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(C0532R.id.container, this.mIntermittentTrainingFragment);
            transaction.commitAllowingStateLoss();
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 8705) {
            if (data != null && data.getBooleanExtra("is_start_sport", false)) {
                setResult(8705, data);
                finish();
            }
        } else if (requestCode == 8706 && data != null && data.getBooleanExtra("is_start_sport", false)) {
            setResult(8706, data);
            finish();
        }
    }
}
