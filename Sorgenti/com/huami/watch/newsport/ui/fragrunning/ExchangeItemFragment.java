package com.huami.watch.newsport.ui.fragrunning;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.timeticker.action.BaseTicker;
import com.huami.watch.newsport.timeticker.listener.ITickerListener;
import com.huami.watch.newsport.ui.view.RTNumberTextView;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;

public class ExchangeItemFragment extends Fragment implements ITickerListener {
    private IExchangeItemAction mAction = null;
    private View mBatteryView = null;
    private int mBgType = 0;
    private View mContainer = null;
    private long mCurrentTime = -1;
    private RTNumberTextView mExchangeTimeLinear = null;
    private TextView mExchangeTitle = null;
    private ImageView mSportImg = null;
    private HmTextView mSportTitle = null;
    private int mSportType = -1;
    private BaseTicker mTicker;
    private TextView mTimeTitle = null;
    private TextView mTotalTimeLinear = null;
    private long mTrackId = -1;

    public interface IExchangeItemAction {
        int getExchangedCurrentBatteryLevel();

        void onExchangedItemStart(long j);
    }

    class C07781 implements Runnable {
        C07781() {
        }

        public void run() {
            if (!ExchangeItemFragment.this.isDetached() && ExchangeItemFragment.this.isAdded() && ExchangeItemFragment.this.mExchangeTimeLinear != null && ExchangeItemFragment.this.mTotalTimeLinear != null && ExchangeItemFragment.this.mBatteryView != null) {
                if (ExchangeItemFragment.this.mBgType == 1) {
                    ExchangeItemFragment.this.mExchangeTimeLinear.setTextColor(-16777216);
                }
                ExchangeItemFragment.this.mExchangeTimeLinear.setText(DataFormatUtils.parseCrossingSecondToDefaultFormattedTimeContainHour((System.currentTimeMillis() - ExchangeItemFragment.this.mCurrentTime) / 1000));
                ExchangeItemFragment.this.mTotalTimeLinear.setText(DataFormatUtils.parseCrossingSecondToDefaultFormattedTimeContainHour((System.currentTimeMillis() - ExchangeItemFragment.this.mTrackId) / 1000));
                if (ExchangeItemFragment.this.mAction != null) {
                    ExchangeItemFragment.this.updateBattery(ExchangeItemFragment.this.mAction.getExchangedCurrentBatteryLevel());
                }
            }
        }
    }

    public static ExchangeItemFragment newInstance(long trackId, int sportType) {
        ExchangeItemFragment fragment = new ExchangeItemFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("track_id", trackId);
        bundle.putInt("sport_type", sportType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mTicker = new BaseTicker();
        this.mTicker.setIntervalTime(1000);
        this.mTicker.setTickerListener(this);
        this.mTrackId = getArguments().getLong("track_id", -1);
        this.mSportType = getArguments().getInt("sport_type", -1);
        if (this.mTrackId == -1 || !SportType.isSportTypeValid(this.mSportType)) {
            throw new IllegalArgumentException("err found, trackid:" + this.mTrackId + ", sport type:" + this.mSportType);
        }
        this.mBgType = BackgroundUtils.getCustomBgType(getActivity());
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_exchange_item, container, false);
        this.mSportImg = (ImageView) root.findViewById(C0532R.id.sport_img);
        this.mSportTitle = (HmTextView) root.findViewById(C0532R.id.sport_type_txt);
        this.mExchangeTimeLinear = (RTNumberTextView) root.findViewById(C0532R.id.total_time_linear);
        this.mTotalTimeLinear = (TextView) root.findViewById(C0532R.id.sport_bar_time);
        this.mBatteryView = root.findViewById(C0532R.id.battery_view);
        this.mExchangeTitle = (TextView) root.findViewById(C0532R.id.exchange_title);
        this.mContainer = root.findViewById(C0532R.id.main_container);
        this.mTimeTitle = (TextView) root.findViewById(C0532R.id.time_title);
        if (this.mBgType == 1) {
            this.mTimeTitle.setTextColor(-16777216);
            this.mSportTitle.setTextColor(-16777216);
            this.mExchangeTitle.setTextColor(-16777216);
            this.mTotalTimeLinear.setTextColor(-16777216);
            ((TextView) root.findViewById(C0532R.id.exchange_two)).setTextColor(-16777216);
            this.mContainer.setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_2);
            root.findViewById(C0532R.id.mixed_title_container).setBackgroundResource(C0532R.drawable.bg_white_custom);
            root.findViewById(C0532R.id.exchange_icon).setBackgroundResource(C0532R.drawable.bg_white_sport_icon_right_red);
        }
        initView();
        startTick();
        this.mCurrentTime = System.currentTimeMillis();
        if (this.mAction != null) {
            this.mAction.onExchangedItemStart(this.mCurrentTime);
        }
        return root;
    }

    private void startTick() {
        stopTickerIfNeeded();
        this.mTicker.startTicker();
    }

    private void initView() {
        switch (this.mSportType) {
            case 1001:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_running));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_running));
                }
                this.mSportTitle.setText(getString(C0532R.string.exchange_prepare_running));
                return;
            case 1009:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_outdoor_riding));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_outdoor_riding));
                }
                this.mSportTitle.setText(getString(C0532R.string.exchange_prepare_riding));
                return;
            case 1015:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_outdoor_swimming));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_outdoor_swimming));
                }
                this.mSportTitle.setText(getString(C0532R.string.sport_child_swim));
                return;
            default:
                return;
        }
    }

    public void onDetach() {
        super.onDetach();
        stopTickerIfNeeded();
    }

    private void stopTickerIfNeeded() {
        if (this.mTicker.isRunning()) {
            this.mTicker.stopTicker();
        }
    }

    public void onTicker(int millSecond) {
        if (this.mExchangeTimeLinear != null) {
            Global.getGlobalUIHandler().post(new C07781());
        }
    }

    public void updateBattery(int level) {
        int pos = 13;
        if (this.mBatteryView != null) {
            if (level / 7 <= 13) {
                pos = level / 7;
            }
            this.mBatteryView.setBackgroundResource(BackgroundUtils.getBatteryResArray(this.mBgType)[pos]);
        }
    }

    public void setListener(IExchangeItemAction listener) {
        this.mAction = listener;
    }
}
