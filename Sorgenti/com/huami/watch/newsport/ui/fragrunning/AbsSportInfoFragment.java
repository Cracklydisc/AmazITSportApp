package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.battery.BatteryManager.IBatteryChangedListener;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.sportcenter.SportInfoOrderManagerWrapper;
import com.huami.watch.newsport.sportcenter.action.ISportInfoOrderManager;
import com.huami.watch.newsport.sportcenter.action.ISportInfoOrderManager.SportInfo;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.ui.view.RTNumberTextView;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.SportLinearViewUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.Calendar;
import java.util.List;

public abstract class AbsSportInfoFragment extends Fragment {
    protected static final String TAG = AbsSportInfoFragment.class.getName();
    protected int intervalRemindType;
    protected boolean isForeground = false;
    protected boolean isOnlySingleTrainType;
    protected HmImageView mAPmView = null;
    protected int mBasePos = 0;
    protected IBatteryChangedListener mBatteryListener;
    protected HmImageView mBatteryView;
    protected int mBgType = 0;
    private Calendar mCalendar = null;
    protected int mIntermittentTrainType = -1;
    protected int mItemCount = 0;
    protected ISportInfoOrderManager mSportInfoOrderManager;
    protected int mSportType = -1;
    protected HmLinearLayout mTimeView = null;
    protected List<View> mViews;

    public abstract void onMillUpdate(OutdoorSportSnapshot outdoorSportSnapshot);

    public abstract void refreshView(OutdoorSportSnapshot outdoorSportSnapshot);

    public abstract void updateBattery(int i);

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSportType = getArguments().getInt("sport_type", -1);
        this.mBasePos = getArguments().getInt("base_position", 0);
        this.mItemCount = getArguments().getInt("sport_item_count", 0);
        this.mIntermittentTrainType = getArguments().getInt("intermittent_train_type", -1);
        this.intervalRemindType = getArguments().getInt("intermittent_remind_type", TrainRemindType.NONE.getType());
        this.isOnlySingleTrainType = getArguments().getBoolean("only_single_train_type", false);
        boolean isMixedSport = getArguments().getBoolean("is_mixed_sport", false);
        if (SportType.isSportTypeValid(this.mSportType)) {
            this.mSportInfoOrderManager = new SportInfoOrderManagerWrapper(getActivity(), this.mSportType, isMixedSport, this.mIntermittentTrainType, this.intervalRemindType, this.isOnlySingleTrainType);
            this.mBgType = BackgroundUtils.getCustomBgType(getActivity());
            return;
        }
        throw new IllegalArgumentException("err sport type");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IBatteryChangedListener) {
            this.mBatteryListener = (IBatteryChangedListener) activity;
        }
    }

    protected void requestBatteryLevel() {
        if (this.mBatteryListener != null) {
            this.mBatteryListener.doBatteryChangedRequest();
        }
    }

    protected void initOriginalView() {
        OutdoorSportSnapshot sportSnapshot = OutdoorSportSnapshot.createSportStatus(this.mSportType);
        for (int i = 0; i < this.mItemCount; i++) {
            refreshCorrespoingView((View) this.mViews.get(i), this.mSportInfoOrderManager.getSportInfo(this.mBasePos + i, sportSnapshot), sportSnapshot);
        }
        int pos = this.mSportInfoOrderManager.getTimePickerPosition();
        if (pos >= this.mBasePos && pos < this.mBasePos + this.mItemCount && this.mSportInfoOrderManager.isShowMillSec()) {
            refreshMillView((View) this.mViews.get(pos - this.mBasePos), this.mSportInfoOrderManager.getSportInfo(pos, sportSnapshot));
        }
    }

    protected void refreshCorrespoingView(View view, SportInfo sportInfo, OutdoorSportSnapshot sportSnapshot) {
        if (sportInfo == null) {
            view.setVisibility(4);
            return;
        }
        view.setVisibility(0);
        int imgRes = sportInfo.getImgRes();
        StringBuilder builder = new StringBuilder();
        builder.append(sportInfo.getValue());
        if (view.findViewById(C0532R.id.item_img) != null) {
            if (imgRes == 0) {
                view.findViewById(C0532R.id.item_img).setBackground(null);
            } else if (imgRes == 1) {
                view.findViewById(C0532R.id.item_img).setBackground(getResources().getDrawable(C0532R.drawable.bg_white_up));
            } else if (imgRes == 2) {
                view.findViewById(C0532R.id.item_img).setBackground(getResources().getDrawable(C0532R.drawable.bg_white_down));
            } else if (imgRes == 3) {
                view.findViewById(C0532R.id.item_img).setBackground(getResources().getDrawable(C0532R.drawable.downhill_down));
            }
        }
        if (this.mBgType == 1) {
            ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-16777216);
        } else if (this.mBgType == 0) {
            ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-1);
        }
        ((TextView) view.findViewById(C0532R.id.item_value)).setText(builder.toString());
        if (sportInfo.getUnitRes() != 0) {
            view.findViewById(C0532R.id.item_unit).setVisibility(0);
            ((TextView) view.findViewById(C0532R.id.item_unit)).setText(getString(sportInfo.getUnitRes()));
        } else {
            view.findViewById(C0532R.id.item_unit).setVisibility(8);
        }
        int heartRange = sportInfo.getHeartRange();
        if (heartRange > 0) {
            if (this.mBgType == 1) {
                if (heartRange == 4 || heartRange == 3 || heartRange > 5) {
                    ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.black));
                } else {
                    ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.white));
                }
            } else if (heartRange == 1 || heartRange == 5 || heartRange > 5) {
                ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.white));
            } else {
                ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.black));
            }
            ((TextView) view.findViewById(C0532R.id.item_unit)).setText(getHeartRangeStr(heartRange));
        } else {
            ((TextView) view.findViewById(C0532R.id.item_unit)).setText(getString(sportInfo.getUnitRes()));
            if (this.mBgType == 1) {
                ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.black));
            } else {
                ((TextView) view.findViewById(C0532R.id.item_unit)).setTextColor(getResources().getColor(C0532R.color.white));
            }
        }
        view.findViewById(C0532R.id.item_unit_container).setBackgroundColor(getHeartRateRangeColor(heartRange));
        if (sportSnapshot.getCurrentSecond() >= 5) {
            view.findViewById(C0532R.id.item_top_value).setVisibility(4);
        }
        TrainUnit currentTrainUnit = SportDataManager.getInstance().getCurrentTrainUnit();
        if (currentTrainUnit != null) {
            Log.i(TAG, " currentTrainUnit:" + currentTrainUnit.toString());
            int trainTargetType = currentTrainUnit.getUnitType().getType();
            Log.i(TAG, " trainTargetType:" + trainTargetType + " ,sportInfo.needRun:" + sportInfo.getNeedRun());
            if (sportInfo.getNeedRun() != null && !TextUtils.isEmpty(sportInfo.getNeedRun())) {
                String needRun = sportInfo.getNeedRun();
                if (trainTargetType == TrainTargetType.DIATANCE.getType()) {
                    if (UnitConvertUtils.isImperial()) {
                        ((HmTextView) view.findViewById(C0532R.id.item_need_do)).setText(String.format(getResources().getString(C0532R.string.this_time_need_run_mileage_mile), new Object[]{needRun}));
                    } else {
                        ((HmTextView) view.findViewById(C0532R.id.item_need_do)).setText(String.format(getResources().getString(C0532R.string.this_time_need_run_mileage), new Object[]{needRun}));
                    }
                } else if (trainTargetType == TrainTargetType.TIME.getType()) {
                    ((HmTextView) view.findViewById(C0532R.id.item_need_do)).setText(String.format(getResources().getString(C0532R.string.this_time_need_run_total_time), new Object[]{needRun}));
                }
            } else if (trainTargetType == TrainTargetType.KEYPRESS.getType()) {
                HmTextView hmTextView = (HmTextView) view.findViewById(C0532R.id.item_top_value);
            }
            if (sportInfo.itemValueRemind > 0) {
                HmTextView itemValueRigthRemind = (HmTextView) view.findViewById(C0532R.id.item_value_right_remind);
                if (itemValueRigthRemind != null) {
                    itemValueRigthRemind.setVisibility(8);
                }
                Log.i(TAG, "SportInfo:" + sportInfo.toString());
                HmTextView itemValue = (HmTextView) view.findViewById(C0532R.id.item_value);
                view.findViewById(C0532R.id.item_value).setVisibility(8);
                view.findViewById(C0532R.id.item_value_total_time).setVisibility(0);
                RTNumberTextView firstMunuteValuView = (RTNumberTextView) view.findViewById(C0532R.id.first_minute_value);
                firstMunuteValuView.setText(sportInfo.getInterValUsedMinute());
                HmTextView firstMinuteFontView = (HmTextView) view.findViewById(C0532R.id.first_minute_font);
                firstMinuteFontView.setText(getResources().getString(C0532R.string.time_picker_minute));
                RTNumberTextView secondsValueView = (RTNumberTextView) view.findViewById(C0532R.id.seconds_value);
                secondsValueView.setText(sportInfo.getInterValUsedSecond());
                HmTextView seccondValueFontView = (HmTextView) view.findViewById(C0532R.id.second_value_font);
                seccondValueFontView.setText(getResources().getString(C0532R.string.sport_time_seconds));
                RTNumberTextView thirdMuniteValueView = (RTNumberTextView) view.findViewById(C0532R.id.third_munute_value);
                thirdMuniteValueView.setText("/" + sportInfo.getInterValTotalTimes());
                HmTextView thirdMuniteFontView = (HmTextView) view.findViewById(C0532R.id.third_value_font);
                thirdMuniteFontView.setText(getResources().getString(C0532R.string.time_picker_minute));
                if (this.mBgType == 1) {
                    Log.i(TAG, "  interval bg red");
                    firstMunuteValuView.setTextColor(-65536);
                    secondsValueView.setTextColor(-65536);
                    thirdMuniteValueView.setTextColor(-65536);
                    firstMinuteFontView.setTextColor(-65536);
                    seccondValueFontView.setTextColor(-65536);
                    thirdMuniteFontView.setTextColor(-65536);
                } else if (this.mBgType == 0) {
                    Log.i(TAG, "  interval bg green ");
                    firstMunuteValuView.setTextColor(-16711936);
                    secondsValueView.setTextColor(-16711936);
                    thirdMuniteValueView.setTextColor(-16711936);
                    firstMinuteFontView.setTextColor(-1);
                    seccondValueFontView.setTextColor(-1);
                    thirdMuniteFontView.setTextColor(-1);
                }
            }
        }
        HmTextView itemValueRightRemindView;
        if (sportInfo.getDataItemType() == 251) {
            itemValueRightRemindView = (HmTextView) view.findViewById(C0532R.id.item_value_right_remind);
            if (itemValueRightRemindView != null) {
                itemValueRightRemindView.setVisibility(0);
                if (UnitConvertUtils.isImperial()) {
                    itemValueRightRemindView.setText(getResources().getString(C0532R.string.km_imperial));
                } else {
                    itemValueRightRemindView.setText(getResources().getString(C0532R.string.km_metric));
                }
                if (this.mBgType == 1) {
                    itemValueRightRemindView.setTextColor(-16777216);
                } else if (this.mBgType == 0) {
                    itemValueRightRemindView.setTextColor(-1);
                }
            }
        } else if (sportInfo.getDataItemType() == 252) {
            itemValueRightRemindView = (HmTextView) view.findViewById(C0532R.id.item_value_right_remind);
            if (itemValueRightRemindView != null) {
                itemValueRightRemindView.setText(getResources().getString(C0532R.string.time_picker_minute));
                if (isZh(getActivity())) {
                    itemValueRightRemindView.setVisibility(8);
                    if (this.mBgType == 1) {
                        itemValueRightRemindView.setTextColor(-16777216);
                        return;
                    } else if (this.mBgType == 0) {
                        itemValueRightRemindView.setTextColor(-1);
                        return;
                    } else {
                        return;
                    }
                }
                itemValueRightRemindView.setVisibility(8);
            }
        } else {
            itemValueRightRemindView = (HmTextView) view.findViewById(C0532R.id.item_value_right_remind);
            if (itemValueRightRemindView != null) {
                itemValueRightRemindView.setVisibility(8);
            }
        }
    }

    private boolean isZh(Context mContext) {
        if (mContext.getResources().getConfiguration().locale.getLanguage().endsWith("zh")) {
            return true;
        }
        return false;
    }

    protected void refreshMillView(View view, SportInfo sportInfo) {
        if (sportInfo == null || view.getVisibility() != 0) {
            view.setVisibility(4);
            return;
        }
        if (this.mBgType == 1) {
            ((TextView) view.findViewById(C0532R.id.item_top_value)).setTextColor(getResources().getColor(C0532R.color.black));
        }
        view.findViewById(C0532R.id.item_top_value).setVisibility(0);
        ((TextView) view.findViewById(C0532R.id.item_top_value)).setText(sportInfo.getMillValue());
    }

    protected void reCalSysTime(HmImageView amPmView, HmLinearLayout timeView) {
        String timeStr;
        if (this.mCalendar == null) {
            this.mCalendar = Calendar.getInstance();
        }
        this.mCalendar.setTimeInMillis(System.currentTimeMillis());
        if (DateFormat.is24HourFormat(getActivity())) {
            amPmView.setVisibility(4);
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "HH:mm", true);
        } else {
            amPmView.setVisibility(0);
            int amOrpm = this.mCalendar.get(9);
            if (amPmView != null) {
                if (amOrpm == 0) {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.am));
                } else {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.pm));
                }
            }
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "hh:mm", true);
        }
        SportLinearViewUtils.initView(getActivity(), timeView, timeStr, this.mBgType, true);
    }

    private int getHeartRateRangeColor(int range) {
        switch (range) {
            case 1:
                if (this.mBgType == 1) {
                    return getResources().getColor(C0532R.color.bg_white_sport_history_heart_range0);
                }
                return getResources().getColor(C0532R.color.sport_history_heart_range0);
            case 2:
                if (this.mBgType == 1) {
                    return getResources().getColor(C0532R.color.bg_white_sport_history_heart_range1);
                }
                return getResources().getColor(C0532R.color.sport_history_heart_range1);
            case 3:
                if (this.mBgType == 1) {
                    return getResources().getColor(C0532R.color.bg_white_sport_history_heart_range2);
                }
                return getResources().getColor(C0532R.color.sport_history_heart_range2);
            case 4:
                if (this.mBgType == 1) {
                    return getResources().getColor(C0532R.color.bg_white_sport_history_heart_range3);
                }
                return getResources().getColor(C0532R.color.sport_history_heart_range3);
            case 5:
                if (this.mBgType == 1) {
                    return getResources().getColor(C0532R.color.bg_white_sport_history_heart_range4);
                }
                return getResources().getColor(C0532R.color.sport_history_heart_range4);
            default:
                return getResources().getColor(C0532R.color.transparent);
        }
    }

    private String getHeartRangeStr(int range) {
        switch (range) {
            case 1:
                return getResources().getString(C0532R.string.heart_region_warm_up_and_relax);
            case 2:
                return getResources().getString(C0532R.string.heart_region_fat_burning);
            case 3:
                return getResources().getString(C0532R.string.heart_region_heart_and_lung_enhancement);
            case 4:
                return getResources().getString(C0532R.string.heart_region_endurance_enhancement);
            case 5:
                return getResources().getString(C0532R.string.heart_region_anaerobic＿limit);
            default:
                if (range < 1) {
                    return getResources().getString(C0532R.string.sport_main_heart_info_title);
                }
                return getResources().getString(C0532R.string.sport_main_beyond_heart_info_title);
        }
    }
}
