package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.ui.view.SportKeyEventListView;
import com.huami.watch.newsport.ui.view.SportKeyEventListView.ItemFocusedListener;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.SportLinearViewUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import com.huami.watch.wearubc.UbcInterface;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class V2SportPauseContinueFragment extends Fragment {
    private static final String TAG = V2SportPauseContinueFragment.class.getName();
    private boolean isStopSport;
    private HmImageView mAPmView;
    private SportPauseContinueAdapter mAdapter;
    private List<Integer> mAdapterContentList;
    private HmImageView mBatteryView;
    private Calendar mCalendar;
    private BaseConfig mConfig;
    private int mDefaultPos;
    private Handler mHandler;
    private View mHeadView;
    private boolean mIsTraingMode;
    private SportKeyEventListView mListView;
    private UserActionListener mListener;
    private OutdoorSportSnapshot mOutdoorSportStatus;
    private TextView mPauseDisUnitView;
    private TextView mPausedDisView;
    private TextView mPausedTimeView;
    private int mSportState;
    private int mSportType;
    private HmLinearLayout mTimeView;
    private Runnable mUpdateTickerRunnable;
    private RelativeLayout rootView;

    public interface UserActionListener {
        boolean onContinueButtonClicked();

        void onHalfSecStart();

        boolean onPauseButtonClieked();

        boolean onStopButtonClicked(boolean z);
    }

    class C08101 implements ItemFocusedListener {
        C08101() {
        }

        public void hasItemFocused(boolean hasFocus) {
            LogUtil.m9i(false, V2SportPauseContinueFragment.TAG, "hasItemFocused:" + hasFocus);
        }

        public void focusedItem(int position) {
            LogUtil.m9i(false, V2SportPauseContinueFragment.TAG, "focusedItem:" + position);
            V2SportPauseContinueFragment.this.mDefaultPos = position;
            V2SportPauseContinueFragment.this.mAdapter.notifyDataSetChanged();
        }

        public void onItemClicked(int pos) {
            LogUtil.m9i(false, V2SportPauseContinueFragment.TAG, "onItemClicked:" + pos);
            View view = V2SportPauseContinueFragment.this.mListView.getChildAt(V2SportPauseContinueFragment.this.mListView.getHeaderViewsCount() + pos);
            if (view instanceof ViewGroup) {
                ((ViewGroup) view).getChildAt(0).performClick();
            } else {
                view.performClick();
            }
        }
    }

    class C08123 extends Callback {
        C08123() {
        }

        protected void doCallback(int i, Object o) {
            Debug.m5i("notify_firstbeat", "notify callback come back");
        }
    }

    class C08145 implements OnClickListener {
        C08145() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C08156 implements Runnable {
        final /* synthetic */ V2SportPauseContinueFragment this$0;

        public void run() {
            this.this$0.mAdapter.notifyDataSetChanged();
        }
    }

    private static class MyHandler extends Handler {
        private WeakReference<V2SportPauseContinueFragment> mFragmentWeakRef = null;

        public MyHandler(V2SportPauseContinueFragment fragmentWeak) {
            this.mFragmentWeakRef = new WeakReference(fragmentWeak);
        }

        public void handleMessage(Message msg) {
            V2SportPauseContinueFragment fragment = (V2SportPauseContinueFragment) this.mFragmentWeakRef.get();
            Debug.m5i(V2SportPauseContinueFragment.TAG, "SportControlStatus, handler, handle fragment----->" + fragment);
            if (fragment != null) {
                switch (msg.what) {
                    case 1:
                    case 2:
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private class SportPauseContinueAdapter extends BaseAdapter {

        private class ViewHolder {
            HmHaloButton mContentBtn;

            private ViewHolder() {
            }
        }

        public int getCount() {
            return V2SportPauseContinueFragment.this.mAdapterContentList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d(V2SportPauseContinueFragment.TAG, "getItemId : " + position);
            }
            return (long) position;
        }

        public int getItemViewType(int position) {
            return ((Integer) V2SportPauseContinueFragment.this.mAdapterContentList.get(position)).intValue();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = V2SportPauseContinueFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.fragment_v2_pause_continue_list_item, parent, false);
                holder = new ViewHolder();
                holder.mContentBtn = (HmHaloButton) convertView.findViewById(C0532R.id.continue_pause_button);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            switch (getItemViewType(position)) {
                case 0:
                    switch (V2SportPauseContinueFragment.this.mSportState) {
                        case 0:
                            holder.mContentBtn.setText(V2SportPauseContinueFragment.this.getString(C0532R.string.running_button_continue));
                            break;
                        case 1:
                            holder.mContentBtn.setText(V2SportPauseContinueFragment.this.getString(C0532R.string.running_button_pause));
                            break;
                        default:
                            break;
                    }
                case 1:
                    holder.mContentBtn.setText(V2SportPauseContinueFragment.this.getString(C0532R.string.running_button_save));
                    break;
                case 2:
                    holder.mContentBtn.setText(V2SportPauseContinueFragment.this.getString(C0532R.string.give_up_sport_record));
                    break;
                case 3:
                    holder.mContentBtn.setText(V2SportPauseContinueFragment.this.getString(C0532R.string.sport_soccer_sec_half));
                    break;
            }
            if (V2SportPauseContinueFragment.this.mDefaultPos == position) {
                holder.mContentBtn.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_positive);
            } else {
                holder.mContentBtn.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_negative);
            }
            holder.mContentBtn.setWithHalo(false);
            holder.mContentBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!V2SportPauseContinueFragment.this.isStopSport) {
                        Debug.m5i(V2SportPauseContinueFragment.TAG, "onclick: " + position);
                        switch (SportPauseContinueAdapter.this.getItemViewType(position)) {
                            case 0:
                                switch (V2SportPauseContinueFragment.this.mSportState) {
                                    case 0:
                                        V2SportPauseContinueFragment.this.continueSport();
                                        return;
                                    case 1:
                                        V2SportPauseContinueFragment.this.pauseSport();
                                        return;
                                    default:
                                        return;
                                }
                            case 1:
                                V2SportPauseContinueFragment.this.showGiveUpRecordDialog(false);
                                return;
                            case 2:
                                UbcInterface.recordCountEvent("0051");
                                V2SportPauseContinueFragment.this.showGiveUpRecordDialog(true);
                                return;
                            case 3:
                                if (V2SportPauseContinueFragment.this.mListener != null) {
                                    V2SportPauseContinueFragment.this.mListener.onHalfSecStart();
                                }
                                V2SportPauseContinueFragment.this.mAdapterContentList.remove(position);
                                V2SportPauseContinueFragment.this.mAdapter.notifyDataSetChanged();
                                return;
                            default:
                                return;
                        }
                    }
                }
            });
            return convertView;
        }
    }

    private static final class UpdateTickerRunnable implements Runnable {
        private WeakReference<V2SportPauseContinueFragment> mControlFragmentWeakReference = null;

        public UpdateTickerRunnable(V2SportPauseContinueFragment controlFragment) {
            this.mControlFragmentWeakReference = new WeakReference(controlFragment);
        }

        public void run() {
            if (this.mControlFragmentWeakReference != null) {
                V2SportPauseContinueFragment sportControlFragment = (V2SportPauseContinueFragment) this.mControlFragmentWeakReference.get();
                if (sportControlFragment != null && sportControlFragment.isAdded() && sportControlFragment.mPausedTimeView != null) {
                    sportControlFragment.mPausedTimeView.setText(DataFormatUtils.parseSecondToDefaultFormattedTime((long) sportControlFragment.mOutdoorSportStatus.getCurrentPausedSecond()));
                    if (sportControlFragment.mPausedDisView != null && sportControlFragment.mOutdoorSportStatus != null) {
                        if (SportType.isSwimMode(sportControlFragment.mSportType)) {
                            sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.sport_pause_swim_dis_unit));
                            sportControlFragment.mPausedDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) sportControlFragment.mOutdoorSportStatus.getDistance()), sportControlFragment.mConfig.getUnit())));
                        } else if (sportControlFragment.mSportType == 11) {
                            sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.skiing_downhill_desecnd));
                            sportControlFragment.mPausedDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportControlFragment.mOutdoorSportStatus.getClimbdisDescend()), false));
                        } else if (17 == sportControlFragment.mSportType) {
                            sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.sport_pause_normal_stroke_unit));
                            sportControlFragment.mPausedDisView.setText(String.valueOf(sportControlFragment.mOutdoorSportStatus.getmStrokes()));
                        } else {
                            sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.sport_pause_normal_dis_unit));
                            sportControlFragment.mPausedDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportControlFragment.mOutdoorSportStatus.getDistance()), false));
                        }
                    }
                }
            }
        }
    }

    public V2SportPauseContinueFragment() {
        this.mSportType = -1;
        this.mConfig = null;
        this.mHandler = null;
        this.mUpdateTickerRunnable = null;
        this.mOutdoorSportStatus = null;
        this.mAdapter = null;
        this.mCalendar = null;
        this.isStopSport = false;
        this.rootView = null;
        this.mAPmView = null;
        this.mTimeView = null;
        this.mListView = null;
        this.mHeadView = null;
        this.mPausedDisView = null;
        this.mPauseDisUnitView = null;
        this.mPausedTimeView = null;
        this.mSportState = 1;
        this.mDefaultPos = 0;
        this.mAdapterContentList = new ArrayList();
        this.mHandler = new MyHandler(this);
        Debug.m5i(TAG, "SportControlStatus, handler:" + this.mHandler);
    }

    public static V2SportPauseContinueFragment newInstance(int sportType, BaseConfig config) {
        V2SportPauseContinueFragment fragment = new V2SportPauseContinueFragment();
        Bundle argument = new Bundle();
        argument.putInt("sport_type", sportType);
        argument.putParcelable("config", config);
        fragment.setArguments(argument);
        Debug.m5i(TAG, "SportControlStatus, fragment:" + fragment.toString());
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (UserActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement UserActionListener");
        }
    }

    public void onDetach() {
        super.onDetach();
        this.isStopSport = false;
        this.mListener = null;
        this.mDefaultPos = 0;
    }

    public void onSaveInstanceState(Bundle outState) {
        Debug.m5i(TAG, "SportControlStatus, onSaveInstanceState:mSportState----->:");
        super.onSaveInstanceState(outState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle argument = getArguments();
        this.mSportType = argument.getInt("sport_type");
        this.mConfig = (BaseConfig) argument.getParcelable("config");
        this.mUpdateTickerRunnable = new UpdateTickerRunnable(this);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.rootView = (RelativeLayout) inflater.inflate(C0532R.layout.fragment_v2_pause_continue_layout, container, false);
        this.rootView.findViewById(C0532R.id.time_battery_contanier).setBackgroundColor(-16777216);
        this.mAPmView = (HmImageView) this.rootView.findViewById(C0532R.id.am_pm_img);
        this.mTimeView = (HmLinearLayout) this.rootView.findViewById(C0532R.id.sport_bar_time);
        this.mBatteryView = (HmImageView) this.rootView.findViewById(C0532R.id.battery_view);
        this.mListView = (SportKeyEventListView) this.rootView.findViewById(C0532R.id.list_view);
        ArcScrollbarHelper.setArcScrollBarDrawable(this.mListView);
        this.mHeadView = inflater.inflate(C0532R.layout.fragment_pause_continue_head_view, this.mListView, false);
        this.mPausedDisView = (TextView) this.mHeadView.findViewById(C0532R.id.paused_dis);
        this.mPauseDisUnitView = (TextView) this.mHeadView.findViewById(C0532R.id.paused_dis_unit);
        this.mPausedTimeView = (TextView) this.mHeadView.findViewById(C0532R.id.paused_time);
        if (!(SportType.isSportTypeNeedDis(this.mSportType) || this.mSportType == 17)) {
            this.mHeadView.findViewById(C0532R.id.splite_line).setVisibility(8);
            this.mHeadView.findViewById(C0532R.id.dis_container).setVisibility(8);
        }
        this.mSportState = UnitConvertUtils.isHuangheMode() ? 1 : 0;
        this.mDefaultPos = 0;
        this.mListView.addHeaderView(this.mHeadView, null, false);
        initAdapterContent();
        this.mAdapter = new SportPauseContinueAdapter();
        this.mListView.setAdapter(this.mAdapter);
        this.mListView.setHasFocused(false);
        this.mListView.setFocusable(false);
        this.mListView.setItemFocusedListener(new C08101());
        this.isStopSport = false;
        return this.rootView;
    }

    public void updateBattery(final int level) {
        if (!isDetached() && isAdded()) {
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    int pos = 13;
                    if (!V2SportPauseContinueFragment.this.isDetached() && V2SportPauseContinueFragment.this.isAdded()) {
                        if (V2SportPauseContinueFragment.this.mBatteryView != null) {
                            if (level / 7 <= 13) {
                                pos = level / 7;
                            }
                            V2SportPauseContinueFragment.this.mBatteryView.setBackgroundResource(BackgroundUtils.getBatteryResArray(0)[pos]);
                        }
                        V2SportPauseContinueFragment.this.reCalSysTime(V2SportPauseContinueFragment.this.mAPmView, V2SportPauseContinueFragment.this.mTimeView);
                    }
                }
            });
        }
    }

    protected void reCalSysTime(HmImageView amPmView, HmLinearLayout timeView) {
        String timeStr;
        if (this.mCalendar == null) {
            this.mCalendar = Calendar.getInstance();
        }
        this.mCalendar.setTimeInMillis(System.currentTimeMillis());
        if (DateFormat.is24HourFormat(getActivity())) {
            amPmView.setVisibility(4);
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "HH:mm", true);
        } else {
            amPmView.setVisibility(0);
            int amOrpm = this.mCalendar.get(9);
            if (amPmView != null) {
                if (amOrpm == 0) {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.am));
                } else {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.pm));
                }
            }
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "hh:mm", true);
        }
        SportLinearViewUtils.initView(getActivity(), timeView, timeStr, 0, true);
    }

    public void setIsTraingMode(boolean isTraingMode) {
        this.mIsTraingMode = isTraingMode;
    }

    public void doSportStatusUpdate(OutdoorSportSnapshot sportStatus) {
        if (isDetached() || !isAdded()) {
            Debug.m5i(TAG, "doSportStatusUpdate failed, detached or not added");
            return;
        }
        this.mOutdoorSportStatus = sportStatus;
        Global.getGlobalUIHandler().removeCallbacks(this.mUpdateTickerRunnable);
        Global.getGlobalUIHandler().post(this.mUpdateTickerRunnable);
    }

    private void continueSport() {
        if (this.mListener != null) {
            this.mListener.onContinueButtonClicked();
        }
    }

    private void pauseSport() {
        if (this.mListener != null) {
            this.mListener.onPauseButtonClieked();
        }
    }

    private void notifySesnorUpdateFirstBeatData() {
        SensorHubManagerWrapper.getInstance(getActivity()).notifySensorUpdateSportData(new C08123());
    }

    private void showGiveUpRecordDialog(boolean isGiveupThisRecord) {
        int messageId;
        Debug.m5i(TAG, "showGiveUpRecordDialog");
        final boolean isReachTargetDistance = SportDataManager.getInstance().isValidSportWhenEndSport();
        boolean isTrainingModeCompleted = false;
        if (isGiveupThisRecord) {
            messageId = C0532R.string.sport_main_give_up_this_sport;
        } else {
            if (this.mIsTraingMode) {
                TrainingInfo info = TrainingInfoManager.getInstance(getActivity()).getTrainingInfo();
                if (!Global.DEBUG_TRAINING_MODE) {
                    isTrainingModeCompleted = this.mOutdoorSportStatus != null && ((this.mOutdoorSportStatus.getDistance() >= info.getDistance() && this.mSportType == 1) || ((this.mOutdoorSportStatus.getTotalTime() >= ((long) info.getMinTime()) && (this.mSportType == 9 || this.mSportType == 14)) || (this.mSportType == 17 && this.mOutdoorSportStatus.getmStrokes() > 0)));
                } else if (this.mOutdoorSportStatus == null || ((this.mOutdoorSportStatus.getDistance() < info.getDistance() || this.mSportType != 1) && ((this.mOutdoorSportStatus.getTotalTime() < 180000 || this.mSportType != 9) && (this.mSportType != 17 || this.mOutdoorSportStatus.getmStrokes() <= 0)))) {
                    isTrainingModeCompleted = false;
                } else {
                    isTrainingModeCompleted = true;
                }
                Debug.m5i(TAG, "total time:" + (this.mOutdoorSportStatus != null ? this.mOutdoorSportStatus.getTotalTime() : 0));
            }
            if (!isReachTargetDistance) {
                messageId = this.mIsTraingMode ? C0532R.string.not_complete_train_task : this.mSportType == 14 ? C0532R.string.swimming_stop_dialog_msg_not_reach_minimum_distance : this.mSportType == 11 ? C0532R.string.skiing_stop_dialog_msg_not_reach_minimum_distance : SportType.isSportTypeNeedDis(this.mSportType) ? C0532R.string.running_stop_dialog_msg_not_reach_minimum_distance : C0532R.string.indoor_riding_time_too_short;
            } else if (!this.mIsTraingMode || isTrainingModeCompleted) {
                notifySesnorUpdateFirstBeatData();
                this.isStopSport = true;
                if (this.mListener != null) {
                    this.mListener.onStopButtonClicked(false);
                    return;
                }
                return;
            } else {
                messageId = C0532R.string.not_complete_train_task;
            }
            if (this.mSportType == 17) {
                messageId = C0532R.string.tennic_strokes_times_too_short;
            }
        }
        final boolean finalIsTrainingModeCompleted = isTrainingModeCompleted;
        Builder builder = new Builder(getActivity()).setMessage(messageId).setPositiveButton(getString(C0532R.string.decision_no), new C08145()).setNegativeButton(getString(C0532R.string.decision_yes), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (V2SportPauseContinueFragment.this.mIsTraingMode && !finalIsTrainingModeCompleted && isReachTargetDistance) {
                    V2SportPauseContinueFragment.this.notifySesnorUpdateFirstBeatData();
                    if (V2SportPauseContinueFragment.this.mListener != null) {
                        V2SportPauseContinueFragment.this.mListener.onStopButtonClicked(false);
                    }
                } else if (V2SportPauseContinueFragment.this.mListener != null) {
                    V2SportPauseContinueFragment.this.mListener.onStopButtonClicked(true);
                }
                V2SportPauseContinueFragment.this.isStopSport = true;
                dialog.dismiss();
            }
        }).setSystemType(true);
        if (!UnitConvertUtils.isZh()) {
            builder.setMessageTextSize(15.0f);
        }
        builder.create().show();
    }

    private void initAdapterContent() {
        this.mAdapterContentList.clear();
        this.mAdapterContentList.add(Integer.valueOf(0));
        OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) SportDataManager.getInstance().getSportSnapshot();
        if (this.mSportType == 18 && snapshot != null && snapshot.getSecHalfStartTime() == -1) {
            this.mAdapterContentList.add(Integer.valueOf(3));
        }
        this.mAdapterContentList.add(Integer.valueOf(1));
        this.mAdapterContentList.add(Integer.valueOf(2));
    }
}
