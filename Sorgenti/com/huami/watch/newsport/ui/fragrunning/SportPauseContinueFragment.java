package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.ui.view.HmSportLayout;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnDismissedListener;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnSwipeProgressChangedListener;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.SportLinearViewUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.lang.ref.WeakReference;
import java.util.Calendar;

@Deprecated
public class SportPauseContinueFragment extends Fragment {
    public volatile int currnetMFinishStatus;
    private boolean isReachTargetDistance;
    private HmImageView mAPmView;
    private HmImageView mBatteryView;
    private Calendar mCalendar;
    private BaseConfig mConfig;
    private HmHaloButton mContinuePauseButtonTitleView;
    private volatile int mCurrnetSelectStatus;
    private Animation mDialogDismissAnim;
    private OnClickListener mDialogDismissListener;
    private TextView mDialogMsg;
    private HmHaloButton mDialogNegativeButton;
    private HmHaloButton mDialogPositiveButton;
    private Animation mDialogShowAnim;
    private HmSportLayout mDialogView;
    private Handler mHandler;
    private boolean mIsTraingMode;
    private boolean mIsViewShownPause;
    private UserActionListener mListener;
    private OutdoorSportSnapshot mOutdoorSportStatus;
    private OnClickListener mPauseContinueClickListener;
    private TextView mPauseDisUnitView;
    private HmLinearLayout mPauseTimeLinear;
    private TextView mPausedDisView;
    private TextView mPausedTextView;
    private TextView mPausedTimeView;
    private int mSportType;
    private HmHaloButton mStopButton;
    private HmLinearLayout mTimeView;
    private Runnable mUpdateTickerRunnable;
    private RelativeLayout view;

    class AnonymousClass12 implements Runnable {
        final /* synthetic */ SportPauseContinueFragment this$0;
        final /* synthetic */ int val$level;

        public void run() {
            int pos = 13;
            if (!this.this$0.isDetached() && this.this$0.isAdded()) {
                if (this.this$0.mBatteryView != null) {
                    if (this.val$level / 7 <= 13) {
                        pos = this.val$level / 7;
                    }
                    int[] battery = BackgroundUtils.getBatteryResArray(0);
                    this.this$0.mBatteryView.setBackgroundResource(battery[pos]);
                    if (this.this$0.mDialogView != null && this.this$0.mDialogView.getVisibility() == 0) {
                        this.this$0.mDialogView.findViewById(C0532R.id.battery_view).setBackgroundResource(battery[pos]);
                    }
                }
                this.this$0.reCalSysTime(this.this$0.mAPmView, this.this$0.mTimeView);
                if (this.this$0.mDialogView != null && this.this$0.mDialogView.getVisibility() == 0) {
                    this.this$0.reCalSysTime((HmImageView) this.this$0.mDialogView.findViewById(C0532R.id.am_pm_img), (HmLinearLayout) this.this$0.mDialogView.findViewById(C0532R.id.sport_bar_time));
                }
            }
        }
    }

    class C07851 implements OnDismissedListener {
        C07851() {
        }

        public void onDismissed(HmSportLayout view) {
            if (SportPauseContinueFragment.this.mListener != null) {
                SportPauseContinueFragment.this.mListener.onInStopConfirmUI(false);
            }
            SportPauseContinueFragment.this.mDialogView.setVisibility(8);
        }
    }

    class C07862 implements OnSwipeProgressChangedListener {
        C07862() {
        }

        public void onSwipeProgressChanged(HmSportLayout view, float progress, float deltx) {
            if (deltx >= 0.0f && SportPauseContinueFragment.this.mDialogView.getVisibility() == 0) {
                SportPauseContinueFragment.this.mDialogView.setAlpha(1.0f - progress);
            }
        }

        public void onSwipeCancelled(HmSportLayout view) {
        }
    }

    class C07873 implements OnClickListener {
        C07873() {
        }

        public void onClick(View v) {
            if (SportPauseContinueFragment.this.mListener != null) {
                SportPauseContinueFragment.this.mListener.onInStopConfirmUI(true);
            }
            SportPauseContinueFragment.this.setmCurrnetSelectStatus(1);
            SportPauseContinueFragment.this.onStopClick(SportPauseContinueFragment.this.getActivity());
        }
    }

    class C07884 extends Callback {
        C07884() {
        }

        protected void doCallback(int i, Object o) {
            Debug.m5i("notify_firstbeat", "notify callback come back");
        }
    }

    class C07895 implements OnClickListener {
        C07895() {
        }

        public void onClick(View v) {
            if (SportPauseContinueFragment.this.mListener != null) {
                Debug.m5i("SportPauseContinueFragment", "SportControlStatus, click:state--->");
                if (SportPauseContinueFragment.this.mListener != null) {
                    SportPauseContinueFragment.this.mListener.onInStopConfirmUI(false);
                }
                SportPauseContinueFragment.this.setmCurrnetSelectStatus(0);
                SportPauseContinueFragment.this.mListener.onContinueButtonClicked();
            }
        }
    }

    class C07906 implements OnClickListener {
        C07906() {
        }

        public void onClick(View v) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportPauseContinueFragment", "positive button clicked, reached minimum distance.");
            }
            SportPauseContinueFragment.this.prepareStop();
        }
    }

    class C07917 implements OnClickListener {
        C07917() {
        }

        public void onClick(View v) {
            if (SportPauseContinueFragment.this.mListener != null) {
                SportPauseContinueFragment.this.mListener.onInStopConfirmUI(false);
            }
            SportPauseContinueFragment.this.dismissDialog();
        }
    }

    class C07928 implements OnClickListener {
        C07928() {
        }

        public void onClick(View v) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportPauseContinueFragment", "positive button clicked. not reach minimum distance.");
            }
            SportPauseContinueFragment.this.prepareStop();
        }
    }

    class C07939 implements OnClickListener {
        C07939() {
        }

        public void onClick(View v) {
            if (SportPauseContinueFragment.this.mListener != null) {
                SportPauseContinueFragment.this.mListener.onInStopConfirmUI(false);
            }
            SportPauseContinueFragment.this.dismissDialog();
        }
    }

    private static class MyHandler extends Handler {
        private WeakReference<SportPauseContinueFragment> mFragmentWeakRef = null;

        public MyHandler(SportPauseContinueFragment fragmentWeak) {
            this.mFragmentWeakRef = new WeakReference(fragmentWeak);
        }

        public void handleMessage(Message msg) {
            SportPauseContinueFragment fragment = (SportPauseContinueFragment) this.mFragmentWeakRef.get();
            Debug.m5i("SportPauseContinueFragment", "SportControlStatus, handler, handle fragment----->" + fragment);
            if (fragment != null) {
                switch (msg.what) {
                    case 1:
                        return;
                    case 2:
                        fragment.onChangeToPausedState();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private static final class UpdateTickerRunnable implements Runnable {
        private WeakReference<SportPauseContinueFragment> mControlFragmentWeakReference = null;

        public UpdateTickerRunnable(SportPauseContinueFragment controlFragment) {
            this.mControlFragmentWeakReference = new WeakReference(controlFragment);
        }

        public void run() {
            if (this.mControlFragmentWeakReference != null) {
                SportPauseContinueFragment sportControlFragment = (SportPauseContinueFragment) this.mControlFragmentWeakReference.get();
                if (sportControlFragment != null && sportControlFragment.isAdded() && sportControlFragment.mPausedTimeView != null) {
                    int tickerValue = sportControlFragment.mOutdoorSportStatus.getCurrentPausedSecond();
                    sportControlFragment.mPausedTimeView.setText(DataFormatUtils.parseSecondToDefaultFormattedTime((long) tickerValue));
                    if (sportControlFragment.mPauseTimeLinear != null) {
                        SportLinearViewUtils.initView(sportControlFragment.getActivity(), sportControlFragment.mPauseTimeLinear, DataFormatUtils.parseSecondToDefaultFormattedTime((long) tickerValue), 0, false);
                    }
                    if (sportControlFragment.mPausedDisView != null && sportControlFragment.mOutdoorSportStatus != null) {
                        if (SportType.isSwimMode(sportControlFragment.mSportType)) {
                            sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.sport_pause_swim_dis_unit));
                            sportControlFragment.mPausedDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) sportControlFragment.mOutdoorSportStatus.getDistance(), sportControlFragment.mConfig.getUnit())));
                            return;
                        }
                        sportControlFragment.mPauseDisUnitView.setText(sportControlFragment.getString(C0532R.string.sport_pause_normal_dis_unit));
                        sportControlFragment.mPausedDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) sportControlFragment.mOutdoorSportStatus.getDistance()), false));
                    }
                }
            }
        }
    }

    public interface UserActionListener {
        boolean onContinueButtonClicked();

        void onInStopConfirmUI(boolean z);

        boolean onStopButtonClicked(boolean z);

        void onTrainStatusChange(boolean z);
    }

    public SportPauseContinueFragment() {
        this.mPausedTextView = null;
        this.mPausedTimeView = null;
        this.mPauseTimeLinear = null;
        this.mPausedDisView = null;
        this.mPauseDisUnitView = null;
        this.mAPmView = null;
        this.mTimeView = null;
        this.mContinuePauseButtonTitleView = null;
        this.mStopButton = null;
        this.mHandler = null;
        this.mSportType = -1;
        this.mConfig = null;
        this.mIsViewShownPause = true;
        this.isReachTargetDistance = true;
        this.mOutdoorSportStatus = null;
        this.mUpdateTickerRunnable = null;
        this.view = null;
        this.currnetMFinishStatus = 2;
        this.mCurrnetSelectStatus = 0;
        this.mPauseContinueClickListener = new C07895();
        this.mDialogDismissListener = new OnClickListener() {
            public void onClick(View v) {
                SportPauseContinueFragment.this.dismissDialog();
            }
        };
        this.mCalendar = null;
        this.mHandler = new MyHandler(this);
        Debug.m5i("SportPauseContinueFragment", "SportControlStatus, handler:" + this.mHandler);
    }

    public void onSaveInstanceState(Bundle outState) {
        Debug.m5i("SportPauseContinueFragment", "SportControlStatus, onSaveInstanceState:mSportState----->:");
        super.onSaveInstanceState(outState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle argument = getArguments();
        this.mSportType = argument.getInt("sport_type");
        this.mConfig = (BaseConfig) argument.getParcelable("config");
        this.mUpdateTickerRunnable = new UpdateTickerRunnable(this);
    }

    public synchronized View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Debug.m5i("SportPauseContinueFragment", "SportControlStatus, onCreateView");
        if (SportType.isSportTypeNeedDis(this.mSportType)) {
            this.view = (RelativeLayout) inflater.inflate(C0532R.layout.fragment_sport_control_with_distance, container, false);
            this.mPausedDisView = (TextView) this.view.findViewById(C0532R.id.paused_dis);
            this.mPauseDisUnitView = (TextView) this.view.findViewById(C0532R.id.paused_dis_unit);
        } else {
            this.view = (RelativeLayout) inflater.inflate(C0532R.layout.fragment_sport_control, container, false);
        }
        this.view.setTag("tag_sport_control");
        this.mAPmView = (HmImageView) this.view.findViewById(C0532R.id.am_pm_img);
        this.mTimeView = (HmLinearLayout) this.view.findViewById(C0532R.id.sport_bar_time);
        this.mBatteryView = (HmImageView) this.view.findViewById(C0532R.id.battery_view);
        this.mContinuePauseButtonTitleView = (HmHaloButton) this.view.findViewById(C0532R.id.continue_pause_button_title);
        this.mStopButton = (HmHaloButton) this.view.findViewById(C0532R.id.stop_button);
        this.mPausedTextView = (TextView) this.view.findViewById(C0532R.id.paused_text);
        this.mPausedTimeView = (TextView) this.view.findViewById(C0532R.id.paused_time);
        this.mDialogView = (HmSportLayout) this.view.findViewById(C0532R.id.dialog_container);
        this.mDialogView.setVisibility(8);
        this.mDialogMsg = (TextView) this.mDialogView.findViewById(C0532R.id.message);
        this.mDialogPositiveButton = (HmHaloButton) this.mDialogView.findViewById(C0532R.id.button1);
        this.mDialogPositiveButton.setOnClickListener(this.mDialogDismissListener);
        this.mDialogNegativeButton = (HmHaloButton) this.mDialogView.findViewById(C0532R.id.button2);
        this.mDialogNegativeButton.setOnClickListener(this.mDialogDismissListener);
        ((HmHaloButton) this.view.findViewById(C0532R.id.continue_pause_button_title)).setWithHalo(false);
        ((HmHaloButton) this.view.findViewById(C0532R.id.stop_button)).setWithHalo(false);
        this.mDialogPositiveButton.setWithHalo(false);
        this.mDialogNegativeButton.setWithHalo(false);
        this.mDialogView.setOnDismissedListener(new C07851());
        this.mDialogView.setAppOnSwipeProgressChangedListener(new C07862());
        this.mStopButton.setOnClickListener(new C07873());
        initPausedUI();
        this.mContinuePauseButtonTitleView.setOnClickListener(this.mPauseContinueClickListener);
        changeToPausedState();
        if (SportType.isSwimMode(this.mSportType)) {
            if (this.mPausedDisView != null) {
                this.mPausedDisView.setText(getString(C0532R.string.default_int_value));
            }
            if (this.mPauseDisUnitView != null) {
                this.mPauseDisUnitView.setText(getString(C0532R.string.sport_pause_swim_dis_unit));
            }
        } else {
            if (this.mPausedDisView != null) {
                this.mPausedDisView.setText(getString(C0532R.string.default_double_value));
            }
            if (this.mPauseDisUnitView != null) {
                this.mPauseDisUnitView.setText(getString(C0532R.string.sport_pause_normal_dis_unit));
            }
        }
        return this.view;
    }

    public void onDestroy() {
        super.onDestroy();
        this.mHandler.removeCallbacksAndMessages(null);
    }

    public synchronized void changeToPausedState() {
        this.mHandler.removeCallbacksAndMessages(null);
        this.mHandler.sendEmptyMessage(2);
    }

    private void popupStopAlertDialog(Context context, int msgRes, int positiveButtonTextRes, OnClickListener positiveClickListener, int negativeButtonTextRes, OnClickListener negativeClickListener, boolean animate) {
        setmCurrnetFinishSelectStatus(2);
        if (this.mDialogMsg != null) {
            this.mDialogMsg.setText(msgRes);
            if (positiveClickListener != null) {
                this.mDialogPositiveButton.setOnClickListener(positiveClickListener);
            }
            if (negativeClickListener != null) {
                this.mDialogNegativeButton.setOnClickListener(negativeClickListener);
            }
            if (this.mDialogShowAnim == null) {
                this.mDialogShowAnim = AnimationUtils.loadAnimation(getActivity(), C0532R.anim.grow_fade_in_center);
            }
            this.mDialogView.setVisibility(0);
            this.mDialogView.setAlpha(1.0f);
            if (animate) {
                this.mDialogView.startAnimation(this.mDialogShowAnim);
            }
            SensorHubManagerWrapper.getInstance(getActivity()).notifySensorUpdateSportData(new C07884());
        }
    }

    public synchronized void onChangeToPausedState() {
        Debug.m3d("SportPauseContinueFragment", "onChangeToPausedState");
        if (this.mContinuePauseButtonTitleView == null) {
            Debug.m6w("SportPauseContinueFragment", "continue button is null");
        } else {
            initPausedUI();
        }
    }

    private synchronized void initPausedUI() {
        if (this.mContinuePauseButtonTitleView != null) {
            this.mIsViewShownPause = false;
            this.mContinuePauseButtonTitleView.setText(C0532R.string.running_button_continue);
            if (this.mPauseTimeLinear != null) {
                SportLinearViewUtils.initView(getActivity(), this.mPauseTimeLinear, getActivity().getString(C0532R.string.default_time_value), 0, false);
            } else {
                this.mPausedTimeView.setText(C0532R.string.default_time_value);
                this.mPausedTimeView.setVisibility(0);
                this.mPausedTextView.setVisibility(0);
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Debug.m5i("SportPauseContinueFragment", "SportControlStatus, onConfigurationChanged---->newConfig" + newConfig.toString());
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (UserActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement UserActionListener");
        }
    }

    public void onStopClick(Context context) {
        onStopClick(context, true);
    }

    public void onStopClick(Context context, boolean animate) {
        int i = C0532R.string.running_stop_dialog_msg_reach_minimum_distance;
        this.isReachTargetDistance = SportDataManager.getInstance().isValidSportWhenEndSport();
        boolean isTrainingModeCompleted = false;
        if (this.mIsTraingMode) {
            TrainingInfo info = TrainingInfoManager.getInstance(getActivity()).getTrainingInfo();
            if (!Global.DEBUG_TRAINING_MODE) {
                isTrainingModeCompleted = this.mOutdoorSportStatus != null && ((this.mOutdoorSportStatus.getDistance() >= info.getDistance() && this.mSportType == 1) || (this.mOutdoorSportStatus.getTotalTime() >= ((long) info.getMinTime()) && this.mSportType == 9));
            } else if (this.mOutdoorSportStatus == null || ((this.mOutdoorSportStatus.getDistance() < info.getDistance() || this.mSportType != 1) && (this.mOutdoorSportStatus.getTotalTime() < 180000 || this.mSportType != 9))) {
                isTrainingModeCompleted = false;
            } else {
                isTrainingModeCompleted = true;
            }
            Debug.m5i("SportPauseContinueFragment", "total time:" + (this.mOutdoorSportStatus != null ? this.mOutdoorSportStatus.getTotalTime() : 0));
        }
        if (this.mListener != null) {
            this.mListener.onTrainStatusChange(isTrainingModeCompleted);
        }
        if (this.isReachTargetDistance) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportPauseContinueFragment", "reach minimum distance. current status " + this.mOutdoorSportStatus);
            }
            if (this.mIsTraingMode && !isTrainingModeCompleted) {
                i = C0532R.string.not_complete_train_task;
            }
            popupStopAlertDialog(context, i, C0532R.string.running_stop_dialog_positive_text, new C07906(), C0532R.string.running_stop_dialog_negative_text, new C07917(), animate);
            return;
        }
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportPauseContinueFragment", "not reach minimum distance. current status " + this.mOutdoorSportStatus);
        }
        i = this.mIsTraingMode ? C0532R.string.not_complete_train_task : SportType.isSportTypeNeedDis(this.mSportType) ? C0532R.string.running_stop_dialog_msg_not_reach_minimum_distance : C0532R.string.indoor_riding_time_too_short;
        popupStopAlertDialog(context, i, C0532R.string.running_stop_dialog_positive_text, new C07928(), C0532R.string.running_stop_dialog_negative_text, new C07939(), animate);
    }

    private void prepareStop() {
        if (this.mListener != null) {
            this.mListener.onStopButtonClicked(this.isReachTargetDistance);
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void dismissDialog() {
        if (this.mDialogView != null) {
            if (this.mDialogDismissAnim == null) {
                this.mDialogDismissAnim = AnimationUtils.loadAnimation(getActivity(), C0532R.anim.shrink_fade_out_center);
                this.mDialogDismissAnim.setAnimationListener(new AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        SportPauseContinueFragment.this.mDialogView.setVisibility(8);
                        SportPauseContinueFragment.this.mDialogPositiveButton.setOnClickListener(SportPauseContinueFragment.this.mDialogDismissListener);
                        SportPauseContinueFragment.this.mDialogNegativeButton.setOnClickListener(SportPauseContinueFragment.this.mDialogDismissListener);
                    }
                });
            }
            this.mDialogView.startAnimation(this.mDialogDismissAnim);
        }
    }

    public void setmCurrnetSelectStatus(int mCurrnetSelectStatus) {
        this.mCurrnetSelectStatus = mCurrnetSelectStatus;
        switch (this.mCurrnetSelectStatus) {
            case 0:
                this.mContinuePauseButtonTitleView.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
                this.mStopButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
                return;
            case 1:
                this.mContinuePauseButtonTitleView.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
                this.mStopButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
                return;
            default:
                return;
        }
    }

    public void setmCurrnetFinishSelectStatus(int finishSelectStatus) {
        this.currnetMFinishStatus = finishSelectStatus;
        switch (this.currnetMFinishStatus) {
            case 2:
                this.mDialogPositiveButton.setBackground(getActivity().getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
                this.mDialogNegativeButton.setBackground(getActivity().getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
                return;
            case 3:
                this.mDialogNegativeButton.setBackground(getActivity().getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
                this.mDialogPositiveButton.setBackground(getActivity().getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
                return;
            default:
                return;
        }
    }

    protected void reCalSysTime(HmImageView amPmView, HmLinearLayout timeView) {
        String timeStr;
        if (this.mCalendar == null) {
            this.mCalendar = Calendar.getInstance();
        }
        this.mCalendar.setTimeInMillis(System.currentTimeMillis());
        if (DateFormat.is24HourFormat(getActivity())) {
            amPmView.setVisibility(4);
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "HH:mm", true);
        } else {
            amPmView.setVisibility(0);
            int amOrpm = this.mCalendar.get(9);
            if (amPmView != null) {
                if (amOrpm == 0) {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.am));
                } else {
                    amPmView.setBackground(getResources().getDrawable(C0532R.drawable.pm));
                }
            }
            timeStr = DataFormatUtils.parseMilliSecondToFormattedTime(this.mCalendar.getTimeInMillis(), "hh:mm", true);
        }
        SportLinearViewUtils.initView(getActivity(), timeView, timeStr, 0, true);
    }
}
