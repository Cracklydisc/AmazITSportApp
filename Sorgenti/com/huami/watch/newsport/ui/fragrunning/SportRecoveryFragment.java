package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.utils.SportLinearViewUtils;

@Deprecated
public class SportRecoveryFragment extends Fragment implements OnClickListener {
    private View mConfirmView = null;
    private IRecoveryListener mListener = null;
    private long mRecoveryStartTime = -1;
    private LinearLayout mRecoveryTimeView = null;

    public interface IRecoveryListener {
        void onRecoveryConfirm();
    }

    public static synchronized SportRecoveryFragment newInstance() {
        SportRecoveryFragment fragment;
        synchronized (SportRecoveryFragment.class) {
            fragment = new SportRecoveryFragment();
        }
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IRecoveryListener) {
            this.mListener = (IRecoveryListener) activity;
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_recovery_layout, container, false);
        this.mConfirmView = root.findViewById(C0532R.id.recovery_confirm_container);
        this.mRecoveryTimeView = (LinearLayout) root.findViewById(C0532R.id.recovery_time_container);
        SportLinearViewUtils.initView(getActivity(), this.mRecoveryTimeView, String.format("%02d", new Object[]{Integer.valueOf(3)}), 0, false);
        this.mConfirmView.setOnClickListener(this);
        this.mRecoveryStartTime = System.currentTimeMillis();
        return root;
    }

    public void onDestroy() {
        super.onDestroy();
        DataManager.getInstance().updateSportRecoveryTime(this.mRecoveryStartTime, 10800000);
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onClick(View v) {
        if (v.getId() == C0532R.id.recovery_confirm_container && this.mListener != null) {
            this.mListener.onRecoveryConfirm();
        }
    }
}
