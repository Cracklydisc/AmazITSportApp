package com.huami.watch.newsport.ui.fragrunning;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.VibratorUtil;
import java.lang.ref.WeakReference;

public class SportCountDownFragment extends Fragment {
    private static final long[] VIBRATOR_TIME = new long[]{0, 500};
    private NumberTextView mFirstCountView;
    private Handler mHandler;
    private OnCountDownListener mListener;
    private NumberTextView mSecondCountView;
    private int mStartupCount;
    private NumberTextView mThirdCountView;
    private TextView mTintView;
    private Vibrator mVibrator;

    public interface OnCountDownListener {
        void onCountFinished();
    }

    class C07791 implements OnTouchListener {
        C07791() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    class C07802 implements AnimatorUpdateListener {
        C07802() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float current = ((Float) animation.getAnimatedValue()).floatValue();
            float alpha = 1.0f - current;
            float translation = current * 10.0f;
            if (SportCountDownFragment.this.mStartupCount == 3) {
                SportCountDownFragment.this.mThirdCountView.setAlpha(alpha);
                SportCountDownFragment.this.mThirdCountView.setTranslationY(translation);
            } else if (SportCountDownFragment.this.mStartupCount == 2) {
                SportCountDownFragment.this.mSecondCountView.setAlpha(alpha);
                SportCountDownFragment.this.mSecondCountView.setTranslationY(translation);
            } else if (SportCountDownFragment.this.mStartupCount == 1) {
                SportCountDownFragment.this.mFirstCountView.setAlpha(alpha);
                SportCountDownFragment.this.mFirstCountView.setScaleY(alpha);
                SportCountDownFragment.this.mTintView.setAlpha(alpha);
            }
        }
    }

    class C07824 implements AnimatorUpdateListener {
        C07824() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float current = ((Float) animation.getAnimatedValue()).floatValue();
            float downAlpha = current;
            float downTranslation = -((1.0f - current) * 10.0f);
            if (SportCountDownFragment.this.mStartupCount == 3) {
                SportCountDownFragment.this.mThirdCountView.setAlpha(downAlpha);
                SportCountDownFragment.this.mThirdCountView.setTranslationY(downTranslation);
            } else if (SportCountDownFragment.this.mStartupCount == 2) {
                SportCountDownFragment.this.mSecondCountView.setAlpha(downAlpha);
                SportCountDownFragment.this.mSecondCountView.setTranslationY(downTranslation);
            } else if (SportCountDownFragment.this.mStartupCount == 1) {
                SportCountDownFragment.this.mFirstCountView.setAlpha(downAlpha);
                SportCountDownFragment.this.mFirstCountView.setTranslationY(downTranslation);
            }
        }
    }

    private static final class CountDownHandler extends Handler {
        private WeakReference<SportCountDownFragment> mFragment = null;

        public CountDownHandler(SportCountDownFragment mFragment) {
            this.mFragment = new WeakReference(mFragment);
        }

        public void handleMessage(Message msg) {
            SportCountDownFragment fragment = (SportCountDownFragment) this.mFragment.get();
            if (fragment != null) {
                switch (msg.what) {
                    case 1:
                        fragment.showAnimation();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public SportCountDownFragment() {
        this.mStartupCount = 3;
        this.mHandler = null;
        this.mFirstCountView = null;
        this.mSecondCountView = null;
        this.mThirdCountView = null;
        this.mTintView = null;
        this.mVibrator = null;
        this.mHandler = new CountDownHandler(this);
    }

    public static SportCountDownFragment newInstance() {
        return new SportCountDownFragment();
    }

    private void refreshView() {
        if (this.mStartupCount == 3) {
            this.mThirdCountView.setVisibility(0);
            this.mSecondCountView.setVisibility(0);
            this.mFirstCountView.setVisibility(4);
            this.mSecondCountView.setAlpha(0.0f);
        } else if (this.mStartupCount == 2) {
            this.mFirstCountView.setVisibility(0);
            this.mSecondCountView.setVisibility(0);
            this.mThirdCountView.setVisibility(4);
            this.mFirstCountView.setAlpha(0.0f);
        } else if (this.mStartupCount == 1) {
            this.mThirdCountView.setVisibility(4);
            this.mSecondCountView.setVisibility(4);
            this.mFirstCountView.setVisibility(0);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(C0532R.layout.fragment_sport_count_down, container, false);
        this.mFirstCountView = (NumberTextView) view.findViewById(C0532R.id.count_text_one_view);
        this.mTintView = (TextView) view.findViewById(C0532R.id.tint);
        this.mFirstCountView.setPivotY(82.0f);
        this.mSecondCountView = (NumberTextView) view.findViewById(C0532R.id.count_text_two_view);
        this.mThirdCountView = (NumberTextView) view.findViewById(C0532R.id.count_text_three_view);
        this.mThirdCountView.setAlpha(0.0f);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(getActivity(), (ViewGroup) view);
        view.setOnTouchListener(new C07791());
        return view;
    }

    public void startCount() {
        this.mStartupCount = 3;
        this.mHandler.sendEmptyMessage(1);
    }

    private void showAnimation() {
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        final ValueAnimator downAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        valueAnimator.setDuration(200);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setStartDelay(800);
        valueAnimator.addUpdateListener(new C07802());
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animation) {
                SportCountDownFragment.this.refreshView();
            }

            public void onAnimationEnd(Animator animation) {
                SportCountDownFragment.this.mStartupCount = SportCountDownFragment.this.mStartupCount - 1;
                if (SportCountDownFragment.this.mStartupCount > 0) {
                    downAnimator.start();
                } else if (SportCountDownFragment.this.mListener != null) {
                    SportCountDownFragment.this.setVibrator();
                    SportCountDownFragment.this.mListener.onCountFinished();
                }
            }
        });
        downAnimator.setDuration(200);
        downAnimator.setInterpolator(new LinearInterpolator());
        downAnimator.addUpdateListener(new C07824());
        downAnimator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animation) {
                SportCountDownFragment.this.setVibrator();
            }

            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                valueAnimator.start();
            }
        });
        downAnimator.start();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCountDownListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCountDownListener");
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void setVibrator() {
        if (this.mVibrator == null) {
            this.mVibrator = (Vibrator) getActivity().getSystemService("vibrator");
        }
        this.mVibrator.vibrate(VIBRATOR_TIME, -1, VibratorUtil.getMostStrongAttributes());
    }
}
