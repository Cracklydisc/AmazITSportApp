package com.huami.watch.newsport.ui.fragrunning;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.gps.callback.IPointProvider;
import com.huami.watch.newsport.gps.controller.PointProviderManager;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.reminder.impl.RouteOffsetRemind;
import com.huami.watch.newsport.route.action.ITrailBitmapProvider;
import com.huami.watch.newsport.route.action.ITrailBitmapProvider.INavigationOritationListener;
import com.huami.watch.newsport.route.controller.CompassManager;
import com.huami.watch.newsport.route.controller.CompassManager.OnSensorChangeListener;
import com.huami.watch.newsport.route.controller.RouteTrailManager;
import com.huami.watch.newsport.ui.view.CompassView;
import com.huami.watch.newsport.ui.view.NewNumberTextView;
import com.huami.watch.newsport.ui.view.NoTrailView;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.SlptRouteManager;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class SportRouteFragment extends Fragment implements OnClickListener, INavigationOritationListener {
    private static final String TAG = SportRouteFragment.class.getName();
    private static AtomicBoolean routeOffsetStatus = new AtomicBoolean(false);
    private static int sRunningCount = 0;
    private RouteOffsetRemind dialog = null;
    private HmTextView hmRemindOffsetView = null;
    private int mBgType = 0;
    private TextView mClimbUpView = null;
    private CompassView mCompassView = null;
    private TextView mCurAltView = null;
    private TextView mCurLat = null;
    private TextView mCurLng = null;
    private Bitmap mEndBitmap = null;
    private View mEnlargeBtn = null;
    private ImageView mEnlargeImg = null;
    private boolean mIsDestroyed = false;
    private boolean mIsFirstShowRoute = true;
    private boolean mIsForeground = false;
    private boolean mIsNeedRefreshView = false;
    private volatile boolean mIsProccessing = false;
    private AtomicBoolean mIsScaling = new AtomicBoolean(false);
    private boolean mIsTrailShowed;
    private View mNarrowBtn = null;
    private ImageView mNarrowImg = null;
    private Bitmap mNaviEndBitmap = null;
    private Bitmap mNaviStartBitmap = null;
    private NoTrailView mNoTrailView = null;
    private IPointProvider mPointProvider = null;
    private Runnable mRefreshTrailRunnable = new RefreshViewRunnable(this);
    private Runnable mSetTrailRunnable = new SetTrailRunnable(this);
    private SlptRouteManager mSlptRouteManager = null;
    private int mSportType = -1;
    private Bitmap mStartBitmap = null;
    private TextView mTitleView = null;
    private Bitmap mTrailBitmap = null;
    private ITrailBitmapProvider mTrailBitmapProvider = null;
    private ImageView mTrailView = null;
    protected ValueAnimator mValueAnimator = null;

    class C07951 implements OnSensorChangeListener {

        class C07941 implements Runnable {
            C07941() {
            }

            public void run() {
                if (!SportRouteFragment.this.mIsDestroyed) {
                    SportRouteFragment.this.mTrailBitmapProvider.drawOritationBitmap(SportRouteFragment.this.mTrailBitmap);
                    SportRouteFragment.this.mTrailView.setBackground(new BitmapDrawable(SportRouteFragment.this.getResources(), SportRouteFragment.this.mTrailBitmap));
                    SportRouteFragment.this.mIsProccessing = false;
                }
            }
        }

        C07951() {
        }

        public void onChange(float degree) {
            if (SportRouteFragment.this.mIsForeground) {
                if (SportRouteFragment.this.mSlptRouteManager != null) {
                    Debug.m3d(SportRouteFragment.TAG, "sensor value :" + degree);
                    Debug.m3d(SportRouteFragment.TAG, "mSlptRouteManager input value: " + (360.0f - degree));
                    SportRouteFragment.this.mSlptRouteManager.rotateMap((double) (360.0f - degree), 1000);
                }
                if (SportRouteFragment.this.mCompassView != null) {
                    SportRouteFragment.this.mCompassView.onRotate(360.0f - degree);
                    if (!SportRouteFragment.this.mIsProccessing) {
                        SportRouteFragment.this.mIsProccessing = true;
                        Global.getGlobalUIHandler().post(new C07941());
                    }
                }
            }
        }
    }

    class C07962 implements Runnable {
        C07962() {
        }

        public void run() {
            if (SportRouteFragment.this.mTrailBitmapProvider != null) {
                SportRouteFragment.this.mTrailBitmapProvider.stop();
                SportRouteFragment.this.mTrailBitmapProvider = null;
            }
        }
    }

    class C07984 implements AnimatorUpdateListener {
        private boolean mIsReset = false;

        C07984() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (SportRouteFragment.this.isDetached() || !SportRouteFragment.this.isAdded()) {
                Debug.m5i(SportRouteFragment.TAG, "fragment is detached");
                return;
            }
            int value = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            if (value < 1000) {
                SportRouteFragment.this.mNoTrailView.update(0.0f, (value * 65) / 1000, 64 - ((value * 64) / 1000), 117 - ((value * 117) / 1000));
                this.mIsReset = false;
            } else if (!this.mIsReset) {
                SportRouteFragment.this.mNoTrailView.update(0.0f, 0, 0, 0);
                this.mIsReset = true;
            }
        }
    }

    class C07995 implements Runnable {
        C07995() {
        }

        public void run() {
            if (SportRouteFragment.this.isAdded()) {
                SportRouteFragment.this.cancelNoTrailAnimation();
                if (SportRouteFragment.this.mNoTrailView.getVisibility() != 8 || SportRouteFragment.this.mTrailView.getVisibility() != 0) {
                    SportRouteFragment.this.mNoTrailView.setVisibility(8);
                    SportRouteFragment.this.mTrailView.setVisibility(0);
                    SportRouteFragment.this.mTrailBitmapProvider.drawOnBitmap(SportRouteFragment.this.mTrailBitmap);
                    SportRouteFragment.this.mTrailView.setImageDrawable(new BitmapDrawable(SportRouteFragment.this.getResources(), SportRouteFragment.this.mTrailBitmap));
                }
            }
        }
    }

    private static final class RefreshViewRunnable implements Runnable {
        private WeakReference<SportRouteFragment> mFragmentRef = null;

        public RefreshViewRunnable(SportRouteFragment fragment) {
            this.mFragmentRef = new WeakReference(fragment);
        }

        public void run() {
            SportRouteFragment fragment = (SportRouteFragment) this.mFragmentRef.get();
            if (fragment != null && fragment.isAdded()) {
                if (Global.DEBUG_LEVEL_3) {
                    Debug.m3d(SportRouteFragment.TAG, "is show whole trail " + fragment.mIsTrailShowed + ". ");
                }
                double[] location;
                if (fragment.mPointProvider.pointSize() <= 1) {
                    location = fragment.getLastLocation(fragment.mPointProvider);
                    if (location != null) {
                        fragment.mSlptRouteManager.updateCurrentLocation(location[0], location[1]);
                    } else if (fragment.mIsFirstShowRoute || fragment.mIsNeedRefreshView) {
                        if (fragment.mTrailBitmapProvider.isInitedNavigationData()) {
                            fragment.mIsFirstShowRoute = false;
                        }
                        Global.getGlobalUIHandler().removeCallbacks(fragment.mSetTrailRunnable);
                        if (fragment.mTrailBitmapProvider.refreshBitmap(true)) {
                            Global.getGlobalUIHandler().post(fragment.mSetTrailRunnable);
                        }
                        fragment.mIsNeedRefreshView = false;
                    }
                } else if (fragment.mIsTrailShowed) {
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m3d(SportRouteFragment.TAG, "add trail to view");
                    }
                    Global.getGlobalUIHandler().removeCallbacks(fragment.mSetTrailRunnable);
                    if (fragment.mTrailBitmapProvider.refreshBitmap(false)) {
                        Global.getGlobalUIHandler().post(fragment.mSetTrailRunnable);
                        double[] lastLocation = fragment.getLastLocation(fragment.mPointProvider);
                        if (lastLocation != null) {
                            fragment.mSlptRouteManager.updateCurrentLocation(lastLocation[0], lastLocation[1]);
                        }
                    }
                } else {
                    location = fragment.getLastLocation(fragment.mPointProvider);
                    if (location != null) {
                        fragment.mSlptRouteManager.updateCurrentLocation(location[0], location[1]);
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        Debug.m3d(SportRouteFragment.TAG, "show whole trail");
                    }
                    if (fragment.mTrailBitmapProvider.refreshBitmap(true)) {
                        fragment.startShowTrailView();
                        fragment.mIsTrailShowed = true;
                    }
                }
            }
        }
    }

    private static final class SetTrailRunnable implements Runnable {
        private WeakReference<SportRouteFragment> mFragmentRef = null;

        public SetTrailRunnable(SportRouteFragment fragment) {
            this.mFragmentRef = new WeakReference(fragment);
        }

        public void run() {
            SportRouteFragment fragment = (SportRouteFragment) this.mFragmentRef.get();
            if (fragment != null && fragment.isAdded() && !fragment.isDetached() && fragment.mTrailBitmap != null && !fragment.mTrailBitmap.isRecycled()) {
                fragment.mTrailBitmapProvider.drawOnBitmap(fragment.mTrailBitmap);
                fragment.mTrailView.setImageDrawable(new BitmapDrawable(fragment.getResources(), fragment.mTrailBitmap));
            }
        }
    }

    public static SportRouteFragment newInstance(int sportType) {
        SportRouteFragment fragment = new SportRouteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("sport_type", sportType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mSportType = getArguments().getInt("sport_type");
        }
        this.mBgType = BackgroundUtils.getCustomBgType(getActivity());
        this.mStartBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_start_route);
        if (this.mBgType == 1) {
            this.mEndBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.bg_white_sport_map_location);
        } else {
            this.mEndBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_map_fun_icon_location);
        }
        this.mNaviStartBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_map_fun_icon_cp_dot);
        this.mNaviEndBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_map_fun_icon_end);
        this.mTrailBitmapProvider = new RouteTrailManager(320, 231, this.mStartBitmap, this.mEndBitmap, this.mNaviStartBitmap, this.mNaviEndBitmap, this.mSportType, this.mBgType);
        this.mPointProvider = PointProviderManager.getInstance().getPointProvider();
        this.mTrailBitmapProvider.start(this.mPointProvider);
        this.mTrailBitmap = Bitmap.createBitmap(320, 231, Config.ARGB_8888);
        this.mTrailBitmapProvider.setOritationListener(this);
        this.mSlptRouteManager = new SlptRouteManager();
        this.mSlptRouteManager.setZoomLevel(this.mTrailBitmapProvider.getScale());
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sRunningCount++;
        View root = inflater.inflate(C0532R.layout.fragment_route_layout, container, false);
        this.mTrailView = (ImageView) root.findViewById(C0532R.id.sport_info_trail);
        this.mNoTrailView = (NoTrailView) root.findViewById(C0532R.id.sport_info_no_trail);
        this.mTitleView = (TextView) root.findViewById(C0532R.id.sport_info_trail_title);
        this.mCurAltView = (NewNumberTextView) root.findViewById(C0532R.id.cur_altitude);
        this.mClimbUpView = (NewNumberTextView) root.findViewById(C0532R.id.climb_up);
        this.mCurLng = (NewNumberTextView) root.findViewById(C0532R.id.lngvalue);
        this.mCurLat = (NewNumberTextView) root.findViewById(C0532R.id.latvalue);
        this.mCompassView = (CompassView) root.findViewById(C0532R.id.compass_view);
        this.mEnlargeImg = (ImageView) root.findViewById(C0532R.id.img_enlarge);
        this.mNarrowImg = (ImageView) root.findViewById(C0532R.id.img_narrow);
        this.mEnlargeBtn = root.findViewById(C0532R.id.enlarge);
        this.mNarrowBtn = root.findViewById(C0532R.id.narrow);
        this.hmRemindOffsetView = (HmTextView) root.findViewById(C0532R.id.sport_info_trail_title_remind);
        this.hmRemindOffsetView.setVisibility(8);
        this.mEnlargeBtn.setOnClickListener(this);
        this.mNarrowBtn.setOnClickListener(this);
        if (this.mBgType == 1) {
            root.findViewById(C0532R.id.route_container).setBackgroundResource(C0532R.drawable.bg_white_custom);
            ((TextView) root.findViewById(C0532R.id.cur_altitude_title)).setTextColor(-16777216);
            ((TextView) root.findViewById(C0532R.id.climb_up_title)).setTextColor(-16777216);
            ((TextView) root.findViewById(C0532R.id.lnglat)).setTextColor(-16777216);
            this.mTitleView.setTextColor(-16777216);
            this.mCurAltView.setTextColor(-16777216);
            this.mClimbUpView.setTextColor(-16777216);
            this.mCurLat.setTextColor(-16777216);
            this.mCurLng.setTextColor(-16777216);
        }
        if (this.mSportType == 11) {
            root.findViewById(C0532R.id.climb_up_title).setVisibility(8);
            this.mClimbUpView.setVisibility(8);
        } else {
            root.findViewById(C0532R.id.lnglat).setVisibility(8);
            this.mCurLng.setVisibility(8);
            this.mCurLat.setVisibility(8);
        }
        if (!(this.mSportType == 7 || this.mSportType == 9 || this.mSportType == 11)) {
            root.findViewById(C0532R.id.param_container).setVisibility(8);
        }
        init();
        initTitleContent();
        initCompassSensor();
        this.mEnlargeImg.setAlpha(0.5f);
        this.mEnlargeBtn.setClickable(false);
        this.mEnlargeBtn.setFocusable(false);
        return root;
    }

    private void init() {
        this.mTrailBitmapProvider.drawOnBitmap(this.mTrailBitmap);
        this.mTrailView.setImageDrawable(new BitmapDrawable(getResources(), this.mTrailBitmap));
    }

    private void initCompassSensor() {
        CompassManager.getManager().initSensor(getActivity(), new C07951());
    }

    private void initTitleContent() {
        String[] routeInfos = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(getActivity(), this.mSportType);
        if (routeInfos != null && routeInfos.length >= 2 && !TextUtils.isEmpty(routeInfos[0])) {
            this.mTitleView.setText(routeInfos[0]);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        cancelNoTrailAnimation();
        this.mValueAnimator = null;
    }

    public void onResume() {
        super.onResume();
        Debug.m5i(TAG, "onResume, isForeground: " + this.mIsForeground);
        if (this.mIsForeground) {
            CompassManager.getManager().registerListener();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mSlptRouteManager != null) {
            this.mSlptRouteManager.onPause();
        }
    }

    public void onStop() {
        super.onStop();
        Debug.m5i(TAG, "onStop");
        CompassManager.getManager().unRegisterListener();
    }

    public void onDestroy() {
        super.onDestroy();
        CompassManager.getManager().destroy();
        Global.getGlobalUIHandler().removeCallbacks(this.mSetTrailRunnable);
        Global.getGlobalRouteHandler().removeCallbacks(this.mRefreshTrailRunnable);
        Global.getGlobalRouteHandler().post(new C07962());
        synchronized (this) {
            this.mIsDestroyed = true;
            if (this.mTrailBitmap != null) {
                this.mTrailBitmap.recycle();
                this.mTrailBitmap = null;
            }
            if (this.mStartBitmap != null) {
                this.mStartBitmap.recycle();
                this.mStartBitmap = null;
            }
            if (this.mEndBitmap != null) {
                this.mEndBitmap.recycle();
                this.mEndBitmap = null;
            }
            if (this.mNaviStartBitmap != null) {
                this.mNaviStartBitmap.recycle();
                this.mNaviStartBitmap = null;
            }
            if (this.mNaviEndBitmap != null) {
                this.mNaviEndBitmap.recycle();
                this.mNaviEndBitmap = null;
            }
        }
    }

    public void setIsForeground(boolean isForeground) {
        Debug.m5i(TAG, "setIsForeground, isForeground: " + isForeground);
        this.mIsForeground = isForeground;
        if (isForeground) {
            if (this.mSlptRouteManager != null) {
                this.mSlptRouteManager.showMapView();
            }
            CompassManager.getManager().registerListener();
            return;
        }
        if (this.mSlptRouteManager != null) {
            this.mSlptRouteManager.hideMapView();
        }
        CompassManager.getManager().unRegisterListener();
    }

    public boolean isForeground() {
        return this.mIsForeground;
    }

    public void refreshSportView(final OutdoorSportSnapshot sportSnapshot) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "refresh sport view");
        }
        if (isAdded() && !isDetached()) {
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    if (SportRouteFragment.this.isAdded()) {
                        SportRouteFragment.this.refreshCurrentPage();
                        if (sportSnapshot != null) {
                            double altitude = (double) sportSnapshot.getAbsoluteAtitude();
                            float climbUp = sportSnapshot.getClimbUp();
                            if (altitude <= -20000.0d) {
                                SportRouteFragment.this.mCurAltView.setText("--");
                                SportRouteFragment.this.mClimbUpView.setText("--");
                            } else {
                                SportRouteFragment.this.mCurAltView.setText(SportRouteFragment.this.getString(UnitConvertUtils.isImperial() ? C0532R.string.route_cur_altitude_value_imperial : C0532R.string.route_cur_altitude_value, Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter(altitude, SportRouteFragment.this.mSportType)))));
                                if (climbUp >= 0.0f) {
                                    SportRouteFragment.this.mClimbUpView.setText(SportRouteFragment.this.getString(UnitConvertUtils.isImperial() ? C0532R.string.route_climb_up_value_imperial : C0532R.string.route_climb_up_value, Integer.valueOf((int) UnitConvertUtils.convertDistanceToFtOrMeter((double) climbUp, SportRouteFragment.this.mSportType))));
                                }
                            }
                            String[] curLatLng = SportRouteFragment.this.getLatLng(sportSnapshot);
                            SportRouteFragment.this.mCurLng.setText(curLatLng[0]);
                            SportRouteFragment.this.mCurLat.setText(curLatLng[1]);
                            return;
                        }
                        SportRouteFragment.this.mCurAltView.setText("--");
                        SportRouteFragment.this.mClimbUpView.setText("--");
                        SportRouteFragment.this.mCurLat.setText("--");
                        SportRouteFragment.this.mCurLng.setText("--");
                    }
                }
            });
            Global.getGlobalRouteHandler().removeCallbacks(this.mRefreshTrailRunnable);
            Global.getGlobalRouteHandler().post(this.mRefreshTrailRunnable);
        }
    }

    private String[] getLatLng(OutdoorSportSnapshot snapshot) {
        String[] curlatLng = new String[2];
        Long[] lngLat = snapshot.getCurGPSLonLatPoint();
        if (lngLat == null || lngLat.length < 3) {
            curlatLng[0] = "--";
            curlatLng[1] = "--";
        } else {
            int lng = (int) (lngLat[1].longValue() / 3000000);
            int lngMin = Math.abs((int) ((lngLat[1].longValue() / 50000) % 60));
            int lngSec = Math.abs((int) (((((float) lngLat[1].longValue()) / 50000.0f) * 60.0f) % 60.0f));
            int lat = (int) (lngLat[2].longValue() / 3000000);
            int latMin = Math.abs((int) ((lngLat[2].longValue() / 50000) % 60));
            int latSec = Math.abs((int) (((((float) lngLat[2].longValue()) / 50000.0f) * 60.0f) % 60.0f));
            if (((float) Math.abs(lng)) > 180.0f || ((float) Math.abs(lat)) > 90.0f) {
                curlatLng[0] = "--";
                curlatLng[1] = "--";
            } else {
                StringBuilder lngBuilder = new StringBuilder();
                StringBuilder latBuilder = new StringBuilder();
                lngBuilder.append(lng < 0 ? "W" : "E").append(Math.abs(lng)).append("°").append(lngMin).append("'").append(lngSec).append("\"");
                latBuilder.append(lat < 0 ? "S" : "N").append(Math.abs(lat)).append("°").append(latMin).append("'").append(latSec).append("\"");
                curlatLng[0] = lngBuilder.toString();
                curlatLng[1] = latBuilder.toString();
            }
        }
        return curlatLng;
    }

    private void refreshCurrentPage() {
        if (!this.mIsTrailShowed) {
            startShowNoTrailView();
        }
    }

    private void startShowNoTrailView() {
        if (isAdded() && !isDetached()) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d(TAG, "start show no trail view");
            }
            this.mNoTrailView.setVisibility(0);
            startShowNoTrailAnimation();
        }
    }

    private void startShowNoTrailAnimation() {
        if (this.mValueAnimator == null) {
            this.mValueAnimator = ValueAnimator.ofInt(new int[]{2000});
            this.mValueAnimator.setDuration(2000);
            this.mValueAnimator.setInterpolator(new LinearInterpolator());
            this.mValueAnimator.setRepeatMode(1);
            this.mValueAnimator.setRepeatCount(-1);
            this.mValueAnimator.addUpdateListener(new C07984());
        }
        cancelNoTrailAnimation();
        this.mValueAnimator.start();
    }

    private void startShowTrailView() {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "start show trail view");
        }
        Global.getGlobalUIHandler().post(new C07995());
    }

    private void cancelNoTrailAnimation() {
        if (this.mValueAnimator != null && this.mValueAnimator.isStarted()) {
            this.mValueAnimator.cancel();
        }
    }

    public void onClick(View v) {
        if (this.mTrailBitmapProvider != null) {
            int nowScale = this.mTrailBitmapProvider.getScale();
            int id = v.getId();
            if (id == C0532R.id.enlarge) {
                nowScale--;
            } else if (id == C0532R.id.narrow) {
                nowScale++;
            }
            if (nowScale < this.mTrailBitmapProvider.getMaxScale()) {
                disableEnlargeView();
                return;
            }
            enableEnlargeView();
            if (nowScale > this.mTrailBitmapProvider.getMinScale()) {
                disableNarrowView();
                return;
            }
            enableNarrowView();
            if (!this.mIsScaling.get()) {
                this.mIsScaling.set(true);
                final int finalNowScale = nowScale;
                Global.getGlobalRouteHandler().post(new Runnable() {

                    class C08001 implements Runnable {
                        C08001() {
                        }

                        public void run() {
                            if (finalNowScale == SportRouteFragment.this.mTrailBitmapProvider.getMaxScale()) {
                                SportRouteFragment.this.disableEnlargeView();
                            } else if (finalNowScale == SportRouteFragment.this.mTrailBitmapProvider.getMinScale()) {
                                SportRouteFragment.this.disableNarrowView();
                            }
                        }
                    }

                    public void run() {
                        if (SportRouteFragment.this.mSlptRouteManager != null) {
                            SportRouteFragment.this.mSlptRouteManager.setZoomLevel(finalNowScale);
                        }
                        SportRouteFragment.this.mTrailBitmapProvider.setScale(finalNowScale);
                        SportRouteFragment.this.mIsScaling.set(false);
                        SportRouteFragment.this.mIsNeedRefreshView = true;
                        Global.getGlobalUIHandler().post(new C08001());
                    }
                });
            }
        }
    }

    private void disableEnlargeView() {
        this.mEnlargeImg.setAlpha(0.5f);
        this.mEnlargeBtn.setClickable(false);
        this.mEnlargeBtn.setFocusable(false);
    }

    private void enableEnlargeView() {
        this.mEnlargeImg.setAlpha(1.0f);
        this.mEnlargeBtn.setClickable(true);
        this.mEnlargeBtn.setFocusable(true);
    }

    private void disableNarrowView() {
        this.mNarrowImg.setAlpha(0.5f);
        this.mNarrowBtn.setClickable(false);
        this.mNarrowBtn.setFocusable(false);
    }

    private void enableNarrowView() {
        this.mNarrowImg.setAlpha(1.0f);
        this.mNarrowBtn.setClickable(true);
        this.mNarrowBtn.setFocusable(true);
    }

    public float getOritationAngle() {
        return CompassManager.getManager().getRotateAngle();
    }

    private double[] getLastLocation(IPointProvider pointProvider) {
        ArrayList<SportLocationData> points = new ArrayList();
        if (pointProvider == null) {
            return null;
        }
        pointProvider.getAllGpsPoints(points);
        int size = points.size();
        if (size == 0) {
            return null;
        }
        SportLocationData loc = (SportLocationData) points.get(size - 1);
        return new double[]{(double) loc.mLongitude, (double) loc.mLatitude};
    }

    public void doRouteRunStatus(int routeStatus) {
        switch (routeStatus) {
            case 7:
                showRemindRouteOffsetDialog();
                return;
            case 8:
                rebackToNormalRoute();
                return;
            case 9:
                this.hmRemindOffsetView.setVisibility(0);
                this.hmRemindOffsetView.setText(getString(C0532R.string.gpx_alreay_offset));
                return;
            default:
                return;
        }
    }

    public void showRemindRouteOffsetDialog() {
        this.hmRemindOffsetView.setVisibility(0);
        this.hmRemindOffsetView.setText(getString(C0532R.string.gpx_alreay_offset));
    }

    private void rebackToNormalRoute() {
        this.hmRemindOffsetView.setVisibility(8);
    }
}
