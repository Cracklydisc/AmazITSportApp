package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.ui.fragrunning.V2SportPauseContinueFragment.UserActionListener;
import com.huami.watch.newsport.ui.view.SportKeyEventListView;
import com.huami.watch.newsport.ui.view.SportKeyEventListView.ItemFocusedListener;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import com.huami.watch.wearubc.UbcInterface;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class V2HuangHeSportPauseContinueFragment extends Fragment {
    private static final String TAG = V2HuangHeSportPauseContinueFragment.class.getName();
    private boolean isStopSport = false;
    private SportPauseContinueAdapter mAdapter = null;
    private List<Integer> mAdapterContentList = new ArrayList();
    private Calendar mCalendar = null;
    private BaseConfig mConfig = null;
    private int mDefaultPos = 0;
    private boolean mIsTraingMode;
    private SportKeyEventListView mListView = null;
    private UserActionListener mListener;
    private OutdoorSportSnapshot mOutdoorSportStatus = null;
    private int mSportState = 1;
    private int mSportType = -1;
    private RelativeLayout rootView = null;

    class C08041 implements ItemFocusedListener {
        C08041() {
        }

        public void hasItemFocused(boolean hasFocus) {
            LogUtil.m9i(false, V2HuangHeSportPauseContinueFragment.TAG, "hasItemFocused:" + hasFocus);
        }

        public void focusedItem(int position) {
            LogUtil.m9i(false, V2HuangHeSportPauseContinueFragment.TAG, "focusedItem:" + position);
            V2HuangHeSportPauseContinueFragment.this.mDefaultPos = position;
            V2HuangHeSportPauseContinueFragment.this.mAdapter.notifyDataSetChanged();
        }

        public void onItemClicked(int pos) {
            LogUtil.m9i(false, V2HuangHeSportPauseContinueFragment.TAG, "onItemClicked:" + pos);
            View view = V2HuangHeSportPauseContinueFragment.this.mListView.getChildAt(V2HuangHeSportPauseContinueFragment.this.mListView.getHeaderViewsCount() + pos);
            if (view instanceof ViewGroup) {
                ((ViewGroup) view).getChildAt(0).performClick();
            } else {
                view.performClick();
            }
        }
    }

    class C08052 extends Callback {
        C08052() {
        }

        protected void doCallback(int i, Object o) {
            Debug.m5i("notify_firstbeat", "notify callback come back");
        }
    }

    class C08074 implements OnClickListener {
        C08074() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C08085 implements Runnable {
        C08085() {
        }

        public void run() {
            V2HuangHeSportPauseContinueFragment.this.mAdapter.notifyDataSetChanged();
        }
    }

    private class SportPauseContinueAdapter extends BaseAdapter {

        private class ViewHolder {
            HmHaloButton mContentBtn;

            private ViewHolder() {
            }
        }

        public int getCount() {
            return V2HuangHeSportPauseContinueFragment.this.mAdapterContentList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d(V2HuangHeSportPauseContinueFragment.TAG, "getItemId : " + position);
            }
            return (long) position;
        }

        public int getItemViewType(int position) {
            return ((Integer) V2HuangHeSportPauseContinueFragment.this.mAdapterContentList.get(position)).intValue();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = V2HuangHeSportPauseContinueFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.fragment_v2_huanghe_pause_continue_list_item, parent, false);
                holder = new ViewHolder();
                holder.mContentBtn = (HmHaloButton) convertView.findViewById(C0532R.id.continue_pause_button);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Drawable drawable;
            switch (getItemViewType(position)) {
                case 0:
                    switch (V2HuangHeSportPauseContinueFragment.this.mSportState) {
                        case 0:
                            holder.mContentBtn.setText(V2HuangHeSportPauseContinueFragment.this.getString(C0532R.string.running_button_continue));
                            drawable = V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.sport_btn_icon_play);
                            drawable.setBounds(41, 0, drawable.getMinimumWidth() + 41, drawable.getMinimumHeight());
                            holder.mContentBtn.setCompoundDrawables(drawable, null, null, null);
                            break;
                        case 1:
                            holder.mContentBtn.setText(V2HuangHeSportPauseContinueFragment.this.getString(C0532R.string.running_button_pause));
                            drawable = V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.sport_btn_icon_stop);
                            drawable.setBounds(41, 0, drawable.getMinimumWidth() + 41, drawable.getMinimumHeight());
                            holder.mContentBtn.setCompoundDrawables(drawable, null, null, null);
                            break;
                        default:
                            break;
                    }
                case 1:
                    holder.mContentBtn.setText(V2HuangHeSportPauseContinueFragment.this.getString(C0532R.string.running_button_save));
                    drawable = V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.sport_btn_icon_finish);
                    drawable.setBounds(41, 0, drawable.getMinimumWidth() + 41, drawable.getMinimumHeight());
                    holder.mContentBtn.setCompoundDrawables(drawable, null, null, null);
                    holder.mContentBtn.setBackground(V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.huanghe_finish_bg));
                    break;
                case 2:
                    holder.mContentBtn.setText(V2HuangHeSportPauseContinueFragment.this.getString(C0532R.string.give_up_sport_record));
                    drawable = V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.sport_btn_icon_delete);
                    drawable.setBounds(41, 0, drawable.getMinimumWidth() + 41, drawable.getMinimumHeight());
                    holder.mContentBtn.setCompoundDrawables(drawable, null, null, null);
                    holder.mContentBtn.setBackground(V2HuangHeSportPauseContinueFragment.this.getResources().getDrawable(C0532R.drawable.huanghe_discard_bg));
                    break;
                case 3:
                    holder.mContentBtn.setText(V2HuangHeSportPauseContinueFragment.this.getString(C0532R.string.sport_soccer_sec_half));
                    break;
            }
            holder.mContentBtn.setWithHalo(false);
            holder.mContentBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!V2HuangHeSportPauseContinueFragment.this.isStopSport) {
                        Debug.m5i(V2HuangHeSportPauseContinueFragment.TAG, "onclick: " + position);
                        switch (SportPauseContinueAdapter.this.getItemViewType(position)) {
                            case 0:
                                switch (V2HuangHeSportPauseContinueFragment.this.mSportState) {
                                    case 0:
                                        V2HuangHeSportPauseContinueFragment.this.continueSport();
                                        return;
                                    case 1:
                                        V2HuangHeSportPauseContinueFragment.this.pauseSport();
                                        return;
                                    default:
                                        return;
                                }
                            case 1:
                                V2HuangHeSportPauseContinueFragment.this.showGiveUpRecordDialog(false);
                                return;
                            case 2:
                                UbcInterface.recordCountEvent("0051");
                                V2HuangHeSportPauseContinueFragment.this.showGiveUpRecordDialog(true);
                                return;
                            case 3:
                                if (V2HuangHeSportPauseContinueFragment.this.mListener != null) {
                                    V2HuangHeSportPauseContinueFragment.this.mListener.onHalfSecStart();
                                }
                                V2HuangHeSportPauseContinueFragment.this.mAdapterContentList.remove(position);
                                V2HuangHeSportPauseContinueFragment.this.mAdapter.notifyDataSetChanged();
                                return;
                            default:
                                return;
                        }
                    }
                }
            });
            return convertView;
        }
    }

    public static V2HuangHeSportPauseContinueFragment newInstance(int sportType, BaseConfig config) {
        V2HuangHeSportPauseContinueFragment fragment = new V2HuangHeSportPauseContinueFragment();
        Bundle argument = new Bundle();
        argument.putInt("sport_type", sportType);
        argument.putParcelable("config", config);
        fragment.setArguments(argument);
        Debug.m5i(TAG, "SportControlStatus, fragment:" + fragment.toString());
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (UserActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement UserActionListener");
        }
    }

    public void onDetach() {
        super.onDetach();
        this.isStopSport = false;
        this.mListener = null;
        this.mDefaultPos = 0;
    }

    public void onSaveInstanceState(Bundle outState) {
        Debug.m5i(TAG, "SportControlStatus, onSaveInstanceState:mSportState----->:");
        super.onSaveInstanceState(outState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle argument = getArguments();
        this.mSportType = argument.getInt("sport_type");
        this.mConfig = (BaseConfig) argument.getParcelable("config");
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int i;
        this.rootView = (RelativeLayout) inflater.inflate(C0532R.layout.fragment_v2_huanghe_pause_continue_layout, container, false);
        this.mListView = (SportKeyEventListView) this.rootView.findViewById(C0532R.id.list_view);
        this.mListView.setHasBottomView(false);
        ArcScrollbarHelper.setArcScrollBarDrawable(this.mListView);
        if (UnitConvertUtils.isHuangheMode()) {
            i = 1;
        } else {
            i = 0;
        }
        this.mSportState = i;
        this.mDefaultPos = 0;
        initAdapterContent();
        this.mAdapter = new SportPauseContinueAdapter();
        this.mListView.setAdapter(this.mAdapter);
        this.mListView.setHasFocused(false);
        this.mListView.addHeaderView(inflater.inflate(C0532R.layout.fragment_v2_huanghe_pause_continue_head, this.mListView, false), null, false);
        this.mListView.setFocusable(false);
        this.mListView.setItemFocusedListener(new C08041());
        this.isStopSport = false;
        return this.rootView;
    }

    public void setIsTraingMode(boolean isTraingMode) {
        this.mIsTraingMode = isTraingMode;
    }

    private void continueSport() {
        if (this.mListener != null) {
            this.mListener.onContinueButtonClicked();
        }
    }

    private void pauseSport() {
        if (this.mListener != null) {
            this.mListener.onPauseButtonClieked();
        }
    }

    private void notifySesnorUpdateFirstBeatData() {
        SensorHubManagerWrapper.getInstance(getActivity()).notifySensorUpdateSportData(new C08052());
    }

    private void showGiveUpRecordDialog(boolean isGiveupThisRecord) {
        int messageId;
        Debug.m5i(TAG, "showGiveUpRecordDialog");
        final boolean isReachTargetDistance = SportDataManager.getInstance().isValidSportWhenEndSport();
        boolean isTrainingModeCompleted = false;
        if (isGiveupThisRecord) {
            messageId = C0532R.string.sport_main_give_up_this_sport;
        } else {
            if (this.mIsTraingMode) {
                TrainingInfo info = TrainingInfoManager.getInstance(getActivity()).getTrainingInfo();
                if (!Global.DEBUG_TRAINING_MODE) {
                    isTrainingModeCompleted = this.mOutdoorSportStatus != null && ((this.mOutdoorSportStatus.getDistance() >= info.getDistance() && this.mSportType == 1) || ((this.mOutdoorSportStatus.getTotalTime() >= ((long) info.getMinTime()) && this.mSportType == 9) || (this.mSportType == 17 && this.mOutdoorSportStatus.getmStrokes() > 0)));
                } else if (this.mOutdoorSportStatus == null || ((this.mOutdoorSportStatus.getDistance() < info.getDistance() || this.mSportType != 1) && ((this.mOutdoorSportStatus.getTotalTime() < 180000 || this.mSportType != 9) && (this.mSportType != 17 || this.mOutdoorSportStatus.getmStrokes() <= 0)))) {
                    isTrainingModeCompleted = false;
                } else {
                    isTrainingModeCompleted = true;
                }
                Debug.m5i(TAG, "total time:" + (this.mOutdoorSportStatus != null ? this.mOutdoorSportStatus.getTotalTime() : 0));
            }
            if (!isReachTargetDistance) {
                messageId = this.mIsTraingMode ? C0532R.string.not_complete_train_task : this.mSportType == 14 ? C0532R.string.swimming_stop_dialog_msg_not_reach_minimum_distance : this.mSportType == 11 ? C0532R.string.skiing_stop_dialog_msg_not_reach_minimum_distance : SportType.isSportTypeNeedDis(this.mSportType) ? C0532R.string.running_stop_dialog_msg_not_reach_minimum_distance : C0532R.string.indoor_riding_time_too_short;
            } else if (!this.mIsTraingMode || isTrainingModeCompleted) {
                notifySesnorUpdateFirstBeatData();
                this.isStopSport = true;
                if (this.mListener != null) {
                    this.mListener.onStopButtonClicked(false);
                    return;
                }
                return;
            } else {
                messageId = C0532R.string.not_complete_train_task;
            }
            if (this.mSportType == 17) {
                messageId = C0532R.string.tennic_strokes_times_too_short;
            }
        }
        final boolean finalIsTrainingModeCompleted = isTrainingModeCompleted;
        Builder builder = new Builder(getActivity()).setMessage(messageId).setPositiveButton(getString(C0532R.string.decision_no), new C08074()).setNegativeButton(getString(C0532R.string.decision_yes), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (V2HuangHeSportPauseContinueFragment.this.mIsTraingMode && !finalIsTrainingModeCompleted && isReachTargetDistance) {
                    V2HuangHeSportPauseContinueFragment.this.notifySesnorUpdateFirstBeatData();
                    if (V2HuangHeSportPauseContinueFragment.this.mListener != null) {
                        V2HuangHeSportPauseContinueFragment.this.mListener.onStopButtonClicked(false);
                    }
                } else if (V2HuangHeSportPauseContinueFragment.this.mListener != null) {
                    V2HuangHeSportPauseContinueFragment.this.mListener.onStopButtonClicked(true);
                }
                V2HuangHeSportPauseContinueFragment.this.isStopSport = true;
                dialog.dismiss();
            }
        }).setSystemType(true);
        if (!UnitConvertUtils.isZh()) {
            builder.setMessageTextSize(15.0f);
        }
        builder.create().show();
    }

    public void changeSportState(int sportState) {
        if (this.mSportState != sportState) {
            this.mSportState = sportState;
            Global.getGlobalUIHandler().post(new C08085());
        }
    }

    private void initAdapterContent() {
        this.mAdapterContentList.clear();
        this.mAdapterContentList.add(Integer.valueOf(0));
        OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) SportDataManager.getInstance().getSportSnapshot();
        if (this.mSportType == 18 && snapshot != null && snapshot.getSecHalfStartTime() == -1) {
            this.mAdapterContentList.add(Integer.valueOf(3));
        }
        this.mAdapterContentList.add(Integer.valueOf(1));
        this.mAdapterContentList.add(Integer.valueOf(2));
    }
}
