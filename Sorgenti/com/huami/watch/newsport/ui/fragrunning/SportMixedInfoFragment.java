package com.huami.watch.newsport.ui.fragrunning;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.ui.view.ExchangeView;
import com.huami.watch.newsport.ui.view.ExchangeView.IExchangeViewAnim;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.LinkedList;
import java.util.List;

public class SportMixedInfoFragment extends AbsSportInfoFragment implements IExchangeViewAnim {
    private View mBatteryView = null;
    private View mContainer = null;
    private View mExchangeContainer = null;
    private ExchangeView mExchangeView = null;
    private View mFirstContainer = null;
    private View mFourContainer = null;
    private boolean mIsExchangeStarted = false;
    private IExchangeViewAnim mListener = null;
    private View mSecondContainer = null;
    private TextView mSportBarTimeLinear = null;
    private ImageView mSportImg = null;
    private HmTextView mSportTitle = null;
    private View mThirdContainer = null;
    private TextView mTimeTitle = null;
    private List<View> mViews;

    public static AbsSportInfoFragment newInstance(int sportType, boolean isMixedSport) {
        AbsSportInfoFragment fragment = new SportMixedInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("sport_type", sportType);
        bundle.putBoolean("is_mixed_sport", isMixedSport);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IExchangeViewAnim) {
            this.mListener = (IExchangeViewAnim) activity;
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mViews = new LinkedList();
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root;
        if (SportType.isSwimMode(this.mSportType)) {
            root = inflater.inflate(C0532R.layout.mixed_three_data_item, container, false);
        } else {
            root = inflater.inflate(C0532R.layout.mixed_four_data_item, container, false);
        }
        this.mContainer = root.findViewById(C0532R.id.main_container);
        this.mTimeTitle = (TextView) root.findViewById(C0532R.id.time_title);
        this.mSportBarTimeLinear = (TextView) root.findViewById(C0532R.id.sport_bar_time);
        this.mBatteryView = root.findViewById(C0532R.id.battery_view);
        this.mSportImg = (ImageView) root.findViewById(C0532R.id.sport_img);
        this.mSportTitle = (HmTextView) root.findViewById(C0532R.id.sport_type_txt);
        this.mExchangeView = (ExchangeView) root.findViewById(C0532R.id.exchange_view);
        this.mExchangeContainer = root.findViewById(C0532R.id.exchange_container);
        this.mExchangeView.setAnimListener(this);
        this.mExchangeContainer.setVisibility(4);
        this.mFirstContainer = root.findViewById(C0532R.id.first_item);
        this.mSecondContainer = root.findViewById(C0532R.id.sec_item);
        this.mThirdContainer = root.findViewById(C0532R.id.third_item);
        if (!SportType.isSwimMode(this.mSportType)) {
            this.mFourContainer = root.findViewById(C0532R.id.forth_item);
        }
        if (this.mBgType == 1) {
            this.mTimeTitle.setTextColor(-16777216);
            this.mSportTitle.setTextColor(-16777216);
            this.mSportBarTimeLinear.setTextColor(-16777216);
            if (SportType.isSwimMode(this.mSportType)) {
                this.mContainer.setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_4);
            } else {
                this.mContainer.setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_1);
            }
            root.findViewById(C0532R.id.mixed_title_container).setBackgroundResource(C0532R.drawable.bg_white_custom);
        }
        this.mViews.add(this.mFirstContainer);
        this.mViews.add(this.mSecondContainer);
        this.mViews.add(this.mThirdContainer);
        if (this.mFourContainer != null) {
            this.mViews.add(this.mFourContainer);
        }
        initView();
        return root;
    }

    private void initView() {
        switch (this.mSportType) {
            case 1001:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_running));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_running));
                }
                this.mSportTitle.setText(getString(C0532R.string.sport_widget_title_run));
                break;
            case 1009:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_outdoor_riding));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_outdoor_riding));
                }
                this.mSportTitle.setText(getString(C0532R.string.sport_child_riding));
                break;
            case 1015:
                if (this.mBgType == 1) {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.bg_white_sport_fun_icon_outdoor_swimming));
                } else {
                    this.mSportImg.setImageDrawable(getActivity().getDrawable(C0532R.drawable.sport_fun_icon_outdoor_swimming));
                }
                this.mSportTitle.setText(getString(C0532R.string.sport_child_swim));
                break;
        }
        initOriginalView();
    }

    public void refreshView(OutdoorSportSnapshot sportSnapshot) {
        if (isDetached() || !isAdded()) {
            LogUtil.m9i(true, TAG, "refresh failed, fragment isDetach:" + isDetached() + ", isAdded:" + isAdded());
            return;
        }
        for (int i = 0; i < this.mViews.size(); i++) {
            refreshCorrespoingView((View) this.mViews.get(i), this.mSportInfoOrderManager.getSportInfo(i, sportSnapshot), sportSnapshot);
        }
        if (this.mSportBarTimeLinear != null) {
            this.mSportBarTimeLinear.setText(DataFormatUtils.parseCrossingSecondToDefaultFormattedTimeContainHour(sportSnapshot.getMixedTotalTime() / 1000));
        }
    }

    public void updateBattery(int level) {
        int pos = 13;
        if (this.mBatteryView != null) {
            if (level / 7 <= 13) {
                pos = level / 7;
            }
            this.mBatteryView.setBackgroundResource(BackgroundUtils.getBatteryResArray(this.mBgType)[pos]);
        }
    }

    public void onMillUpdate(OutdoorSportSnapshot sportSnapshot) {
        if (!isDetached() && isAdded()) {
            int pos = this.mSportInfoOrderManager.getTimePickerPosition();
            refreshMillView((View) this.mViews.get(pos - this.mBasePos), this.mSportInfoOrderManager.getSportInfo(pos, sportSnapshot));
        }
    }

    public void showExchangeView() {
        this.mIsExchangeStarted = true;
        Debug.m5i("test_exchange", "showExchangeView start");
        if (this.mExchangeContainer != null) {
            this.mExchangeContainer.setVisibility(0);
        }
        Debug.m5i("test_exchange", "----------showExchangeView start-------------");
        this.mExchangeView.startAnim();
    }

    public void cancleExchangeView() {
        Debug.m5i("test_exchange", "cancleExchangeView");
        if (this.mExchangeContainer != null && this.mExchangeContainer.getVisibility() == 0) {
            Debug.m5i("test_exchange", "----cancleExchangeView start----");
            if (this.mExchangeView.reduceAnim()) {
                this.mIsExchangeStarted = true;
            }
        }
    }

    public void onExchangViewAnimationEnd() {
        Debug.m5i("test_exchange", "----------showExchangeView end-------------");
        this.mExchangeContainer.setVisibility(4);
        this.mIsExchangeStarted = false;
        if (this.mListener != null) {
            this.mListener.onExchangViewAnimationEnd();
        }
    }

    public void onExchangViewAnimationCancle() {
        Debug.m5i("test_exchange", "----cancleExchangeView end----");
        this.mExchangeContainer.setVisibility(4);
        this.mIsExchangeStarted = false;
        if (this.mListener != null) {
            this.mListener.onExchangViewAnimationCancle();
        }
    }

    public boolean isExchangedAnimStarted() {
        return this.mIsExchangeStarted;
    }
}
