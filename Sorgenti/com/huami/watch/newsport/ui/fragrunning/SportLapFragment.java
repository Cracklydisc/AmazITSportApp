package com.huami.watch.newsport.ui.fragrunning;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.sportcenter.model.CurLapInfo;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class SportLapFragment extends Fragment {
    private static final String TAG = SportLapFragment.class.getName();
    private BaseConfig mConfig = null;
    private HmTextView mCurLapDisView = null;
    private NumberTextView mCurLapRunningTimeView = null;
    private LapHandler mHandler = null;
    private ILapListener mListener = null;
    private int mSportType = -1;
    private HmTextView mTitleView = null;
    private NumberTextView mTotalTimeView = null;

    public interface ILapListener {
        void onLapDismissed();
    }

    private final class LapHandler extends Handler {
        private LapHandler() {
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (!SportLapFragment.this.isDetached() && SportLapFragment.this.isAdded() && SportLapFragment.this.mListener != null) {
                SportLapFragment.this.mListener.onLapDismissed();
            }
        }
    }

    public static synchronized SportLapFragment newInstance(int sportType) {
        SportLapFragment fragment;
        synchronized (SportLapFragment.class) {
            fragment = new SportLapFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("sport_type", sportType);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler = new LapHandler();
        this.mSportType = getArguments().getInt("sport_type", -1);
        this.mConfig = DataManager.getInstance().getSportConfig(getActivity(), this.mSportType);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_lap_info, container, false);
        this.mTitleView = (HmTextView) root.findViewById(C0532R.id.lap_title);
        this.mCurLapRunningTimeView = (NumberTextView) root.findViewById(C0532R.id.cur_lap_time);
        this.mTotalTimeView = (NumberTextView) root.findViewById(C0532R.id.total_time);
        this.mCurLapDisView = (HmTextView) root.findViewById(C0532R.id.cur_lap_dis);
        this.mHandler.removeCallbacksAndMessages(null);
        this.mHandler.sendEmptyMessageDelayed(0, 5000);
        return root;
    }

    public void updateCurLapInfo(final CurLapInfo curLapInfo) {
        if (curLapInfo != null && !isDetached() && isAdded()) {
            Debug.m5i(TAG, "updateCurLapInfo");
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    if (!SportLapFragment.this.isDetached() && SportLapFragment.this.isAdded()) {
                        if (curLapInfo.getFlag() == 0) {
                            SportLapFragment.this.mTitleView.setText(SportLapFragment.this.getString(C0532R.string.manual_lap_title, Integer.valueOf(curLapInfo.getNumber())));
                            SportLapFragment.this.mCurLapDisView.setVisibility(0);
                            SportLapFragment.this.mCurLapDisView.setText(SportLapFragment.this.getString(C0532R.string.sport_lap_manual_current));
                        } else {
                            SportLapFragment.this.mTitleView.setText(SportLapFragment.this.getString(C0532R.string.auto_lap_title, Integer.valueOf(curLapInfo.getNumber())));
                            SportLapFragment.this.mCurLapDisView.setVisibility(0);
                            if (SportLapFragment.this.mSportType == 14) {
                                SportLapFragment.this.mCurLapDisView.setText(SportLapFragment.this.getString(SportLapFragment.this.mConfig.getUnit() == 0 ? C0532R.string.sport_lap_swim_length_time : C0532R.string.sport_lap_swim_length_time_imperial, String.valueOf((int) SportLapFragment.this.mConfig.getDistanceAutoLap())));
                            } else {
                                SportLapFragment.this.mCurLapDisView.setText(SportLapFragment.this.getString(UnitConvertUtils.isImperial() ? C0532R.string.sport_auto_lap_dis_imperial : C0532R.string.sport_auto_lap_dis, DataFormatUtils.parseFormattedRealNumber((double) (SportLapFragment.this.mConfig.getDistanceAutoLap() / 1000.0f), true)));
                            }
                            SportLapFragment.this.mCurLapDisView.requestLayout();
                        }
                        SportLapFragment.this.mCurLapRunningTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime(curLapInfo.getRunningTime()));
                        SportLapFragment.this.mTotalTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime(curLapInfo.getTotalTime()));
                    }
                }
            });
        }
    }

    public void setLapListener(ILapListener listener) {
        this.mListener = listener;
    }
}
