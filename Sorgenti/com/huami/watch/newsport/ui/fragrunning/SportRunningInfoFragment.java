package com.huami.watch.newsport.ui.fragrunning;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.customtrain.model.CustomIntervalTrain;
import com.huami.watch.newsport.sportcenter.SportInfoOrderManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.ui.view.RTNumberTextView;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.LinkedList;

public class SportRunningInfoFragment extends AbsSportInfoFragment {
    private static final String TAG = SportRunningInfoFragment.class.getSimpleName();
    private CustomIntervalTrain customIntervalTrain = null;
    private View firstContainer;
    private View mFifthView;
    private View mFirstView;
    private View mFourthView;
    private View mSecView;
    private View mSixView;
    private View mThirdView;
    private View parentContainer;
    private View secondContainer;
    private View threeContainer;

    class C08021 implements Runnable {
        C08021() {
        }

        public void run() {
            SportRunningInfoFragment.this.requestBatteryLevel();
            SportRunningInfoFragment.this.initOriginalView();
        }
    }

    class C08032 implements Runnable {
        C08032() {
        }

        public void run() {
            HmTextView startToggleItem = (HmTextView) SportRunningInfoFragment.this.mFirstView.findViewById(C0532R.id.start_toggle_item);
            if (startToggleItem != null) {
                startToggleItem.setVisibility(0);
            }
        }
    }

    public static AbsSportInfoFragment newInstance(int sportTYpe, int basePos, int itemCount, boolean isMixedSport, int intervailTrainType, int intervalRemindType, boolean isOnlySingleTrainType) {
        AbsSportInfoFragment fragment = new SportRunningInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("sport_type", sportTYpe);
        bundle.putInt("base_position", basePos);
        bundle.putInt("sport_item_count", itemCount);
        bundle.putInt("intermittent_train_type", intervailTrainType);
        bundle.putBoolean("is_mixed_sport", isMixedSport);
        bundle.putInt("intermittent_remind_type", intervalRemindType);
        bundle.putBoolean("only_single_train_type", isOnlySingleTrainType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mViews = new LinkedList();
    }

    private void initViewList() {
        if (this.mItemCount < 2) {
            this.mViews.add(this.mFirstView);
        } else if (this.mItemCount < 3) {
            this.mViews.add(this.mFirstView);
            this.mViews.add(this.mSecView);
        } else if (this.mItemCount < 4) {
            this.mViews.add(this.mFirstView);
            this.mViews.add(this.mSecView);
            this.mViews.add(this.mThirdView);
        } else if (this.mItemCount < 5) {
            this.mViews.add(this.mFirstView);
            this.mViews.add(this.mSecView);
            this.mViews.add(this.mThirdView);
            this.mViews.add(this.mFourthView);
        } else if (this.mItemCount < 6) {
            this.mViews.add(this.mFirstView);
            this.mViews.add(this.mSecView);
            this.mViews.add(this.mThirdView);
            this.mViews.add(this.mFourthView);
            this.mViews.add(this.mFifthView);
        } else if (this.mItemCount < 7) {
            this.mViews.add(this.mFirstView);
            this.mViews.add(this.mSecView);
            this.mViews.add(this.mThirdView);
            this.mViews.add(this.mFourthView);
            this.mViews.add(this.mFifthView);
            this.mViews.add(this.mSixView);
        }
        if (this.mIntermittentTrainType == -1) {
            int curPos = this.mSportInfoOrderManager.getTimePickerPosition() - this.mBasePos;
            if (curPos <= this.mItemCount - 1 && curPos >= 0) {
                this.mViews.add(curPos, (View) this.mViews.remove(0));
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = null;
        if (this.mItemCount == 6) {
            root = inflater.inflate(C0532R.layout.six_items_layout, container, false);
            this.parentContainer = root.findViewById(C0532R.id.running_container);
            this.mFirstView = root.findViewById(C0532R.id.first_item);
            this.mSecView = root.findViewById(C0532R.id.sec_item);
            this.mThirdView = root.findViewById(C0532R.id.third_item);
            this.mFourthView = root.findViewById(C0532R.id.forth_item);
            this.mFifthView = root.findViewById(C0532R.id.fifth_item);
            this.mSixView = root.findViewById(C0532R.id.six_item);
            if (this.mBgType == 1) {
                root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_6);
            }
        } else if (this.mItemCount == 5) {
            root = inflater.inflate(C0532R.layout.five_items_layout, container, false);
            this.parentContainer = root.findViewById(C0532R.id.running_container);
            this.mFirstView = root.findViewById(C0532R.id.first_item);
            this.mSecView = root.findViewById(C0532R.id.sec_item);
            this.mThirdView = root.findViewById(C0532R.id.third_item);
            this.mFourthView = root.findViewById(C0532R.id.forth_item);
            this.mFifthView = root.findViewById(C0532R.id.fifth_item);
            if (this.mBgType == 1) {
                root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_5);
            }
        } else if (this.mItemCount == 4) {
            root = inflater.inflate(C0532R.layout.four_items_layout, container, false);
            this.parentContainer = root.findViewById(C0532R.id.running_container);
            this.mFirstView = root.findViewById(C0532R.id.first_item);
            this.mSecView = root.findViewById(C0532R.id.sec_item);
            this.mThirdView = root.findViewById(C0532R.id.third_item);
            this.mFourthView = root.findViewById(C0532R.id.forth_item);
            if (this.mBgType == 1) {
                root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_4);
            }
        } else if (this.mItemCount == 3 && this.mIntermittentTrainType == -1) {
            root = inflater.inflate(C0532R.layout.three_items_layout, container, false);
            this.parentContainer = root.findViewById(C0532R.id.running_container);
            this.mFirstView = root.findViewById(C0532R.id.first_item);
            this.firstContainer = root.findViewById(C0532R.id.first_container);
            this.mSecView = root.findViewById(C0532R.id.sec_item);
            this.secondContainer = root.findViewById(C0532R.id.second_container);
            this.threeContainer = root.findViewById(C0532R.id.third_container);
            this.mThirdView = root.findViewById(C0532R.id.third_item);
            if (this.mBgType == 1) {
                root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_3);
            }
        } else if (this.mItemCount != 2 || this.mIntermittentTrainType != -1) {
            if (this.mItemCount != 1) {
                if (this.mBasePos < 2) {
                    switch (this.mIntermittentTrainType) {
                        case 0:
                            if (this.mItemCount == 2) {
                                root = inflater.inflate(C0532R.layout.intermitt_train_two_items_layout, container, false);
                                this.parentContainer = root.findViewById(C0532R.id.running_container);
                                this.firstContainer = root.findViewById(C0532R.id.first_container);
                                this.secondContainer = root.findViewById(C0532R.id.second_container);
                                this.mFirstView = root.findViewById(C0532R.id.first_item);
                                this.mSecView = root.findViewById(C0532R.id.sec_item);
                                if (this.mBgType == 1) {
                                    root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_2);
                                }
                                if (this.mBgType == 1) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-65536);
                                } else if (this.mBgType == 0) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-16711936);
                                }
                                getIntermittenTrain(0);
                                break;
                            }
                            break;
                        case 1:
                            if (this.mItemCount == 2) {
                                root = inflater.inflate(C0532R.layout.intermitt_train_two_items_layout, container, false);
                                this.parentContainer = root.findViewById(C0532R.id.running_container);
                                this.firstContainer = root.findViewById(C0532R.id.first_container);
                                this.secondContainer = root.findViewById(C0532R.id.second_container);
                                this.mFirstView = root.findViewById(C0532R.id.first_item);
                                this.mSecView = root.findViewById(C0532R.id.sec_item);
                                if (this.mBgType == 1) {
                                    root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_2);
                                }
                                if (this.mBgType == 1) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-65536);
                                } else if (this.mBgType == 0) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-16711936);
                                }
                                getIntermittenTrain(1);
                                break;
                            }
                            break;
                        case 2:
                            if (this.mItemCount == 2) {
                                root = inflater.inflate(C0532R.layout.intermitt_train_two_items_layout, container, false);
                                this.parentContainer = root.findViewById(C0532R.id.running_container);
                                this.firstContainer = root.findViewById(C0532R.id.first_container);
                                this.secondContainer = root.findViewById(C0532R.id.second_container);
                                this.mFirstView = root.findViewById(C0532R.id.first_item);
                                this.mSecView = root.findViewById(C0532R.id.sec_item);
                                if (this.mBgType == 1) {
                                    root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_triathlon_bg_2);
                                }
                                if (this.mBgType == 1) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-65536);
                                } else if (this.mBgType == 0) {
                                    ((TextView) this.firstContainer.findViewById(C0532R.id.item_need_do)).setTextColor(-16711936);
                                }
                                getIntermittenTrain(2);
                                break;
                            }
                            break;
                        default:
                            break;
                    }
                }
                root = inflater.inflate(C0532R.layout.three_items_layout, container, false);
                this.parentContainer = root.findViewById(C0532R.id.running_container);
                this.mFirstView = root.findViewById(C0532R.id.first_item);
                this.firstContainer = root.findViewById(C0532R.id.first_container);
                this.mSecView = root.findViewById(C0532R.id.sec_item);
                this.secondContainer = root.findViewById(C0532R.id.second_container);
                this.threeContainer = root.findViewById(C0532R.id.third_container);
                this.mThirdView = root.findViewById(C0532R.id.third_item);
                if (this.mBgType == 1) {
                    root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_3);
                }
                refreshSportInfoData(this.mIntermittentTrainType);
            } else {
                root = inflater.inflate(C0532R.layout.one_item_layout, container, false);
                this.mFirstView = root.findViewById(C0532R.id.first_item);
                if (this.mBgType == 1) {
                    root.findViewById(C0532R.id.running_container).setBackgroundColor(-1);
                }
                if (this.mBgType == 1) {
                    root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_2);
                }
            }
        } else {
            root = inflater.inflate(C0532R.layout.two_items_layout, container, false);
            this.parentContainer = root.findViewById(C0532R.id.running_container);
            this.mFirstView = root.findViewById(C0532R.id.first_item);
            this.mSecView = root.findViewById(C0532R.id.sec_item);
            if (this.mBgType == 1) {
                root.findViewById(C0532R.id.running_container).setBackgroundResource(C0532R.drawable.bg_white_sport_data_bg_2);
            }
        }
        this.mAPmView = (HmImageView) root.findViewById(C0532R.id.am_pm_img);
        this.mTimeView = (HmLinearLayout) root.findViewById(C0532R.id.sport_bar_time);
        this.mBatteryView = (HmImageView) root.findViewById(C0532R.id.battery_view);
        initViewList();
        Global.getGlobalUIHandler().post(new C08021());
        return root;
    }

    private void getIntermittenTrain(int interivalTrainType) {
        if (this.customIntervalTrain != null) {
        }
    }

    public void updateBattery(int level) {
        int pos = 13;
        if (this.mBatteryView != null) {
            if (level / 7 <= 13) {
                pos = level / 7;
            }
            this.mBatteryView.setBackgroundResource(BackgroundUtils.getBatteryResArray(this.mBgType)[pos]);
        }
    }

    public void onMillUpdate(OutdoorSportSnapshot sportSnapshot) {
        if (!isDetached() && isAdded() && this.mSportInfoOrderManager != null) {
            int pos = this.mSportInfoOrderManager.getTimePickerPosition();
            if (pos >= this.mBasePos && pos < this.mBasePos + this.mItemCount && this.mSportInfoOrderManager.isShowMillSec()) {
                refreshMillView((View) this.mViews.get(pos - this.mBasePos), this.mSportInfoOrderManager.getSportInfo(pos, sportSnapshot));
            }
        }
    }

    public void refreshView(OutdoorSportSnapshot sportSnapshot) {
        if (isDetached() || !isAdded()) {
            LogUtil.m9i(true, TAG, "refresh failed, fragment isDetach:" + isDetached() + ", isAdded:" + isAdded());
            return;
        }
        for (int i = 0; i < this.mItemCount; i++) {
            refreshCorrespoingView((View) this.mViews.get(i), this.mSportInfoOrderManager.getSportInfo(this.mBasePos + i, sportSnapshot), sportSnapshot);
            refreshClickKeyDownFinishSeg((View) this.mViews.get(i), i);
        }
        reCalSysTime(this.mAPmView, this.mTimeView);
    }

    public void refreshClickKeyDownFinishSeg(View view, int index) {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "refreshClickKeyDownFinishSeg: basePos:" + this.mBasePos + ",index:" + index);
        if (index == 0 && this.mBasePos == 0 && this.mIntermittentTrainType != -1 && SportDataManager.getInstance().getCurrentTrainUnit().getUnitType().getType() == TrainTargetType.KEYPRESS.getType()) {
            HmTextView toggleTxtView = (HmTextView) view.findViewById(C0532R.id.start_toggle_item);
            if (toggleTxtView != null) {
                toggleTxtView.setVisibility(8);
            }
            HmTextView itemNeedToDo = (HmTextView) view.findViewById(C0532R.id.item_need_do);
            if (itemNeedToDo != null) {
                itemNeedToDo.setVisibility(0);
                itemNeedToDo.setText(getResources().getString(C0532R.string.click_finsh_this_train_unit));
            }
        }
        if (index != 0 || this.mBasePos != 2 || this.mIntermittentTrainType == -1) {
            return;
        }
        if (this.mItemCount == 3) {
            if (this.mBgType == 1) {
                ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-65536);
            } else if (this.mBgType == 0) {
                ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-16711936);
            }
        } else if (this.mItemCount != 2) {
        } else {
            if (this.mBgType == 1) {
                ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-16777216);
            } else if (this.mBgType == 0) {
                ((TextView) view.findViewById(C0532R.id.item_value)).setTextColor(-1);
            }
        }
    }

    public void refreshSportInfoData(int interivalTrainType) {
        if (this.mFirstView != null) {
            HmTextView startToggleItem = (HmTextView) this.mFirstView.findViewById(C0532R.id.start_toggle_item);
            if (startToggleItem != null) {
                startToggleItem.setVisibility(8);
            }
        }
        Log.i(TAG, " baseCount:" + this.mBasePos + ",interivalTrainType:" + interivalTrainType);
        if (this.mSportInfoOrderManager != null) {
            ((SportInfoOrderManagerWrapper) this.mSportInfoOrderManager).refreshSportDataItem();
        }
        Log.i(TAG, " mBasePos:" + this.mBasePos + ",sportItemSize:" + this.mSportInfoOrderManager.getSportItemSize());
        if (this.mBasePos != 2) {
            return;
        }
        if (this.mSportInfoOrderManager.getSportItemSize() == 4) {
            if (this.mBgType == 1) {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.bg_white_sport_data_bg_2));
            } else {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.sport_data_bg_2));
            }
            if (this.threeContainer != null) {
                this.threeContainer.setVisibility(8);
            }
            ((HmLinearLayout) this.firstContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
            ((HmLinearLayout) this.secondContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
        } else if (this.mSportInfoOrderManager.getSportItemSize() == 5) {
            if (this.mBgType == 1) {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.bg_white_sport_data_bg_3));
            } else {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.sport_data_bg_3));
            }
            this.threeContainer.setVisibility(0);
            ((HmTextView) this.firstContainer.findViewById(C0532R.id.item_top_value)).setVisibility(4);
            ((HmLinearLayout) this.firstContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
            ((HmLinearLayout) this.secondContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
            ((HmLinearLayout) this.threeContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
        }
    }

    public void refreshTrainUnitNearFinish() {
        Log.i(TAG, " todo refreshTrainUnitNearFinish ");
        if (this.mFirstView != null) {
            Global.getGlobalUIHandler().post(new C08032());
        }
    }

    public void refreshIntervalFinishContinuedSport() {
        if (this.mBasePos == 0) {
            if (this.mSportInfoOrderManager != null) {
                ((SportInfoOrderManagerWrapper) this.mSportInfoOrderManager).refreshSportDataItem();
            }
            if (this.mBgType == 1) {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.bg_white_sport_data_bg_2));
            } else {
                this.parentContainer.setBackground(getResources().getDrawable(C0532R.drawable.sport_data_bg_2));
            }
            ((HmLinearLayout) this.firstContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
            ((RelativeLayout) this.firstContainer.findViewById(C0532R.id.item_top_container)).setVisibility(8);
            ((RTNumberTextView) this.firstContainer.findViewById(C0532R.id.item_value)).setTextSize(67.0f);
            ((HmLinearLayout) this.secondContainer).setLayoutParams(new LayoutParams(-1, -2, 1.0f));
        }
    }
}
