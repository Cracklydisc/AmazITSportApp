package com.huami.watch.newsport.ui.fragrunning;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.RealTimeView;
import com.huami.watch.newsport.ui.view.RealTimeView.RealTimeBean;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SportRealGraphFragment extends Fragment {
    private static final String TAG = SportRealGraphFragment.class.getName();
    private int mBgType = 0;
    private BaseConfig mConfig = null;
    private int mGraphType = 0;
    private HmImageView mHeartImg = null;
    private RealTimeView mHeartLineChart = null;
    private HmTextView mHeartValueUnit = null;
    private NumberTextView mHeartValueView = null;
    private int mLastHeartSize = -1;
    private long mLastTimeStamp = -1;
    private int[] mRangeHeart = new int[5];
    private int mSportType = -1;
    private HmTextView mTitle = null;
    private long mTotalTimeCount = 0;
    private long mTrackid = -1;

    public static SportRealGraphFragment newInstance(long trackid, int sportType, BaseConfig config) {
        SportRealGraphFragment fragment = new SportRealGraphFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("start_time", trackid);
        bundle.putInt("sport_type", sportType);
        bundle.putParcelable("config", config);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mTrackid = getArguments().getLong("start_time", -1);
        this.mSportType = getArguments().getInt("sport_type", -1);
        this.mConfig = (BaseConfig) getArguments().getParcelable("config");
        this.mGraphType = DataManager.getInstance().getSportConfig(getActivity(), this.mSportType).getRealGraphShownType();
        if (SportType.isSwimMode(this.mSportType) && this.mGraphType == 0) {
            this.mGraphType = 5;
        }
        this.mBgType = BackgroundUtils.getCustomBgType(getActivity());
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_heart_info, container, false);
        this.mHeartValueUnit = (HmTextView) root.findViewById(C0532R.id.cur_heart_unit);
        this.mHeartValueView = (NumberTextView) root.findViewById(C0532R.id.cur_heart_value);
        this.mHeartLineChart = (RealTimeView) root.findViewById(C0532R.id.heart_info_chart);
        this.mTitle = (HmTextView) root.findViewById(C0532R.id.heart_info_title);
        this.mHeartImg = (HmImageView) root.findViewById(C0532R.id.heart_range_img);
        initHeartRange();
        init();
        initLineChartConfig();
        if (this.mBgType == 1) {
            root.findViewById(C0532R.id.real_graph_container).setBackgroundColor(-1);
        }
        return root;
    }

    private void initHeartRange() {
        int maxHeart = UserInfoManager.getDefaultSafeHeart(getActivity());
        this.mRangeHeart[0] = (int) (((float) maxHeart) * 0.5f);
        this.mRangeHeart[1] = (int) (((float) maxHeart) * 0.6f);
        this.mRangeHeart[2] = (int) (((float) maxHeart) * 0.7f);
        this.mRangeHeart[3] = (int) (((float) maxHeart) * 0.8f);
        this.mRangeHeart[4] = (int) (((float) maxHeart) * 0.9f);
    }

    public void onDetach() {
        super.onDetach();
        this.mLastHeartSize = -1;
    }

    private void judgeHeartLevel(int heartRange) {
        if (this.mBgType == 1) {
            this.mTitle.setTextColor(-16777216);
        } else {
            this.mTitle.setTextColor(-1);
        }
        switch (heartRange) {
            case 1:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_1);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_1);
                }
                this.mTitle.setText(getString(C0532R.string.heart_region_warm_up_and_relax));
                return;
            case 2:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_2);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_2);
                }
                this.mTitle.setText(getString(C0532R.string.heart_region_fat_burning));
                return;
            case 3:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_3);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_3);
                }
                this.mTitle.setText(getString(C0532R.string.heart_region_heart_and_lung_enhancement));
                return;
            case 4:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_4);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_4);
                }
                this.mTitle.setText(getString(C0532R.string.heart_region_endurance_enhancement));
                return;
            case 5:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_5);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_5);
                }
                this.mTitle.setText(getString(C0532R.string.heart_region_anaerobic＿limit));
                return;
            default:
                if (this.mBgType == 1) {
                    this.mHeartImg.setImageResource(C0532R.drawable.bg_white_sport_hr_diagram_0);
                } else {
                    this.mHeartImg.setImageResource(C0532R.drawable.sport_hr_diagram_0);
                }
                if (heartRange < 1) {
                    this.mTitle.setText(getString(C0532R.string.sport_main_heart_info_title));
                    return;
                } else {
                    this.mTitle.setText(getString(C0532R.string.sport_main_beyond_heart_info_title));
                    return;
                }
        }
    }

    private void init() {
        if (this.mBgType == 1) {
            this.mTitle.setTextColor(-16777216);
            this.mHeartValueUnit.setTextColor(-16777216);
            this.mHeartValueView.setTextColor(-16777216);
        } else {
            this.mTitle.setTextColor(-1);
            this.mHeartValueUnit.setTextColor(-1);
            this.mHeartValueView.setTextColor(-1);
        }
        switch (this.mGraphType) {
            case 0:
                this.mHeartImg.setVisibility(0);
                this.mTitle.setText(getString(C0532R.string.sport_main_heart_info_title));
                this.mHeartValueUnit.setText(getString(C0532R.string.settings_safe_heart_rate_unit));
                return;
            case 1:
                this.mHeartImg.setVisibility(4);
                this.mTitle.setText(getString(C0532R.string.running_pace_desc));
                if (UnitConvertUtils.isImperial()) {
                    this.mHeartValueUnit.setText(getString(C0532R.string.km_imperial_pace_unit));
                    return;
                } else {
                    this.mHeartValueUnit.setText(getString(C0532R.string.km_metric_pace_unit));
                    return;
                }
            case 2:
                this.mTitle.setText(getString(C0532R.string.crossing_history_detail_absolute_altitude));
                this.mHeartImg.setVisibility(4);
                if (UnitConvertUtils.isImperial()) {
                    this.mHeartValueUnit.setText(getString(C0532R.string.metre_imperial));
                    return;
                } else {
                    this.mHeartValueUnit.setText(getString(C0532R.string.metre_metric));
                    return;
                }
            case 3:
                this.mTitle.setText(getString(C0532R.string.swim_stroke_speed));
                this.mHeartImg.setVisibility(4);
                this.mHeartValueUnit.setText("");
                return;
            case 4:
                this.mTitle.setText(getString(C0532R.string.running_speed_desc));
                this.mHeartImg.setVisibility(4);
                if (UnitConvertUtils.isImperial()) {
                    this.mHeartValueUnit.setText(getString(C0532R.string.km_imperial_speed_unit));
                    return;
                } else {
                    this.mHeartValueUnit.setText(getString(C0532R.string.km_metric_speed_unit));
                    return;
                }
            case 5:
                this.mHeartImg.setVisibility(4);
                this.mTitle.setText(getString(C0532R.string.running_pace_desc));
                if (this.mConfig.getUnit() == 1) {
                    this.mHeartValueUnit.setText(getString(C0532R.string.pace_swim_unit_yd));
                    return;
                } else {
                    this.mHeartValueUnit.setText(getString(C0532R.string.swim_pace_unit));
                    return;
                }
            default:
                return;
        }
    }

    private void initLineChartConfig() {
        int startColor;
        int endColor;
        switch (this.mGraphType) {
            case 0:
                if (this.mBgType != 1) {
                    startColor = getResources().getColor(C0532R.color.outdoor_heart_start_color);
                    endColor = getResources().getColor(C0532R.color.outdoor_heart_end_color);
                    break;
                }
                startColor = getResources().getColor(C0532R.color.bg_white_outdoor_heart_start_color);
                endColor = getResources().getColor(C0532R.color.bg_white_outdoor_heart_end_color);
                break;
            case 1:
            case 4:
            case 5:
                if (this.mBgType != 1) {
                    startColor = getResources().getColor(C0532R.color.outdoor_speed_start_color);
                    endColor = getResources().getColor(C0532R.color.outdoor_speed_end_color);
                    break;
                }
                startColor = getResources().getColor(C0532R.color.bg_white_outdoor_speed_start_color);
                endColor = getResources().getColor(C0532R.color.bg_white_outdoor_speed_end_color);
                break;
            case 2:
                if (this.mBgType != 1) {
                    startColor = getResources().getColor(C0532R.color.outdoor_altitude_start_color);
                    endColor = getResources().getColor(C0532R.color.outdoor_altitude_end_color);
                    break;
                }
                startColor = getResources().getColor(C0532R.color.bg_white_outdoor_altitude_start_color);
                endColor = getResources().getColor(C0532R.color.bg_white_outdoor_altitude_end_color);
                break;
            case 3:
                if (this.mBgType != 1) {
                    startColor = getResources().getColor(C0532R.color.outdoor_stroke_cadence_start_color);
                    endColor = getResources().getColor(C0532R.color.outdoor_stroke_cadence_end_color);
                    break;
                }
                startColor = getResources().getColor(C0532R.color.bg_white_outdoor_stroke_cadence_start_color);
                endColor = getResources().getColor(C0532R.color.bg_white_outdoor_stroke_cadence_end_color);
                break;
            default:
                if (this.mBgType != 1) {
                    startColor = getResources().getColor(C0532R.color.outdoor_heart_start_color);
                    endColor = getResources().getColor(C0532R.color.outdoor_heart_end_color);
                    break;
                }
                startColor = getResources().getColor(C0532R.color.bg_white_outdoor_heart_start_color);
                endColor = getResources().getColor(C0532R.color.bg_white_outdoor_heart_end_color);
                break;
        }
        this.mHeartLineChart.configCurveCoverGradient(true, new int[]{startColor, endColor}, new float[]{0.0f, 1.0f}, 2, endColor, this.mBgType);
        this.mHeartLineChart.setGraphType(this.mGraphType);
    }

    public void updateRealTimeView(OutdoorSportSnapshot sportSnapshot, List<HeartRate> heartRates) {
        if (sportSnapshot != null && !isDetached() && isAdded()) {
            switch (this.mGraphType) {
                case 0:
                    if (sportSnapshot.getHeartQuantity() > 1) {
                        this.mHeartValueView.setText(C0532R.string.sport_pause_heart_shown);
                        judgeHeartLevel(-1);
                        break;
                    }
                    this.mHeartValueView.setText("" + ((int) sportSnapshot.getHeartRate()));
                    if (this.mGraphType == 0) {
                        judgeHeartLevel(sportSnapshot.getHeartRange());
                        break;
                    }
                    break;
                case 1:
                    if (!isValidNormalPace(sportSnapshot.getRealTimePace())) {
                        this.mHeartValueView.setText(C0532R.string.sport_pause_heart_shown);
                        break;
                    } else {
                        this.mHeartValueView.setText(DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(sportSnapshot.getRealTimePace())));
                        break;
                    }
                case 2:
                    double absoluteAltitude = (double) sportSnapshot.getAbsoluteAtitude();
                    if (absoluteAltitude != -20000.0d) {
                        this.mHeartValueView.setText(String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter(absoluteAltitude, this.mSportType))));
                        break;
                    } else {
                        this.mHeartValueView.setText(C0532R.string.sport_pause_heart_shown);
                        break;
                    }
                case 3:
                    this.mHeartValueView.setText(String.valueOf((int) (sportSnapshot.getRtStrokeSpeed() * 60.0f)));
                    break;
                case 4:
                    this.mHeartValueView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(sportSnapshot.getRealTimeSpeed())), false));
                    break;
                case 5:
                    if (!isValidSwimPace(sportSnapshot.getRealTimePace())) {
                        this.mHeartValueView.setText(C0532R.string.sport_pause_heart_shown);
                        break;
                    } else {
                        this.mHeartValueView.setText(DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(sportSnapshot.getRealTimePace(), this.mConfig.getUnit())));
                        break;
                    }
            }
            if (heartRates != null && !heartRates.isEmpty() && this.mTrackid != -1) {
                List<HeartRate> realTimeHeartRates = new ArrayList(heartRates.size());
                realTimeHeartRates.addAll(heartRates);
                this.mLastTimeStamp = ((HeartRate) realTimeHeartRates.get(realTimeHeartRates.size() - 1)).getTimestamp() - this.mTrackid;
                int totalMinCounts = (int) (this.mLastTimeStamp / 60000);
                if (((long) totalMinCounts) > this.mTotalTimeCount) {
                    long endIndex;
                    long startIndex;
                    long startTime;
                    this.mTotalTimeCount = (long) totalMinCounts;
                    Debug.m5i(TAG, "totalMinCount:" + totalMinCounts);
                    if (this.mLastTimeStamp > 1200000) {
                        endIndex = (long) totalMinCounts;
                        startIndex = (long) (totalMinCounts - 19);
                        startTime = startIndex * 60;
                    } else {
                        endIndex = (long) totalMinCounts;
                        startIndex = 1;
                        startTime = 0;
                    }
                    this.mHeartLineChart.setRealTimeValue(createHeartRatePointsInfo(realTimeHeartRates, startTime, this.mTotalTimeCount * 60000, startIndex, endIndex));
                    this.mLastHeartSize = realTimeHeartRates.size();
                }
            }
        }
    }

    private boolean isValidNormalPace(float pace) {
        if (Float.compare(pace, 0.0f) == 0) {
            return false;
        }
        int standardSec = 1800;
        if (this.mSportType == 6) {
            standardSec = 3600;
        }
        if (((int) (1000.0f * pace)) <= standardSec) {
            return true;
        }
        return false;
    }

    private boolean isValidSwimPace(float pace) {
        if (Float.compare(pace, 0.0f) != 0 && ((int) (100.0f * pace)) <= 600) {
            return true;
        }
        return false;
    }

    private List<RealTimeBean> createHeartRatePointsInfo(List<? extends HeartRate> heartRates, long startTime, long endTime, long startIndex, long endIndex) {
        List<RealTimeBean> timeBeanList = new LinkedList();
        int lastIndex = -1;
        float totalHeart = 0.0f;
        float totalNum = 0.0f;
        int i = heartRates.size() - 1;
        while (i >= 0) {
            HeartRate rate = (HeartRate) heartRates.get(i);
            long now = rate.getTimestamp() - this.mTrackid;
            int curIndex = (int) ((endTime - now) / 60000);
            if ((this.mGraphType != 0 || rate.getHeartQuality() <= 1) && now <= endTime) {
                if (curIndex < 0 || curIndex > 20 || lastIndex == 20) {
                    Debug.m5i(TAG, "createHeartRatePointsInfo, index is below 0:" + curIndex);
                } else if ((curIndex == lastIndex || lastIndex == -1) && (i != 0 || lastIndex == 19)) {
                    if (lastIndex == -1) {
                        lastIndex = curIndex;
                        totalNum = 0.0f;
                        totalHeart = 0.0f;
                    }
                    switch (this.mGraphType) {
                        case 0:
                            totalHeart += (float) rate.getHeartRate();
                            totalNum += 1.0f;
                            break;
                        case 1:
                        case 4:
                        case 5:
                            totalHeart += rate.getDistance();
                            totalNum = 60.0f;
                            break;
                        case 2:
                            if (rate.getAltitude() == -20000) {
                                break;
                            }
                            totalHeart = (float) (((double) totalHeart) + UnitConvertUtils.convertDistanceToFtOrMeter((double) rate.getAltitude(), this.mSportType));
                            totalNum += 1.0f;
                            Debug.m5i("edward", "altitude:" + totalHeart + ", num:" + totalNum);
                            break;
                        case 3:
                            int remianSec = 0;
                            if (i > 0) {
                                long nextTime = ((HeartRate) heartRates.get(i - 1)).getTimestamp() - this.mTrackid;
                                remianSec = (int) ((now - nextTime) / 1000);
                                LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "remainsec:" + remianSec + ", stroke:" + rate.getStrokeSpeed() + ", now:" + now + ", next:" + nextTime);
                            }
                            totalHeart += rate.getStrokeSpeed() * ((float) remianSec);
                            totalNum += (float) remianSec;
                            break;
                        default:
                            break;
                    }
                } else {
                    float value;
                    if (this.mGraphType == 4) {
                        value = NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(totalNum == 0.0f ? 0.0f : totalHeart / totalNum)), true));
                    } else if (this.mGraphType == 1) {
                        value = SportDataFilterUtils.parsePace(totalHeart == 0.0f ? 0.0f : totalNum / totalHeart);
                    } else if (this.mGraphType == 5) {
                        value = SportDataFilterUtils.parsePace(totalHeart == 0.0f ? 0.0f : totalNum / totalHeart);
                    } else if (this.mGraphType == 3) {
                        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "num:" + totalNum + ", stroke:" + totalHeart);
                        value = totalNum == 0.0f ? 0.0f : (totalHeart / totalNum) * 60.0f;
                    } else if (this.mGraphType == 2) {
                        OutdoorSportSnapshot snapshot = (OutdoorSportSnapshot) SportDataManager.getInstance().getSportSnapshot();
                        if (totalNum == 0.0f) {
                            value = 0.0f;
                        } else {
                            value = (totalHeart / totalNum) + (snapshot != null ? (float) UnitConvertUtils.convertDistanceToFtOrMeter((double) snapshot.getOffsetAltitude(), this.mSportType) : 0.0f);
                        }
                    } else {
                        value = totalNum == 0.0f ? 0.0f : totalHeart / totalNum;
                    }
                    if (this.mGraphType == 0 && Float.compare(value, 0.0f) == 0) {
                        lastIndex = curIndex;
                        totalNum = 0.0f;
                        totalHeart = 0.0f;
                    } else {
                        Debug.m5i(TAG, "index:" + lastIndex + ", value:" + value);
                        timeBeanList.add(new RealTimeBean(19 - lastIndex, value));
                        lastIndex = curIndex;
                        totalNum = 0.0f;
                        totalHeart = 0.0f;
                    }
                }
            }
            i--;
        }
        return timeBeanList;
    }
}
