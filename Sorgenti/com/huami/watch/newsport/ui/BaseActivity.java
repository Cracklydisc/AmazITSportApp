package com.huami.watch.newsport.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.HmFragmentActivity;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.newsport.utils.LogUtil;

public abstract class BaseActivity extends HmFragmentActivity {
    private static final String TAG = BaseActivity.class.getName();
    private EventCallBack eventCallBack;
    protected volatile boolean isAllowBack = true;
    protected volatile boolean isAllowSlptEvent = false;
    protected volatile boolean isViewDispatchEvent = true;
    private KeyeventProcessor keyeventProcessor;
    private BroadcastReceiver mHomeReceiver = new C06731();
    private boolean mIsReceiveHomeKey = false;
    private IWakeupFromSlpt mSlptListener;

    class C06731 extends BroadcastReceiver {
        C06731() {
        }

        public void onReceive(Context context, Intent intent) {
            BaseActivity.this.finish();
        }
    }

    public interface IWakeupFromSlpt {
        void onWakeupFromSlpt(KeyEvent keyEvent);
    }

    protected void setKeyEventListener(EventCallBack callBack) {
        this.eventCallBack = callBack;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.eventCallBack != null && this.keyeventProcessor == null) {
            this.keyeventProcessor = new KeyeventProcessor(this.eventCallBack);
        }
        if (this.keyeventProcessor != null && (event.getAction() == 1 || event.getAction() == 0)) {
            this.keyeventProcessor.injectKeyEvent(event);
        }
        LogUtils.print(TAG, "dispatchKeyEvent back keyCode:" + event.getKeyCode() + ", " + event);
        if (event.getKeyCode() == 4) {
            LogUtils.print(TAG, "dispatchKeyEvent back ");
            if (this.isAllowBack) {
                return super.dispatchKeyEvent(event);
            }
            return false;
        }
        if (this.isAllowSlptEvent && ((event.getKeyCode() == 131 || event.getKeyCode() == 132 || event.getKeyCode() == 137 || event.getKeyCode() == 135) && this.mSlptListener != null)) {
            this.mSlptListener.onWakeupFromSlpt(event);
        }
        if (this.isViewDispatchEvent) {
            return super.dispatchKeyEvent(event);
        }
        return false;
    }

    public void setSlptListener(IWakeupFromSlpt listener) {
        this.mSlptListener = listener;
    }

    protected void setIsReceiveHomeKey(boolean isReceiveHomeKey) {
        this.mIsReceiveHomeKey = isReceiveHomeKey;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.mIsReceiveHomeKey) {
            registerHomeKeyReceiver();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mIsReceiveHomeKey) {
            unregisterHomeKeyReceiver();
        }
    }

    private void registerHomeKeyReceiver() {
        LogUtil.m9i(true, TAG, "registerHomeKeyReceiver");
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watch.ACTION_LAUNCH_HOME");
        registerReceiver(this.mHomeReceiver, filter);
    }

    private void unregisterHomeKeyReceiver() {
        LogUtil.m9i(true, TAG, "unregisterHomeKeyReceiver");
        try {
            unregisterReceiver(this.mHomeReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
