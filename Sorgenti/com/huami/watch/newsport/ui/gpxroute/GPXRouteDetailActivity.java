package com.huami.watch.newsport.ui.gpxroute;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Route;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.hs.gpxparser.utils.LogUtils;
import com.hs.gpxparser.utils.SDCardGPSUtils;
import com.huami.watch.common.widget.HmRelativeLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.route.utils.TrailWayPointDrawer;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.utils.ClickScaleAnimation;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class GPXRouteDetailActivity extends BaseActivity implements OnClickListener, EventCallBack {
    public static int INTENT_RESULT = 500;
    private static final String TAG = GPXRouteDetailActivity.class.getName();
    private final int CONTAINER_LOADING = 100;
    private final int CONTAINER_ROUTER = 300;
    private final int CONTAiNER_LOADING_ERROR = 200;
    private String currnetGPXFile;
    private final int drawHeight = 200;
    private final int drawWidth = 200;
    private LoadingGPXDataRunnable loadingGPXDataRunnable;
    private Handler mMainThreadHandler = null;
    private int mSportType = -1;
    private TrailWayPointDrawer mTrailDrawer;
    private HmRelativeLayout rlRouteContainer;
    private RelativeLayout rlRouteUsed;
    private HmRelativeLayout rldialog;
    private ImageView sportInfoTrail;
    private TextView sportInfoTrailTitle;
    private String sportRouteTitle;
    private HmTextView sportRouteTitleView;
    WakeLock wakeLock = null;
    private volatile List<Waypoint> waypointList = null;

    class C08171 extends AnimatorListenerAdapter {
        C08171() {
        }

        public void onAnimationEnd(Animator animation) {
            GPSSPUtils.setCurrentGPXRouteBySportType(GPXRouteDetailActivity.this, GPXRouteDetailActivity.this.sportRouteTitle, GPXRouteDetailActivity.this.currnetGPXFile, GPXRouteDetailActivity.this.mSportType);
            GPXRouteDetailActivity.this.SelectedThisRoute();
        }
    }

    class LoadingGPXDataRunnable implements Runnable {
        private WeakReference<GPXRouteDetailActivity> activity = null;
        private String fileName;

        public LoadingGPXDataRunnable(GPXRouteDetailActivity context, String fileName) {
            this.activity = new WeakReference(context);
            this.fileName = fileName;
        }

        public void run() {
            LogUtils.print(GPXRouteDetailActivity.TAG, " LoadingGPXDataRunnable run");
            GPXRouteDetailActivity.this.loadingGPXData(this.fileName);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.fragment_route_detail_layout);
        this.currnetGPXFile = getIntent().getStringExtra("currnet_file_path");
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        if (this.mSportType > 0) {
            LogUtils.print(TAG, "onCreate currnetGPXFile:" + this.currnetGPXFile + ",mSportType:" + this.mSportType);
            this.mMainThreadHandler = Global.getGlobalUIHandler();
            initView();
            setKeyEventListener(this);
            this.loadingGPXDataRunnable = new LoadingGPXDataRunnable(this, this.currnetGPXFile);
            LogUtils.print(TAG, "onCreate");
            Global.getGlobalWorkHandler().post(this.loadingGPXDataRunnable);
        }
    }

    private void initView() {
        this.rldialog = (HmRelativeLayout) findViewById(C0532R.id.rl_dialog);
        this.rlRouteContainer = (HmRelativeLayout) findViewById(C0532R.id.rl_route_container);
        this.sportInfoTrail = (ImageView) findViewById(C0532R.id.sport_info_trail);
        this.sportInfoTrailTitle = (TextView) findViewById(C0532R.id.sport_info_trail_title);
        this.sportRouteTitleView = (HmTextView) findViewById(C0532R.id.sport_info_trail_title);
        this.rlRouteUsed = (RelativeLayout) findViewById(C0532R.id.rl_route_used);
        this.rlRouteUsed.setOnClickListener(this);
    }

    public void initRoutePic(List<Waypoint> listPoints, String sportRouteTitle) {
        toggerContainer(300);
        LogUtils.print(TAG, "initRoutePic");
        this.sportRouteTitleView.setText(sportRouteTitle);
        this.mTrailDrawer = new TrailWayPointDrawer(this, 200, 200, true);
        this.mTrailDrawer.startDraw();
        this.mTrailDrawer.setStart(C0532R.drawable.sport_start_route);
        this.mTrailDrawer.setEnd(C0532R.drawable.sport_history_end_route);
        this.mTrailDrawer.addLocationDatasToBitmap(listPoints);
        this.sportInfoTrail.setImageDrawable(new BitmapDrawable(getResources(), this.mTrailDrawer.newTrailBitmap()));
        productRouteIcon(this, this.currnetGPXFile, this.waypointList);
    }

    private void toggerContainer(int type) {
        switch (type) {
            case 100:
                this.rlRouteContainer.setVisibility(8);
                this.rldialog.setVisibility(0);
                return;
            case 300:
                LogUtils.print(TAG, "toggerContainer : CONTAINER_ROUTER ");
                this.rldialog.setVisibility(8);
                this.rlRouteContainer.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case C0532R.id.rl_route_used:
                Log.i(TAG, "onClick:  user route  ");
                new ClickScaleAnimation(view, new C08171()).startClickAnimation();
                return;
            default:
                return;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        releaseWakeLock();
    }

    private void SelectedThisRoute() {
        jumpToSportSettingPage();
    }

    private void jumpToSportSettingPage() {
        Log.i(TAG, " jumpToSportSettingPage ");
        setResult(-1);
        finish();
    }

    private void productRouteIcon(final Context mContext, final String filepath, final List<Waypoint> listData) {
        new Thread() {
            public void run() {
                byte[] listObjects = SDCardGPSUtils.getGPSToKernelData(mContext, listData);
                if (listObjects != null && listObjects.length > 0) {
                    LogUtils.print(GPXRouteDetailActivity.TAG, " gpx data to kernel　has data ");
                    HmSensorHubConfigManager.getHmSensorHubConfigManager(mContext).syncGpxTrailData(listObjects, listObjects.length);
                }
                if (TextUtils.isEmpty(GPXRouteDetailActivity.this.sportRouteTitle)) {
                    GPXRouteDetailActivity.this.sportRouteTitle = GPXRouteDetailActivity.this.currnetGPXFile;
                }
                if (new File(SDCardGPSUtils.getGPXRouteImagePath(filepath)).exists()) {
                    Log.i(GPXRouteDetailActivity.TAG, "run: product image is not exist ");
                    return;
                }
                Log.i(GPXRouteDetailActivity.TAG, "run:  product to image bitmap ");
                if (listData != null && listData.size() > 0) {
                    GPXRouteDetailActivity.this.mTrailDrawer = new TrailWayPointDrawer(mContext, 100, 100, true);
                    GPXRouteDetailActivity.this.mTrailDrawer.startDraw();
                    GPXRouteDetailActivity.this.mTrailDrawer.setStart(C0532R.drawable.sport_start_route);
                    GPXRouteDetailActivity.this.mTrailDrawer.setEnd(C0532R.drawable.sport_history_end_route);
                    GPXRouteDetailActivity.this.mTrailDrawer.addLocationDatasToBitmap(listData);
                    Bitmap bitmap = GPXRouteDetailActivity.this.mTrailDrawer.newTrailBitmap();
                    LogUtils.print(GPXRouteDetailActivity.TAG, " save bitmap to sd card : " + filepath);
                    SDCardGPSUtils.saveBitmapToSDCard(bitmap, filepath);
                }
            }
        }.start();
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            Log.i(TAG, " onKeyClick key_center");
            if (this.rlRouteUsed != null) {
                this.rlRouteUsed.performClick();
            }
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    private void loadingGPXData(String fileName) {
        acquireWakeLock();
        LogUtils.print(TAG, "loadingGPXData");
        GPX gpx = SDCardGPSUtils.getWayPointsFromFileName(fileName);
        this.waypointList = null;
        if ((gpx == null || gpx.getTracks().size() <= 0) && (gpx == null || gpx.getRoutes().size() <= 0)) {
            LogUtils.print(TAG, "testParSer size = 0");
        } else {
            LogUtils.print(TAG, "testParSer size > 0");
            if (gpx.getMetadata() == null || gpx.getMetadata().getName() == null) {
                Iterator<Track> iterator = gpx.getTracks().iterator();
                if (iterator.hasNext()) {
                    this.sportRouteTitle = ((Track) iterator.next()).getName();
                }
            } else {
                this.sportRouteTitle = gpx.getMetadata().getName();
            }
            if (this.sportRouteTitle == null || this.sportRouteTitle.length() == 0) {
                this.sportRouteTitle = fileName;
            }
            Iterator<Track> trackIterator = gpx.getTracks().iterator();
            while (trackIterator.hasNext()) {
                this.waypointList = ((TrackSegment) ((Track) trackIterator.next()).getTrackSegments().get(0)).getWaypoints();
            }
            if (gpx.getTracks() == null || gpx.getTracks().size() == 0) {
                HashSet<Route> routeHashSet = gpx.getRoutes();
                if (routeHashSet != null && routeHashSet.size() > 0) {
                    Route route = (Route) routeHashSet.iterator().next();
                    if (route != null) {
                        this.waypointList = route.getRoutePoints();
                    }
                }
            }
            if (this.waypointList == null || this.waypointList.size() <= 0) {
                LogUtils.print(TAG, "loadingGPXData error  :");
            } else {
                LogUtils.print(TAG, "loadingGPXData normal : size:" + this.waypointList.size());
                final List<Waypoint> finalWaypointList = this.waypointList;
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        LogUtils.print(GPXRouteDetailActivity.TAG, "run");
                        GPXRouteDetailActivity.this.initRoutePic(finalWaypointList, GPXRouteDetailActivity.this.sportRouteTitle);
                    }
                });
            }
        }
        releaseWakeLock();
    }

    private void acquireWakeLock() {
        if (this.wakeLock == null) {
            this.wakeLock = ((PowerManager) getSystemService("power")).newWakeLock(536870913, "PostLocationService");
            if (this.wakeLock != null) {
                this.wakeLock.acquire();
            }
        }
    }

    private void releaseWakeLock() {
        if (this.wakeLock != null) {
            this.wakeLock.release();
            this.wakeLock = null;
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
