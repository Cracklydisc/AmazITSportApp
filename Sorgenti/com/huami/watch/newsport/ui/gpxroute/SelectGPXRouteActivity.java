package com.huami.watch.newsport.ui.gpxroute;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.ui.adapter.SelectRouteAdapter;
import com.huami.watch.newsport.ui.adapter.SelectRouteAdapter.IConfigListener;
import com.huami.watch.newsport.ui.view.CustomDialog;
import com.huami.watch.newsport.ui.view.CustomDialog.Builder;

public class SelectGPXRouteActivity extends BaseActivity implements IConfigListener {
    private static final String TAG = SelectGPXRouteActivity.class.getName();
    private CustomDialog customDialog = null;
    private SelectRouteAdapter mAdapter;
    private int mSportType = -1;

    class C08201 implements OnDismissListener {
        C08201() {
        }

        public void onDismiss(DialogInterface dialog) {
            Intent intent = new Intent();
            intent.putExtra("has_no_gpx_file", true);
            SelectGPXRouteActivity.this.setResult(-1, intent);
            SelectGPXRouteActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            this.mSportType = getIntent().getIntExtra("sport_type", -1);
            Log.i(TAG, " mSportType:" + this.mSportType);
        }
        if (this.mSportType == -1) {
            finish();
        }
        HmMenuView view = new HmMenuView(this, null, C0532R.string.gpx_select_empty);
        this.mAdapter = new SelectRouteAdapter(this, this, this.mSportType);
        view.setSimpleMenuAdapter(this.mAdapter);
        setContentView(view);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "requestCode:" + requestCode + ",resultCode:" + requestCode + ",data==null?:" + (data == null));
        if (data != null) {
            int pageJumpType = data.getIntExtra("jump_page_type", 0);
            Log.i(TAG, " pageJumpType:" + pageJumpType);
            switch (pageJumpType) {
                case 1:
                    setResult(-1);
                    finish();
                    return;
                default:
                    return;
            }
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
        setResult(-1);
        finish();
    }

    public void setShowDialogResult() {
        showGPXNoFileDialog();
    }

    private void showGPXNoFileDialog() {
        if (this.customDialog != null) {
            this.customDialog.dismiss();
            this.customDialog = null;
        }
        Builder builder = new Builder(this);
        builder.setMessage(" " + getString(C0532R.string.gpx_no_route_guide));
        builder.setDialogTitle(getString(C0532R.string.gpx_no_find_route));
        this.customDialog = builder.create();
        this.customDialog.setOnDismissListener(new C08201());
        this.customDialog.show();
    }
}
