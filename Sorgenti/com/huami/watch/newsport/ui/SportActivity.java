package com.huami.watch.newsport.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmViewPager;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.battery.BatteryManager;
import com.huami.watch.newsport.battery.BatteryManager.IBatteryChangedListener;
import com.huami.watch.newsport.bleconn.BleConnManager;
import com.huami.watch.newsport.bleconn.BleConnManager.IBleCoreConnCallback;
import com.huami.watch.newsport.cadence.controller.SportCadenceDeviceManager;
import com.huami.watch.newsport.cadence.controller.SportCadenceDeviceManager.IBluetoothGattState;
import com.huami.watch.newsport.cadence.controller.SportCadenceDeviceManager.ICyclingDataListener;
import com.huami.watch.newsport.cadence.model.CyclingDataSnapshot;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.common.model.snapshot.OutdoorSportSnapshot;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.medal.utils.MedalUtils;
import com.huami.watch.newsport.reminder.impl.DeviceConnReminder;
import com.huami.watch.newsport.sportcenter.SportInfoOrderManagerWrapper;
import com.huami.watch.newsport.sportcenter.action.ISportInfoOrderManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager.ISportConnectionListener;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager.ISportDataListener;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.ui.BaseActivity.IWakeupFromSlpt;
import com.huami.watch.newsport.ui.adapter.SportFragmentAdapter;
import com.huami.watch.newsport.ui.fragrunning.AbsSportInfoFragment;
import com.huami.watch.newsport.ui.fragrunning.ExchangeItemFragment;
import com.huami.watch.newsport.ui.fragrunning.ExchangeItemFragment.IExchangeItemAction;
import com.huami.watch.newsport.ui.fragrunning.SportCountDownFragment;
import com.huami.watch.newsport.ui.fragrunning.SportCountDownFragment.OnCountDownListener;
import com.huami.watch.newsport.ui.fragrunning.SportLapFragment;
import com.huami.watch.newsport.ui.fragrunning.SportLapFragment.ILapListener;
import com.huami.watch.newsport.ui.fragrunning.SportMixedInfoFragment;
import com.huami.watch.newsport.ui.fragrunning.SportRealGraphFragment;
import com.huami.watch.newsport.ui.fragrunning.SportRecoveryFragment;
import com.huami.watch.newsport.ui.fragrunning.SportRecoveryFragment.IRecoveryListener;
import com.huami.watch.newsport.ui.fragrunning.SportRouteFragment;
import com.huami.watch.newsport.ui.fragrunning.SportRunningInfoFragment;
import com.huami.watch.newsport.ui.fragrunning.V2HuangHeSportPauseContinueFragment;
import com.huami.watch.newsport.ui.fragrunning.V2SportPauseContinueFragment;
import com.huami.watch.newsport.ui.fragrunning.V2SportPauseContinueFragment.UserActionListener;
import com.huami.watch.newsport.ui.history.SportHistoryDetailActivity;
import com.huami.watch.newsport.ui.view.ExchangeView.IExchangeViewAnim;
import com.huami.watch.newsport.utils.HubCli;
import com.huami.watch.newsport.utils.IntertelRunDataParse;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.ScreenUtils;
import com.huami.watch.newsport.utils.SportGraphUtils;
import com.huami.watch.newsport.utils.TPUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.prompt.PromptWindow.IDialogKeyEventListener;
import com.huami.watch.watchmanager.HmWatchManager;
import com.huami.watch.wearubc.UbcInterface;
import java.util.ArrayList;
import java.util.List;

public class SportActivity extends BaseActivity implements EventCallBack, IBatteryChangedListener, ISportConnectionListener, ISportDataListener, IWakeupFromSlpt, IExchangeItemAction, OnCountDownListener, ILapListener, IRecoveryListener, UserActionListener, IExchangeViewAnim, IDialogKeyEventListener {
    private static final String TAG = SportActivity.class.getName();
    private int currnetTrainIndex = 0;
    private volatile int customIntermittentTrainType = -1;
    private int index = 0;
    private int intervalRemindType = TrainRemindType.NONE.getType();
    private volatile boolean isDoRecordOneLap = false;
    private boolean isOnlySingleTrainType = false;
    private boolean isRegistered = false;
    private boolean isRegisteringSensor = false;
    private boolean isStartedLoadingService = false;
    private boolean isUnRegisteringSensor = false;
    private List<TrainUnit> listTrainUnit = null;
    private SportFragmentAdapter mAdapter;
    private BatteryManager mBattermanager = null;
    private int mBgType = 0;
    private IBleCoreConnCallback mBleCoreConnCallback = new IBleCoreConnCallback() {
        public void onConnectStateChange(int id, int code) {
            BleConnManager.getInstance().unRegisterBleConnListener(this);
            if (!SportActivity.this.mIsDestroyed && code == 100 && id == 0) {
                BleConnManager.getInstance().notifyBleSportStart(SportActivity.this.mSportType);
            }
        }
    };
    private BluetoothDevice mBluetoothDevice = null;
    private IBluetoothGattState mBluetoothGattState = new IBluetoothGattState() {
        public void onConnectStateChanged(int oldState, int newState) {
            LogUtils.print(" SportActivity SportCadenceDeviceManager ", "onConnectStateChanged:" + oldState + "-->" + newState);
            if (newState == 2) {
                SportActivity.this.notifyDeviceConnRemidner(true);
            } else if (newState == 0) {
                SportActivity.this.notifyDeviceConnRemidner(false);
            }
        }
    };
    private BluetoothReceiver mBluetoothReceiver = null;
    private int mClickWakeUpValue = 0;
    private SportCountDownFragment mCountDownFragment = null;
    private int mCurIndex = -1;
    private int mCurLevel = -1;
    private int mCurSportType = -1;
    private volatile int mCurrnetCalence = -1;
    private ICyclingDataListener mCyclingDataListener = new ICyclingDataListener() {
        public void onSummaryChanged(CyclingDataSummary summary) {
            if (summary == null) {
                LogUtils.print(" SportActivity SportCadenceDeviceManager ", "onSummaryChanged: is null ");
            } else if (SportActivity.this.mSportInfoManager != null) {
                if (SportActivity.this.mOutdoorSportSnapshot != null) {
                    SportActivity.this.mOutdoorSportSnapshot.setAveCadence(summary.getAveCadence());
                    SportActivity.this.mOutdoorSportSnapshot.setMaxCadence((float) summary.getMaxCandence());
                    SportActivity.this.mOutdoorSportSnapshot.setDeviceType(6);
                    SportActivity.this.mOutdoorSportSnapshot.setDeviceBrand(600);
                }
                SportActivity.this.mSportDataManager.addCadenceSummaryData(summary);
                LogUtils.print(" SportActivity SportCadenceDeviceManager ", "onSummaryChanged:" + summary.toString());
            }
        }

        public void onDetailChangedList(List<CyclingDataSnapshot> listData) {
            Log.i(SportActivity.TAG, "onDetailChangedList: ");
            if (listData != null && listData.size() > 0) {
                Log.i(SportActivity.TAG, "onDetailChangedList: size: " + listData.size());
                if (SportActivity.this.mSportDataManager != null) {
                    CyclingDataSnapshot lastSnapShot = (CyclingDataSnapshot) listData.get(listData.size() - 1);
                    Log.i(SportActivity.TAG, "onDetailChangedList: " + lastSnapShot.toString());
                    SportActivity.this.mSportDataManager.setCurrnetCaldence(lastSnapShot != null ? lastSnapShot.getCurCadence() : 0);
                }
                SportActivity.this.mSportDataManager.saveCadenceDetailList(SportActivity.this.coverSnapToDetailData(listData));
            }
        }

        public void onLastSummaryQueryEnd() {
            LogUtils.print(" SportActivity SportCadenceDeviceManager ", "onLastSummaryQueryEnd");
        }
    };
    private DeviceConnReminder mDeviceReminder = null;
    private ValueAnimator mEndLoadingAnimator = null;
    private ExchangeItemFragment mExchangedItemFragment = null;
    private long mExchangedItemStartTime = -1;
    private View mFragmentContainer = null;
    private int mGpsStatus = 2;
    private SportRealGraphFragment mHeartInfoFragment = null;
    private HubCli mHubCli = null;
    private ViewPagerPageIndicator mIndicator;
    private boolean mIsConnectedService = false;
    private boolean mIsContinueSport = false;
    private boolean mIsDestroyed = false;
    private boolean mIsGeoOn = false;
    private boolean mIsInvokeLongClick = false;
    private boolean mIsPauseSport = false;
    private boolean mIsPlayingPauseContinueAnim = false;
    private boolean mIsSensorHubDataOn = false;
    private boolean mIsShownPauseUI = false;
    private boolean mIsStopFromChildSport = false;
    private SportLapFragment mLapFragment = null;
    private SensorEventListener mListener;
    private Animation mLoadingAnim = null;
    private Object mLockStartTimeObj = new Object();
    private Sensor mOrientationSensor;
    private OutdoorSportSnapshot mOutdoorSportSnapshot = null;
    private final OnPageChangeListener mPageListener = new C06991();
    private ValueAnimator mPauseContinueEndAnimator = null;
    private AnimatorListener mPauseContinueEndListener = new AnimatorListenerAdapter() {
        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            SportActivity.this.mPauseContinueImg.setAlpha(0.0f);
            SportActivity.this.mPauseContinueImg.setVisibility(4);
            SportActivity.this.mIsPlayingPauseContinueAnim = false;
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            SportActivity.this.mIsPlayingPauseContinueAnim = false;
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            SportActivity.this.mPauseContinueImg.setAlpha(1.0f);
            SportActivity.this.mPauseContinueImg.setVisibility(0);
        }
    };
    private Fragment mPauseContinueFragment = null;
    private HmImageView mPauseContinueImg = null;
    private ValueAnimator mPauseContinueStartAnimator = null;
    private AnimatorListener mPauseContinueStartListener = new AnimatorListenerAdapter() {
        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            SportActivity.this.mPauseContinueImg.setAlpha(0.0f);
            SportActivity.this.mPauseContinueImg.setVisibility(4);
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (SportActivity.this.mPauseContinueEndAnimator.isRunning()) {
                SportActivity.this.mPauseContinueEndAnimator.cancel();
            }
            if (SportActivity.this.mIsContinueSport) {
                if (SportActivity.this.mUIHandler != null && !SportActivity.this.mIsPauseSport) {
                    SportActivity.this.mUIHandler.sendEmptyMessageDelayed(4105, 2000);
                }
            } else if (SportActivity.this.mUIHandler != null && SportActivity.this.mIsPauseSport) {
                SportActivity.this.mUIHandler.sendEmptyMessageDelayed(4112, 2000);
            }
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            SportActivity.this.mPauseContinueImg.setAlpha(0.0f);
            SportActivity.this.mPauseContinueImg.setVisibility(0);
        }
    };
    private BroadcastReceiver mPowerReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.m5i(SportActivity.TAG, "receive power key event: " + action);
            if ("com.huami.watch.POWER_KEY_PRESSED".equals(action)) {
                if (SportActivity.this.mSportDataManager != null && UnitConvertUtils.isHuangheMode()) {
                    int curStatus = SportActivity.this.mSportDataManager.getSportStatus();
                    if (curStatus == 1 || curStatus == 3 || curStatus == 5) {
                        SportActivity.this.onPauseButtonClieked();
                        SportActivity.this.mViewPager.setCurrentItem(0, true);
                    } else if (curStatus == 4 || curStatus == 2) {
                        SportActivity.this.onContinueButtonClicked();
                    }
                }
            } else if ("android.intent.action.SCREEN_ON".equalsIgnoreCase(action)) {
                int curItem = Integer.parseInt(SystemProperties.get("sys.watchface.slpt.index", String.valueOf(-1)));
                Debug.m5i(SportActivity.TAG, "action screen on, slpt index:" + curItem);
                if (curItem > 0) {
                    if (UnitConvertUtils.isHuangheMode()) {
                        curItem++;
                    }
                    SportActivity.this.mViewPager.setCurrentItem(curItem, false);
                }
            } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                Log.i(SportActivity.TAG, "the screen is off");
                SportActivity.this.mSportDataManager.notifySportBackground();
            } else if ("android.intent.action.ACTION_SHUTDOWN".equals(action)) {
                Debug.m5i(SportActivity.TAG, "=============================receive shutdown action start==================================");
                SportActivity.this.mSportDataManager.shutDown();
                Debug.m5i(SportActivity.TAG, "=============================receive shutdown action end==================================");
            }
        }
    };
    private SportRecoveryFragment mRecoveryFragment = null;
    private TextView mResultTextView = null;
    private SportRouteFragment mRouteFragment = null;
    private SensorManager mSensorManager;
    private BaseConfig mSportConfig = null;
    private View mSportContainer = null;
    private SportDataManager mSportDataManager = null;
    private ISportInfoOrderManager mSportInfoManager = null;
    private int mSportType = -1;
    private View mStopAnimContainer = null;
    private View mStopLoadingAnimView = null;
    private Toast mToast = null;
    private int mTrainingMode = -1;
    private UIHandler mUIHandler = null;
    private HmViewPager mViewPager;
    private volatile int requestCadenceDataIndex = 0;

    class C06991 extends SimpleOnPageChangeListener {
        C06991() {
        }

        public void onPageScrollStateChanged(int state) {
            if (state == 0) {
                if (SportActivity.this.mRouteFragment != null) {
                    if (SportActivity.this.mAdapter.getItem(SportActivity.this.mViewPager.getCurrentItem()) instanceof SportRouteFragment) {
                        SportActivity.this.mRouteFragment.setIsForeground(true);
                    } else {
                        SportActivity.this.mRouteFragment.setIsForeground(false);
                    }
                }
                int curItem = SportActivity.this.mViewPager.getCurrentItem();
                LogUtil.m9i(true, SportActivity.TAG, "set selected item:" + curItem);
                if (UnitConvertUtils.isHuangheMode() && curItem != 0) {
                    curItem--;
                }
                SystemProperties.set("sys.watchface.android.index", "" + curItem);
            }
        }
    }

    class C07002 implements Runnable {
        C07002() {
        }

        public void run() {
            HmWatchManager.getHmWatchManager(Global.getApplicationContext()).disableFeature(1, true);
        }
    }

    class C07013 implements Runnable {
        C07013() {
        }

        public void run() {
            HmWatchManager.getHmWatchManager(Global.getApplicationContext()).disableFeature(1, false);
        }
    }

    class C07024 implements Runnable {
        C07024() {
        }

        public void run() {
            if (!SportActivity.this.isFinishing() && !SportActivity.this.isDestroyed()) {
                SportActivity.this.mBattermanager.registerBatteryReceiver();
            }
        }
    }

    class C07035 implements Runnable {
        C07035() {
        }

        public void run() {
            if (!SportActivity.this.isDestroyed()) {
                SportActivity.this.mSportDataManager.setSportConnectionListener(SportActivity.this);
                SportActivity.this.mSportDataManager.registerSportDataListener(SportActivity.this);
                SportActivity.this.mSportDataManager.bindService(SportActivity.this);
            }
        }
    }

    private class BluetoothReceiver extends BroadcastReceiver {
        private BluetoothReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Debug.m3d(SportActivity.TAG, "BluetoothReceiver action:" + action);
            if (!SportActivity.this.isFinishing() && !SportActivity.this.isDestroyed()) {
                if ("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                    try {
                        int state = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                        Debug.m3d(SportActivity.TAG, "receiver blue connect state:" + state);
                        if (state == 2) {
                            SportActivity.this.notifyDeviceConnRemidner(true);
                        } else if (state == 0) {
                            SportActivity.this.notifyDeviceConnRemidner(false);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        Debug.m3d(SportActivity.TAG, "" + e.toString());
                    }
                } else if ("android.ble.heartrate.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                    Debug.m5i(SportActivity.TAG, "ACTION_CONNECTION_STATE_CHANGED");
                    int curState = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
                    int preState = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
                    LogUtil.m9i(true, SportActivity.TAG, "cur state:" + curState + ", preState:" + preState);
                    if (curState == preState) {
                        return;
                    }
                    if (curState == 2) {
                        SportActivity.this.notifyDeviceConnRemidner(true);
                    } else if (curState == 0 && preState != 1) {
                        SportActivity.this.notifyDeviceConnRemidner(false);
                    }
                }
            }
        }
    }

    private class UIHandler extends Handler {
        public UIHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 4105:
                    SportActivity.this.mPauseContinueEndAnimator.start();
                    return;
                case 4112:
                    SportActivity.this.mPauseContinueImg.setAlpha(0.0f);
                    SportActivity.this.mPauseContinueImg.setVisibility(4);
                    SportActivity.this.mIsPlayingPauseContinueAnim = false;
                    SportActivity.this.showPauseContinueView();
                    return;
                case 4113:
                    if ((SportActivity.this.mSportDataManager.getSportStatus() == 1 || SportActivity.this.mSportDataManager.getSportStatus() == 3) && SportType.isSportTypeValid(SportActivity.this.mSportDataManager.getNextChildSportType())) {
                        for (int i = 0; i < SportActivity.this.mAdapter.getCount(); i++) {
                            if (SportActivity.this.mAdapter.getItem(i) instanceof SportMixedInfoFragment) {
                                ((SportMixedInfoFragment) SportActivity.this.mAdapter.getItem(i)).showExchangeView();
                            }
                        }
                        SportActivity.this.hideFragment(SportActivity.this.mPauseContinueFragment);
                        LogUtils.print(SportActivity.TAG, "onKeyLongOneSecondTimeOut, showExchangeView");
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.isAllowBack = false;
        this.isAllowSlptEvent = true;
        setSlptListener(this);
        if (getIntent() != null) {
            this.mSportType = getIntent().getIntExtra("sport_type", -1);
            this.mTrainingMode = getIntent().getIntExtra("entrance_type", -1);
            this.mGpsStatus = getIntent().getIntExtra("sport_gps_status", 2);
            this.mBluetoothDevice = (BluetoothDevice) getIntent().getParcelableExtra("le_qi_device");
            this.mSportConfig = (BaseConfig) getIntent().getParcelableExtra("sport_config");
            this.customIntermittentTrainType = getIntent().getIntExtra("intermittent_train_type", -1);
            this.intervalRemindType = getIntent().getIntExtra("intermittent_remind_type", TrainRemindType.NONE.getType());
            this.isOnlySingleTrainType = getIntent().getBooleanExtra("only_single_train_type", false);
        }
        if (SportType.isSportTypeValid(this.mSportType)) {
            if (this.mSportConfig == null) {
                this.mSportConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            }
            SystemProperties.set("sys.sport.android.scale", String.valueOf(50));
            SystemProperties.set("sys.watchface.android.index", "0");
            this.mBgType = this.mSportConfig.getBgType();
            setContentView(C0532R.layout.sport_layout);
            this.mUIHandler = new UIHandler(Global.getGlobalUIHandler().getLooper());
            initPauseContinueAnim();
            this.mFragmentContainer = findViewById(C0532R.id.fragment_container);
            this.mSportContainer = findViewById(C0532R.id.sport_info_container);
            this.mViewPager = (HmViewPager) findViewById(C0532R.id.pager);
            this.mIndicator = (ViewPagerPageIndicator) findViewById(C0532R.id.indicator);
            if (this.mSportConfig.getBgType() == 1) {
                this.mIndicator.setIndicatorColor(getResources().getColor(C0532R.color.indicator_black_color_normal), getResources().getColor(C0532R.color.indicator_black_color_focused));
            }
            this.mStopAnimContainer = findViewById(C0532R.id.waiting_stop_container);
            this.mStopLoadingAnimView = findViewById(C0532R.id.loading_red_dot);
            this.mPauseContinueImg = (HmImageView) findViewById(C0532R.id.pause_continue_img);
            this.mResultTextView = (TextView) findViewById(C0532R.id.result_tips);
            this.mDeviceReminder = new DeviceConnReminder(this);
            init();
            initPager();
            ScreenUtils.setHomeKeyLock(true);
            registerPowerReceiver();
            setKeyEventListener(this);
            registerBlueToothReceiver();
            initSensorDataAndGeo();
            Secure.putInt(getContentResolver(), "sport_background_mode", this.mSportConfig.getBgType());
            SystemProperties.set("sport.show.theme", String.valueOf(this.mSportConfig.getBgType()));
            SportGraphUtils.setSportDisplayType(this, this.mSportType, this.mSportConfig.getRealGraphShownType());
            BleConnManager.getInstance().registerBleConnListener(this.mBleCoreConnCallback);
            BleConnManager.getInstance().bindBleService(this, 0);
            TPUtils.notifyKernelBgColor(this.mSportConfig.getBgType());
            SystemProperties.set("sys.watchface.android.unit", "" + this.mSportConfig.getUnit());
            Global.getGlobalWorkHandler().post(new C07002());
            return;
        }
        Debug.m5i(TAG, "err sport type: " + this.mSportType);
        finish();
    }

    protected void onResume() {
        super.onResume();
        this.mSportDataManager.notifySportForeground();
        int autoPauseType = this.mSportDataManager.getAutoPauseStatusInScreenOff();
        Log.i(TAG, "autoPauseType" + autoPauseType);
        if (autoPauseType == 1) {
            doSportAutoPaused();
        } else if (autoPauseType == 2) {
            doSportAutoResumed();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (!this.mIsConnectedService) {
            Debug.m6w(TAG, "sport service is not bind while on sport activity destroy");
        }
        if (this.mAdapter != null) {
            this.mAdapter.clearAdapterInfo();
        }
        this.mIsDestroyed = true;
        this.mSportDataManager.setDialogKeyEventListener(null);
        BleConnManager.getInstance().notifyBleSportStop(this.mSportType);
        BleConnManager.getInstance().unbindBleService(0);
        Secure.putInt(getContentResolver(), "sport_background_mode", 0);
        this.mSportDataManager.unregisterSportDataListener(this);
        this.mSportDataManager.setSportConnectionListener(null);
        this.mSportDataManager.unbindService(this);
        this.mSportDataManager.stopService(this);
        this.mBattermanager.unRegisterBatteryReceiver();
        ScreenUtils.setHomeKeyLock(false);
        unRegisterPowerReceiver();
        stopCadenceServer();
        unRegisterBlueToothReceiver();
        BleConnManager.getInstance().unRegisterBleConnListener(this.mBleCoreConnCallback);
        endLoadingAnim();
        if (this.mDeviceReminder != null) {
            this.mDeviceReminder.reset();
        }
        TPUtils.notifyKernelBgColor(0);
        Global.getGlobalWorkHandler().post(new C07013());
    }

    private void init() {
        this.mSportDataManager = SportDataManager.getInstance();
        this.mSportDataManager.setDialogKeyEventListener(this);
        this.mSportDataManager.clearSportDataListener();
        this.mBattermanager = new BatteryManager(this);
        this.mCountDownFragment = SportCountDownFragment.newInstance();
        this.mRecoveryFragment = SportRecoveryFragment.newInstance();
        this.mLapFragment = SportLapFragment.newInstance(this.mSportType);
        this.mLapFragment.setLapListener(this);
        Global.getGlobalUIHandler().postDelayed(new C07024(), 1000);
        this.mBattermanager.setBatterListener(this);
        this.mViewPager.addOnPageChangeListener(this.mPageListener);
        this.mSportDataManager.startService(this);
        Global.getGlobalUIHandler().postDelayed(new C07035(), 300);
        startCadenceServer();
    }

    private void initPager() {
        Debug.m5i(TAG, "initPager");
        initCurSportType();
        if (this.mAdapter == null) {
            this.mAdapter = new SportFragmentAdapter(getSupportFragmentManager());
            this.mViewPager.setAdapter(this.mAdapter);
            this.mIndicator.setViewPager(this.mViewPager);
            this.mIndicator.showIndicator(true);
        } else if (!SportType.isMixedSport(this.mSportType)) {
            return;
        }
        this.mAdapter.clearAdapterInfo();
        this.mSportInfoManager = new SportInfoOrderManagerWrapper(this, this.mCurSportType, SportType.isMixedSport(this.mSportType), this.customIntermittentTrainType, this.intervalRemindType, this.isOnlySingleTrainType);
        if (UnitConvertUtils.isHuangheMode()) {
            this.mPauseContinueFragment = V2HuangHeSportPauseContinueFragment.newInstance(this.mCurSportType, this.mSportConfig);
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).setIsTraingMode(TrainingInfoManager.isInTrainingMode(this.mTrainingMode));
        } else {
            this.mPauseContinueFragment = V2SportPauseContinueFragment.newInstance(this.mCurSportType, this.mSportConfig);
            ((V2SportPauseContinueFragment) this.mPauseContinueFragment).setIsTraingMode(TrainingInfoManager.isInTrainingMode(this.mTrainingMode));
        }
        if (UnitConvertUtils.isHuangheMode()) {
            this.mAdapter.addFragment(this.mPauseContinueFragment);
        }
        if (SportType.isMixedSport(this.mSportType)) {
            this.mAdapter.addFragment(SportMixedInfoFragment.newInstance(this.mCurSportType, SportType.isMixedSport(this.mSportType)));
        } else {
            int firstDataCount;
            if (this.mCurSportType == 1 && this.customIntermittentTrainType != -1) {
                firstDataCount = 2;
            } else if (this.mSportInfoManager.getSportItemSize() >= this.mSportConfig.getDefaultDataShown()) {
                firstDataCount = this.mSportConfig.getDefaultDataShown();
            } else {
                firstDataCount = this.mSportInfoManager.getSportItemSize();
            }
            System.putInt(getContentResolver(), "key_sport_lock_source", firstDataCount);
            Log.i(TAG, " intermittent train first: baseCount:" + 0 + ",firstDataCount:" + firstDataCount);
            this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(this.mCurSportType, 0, firstDataCount, SportType.isMixedSport(this.mSportType), this.customIntermittentTrainType, this.intervalRemindType, this.isOnlySingleTrainType));
            LoadLazy(firstDataCount);
        }
        this.mAdapter.notifyDataSetChanged();
        this.mBattermanager.registerBatteryReceiver();
        if (UnitConvertUtils.isHuangheMode()) {
            this.mViewPager.setCurrentItem(1);
        }
    }

    private void LoadLazy(final int firstDataCount) {
        Global.getGlobalUIHandler().postDelayed(new Runnable() {
            public void run() {
                int baseCount = 0 + firstDataCount;
                Log.i(SportActivity.TAG, "loadLazy:,sportItemSize:" + SportActivity.this.mSportInfoManager.getSportItemSize() + ",firstDataCount:" + firstDataCount + ",baseCount:" + baseCount);
                if (SportActivity.this.mSportInfoManager.getSportItemSize() > firstDataCount) {
                    int remainCount = SportActivity.this.mSportInfoManager.getSportItemSize() - firstDataCount;
                    while (remainCount > 6) {
                        if (remainCount % 7 == 0) {
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 4, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 4;
                            remainCount -= 4;
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 3, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 3;
                            remainCount -= 3;
                        } else if (remainCount % 8 == 0) {
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 4, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 4;
                            remainCount -= 4;
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 4, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 4;
                            remainCount -= 4;
                        } else if (remainCount % 9 == 0) {
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 5, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 5;
                            remainCount -= 5;
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 4, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 4;
                            remainCount -= 4;
                        } else if (remainCount % 10 == 0) {
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 5, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 5;
                            remainCount -= 5;
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 5, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 5;
                            remainCount -= 5;
                        } else {
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 6, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            baseCount += 6;
                            remainCount -= 6;
                        }
                    }
                    if (remainCount > 0) {
                        if (remainCount > firstDataCount) {
                            Log.i(SportActivity.TAG, " remindCount>0 :remindCount:" + remainCount);
                            if (remainCount == 6) {
                                SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 4, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                                baseCount += 4;
                                remainCount -= 4;
                                SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 2, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                                baseCount += 2;
                                remainCount -= 2;
                            } else if (remainCount == 5) {
                                SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 3, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                                baseCount += 3;
                                remainCount -= 3;
                                SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 2, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                                baseCount += 2;
                                remainCount -= 2;
                            } else if (remainCount == 3) {
                                SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, 3, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                            }
                        } else {
                            Log.i(SportActivity.TAG, " remindCount>0 :remindCount:" + remainCount);
                            SportActivity.this.mAdapter.addFragment(SportRunningInfoFragment.newInstance(SportActivity.this.mCurSportType, baseCount, remainCount, SportType.isMixedSport(SportActivity.this.mSportType), SportActivity.this.customIntermittentTrainType, SportActivity.this.intervalRemindType, SportActivity.this.isOnlySingleTrainType));
                        }
                    }
                }
                if (!SportType.isSportTypeNeedGps(SportActivity.this.mCurSportType) || 18 == SportActivity.this.mCurSportType) {
                    SportActivity.this.mRouteFragment = null;
                } else {
                    SportActivity.this.mRouteFragment = SportRouteFragment.newInstance(SportActivity.this.mCurSportType);
                    SportActivity.this.mRouteFragment.setIsForeground(false);
                }
                if (SportGraphUtils.isNeedShowRealGraphy(SportActivity.this.mCurSportType)) {
                    SportActivity.this.mHeartInfoFragment = SportRealGraphFragment.newInstance(SportActivity.this.mSportDataManager.getSportTrackId() < 0 ? System.currentTimeMillis() : SportActivity.this.mSportDataManager.getSportTrackId(), SportActivity.this.mCurSportType, SportActivity.this.mSportConfig);
                } else {
                    SportActivity.this.mHeartInfoFragment = null;
                }
                if (SportActivity.this.mHeartInfoFragment != null) {
                    SportActivity.this.mAdapter.addFragment(SportActivity.this.mHeartInfoFragment);
                }
                if (SportActivity.this.mRouteFragment != null) {
                    SportActivity.this.mAdapter.addFragment(SportActivity.this.mRouteFragment);
                }
                SportActivity.this.mViewPager.setOffscreenPageLimit(SportActivity.this.mAdapter.getDesireCount());
                SportActivity.this.mAdapter.notifyDataSetChanged();
            }
        }, 2000);
    }

    public void onServiceConnected() {
        if (!this.mIsConnectedService && !isDestroyed() && !isFinishing()) {
            this.mIsConnectedService = true;
            int status = this.mSportDataManager.getSportStatus();
            if (status == 0) {
                startNewSport();
                return;
            }
            initPager();
            showSportView();
            if (status == 2 || status == 4) {
                showPauseContinueView();
            }
        }
    }

    public void onServiceDisconnected() {
        this.mIsConnectedService = false;
    }

    public void doSportDataReady(SportSnapshot sportStatus) {
        if (this.mAdapter == null) {
            Debug.m5i(TAG, "doSportDataReady, adapter is null");
            return;
        }
        if (UnitConvertUtils.isHuangheMode()) {
            if (this.mViewPager.getCurrentItem() != 0) {
                refreshCurRealUI(sportStatus);
            } else if (this.mLapFragment != null && this.mLapFragment.isAdded()) {
                this.mLapFragment.updateCurLapInfo(this.mSportDataManager.getCurLapInfo());
            }
        } else if (this.mSportDataManager.getSportStatus() != 2 || this.mPauseContinueFragment == null) {
            refreshCurRealUI(sportStatus);
        } else {
            ((V2SportPauseContinueFragment) this.mPauseContinueFragment).doSportStatusUpdate((OutdoorSportSnapshot) sportStatus);
            if (this.mPauseContinueFragment.isVisible()) {
                ((V2SportPauseContinueFragment) this.mPauseContinueFragment).updateBattery(this.mCurLevel);
            }
        }
        this.mOutdoorSportSnapshot = (OutdoorSportSnapshot) sportStatus;
        if (this.requestCadenceDataIndex % 10 == 0) {
            requestCadenceData();
        }
        this.requestCadenceDataIndex++;
    }

    private void refreshCurRealUI(final SportSnapshot sportStatus) {
        if (this.mSportDataManager.getSportStatus() == 1 || this.mSportDataManager.getSportStatus() == 3) {
            final Fragment fragment = this.mAdapter.getItem(this.mViewPager.getCurrentItem());
            if (fragment != null && (fragment instanceof AbsSportInfoFragment)) {
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        ((AbsSportInfoFragment) fragment).refreshView((OutdoorSportSnapshot) sportStatus);
                        ((AbsSportInfoFragment) fragment).updateBattery(SportActivity.this.mCurLevel);
                    }
                });
            } else if (this.mRouteFragment != null && this.mRouteFragment.isForeground()) {
                this.mRouteFragment.refreshSportView((OutdoorSportSnapshot) sportStatus);
            } else if ((fragment instanceof SportRealGraphFragment) && this.mHeartInfoFragment != null) {
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        SportActivity.this.mHeartInfoFragment.updateRealTimeView((OutdoorSportSnapshot) sportStatus, SportActivity.this.mSportDataManager.getHeartRatesInfo());
                    }
                });
            }
            if (this.mLapFragment != null && this.mLapFragment.isAdded()) {
                this.mLapFragment.updateCurLapInfo(this.mSportDataManager.getCurLapInfo());
            }
        }
    }

    public void doSportMillSecUpdate(int millSec) {
        final Fragment fragment = this.mAdapter.getItem(this.mViewPager.getCurrentItem());
        if (fragment != null && (fragment instanceof AbsSportInfoFragment)) {
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    ((AbsSportInfoFragment) fragment).onMillUpdate(SportActivity.this.mOutdoorSportSnapshot);
                }
            });
        }
    }

    public void doSportStarted() {
        Debug.m5i(TAG, "doSportStarted");
        final SportSnapshot sportSnapshot = this.mSportDataManager.getSportSnapshot();
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (SportActivity.this.mAdapter != null) {
                    for (int i = 0; i < SportActivity.this.mAdapter.getCount(); i++) {
                        Fragment fragment = SportActivity.this.mAdapter.getItem(i);
                        if (fragment != null && (fragment instanceof AbsSportInfoFragment)) {
                            ((AbsSportInfoFragment) fragment).refreshView((OutdoorSportSnapshot) sportSnapshot);
                            ((AbsSportInfoFragment) fragment).updateBattery(SportActivity.this.mCurLevel);
                        }
                    }
                }
            }
        });
        debugStartGeoAndSensorData();
    }

    public void doSportPaused() {
        Debug.m5i(TAG, "doSportPaused");
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(0);
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    SportActivity.this.findViewById(C0532R.id.huanghe_pause_bg).setVisibility(0);
                }
            });
            return;
        }
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                SportActivity.this.showPauseContinueView();
            }
        });
    }

    public void doSportAutoPaused() {
        Debug.m5i(TAG, "doSportAutoPaused");
        if (SportType.isMixedSport(this.mSportType) && this.mAdapter != null && this.mAdapter.getCount() > 0) {
            Fragment fragment = this.mAdapter.getItem(0);
            if (fragment != null && ((SportMixedInfoFragment) fragment).isExchangedAnimStarted()) {
                Debug.m5i(TAG, "doSportAutoPaused failed, the exchange is started");
                return;
            }
        }
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 500, 200, 500});
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(0);
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    SportActivity.this.findViewById(C0532R.id.huanghe_pause_bg).setVisibility(0);
                }
            });
            return;
        }
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                SportActivity.this.showPauseContinueView();
            }
        });
    }

    public void doSportAutoResumed() {
        Debug.m5i(TAG, "doSportAutoResumed");
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 800});
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(1);
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    SportActivity.this.findViewById(C0532R.id.huanghe_pause_bg).setVisibility(8);
                }
            });
            return;
        }
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                SportActivity.this.mIsPlayingPauseContinueAnim = false;
                SportActivity.this.hideFragment(SportActivity.this.mPauseContinueFragment);
                SportActivity.this.showSportView();
            }
        });
    }

    public void doSportContinued() {
        Debug.m5i(TAG, "doSportContinued");
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(1);
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    SportActivity.this.findViewById(C0532R.id.huanghe_pause_bg).setVisibility(8);
                }
            });
            return;
        }
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                SportActivity.this.mIsPlayingPauseContinueAnim = false;
                SportActivity.this.hideFragment(SportActivity.this.mPauseContinueFragment);
                SportActivity.this.showSportView();
            }
        });
    }

    public void doSportStopped(SportSnapshot sportStatus) {
        Debug.m3d(TAG, "onSportStopped");
        debugStopGeoAndSensorData();
        SystemProperties.set("sys.watchface.android.index", "0");
        OutdoorSportSnapshot runningStatus = (OutdoorSportSnapshot) sportStatus;
        DataManager.getInstance().updateSpecificSportNum(this.mSportType, System.currentTimeMillis());
        if (SportDataManager.getInstance().isValidSportWhenEndSport()) {
            this.mSportDataManager.stopService(this);
            goToHistoryDetailUI();
            return;
        }
        Debug.m3d(TAG, "invalid sport record, finish. sport status " + sportStatus);
        finish();
    }

    private void goToHistoryDetailUI() {
        Debug.m5i(TAG, "goToHistoryDetailUI, the sport is end");
        Global.getGlobalUIHandler().post(new Runnable() {

            class C06981 implements Runnable {
                C06981() {
                }

                public void run() {
                    SportActivity.this.finish();
                }
            }

            public void run() {
                SportActivity.this.endLoadingAnim();
                Intent intent = new Intent(SportActivity.this, SportHistoryDetailActivity.class);
                if (SportActivity.this.mSportDataManager.getSportSummry() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("sport_summary", SportActivity.this.mSportDataManager.getSportSummry());
                    bundle.putBoolean("is_from_end_sport", true);
                    intent.putExtras(bundle);
                }
                intent.putExtra("entrance_type", SportActivity.this.mTrainingMode);
                SportActivity.this.startActivity(intent);
                SportActivity.this.overridePendingTransition(0, 0);
                Global.getGlobalUIHandler().postDelayed(new C06981(), 500);
            }
        });
        MedalUtils.checkMedalActive(this.mSportDataManager.getSportTrackId());
    }

    public void doChildSportStopped(SportSnapshot sportStatus) {
        Debug.m5i(TAG, "doChildSportStopped");
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                SportActivity.this.mBattermanager.unRegisterBatteryReceiver();
                SportActivity.this.mIsStopFromChildSport = true;
                SportActivity.this.mAdapter.clearAdapterInfo();
            }
        });
        showExchangedItemView();
        if (SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType())) {
            SensorHubManagerWrapper.getInstance(this).notifySensorOpenHeartRate(null);
        }
        synchronized (this.mLockStartTimeObj) {
            if (this.mExchangedItemStartTime > 0) {
                if (SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType())) {
                    Debug.m5i(TAG, "doChildSportStopped, sendChangeItemTimer: " + this.mExchangedItemStartTime);
                    SensorHubManagerWrapper.getInstance(this).startChangeItemTimer(this.mSportDataManager.getNextChildSportType(), this.mExchangedItemStartTime);
                }
                this.mExchangedItemStartTime = -1;
            }
        }
    }

    public void doSportStateErr() {
        Debug.m5i(TAG, "doSportStateErr");
    }

    public void doRemindUserRouteOffset(int routeStatus) {
        Debug.m5i(TAG, "　doRemindUserRouteOffset ");
        if (this.mRouteFragment != null) {
            this.mRouteFragment.doRouteRunStatus(routeStatus);
        }
    }

    public void doManualOrAutoLapStart(boolean isManual) {
        LogUtil.m9i(true, TAG, "doManualOrAutoLapStart, isManual:" + isManual + "pause State:" + isPauseState());
        if (!isPauseState()) {
            showLapInfoView(isManual);
        }
    }

    public void doSingleTrainUnitNearlyCompleted() {
        LogUtil.m9i(true, TAG, "doSingleTrainUnitNearlyCompleted");
        ((SportRunningInfoFragment) this.mAdapter.getItem(0)).refreshTrainUnitNearFinish();
        ((SportRunningInfoFragment) this.mAdapter.getItem(1)).refreshTrainUnitNearFinish();
    }

    public void doSingleTrainUnitCompleted() {
        LogUtil.m9i(true, TAG, "doSingleTrainUnitCompleted");
    }

    public void doNextTrainUnitStart() {
        LogUtil.m9i(true, TAG, "doNextTrainUnitStart");
        TrainUnit trainUnit = this.mSportDataManager.getCurrentTrainUnit();
        if (trainUnit != null) {
            TrainTargetType trainTargetType = trainUnit.getUnitType();
            if (trainTargetType == null) {
                LogUtil.m9i(true, TAG, "train target type is null");
                return;
            }
            final int trainType = trainTargetType.getType();
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    if (SportActivity.this.mAdapter != null) {
                        for (int i = 0; i < SportActivity.this.mAdapter.getCount(); i++) {
                            Fragment fragment = SportActivity.this.mAdapter.getItem(i);
                            if (fragment instanceof SportRunningInfoFragment) {
                                ((SportRunningInfoFragment) fragment).refreshSportInfoData(trainType);
                            }
                        }
                    }
                }
            });
        }
    }

    public void doWholeTrainUnitCompleted() {
        LogUtil.m9i(true, TAG, "doWholeTrainUnitCompleted");
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (SportActivity.this.mAdapter != null) {
                    for (int i = 0; i < SportActivity.this.mAdapter.getCount(); i++) {
                        Fragment fragment = SportActivity.this.mAdapter.getItem(i);
                        if (fragment instanceof SportRunningInfoFragment) {
                            ((SportRunningInfoFragment) fragment).refreshIntervalFinishContinuedSport();
                            break;
                        }
                    }
                }
                SportActivity.this.mSportDataManager.pauseSport();
            }
        });
    }

    public void onCountFinished() {
        int i = 1;
        Debug.m5i(TAG, "onCountFinished");
        if (this.mIsConnectedService) {
            hideFragment(this.mCountDownFragment);
            showSportView();
            this.mSportDataManager.setIsTrainingMode(this.mTrainingMode == 2);
            SportDataManager sportDataManager = this.mSportDataManager;
            if (this.customIntermittentTrainType < 0) {
                i = 0;
            }
            sportDataManager.setIsInterValTrainMode(i);
            this.mSportDataManager.setGpsStatus(this.mGpsStatus);
            if (this.mIsStopFromChildSport) {
                this.mSportDataManager.startNextChildSport();
                this.mIsStopFromChildSport = false;
                return;
            }
            if (this.customIntermittentTrainType != -1) {
                this.mSportDataManager.addTrainProgram(getTrainProgramData());
            }
            this.mSportDataManager.startSport(this.mSportType);
            return;
        }
        Debug.m5i(TAG, "onCountFinished, not conn sport service");
    }

    private void startCountDownSport() {
        if (SportType.isMixedSport(this.mSportType)) {
            initPager();
        }
        showSportView();
        if (this.mSportType == 14 || this.mSportType == 15) {
            showFragment(this.mCountDownFragment);
            this.mCountDownFragment.startCount();
            return;
        }
        onCountFinished();
    }

    private boolean isPauseState() {
        int currentStatus = this.mSportDataManager.getSportStatus();
        boolean isPauseState = false;
        if (currentStatus == 4 || currentStatus == 2) {
            isPauseState = true;
        }
        Debug.m5i(TAG, "isPauseState, " + isPauseState);
        return isPauseState;
    }

    private void showPauseContinueView() {
        if (this.mPauseContinueFragment == null || this.mPauseContinueFragment.isVisible() || this.mIsPlayingPauseContinueAnim || !isPauseState() || this.mIsShownPauseUI) {
            Object valueOf;
            String str = TAG;
            StringBuilder append = new StringBuilder().append("showPauseContinueView, mPauseContinueFragment: ");
            if (this.mPauseContinueFragment != null) {
                valueOf = Boolean.valueOf(this.mPauseContinueFragment.isVisible());
            } else {
                valueOf = "null";
            }
            Debug.m5i(str, append.append(valueOf).append(", mIsPlayingPauseContinueAnim:").append(this.mIsPlayingPauseContinueAnim).append(", isPauseState:").append(isPauseState()).append(", mIsShownPauseUI:").append(this.mIsShownPauseUI).toString());
            return;
        }
        Debug.m5i(TAG, "showPauseContinueView");
        this.mIsShownPauseUI = true;
        if (this.mRouteFragment != null) {
            this.mRouteFragment.setIsForeground(false);
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(C0532R.anim.fragment_slide_right_enter, 0);
        fragmentTransaction.replace(C0532R.id.fragment_container, this.mPauseContinueFragment);
        fragmentTransaction.commitAllowingStateLoss();
        checkPauseUIState();
    }

    private void checkPauseUIState() {
        Global.getGlobalUIHandler().postDelayed(new Runnable() {
            public void run() {
                if (!SportActivity.this.isDestroyed() && SportActivity.this.mPauseContinueFragment != null) {
                    Debug.m5i(SportActivity.TAG, "checkPauseUIState, mIsShownPauseUI:" + SportActivity.this.mIsShownPauseUI + ", visible:" + SportActivity.this.mPauseContinueFragment.isVisible());
                    FragmentTransaction fragmentTransaction;
                    if (SportActivity.this.mIsShownPauseUI) {
                        if (!SportActivity.this.mPauseContinueFragment.isVisible()) {
                            fragmentTransaction = SportActivity.this.getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(C0532R.anim.fragment_slide_right_enter, 0);
                            fragmentTransaction.replace(C0532R.id.fragment_container, SportActivity.this.mPauseContinueFragment);
                            fragmentTransaction.commitAllowingStateLoss();
                            SportActivity.this.checkPauseUIState();
                        }
                    } else if (SportActivity.this.mPauseContinueFragment.isVisible()) {
                        fragmentTransaction = SportActivity.this.getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.remove(SportActivity.this.mPauseContinueFragment);
                        fragmentTransaction.commitAllowingStateLoss();
                        SportActivity.this.checkPauseUIState();
                    }
                }
            }
        }, 300);
    }

    private void showLapInfoView(boolean isManual) {
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 800});
        if (!this.mLapFragment.isVisible()) {
            showFragment(this.mLapFragment);
        }
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(C0532R.id.fragment_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void onLapDismissed() {
        this.isDoRecordOneLap = false;
        if (this.mLapFragment.isVisible()) {
            hideFragment(this.mLapFragment);
        }
    }

    private void showSportView() {
        if (this.mSportContainer.getVisibility() != 0) {
            int curState = this.mSportDataManager.getSportStatus();
            if (curState == 2 || curState == 5) {
                this.mSportContainer.setVisibility(0);
                SwipeDismissUtil.requestSwipeDismissAlphaBackground(this, this.mViewPager);
            } else {
                this.mSportContainer.setVisibility(0);
                SwipeDismissUtil.requestSwipeDismissAlphaBackground(this, this.mViewPager);
            }
        }
    }

    private void showExchangedItemView() {
        this.mExchangedItemFragment = ExchangeItemFragment.newInstance(this.mSportDataManager.getSportTrackId(), this.mSportDataManager.getNextChildSportType());
        this.mExchangedItemFragment.setListener(this);
        showFragment(this.mExchangedItemFragment);
    }

    private void startNewSport() {
        startCountDownSport();
    }

    private void hideFragment(Fragment fragment) {
        if (fragment instanceof V2SportPauseContinueFragment) {
            if (this.mIsShownPauseUI) {
                if (this.mRouteFragment != null && (this.mAdapter.getItem(this.mViewPager.getCurrentItem()) instanceof SportRouteFragment)) {
                    this.mRouteFragment.setIsForeground(true);
                }
                this.mIsShownPauseUI = false;
                checkPauseUIState();
                Debug.m5i(TAG, "hideFragment pauseUI");
            } else {
                return;
            }
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void initCurSportType() {
        if (!SportType.isMixedSport(this.mSportType)) {
            this.mCurSportType = this.mSportType;
        } else if (this.mSportDataManager != null) {
            int nextChildType = this.mSportDataManager.getNextChildSportType();
            if (SportType.isSportTypeValid(nextChildType)) {
                this.mCurSportType = nextChildType;
            } else {
                this.mCurSportType = ((Integer) ((MixedBaseConfig) this.mSportConfig).getChildSports().get(0)).intValue();
            }
        }
    }

    public void onBatteryChanged(int level, int continueHour, float continueDis, int status) {
        if (this.mAdapter != null) {
            this.mCurLevel = level;
        }
    }

    public void doBatteryChangedRequest() {
    }

    public boolean onContinueButtonClicked() {
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 800});
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(1);
            findViewById(C0532R.id.huanghe_pause_bg).setVisibility(8);
            if (this.mViewPager.getCurrentItem() == 0 && this.mCurIndex >= 0) {
                this.mViewPager.setCurrentItem(this.mCurIndex, true);
            }
        } else {
            hideFragment(this.mPauseContinueFragment);
        }
        return this.mSportDataManager.continueSport();
    }

    public boolean onPauseButtonClieked() {
        this.mCurIndex = this.mViewPager.getCurrentItem();
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 800});
        if (UnitConvertUtils.isHuangheMode()) {
            ((V2HuangHeSportPauseContinueFragment) this.mPauseContinueFragment).changeSportState(0);
            findViewById(C0532R.id.huanghe_pause_bg).setVisibility(0);
        }
        return this.mSportDataManager.pauseSport();
    }

    public boolean onStopButtonClicked(boolean isGiveup) {
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        this.mSportDataManager.setIsGiveupRecord(isGiveup);
        if (this.mStopAnimContainer != null) {
            if (!this.mSportDataManager.isValidSportWhenEndSport()) {
                this.mResultTextView.setText(C0532R.string.sport_result_invalid);
            }
            this.mStopAnimContainer.setVisibility(0);
            startLoadingAnim();
        }
        return this.mSportDataManager.stopSport(true);
    }

    public void onHalfSecStart() {
        LogUtil.m9i(true, TAG, "onHalfSecStart");
        onContinueButtonClicked();
        this.mSportDataManager.notifyHalfSecStart();
    }

    public void onExchangeItemEnd() {
        Debug.m5i(TAG, "onExchangeItemEnd");
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 500, 200, 500});
        if (this.mSportDataManager.getSportStatus() == 0) {
            hideFragment(this.mExchangedItemFragment);
            SensorHubManagerWrapper.getInstance(this).stopChangeItemTimer(System.currentTimeMillis());
            startCountDownSport();
            return;
        }
        Debug.m5i(TAG, "onExchangeItemEnd, the sport is not stopped, cannot stop:" + this.mSportDataManager.getSportStatus());
    }

    public void onExchangedItemStart(long timeStamps) {
        Debug.m5i(TAG, "onExchangedItemStart");
        synchronized (this.mLockStartTimeObj) {
            this.mExchangedItemStartTime = timeStamps;
            if (SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType()) && this.mSportDataManager.getSportStatus() == 0) {
                Debug.m5i(TAG, "onExchangedItemStart, sendChangeItemTimer: " + this.mExchangedItemStartTime);
                SensorHubManagerWrapper.getInstance(this).startChangeItemTimer(this.mSportDataManager.getNextChildSportType(), this.mExchangedItemStartTime);
                this.mExchangedItemStartTime = -1;
            }
        }
    }

    public int getExchangedCurrentBatteryLevel() {
        return this.mCurLevel;
    }

    public void onRecoveryConfirm() {
        goToHistoryDetailUI();
    }

    private void registerPowerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.huami.watch.POWER_KEY_PRESSED");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.ACTION_SHUTDOWN");
        registerReceiver(this.mPowerReceiver, intentFilter);
    }

    private void unRegisterPowerReceiver() {
        if (this.mPowerReceiver == null) {
            Debug.m5i(TAG, "power key receiver is null");
            return;
        }
        try {
            unregisterReceiver(this.mPowerReceiver);
        } catch (Exception e) {
            Debug.m6w(TAG, "power key receiver not registered");
        }
    }

    public void onWakeupFromSlpt(KeyEvent event) {
        Debug.m5i(TAG, "onWakeupFromSlpt, event:" + event + ", status:" + this.mSportDataManager.getSportStatus());
        int i;
        if (event.getKeyCode() == 135 && event.getFlags() == 8) {
            if (!SportType.isMixedSport(this.mSportType)) {
                return;
            }
            if (event.getAction() == 1) {
                if (!(this.mUIHandler.hasMessages(4113) || SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType()))) {
                    handleKeyUpEvent(this.mSportDataManager.getSportStatus());
                }
                this.mUIHandler.removeMessages(4113);
                for (i = 0; i < this.mAdapter.getCount(); i++) {
                    if (this.mAdapter.getItem(i) instanceof SportMixedInfoFragment) {
                        ((SportMixedInfoFragment) this.mAdapter.getItem(i)).cancleExchangeView();
                    }
                }
            } else if (event.getAction() == 0) {
                this.mUIHandler.sendEmptyMessageDelayed(4113, 1000);
            }
        } else if (event.getKeyCode() != 131 || event.getFlags() != 8) {
            if (event.getAction() == 1 && event.getFlags() == 8) {
                int sportStatus = this.mSportDataManager.getSportStatus();
                if (event.getKeyCode() == 131) {
                    handleKeyUpEvent(sportStatus);
                } else if (event.getKeyCode() == 132) {
                    handleKeyDownEvent(sportStatus);
                }
            }
            if (event.getKeyCode() != 137 || event.getRepeatCount() != 0 || !SportType.isMixedSport(this.mSportType)) {
                return;
            }
            if (event.getAction() == 1) {
                if (!(this.mUIHandler.hasMessages(4113) || SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType()))) {
                    handleKeyUpEvent(this.mSportDataManager.getSportStatus());
                }
                this.mUIHandler.removeMessages(4113);
                for (i = 0; i < this.mAdapter.getCount(); i++) {
                    if (this.mAdapter.getItem(i) instanceof SportMixedInfoFragment) {
                        ((SportMixedInfoFragment) this.mAdapter.getItem(i)).cancleExchangeView();
                    }
                }
            } else if (event.getAction() == 0) {
                this.mUIHandler.sendEmptyMessage(4113);
            }
        } else if (event.getAction() == 1) {
            if (SportType.isMixedSport(this.mSportType)) {
                this.mUIHandler.removeMessages(4113);
                for (i = 0; i < this.mAdapter.getCount(); i++) {
                    if (this.mAdapter.getItem(i) instanceof SportMixedInfoFragment) {
                        ((SportMixedInfoFragment) this.mAdapter.getItem(i)).cancleExchangeView();
                    }
                }
                handleKeyUpEvent(this.mSportDataManager.getSportStatus());
                return;
            }
            handleKeyUpEvent(this.mSportDataManager.getSportStatus());
        } else if (event.getAction() == 0 && SportType.isMixedSport(this.mSportType)) {
            this.mUIHandler.sendEmptyMessageDelayed(4113, 1000);
        }
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        LogUtils.print(TAG, "onKeyClick sportStatus:" + this.mSportDataManager.getSportStatus() + ", keyEvent:" + hmKeyEvent);
        if (this.mSportDataManager != null && (this.mPauseContinueFragment == null || !this.mPauseContinueFragment.isVisible())) {
            int sportStatus = this.mSportDataManager.getSportStatus();
            if (hmKeyEvent == HMKeyEvent.KEY_UP) {
                handleKeyUpEvent(sportStatus);
            } else if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
                if (sportStatus != 2 && (sportStatus == 1 || sportStatus == 3)) {
                    if (this.isDoRecordOneLap) {
                        hideFragment(this.mLapFragment);
                        this.isDoRecordOneLap = false;
                    } else {
                        LogUtils.print(TAG, "onKeyClick sportStatus:" + sportStatus + ", viewPager +1 ");
                        if (this.mViewPager != null) {
                            UbcInterface.recordCountEvent("0047");
                            this.mViewPager.setCurrentItem((this.mViewPager.getCurrentItem() + 1) % this.mViewPager.getChildCount(), true);
                        }
                    }
                }
            } else if (hmKeyEvent == HMKeyEvent.KEY_DOWN) {
                handleKeyDownEvent(sportStatus);
            }
        }
        return false;
    }

    private void handleKeyUpEvent(int sportStatus) {
        Debug.m5i(TAG, "handleKeyUpEvent, sportStatus:" + sportStatus);
        if ((this.mPauseContinueFragment != null && this.mPauseContinueFragment.isVisible()) || this.mSportDataManager == null) {
            return;
        }
        if (!SportType.isMixedSport(this.mSportType)) {
            this.isDoRecordOneLap = false;
            if (isCanExecutePauseSport(sportStatus)) {
                Debug.m5i(TAG, "handleKeyUpEvent, pauseSport");
                UbcInterface.recordCountEvent("0045");
                this.mUIHandler.removeMessages(4105);
                this.mSportDataManager.pauseSport();
                startSportPauseAnim();
            } else if (isCanExecuteContinueSport(sportStatus) && !this.mPauseContinueFragment.isVisible()) {
                Debug.m5i(TAG, "handleKeyUpEvent, continueSport");
                UbcInterface.recordCountEvent("0046");
                this.mIsPlayingPauseContinueAnim = false;
                this.mUIHandler.removeMessages(4112);
                this.mSportDataManager.continueSport();
                startSportContinueAnim();
            }
        } else if (this.mExchangedItemFragment == null || !this.mExchangedItemFragment.isVisible()) {
            SportMixedInfoFragment fragment = (SportMixedInfoFragment) this.mAdapter.getItem(0);
            if (fragment == null) {
                Debug.m5i(TAG, "handleKeyUpEvent, fragment is null");
            } else if (!fragment.isExchangedAnimStarted()) {
                if (isCanExecutePauseSport(sportStatus)) {
                    Debug.m5i(TAG, "handleKeyUpEvent, pauseSport");
                    UbcInterface.recordCountEvent("0045");
                    this.mUIHandler.removeMessages(4105);
                    this.mSportDataManager.pauseSport();
                    startSportPauseAnim();
                } else if (isCanExecuteContinueSport(sportStatus) && !this.mPauseContinueFragment.isVisible()) {
                    Debug.m5i(TAG, "handleKeyUpEvent, continueSport");
                    UbcInterface.recordCountEvent("0046");
                    this.mIsPlayingPauseContinueAnim = false;
                    this.mUIHandler.removeMessages(4112);
                    this.mSportDataManager.continueSport();
                    startSportContinueAnim();
                }
            }
        } else {
            Debug.m5i(TAG, "handleKeyUpEvent, onExchangeItemEnd");
            onExchangeItemEnd();
        }
    }

    private boolean isCanExecutePauseSport(int sportStatus) {
        if (sportStatus == 1 || sportStatus == 3 || sportStatus == 5) {
            return true;
        }
        return false;
    }

    private boolean isCanExecuteContinueSport(int sportStatus) {
        if (sportStatus == 2 || sportStatus == 4) {
            return true;
        }
        return false;
    }

    private void handleKeyDownEvent(int sportStatus) {
        if ((this.mPauseContinueFragment != null && this.mPauseContinueFragment.isVisible()) || this.mSportDataManager == null) {
            return;
        }
        if ((this.mSportDataManager.getSportStatus() == 1 || this.mSportDataManager.getSportStatus() == 3) && !SportType.isMixedSport(this.mSportType)) {
            UbcInterface.recordCountEvent("0048");
            if (this.mSportDataManager.isCustomTrainMode()) {
                TrainUnit unit = this.mSportDataManager.getCurrentTrainUnit();
                LogUtil.m9i(true, TAG, "unit:" + unit);
                if (unit.getUnitType() == TrainTargetType.KEYPRESS) {
                    this.mSportDataManager.notifyStartNextCustomTrainUnit();
                }
            } else if (SportType.isNeedManualLap(this.mCurSportType)) {
                this.mSportDataManager.doRecordOneLap();
                this.isDoRecordOneLap = true;
            }
        }
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        LogUtils.print(TAG, "onKeyLongOneSecond");
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        LogUtils.print(TAG, "onKeyLongThreeSecond");
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        LogUtils.print(TAG, "onKeyLongOneSecondTimeOut:" + this.mSportDataManager.getSportStatus());
        if (hmKeyEvent != HMKeyEvent.KEY_UP || SportType.isMixedSport(this.mSportType)) {
        }
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        LogUtils.print(TAG, "onKeyLongThreeSecondTimeOut");
        return false;
    }

    public void onExchangViewAnimationEnd() {
        LogUtil.m9i(true, TAG, "onExchangViewAnimationEnd");
        this.mSportDataManager.stopSport(false);
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 500, 200, 500});
    }

    public void onExchangViewAnimationCancle() {
        LogUtil.m9i(true, TAG, "onExchangViewAnimationCancle");
        if (isPauseState() && this.mPauseContinueFragment != null && !this.mPauseContinueFragment.isVisible()) {
            doSportAutoPaused();
        }
    }

    private void startCadenceServer() {
        if ((this.mSportType == 10 || this.mSportType == 9) && this.mBluetoothDevice != null) {
            LogUtils.print(" SportActivity SportCadenceDeviceManager", "startCadenceServer");
            SportCadenceDeviceManager.getInstance().bindCadenceService(getApplicationContext());
            SportCadenceDeviceManager.getInstance().setDeviceAddress(this.mBluetoothDevice);
            SportCadenceDeviceManager.getInstance().setCyclingDataListener(this.mCyclingDataListener);
            SportCadenceDeviceManager.getInstance().registeBlueStateChangeListener(this.mBluetoothGattState);
            SportCadenceDeviceManager.getInstance().startRequestDataInfo();
            return;
        }
        LogUtils.print(TAG, "startCadenceServer mBluetoothDevice is null ");
    }

    private void stopCadenceServer() {
        if (this.mSportType == 10 || this.mSportType == 9) {
            LogUtils.print(" SportActivity SportCadenceDeviceManager ", "stopCadenceServer");
            SportCadenceDeviceManager.getInstance().endRequestDataInfo();
            SportCadenceDeviceManager.getInstance().unBindCadenceService(getApplicationContext());
            SportCadenceDeviceManager.getInstance().setCyclingDataListener(null);
            SportCadenceDeviceManager.getInstance().unRegisteBlueStateChangeListener(this.mBluetoothGattState);
            SportCadenceDeviceManager.getInstance().stopCadenceService(this);
        }
    }

    private void requestCadenceData() {
        if (this.mSportType == 10 || this.mSportType == 9) {
            LogUtils.print(" SportActivity SportCadenceDeviceManager ", "request Summary info ");
            SportCadenceDeviceManager.getInstance().requestSummaryInfo();
        }
    }

    private List<CyclingDetail> coverSnapToDetailData(List<CyclingDataSnapshot> listData) {
        List<CyclingDetail> list = new ArrayList();
        for (CyclingDataSnapshot snapShot : listData) {
            CyclingDetail currnetDetail = new CyclingDetail();
            currnetDetail.setTrackId(this.mSportDataManager.getSportTrackId());
            currnetDetail.setCurTime(snapShot.getCurTime());
            currnetDetail.setCurCadence(snapShot.getCurCadence());
            currnetDetail.setCurDistance(snapShot.getCurDistance() * 1000.0f);
            currnetDetail.setCurSpeed(snapShot.getCurSpeed());
            currnetDetail.setTemperature(snapShot.getCurTemperature());
            currnetDetail.setDuration(snapShot.getCurDuration() * 1000);
            list.add(currnetDetail);
        }
        return list;
    }

    private void notifyDeviceConnRemidner(boolean isConn) {
        if (this.mDeviceReminder != null) {
            this.mDeviceReminder.notifyDeviceConnStatus(isConn);
        }
    }

    private void registerBlueToothReceiver() {
        if (this.mBluetoothReceiver == null) {
            this.mBluetoothReceiver = new BluetoothReceiver();
        }
        IntentFilter intentFilter = new IntentFilter("android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
        intentFilter.addAction("android.ble.heartrate.profile.action.CONNECTION_STATE_CHANGED");
        registerReceiver(this.mBluetoothReceiver, intentFilter);
    }

    private void unRegisterBlueToothReceiver() {
        if (this.mBluetoothReceiver == null) {
            Debug.m5i(TAG, "bluetooth receiver is null");
            return;
        }
        try {
            unregisterReceiver(this.mBluetoothReceiver);
        } catch (Exception e) {
            Debug.m6w(TAG, "bluetooth receiver not registered");
        }
    }

    public void dispatchDialogKeyEvent(@NonNull KeyEvent event) {
        Debug.m5i(TAG, "dispatchDialogKeyEvent, " + event);
        if (event.getFlags() != 8) {
            return;
        }
        if (event.getKeyCode() == 136 && event.getAction() == 1 && event.getRepeatCount() == 0) {
            handleKeyDownEvent(this.mSportDataManager.getSportStatus());
        } else if (event.getKeyCode() != 135) {
        } else {
            if (SportType.isMixedSport(this.mSportType)) {
                if (event.getAction() == 1) {
                    if (!(this.mUIHandler.hasMessages(4113) || SportType.isSportTypeValid(this.mSportDataManager.getNextChildSportType()))) {
                        handleKeyUpEvent(this.mSportDataManager.getSportStatus());
                    }
                    this.mUIHandler.removeMessages(4113);
                    if (this.mIsInvokeLongClick) {
                        for (int i = 0; i < this.mAdapter.getCount(); i++) {
                            if (this.mAdapter.getItem(i) instanceof SportMixedInfoFragment) {
                                ((SportMixedInfoFragment) this.mAdapter.getItem(i)).cancleExchangeView();
                            }
                        }
                    } else {
                        handleKeyUpEvent(this.mSportDataManager.getSportStatus());
                    }
                    this.mIsInvokeLongClick = false;
                } else if (event.getAction() == 0) {
                    this.mUIHandler.sendEmptyMessageDelayed(4113, 1000);
                }
            } else if (event.getAction() == 1) {
                handleKeyUpEvent(this.mSportDataManager.getSportStatus());
            }
        }
    }

    private void startLoadingAnim() {
        Debug.m5i(TAG, "startLoadingAnim");
        if (this.mStopLoadingAnimView == null) {
            Debug.m5i(TAG, "startLoadingAnim, loading view is null");
            return;
        }
        if (this.mLoadingAnim == null) {
            this.mLoadingAnim = AnimationUtils.loadAnimation(this, C0532R.anim.white_dot_scale);
            this.mLoadingAnim.setAnimationListener(new AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }
            });
        }
        if (this.mStopLoadingAnimView != null) {
            this.mStopLoadingAnimView.startAnimation(this.mLoadingAnim);
        }
    }

    private void endLoadingAnim() {
        Debug.m5i(TAG, "endLoadingAnim");
        if (this.mStopLoadingAnimView != null) {
            this.mStopLoadingAnimView.clearAnimation();
        }
    }

    private void initPauseContinueAnim() {
        this.mPauseContinueStartAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.mPauseContinueStartAnimator.setDuration(250);
        this.mPauseContinueStartAnimator.addListener(this.mPauseContinueStartListener);
        this.mPauseContinueStartAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                SportActivity.this.mPauseContinueImg.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        this.mPauseContinueEndAnimator = ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        this.mPauseContinueEndAnimator.setDuration(250);
        this.mPauseContinueEndAnimator.addListener(this.mPauseContinueEndListener);
        this.mPauseContinueEndAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                SportActivity.this.mPauseContinueImg.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
    }

    private void startSportPauseAnim() {
        if (this.mPauseContinueImg != null) {
            Debug.m5i(TAG, "startSportPauseAnim");
            this.mIsPlayingPauseContinueAnim = true;
            this.mIsPauseSport = true;
            this.mIsContinueSport = false;
            this.mPauseContinueStartAnimator.cancel();
            this.mPauseContinueEndAnimator.cancel();
            if (this.mBgType == 1) {
                this.mPauseContinueImg.setBackground(getDrawable(C0532R.drawable.bg_white_sport_fun_btn_stop));
            } else {
                this.mPauseContinueImg.setBackground(getDrawable(C0532R.drawable.sport_fun_btn_stop));
            }
            this.mPauseContinueStartAnimator.start();
            VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 500, 200, 500});
        }
    }

    private void startSportContinueAnim() {
        if (this.mPauseContinueImg != null) {
            Debug.m5i(TAG, "startSportContinueAnim");
            this.mIsPlayingPauseContinueAnim = true;
            this.mIsContinueSport = true;
            this.mIsPauseSport = false;
            this.mPauseContinueStartAnimator.cancel();
            this.mPauseContinueEndAnimator.cancel();
            if (this.mBgType == 1) {
                this.mPauseContinueImg.setBackground(getDrawable(C0532R.drawable.bg_white_sport_fun_btn_play));
            } else {
                this.mPauseContinueImg.setBackground(getDrawable(C0532R.drawable.sport_fun_btn_play));
            }
            this.mPauseContinueStartAnimator.start();
            VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 800});
        }
    }

    private void initSensorDataAndGeo() {
        boolean z;
        boolean z2 = false;
        if (this.mHubCli == null) {
            this.mHubCli = new HubCli();
        }
        if (this.mSensorManager == null) {
            this.mSensorManager = (SensorManager) getSystemService("sensor");
            this.mOrientationSensor = this.mSensorManager.getDefaultSensor(3);
        }
        if (this.mListener == null) {
            this.mListener = new SensorEventListener() {
                public void onSensorChanged(SensorEvent event) {
                }

                public void onAccuracyChanged(Sensor sensor, int accuracy) {
                }
            };
        }
        if (SystemProperties.getInt("sport_geomagnetic", 0) == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mIsGeoOn = z;
        if (SystemProperties.getInt("sport_sensor_hub_data", 0) == 1 || System.getInt(getContentResolver(), "sensor_caputuring_setting", 0) == 1) {
            z2 = true;
        }
        this.mIsSensorHubDataOn = z2;
        Debug.m5i(TAG, "initSensorDataAndGeo, mIsGeoOn:" + this.mIsGeoOn + ", mIsSensorHubDataOn:" + this.mIsSensorHubDataOn);
    }

    private void debugStartGeoAndSensorData() {
        Debug.m5i(TAG, "debugStartGeoAndSensorData, mIsGeoOn:" + this.mIsGeoOn + ", mIsSensorHubDataOn" + this.mIsSensorHubDataOn);
        if (this.mSportType == 14 || this.mSportType == 15) {
        }
        if (this.mIsSensorHubDataOn) {
            this.mHubCli.reduceCaptureData();
            this.mHubCli.startCapture();
        }
    }

    private void debugStopGeoAndSensorData() {
        Debug.m5i(TAG, "debugStopGeoAndSensorData");
        if (this.mSportType == 14 || this.mSportType == 15) {
        }
        if (this.mIsSensorHubDataOn) {
            this.mHubCli.stopCapture();
        }
    }

    private TrainProgram getTrainProgramData() {
        return IntertelRunDataParse.getInstance().parsetJson(this);
    }
}
