package com.huami.watch.newsport.ui.fragtennic;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.huami.watch.newsport.C0532R;

public class TennicSelectHandFragment extends Fragment implements OnClickListener {
    private LinearLayout llLeftHandLayout;
    private LinearLayout llRightHandLayout;
    private TennicClickEvent mListener;

    public interface TennicClickEvent {
        void onLeftHandClick();

        void onRightHandClick();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TennicClickEvent) {
            this.mListener = (TennicClickEvent) activity;
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.dialog_tennic_select_hand_remind, container, false);
        this.llLeftHandLayout = (LinearLayout) root.findViewById(C0532R.id.ll_left_hand);
        this.llRightHandLayout = (LinearLayout) root.findViewById(C0532R.id.ll_right_hand);
        this.llLeftHandLayout.setOnClickListener(this);
        return root;
    }

    public void onClick(View v) {
        int viewId = v.getId();
        if (this.mListener != null) {
            switch (viewId) {
                case C0532R.id.ll_left_hand:
                    this.mListener.onLeftHandClick();
                    return;
                case C0532R.id.ll_right_hand:
                    this.mListener.onRightHandClick();
                    return;
                default:
                    return;
            }
        }
    }
}
