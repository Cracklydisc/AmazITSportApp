package com.huami.watch.newsport.ui;

import android.content.Intent;
import android.os.Bundle;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.ConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.SportAutoPauseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.SportLapConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.SportRemindConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.SportTargetConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.TETargetConfigMenuAdapter;

public class SportSettingsActivity extends BaseActivity {
    private static final String TAG = SportSettingsActivity.class.getName();
    private BaseConfigMenuAdapter mAdapter;
    private int mSettingsType = -1;
    private int mSportType = -1;

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null) {
            this.mSettingsType = intent.getIntExtra("setting_type", -1);
            this.mSportType = intent.getIntExtra("sport_type", -1);
        }
        if (isAvailableSetting(this.mSettingsType) && SportType.isSportTypeValid(this.mSportType)) {
            HmMenuView view = null;
            if (this.mSettingsType == 0) {
                if (ConfigMenuAdapter.isNeedTERemind(this.mSportType)) {
                    view = new HmMenuView(this, null, C0532R.string.sport_settings_by_sport_data);
                } else {
                    view = new HmMenuView(this, null, C0532R.string.settings_sport_target);
                }
                this.mAdapter = new SportTargetConfigMenuAdapter(this, null, this.mSportType);
            } else if (this.mSettingsType == 1) {
                view = new HmMenuView(this, null, C0532R.string.settings_sport_remind);
                this.mAdapter = new SportRemindConfigMenuAdapter(this, null, this.mSportType);
            } else if (this.mSettingsType == 2) {
                view = new HmMenuView(this, null, C0532R.string.settings_sport_lap);
                this.mAdapter = new SportLapConfigMenuAdapter(this, null, this.mSportType);
            } else if (this.mSettingsType == 3) {
                if (SportType.isMixedSport(this.mSportType)) {
                    view = new HmMenuView(this, null, C0532R.string.settings_sport_auto_pause_triathlon);
                } else {
                    view = new HmMenuView(this, null, C0532R.string.settings_sport_auto_pause);
                }
                this.mAdapter = new SportAutoPauseConfigMenuAdapter(this, null, this.mSportType);
            } else if (this.mSettingsType == 4) {
                view = new HmMenuView(this, null, C0532R.string.sport_settings_te_title);
                this.mAdapter = new TETargetConfigMenuAdapter(this, null, this.mSportType);
            }
            view.setSimpleMenuAdapter(this.mAdapter);
            setContentView(view);
            if (SportType.isMixedSport(this.mSportType) && this.mSettingsType == 1) {
                view.setScrollToCurrentItem(1);
                return;
            }
            return;
        }
        Debug.m5i(TAG, "err setting type:" + this.mSettingsType);
        finish();
    }

    private boolean isAvailableSetting(int type) {
        if (type == 1 || type == 0 || type == 2 || type == 3 || type == 4) {
            return true;
        }
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.mAdapter != null) {
            this.mAdapter.onDataChanged();
        }
        if (requestCode == 32769) {
            setResult(-1);
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
