package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class SportControlMenuAdapter extends BaseConfigMenuAdapter {
    private static final int[] COMPOUND_CONFIG = new int[]{2, 6, 12};
    private static final int[] CROSSING_CONFIG = new int[]{14, 2, 8, 7, 10, 5, 6, 12};
    private static final int[] ELLIPTICAL_MACHINE_CONFIG = new int[]{14, 12};
    private static final int[] INDOORUN_CONFIG = new int[]{14, 2, 8, 7, 5, 12};
    private static final int[] INRIDING_CONFIG = new int[]{14, 12};
    private static final int[] MOUNTAINEER_CONFIG = new int[]{14, 2, 8, 7, 10, 5, 6, 12};
    private static final int[] OPEN_WATER_SWIM_CONFIG = new int[]{7, 15, 5, 12};
    private static final int[] OUTRIDING_CONFIG = new int[]{14, 2, 8, 7, 5, 6, 12};
    private static final int[] RUNNING_CONFIG = new int[]{11, 14, 2, 8, 7, 5, 6, 12};
    private static final int[] SKIING_CONFIG = new int[]{2, 7, 5, 6, 12};
    private static final int[] SWIM_CONFIG = new int[]{8, 7, 5, 12};
    private static final int[] TENNIC_CONFIG = new int[]{16};
    private static final int[] TRIATHLON_CONFIG = new int[]{14, 2, 6, 12};
    private static final int[] WALKING_CONFIG = new int[]{14, 2, 8, 5, 6, 12};
    private int[] mSportConfigs;

    private void initConfig() {
        switch (this.mSportType) {
            case 1:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
            case 6:
                this.mSportConfigs = WALKING_CONFIG;
                return;
            case 7:
                this.mSportConfigs = CROSSING_CONFIG;
                return;
            case 8:
                this.mSportConfigs = INDOORUN_CONFIG;
                return;
            case 9:
                this.mSportConfigs = OUTRIDING_CONFIG;
                return;
            case 10:
                this.mSportConfigs = INRIDING_CONFIG;
                return;
            case 11:
                this.mSportConfigs = SKIING_CONFIG;
                return;
            case 12:
                this.mSportConfigs = ELLIPTICAL_MACHINE_CONFIG;
                return;
            case 13:
                this.mSportConfigs = MOUNTAINEER_CONFIG;
                return;
            case 14:
                this.mSportConfigs = SWIM_CONFIG;
                return;
            case 15:
                this.mSportConfigs = OPEN_WATER_SWIM_CONFIG;
                return;
            case 17:
                this.mSportConfigs = TENNIC_CONFIG;
                return;
            case 2001:
                this.mSportConfigs = TRIATHLON_CONFIG;
                return;
            case 2002:
                this.mSportConfigs = COMPOUND_CONFIG;
                return;
            default:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
        }
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        List<MenuInfo> menuInfoList = new ArrayList();
        initConfig();
        MenuInfo info = new MenuInfo();
        for (int tag : this.mSportConfigs) {
            switch (tag) {
                case 2:
                    info = new MenuInfo();
                    if (SportType.isMixedSport(this.mConfig.getSportType())) {
                        info.title = context.getString(C0532R.string.settings_sport_auto_pause_triathlon);
                    } else {
                        info.title = context.getString(C0532R.string.settings_sport_auto_pause);
                    }
                    if (!this.mConfig.isAutoPause()) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                    } else if (this.mSportType != 1 && this.mSportType != 9) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_on);
                    } else if (Float.compare(this.mConfig.getAutoPausePace(), 0.0f) == 0) {
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                    } else if (this.mSportType == 9) {
                        if (Math.round(this.mConfig.getAutoPausePace() * 3.6f) <= 0) {
                            info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                        } else {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_speed, new Object[]{String.valueOf(Math.round(this.mConfig.getAutoPausePace() * 3.6f))});
                        }
                    } else {
                        int secondPerKm = Math.round(this.mConfig.getAutoPausePace() * 1000.0f);
                        int minute = secondPerKm / 60;
                        int second = secondPerKm % 60;
                        new StringBuilder().append(String.format("%02d", new Object[]{Integer.valueOf(minute)})).append("'").append(String.format("%02d", new Object[]{Integer.valueOf(second)})).append("\"");
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_pace, new Object[]{builder.toString()});
                    }
                    info.iconResId = C0532R.drawable.sport_set_icon_auto_pause;
                    break;
                case 5:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_data_display;
                    info.title = context.getString(C0532R.string.settings_sport_items);
                    info.content = this.mConfig.getDefaultDataShown() == 6 ? context.getString(C0532R.string.settings_sport_six_data) : context.getString(C0532R.string.settings_sport_four_data);
                    break;
                case 6:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_3d_data;
                    info.title = context.getString(C0532R.string.settings_sport_3d_data);
                    info.content = this.mConfig.is3DSportModel() ? context.getString(C0532R.string.setting_on) : context.getString(C0532R.string.setting_off);
                    break;
                case 7:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_realchart;
                    info.title = context.getString(C0532R.string.settings_sport_real_graph);
                    int type = this.mConfig.getRealGraphShownType();
                    if (SportType.isSwimMode(this.mSportType) && type == 0) {
                        type = 5;
                    }
                    switch (type) {
                        case 0:
                            info.content = context.getString(C0532R.string.sport_main_heart_info_title);
                            break;
                        case 1:
                        case 5:
                            info.content = context.getString(C0532R.string.sport_main_pace_info_title);
                            break;
                        case 2:
                            info.content = context.getString(C0532R.string.crossing_history_detail_absolute_altitude);
                            break;
                        case 3:
                            info.content = context.getString(C0532R.string.swim_stroke_speed);
                            break;
                        case 4:
                            info.content = context.getString(C0532R.string.running_speed_desc);
                            break;
                        default:
                            break;
                    }
                case 8:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_iconauto_section;
                    info.title = context.getString(C0532R.string.settings_sport_auto_section);
                    if (this.mConfig.isOpenAutoLapRecord()) {
                        if (this.mSportType != 14) {
                            float distance = this.mConfig.getDistanceAutoLap() / 1000.0f;
                            if (!UnitConvertUtils.isImperial()) {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
                                break;
                            }
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
                            break;
                        }
                        int distance2 = (int) this.mConfig.getDistanceAutoLap();
                        if (this.mConfig.getUnit() != 0) {
                            info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(distance2)});
                            break;
                        }
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(distance2)});
                        break;
                    }
                    info.content = context.getString(C0532R.string.setting_off);
                    break;
                case 10:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.settings_sport_measure_heart);
                    info.iconResId = C0532R.drawable.sport_set_icon_chart_hr;
                    info.content = this.mConfig.isMeasureHeart() ? context.getString(C0532R.string.setting_on) : context.getString(C0532R.string.setting_off);
                    break;
                case 11:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.settings_sport_rt_guide);
                    info.iconResId = C0532R.drawable.sport_set_icon_te_remind;
                    info.content = this.mConfig.isRealTimeGuide() ? context.getString(C0532R.string.setting_on) : context.getString(C0532R.string.setting_off);
                    break;
                case 12:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.settings_sport_bg_type);
                    info.iconResId = C0532R.drawable.sport_set_iconbg_color;
                    info.content = this.mConfig.getBgType() == 0 ? context.getString(C0532R.string.settings_sport_bg_type_normal) : context.getString(C0532R.string.settings_sport_bg_type_white);
                    break;
                case 14:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.settings_item_title_km_voice_remind);
                    info.iconResId = C0532R.drawable.sport_list_voice_on;
                    info.content = this.mConfig.isRemindPlayVoice() ? context.getString(C0532R.string.setting_on) : context.getString(C0532R.string.setting_off);
                    break;
                case 15:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.swim_unit_title);
                    info.iconResId = C0532R.drawable.sport_list_voice_on;
                    info.content = this.mConfig.getUnit() == 0 ? context.getString(C0532R.string.metre_metric) : context.getString(C0532R.string.swim_unit);
                    break;
                case 16:
                    info = new MenuInfo();
                    info.title = context.getString(C0532R.string.sport_setting_clap_the_hands);
                    info.iconResId = C0532R.drawable.sport_set_icon_tennis_left;
                    switch (this.mConfig.getTennicClapThaHands()) {
                        case 0:
                            info.iconResId = C0532R.drawable.sport_set_icon_tennis_left;
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_setting_left_hand);
                            break;
                        case 1:
                            info.iconResId = C0532R.drawable.sport_set_icon_tennis_right;
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_setting_right_hand);
                            break;
                        default:
                            break;
                    }
                default:
                    throw new IllegalArgumentException("err sport tag:" + tag);
            }
            if (!((UnitConvertUtils.isOverSeaVersion() && tag == 14) || (UnitConvertUtils.isHuangheMode() && tag == 11))) {
                info.tag = Integer.valueOf(tag);
                menuInfoList.add(info);
            }
        }
        return menuInfoList;
    }

    public SportControlMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    public void omMenuItemClick(int position) {
        MenuInfo info = (MenuInfo) this.mMenuList.get(position);
        switch (((Integer) info.tag).intValue()) {
            case 2:
                if (this.mSportType != 1 && this.mSportType != 9) {
                    this.mConfig.setIsAutoPause(!this.mConfig.isAutoPause());
                    if (info != null) {
                        info.content = this.mMenuActivity.getString(this.mConfig.isAutoPause() ? C0532R.string.setting_on : C0532R.string.setting_off);
                        notifyDataChanged();
                        break;
                    }
                }
                Intent autoPauseIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                autoPauseIntent.putExtra("selector_type", "select_target_auto_pause_pace");
                autoPauseIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(autoPauseIntent, 291);
                break;
                break;
            case 5:
                Intent lockIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                lockIntent.putExtra("selector_type", "select_target_lock_source");
                lockIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(lockIntent, 291);
                break;
            case 6:
                this.mConfig.setIs3DSportModel(!this.mConfig.is3DSportModel());
                if (info != null) {
                    info.content = this.mMenuActivity.getString(this.mConfig.is3DSportModel() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    notifyDataChanged();
                    break;
                }
                break;
            case 7:
                Intent graphIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                graphIntent.putExtra("selector_type", "select_target_graph_source");
                graphIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(graphIntent, 291);
                break;
            case 8:
                Intent lapIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                lapIntent.putExtra("selector_type", "select_target_auto_lap_dis");
                lapIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(lapIntent, 291);
                break;
            case 10:
                this.mConfig.setIsMeasureHeart(!this.mConfig.isMeasureHeart());
                if (info != null) {
                    info.content = this.mMenuActivity.getString(this.mConfig.isMeasureHeart() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    notifyDataChanged();
                    break;
                }
                break;
            case 11:
                this.mConfig.setIsRealTimeGuide(!this.mConfig.isRealTimeGuide());
                if (info != null) {
                    info.iconResId = C0532R.drawable.sport_set_icon_te_remind;
                    info.content = this.mConfig.isRealTimeGuide() ? this.mMenuActivity.getString(C0532R.string.setting_on) : this.mMenuActivity.getString(C0532R.string.setting_off);
                    notifyDataChanged();
                    break;
                }
                break;
            case 12:
                Intent bgIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                bgIntent.putExtra("selector_type", "select_target_background_type");
                bgIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(bgIntent, 291);
                break;
            case 14:
                this.mConfig.setIsRemindPlayVoice(!this.mConfig.isRemindPlayVoice());
                if (info != null) {
                    info.content = this.mMenuActivity.getString(this.mConfig.isRemindPlayVoice() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    notifyDataChanged();
                    break;
                }
                break;
            case 15:
                Intent unitIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                unitIntent.putExtra("selector_type", "select_target_swim_unit");
                unitIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(unitIntent, 291);
                break;
            case 16:
                Intent clapThaHandsIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                clapThaHandsIntent.putExtra("selector_type", "select_clap_the_hand");
                clapThaHandsIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(clapThaHandsIntent, 291);
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < this.mMenuList.size(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 2:
                    if (this.mConfig.isAutoPause()) {
                        if (this.mSportType == 1 || this.mSportType == 9) {
                            if (Float.compare(this.mConfig.getAutoPausePace(), 0.0f) != 0) {
                                if (this.mSportType != 9) {
                                    int secondPerKm = Math.round(this.mConfig.getAutoPausePace() * 1000.0f);
                                    int minute = secondPerKm / 60;
                                    int second = secondPerKm % 60;
                                    new StringBuilder().append(String.format("%02d", new Object[]{Integer.valueOf(minute)})).append("'").append(String.format("%02d", new Object[]{Integer.valueOf(second)})).append("\"");
                                    info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_pace, new Object[]{builder.toString()});
                                    break;
                                }
                                if (Math.round(this.mConfig.getAutoPausePace() * 3.6f) > 0) {
                                    info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_speed, new Object[]{String.valueOf(Math.round(this.mConfig.getAutoPausePace() * 3.6f))});
                                    break;
                                } else {
                                    info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                                    break;
                                }
                            }
                            info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                            break;
                        }
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_on);
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                    break;
                case 5:
                    info.content = this.mConfig.getDefaultDataShown() == 6 ? this.mMenuActivity.getString(C0532R.string.settings_sport_six_data) : this.mMenuActivity.getString(C0532R.string.settings_sport_four_data);
                    break;
                case 7:
                    int type = this.mConfig.getRealGraphShownType();
                    if (SportType.isSwimMode(this.mSportType) && type == 0) {
                        type = 5;
                    }
                    switch (type) {
                        case 0:
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_main_heart_info_title);
                            break;
                        case 1:
                        case 5:
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_main_pace_info_title);
                            break;
                        case 2:
                            info.content = this.mMenuActivity.getString(C0532R.string.crossing_history_detail_absolute_altitude);
                            break;
                        case 3:
                            info.content = this.mMenuActivity.getString(C0532R.string.swim_stroke_speed);
                            break;
                        case 4:
                            info.content = this.mMenuActivity.getString(C0532R.string.running_speed_desc);
                            break;
                        default:
                            break;
                    }
                case 8:
                    info.title = this.mMenuActivity.getString(C0532R.string.settings_sport_auto_section);
                    if (this.mConfig.isOpenAutoLapRecord()) {
                        if (this.mSportType != 14) {
                            float distance = this.mConfig.getDistanceAutoLap() / 1000.0f;
                            if (!UnitConvertUtils.isImperial()) {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{String.valueOf(distance)});
                                break;
                            } else {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{String.valueOf(distance)});
                                break;
                            }
                        }
                        int distance2 = (int) this.mConfig.getDistanceAutoLap();
                        if (this.mConfig.getUnit() != 0) {
                            info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(distance2)});
                            break;
                        } else {
                            info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(distance2)});
                            break;
                        }
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                    break;
                case 12:
                    info.content = this.mConfig.getBgType() == 0 ? this.mMenuActivity.getString(C0532R.string.settings_sport_bg_type_normal) : this.mMenuActivity.getString(C0532R.string.settings_sport_bg_type_white);
                    break;
                case 15:
                    info.content = this.mConfig.getUnit() == 0 ? this.mMenuActivity.getString(C0532R.string.metre_metric) : this.mMenuActivity.getString(C0532R.string.swim_unit);
                    break;
                case 16:
                    switch (this.mConfig.getTennicClapThaHands()) {
                        case 0:
                            info.iconResId = C0532R.drawable.sport_set_icon_tennis_left;
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_setting_left_hand);
                            break;
                        case 1:
                            info.iconResId = C0532R.drawable.sport_set_icon_tennis_right;
                            info.content = this.mMenuActivity.getString(C0532R.string.sport_setting_right_hand);
                            break;
                        default:
                            break;
                    }
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
