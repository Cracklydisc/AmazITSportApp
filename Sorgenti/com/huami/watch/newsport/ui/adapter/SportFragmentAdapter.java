package com.huami.watch.newsport.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.log.Debug;
import java.util.LinkedList;
import java.util.List;

public class SportFragmentAdapter extends FragmentPagerAdapter {
    FragmentManager fm;
    int mCount = 0;
    int mDataChangedCount = 0;
    List<Fragment> mFragmentLists = new LinkedList();

    public SportFragmentAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    public void addFragment(Fragment fragment) {
        this.mFragmentLists.add(fragment);
        this.mDataChangedCount = this.mFragmentLists.size();
    }

    public Fragment getItem(int position) {
        if (this.mFragmentLists != null && !this.mFragmentLists.isEmpty()) {
            return (Fragment) this.mFragmentLists.get(position);
        }
        Debug.m5i("SportFragmentAdapter", "fragment is null");
        return null;
    }

    public int getCount() {
        return this.mCount;
    }

    public int getItemPosition(Object object) {
        return -2;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == ((Fragment) obj).getView();
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    public void notifyDataSetChanged() {
        this.mCount = this.mDataChangedCount;
        super.notifyDataSetChanged();
    }

    public int getDesireCount() {
        return this.mDataChangedCount;
    }

    public void clearAdapterInfo() {
        try {
            FragmentTransaction transaction = this.fm.beginTransaction();
            for (Fragment fragment : this.mFragmentLists) {
                transaction.remove(fragment);
            }
            this.mCount = 0;
            this.mDataChangedCount = 0;
            transaction.commitAllowingStateLoss();
            this.fm.executePendingTransactions();
            this.mFragmentLists.clear();
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
