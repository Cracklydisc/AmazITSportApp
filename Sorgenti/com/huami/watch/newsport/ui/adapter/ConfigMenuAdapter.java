package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Global;
import android.util.Log;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.hs.gpxparser.utils.LogUtils;
import com.hs.gpxparser.utils.SDCardGPSUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.setting.swimlength.SwimLengthActivity;
import com.huami.watch.newsport.ui.RunningTargetSettingActivity;
import com.huami.watch.newsport.ui.SportControlSettingActivity;
import com.huami.watch.newsport.ui.SportSettingsActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.ui.gpxroute.SelectGPXRouteActivity;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ConfigMenuAdapter extends BaseConfigMenuAdapter {
    private static final int[] COMPOUND_CONFIG = new int[]{1, 13, 3};
    private static final int[] CROSSING_CONFIG = new int[]{1, 13, 4, 3};
    private static final int[] ELLIPTICAL_MACHINE_CONFIG = new int[]{0, 1, 13, 3};
    private static final int[] INDOORUN_CONFIG = new int[]{0, 1, 13, 3};
    private static final int[] INRIDING_CONFIG = new int[]{0, 1, 13, 3};
    private static final int[] MOUNTAINEER_CONFIG = new int[]{1, 13, 4, 3};
    private static final int[] OPEN_WATER_SWIM_CONFIG = new int[]{0, 13};
    private static final int[] OUTRIDING_CONFIG = new int[]{0, 1, 13, 4, 3};
    private static final int[] RUNNING_CONFIG = new int[]{0, 1, 13, 4, 3};
    private static final int[] SKIING_CONFIG = new int[]{1, 13, 4, 3};
    private static final int[] SOCCER_CONFIG = new int[]{1};
    private static final int[] SWIM_CONFIG = new int[]{0, 1, 13, 9};
    private static final int[] TENNIC_CONFIG = new int[]{1, 13};
    private static final int[] TRIATHLON_CONFIG = new int[]{1, 13, 3};
    private static final int[] WALKING_CONFIG = new int[]{0, 1, 13, 4, 3};
    private int[] mSportConfigs;

    private void initConfig() {
        switch (this.mSportType) {
            case 1:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
            case 6:
                this.mSportConfigs = WALKING_CONFIG;
                return;
            case 7:
                this.mSportConfigs = CROSSING_CONFIG;
                return;
            case 8:
                this.mSportConfigs = INDOORUN_CONFIG;
                return;
            case 9:
                this.mSportConfigs = OUTRIDING_CONFIG;
                return;
            case 10:
                this.mSportConfigs = INRIDING_CONFIG;
                return;
            case 11:
                this.mSportConfigs = SKIING_CONFIG;
                return;
            case 12:
                this.mSportConfigs = ELLIPTICAL_MACHINE_CONFIG;
                return;
            case 13:
                this.mSportConfigs = MOUNTAINEER_CONFIG;
                return;
            case 14:
                this.mSportConfigs = SWIM_CONFIG;
                return;
            case 15:
                this.mSportConfigs = OPEN_WATER_SWIM_CONFIG;
                return;
            case 17:
                this.mSportConfigs = TENNIC_CONFIG;
                return;
            case 18:
                this.mSportConfigs = SOCCER_CONFIG;
                return;
            case 2001:
                this.mSportConfigs = TRIATHLON_CONFIG;
                return;
            case 2002:
                this.mSportConfigs = COMPOUND_CONFIG;
                return;
            default:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
        }
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        List<MenuInfo> menuInfoList = new ArrayList();
        initConfig();
        MenuInfo info = new MenuInfo();
        for (int tag : this.mSportConfigs) {
            switch (tag) {
                case 0:
                    info.iconResId = C0532R.drawable.sport_set_icon_target;
                    info.title = context.getString(C0532R.string.settings_sport_target);
                    if (!isOpenTarget(this.mConfig)) {
                        info.content = context.getString(C0532R.string.setting_no_set_target);
                        break;
                    }
                    info.content = null;
                    break;
                case 1:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_remind;
                    info.title = context.getString(C0532R.string.settings_sport_remind);
                    break;
                case 3:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_device_connect;
                    info.title = context.getString(C0532R.string.settings_sport_cadence_link);
                    break;
                case 4:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_sport_route;
                    info.title = context.getString(C0532R.string.settings_sport_route);
                    String[] selectContent = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(this.mMenuActivity, this.mSportType);
                    info.content = null;
                    if (!(selectContent == null || selectContent.length <= 0 || selectContent[0] == null || selectContent[1] == null)) {
                        boolean isFileExists = new File(SDCardGPSUtils.getGPXRouteFilePath(selectContent[1])).exists();
                        Log.i("HmConfigMenuAdapter", " selectContent: content:" + selectContent[0] + ",filePath:" + selectContent[1] + "isFileExists:" + isFileExists);
                        if (isFileExists) {
                            info.content = selectContent[0];
                        } else {
                            info.content = null;
                            GPSSPUtils.setCurrentGPXRouteBySportType(this.mMenuActivity, null, null, this.mSportType);
                        }
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 9:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
                    info.title = context.getString(C0532R.string.settings_sport_swim_length);
                    if (this.mConfig.getUnit() != 0) {
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                    break;
                case 13:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_sportset;
                    info.title = context.getString(C0532R.string.settings_sport_control);
                    break;
                default:
                    throw new IllegalArgumentException("err sport tag:" + tag);
            }
            info.tag = Integer.valueOf(tag);
            menuInfoList.add(info);
        }
        return menuInfoList;
    }

    public ConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    public void omMenuItemClick(int position) {
        switch (((Integer) ((MenuInfo) this.mMenuList.get(position)).tag).intValue()) {
            case 0:
                if (!isOpenTarget(this.mConfig)) {
                    Intent targetDistanceIntent;
                    if (!isNeedTERemind(this.mSportType)) {
                        targetDistanceIntent = new Intent(this.mMenuActivity, SportSettingsActivity.class);
                        targetDistanceIntent.putExtra("setting_type", 0);
                        targetDistanceIntent.putExtra("sport_type", this.mSportType);
                        this.mMenuActivity.startActivityForResult(targetDistanceIntent, 291);
                        break;
                    }
                    targetDistanceIntent = new Intent(this.mMenuActivity, SportSettingsActivity.class);
                    targetDistanceIntent.putExtra("setting_type", 4);
                    targetDistanceIntent.putExtra("sport_type", this.mSportType);
                    this.mMenuActivity.startActivityForResult(targetDistanceIntent, 291);
                    break;
                }
                Intent teIntent = new Intent(this.mMenuActivity, RunningTargetSettingActivity.class);
                teIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(teIntent, 291);
                break;
            case 1:
                Intent targetRemindIntent = new Intent(this.mMenuActivity, SportSettingsActivity.class);
                targetRemindIntent.putExtra("setting_type", 1);
                targetRemindIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetRemindIntent, 291);
                break;
            case 3:
                startConnectBleActivity();
                break;
            case 4:
                Intent routeSelect = new Intent(this.mMenuActivity, SelectGPXRouteActivity.class);
                routeSelect.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(routeSelect, 291);
                break;
            case 9:
                PointUtils.recordEventProperty("0040", this.mSportType);
                Intent swimIntent = new Intent(this.mMenuActivity, SwimLengthActivity.class);
                swimIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(swimIntent, 292);
                break;
            case 13:
                Intent controlIntent = new Intent(this.mMenuActivity, SportControlSettingActivity.class);
                controlIntent.putExtra("widget_settings_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(controlIntent, 291);
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < this.mMenuList.size(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 0:
                    if (!isOpenTarget(this.mConfig)) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_no_set_target);
                        break;
                    } else {
                        info.content = null;
                        break;
                    }
                case 3:
                    LogUtils.print("HmConfigMenuAdapter", "onDataChanged SPORT_DEVICE_CONN ");
                    break;
                case 4:
                    LogUtils.print("HmConfigMenuAdapter", " SPORT_ROUTE_IMPORT onDataChanged");
                    String[] selectContent = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(this.mMenuActivity, this.mSportType);
                    if (selectContent != null && selectContent.length > 0 && selectContent[0] != null && selectContent[1] != null) {
                        info.content = selectContent[0];
                        if (!new File(SDCardGPSUtils.getGPXRouteFilePath(selectContent[1])).exists()) {
                            info.content = null;
                            GPSSPUtils.setCurrentGPXRouteBySportType(this.mMenuActivity, null, null, this.mSportType);
                            break;
                        }
                        info.content = selectContent[0];
                        break;
                    }
                    info.content = null;
                    GPSSPUtils.setCurrentGPXRouteBySportType(this.mMenuActivity, null, null, this.mSportType);
                    break;
                case 9:
                    info.title = this.mMenuActivity.getString(C0532R.string.settings_sport_swim_length);
                    if (this.mConfig.getUnit() != 0) {
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                    break;
                default:
                    break;
            }
        }
        notifyDataChanged();
    }

    private void startConnectBleActivity() {
        boolean isAirPlaneMode = true;
        try {
            if (Global.getInt(this.mMenuActivity.getContentResolver(), "airplane_mode_on", 0) != 1) {
                isAirPlaneMode = false;
            }
            if (isAirPlaneMode) {
                Intent intentAir = new Intent();
                intentAir.setComponent(new ComponentName("com.huami.watch.wearairplanemodedialog", "com.huami.watch.wearairplanemodedialog.AirPlaneModeDialogActivity"));
                intentAir.setFlags(268435456);
                this.mMenuActivity.startActivity(intentAir);
                return;
            }
            Intent connectIntent = new Intent("com.huami.moblie.watchsettings.actions.bluetooth.DevicePicker");
            connectIntent.setPackage("com.huami.mobile.watchsettings");
            connectIntent.putExtra("which_sub_settings", 1);
            connectIntent.putExtra("show_ble", 1);
            connectIntent.addCategory("android.intent.category.DEFAULT");
            this.mMenuActivity.startActivityForResult(connectIntent, 102);
            this.mMenuActivity.overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
        } catch (ActivityNotFoundException e) {
            Debug.m3d("HmConfigMenuAdapter", "bluetooth activity not found");
        }
    }

    public static boolean isOpenTarget(BaseConfig config) {
        if (config == null) {
            return false;
        }
        if ((config.isRemindTE() && !UnitConvertUtils.isHuangheMode()) || config.isRemindTargetDistance() || config.isRemindTargetTimeCost() || config.isRemindCalorie() || config.isRemindTargetTrips()) {
            return true;
        }
        return false;
    }

    public static boolean isNeedTERemind(int sportType) {
        if (sportType != 1 || UnitConvertUtils.isHuangheMode()) {
            return false;
        }
        return true;
    }
}
