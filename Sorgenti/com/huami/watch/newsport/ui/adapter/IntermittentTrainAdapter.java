package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.SportTargetRecommandActivity;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.ui.IntermittentTrainingActivity;
import com.huami.watch.newsport.ui.SelectIntermittentTrainActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.ui.view.IntervalRunDialog;
import com.huami.watch.newsport.ui.view.IntervalRunDialog.Builder;
import java.util.ArrayList;
import java.util.List;

public class IntermittentTrainAdapter extends BaseConfigMenuAdapter {
    private static final String TAG = IntermittentTrainAdapter.class.getSimpleName();
    private IntervalRunDialog intervalRunDialog = null;

    class C07151 implements OnKeyListener {
        C07151() {
        }

        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            Log.i(IntermittentTrainAdapter.TAG, " keyCode:" + keyCode + ",event:" + event.getKeyCode());
            if (keyCode == HMKeyEvent.KEY_UP.realAndKeyCode()) {
                Log.i(IntermittentTrainAdapter.TAG, " keyUp ");
                IntermittentTrainAdapter.this.jumpToSelectIntermittentTrainPage();
                dialog.dismiss();
            } else if (keyCode == HMKeyEvent.KEY_CENTER.realAndKeyCode()) {
                Log.i(IntermittentTrainAdapter.TAG, " keyCenter ");
                IntermittentTrainAdapter.this.jumpToSelectIntermittentTrainPage();
                dialog.dismiss();
            } else if (keyCode == HMKeyEvent.KEY_DOWN.realAndKeyCode()) {
                Log.i(IntermittentTrainAdapter.TAG, " keyDown ");
            }
            return false;
        }
    }

    class C07162 implements OnDismissListener {
        C07162() {
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
        }
    }

    class C07173 implements OnClickListener {
        C07173() {
        }

        public void onClick(DialogInterface dialog, int which) {
            IntermittentTrainAdapter.this.jumpToSelectIntermittentTrainPage();
            dialog.dismiss();
        }
    }

    public IntermittentTrainAdapter(Activity context, IConfigListener listener, int sportType, boolean isSupportRecommend) {
        super(context, listener, sportType, isSupportRecommend);
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        Log.i(TAG, "getMenuInfos :" + this.isSupportRecommandSport);
        List<MenuInfo> menuInfoList = new ArrayList();
        MenuInfo menuInfo = new MenuInfo();
        menuInfo.title = this.mMenuActivity.getString(C0532R.string.run_train_recommend);
        menuInfo.iconResId = C0532R.drawable.sport_set_icon_target_te;
        menuInfo.tag = Integer.valueOf(0);
        Log.i(TAG, "isSupportRecommendSport:" + this.isSupportRecommandSport);
        if (this.isSupportRecommandSport) {
            menuInfoList.add(menuInfo);
        }
        menuInfo = new MenuInfo();
        menuInfo.title = this.mMenuActivity.getString(C0532R.string.run_train_intermittent);
        menuInfo.tag = Integer.valueOf(1);
        menuInfo.iconResId = C0532R.drawable.sport_set_icon_interval_run;
        menuInfoList.add(menuInfo);
        return menuInfoList;
    }

    public void omMenuItemClick(int position) {
        switch (((Integer) ((MenuInfo) this.mMenuList.get(position)).tag).intValue()) {
            case 0:
                Intent intent = new Intent(this.mMenuActivity, SportTargetRecommandActivity.class);
                intent.addFlags(536870912);
                intent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(intent, 8705);
                Log.i(TAG, " train_recommend ");
                return;
            case 1:
                Log.i(TAG, " train_intermittent ");
                showTips();
                return;
            default:
                return;
        }
    }

    public void onDataChanged() {
        super.onDataChanged();
    }

    private void jumpToSelectIntermittentTrainPage() {
        Intent selectIntermittentTrainIntent = new Intent(this.mMenuActivity, SelectIntermittentTrainActivity.class);
        selectIntermittentTrainIntent.addFlags(536870912);
        selectIntermittentTrainIntent.putExtra("widget_settings_type", this.mSportType);
        ((IntermittentTrainingActivity) this.mMenuActivity).startActivityForResult(selectIntermittentTrainIntent, 8706);
    }

    private void showTips() {
        if (DataManager.getInstance().isFirstShowIntervalTrainTips()) {
            this.intervalRunDialog = new Builder(this.mMenuActivity).setPositiveButton(new C07173()).setOnDismissListener(new C07162()).setOnKeyListener(new C07151()).create();
            this.intervalRunDialog.show();
            DataManager.getInstance().setIsFirstShowIntervalTrainTips(false);
            return;
        }
        jumpToSelectIntermittentTrainPage();
    }
}
