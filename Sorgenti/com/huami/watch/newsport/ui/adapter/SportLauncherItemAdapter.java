package com.huami.watch.newsport.ui.adapter;

import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.DrawableItem.OnClickListener;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.wheel.AbstractWheelAdapter;
import clc.utils.venus.widgets.wheel.WheelViewItemsLayout;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SortInfo;
import com.huami.watch.newsport.ui.view.MainSettingsAction;
import com.huami.watch.newsport.ui.view.SportLauncherTitle;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SportLauncherItemAdapter extends AbstractWheelAdapter implements OnClickListener {
    public static int[] SETTINGS_ACTION_IDS = new int[]{C0532R.id.start_run, C0532R.id.start_walk, C0532R.id.start_outdoor_riding, C0532R.id.start_indoor_swim, C0532R.id.start_open_water_swim, C0532R.id.start_indoor_run, C0532R.id.start_indoor_riding, C0532R.id.start_ellipticals, C0532R.id.start_mountaineer, C0532R.id.start_cross, C0532R.id.start_triathlon, C0532R.id.start_skiing, C0532R.id.start_tennis, C0532R.id.start_soccer, C0532R.id.start_compound};
    public static int[] SETTING_ACTION_ICON_RES_ARRAY = new int[]{C0532R.drawable.sport_menu_running, C0532R.drawable.sport_menu_walk, C0532R.drawable.sport_menu_outdoor_riding, C0532R.drawable.sport_menu_indoor_swimming, C0532R.drawable.sport_menu_outdoor_swimming, C0532R.drawable.sport_menu_treadmill, C0532R.drawable.sport_menu_indoor_riding, C0532R.drawable.sport_menu_ellipticals, C0532R.drawable.sport_menu_climb_mountain, C0532R.drawable.sport_menu_trail_running, C0532R.drawable.sport_menu_triathlon, C0532R.drawable.sport_menu_skiing, C0532R.drawable.sport_menu_tennis, C0532R.drawable.sport_menu_football, C0532R.drawable.sport_menu_complex_sport};
    public static int[] SETTING_ACTION_TXT_RES_ARRAY = new int[]{C0532R.string.sport_widget_title_run, C0532R.string.sport_widget_title_walk, C0532R.string.sport_widget_title_outdoor_riding, C0532R.string.sport_widget_title_indoor_swim, C0532R.string.sport_widget_title_open_water_swim, C0532R.string.sport_widget_title_indoor_run, C0532R.string.sport_widget_title_indoor_riding, C0532R.string.sport_widget_title_elliptical, C0532R.string.sport_widget_title_mountaineer, C0532R.string.sport_widget_title_cross, C0532R.string.sport_widget_title_triathlon, C0532R.string.sport_widget_title_skiing, C0532R.string.sport_widget_title_tennis, C0532R.string.sport_widget_title_soccer, C0532R.string.sport_widget_title_compound};
    public static final int[] SPORT_TYPE_ARRAYS = new int[]{1, 6, 9, 14, 15, 8, 10, 12, 13, 7, 2001, 11, 17, 18, 2002};
    private boolean isClickPendding;
    private XViewWrapper mContext;
    private ISportItemClickListener mListener;
    private boolean mLockForLaunch;
    List<SportSortInfo> mSportSortInfos;
    private Map<Integer, MainSettingsAction> mSportTitleMap;
    private int mTitle;

    public interface ISportItemClickListener {
        void onSportClicked(int i);
    }

    class C07231 implements Runnable {
        C07231() {
        }

        public void run() {
            SportLauncherItemAdapter.this.isClickPendding = false;
        }
    }

    private class SportSortInfo implements Comparable<SportSortInfo> {
        int count;
        int resId;
        int sportId;
        int sportType;
        int titleResId;

        public SportSortInfo(int sportType, int sportId, int resId, int titleResId) {
            this.sportType = sportType;
            this.sportId = sportId;
            this.resId = resId;
            this.titleResId = titleResId;
        }

        public String toString() {
            return "SportSortInfo{sportType=" + this.sportType + ", sportId=" + this.sportId + ", resId=" + this.resId + ", titleResId=" + this.titleResId + ", count=" + this.count + '}';
        }

        public int compareTo(@NonNull SportSortInfo obj) {
            return obj.count - this.count;
        }
    }

    public SportLauncherItemAdapter(XViewWrapper context) {
        this(context, C0532R.string.gpx_select_empty);
    }

    public SportLauncherItemAdapter(XViewWrapper context, int titleResId) {
        this.mListener = null;
        this.mSportTitleMap = new HashMap();
        this.mTitle = -1;
        this.mSportSortInfos = new LinkedList();
        this.mLockForLaunch = false;
        this.isClickPendding = false;
        this.mContext = context;
        this.mTitle = titleResId;
        init();
    }

    private void init() {
        int i = 0;
        while (i < SPORT_TYPE_ARRAYS.length) {
            if (!(ModelUtil.isModelHuanghe(this.mContext.getContext()) && (SETTINGS_ACTION_IDS[i] == C0532R.id.start_indoor_swim || SETTINGS_ACTION_IDS[i] == C0532R.id.start_open_water_swim || SETTINGS_ACTION_IDS[i] == C0532R.id.start_triathlon || SETTINGS_ACTION_IDS[i] == C0532R.id.start_compound))) {
                this.mSportSortInfos.add(new SportSortInfo(SPORT_TYPE_ARRAYS[i], SETTINGS_ACTION_IDS[i], SETTING_ACTION_ICON_RES_ARRAY[i], SETTING_ACTION_TXT_RES_ARRAY[i]));
            }
            i++;
        }
    }

    public int getItemsCount() {
        return this.mSportSortInfos.size() + 1;
    }

    public DrawableItem getItem(int index, DrawableItem drawableItem, BaseDrawableGroup parent) {
        if (!(parent instanceof WheelViewItemsLayout)) {
            return null;
        }
        DrawableItem item;
        WheelViewItemsLayout itemLayout = (WheelViewItemsLayout) parent;
        RectF itemRegion = new RectF(0.0f, 0.0f, itemLayout.getWidth(), itemLayout.getItemHeight());
        if (index == 0) {
            DrawableItem title = new SportLauncherTitle(this.mContext, -1, this.mTitle, itemRegion, Align.CENTER);
            title.setTitleColor(-1);
            title.setTitleSize(this.mContext.getContext().getResources().getDimensionPixelSize(C0532R.dimen.widget_header_txt_dimen));
            item = title;
        } else {
            item = new MainSettingsAction(this.mContext, itemRegion, ((SportSortInfo) this.mSportSortInfos.get(index - 1)).titleResId, ((SportSortInfo) this.mSportSortInfos.get(index - 1)).resId, ((SportSortInfo) this.mSportSortInfos.get(index - 1)).sportId);
            item.setRelativeX((itemLayout.getWidth() / 2.0f) - (item.getWidth() / 2.0f));
            this.mSportTitleMap.put(Integer.valueOf(((SportSortInfo) this.mSportSortInfos.get(index - 1)).sportType), (MainSettingsAction) item);
        }
        item.setTag(Integer.valueOf(index));
        item.setOnClickListener(this);
        return item;
    }

    public String getCorrespondingSportStr(int sportType) {
        if (this.mSportSortInfos == null || this.mSportSortInfos.isEmpty()) {
            return "";
        }
        if (sportType == 1) {
            return this.mContext.getResources().getString(C0532R.string.sport_child_running);
        }
        if (sportType == 6) {
            return this.mContext.getResources().getString(C0532R.string.title_sport_control_walking);
        }
        for (SportSortInfo sortInfo : this.mSportSortInfos) {
            if (sortInfo.sportType == sportType) {
                return this.mContext.getResources().getString(sortInfo.titleResId);
            }
        }
        return "";
    }

    public void refreshViewStatus(int type, boolean isStarted) {
        MainSettingsAction item = (MainSettingsAction) this.mSportTitleMap.get(Integer.valueOf(type));
        if (item == null) {
            Debug.m5i("HmSportLauncherItemAdapter", "RefreshViewStatus failed, sport type is not exist: " + type);
            return;
        }
        switch (type) {
            case 1:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.running_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_run));
                return;
            case 6:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.walking_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_walk));
                return;
            case 7:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.crossing_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_cross));
                return;
            case 8:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.indoor_run_progressing) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_indoor_run));
                return;
            case 9:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.outdoor_riding_progressing) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_outdoor_riding));
                return;
            case 10:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.indoor_riding_progressing) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_indoor_riding));
                return;
            case 11:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_in_skiing) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_skiing));
                return;
            case 12:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.elliptical_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_elliptical));
                return;
            case 13:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_mountaineering) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_mountaineer));
                return;
            case 14:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_indoor_swim_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_indoor_swim));
                return;
            case 15:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_open_water_swim_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_open_water_swim));
                return;
            case 17:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_tennising) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_tennis));
                return;
            case 18:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_soccering) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_soccer));
                return;
            case 2001:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_triathlon_in_progress) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_triathlon));
                return;
            case 2002:
                item.setText(isStarted ? this.mContext.getContext().getString(C0532R.string.sport_widget_title_compounding) : this.mContext.getContext().getString(C0532R.string.sport_widget_title_compound));
                return;
            default:
                Debug.m5i("HmSportLauncherItemAdapter", "Invalid sport type");
                return;
        }
    }

    public void onClick(DrawableItem item) {
        if (item instanceof MainSettingsAction) {
            MainSettingsAction action = (MainSettingsAction) item;
            if (!this.isClickPendding && action.isClickable()) {
                this.isClickPendding = true;
                handleOnItemClick(action);
                action.getIconDrawable().startHaloClickAnimation(new C07231());
            }
        }
    }

    private void handleOnItemClick(MainSettingsAction action) {
        if (action != null && this.mListener != null) {
            switch (action.getId()) {
                case C0532R.id.start_run:
                    this.mListener.onSportClicked(1);
                    return;
                case C0532R.id.start_walk:
                    this.mListener.onSportClicked(6);
                    return;
                case C0532R.id.start_cross:
                    this.mListener.onSportClicked(7);
                    return;
                case C0532R.id.start_indoor_run:
                    this.mListener.onSportClicked(8);
                    return;
                case C0532R.id.start_outdoor_riding:
                    this.mListener.onSportClicked(9);
                    return;
                case C0532R.id.start_indoor_riding:
                    this.mListener.onSportClicked(10);
                    return;
                case C0532R.id.start_skiing:
                    this.mListener.onSportClicked(11);
                    return;
                case C0532R.id.start_ellipticals:
                    this.mListener.onSportClicked(12);
                    return;
                case C0532R.id.start_mountaineer:
                    this.mListener.onSportClicked(13);
                    return;
                case C0532R.id.start_indoor_swim:
                    this.mListener.onSportClicked(14);
                    return;
                case C0532R.id.start_open_water_swim:
                    this.mListener.onSportClicked(15);
                    return;
                case C0532R.id.start_triathlon:
                    this.mListener.onSportClicked(2001);
                    return;
                case C0532R.id.start_tennis:
                    this.mListener.onSportClicked(17);
                    return;
                case C0532R.id.start_soccer:
                    this.mListener.onSportClicked(18);
                    return;
                case C0532R.id.start_compound:
                    this.mListener.onSportClicked(2002);
                    return;
                default:
                    return;
            }
        }
    }

    public int refreshSportSortInfo(Map<Integer, SortInfo> sortInfos) {
        if (sortInfos == null) {
            return -1;
        }
        int recentSportType = -1;
        long recentSportTime = -1;
        for (SportSortInfo sortInfo : this.mSportSortInfos) {
            SortInfo sortMap = (SortInfo) sortInfos.get(Integer.valueOf(sortInfo.sportType));
            if (sortMap != null) {
                sortInfo.count = sortMap.getCount();
                if (sortMap.getTime() > recentSportTime) {
                    recentSportType = sortMap.getType();
                    recentSportTime = sortMap.getTime();
                }
            }
        }
        Debug.m5i("HmSportLauncherItemAdapter", "initSportSortInfo, sportType:" + recentSportType);
        int index = -1;
        for (int i = 0; i < this.mSportSortInfos.size(); i++) {
            if (((SportSortInfo) this.mSportSortInfos.get(i)).sportType == recentSportType) {
                index = i;
                break;
            }
        }
        if (index != -1) {
        } else {
        }
        for (SportSortInfo info : this.mSportSortInfos) {
            Debug.m5i("HmSportLauncherItemAdapter", "initSportSortInfo, info:" + info.toString());
        }
        return index;
    }

    public void setSportItemClickListener(ISportItemClickListener listener) {
        this.mListener = listener;
    }
}
