package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.customtrain.utils.TrainSpUtils;
import com.huami.watch.newsport.train.TrainDataUtils;
import com.huami.watch.newsport.train.model.TrainUnit;
import com.huami.watch.newsport.ui.SelectIntermittentTrainActivity;
import com.huami.watch.newsport.ui.SportActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.xmlparser.utils.FileUtils;
import java.util.ArrayList;
import java.util.List;

public class SelectIntermittentTrainAdapter extends BaseConfigMenuAdapter {
    private static final String TAG = SelectIntermittentTrainAdapter.class.getSimpleName();
    private String[] files;

    public SelectIntermittentTrainAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    private void initGetData() {
        this.files = FileUtils.getFileIntervalTrain(this.mMenuActivity);
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        List<MenuInfo> list = new ArrayList();
        initGetData();
        if (!(this.files == null || this.files.length == 0)) {
            int index;
            String[] arr$ = this.files;
            int len$ = arr$.length;
            int i$ = 0;
            int index2 = 0;
            while (i$ < len$) {
                String currnet = arr$[i$];
                MenuInfo menuInfo = new MenuInfo();
                menuInfo.title = currnet.replace(".json", "");
                menuInfo.iconResId = C0532R.drawable.sport_set_icon_interval_run;
                index = index2 + 1;
                menuInfo.tag = Integer.valueOf(index2);
                list.add(menuInfo);
                i$++;
                index2 = index;
            }
            index = index2;
        }
        return list;
    }

    public void omMenuItemClick(final int i) {
        Log.i(TAG, " onMenuItemClick index:" + i);
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                TrainSpUtils.setCustomIntervttentTrain(SelectIntermittentTrainAdapter.this.mMenuActivity, SelectIntermittentTrainAdapter.this.files[i]);
                final int firstTrainUnit = ((TrainUnit) TrainDataUtils.getTrainProgramByResult(SelectIntermittentTrainAdapter.this.mMenuActivity).getTrainUnitList().get(0)).getUnitType().getType();
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        Intent intent = new Intent(SelectIntermittentTrainAdapter.this.mMenuActivity, SportActivity.class);
                        intent.putExtra("sport_type", SelectIntermittentTrainAdapter.this.mSportType);
                        intent.putExtra("sport_gps_status", 2);
                        intent.putExtra("entrance_type", -1);
                        intent.putExtra("intermittent_train_type", firstTrainUnit);
                        SelectIntermittentTrainAdapter.this.mMenuActivity.startActivity(intent);
                        ((SelectIntermittentTrainActivity) SelectIntermittentTrainAdapter.this.mMenuActivity).finish();
                    }
                });
            }
        });
    }

    public void onDataChanged() {
        super.onDataChanged();
    }
}
