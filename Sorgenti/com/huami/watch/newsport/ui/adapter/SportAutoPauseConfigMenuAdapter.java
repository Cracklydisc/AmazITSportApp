package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import java.util.ArrayList;
import java.util.List;

public class SportAutoPauseConfigMenuAdapter extends BaseConfigMenuAdapter {
    public SportAutoPauseConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        List<MenuInfo> menuInfos = new ArrayList();
        MenuInfo info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_auto_pause;
        String string = this.mConfig.isAutoPause() ? this.mSportType == 9 ? context.getString(C0532R.string.settings_sport_auto_speed_title) : context.getString(C0532R.string.settings_sport_auto_pace_title) : context.getString(C0532R.string.settings_sport_auto_pause);
        info.title = string;
        if (!this.mConfig.isAutoPause()) {
            info.content = context.getString(C0532R.string.setting_off);
        } else if (Float.compare(this.mConfig.getAutoPausePace(), 0.0f) == 0) {
            info.content = context.getString(C0532R.string.settings_content_stand_still);
        } else if (this.mSportType == 9) {
            if (Math.round(this.mConfig.getAutoPausePace() * 3.6f) <= 0) {
                info.content = context.getString(C0532R.string.settings_content_stand_still);
            } else {
                info.content = context.getString(C0532R.string.setting_auto_pause_speed, new Object[]{String.valueOf(Math.round(this.mConfig.getAutoPausePace() * 3.6f))});
            }
        } else {
            int secondPerKm = Math.round(this.mConfig.getAutoPausePace() * 1000.0f);
            int minute = secondPerKm / 60;
            int second = secondPerKm % 60;
            new StringBuilder().append(String.format("%02d", new Object[]{Integer.valueOf(minute)})).append("'").append(String.format("%02d", new Object[]{Integer.valueOf(second)})).append("\"");
            info.content = context.getString(C0532R.string.setting_auto_pause_pace, new Object[]{builder.toString()});
        }
        info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        menuInfos.add(info);
        return menuInfos;
    }

    public void omMenuItemClick(int i) {
        switch (((Integer) ((MenuInfo) this.mMenuList.get(i)).tag).intValue()) {
            case 0:
                Intent targetHeartRateIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetHeartRateIntent.putExtra("selector_type", "select_target_auto_pause_pace");
                targetHeartRateIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetHeartRateIntent, 291);
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < getCount(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 0:
                    String string = this.mConfig.isAutoPause() ? this.mSportType == 9 ? this.mMenuActivity.getString(C0532R.string.settings_sport_auto_speed_title) : this.mMenuActivity.getString(C0532R.string.settings_sport_auto_pace_title) : this.mMenuActivity.getString(C0532R.string.settings_sport_auto_pause);
                    info.title = string;
                    if (this.mConfig.isAutoPause()) {
                        if (Float.compare(this.mConfig.getAutoPausePace(), 0.0f) != 0) {
                            if (this.mSportType != 9) {
                                int secondPerKm = Math.round(this.mConfig.getAutoPausePace() * 1000.0f);
                                int minute = secondPerKm / 60;
                                int second = secondPerKm % 60;
                                new StringBuilder().append(String.format("%02d", new Object[]{Integer.valueOf(minute)})).append("'").append(String.format("%02d", new Object[]{Integer.valueOf(second)})).append("\"");
                                info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_pace, new Object[]{builder.toString()});
                                break;
                            }
                            if (Math.round(this.mConfig.getAutoPausePace() * 3.6f) > 0) {
                                info.content = this.mMenuActivity.getString(C0532R.string.setting_auto_pause_speed, new Object[]{String.valueOf(Math.round(this.mConfig.getAutoPausePace() * 3.6f))});
                                break;
                            } else {
                                info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                                break;
                            }
                        }
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_content_stand_still);
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                    break;
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
