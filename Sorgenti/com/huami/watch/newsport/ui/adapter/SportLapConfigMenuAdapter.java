package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class SportLapConfigMenuAdapter extends BaseConfigMenuAdapter {
    public SportLapConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        List<MenuInfo> menuInfos = new ArrayList();
        MenuInfo info = new MenuInfo();
        info.title = context.getString(C0532R.string.settings_sport_lap_auto);
        if (this.mConfig.isOpenAutoLapRecord()) {
            float distance = this.mConfig.getDistanceAutoLap() / 1000.0f;
            if (UnitConvertUtils.isImperial()) {
                if (SportType.isMeterStandard(this.mConfig.getSportType())) {
                    info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_imperial_settings_item_value_target_distance), new Object[]{String.valueOf((int) this.mConfig.getDistanceAutoLap())});
                } else {
                    info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
                }
            } else if (SportType.isMeterStandard(this.mConfig.getSportType())) {
                info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_metric_settings_item_value_target_distance), new Object[]{String.valueOf((int) this.mConfig.getDistanceAutoLap())});
            } else {
                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
            }
        } else {
            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
        }
        info.iconResId = C0532R.drawable.sport_set_icon_subsection;
        info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        menuInfos.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_subsection_remind;
        info.title = context.getString(C0532R.string.settings_sport_lap_auto_remind);
        info.content = this.mConfig.isRemindAutoLap() ? context.getString(C0532R.string.setting_on) : context.getString(C0532R.string.setting_off);
        info.tag = Integer.valueOf(1);
        menuInfos.add(info);
        return menuInfos;
    }

    public void omMenuItemClick(int i) {
        MenuInfo info = (MenuInfo) this.mMenuList.get(i);
        switch (((Integer) info.tag).intValue()) {
            case 0:
                Intent targetHeartRateIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetHeartRateIntent.putExtra("selector_type", "select_target_auto_lap_dis");
                targetHeartRateIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetHeartRateIntent, 291);
                break;
            case 1:
                this.mConfig.setIsRemindAutoLap(!this.mConfig.isRemindAutoLap());
                if (info != null) {
                    info.content = this.mMenuActivity.getString(this.mConfig.isRemindAutoLap() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    notifyDataChanged();
                    break;
                }
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < getCount(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 0:
                    if (!this.mConfig.isOpenAutoLapRecord()) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                        break;
                    }
                    float distance = this.mConfig.getDistanceAutoLap() / 1000.0f;
                    if (!UnitConvertUtils.isImperial()) {
                        if (!SportType.isMeterStandard(this.mConfig.getSportType())) {
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
                            break;
                        }
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_metric_settings_item_value_target_distance), new Object[]{String.valueOf((int) this.mConfig.getDistanceAutoLap())});
                        break;
                    } else if (!SportType.isMeterStandard(this.mConfig.getSportType())) {
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{Float.valueOf(distance)});
                        break;
                    } else {
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_imperial_settings_item_value_target_distance), new Object[]{String.valueOf((int) this.mConfig.getDistanceAutoLap())});
                        break;
                    }
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
