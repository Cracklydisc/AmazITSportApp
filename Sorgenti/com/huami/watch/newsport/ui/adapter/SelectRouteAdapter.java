package com.huami.watch.newsport.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.hs.gpxparser.appmodel.GPXAppRoute;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.hs.gpxparser.utils.LogUtils;
import com.hs.gpxparser.utils.SDCardGPSUtils;
import com.huami.watch.common.HmMenuActivity.HmMeunAdaper;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.gpxroute.GPXRouteDetailActivity;
import com.huami.watch.newsport.ui.gpxroute.SelectGPXRouteActivity;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import java.util.ArrayList;
import java.util.List;

public class SelectRouteAdapter extends HmMeunAdaper {
    private static final String TAG = SelectRouteAdapter.class.getName();
    private String currentSelectFileName = "";
    private String currentSelectdName = "";
    private volatile int currentSelectedIndex = -1;
    private volatile List<GPXAppRoute> gpxRouteList = new ArrayList();
    private IConfigListener iConfigListener;
    private Context mContext;
    Handler myHandler = new C07211();
    private List<MenuInfo> routeInfos = new ArrayList();
    private int sportType = -1;

    class C07211 extends Handler {
        C07211() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 100:
                    SelectRouteAdapter.this.initRouteInfo(SelectRouteAdapter.this.gpxRouteList);
                    return;
                case 200:
                    LogUtils.print(SelectRouteAdapter.TAG, "handleMessage handle icon ");
                    return;
                case 300:
                    SelectRouteAdapter.this.showNoGPXFileDialog();
                    return;
                default:
                    return;
            }
        }
    }

    class C07222 extends Thread {
        C07222() {
        }

        public void run() {
            SelectRouteAdapter.this.gpxRouteList = SDCardGPSUtils.getGPXAppRouteFromSDCard();
            SelectRouteAdapter.this.initGPXIconBitmap(SelectRouteAdapter.this.gpxRouteList);
            SelectRouteAdapter.this.routeHeadInfo();
            if (SelectRouteAdapter.this.gpxRouteList == null || SelectRouteAdapter.this.gpxRouteList.size() <= 0) {
                Log.i(SelectRouteAdapter.TAG, " route file is Empty ");
                Message msg = SelectRouteAdapter.this.myHandler.obtainMessage();
                msg.what = 300;
                SelectRouteAdapter.this.myHandler.sendMessage(msg);
                return;
            }
            Log.i(SelectRouteAdapter.TAG, "---size---" + SelectRouteAdapter.this.gpxRouteList.size());
            msg = SelectRouteAdapter.this.myHandler.obtainMessage();
            msg.what = 100;
            SelectRouteAdapter.this.myHandler.sendMessage(msg);
        }
    }

    public interface IConfigListener {
    }

    public SelectRouteAdapter(Context mContext, IConfigListener iConfigListener, int type) {
        this.mContext = mContext;
        this.iConfigListener = iConfigListener;
        this.sportType = type;
        getRouteInfos(mContext);
    }

    private void showNoGPXFileDialog() {
        ((SelectGPXRouteActivity) this.mContext).setShowDialogResult();
    }

    private void routeHeadInfo() {
        MenuInfo info = new MenuInfo();
        info.title = this.mContext.getString(C0532R.string.gpx_select_closed);
        info.content = "";
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_trajectory;
        info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.route_select_close_txt_size);
        info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        this.routeInfos.add(info);
    }

    private void initRouteInfo(List<GPXAppRoute> gpxRoutesList) {
        if (gpxRoutesList != null) {
            for (GPXAppRoute gpxRoute : gpxRoutesList) {
                MenuInfo info = new MenuInfo();
                info.title = gpxRoute.getGpxFileName();
                if (this.mContext != null) {
                    String[] contents = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(this.mContext, this.sportType);
                    if (contents.length == 2) {
                        this.currentSelectdName = contents[0];
                        this.currentSelectFileName = contents[1];
                    }
                }
                LogUtils.print(TAG, "initRouteInfo iconBitmap is null ?:" + (gpxRoute.getRouteImgBitmap() == null));
                info.iconBitmap = gpxRoute.getRouteImgBitmap();
                info.iconResId = C0532R.drawable.lan_quicksettings_icon_trajectory;
                info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                info.tag = Integer.valueOf(0);
                this.routeInfos.add(info);
            }
        }
        notifyDataChanged();
    }

    private void getRouteInfos(Context mContext) {
        new C07222().start();
    }

    private void initGPXIconBitmap(List<GPXAppRoute> gpxRouteList) {
        if (gpxRouteList != null && gpxRouteList.size() != 0) {
            for (GPXAppRoute gpxroute : gpxRouteList) {
                LogUtils.print(TAG, "initGPXIconBitmap filePath:" + gpxroute.getGpxFileName());
                gpxroute.setRouteImgBitmap(SDCardGPSUtils.getGPSBitmapFromGPXFileName(gpxroute.getGpxFileName().replace(".gpx", "")));
            }
        }
    }

    public int getCount() {
        return this.routeInfos.size();
    }

    public MenuInfo getItemMenu(int position) {
        return (MenuInfo) this.routeInfos.get(position);
    }

    public void omMenuItemClick(int position) {
        LogUtils.print(TAG, "omMenuItemClick:" + position);
        this.currentSelectedIndex = position;
        jumpToRouteDetailPage(this.currentSelectedIndex);
    }

    private void jumpToRouteDetailPage(int currentSelectedIndex) {
        if (currentSelectedIndex != 0) {
            Intent routeDetailIntent = new Intent(this.mContext, GPXRouteDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("currnet_file_path", ((GPXAppRoute) this.gpxRouteList.get(currentSelectedIndex - 1)).getGpxFileName());
            bundle.putInt("sport_type", this.sportType);
            routeDetailIntent.putExtras(bundle);
            ((SelectGPXRouteActivity) this.mContext).startActivityForResult(routeDetailIntent, GPXRouteDetailActivity.INTENT_RESULT);
        } else if (currentSelectedIndex == 0) {
            LogUtils.print(TAG, " gpx data to kernel　has not data close  ");
            byte[] clearGPSDData = SDCardGPSUtils.getClearGPSToKernelData();
            HmSensorHubConfigManager.getHmSensorHubConfigManager(this.mContext).syncGpxTrailData(clearGPSDData, clearGPSDData.length);
            GPSSPUtils.setCurrentGPXRouteBySportType(this.mContext, null, null, this.sportType);
            ((SelectGPXRouteActivity) this.mContext).setResult(-1);
            finishThisActivity();
        }
    }

    private void finishThisActivity() {
        if (this.gpxRouteList != null && this.gpxRouteList.size() > 0) {
            for (GPXAppRoute item : this.gpxRouteList) {
                Bitmap bitmap = item.getRouteImgBitmap();
                if (!(bitmap == null || bitmap.isRecycled())) {
                    bitmap.recycle();
                }
            }
        }
        ((SelectGPXRouteActivity) this.mContext).finish();
    }
}
