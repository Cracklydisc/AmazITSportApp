package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import com.huami.watch.common.HmMenuActivity.HmMeunAdaper;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseConfigMenuAdapter extends HmMeunAdaper {
    protected boolean isSupportRecommandSport;
    protected BaseConfig mConfig;
    protected IConfigListener mConfigListener;
    protected Activity mMenuActivity;
    protected List<MenuInfo> mMenuList;
    protected int mSportType;
    protected int[] maxList;
    protected int[] minList;

    public interface IConfigListener {
    }

    public abstract List<MenuInfo> getMenuInfos(Context context);

    public BaseConfigMenuAdapter(Activity context, IConfigListener listener, int sportType, boolean isSupportRemondSport) {
        this.mConfig = null;
        this.mMenuList = new ArrayList();
        this.isSupportRecommandSport = false;
        this.minList = new int[5];
        this.maxList = new int[5];
        this.isSupportRecommandSport = isSupportRemondSport;
        this.mSportType = sportType;
        this.mMenuActivity = context;
        this.mConfigListener = listener;
        this.mConfig = DataManager.getInstance().getSportConfig(context, sportType);
        initMenuInfo(context);
    }

    public BaseConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        this.mConfig = null;
        this.mMenuList = new ArrayList();
        this.isSupportRecommandSport = false;
        this.minList = new int[5];
        this.maxList = new int[5];
        this.mSportType = sportType;
        this.mMenuActivity = context;
        this.mConfigListener = listener;
        this.mConfig = DataManager.getInstance().getSportConfig(context, sportType);
        initMenuInfo(context);
    }

    private void initMenuInfo(Context context) {
        List<MenuInfo> menuInfos = getMenuInfos(context);
        if (menuInfos != null) {
            this.mMenuList.addAll(menuInfos);
        }
    }

    public int getCount() {
        return this.mMenuList.size();
    }

    public MenuInfo getItemMenu(int i) {
        return (MenuInfo) this.mMenuList.get(i);
    }

    public void omMenuItemClick(int i) {
    }

    public void onDataChanged() {
        super.onDataChanged();
        this.mConfig = DataManager.getInstance().getSportConfig(this.mMenuActivity, this.mSportType);
    }
}
