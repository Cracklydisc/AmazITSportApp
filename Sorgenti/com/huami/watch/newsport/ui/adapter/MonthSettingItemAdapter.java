package com.huami.watch.newsport.ui.adapter;

import android.graphics.Paint.Align;
import android.graphics.RectF;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.DrawableItem.OnClickListener;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.wheel.AbstractWheelAdapter;
import clc.utils.venus.widgets.wheel.WheelViewItemsLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.view.MainSettingsAction;
import com.huami.watch.newsport.ui.view.SportLauncherTitle;

public class MonthSettingItemAdapter extends AbstractWheelAdapter implements OnClickListener {
    private static final String TAG = MonthSettingItemAdapter.class.getName();
    private boolean isClickPendding = false;
    private XViewWrapper mContext;
    private IMonthClickListener mListener = null;

    class C07181 implements Runnable {
        C07181() {
        }

        public void run() {
            MonthSettingItemAdapter.this.isClickPendding = false;
        }
    }

    public interface IMonthClickListener {
        void onMonthTargetClicked();
    }

    public MonthSettingItemAdapter(XViewWrapper context) {
        this.mContext = context;
    }

    public int getItemsCount() {
        return 2;
    }

    public DrawableItem getItem(int index, DrawableItem drawableItem, BaseDrawableGroup parent) {
        if (!(parent instanceof WheelViewItemsLayout)) {
            return null;
        }
        DrawableItem item;
        WheelViewItemsLayout itemLayout = (WheelViewItemsLayout) parent;
        RectF itemRegion = new RectF(0.0f, 0.0f, itemLayout.getWidth(), itemLayout.getItemHeight());
        if (index == 0) {
            DrawableItem title = new SportLauncherTitle(this.mContext, -1, C0532R.string.sport_month_settings, itemRegion, Align.CENTER);
            title.setTitleColor(-1);
            title.setTitleSize(this.mContext.getContext().getResources().getDimensionPixelSize(C0532R.dimen.widget_header_txt_dimen));
            item = title;
        } else {
            item = new MainSettingsAction(this.mContext, itemRegion, C0532R.string.sport_month_target, C0532R.drawable.icon_goal, index);
            item.setRelativeX((itemLayout.getWidth() / 2.0f) - (item.getWidth() / 2.0f));
        }
        item.setTag(Integer.valueOf(index));
        item.setOnClickListener(this);
        return item;
    }

    public void onClick(DrawableItem item) {
        if (item instanceof MainSettingsAction) {
            MainSettingsAction action = (MainSettingsAction) item;
            if (!this.isClickPendding && action.isClickable()) {
                this.isClickPendding = true;
                handleOnItemClick(action);
                action.getIconDrawable().startHaloClickAnimation(new C07181());
            }
        }
    }

    private void handleOnItemClick(MainSettingsAction action) {
        if (action != null && this.mListener != null) {
            this.mListener.onMonthTargetClicked();
        }
    }

    public void setMonthClickListener(IMonthClickListener listener) {
        this.mListener = listener;
    }
}
