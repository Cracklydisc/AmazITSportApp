package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.setting.heartregion.HearRegionSettingActivity;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class SportRemindConfigMenuAdapter extends BaseConfigMenuAdapter {
    private static final int[] COMPOUND_CONFIG = new int[]{2, 0, 3};
    private static final int[] CROSSING_CONFIG = new int[]{0, 2, 4};
    private static final int[] ELLIPTICAL_MACHINE_CONFIG = new int[]{2};
    private static final int[] INDOORUN_CONFIG = new int[]{0, 2, 3, 4, 1};
    private static final int[] INRIDING_CONFIG = new int[]{2};
    private static final int[] MOUNTAINEER_CONFIG = new int[]{2, 4};
    private static final int[] OPEN_WATER_CONFIG = new int[0];
    private static final int[] OUTRIDING_CONFIG = new int[]{0, 2, 4, 1};
    private static final int[] RUNNING_CONFIG = new int[]{0, 2, 3, 4, 1};
    private static final int[] SKIING_CONFIG = new int[]{2, 1};
    private static final int[] SOCCER_CONFIG = new int[]{2};
    private static final int[] SWIM_CONFIG = new int[]{4};
    private static final String TAG = SportRemindConfigMenuAdapter.class.getSimpleName();
    private static final int[] TENNIC_CONFIG = new int[]{2};
    private static final int[] TRIATHLON_CONFIG = new int[]{0, 2, 3};
    private static final int[] WALKING_CONFIG = new int[]{0, 2, 4};
    private int[] mSportConfigs;

    public SportRemindConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    private void initConfig() {
        switch (this.mSportType) {
            case 1:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
            case 6:
                this.mSportConfigs = WALKING_CONFIG;
                return;
            case 7:
                this.mSportConfigs = CROSSING_CONFIG;
                return;
            case 8:
                this.mSportConfigs = INDOORUN_CONFIG;
                return;
            case 9:
                this.mSportConfigs = OUTRIDING_CONFIG;
                return;
            case 10:
                this.mSportConfigs = INRIDING_CONFIG;
                return;
            case 11:
                this.mSportConfigs = SKIING_CONFIG;
                return;
            case 12:
                this.mSportConfigs = ELLIPTICAL_MACHINE_CONFIG;
                return;
            case 13:
                this.mSportConfigs = MOUNTAINEER_CONFIG;
                return;
            case 14:
                this.mSportConfigs = SWIM_CONFIG;
                return;
            case 15:
                this.mSportConfigs = OPEN_WATER_CONFIG;
                return;
            case 17:
                this.mSportConfigs = TENNIC_CONFIG;
                return;
            case 18:
                this.mSportConfigs = SOCCER_CONFIG;
                return;
            case 2001:
                this.mSportConfigs = TRIATHLON_CONFIG;
                return;
            case 2002:
                this.mSportConfigs = COMPOUND_CONFIG;
                return;
            default:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
        }
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        initConfig();
        List<MenuInfo> menuInfos = new ArrayList();
        MenuInfo info = new MenuInfo();
        for (int tag : this.mSportConfigs) {
            switch (tag) {
                case 0:
                    info = new MenuInfo();
                    if (SportType.isMixedSport(this.mConfig.getSportType())) {
                        info.title = context.getString(C0532R.string.settings_item_title_km_remind_triathlon);
                    } else {
                        info.title = context.getString(C0532R.string.settings_item_title_km_remind);
                    }
                    info.iconResId = C0532R.drawable.sport_set_icon_mileage_remind;
                    if (this.mConfig.getSportType() != 9 && this.mConfig.getSportType() != 7) {
                        info.content = context.getString(this.mConfig.isRemindPerKM() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    } else if (this.mConfig.isRemindPerKM()) {
                        int contentResId;
                        if (UnitConvertUtils.isImperial()) {
                            contentResId = C0532R.string.per_correspond_mi;
                        } else {
                            contentResId = C0532R.string.per_correspond_km;
                        }
                        info.content = context.getString(contentResId, new Object[]{Integer.valueOf(this.mConfig.getRemindKm())});
                    } else {
                        info.content = context.getString(C0532R.string.setting_off);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 1:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_pace_remind;
                    if (this.mSportType == 11) {
                        info.title = context.getString(C0532R.string.speed_remind_title);
                    } else {
                        info.title = context.getString(C0532R.string.settings_item_title_pace_remind);
                    }
                    if (!this.mConfig.isRemindTargetPace()) {
                        info.content = context.getString(C0532R.string.setting_off);
                    } else if (this.mSportType == 11) {
                        text = String.valueOf(Math.round(3.6f * (Float.compare(this.mConfig.getTargetPace(), 0.0f) == 0 ? 0.0f : 1.0f / this.mConfig.getTargetPace()))) + (UnitConvertUtils.isImperial() ? " mi/h" : " km/h");
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.skiing_above_skiing_spped), new Object[]{text});
                    } else {
                        int secondPerKm = Math.round(this.mConfig.getTargetPace() * 1000.0f);
                        text = (secondPerKm / 60) + "'" + (secondPerKm % 60) + "\"";
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_pace), new Object[]{text});
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 2:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_safehr_remind;
                    info.title = context.getString(C0532R.string.settings_item_title_safe_heartrate_remind);
                    if (this.mConfig.isRemindSafeHeartRate()) {
                        int heartRate = (int) this.mConfig.getSafeHeartRateHigh();
                        if (heartRate < 0) {
                            heartRate = UserInfoManager.getDefaultSafeHeart(this.mMenuActivity);
                        } else if (heartRate < 97 || heartRate > 202) {
                            heartRate = 180;
                        }
                        this.mConfig.setSafeHeartRateHigh((float) heartRate);
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.safe_hearate_remind_content), new Object[]{Integer.valueOf(heartRate)});
                    } else {
                        info.content = context.getString(C0532R.string.setting_off);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 3:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_hr_remind;
                    if (SportType.isMixedSport(this.mConfig.getSportType())) {
                        info.title = context.getString(C0532R.string.setting_sport_heart_region_remind_triathlon);
                    } else {
                        info.title = context.getString(C0532R.string.setting_sport_heart_region_remind);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    if (!this.mConfig.ismHeartRegionRemind()) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                        break;
                    }
                    info.content = getCorresRegion();
                    break;
                case 4:
                    String string;
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_subsection_remind;
                    info.title = context.getString(C0532R.string.settings_sport_lap_auto_remind);
                    if (this.mConfig.isRemindAutoLap()) {
                        string = context.getString(C0532R.string.setting_on);
                    } else {
                        string = context.getString(C0532R.string.setting_off);
                    }
                    info.content = string;
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                default:
                    throw new IllegalArgumentException("err sport tag:" + tag);
            }
            info.tag = Integer.valueOf(tag);
            menuInfos.add(info);
        }
        return menuInfos;
    }

    private String getCorresRegion() {
        switch (this.mConfig.getmHeartRegionType()) {
            case 0:
                return this.mMenuActivity.getString(C0532R.string.heart_region_warm_up_and_relax);
            case 1:
                return this.mMenuActivity.getString(C0532R.string.heart_region_fat_burning);
            case 2:
                return this.mMenuActivity.getString(C0532R.string.heart_region_heart_and_lung_enhancement);
            case 3:
                return this.mMenuActivity.getString(C0532R.string.heart_region_endurance_enhancement);
            case 4:
                return this.mMenuActivity.getString(C0532R.string.heart_region_anaerobic＿limit);
            case 5:
                return this.mMenuActivity.getString(C0532R.string.setting_sport_heart_region_text_custom);
            default:
                return null;
        }
    }

    public void omMenuItemClick(int i) {
        int valueResId = C0532R.string.setting_on;
        boolean z = true;
        MenuInfo info = (MenuInfo) this.mMenuList.get(i);
        switch (((Integer) info.tag).intValue()) {
            case 0:
                if (this.mConfig.getSportType() != 9 && this.mConfig.getSportType() != 7) {
                    boolean z2;
                    BaseConfig baseConfig = this.mConfig;
                    if (this.mConfig.isRemindPerKM()) {
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                    baseConfig.setIsRemindPerKM(z2);
                    if (info != null) {
                        if (!this.mConfig.isRemindPerKM()) {
                            valueResId = C0532R.string.setting_off;
                        }
                        info.content = this.mMenuActivity.getString(valueResId);
                        notifyDataChanged();
                        break;
                    }
                }
                Intent remindkmIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                remindkmIntent.putExtra("selector_type", "select_target_remind_km");
                remindkmIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(remindkmIntent, 291);
                break;
                break;
            case 1:
                Intent targetPaceIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetPaceIntent.putExtra("selector_type", "select_target_pace");
                targetPaceIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetPaceIntent, 291);
                break;
            case 2:
                Intent targetHeartRateIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetHeartRateIntent.putExtra("selector_type", "select_target_heart_rate");
                targetHeartRateIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetHeartRateIntent, 291);
                break;
            case 3:
                Intent heartRegionIntent = new Intent(this.mMenuActivity, HearRegionSettingActivity.class);
                heartRegionIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(heartRegionIntent, 293);
                break;
            case 4:
                BaseConfig baseConfig2 = this.mConfig;
                if (this.mConfig.isRemindAutoLap()) {
                    z = false;
                }
                baseConfig2.setIsRemindAutoLap(z);
                if (info != null) {
                    if (!this.mConfig.isRemindAutoLap()) {
                        valueResId = C0532R.string.setting_off;
                    }
                    info.iconResId = C0532R.drawable.sport_set_icon_subsection_remind;
                    info.content = this.mMenuActivity.getString(valueResId);
                    notifyDataChanged();
                    break;
                }
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < getCount(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 0:
                    if (this.mConfig.getSportType() == 9 || this.mConfig.getSportType() == 7) {
                        if (!this.mConfig.isRemindPerKM()) {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                            break;
                        }
                        int contentResId;
                        if (UnitConvertUtils.isImperial()) {
                            contentResId = C0532R.string.per_correspond_mi;
                        } else {
                            contentResId = C0532R.string.per_correspond_km;
                        }
                        info.content = this.mMenuActivity.getString(contentResId, new Object[]{Integer.valueOf(this.mConfig.getRemindKm())});
                        break;
                    }
                    info.content = this.mMenuActivity.getString(this.mConfig.isRemindPerKM() ? C0532R.string.setting_on : C0532R.string.setting_off);
                    break;
                    break;
                case 1:
                    if (this.mConfig.isRemindTargetPace()) {
                        String text;
                        if (this.mSportType != 11) {
                            int secondPerKm = Math.round(this.mConfig.getTargetPace() * 1000.0f);
                            text = (secondPerKm / 60) + "'" + (secondPerKm % 60) + "\"";
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_pace), new Object[]{text});
                            break;
                        }
                        text = String.valueOf(Math.round(3.6f * (Float.compare(this.mConfig.getTargetPace(), 0.0f) == 0 ? 0.0f : 1.0f / this.mConfig.getTargetPace()))) + (UnitConvertUtils.isImperial() ? " mi/h" : " km/h");
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.skiing_above_skiing_spped), new Object[]{text});
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                    break;
                case 2:
                    info.title = this.mMenuActivity.getString(C0532R.string.settings_item_title_safe_heartrate_remind);
                    if (!this.mConfig.isRemindSafeHeartRate()) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                        break;
                    }
                    int heartRate = (int) this.mConfig.getSafeHeartRateHigh();
                    if (heartRate < 0) {
                        heartRate = UserInfoManager.getDefaultSafeHeart(this.mMenuActivity);
                    } else if (heartRate < 97 || heartRate > 202) {
                        heartRate = 180;
                        this.mConfig.setSafeHeartRateHigh((float) 180);
                    }
                    info.content = String.format(this.mMenuActivity.getString(C0532R.string.safe_hearate_remind_content), new Object[]{Integer.valueOf(heartRate)});
                    break;
                case 3:
                    LogUtils.print(TAG, " heartRegionRemind ");
                    if (!this.mConfig.ismHeartRegionRemind()) {
                        info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                        break;
                    } else {
                        info.content = getCorresRegion();
                        break;
                    }
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
