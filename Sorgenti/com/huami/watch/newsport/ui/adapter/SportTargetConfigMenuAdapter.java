package com.huami.watch.newsport.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class SportTargetConfigMenuAdapter extends BaseConfigMenuAdapter {
    private static final int[] ELLIPTICAL_MACHINE_CONFIG = new int[]{0, 2};
    private static final int[] INDOORUN_CONFIG = new int[]{1, 0, 2};
    private static final int[] INRIDING_CONFIG = new int[]{0, 2};
    private static final int[] OPEN_WATER_CONFIG = new int[]{1, 0, 2};
    private static final int[] OUTRIDING_CONFIG = new int[]{1, 0, 2};
    private static final int[] RUNNING_CONFIG = new int[]{1, 0, 2};
    private static final int[] SWIM_CONFIG = new int[]{3, 0, 2};
    private static final int[] WALKING_CONFIG = new int[]{1, 0, 2};
    private int[] mSportConfigs;

    private void initConfig() {
        switch (this.mSportType) {
            case 1:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
            case 6:
                this.mSportConfigs = WALKING_CONFIG;
                return;
            case 7:
                return;
            case 8:
                this.mSportConfigs = INDOORUN_CONFIG;
                return;
            case 9:
                this.mSportConfigs = OUTRIDING_CONFIG;
                return;
            case 10:
                this.mSportConfigs = INRIDING_CONFIG;
                return;
            case 12:
                this.mSportConfigs = ELLIPTICAL_MACHINE_CONFIG;
                return;
            case 14:
                this.mSportConfigs = SWIM_CONFIG;
                return;
            case 15:
                this.mSportConfigs = OPEN_WATER_CONFIG;
                return;
            default:
                this.mSportConfigs = RUNNING_CONFIG;
                return;
        }
    }

    public SportTargetConfigMenuAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        initConfig();
        List<MenuInfo> menuInfos = new ArrayList();
        MenuInfo info = new MenuInfo();
        for (int tag : this.mSportConfigs) {
            switch (tag) {
                case 0:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_list_target_time;
                    info.title = context.getString(C0532R.string.settings_item_title_target_use_time_remind);
                    if (this.mConfig.isRemindTargetTimeCost()) {
                        long timespan = this.mConfig.getTargetTimeCost();
                        int minute = ((int) (timespan / 60000)) % 60;
                        int hour = (int) (timespan / 3600000);
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_target_use_time), new Object[]{Integer.valueOf(hour), Integer.valueOf(minute)});
                    } else {
                        info.content = context.getString(C0532R.string.setting_off);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 1:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_target_mileage;
                    info.title = context.getString(C0532R.string.settings_item_title_target_distance_remind);
                    if (!this.mConfig.isRemindTargetDistance()) {
                        info.content = context.getString(C0532R.string.setting_off);
                    } else if (SportType.isSwimMode(this.mConfig.getSportType())) {
                        distance = (float) this.mConfig.getTargetDistance();
                        if (this.mConfig.getUnit() == 0) {
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_metric_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                        } else {
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd), new Object[]{String.valueOf(Math.round(distance))});
                        }
                    } else {
                        distance = ((float) this.mConfig.getTargetDistance()) / 1000.0f;
                        if (UnitConvertUtils.isImperial()) {
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                        } else {
                            info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                        }
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 2:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_target_calorie;
                    info.title = context.getString(C0532R.string.settings_item_title_target_calorie_remind);
                    if (this.mConfig.isRemindCalorie()) {
                        int calorie = this.mConfig.getTargetCalorie();
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_target_calorie), new Object[]{Integer.valueOf(calorie)});
                    } else {
                        info.content = context.getString(C0532R.string.setting_off);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 3:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_lap_remind;
                    info.title = context.getString(C0532R.string.target_swim_trips);
                    if (this.mConfig.isRemindTargetTrips()) {
                        info.content = String.valueOf(this.mConfig.getTargetTrips());
                    } else {
                        info.content = context.getString(C0532R.string.setting_off);
                    }
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                case 4:
                    info = new MenuInfo();
                    info.iconResId = C0532R.drawable.sport_set_icon_target_te;
                    info.title = context.getString(C0532R.string.settings_sport_target_TE);
                    float targetTE = this.mConfig.getTargetTE();
                    String text = String.format("%.1f", new Object[]{Float.valueOf(targetTE)});
                    if (!this.mConfig.isRemindTE()) {
                        text = context.getString(C0532R.string.setting_off);
                    }
                    info.content = text;
                    info.titleSize = context.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                    info.valueSize = context.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                    break;
                default:
                    throw new IllegalArgumentException("err sport tag:" + tag);
            }
            info.tag = Integer.valueOf(tag);
            menuInfos.add(info);
        }
        return menuInfos;
    }

    public void omMenuItemClick(int i) {
        switch (((Integer) ((MenuInfo) this.mMenuList.get(i)).tag).intValue()) {
            case 0:
                PointUtils.recordEventProperty("0010", this.mSportType);
                Intent targetTimeIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetTimeIntent.putExtra("selector_type", "select_target_time");
                targetTimeIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetTimeIntent, 291);
                break;
            case 1:
                PointUtils.recordEventProperty("0009", this.mSportType);
                Intent targetDistanceIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetDistanceIntent.putExtra("selector_type", "select_target_distance");
                targetDistanceIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetDistanceIntent, 291);
                break;
            case 2:
                PointUtils.recordEventProperty("0011", this.mSportType);
                Intent targetCalorieIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetCalorieIntent.putExtra("selector_type", "select_target_calorie");
                targetCalorieIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetCalorieIntent, 291);
                break;
            case 3:
                PointUtils.recordEventProperty("0039", this.mSportType);
                Intent targetTripsIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetTripsIntent.putExtra("selector_type", "select_target_trips");
                targetTripsIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetTripsIntent, 291);
                break;
            case 4:
                PointUtils.recordEventProperty("0012", this.mSportType);
                Intent targetTEIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                targetTEIntent.putExtra("selector_type", "select_target_te");
                targetTEIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(targetTEIntent, 291);
                break;
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < getCount(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            if (info != null) {
                switch (((Integer) info.tag).intValue()) {
                    case 0:
                        if (!this.mConfig.isRemindTargetTimeCost()) {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                            break;
                        }
                        long timespan = this.mConfig.getTargetTimeCost();
                        int minute = ((int) (timespan / 60000)) % 60;
                        int hour = (int) (timespan / 3600000);
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_target_use_time), new Object[]{Integer.valueOf(hour), Integer.valueOf(minute)});
                        this.mConfig.setIsRemindTE(false);
                        break;
                    case 1:
                        if (!this.mConfig.isRemindTargetDistance()) {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                            break;
                        }
                        float distance;
                        if (SportType.isSwimMode(this.mConfig.getSportType())) {
                            distance = (float) this.mConfig.getTargetDistance();
                            if (this.mConfig.getUnit() == 0) {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.meter_metric_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                            } else {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd), new Object[]{String.valueOf(Math.round(distance))});
                            }
                        } else {
                            distance = ((float) this.mConfig.getTargetDistance()) / 1000.0f;
                            if (UnitConvertUtils.isImperial()) {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_imperial_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                            } else {
                                info.content = String.format(this.mMenuActivity.getString(C0532R.string.km_metric_settings_item_value_target_distance), new Object[]{String.valueOf(Math.round(distance))});
                            }
                        }
                        this.mConfig.setIsRemindTE(false);
                        break;
                    case 2:
                        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_title_target_calorie_remind);
                        if (!this.mConfig.isRemindCalorie()) {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                            break;
                        }
                        int calorie = this.mConfig.getTargetCalorie();
                        info.content = String.format(this.mMenuActivity.getString(C0532R.string.settings_item_value_target_calorie), new Object[]{Integer.valueOf(calorie)});
                        this.mConfig.setIsRemindTE(false);
                        break;
                    case 3:
                        if (this.mConfig.isRemindTargetTrips()) {
                            info.content = String.valueOf(this.mConfig.getTargetTrips());
                        } else {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                        }
                        this.mConfig.setIsRemindTE(false);
                        break;
                    case 4:
                        if (!this.mConfig.isRemindTE()) {
                            info.content = this.mMenuActivity.getString(C0532R.string.setting_off);
                            break;
                        }
                        float targetTE = this.mConfig.getTargetTE();
                        info.content = String.format("%.1f", new Object[]{Float.valueOf(targetTE)});
                        this.mConfig.setIsRemindTE(false);
                        break;
                    default:
                        break;
                }
            }
        }
        DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
        notifyDataChanged();
    }
}
