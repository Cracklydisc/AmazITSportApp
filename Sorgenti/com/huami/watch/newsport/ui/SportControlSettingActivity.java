package com.huami.watch.newsport.ui;

import android.content.Intent;
import android.os.Bundle;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.SportControlMenuAdapter;

public class SportControlSettingActivity extends BaseActivity {
    private BaseConfigMenuAdapter mAdapter = null;
    private int mSportType = -1;

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("widget_settings_type", -1);
        if (SportType.isSportTypeValid(this.mSportType)) {
            HmMenuView view = new HmMenuView(this, null, C0532R.string.settings_sport_control);
            this.mAdapter = new SportControlMenuAdapter(this, null, this.mSportType);
            view.setSimpleMenuAdapter(this.mAdapter);
            setContentView(view);
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.mAdapter != null) {
            this.mAdapter.onDataChanged();
        }
    }
}
