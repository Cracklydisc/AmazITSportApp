package com.huami.watch.newsport.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.ui.fragpicker.AbsSportPickerFragment.OnPickerSelectedListener;
import com.huami.watch.newsport.ui.fragpicker.MonthTargetPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.SelectCompoundPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2BackgroundPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2IndoorPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2RemindKmDistancePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2SwimUnitPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TEStagePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetAutoLapDistancePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetAutoPauseFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetCaloriePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetClapThaHandsFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetDistancePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetHeartRateFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetHeartRegionRemindFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetHeartRegionSimplePickFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetLockSourceFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetPacePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetRealGraphFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetSwimLengthFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetTEFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetTimePickerFragment;
import com.huami.watch.newsport.ui.fragpicker.v2.V2TargetTripsPickerFragment;

public class SportPickerActivity extends BaseActivity implements OnPickerSelectedListener {
    private static final String TAG = SportPickerActivity.class.getName();
    private BaseConfig mConfig = null;
    private FragmentManager mFragmentManager = null;
    private boolean mIsFromIndoor = false;
    private int mSportType = -1;
    private double sportData;

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            this.mSportType = getIntent().getIntExtra("sport_type", -1);
        }
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_selector);
            Intent intent = getIntent();
            String selectorType = intent.getStringExtra("selector_type");
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            Fragment fragment = null;
            if (selectorType.equals("select_target_distance")) {
                fragment = V2TargetDistancePickerFragment.newInstance(this.mConfig);
            } else if (selectorType.equals("select_target_pace")) {
                fragment = V2TargetPacePickerFragment.newInstance(this.mConfig);
            } else if (selectorType.equals("select_target_time")) {
                fragment = V2TargetTimePickerFragment.newInstance(this.mConfig);
            } else if (!selectorType.equals("select_target_individual")) {
                if (selectorType.equals("select_target_heart_rate")) {
                    fragment = V2TargetHeartRateFragment.newInstance(this.mConfig);
                } else if (selectorType.equals("select_target_lock_source")) {
                    fragment = V2TargetLockSourceFragment.newInstance(this.mConfig);
                } else if (selectorType.equals("select_target_indoor_run")) {
                    this.sportData = intent.getDoubleExtra("distance_picker", 0.0d);
                    fragment = V2IndoorPickerFragment.newInstance(this.sportData);
                    this.mIsFromIndoor = true;
                } else if (selectorType.equalsIgnoreCase("select_target_calorie")) {
                    fragment = V2TargetCaloriePickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_graph_source")) {
                    fragment = V2TargetRealGraphFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_auto_lap_dis")) {
                    fragment = V2TargetAutoLapDistancePickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_month_settings")) {
                    fragment = MonthTargetPickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_trips")) {
                    fragment = V2TargetTripsPickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_heart_region_remind")) {
                    fragment = V2TargetHeartRegionRemindFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_te")) {
                    fragment = V2TargetTEFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_swim_length")) {
                    fragment = V2TargetSwimLengthFragment.newInstance(this.mConfig, intent.getBooleanExtra("key_is_first_config", false));
                } else if (selectorType.equalsIgnoreCase("select_target_heart_region_simple")) {
                    fragment = V2TargetHeartRegionSimplePickFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_auto_pause_pace")) {
                    fragment = V2TargetAutoPauseFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_background_type")) {
                    fragment = V2BackgroundPickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_select_te_stage")) {
                    fragment = V2TEStagePickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_swim_unit")) {
                    fragment = V2SwimUnitPickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_remind_km")) {
                    fragment = V2RemindKmDistancePickerFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_clap_the_hand")) {
                    fragment = V2TargetClapThaHandsFragment.newInstance(this.mConfig);
                } else if (selectorType.equalsIgnoreCase("select_target_compound_sport")) {
                    fragment = SelectCompoundPickerFragment.newInstance((MixedBaseConfig) this.mConfig);
                }
            }
            if (fragment == null) {
                throw new IllegalArgumentException("Invalid selector type : " + selectorType);
            }
            this.mFragmentManager = getFragmentManager();
            FragmentTransaction transaction = this.mFragmentManager.beginTransaction();
            transaction.replace(C0532R.id.picker_container, fragment);
            transaction.commit();
            return;
        }
        Debug.m5i(TAG, "err sport type:" + this.mSportType);
        finish();
    }

    public void onSelectFinish() {
        if (!this.mIsFromIndoor) {
            setResult(-1);
            finish();
            overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
