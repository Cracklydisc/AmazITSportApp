package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.Shader.TileMode;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class RealTimeView extends View {
    private static final String TAG = RealTimeView.class.getName();
    private int DEFAULT_LINE_COLOR;
    private int DEFAULT_LINE_STROKE_WIDTH;
    private int DEFAULT_TEXIT_SIZE;
    private int DEFAULT_TEXT_COLOR;
    private int DEFAULT_TEXT_STROKE_WIDTH;
    private boolean Debug_VIEW;
    private Context mContext;
    private int[] mCoverColors;
    private Path mCoverPath;
    private float[] mCoverRanges;
    private int mCurveColor;
    private int mCurveHeight;
    private int mCurvePaddingBottom;
    private int mCurvePaddingTop;
    private Paint mCurvePaint;
    private int mGraphType;
    private int mHeight;
    private boolean mIsShownCoverColor;
    private int mLineColor;
    private int mLineWidth;
    private Bitmap mMaxBitmap;
    private int mMaxIndex;
    private RealValueData mMaxRealValue;
    private float mMaxValue;
    private Bitmap mMinBitmap;
    private int mMinIndex;
    private RealValueData mMinRealValue;
    private float mMinValue;
    private Paint mPathClosedCurvePaint;
    private Paint mPathCurvePaint;
    private List<RealPath> mRealPathList;
    private List<RealTimeBean> mRealTimeValues;
    private List<RealValueData> mRealValueDatas;
    private int mTextColor;
    private Paint mTextPaint;
    private int mTextSize;
    private String mTitle;
    private List<RealValueData> mValues;
    private int mWidth;
    private String[] mXArray;

    private class RealPath {
        public boolean isDivide = false;
        public Path path;

        public RealPath(boolean isDivide, Path path) {
            this.isDivide = isDivide;
            this.path = path;
        }
    }

    public static class RealTimeBean implements Comparator<RealTimeBean> {
        public int index = -1;
        public float value = -1.0f;

        public RealTimeBean(int index, float value) {
            this.index = index;
            this.value = value;
        }

        public int compare(RealTimeBean o1, RealTimeBean o2) {
            int index = o1.index - o2.index;
            if (index > 0) {
                return 1;
            }
            if (index < 0) {
                return -1;
            }
            return 0;
        }

        public String toString() {
            return "RealTimeBean{index=" + this.index + ", value=" + this.value + '}';
        }
    }

    private class RealValueData {
        public int index;
        public float xValue;
        public float yValue;

        private RealValueData() {
        }

        public String toString() {
            return "RealValueData{index=" + this.index + ", xValue=" + this.xValue + ", yValue=" + this.yValue + '}';
        }
    }

    public RealTimeView(Context context) {
        this(context, null);
    }

    public RealTimeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RealTimeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.DEFAULT_LINE_STROKE_WIDTH = 5;
        this.DEFAULT_LINE_COLOR = -65536;
        this.DEFAULT_TEXT_STROKE_WIDTH = 1;
        this.DEFAULT_TEXT_COLOR = -1;
        this.DEFAULT_TEXIT_SIZE = 15;
        this.mCurvePaint = null;
        this.mPathCurvePaint = null;
        this.mPathClosedCurvePaint = null;
        this.mTextPaint = null;
        this.mCurvePaddingBottom = 18;
        this.mCurvePaddingTop = 5;
        this.mCurveHeight = 80;
        this.mTextSize = 15;
        this.mCurveColor = -65536;
        this.mTextColor = -1;
        this.mLineColor = -1;
        this.mTitle = null;
        this.mXArray = new String[]{"20", "10", "1"};
        this.mGraphType = 0;
        this.mIsShownCoverColor = true;
        this.mCoverColors = null;
        this.mCoverRanges = null;
        this.mLineWidth = 2;
        this.mMinValue = Float.MAX_VALUE;
        this.mMaxValue = Float.MIN_VALUE;
        this.mMinIndex = -1;
        this.mMaxIndex = -1;
        this.Debug_VIEW = false;
        this.mRealPathList = new LinkedList();
        this.mRealValueDatas = new LinkedList();
        this.mRealTimeValues = new LinkedList();
        this.mValues = new LinkedList();
        this.mMinRealValue = null;
        this.mMaxRealValue = null;
        this.mContext = context;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, C0532R.styleable.RealDataDisplay);
            this.mCurveHeight = a.getDimensionPixelOffset(0, 80);
            this.mTextSize = a.getDimensionPixelSize(3, 15);
            this.mCurvePaddingBottom = a.getDimensionPixelOffset(4, 18);
            this.mCurvePaddingTop = a.getDimensionPixelOffset(5, 5);
        }
        this.mLineColor = context.getResources().getColor(C0532R.color.real_graph_line_color);
        init();
    }

    private void init() {
        this.mTitle = getResources().getString(C0532R.string.sport_main_graph_title);
        this.mCurvePaint = new Paint(1);
        this.mCurvePaint.setColor(this.mCurveColor);
        this.mCurvePaint.setStrokeWidth((float) this.mLineWidth);
        this.mCurvePaint.setStyle(Style.STROKE);
        PathEffect pathEffect = new DashPathEffect(new float[]{8.0f, 8.0f}, 4.0f);
        this.mPathCurvePaint = new Paint(1);
        this.mPathCurvePaint.setColor(this.mCurveColor);
        this.mPathCurvePaint.setStrokeWidth((float) this.mLineWidth);
        this.mPathCurvePaint.setStyle(Style.STROKE);
        this.mPathCurvePaint.setPathEffect(pathEffect);
        this.mPathClosedCurvePaint = new Paint(1);
        this.mPathClosedCurvePaint.setStyle(Style.FILL);
        this.mTextPaint = new Paint(1);
        this.mTextPaint.setColor(this.mTextColor);
        this.mTextPaint.setStyle(Style.STROKE);
        this.mTextPaint.setTextSize((float) this.mTextSize);
        this.mMaxBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_chart_point);
        this.mMinBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_chart_point);
        this.mCoverColors = new int[]{getResources().getColor(C0532R.color.outdoor_heart_start_color), getResources().getColor(C0532R.color.outdoor_heart_end_color)};
        this.mCoverRanges = new float[]{0.0f, 1.0f};
        this.mCoverPath = new Path();
    }

    public void configCurveCoverGradient(boolean isShowCover, int[] coverColors, float[] ranges, int lineWidth, int lineColor, int bgType) {
        this.mIsShownCoverColor = isShowCover;
        this.mCoverColors = coverColors;
        this.mCoverRanges = ranges;
        this.mLineWidth = lineWidth;
        this.mCurveColor = lineColor;
        if (this.mCurvePaint != null) {
            this.mCurvePaint.setColor(this.mCurveColor);
            this.mCurvePaint.setStrokeWidth((float) this.mLineWidth);
        }
        if (this.mPathCurvePaint != null) {
            this.mPathCurvePaint.setColor(this.mCurveColor);
            this.mPathCurvePaint.setStrokeWidth((float) this.mLineWidth);
        }
        if (bgType == 1) {
            this.mTextColor = -16777216;
            this.mLineColor = this.mContext.getResources().getColor(C0532R.color.bg_white_real_graph_line_color);
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initWidthAndHeight();
        float textHeight = getTextHeight(this.mTextPaint);
        int startX = getPaddingLeft();
        int endX = this.mWidth - getPaddingRight();
        this.mTextPaint.setColor(this.mLineColor);
        int width = endX - startX;
        float unit = ((float) width) / 19.0f;
        for (int i = 0; i < 20; i++) {
            canvas.drawLine((((float) i) * unit) + ((float) startX), textHeight, (((float) i) * unit) + ((float) startX), textHeight + ((float) this.mCurveHeight), this.mTextPaint);
        }
        this.mTextPaint.setColor(this.mTextColor);
        if (this.mXArray != null && this.mXArray.length == 3) {
            int xAxisHeight = (int) (((float) this.mCurveHeight) + textHeight);
            float unitXAxis = ((float) width) / 19.0f;
            canvas.drawText(this.mXArray[0], ((float) startX) - (this.mTextPaint.measureText(this.mXArray[0]) / 2.0f), ((float) xAxisHeight) + textHeight, this.mTextPaint);
            canvas.drawText(this.mXArray[1], (((float) startX) + (9.0f * unitXAxis)) - (this.mTextPaint.measureText(this.mXArray[1]) / 2.0f), ((float) xAxisHeight) + textHeight, this.mTextPaint);
            canvas.drawText(this.mXArray[2], ((float) (startX + width)) - (this.mTextPaint.measureText(this.mXArray[2]) / 2.0f), ((float) xAxisHeight) + textHeight, this.mTextPaint);
        }
        if (this.mTitle != null) {
            canvas.drawText(this.mTitle, (((float) this.mWidth) - this.mTextPaint.measureText(this.mTitle)) / 2.0f, ((float) ((int) (((((float) this.mCurveHeight) + textHeight) + textHeight) + 4.0f))) + textHeight, this.mTextPaint);
        }
        reCalDataPath();
        for (RealPath realPath : this.mRealPathList) {
            canvas.drawPath(realPath.path, realPath.isDivide ? this.mPathCurvePaint : this.mCurvePaint);
        }
        if (!(this.mCoverColors == null || !this.mIsShownCoverColor || this.mMaxRealValue == null)) {
            this.mPathClosedCurvePaint.setShader(new LinearGradient(0.0f, textHeight + ((float) this.mCurveHeight), 0.0f, this.mMaxRealValue.yValue, this.mCoverColors, this.mCoverRanges, TileMode.CLAMP));
        }
        if (this.mIsShownCoverColor) {
            canvas.drawPath(this.mCoverPath, this.mPathClosedCurvePaint);
        }
        if (!(this.mMinIndex == -1 || this.mMinRealValue == null)) {
            canvas.drawBitmap(this.mMinBitmap, this.mMinRealValue.xValue - ((float) (this.mMinBitmap.getWidth() / 2)), this.mMinRealValue.yValue - ((float) (this.mMinBitmap.getHeight() / 2)), this.mCurvePaint);
            String minStr = getMinOrMaxValue(this.mMinValue);
            canvas.drawText(minStr, this.mMinRealValue.xValue - (this.mTextPaint.measureText(String.valueOf(minStr)) / 2.0f), (this.mMinRealValue.yValue + (getTextHeight(this.mTextPaint) / 2.0f)) + ((float) (this.mMinBitmap.getHeight() / 2)), this.mTextPaint);
        }
        if (this.mMaxIndex != -1 && this.mMaxIndex != this.mMinIndex && this.mMaxRealValue != null) {
            canvas.drawBitmap(this.mMaxBitmap, this.mMaxRealValue.xValue - ((float) (this.mMaxBitmap.getWidth() / 2)), this.mMaxRealValue.yValue - ((float) (this.mMaxBitmap.getHeight() / 2)), this.mCurvePaint);
            String maxStr = getMinOrMaxValue(this.mMaxValue);
            canvas.drawText(maxStr, this.mMaxRealValue.xValue - (this.mTextPaint.measureText(String.valueOf(maxStr)) / 2.0f), this.mMaxRealValue.yValue - ((float) (this.mMaxBitmap.getHeight() / 2)), this.mTextPaint);
        }
    }

    private String getMinOrMaxValue(float value) {
        if (this.mGraphType == 1) {
            return DataFormatUtils.parseSecondPerMeterToFormattedPace(value);
        }
        if (this.mGraphType == 5) {
            return DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(value);
        }
        if (this.mGraphType == 3 || this.mGraphType == 2 || this.mGraphType == 0) {
            return String.valueOf("" + ((int) value));
        }
        return String.valueOf(value);
    }

    public void setGraphType(int graphType) {
        this.mGraphType = graphType;
    }

    private void initWidthAndHeight() {
        this.mWidth = getMeasuredWidth();
        this.mHeight = getMeasuredHeight();
    }

    private float getTextHeight(Paint paint) {
        FontMetrics fm = paint.getFontMetrics();
        return (float) Math.ceil((double) (fm.descent - fm.ascent));
    }

    private void reCalDataPath() {
        this.mMaxRealValue = null;
        this.mMinRealValue = null;
        this.mRealPathList.clear();
        this.mCoverPath.reset();
        convertBean2Value();
        if (!this.mValues.isEmpty()) {
            RealValueData last = (RealValueData) this.mValues.get(0);
            this.mCoverPath.moveTo(last.xValue, getTextHeight(this.mTextPaint) + ((float) this.mCurveHeight));
            this.mCoverPath.lineTo(last.xValue, last.yValue);
            for (int i = 1; i < this.mValues.size(); i++) {
                RealValueData realValueData = (RealValueData) this.mValues.get(i);
                Path segPath = new Path();
                int curIndex = realValueData.index;
                boolean isDivde = false;
                segPath.moveTo(last.xValue, last.yValue);
                segPath.lineTo(realValueData.xValue, realValueData.yValue);
                this.mCoverPath.lineTo(realValueData.xValue, realValueData.yValue);
                if (curIndex != last.index - 1) {
                    isDivde = true;
                }
                this.mRealPathList.add(new RealPath(isDivde, segPath));
                last = realValueData;
            }
            this.mCoverPath.lineTo(last.xValue, getTextHeight(this.mTextPaint) + ((float) this.mCurveHeight));
            this.mCoverPath.close();
        }
    }

    private void calMinAndMax() {
        this.mMinValue = Float.MAX_VALUE;
        this.mMaxValue = Float.MIN_VALUE;
        if (this.Debug_VIEW) {
            Debug.m5i(TAG, "==================================================");
        }
        for (int i = 0; i < this.mRealTimeValues.size(); i++) {
            RealTimeBean bean = (RealTimeBean) this.mRealTimeValues.get(i);
            if (this.Debug_VIEW) {
                Debug.m5i(TAG, "calMinAndMax, bean:" + bean.toString());
            }
            if (bean.value > this.mMaxValue) {
                this.mMaxValue = bean.value;
                this.mMaxIndex = bean.index;
            }
            if (bean.value < this.mMinValue) {
                this.mMinValue = bean.value;
                this.mMinIndex = bean.index;
            }
        }
        if (this.Debug_VIEW) {
            Debug.m5i(TAG, "calMinAndMax, mMinIndex:" + this.mMinIndex + ", mMaxIndex:" + this.mMaxIndex);
            Debug.m5i(TAG, "==================================================");
        }
    }

    private void convertBean2Value() {
        this.mValues.clear();
        if (!this.mRealTimeValues.isEmpty()) {
            int i;
            calMinAndMax();
            int startX = getPaddingLeft();
            float unit = ((float) ((this.mWidth - getPaddingRight()) - startX)) / 19.0f;
            float range = this.mMaxValue - this.mMinValue;
            float maxValue = this.mMaxValue;
            if (this.mMaxValue == this.mMinValue) {
                maxValue = 2.0f * this.mMaxValue;
                range = this.mMaxValue;
            }
            for (i = 0; i < this.mRealTimeValues.size(); i++) {
                RealValueData valueData = new RealValueData();
                valueData.index = ((RealTimeBean) this.mRealTimeValues.get(i)).index;
                valueData.xValue = (((float) ((RealTimeBean) this.mRealTimeValues.get(i)).index) * unit) + ((float) startX);
                valueData.yValue = ((((maxValue - ((RealTimeBean) this.mRealTimeValues.get(i)).value) * ((float) ((this.mCurveHeight - this.mCurvePaddingTop) - this.mCurvePaddingBottom))) / range) + getTextHeight(this.mTextPaint)) + ((float) this.mCurvePaddingTop);
                this.mValues.add(valueData);
                if (this.mMinIndex == valueData.index) {
                    this.mMinRealValue = valueData;
                }
                if (this.mMaxIndex == valueData.index) {
                    this.mMaxRealValue = valueData;
                }
            }
            if (this.Debug_VIEW) {
                for (i = 0; i < this.mValues.size(); i++) {
                    Debug.m5i(TAG, "cordinate, index:" + i + ", cor:" + ((RealValueData) this.mValues.get(i)).toString());
                }
                Debug.m5i(TAG, "cordinate, min:" + this.mMinRealValue.toString() + ", max:" + this.mMaxRealValue.toString());
            }
        }
    }

    public void setRealTimeValue(List<RealTimeBean> realTimeBeanList) {
        if (this.Debug_VIEW) {
            int index = 0;
            for (RealTimeBean bean : realTimeBeanList) {
                Debug.m5i(TAG, "index: " + index + ", " + bean.toString());
                index++;
            }
        }
        this.mRealTimeValues = realTimeBeanList;
        postInvalidate();
    }
}
