package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wearable.view.WearableListView.OnCenterProximityListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.huami.watch.common.log.Debug;

public abstract class BaseWearItemView extends FrameLayout implements OnCenterProximityListener {
    private View mItemView;

    public abstract View initView(Context context);

    public BaseWearItemView(@NonNull Context context) {
        this(context, null);
    }

    public BaseWearItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseWearItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mItemView = null;
        this.mItemView = initView(context);
    }

    public void onCenterPosition(boolean b) {
        float scale = Math.abs(getY() - ((float) getHeight())) / ((float) (getHeight() / 2));
        Debug.m5i("center_pos", "getY:" + getY() + ", getHeight:" + getHeight());
        this.mItemView.setScaleX(1.5f - (0.5f * scale));
        this.mItemView.setScaleY(1.5f - (0.5f * scale));
        this.mItemView.setAlpha(1.0f - (0.64f * scale));
    }

    public void onNonCenterPosition(boolean b) {
        this.mItemView.setScaleX(1.0f);
        this.mItemView.setScaleY(1.0f);
        this.mItemView.setAlpha(0.64f);
    }
}
