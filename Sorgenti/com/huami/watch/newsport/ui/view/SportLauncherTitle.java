package com.huami.watch.newsport.ui.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.IconDrawable;
import clc.utils.venus.widgets.common.XTextView;
import com.huami.watch.newsport.C0532R;

public class SportLauncherTitle extends BaseDrawableGroup {
    float headerHeightScaled;
    private XTextView mContent;
    private IconDrawable mLoadingView;
    private XTextView mTitle;
    float settingsItemHeight;

    class C08771 implements AnimatorUpdateListener {
        final /* synthetic */ SportLauncherTitle this$0;

        public void onAnimationUpdate(ValueAnimator arg0) {
            float proc = ((Float) arg0.getAnimatedValue()).floatValue();
            if (this.this$0.mLoadingView != null && this.this$0.mLoadingView.getParent() != null && this.this$0.mLoadingView.isVisible()) {
                this.this$0.mLoadingView.setAlpha(proc);
                this.this$0.mLoadingView.invalidate();
            }
        }
    }

    public SportLauncherTitle(XViewWrapper context, int iconResId, int titleResId, RectF textRegion, Align align) {
        this(context, iconResId, context.getResources().getString(titleResId), "", textRegion, align);
    }

    public SportLauncherTitle(XViewWrapper context, int iconResId, String title, String content, RectF textRegion, Align align) {
        super(context);
        if (iconResId != -1) {
            this.mLoadingView = new IconDrawable(context, iconResId);
        }
        this.mTitle = new XTextView(context, title, textRegion, align);
        this.mTitle.resize(textRegion);
        this.mContent = new XTextView(context, content, textRegion, align);
        this.mContent.resize(textRegion);
        addItem(this.mTitle);
        addItem(this.mContent);
        addItem(this.mLoadingView);
        float halfH = this.localRect.height() / 2.0f;
        float halfW = this.mTitle.getWidth() / 2.0f;
        this.settingsItemHeight = context.getResources().getDimension(C0532R.dimen.widget_header_title_height);
        this.headerHeightScaled = context.getResources().getDimension(C0532R.dimen.widget_header_height_scaled);
        this.mTitle.setRelativeX(halfW - (this.mTitle.getWidth() / 2.0f));
        this.mTitle.setRelativeY(((halfH - (this.mTitle.getHeight() / 2.0f)) + (this.settingsItemHeight / 2.0f)) - (this.headerHeightScaled / 2.0f));
        this.mContent.setRelativeX(10.0f);
        this.mContent.setRelativeY(-3.0f);
    }

    public void setTitleColor(int color) {
        if (this.mTitle != null) {
            this.mTitle.setTextColor(color);
        }
    }

    public void setTitleSize(int sizePx) {
        if (this.mTitle != null) {
            this.mTitle.setTextSize((float) sizePx);
        }
    }
}
