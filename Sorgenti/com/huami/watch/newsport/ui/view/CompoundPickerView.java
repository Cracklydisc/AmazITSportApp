package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.huami.watch.common.widget.HmPickerView;

public class CompoundPickerView extends HmPickerView {
    int lastX;
    private boolean mNeedIntercept = true;

    public CompoundPickerView(Context context) {
        super(context);
    }

    public CompoundPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent) {
        if (this.mNeedIntercept) {
            if (getCenterIndex() == 1) {
                getParent().requestDisallowInterceptTouchEvent(false);
            } else {
                getParent().requestDisallowInterceptTouchEvent(true);
            }
            int x = (int) motionevent.getRawX();
            switch (motionevent.getAction()) {
                case 0:
                    this.lastX = x;
                    break;
                case 2:
                    if (x - this.lastX <= 0) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    }
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }
        return super.onInterceptTouchEvent(motionevent);
    }

    public void setNeedIntercept(boolean intercept) {
        this.mNeedIntercept = intercept;
    }
}
