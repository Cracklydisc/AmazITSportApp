package com.huami.watch.newsport.ui.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;

public class CustomDialog extends Dialog {

    public static class Builder {
        private Context context;
        private String mTitle;
        private String message;
        private OnClickListener positiveButtonClickListener;
        private String positiveButtonText;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setDialogTitle(String title) {
            this.mTitle = title;
            return this;
        }

        public CustomDialog create() {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
            final CustomDialog dialog = new CustomDialog(this.context, C0532R.style.base_dialog);
            View layout = inflater.inflate(C0532R.layout.dialog_no_gpx_file, null);
            dialog.addContentView(layout, new LayoutParams(-1, -1));
            if (!(this.positiveButtonText == null || this.positiveButtonClickListener == null)) {
                ((ImageView) layout.findViewById(C0532R.id.txt_confirm)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Builder.this.positiveButtonClickListener.onClick(dialog, -1);
                    }
                });
            }
            if (this.message != null) {
                ((HmTextView) layout.findViewById(C0532R.id.train_plan_markd_desc)).setText(this.message.trim());
            }
            if (this.mTitle != null) {
                TextView titleView = (TextView) layout.findViewById(C0532R.id.dialog_titile);
                titleView.setVisibility(0);
                titleView.setText(this.mTitle);
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }
}
