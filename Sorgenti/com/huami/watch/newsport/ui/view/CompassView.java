package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;

public class CompassView extends View {
    private int mBgType;
    private float mDegree;
    private Matrix mMatrix;
    private Paint mPaint;
    private Drawable mPointer;
    private PointerUtil pointerUtil;
    private float[] pos;

    private static class PointerUtil {
        private float centerX;
        private float centerY;
        private float endDegree;
        private float radius;
        private float radiusShort;
        private float startDegree;

        public PointerUtil(float centerX, float centerY, float radius, float radiusShort) {
            this.centerX = centerX;
            this.centerY = centerY;
            this.radius = radius;
            this.radiusShort = radiusShort;
            float degree = ((float) Math.asin((double) (radiusShort / radius))) * 57.29578f;
            this.startDegree = -90.0f - degree;
            this.endDegree = 90.0f + degree;
        }

        public void getPosition(float degree, float[] pos) {
            if (pos == null || pos.length >= 2) {
                degree %= 360.0f;
                if (degree > 180.0f) {
                    degree -= 360.0f;
                }
                if (degree < this.startDegree || degree > this.endDegree) {
                    pos[0] = (float) (((double) this.centerX) - (Math.tan((double) (degree * 0.017453292f)) * ((double) this.radiusShort)));
                    pos[1] = this.centerY + this.radiusShort;
                    return;
                }
                pos[0] = (float) (((double) this.centerX) + (Math.sin((double) (degree * 0.017453292f)) * ((double) this.radius)));
                pos[1] = (float) (((double) this.centerY) - (Math.cos((double) (degree * 0.017453292f)) * ((double) this.radius)));
                return;
            }
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public CompassView(Context context) {
        this(context, null);
    }

    public CompassView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CompassView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mMatrix = new Matrix();
        this.mBgType = 0;
        this.mPaint = null;
        this.mDegree = 0.0f;
        this.pos = new float[2];
        this.pointerUtil = new PointerUtil(160.0f, 160.0f, 160.0f, 140.0f);
        this.mBgType = BackgroundUtils.getCustomBgType(context);
        if (this.mBgType == 1) {
            this.mPointer = getResources().getDrawable(C0532R.drawable.bg_white_sport_map_north, null);
        } else {
            this.mPointer = getResources().getDrawable(C0532R.drawable.sport_map_fun_icon_compass, null);
        }
        this.mPointer.setBounds(0, 0, this.mPointer.getIntrinsicWidth(), this.mPointer.getIntrinsicHeight());
        this.mPaint = new Paint(1);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(-1);
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawColor(0, Mode.CLEAR);
        if (this.mBgType == 1) {
            canvas.drawRect(0.0f, 0.0f, 320.0f, 320.0f, this.mPaint);
        }
        this.mMatrix.setRotate(this.mDegree);
        this.mMatrix.preTranslate(((float) (-this.mPointer.getIntrinsicWidth())) * 0.5f, 0.0f);
        this.mMatrix.postTranslate(this.pos[0], this.pos[1]);
        canvas.save(1);
        canvas.concat(this.mMatrix);
        this.mPointer.draw(canvas);
        canvas.restore();
    }

    public void onRotate(float degree) {
        this.mDegree = degree;
        this.pointerUtil.getPosition(degree, this.pos);
        postInvalidate();
    }

    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
