package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class CircleProgressView extends View {
    private int circleRadius;
    private int colorArcResource;
    private int colorCircleResource;
    private int mCircleLineStrokeWidth;
    private int mMaxProgress;
    private final Paint mPaint = new Paint();
    private int mProgress = 0;
    private final RectF mRectF = new RectF();

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mMaxProgress = context.getResources().getInteger(C0532R.dimen.quicksettings_chargingCircleMaxProgress);
        this.circleRadius = context.getResources().getDimensionPixelOffset(C0532R.dimen.quicksettings_chargingCircleRadius);
        this.mCircleLineStrokeWidth = context.getResources().getDimensionPixelOffset(C0532R.dimen.quicksettings_chargingCircleLineStrokeWidth);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth((float) this.mCircleLineStrokeWidth);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.colorCircleResource = context.getResources().getColor(C0532R.color.quicksettings_chargingBgCircle_color);
        this.colorArcResource = context.getResources().getColor(C0532R.color.quicksettings_chargingCircle_color);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        canvas.drawColor(0);
        this.mPaint.setColor(this.colorCircleResource);
        canvas.drawArc(this.mRectF, -90.0f, 360.0f, false, this.mPaint);
        this.mPaint.setColor(this.colorArcResource);
        canvas.drawArc(this.mRectF, -90.0f, 360.0f * (((float) this.mProgress) / ((float) this.mMaxProgress)), false, this.mPaint);
        canvas.restore();
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        float centerX = ((float) (right - left)) / 2.0f;
        float centerY = ((float) (bottom - top)) / 2.0f;
        this.mRectF.set(centerX - ((float) this.circleRadius), centerY - ((float) this.circleRadius), ((float) this.circleRadius) + centerX, ((float) this.circleRadius) + centerY);
    }

    public void setProgress(int progress) {
        this.mProgress = progress;
        invalidate();
    }
}
