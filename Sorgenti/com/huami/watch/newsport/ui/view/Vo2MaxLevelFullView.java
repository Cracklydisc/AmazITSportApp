package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class Vo2MaxLevelFullView extends View {
    private static final String TAG = Vo2MaxLevelFullView.class.getSimpleName();
    private int alphaInt;
    private int[] colorArr;
    private boolean leftHasDataStaus;
    private float leftRateAngle;
    private float leftStartAngle;
    private float mCanvasHeight;
    private float mCanvasWidth;
    private float mMarginBottom;
    private float mMarginLeft;
    private float mMarginRigth;
    private float mMarginTop;
    private Paint mPaint;
    private float mScreenCenterX;
    private float mScreenCenterY;
    private Paint mSectorViewPaint;
    private float mSectorViewWidth;
    private float rightRateAngle;
    private float rightStartAngle;
    private boolean rigthHasDataStatus;
    private float totalRate;
    private float triangR;
    private Bitmap triangleBitmap;

    public Vo2MaxLevelFullView(Context context) {
        this(context, null);
    }

    public Vo2MaxLevelFullView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Vo2MaxLevelFullView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mMarginLeft = 20.0f;
        this.mMarginRigth = 20.0f;
        this.mMarginTop = 20.0f;
        this.mMarginBottom = 20.0f;
        this.mCanvasWidth = 0.0f;
        this.mCanvasHeight = 0.0f;
        this.mScreenCenterX = 0.0f;
        this.mScreenCenterY = 0.0f;
        this.mSectorViewWidth = 35.0f;
        this.leftRateAngle = 0.0f;
        this.rightRateAngle = 0.0f;
        this.leftStartAngle = 148.0f;
        this.rightStartAngle = 23.0f;
        this.alphaInt = 50;
        this.triangR = 0.0f;
        this.totalRate = 69.0f;
        initPatin();
    }

    private void initPatin() {
        this.mPaint = new Paint();
        this.mPaint.setColor(-65536);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setTextAlign(Align.CENTER);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeCap(Cap.BUTT);
        this.mSectorViewPaint = new Paint();
        this.mSectorViewPaint.setAntiAlias(true);
        this.mSectorViewPaint.setStrokeWidth(this.mSectorViewWidth);
        this.mSectorViewPaint.setTextAlign(Align.RIGHT);
        this.mSectorViewPaint.setStyle(Style.STROKE);
        this.mSectorViewPaint.setStrokeCap(Cap.BUTT);
        this.mSectorViewPaint.setColor(-65536);
        this.mSectorViewPaint.setAlpha(150);
        this.triangleBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.rade_triangle);
        this.colorArr = new int[7];
        this.colorArr[0] = getResources().getColor(C0532R.color.max_vo2_max_level_1);
        this.colorArr[1] = getResources().getColor(C0532R.color.max_vo2_max_level_2);
        this.colorArr[2] = getResources().getColor(C0532R.color.max_vo2_max_level_3);
        this.colorArr[3] = getResources().getColor(C0532R.color.max_vo2_max_level_4);
        this.colorArr[4] = getResources().getColor(C0532R.color.max_vo2_max_level_5);
        this.colorArr[5] = getResources().getColor(C0532R.color.max_vo2_max_level_6);
        this.colorArr[6] = getResources().getColor(C0532R.color.max_vo2_max_level_7);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        initMeasurceData();
        this.mPaint.setStyle(Style.STROKE);
        drawSectorView(canvas);
        this.mPaint.setColor(-16777216);
        this.mPaint.setStyle(Style.FILL);
        canvas.drawCircle(this.mScreenCenterX, this.mScreenCenterY + 24.0f, (this.mCanvasWidth * 13.0f) / 30.0f, this.mPaint);
        if (this.rightRateAngle > 0.0f) {
            drawRigthVo2MaxTriangleLevel(canvas);
        }
        if (this.leftRateAngle > 0.0f) {
            drawVLeftvo2maxTriangleLevel(canvas);
        }
    }

    private void drawSectorView(Canvas canvas) {
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeCap(Cap.BUTT);
        float RCircle = (this.mCanvasWidth / 2.0f) - (this.mSectorViewWidth / 2.0f);
        float left = this.mScreenCenterX - RCircle;
        float top = (this.mScreenCenterY + 12.0f) - RCircle;
        float right = this.mScreenCenterX + RCircle;
        float bottom = (this.mScreenCenterY + 12.0f) + RCircle;
        this.mSectorViewPaint.setColor(-16711936);
        RectF rect = new RectF(left, top, right, bottom);
        for (int i = 0; i < 7; i++) {
            this.mSectorViewPaint.setColor(this.colorArr[i]);
            Canvas canvas2;
            if (this.leftHasDataStaus) {
                this.mSectorViewPaint.setAlpha(255);
                canvas2 = canvas;
                canvas2.drawArc(rect, ((float) (i * 10)) + this.leftStartAngle, 9.0f, false, this.mSectorViewPaint);
            } else {
                this.mSectorViewPaint.setAlpha((this.alphaInt * 255) / 100);
                canvas2 = canvas;
                canvas2.drawArc(rect, ((float) (i * 10)) + this.leftStartAngle, 9.0f, false, this.mSectorViewPaint);
            }
            if (this.rigthHasDataStatus) {
                this.mSectorViewPaint.setAlpha(255);
                canvas.drawArc(rect, this.rightStartAngle - ((float) (i * 10)), 9.0f, false, this.mSectorViewPaint);
            } else {
                this.mSectorViewPaint.setAlpha((this.alphaInt * 255) / 100);
                canvas.drawArc(rect, this.rightStartAngle - ((float) (i * 10)), 9.0f, false, this.mSectorViewPaint);
            }
        }
    }

    private void drawVLeftvo2maxTriangleLevel(Canvas canvas) {
        this.triangR = (this.mCanvasWidth / 2.0f) + ((float) (this.triangleBitmap.getWidth() / 4));
        Matrix matrix = new Matrix();
        matrix.postRotate((90.0f + this.leftStartAngle) + this.leftRateAngle);
        matrix.postTranslate(this.mScreenCenterX - ((float) (((double) this.triangR) * Math.cos(Math.toRadians((double) (((this.leftStartAngle - 9.0f) + this.leftRateAngle) - 180.0f))))), this.mScreenCenterY - ((float) (((double) this.triangR) * Math.sin(Math.toRadians((double) (((this.leftStartAngle - 9.0f) + this.leftRateAngle) - 180.0f))))));
        canvas.drawBitmap(this.triangleBitmap, matrix, this.mSectorViewPaint);
    }

    private void drawRigthVo2MaxTriangleLevel(Canvas canvas) {
        this.triangR = (this.mCanvasWidth / 2.0f) + ((float) (this.triangleBitmap.getWidth() / 4));
        Matrix matrix = new Matrix();
        matrix.postRotate((90.0f + this.rightStartAngle) - this.rightRateAngle);
        matrix.postTranslate(this.mScreenCenterX + ((float) (((double) this.triangR) * Math.cos(Math.toRadians((double) ((this.rightStartAngle + 9.0f) - this.rightRateAngle))))), this.mScreenCenterY + ((float) (((double) this.triangR) * Math.sin(Math.toRadians((double) ((this.rightStartAngle + 9.0f) - this.rightRateAngle))))));
        canvas.drawBitmap(this.triangleBitmap, matrix, this.mSectorViewPaint);
    }

    private void initMeasurceData() {
        this.mCanvasWidth = (((float) getWidth()) - this.mMarginLeft) - this.mMarginRigth;
        this.mCanvasHeight = (((float) getHeight()) - this.mMarginTop) - this.mMarginBottom;
        this.mScreenCenterX = this.mMarginLeft + (this.mCanvasWidth / 2.0f);
        this.mScreenCenterY = this.mMarginTop + (this.mCanvasHeight / 2.0f);
    }

    public void setLeftAndRightLevel(int leftLevel, boolean isleftHasData, int rightLevel, boolean isRightHasData) {
        if (rightLevel == 0) {
            this.rightRateAngle = 0.0f;
        }
        if (leftLevel == 0) {
            this.leftRateAngle = 0.0f;
        }
        if (rightLevel != 0) {
            this.rightRateAngle = (float) ((((double) this.totalRate) * (((double) (rightLevel - 1)) + 0.5d)) / 7.0d);
        }
        if (leftLevel != 0) {
            this.leftRateAngle = (float) ((((double) this.totalRate) * (((double) (leftLevel - 1)) + 0.5d)) / 7.0d);
        }
        this.leftHasDataStaus = isleftHasData;
        this.rigthHasDataStatus = isRightHasData;
        postInvalidate();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.triangleBitmap != null && !this.triangleBitmap.isRecycled()) {
            this.triangleBitmap.recycle();
        }
    }
}
