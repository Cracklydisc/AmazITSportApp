package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.huami.watch.newsport.C0532R;
import java.math.BigDecimal;

public class DailyPerformenceChatView extends View {
    private static final String TAG = DailyPerformenceChatView.class.getSimpleName();
    public static int invalidPointValue = 127;
    public static int invalideLessPaintValue = -20;
    public static int invalieBiggerPaintValue = 20;
    private float[] LargerDividingLine;
    private float[] LowDividingLine;
    private int SEGMENT_BOTTOM;
    private int SEGMENT_FIVE;
    private int SEGMENT_TEN;
    private float canvanHeigth;
    private float canvanWidth;
    private float circleRadiusSize;
    private float dataLineFontSize;
    private Paint dataLinePaint;
    private int[] dataStageNum;
    private float fontMargin;
    private String hasNoDataResult;
    private String[] leftTextList;
    private int mCircleLineStrokeWidth;
    private Paint mFontPaint;
    private float mHeigthStep;
    private Paint mLineColorDeepPaint;
    private Paint mLineColorLowPaint;
    private int mSegemntHeigthNum;
    private float mSegemntWidthNum;
    private Paint mSelectStatusPaint;
    private String mTitle;
    private String mUnit;
    private int mVerticalMaxY;
    private float mWidthStep;
    private float marginBottom;
    private float marginLeft;
    private float marginRight;
    private float marginTop;
    private float normalFontSize;
    private String[] rigthTextList;
    private int[] sportStatusData;
    private float titleFontSize;
    private float totalDistance;
    private float unitFontSize;

    public DailyPerformenceChatView(Context context) {
        this(context, null);
    }

    public DailyPerformenceChatView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DailyPerformenceChatView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCircleLineStrokeWidth = 1;
        this.canvanWidth = 0.0f;
        this.canvanHeigth = 0.0f;
        this.marginLeft = 72.0f;
        this.marginRight = 60.0f;
        this.marginTop = 10.0f;
        this.marginBottom = 50.0f;
        this.dataLineFontSize = 45.0f;
        this.fontMargin = 5.0f;
        this.mWidthStep = 0.0f;
        this.mHeigthStep = 0.0f;
        this.mSegemntWidthNum = 1.0f;
        this.mSegemntHeigthNum = 5;
        this.circleRadiusSize = 3.0f;
        this.dataStageNum = new int[this.mSegemntHeigthNum];
        this.LowDividingLine = new float[]{-25.0f, -15.0f, -5.0f, 5.0f, 15.0f};
        this.LargerDividingLine = new float[]{-15.0f, -5.0f, 5.0f, 15.0f, 25.0f};
        this.leftTextList = new String[]{"-20", "-10", "-5", "+5", "+10", "+20"};
        this.mTitle = "";
        this.mUnit = "";
        this.normalFontSize = 10.0f;
        this.titleFontSize = 25.0f;
        this.unitFontSize = 15.0f;
        this.mVerticalMaxY = 25;
        this.SEGMENT_FIVE = 5;
        this.SEGMENT_TEN = 10;
        this.SEGMENT_BOTTOM = 6;
        this.totalDistance = 0.0f;
        this.hasNoDataResult = "";
        initPaint();
    }

    private void initPaint() {
        this.mUnit = getResources().getString(C0532R.string.sport_history_item_distance_desc);
        this.mTitle = getResources().getString(C0532R.string.first_beat_sport_status);
        this.rigthTextList = getResources().getStringArray(C0532R.array.dally_performence_level_status);
        this.hasNoDataResult = getResources().getString(C0532R.string.daily_propermance_has_no_data);
        this.mLineColorLowPaint = new Paint();
        this.mLineColorLowPaint.setAntiAlias(true);
        this.mLineColorLowPaint.setStrokeWidth((float) this.mCircleLineStrokeWidth);
        this.mLineColorLowPaint.setStyle(Style.STROKE);
        this.mLineColorLowPaint.setStrokeCap(Cap.ROUND);
        this.mLineColorLowPaint.setColor(-1);
        this.mLineColorLowPaint.setAlpha(100);
        this.mLineColorDeepPaint = new Paint();
        this.mLineColorDeepPaint.setAntiAlias(true);
        this.mLineColorDeepPaint.setStrokeWidth((float) this.mCircleLineStrokeWidth);
        this.mLineColorDeepPaint.setStyle(Style.STROKE);
        this.mLineColorDeepPaint.setStrokeCap(Cap.ROUND);
        this.mLineColorDeepPaint.setColor(-1);
        this.dataLinePaint = new Paint();
        this.dataLinePaint.setAntiAlias(true);
        this.dataLinePaint.setStrokeWidth(2.0f);
        this.dataLinePaint.setStyle(Style.STROKE);
        this.dataLinePaint.setStrokeCap(Cap.ROUND);
        this.dataLinePaint.setColor(-1);
        this.mSelectStatusPaint = new Paint();
        this.mSelectStatusPaint.setAntiAlias(true);
        this.mSelectStatusPaint.setStrokeWidth(2.0f);
        this.mSelectStatusPaint.setStyle(Style.FILL);
        this.mSelectStatusPaint.setStrokeCap(Cap.ROUND);
        this.mSelectStatusPaint.setColor(-16711936);
        this.mSelectStatusPaint.setAlpha(100);
        this.mFontPaint = new Paint();
        this.mFontPaint.setAntiAlias(true);
        this.mFontPaint.setStrokeWidth(2.0f);
        this.mFontPaint.setStyle(Style.FILL);
        this.mFontPaint.setStrokeCap(Cap.ROUND);
        this.mFontPaint.setColor(-1);
        this.mFontPaint.setTextSize(this.normalFontSize);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvanWidth = (((float) getWidth()) - this.marginRight) - this.marginLeft;
        this.canvanHeigth = (((float) getHeight()) - this.marginBottom) - this.marginTop;
        if (this.mSegemntWidthNum == 1.0f) {
            this.mWidthStep = 0.0f;
        } else {
            this.mWidthStep = this.canvanWidth / (this.mSegemntWidthNum - 1.0f);
        }
        this.mHeigthStep = this.canvanHeigth / ((float) this.mSegemntHeigthNum);
        drawBackColor(canvas);
        canvas.drawRect(this.marginLeft, this.marginTop, ((float) getWidth()) - this.marginRight, ((float) getHeight()) - this.marginBottom, this.mLineColorDeepPaint);
        drawChartViewTitle(canvas);
        drawChartViewUnit(canvas);
        this.mFontPaint.setTextSize(this.normalFontSize);
        drawHorizontalLine(canvas);
        drawVerticalLine(canvas);
        drawDataLine(canvas);
        this.mFontPaint.setTextSize(this.normalFontSize);
        drawSecondLeftText(canvas);
        drawBottomText(canvas, this.totalDistance);
        drawSecondRightText(canvas);
    }

    private void drawBackColor(Canvas canvas) {
        drawNinePathBitmap(canvas);
    }

    private void drawChartViewTitle(Canvas canvas) {
        this.mFontPaint.setTextSize(this.titleFontSize);
        this.mFontPaint.setTextAlign(Align.LEFT);
        float x = this.marginLeft + 10.0f;
        float y = this.marginTop - 10.0f;
    }

    private void drawChartViewUnit(Canvas canvas) {
        this.mFontPaint.setTextSize(this.unitFontSize);
        this.mFontPaint.setTextAlign(Align.RIGHT);
        canvas.drawText(this.mUnit, this.marginLeft + this.canvanWidth, (this.marginTop + this.canvanHeigth) + 30.0f, this.mFontPaint);
    }

    private void drawSecondRightText(Canvas canvas) {
        this.mFontPaint.setTextAlign(Align.LEFT);
        Rect rect = new Rect();
        for (int i = 0; i < this.leftTextList.length; i++) {
            float x = (this.marginLeft + this.canvanWidth) + 5.0f;
            this.mFontPaint.getTextBounds(this.leftTextList[i], 0, this.leftTextList[i].length(), rect);
            canvas.drawText(this.leftTextList[i], x, ((this.marginTop + this.canvanHeigth) - (this.mHeigthStep * ((float) i))) + ((float) (rect.height() / 2)), this.mFontPaint);
        }
    }

    private void drawBottomText(Canvas canvas, float totalDistance) {
        initSegmentBottom();
        this.mFontPaint.setTextAlign(Align.LEFT);
        Rect rect = new Rect();
        float bottomWidthSegment = this.canvanWidth / ((float) this.SEGMENT_BOTTOM);
        String result = "";
        int i;
        float startX;
        float startY;
        float stopY;
        if (totalDistance >= 6.0f) {
            float bottomSegment = Math.abs(totalDistance - 1.0f) / ((float) this.SEGMENT_BOTTOM);
            Log.i(TAG, " SEGMENT_BOTTOM:" + this.SEGMENT_BOTTOM);
            for (i = 0; i <= this.SEGMENT_BOTTOM; i++) {
                startX = this.marginLeft + (((float) i) * bottomWidthSegment);
                startY = this.marginTop + this.canvanHeigth;
                stopY = startY + 6.0f;
                canvas.drawLine(startX, startY, startX, stopY, this.mLineColorDeepPaint);
                if (totalDistance < 1.0f) {
                    result = getDistanceFormat(((float) i) * bottomSegment);
                } else {
                    result = getDistanceFormat(1.0f + (((float) i) * bottomSegment));
                }
                Log.i(TAG, " drawBottomText startX:" + startX + ",result:" + result);
                this.mFontPaint.getTextBounds(result, 0, result.length(), rect);
                canvas.drawText(result, startX - ((float) (rect.width() / 2)), 10.0f + stopY, this.mFontPaint);
            }
        } else if (totalDistance >= 1.0f && totalDistance < 6.0f) {
            int widthNum = (int) totalDistance;
            Log.i(TAG, " mSegemntWidthNum+1:" + (widthNum + 1));
            for (i = 1; i <= widthNum + 1; i++) {
                if (i == widthNum + 1) {
                    startX = (this.marginLeft + (this.mWidthStep * ((float) (i - 2)))) + (this.mWidthStep * (totalDistance - ((float) widthNum)));
                    result = getDistanceFormat(totalDistance);
                } else {
                    startX = this.marginLeft + (this.mWidthStep * ((float) (i - 1)));
                    result = String.valueOf(i);
                }
                startY = this.marginTop + this.canvanHeigth;
                stopY = startY + 6.0f;
                canvas.drawLine(startX, startY, startX, stopY, this.mLineColorDeepPaint);
                this.mFontPaint.getTextBounds(result, 0, result.length(), rect);
                canvas.drawText(result, startX - ((float) (rect.width() / 2)), 10.0f + stopY, this.mFontPaint);
            }
        }
    }

    private void initSegmentBottom() {
        if (this.totalDistance >= 6.0f) {
            this.SEGMENT_BOTTOM = 6;
        } else if (this.totalDistance <= 1.0f) {
            this.SEGMENT_BOTTOM = 1;
        }
    }

    private String getDistanceFormat(float number) {
        return new BigDecimal((double) number).setScale(2, 4).toString();
    }

    private void drawSecondLeftText(Canvas canvas) {
        this.mFontPaint.setTextAlign(Align.RIGHT);
        Rect rect = new Rect();
        this.mFontPaint.getTextBounds(this.rigthTextList[0], 0, this.rigthTextList[0].length(), rect);
        float x = this.marginLeft - 5.0f;
        canvas.drawText(this.rigthTextList[0], x, ((this.marginTop + this.canvanHeigth) - (this.mHeigthStep * 1.0f)) + ((float) (rect.height() / 2)), this.mFontPaint);
        this.mFontPaint.getTextBounds(this.rigthTextList[1], 0, this.rigthTextList[1].length(), rect);
        canvas.drawText(this.rigthTextList[1], x, ((this.marginTop + this.canvanHeigth) - (2.5f * this.mHeigthStep)) + ((float) (rect.height() / 2)), this.mFontPaint);
        this.mFontPaint.getTextBounds(this.rigthTextList[2], 0, this.rigthTextList[2].length(), rect);
        canvas.drawText(this.rigthTextList[2], x, ((this.marginTop + this.canvanHeigth) - (4.0f * this.mHeigthStep)) + ((float) (rect.height() / 2)), this.mFontPaint);
    }

    private void drawDataLine(Canvas canvas) {
        if (this.sportStatusData == null || this.sportStatusData.length <= 0 || this.totalDistance < 1.0f) {
            this.mFontPaint.setTextSize(this.titleFontSize);
            float hasNoDataX = this.marginLeft + (this.canvanWidth / 2.0f);
            float hasNoDataY = (this.marginTop + (this.canvanHeigth / 2.0f)) - 10.0f;
            this.mFontPaint.setTextAlign(Align.CENTER);
            canvas.drawText(this.hasNoDataResult, hasNoDataX, hasNoDataY, this.mFontPaint);
            return;
        }
        this.dataLinePaint.setColor(-1);
        this.dataLinePaint.setTextSize(this.dataLineFontSize);
        this.mLineColorDeepPaint.setStyle(Style.FILL);
        float lastPaintX = 0.0f;
        float lastPaintY = 0.0f;
        int currnetPaintY = invalidPointValue;
        int totalDistanceInt = (int) (((double) this.totalDistance) - 0.5d);
        Log.i(TAG, " sportStatusDataSize:" + this.sportStatusData.length);
        for (int i = 0; i < this.sportStatusData.length; i++) {
            float circleX;
            if (i == this.sportStatusData.length - 1) {
                circleX = (this.marginLeft + (this.mWidthStep * ((float) (totalDistanceInt - 1)))) + (this.mWidthStep * (this.totalDistance - ((float) totalDistanceInt)));
            } else {
                circleX = this.marginLeft + (this.mWidthStep * ((float) i));
            }
            currnetPaintY = this.sportStatusData[i];
            float circleTestY = (this.marginTop + (this.canvanHeigth / 2.0f)) - (((((float) currnetPaintY) / ((float) this.mVerticalMaxY)) * this.canvanHeigth) / 2.0f);
            float circleY = getDataPaintHeigth(currnetPaintY);
            Log.i(TAG, "drawDataLine circleY :" + circleY + ",circleTestY:" + circleTestY);
            if (currnetPaintY >= invalideLessPaintValue && currnetPaintY <= invalieBiggerPaintValue) {
                canvas.drawCircle(circleX, circleY, this.circleRadiusSize, this.mLineColorDeepPaint);
                if (i == 0) {
                    lastPaintX = circleX;
                    lastPaintY = circleY;
                } else {
                    canvas.drawLine(lastPaintX, lastPaintY, circleX, circleY, this.dataLinePaint);
                    lastPaintX = circleX;
                    lastPaintY = circleY;
                }
            }
        }
        this.mLineColorDeepPaint.setStyle(Style.STROKE);
    }

    private float getDataPaintHeigth(int dataPointY) {
        float y = this.marginTop + this.canvanHeigth;
        if (dataPointY < invalideLessPaintValue) {
            return (float) invalidPointValue;
        }
        if (dataPointY < -10) {
            return y - ((((float) (dataPointY - invalideLessPaintValue)) * this.mHeigthStep) / ((float) this.SEGMENT_TEN));
        }
        if (dataPointY < -5) {
            return (y - this.mHeigthStep) - ((((float) (dataPointY + 10)) * this.mHeigthStep) / ((float) this.SEGMENT_FIVE));
        }
        if (dataPointY < 5) {
            return (y - (this.mHeigthStep * 2.0f)) - ((((float) (dataPointY + 5)) * this.mHeigthStep) / ((float) this.SEGMENT_TEN));
        }
        if (dataPointY < 10) {
            return (y - (this.mHeigthStep * 3.0f)) - ((((float) (dataPointY - 5)) * this.mHeigthStep) / ((float) this.SEGMENT_FIVE));
        }
        if (dataPointY <= 20) {
            return (y - (this.mHeigthStep * 4.0f)) - ((((float) (dataPointY - 10)) * this.mHeigthStep) / ((float) this.SEGMENT_TEN));
        }
        return (float) invalidPointValue;
    }

    private void drawHorizontalLine(Canvas canvas) {
        int horizontalLineNums = this.mSegemntHeigthNum - 1;
        this.mLineColorLowPaint.setColor(-16777216);
        for (int i = 0; i < horizontalLineNums; i++) {
            float stopY = this.marginTop + (this.mHeigthStep * ((float) (i + 1)));
            Canvas canvas2 = canvas;
            canvas2.drawLine(this.marginLeft, stopY, ((float) getWidth()) - this.marginRight, stopY, this.mLineColorLowPaint);
        }
    }

    private void drawNinePathBitmap(Canvas canvas) {
        drawGreenNinePath(canvas);
        drawRedNinePath(canvas);
        drawYellowNinePath(canvas);
    }

    private void drawGreenNinePath(Canvas canvas) {
        float left = this.marginLeft;
        float top = this.marginTop;
        float right = this.marginLeft + this.canvanWidth;
        float bottom = this.marginTop + (2.0f * this.mHeigthStep);
        Bitmap bmp_9 = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_state_bg_green);
        new NinePatch(bmp_9, bmp_9.getNinePatchChunk(), null).draw(canvas, new Rect((int) left, (int) top, (int) right, (int) bottom));
    }

    private void drawRedNinePath(Canvas canvas) {
        float left = this.marginLeft;
        float top = this.marginTop + (3.0f * this.mHeigthStep);
        float right = this.marginLeft + this.canvanWidth;
        float bottom = this.marginTop + (5.0f * this.mHeigthStep);
        Bitmap bmp_9 = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_state_bg_red);
        new NinePatch(bmp_9, bmp_9.getNinePatchChunk(), null).draw(canvas, new Rect((int) left, (int) top, (int) right, (int) bottom));
    }

    private void drawYellowNinePath(Canvas canvas) {
        float left = this.marginLeft;
        float top = this.marginTop + (2.0f * this.mHeigthStep);
        float right = this.marginLeft + this.canvanWidth;
        float bottom = this.marginTop + (3.0f * this.mHeigthStep);
        Bitmap bmp_9 = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_state_bg_yellow);
        new NinePatch(bmp_9, bmp_9.getNinePatchChunk(), null).draw(canvas, new Rect((int) left, (int) top, (int) right, (int) bottom));
    }

    private void drawVerticalLine(Canvas canvas) {
        this.mLineColorLowPaint.setColor(-1);
        this.mLineColorLowPaint.setAlpha(150);
        int verticalLineNum = ((int) Math.round(((double) this.mSegemntWidthNum) + 0.5d)) - 1;
        Log.i(TAG, "verticalLineNum" + verticalLineNum + " , number:" + Math.round(((double) this.mSegemntWidthNum) + 0.5d));
        for (int i = 0; i < verticalLineNum; i++) {
            float stopX;
            float startX;
            if (i == verticalLineNum - 1) {
                stopX = this.marginLeft + (this.mWidthStep * ((this.mSegemntWidthNum + ((float) i)) - ((float) verticalLineNum)));
                startX = stopX;
                Log.i(TAG, "verticalLineNum: index:" + i + " ," + ((this.mSegemntWidthNum - ((float) verticalLineNum)) + ((float) i)) + ",mSegemntWidthNum:" + this.mSegemntWidthNum + ",verticalLineNum:" + verticalLineNum);
            } else {
                stopX = this.marginLeft + (this.mWidthStep * ((float) (i + 1)));
                startX = stopX;
                Log.i(TAG, "verticalLineNum: index:" + i + " ," + (i + 1));
            }
            canvas.drawLine(startX, this.marginTop, stopX, ((float) getHeight()) - this.marginBottom, this.mLineColorLowPaint);
        }
    }

    public void setSportStatusData(int[] data, double distance) {
        if (data != null && data.length != 0) {
            this.mSegemntWidthNum = (float) distance;
            this.totalDistance = (float) distance;
            this.sportStatusData = data;
            Log.i(TAG, "----dailyPormenceData---distance:" + distance);
            printIntData(data);
            postInvalidate();
        }
    }

    private void printIntData(int[] data) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i : data) {
            sb.append(i + ",");
        }
        sb.append("]");
        Log.i(TAG, " printIntData:" + sb.toString());
    }
}
