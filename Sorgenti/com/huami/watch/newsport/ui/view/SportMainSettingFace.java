package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import clc.utils.venus.XViewWrapper;

public class SportMainSettingFace extends XViewWrapper {
    private boolean isSlideDismissAllow = false;

    public SportMainSettingFace(Context context, AttributeSet attrs) {
        super(context, attrs);
        initOrition();
    }

    public SportMainSettingFace(Context context) {
        super(context);
        initOrition();
    }

    private void initOrition() {
        setReleaseOrientationWhileInteractiveWithParent(13);
    }

    public void setDismissAllow(boolean canDismiss) {
        this.isSlideDismissAllow = canDismiss;
    }

    public boolean canScrollHorizontally(int direction) {
        if (this.isSlideDismissAllow) {
            return super.canScrollHorizontally(direction);
        }
        return true;
    }
}
