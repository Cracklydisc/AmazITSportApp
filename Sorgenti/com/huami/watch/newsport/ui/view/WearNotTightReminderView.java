package com.huami.watch.newsport.ui.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.PathInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.newsport.C0532R;

public class WearNotTightReminderView extends FrameLayout {
    private static final PathInterpolator EASE = new PathInterpolator(0.42f, 0.0f, 0.42f, 1.01f);
    private static final LinearInterpolator LINEAR = new LinearInterpolator();
    private AnimatorSet mAnims;
    private ImageView mArrow1;
    private ImageView mArrow2;
    private ImageView mArrow3;
    private ImageView mBg;
    private TextView mText1;
    private TextView mText2;
    private ImageView mWatch1;
    private ImageView mWatch2;

    class C08801 implements Runnable {
        final /* synthetic */ WearNotTightReminderView this$0;

        public void run() {
            this.this$0.reset();
            this.this$0.startAnim(false);
        }
    }

    public WearNotTightReminderView(Context context) {
        super(context);
        setBackgroundColor(-16777216);
        this.mBg = new ImageView(context);
        this.mBg.setImageResource(C0532R.drawable.sport_tips_bg);
        LayoutParams lp = new LayoutParams(-2, -2);
        lp.leftMargin = 18;
        lp.topMargin = 40;
        addView(this.mBg, lp);
        this.mWatch1 = addImageView(context, C0532R.drawable.sport_tips_watch1);
        this.mWatch1.setImageAlpha(0);
        this.mWatch2 = addImageView(context, C0532R.drawable.sport_tips_watch2);
        this.mWatch2.setImageAlpha(0);
        this.mArrow1 = addImageView(context, C0532R.drawable.sport_tips_arrow1);
        this.mArrow2 = addImageView(context, C0532R.drawable.sport_tips_arrow2);
        this.mArrow3 = addImageView(context, C0532R.drawable.sport_tips_arrow3);
        this.mText1 = new TextView(context);
        this.mText1.setGravity(1);
        this.mText1.setText(C0532R.string.reminder_wear_not_tight_0);
        this.mText1.setTextColor(-1);
        this.mText1.setTextSize(15.0f);
        LayoutParams lpt1 = new LayoutParams(200, -2);
        lpt1.topMargin = 226;
        lpt1.gravity = 1;
        addView(this.mText1, lpt1);
        this.mText1.setAlpha(0.0f);
        this.mText2 = new TextView(context);
        this.mText2.setGravity(1);
        this.mText2.setText(C0532R.string.reminder_wear_not_tight_1);
        this.mText2.setTextColor(-1);
        this.mText2.setTextSize(15.0f);
        LayoutParams lpt2 = new LayoutParams(200, -2);
        lpt2.topMargin = 226;
        lpt2.gravity = 1;
        addView(this.mText2, lpt2);
        this.mText2.setAlpha(0.0f);
        reset();
        startAnim(true);
    }

    private ImageView addImageView(Context context, int resid) {
        ImageView view = new ImageView(context);
        view.setImageResource(resid);
        addView(view, new LayoutParams(-2, -2));
        view.setPivotX(0.0f);
        view.setPivotY(0.0f);
        return view;
    }

    private void reset() {
        this.mWatch1.setX(163.0f);
        this.mWatch1.setY(62.0f);
        this.mWatch2.setX(105.0f);
        this.mWatch2.setY(93.0f);
        this.mArrow1.setX(200.0f);
        this.mArrow1.setY(79.0f);
        this.mArrow1.setImageAlpha(0);
        this.mArrow2.setX(85.0f);
        this.mArrow2.setY(60.0f);
        this.mArrow2.setImageAlpha(0);
        this.mArrow3.setX(173.0f);
        this.mArrow3.setY(191.0f);
        this.mArrow3.setImageAlpha(0);
    }

    private void startAnim(boolean first) {
        this.mAnims = new AnimatorSet();
        if (!first) {
            this.mAnims.play(obtainAnim(this.mWatch2, "alpha", 255, 0, 375, 0, LINEAR));
            this.mAnims.play(obtainAnim(this.mText2, "alpha", 1.0f, 0.0f, 375, 0, LINEAR));
        }
        Animator animT1A1 = obtainAnim(this.mText1, "alpha", 0.0f, 1.0f, 375, 0, LINEAR);
        Animator animT1A2 = obtainAnim(this.mText1, "alpha", 1.0f, 0.0f, 375, 3625, LINEAR);
        Animator animW1A1 = obtainAnim(this.mWatch1, "alpha", 0, 255, 375, 0, LINEAR);
        Animator animW1A2 = obtainAnim(this.mWatch1, "alpha", 255, 0, 1, 3625, LINEAR);
        Animator animW1X = obtainAnim(this.mWatch1, "x", 163.0f, 105.0f, 625, 583, EASE);
        Animator animW1Y = obtainAnim(this.mWatch1, "y", 62.0f, 93.0f, 625, 583, EASE);
        Animator animW2A = obtainAnim(this.mWatch2, "alpha", 0, 255, 1, 3625, LINEAR);
        Animator animT2A = obtainAnim(this.mText2, "alpha", 0.0f, 1.0f, 375, 3625, LINEAR);
        ObjectAnimator animA1A1 = obtainAnim(this.mArrow1, "alpha", 0, 255, 125, 1000, LINEAR);
        ObjectAnimator animA1A2 = obtainAnim(this.mArrow1, "alpha", 255, 0, 375, 208, LINEAR);
        ObjectAnimator animA1X1 = obtainAnim(this.mArrow1, "x", 200.0f, 182.0f, 208, 1000, EASE);
        Animator animA1Y1 = obtainAnim(this.mArrow1, "y", 79.0f, 91.0f, 208, 1000, EASE);
        ObjectAnimator animA1X2 = obtainAnim(this.mArrow1, "x", 182.0f, 200.0f, 1166, 0, EASE);
        Animator animA1Y2 = obtainAnim(this.mArrow1, "y", 91.0f, 79.0f, 1166, 0, EASE);
        ObjectAnimator animA1A3 = obtainAnim(this.mArrow1, "alpha", 0, 255, 125, 2416, LINEAR);
        ObjectAnimator animA1A4 = obtainAnim(this.mArrow1, "alpha", 255, 0, 375, 208, LINEAR);
        ObjectAnimator animA1X3 = obtainAnim(this.mArrow1, "x", 200.0f, 182.0f, 208, 2416, EASE);
        Animator animA1Y3 = obtainAnim(this.mArrow1, "y", 79.0f, 91.0f, 208, 2416, EASE);
        Animator animA2A1 = obtainAnim(this.mArrow2, "alpha", 0, 255, 125, 3958, LINEAR);
        Animator animA2A2 = obtainAnim(this.mArrow2, "alpha", 255, 0, 375, 208, LINEAR);
        Animator animA2X1 = obtainAnim(this.mArrow2, "x", 85.0f, 99.0f, 250, 3958, EASE);
        Animator animA2Y1 = obtainAnim(this.mArrow2, "y", 60.0f, 76.0f, 250, 3958, EASE);
        Animator animA2X2 = obtainAnim(this.mArrow2, "x", 99.0f, 85.0f, 1208, 0, EASE);
        Animator animA2Y2 = obtainAnim(this.mArrow2, "y", 76.0f, 60.0f, 1208, 0, EASE);
        Animator animA2A3 = obtainAnim(this.mArrow2, "alpha", 0, 255, 125, 5416, LINEAR);
        Animator animA2A4 = obtainAnim(this.mArrow2, "alpha", 255, 0, 375, 208, LINEAR);
        Animator animA2X3 = obtainAnim(this.mArrow2, "x", 85.0f, 99.0f, 250, 5416, EASE);
        Animator animA2Y3 = obtainAnim(this.mArrow2, "y", 60.0f, 76.0f, 250, 5416, EASE);
        Animator animA3A1 = obtainAnim(this.mArrow3, "alpha", 0, 255, 125, 3958, LINEAR);
        Animator animA3A2 = obtainAnim(this.mArrow3, "alpha", 255, 0, 375, 208, LINEAR);
        Animator animA3X1 = obtainAnim(this.mArrow3, "x", 173.0f, 162.0f, 250, 3958, EASE);
        Animator animA3Y1 = obtainAnim(this.mArrow3, "y", 191.0f, 173.0f, 250, 3958, EASE);
        Animator animA3X2 = obtainAnim(this.mArrow3, "x", 162.0f, 173.0f, 1208, 0, EASE);
        Animator animA3Y2 = obtainAnim(this.mArrow3, "y", 173.0f, 191.0f, 1208, 0, EASE);
        Animator animA3A3 = obtainAnim(this.mArrow3, "alpha", 0, 255, 125, 5416, LINEAR);
        Animator animA3A4 = obtainAnim(this.mArrow3, "alpha", 255, 0, 375, 208, LINEAR);
        Animator animA3X3 = obtainAnim(this.mArrow3, "x", 173.0f, 162.0f, 250, 5416, EASE);
        Animator animA3Y3 = obtainAnim(this.mArrow3, "y", 191.0f, 173.0f, 250, 5416, EASE);
        this.mAnims.play(animW1A1);
        this.mAnims.play(animW1A2).after(animW1A1);
        this.mAnims.play(animW1X);
        this.mAnims.play(animW1Y);
        this.mAnims.play(animT1A1);
        this.mAnims.play(animT1A2).after(animT1A1);
        this.mAnims.play(animW2A).after(animW1A1);
        this.mAnims.play(animT2A).after(animW1A1);
        this.mAnims.play(animA1A1);
        this.mAnims.play(animA1A2).after(animA1A1);
        this.mAnims.play(animA1X1);
        this.mAnims.play(animA1Y1);
        this.mAnims.play(animA1X2).after(animA1X1);
        this.mAnims.play(animA1Y2).after(animA1Y1);
        this.mAnims.play(animA1A3);
        this.mAnims.play(animA1A4).after(animA1A3);
        this.mAnims.play(animA1X3);
        this.mAnims.play(animA1Y3);
        this.mAnims.play(animA2A1);
        this.mAnims.play(animA2A2).after(animA2A1);
        this.mAnims.play(animA2X1);
        this.mAnims.play(animA2Y1);
        this.mAnims.play(animA2X2).after(animA2X1);
        this.mAnims.play(animA2Y2).after(animA2Y1);
        this.mAnims.play(animA2A3);
        this.mAnims.play(animA2A4).after(animA2A3);
        this.mAnims.play(animA2X3);
        this.mAnims.play(animA2Y3);
        this.mAnims.play(animA3A1);
        this.mAnims.play(animA3A2).after(animA3A1);
        this.mAnims.play(animA3X1);
        this.mAnims.play(animA3Y1);
        this.mAnims.play(animA3X2).after(animA3X1);
        this.mAnims.play(animA3Y2).after(animA3Y1);
        this.mAnims.play(animA3A3);
        this.mAnims.play(animA3A4).after(animA3A3);
        this.mAnims.play(animA3X3);
        this.mAnims.play(animA3Y3);
        this.mAnims.start();
    }

    private ObjectAnimator obtainAnim(Object target, String propertyName, float startValue, float endValue, int duration, int startDelay, Interpolator interpolator) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(target, propertyName, new float[]{startValue, endValue}).setDuration((long) duration);
        anim.setStartDelay((long) startDelay);
        anim.setInterpolator(interpolator);
        return anim;
    }

    private ObjectAnimator obtainAnim(Object target, String propertyName, int startValue, int endValue, int duration, int startDelay, Interpolator interpolator) {
        ObjectAnimator anim = ObjectAnimator.ofInt(target, propertyName, new int[]{startValue, endValue}).setDuration((long) duration);
        anim.setStartDelay((long) startDelay);
        anim.setInterpolator(interpolator);
        return anim;
    }
}
