package com.huami.watch.newsport.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;

public class ExchangeView extends View {
    private int mBgType;
    private Context mContext;
    private Paint mExchangePaint;
    private Bitmap mExchangedBitmap;
    private boolean mIsReduceAnimCancled;
    private boolean mIsStartAnimCancled;
    private IExchangeViewAnim mListener;
    private Paint mPaint;
    private int mProgress;
    private ValueAnimator mReduceValueAnimator;
    private float mScale;
    private ValueAnimator mValueAnimator;

    public interface IExchangeViewAnim {
        void onExchangViewAnimationCancle();

        void onExchangViewAnimationEnd();
    }

    class C08671 implements AnimatorUpdateListener {
        C08671() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            ExchangeView.this.mProgress = ((Integer) animation.getAnimatedValue()).intValue();
            ExchangeView.this.postInvalidate();
        }
    }

    class C08682 extends AnimatorListenerAdapter {
        C08682() {
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            ExchangeView.this.mProgress = 0;
            ExchangeView.this.mIsStartAnimCancled = false;
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (!ExchangeView.this.mIsStartAnimCancled && ExchangeView.this.mListener != null) {
                ExchangeView.this.mListener.onExchangViewAnimationEnd();
            }
        }

        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            ExchangeView.this.mIsStartAnimCancled = true;
        }
    }

    class C08693 implements AnimatorUpdateListener {
        C08693() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            ExchangeView.this.mProgress = ((Integer) animation.getAnimatedValue()).intValue();
            ExchangeView.this.postInvalidate();
        }
    }

    class C08704 extends AnimatorListenerAdapter {
        C08704() {
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            ExchangeView.this.mIsReduceAnimCancled = false;
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (!ExchangeView.this.mIsReduceAnimCancled && ExchangeView.this.mListener != null) {
                ExchangeView.this.mListener.onExchangViewAnimationCancle();
            }
        }

        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            ExchangeView.this.mIsReduceAnimCancled = true;
        }
    }

    public ExchangeView(Context context) {
        this(context, null);
    }

    public ExchangeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExchangeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mScale = 0.0f;
        this.mProgress = 0;
        this.mExchangedBitmap = null;
        this.mBgType = 0;
        this.mIsStartAnimCancled = false;
        this.mIsReduceAnimCancled = false;
        this.mListener = null;
        init(context);
        this.mContext = context;
    }

    private void init(Context context) {
        this.mBgType = BackgroundUtils.getCustomBgType(context);
        this.mPaint = new Paint(1);
        this.mPaint.setStrokeWidth(8.0f);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(context.getResources().getColor(C0532R.color.exchange_big_cirlce));
        this.mExchangePaint = new Paint(1);
        this.mExchangePaint.setStyle(Style.STROKE);
        this.mExchangePaint.setStrokeWidth(4.0f);
        this.mExchangePaint.setColor(context.getResources().getColor(C0532R.color.exchange_small_cirlce));
        if (this.mBgType == 1) {
            this.mExchangedBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.bg_white_sport_fun_btn_next);
        } else {
            this.mExchangedBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_fun_btn_next);
        }
        this.mValueAnimator = ValueAnimator.ofInt(new int[]{0, 360});
        this.mValueAnimator.addUpdateListener(new C08671());
        this.mValueAnimator.setDuration(2000);
        this.mValueAnimator.addListener(new C08682());
    }

    public void startAnim() {
        if (this.mValueAnimator != null) {
            if (!this.mValueAnimator.isRunning()) {
                this.mValueAnimator.cancel();
            } else {
                return;
            }
        }
        if (this.mReduceValueAnimator != null) {
            this.mReduceValueAnimator.cancel();
        }
        this.mValueAnimator.start();
    }

    public boolean reduceAnim() {
        if ((this.mValueAnimator != null && !this.mValueAnimator.isRunning()) || (this.mReduceValueAnimator != null && this.mReduceValueAnimator.isRunning())) {
            return false;
        }
        if (this.mValueAnimator != null) {
            this.mValueAnimator.cancel();
        }
        this.mReduceValueAnimator = ValueAnimator.ofInt(new int[]{this.mProgress, 0});
        this.mReduceValueAnimator.addUpdateListener(new C08693());
        this.mReduceValueAnimator.setDuration((long) (2000.0f * (((float) this.mProgress) / 360.0f)));
        this.mReduceValueAnimator.addListener(new C08704());
        this.mReduceValueAnimator.start();
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int centerX = getHeight() / 2;
        int centerY = getWidth() / 2;
        canvas.drawBitmap(this.mExchangedBitmap, (float) (centerX - (this.mExchangedBitmap.getWidth() / 2)), (float) (centerY - (this.mExchangedBitmap.getHeight() / 2)), this.mPaint);
        canvas.drawCircle((float) centerX, (float) centerY, (float) ((this.mExchangedBitmap.getWidth() / 2) - 2), this.mExchangePaint);
        canvas.drawArc((float) ((centerX - (this.mExchangedBitmap.getWidth() / 2)) + 2), (float) ((centerY - (this.mExchangedBitmap.getHeight() / 2)) + 2), (float) (((this.mExchangedBitmap.getWidth() / 2) + centerX) - 2), (float) (((this.mExchangedBitmap.getHeight() / 2) + centerY) - 2), -90.0f, (float) this.mProgress, false, this.mPaint);
    }

    public void setAnimListener(IExchangeViewAnim listener) {
        this.mListener = listener;
    }
}
