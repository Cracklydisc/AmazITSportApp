package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.huami.watch.newsport.C0532R;
import java.util.List;

public class ProportionView extends FrameLayout {
    private List<ProportionViewData> mDatas;
    private float mDefaultTextSize;
    private int[] mHeights;
    private int mHorizontalGap;
    private int mMax;
    private int mMin;
    private int mVerticalGap;
    private int[] mWidths;

    public static class ProportionViewData {
        int color;
        String mLable;
        int value;

        public ProportionViewData(String label, int value, int color) {
            this.mLable = label;
            this.value = value;
            this.color = color;
        }
    }

    public ProportionView(Context context) {
        this(context, null);
    }

    public ProportionView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProportionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mMax = 0;
        this.mMin = Integer.MAX_VALUE;
        this.mVerticalGap = 0;
        this.mHorizontalGap = 0;
        this.mDefaultTextSize = 0.0f;
        this.mDefaultTextSize = context.getResources().getDimension(C0532R.dimen.proportion_title_size);
        Log.d("bug", "============= mDefaultTextSize " + this.mDefaultTextSize);
    }

    public void setVerticalGap(int verticalGap) {
        this.mVerticalGap = verticalGap;
    }

    public void setHorizontalGap(int horizontalGap) {
        this.mHorizontalGap = horizontalGap;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mDatas == null || this.mDatas.size() == 0) {
            setMeasuredDimension(0, 0);
            return;
        }
        int measueredWidth = MeasureSpec.getSize(widthMeasureSpec);
        int measueredHeight = MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int count = getChildCount();
        int childrenHeightMeasureSpec = heightMeasureSpec;
        int dataLength = this.mDatas.size();
        if (heightMode != 1073741824 || dataLength <= 0) {
            childrenHeightMeasureSpec = heightMeasureSpec;
        } else {
            int contentHeight = (int) ((((float) (((measueredHeight - getPaddingTop()) - getPaddingBottom()) - (this.mVerticalGap * (dataLength - 1)))) * 1.0f) / ((float) dataLength));
            if (contentHeight < 0) {
                contentHeight = 0;
            }
            childrenHeightMeasureSpec = MeasureSpec.makeMeasureSpec(contentHeight, 1073741824);
        }
        int[] widths = new int[3];
        int[] heights = new int[dataLength];
        int realParentWidth = ((measueredWidth - getPaddingLeft()) - getPaddingRight()) - (this.mHorizontalGap * 2);
        Log.d("custom", "count : " + count + "   dataLength " + dataLength + "  realParentWidth:  " + realParentWidth);
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            int dataColumn = i % 3;
            int dataRow = i / 3;
            Log.d("custom", "child index : " + i + "   child : " + child.toString() + " dataRow  :" + dataRow + "   dataColumn: " + dataColumn);
            if (dataColumn != 1) {
                measureChild(child, MeasureSpec.makeMeasureSpec(realParentWidth, Integer.MIN_VALUE), childrenHeightMeasureSpec);
                int childMeasuredWidth = child.getMeasuredWidth();
                int childMeasuredHeight = child.getMeasuredHeight();
                Log.d("custom", "childMeasuerdWidth :  " + childMeasuredWidth + "   childMeasuerHeight : " + childMeasuredHeight + "  heights[" + dataRow + "] : " + heights[dataRow]);
                if (heights[dataRow] > childMeasuredHeight) {
                    childMeasuredHeight = heights[dataRow];
                }
                heights[dataRow] = childMeasuredHeight;
                if (widths[dataColumn] > childMeasuredWidth) {
                    childMeasuredWidth = widths[dataColumn];
                }
                widths[dataColumn] = childMeasuredWidth;
            } else {
                widths[dataColumn] = 0;
            }
        }
        int hasMeasuerdWidth = 0;
        int maxHeight = 0;
        for (int i2 : widths) {
            hasMeasuerdWidth += i2;
        }
        int reminWidth = realParentWidth - hasMeasuerdWidth;
        Log.d("custom", "measueredWidth ： " + realParentWidth + "  hasMeasuerdWidth :  " + hasMeasuerdWidth + "  reminWidth :  " + reminWidth);
        widths[1] = reminWidth;
        for (int j = 0; j < dataLength; j++) {
            int realIndex = (j * 3) + 1;
            child = getChildAt(realIndex);
            int height = heights[realIndex / 3];
            float percent = 0.0f;
            if (this.mMax != 0) {
                percent = (((float) ((ProportionViewData) this.mDatas.get(j)).value) * 1.0f) / ((float) this.mMax);
            }
            int needWidth = ((int) (((float) (reminWidth - 1)) * percent)) + 1;
            int cwspec = MeasureSpec.makeMeasureSpec(needWidth, 1073741824);
            int chspecc = MeasureSpec.makeMeasureSpec(height, 1073741824);
            LayoutParams lp = (FrameLayout.LayoutParams) child.getLayoutParams();
            lp.width = needWidth;
            lp.height = height;
            child.setLayoutParams(lp);
            measureChild(child, cwspec, chspecc);
            Log.d("custom", "width : " + MeasureSpec.getSize(cwspec) + "  percent * reminWidth    " + (((float) reminWidth) * percent) + "   measured : " + child.getMeasuredWidth() + "   " + child.getMeasuredHeight() + "    " + needWidth);
        }
        for (int i22 : heights) {
            maxHeight += i22;
        }
        maxHeight = ((getPaddingTop() + maxHeight) + getPaddingBottom()) + (this.mVerticalGap * (heights.length - 1));
        this.mWidths = widths;
        this.mHeights = heights;
        setMeasuredDimension(measueredWidth, maxHeight);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int count = getChildCount();
        int parentLeft = getPaddingLeft();
        int parentRight = (right - left) - getPaddingRight();
        int parentBottom = (bottom - top) - getPaddingBottom();
        int totalXOffset = parentLeft;
        int totalYOffset = getPaddingTop();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) child.getLayoutParams();
            int width = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();
            Log.d("custom", "index : " + i + "  width:  " + width + "  height:  " + height);
            int dataColumn = i % 3;
            int dataRow = i / 3;
            int maxHeight = this.mHeights[dataRow];
            if (dataColumn == 0) {
                totalXOffset = (this.mWidths[dataColumn] + parentLeft) - width;
            } else {
                totalXOffset += this.mHorizontalGap;
            }
            if (dataRow > 0 && dataColumn == 0) {
                totalYOffset += this.mHeights[dataRow - 1] + this.mVerticalGap;
            }
            int childLeft = totalXOffset + lp.leftMargin;
            int childTop = ((lp.topMargin + totalYOffset) + (maxHeight / 2)) - (height / 2);
            child.layout(childLeft, childTop, childLeft + width, childTop + height);
            totalXOffset += width;
        }
    }

    public void setData(List<ProportionViewData> datas) {
        this.mDatas = datas;
        removeAllViews();
        addViewInner();
        requestLayout();
    }

    private void addViewInner() {
        if (this.mDatas != null) {
            for (ProportionViewData data : this.mDatas) {
                this.mMax = this.mMax > data.value ? this.mMax : data.value;
                this.mMin = this.mMin < data.value ? this.mMin : data.value;
                TextView textView = new TextView(getContext());
                textView.setText(data.mLable);
                textView.setTextSize(0, this.mDefaultTextSize);
                textView.setTextColor(getResources().getColor(C0532R.color.default_font_color_white));
                View view = new View(getContext());
                view.setBackgroundColor(data.color);
                TextView percentView = new TextView(getContext());
                percentView.setText(String.valueOf(data.value) + "%");
                percentView.setTextColor(getResources().getColor(C0532R.color.default_font_color_white));
                super.addView(textView);
                super.addView(view);
                super.addView(percentView);
            }
        }
    }

    protected boolean addViewInLayout(View child, int index, LayoutParams params) {
        throw new RuntimeException("not support addViewInLayout");
    }

    public void addView(View child, int width, int height) {
        throw new RuntimeException("not support addView");
    }

    public View getChildAt(int index) {
        if (this.mDatas == null || this.mDatas.size() == 0) {
            return null;
        }
        return super.getChildAt(index);
    }
}
