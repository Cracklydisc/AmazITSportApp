package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.newsport.C0532R;

public class HillView extends View {
    private Bitmap bgBitmap;
    private int mCenterCircleRadius;
    private Paint mCirclePaint;
    private Context mContext;
    private String[] mDefaultItemPercentStr;
    private String[] mDefaultItemStr;
    private String[] mDefaultItemTimeStr;
    private int mHillCenterColor;
    private int mHillDownColor;
    private int mHillEvenColor;
    private int mHillUpColor;
    private int mItemOffset;
    private int[] mPercent;
    private int mRadius;
    private int mRectLength;
    private Paint mTextPaint;
    private int mTextSize;

    public HillView(Context context) {
        this(context, null);
    }

    public HillView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HillView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mRadius = 100;
        this.mRectLength = 9;
        this.mHillCenterColor = -16777216;
        this.mHillEvenColor = -256;
        this.mHillUpColor = -65536;
        this.mHillDownColor = -16776961;
        this.mTextSize = 13;
        this.mPercent = new int[]{20, 20, 60};
        this.mCenterCircleRadius = 20;
        this.mItemOffset = 8;
        this.mContext = context;
        TypedArray ta = context.obtainStyledAttributes(attrs, C0532R.styleable.HillViewData);
        if (ta != null) {
            this.mRadius = ta.getDimensionPixelOffset(0, 120);
            this.mRectLength = ta.getDimensionPixelOffset(1, 20);
            this.mHillEvenColor = ta.getColor(2, this.mContext.getResources().getColor(C0532R.color.hill_even_color));
            this.mHillUpColor = ta.getColor(3, this.mContext.getResources().getColor(C0532R.color.hill_up_color));
            this.mHillDownColor = ta.getColor(4, this.mContext.getResources().getColor(C0532R.color.hill_down_color));
            this.mTextSize = ta.getDimensionPixelSize(5, 15);
            ta.recycle();
        }
        init();
    }

    private void init() {
        this.mCirclePaint = new Paint(1);
        this.mCirclePaint.setStyle(Style.FILL);
        this.mTextPaint = new Paint(1);
        this.mTextPaint.setColor(-1);
        this.mTextPaint.setTextSize((float) this.mTextSize);
        this.mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        this.mDefaultItemStr = new String[3];
        this.mDefaultItemStr[0] = this.mContext.getString(C0532R.string.hill_up);
        this.mDefaultItemStr[1] = this.mContext.getString(C0532R.string.hill_down);
        this.mDefaultItemStr[2] = this.mContext.getString(C0532R.string.hill_even);
        this.mDefaultItemTimeStr = new String[3];
        this.mDefaultItemTimeStr[0] = this.mContext.getString(C0532R.string.hill_default_time);
        this.mDefaultItemTimeStr[1] = this.mContext.getString(C0532R.string.hill_default_time);
        this.mDefaultItemTimeStr[2] = this.mContext.getString(C0532R.string.hill_default_time);
        this.mDefaultItemPercentStr = new String[3];
        this.mDefaultItemPercentStr[0] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(0)});
        this.mDefaultItemPercentStr[1] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(0)});
        this.mDefaultItemPercentStr[2] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(0)});
        this.bgBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_pie_chart_mask);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int left = getPaddingLeft();
        int right = getPaddingRight();
        int top = getPaddingTop();
        int width = (getWidth() - left) - right;
        int height = (getHeight() - top) - getPaddingBottom();
        int centerX = getWidth() / 2;
        int centerY = top + this.mRadius;
        int upAngle = (int) ((360.0f * ((float) this.mPercent[0])) / 100.0f);
        int downAngle = (int) ((360.0f * ((float) this.mPercent[1])) / 100.0f);
        int evenAngle = (360 - upAngle) - downAngle;
        this.mCirclePaint.setColor(this.mHillUpColor);
        canvas.drawArc((float) (centerX - this.mRadius), (float) (centerY - this.mRadius), (float) (this.mRadius + centerX), (float) (this.mRadius + centerY), -90.0f, (float) upAngle, true, this.mCirclePaint);
        this.mCirclePaint.setColor(this.mHillEvenColor);
        canvas.drawArc((float) (centerX - this.mRadius), (float) (centerY - this.mRadius), (float) (this.mRadius + centerX), (float) (this.mRadius + centerY), (float) (upAngle - 90), (float) evenAngle, true, this.mCirclePaint);
        if (evenAngle != 360) {
            this.mCirclePaint.setColor(-16777216);
            this.mCirclePaint.setStrokeWidth(2.0f);
            double degree = (((double) upAngle) * 3.141592653589793d) / 180.0d;
            canvas.drawLine((float) centerX, (float) centerY, (float) (((double) centerX) + (((double) this.mRadius) * Math.sin(degree))), (float) (((double) centerY) - (((double) this.mRadius) * Math.cos(degree))), this.mCirclePaint);
        }
        this.mCirclePaint.setColor(this.mHillDownColor);
        canvas.drawArc((float) (centerX - this.mRadius), (float) (centerY - this.mRadius), (float) (this.mRadius + centerX), (float) (this.mRadius + centerY), (float) ((upAngle - 90) + evenAngle), (float) downAngle, true, this.mCirclePaint);
        if (downAngle != 0) {
            this.mCirclePaint.setColor(-16777216);
            this.mCirclePaint.setStrokeWidth(2.0f);
            degree = (((double) (upAngle + evenAngle)) * 3.141592653589793d) / 180.0d;
            canvas.drawLine((float) centerX, (float) centerY, (float) (((double) centerX) + (((double) this.mRadius) * Math.sin(degree))), (float) (((double) centerY) - (((double) this.mRadius) * Math.cos(degree))), this.mCirclePaint);
        }
        if (upAngle != 0) {
            this.mCirclePaint.setColor(-16777216);
            this.mCirclePaint.setStrokeWidth(2.0f);
            canvas.drawLine((float) centerX, (float) centerY, (float) centerX, (float) (centerY - this.mRadius), this.mCirclePaint);
        }
        this.mCirclePaint.setColor(this.mHillCenterColor);
        canvas.drawCircle((float) centerX, (float) centerY, (float) this.mCenterCircleRadius, this.mCirclePaint);
        this.mCirclePaint.setColor(this.mHillDownColor);
        int textLeft = ((getWidth() / 2) - (((((getTextWidth(this.mDefaultItemStr[0]) + getTextWidth(this.mDefaultItemTimeStr[0])) + getTextWidth(this.mDefaultItemPercentStr[0])) + this.mRectLength) + (this.mItemOffset * 2)) / 2)) - 20;
        canvas.drawRect(new Rect(textLeft, height - this.mRectLength, this.mRectLength + textLeft, height), this.mCirclePaint);
        int firstLeft = (this.mRectLength + textLeft) + 5;
        canvas.drawText(this.mDefaultItemStr[1], (float) firstLeft, (float) getCenterY(height - (this.mRectLength / 2)), this.mTextPaint);
        int secondLeft = (getTextWidth(this.mDefaultItemStr[1]) + firstLeft) + this.mItemOffset;
        canvas.drawText(this.mDefaultItemTimeStr[1], (float) secondLeft, (float) getCenterY(height - (this.mRectLength / 2)), this.mTextPaint);
        int thirdLeft = (getTextWidth(this.mDefaultItemTimeStr[2]) + secondLeft) + this.mItemOffset;
        canvas.drawText(this.mDefaultItemPercentStr[1], (float) (left + thirdLeft), (float) getCenterY(height - (this.mRectLength / 2)), this.mTextPaint);
        this.mCirclePaint.setColor(this.mHillEvenColor);
        canvas.drawRect(new Rect(textLeft, (height - (this.mRectLength * 2)) - this.mItemOffset, this.mRectLength + textLeft, (height - this.mRectLength) - this.mItemOffset), this.mCirclePaint);
        canvas.drawText(this.mDefaultItemStr[2], (float) firstLeft, (float) getCenterY(((height - (this.mRectLength * 2)) - this.mItemOffset) + (this.mRectLength / 2)), this.mTextPaint);
        canvas.drawText(this.mDefaultItemTimeStr[2], (float) secondLeft, (float) getCenterY(((height - (this.mRectLength * 2)) - this.mItemOffset) + (this.mRectLength / 2)), this.mTextPaint);
        canvas.drawText(this.mDefaultItemPercentStr[2], (float) (left + thirdLeft), (float) getCenterY(((height - (this.mRectLength * 2)) - this.mItemOffset) + (this.mRectLength / 2)), this.mTextPaint);
        this.mCirclePaint.setColor(this.mHillUpColor);
        canvas.drawRect(new Rect(textLeft, (height - (this.mRectLength * 3)) - (this.mItemOffset * 2), this.mRectLength + textLeft, (height - (this.mRectLength * 2)) - (this.mItemOffset * 2)), this.mCirclePaint);
        canvas.drawText(this.mDefaultItemStr[0], (float) firstLeft, (float) getCenterY(((height - (this.mRectLength * 3)) - (this.mItemOffset * 2)) + (this.mRectLength / 2)), this.mTextPaint);
        canvas.drawText(this.mDefaultItemTimeStr[0], (float) secondLeft, (float) getCenterY(((height - (this.mRectLength * 3)) - (this.mItemOffset * 2)) + (this.mRectLength / 2)), this.mTextPaint);
        canvas.drawText(this.mDefaultItemPercentStr[0], (float) (left + thirdLeft), (float) getCenterY(((height - (this.mRectLength * 3)) - (this.mItemOffset * 2)) + (this.mRectLength / 2)), this.mTextPaint);
        canvas.drawBitmap(this.bgBitmap, (float) ((centerX - this.mRadius) + 5), (float) ((centerY - this.mRadius) + 5), this.mTextPaint);
    }

    public void setTimeCost(long even, long up, long down) {
        if (even < 0) {
            even = 0;
        }
        String upStr = DataFormatUtils.parseMillSecondToDefaultFormattedTime(up);
        String downStr = DataFormatUtils.parseMillSecondToDefaultFormattedTime(down);
        String evenStr = DataFormatUtils.parseMillSecondToDefaultFormattedTime(even);
        this.mDefaultItemTimeStr[0] = upStr;
        this.mDefaultItemTimeStr[1] = downStr;
        this.mDefaultItemTimeStr[2] = evenStr;
        long total = (even + up) + down;
        if (total == 0) {
            this.mPercent[0] = 0;
            this.mPercent[1] = 0;
            this.mPercent[2] = 100;
        } else {
            int upPer = (int) ((100 * up) / total);
            int downPer = (int) ((100 * down) / total);
            int evenPer = (100 - upPer) - downPer;
            this.mPercent[0] = upPer;
            this.mPercent[1] = downPer;
            this.mPercent[2] = evenPer;
        }
        this.mDefaultItemPercentStr[0] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(this.mPercent[0])});
        this.mDefaultItemPercentStr[1] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(this.mPercent[1])});
        this.mDefaultItemPercentStr[2] = this.mContext.getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(this.mPercent[2])});
        postInvalidate();
    }

    private int getTextWidth(String text) {
        return (int) this.mTextPaint.measureText(text);
    }

    private int getCenterY(int height) {
        FontMetrics fontMetrics = this.mTextPaint.getFontMetrics();
        return (int) ((((float) height) + ((fontMetrics.descent - fontMetrics.ascent) / 2.0f)) - fontMetrics.descent);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.bgBitmap != null && !this.bgBitmap.isRecycled()) {
            this.bgBitmap.recycle();
        }
    }
}
