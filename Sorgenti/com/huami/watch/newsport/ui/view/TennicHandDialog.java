package com.huami.watch.newsport.ui.view;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.HmKeyEventScrollView;
import com.huami.watch.common.widget.HmKeyEventScrollView.OnKeyCallBackListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;

public class TennicHandDialog extends Dialog {
    private DismissListener dismissListener;
    private KeyEventDispatcher keyEventDispatcher;

    public static class Builder implements OnKeyCallBackListener {
        private LinearLayout buttonNegativeButton;
        private LinearLayout buttonPositiveButton;
        private boolean isPositive;
        private OnClickListener negativeButtonClickListener;
        private OnClickListener positiveButtonClickListener;
        private HmKeyEventScrollView scrollView;

        class C08781 implements View.OnClickListener {
            final /* synthetic */ Builder this$0;
            final /* synthetic */ TennicHandDialog val$dialog;

            public void onClick(View v) {
                this.this$0.positiveButtonClickListener.onClick(this.val$dialog, -1);
            }
        }

        class C08792 implements View.OnClickListener {
            final /* synthetic */ Builder this$0;
            final /* synthetic */ TennicHandDialog val$dialog;

            public void onClick(View v) {
                this.this$0.negativeButtonClickListener.onClick(this.val$dialog, -1);
            }
        }

        public void onKeyCall(HMKeyEvent key) {
            if (key == HMKeyEvent.KEY_CENTER) {
                if (this.isPositive) {
                    if (this.buttonPositiveButton != null) {
                        this.buttonPositiveButton.performClick();
                        this.buttonPositiveButton.setPressed(true);
                    }
                } else if (this.buttonNegativeButton != null) {
                    this.buttonNegativeButton.performClick();
                    this.buttonNegativeButton.setPressed(true);
                }
            } else if (key == HMKeyEvent.KEY_DOWN) {
                if (this.scrollView != null && this.scrollView.isScrolledToBottom()) {
                }
            } else if (key != HMKeyEvent.KEY_UP || this.scrollView == null || !this.scrollView.isScrolledToTop()) {
            }
        }
    }

    public interface DismissListener {
        void dismiss();
    }

    public void dismiss() {
        super.dismiss();
        if (this.dismissListener != null) {
            this.dismissListener.dismiss();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        this.keyEventDispatcher.dispatchKeyevent((Dialog) this, event);
        return super.dispatchKeyEvent(event);
    }
}
