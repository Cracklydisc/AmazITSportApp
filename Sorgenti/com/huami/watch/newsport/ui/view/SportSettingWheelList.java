package com.huami.watch.newsport.ui.view;

import android.graphics.RectF;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.wheel.WheelVerticalLayout;
import clc.utils.venus.widgets.wheel.WheelView;
import clc.utils.venus.widgets.wheel.WheelViewItemsLayout;
import com.huami.watch.utils.LayoutUtils;

public class SportSettingWheelList extends WheelView {
    private int mItemHeight;

    public SportSettingWheelList(XViewWrapper context, RectF regionF) {
        this(context, regionF, 0);
    }

    public SportSettingWheelList(XViewWrapper context, RectF regionF, int itemH) {
        super(context);
        this.mItemHeight = 0;
        this.mItemHeight = itemH;
        resize(regionF);
        setAnchorForAnimListenerEnable(true);
    }

    protected WheelViewItemsLayout onCreateItemsLayout(XViewWrapper wrapper) {
        WheelVerticalLayout defaultLayout = new WheelVerticalLayout((WheelView) this, new RectF(0.0f, 0.0f, this.localRect.width(), this.localRect.height()), this.localRect.width(), (float) (this.mItemHeight > 0 ? this.mItemHeight : LayoutUtils.getItemHeight(this.mContext.getContext())));
        calculateGlobalTouchRect();
        return defaultLayout;
    }
}
