package com.huami.watch.newsport.ui.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class ColumGradientProgressBar extends View {
    private static final String TAG = ColumGradientProgressBar.class.getSimpleName();
    private int columnSize;
    private float mBottom;
    private Paint mFillPaint;
    private float mLeft;
    private Paint mLinePaint;
    private float mRight;
    private float mStepTop;
    private float mStepWidth;
    private Paint mTextFontBig;
    private Paint mTextFontSmall;
    private float mTop;
    private float progressLevelHalf;
    private int progressLevelInt;
    private String teValue;

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.progressLevelInt == 0) {
            drawRectProgress(canvas, this.mLeft, this.mTop, this.mRight, this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
            drawRectProgress(canvas, this.mLeft + ((this.mRight - this.mLeft) + this.mStepWidth), this.mTop - this.mStepTop, this.mRight + ((this.mRight - this.mLeft) + this.mStepWidth), this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
            drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * 2.0f), this.mTop - (this.mStepTop * 2.0f), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * 2.0f), this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
            drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * 3.0f), this.mTop - (this.mStepTop * 3.0f), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * 3.0f), this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
            drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * 4.0f), this.mTop - (this.mStepTop * 4.0f), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * 4.0f), this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
        } else {
            for (int i = 0; i < this.columnSize; i++) {
                if (i < this.progressLevelInt) {
                    drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mTop - (this.mStepTop * ((float) i)), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mBottom, this.mLinePaint, this.mFillPaint, 100.0f);
                } else if (i == this.progressLevelInt) {
                    drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mTop - (this.mStepTop * ((float) i)), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mBottom, this.mLinePaint, this.mFillPaint, this.progressLevelHalf);
                } else if (i > this.progressLevelInt) {
                    drawRectProgress(canvas, this.mLeft + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mTop - (this.mStepTop * ((float) i)), this.mRight + (((this.mRight - this.mLeft) + this.mStepWidth) * ((float) i)), this.mBottom, this.mLinePaint, this.mFillPaint, 0.0f);
                }
            }
        }
        canvas.drawText(this.teValue, this.mLeft, this.mBottom + 50.0f, this.mTextFontBig);
        canvas.drawText("/5", this.mLeft + 70.0f, this.mBottom + 50.0f, this.mTextFontSmall);
    }

    private void drawRectProgress(Canvas canvas, float left, float top, float right, float bottom, Paint mLinePaint, Paint mFillPaint, float percent) {
        canvas.drawRect(left, top, right, bottom, mLinePaint);
        if (percent > 0.0f) {
            canvas.drawRect(left, top, left + (((right - left) * percent) / 100.0f), bottom, mFillPaint);
        }
    }
}
