package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmViewPager;

public class SportViewPager extends HmViewPager {
    private boolean isDeliverEvent = false;
    private boolean isFirstMesure = true;
    private boolean mIsAutoPaused = false;
    private float mX = -1.0f;
    private float mY = -1.0f;

    public SportViewPager(Context context) {
        super(context);
    }

    public SportViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            Debug.m3d("HmSportViewPager", "catch a input exception");
            return false;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.mX = ev.getX();
                this.mY = ev.getY();
                this.isFirstMesure = true;
                this.isDeliverEvent = false;
                break;
            case 1:
                this.isFirstMesure = true;
                this.isDeliverEvent = false;
                this.mY = -1.0f;
                this.mX = -1.0f;
                break;
            case 2:
                if (this.isFirstMesure) {
                    if (Math.abs(ev.getX() - this.mX) > Math.abs(ev.getY() - this.mY)) {
                        this.isDeliverEvent = true;
                    }
                    this.isFirstMesure = false;
                    break;
                } else if (this.isDeliverEvent) {
                    return true;
                }
                break;
        }
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (Exception e) {
            Debug.m3d("HmSportViewPager", "catch a input exception");
            return false;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            return super.onTouchEvent(event);
        } catch (Exception e) {
            Debug.m3d("HmSportViewPager", "catch a input exception");
            return false;
        }
    }
}
