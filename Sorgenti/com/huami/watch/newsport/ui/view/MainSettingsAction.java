package com.huami.watch.newsport.ui.view;

import android.graphics.Matrix;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.IconDrawable;
import clc.utils.venus.widgets.common.XTextView;
import clc.utils.venus.widgets.senior.IconBoard;
import clc.utils.venus.widgets.wheel.WheelView.ScrollProcMonitor;
import com.huami.watch.newsport.C0532R;

public class MainSettingsAction extends BaseDrawableGroup implements ScrollProcMonitor {
    private boolean mClickable = true;
    private IconBoard mIconBoard;
    private IconDrawable mIconDrawable;
    private int mId = -1;
    private BaseDrawableGroup mInfoContainer;
    private XTextView mLabel;
    private float mScaleFactor = 1.0f;

    public MainSettingsAction(XViewWrapper context, RectF region, int labelId, int iconResId, int itemId) {
        super(context);
        resize(region);
        this.mId = itemId;
        doInit(labelId, iconResId);
        updateProc(false, this.mScaleFactor, -1);
        this.mIconBoard.setScaleFactor(1.0f - this.mScaleFactor);
    }

    private void doInit(int labelId, int iconResId) {
        this.mScaleFactor = Float.valueOf(this.mContext.getResources().getString(C0532R.string.widget_settings_item_high_light_scale_factor)).floatValue();
        this.mIconDrawable = new IconDrawable(this.mContext, ((BitmapDrawable) this.mContext.getResources().getDrawable(iconResId)).getBitmap());
        this.mIconDrawable.setEnableHaloFeature(true);
        this.mIconDrawable.setHolaScaleRange(0.9f, 1.38f, 1.38f);
        this.mIconBoard = new IconBoard(this.mContext, this.mIconDrawable);
        String text = (String) this.mContext.getResources().getText(labelId);
        RectF textRegion = new RectF(0.0f, 0.0f, 300.0f, this.localRect.height());
        this.mInfoContainer = new BaseDrawableGroup(this.mContext);
        this.mInfoContainer.resize(textRegion);
        this.mLabel = new XTextView(this.mContext, text, textRegion, Align.LEFT);
        setName(text);
        this.mLabel.setTextSize((float) this.mContext.getResources().getDimensionPixelSize(C0532R.dimen.sport_wiget_title_size));
        this.mLabel.setTextColor(-1);
        this.mIconBoard.setRelativeX((float) this.mContext.getResources().getDimensionPixelOffset(C0532R.dimen.widget_settings_icon_rx));
        this.mIconBoard.setRelativeY((getHeight() / 2.0f) - (this.mIconBoard.getHeight() / 2.0f));
        this.mInfoContainer.setRelativeX((float) this.mContext.getResources().getDimensionPixelOffset(C0532R.dimen.widget_settings_label_rx));
        this.mInfoContainer.setRelativeY((getHeight() / 2.0f) - (this.mInfoContainer.getHeight() / 2.0f));
        this.mInfoContainer.addItem(this.mLabel);
        addItem(this.mIconBoard);
        addItem(this.mInfoContainer);
    }

    public void updateProc(boolean focused, float proc, int index) {
        float f = 0.0f;
        this.mIconBoard.setProgress(focused, proc);
        float abs = Math.abs(proc);
        float scale = 1.0f - ((1.0f - this.mScaleFactor) * abs);
        Matrix m = this.mInfoContainer.getMatrix();
        float relativeX = this.mInfoContainer.getRelativeX();
        if (proc <= 0.0f) {
            f = this.mInfoContainer.getHeight();
        }
        m.setScale(scale, scale, relativeX, f);
        if (abs > 0.4f) {
            this.mLabel.setAlpha(0.64f);
        } else {
            this.mLabel.setAlpha((-0.90000004f * abs) + 1.0f);
        }
        this.mClickable = abs < 0.2f;
    }

    public void setText(String content) {
        if (this.mLabel != null) {
            this.mLabel.setText(content);
            this.mLabel.invalidate(true);
        }
    }

    public boolean isItemVisible() {
        return isVisible();
    }

    public int getId() {
        return this.mId;
    }

    public IconDrawable getIconDrawable() {
        return this.mIconDrawable;
    }

    public boolean isClickable() {
        return this.mClickable;
    }
}
