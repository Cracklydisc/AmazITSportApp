package com.huami.watch.newsport.ui.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class Vo2MaxLevelCircleView extends View {
    private static final String TAG = Vo2MaxLevelCircleView.class.getSimpleName();
    private Bitmap mBitmap;
    private float mBitmapLeft;
    private float mBitmapTop;
    private float mCanvasHeigth;
    private float mCanvasWidth;
    private float mMarginBottom;
    private float mMarginLeft;
    private float mMarginRight;
    private float mMarginTop;
    private Paint mPaint;
    private float smallTextSize;
    private int vo2MaxLevel;
    private int vo2MaxValue;
    private String vo2maxUnit;

    public void draw(Canvas canvas) {
        super.draw(canvas);
        initMeasureData();
        canvas.drawBitmap(this.mBitmap, this.mBitmapLeft, this.mBitmapTop, this.mPaint);
        drawVo2MaxValue(canvas);
    }

    private void drawVo2MaxUnit(Canvas canvas, Rect rect) {
        String vo2MaxUnitStr = String.valueOf(this.vo2maxUnit);
        this.mPaint.setTextAlign(Align.LEFT);
        this.mPaint.setTextSize(this.smallTextSize);
        this.mPaint.getTextBounds(vo2MaxUnitStr, 0, vo2MaxUnitStr.length(), rect);
        canvas.drawText(vo2MaxUnitStr, (this.mMarginLeft + (this.mCanvasWidth / 2.0f)) - ((float) (rect.width() / 2)), (this.mMarginTop + (this.mCanvasHeigth / 2.0f)) + 30.0f, this.mPaint);
    }

    private void drawVo2MaxValue(Canvas canvas) {
        Rect rect = new Rect();
        String vo2MaxStr = String.valueOf(this.vo2MaxValue);
        this.mPaint.getTextBounds(vo2MaxStr, 0, vo2MaxStr.length(), rect);
        this.mPaint.setTextAlign(Align.LEFT);
        canvas.drawText(vo2MaxStr, (this.mMarginLeft + (this.mCanvasWidth / 2.0f)) - ((float) (rect.width() / 2)), this.mMarginTop + (this.mCanvasHeigth / 2.0f), this.mPaint);
        drawVo2MaxUnit(canvas, rect);
    }

    private void initMeasureData() {
        this.mCanvasWidth = (((float) getWidth()) - this.mMarginLeft) - this.mMarginRight;
        this.mCanvasHeigth = (((float) getHeight()) - this.mMarginTop) - this.mMarginBottom;
        this.mBitmap = getBitmapByVo2maxLevel(this.vo2MaxLevel);
        this.mBitmapLeft = (this.mMarginLeft + (this.mCanvasWidth / 2.0f)) - ((float) (this.mBitmap.getWidth() / 2));
        this.mBitmapTop = (this.mMarginTop + (this.mCanvasHeigth / 2.0f)) - ((float) (this.mBitmap.getHeight() / 2));
    }

    private Bitmap getBitmapByVo2maxLevel(int level) {
        switch (level) {
            case 0:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_0)).getBitmap();
            case 1:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_1)).getBitmap();
            case 2:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_2)).getBitmap();
            case 3:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_3)).getBitmap();
            case 4:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_4)).getBitmap();
            case 5:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_5)).getBitmap();
            case 6:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_6)).getBitmap();
            case 7:
                return ((BitmapDrawable) getResources().getDrawable(C0532R.drawable.sport_result_vo2max_7)).getBitmap();
            default:
                return null;
        }
    }
}
