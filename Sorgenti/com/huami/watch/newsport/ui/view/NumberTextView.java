package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.huami.watch.common.widget.HmTextView;

public class NumberTextView extends HmTextView {
    static Typeface tf = null;

    public NumberTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NumberTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NumberTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (tf == null) {
            tf = Typeface.createFromAsset(getContext().getAssets(), "huamifont.ttf");
        }
        setTypeface(tf);
    }
}
