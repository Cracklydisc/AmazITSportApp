package com.huami.watch.newsport.ui.view;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.view.View;

public class SimpleDialogRemind extends Dialog {

    public static class Builder {
        private OnClickListener positiveButtonClickListener;

        class C08751 implements View.OnClickListener {
            final /* synthetic */ Builder this$0;
            final /* synthetic */ SimpleDialogRemind val$dialog;

            public void onClick(View v) {
                this.this$0.positiveButtonClickListener.onClick(this.val$dialog, -1);
            }
        }
    }
}
