package com.huami.watch.newsport.ui.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;

public class SportKeyEventListView extends ListView implements EventCallBack, KeyeventConsumer {
    private static final int OFFSET_Y = 2;
    private boolean canAccept = true;
    private boolean hasBottomView = true;
    private boolean hasFocused = true;
    private ItemFocusedListener mListener = null;
    private KeyeventProcessor processor;
    private TextView textView;
    private boolean withSelector = true;

    public interface ItemFocusedListener {
        void focusedItem(int i);

        void hasItemFocused(boolean z);

        void onItemClicked(int i);
    }

    class C08761 implements OnScrollListener {
        C08761() {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (absListView != null && i == 0) {
                try {
                    int focused = SportKeyEventListView.this.getCenterPos();
                    if (SportKeyEventListView.this.getPositionForView(SportKeyEventListView.this.getChildAt(focused)) >= SportKeyEventListView.this.getHeaderViewsCount()) {
                        SportKeyEventListView.this.mListener.hasItemFocused(true);
                    } else {
                        SportKeyEventListView.this.mListener.hasItemFocused(false);
                    }
                    if (focused >= SportKeyEventListView.this.getCount() - 1) {
                        focused = SportKeyEventListView.this.getCount() - 2;
                    }
                    if (focused <= 0) {
                        focused = 1;
                    }
                    SportKeyEventListView.this.mListener.focusedItem(focused - SportKeyEventListView.this.getHeaderViewsCount());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        }
    }

    public SportKeyEventListView(Context context) {
        super(context);
        init();
    }

    public SportKeyEventListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SportKeyEventListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!this.withSelector) {
        }
    }

    private int getCenterY() {
        int windowHeight = ((Activity) getContext()).getWindow().getDecorView().getHeight();
        int headViewHeight = 0;
        if (getHeaderViewsCount() > 0) {
            for (int i = 0; i < getHeaderViewsCount(); i++) {
                headViewHeight += getChildAt(i).getHeight();
            }
        }
        return headViewHeight + (((windowHeight - headViewHeight) / 2) / 2);
    }

    private int getCenterPos() {
        int count = getChildCount();
        int centerY = getCenterY();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            int top = view.getTop();
            int bottom = view.getBottom();
            if (centerY >= top && centerY <= bottom) {
                return i;
            }
        }
        return -1;
    }

    private void scrollToNext() {
        int scroll;
        int pos = getCenterPos();
        View view = getChildAt(pos);
        int centerY = view.getBottom() - (view.getHeight() / 2);
        if (centerY <= getCenterY() + 2) {
            scroll = view.getHeight() - (getCenterY() - centerY);
            int nextPos = pos + 1;
            if (nextPos >= getCount() - 1) {
                nextPos = getCount() - 2;
            }
            if (nextPos <= 0) {
                nextPos = 1;
            }
            this.mListener.focusedItem(nextPos - getHeaderViewsCount());
        } else {
            scroll = centerY - getCenterY();
        }
        smoothScrollBy(scroll, 600);
    }

    private void scrollToPrevious() {
        int scroll;
        int pos = getCenterPos();
        View view = getChildAt(pos);
        int centerY = view.getBottom() - (view.getHeight() / 2);
        if (centerY >= getCenterY() - 2) {
            scroll = view.getHeight() - (centerY - getCenterY());
            int prePos = pos - 1;
            if (prePos >= getCount() - 1) {
                prePos = getCount() - 2;
            }
            if (prePos <= 0) {
                prePos = 1;
            }
            this.mListener.focusedItem(prePos - getHeaderViewsCount());
        } else {
            scroll = getCenterY() - centerY;
        }
        smoothScrollBy(-scroll, 600);
    }

    public void injectKeyevent(KeyEvent event) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(event);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    private void addDefaultFooter() {
        if (this.textView == null) {
            this.textView = new TextView(getContext());
            this.textView.setHeight((int) TypedValue.applyDimension(0, 40.0f, getResources().getDisplayMetrics()));
        }
        removeFooterView(this.textView);
        addFooterView(this.textView);
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        try {
            if (theKey == HMKeyEvent.KEY_DOWN) {
                scrollToNext();
                return true;
            } else if (theKey == HMKeyEvent.KEY_UP) {
                scrollToPrevious();
                return true;
            } else {
                if (theKey == HMKeyEvent.KEY_CENTER) {
                    int centerPos = getCenterPos();
                    if (centerPos >= getCount() - 1) {
                        centerPos = getCount() - 2;
                    }
                    if (centerPos <= 0) {
                        centerPos = 1;
                    }
                    if (this.mListener != null) {
                        this.mListener.onItemClicked(centerPos - getHeaderViewsCount());
                    }
                }
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCanAccept(boolean canAccept) {
        this.canAccept = canAccept;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean isHasFocused() {
        return this.hasFocused;
    }

    public void setHasFocused(boolean hasFocused) {
        this.hasFocused = hasFocused;
    }

    public void setHasBottomView(boolean hasBottomView) {
        this.hasBottomView = hasBottomView;
        if (!hasBottomView && this.textView != null) {
            removeFooterView(this.textView);
        }
    }

    public void setAdapter(ListAdapter adapter) {
        if (this.hasBottomView) {
            addDefaultFooter();
        }
        super.setAdapter(adapter);
    }

    public boolean hasFocusedItems() {
        try {
            if (getPositionForView(getChildAt(getCenterPos())) >= getHeaderViewsCount()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setItemFocusedListener(ItemFocusedListener itemFocusedListener) {
        if (itemFocusedListener != null) {
            this.mListener = itemFocusedListener;
            setOnScrollListener(new C08761());
        }
    }
}
