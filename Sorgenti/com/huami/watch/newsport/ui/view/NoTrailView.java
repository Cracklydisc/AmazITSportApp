package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;

public class NoTrailView extends View {
    private float mAngle = 0.0f;
    private int mBgType = 0;
    private int mCenterX = 0;
    private int mCenterY = 0;
    private int mCircleBorderColor = 452984831;
    private Paint mCircleBorderPaint = null;
    private int mCircleColor = 218103807;
    private Paint mCirclePaint = null;
    private int mHeight = 0;
    private Matrix mMatrix = null;
    private int mRadius = 0;
    private Bitmap mStartBitmap = null;
    private Paint mTrailPaint = null;
    private int mWidth = 0;

    public NoTrailView(Context context) {
        super(context);
        init(context);
    }

    public NoTrailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NoTrailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public NoTrailView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        this.mBgType = BackgroundUtils.getCustomBgType(context);
        this.mTrailPaint = new Paint();
        this.mCirclePaint = new Paint();
        this.mCirclePaint.setColor(this.mCircleColor);
        this.mCircleBorderPaint = new Paint();
        this.mCircleBorderPaint.setStyle(Style.STROKE);
        this.mCircleBorderPaint.setStrokeWidth(1.0f);
        this.mCircleBorderPaint.setColor(this.mCircleBorderColor);
        this.mMatrix = new Matrix();
    }

    public void update(float angle, int radius, int alpha, int borderAlpha) {
        this.mAngle = angle;
        this.mRadius = radius;
        this.mCirclePaint.setAlpha(alpha);
        this.mCircleBorderPaint.setAlpha(borderAlpha);
        invalidate();
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mWidth = w;
        this.mCenterX = this.mWidth / 2;
        this.mHeight = h;
        this.mCenterY = this.mHeight / 2;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((float) this.mCenterX, (float) this.mCenterY, (float) this.mRadius, this.mCirclePaint);
        canvas.drawCircle((float) this.mCenterX, (float) this.mCenterY, (float) this.mRadius, this.mCircleBorderPaint);
        this.mMatrix.reset();
        this.mMatrix.postRotate(this.mAngle, (float) (this.mStartBitmap.getScaledWidth(canvas) / 2), (float) (this.mStartBitmap.getScaledHeight(canvas) / 2));
        this.mMatrix.postTranslate((float) (this.mCenterX - (this.mStartBitmap.getWidth() / 2)), (float) (this.mCenterY - (this.mStartBitmap.getHeight() / 2)));
        canvas.drawBitmap(this.mStartBitmap, this.mMatrix, this.mTrailPaint);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mBgType == 1) {
            this.mStartBitmap = BitmapFactory.decodeResource(getContext().getResources(), C0532R.drawable.bg_white_sport_map_location);
        } else {
            this.mStartBitmap = BitmapFactory.decodeResource(getContext().getResources(), C0532R.drawable.sport_map_fun_icon_location);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mStartBitmap.recycle();
    }
}
