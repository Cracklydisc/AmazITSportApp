package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class Vo2maxStatusView extends View {
    private static final String TAG = Vo2maxStatusView.class.getSimpleName();
    private final int SEGMENTS;
    private float[] angleList;
    private float bitmapCenterX;
    private float bitmapCenterY;
    private float centerX;
    private float centerY;
    private int[] colorArr;
    private float currentAngle;
    private int currentVo2max;
    private float currnetBitmapAngle;
    private int endVo2maxMax;
    private Paint mPaint;
    private float marginBottom;
    private float marginLeft;
    private float marginRigth;
    private float marginTop;
    private float radius;
    private Bitmap remindBitmap;
    private int separedRadis;
    private int startVo2maxMin;
    private int testVo2maxData;
    private int[] vo2maxLevelValues;

    public Vo2maxStatusView(Context context) {
        this(context, null);
    }

    public Vo2maxStatusView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Vo2maxStatusView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.marginLeft = 0.0f;
        this.marginRigth = 10.0f;
        this.marginTop = 10.0f;
        this.marginBottom = 10.0f;
        this.separedRadis = 1;
        this.SEGMENTS = 7;
        this.radius = 122.0f;
        this.startVo2maxMin = 15;
        this.endVo2maxMax = 65;
        this.currentAngle = 150.0f;
        this.angleList = new float[7];
        this.colorArr = new int[7];
        this.vo2maxLevelValues = new int[]{32, 37, 43, 50, 56, 62};
        this.centerY = 0.0f;
        this.bitmapCenterY = 0.0f;
        this.currnetBitmapAngle = 0.0f;
        this.currentVo2max = 0;
        this.testVo2maxData = 30;
        initPatin();
    }

    private void initPatin() {
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth(getResources().getDimension(C0532R.dimen.width_vo2_max_chart_view));
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeCap(Cap.BUTT);
        this.mPaint.setTextAlign(Align.CENTER);
        this.mPaint.setColor(-65536);
        this.remindBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_result_vo2max_p);
        this.radius = getResources().getDimension(C0532R.dimen.radius_vo2_max_status_view);
        this.colorArr[0] = getResources().getColor(C0532R.color.max_vo2_max_level_1);
        this.colorArr[1] = getResources().getColor(C0532R.color.max_vo2_max_level_2);
        this.colorArr[2] = getResources().getColor(C0532R.color.max_vo2_max_level_3);
        this.colorArr[3] = getResources().getColor(C0532R.color.max_vo2_max_level_4);
        this.colorArr[4] = getResources().getColor(C0532R.color.max_vo2_max_level_5);
        this.colorArr[5] = getResources().getColor(C0532R.color.max_vo2_max_level_6);
        this.colorArr[6] = getResources().getColor(C0532R.color.max_vo2_max_level_7);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Log.i(TAG, " getWidth:" + getWidth() + ",getHeigth:" + getHeight());
        initMeasureData();
        initAngle();
        initArcDraw(canvas);
        if (this.currentVo2max > 0) {
            initBitmap(canvas);
        }
    }

    private void initMeasureData() {
        this.centerX = (float) (getWidth() / 2);
        this.centerY = (float) (getHeight() / 2);
        this.startVo2maxMin = this.vo2maxLevelValues[0] - 5;
        this.endVo2maxMax = this.vo2maxLevelValues[this.vo2maxLevelValues.length - 1] + 5;
    }

    private void initBitmap(Canvas canvas) {
        Matrix matrix = new Matrix();
        Log.i(TAG, " currentBitmapAngle:" + this.currnetBitmapAngle);
        matrix.postTranslate((float) ((-this.remindBitmap.getWidth()) / 2), (float) ((-this.remindBitmap.getHeight()) / 2));
        matrix.postRotate(240.0f + this.currnetBitmapAngle);
        this.bitmapCenterX = this.centerX + ((float) (((double) this.radius) * Math.cos(Math.toRadians((double) (30.0f - this.currnetBitmapAngle)))));
        this.bitmapCenterY = this.centerY + ((float) (((double) this.radius) * Math.sin(Math.toRadians((double) (30.0f - this.currnetBitmapAngle)))));
        matrix.postTranslate(this.centerX, this.centerY);
        canvas.drawBitmap(this.remindBitmap, matrix, null);
    }

    private void initAngle() {
        int i;
        int totalAngle = 240 - (this.separedRadis * 6);
        for (i = 0; i < 6; i++) {
            if (i == 0) {
                this.angleList[i] = (((float) (this.vo2maxLevelValues[i] - this.startVo2maxMin)) / ((float) (this.endVo2maxMax - this.startVo2maxMin))) * ((float) totalAngle);
            } else {
                this.angleList[i] = (((float) (this.vo2maxLevelValues[i] - this.vo2maxLevelValues[i - 1])) / ((float) (this.endVo2maxMax - this.startVo2maxMin))) * ((float) totalAngle);
            }
        }
        this.angleList[6] = (((float) (this.endVo2maxMax - this.vo2maxLevelValues[5])) / ((float) (this.endVo2maxMax - this.startVo2maxMin))) * ((float) totalAngle);
        for (i = 0; i < 7; i++) {
            Log.i(TAG, " currenetAngle:" + this.angleList[i]);
        }
    }

    private void initArcDraw(Canvas canvas) {
        this.centerX = (float) (getWidth() / 2);
        this.centerY = (float) ((getHeight() / 2) + 10);
        Log.i(TAG, "centerX:" + this.centerX + ",centerY:" + this.centerY);
        RectF oval = new RectF(this.centerX - this.radius, this.centerY - this.radius, this.centerX + this.radius, this.centerY + this.radius);
        this.currentAngle = 150.0f;
        for (int i = 0; i < 7; i++) {
            if (i == 6) {
                this.mPaint.setColor(this.colorArr[i]);
                Log.i(TAG, " index:" + i + " ,curnetAngle:" + this.currentAngle + ",angle:" + this.angleList[i]);
                canvas.drawArc(oval, this.currentAngle, this.angleList[i], false, this.mPaint);
            } else {
                this.mPaint.setColor(this.colorArr[i]);
                Log.i(TAG, " index:" + i + " ,curnetAngle:" + this.currentAngle + ",angle:" + this.angleList[i]);
                canvas.drawArc(oval, this.currentAngle, Math.abs(this.angleList[i]), false, this.mPaint);
                this.currentAngle = (this.currentAngle + this.angleList[i]) + ((float) this.separedRadis);
            }
        }
    }

    public void setCurrentVo2maxValue(int vo2maxValue, int[] levelValues, int sportLevel) {
        if (levelValues != null && levelValues.length > 0) {
            printLevelValueStr(levelValues);
            this.vo2maxLevelValues = levelValues;
        }
        this.currentVo2max = vo2maxValue;
        if (this.currentVo2max >= this.endVo2maxMax) {
            this.currentVo2max = this.endVo2maxMax;
        }
        this.startVo2maxMin = levelValues[0] - 5;
        this.endVo2maxMax = levelValues[levelValues.length - 1] + 5;
        float vo2maxSepareRadis = (float) (240 / (this.endVo2maxMax - this.startVo2maxMin));
        printLevelValueStr(levelValues);
        if (this.currentVo2max >= this.endVo2maxMax) {
            this.currnetBitmapAngle = ((((float) (this.endVo2maxMax - this.startVo2maxMin)) * ((float) (240 - (this.separedRadis * 6)))) / ((float) (this.endVo2maxMax - this.startVo2maxMin))) + (((float) (sportLevel - 1)) * vo2maxSepareRadis);
        } else if (this.currentVo2max >= this.startVo2maxMin) {
            this.currnetBitmapAngle = ((((float) ((this.currentVo2max - (sportLevel - 1)) - this.startVo2maxMin)) * ((float) (240 - (this.separedRadis * 6)))) / ((float) (this.endVo2maxMax - this.startVo2maxMin))) + (((float) (sportLevel - 1)) * vo2maxSepareRadis);
        } else {
            this.currnetBitmapAngle = 0.0f;
        }
        postInvalidate();
    }

    private void printLevelValueStr(int[] levelValues) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i : levelValues) {
            sb.append(i + ",");
        }
        sb.append("]");
        Log.i(TAG, " printLevelValueStr:" + sb.toString());
    }
}
