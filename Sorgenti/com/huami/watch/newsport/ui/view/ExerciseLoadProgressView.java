package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;

public class ExerciseLoadProgressView extends View {
    private static final String TAG = ExerciseLoadProgressView.class.getSimpleName();
    private float bitmapTriangleRadius;
    private float circleHeigth;
    private float circleWidth;
    float circleX;
    float circleY;
    private float currnetPaintWidth;
    private int currnetRateAngle;
    private int intervalAngleValue;
    float mCanvasHeight;
    float mCanvasWidth;
    private Paint mFontPaint;
    private Paint mLinePaint;
    float mMarginBottom;
    float mMarginLeft;
    float mMarginRight;
    float mMarginTop;
    private int mNearDaysFontSize;
    private int mSuggessFontSize;
    private int mTrainLoadNumFontSize;
    private int maxValue;
    private int minValue;
    private String neardays;
    private int normalLineWidth;
    private int overReachValue;
    private int[] rotationAngleArray;
    private int selectedLineWidth;
    private String suggess;
    private Bitmap triangleBitmap;
    private int weeklyTrainLoadSum;
    float f23x;
    float f24y;

    public ExerciseLoadProgressView(Context context) {
        this(context, null);
    }

    public ExerciseLoadProgressView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExerciseLoadProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.normalLineWidth = 10;
        this.selectedLineWidth = 20;
        this.bitmapTriangleRadius = 200.0f;
        this.currnetRateAngle = 0;
        this.minValue = 150;
        this.maxValue = 300;
        this.overReachValue = 500;
        this.mNearDaysFontSize = 20;
        this.mTrainLoadNumFontSize = 50;
        this.mSuggessFontSize = 20;
        this.intervalAngleValue = 2;
        this.rotationAngleArray = new int[3];
        this.mMarginLeft = 10.0f;
        this.mMarginRight = 10.0f;
        this.mMarginTop = 10.0f;
        this.mMarginBottom = 10.0f;
        this.f23x = 0.0f;
        this.f24y = 0.0f;
        this.circleX = 0.0f;
        this.circleY = 0.0f;
        this.mCanvasHeight = 0.0f;
        this.mCanvasWidth = 0.0f;
        this.currnetPaintWidth = 0.0f;
        this.circleWidth = 0.0f;
        this.circleHeigth = 0.0f;
        initPaint();
    }

    private void initPaint() {
        this.mLinePaint = new Paint();
        this.mLinePaint.setAntiAlias(true);
        this.mLinePaint.setStrokeWidth((float) this.normalLineWidth);
        this.mLinePaint.setStyle(Style.STROKE);
        this.mLinePaint.setStrokeCap(Cap.BUTT);
        this.mLinePaint.setColor(-1);
        this.mFontPaint = new Paint();
        this.mFontPaint.setAntiAlias(true);
        this.mFontPaint.setStyle(Style.STROKE);
        this.mFontPaint.setColor(-1);
        this.mFontPaint.setTextSize(40.0f);
        this.triangleBitmap = BitmapFactory.decodeResource(getResources(), C0532R.drawable.rade_triangle);
        this.neardays = getResources().getString(C0532R.string.train_load_near_seven_days);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initMessageData();
        getComputerRateAngle();
        this.mLinePaint.setTextAlign(Align.LEFT);
        drawArcTreeColor(canvas);
        if (this.currnetRateAngle >= 0) {
            drawTriangleBitmap(canvas);
        }
    }

    private void initMessageData() {
        this.rotationAngleArray[0] = 180 / this.rotationAngleArray.length;
        this.rotationAngleArray[1] = (180 / this.rotationAngleArray.length) - (this.intervalAngleValue * 2);
        this.rotationAngleArray[2] = 180 / this.rotationAngleArray.length;
        this.mCanvasHeight = (float) (((getHeight() * 3) / 4) * 2);
        this.mCanvasWidth = (float) getWidth();
    }

    private void drawTriangleBitmap(Canvas canvas) {
        this.mLinePaint.setTextAlign(Align.CENTER);
        Matrix matrix = new Matrix();
        matrix.postRotate((float) (this.currnetRateAngle + 270));
        this.circleHeigth = (this.circleHeigth / 2.0f) - ((float) (this.triangleBitmap.getHeight() / 2));
        float r = (this.mCanvasHeight / 2.0f) - 7.0f;
        this.currnetRateAngle = this.currnetRateAngle >= 180 ? 180 : this.currnetRateAngle;
        Log.i(TAG, " currnetRateAngle:" + this.currnetRateAngle);
        if (this.currnetRateAngle == 0) {
            this.f24y = (this.mCanvasHeight / 2.0f) + ((float) (this.triangleBitmap.getWidth() / 2));
        } else if (this.currnetRateAngle > 0 && this.currnetRateAngle < 180) {
            this.f24y = (this.mCanvasHeight / 2.0f) - ((float) (((double) r) * Math.sin(Math.toRadians((double) this.currnetRateAngle))));
            if (this.f24y >= (this.mCanvasHeight / 2.0f) - ((float) (this.triangleBitmap.getWidth() / 2))) {
                this.f24y = (this.mCanvasHeight / 2.0f) - ((float) (this.triangleBitmap.getWidth() / 2));
            }
        } else if (this.currnetRateAngle >= 180) {
            this.f24y = (this.mCanvasHeight / 2.0f) - ((float) (this.triangleBitmap.getWidth() / 2));
        }
        this.f23x = (this.mCanvasWidth / 2.0f) - ((float) (((double) r) * Math.cos(Math.toRadians((double) this.currnetRateAngle))));
        Log.i(TAG, " draw triangle bitmap x :" + this.f23x + ",Y:" + this.f24y + ",r:" + r + ", currnetRateAngle:" + this.currnetRateAngle + ",MathSin:" + Math.sin(Math.toRadians((double) this.currnetRateAngle)));
        matrix.postTranslate(this.f23x, this.f24y);
        canvas.drawBitmap(this.triangleBitmap, matrix, this.mLinePaint);
    }

    private int getComputerRateAngle() {
        if (this.minValue == 0 || this.maxValue == 0 || this.maxValue - this.minValue == 0 || this.overReachValue - this.maxValue == 0) {
            return 0;
        }
        if (this.weeklyTrainLoadSum <= this.minValue) {
            this.currnetRateAngle = (this.rotationAngleArray[0] * this.weeklyTrainLoadSum) / this.minValue;
        } else if (this.weeklyTrainLoadSum <= this.maxValue) {
            this.currnetRateAngle = ((this.rotationAngleArray[0] * (this.weeklyTrainLoadSum - this.minValue)) / (this.maxValue - this.minValue)) + (this.rotationAngleArray[0] + this.intervalAngleValue);
        } else if (this.weeklyTrainLoadSum <= this.overReachValue) {
            this.currnetRateAngle = (((this.rotationAngleArray[0] + this.intervalAngleValue) + this.rotationAngleArray[1]) + this.intervalAngleValue) + ((this.rotationAngleArray[2] * (this.weeklyTrainLoadSum - this.maxValue)) / (this.overReachValue - this.maxValue));
        } else {
            this.currnetRateAngle = ((this.rotationAngleArray[0] + (this.intervalAngleValue * 2)) + this.rotationAngleArray[1]) + this.rotationAngleArray[2];
        }
        return this.currnetRateAngle;
    }

    private void drawArcTreeColor(Canvas canvas) {
        RectF oval = new RectF(this.mMarginLeft, this.mMarginTop, this.mCanvasWidth - this.mMarginRight, this.mCanvasHeight - this.mMarginBottom);
        this.circleWidth = oval.width();
        this.circleHeigth = oval.height();
        this.mLinePaint.setColor(-256);
        this.currnetPaintWidth = getLineWidthByAngle(this.currnetRateAngle, 0, 60);
        this.mLinePaint.setStrokeWidth(this.currnetPaintWidth);
        if (((float) this.normalLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(oval, -180.0f, (float) this.rotationAngleArray[0], false, this.mLinePaint);
        } else if (((float) this.selectedLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(new RectF(this.mMarginLeft + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), this.mMarginTop + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasWidth - this.mMarginRight) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasHeight - this.mMarginBottom) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f)), -180.0f, (float) this.rotationAngleArray[0], false, this.mLinePaint);
        }
        this.mLinePaint.setColor(-16711936);
        this.currnetPaintWidth = getLineWidthByAngle(this.currnetRateAngle, 60, 120);
        this.mLinePaint.setStrokeWidth(this.currnetPaintWidth);
        if (((float) this.normalLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(oval, (float) ((this.rotationAngleArray[0] - 180) + this.intervalAngleValue), (float) this.rotationAngleArray[1], false, this.mLinePaint);
        } else if (((float) this.selectedLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(new RectF(this.mMarginLeft + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), this.mMarginTop + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasWidth - this.mMarginRight) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasHeight - this.mMarginBottom) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f)), (float) ((this.rotationAngleArray[0] - 180) + this.intervalAngleValue), (float) this.rotationAngleArray[1], false, this.mLinePaint);
        }
        this.mLinePaint.setColor(-65536);
        this.currnetPaintWidth = getLineWidthByAngle(this.currnetRateAngle, 120, 180);
        this.mLinePaint.setStrokeWidth(this.currnetPaintWidth);
        if (((float) this.normalLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(oval, (float) (-this.rotationAngleArray[2]), (float) this.rotationAngleArray[2], false, this.mLinePaint);
        } else if (((float) this.selectedLineWidth) == this.currnetPaintWidth) {
            canvas.drawArc(new RectF(this.mMarginLeft + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), this.mMarginTop + ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasWidth - this.mMarginRight) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f), (this.mCanvasHeight - this.mMarginBottom) - ((this.currnetPaintWidth - ((float) this.normalLineWidth)) / 2.0f)), (float) (-this.rotationAngleArray[2]), (float) this.rotationAngleArray[2], false, this.mLinePaint);
        }
    }

    private float getLineWidthByAngle(int currnetRateAngle, int startAngle, int endAngle) {
        if (currnetRateAngle <= 0) {
            return (float) this.normalLineWidth;
        }
        if ((currnetRateAngle > endAngle || currnetRateAngle < startAngle) && (currnetRateAngle < 180 || endAngle != 180)) {
            return (float) this.normalLineWidth;
        }
        return (float) this.selectedLineWidth;
    }

    public void setTraingLoadProgress(int weeklyTrainLoadSum, int minValue, int maxValue, int overReachValue) {
        this.suggess = FirstBeatDataUtils.getSuggessTextByCurrentLoad(getContext(), weeklyTrainLoadSum, minValue, maxValue, overReachValue);
        this.weeklyTrainLoadSum = weeklyTrainLoadSum;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.overReachValue = overReachValue;
        postInvalidate();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.triangleBitmap != null && !this.triangleBitmap.isRecycled()) {
            this.triangleBitmap.recycle();
        }
    }
}
