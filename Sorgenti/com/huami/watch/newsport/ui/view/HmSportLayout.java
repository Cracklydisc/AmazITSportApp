package com.huami.watch.newsport.ui.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EdgeEffect;
import android.widget.FrameLayout;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import com.huami.watch.newsport.C0532R;

public class HmSportLayout extends FrameLayout implements EventCallBack, KeyeventConsumer {
    private static final String TAG = HmSportLayout.class.getName();
    private boolean DEBUG = true;
    private float alphaProgress;
    AnimatorListener animatorListener = new C08722();
    private View buttonNegativeButton;
    private View buttonPositiveButton;
    private boolean canAccept = true;
    private boolean isPositive = true;
    private int mActiveTouchId;
    private boolean mAnimationEndDismissed = false;
    private boolean mAnimationRunning = false;
    private ValueAnimator mAnimator;
    private OnSwipeProgressChangedListener mAppProgressListener;
    private Interpolator mCancelInterpolator;
    private boolean mDiscardIntercept;
    private Interpolator mDismissInterpolator;
    private Interpolator mDismissLinearInterpolator;
    private boolean mDismissed = false;
    private OnDismissedListener mDismissedListener;
    private float mDownX;
    private float mDownY;
    private EdgeEffect mEdgeEffect = null;
    private boolean mIsDrawingEdge = false;
    private float mLastDeltaXValue = 0.0f;
    private float mLastX;
    private float mLastY;
    private int mMaxFlingVelocity;
    private int mMinFlingVelocity;
    private int mSlop;
    private int mSwipeDirection = 0;
    private boolean mSwiping = false;
    private float mTranslationX;
    private VelocityTracker mVelocityTracker;
    private KeyeventProcessor processor;
    private int screenHeight;
    private int screenWidth;
    private ViewGroup scrolledView;
    AnimatorUpdateListener updateListener = new C08711();

    public interface OnSwipeProgressChangedListener {
        void onSwipeCancelled(HmSportLayout hmSportLayout);

        void onSwipeProgressChanged(HmSportLayout hmSportLayout, float f, float f2);
    }

    public interface OnDismissedListener {
        void onDismissed(HmSportLayout hmSportLayout);
    }

    class C08711 implements AnimatorUpdateListener {
        C08711() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float value = ((Float) animation.getAnimatedValue()).floatValue();
            if (HmSportLayout.this.mDismissed) {
                if (HmSportLayout.this.mSwipeDirection == 0) {
                    HmSportLayout.this.setProgress(HmSportLayout.this.mLastDeltaXValue + value);
                } else if (HmSportLayout.this.mSwipeDirection == 1) {
                    HmSportLayout.this.setProgress(HmSportLayout.this.mLastDeltaXValue - value);
                }
            } else if (HmSportLayout.this.mSwipeDirection == 0) {
                HmSportLayout.this.setProgress(HmSportLayout.this.mLastDeltaXValue - value);
            } else if (HmSportLayout.this.mSwipeDirection == 1) {
                HmSportLayout.this.setProgress(HmSportLayout.this.mLastDeltaXValue + value);
            }
        }
    }

    class C08722 implements AnimatorListener {
        C08722() {
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            if (HmSportLayout.this.mAnimationEndDismissed) {
                HmSportLayout.this.scrolledView.scrollTo(0, 0);
                HmSportLayout.this.dismiss();
            } else {
                HmSportLayout.this.cancel();
            }
            HmSportLayout.this.mAnimationEndDismissed = false;
            HmSportLayout.this.mAnimationRunning = false;
            HmSportLayout.this.resetMembers();
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    public HmSportLayout(Context context) {
        super(context);
        init(context);
    }

    public HmSportLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HmSportLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        ViewConfiguration vc = ViewConfiguration.get(getContext());
        this.mEdgeEffect = new EdgeEffect(context);
        this.mSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        this.mCancelInterpolator = new DecelerateInterpolator(1.5f);
        this.mDismissInterpolator = new AccelerateInterpolator(1.5f);
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRealMetrics(dm);
        this.screenHeight = dm.heightPixels;
        this.screenWidth = dm.widthPixels;
        initAnimation();
    }

    private void initAnimation() {
        this.mAnimator = new ValueAnimator();
        this.mAnimator.addUpdateListener(this.updateListener);
        this.mAnimator.addListener(this.animatorListener);
        this.mDismissLinearInterpolator = new LinearInterpolator();
        this.mAnimator.setInterpolator(this.mDismissLinearInterpolator);
        this.mAnimator.setDuration(400);
    }

    public void setOnDismissedListener(OnDismissedListener listener) {
        this.mDismissedListener = listener;
    }

    public void setAppOnSwipeProgressChangedListener(OnSwipeProgressChangedListener listener) {
        this.mAppProgressListener = listener;
    }

    private void dismiss() {
        if (this.mDismissedListener != null) {
            this.mDismissedListener.onDismissed(this);
        }
    }

    protected void cancel() {
        if (this.mAppProgressListener != null) {
            this.mAppProgressListener.onSwipeCancelled(this);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!this.mAnimationRunning) {
            ev.offsetLocation(this.mTranslationX, 0.0f);
            int pointerIndex;
            switch (ev.getActionMasked()) {
                case 0:
                    resetMembers();
                    this.mDownX = ev.getRawX();
                    this.mDownY = ev.getRawY();
                    this.mActiveTouchId = ev.getPointerId(0);
                    this.mVelocityTracker = VelocityTracker.obtain();
                    this.mVelocityTracker.addMovement(ev);
                    this.scrolledView = this;
                    if (getParent() != null) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    }
                    break;
                case 1:
                case 3:
                    this.mIsDrawingEdge = false;
                    this.mEdgeEffect.onRelease();
                    resetMembers();
                    break;
                case 2:
                    if (!(this.mVelocityTracker == null || this.mDiscardIntercept)) {
                        pointerIndex = ev.findPointerIndex(this.mActiveTouchId);
                        if (pointerIndex != -1) {
                            float dx = ev.getRawX() - this.mDownX;
                            float x = ev.getX(pointerIndex);
                            float y = ev.getY(pointerIndex);
                            if (dx != 0.0f && canScroll(this, false, dx, x, y)) {
                                this.mDiscardIntercept = true;
                                break;
                            }
                            updateSwiping(ev);
                            if (this.mIsDrawingEdge) {
                                return true;
                            }
                        }
                        Log.e(TAG, "Invalid pointer index: ignoring.");
                        this.mDiscardIntercept = true;
                        break;
                    }
                    break;
                case 5:
                    this.mActiveTouchId = ev.getPointerId(ev.getActionIndex());
                    break;
                case 6:
                    int actionIndex = ev.getActionIndex();
                    if (ev.getPointerId(actionIndex) == this.mActiveTouchId) {
                        if (actionIndex == 0) {
                            pointerIndex = 1;
                        } else {
                            pointerIndex = 0;
                        }
                        this.mActiveTouchId = ev.getPointerId(pointerIndex);
                        break;
                    }
                    break;
            }
            if (this.mDiscardIntercept || !this.mSwiping) {
                return false;
            }
            return true;
        } else if (getParent() == null) {
            return true;
        } else {
            getParent().requestDisallowInterceptTouchEvent(true);
            return true;
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mAnimationRunning) {
            return true;
        }
        if (this.mVelocityTracker == null) {
            return super.onTouchEvent(ev);
        }
        switch (ev.getActionMasked()) {
            case 1:
                if (!this.mIsDrawingEdge) {
                    updateDismiss(ev);
                    this.mLastDeltaXValue = ev.getRawX() - this.mDownX;
                    float endValue = 0.0f;
                    if (this.mDismissed) {
                        if (this.mSwipeDirection == 0) {
                            endValue = ((float) this.screenWidth) - this.mLastDeltaXValue;
                        } else if (this.mSwipeDirection == 1) {
                            endValue = ((float) this.screenWidth) + this.mLastDeltaXValue;
                        }
                    } else if (this.mSwiping) {
                        if (this.mSwipeDirection == 0) {
                            endValue = this.mLastDeltaXValue;
                        } else if (this.mSwipeDirection == 1) {
                            endValue = -this.mLastDeltaXValue;
                        }
                    }
                    this.mAnimator.setFloatValues(new float[]{0.0f, endValue});
                    this.mAnimator.start();
                    this.mAnimationRunning = true;
                }
                this.mEdgeEffect.onRelease();
                this.mIsDrawingEdge = false;
                return true;
            case 2:
                if (this.mIsDrawingEdge) {
                    performDrag(ev.getRawX());
                    return true;
                }
                this.mVelocityTracker.addMovement(ev);
                this.mLastX = ev.getRawX();
                updateSwiping(ev);
                if (!this.mSwiping) {
                    return true;
                }
                setProgress(ev.getRawX() - this.mDownX);
                return true;
            case 3:
                this.mIsDrawingEdge = false;
                this.mEdgeEffect.onRelease();
                resetMembers();
                return true;
            default:
                return true;
        }
    }

    private void performDrag(float x) {
        float deltaX = this.mLastX - x;
        this.mLastX = x;
        float scrollX = ((float) getScrollX()) + deltaX;
        int width = getClientWidth();
        if (scrollX > 0.0f) {
            this.mEdgeEffect.onPull(Math.abs(scrollX) / ((float) width));
        }
        postInvalidateOnAnimation();
    }

    private int getClientWidth() {
        return (getMeasuredWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private void updateDismiss(MotionEvent ev) {
        float deltaX = ev.getRawX() - this.mDownX;
        if (!this.mDismissed) {
            this.mVelocityTracker.addMovement(ev);
            this.mVelocityTracker.computeCurrentVelocity(1000);
            if (this.mSwipeDirection == 1) {
                if (deltaX < (-(((float) getWidth()) * 0.33f)) && ev.getRawX() <= this.mLastX) {
                    this.mDismissed = true;
                    this.mAnimationEndDismissed = true;
                }
            } else if (deltaX > ((float) getWidth()) * 0.33f && ev.getRawX() >= this.mLastX) {
                this.mDismissed = true;
                this.mAnimationEndDismissed = true;
            }
        }
        if (!this.mDismissed || !this.mSwiping) {
            return;
        }
        if (this.mSwipeDirection == 1) {
            if (deltaX > (-(((float) getWidth()) * 0.33f))) {
                this.mDismissed = false;
                this.mAnimationEndDismissed = false;
            }
        } else if (this.mSwipeDirection == 0 && deltaX < ((float) getWidth()) * 0.33f) {
            this.mDismissed = false;
            this.mAnimationEndDismissed = false;
        }
    }

    private void setProgress(float deltaX) {
        float f = 0.0f;
        this.mTranslationX = deltaX;
        if (this.mSwipeDirection == 1 && deltaX <= 0.0f) {
            if ((-deltaX) - 210.0f > 0.0f) {
                f = ((-deltaX) - 210.0f) / ((float) getWidth());
            }
            this.alphaProgress = f;
            this.scrolledView.scrollBy(deltaXToScrollInt(deltaX, this.scrolledView), 0);
        } else if (this.mSwipeDirection == 0 && deltaX >= 0.0f) {
            if (deltaX - 210.0f > 0.0f) {
                f = (deltaX - 210.0f) / ((float) getWidth());
            }
            this.alphaProgress = f;
            this.scrolledView.scrollBy(deltaXToScrollInt(deltaX, this.scrolledView), 0);
        }
        if (this.mAppProgressListener != null) {
            this.mAppProgressListener.onSwipeProgressChanged(this, this.alphaProgress, deltaX);
        }
    }

    private int deltaXToScrollInt(float deltX, ViewGroup vg) {
        return (-((int) deltX)) - vg.getScrollX();
    }

    private void updateSwiping(MotionEvent ev) {
        boolean z = true;
        if (!this.mSwiping) {
            float deltaX = ev.getRawX() - this.mDownX;
            float deltaY = ev.getRawY() - this.mDownY;
            if ((deltaX * deltaX) + (deltaY * deltaY) > ((float) (this.mSlop * this.mSlop))) {
                boolean z2;
                if (this.mSwipeDirection == 1) {
                    deltaX = -deltaX;
                }
                if (deltaX <= ((float) (this.mSlop * 2)) || Math.abs(deltaY) >= 60.0f || deltaX <= Math.abs(deltaY)) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                this.mSwiping = z2;
                if ((-deltaX) <= ((float) (this.mSlop * 2)) || Math.abs(deltaY) >= 60.0f || Math.abs(deltaX) <= Math.abs(deltaY)) {
                    z = false;
                }
                this.mIsDrawingEdge = z;
                return;
            }
            this.mSwiping = false;
        }
    }

    private void resetMembers() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
        }
        this.mVelocityTracker = null;
        this.mTranslationX = 0.0f;
        this.mDownX = 0.0f;
        this.mDownY = 0.0f;
        this.mSwiping = false;
        this.mDismissed = false;
        this.mDiscardIntercept = false;
        this.mAnimationEndDismissed = false;
    }

    protected boolean canScroll(View v, boolean checkV, float dx, float x, float y) {
        if (v instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) v;
            int scrollX = v.getScrollX();
            int scrollY = v.getScrollY();
            for (int i = group.getChildCount() - 1; i >= 0; i--) {
                View child = group.getChildAt(i);
                if (((float) scrollX) + x >= ((float) child.getLeft()) && ((float) scrollX) + x < ((float) child.getRight()) && ((float) scrollY) + y >= ((float) child.getTop()) && ((float) scrollY) + y < ((float) child.getBottom())) {
                    if (canScroll(child, true, dx, (((float) scrollX) + x) - ((float) child.getLeft()), (((float) scrollY) + y) - ((float) child.getTop()))) {
                        return true;
                    }
                }
            }
        }
        return checkV && v.canScrollHorizontally((int) (-dx));
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (!this.mEdgeEffect.isFinished()) {
            canvas.save();
            int width = getWidth();
            int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
            canvas.rotate(90.0f);
            canvas.translate((float) (-getPaddingTop()), (float) (-width));
            this.mEdgeEffect.setSize(height, width);
            this.mEdgeEffect.draw(canvas);
            postInvalidateOnAnimation();
            canvas.restore();
        }
    }

    public boolean onKeyClick(HMKeyEvent key) {
        if (key == HMKeyEvent.KEY_CENTER) {
            if (this.isPositive) {
                if (this.buttonPositiveButton != null) {
                    this.buttonPositiveButton.performClick();
                    this.buttonPositiveButton.setPressed(true);
                }
            } else if (this.buttonNegativeButton != null) {
                this.buttonNegativeButton.performClick();
                this.buttonNegativeButton.setPressed(true);
            }
        } else if (key == HMKeyEvent.KEY_DOWN) {
            changeBtnFocusable();
        } else if (key == HMKeyEvent.KEY_UP) {
            changeBtnFocusable();
        }
        return true;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public void injectKeyevent(KeyEvent keyEvent) {
        if (this.processor == null) {
            this.processor = new KeyeventProcessor(this);
        }
        this.processor.injectKeyEvent(keyEvent);
    }

    public boolean canAccept() {
        return this.canAccept;
    }

    private void changeBtnFocusable() {
        if (this.buttonPositiveButton != null && this.buttonNegativeButton != null) {
            if (this.isPositive) {
                this.buttonPositiveButton.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_negative);
                this.buttonNegativeButton.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_positive);
            } else {
                this.buttonPositiveButton.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_positive);
                this.buttonNegativeButton.setBackgroundResource(C0532R.drawable.system_dialog_button_bg_negative);
            }
            this.isPositive = !this.isPositive;
        }
    }

    public void setKeyView(View posView, View negaView) {
        this.buttonPositiveButton = posView;
        this.buttonNegativeButton = negaView;
    }
}
