package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import com.huami.watch.common.widget.XAxisChart;
import com.huami.watch.math3.analysis.interpolation.SplineInterpolator;
import com.huami.watch.newsport.C0532R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class NewSportChart<T extends Number> extends XAxisChart {
    private static final float[] BENCH_SECTION = new float[]{0.0f, 0.14f, 0.36f, 0.64f, 0.86f, 1.0f};
    private static final float[] HEART_BENCH_SECTION = new float[]{0.0f, 0.33f, 1.0f};
    private float height;
    private float mAveCoordinate;
    private T mAveValue;
    private float mAverageHundredTextPadding;
    private Paint mAveragePaint;
    private String mAverageText;
    private float mAverageTextPaddingLeft;
    private Paint mAverageTextPaint;
    private Shader mBenchGradient;
    private Paint mBenchmarkPaint;
    private Paint mClosePathPaint;
    private Context mContext;
    private int[] mCoverColors;
    private float[] mCoverRanges;
    private int[] mCurveColors;
    private float[] mCurveRanges;
    private boolean mDataProvided;
    private float mEveryPointValueHeight;
    private int mGreenColor;
    private AtomicBoolean mIsDataLoadFinished;
    private boolean mIsDrawMinOrMax;
    private boolean mIsShowAverageLine;
    private boolean mIsShowBenchZone;
    private boolean mIsShowDates;
    private boolean mIsShowDigitTop;
    private boolean mIsShownCoverColor;
    private boolean mIsShownLineNum;
    private boolean mIsShownLoss;
    private boolean mIshownMinOrMaxValue;
    private float mLineWidth;
    private ILineChartLoadingListener mLoadingListener;
    private float mMaxCoordinate;
    private T mMaxOrigin;
    private boolean mMaxPainted;
    private float mMaxRealDataCoordinate;
    private T mMaxRealOrigin;
    private String mMaxValueStr;
    private float mMinCoordinate;
    private boolean mMinMaxFound;
    private float mMinOrMaxMargin;
    private T mMinOrigin;
    private boolean mMinPainted;
    private float mMinRealDataCoordinate;
    private T mMinRealOrigin;
    private String mMinValueStr;
    private String mNoDataHint;
    private PointF mNoDataLocation;
    private Paint mNoDataPaint;
    private float mPointPaddingLeft;
    private int mRedColor;
    private boolean mShowEveryPoint;
    protected float mStaveLabelWidth;
    private float mVMinMaxHeight;
    private float mVMinOffset;
    private Paint mValueMinMaxPaint;
    private List<Path> mValueSetJointClosedPaths;
    private Paint mValueSetJointPaint;
    private List<Path> mValueSetJointPaths;
    private Paint mValueSetLineLowerPaint;
    private List<ValueSet> mValues;
    private int mYellowColor;
    private Bitmap masterBitmap;
    private float width;

    public interface ILineChartLoadingListener {
        void onLoadingFinished();
    }

    private class ValueSet implements Comparable<ValueSet> {
        private List<PointF> dots = new ArrayList();
        private PointF first;
        private PointF last;
        private List<PointF> maxDots = new ArrayList();
        private List<PointF> minDots = new ArrayList();
        private Path requiredClosedPath = new Path();
        private Path requiredPath = new Path();
        private List<T> values = new ArrayList();
        private List<Float> xIndices = new ArrayList();

        public ValueSet(float[] xs, T[] vals) {
            for (float x : xs) {
                this.xIndices.add(Float.valueOf(x));
            }
            for (T val : vals) {
                this.values.add(val);
            }
        }

        public float minCoordinate() {
            if (this.dots.size() < 1) {
                throw new IllegalStateException();
            }
            float min = Float.MAX_VALUE;
            for (PointF p : this.dots) {
                if (min > p.y) {
                    min = p.y;
                }
            }
            return min;
        }

        public float maxCoordinate() {
            if (this.dots.size() < 1) {
                throw new IllegalStateException();
            }
            float max = -3.4028235E38f;
            for (PointF p : this.dots) {
                if (max < p.y) {
                    max = p.y;
                }
            }
            return max;
        }

        public PointF first() {
            return this.first;
        }

        public PointF last() {
            return this.last;
        }

        public T minOrigin() {
            T min = (Number) this.values.get(0);
            for (int i = 1; i < this.values.size(); i++) {
                if (((Number) this.values.get(i)).doubleValue() < min.doubleValue()) {
                    min = (Number) this.values.get(i);
                }
            }
            return min;
        }

        public T maxOrigin() {
            T max = (Number) this.values.get(0);
            for (int i = 1; i < this.values.size(); i++) {
                if (((Number) this.values.get(i)).doubleValue() > max.doubleValue()) {
                    max = (Number) this.values.get(i);
                }
            }
            return max;
        }

        public void draw(Canvas canvas) {
            if (NewSportChart.this.mIsShownCoverColor) {
                canvas.drawPath(this.requiredClosedPath, NewSportChart.this.mClosePathPaint);
            }
            canvas.drawPath(this.requiredPath, NewSportChart.this.mValueSetLineLowerPaint);
            if (NewSportChart.this.mShowEveryPoint) {
                drawEveryPoint(canvas);
            } else {
                drawMinMax(canvas);
            }
        }

        private void drawEveryPoint(Canvas canvas) {
            int i = 0;
            for (PointF point : this.dots) {
                float numWicth = NewSportChart.this.mValueMinMaxPaint.measureText(String.valueOf(point.y));
                Bitmap minBitmap = BitmapFactory.decodeResource(NewSportChart.this.getResources(), C0532R.drawable.sport_heart_min);
                NewSportChart.this.mEveryTextPaint.setColor(NewSportChart.this.mContext.getResources().getColor(C0532R.color.black));
                canvas.drawCircle(point.x, point.y, 4.0f, NewSportChart.this.mEveryTextPaint);
                NewSportChart.this.mEveryTextPaint.setColor(NewSportChart.this.mContext.getResources().getColor(C0532R.color.white));
                canvas.drawCircle(point.x, point.y, 3.0f, NewSportChart.this.mEveryTextPaint);
                canvas.drawText(String.valueOf(((Number) this.values.get(i)).intValue()), point.x - (NewSportChart.this.mEveryTextPaint.measureText(String.valueOf(((Number) this.values.get(i)).intValue())) / 2.0f), (point.y - NewSportChart.this.mMinOrMaxMargin) - 2.0f, NewSportChart.this.mEveryTextPaint);
                Log.i("LineChart", "every:" + point.y + " local" + (point.x - (numWicth / 2.0f)));
                i++;
                minBitmap.recycle();
            }
        }

        private void drawMinMax(Canvas canvas) {
            if (NewSportChart.this.mIsDrawMinOrMax) {
                String max = String.valueOf(NewSportChart.this.mMaxRealOrigin.intValue());
                String min = String.valueOf(NewSportChart.this.mMinRealOrigin.intValue());
                float minWidth = NewSportChart.this.mValueMinMaxPaint.measureText(min);
                float maxWidth = NewSportChart.this.mValueMinMaxPaint.measureText(max);
                for (PointF point : this.minDots) {
                    if (!NewSportChart.this.mMinPainted) {
                        Bitmap minBitmap = BitmapFactory.decodeResource(NewSportChart.this.getResources(), C0532R.drawable.sport_result_chart_point);
                        canvas.drawBitmap(minBitmap, point.x - ((float) (minBitmap.getWidth() / 2)), (point.y - ((float) (minBitmap.getHeight() / 2))) - NewSportChart.this.mVMinMaxHeight, NewSportChart.this.mValueMinMaxPaint);
                        Log.i("LineChart", "min value:" + min);
                        NewSportChart.this.mMinPainted = true;
                        minBitmap.recycle();
                    }
                }
                for (PointF point2 : this.maxDots) {
                    if (!NewSportChart.this.mMaxPainted) {
                        Bitmap maxBitmap = BitmapFactory.decodeResource(NewSportChart.this.getResources(), C0532R.drawable.sport_result_chart_point);
                        canvas.drawBitmap(maxBitmap, point2.x - ((float) (maxBitmap.getWidth() / 2)), point2.y - ((float) (maxBitmap.getHeight() / 2)), NewSportChart.this.mValueMinMaxPaint);
                        Log.i("LineChart", "max value:" + max);
                        NewSportChart.this.mMaxPainted = true;
                        maxBitmap.recycle();
                    }
                }
            }
        }

        private void checkMinMax(float min, float max, PointF location) {
            if (NewSportChart.this.mMinRealDataCoordinate >= NewSportChart.this.mMaxRealDataCoordinate) {
                if (!NewSportChart.this.mMinMaxFound) {
                    this.maxDots.add(location);
                    NewSportChart.this.mMinMaxFound = true;
                }
            } else if (location.y <= NewSportChart.this.mMinRealDataCoordinate) {
                this.maxDots.add(location);
            } else if (location.y >= NewSportChart.this.mMaxRealDataCoordinate) {
                this.minDots.add(new PointF(location.x, location.y + NewSportChart.this.mVMinMaxHeight));
            }
        }

        public void calculatePartsOnScreen(float min, float max) {
            this.requiredPath.reset();
            this.requiredClosedPath.reset();
            int size = this.dots.size();
            PointF point;
            if (size == 1) {
                point = (PointF) this.dots.get(0);
                checkMinMax(min, max, point);
                this.requiredPath.addOval(point.x - 3.0f, point.y - 3.0f, point.x + 3.0f, point.y + 3.0f, Direction.CCW);
            } else if (size == 2) {
                checkMinMax(min, max, (PointF) this.dots.get(0));
                checkMinMax(min, max, (PointF) this.dots.get(1));
                this.requiredPath.addPath(NewSportChart.this.createPath((PointF) this.dots.get(0), (PointF) this.dots.get(1)));
                if (NewSportChart.this.mIsShownCoverColor) {
                    this.requiredClosedPath.addPath(NewSportChart.this.createClosedPath(new PointF(((PointF) this.dots.get(0)).x, ((float) NewSportChart.this.getPaddingTop()) + NewSportChart.this.mYMaxHeight), (PointF) this.dots.get(0)));
                    this.requiredClosedPath.addPath(NewSportChart.this.createClosedPath((PointF) this.dots.get(0), (PointF) this.dots.get(1)));
                    this.requiredClosedPath.addPath(NewSportChart.this.createClosedPath((PointF) this.dots.get(1), new PointF(((PointF) this.dots.get(1)).x, ((float) NewSportChart.this.getPaddingTop()) + NewSportChart.this.mYMaxHeight)));
                }
            } else if (size > 2) {
                PointF first = (PointF) this.dots.get(0);
                checkMinMax(min, max, first);
                this.requiredPath.moveTo(first.x, first.y);
                this.requiredClosedPath.moveTo(first.x, ((float) NewSportChart.this.getPaddingTop()) + NewSportChart.this.mYMaxHeight);
                this.requiredClosedPath.lineTo(first.x, first.y);
                for (int j = 1; j < this.dots.size(); j++) {
                    point = (PointF) this.dots.get(j);
                    checkMinMax(min, max, point);
                    this.requiredPath.lineTo(point.x, point.y);
                    this.requiredClosedPath.lineTo(point.x, point.y);
                    if (j == this.dots.size() - 1) {
                        this.requiredClosedPath.lineTo(point.x, ((float) NewSportChart.this.getPaddingTop()) + NewSportChart.this.mYMaxHeight);
                        this.requiredClosedPath.close();
                    }
                }
            }
        }

        public void calcDotsOnScreen(float left, float width, float top, float height, float offset, T min, T max, SplineInterpolator interpolator) {
            this.dots.clear();
            int size = this.values.size();
            if (size == 1) {
                this.dots.add(new PointF(left + ((((Float) this.xIndices.get(0)).floatValue() * width) / NewSportChart.this.mMaxXSize), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(0)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
            } else if (size == 2) {
                this.dots.add(new PointF(left + (((Float) this.xIndices.get(0)).floatValue() * (width / NewSportChart.this.mMaxXSize)), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(0)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
                this.dots.add(new PointF(left + (((Float) this.xIndices.get(1)).floatValue() * (width / NewSportChart.this.mMaxXSize)), min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((((Number) this.values.get(1)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue()))));
            } else if (size > 2) {
                double[] x = new double[size];
                double[] y = new double[size];
                for (int i = 0; i < size; i++) {
                    x[i] = (double) (((((Float) this.xIndices.get(i)).floatValue() * width) / NewSportChart.this.mMaxXSize) + left);
                    y[i] = min.floatValue() >= max.floatValue() ? (double) ((height / 2.0f) + top) : (double) (((top + height) - offset) - (((((Number) this.values.get(i)).floatValue() - min.floatValue()) * (height - (2.0f * offset))) / (max.floatValue() - min.floatValue())));
                    this.dots.add(new PointF((float) x[i], (float) y[i]));
                }
            }
        }

        public void optimizeDots(float top, float height, float offset) {
            float half = top + (height / 2.0f);
            float halfmax = (height / 2.0f) - offset;
            for (PointF dot : this.dots) {
                if (dot.y < half) {
                    if (dot.y == NewSportChart.this.mMinRealDataCoordinate) {
                        dot.y = half - (((half - dot.y) * halfmax) / (half - NewSportChart.this.mMaxCoordinate));
                        NewSportChart.this.mMinRealDataCoordinate = dot.y;
                    } else {
                        dot.y = half - (((half - dot.y) * halfmax) / (half - NewSportChart.this.mMaxCoordinate));
                    }
                } else if (dot.y > half) {
                    if (dot.y == NewSportChart.this.mMaxRealDataCoordinate) {
                        dot.y = (((dot.y - half) * halfmax) / (NewSportChart.this.mMinCoordinate - half)) + half;
                        NewSportChart.this.mMaxRealDataCoordinate = dot.y;
                    } else {
                        dot.y = (((dot.y - half) * halfmax) / (NewSportChart.this.mMinCoordinate - half)) + half;
                    }
                }
            }
            this.first = (PointF) this.dots.get(0);
            this.last = (PointF) this.dots.get(this.dots.size() - 1);
        }

        public int compareTo(ValueSet another) {
            if (((Float) this.xIndices.get(this.xIndices.size() - 1)).floatValue() < ((Float) another.xIndices.get(0)).floatValue()) {
                return -1;
            }
            return ((Float) this.xIndices.get(0)).floatValue() > ((Float) another.xIndices.get(another.xIndices.size() + -1)).floatValue() ? 1 : 0;
        }
    }

    public NewSportChart(Context context) {
        this(context, null);
    }

    public NewSportChart(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NewSportChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mVMinOffset = 0.0f;
        this.mVMinMaxHeight = 0.0f;
        this.mShowEveryPoint = false;
        this.mValues = new ArrayList(0);
        this.mValueSetJointPaths = new ArrayList();
        this.mValueSetJointClosedPaths = new ArrayList();
        this.mDataProvided = false;
        this.mBenchGradient = null;
        this.mIsShowAverageLine = false;
        this.mIsShowBenchZone = false;
        this.mIsShowDigitTop = false;
        this.mIsShowDates = false;
        this.mIsShownLoss = false;
        this.mContext = null;
        this.mStaveLabelWidth = 0.0f;
        this.mLineWidth = 0.0f;
        this.mIsDataLoadFinished = new AtomicBoolean(false);
        this.mLoadingListener = null;
        this.masterBitmap = null;
        this.mEveryPointValueHeight = 0.0f;
        this.mIsShownCoverColor = false;
        this.mCoverColors = null;
        this.mCoverRanges = null;
        this.mIsShownLineNum = false;
        this.mIsDrawMinOrMax = true;
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        Resources res = context.getResources();
        this.mVMinMaxHeight = res.getDimension(C0532R.dimen.size_linechart_minmax_font);
        float lineWidth = res.getDimension(C0532R.dimen.size_linechart_line_width);
        float averageLineWidth = res.getDimension(C0532R.dimen.average_line_width);
        this.mAverageTextPaddingLeft = res.getDimension(C0532R.dimen.average_text_padding_left);
        this.mAverageHundredTextPadding = res.getDimension(C0532R.dimen.average_hundred_text_padding_left);
        this.mMinOrMaxMargin = res.getDimension(C0532R.dimen.min_max_margin);
        this.masterBitmap = BitmapFactory.decodeResource(res, C0532R.drawable.sport_result_chart_default);
        float radius = res.getDimension(C0532R.dimen.size_shadow_radius1);
        float dy = res.getDimension(C0532R.dimen.size_shadow_dy1);
        float averageTextSize = res.getDimension(C0532R.dimen.average_text_size);
        int shadowColor = res.getColor(C0532R.color.color_shadow);
        int benchmarkColor = res.getColor(C0532R.color.color_chart_benchmark_line);
        int greenColor = res.getColor(C0532R.color.color_green);
        int redColor = res.getColor(C0532R.color.color_red);
        int grayColor = res.getColor(C0532R.color.average_line_color);
        int whiteColor = res.getColor(C0532R.color.average_text_color);
        this.mGreenColor = res.getColor(C0532R.color.bench_green_color);
        this.mYellowColor = res.getColor(C0532R.color.bench_yellow_color);
        this.mRedColor = res.getColor(C0532R.color.bench_red_color);
        PathEffect effect = new DashPathEffect(new float[]{8.0f, 8.0f}, 4.0f);
        PathEffect dashPathEffect = new DashPathEffect(new float[]{3.0f, 3.0f}, 0.0f);
        this.mValueSetLineLowerPaint = createLinePaint(lineWidth, greenColor, radius, dy, shadowColor, new CornerPathEffect(5.0f));
        this.mValueSetJointPaint = createLinePaint(lineWidth, greenColor, radius, dy, shadowColor, effect);
        this.mBenchmarkPaint = createLinePaint(lineWidth, benchmarkColor, radius, dy, shadowColor, effect);
        this.mAveragePaint = createLinePaint(averageLineWidth, grayColor, radius, dy, shadowColor, dashPathEffect);
        this.mClosePathPaint = new Paint(1);
        this.mClosePathPaint.setStyle(Style.FILL);
        this.mAverageTextPaint = new Paint(1);
        this.mAverageTextPaint.setTextSize(averageTextSize);
        this.mAverageTextPaint.setColor(whiteColor);
        this.mAverageTextPaint.setShadowLayer(radius, 0.0f, dy, shadowColor);
        this.mNoDataPaint = new Paint(1);
        this.mNoDataPaint.setTextSize(this.mVMinMaxHeight);
        this.mNoDataPaint.setColor(-1);
        this.mNoDataPaint.setShadowLayer(radius, 0.0f, dy, shadowColor);
        this.mValueMinMaxPaint = new Paint(this.mNoDataPaint);
        this.mValueMinMaxPaint.setTypeface(Typeface.DEFAULT_BOLD);
        setVMinOffset(res.getDimension(C0532R.dimen.sport_size_linechart_yoffset));
        this.mNoDataHint = res.getString(C0532R.string.no_chart_data_hint);
        this.mAverageText = res.getString(C0532R.string.average_text);
        this.mStaveLabelWidth = this.mXAxisLabelPaint.measureText("000");
        this.mLineWidth = res.getDimension(C0532R.dimen.size_linechart_width);
        this.mPointPaddingLeft = res.getDimension(C0532R.dimen.size_linechart_point_padding_left);
        this.mEveryPointValueHeight = res.getDimension(C0532R.dimen.pickview_unit_size);
        this.mEveryTextPaint = new Paint(1);
        this.mEveryTextPaint.setTextSize(this.mEveryPointValueHeight);
        this.mEveryTextPaint.setColor(res.getColor(C0532R.color.white));
    }

    protected void drawXAxis(Canvas canvas) {
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) getPaddingLeft(), this.mYMaxHeight + ((float) getPaddingTop()), (float) (getWidth() - getPaddingRight()), this.mYMaxHeight + ((float) getPaddingTop()), this.mXAxisPaint);
    }

    protected void drawYAxis(Canvas canvas) {
        if (this.mYLabels != null && this.mYLabelLocations != null && this.mYLineLocations != null) {
            for (int i = 0; i < this.mYLabels.length; i++) {
                if (i == this.mYLabels.length - 1) {
                    float value = convertToFloat(this.mYLabels[i], -1.0f);
                    if (value >= 0.0f && Float.compare(value, 0.0f) == 0) {
                    }
                }
                canvas.drawLine(this.mYLineLocations[i].x, this.mYLineLocations[i].y, this.mYLineLocations[i].x - this.mLineOffset, this.mYLineLocations[i].y, this.mGridPaint);
                canvas.drawText(this.mYLabels[i], this.mYLabelLocations[i].x, this.mYLabelLocations[i].y, this.mXAxisLabelPaint);
            }
        }
    }

    private float convertToFloat(String number, float defaultValue) {
        if (!TextUtils.isEmpty(number)) {
            try {
                defaultValue = Float.parseFloat(number);
            } catch (Exception e) {
            }
        }
        return defaultValue;
    }

    public void setIsMaxOrMinValue(boolean isSetMaxOrMin) {
        this.mIshownMinOrMaxValue = isSetMaxOrMin;
    }

    public void setMaxOrMinValue(T minOrigin, T maxOrigin) {
        this.mMinOrigin = minOrigin;
        this.mMaxOrigin = maxOrigin;
    }

    public void configLineGradient(int[] colors, float[] ranges) {
        this.mCurveColors = colors;
        this.mCurveRanges = ranges;
    }

    public void configInitLinechartBase(float curveStrokeWidth, int lineColor) {
        Resources res = this.mContext.getResources();
        float lineWidth = curveStrokeWidth;
        int greenColor = lineColor;
        this.mValueSetLineLowerPaint.setStrokeWidth(lineWidth);
        this.mValueSetLineLowerPaint.setColor(greenColor);
        this.mValueSetJointPaint.setStrokeWidth(lineWidth);
        this.mValueSetJointPaint.setColor(greenColor);
        this.mBenchmarkPaint.setStrokeWidth(lineWidth);
    }

    private Paint createLinePaint(float width, int color, float radius, float dy, int shadowColor, PathEffect effect) {
        Paint p = new Paint(1);
        p.setStrokeWidth(width);
        p.setStyle(Style.STROKE);
        p.setColor(color);
        p.setPathEffect(effect);
        p.setShadowLayer(radius, 0.0f, dy, shadowColor);
        return p;
    }

    public void configCurveCoverGradient(boolean isShowCover, int[] coverColors, float[] ranges) {
        this.mIsShownCoverColor = isShowCover;
        this.mCoverColors = coverColors;
        this.mCoverRanges = ranges;
    }

    public void setMaxOrMinDisplayStr(String max, String min) {
        this.mMaxValueStr = max;
        this.mMinValueStr = min;
    }

    public void setVMinOffset(float offset) {
        this.mVMinOffset = offset;
    }

    public void setIsShownAveLine(boolean isShownAveLine) {
        this.mIsShowAverageLine = isShownAveLine;
    }

    public void setXAxis(float offset, float textSize) {
        setXAxisTextSize(textSize);
        setxAxisOffset(offset);
    }

    public void setAveValue(T aveValue) {
        this.mAveValue = aveValue;
    }

    public void setShowDigitIsTop(boolean isTop) {
        this.mIsShowDigitTop = isTop;
    }

    public void setNoDataHint(String hint) {
    }

    public void setNoData() {
        this.mValues.clear();
        this.mDataProvided = true;
        resetParams();
        this.mIsDataLoadFinished.set(true);
    }

    public void addData(float[] xs, T[] yValues) {
        if (xs == null || yValues == null || xs.length != yValues.length) {
            throw new IllegalArgumentException("ill-formated data to linechart");
        }
        this.mValues.add(new ValueSet(xs, yValues));
        this.mDataProvided = true;
        resetParams();
        this.mIsDataLoadFinished.set(true);
    }

    public void setData(String[] xLabels, float xRange, boolean dontPaintLastXLabel) {
        super.setData(xLabels, xRange, dontPaintLastXLabel);
        resetParams();
    }

    public void setData(String[] xLabels, String[] yLabels, float xRange, boolean dontPaintLastXLabel) {
        super.setData(xLabels, yLabels, xRange, dontPaintLastXLabel);
        resetParams();
    }

    private void calMinOrMaxCoordinate(float top, float height, float offset, T min, T max) {
        this.mMaxCoordinate = min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : ((top + height) - offset) - (((this.mMaxOrigin.floatValue() - min.floatValue()) * (height - (offset * 2.0f))) / (max.floatValue() - min.floatValue()));
        this.mMinCoordinate = min.floatValue() >= max.floatValue() ? (height / 2.0f) + top : (top + height) - offset;
    }

    private void resetParams() {
        this.mParamsInitialized = false;
        this.mAnimate = false;
        invalidate();
    }

    private synchronized void calculateDrawingParams() {
        if (!this.mParamsInitialized) {
            float left = (float) getPaddingLeft();
            float top = (float) getPaddingTop();
            this.width = ((float) (getWidth() - getPaddingRight())) - left;
            this.height = 0.0f;
            if (this.mShowEveryPoint) {
                this.height = ((((float) (getHeight() - getPaddingBottom())) - top) - (this.mEveryPointValueHeight * 2.0f)) - this.mDateLineHeight;
            } else {
                this.height = ((float) (getHeight() - getPaddingBottom())) - top;
            }
            calculateXAxisParams(left, this.width, top, this.height);
            if (this.mValues.size() > 0) {
                int i;
                Collections.sort(this.mValues);
                this.mMinRealOrigin = ((ValueSet) this.mValues.get(0)).minOrigin();
                this.mMaxRealOrigin = ((ValueSet) this.mValues.get(0)).maxOrigin();
                for (ValueSet set : this.mValues) {
                    T min = set.minOrigin();
                    T max = set.maxOrigin();
                    if (this.mMinRealOrigin.doubleValue() > min.doubleValue()) {
                        this.mMinRealOrigin = min;
                    }
                    if (this.mMaxRealOrigin.doubleValue() < max.doubleValue()) {
                        this.mMaxRealOrigin = max;
                    }
                }
                if (!this.mIshownMinOrMaxValue) {
                    this.mMinOrigin = this.mMinRealOrigin;
                    this.mMaxOrigin = this.mMaxRealOrigin;
                }
                SplineInterpolator interpolator = new SplineInterpolator();
                for (ValueSet set2 : this.mValues) {
                    set2.calcDotsOnScreen(left, this.width, top, this.mYMaxHeight, this.mVMinOffset, this.mMinOrigin, this.mMaxOrigin, interpolator);
                }
                int count = 0;
                for (i = 0; i < this.mValues.size(); i++) {
                    count += ((ValueSet) this.mValues.get(i)).dots.size();
                }
                if (count <= 1) {
                    this.mIsShowAverageLine = false;
                }
                this.mMinRealDataCoordinate = Float.MAX_VALUE;
                this.mMaxRealDataCoordinate = -3.4028235E38f;
                for (ValueSet set22 : this.mValues) {
                    float fmin = set22.minCoordinate();
                    float fmax = set22.maxCoordinate();
                    if (this.mMinRealDataCoordinate > fmin) {
                        this.mMinRealDataCoordinate = fmin;
                    }
                    if (this.mMaxRealDataCoordinate < fmax) {
                        this.mMaxRealDataCoordinate = fmax;
                    }
                }
                if (this.mIshownMinOrMaxValue) {
                    calMinOrMaxCoordinate(top, this.mYMaxHeight, this.mVMinOffset, this.mMinOrigin, this.mMaxOrigin);
                } else {
                    this.mMinCoordinate = this.mMinRealDataCoordinate;
                    this.mMaxCoordinate = this.mMaxRealDataCoordinate;
                }
                for (ValueSet set222 : this.mValues) {
                    set222.optimizeDots(top, this.mYMaxHeight, this.mVMinOffset);
                }
                calculateYAxis(left, (double) this.mMaxCoordinate, (double) this.mMinCoordinate);
                calculateAveLine();
                this.mMinMaxFound = false;
                for (ValueSet set2222 : this.mValues) {
                    set2222.calculatePartsOnScreen(this.mMinCoordinate, this.mMaxCoordinate);
                }
                this.mValueSetJointPaths.clear();
                this.mValueSetJointClosedPaths.clear();
                if (this.mIsShownLoss) {
                    ValueSet last = (ValueSet) this.mValues.get(0);
                    for (i = 1; i < this.mValues.size(); i++) {
                        ValueSet current = (ValueSet) this.mValues.get(i);
                        this.mValueSetJointPaths.add(createPath(last.last(), current.first()));
                        this.mValueSetJointClosedPaths.add(createPath(last.last(), current.first()));
                        last = current;
                    }
                }
            } else {
                this.mNoDataLocation = new PointF((((float) getWidth()) - this.mValueMinMaxPaint.measureText(this.mNoDataHint)) / 2.0f, ((this.height - this.mVMinMaxHeight) / 2.0f) + top);
            }
            if (this.mAnimate) {
                this.x = 0.0f;
                this.y = 0.0f;
            } else {
                this.x = this.mMaxXSize;
                this.y = 1.0f;
            }
            this.mParamsInitialized = true;
        }
    }

    private void calculateAveLine() {
        if (!this.mIsShowAverageLine || this.mMinOrigin == null || this.mMaxOrigin == null || this.mAveValue == null) {
            Log.i("LineChart", "calculateBenchmark, mMinOrigin: " + this.mMinOrigin + ", mMaxOrigin: " + this.mMaxOrigin);
            return;
        }
        float radio;
        float aveValue = this.mAveValue.floatValue();
        float minHeart = this.mMinRealOrigin.floatValue();
        float maxHeart = this.mMaxRealOrigin.floatValue();
        if (minHeart == maxHeart) {
            radio = this.mMaxRealDataCoordinate / minHeart;
        } else {
            radio = (this.mMaxRealDataCoordinate - this.mMinRealDataCoordinate) / (maxHeart - minHeart);
        }
        this.mAveCoordinate = this.mMaxRealDataCoordinate + ((minHeart - aveValue) * radio);
    }

    private Shader createBenchGradient() {
        if (this.mBenchGradient == null) {
            this.mBenchGradient = new LinearGradient(0.0f, this.mMinCoordinate, 0.0f, this.mMaxCoordinate, this.mCurveColors == null ? new int[]{this.mGreenColor, this.mYellowColor, this.mRedColor, this.mRedColor, this.mYellowColor, this.mGreenColor} : this.mCurveColors, this.mCurveColors == null ? BENCH_SECTION : this.mCurveRanges, TileMode.CLAMP);
        }
        return this.mBenchGradient;
    }

    public void onDraw(Canvas canvas) {
        calculateDrawingParams();
        super.onDraw(canvas);
        if (this.mCoverColors != null && this.mIsShownCoverColor) {
            this.mClosePathPaint.setShader(new LinearGradient(0.0f, this.mYMaxHeight, 0.0f, 0.0f, this.mCoverColors, this.mCoverRanges, TileMode.CLAMP));
        }
        drawYValues(canvas);
        drawAveragemark(canvas);
        if (this.mIsDataLoadFinished.get()) {
            Log.i("LineChart", "linchart onDraw:" + this.mIsDataLoadFinished + ", finished:" + this.mIsDataLoadFinished.get());
            if (this.mLoadingListener != null) {
                this.mLoadingListener.onLoadingFinished();
            }
            this.mIsDataLoadFinished.set(false);
        }
    }

    private void drawAveragemark(Canvas canvas) {
        if (this.mMinOrigin == null || this.mMaxOrigin == null || !this.mIsShowAverageLine || this.mAveValue == null) {
            Log.i("LineChart", "mIsShowBenchZone: " + this.mIsShowBenchZone + ", mMinOrigin: " + this.mMinOrigin + ", mMaxOrigin: " + this.mMaxOrigin + ", mIsShowAverageLine: " + this.mIsShowAverageLine + ", mAveValue: " + this.mAveValue);
            return;
        }
        Path path = new Path();
        StringBuffer str = new StringBuffer().append(this.mAverageText).append(this.mAveValue.intValue());
        path.moveTo((float) getPaddingLeft(), this.mAveCoordinate);
        path.lineTo((float) (getWidth() - getPaddingRight()), this.mAveCoordinate);
        canvas.drawPath(path, this.mAveragePaint);
    }

    private void drawYValues(Canvas canvas) {
        if (this.mValues.size() != 0) {
            this.mValueSetLineLowerPaint.setShader(createBenchGradient());
            this.mValueSetJointPaint.setShader(createBenchGradient());
            this.mMaxPainted = false;
            this.mMinPainted = false;
            for (ValueSet set : this.mValues) {
                set.draw(canvas);
            }
            if (this.mIsShownLoss) {
                for (Path path : this.mValueSetJointPaths) {
                    canvas.drawPath(path, this.mValueSetJointPaint);
                }
            }
        } else if (this.mDataProvided) {
            if (!(this.masterBitmap == null || this.masterBitmap.isRecycled())) {
                canvas.drawBitmap(this.masterBitmap, (float) getPaddingLeft(), (float) (getPaddingTop() + 10), this.mNoDataPaint);
            }
            canvas.drawText(this.mNoDataHint, this.mNoDataLocation.x, this.mNoDataLocation.y, this.mNoDataPaint);
        }
    }

    private Path createPath(float startx, float starty, float endx, float endy) {
        Path path = new Path();
        path.moveTo(startx, starty);
        path.lineTo(endx, endy);
        return path;
    }

    private Path createPath(PointF start, PointF end) {
        return createPath(start.x, start.y, end.x, end.y);
    }

    private Path createClosedPath(float startx, float starty, float endx, float endy) {
        Path path = new Path();
        path.moveTo(startx, starty);
        path.lineTo(endx, endy);
        if (endy < starty) {
            path.lineTo(endx, starty);
        } else {
            path.lineTo(startx, endy);
        }
        path.close();
        return path;
    }

    private Path createClosedPath(PointF start, PointF end) {
        return createClosedPath(start.x, start.y, end.x, end.y);
    }

    public void setLineChartListener(ILineChartLoadingListener lineChartListener) {
        this.mLoadingListener = lineChartListener;
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.masterBitmap != null && !this.masterBitmap.isRecycled()) {
            this.masterBitmap.recycle();
        }
    }
}
