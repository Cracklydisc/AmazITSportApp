package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class TrainingEffectView extends View {
    private float biggerTxtFontSize;
    private float bottom;
    private float currnetLevel;
    private int drawColorResource;
    private float left;
    private float levelFloat;
    private int levelInt;
    private float mBlackWidth;
    private float mCanvasHeigth;
    private float mCanvasWidth;
    private float mLineWidth;
    private float mMarginLeft;
    private float mMarginRight;
    private float mMarginTop;
    private Paint mPaint;
    private float mProgressLevelHeigth;
    private float mReactWidthStep;
    private Paint mTxtPaint;
    private float normalTxtFontSize;
    private float right;
    private int segments;
    private float top;

    public TrainingEffectView(Context context) {
        this(context, null);
    }

    public TrainingEffectView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TrainingEffectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.segments = 5;
        this.mMarginLeft = 10.0f;
        this.mMarginTop = 10.0f;
        this.mMarginRight = 10.0f;
        this.mCanvasWidth = 0.0f;
        this.mCanvasHeigth = 0.0f;
        this.mLineWidth = 5.0f;
        this.mBlackWidth = 2.0f;
        this.mReactWidthStep = 0.0f;
        this.normalTxtFontSize = 25.0f;
        this.biggerTxtFontSize = 50.0f;
        this.mProgressLevelHeigth = 15.0f;
        this.levelInt = 0;
        this.levelFloat = 0.0f;
        this.left = 0.0f;
        this.top = 0.0f;
        this.right = 0.0f;
        this.bottom = 0.0f;
        initPaint();
    }

    private void initPaint() {
        this.drawColorResource = getResources().getColor(C0532R.color.te_color_step_01);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth(this.mLineWidth);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setStrokeCap(Cap.BUTT);
        this.mPaint.setColor(-7829368);
        this.mTxtPaint = new Paint();
        this.mTxtPaint.setAntiAlias(true);
        this.mTxtPaint.setColor(-1);
        this.mTxtPaint.setStyle(Style.STROKE);
        this.mTxtPaint.setTextSize(this.biggerTxtFontSize);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.mCanvasWidth = (((float) getWidth()) - this.mMarginLeft) - this.mMarginRight;
        this.mReactWidthStep = (this.mCanvasWidth - (((float) (this.segments - 1)) * this.mBlackWidth)) / ((float) this.segments);
        drawGrayInfo(canvas);
        drawColor(canvas);
    }

    private void drawGrayInfo(Canvas mCanvas) {
        this.mPaint.setColor(-7829368);
        for (int i = 0; i < this.segments; i++) {
            this.left = this.mMarginLeft + ((this.mReactWidthStep + this.mBlackWidth) * ((float) i));
            this.right = (this.mMarginLeft + (this.mReactWidthStep * ((float) (i + 1)))) + (this.mBlackWidth * ((float) i));
            Canvas canvas = mCanvas;
            canvas.drawRect(this.left, this.mMarginTop, this.right, this.mProgressLevelHeigth + this.mMarginTop, this.mPaint);
        }
    }

    private void drawColor(Canvas mCanvas) {
        if (this.levelInt != 0 || this.levelFloat != 0.0f) {
            Canvas canvas;
            for (int i = 0; i < this.levelInt; i++) {
                this.left = this.mMarginLeft + ((this.mReactWidthStep + this.mBlackWidth) * ((float) i));
                this.right = (this.mMarginLeft + (this.mReactWidthStep * ((float) (i + 1)))) + (this.mBlackWidth * ((float) i));
                this.mPaint.setColor(this.drawColorResource);
                canvas = mCanvas;
                canvas.drawRect(this.left, this.mMarginTop, this.right, this.mProgressLevelHeigth + this.mMarginTop, this.mPaint);
            }
            int next = this.levelInt + 1;
            this.left = this.mMarginLeft + ((this.mReactWidthStep + this.mBlackWidth) * ((float) this.levelInt));
            this.right = ((this.mMarginLeft + (this.mReactWidthStep * ((float) this.levelInt))) + (this.mBlackWidth * ((float) this.levelInt))) + (this.mReactWidthStep * this.levelFloat);
            this.mPaint.setColor(this.drawColorResource);
            canvas = mCanvas;
            canvas.drawRect(this.left, this.mMarginTop, this.right, this.mProgressLevelHeigth + this.mMarginTop, this.mPaint);
        }
    }

    public void setProgressLevel(float level) {
        this.currnetLevel = level;
        this.levelInt = (int) Math.floor((double) level);
        this.levelFloat = level - ((float) this.levelInt);
        postInvalidate();
    }
}
