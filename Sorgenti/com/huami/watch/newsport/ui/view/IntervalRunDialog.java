package com.huami.watch.newsport.ui.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmKeyEventScrollView;
import com.huami.watch.common.widget.HmKeyEventScrollView.OnKeyCallBackListener;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;
import com.huami.watch.newsport.C0532R;

public class IntervalRunDialog extends Dialog {
    private DismissListener dismissListener;
    private KeyEventDispatcher keyEventDispatcher;

    public static class Builder implements OnKeyCallBackListener {
        private HmHaloButton buttonNegativeButton;
        private HmHaloButton buttonPositiveButton;
        private LinearLayout contentView;
        private Context context;
        private boolean isPositive = true;
        private OnCancelListener mOnCancelListener;
        private OnDismissListener mOnDismissListener;
        private OnKeyListener mOnKeyListener;
        private OnClickListener positiveButtonClickListener;
        private HmKeyEventScrollView scrollView;
        private boolean systemType = false;
        private int theme = -1;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setPositiveButton(OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            this.mOnKeyListener = onKeyListener;
            return this;
        }

        public Builder setOnDismissListener(OnDismissListener onDismissListener) {
            this.mOnDismissListener = onDismissListener;
            return this;
        }

        public void onKeyCall(HMKeyEvent key) {
            if (key == HMKeyEvent.KEY_CENTER) {
                if (this.isPositive) {
                    if (this.buttonPositiveButton != null) {
                        this.buttonPositiveButton.performClick();
                        this.buttonPositiveButton.setPressed(true);
                    }
                } else if (this.buttonNegativeButton != null) {
                    this.buttonNegativeButton.performClick();
                    this.buttonNegativeButton.setPressed(true);
                }
            } else if (key == HMKeyEvent.KEY_DOWN) {
                if (this.scrollView != null && this.scrollView.isScrolledToBottom()) {
                }
            } else if (key != HMKeyEvent.KEY_UP || this.scrollView == null || !this.scrollView.isScrolledToTop()) {
            }
        }

        public IntervalRunDialog create() {
            final IntervalRunDialog dialog = new IntervalRunDialog(this.context, this.theme);
            if (this.systemType) {
                dialog.getWindow().setType(2003);
                dialog.getWindow().requestFeature(11);
            }
            this.contentView = (LinearLayout) LayoutInflater.from(this.context).inflate(C0532R.layout.dialog_no_remind_in_training_period, null, false);
            this.buttonPositiveButton = (HmHaloButton) this.contentView.findViewById(C0532R.id.positiveButton);
            this.buttonPositiveButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Builder.this.positiveButtonClickListener.onClick(dialog, -1);
                }
            });
            if (this.mOnKeyListener != null) {
                dialog.setOnKeyListener(this.mOnKeyListener);
            }
            if (this.mOnDismissListener != null) {
                dialog.setOnDismissListener(this.mOnDismissListener);
            }
            if (this.mOnCancelListener != null) {
                dialog.setOnCancelListener(this.mOnCancelListener);
            }
            dialog.setContentView(this.contentView);
            SwipeDismissUtil.requestSwipeDismissAlphaBackgroud(dialog.getWindow(), this.contentView);
            return dialog;
        }
    }

    public interface DismissListener {
        void dismiss();
    }

    public IntervalRunDialog(Context context, int theme) {
        super(context, theme);
    }

    public void dismiss() {
        super.dismiss();
        if (this.dismissListener != null) {
            this.dismissListener.dismiss();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        this.keyEventDispatcher.dispatchKeyevent((Dialog) this, event);
        return super.dispatchKeyEvent(event);
    }
}
