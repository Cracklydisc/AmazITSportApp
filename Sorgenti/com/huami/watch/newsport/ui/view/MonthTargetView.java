package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class MonthTargetView extends View {
    private Context mContext;
    private Paint mPaint;
    private float mScale;
    private Paint mTextPaint;

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int middle = getHeight() / 2;
        int width = getWidth();
        int left = getPaddingLeft();
        int right = width - getPaddingRight();
        this.mPaint.setColor(this.mContext.getResources().getColor(C0532R.color.sport_history_item_color));
        canvas.drawLine((float) left, (float) middle, (float) right, (float) middle, this.mPaint);
        this.mPaint.setColor(this.mContext.getResources().getColor(C0532R.color.default_font_color));
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) left, (float) middle, this.mScale * ((float) ((width - getLeft()) - getPaddingRight())), (float) middle, this.mPaint);
        StringBuilder builder = new StringBuilder();
        builder.append((int) (this.mScale * 100.0f)).append("%");
        canvas.drawText(builder.toString(), ((float) left) + (((float) ((width - getPaddingRight()) - getPaddingLeft())) * this.mScale), (float) middle, this.mTextPaint);
    }
}
