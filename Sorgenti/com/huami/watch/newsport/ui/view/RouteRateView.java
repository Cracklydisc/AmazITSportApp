package com.huami.watch.newsport.ui.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.huami.watch.common.widget.HmTextView;

public class RouteRateView extends HmTextView {
    private String content;
    private int lineLongth;
    private int lineWidth;
    private Paint mLinePaint;
    private Path mLinePath;
    private Paint mTextPaint;
    private int marginLeft;
    private int marginTop;

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.mLinePath.moveTo((float) this.marginLeft, (float) this.marginTop);
        this.mLinePath.lineTo((float) this.marginLeft, (float) (this.marginTop + this.lineWidth));
        this.mLinePath.lineTo((float) (this.marginLeft + this.lineLongth), (float) (this.marginTop + this.lineWidth));
        this.mLinePath.lineTo((float) (this.marginLeft + this.lineLongth), (float) this.marginTop);
        canvas.drawText(this.content, (float) ((this.marginLeft + (this.lineLongth / 2)) - 30), (float) (this.marginTop + (this.lineWidth / 2)), this.mTextPaint);
        canvas.drawPath(this.mLinePath, this.mLinePaint);
    }
}
