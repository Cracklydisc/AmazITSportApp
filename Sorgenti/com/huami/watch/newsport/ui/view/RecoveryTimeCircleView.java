package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class RecoveryTimeCircleView extends View {
    private static final String TAG = RecoveryTimeCircleView.class.getSimpleName();
    private float biggTxtSize;
    private float colorAngleStep;
    private float levelFloat;
    private int levelInt;
    private float mCanvasHeigth;
    private float mCanvasWidth;
    private float mCenterX;
    private float mCenterY;
    private float mCircleLineStrokeWidth;
    private Paint mCircleRedPaint;
    private Paint mFontPaint;
    private float mMarginBottom;
    private float mMarginLeft;
    private float mMarginRigth;
    private float mMarginTop;
    private int mSegments;
    private float radiusLength;
    private int remainingHourTime;
    private float smallTxtSize;
    private int spaceAngleStep;
    private int spaceFreeAngle;
    private float startAngle;
    private Typeface tf;
    private String timeUnit;

    public RecoveryTimeCircleView(Context context) {
        this(context, null);
    }

    public RecoveryTimeCircleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecoveryTimeCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCircleLineStrokeWidth = 10.0f;
        this.mMarginLeft = 0.0f;
        this.mMarginRigth = 0.0f;
        this.mMarginTop = 0.0f;
        this.mMarginBottom = 0.0f;
        this.mCanvasHeigth = 0.0f;
        this.mCanvasWidth = 0.0f;
        this.radiusLength = 55.0f;
        this.mSegments = 9;
        this.spaceFreeAngle = 60;
        this.spaceAngleStep = 3;
        this.colorAngleStep = 60.0f;
        this.startAngle = 0.0f;
        this.biggTxtSize = 60.0f;
        this.smallTxtSize = 30.0f;
        initPaint();
    }

    private void initPaint() {
        this.mCircleRedPaint = new Paint();
        this.mCircleRedPaint.setAntiAlias(true);
        this.mCircleRedPaint.setStrokeWidth(this.mCircleLineStrokeWidth);
        this.mCircleRedPaint.setStyle(Style.STROKE);
        this.mCircleRedPaint.setStrokeCap(Cap.BUTT);
        this.mCircleRedPaint.setColor(getResources().getColor(C0532R.color.widget_color_arc));
        if (this.tf == null) {
            this.tf = Typeface.createFromAsset(getContext().getAssets(), "huamifont-medium.otf");
        }
        this.mFontPaint = new Paint();
        this.mFontPaint.setAntiAlias(true);
        this.mFontPaint.setStyle(Style.STROKE);
        this.mFontPaint.setStrokeCap(Cap.ROUND);
        this.mFontPaint.setColor(-1);
        this.mFontPaint.setTypeface(this.tf);
        this.mFontPaint.setTextSize(this.smallTxtSize);
        this.timeUnit = getResources().getString(C0532R.string.time_unit_hour);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        initData();
        drawCircleBack(canvas);
    }

    private void initData() {
        this.mCanvasHeigth = (((float) getHeight()) - this.mMarginTop) - this.mMarginBottom;
        this.mCanvasWidth = (((float) getWidth()) - this.mMarginLeft) - this.mMarginRigth;
        this.colorAngleStep = (float) (((360 - this.spaceFreeAngle) - ((this.mSegments - 1) * this.spaceAngleStep)) / this.mSegments);
        this.startAngle = (float) ((this.spaceFreeAngle / 2) + 90);
        Log.i(TAG, " colorAngleStep:" + this.colorAngleStep + " ,startAngle:" + this.startAngle);
    }

    private void drawCircleBack(Canvas canvas) {
        Log.i(TAG, " levelInt:" + this.levelInt + ",levelFloat:" + this.levelFloat);
        this.mCenterX = this.mMarginLeft + (this.mCanvasWidth / 2.0f);
        this.mCenterY = this.mMarginTop + (this.mCanvasHeigth / 2.0f);
        RectF rect = new RectF(this.mCenterX - this.radiusLength, this.mCenterY - this.radiusLength, this.mCenterX + this.radiusLength, this.mCenterY + this.radiusLength);
        this.mCircleRedPaint.setColor(getResources().getColor(C0532R.color.widget_recory_time_gray));
        for (int i = 0; i < this.mSegments; i++) {
            float startCircleAngle = this.startAngle + ((this.colorAngleStep + ((float) this.spaceAngleStep)) * ((float) i));
            Log.i(TAG, " startCircleAngle:" + startCircleAngle + ",endCircleAngle:" + (startCircleAngle + this.colorAngleStep));
            canvas.drawArc(rect, startCircleAngle, this.colorAngleStep, false, this.mCircleRedPaint);
        }
        drawRedColor(canvas, rect);
    }

    private void drawRedColor(Canvas canvas, RectF rect) {
        this.mCircleRedPaint.setColor(getResources().getColor(C0532R.color.widget_color_arc));
        float startCircleAngle = this.startAngle;
        float endCircleAngle = 0.0f;
        for (int i = 0; i < this.levelInt; i++) {
            startCircleAngle = this.startAngle + ((this.colorAngleStep + ((float) this.spaceAngleStep)) * ((float) i));
            endCircleAngle = startCircleAngle + this.colorAngleStep;
            Log.i(TAG, " startCircleAngle:" + startCircleAngle + ",endCircleAngle:" + endCircleAngle);
            canvas.drawArc(rect, startCircleAngle, this.colorAngleStep, false, this.mCircleRedPaint);
        }
        Log.i(TAG, " last red startCircleAngle:" + startCircleAngle + ",endCircleAngle:" + (this.colorAngleStep * this.levelFloat));
        canvas.drawArc(rect, endCircleAngle + ((float) this.spaceAngleStep), this.colorAngleStep * this.levelFloat, false, this.mCircleRedPaint);
    }

    public void setReceryHourTime(float remainingTime, float totalReceryHour) {
        float levelTotal = remainingTime / (totalReceryHour / ((float) this.mSegments));
        this.levelInt = (int) Math.floor((double) levelTotal);
        this.levelFloat = levelTotal - ((float) this.levelInt);
        this.remainingHourTime = (int) remainingTime;
        postInvalidate();
    }
}
