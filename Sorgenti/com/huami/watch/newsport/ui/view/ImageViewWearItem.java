package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import com.huami.watch.newsport.C0532R;

public class ImageViewWearItem extends BaseWearItemView {
    public ImageViewWearItem(@NonNull Context context) {
        super(context);
    }

    public ImageViewWearItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewWearItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public View initView(Context context) {
        return View.inflate(context, C0532R.layout.image_header, this);
    }
}
