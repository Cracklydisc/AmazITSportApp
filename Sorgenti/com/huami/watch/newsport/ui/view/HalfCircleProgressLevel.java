package com.huami.watch.newsport.ui.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

public class HalfCircleProgressLevel extends View {
    private static final String TAG = HalfCircleProgressLevel.class.getSimpleName();
    private int currentLevel;
    private int currentRataAngle;
    private Paint currnetLevelPaint;
    private String currnetLevelStr;
    private float mCurrnetLevelFontSize;
    private float mCurrnetUnitFontSize;
    private Paint mLinePaint;
    private Paint mProgressLevelPaint;
    private Bitmap remindBitmap;
    private String unitStr;
    private float widgetHeigth;
    private float widgetWidth;

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float x = (float) ((getWidth() - (getHeight() / 2)) / 2);
        float y = (float) (getHeight() / 4);
        RectF oval = new RectF(x, y, ((float) getWidth()) - x, ((float) getHeight()) - y);
        this.widgetHeigth = oval.height();
        this.widgetWidth = oval.width();
        canvas.drawArc(oval, -180.0f, 180.0f, false, this.mLinePaint);
        canvas.drawArc(oval, -180.0f, (float) this.currentRataAngle, false, this.mProgressLevelPaint);
        Matrix matrix = new Matrix();
        matrix.postRotate((float) (this.currentRataAngle + 270));
        matrix.postTranslate((float) (((double) (getWidth() / 2)) - (((double) (this.widgetHeigth / 2.0f)) * Math.cos(Math.toRadians((double) this.currentRataAngle)))), (float) (((double) (getHeight() / 2)) - (((double) (this.widgetHeigth / 2.0f)) * Math.sin(Math.toRadians((double) this.currentRataAngle)))));
        canvas.drawBitmap(this.remindBitmap, matrix, this.mLinePaint);
        this.currnetLevelStr = this.currentLevel + "";
        Rect rect = new Rect();
        this.currnetLevelPaint.setTextSize(this.mCurrnetLevelFontSize);
        this.currnetLevelPaint.getTextBounds(String.valueOf(this.currentLevel), 0, this.currnetLevelStr.length(), rect);
        int levelNumeWdith = rect.width();
        canvas.drawText(String.valueOf(this.currentLevel), (float) ((getWidth() / 2) - (levelNumeWdith / 2)), (float) (getHeight() / 2), this.currnetLevelPaint);
        this.currnetLevelPaint.setTextSize(this.mCurrnetUnitFontSize);
        this.currnetLevelPaint.getTextBounds(this.unitStr, 0, this.unitStr.length(), rect);
        levelNumeWdith = rect.width();
        canvas.drawText(this.unitStr, (float) ((getWidth() / 2) - (levelNumeWdith / 2)), (float) ((getHeight() / 2) + 30), this.currnetLevelPaint);
    }
}
