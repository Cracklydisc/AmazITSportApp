package com.huami.watch.newsport.ui.view;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class ArcPercentWidgetDrawable extends Drawable {
    private float mAngleMax = 0.0f;
    private float mAngleStart = 0.0f;
    private Paint mArcPaint;
    private RectF mArcRect;
    private float mArcWidth;
    private Paint mBasePaint;
    private Paint mBoundaryPaint;
    private float mEndRadius = 9.0f;
    private float mRadius = 0.0f;
    private float percent;

    public ArcPercentWidgetDrawable(float baseWidth, int baseColor, float arcWidth, int arcColor, float shadowRadius, float shadowDx, float shadowDy, int shadowColor, float endRadius, int endColor) {
        this.mArcWidth = arcWidth;
        this.percent = 0.0f;
        this.mBasePaint = new Paint(1);
        this.mBasePaint.setStrokeCap(Cap.ROUND);
        this.mBasePaint.setColor(baseColor);
        this.mBasePaint.setStrokeWidth(baseWidth);
        this.mBasePaint.setStyle(Style.STROKE);
        this.mArcPaint = new Paint(1);
        this.mArcPaint.setStrokeCap(Cap.ROUND);
        this.mArcPaint.setColor(arcColor);
        this.mArcPaint.setStrokeWidth(this.mArcWidth);
        this.mArcPaint.setShadowLayer(shadowRadius, shadowDx, shadowDy, shadowColor);
        this.mArcPaint.setStyle(Style.STROKE);
        this.mBoundaryPaint = new Paint(1);
        this.mBoundaryPaint.setColor(endColor);
        this.mBoundaryPaint.setStyle(Style.FILL);
        this.mEndRadius = endRadius / 2.0f;
    }

    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        init(bounds.left, bounds.top, bounds.right, bounds.bottom);
    }

    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        init(left, top, right, bottom);
    }

    private void init(int left, int top, int right, int bottom) {
        float radius = (((float) Math.min(right - left, bottom - top)) - this.mArcWidth) / 2.0f;
        this.mRadius = radius;
        float cx = (float) ((left + right) / 2);
        float cy = (float) ((top + bottom) / 2);
        this.mArcRect = new RectF(cx - radius, cy - radius, cx + radius, cy + radius);
    }

    public void draw(Canvas canvas) {
        int i;
        int i2 = 1;
        if (this.mAngleMax == 0.0f) {
            i = 1;
        } else {
            i = 0;
        }
        if (this.mAngleStart != 0.0f) {
            i2 = 0;
        }
        if ((i | i2) == 0) {
            canvas.drawArc(this.mArcRect, this.mAngleStart, this.mAngleMax, false, this.mBasePaint);
            if (this.percent > 0.0f) {
                if (this.percent > 1.0f) {
                    this.percent = 1.0f;
                }
                Canvas canvas2 = canvas;
                canvas2.drawArc(this.mArcRect, this.mAngleStart, this.percent * this.mAngleMax, false, this.mArcPaint);
            }
        }
    }

    public void setAlpha(int alpha) {
        this.mArcPaint.setAlpha(alpha);
    }

    public void setColorFilter(ColorFilter cf) {
        this.mArcPaint.setColorFilter(cf);
    }

    public int getOpacity() {
        return 1 - this.mArcPaint.getAlpha();
    }

    public void setPercent(float percent) {
        this.percent = percent;
        invalidateSelf();
    }

    public void setAngleRange(float anglestart, float anglemax) {
        this.mAngleStart = anglestart;
        this.mAngleMax = anglemax;
    }
}
