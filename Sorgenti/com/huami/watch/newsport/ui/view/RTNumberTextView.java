package com.huami.watch.newsport.ui.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.huami.watch.common.widget.HmTextView;

public class RTNumberTextView extends HmTextView {
    static Typeface tf = null;

    public RTNumberTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RTNumberTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RTNumberTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (tf == null) {
            tf = Typeface.createFromAsset(getContext().getAssets(), "huamisport-Regular.otf");
        }
        setTypeface(tf);
    }
}
