package com.huami.watch.newsport.ui.fragbase;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.newsport.ui.fragps.GPSearchFragment;
import com.huami.watch.newsport.ui.fragps.IntermittentTraingFragment;
import com.huami.watch.newsport.ui.fragps.SelectIntermittentTrainFragment;
import com.huami.watch.newsport.ui.fragps.SettingFragment;
import com.huami.watch.newsport.ui.fragps.SportTargetRemindFragment;

public class BaseFragment extends Fragment {
    protected int mSportType = -1;

    public static BaseFragment newInstance(int sportType, int fragmentType) {
        BaseFragment fragment;
        if (fragmentType == 0) {
            fragment = new GPSearchFragment();
        } else if (fragmentType == 1) {
            fragment = new SettingFragment();
        } else if (fragmentType == 2) {
            fragment = new SportTargetRemindFragment();
        } else if (fragmentType == 3) {
            fragment = new IntermittentTraingFragment();
        } else if (fragmentType == 4) {
            fragment = new SelectIntermittentTrainFragment();
        } else {
            fragment = new BaseFragment();
        }
        Bundle bundle = new Bundle();
        bundle.putInt("fragment_sport_type", sportType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (getArguments() != null) {
            this.mSportType = getArguments().getInt("fragment_sport_type");
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
