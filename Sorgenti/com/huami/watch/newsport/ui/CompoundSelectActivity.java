package com.huami.watch.newsport.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import com.huami.watch.common.widget.HmViewPager;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import com.huami.watch.newsport.ui.fragpicker.AbsMultiPageSportPickerFragment;
import com.huami.watch.newsport.ui.fragpicker.AbsMultiPageSportPickerFragment.IResultConfirmLisener;
import com.huami.watch.newsport.ui.fragpicker.CompoundPickerFragment;
import java.util.ArrayList;
import java.util.List;

public class CompoundSelectActivity extends BaseActivity implements EventCallBack, IResultConfirmLisener {
    private MixedBaseConfig mConfig = null;
    private List<Fragment> mFragmentList = new ArrayList();
    private HmViewPager mViewPager = null;

    class C06741 implements OnPageChangeListener {
        C06741() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            CompoundSelectActivity.this.setCanacceptKeyUpOrDownEvent(position);
        }

        public void onPageScrollStateChanged(int state) {
        }
    }

    private class CompundPageAdapter extends FragmentPagerAdapter {
        public CompundPageAdapter(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            return (Fragment) CompoundSelectActivity.this.mFragmentList.get(position);
        }

        public int getCount() {
            return CompoundSelectActivity.this.mFragmentList.size();
        }
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_compound_layout);
        this.mViewPager = (HmViewPager) findViewById(C0532R.id.pager);
        ViewPagerPageIndicator indicator = (ViewPagerPageIndicator) findViewById(C0532R.id.indicator);
        this.mConfig = (MixedBaseConfig) DataManager.getInstance().getSportConfig(this, 2002);
        setKeyEventListener(this);
        initFragmentList();
        this.mViewPager.setAdapter(new CompundPageAdapter(getSupportFragmentManager()));
        this.mViewPager.setOffscreenPageLimit(this.mFragmentList.size());
        indicator.setViewPager(this.mViewPager);
        indicator.showIndicator(true);
        this.mViewPager.addOnPageChangeListener(new C06741());
    }

    private void initFragmentList() {
        this.mFragmentList.clear();
        this.mFragmentList.add(CompoundPickerFragment.newInstance(this.mConfig, 0));
        this.mFragmentList.add(CompoundPickerFragment.newInstance(this.mConfig, 1));
        this.mFragmentList.add(CompoundPickerFragment.newInstance(this.mConfig, 2));
    }

    public void onClickConfirmButton() {
        List<Integer> childSports = new ArrayList();
        for (int i = 0; i < this.mFragmentList.size(); i++) {
            int sportType = getSportType(((AbsMultiPageSportPickerFragment) this.mFragmentList.get(i)).getItemPostion(), i);
            if (SportType.isSportTypeValid(sportType)) {
                childSports.add(Integer.valueOf(sportType));
            }
        }
        this.mConfig.setChildSports(childSports);
        DataManager.getInstance().setSportConfig(this, this.mConfig);
        setResult(-1);
        finish();
    }

    public void initViewCompleted() {
        setCanacceptKeyUpOrDownEvent(this.mViewPager.getCurrentItem());
    }

    private int getSportType(int index, int column) {
        if (column == 2) {
            if (index == 1) {
                return 1015;
            }
            if (index == 2) {
                return 1009;
            }
            if (index == 3) {
                return 1001;
            }
        } else if (index == 0) {
            return 1015;
        } else {
            if (index == 1) {
                return 1009;
            }
            if (index == 2) {
                return 1001;
            }
        }
        return -1;
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            int curIndex = this.mViewPager.getCurrentItem();
            int nextIndex = curIndex + 1 >= this.mFragmentList.size() ? this.mFragmentList.size() - 1 : curIndex + 1;
            this.mViewPager.setCurrentItem(nextIndex, true);
            setCanacceptKeyUpOrDownEvent(nextIndex);
            if (nextIndex == this.mFragmentList.size() - 1 && curIndex == nextIndex) {
                int centerIndex = ((AbsMultiPageSportPickerFragment) this.mFragmentList.get(this.mFragmentList.size() - 1)).getCurIndex();
                ((AbsMultiPageSportPickerFragment) this.mFragmentList.get(nextIndex)).setCenterIndex(1);
                if (centerIndex == 1) {
                    onClickConfirmButton();
                }
            }
        } else if (hmKeyEvent == HMKeyEvent.KEY_DOWN || hmKeyEvent == HMKeyEvent.KEY_UP) {
            setCanacceptKeyUpOrDownEvent(this.mViewPager.getCurrentItem());
        }
        return false;
    }

    private void setCanacceptKeyUpOrDownEvent(int index) {
        for (int i = 0; i < this.mFragmentList.size(); i++) {
            if (index == i) {
                ((AbsMultiPageSportPickerFragment) this.mFragmentList.get(i)).setCanAccept(true);
            } else {
                ((AbsMultiPageSportPickerFragment) this.mFragmentList.get(i)).setCanAccept(false);
            }
        }
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }
}
