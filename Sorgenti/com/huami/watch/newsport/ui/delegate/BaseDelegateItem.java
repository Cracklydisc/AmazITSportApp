package com.huami.watch.newsport.ui.delegate;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryRequest;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;
import com.huami.watch.newsport.ui.view.HillView;
import com.huami.watch.newsport.ui.view.NewSportChart;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.ProportionView;
import com.huami.watch.newsport.ui.view.ProportionView.ProportionViewData;
import com.huami.watch.newsport.ui.view.TrainingEffectView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.ResourceUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BaseDelegateItem {
    protected static final float[] BENCH_RANGE = new float[]{0.5f, 0.69f};
    private static final String TAG = BaseDelegateItem.class.getSimpleName();
    private final int HEAD_PADDING_BOTTOM = 5;
    private final int HEAD_PADDING_LEFT = 70;
    private final int HEAD_PADDING_RIGHT = 30;
    private final int HEAD_PADDING_TOP = 5;
    protected View dailyPerformenceChatView = null;
    protected NewSportChart<Float> mAltitudeView = null;
    protected View mCacheAltitudeView = null;
    protected View mCacheCadenceView = null;
    protected View mCacheHeartView = null;
    protected View mCachePaceView = null;
    protected View mCachePercentView = null;
    protected View mCacheSpeedView = null;
    protected View mCacheStepFreqView = null;
    protected View mCacheStrokeView = null;
    protected NewSportChart<Integer> mCadenceView = null;
    protected ProportionView mHeartPercentView = null;
    protected NewSportChart<Integer> mHeartRateView = null;
    private String mInvalidHint;
    protected NewSportChart<Float> mPaceView = null;
    protected NewSportChart<Float> mSpeedView = null;
    protected NewSportChart<Float> mStepFreqView = null;
    protected NewSportChart<Integer> mStrokeView = null;

    public View addModifyItem(Context context, OnClickListener listener) {
        View view = LayoutInflater.from(context).inflate(C0532R.layout.item_sport_history_modify, null);
        LayoutParams lp = new LayoutParams(-1, -2);
        lp.height = 50;
        view.setLayoutParams(lp);
        view.findViewById(C0532R.id.modify_btn).setOnClickListener(listener);
        return view;
    }

    public View addHeadItem(Context context, String title, int color) {
        LayoutParams lp = new LayoutParams(-1, -2);
        lp.setMargins(0, 10, 0, 10);
        View view = LayoutInflater.from(context).inflate(C0532R.layout.item_sport_history_title, null);
        view.setLayoutParams(lp);
        ((HmTextView) view.findViewById(C0532R.id.sport_history_title)).setText(title);
        return view;
    }

    public View addChildItem(Context mContext, String title, String number, String unit) {
        LayoutParams lp = new LayoutParams(-1, -2);
        View view = LayoutInflater.from(mContext).inflate(C0532R.layout.item_sport_history_view, null);
        view.setLayoutParams(lp);
        if (TextUtils.isEmpty(title)) {
            view.findViewById(C0532R.id.desc).setVisibility(8);
        } else {
            ((HmTextView) view.findViewById(C0532R.id.desc)).setText(title);
        }
        ((NumberTextView) view.findViewById(C0532R.id.number_text_view)).setText(number);
        HmTextView unitTextView = (HmTextView) view.findViewById(C0532R.id.unit_text_view);
        if (unit != null) {
            unitTextView.setVisibility(0);
            if (TextUtils.isEmpty(number)) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) unitTextView.getLayoutParams();
                params.setMarginStart(0);
                unitTextView.setLayoutParams(params);
                unitTextView.setTextSize(28.0f);
            }
            unitTextView.setText(unit);
        } else {
            unitTextView.setVisibility(4);
        }
        return view;
    }

    public View addSpliteLine(Context mContext) {
        LayoutParams lp = new LayoutParams(-1, -2);
        View view = LayoutInflater.from(mContext).inflate(C0532R.layout.split_line, null);
        view.setLayoutParams(lp);
        return view;
    }

    public View addLineChartView(Context context, String leftTitle, String middleTitle, String rightTitle, String leftValue, String middleValue, String rightValue) {
        LayoutParams lp = new LayoutParams(-1, -2);
        View view = LayoutInflater.from(context).inflate(C0532R.layout.item_sport_history_line_chart, null);
        ((TextView) view.findViewById(C0532R.id.left_item).findViewById(C0532R.id.item_unit)).setText(leftTitle);
        ((TextView) view.findViewById(C0532R.id.left_item).findViewById(C0532R.id.item_value)).setText(leftValue);
        ((TextView) view.findViewById(C0532R.id.middle_item).findViewById(C0532R.id.item_unit)).setText(middleTitle);
        ((TextView) view.findViewById(C0532R.id.middle_item).findViewById(C0532R.id.item_value)).setText(middleValue);
        ((TextView) view.findViewById(C0532R.id.right_item).findViewById(C0532R.id.item_unit)).setText(rightTitle);
        ((TextView) view.findViewById(C0532R.id.right_item).findViewById(C0532R.id.item_value)).setText(rightValue);
        view.setLayoutParams(lp);
        return view;
    }

    public View addLineChartView(Context context, String leftTitle, String middleTitle, String leftValue, String middleValue) {
        LayoutParams lp = new LayoutParams(-1, -2);
        View view = LayoutInflater.from(context).inflate(C0532R.layout.item_sport_history_line_chart, null);
        ((TextView) view.findViewById(C0532R.id.left_item).findViewById(C0532R.id.item_unit)).setText(leftTitle);
        ((TextView) view.findViewById(C0532R.id.left_item).findViewById(C0532R.id.item_value)).setText(leftValue);
        ((TextView) view.findViewById(C0532R.id.middle_item).findViewById(C0532R.id.item_unit)).setText(middleTitle);
        ((TextView) view.findViewById(C0532R.id.middle_item).findViewById(C0532R.id.item_value)).setText(middleValue);
        ((TextView) view.findViewById(C0532R.id.right_item).findViewById(C0532R.id.item_unit)).setText("");
        ((TextView) view.findViewById(C0532R.id.right_item).findViewById(C0532R.id.item_value)).setText("");
        view.setLayoutParams(lp);
        return view;
    }

    public View addHillView(Context context, SportSummary sportSummary) {
        LayoutParams lp = new LayoutParams(-1, -2);
        View view = LayoutInflater.from(context).inflate(C0532R.layout.fragment_history_detail_hill, null);
        lp.height = 270;
        view.setLayoutParams(lp);
        HillView hillView = (HillView) view.findViewById(C0532R.id.hill_view);
        long up = ((OutdoorSportSummary) sportSummary).getAscendTime();
        long down = ((OutdoorSportSummary) sportSummary).getDescendTime();
        long even = (((long) sportSummary.getSportDuration()) - up) - down;
        Debug.m5i(TAG, "addHillView, up:" + up + ", down:" + down + ", duration:" + sportSummary.getSportDuration());
        hillView.setTimeCost(even, up, down);
        return view;
    }

    public View addHorizontalChartView(Context mContext, String[] titles, int[] percents) {
        View view = LayoutInflater.from(mContext).inflate(C0532R.layout.item_heart_region_view, null);
        ProportionView mProportion = (ProportionView) view.findViewById(C0532R.id.proportion);
        List<ProportionViewData> datas = new ArrayList();
        int color = mContext.getResources().getColor(C0532R.color.default_font_color);
        if (!(titles == null || percents == null)) {
            for (int i = 0; i < titles.length; i++) {
                datas.add(new ProportionViewData(titles[i], percents[i], color));
            }
        }
        mProportion.setData(datas);
        mProportion.setVerticalGap(10);
        mProportion.setHorizontalGap(10);
        return view;
    }

    public void configGraphView(Context context, View view, int startColor, int endColor, int lineColor) {
        NewSportChart<? extends Number> chartView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        chartView.setShowDigitIsTop(true);
        chartView.configInitLinechartBase(2.0f, lineColor);
        chartView.configCurveCoverGradient(true, new int[]{startColor, endColor}, new float[]{0.0f, 1.0f});
        chartView.configLineGradient(new int[]{endColor, endColor}, new float[]{0.0f, 1.0f});
        chartView.setShowDigitIsTop(true);
    }

    public View addDallyPerformenceView(final Context mContext, OutdoorSportSummary sportSummary) {
        LayoutParams lp = new LayoutParams(-1, -2);
        lp.height = 230;
        View view = LayoutInflater.from(mContext).inflate(C0532R.layout.item_sport_history_dally_performence, null);
        view.setLayoutParams(lp);
        this.dailyPerformenceChatView = (DailyPerformenceChatView) view.findViewById(C0532R.id.daily_performence_chat_view);
        HmTextView dailyPerpormenceTitleView = (HmTextView) view.findViewById(C0532R.id.daily_perpormence_title);
        Log.i(TAG, " draw dail porpermence trackId:" + sportSummary.getTrackId());
        dailyPerpormenceTitleView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                FirstBeatDataUtils.jumpToFirstBeatHelpPage(mContext, 4);
            }
        });
        return view;
    }

    public View addTrainEffectView(Context mContext, OutdoorSportSummary sportSummary) {
        LayoutParams lp = new LayoutParams(-1, -2);
        lp.setMargins(0, 10, 0, 10);
        lp.height = 266;
        View root = LayoutInflater.from(mContext).inflate(C0532R.layout.fragment_delegate_sport_te_page, null);
        root.setLayoutParams(lp);
        NumberTextView teValueNumber = (NumberTextView) root.findViewById(C0532R.id.te_number_value);
        HmTextView teValueText = (HmTextView) root.findViewById(C0532R.id.te_value_percent);
        ((HmTextView) root.findViewById(C0532R.id.text_te_desc)).setText(Html.fromHtml(mContext.getString(ResourceUtils.getStringId(mContext, ResourceUtils.getStringArray(mContext, C0532R.array.sport_te_desc_array)[FirstBeatDataUtils.getTELevelByValue((float) (sportSummary.getTE() / 10))]))));
        final Context context = mContext;
        ((HmTextView) root.findViewById(C0532R.id.text_te_title)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                FirstBeatDataUtils.jumpToFirstBeatHelpPage(context, 1);
            }
        });
        TrainingEffectView columGradientProgressBar = (TrainingEffectView) root.findViewById(C0532R.id.te_chart);
        float teProgress = ((float) sportSummary.getTE()) / 10.0f;
        LogUtils.print(TAG, "configInfo: te:" + teProgress + "," + DataManager.getInstance().getFirstBeatConfig(mContext).toString());
        columGradientProgressBar.setProgressLevel(teProgress);
        teValueNumber.setText(String.valueOf(teProgress));
        teValueText.setText(String.valueOf("/5"));
        return root;
    }

    public View addHeartPercentView(Context context) {
        View view = addHorizontalChartView(context, null, null);
        this.mHeartPercentView = (ProportionView) view.findViewById(C0532R.id.proportion);
        this.mCachePercentView = view.findViewById(C0532R.id.cache_view);
        ((TextView) view.findViewById(C0532R.id.desc)).setText(context.getString(C0532R.string.heart_region_title));
        return view;
    }

    public void updateHeartPercentView(Context context, int[] percents) {
        if (percents != null && percents.length == 5) {
            int[] colors = new int[]{context.getResources().getColor(C0532R.color.sport_history_heart_range4), context.getResources().getColor(C0532R.color.sport_history_heart_range3), context.getResources().getColor(C0532R.color.sport_history_heart_range2), context.getResources().getColor(C0532R.color.sport_history_heart_range1), context.getResources().getColor(C0532R.color.sport_history_heart_range0)};
            String[] titles = new String[]{context.getResources().getString(C0532R.string.heart_region_anaerobic＿limit), context.getResources().getString(C0532R.string.heart_region_endurance_enhancement), context.getResources().getString(C0532R.string.heart_region_heart_and_lung_enhancement), context.getResources().getString(C0532R.string.heart_region_fat_burning), context.getResources().getString(C0532R.string.heart_region_warm_up_and_relax)};
            List<ProportionViewData> datas = new ArrayList();
            if (!(titles == null || percents == null)) {
                for (int i = 0; i < titles.length; i++) {
                    datas.add(new ProportionViewData(titles[i], percents[i], colors[i]));
                }
            }
            this.mHeartPercentView.setData(datas);
            this.mHeartPercentView.setVerticalGap(10);
            this.mHeartPercentView.setHorizontalGap(10);
        }
    }

    public View addHeartLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String leftTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String middleTitle = context.getString(C0532R.string.sport_graph_title_max);
        String invalidVaule = context.getString(C0532R.string.sport_pause_heart_shown);
        int ave = ((OutdoorSportSummary) mSportSummary).getAvgHeartRate();
        int max = ((OutdoorSportSummary) mSportSummary).getMaxHeartRate();
        int min = ((OutdoorSportSummary) mSportSummary).getMinHeartRate();
        View view = addLineChartView(context, leftTitle, middleTitle, ave <= 0 ? invalidVaule : String.valueOf(ave), max <= 0 ? invalidVaule : String.valueOf(max));
        this.mHeartRateView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mCacheHeartView = view.findViewById(C0532R.id.cache_view);
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_heart_start_color), context.getResources().getColor(C0532R.color.outdoor_heart_end_color), context.getResources().getColor(C0532R.color.outdoor_heart_end_color));
        return view;
    }

    public View addStrokeLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String leftTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String middleTitle = context.getString(C0532R.string.sport_graph_title_max);
        String invalidVaule = context.getString(C0532R.string.sport_pause_heart_shown);
        int ave = Math.round(((OutdoorSportSummary) mSportSummary).getAvgStrokeSpeed() * 60.0f);
        int max = Math.round(((OutdoorSportSummary) mSportSummary).getMaxStrokeSpeed() * 60.0f);
        View view = addLineChartView(context, leftTitle, middleTitle, ave < 0 ? invalidVaule : String.valueOf(ave), max < 0 ? invalidVaule : String.valueOf(max));
        if (((OutdoorSportSummary) mSportSummary).getTotalTrips() < 2) {
            view.findViewById(C0532R.id.left_item).setVisibility(8);
        }
        this.mStrokeView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mStrokeView.setVMinOffset(0.0f);
        this.mCacheStrokeView = view.findViewById(C0532R.id.cache_view);
        ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.stroke_speed_curve_title));
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_start_color), context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_end_color), context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_end_color));
        return view;
    }

    public View addPaceLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String middleVaule;
        String rightVaule;
        String middleTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String rightTitle = context.getString(C0532R.string.sport_graph_title_fast);
        String invalidVaule = context.getString(C0532R.string.sport_pause_heart_shown);
        double ave = (double) ((OutdoorSportSummary) mSportSummary).getTotalPace();
        double max = (double) ((OutdoorSportSummary) mSportSummary).getBestPace();
        if (mSportSummary.getSportType() == 14 || mSportSummary.getSportType() == 15 || mSportSummary.getSportType() == 1015) {
            middleVaule = ave <= 0.0d ? invalidVaule : DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter((float) ave, ((OutdoorSportSummary) mSportSummary).getUnit()));
            if (max <= 0.0d) {
                rightVaule = invalidVaule;
            } else {
                rightVaule = DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter((float) max, ((OutdoorSportSummary) mSportSummary).getUnit()));
            }
        } else {
            middleVaule = ave <= 0.0d ? invalidVaule : DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace((float) ave));
            if (max <= 0.0d) {
                rightVaule = invalidVaule;
            } else {
                rightVaule = DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace((float) max));
            }
        }
        View view = addLineChartView(context, middleTitle, rightTitle, middleVaule, rightVaule);
        if (((OutdoorSportSummary) mSportSummary).getTotalTrips() < 2 && mSportSummary.getSportType() == 14) {
            view.findViewById(C0532R.id.left_item).setVisibility(8);
        }
        this.mPaceView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mCachePaceView = view.findViewById(C0532R.id.cache_view);
        if (UnitConvertUtils.isImperial()) {
            if (!SportType.isSwimMode(mSportSummary.getSportType())) {
                ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_pace_graph_desc_imperial));
            } else if (((OutdoorSportSummary) mSportSummary).getUnit() == 0) {
                ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_swim_pace_graph_desc));
            } else {
                ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_swim_pace_graph_desc_yd));
            }
        } else if (!SportType.isSwimMode(mSportSummary.getSportType())) {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_pace_graph_desc));
        } else if (((OutdoorSportSummary) mSportSummary).getUnit() == 0) {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_swim_pace_graph_desc));
        } else {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_swim_pace_graph_desc_yd));
        }
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_speed_start_color), context.getResources().getColor(C0532R.color.outdoor_speed_end_color), context.getResources().getColor(C0532R.color.outdoor_speed_end_color));
        return view;
    }

    public View addSpeedLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String leftTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String middleTitle = context.getString(C0532R.string.sport_graph_title_max);
        float ave = ((OutdoorSportSummary) mSportSummary).getTotalSpeed();
        float max = ((OutdoorSportSummary) mSportSummary).getMaxSpeed();
        View view = addLineChartView(context, leftTitle, middleTitle, DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(ave)), true), DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(max)), true));
        this.mSpeedView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mSpeedView.setVMinOffset(0.0f);
        this.mCacheSpeedView = view.findViewById(C0532R.id.cache_view);
        if (UnitConvertUtils.isImperial()) {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_speed_graph_desc_imperial));
        } else {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_speed_graph_desc));
        }
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_speed_start_color), context.getResources().getColor(C0532R.color.outdoor_speed_end_color), context.getResources().getColor(C0532R.color.outdoor_speed_end_color));
        return view;
    }

    public View addAltitudeLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String leftValue;
        String leftTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String middleTitle = context.getString(C0532R.string.sport_graph_title_high);
        String rightTitle = context.getString(C0532R.string.sport_graph_title_low);
        String invalidVaule = context.getString(C0532R.string.sport_pause_heart_shown);
        int max = Math.round(((OutdoorSportSummary) mSportSummary).getHighestAltitude());
        int min = Math.round(((OutdoorSportSummary) mSportSummary).getLowestAltitude());
        if (max <= -20000 || min <= -20000) {
            leftValue = invalidVaule;
        } else {
            leftValue = String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) (((float) (min + max)) / 2.0f), mSportSummary.getSportType())));
        }
        View view = addLineChartView(context, leftTitle, middleTitle, rightTitle, leftValue, max <= -20000 ? invalidVaule : String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) max, mSportSummary.getSportType()))), min <= -20000 ? invalidVaule : String.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) min, mSportSummary.getSportType()))));
        this.mAltitudeView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mCacheAltitudeView = view.findViewById(C0532R.id.cache_view);
        if (UnitConvertUtils.isImperial()) {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_altitude_graph_desc_imperial));
        } else {
            ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_altitude_graph_desc));
        }
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_altitude_start_color), context.getResources().getColor(C0532R.color.outdoor_altitude_end_color), context.getResources().getColor(C0532R.color.outdoor_altitude_end_color));
        return view;
    }

    public View addStepFreqLineChart(Context context, String title, String unit, SportSummary mSportSummary) {
        String middleTitle = context.getString(C0532R.string.sport_graph_title_ave);
        String rightTitle = context.getString(C0532R.string.sport_graph_title_max);
        String invalidVaule = context.getString(C0532R.string.sport_pause_heart_shown);
        int ave = UnitConvertUtils.convertStepFreqToTimesPerMin((double) ((OutdoorSportSummary) mSportSummary).getStepFreq());
        int max = UnitConvertUtils.convertStepFreqToTimesPerMin((double) ((OutdoorSportSummary) mSportSummary).getMaxStepFreq());
        View view = addLineChartView(context, middleTitle, rightTitle, ave <= 0 ? invalidVaule : String.valueOf(ave), max <= 0 ? invalidVaule : String.valueOf(max));
        this.mStepFreqView = (NewSportChart) view.findViewById(C0532R.id.history_detail_heartrate);
        this.mStepFreqView.setVMinOffset(0.0f);
        this.mCacheStepFreqView = view.findViewById(C0532R.id.cache_view);
        ((HmTextView) view.findViewById(C0532R.id.history_detail_heartrate_title)).setText(context.getString(C0532R.string.history_step_freq_graph_desc));
        configGraphView(context, view, context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_start_color), context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_end_color), context.getResources().getColor(C0532R.color.outdoor_stroke_cadence_end_color));
        return view;
    }

    protected void loadCurveImg(Context context, SportSummary sportSummary, int curveType, ISportHistoryRequest IRequest, int requestType) {
        if (RecordGraphManager.getInstance(context).isCacheGraphData(sportSummary.getTrackId(), curveType)) {
            View realView = null;
            View cacheView = null;
            switch (curveType) {
                case 1:
                    realView = this.mHeartRateView;
                    cacheView = this.mCacheHeartView;
                    break;
                case 2:
                    realView = this.mSpeedView;
                    cacheView = this.mCacheSpeedView;
                    break;
                case 3:
                    realView = this.mAltitudeView;
                    cacheView = this.mCacheAltitudeView;
                    break;
                case 4:
                    realView = this.mPaceView;
                    cacheView = this.mCachePaceView;
                    break;
                case 5:
                    realView = this.mStepFreqView;
                    cacheView = this.mCacheStepFreqView;
                    break;
                case 9:
                    realView = this.mHeartPercentView;
                    cacheView = this.mCachePercentView;
                    break;
                case 10:
                    realView = this.mStrokeView;
                    cacheView = this.mCacheStrokeView;
                    break;
                case 11:
                    realView = this.mCadenceView;
                    cacheView = this.mCacheCadenceView;
                    break;
                case 12:
                    realView = this.dailyPerformenceChatView;
                    return;
            }
            if (!(realView == null || cacheView == null)) {
                realView.setVisibility(8);
                cacheView.setVisibility(0);
            }
            RecordGraphManager.getInstance(context).postToSearchGraph(sportSummary.getTrackId(), curveType);
        } else if (IRequest != null) {
            IRequest.doSportHistoryDataRequest(requestType);
        }
    }

    public String getChildExchangeTitle(Context context, int type) {
        switch (type) {
            case 1001:
                return context.getResources().getString(C0532R.string.history_riding_exchange_title);
            case 1009:
                return context.getResources().getString(C0532R.string.history_swim_exchange_title);
            case 1015:
                return context.getResources().getString(C0532R.string.history_walk_exchange_title);
            default:
                return "";
        }
    }

    public String getStartTimeString(Context mActivity, OutdoorSportSummary sportSummary) {
        if (DateFormat.is24HourFormat(mActivity)) {
            return DataFormatUtils.parseMilliSecondToFormattedTime(sportSummary.getStartTime(), "yy/MM/dd HH:mm", true);
        }
        String amOrPm;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(sportSummary.getStartTime());
        if (calendar.get(9) == 0) {
            amOrPm = mActivity.getString(C0532R.string.date_am);
        } else {
            amOrPm = mActivity.getString(C0532R.string.date_pm);
        }
        return DataFormatUtils.parseMilliSecondToFormattedTime(sportSummary.getStartTime(), "yy/MM/dd " + amOrPm + " hh:mm", true);
    }

    public String getDistanceBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) walkingSummary.getDistance()), false);
    }

    public String getSwimDistanceBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return "" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) walkingSummary.getDistance()), walkingSummary.getUnit()));
    }

    public String getCostTimeBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseMillSecondToDefaultFormattedTime(walkingSummary.getTrimEndTime() - walkingSummary.getTrimStartTime());
    }

    public String getSwimStyleTitle(Context context, OutdoorSportSummary summary) {
        if (summary == null) {
            return null;
        }
        int swimStyle = summary.getSwimStyle();
        switch (swimStyle) {
            case 1:
            case 2:
            case 3:
            case 6:
                return context.getString(C0532R.string.sport_swim_style_main);
            case 4:
                return context.getString(C0532R.string.sport_swim_style);
            default:
                String swimStyleStr = context.getString(C0532R.string.sport_swim_style_main);
                Debug.m5i(TAG, "swim style:" + swimStyle);
                return swimStyleStr;
        }
    }

    public String getSwimStyle(Context context, OutdoorSportSummary summary) {
        if (summary == null) {
            return null;
        }
        int swimStyle = summary.getSwimStyle();
        switch (swimStyle) {
            case 1:
                return context.getString(C0532R.string.swim_breaststroke);
            case 2:
                return context.getString(C0532R.string.swim_free_style);
            case 3:
                return context.getString(C0532R.string.swim_backstroke);
            case 4:
                return context.getString(C0532R.string.swim_medlyrelay);
            case 6:
                return context.getString(C0532R.string.swim_butterfly);
            default:
                String swimStyleStr = getMInvalidHintString(context);
                Debug.m5i(TAG, "swim style:" + swimStyle);
                return swimStyleStr;
        }
    }

    public String getWalkingTimeBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseMillSecondToDefaultFormattedTime((long) walkingSummary.getTrimSportDuration());
    }

    public String getSwimTotalTrips(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + sportSummary.getTotalTrips();
    }

    public String getSwimTotalStrokes(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + sportSummary.getTotalStrokes();
    }

    public String getSwimAvgDPS(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistance2YDOrMeter((double) sportSummary.getAvgDistancePerStroke(), sportSummary.getUnit()), false);
    }

    public String getSwimSwolf(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + sportSummary.getSwolfPerFixedMeters();
    }

    public String getSwimAvgStrokeSpeed(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + Math.round(sportSummary.getAvgStrokeSpeed() * 60.0f);
    }

    public String getSwimMaxStrokeSpeed(OutdoorSportSummary sportSummary) {
        if (sportSummary == null) {
            return "0";
        }
        return "" + Math.round(sportSummary.getMaxStrokeSpeed() * 60.0f);
    }

    public String getPauseTimeBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseMillSecondToDefaultFormattedTime((long) walkingSummary.getTotalPausedTime());
    }

    public String getExcahngeTimeBySummary(OutdoorSportSummary preSummary, OutdoorSportSummary nextSummary) {
        if (preSummary == null || nextSummary == null) {
            return "";
        }
        return DataFormatUtils.parseMillSecondToDefaultFormattedTime(nextSummary.getStartTime() - preSummary.getEndTime());
    }

    public String getCalorieBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) walkingSummary.getCalorie()))});
    }

    public String getAvergeSpeedBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(walkingSummary.getTotalSpeed())), true);
    }

    public String getMaxSpeedBySummary(OutdoorSportSummary walkingSummary) {
        if (walkingSummary == null) {
            return "";
        }
        return DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(walkingSummary.getMaxSpeed())), true);
    }

    public String getStepFreqBySummary(OutdoorSportSummary walkingSummary) {
        return String.valueOf(UnitConvertUtils.convertStepFreqToTimesPerMin((double) walkingSummary.getStepFreq()));
    }

    public String getStepStrideBySummary(OutdoorSportSummary sportSummary) {
        if (sportSummary == null || !isValidNormalStride(sportSummary.getAvgStride() * 100.0f)) {
            return "--";
        }
        return String.valueOf("" + ((int) UnitConvertUtils.convertCm2InOrCm((double) (sportSummary.getAvgStride() * 100.0f))));
    }

    public String getMaxStepFreqSummary(OutdoorSportSummary walkingSummary) {
        return String.valueOf(UnitConvertUtils.convertStepFreqToTimesPerMin((double) walkingSummary.getMaxStepFreq()));
    }

    public String getAverPaceBySummary(OutdoorSportSummary walkingSummary) {
        if (isValidNormalPace(walkingSummary.getTotalPace(), walkingSummary.getSportType())) {
            return DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(walkingSummary.getTotalPace()));
        }
        return "--";
    }

    public String getBesePaceBySummary(OutdoorSportSummary walkingSummary) {
        if (isValidNormalPace(walkingSummary.getBestPace(), walkingSummary.getSportType())) {
            return DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(walkingSummary.getBestPace()));
        }
        return "--";
    }

    public String getAverPaceBySummaryPer100meter(OutdoorSportSummary walkingSummary) {
        if (walkingSummary.getUnit() == 0) {
            return getPacePer100Meter(walkingSummary.getTotalPace());
        }
        return getPacePer100Meter(walkingSummary.getTotalPace(), walkingSummary.getUnit());
    }

    public String getBesePaceBySummaryPer100meter(OutdoorSportSummary walkingSummary) {
        return getPacePer100Meter(walkingSummary.getBestPace(), walkingSummary.getUnit());
    }

    public String getClimbDistanceBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int climbUp = (int) walkingSummary.getClimbdisascend();
        return "" + (climbUp < 0 ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbUp, walkingSummary.getSportType()))));
    }

    public String getDownHillDescendBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int climbDown = (int) walkingSummary.getClimbdisDescend();
        return "" + (climbDown < 0 ? this.mInvalidHint : DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) climbDown), false));
    }

    public String getClimbUpBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        float climbUp = walkingSummary.getClimbUp();
        return "" + (climbUp < 0.0f ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbUp, walkingSummary.getSportType()))));
    }

    public String getClimbDownBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        float climbDown = walkingSummary.getClimbDown();
        return "" + (climbDown < 0.0f ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbDown, walkingSummary.getSportType()))));
    }

    public String getDownHillClimbDownBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        float climbDown = walkingSummary.getClimbDown();
        return "" + (climbDown < 0.0f ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) climbDown, walkingSummary.getSportType()))));
    }

    public String getHighestAltitudeBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int highAltitude = Math.round(walkingSummary.getHighestAltitude());
        return "" + (highAltitude == -20000 ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) highAltitude, walkingSummary.getSportType()))));
    }

    public String getLowestAltitudeBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int lowAltitude = Math.round(walkingSummary.getLowestAltitude());
        return "" + (lowAltitude == -20000 ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) lowAltitude, walkingSummary.getSportType()))));
    }

    public String getAveAltitudeBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int lowAltitude = Math.round(walkingSummary.getLowestAltitude());
        int hightAltitude = Math.round(walkingSummary.getHighestAltitude());
        if (lowAltitude <= -20000 || hightAltitude <= -20000) {
            return this.mInvalidHint;
        }
        return "" + Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) (((float) (lowAltitude + hightAltitude)) / 2.0f), walkingSummary.getSportType()));
    }

    public String getMaxElevLossBySummary(Context mContext, OutdoorSportSummary walkingSummary) {
        this.mInvalidHint = getMInvalidHintString(mContext);
        int maxDescend = Math.round(walkingSummary.getDownhillMaxAltitudeDescend());
        if (maxDescend < 0) {
            return this.mInvalidHint;
        }
        return "" + (maxDescend < 0 ? this.mInvalidHint : Long.valueOf(Math.round(UnitConvertUtils.convertDistanceToFtOrMeter((double) maxDescend, walkingSummary.getSportType()))));
    }

    public String getMInvalidHintString(Context mContext) {
        if (this.mInvalidHint == null) {
            this.mInvalidHint = mContext.getString(C0532R.string.sport_pause_heart_shown);
        }
        return this.mInvalidHint;
    }

    public String getAvgHeartBySummary(Context context, OutdoorSportSummary sportSummary) {
        this.mInvalidHint = getMInvalidHintString(context);
        int avgHeart = sportSummary.getAvgHeartRate();
        return avgHeart <= 0 ? this.mInvalidHint : String.valueOf(avgHeart);
    }

    public String getMaxHeartBySummary(Context context, OutdoorSportSummary sportSummary) {
        this.mInvalidHint = getMInvalidHintString(context);
        int avgHeart = sportSummary.getMaxHeartRate();
        return avgHeart <= 0 ? this.mInvalidHint : String.valueOf(avgHeart);
    }

    public String getMinHeartBySummary(Context context, OutdoorSportSummary sportSummary) {
        this.mInvalidHint = getMInvalidHintString(context);
        int avgHeart = sportSummary.getMinHeartRate();
        return avgHeart <= 0 ? this.mInvalidHint : String.valueOf(avgHeart);
    }

    public String getDownhillNums(Context context, OutdoorSportSummary sportSummary) {
        return String.valueOf(sportSummary.getDownHillNum());
    }

    private String getPacePer100Meter(float pace) {
        Debug.m5i(TAG, "getPacePer100Meter, pace:" + pace);
        return DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(pace));
    }

    private String getPacePer100Meter(float pace, int unit) {
        Debug.m5i(TAG, "getPacePer100Meter, pace:" + pace);
        return DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter(pace, unit));
    }

    public String getKmOrmi(Context context) {
        if (UnitConvertUtils.isImperial()) {
            return context.getString(C0532R.string.km_imperial);
        }
        return context.getString(C0532R.string.km_metric);
    }

    public String getMeterOrFt(Context context) {
        if (UnitConvertUtils.isImperial()) {
            return context.getString(C0532R.string.metre_imperial);
        }
        return context.getString(C0532R.string.metre_metric);
    }

    public String getSwimUnitDesc(Context context, OutdoorSportSummary outdoorSportSummary) {
        if (outdoorSportSummary.getUnit() == 0) {
            return context.getString(C0532R.string.metre_metric);
        }
        return context.getString(C0532R.string.swim_unit);
    }

    public String getCm(Context context) {
        return context.getString(UnitConvertUtils.isImperial() ? C0532R.string.sport_stride_unit_imperial : C0532R.string.sport_stride_unit);
    }

    public String getPaceKmOrMeter(Context context) {
        if (UnitConvertUtils.isImperial()) {
            return context.getString(C0532R.string.km_imperial_pace_unit);
        }
        return context.getString(C0532R.string.km_metric_pace_unit);
    }

    public String getPaceHundredMeterOrFt(Context context, OutdoorSportSummary summary) {
        if (summary.getUnit() == 0) {
            return context.getString(C0532R.string.swim_pace_unit);
        }
        return context.getString(C0532R.string.pace_swim_unit_yd);
    }

    public String getSpeedKmOrMeter(Context context) {
        if (UnitConvertUtils.isImperial()) {
            return context.getString(C0532R.string.km_imperial_speed_unit);
        }
        return context.getString(C0532R.string.km_metric_speed_unit);
    }

    private boolean isValidNormalPace(float pace, int sportType) {
        if (Float.compare(pace, 0.0f) == 0) {
            return false;
        }
        int standardSec = 1800;
        if (sportType == 6) {
            standardSec = 3600;
        }
        if (((int) (1000.0f * pace)) <= standardSec) {
            return true;
        }
        Debug.m5i(TAG, "isValidNormalPace, false, pace:" + pace);
        return false;
    }

    private boolean isValidNormalStride(float stride) {
        UserInfo info = UserInfoManager.getInstance().getUserInfo(Global.getApplicationContext());
        int height = 170;
        if (info != null) {
            Debug.m5i(TAG, "isValidNormalStride, userInfo:" + info.toString());
            height = info.getHeight();
        }
        int minStride = (int) (((float) height) * 0.25f);
        if (stride <= ((float) height) && stride >= ((float) minStride)) {
            return true;
        }
        Debug.m5i(TAG, "isValidNormalStride, false, stride:" + stride);
        return false;
    }
}
