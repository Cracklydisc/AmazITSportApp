package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class RunningHistoryDetailDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = RunningHistoryDetailDelegate.class.getSimpleName();
    private boolean mIsFromEndSport = false;
    private float totalDistance = 0.0f;
    private OutdoorSportSummary walkingSummary;

    public RunningHistoryDetailDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        LogUtils.print(TAG, "dynamicAddView");
        this.walkingSummary = (OutdoorSportSummary) sportSummary;
        if (this.walkingSummary.getIntervalType() == 0) {
            this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_run);
        } else {
            this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_interval_run);
        }
        this.totalDistance = this.walkingSummary.getDistance();
        this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, this.walkingSummary));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getDistanceBySummary(this.walkingSummary), getKmOrmi(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(this.walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(this.walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(this.walkingSummary), ""));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_pace_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_pace), getAverPaceBySummary(this.walkingSummary), getPaceKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_best_pace), getBesePaceBySummary(this.walkingSummary), getPaceKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_step_freq_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_avg_step_freq_desc), getStepFreqBySummary(this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_max_step_freq_desc), getMaxStepFreqSummary(this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_stride), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_stride), getStepStrideBySummary(this.walkingSummary), getCm(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_speed_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_speed), getAvergeSpeedBySummary(this.walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_max_speed), getMaxSpeedBySummary(this.walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_absolute_altitude), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_high_altitude), getHighestAltitudeBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_low_altitude), getLowestAltitudeBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_altitude), getAveAltitudeBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_climb_up), getClimbDistanceBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_altitude), getClimbUpBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_down_altitude), getClimbDownBySummary(this.mActivity, this.walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, this.walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
        if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
            sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, this.walkingSummary));
        }
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addPaceLineChart(this.mActivity, null, null, sportSummary));
        if (!UnitConvertUtils.isHuangheMode()) {
            sportHistoryItemsView.addView(addDallyPerformenceView(this.mActivity, this.walkingSummary));
        }
        sportHistoryItemsView.addView(addStepFreqLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addAltitudeLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addHillView(this.mActivity, sportSummary));
        loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 5, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 4, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 3, this.mRequestListener, 1);
        loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
        if (!UnitConvertUtils.isHuangheMode()) {
            loadCurveImg(this.mActivity, this.mSportSummary, 12, this.mRequestListener, 2);
        }
    }

    public void destroyView() {
        super.destroyView();
    }

    public void onDailyPerpormentceDataReady(int[] data) {
        if (this.dailyPerformenceChatView != null && this.walkingSummary != null) {
            ((DailyPerformenceChatView) this.dailyPerformenceChatView).setSportStatusData(data, Double.parseDouble(getDistanceBySummary(this.walkingSummary)));
        }
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick:Id:" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }
}
