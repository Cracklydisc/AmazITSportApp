package com.huami.watch.newsport.ui.delegate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmKeyEventScrollView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType;
import com.huami.watch.newsport.curve.task.InflateCurveInfoTask.IInflateCurveInfoListener;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryRequest;
import com.huami.watch.newsport.ui.uiinterface.IDeleteSportSummaryListener;
import com.huami.watch.newsport.ui.uiinterface.IHistoryDetailDelegate;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import java.util.Calendar;
import java.util.List;

public abstract class BaseHistoryDetailDelegate extends BaseDelegateItem implements OnClickListener, IRecordGraphCache, IHistoryDetailDelegate {
    private static final String TAG = BaseHistoryDetailDelegate.class.getSimpleName();
    protected TextView historyDetaiSportTime;
    protected HmTextView historyDetailSportTitle;
    protected Activity mActivity;
    protected Bitmap mAltitudeBitmap = null;
    protected Bitmap mHeartBitmap = null;
    protected Bitmap mHeartPercentBitmap = null;
    protected boolean mIsPreparedCurveInfo = false;
    protected boolean mIsPreparedLocationCurveInfo = false;
    protected boolean mIsPreparedReservedInfo = false;
    protected boolean mIsViewInited = false;
    protected IDeleteSportSummaryListener mListener;
    private Object mLock = new Object();
    protected Bitmap mPaceBitmap = null;
    protected ISportHistoryRequest mRequestListener;
    protected Bitmap mRidingCadenceBitmap = null;
    protected Bitmap mSpeedBitmap = null;
    protected SportSummary mSportSummary = null;
    protected Bitmap mStepFerqBitmap = null;
    protected Bitmap mStrokeStrokeBitmap = null;
    protected HmLinearLayout mSwipeView = null;

    class C07341 implements IInflateCurveInfoListener {
        C07341() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mAltitudeView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheAltitudeView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mAltitudeView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 3);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07352 implements IInflateCurveInfoListener {
        C07352() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mHeartRateView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheHeartView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mHeartRateView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 1);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07363 implements IInflateCurveInfoListener {
        C07363() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mPaceView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCachePaceView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mPaceView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 4);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07374 implements IInflateCurveInfoListener {
        C07374() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mSpeedView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheSpeedView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mSpeedView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 2);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07385 implements IInflateCurveInfoListener {
        C07385() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mStepFreqView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheStepFreqView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mStepFreqView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 5);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07396 implements IInflateCurveInfoListener {
        C07396() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mStrokeView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheStrokeView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mStrokeView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 10);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    class C07407 implements IInflateCurveInfoListener {
        C07407() {
        }

        public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
            BaseHistoryDetailDelegate.this.mCadenceView.setVisibility(0);
            BaseHistoryDetailDelegate.this.mCacheCadenceView.setVisibility(8);
            if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mStrokeView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 11);
            } else {
                Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
            }
        }
    }

    protected abstract void dynamicAddView(HmLinearLayout hmLinearLayout, SportSummary sportSummary);

    public void setActivity(Activity activity) {
        this.mActivity = activity;
        this.mListener = (IDeleteSportSummaryListener) activity;
        this.mRequestListener = (ISportHistoryRequest) activity;
    }

    public View inflateView(SportSummary sportSummary, LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mSportSummary = sportSummary;
        View view = inflater.inflate(C0532R.layout.fragment_sport_history_detail_container, container, false);
        this.historyDetailSportTitle = (HmTextView) view.findViewById(C0532R.id.history_detail_sport_title);
        this.historyDetaiSportTime = (TextView) view.findViewById(C0532R.id.history_detail_sport_time);
        ArcScrollbarHelper.setArcScrollBarDrawable((HmKeyEventScrollView) view.findViewById(C0532R.id.history_detail_root));
        this.mSwipeView = (HmLinearLayout) view.findViewById(C0532R.id.swipe_view);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(this.mActivity, this.mSwipeView);
        dynamicAddView(this.mSwipeView, sportSummary);
        LogUtils.print(TAG, "inflateView");
        this.mIsViewInited = true;
        return view;
    }

    public void destroyView() {
        if (!(this.mHeartBitmap == null || this.mHeartBitmap.isRecycled())) {
            this.mHeartBitmap.recycle();
        }
        if (!(this.mAltitudeBitmap == null || this.mAltitudeBitmap.isRecycled())) {
            this.mAltitudeBitmap.recycle();
        }
        if (!(this.mStepFerqBitmap == null || this.mStepFerqBitmap.isRecycled())) {
            this.mStepFerqBitmap.recycle();
        }
        if (!(this.mPaceBitmap == null || this.mPaceBitmap.isRecycled())) {
            this.mPaceBitmap.recycle();
        }
        if (!(this.mHeartPercentBitmap == null || this.mHeartPercentBitmap.isRecycled())) {
            this.mHeartPercentBitmap.recycle();
        }
        if (!(this.mSpeedBitmap == null || this.mSpeedBitmap.isRecycled())) {
            this.mSpeedBitmap.recycle();
        }
        if (!(this.mStrokeStrokeBitmap == null || this.mStrokeStrokeBitmap.isRecycled())) {
            this.mStrokeStrokeBitmap.recycle();
        }
        if (!(this.mRidingCadenceBitmap == null || this.mRidingCadenceBitmap.isRecycled())) {
            this.mRidingCadenceBitmap.recycle();
        }
        RecordGraphManager.getInstance(this.mActivity).setRecordCache(null);
    }

    public void updateSummary(SportSummary sportSummary) {
    }

    public void updateChildList(List<SportSummary> list) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLocationDataReady(java.util.List<com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations> r12) {
        /*
        r11 = this;
        r5 = 1;
        if (r12 == 0) goto L_0x003e;
    L_0x0003:
        r8 = r12.iterator();
    L_0x0007:
        r0 = r8.hasNext();
        if (r0 == 0) goto L_0x003e;
    L_0x000d:
        r9 = r8.next();
        r9 = (com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations) r9;
        r0 = TAG;
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r2 = "locations.mTrackId: ";
        r1 = r1.append(r2);
        r2 = r9.mTrackId;
        r1 = r1.append(r2);
        r2 = ", mSportSummary.getTrackId():";
        r1 = r1.append(r2);
        r2 = r11.mSportSummary;
        r2 = r2.getTrackId();
        r1 = r1.append(r2);
        r1 = r1.toString();
        com.huami.watch.common.log.Debug.m5i(r0, r1);
        goto L_0x0007;
    L_0x003e:
        r1 = r11.mLock;
        monitor-enter(r1);
        r0 = r11.mIsPreparedLocationCurveInfo;	 Catch:{ all -> 0x00e5 }
        if (r0 != 0) goto L_0x0049;
    L_0x0045:
        r0 = r11.mIsViewInited;	 Catch:{ all -> 0x00e5 }
        if (r0 != 0) goto L_0x0071;
    L_0x0049:
        r0 = TAG;	 Catch:{ all -> 0x00e5 }
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00e5 }
        r2.<init>();	 Catch:{ all -> 0x00e5 }
        r3 = "onHeartDataReady, the curve has prepared,";
        r2 = r2.append(r3);	 Catch:{ all -> 0x00e5 }
        r3 = r11.mIsPreparedLocationCurveInfo;	 Catch:{ all -> 0x00e5 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x00e5 }
        r3 = ", inited:";
        r2 = r2.append(r3);	 Catch:{ all -> 0x00e5 }
        r3 = r11.mIsViewInited;	 Catch:{ all -> 0x00e5 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x00e5 }
        r2 = r2.toString();	 Catch:{ all -> 0x00e5 }
        com.huami.watch.common.log.Debug.m5i(r0, r2);	 Catch:{ all -> 0x00e5 }
        monitor-exit(r1);	 Catch:{ all -> 0x00e5 }
    L_0x0070:
        return;
    L_0x0071:
        r0 = 1;
        r11.mIsPreparedLocationCurveInfo = r0;	 Catch:{ all -> 0x00e5 }
        monitor-exit(r1);	 Catch:{ all -> 0x00e5 }
        r0 = r11.mAltitudeView;
        if (r0 == 0) goto L_0x0070;
    L_0x0079:
        r10 = 0;
        if (r12 == 0) goto L_0x0099;
    L_0x007c:
        r8 = r12.iterator();
    L_0x0080:
        r0 = r8.hasNext();
        if (r0 == 0) goto L_0x0099;
    L_0x0086:
        r9 = r8.next();
        r9 = (com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations) r9;
        r0 = r9.mTrackId;
        r2 = r11.mSportSummary;
        r2 = r2.getTrackId();
        r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
        if (r0 != 0) goto L_0x0080;
    L_0x0098:
        r10 = r9;
    L_0x0099:
        r0 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r1 = r11.mActivity;
        r2 = r11.mAltitudeView;
        r3 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r4 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.ALTITUDE;
        r6 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$1;
        r6.<init>();
        r7 = r11.mSportSummary;
        r7 = r7.getSportType();
        r0.<init>(r1, r2, r3, r4, r5, r6, r7);
        if (r10 != 0) goto L_0x00e8;
    L_0x00b3:
        r1 = 0;
    L_0x00b4:
        r0 = r0.setLocationDatas(r1);
        r1 = 3;
        r1 = new java.lang.Object[r1];
        r2 = 0;
        r3 = r11.mSportSummary;
        r6 = r3.getTrackId();
        r3 = java.lang.Long.valueOf(r6);
        r1[r2] = r3;
        r2 = r11.mSportSummary;
        r2 = r2.getStartTime();
        r2 = java.lang.Long.valueOf(r2);
        r1[r5] = r2;
        r2 = 2;
        r3 = r11.mSportSummary;
        r4 = r3.getEndTime();
        r3 = java.lang.Long.valueOf(r4);
        r1[r2] = r3;
        r0.execute(r1);
        goto L_0x0070;
    L_0x00e5:
        r0 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x00e5 }
        throw r0;
    L_0x00e8:
        r1 = r10.locationDatas;
        goto L_0x00b4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate.onLocationDataReady(java.util.List):void");
    }

    public void onDailyPerpormentceDataReady(int[] data) {
    }

    private int getDefaultSafeHeart(Context context) {
        UserInfo info = UserInfoManager.getInstance().getUserInfo(context);
        if (info == null) {
            Debug.m5i(TAG, "User info is null");
            return 180;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(1) - info.getYear();
        if (220 - year >= 0) {
            return 220 - year;
        }
        return 180;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHeartDataReady(java.util.List<? extends com.huami.watch.newsport.common.model.HeartRate> r31) {
        /*
        r30 = this;
        r7 = 1;
        r0 = r30;
        r3 = r0.mLock;
        monitor-enter(r3);
        r0 = r30;
        r2 = r0.mIsPreparedCurveInfo;	 Catch:{ all -> 0x009a }
        if (r2 != 0) goto L_0x0012;
    L_0x000c:
        r0 = r30;
        r2 = r0.mIsViewInited;	 Catch:{ all -> 0x009a }
        if (r2 != 0) goto L_0x003e;
    L_0x0012:
        r2 = TAG;	 Catch:{ all -> 0x009a }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009a }
        r4.<init>();	 Catch:{ all -> 0x009a }
        r5 = "onHeartDataReady, the curve has prepared,";
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r0 = r30;
        r5 = r0.mIsPreparedCurveInfo;	 Catch:{ all -> 0x009a }
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r5 = ", inited:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r0 = r30;
        r5 = r0.mIsViewInited;	 Catch:{ all -> 0x009a }
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r4 = r4.toString();	 Catch:{ all -> 0x009a }
        com.huami.watch.common.log.Debug.m5i(r2, r4);	 Catch:{ all -> 0x009a }
        monitor-exit(r3);	 Catch:{ all -> 0x009a }
    L_0x003d:
        return;
    L_0x003e:
        r0 = r30;
        r2 = r0.mSportSummary;	 Catch:{ all -> 0x009a }
        r2 = r2.getSportType();	 Catch:{ all -> 0x009a }
        r4 = 14;
        if (r2 != r4) goto L_0x0058;
    L_0x004a:
        r0 = r30;
        r2 = r0.mSportSummary;	 Catch:{ all -> 0x009a }
        r2 = (com.huami.watch.newsport.common.model.OutdoorSportSummary) r2;	 Catch:{ all -> 0x009a }
        r2 = r2.getTotalTrips();	 Catch:{ all -> 0x009a }
        r4 = 2;
        if (r2 >= r4) goto L_0x0058;
    L_0x0057:
        r7 = 0;
    L_0x0058:
        r2 = 1;
        r0 = r30;
        r0.mIsPreparedCurveInfo = r2;	 Catch:{ all -> 0x009a }
        monitor-exit(r3);	 Catch:{ all -> 0x009a }
        r10 = 0;
        if (r31 == 0) goto L_0x009f;
    L_0x0061:
        r0 = r30;
        r2 = r0.mSportSummary;
        r2 = r2.getSportType();
        r2 = com.huami.watch.newsport.common.model.SportType.isChildSport(r2);
        if (r2 == 0) goto L_0x009d;
    L_0x006f:
        r10 = new java.util.ArrayList;
        r10.<init>();
        r12 = r31.iterator();
    L_0x0078:
        r2 = r12.hasNext();
        if (r2 == 0) goto L_0x009f;
    L_0x007e:
        r22 = r12.next();
        r22 = (com.huami.watch.newsport.common.model.HeartRate) r22;
        r2 = r22.getTrackId();
        r0 = r30;
        r4 = r0.mSportSummary;
        r4 = r4.getTrackId();
        r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
        if (r2 != 0) goto L_0x0078;
    L_0x0094:
        r0 = r22;
        r10.add(r0);
        goto L_0x0078;
    L_0x009a:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x009a }
        throw r2;
    L_0x009d:
        r10 = r31;
    L_0x009f:
        r0 = r30;
        r2 = r0.mHeartPercentView;
        if (r2 == 0) goto L_0x00d9;
    L_0x00a5:
        if (r10 == 0) goto L_0x00ad;
    L_0x00a7:
        r2 = r10.isEmpty();
        if (r2 == 0) goto L_0x035e;
    L_0x00ad:
        r2 = 5;
        r15 = new int[r2];
        r2 = 0;
        r3 = 0;
        r15[r2] = r3;
        r2 = 1;
        r3 = 0;
        r15[r2] = r3;
        r2 = 2;
        r3 = 0;
        r15[r2] = r3;
        r2 = 3;
        r3 = 0;
        r15[r2] = r3;
        r2 = 4;
        r3 = 0;
        r3 = r15[r3];
        r3 = 100 - r3;
        r4 = 1;
        r4 = r15[r4];
        r3 = r3 - r4;
        r4 = 2;
        r4 = r15[r4];
        r3 = r3 - r4;
        r4 = 3;
        r4 = r15[r4];
        r3 = r3 - r4;
        r15[r2] = r3;
        r0 = r30;
        r0.updatePercentView(r15);
    L_0x00d9:
        r0 = r30;
        r2 = r0.mSportSummary;
        com.huami.watch.newsport.curve.CurveInfo.CurveInfoUtils.optimizeHeartRateWithoutPauseTime(r2, r10);
        r0 = r30;
        r2 = r0.mHeartRateView;
        if (r2 == 0) goto L_0x014a;
    L_0x00e6:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mHeartRateView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.HEART_RATE;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$2;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
    L_0x014a:
        r0 = r30;
        r2 = r0.mPaceView;
        if (r2 == 0) goto L_0x01b4;
    L_0x0150:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mPaceView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.PACE;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$3;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
    L_0x01b4:
        r0 = r30;
        r2 = r0.mSpeedView;
        if (r2 == 0) goto L_0x021e;
    L_0x01ba:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mSpeedView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.SPEED;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$4;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
    L_0x021e:
        r0 = r30;
        r2 = r0.mStepFreqView;
        if (r2 == 0) goto L_0x0288;
    L_0x0224:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mStepFreqView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.STEPFERQ;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$5;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
    L_0x0288:
        r0 = r30;
        r2 = r0.mStrokeView;
        if (r2 == 0) goto L_0x02f2;
    L_0x028e:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mStrokeView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.STROKE;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$6;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
    L_0x02f2:
        r0 = r30;
        r2 = r0.mCadenceView;
        if (r2 == 0) goto L_0x003d;
    L_0x02f8:
        r2 = new com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
        r0 = r30;
        r3 = r0.mActivity;
        r0 = r30;
        r4 = r0.mCadenceView;
        r5 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType.TIME;
        r6 = com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType.CADENCE;
        r8 = new com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate$7;
        r0 = r30;
        r8.<init>();
        r0 = r30;
        r9 = r0.mSportSummary;
        r9 = r9.getSportType();
        r2.<init>(r3, r4, r5, r6, r7, r8, r9);
        r2 = r2.setHeartRateDatas(r10);
        r3 = 3;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getTrackId();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 1;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getStartTime();
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r4 = 2;
        r0 = r30;
        r5 = r0.mSportSummary;
        r8 = r5.getEndTime();
        r0 = r30;
        r5 = r0.mSportSummary;
        r5 = r5.getTotalPausedTime();
        r0 = (long) r5;
        r28 = r0;
        r8 = r8 - r28;
        r5 = java.lang.Long.valueOf(r8);
        r3[r4] = r5;
        r2.execute(r3);
        goto L_0x003d;
    L_0x035e:
        r0 = r30;
        r2 = r0.mActivity;
        r0 = r30;
        r14 = r0.getDefaultSafeHeart(r2);
        r2 = 5;
        r0 = new float[r2];
        r21 = r0;
        r2 = 0;
        r3 = (float) r14;
        r4 = 1056964608; // 0x3f000000 float:0.5 double:5.222099017E-315;
        r3 = r3 * r4;
        r3 = java.lang.String.valueOf(r3);
        r4 = 1;
        r3 = com.huami.watch.newsport.utils.NumeriConversionUtils.getFloatValue(r3, r4);
        r21[r2] = r3;
        r2 = 1;
        r3 = (float) r14;
        r4 = 1058642330; // 0x3f19999a float:0.6 double:5.230388065E-315;
        r3 = r3 * r4;
        r3 = java.lang.String.valueOf(r3);
        r4 = 1;
        r3 = com.huami.watch.newsport.utils.NumeriConversionUtils.getFloatValue(r3, r4);
        r21[r2] = r3;
        r2 = 2;
        r3 = (float) r14;
        r4 = 1060320051; // 0x3f333333 float:0.7 double:5.23867711E-315;
        r3 = r3 * r4;
        r3 = java.lang.String.valueOf(r3);
        r4 = 1;
        r3 = com.huami.watch.newsport.utils.NumeriConversionUtils.getFloatValue(r3, r4);
        r21[r2] = r3;
        r2 = 3;
        r3 = (float) r14;
        r4 = 1061997773; // 0x3f4ccccd float:0.8 double:5.246966156E-315;
        r3 = r3 * r4;
        r3 = java.lang.String.valueOf(r3);
        r4 = 1;
        r3 = com.huami.watch.newsport.utils.NumeriConversionUtils.getFloatValue(r3, r4);
        r21[r2] = r3;
        r2 = 4;
        r3 = (float) r14;
        r4 = 1063675494; // 0x3f666666 float:0.9 double:5.2552552E-315;
        r3 = r3 * r4;
        r3 = java.lang.String.valueOf(r3);
        r4 = 1;
        r3 = com.huami.watch.newsport.utils.NumeriConversionUtils.getFloatValue(r3, r4);
        r21[r2] = r3;
        r2 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "maxHeart:";
        r4 = r4.append(r5);
        r4 = r4.append(r14);
        r5 = ", heart0:";
        r4 = r4.append(r5);
        r5 = 0;
        r5 = r21[r5];
        r4 = r4.append(r5);
        r5 = ", heart1:";
        r4 = r4.append(r5);
        r5 = 1;
        r5 = r21[r5];
        r4 = r4.append(r5);
        r5 = ", heart2:";
        r4 = r4.append(r5);
        r5 = 2;
        r5 = r21[r5];
        r4 = r4.append(r5);
        r5 = ", heart3:";
        r4 = r4.append(r5);
        r5 = 3;
        r5 = r21[r5];
        r4 = r4.append(r5);
        r5 = ", heart4:";
        r4 = r4.append(r5);
        r5 = 4;
        r5 = r21[r5];
        r4 = r4.append(r5);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        r23 = 0;
        r16 = 0;
        r17 = 0;
        r18 = 0;
        r19 = 0;
        r20 = 0;
        r13 = new java.util.ArrayList;
        r13.<init>();
        r12 = r10.iterator();
    L_0x0431:
        r2 = r12.hasNext();
        if (r2 == 0) goto L_0x044a;
    L_0x0437:
        r22 = r12.next();
        r22 = (com.huami.watch.newsport.common.model.HeartRate) r22;
        r2 = r22.getHeartQuality();
        r3 = 1;
        if (r2 > r3) goto L_0x0431;
    L_0x0444:
        r0 = r22;
        r13.add(r0);
        goto L_0x0431;
    L_0x044a:
        r2 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "size:";
        r4 = r4.append(r5);
        r5 = r13.size();
        r4 = r4.append(r5);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        r24 = 0;
        r23 = 0;
        r11 = 0;
    L_0x046d:
        r2 = r13.size();
        if (r11 >= r2) goto L_0x0589;
    L_0x0473:
        r2 = r13.get(r11);
        r2 = (com.huami.watch.newsport.common.model.HeartRate) r2;
        r26 = r2.getHeartRate();
        r2 = r13.size();
        r2 = r2 + -1;
        if (r11 >= r2) goto L_0x04c7;
    L_0x0485:
        r2 = r11 + 1;
        r2 = r13.get(r2);
        r2 = (com.huami.watch.newsport.common.model.HeartRate) r2;
        r4 = r2.getTimestamp();
        r2 = r13.get(r11);
        r2 = (com.huami.watch.newsport.common.model.HeartRate) r2;
        r2 = r2.getTimestamp();
        r24 = r4 - r2;
    L_0x049d:
        r2 = 60000; // 0xea60 float:8.4078E-41 double:2.9644E-319;
        r2 = (r24 > r2 ? 1 : (r24 == r2 ? 0 : -1));
        if (r2 <= 0) goto L_0x04a6;
    L_0x04a4:
        r24 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
    L_0x04a6:
        r0 = r26;
        r2 = (float) r0;
        r3 = 4;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 < 0) goto L_0x04ca;
    L_0x04b0:
        r0 = r26;
        if (r0 >= r14) goto L_0x04ca;
    L_0x04b4:
        r0 = r16;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r16 = r0;
    L_0x04bc:
        r0 = r23;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r23 = r0;
    L_0x04c4:
        r11 = r11 + 1;
        goto L_0x046d;
    L_0x04c7:
        r24 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        goto L_0x049d;
    L_0x04ca:
        r0 = r26;
        r2 = (float) r0;
        r3 = 3;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 < 0) goto L_0x04e7;
    L_0x04d4:
        r0 = r26;
        r2 = (float) r0;
        r3 = 4;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 >= 0) goto L_0x04e7;
    L_0x04de:
        r0 = r17;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r17 = r0;
        goto L_0x04bc;
    L_0x04e7:
        r0 = r26;
        r2 = (float) r0;
        r3 = 2;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 < 0) goto L_0x054d;
    L_0x04f1:
        r0 = r26;
        r2 = (float) r0;
        r3 = 3;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 >= 0) goto L_0x054d;
    L_0x04fb:
        r0 = r18;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r18 = r0;
        r3 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;
        r4 = TAG;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r5 = "value:";
        r2 = r2.append(r5);
        r0 = r26;
        r2 = r2.append(r0);
        r5 = ", time:";
        r2 = r2.append(r5);
        r0 = r24;
        r2 = r2.append(r0);
        r5 = ", range:";
        r2 = r2.append(r5);
        r0 = r18;
        r2 = r2.append(r0);
        r5 = ", curTime:";
        r5 = r2.append(r5);
        r2 = r13.get(r11);
        r2 = (com.huami.watch.newsport.common.model.HeartRate) r2;
        r8 = r2.getTimestamp();
        r2 = r5.append(r8);
        r2 = r2.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r3, r4, r2);
        goto L_0x04bc;
    L_0x054d:
        r0 = r26;
        r2 = (float) r0;
        r3 = 1;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 < 0) goto L_0x056b;
    L_0x0557:
        r0 = r26;
        r2 = (float) r0;
        r3 = 2;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 >= 0) goto L_0x056b;
    L_0x0561:
        r0 = r19;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r19 = r0;
        goto L_0x04bc;
    L_0x056b:
        r0 = r26;
        r2 = (float) r0;
        r3 = 0;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 < 0) goto L_0x04c4;
    L_0x0575:
        r0 = r26;
        r2 = (float) r0;
        r3 = 1;
        r3 = r21[r3];
        r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1));
        if (r2 >= 0) goto L_0x04c4;
    L_0x057f:
        r0 = r20;
        r2 = (long) r0;
        r2 = r2 + r24;
        r0 = (int) r2;
        r20 = r0;
        goto L_0x04bc;
    L_0x0589:
        r2 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "range0:";
        r4 = r4.append(r5);
        r0 = r16;
        r4 = r4.append(r0);
        r5 = ", range1:";
        r4 = r4.append(r5);
        r0 = r17;
        r4 = r4.append(r0);
        r5 = ", range2:";
        r4 = r4.append(r5);
        r0 = r18;
        r4 = r4.append(r0);
        r5 = ", range3:";
        r4 = r4.append(r5);
        r0 = r19;
        r4 = r4.append(r0);
        r5 = ", range4:";
        r4 = r4.append(r5);
        r0 = r20;
        r4 = r4.append(r0);
        r5 = ", total:";
        r4 = r4.append(r5);
        r0 = r23;
        r4 = r4.append(r0);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        if (r23 != 0) goto L_0x0601;
    L_0x05e3:
        r2 = 5;
        r15 = new int[r2];
        r2 = 0;
        r3 = 0;
        r15[r2] = r3;
        r2 = 1;
        r3 = 0;
        r15[r2] = r3;
        r2 = 2;
        r3 = 0;
        r15[r2] = r3;
        r2 = 3;
        r3 = 0;
        r15[r2] = r3;
        r2 = 4;
        r3 = 0;
        r15[r2] = r3;
        r0 = r30;
        r0.updatePercentView(r15);
        goto L_0x00d9;
    L_0x0601:
        r2 = 5;
        r15 = new int[r2];
        r2 = 0;
        r0 = r16;
        r3 = (float) r0;
        r0 = r23;
        r4 = (float) r0;
        r3 = r3 / r4;
        r4 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
        r3 = r3 * r4;
        r3 = (int) r3;
        r15[r2] = r3;
        r2 = 1;
        r0 = r17;
        r3 = (float) r0;
        r0 = r23;
        r4 = (float) r0;
        r3 = r3 / r4;
        r4 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
        r3 = r3 * r4;
        r3 = (int) r3;
        r15[r2] = r3;
        r2 = 2;
        r0 = r18;
        r3 = (float) r0;
        r0 = r23;
        r4 = (float) r0;
        r3 = r3 / r4;
        r4 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
        r3 = r3 * r4;
        r3 = (int) r3;
        r15[r2] = r3;
        r2 = 3;
        r0 = r19;
        r3 = (float) r0;
        r0 = r23;
        r4 = (float) r0;
        r3 = r3 / r4;
        r4 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
        r3 = r3 * r4;
        r3 = (int) r3;
        r15[r2] = r3;
        r2 = 4;
        r3 = 0;
        r3 = r15[r3];
        r3 = 100 - r3;
        r4 = 1;
        r4 = r15[r4];
        r3 = r3 - r4;
        r4 = 2;
        r4 = r15[r4];
        r3 = r3 - r4;
        r4 = 3;
        r4 = r15[r4];
        r3 = r3 - r4;
        r15[r2] = r3;
        r0 = r30;
        r0.updatePercentView(r15);
        goto L_0x00d9;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.ui.delegate.BaseHistoryDetailDelegate.onHeartDataReady(java.util.List):void");
    }

    private void updatePercentView(final int[] percents) {
        Global.getGlobalUIHandler().post(new Runnable() {

            class C07411 implements Runnable {
                C07411() {
                }

                public void run() {
                    if (BaseHistoryDetailDelegate.this.mSportSummary != null) {
                        RecordGraphManager.getInstance(BaseHistoryDetailDelegate.this.mActivity).saveGraphData2Cache(BaseHistoryDetailDelegate.this.mHeartPercentView, BaseHistoryDetailDelegate.this.mSportSummary.getTrackId(), 9);
                    } else {
                        Debug.m5i(BaseHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (BaseHistoryDetailDelegate.this.mSportSummary != null ? Integer.valueOf(BaseHistoryDetailDelegate.this.mSportSummary.getCurrentStatus()) : "summary is null"));
                    }
                }
            }

            public void run() {
                BaseHistoryDetailDelegate.this.updateHeartPercentView(BaseHistoryDetailDelegate.this.mActivity, percents);
                Global.getGlobalWorkHandler().postDelayed(new C07411(), 500);
            }
        });
    }

    public void onDrawableFound(long trackId, Bitmap bitmap, int graphType) {
        if (graphType == 1) {
            if (this.mCacheHeartView != null && bitmap != null) {
                this.mHeartBitmap = bitmap;
                this.mCacheHeartView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mHeartBitmap));
                this.mHeartRateView.setVisibility(8);
            }
        } else if (graphType == 3) {
            if (this.mCacheAltitudeView != null && bitmap != null) {
                this.mAltitudeBitmap = bitmap;
                this.mCacheAltitudeView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mAltitudeBitmap));
                this.mAltitudeView.setVisibility(8);
            }
        } else if (graphType == 4) {
            if (this.mCachePaceView != null && bitmap != null) {
                this.mPaceBitmap = bitmap;
                this.mCachePaceView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mPaceBitmap));
                this.mPaceView.setVisibility(8);
            }
        } else if (graphType == 5) {
            if (this.mCacheStepFreqView != null && bitmap != null) {
                this.mStepFerqBitmap = bitmap;
                this.mCacheStepFreqView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mStepFerqBitmap));
                this.mStepFreqView.setVisibility(8);
            }
        } else if (graphType == 9) {
            if (this.mCachePercentView != null && bitmap != null) {
                this.mHeartPercentBitmap = bitmap;
                this.mCachePercentView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mHeartPercentBitmap));
                this.mHeartPercentView.setVisibility(8);
            }
        } else if (graphType == 2) {
            if (this.mCacheSpeedView != null && bitmap != null) {
                this.mSpeedBitmap = bitmap;
                this.mCacheSpeedView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mSpeedBitmap));
                this.mSpeedView.setVisibility(8);
            }
        } else if (graphType == 10) {
            if (this.mCacheStrokeView != null && bitmap != null) {
                this.mStrokeStrokeBitmap = bitmap;
                this.mCacheStrokeView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mStrokeStrokeBitmap));
                this.mStrokeView.setVisibility(8);
            }
        } else if (graphType == 11 && this.mCacheCadenceView != null && bitmap != null) {
            this.mRidingCadenceBitmap = bitmap;
            this.mCacheCadenceView.setBackground(new BitmapDrawable(this.mActivity.getResources(), this.mRidingCadenceBitmap));
            this.mCadenceView.setVisibility(8);
        }
    }

    public void onSaveCacheResult(int graphType, boolean isSuccess) {
        Debug.m5i(TAG, "onSaveCacheResult, graph type:" + graphType + ", " + isSuccess);
    }

    public void onDeleteCacheResult(int graphType, boolean isSuccess) {
        Debug.m5i(TAG, "onDeleteCacheResult, graph type:" + graphType + ", " + isSuccess);
    }
}
