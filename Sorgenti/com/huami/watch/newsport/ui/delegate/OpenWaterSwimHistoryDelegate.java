package com.huami.watch.newsport.ui.delegate;

import android.content.Context;
import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class OpenWaterSwimHistoryDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = OpenWaterSwimHistoryDelegate.class.getName();
    private boolean mIsFromEndSport = false;

    public OpenWaterSwimHistoryDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        LogUtils.print(TAG, "dynamicAddView");
        if (sportSummary != null) {
            String string;
            this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_open_water_swim);
            OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
            this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, walkingSummary));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getSwimDistanceBySummary(walkingSummary), getSwimUnitDesc(this.mActivity, walkingSummary)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, getSwimStyleTitle(this.mActivity, walkingSummary), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, null, "", getSwimStyle(this.mActivity, walkingSummary)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_stroke), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_stroke_times), getSwimTotalStrokes(walkingSummary), this.mActivity.getResources().getString(C0532R.string.stroke_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_avg_dps), getSwimAvgDPS(walkingSummary), getSwimUnitDesc(this.mActivity, walkingSummary)));
            Context context = this.mActivity;
            String string2 = this.mActivity.getResources().getString(C0532R.string.swim_swolf);
            String swimSwolf = getSwimSwolf(walkingSummary);
            if (walkingSummary.getUnit() == 0) {
                string = this.mActivity.getResources().getString(C0532R.string.swim_swolf_unit, new Object[]{String.valueOf(((OutdoorSportSummary) this.mSportSummary).getSwimPoolLength())});
            } else {
                string = this.mActivity.getResources().getString(C0532R.string.swim_swolf_unit_yd, new Object[]{String.valueOf(Math.round(UnitConvertUtils.convertDistance2YDOrMeter((double) ((OutdoorSportSummary) this.mSportSummary).getSwimPoolLength(), ((OutdoorSportSummary) this.mSportSummary).getUnit())))});
            }
            sportHistoryItemsView.addView(addChildItem(context, string2, swimSwolf, string));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_stroke_speed), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_avg_stroke_speed), getSwimAvgStrokeSpeed(walkingSummary), this.mActivity.getString(C0532R.string.swim_stroke_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_max_stroke_speed), getSwimMaxStrokeSpeed(walkingSummary), this.mActivity.getString(C0532R.string.swim_stroke_unit)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.swim_pace), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_pace), getAverPaceBySummaryPer100meter(walkingSummary), getPaceHundredMeterOrFt(this.mActivity, walkingSummary)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_best_pace), getBesePaceBySummaryPer100meter(walkingSummary), getPaceHundredMeterOrFt(this.mActivity, walkingSummary)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addStrokeLineChart(this.mActivity, null, null, sportSummary));
            sportHistoryItemsView.addView(addPaceLineChart(this.mActivity, null, null, sportSummary));
            loadCurveImg(this.mActivity, this.mSportSummary, 4, this.mRequestListener, 0);
            loadCurveImg(this.mActivity, this.mSportSummary, 10, this.mRequestListener, 0);
        }
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick:Id:" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }
}
