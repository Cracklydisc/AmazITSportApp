package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import java.util.List;

public class CompoundHistoryDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = CompoundHistoryDelegate.class.getName();
    private List<SportSummary> mChildList = null;
    private TextView mFirstView = null;
    private boolean mIsFromEndSport = false;
    private TextView mSecView = null;

    public CompoundHistoryDelegate(boolean isFinishFromEndSport, List<SportSummary> childList) {
        this.mIsFromEndSport = isFinishFromEndSport;
        this.mChildList = childList;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_compound);
        if (sportSummary != null) {
            OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
            this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, walkingSummary));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
            if (this.mSportSummary.getChildSportSummrys().size() > 1) {
                View view = addChildItem(this.mActivity, getChildExchangeTitle(this.mActivity, ((Integer) this.mSportSummary.getChildTypes().get(1)).intValue()), "", "");
                this.mFirstView = (TextView) view.findViewById(C0532R.id.number_text_view);
                sportHistoryItemsView.addView(view);
                if (this.mSportSummary.getChildSportSummrys().size() > 2) {
                    View secView = addChildItem(this.mActivity, getChildExchangeTitle(this.mActivity, ((Integer) this.mSportSummary.getChildTypes().get(2)).intValue()), "", "");
                    this.mSecView = (TextView) secView.findViewById(C0532R.id.number_text_view);
                    sportHistoryItemsView.addView(secView);
                }
            }
            updateChildList(this.mChildList);
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        }
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick:Id:" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }

    public void updateChildList(List<SportSummary> childSummaryList) {
        if (childSummaryList != null && this.mFirstView != null) {
            if (childSummaryList.size() > 1) {
                this.mFirstView.setText(getExcahngeTimeBySummary((OutdoorSportSummary) this.mChildList.get(0), (OutdoorSportSummary) this.mChildList.get(1)));
            }
            if (childSummaryList.size() > 2) {
                this.mSecView.setText(getExcahngeTimeBySummary((OutdoorSportSummary) this.mChildList.get(1), (OutdoorSportSummary) this.mChildList.get(2)));
            }
        }
    }
}
