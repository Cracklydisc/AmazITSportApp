package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class TennisHistoryDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = TennisHistoryDelegate.class.getSimpleName();
    private boolean mIsFromEndSport = false;

    public TennisHistoryDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        LogUtils.print(TAG, "dynamicAddView");
        if (sportSummary != null) {
            OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
            this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_tennis);
            this.historyDetaiSportTime.setText(walkingSummary == null ? "" : getStartTimeString(this.mActivity, walkingSummary));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_history_total_stokes), String.valueOf(walkingSummary.getmStrokes()), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
            int total = walkingSummary.getmStrokes();
            int foreHand = walkingSummary.getmForeHand();
            int backHand = walkingSummary.getmBackHand();
            int serve = walkingSummary.getmServe();
            int foreHandPercent = 0;
            int backHandPercent = 0;
            int mServerPercent = 0;
            if (total > 0) {
                foreHandPercent = (foreHand * 100) / total;
                if (backHand + serve > 0) {
                    backHandPercent = (int) (((float) (100 - foreHandPercent)) * (((float) backHand) / ((float) (backHand + serve))));
                }
                mServerPercent = (100 - foreHandPercent) - backHandPercent;
            }
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_tennis_strokes), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_tennis_forehand) + "   " + foreHandPercent + "%", String.valueOf(walkingSummary.getmForeHand()), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_tennis_backhand) + "   " + backHandPercent + "%", String.valueOf(walkingSummary.getmBackHand()), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_tennis_serve) + "   " + mServerPercent + "%", String.valueOf(walkingSummary.getmServe()), ""));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
            if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
                sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
            }
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
            loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
            loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
        }
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick :" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }
}
