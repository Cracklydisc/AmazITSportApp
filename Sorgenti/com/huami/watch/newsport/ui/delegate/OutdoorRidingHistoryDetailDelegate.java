package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.XCoordinateType;
import com.huami.watch.newsport.curve.controller.CurveInfoPointFactory.YCoordinateType;
import com.huami.watch.newsport.curve.task.InflateCurveInfoTask;
import com.huami.watch.newsport.curve.task.InflateCurveInfoTask.IInflateCurveInfoListener;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class OutdoorRidingHistoryDetailDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = OutdoorRidingHistoryDetailDelegate.class.getSimpleName();
    private SyncDatabaseManager<CyclingDetail> mCadenceManager = null;
    private boolean mIsFromEndSport = false;
    private View mRidingCadenceView = null;

    class C07461 implements Runnable {
        final /* synthetic */ OutdoorRidingHistoryDetailDelegate this$0;

        class C07451 implements Runnable {

            class C07441 implements IInflateCurveInfoListener {
                C07441() {
                }

                public void onLoadFinished(XCoordinateType xType, YCoordinateType yType) {
                    if (C07461.this.this$0.mSportSummary != null) {
                        RecordGraphManager.getInstance(C07461.this.this$0.mActivity).saveGraphData2Cache(C07461.this.this$0.mCadenceView, C07461.this.this$0.mSportSummary.getTrackId(), 11);
                    } else {
                        Debug.m5i(OutdoorRidingHistoryDetailDelegate.TAG, "not saved the bitmap, sport status:" + (C07461.this.this$0.mSportSummary != null ? Integer.valueOf(C07461.this.this$0.mSportSummary.getCurrentStatus()) : "summary is null"));
                    }
                }
            }

            C07451() {
            }

            public void run() {
                if (C07461.this.this$0.mCadenceView != null) {
                    C07461.this.this$0.mRidingCadenceView.setVisibility(0);
                    new InflateCurveInfoTask(C07461.this.this$0.mActivity, C07461.this.this$0.mCadenceView, XCoordinateType.TIME, YCoordinateType.CADENCE, true, new C07441(), C07461.this.this$0.mSportSummary.getSportType()).execute(new Object[]{Long.valueOf(C07461.this.this$0.mSportSummary.getTrackId()), Long.valueOf(C07461.this.this$0.mSportSummary.getStartTime()), Long.valueOf(C07461.this.this$0.mSportSummary.getEndTime() - ((long) C07461.this.this$0.mSportSummary.getTotalPausedTime()))});
                }
            }
        }

        public void run() {
            this.this$0.mCadenceManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(this.this$0.mActivity));
            List<? extends CyclingDetail> cyclingDetaiList = this.this$0.mCadenceManager.selectAll(this.this$0.mActivity, "track_id>=?", new String[]{"" + this.this$0.mSportSummary.getTrackId()}, "cur_time ASC", null);
            if (cyclingDetaiList != null && !cyclingDetaiList.isEmpty()) {
                Global.getGlobalUIHandler().post(new C07451());
            }
        }
    }

    public OutdoorRidingHistoryDetailDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
        this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_outdoor_riding);
        this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, walkingSummary));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getDistanceBySummary(walkingSummary), getKmOrmi(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_speed_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_speed), getAvergeSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_max_speed), getMaxSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_absolute_altitude), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_high_altitude), getHighestAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_low_altitude), getLowestAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_altitude), getAveAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_climb_up), getClimbDistanceBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_altitude), getClimbUpBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_down_altitude), getClimbDownBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
        if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
            sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
        }
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addSpeedLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addAltitudeLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addHillView(this.mActivity, sportSummary));
        loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 2, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 3, this.mRequestListener, 1);
        loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick :" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }
}
