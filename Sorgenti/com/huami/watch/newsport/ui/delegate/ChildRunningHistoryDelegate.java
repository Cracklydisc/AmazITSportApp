package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class ChildRunningHistoryDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = ChildRunningHistoryDelegate.class.getName();
    private boolean mIsFromEndSport = false;

    public ChildRunningHistoryDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        LogUtils.print(TAG, "dynamicAddView");
        this.historyDetailSportTitle.setText(C0532R.string.sport_child_running);
        if (sportSummary != null) {
            OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getDistanceBySummary(walkingSummary), getKmOrmi(this.mActivity)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_pace_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_pace), getAverPaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_best_pace), getBesePaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_step_freq_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_avg_step_freq_desc), getStepFreqBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_max_step_freq_desc), getMaxStepFreqSummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_stride), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_stride), getStepStrideBySummary(walkingSummary), getCm(this.mActivity)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_speed_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_speed), getAvergeSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_max_speed), getMaxSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_absolute_altitude), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_high_altitude), getHighestAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_low_altitude), getLowestAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_altitude), getAveAltitudeBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_climb_up), getClimbDistanceBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_altitude), getClimbUpBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.outdoor_climb_down_altitude), getClimbDownBySummary(this.mActivity, walkingSummary), getMeterOrFt(this.mActivity)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
            sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
            if (!UnitConvertUtils.isHuangheMode()) {
                sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
            }
            sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
            sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
            sportHistoryItemsView.addView(addPaceLineChart(this.mActivity, null, null, sportSummary));
            if (!UnitConvertUtils.isHuangheMode()) {
                sportHistoryItemsView.addView(addDallyPerformenceView(this.mActivity, walkingSummary));
            }
            sportHistoryItemsView.addView(addStepFreqLineChart(this.mActivity, null, null, sportSummary));
            sportHistoryItemsView.addView(addAltitudeLineChart(this.mActivity, null, null, sportSummary));
            sportHistoryItemsView.addView(addHillView(this.mActivity, sportSummary));
            loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
            loadCurveImg(this.mActivity, this.mSportSummary, 5, this.mRequestListener, 0);
            loadCurveImg(this.mActivity, this.mSportSummary, 4, this.mRequestListener, 0);
            loadCurveImg(this.mActivity, this.mSportSummary, 3, this.mRequestListener, 1);
            loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
            if (!UnitConvertUtils.isHuangheMode()) {
                loadCurveImg(this.mActivity, this.mSportSummary, 12, this.mRequestListener, 2);
            }
        }
    }

    public void destroyView() {
        super.destroyView();
    }

    public void onClick(View v) {
    }
}
