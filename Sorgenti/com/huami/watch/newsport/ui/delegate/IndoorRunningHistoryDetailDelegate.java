package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class IndoorRunningHistoryDetailDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = IndoorRunningHistoryDetailDelegate.class.getSimpleName();
    private HmTextView mDistanceView;
    private boolean mIsFromEndSport = false;
    private View mModifyBtn;
    private HmTextView mPaceTextView;
    private HmTextView mSpeedTextView;

    public IndoorRunningHistoryDetailDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        this.historyDetailSportTitle.setText(C0532R.string.history_detail_indoor_running_title);
        OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
        this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, walkingSummary));
        View view = addModifyItem(this.mActivity, this);
        this.mModifyBtn = view;
        sportHistoryItemsView.addView(view);
        if (!this.mIsFromEndSport) {
            this.mModifyBtn.setVisibility(8);
        }
        view = addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getDistanceBySummary(walkingSummary), getKmOrmi(this.mActivity));
        this.mDistanceView = (HmTextView) view.findViewById(C0532R.id.number_text_view);
        sportHistoryItemsView.addView(view);
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_pace_desc), C0532R.color.default_font_color_white));
        view = addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_pace), getAverPaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity));
        this.mPaceTextView = (HmTextView) view.findViewById(C0532R.id.number_text_view);
        sportHistoryItemsView.addView(view);
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_best_pace), getBesePaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_step_freq_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_avg_step_freq_desc), getStepFreqBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_max_step_freq_desc), getMaxStepFreqSummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_stride), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_stride), getStepStrideBySummary(walkingSummary), getCm(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_speed_desc), C0532R.color.default_font_color_white));
        view = addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_speed), getAvergeSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity));
        this.mSpeedTextView = (HmTextView) view.findViewById(C0532R.id.number_text_view);
        sportHistoryItemsView.addView(view);
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_max_speed), getMaxSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
        if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
            sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
        }
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addPaceLineChart(this.mActivity, null, null, sportSummary));
        loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 4, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
    }

    public void updateSummary(SportSummary sportSummary) {
        this.mSportSummary = sportSummary;
        OutdoorSportSummary summary = this.mSportSummary;
        this.mDistanceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) summary.getDistance()), true));
        this.mPaceTextView.setText(DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace(summary.getTotalPace())));
        this.mSpeedTextView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed(summary.getSpeed())), true));
        this.mModifyBtn.setVisibility(8);
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick :" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            case C0532R.id.modify_btn:
                this.mListener.onModifyDistance(UnitConvertUtils.isImperial() ? UnitConvertUtils.convertDistanceToMileOrKm((double) (this.mSportSummary.getDistance() / 1000.0f)) : NumeriConversionUtils.getDoubleValue(DataFormatUtils.parseFormattedRealNumber((double) (this.mSportSummary.getDistance() / 1000.0f), false)));
                return;
            default:
                return;
        }
    }
}
