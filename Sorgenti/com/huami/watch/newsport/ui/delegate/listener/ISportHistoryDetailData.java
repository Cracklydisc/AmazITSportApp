package com.huami.watch.newsport.ui.delegate.listener;

import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.gps.model.SportLocationData;
import java.util.Arrays;
import java.util.List;

public interface ISportHistoryDetailData {

    public static final class TrailLocations {
        private static final int[] mRidingTrailColors = new int[]{-16591415, -16730444, C0532R.drawable.his_route_sport_map_icon_trail_riding_over};
        private static final int[] mRunningTrailColors = new int[]{-41472, -51658, C0532R.drawable.his_route_sport_map_icon_trail_run_over};
        private static final int[] mSwimTrailColors = new int[]{-13203713, -16748589, C0532R.drawable.his_route_sport_map_icon_trail_swim_over};
        private static final int[] mWalkingTrailColors = new int[]{-16719090, -16728819, C0532R.drawable.his_route_sport_map_icon_trail_walk_over};
        public List<? extends SportLocationData> locationDatas;
        public int[] mColors = mRunningTrailColors;
        public long mTrackId = -1;
        public int sportType = 1;

        public TrailLocations(List<? extends SportLocationData> locationDatas, int sportType, long trackId) {
            this.locationDatas = locationDatas;
            this.sportType = sportType;
            this.mTrackId = trackId;
            switch (sportType) {
                case 1:
                case 1001:
                    this.mColors = mRunningTrailColors;
                    return;
                case 6:
                    this.mColors = mWalkingTrailColors;
                    return;
                case 9:
                case 1009:
                    this.mColors = mRidingTrailColors;
                    return;
                case 14:
                case 15:
                case 1015:
                    this.mColors = mSwimTrailColors;
                    return;
                default:
                    return;
            }
        }

        public String toString() {
            return "TrailLocations{locationDatas=" + this.locationDatas + ", sportType=" + this.sportType + ", mColors=" + Arrays.toString(this.mColors) + '}';
        }
    }

    void onDailyPropermenceDataReady(int[] iArr);

    void onHeartDataReady(List<? extends HeartRate> list);

    void onLocationDataReady(List<TrailLocations> list);
}
