package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.db.SportDatabaseControlCenter;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.List;

public class IndoorRidingHistoryDetailDelegate extends BaseHistoryDetailDelegate {
    private static final String TAG = IndoorRidingHistoryDetailDelegate.class.getSimpleName();
    private boolean mIsFromEndSport = false;

    class C07431 extends Callback {
        final /* synthetic */ IndoorRidingHistoryDetailDelegate this$0;
        final /* synthetic */ SportDatabaseControlCenter val$controlCenter;

        protected void doCallback(int code, Object o) {
            List<RunningInfoPerLap> perLaps = (List) o;
            long lastAutoLapTime = 0;
            for (RunningInfoPerLap lap : perLaps) {
                if (lap.getLapType() == 0) {
                    long now = System.currentTimeMillis();
                    Debug.m5i("test_lap", "--------trackid_start----------");
                    this.val$controlCenter.getLapMaxPace(this.this$0.mSportSummary.getTrackId() + lastAutoLapTime, lap.getTotalCostTime() + this.this$0.mSportSummary.getTrackId(), this.this$0.mSportSummary.getTrackId());
                    lastAutoLapTime = lap.getTotalCostTime();
                    Debug.m5i("test_lap", "--------trackid_end----------, time:" + (System.currentTimeMillis() - now));
                }
            }
            lastAutoLapTime = 0;
            for (RunningInfoPerLap lap2 : perLaps) {
                if (lap2.getLapType() == 0) {
                    now = System.currentTimeMillis();
                    Debug.m5i("test_lap", "--------start----------");
                    this.val$controlCenter.getLapMaxPace(this.this$0.mSportSummary.getTrackId() + lastAutoLapTime, lap2.getTotalCostTime() + this.this$0.mSportSummary.getTrackId());
                    lastAutoLapTime = lap2.getTotalCostTime();
                    Debug.m5i("test_lap", "--------end----------, time:" + (System.currentTimeMillis() - now));
                }
            }
            int index = 0;
            lastAutoLapTime = 0;
            List<? extends HeartRate> allHearts = new SyncDatabaseManager(HeartRateDao.getInstance(this.this$0.mActivity)).selectAll(this.this$0.mActivity, "track_id=?", new String[]{"" + this.this$0.mSportSummary.getTrackId()}, null, null);
            for (RunningInfoPerLap lap22 : perLaps) {
                if (lap22.getLapType() == 0) {
                    now = System.currentTimeMillis();
                    Debug.m5i("test_lap", "--------heart, start----------");
                    long startTime = lastAutoLapTime + this.this$0.mSportSummary.getTrackId();
                    long endTime = lap22.getTotalCostTime() + this.this$0.mSportSummary.getTrackId();
                    float pace = 0.0f;
                    for (int i = index; i < allHearts.size(); i++) {
                        HeartRate rate = (HeartRate) allHearts.get(i);
                        if (rate.getTimestamp() >= startTime) {
                            if (rate.getTimestamp() > endTime) {
                                break;
                            }
                            if (rate.getPace() > pace) {
                                pace = rate.getPace();
                            }
                            index++;
                        }
                    }
                    Debug.m5i("test_lap", "pace:" + pace);
                    lastAutoLapTime = lap22.getTotalCostTime();
                    Debug.m5i("test_lap", "--------heart, end----------, time:" + (System.currentTimeMillis() - now));
                }
            }
        }
    }

    public IndoorRidingHistoryDetailDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_indoor_riding);
        OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
        this.historyDetaiSportTime.setText(getStartTimeString(this.mActivity, walkingSummary));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
        if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
            sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
        }
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
        loadCurveImg(this.mActivity, this.mSportSummary, 1, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, this.mSportSummary, 9, this.mRequestListener, 0);
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick :" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }
}
