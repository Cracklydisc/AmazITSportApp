package com.huami.watch.newsport.ui.delegate;

import android.view.View;
import android.view.View.OnClickListener;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.ui.uiinterface.IHistoryDetailDelegate;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class WalkingHistoryDetailDelegate extends BaseHistoryDetailDelegate implements OnClickListener, IRecordGraphCache, IHistoryDetailDelegate {
    private static final String TAG = RunningHistoryDetailDelegate.class.getSimpleName();
    private boolean mIsFromEndSport = false;

    public WalkingHistoryDetailDelegate(boolean isFinishFromEndSport) {
        this.mIsFromEndSport = isFinishFromEndSport;
    }

    protected void dynamicAddView(HmLinearLayout sportHistoryItemsView, SportSummary sportSummary) {
        LogUtils.print(TAG, "dynamicAddView");
        OutdoorSportSummary walkingSummary = (OutdoorSportSummary) sportSummary;
        this.historyDetailSportTitle.setText(C0532R.string.sport_widget_title_walk);
        this.historyDetaiSportTime.setText(walkingSummary == null ? "" : getStartTimeString(this.mActivity, walkingSummary));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.walking_step_count), walkingSummary == null ? "" : "" + walkingSummary.getStepCount(), this.mActivity.getResources().getString(C0532R.string.walking_step)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_lap_dis), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_total_distance_desc), getDistanceBySummary(walkingSummary), getKmOrmi(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_cost_time_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_cost_time), getCostTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_move_time), getWalkingTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.history_detail_paused_time), getPauseTimeBySummary(walkingSummary), ""));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_pace_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_pace), getAverPaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_best_pace), getBesePaceBySummary(walkingSummary), getPaceKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_step_freq_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_avg_step_freq_desc), getStepFreqBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_max_step_freq_desc), getMaxStepFreqSummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_step_freq_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_stride), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_stride), getStepStrideBySummary(walkingSummary), getCm(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_speed_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_ave_move_speed), getAvergeSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.crossing_history_detail_max_speed), getMaxSpeedBySummary(walkingSummary), getSpeedKmOrMeter(this.mActivity)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_calorie_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_total_kcal), getCalorieBySummary(walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_calorie_unit)));
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.running_heartrate_desc), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_avg_heart_rate), getAvgHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_max_heart_rate), getMaxHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addChildItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.sport_main_min_heart_rate), getMinHeartBySummary(this.mActivity, walkingSummary), this.mActivity.getResources().getString(C0532R.string.running_heartrate_unit)));
        sportHistoryItemsView.addView(addHeartPercentView(this.mActivity));
        if (!(this.mIsFromEndSport || UnitConvertUtils.isHuangheMode())) {
            sportHistoryItemsView.addView(addTrainEffectView(this.mActivity, walkingSummary));
        }
        sportHistoryItemsView.addView(addHeadItem(this.mActivity, this.mActivity.getResources().getString(C0532R.string.detail_curve_graph), C0532R.color.default_font_color_white));
        sportHistoryItemsView.addView(addHeartLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addPaceLineChart(this.mActivity, null, null, sportSummary));
        sportHistoryItemsView.addView(addStepFreqLineChart(this.mActivity, null, null, sportSummary));
        loadCurveImg(this.mActivity, sportSummary, 1, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, sportSummary, 5, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, sportSummary, 4, this.mRequestListener, 0);
        loadCurveImg(this.mActivity, sportSummary, 9, this.mRequestListener, 0);
    }

    public void onClick(View v) {
        LogUtils.print(TAG, "onClick:Id:" + v.getId());
        switch (v.getId()) {
            case C0532R.id.delete_summary:
                LogUtils.print(TAG, "onClick delete this Summary ");
                this.mListener.onSportSummaryDelete();
                return;
            default:
                return;
        }
    }

    public void updateSummary(SportSummary sportSummary) {
    }
}
