package com.huami.watch.newsport.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.huangheproduction.model.HuangHeSportType;
import org.json.JSONException;
import org.json.JSONObject;

public class VoiceSportActivity extends Activity {
    private static final String TAG = VoiceSportActivity.class.getName();

    public static final class VoiceSettingsType {
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Debug.m5i(TAG, "onCreate");
        String action = getIntent().getAction();
        Debug.m5i(TAG, "action:" + action);
        if (action == null) {
            finish();
        } else {
            parseVoiceAction(action);
        }
    }

    private boolean parseVoiceAction(String action) {
        Intent sportIntent;
        if ("com.huami.watch.sports.action.SPORTS".equalsIgnoreCase(action)) {
            try {
                int sportType = getSportType(new JSONObject(getIntent().getStringExtra("voice_intention")).optJSONObject("data").optString("category", "RUNNING"));
                if (SportType.isSportTypeValid(sportType)) {
                    sportIntent = new Intent("com.huami.watch.sport.action.SPORT_GPS_SEARCH");
                    sportIntent.setPackage("com.huami.watch.newsport");
                    sportIntent.putExtra("sport_type", sportType);
                    sportIntent.addFlags(805339136);
                    startActivity(sportIntent);
                    finish();
                    return true;
                }
                finish();
                return false;
            } catch (JSONException e) {
                e.printStackTrace();
                Debug.m5i(TAG, "err");
                finish();
                return false;
            }
        } else if ("com.huami.watch.sports.action.HISTORY".equalsIgnoreCase(action)) {
            sportIntent = new Intent("com.huami.watch.sport.action.SPORT_HISTORY");
            sportIntent.setPackage("com.huami.watch.newsport");
            sportIntent.addFlags(268468224);
            startActivity(sportIntent);
            finish();
            return true;
        } else if ("com.huami.watch.sports.action.SETTINGS".equalsIgnoreCase(action)) {
            finish();
            return true;
        } else if (!"com.huami.watch.sports.action.ACHIEVEMENT".equalsIgnoreCase(action)) {
            return false;
        } else {
            finish();
            return true;
        }
    }

    private int getSportType(String type) {
        String[] sportList = getResources().getStringArray(C0532R.array.support_sport);
        for (int i = 0; i < sportList.length; i++) {
            if (sportList[i].equalsIgnoreCase(type)) {
                return HuangHeSportType.transferSportTypeFromHuangHe2Zhufeng(i + 1);
            }
        }
        return -1;
    }
}
