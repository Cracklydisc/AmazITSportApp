package com.huami.watch.newsport.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.newsport.ui.fragps.SettingFragment;
import com.huami.watch.newsport.ui.view.CustomDialog;

public class SportWidgetSettingActivity extends BaseActivity {
    private CustomDialog customDialog = null;
    private BaseFragment mSettingsFragment;
    private int mSportType = -1;
    private int mSwimLength = -1;
    private int mSwimUnit = 0;

    class C07131 implements OnClickListener {
        C07131() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C07142 implements OnDismissListener {
        C07142() {
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
        }
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("widget_settings_type", -1);
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_widget_setting_layout);
            BaseConfig config = DataManager.getInstance().getSportConfig(this, this.mSportType);
            this.mSwimLength = config.getTargetSwimLength();
            this.mSwimUnit = config.getUnit();
            if (config.getTargetTEStage() < 0) {
                config.setIsRemindTE(false);
                DataManager.getInstance().setSportConfig(this, config);
            }
            this.mSettingsFragment = BaseFragment.newInstance(this.mSportType, 1);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(C0532R.id.container, this.mSettingsFragment);
            transaction.commitAllowingStateLoss();
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 291) {
            if (data != null) {
                if (!(data.getBooleanExtra("has_no_gpx_file", false) || this.mSettingsFragment == null)) {
                    ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
                }
            } else if (this.mSettingsFragment != null) {
                ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == 291 && this.mSettingsFragment != null) {
            ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == 292) {
            BaseConfig config = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (config.isOpenAutoLapRecord() && !(this.mSwimLength == config.getTargetSwimLength() && this.mSwimUnit == config.getUnit())) {
                showSwimLengthChangedDialog();
            }
            if (this.mSettingsFragment != null) {
                ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void showSwimLengthChangedDialog() {
        Builder systemDialog = new Builder(this);
        systemDialog.setTitle(getString(C0532R.string.adjust_tips_title));
        systemDialog.setMessage(getString(C0532R.string.adjust_swim_tips));
        systemDialog.setPositiveButton(getString(C0532R.string.picker_default_positive_text), new C07131());
        systemDialog.setOnDismissListener(new C07142());
        systemDialog.create().show();
    }
}
