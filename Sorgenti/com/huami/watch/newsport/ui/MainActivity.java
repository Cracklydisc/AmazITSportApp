package com.huami.watch.newsport.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.snapshot.SportSnapshot;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager.ISportConnectionListener;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager.ISportDataListener;

public class MainActivity extends BaseActivity implements ISportConnectionListener, ISportDataListener {
    boolean mIsServiceConnected = false;
    SportDataManager mSportDatamanager = null;

    class C06861 implements OnClickListener {
        C06861() {
        }

        public void onClick(View view) {
            if (MainActivity.this.mIsServiceConnected) {
                MainActivity.this.mSportDatamanager.startSport(2001);
            }
        }
    }

    class C06872 implements OnClickListener {
        C06872() {
        }

        public void onClick(View view) {
            if (MainActivity.this.mIsServiceConnected) {
                MainActivity.this.mSportDatamanager.startSport(1);
            }
        }
    }

    class C06883 implements OnClickListener {
        C06883() {
        }

        public void onClick(View view) {
            if (MainActivity.this.mIsServiceConnected) {
                MainActivity.this.mSportDatamanager.pauseSport();
            }
        }
    }

    class C06894 implements OnClickListener {
        C06894() {
        }

        public void onClick(View view) {
            if (MainActivity.this.mIsServiceConnected) {
                MainActivity.this.mSportDatamanager.continueSport();
            }
        }
    }

    class C06905 implements OnClickListener {
        C06905() {
        }

        public void onClick(View view) {
            if (MainActivity.this.mIsServiceConnected) {
                MainActivity.this.mSportDatamanager.stopSport(false);
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_main);
        this.mSportDatamanager = SportDataManager.getInstance();
        findViewById(C0532R.id.start_mix).setOnClickListener(new C06861());
        findViewById(C0532R.id.start).setOnClickListener(new C06872());
        findViewById(C0532R.id.pause).setOnClickListener(new C06883());
        findViewById(C0532R.id.continu).setOnClickListener(new C06894());
        findViewById(C0532R.id.stop).setOnClickListener(new C06905());
        bindSportService();
    }

    private void bindSportService() {
        this.mSportDatamanager.registerSportDataListener(this);
        this.mSportDatamanager.setSportConnectionListener(this);
        this.mSportDatamanager.startService(this);
        this.mSportDatamanager.bindService(this);
    }

    public void onServiceConnected() {
        logI("onServiceConnected");
        this.mIsServiceConnected = true;
    }

    public void onServiceDisconnected() {
        logI("onServiceDisconnected");
        this.mIsServiceConnected = false;
    }

    private void logI(String content) {
        Debug.m5i("edward", content);
    }

    public void doSportDataReady(SportSnapshot sportStatus) {
        logI("doSportDataReady, sportStatus:" + sportStatus.toString());
    }

    public void doSportMillSecUpdate(int millSec) {
    }

    public void doSportStarted() {
        logI("doSportStarted");
    }

    public void doSportPaused() {
        logI("doSportPaused");
    }

    public void doSportAutoPaused() {
        logI("doSportAutoPaused");
    }

    public void doSportAutoResumed() {
        logI("doSportAutoResumed");
    }

    public void doSportContinued() {
        logI("doSportContinued");
    }

    public void doSportStopped(SportSnapshot sportStatus) {
        logI("doSportStopped");
    }

    public void doChildSportStopped(SportSnapshot sportStatus) {
        logI("doChildSportStopped");
        this.mSportDatamanager.startNextChildSport();
    }

    public void doSportStateErr() {
        logI("doSportStateErr");
    }

    public void doRemindUserRouteOffset(int routeStatus) {
        logI("doRemindUserRouteOffset");
    }

    public void doManualOrAutoLapStart(boolean isManual) {
    }

    public void doSingleTrainUnitCompleted() {
    }

    public void doNextTrainUnitStart() {
    }

    public void doWholeTrainUnitCompleted() {
    }

    public void doSingleTrainUnitNearlyCompleted() {
    }
}
