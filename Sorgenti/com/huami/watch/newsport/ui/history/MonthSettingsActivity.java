package com.huami.watch.newsport.ui.history;

import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter.ISportItemClickListener;
import com.huami.watch.newsport.ui.view.SportMainSettingFace;
import com.huami.watch.newsport.ui.view.SportSettingWheelList;

public class MonthSettingsActivity extends BaseActivity implements ISportItemClickListener {
    private static final String TAG = MonthSettingsActivity.class.getName();
    private SportLauncherItemAdapter mAdapter = null;
    private SportMainSettingFace mSportMainSettingFace = null;
    private SportSettingWheelList mWheelList = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.fragment_month_setting);
        initSportList();
    }

    private void initSportList() {
        this.mSportMainSettingFace = (SportMainSettingFace) findViewById(C0532R.id.main_list);
        this.mSportMainSettingFace.setDismissAllow(true);
        int marginTop = getResources().getDimensionPixelSize(C0532R.dimen.settings_item_layout_drawing_top);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        this.mWheelList = new SportSettingWheelList(this.mSportMainSettingFace, new RectF(0.0f, 0.0f, (float) dm.widthPixels, (float) dm.widthPixels));
        this.mWheelList.setMarginTop(marginTop);
        this.mAdapter = new SportLauncherItemAdapter(this.mSportMainSettingFace, C0532R.string.sport_month_sport_title);
        this.mAdapter.setSportItemClickListener(this);
        this.mWheelList.getXContext().setParentNeedTouchEvent(true);
        this.mWheelList.setViewAdapter(this.mAdapter);
        this.mWheelList.standup(false, true);
        this.mSportMainSettingFace.addDrawingItem(this.mWheelList);
        this.mSportMainSettingFace.postInvalidate();
    }

    public void onSportClicked(int sportType) {
        Intent graphIntent = new Intent(this, SportPickerActivity.class);
        graphIntent.putExtra("selector_type", "select_target_month_settings");
        graphIntent.putExtra("sport_type", sportType);
        startActivityForResult(graphIntent, 291);
    }
}
