package com.huami.watch.newsport.ui.history;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.PageTransformer;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.AsyncDatabaseManager;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.gpxexport.ExportGPXActivity;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.stepmodel.IndoorLearningArgCallback;
import com.huami.watch.newsport.stepmodel.StepModelManager;
import com.huami.watch.newsport.syncservice.SyncManager;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainingInfo;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryRequest;
import com.huami.watch.newsport.ui.history.fragment.MoreOperationFragment;
import com.huami.watch.newsport.ui.history.fragment.MoreOperationFragment.IOperationListener;
import com.huami.watch.newsport.ui.history.fragment.SportCircleCountFragment;
import com.huami.watch.newsport.ui.history.fragment.SportDetailVo2maxFragment;
import com.huami.watch.newsport.ui.history.fragment.SportFullRecoveryFragment;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryDetailDeleteFragment;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryDetailDeleteFragment.IDeleteDataListener;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryDetailFragment;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryRouteFragment;
import com.huami.watch.newsport.ui.history.fragment.SportTEFragment;
import com.huami.watch.newsport.ui.uiinterface.IDeleteSportSummaryListener;
import com.huami.watch.newsport.ui.uiinterface.ILoadingAnimListener;
import com.huami.watch.newsport.ui.view.DailyPerformenceChatView;
import com.huami.watch.newsport.ui.view.HmSportLayout;
import com.huami.watch.newsport.ui.view.SportViewPager;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SportHistoryDetailActivity extends BaseActivity implements EventCallBack, IRecordGraphCache, ISportHistoryRequest, IOperationListener, IDeleteDataListener, IDeleteSportSummaryListener, ILoadingAnimListener {
    private static final String TAG = SportHistoryDetailActivity.class.getSimpleName();
    private static volatile int childCounts = 3;
    private int CURRNET_DELETE_DIALOG_OPEN_STATUS = 200;
    private volatile int CURRNET_DELETE_DIALOG_STATUS = 0;
    private Fragment currnetFragment;
    private volatile int currnetPageChangeStatus = 0;
    private boolean isRemoveAutoFlip = false;
    private KeyEventDispatcher keyEventDispatcher;
    private RunningViewPagerAdapter mAdapter;
    private Runnable mAutoFlipRunnable = new Runnable() {
        public void run() {
            if (!SportHistoryDetailActivity.this.isDestroyed() && !SportHistoryDetailActivity.this.isFinishing() && SportHistoryDetailActivity.this.mAdapter != null) {
                int curIndex = SportHistoryDetailActivity.this.mSportViewPager.getCurrentItem();
                int curFragType = SportHistoryDetailActivity.this.getFragmentType(curIndex);
                if (curFragType == 7 || curFragType == 9 || curFragType == 8) {
                    SportHistoryDetailActivity.this.mSportViewPager.setCurrentItem(curIndex + 1, true);
                    SportHistoryDetailActivity.this.autoFlip();
                }
            }
        }
    };
    private List<SportSummary> mChildSummarys = new ArrayList();
    private BaseConfig mConfig = null;
    private int mContentFlags = 0;
    private SyncDatabaseManager<DailyPerformanceInfo> mDailyPerformanceInfoManager = null;
    private AsyncDatabaseManager<SportSummary> mDatabaseManager = null;
    private int mFlags = 0;
    private int mFragmentCount = 1;
    private SparseArray<Fragment> mFragmentMap = new SparseArray();
    private List<FragmentTag> mFragmentTypeList = new ArrayList();
    private List<HeartRate> mHeartRateList = new ArrayList();
    private SyncDatabaseManager<HeartRate> mHeartRateSyncManager;
    private List<Fragment> mHistoryFragmentList = new ArrayList();
    private ViewPagerPageIndicator mIndicator;
    private Fragment mInitFragment = null;
    private float mInitSportDis = 0.0f;
    private boolean mIsFromEndSport = false;
    private boolean mIsHasTrail = false;
    private boolean mIsMixedSport = false;
    private boolean mIsModify = false;
    private boolean mIsSaveRecord = true;
    private int mLapFlags = 0;
    private List<TrailLocations> mLocationDataList = new ArrayList();
    private SyncDatabaseManager<SportLocationData> mLocationSyncManager;
    private Fragment mMoreOperationFragment = null;
    private PageTransformer mPageTransformer = new C08317();
    private SportHistoryDetailDeleteFragment mSportDeleteFragment = null;
    private Fragment mSportHistroryDetaikFragment;
    private SportSummary mSportSummary = null;
    private int mSportType = -1;
    private SportViewPager mSportViewPager;
    private SyncDatabaseManager<SportSummary> mSummaryManager = null;
    private SyncDatabaseManager<RunningInfoPerLap> mSyncLapDBManager = null;
    private int mTE = 0;
    private HmSportLayout mTrainContainer = null;
    private TrainingInfo mTrainInfo = null;
    private int mTrainingMode = -1;
    private int recoveryTimeHout = -1;

    public interface IDeleteState {
        void onSwitchDeleteStateSuccess();
    }

    class C08251 implements Runnable {

        class C08241 implements Runnable {
            C08241() {
            }

            public void run() {
                ((SportHistoryDetailFragment) SportHistoryDetailActivity.this.mSportHistroryDetaikFragment).updateChildList(SportHistoryDetailActivity.this.mChildSummarys);
            }
        }

        C08251() {
        }

        public void run() {
            if (!SportHistoryDetailActivity.this.isDestroyed() && !SportHistoryDetailActivity.this.isFinishing()) {
                List<Long> childTrackIds = SportHistoryDetailActivity.this.mSportSummary.getChildSportSummrys();
                StringBuilder whereClause = new StringBuilder();
                whereClause.append("track_id=? OR track_id=? OR track_id=?");
                String[] whereClauseArgs = new String[childTrackIds.size()];
                for (int i = 0; i < childTrackIds.size(); i++) {
                    whereClauseArgs[i] = String.valueOf(childTrackIds.get(i));
                }
                List<? extends SportSummary> summaries = SportHistoryDetailActivity.this.mSummaryManager.selectAll(SportHistoryDetailActivity.this, whereClause.toString(), whereClauseArgs, null, null);
                for (SportSummary summary : summaries) {
                    Debug.m5i(SportHistoryDetailActivity.TAG, "child summary:" + summary.toString());
                }
                if (summaries != null) {
                    SportHistoryDetailActivity.this.mChildSummarys.addAll(summaries);
                }
                if (SportHistoryDetailActivity.this.mChildSummarys != null && SportHistoryDetailActivity.this.mChildSummarys.size() > 0) {
                    SportHistoryDetailActivity.access$412(SportHistoryDetailActivity.this, SportHistoryDetailActivity.this.mChildSummarys.size());
                }
                SportHistoryDetailActivity.this.loadLapInfo();
                SportHistoryDetailActivity.this.refreshFragmentCount();
                if (SportHistoryDetailActivity.this.mSportHistroryDetaikFragment != null) {
                    Global.getGlobalUIHandler().post(new C08241());
                }
            }
        }
    }

    class C08262 implements Runnable {
        C08262() {
        }

        public void run() {
            if (!SportHistoryDetailActivity.this.isDestroyed() && !SportHistoryDetailActivity.this.isFinishing()) {
                SportHistoryDetailActivity.this.loadLapInfo();
                SportHistoryDetailActivity.this.refreshFragmentCount();
            }
        }
    }

    class C08273 implements Runnable {
        C08273() {
        }

        public void run() {
            int size = SportHistoryDetailActivity.this.mAdapter.refreshAdapter();
            if (size > 2) {
                SportHistoryDetailActivity.this.mSportViewPager.setOffscreenPageLimit(size);
            }
        }
    }

    class C08284 implements OnPageChangeListener {
        C08284() {
        }

        public void onPageScrolled(int i, float v, int i1) {
        }

        public void onPageSelected(int i) {
            SportHistoryDetailActivity.this.currnetFragment = SportHistoryDetailActivity.this.mAdapter.getItem(i);
        }

        public void onPageScrollStateChanged(int i) {
            SportHistoryDetailActivity.this.currnetPageChangeStatus = i;
        }
    }

    class C08295 implements OnClickListener {
        C08295() {
        }

        public void onClick(DialogInterface dialog, int which) {
            SportHistoryDetailActivity.this.onDeleteConfirm();
            dialog.dismiss();
        }
    }

    class C08306 implements OnClickListener {
        C08306() {
        }

        public void onClick(DialogInterface dialog, int which) {
            LogUtils.print(SportHistoryDetailActivity.TAG, "onClick cancel ");
            SportHistoryDetailActivity.this.onDeleteDismiss();
            dialog.dismiss();
        }
    }

    class C08317 implements PageTransformer {
        C08317() {
        }

        public void transformPage(View page, float position) {
        }
    }

    class C08328 extends Callback {
        C08328() {
        }

        protected void doCallback(int success, Object param) {
            Debug.m5i(SportHistoryDetailActivity.TAG, "update: " + success);
            if (success == 0) {
                if (ModelUtil.isBoudType_iOS(SportHistoryDetailActivity.this)) {
                    SyncManager.getInstance().triggerUploadSportData();
                } else {
                    SyncManager.getInstance().startUploadNotSyncSportDatas();
                }
                float disDiff = ((float) SportHistoryDetailActivity.this.mSportSummary.getTrimedDistance()) - SportHistoryDetailActivity.this.mInitSportDis;
                if (Double.compare((double) disDiff, 0.0d) != 0) {
                    SportHistoryDetailActivity.this.getContentResolver().notifyChange(Uri.parse("content://com.huami.watch.sport.provider/sport_summary_data_info"), null);
                    SportSummary summary = OutdoorSportSummary.createOutdoorSportSummary(8);
                    summary.setTrackId(SportHistoryDetailActivity.this.mSportSummary.getTrackId());
                    summary.setDistance(disDiff);
                    DataManager.getInstance().reModifySummaryToStatAndMileDay(SportHistoryDetailActivity.this, summary.getTrimedDistance(), summary.getTrackId());
                }
            }
        }
    }

    class C08349 implements Runnable {

        class C08331 implements IndoorLearningArgCallback {
            C08331() {
            }

            public void onLearningResult(float[] learningMatrixArray, int matrixSize, int[] revisedStepLArray, int stepLSize) {
                int i;
                Debug.m5i(SportHistoryDetailActivity.TAG, "matrixSize: " + matrixSize + ", stepLSize: " + stepLSize);
                Float[] learningArray = null;
                Integer[] setpArray = null;
                if (learningMatrixArray != null) {
                    learningArray = new Float[learningMatrixArray.length];
                    for (i = 0; i < learningMatrixArray.length; i++) {
                        learningArray[i] = Float.valueOf(learningMatrixArray[i]);
                    }
                }
                if (revisedStepLArray != null) {
                    setpArray = new Integer[revisedStepLArray.length];
                    for (i = 0; i < revisedStepLArray.length; i++) {
                        setpArray[i] = Integer.valueOf(revisedStepLArray[i]);
                    }
                }
                DataManager.getInstance().setStepLearningMatrix(SportHistoryDetailActivity.this.getApplicationContext(), learningArray);
                DataManager.getInstance().setStepLength(SportHistoryDetailActivity.this.getApplicationContext(), setpArray);
                StepModelManager.getInstance(SportHistoryDetailActivity.this.getApplicationContext()).unRegisterCallbackIndoorLearningArg(this);
                StepModelManager.getInstance(SportHistoryDetailActivity.this.getApplicationContext()).destroy();
                SyncManager.getInstance().startUploadStepConfig();
            }
        }

        C08349() {
        }

        public void run() {
            List<? extends HeartRate> heartRateList = SportHistoryDetailActivity.this.mHeartRateSyncManager.selectAll(SportHistoryDetailActivity.this.getApplicationContext(), "track_id=?", new String[]{"" + SportHistoryDetailActivity.this.mSportSummary.getTrackId()}, "time ASC", null);
            if (heartRateList != null && !heartRateList.isEmpty()) {
                int i;
                long[] timeStamps = new long[heartRateList.size()];
                short[] stepFreqs = new short[heartRateList.size()];
                for (i = 0; i < heartRateList.size(); i++) {
                    HeartRate rate = (HeartRate) heartRateList.get(i);
                    timeStamps[i] = rate.getTimestamp();
                    stepFreqs[i] = (short) ((int) (rate.getStepFreq() * 60.0f));
                }
                StepModelManager.getInstance(SportHistoryDetailActivity.this.getApplicationContext()).registerCallbackIndoorLearningArg(new C08331());
                float[] learningMatrix = DataManager.getInstance().getStepLearningMatrix(SportHistoryDetailActivity.this.getApplicationContext());
                if (learningMatrix == null) {
                    learningMatrix = new float[70];
                    for (i = 0; i < 70; i++) {
                        learningMatrix[i] = -1.0f;
                    }
                }
                Debug.m5i(SportHistoryDetailActivity.TAG, "last you hua: " + Arrays.toString(learningMatrix));
                StepModelManager.getInstance(SportHistoryDetailActivity.this.getApplicationContext()).receiveIndoorResultToLearn(timeStamps, stepFreqs, timeStamps.length, learningMatrix, learningMatrix.length, (int) SportHistoryDetailActivity.this.mSportSummary.getDistance());
            }
        }
    }

    private static class FragmentTag {
        public long tag;
        public int type;

        public FragmentTag(int type) {
            this(-1, type);
        }

        public FragmentTag(long tag, int type) {
            this.tag = -1;
            this.type = -1;
            this.tag = tag;
            this.type = type;
        }
    }

    private class RunningViewPagerAdapter extends FragmentPagerAdapter {
        private int count = 0;

        public RunningViewPagerAdapter(FragmentManager fm) {
            super(fm);
            this.count = SportHistoryDetailActivity.this.mFragmentTypeList.size();
        }

        public int refreshAdapter() {
            SportHistoryDetailActivity.this.updateFragmentList();
            this.count = SportHistoryDetailActivity.this.mFragmentTypeList.size();
            notifyDataSetChanged();
            return this.count;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            Debug.m5i(SportHistoryDetailActivity.TAG, "instantiateItem");
            Fragment result = (Fragment) super.instantiateItem(container, position);
            SportHistoryDetailActivity.this.mFragmentMap.put(position, result);
            return result;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            SportHistoryDetailActivity.this.mFragmentMap.remove(position);
        }

        public Fragment getItem(int position) {
            Debug.m5i(SportHistoryDetailActivity.TAG, "getItem, position:" + position);
            if (position <= SportHistoryDetailActivity.this.mHistoryFragmentList.size() - 1) {
                return (Fragment) SportHistoryDetailActivity.this.mHistoryFragmentList.get(position);
            }
            switch (SportHistoryDetailActivity.this.getFragmentType(position)) {
                case 0:
                    SportHistoryDetailActivity.this.mInitFragment = SportHistoryDetailActivity.this.mSportHistroryDetaikFragment = SportHistoryDetailFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, false, false, SportHistoryDetailActivity.this.mIsFromEndSport, SportHistoryDetailActivity.this.mChildSummarys);
                    if (SportHistoryDetailActivity.this.mSportHistroryDetaikFragment == null) {
                        SportHistoryDetailActivity.this.mSportHistroryDetaikFragment = SportHistoryDetailActivity.this.mInitFragment;
                        break;
                    }
                    break;
                case 1:
                    SportHistoryDetailActivity.this.mInitFragment = SportHistoryRouteFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, SportHistoryDetailActivity.this.mIsHasTrail);
                    break;
                case 2:
                    if (!SportHistoryDetailActivity.this.hasLapType(1)) {
                        if (!SportHistoryDetailActivity.this.hasLapType(4)) {
                            if (!SportHistoryDetailActivity.this.hasLapType(2)) {
                                if (SportHistoryDetailActivity.this.hasLapType(8)) {
                                    SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 3);
                                    break;
                                }
                            }
                            SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 1);
                            break;
                        }
                        SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 2);
                        break;
                    }
                    SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 0);
                    break;
                    break;
                case 3:
                    SportHistoryDetailActivity.this.mInitFragment = SportHistoryDetailFragment.newInstance(SportHistoryDetailActivity.this.getSpecificSummary(position), false, true, SportHistoryDetailActivity.this.mIsFromEndSport);
                    break;
                case 4:
                    SportHistoryDetailActivity.this.mInitFragment = SportHistoryDetailFragment.newInstance(SportHistoryDetailActivity.this.getSpecificSummary(position), false, true, SportHistoryDetailActivity.this.mIsFromEndSport);
                    break;
                case 5:
                    SportHistoryDetailActivity.this.mInitFragment = SportHistoryDetailFragment.newInstance(SportHistoryDetailActivity.this.getSpecificSummary(position), false, true, SportHistoryDetailActivity.this.mIsFromEndSport);
                    break;
                case 6:
                    if (!SportHistoryDetailActivity.this.hasLapType(1)) {
                        if (!SportHistoryDetailActivity.this.hasLapType(4)) {
                            if (!SportHistoryDetailActivity.this.hasLapType(2)) {
                                if (SportHistoryDetailActivity.this.hasLapType(8)) {
                                    SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 3);
                                    break;
                                }
                            }
                            SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 1);
                            break;
                        }
                        SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 2);
                        break;
                    }
                    SportHistoryDetailActivity.this.mInitFragment = SportCircleCountFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, 0);
                    break;
                    break;
                case 7:
                    SportHistoryDetailActivity.this.mInitFragment = SportTEFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary);
                    break;
                case 8:
                    SportHistoryDetailActivity.this.mInitFragment = SportFullRecoveryFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary);
                    break;
                case 9:
                    SportHistoryDetailActivity.this.mInitFragment = SportDetailVo2maxFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary);
                    break;
                case 10:
                    SportHistoryDetailActivity.this.mInitFragment = MoreOperationFragment.newInstance(SportHistoryDetailActivity.this.mSportSummary, SportHistoryDetailActivity.this.mIsFromEndSport);
                    break;
                default:
                    throw new IllegalArgumentException("getItem is wrong, cur postion:" + position);
            }
            SportHistoryDetailActivity.this.mHistoryFragmentList.add(SportHistoryDetailActivity.this.mInitFragment);
            return SportHistoryDetailActivity.this.mInitFragment;
        }

        public int getCount() {
            SportHistoryDetailActivity.childCounts = this.count;
            return this.count;
        }
    }

    static /* synthetic */ int access$1776(SportHistoryDetailActivity x0, int x1) {
        int i = x0.mFlags | x1;
        x0.mFlags = i;
        return i;
    }

    static /* synthetic */ int access$412(SportHistoryDetailActivity x0, int x1) {
        int i = x0.mFragmentCount + x1;
        x0.mFragmentCount = i;
        return i;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportSummary = (SportSummary) getIntent().getParcelableExtra("sport_summary");
        if (this.mSportSummary == null) {
            finish();
            return;
        }
        this.mTrainingMode = getIntent().getIntExtra("entrance_type", -1);
        this.mIsFromEndSport = getIntent().getBooleanExtra("is_from_end_sport", false);
        this.mSportType = this.mSportSummary.getSportType();
        this.mInitSportDis = (float) this.mSportSummary.getTrimedDistance();
        this.mIsMixedSport = SportType.isMixedSport(this.mSportType);
        this.mFlags = 0;
        this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
        setContentView(C0532R.layout.activity_sport_history_detail);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(this, (ViewGroup) findViewById(C0532R.id.container));
        RecordGraphManager.getInstance(this).setRecordCache(this);
        this.mLocationSyncManager = new SyncDatabaseManager(LocationDataDao.getInstance(this));
        this.mHeartRateSyncManager = new SyncDatabaseManager(HeartRateDao.getInstance(this));
        this.mDailyPerformanceInfoManager = new SyncDatabaseManager(DailyPerpormanceInfoDao.getInstance(this));
        this.mSyncLapDBManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(this));
        setKeyEventListener(this);
        this.isViewDispatchEvent = false;
        this.mSummaryManager = new SyncDatabaseManager(SportSummaryDao.getInstance(this));
        this.mDatabaseManager = new AsyncDatabaseManager(SportSummaryDao.getInstance(this), Global.getGlobalWorkHandler());
        init();
        checkIsCompleteTrainProject();
        if (this.mIsFromEndSport) {
            autoFlip();
        }
    }

    public void finish() {
        Debug.m5i(TAG, "finish, mIsModify:" + this.mIsModify);
        if (this.mIsModify) {
            Intent intent = new Intent();
            intent.putExtra("is_refresh", true);
            setResult(-1, intent);
        }
        super.finish();
    }

    protected void onDestroy() {
        Debug.m5i(TAG, "onDestroy");
        super.onDestroy();
        RecordGraphManager.getInstance(this).setRecordCache(null);
        if (this.mIsFromEndSport && this.mIsSaveRecord) {
            if (this.mSportSummary.getSportType() == 8 && this.mSportSummary.getCurrentStatus() != 2) {
                updateIndoorRunSportData();
            } else if (UnitConvertUtils.isUseNewUploadMethod(this)) {
                SyncManager.getInstance().triggerUploadSportData();
            } else {
                SyncManager.getInstance().startUploadNotModifyAndSyncSportDatas();
            }
        }
        if (this.mHeartRateList != null) {
            this.mHeartRateList.clear();
        }
        if (this.mLocationDataList != null) {
            this.mLocationDataList.clear();
        }
    }

    private void init() {
        initViewPager();
        Log.i(TAG, " fragmentIntCount:" + this.mFragmentTypeList.size());
        if (this.mIsMixedSport) {
            Global.getGlobalHeartHandler().post(new C08251());
        } else {
            Global.getGlobalHeartHandler().post(new C08262());
        }
    }

    private void loadLapInfo() {
        int i;
        List<? extends RunningInfoPerLap> runningInfoPerLaps = this.mSyncLapDBManager.selectAll(this, "track_id=?", new String[]{"" + this.mSportSummary.getTrackId()}, null, null);
        List<SportSummary> summaryList = new ArrayList();
        summaryList.add(this.mSportSummary);
        if (this.mChildSummarys != null && this.mChildSummarys.size() > 0) {
            summaryList.addAll(this.mChildSummarys);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        for (i = 0; i < summaryList.size(); i++) {
            sb.append("?").append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        sb.append(")");
        String[] sbCondition = new String[summaryList.size()];
        for (i = 0; i < summaryList.size(); i++) {
            sbCondition[i] = String.valueOf(((SportSummary) summaryList.get(i)).getTrackId());
        }
        List<? extends SportLocationData> locationChildDatas = this.mLocationSyncManager.selectAll(this, "track_id in " + sb.toString(), sbCondition, null, "1");
        if (locationChildDatas != null && locationChildDatas.size() > 0) {
            this.mFragmentCount++;
            this.mIsHasTrail = true;
        }
        this.mContentFlags |= 2;
        if (runningInfoPerLaps == null || runningInfoPerLaps.isEmpty()) {
            this.mContentFlags |= 1;
            return;
        }
        for (RunningInfoPerLap lap : runningInfoPerLaps) {
            if (lap.getLapType() == 0) {
                this.mLapFlags |= 1;
            } else if (lap.getLapType() == 1) {
                this.mLapFlags |= 2;
            } else if (lap.getLapType() == 2) {
                this.mLapFlags |= 4;
            } else if (lap.getLapType() == 3) {
                this.mLapFlags |= 8;
            }
        }
        if (!((this.mLapFlags & 1) == 0 && (this.mLapFlags & 4) == 0)) {
            this.mFragmentCount++;
        }
        if (!(((this.mLapFlags & 2) == 0 && (this.mLapFlags & 8) == 0) || this.mIsHasTrail)) {
            this.mFragmentCount++;
        }
        this.mContentFlags |= 1;
    }

    private void refreshFragmentCount() {
        if ((this.mContentFlags & 1) != 0 && (this.mContentFlags & 2) != 0) {
            Global.getGlobalUIHandler().post(new C08273());
        }
    }

    private boolean hasLapType(int lapType) {
        return (this.mLapFlags & lapType) != 0;
    }

    private void initViewPager() {
        this.mHistoryFragmentList.clear();
        this.recoveryTimeHout = getRecoveryTimeHour();
        this.mTE = ((OutdoorSportSummary) this.mSportSummary).getTE();
        this.mSportViewPager = (SportViewPager) findViewById(C0532R.id.running_pager);
        initFragmentList();
        this.mAdapter = new RunningViewPagerAdapter(getSupportFragmentManager());
        this.mSportViewPager.setPageTransformer(true, this.mPageTransformer);
        this.mSportViewPager.setAdapter(this.mAdapter);
        this.mIndicator = (ViewPagerPageIndicator) findViewById(C0532R.id.indicator);
        this.mIndicator.setViewPager(this.mSportViewPager);
        this.mIndicator.showIndicator(true);
        this.mSportViewPager.setOnPageChangeListener(new C08284());
    }

    private int getRecoveryTimeHour() {
        if (!this.mIsFromEndSport) {
            return -1;
        }
        BaseConfig sportBaseConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
        Log.i(TAG, " currnetSportInfo: mSportType:" + this.mSportType + ",isMeasureHeart:" + sportBaseConfig.isMeasureHeart());
        List<Integer> childTypes = ((OutdoorSportSummary) this.mSportSummary).getChildTypes();
        if (SportType.isMixedSport(this.mSportType) && childTypes.size() == 1 && childTypes.contains(Integer.valueOf(1015))) {
            return -1;
        }
        if (SportType.isSuportHasHeartRataSports(this.mSportType) || ((this.mSportType == 7 || this.mSportType == 13) && sportBaseConfig.isMeasureHeart())) {
            Log.i(TAG, " hasHeartRataSport");
            FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(this);
            LogUtils.print(TAG, " firstBeatConfig:" + config.toString());
            float recoveryTime = (float) (((((long) config.getRecoveryTime()) - (((System.currentTimeMillis() - config.getSportFinishTime()) / 1000) / 60)) + 30) / 60);
            Log.i(TAG, "recoveryTime Hour:" + ((int) recoveryTime));
            if (recoveryTime > 97.0f || recoveryTime < 0.0f) {
                recoveryTime = -1.0f;
            }
            return (int) recoveryTime;
        }
        Log.i(TAG, " hasNoHeartRataSport");
        return -1;
    }

    private void showDeleteRecordItemDialog() {
        Builder builder = new Builder(this);
        builder.setMessage(getString(C0532R.string.sport_history_delete_dialog_title));
        builder.setPositiveButton(getString(C0532R.string.sport_confirm_stop_sport), new C08295());
        builder.setNegativeButton(getString(C0532R.string.sport_confirm_not_stop_sport), new C08306());
        builder.create().show();
    }

    private boolean isTEAvailable() {
        if (this.mTE <= 0 || SportType.isSwimMode(this.mSportSummary.getSportType())) {
            return false;
        }
        return true;
    }

    public void onLoadingAnimEnd() {
    }

    public void onSportSummaryDelete() {
        showDeleteRecordItemDialog();
        this.CURRNET_DELETE_DIALOG_STATUS = 1;
    }

    public void onModifyDistance(double distance) {
        if (UnitConvertUtils.isImperial()) {
            distance = NumeriConversionUtils.getDoubleValue(DataFormatUtils.parseFormattedRealNumber(distance, this.mIsModify));
        }
        Intent intent = new Intent(this, SportPickerActivity.class);
        intent.putExtra("selector_type", "select_target_indoor_run");
        intent.putExtra("sport_type", this.mSportSummary.getSportType());
        intent.putExtra("distance_picker", distance);
        startActivityForResult(intent, 257);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        float f = 0.0f;
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 257 && resultCode == -1) {
            this.mIsModify = true;
            OutdoorSportSummary summary = this.mSportSummary;
            summary.setDistance((float) (1000.0d * data.getDoubleExtra("sportNumber", (double) this.mSportSummary.getDistance())));
            SensorHubManagerWrapper.getInstance(this).notifyIndoorModifiedDistanceResult2SensorHub((int) summary.getDistance(), null);
            float movingPace = summary.getDistance() == 0.0f ? 0.0f : ((float) (((long) summary.getSportDuration()) / 1000)) / summary.getDistance();
            summary.setTotalPace(movingPace);
            summary.setTotalSpeed(movingPace == 0.0f ? 0.0f : 1.0f / movingPace);
            summary.setPace(movingPace);
            if (movingPace != 0.0f) {
                f = 1.0f / movingPace;
            }
            summary.setSpeed(f);
            if (this.mSportHistroryDetaikFragment != null) {
                ((SportHistoryDetailFragment) this.mSportHistroryDetaikFragment).updateSummary(summary);
            }
            updateIndoorRunSportData();
        }
    }

    private void updateIndoorRunSportData() {
        this.mSportSummary.setCurrentStatus(2);
        this.mDatabaseManager.update(this, this.mSportSummary, new C08328());
        calculateStepModel();
    }

    private void calculateStepModel() {
        Global.getGlobalWorkHandler().post(new C08349());
    }

    public void onDrawableFound(long trackId, Bitmap bitmap, int graphType) {
        if (this.mFragmentMap != null) {
            for (int i = 0; i < this.mFragmentMap.size(); i++) {
                Fragment fragment = (Fragment) this.mFragmentMap.get(this.mFragmentMap.keyAt(i));
                if (fragment instanceof IRecordGraphCache) {
                    ((IRecordGraphCache) fragment).onDrawableFound(trackId, bitmap, graphType);
                }
            }
        }
    }

    public void onSaveCacheResult(int graphType, boolean isSuccess) {
        if (this.mFragmentMap != null) {
            for (int i = 0; i < this.mFragmentMap.size(); i++) {
                Fragment fragment = (Fragment) this.mFragmentMap.get(this.mFragmentMap.keyAt(i));
                if (fragment instanceof IRecordGraphCache) {
                    ((IRecordGraphCache) fragment).onSaveCacheResult(graphType, isSuccess);
                }
            }
        }
    }

    public void onDeleteCacheResult(int graphType, boolean isSuccess) {
        if (this.mFragmentMap != null) {
            for (int i = 0; i < this.mFragmentMap.size(); i++) {
                Fragment fragment = (Fragment) this.mFragmentMap.get(this.mFragmentMap.keyAt(i));
                if (fragment instanceof IRecordGraphCache) {
                    ((IRecordGraphCache) fragment).onDeleteCacheResult(graphType, isSuccess);
                }
            }
        }
    }

    private synchronized void loadHeartHistoryDetail() {
        if ((this.mFlags & 1) == 0) {
            this.mFlags |= 1;
            Global.getGlobalHeartHandler().post(new Runnable() {
                public void run() {
                    if (SportHistoryDetailActivity.this.mSportSummary == null) {
                        Debug.m5i(SportHistoryDetailActivity.TAG, "loadHistoryDetail, summary is null");
                        return;
                    }
                    List<HeartRate> heartRates = new ArrayList();
                    long trackId;
                    List<HeartRate> heartRateList;
                    if (SportHistoryDetailActivity.this.mIsMixedSport) {
                        for (SportSummary summary : SportHistoryDetailActivity.this.mChildSummarys) {
                            trackId = summary.getTrackId();
                            heartRateList = SportHistoryDetailActivity.this.mHeartRateSyncManager.selectAll(SportHistoryDetailActivity.this, "track_id=?", new String[]{"" + trackId}, null, null);
                            if (heartRateList != null) {
                                heartRates.addAll(heartRateList);
                            }
                        }
                    } else {
                        trackId = SportHistoryDetailActivity.this.mSportSummary.getTrackId();
                        heartRateList = SportHistoryDetailActivity.this.mHeartRateSyncManager.selectAll(SportHistoryDetailActivity.this, "track_id=?", new String[]{"" + trackId}, null, null);
                        LogUtil.m9i(true, SportHistoryDetailActivity.TAG, "curve select complete");
                        heartRates = heartRateList;
                    }
                    if (heartRates != null) {
                        SportHistoryDetailActivity.this.mHeartRateList.addAll(heartRates);
                    }
                    SportHistoryDetailActivity.access$1776(SportHistoryDetailActivity.this, 4);
                    if (SportHistoryDetailActivity.this.mFragmentMap != null) {
                        for (int i = 0; i < SportHistoryDetailActivity.this.mFragmentMap.size(); i++) {
                            Fragment fragment = (Fragment) SportHistoryDetailActivity.this.mFragmentMap.get(SportHistoryDetailActivity.this.mFragmentMap.keyAt(i));
                            if (fragment instanceof ISportHistoryDetailData) {
                                ((ISportHistoryDetailData) fragment).onHeartDataReady(heartRates);
                            }
                        }
                    }
                }
            });
        } else if ((this.mFlags & 4) != 0) {
            Global.getGlobalHeartHandler().post(new Runnable() {
                public void run() {
                    if (SportHistoryDetailActivity.this.mFragmentMap != null) {
                        for (int i = 0; i < SportHistoryDetailActivity.this.mFragmentMap.size(); i++) {
                            Fragment fragment = (Fragment) SportHistoryDetailActivity.this.mFragmentMap.get(SportHistoryDetailActivity.this.mFragmentMap.keyAt(i));
                            if (fragment instanceof ISportHistoryDetailData) {
                                ((ISportHistoryDetailData) fragment).onHeartDataReady(SportHistoryDetailActivity.this.mHeartRateList);
                            }
                        }
                    }
                }
            });
        }
    }

    private synchronized void loadGPSHistoryDetail() {
        if ((this.mFlags & 2) == 0) {
            this.mFlags |= 2;
            Global.getGlobalHeartHandler().post(new Runnable() {
                public void run() {
                    if (SportHistoryDetailActivity.this.mSportSummary == null) {
                        Debug.m5i(SportHistoryDetailActivity.TAG, "loadHistoryDetail, summary is null");
                        return;
                    }
                    long trackId;
                    if (SportHistoryDetailActivity.this.mIsMixedSport) {
                        for (SportSummary summary : SportHistoryDetailActivity.this.mChildSummarys) {
                            trackId = summary.getTrackId();
                            List<SportLocationData> locationList = SportHistoryDetailActivity.this.mLocationSyncManager.selectAll(SportHistoryDetailActivity.this, "track_id=?", new String[]{"" + trackId}, null, null);
                            if (locationList != null) {
                                SportHistoryDetailActivity.this.mLocationDataList.add(new TrailLocations(locationList, summary.getSportType(), summary.getTrackId()));
                            }
                        }
                    } else {
                        trackId = SportHistoryDetailActivity.this.mSportSummary.getTrackId();
                        SportHistoryDetailActivity.this.mLocationDataList.add(new TrailLocations(SportHistoryDetailActivity.this.mLocationSyncManager.selectAll(SportHistoryDetailActivity.this, "track_id=?", new String[]{"" + trackId}, null, null), SportHistoryDetailActivity.this.mSportSummary.getSportType(), SportHistoryDetailActivity.this.mSportSummary.getTrackId()));
                    }
                    synchronized (SportHistoryDetailActivity.this) {
                        SportHistoryDetailActivity.access$1776(SportHistoryDetailActivity.this, 8);
                    }
                    if (Global.DEBUG_LEVEL_3) {
                        SportHistoryDetailActivity.this.testSaveLocData();
                    }
                    if (SportHistoryDetailActivity.this.mFragmentMap != null) {
                        for (int i = 0; i < SportHistoryDetailActivity.this.mFragmentMap.size(); i++) {
                            Fragment fragment = (Fragment) SportHistoryDetailActivity.this.mFragmentMap.get(SportHistoryDetailActivity.this.mFragmentMap.keyAt(i));
                            if (fragment instanceof ISportHistoryDetailData) {
                                ((ISportHistoryDetailData) fragment).onLocationDataReady(SportHistoryDetailActivity.this.mLocationDataList);
                            }
                        }
                    }
                }
            });
        } else if ((this.mFlags & 8) != 0) {
            Global.getGlobalHeartHandler().post(new Runnable() {
                public void run() {
                    if (SportHistoryDetailActivity.this.mFragmentMap != null) {
                        for (int i = 0; i < SportHistoryDetailActivity.this.mFragmentMap.size(); i++) {
                            Fragment fragment = (Fragment) SportHistoryDetailActivity.this.mFragmentMap.get(SportHistoryDetailActivity.this.mFragmentMap.keyAt(i));
                            if (fragment instanceof ISportHistoryDetailData) {
                                ((ISportHistoryDetailData) fragment).onLocationDataReady(SportHistoryDetailActivity.this.mLocationDataList);
                            }
                        }
                    }
                }
            });
        }
    }

    private void testSaveLocData() {
        StringBuilder builder = new StringBuilder();
        builder.append("lat").append(",").append("lng").append(",").append("\r\n");
        for (SportLocationData data : ((TrailLocations) this.mLocationDataList.get(0)).locationDatas) {
            builder.append("N").append(data.mLatitude).append(",").append("E").append(data.mLongitude).append(",").append("\r\n");
        }
        String path = getExternalCacheDir().getAbsolutePath() + File.separator + "test.loc";
        File file = new File(path);
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileWriter writer = new FileWriter(path);
            writer.append(builder.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void loadDailyPropermenceInfo() {
        Global.getGlobalHeartHandler().post(new Runnable() {
            public void run() {
                if (SportHistoryDetailActivity.this.mSportSummary == null) {
                    Debug.m5i(SportHistoryDetailActivity.TAG, "loadHistoryDetail, summary is null");
                    return;
                }
                List<? extends DailyPerformanceInfo> dailyPerformanceInfoList = SportHistoryDetailActivity.this.mDailyPerformanceInfoManager.selectAll(SportHistoryDetailActivity.this, "track_id=?", new String[]{"" + SportHistoryDetailActivity.this.mSportSummary.getTrackId()}, "create_time ASC", null);
                if (dailyPerformanceInfoList != null && !dailyPerformanceInfoList.isEmpty()) {
                    int i;
                    int[] result = new int[dailyPerformanceInfoList.size()];
                    for (i = 0; i < dailyPerformanceInfoList.size(); i++) {
                        DailyPerformanceInfo currentDailyPermanceInfo = (DailyPerformanceInfo) dailyPerformanceInfoList.get(i);
                        if (currentDailyPermanceInfo == null || currentDailyPermanceInfo.getDailyPorpermence() < ((float) DailyPerformenceChatView.invalideLessPaintValue) || currentDailyPermanceInfo.getDailyPorpermence() > ((float) DailyPerformenceChatView.invalieBiggerPaintValue)) {
                            result[i] = DailyPerformenceChatView.invalidPointValue;
                        } else {
                            result[i] = (int) currentDailyPermanceInfo.getDailyPorpermence();
                        }
                    }
                    Log.i(SportHistoryDetailActivity.TAG, " doInBackground:intContnet:" + Utils.dump(result));
                    if (SportHistoryDetailActivity.this.mFragmentMap != null) {
                        for (i = 0; i < SportHistoryDetailActivity.this.mFragmentMap.size(); i++) {
                            Fragment fragment = (Fragment) SportHistoryDetailActivity.this.mFragmentMap.get(SportHistoryDetailActivity.this.mFragmentMap.keyAt(i));
                            if (fragment instanceof ISportHistoryDetailData) {
                                ((ISportHistoryDetailData) fragment).onDailyPropermenceDataReady(result);
                            }
                        }
                    }
                }
            }
        });
    }

    public void doSportHistoryDataRequest(int type) {
        if (type == 0) {
            loadHeartHistoryDetail();
        } else if (type == 1) {
            loadGPSHistoryDetail();
        } else if (type == 2) {
            loadDailyPropermenceInfo();
        }
    }

    private void initNewTrainView() {
        Builder builder = new Builder(this);
        if (this.mSportSummary != null && this.mSportSummary.getSportType() == 9) {
            builder.setMessage(getString(C0532R.string.complete_training_outdoor_riding_target_tips));
        } else if (this.mSportSummary == null || this.mSportSummary.getSportType() != 14) {
            builder.setMessage(getString(C0532R.string.complete_training_target_tips));
        } else {
            builder.setMessage(getString(C0532R.string.complete_training_swim_target_tips));
        }
        builder.setPositiveButton(getString(C0532R.string.sport_confirm_stop_sport), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (SportHistoryDetailActivity.this.mTrainInfo != null) {
                    TrainingInfoManager.getInstance(SportHistoryDetailActivity.this).updateTodayTaskStatus(Long.valueOf(SportHistoryDetailActivity.this.mTrainInfo.getTrainInfoId()));
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getString(C0532R.string.sport_confirm_not_stop_sport), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LogUtils.print(SportHistoryDetailActivity.TAG, "onClick cancel ");
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void checkIsCompleteTrainProject() {
        if (this.mSportSummary == null) {
            Debug.m5i(TAG, "checkIsCompleteTrainProject, summary or mode is null, mSportSummary:" + this.mSportSummary);
        } else if (!isFinishing() && !isDestroyed() && this.mIsFromEndSport) {
            this.mTrainInfo = TrainingInfoManager.getInstance(this).getTrainingInfo();
            if (this.mTrainInfo.getTrainStatus() == 1 || this.mTrainInfo.getTrainInfoId() == -1) {
                Debug.m5i(TAG, "today training has finished");
            } else if (!TrainingInfoManager.getInstance(this).isCompleteTrainInfo(this.mTrainInfo, this.mSportSummary)) {
            } else {
                if (this.mTrainingMode != 2) {
                    initNewTrainView();
                } else if (this.mTrainInfo != null) {
                    TrainingInfoManager.getInstance(this).updateTodayTaskStatus(Long.valueOf(this.mTrainInfo.getTrainInfoId()));
                }
            }
        }
    }

    public void onDeleteConfirm() {
        this.mIsSaveRecord = false;
        resetDeleteDialogStatus();
        SyncManager.getInstance().startDeleteSportData(this.mSportSummary, new IDeleteState() {
            public void onSwitchDeleteStateSuccess() {
                RecordGraphManager.getInstance(Global.getApplicationContext()).deleteCache(SportHistoryDetailActivity.this.mSportSummary.getTrackId());
                SportHistoryDetailActivity.this.getContentResolver().notifyChange(Uri.parse("content://com.huami.watch.sport.provider/sport_summary_data_info"), null);
                Intent intent = new Intent();
                intent.putExtra("is_refresh", true);
                SportHistoryDetailActivity.this.setResult(-1, intent);
                SportHistoryDetailActivity.this.finish();
                SportHistoryDetailActivity.this.overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
            }
        });
    }

    public void onDeleteDismiss() {
    }

    public boolean onKeyClick(HMKeyEvent theKey) {
        LogUtils.print(TAG, "onKeyClick: " + this.CURRNET_DELETE_DIALOG_STATUS + ", DELETE_DIALOG_IS_CLOSE:" + 0);
        if (theKey == HMKeyEvent.KEY_CENTER) {
            cancelAutoFlip();
        }
        switch (this.CURRNET_DELETE_DIALOG_STATUS) {
            case 0:
                if (theKey != HMKeyEvent.KEY_DOWN && theKey != HMKeyEvent.KEY_UP) {
                    if (theKey == HMKeyEvent.KEY_CENTER) {
                        if (this.mSportViewPager.getCurrentItem() != childCounts - 1) {
                            this.mSportViewPager.setCurrentItem(this.mSportViewPager.getCurrentItem() + 1, true);
                            break;
                        }
                        this.mSportViewPager.setCurrentItem(0, true);
                        break;
                    }
                }
                return true;
                break;
            case 1:
                if (theKey != HMKeyEvent.KEY_DOWN && theKey != HMKeyEvent.KEY_UP) {
                    if (theKey == HMKeyEvent.KEY_CENTER) {
                        switch (this.CURRNET_DELETE_DIALOG_OPEN_STATUS) {
                            case 100:
                                onDeleteConfirm();
                                resetDeleteDialogStatus();
                                break;
                            case 200:
                                onDeleteDismiss();
                                resetDeleteDialogStatus();
                                break;
                            default:
                                break;
                        }
                    }
                }
                toggleDeleteDialogStatus();
                break;
                break;
        }
        return false;
    }

    private void toggleDeleteDialogStatus() {
        int i = 100;
        if (this.CURRNET_DELETE_DIALOG_OPEN_STATUS == 100) {
            i = 200;
        }
        this.CURRNET_DELETE_DIALOG_OPEN_STATUS = i;
        switch (this.CURRNET_DELETE_DIALOG_OPEN_STATUS) {
            case 100:
                if (this.mSportHistroryDetaikFragment != null && this.mSportDeleteFragment != null) {
                    this.mSportDeleteFragment.onConfirmUI();
                    return;
                }
                return;
            case 200:
                this.mSportDeleteFragment.onDismissUi();
                return;
            default:
                return;
        }
    }

    private void resetDeleteDialogStatus() {
        this.CURRNET_DELETE_DIALOG_STATUS = 0;
        this.CURRNET_DELETE_DIALOG_OPEN_STATUS = 200;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent theKey) {
        return false;
    }

    public boolean hasRouteTrail() {
        return this.mIsHasTrail;
    }

    public void deleteRecord() {
        showDeleteRecordItemDialog();
    }

    public void exportRouteTrail() {
        Log.i("gpx", "oncreate : " + this.mSportSummary.getTrackId());
        Intent intent = new Intent(this, ExportGPXActivity.class);
        intent.putExtra("trackid", String.valueOf(this.mSportSummary.getTrackId()));
        intent.putExtra("name", "");
        intent.addFlags(268435456);
        startActivity(intent);
    }

    private void initFragmentList() {
        this.mFragmentTypeList.clear();
        if (getIsShowVo2maxPage()) {
            this.mFragmentTypeList.add(new FragmentTag(9));
        }
        if (this.mIsFromEndSport) {
            if (isTEAvailable()) {
                this.mFragmentTypeList.add(new FragmentTag(7));
            }
            if (this.recoveryTimeHout > 0) {
                this.mFragmentTypeList.add(new FragmentTag(8));
            }
        }
        this.mFragmentTypeList.add(new FragmentTag(0));
        if (this.mFragmentTypeList.size() > 2) {
            this.mSportViewPager.setOffscreenPageLimit(this.mFragmentTypeList.size());
        }
    }

    private void updateFragmentList() {
        if (SportType.isMixedSport(this.mSportType)) {
            for (SportSummary sportSummary : this.mChildSummarys) {
                this.mFragmentTypeList.add(new FragmentTag(sportSummary.getTrackId(), getChildFragmentType(sportSummary.getSportType())));
            }
        }
        if (hasLapType(1) || hasLapType(4)) {
            this.mFragmentTypeList.add(new FragmentTag(2));
        }
        if (this.mSportType != 18 && (this.mIsHasTrail || hasLapType(2) || hasLapType(8))) {
            this.mFragmentTypeList.add(new FragmentTag(1));
        }
        this.mFragmentTypeList.add(new FragmentTag(10));
        if (this.mFragmentTypeList.size() > 2) {
            this.mSportViewPager.setOffscreenPageLimit(this.mFragmentTypeList.size());
        }
    }

    private int getFragmentType(int position) {
        Log.i(TAG, " getFragmentType:isTeAvailable:" + isTEAvailable() + ",mIsFromEndSport:" + this.mIsFromEndSport + ",recoveryTimeHout:" + this.recoveryTimeHout + ",fragmentCount:" + this.mFragmentTypeList.size());
        if (this.mFragmentTypeList.isEmpty() || this.mFragmentTypeList.size() < position + 1) {
            return 0;
        }
        return ((FragmentTag) this.mFragmentTypeList.get(position)).type;
    }

    private int getChildFragmentType(int childType) {
        switch (childType) {
            case 1001:
                return 5;
            case 1009:
                return 4;
            case 1015:
                return 3;
            default:
                throw new IllegalArgumentException("err child sport type:" + childType);
        }
    }

    private int getChildType(int fragmentType) {
        switch (fragmentType) {
            case 3:
                return 1015;
            case 4:
                return 1009;
            case 5:
                return 1001;
            default:
                throw new IllegalArgumentException("err child sport type:" + fragmentType);
        }
    }

    private boolean getIsShowVo2maxPage() {
        if (!this.mIsFromEndSport) {
            return false;
        }
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(this);
        if ((this.mSportType != 1 || FirstBeatDataUtils.getRoundingValue(config.getLastRunVo2max()) == FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max())) && (this.mSportSummary == null || !this.mSportSummary.getChildTypes().contains(Integer.valueOf(1001)) || FirstBeatDataUtils.getRoundingValue(config.getLastRunVo2max()) == FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max()))) {
            Log.i(TAG, "getIsShowVo2maxPage:false");
            return false;
        } else if (21.0f > config.getCurrnetRunVo2max() || config.getCurrnetRunVo2max() > 87.99f) {
            Log.i(TAG, "getIsShowVo2maxPage:false is not in vo2max_region ");
            return false;
        } else {
            Log.i(TAG, "getIsShowVo2maxPage:true");
            return true;
        }
    }

    private SportSummary getSpecificSummary(int position) {
        if (this.mFragmentTypeList.isEmpty() || this.mFragmentTypeList.size() < position + 1) {
            return null;
        }
        FragmentTag fragmentTag = (FragmentTag) this.mFragmentTypeList.get(position);
        int type = fragmentTag.type;
        long tag = fragmentTag.tag;
        for (SportSummary summary : this.mChildSummarys) {
            if (summary.getSportType() == getChildType(type) && summary.getTrackId() == tag) {
                return summary;
            }
        }
        return null;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        if (this.currnetFragment != null && this.currnetPageChangeStatus == 0) {
            this.keyEventDispatcher.dispatchKeyevent(this.currnetFragment.getView(), event);
        } else if (this.currnetFragment == null && this.currnetPageChangeStatus == 0) {
            this.currnetFragment = this.mAdapter.getItem(0);
            this.keyEventDispatcher.dispatchKeyevent(this.currnetFragment.getView(), event);
        }
        return super.dispatchKeyEvent(event);
    }

    private void autoFlip() {
        LogUtil.m9i(true, TAG, "autoFlip");
        Global.getGlobalUIHandler().postDelayed(this.mAutoFlipRunnable, 5000);
    }

    private void cancelAutoFlip() {
        LogUtil.m9i(true, TAG, "cancelAutoFlip");
        if (this.mIsFromEndSport && !this.isRemoveAutoFlip) {
            Global.getGlobalUIHandler().removeCallbacks(this.mAutoFlipRunnable);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        cancelAutoFlip();
        return super.dispatchTouchEvent(ev);
    }
}
