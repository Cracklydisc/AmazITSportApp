package com.huami.watch.newsport.ui.history;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnScrollChangeListener;
import android.view.ViewGroup;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventDispatcher;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.newsport.ui.history.fragment.AchiveFragment;
import com.huami.watch.newsport.ui.history.fragment.MonthSettingFragment;
import com.huami.watch.newsport.ui.history.fragment.MonthTargetFragment;
import com.huami.watch.newsport.ui.history.fragment.RunVo2maxFragment;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryFragment;
import com.huami.watch.newsport.ui.history.fragment.SportHistoryFragment.ISportHistoryListener;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class SportHistoryActivity extends BaseActivity implements EventCallBack, ISportHistoryListener {
    private static final String TAG = SportHistoryActivity.class.getSimpleName();
    private AchiveFragment achiveFragment = null;
    private Fragment currentFragment = null;
    private volatile int currnetPageChangeStatus = 0;
    private KeyEventDispatcher keyEventDispatcher;
    private SportHistoryFragment mHistoryFragment = null;
    private ViewPagerPageIndicator mIndicator;
    private ViewPager mPager = null;
    private MonthSettingFragment monthSettingFragment = null;
    private MonthTargetFragment monthTargetFragment = null;
    private RunVo2maxFragment vo2MaxFragment = null;

    class C08211 implements OnScrollChangeListener {
        C08211() {
        }

        public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
            LogUtils.print(SportHistoryActivity.TAG, "onScrollChange");
        }
    }

    class C08222 implements OnPageChangeListener {
        C08222() {
        }

        public void onPageScrolled(int i, float v, int i1) {
            LogUtils.print(SportHistoryActivity.TAG, "onPageScrolled:" + i);
        }

        public void onPageSelected(int i) {
            LogUtils.print(SportHistoryActivity.TAG, "onPageSelected:" + i);
            SportHistoryActivity.this.setCurrentFragment(i);
        }

        public void onPageScrollStateChanged(int i) {
            LogUtils.print(SportHistoryActivity.TAG, "onPageScrollStateChanged:" + i);
            SportHistoryActivity.this.currnetPageChangeStatus = i;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_sport_history);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(this, (ViewGroup) findViewById(C0532R.id.container));
        this.mPager = (ViewPager) findViewById(C0532R.id.running_pager);
        init();
        this.isViewDispatchEvent = false;
        this.mPager.setOnScrollChangeListener(new C08211());
        this.mPager.setOnPageChangeListener(new C08222());
        setKeyEventListener(this);
    }

    private void setCurrentFragment(int index) {
        switch (index) {
            case 0:
                this.currentFragment = this.mHistoryFragment;
                return;
            case 1:
                this.currentFragment = this.vo2MaxFragment;
                return;
            case 2:
                this.currentFragment = this.monthTargetFragment;
                return;
            case 3:
                this.currentFragment = this.monthSettingFragment;
                return;
            case 6:
                this.currentFragment = this.achiveFragment;
                return;
            default:
                return;
        }
    }

    private void init() {
        this.mPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            public Fragment getItem(int position) {
                if (position == 0) {
                    if (SportHistoryActivity.this.mHistoryFragment == null) {
                        SportHistoryActivity.this.mHistoryFragment = SportHistoryFragment.newInstance();
                    }
                    SportHistoryActivity.this.currentFragment = SportHistoryActivity.this.mHistoryFragment;
                    return SportHistoryActivity.this.mHistoryFragment;
                } else if (position != 1) {
                    return null;
                } else {
                    if (SportHistoryActivity.this.vo2MaxFragment == null) {
                        SportHistoryActivity.this.vo2MaxFragment = RunVo2maxFragment.newInstance();
                    }
                    return SportHistoryActivity.this.vo2MaxFragment;
                }
            }

            public int getCount() {
                return UnitConvertUtils.isHuangheMode() ? 1 : 2;
            }
        });
        this.mIndicator = (ViewPagerPageIndicator) findViewById(C0532R.id.indicator);
        this.mIndicator.setViewPager(this.mPager);
        this.mIndicator.showIndicator(true);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4113 && data != null && this.mHistoryFragment != null) {
            boolean isRefresh = data.getBooleanExtra("is_refresh", false);
            Debug.m5i(TAG, "onActivityResult, isRefresh:" + isRefresh);
            if (isRefresh) {
                this.mHistoryFragment.updateData(this);
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (this.keyEventDispatcher == null) {
            this.keyEventDispatcher = new KeyEventDispatcher();
        }
        if (this.currentFragment != null && this.currnetPageChangeStatus == 0) {
            LogUtils.print(TAG, "dispatchKeyEvent currentFragment:" + this.currentFragment.getId());
            this.keyEventDispatcher.dispatchKeyevent(this.currentFragment.getView(), event);
        }
        return super.dispatchKeyEvent(event);
    }

    public void onItemSelected(SportSummary sportSummary) {
        Intent intent = new Intent(this, SportHistoryDetailActivity.class);
        intent.addFlags(536870912);
        Bundle bundle = new Bundle();
        bundle.putParcelable("sport_summary", sportSummary);
        intent.putExtras(bundle);
        startActivityForResult(intent, 4113);
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent != HMKeyEvent.KEY_UP) {
            if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
                if (this.mPager.getCurrentItem() % 2 == 0) {
                    boolean isListViewFocused = this.mHistoryFragment.getKeyEventStatus();
                    Log.i(TAG, " isListViewFocused:" + isListViewFocused);
                    if (!isListViewFocused) {
                        this.mPager.setCurrentItem((this.mPager.getCurrentItem() + 1) % 2);
                    }
                } else {
                    this.mPager.setCurrentItem((this.mPager.getCurrentItem() + 1) % 2);
                }
            } else if (hmKeyEvent == HMKeyEvent.KEY_DOWN) {
            }
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
    }
}
