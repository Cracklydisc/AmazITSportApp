package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmRelativeLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.Vo2maxStatusView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;

public class SportDetailVo2maxFragment extends Fragment {
    private static final String TAG = SportDetailVo2maxFragment.class.getSimpleName();
    private int currnetSportType = -1;
    private HmImageView iconRemindStatusView;
    private HmRelativeLayout llVo2MaxHelpContainer;
    private OutdoorSportSummary outdoorSportSummary;
    private HmTextView txtVo2maxRemindView;
    private HmTextView vo2maxLevelTextView;
    private Vo2maxStatusView vo2maxStatusChartView;
    private NumberTextView vo2maxValueNumberView;

    class C08442 implements OnClickListener {
        C08442() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(SportDetailVo2maxFragment.this.getActivity(), 0);
        }
    }

    public static SportDetailVo2maxFragment newInstance(SportSummary summary) {
        SportDetailVo2maxFragment fragment = new SportDetailVo2maxFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", summary);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.outdoorSportSummary = (OutdoorSportSummary) getArguments().getParcelable("sport_summary");
            this.currnetSportType = this.outdoorSportSummary.getSportType();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_sport_detail_vo2max, container, false);
        ArcScrollbarHelper.setArcScrollBarDrawable(root);
        this.vo2maxStatusChartView = (Vo2maxStatusView) root.findViewById(C0532R.id.vo2_max_chart);
        this.vo2maxValueNumberView = (NumberTextView) root.findViewById(C0532R.id.vo2max_value);
        this.vo2maxLevelTextView = (HmTextView) root.findViewById(C0532R.id.vo2_max_level_status);
        this.txtVo2maxRemindView = (HmTextView) root.findViewById(C0532R.id.txt_vo2max_remind);
        this.iconRemindStatusView = (HmImageView) root.findViewById(C0532R.id.icon_remind_status);
        this.llVo2MaxHelpContainer = (HmRelativeLayout) root.findViewById(C0532R.id.ll_vo2_max_title);
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(getActivity());
        final int runVo2MaxValue = FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max());
        Log.i(TAG, " runVo2maxValue:" + runVo2MaxValue);
        this.vo2maxValueNumberView.setText(String.valueOf(runVo2MaxValue));
        Log.i(TAG, "  need to show this page ");
        if ((this.currnetSportType == 1 || this.outdoorSportSummary.getChildTypes().contains(Integer.valueOf(1001))) && FirstBeatDataUtils.getRoundingValue(config.getLastRunVo2max()) > FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max())) {
            LogUtils.print(TAG, " configInfo vo2Max down");
            this.txtVo2maxRemindView.setText(getResources().getString(C0532R.string.sport_ability_down));
            this.iconRemindStatusView.setBackground(getResources().getDrawable(C0532R.drawable.sport_result_icon_down));
        }
        if ((this.currnetSportType == 1 || this.outdoorSportSummary.getChildTypes().contains(Integer.valueOf(1001))) && FirstBeatDataUtils.getRoundingValue(config.getLastRunVo2max()) < FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max())) {
            LogUtils.print(TAG, " configInfo vo2Max up ");
            this.txtVo2maxRemindView.setText(getResources().getString(C0532R.string.sport_ability_up));
            this.iconRemindStatusView.setBackground(getResources().getDrawable(C0532R.drawable.sport_result_icon_up));
        }
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                if (SportDetailVo2maxFragment.this.getActivity() != null) {
                    final int sportLevel = FirstBeatDataUtils.getFitnessLevelByVo2Max(SportDetailVo2maxFragment.this.getActivity(), runVo2MaxValue);
                    final String[] levelStrStatus = SportDetailVo2maxFragment.this.getActivity().getResources().getStringArray(C0532R.array.vo2_max_level_status);
                    final int[] vo2maxLevelValue = FirstBeatDataUtils.getFitnessLevelArrayValueByVo2maxAge(SportDetailVo2maxFragment.this.getActivity(), runVo2MaxValue);
                    Global.getGlobalUIHandler().post(new Runnable() {
                        public void run() {
                            Log.i(SportDetailVo2maxFragment.TAG, " runVo2maxValue:" + runVo2MaxValue + ",level:" + sportLevel + ",vo2maxLevelVlaue:" + vo2maxLevelValue.length);
                            int runViewLevel = sportLevel;
                            if (sportLevel < 1 || sportLevel > 7 || runVo2MaxValue < 0) {
                                SportDetailVo2maxFragment.this.vo2maxLevelTextView.setText("--");
                            } else {
                                SportDetailVo2maxFragment.this.vo2maxLevelTextView.setText(levelStrStatus[sportLevel - 1]);
                            }
                            SportDetailVo2maxFragment.this.vo2maxStatusChartView.setCurrentVo2maxValue(runVo2MaxValue, vo2maxLevelValue, sportLevel);
                        }
                    });
                }
            }
        });
        this.llVo2MaxHelpContainer.setOnClickListener(new C08442());
        return root;
    }
}
