package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.TrainingEffectView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.ResourceUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;

public class SportTEFragment extends Fragment {
    private static final String TAG = SportTEFragment.class.getSimpleName();
    private int currnetSportType = -1;
    private HmImageView imgUpAndDown = null;
    private OutdoorSportSummary outdoorSportSummary;
    private HmTextView sportResultUpAndDown = null;
    private HmTextView teTitleView;
    private NumberTextView teValueNumber;
    private TextView teValuePercent;
    private HmLinearLayout vo2MaxPageLayout;
    private HmLinearLayout vo2maxClickHelpLayout;

    class C08621 implements OnClickListener {
        C08621() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(SportTEFragment.this.getActivity(), 1);
        }
    }

    public static SportTEFragment newInstance(SportSummary summary) {
        SportTEFragment fragment = new SportTEFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", summary);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.outdoorSportSummary = (OutdoorSportSummary) getArguments().getParcelable("sport_summary");
            this.currnetSportType = this.outdoorSportSummary.getSportType();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_sport_te_page, container, false);
        ArcScrollbarHelper.setArcScrollBarDrawable(root);
        HmTextView hmTextView = (HmTextView) root.findViewById(C0532R.id.text_te_desc);
        this.imgUpAndDown = (HmImageView) root.findViewById(C0532R.id.img_up_and_donw);
        this.teTitleView = (HmTextView) root.findViewById(C0532R.id.text_te_title);
        if (!UnitConvertUtils.isZh()) {
            this.teTitleView.setTextSize(TypedValue.applyDimension(0, 16.0f, getResources().getDisplayMetrics()));
        }
        this.teTitleView.setOnClickListener(new C08621());
        this.teValueNumber = (NumberTextView) root.findViewById(C0532R.id.te_number_value);
        this.teValuePercent = (TextView) root.findViewById(C0532R.id.te_value_percent);
        this.vo2MaxPageLayout = (HmLinearLayout) root.findViewById(C0532R.id.second_page);
        this.vo2MaxPageLayout.setVisibility(8);
        this.vo2maxClickHelpLayout = (HmLinearLayout) root.findViewById(C0532R.id.vo2max_help_click_container);
        hmTextView.setText(Html.fromHtml(getString(ResourceUtils.getStringId(getActivity(), ResourceUtils.getStringArray(getActivity(), C0532R.array.sport_te_desc_array)[FirstBeatDataUtils.getTELevelByValue((float) (this.outdoorSportSummary.getTE() / 10))]))));
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(getActivity());
        if (this.outdoorSportSummary.getChildTypes() != null && this.outdoorSportSummary.getChildTypes().size() > 0) {
            Log.i(TAG, " childType:" + this.outdoorSportSummary.getChildTypes().contains(Integer.valueOf(1001)));
        }
        this.vo2MaxPageLayout.setVisibility(8);
        HmTextView sportAbilityStatusView = (HmTextView) root.findViewById(C0532R.id.sport_result_up_and_down);
        TrainingEffectView columGradientProgressBar = (TrainingEffectView) root.findViewById(C0532R.id.te_chart);
        float teProgress = ((float) this.outdoorSportSummary.getTE()) / 10.0f;
        LogUtils.print(TAG, "configInfo: te:" + teProgress + "," + config.toString());
        columGradientProgressBar.setProgressLevel(teProgress);
        this.teValueNumber.setText(String.valueOf(teProgress));
        this.teValuePercent.setText(String.valueOf("/5"));
        return root;
    }
}
