package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.huami.watch.newsport.common.model.SportSummary;

public class BaseDetailFragment extends Fragment {
    private static final String TAG = BaseDetailFragment.class.getSimpleName();
    protected SportSummary mSportSummary = null;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSportSummary = (SportSummary) getArguments().getParcelable("sport_summary");
    }
}
