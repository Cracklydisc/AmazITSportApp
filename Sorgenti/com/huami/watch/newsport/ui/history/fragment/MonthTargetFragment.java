package com.huami.watch.newsport.ui.history.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmKeyEventListView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.ui.view.CircleProgressView;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

public class MonthTargetFragment extends Fragment {
    private static final String TAG = MonthTargetFragment.class.getName();
    private ItemInfo mCrossingItemInfo = new ItemInfo();
    private ItemInfo mEllipiticalItemInfo = new ItemInfo();
    private ItemInfo mIndoorRidingItemInfo = new ItemInfo();
    private ItemInfo mIndoorRunItemInfo = new ItemInfo();
    private HmKeyEventListView mListView = null;
    private MonthAdapter mMonthAdapter = null;
    private ItemInfo mOpenSwimInfo = new ItemInfo();
    private ItemInfo mOutdoorRidingItemInfo = new ItemInfo();
    private ItemInfo mRunningItemInfo = new ItemInfo();
    private SyncDatabaseManager<SportSummary> mSportSummaryDatabaseManager;
    private ItemInfo mSwimItemInfo = new ItemInfo();
    private ItemInfo mTriathlonItemInfo = new ItemInfo();
    private ItemInfo mWalkingItemInfo = new ItemInfo();

    private class GetSportHistoryTask extends AsyncTask<Void, Void, List<SportSummary>> {
        private WeakReference<Context> mContextRef = null;

        public GetSportHistoryTask(Context context) {
            this.mContextRef = new WeakReference(context);
        }

        protected List<SportSummary> doInBackground(Void... params) {
            if (MonthTargetFragment.this.isDetached() || !MonthTargetFragment.this.isAdded()) {
                return null;
            }
            Context context = (Context) this.mContextRef.get();
            if (context == null) {
                return null;
            }
            if (MonthTargetFragment.this.mSportSummaryDatabaseManager == null) {
                MonthTargetFragment.this.mSportSummaryDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context));
            }
            List<? extends SportSummary> sportSummaries = MonthTargetFragment.this.mSportSummaryDatabaseManager.selectAll(MonthTargetFragment.this.getActivity(), null, null, null, null);
            List<SportSummary> sportSummariesAvailable = new LinkedList();
            if (sportSummaries != null) {
                for (SportSummary sportSummary : sportSummaries) {
                    if (sportSummary.getCurrentStatus() == 0 || sportSummary.getCurrentStatus() == 1 || sportSummary.getCurrentStatus() == 7 || SportType.isChildSport(sportSummary.getSportType())) {
                        Debug.m6w(MonthTargetFragment.TAG, "sport summary is not finished : " + sportSummary);
                    } else {
                        sportSummariesAvailable.add(sportSummary);
                    }
                }
            }
            return sportSummariesAvailable;
        }

        protected void onPostExecute(List<SportSummary> summaries) {
            if (!MonthTargetFragment.this.isDetached() && MonthTargetFragment.this.isAdded() && summaries != null && !summaries.isEmpty()) {
                for (SportSummary summary : summaries) {
                    ItemInfo access$300;
                    switch (summary.getSportType()) {
                        case 1:
                            access$300 = MonthTargetFragment.this.mRunningItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mRunningItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 6:
                            access$300 = MonthTargetFragment.this.mWalkingItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mWalkingItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 7:
                            access$300 = MonthTargetFragment.this.mCrossingItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mCrossingItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 8:
                            access$300 = MonthTargetFragment.this.mIndoorRunItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mIndoorRunItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 9:
                            access$300 = MonthTargetFragment.this.mOutdoorRidingItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mOutdoorRidingItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 10:
                            access$300 = MonthTargetFragment.this.mIndoorRidingItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mIndoorRidingItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 12:
                            access$300 = MonthTargetFragment.this.mEllipiticalItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mEllipiticalItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 14:
                            access$300 = MonthTargetFragment.this.mSwimItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mSwimItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 15:
                            access$300 = MonthTargetFragment.this.mOpenSwimInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mOpenSwimInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        case 2001:
                            access$300 = MonthTargetFragment.this.mTriathlonItemInfo;
                            access$300.dis += (float) summary.getTrimedDistance();
                            access$300 = MonthTargetFragment.this.mTriathlonItemInfo;
                            access$300.costTime += (long) summary.getSportDuration();
                            break;
                        default:
                            break;
                    }
                }
                MonthTargetFragment.this.mMonthAdapter.notifyDataSetChanged();
            }
        }
    }

    private class ItemInfo {
        long costTime;
        float dis;

        private ItemInfo() {
            this.dis = 0.0f;
            this.costTime = 0;
        }
    }

    private class MonthAdapter extends BaseAdapter {

        private class MonthViewHolder {
            CircleProgressView circleProgressView;
            TextView contentView;
            ImageView imgView;
            NumberTextView numberTextView;
            TextView titleView;

            private MonthViewHolder() {
            }
        }

        private MonthAdapter() {
        }

        public int getCount() {
            return 10;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            MonthViewHolder viewHolder;
            if (convertView == null) {
                convertView = MonthTargetFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.fragment_month_target_item, parent, false);
                MonthAdapter monthAdapter = this;
                viewHolder = new MonthViewHolder();
                viewHolder.imgView = (ImageView) convertView.findViewById(C0532R.id.item_img);
                viewHolder.titleView = (TextView) convertView.findViewById(C0532R.id.item_title);
                viewHolder.contentView = (TextView) convertView.findViewById(C0532R.id.item_target);
                viewHolder.circleProgressView = (CircleProgressView) convertView.findViewById(C0532R.id.circle_progress_view);
                viewHolder.numberTextView = (NumberTextView) convertView.findViewById(C0532R.id.number_text_view);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (MonthViewHolder) convertView.getTag();
            }
            int sportType = getSportType(position);
            BaseConfig config = DataManager.getInstance().getSportConfig(MonthTargetFragment.this.getActivity(), sportType);
            double molecule;
            double denominator;
            int percent;
            TextView textView;
            MonthTargetFragment monthTargetFragment;
            Object[] objArr;
            switch (sportType) {
                case 1:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_running));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_run));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mRunningItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 6:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_walk));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_walk));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mWalkingItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 7:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_trail_running));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_cross));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mCrossingItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 8:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_treadmill));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_indoor_run));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mIndoorRunItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 9:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_outdoor_riding));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mOutdoorRidingItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_outdoor_riding));
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 10:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_indoor_riding));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_indoor_riding));
                    textView = viewHolder.contentView;
                    monthTargetFragment = MonthTargetFragment.this;
                    objArr = new Object[2];
                    objArr[0] = parseSecondToDefaultFormattedTime(MonthTargetFragment.this.mIndoorRidingItemInfo.costTime);
                    objArr[1] = parseSecondToDefaultFormattedTime((long) config.getMonthTargetCostTime());
                    textView.setText(monthTargetFragment.getString(C0532R.string.history_month_cost, objArr));
                    if (config.getMonthTargetCostTime() == 0) {
                        percent = 100;
                    } else {
                        percent = (int) ((MonthTargetFragment.this.mIndoorRidingItemInfo.costTime * 100) / ((long) config.getMonthTargetCostTime()));
                    }
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 12:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_ellipticals));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_elliptical));
                    textView = viewHolder.contentView;
                    monthTargetFragment = MonthTargetFragment.this;
                    objArr = new Object[2];
                    objArr[0] = parseSecondToDefaultFormattedTime(MonthTargetFragment.this.mEllipiticalItemInfo.costTime);
                    objArr[1] = parseSecondToDefaultFormattedTime((long) config.getMonthTargetCostTime());
                    textView.setText(monthTargetFragment.getString(C0532R.string.history_month_cost, objArr));
                    if (config.getMonthTargetCostTime() == 0) {
                        percent = 100;
                    } else {
                        percent = (int) ((MonthTargetFragment.this.mEllipiticalItemInfo.costTime * 100) / ((long) config.getMonthTargetCostTime()));
                    }
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 14:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_indoor_swimming));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_indoor_swim));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mSwimItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 15:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_outdoor_swimming));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_open_water_swim));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mOpenSwimInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
                case 2001:
                    viewHolder.imgView.setImageDrawable(MonthTargetFragment.this.getActivity().getDrawable(C0532R.drawable.sport_history_icon_running));
                    viewHolder.titleView.setText(MonthTargetFragment.this.getString(C0532R.string.sport_widget_title_triathlon));
                    molecule = UnitConvertUtils.convertDistanceToKm((double) MonthTargetFragment.this.mTriathlonItemInfo.dis);
                    denominator = (double) (((float) config.getMonthTargetDistance()) / 1000.0f);
                    if (UnitConvertUtils.isImperial()) {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis_imperial, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    } else {
                        viewHolder.contentView.setText(MonthTargetFragment.this.getString(C0532R.string.history_month_dis, new DecimalFormat("#0.00").format(molecule), Integer.valueOf((int) denominator)));
                    }
                    percent = (int) ((100.0d * molecule) / denominator);
                    if (percent > 100) {
                        percent = 100;
                    }
                    viewHolder.circleProgressView.setProgress(percent);
                    viewHolder.numberTextView.setText(percent + "%");
                    break;
            }
            return convertView;
        }

        private int getSportType(int pos) {
            switch (pos) {
                case 0:
                    return 1;
                case 1:
                    return 6;
                case 2:
                    return 8;
                case 3:
                    return 9;
                case 4:
                    return 10;
                case 5:
                    return 7;
                case 6:
                    return 12;
                case 7:
                    return 2001;
                case 8:
                    return 14;
                case 9:
                    return 15;
                default:
                    return -1;
            }
        }

        private String parseSecondToDefaultFormattedTime(long millSec) {
            long second = millSec / 1000;
            int h = (int) (second / 3600);
            int min = (int) ((second % 3600) / 60);
            int sec = (int) (second % 60);
            StringBuilder sb = new StringBuilder();
            if (h > 9) {
                sb.append(h);
            } else {
                sb.append("0").append(h);
            }
            sb.append(":");
            if (min > 9) {
                sb.append(min);
            } else {
                sb.append("0").append(min);
            }
            sb.append(":");
            if (sec > 9) {
                sb.append(sec);
            } else {
                sb.append("0").append(sec);
            }
            return sb.toString();
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_month_target, container, false);
        this.mListView = (HmKeyEventListView) root.findViewById(C0532R.id.sport_history_list_view);
        this.mListView.addHeaderView(inflater.inflate(C0532R.layout.fragment_month_target_title, this.mListView, false), null, false);
        this.mMonthAdapter = new MonthAdapter();
        this.mListView.setAdapter(this.mMonthAdapter);
        new GetSportHistoryTask(getActivity()).execute(new Void[0]);
        return root;
    }

    public void onResume() {
        super.onResume();
        if (this.mMonthAdapter != null) {
            this.mMonthAdapter.notifyDataSetChanged();
        }
    }
}
