package com.huami.watch.newsport.ui.history.fragment;

import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.adapter.MonthSettingItemAdapter;
import com.huami.watch.newsport.ui.adapter.MonthSettingItemAdapter.IMonthClickListener;
import com.huami.watch.newsport.ui.history.MonthSettingsActivity;
import com.huami.watch.newsport.ui.view.SportMainSettingFace;
import com.huami.watch.newsport.ui.view.SportSettingWheelList;

public class MonthSettingFragment extends Fragment implements IMonthClickListener {
    private MonthSettingItemAdapter mAdapter = null;
    private SportMainSettingFace mSportMainSettingFace = null;
    private SportSettingWheelList mWheelList = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_month_setting, container, false);
        initSportList(root);
        return root;
    }

    private void initSportList(View root) {
        this.mSportMainSettingFace = (SportMainSettingFace) root.findViewById(C0532R.id.main_list);
        this.mSportMainSettingFace.setDismissAllow(true);
        int marginTop = getResources().getDimensionPixelSize(C0532R.dimen.settings_item_layout_drawing_top);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        this.mWheelList = new SportSettingWheelList(this.mSportMainSettingFace, new RectF(0.0f, 0.0f, (float) dm.widthPixels, (float) dm.widthPixels));
        this.mWheelList.setMarginTop(marginTop);
        this.mAdapter = new MonthSettingItemAdapter(this.mSportMainSettingFace);
        this.mAdapter.setMonthClickListener(this);
        this.mWheelList.getXContext().setParentNeedTouchEvent(true);
        this.mWheelList.setViewAdapter(this.mAdapter);
        this.mWheelList.standup(false, true);
        this.mSportMainSettingFace.addDrawingItem(this.mWheelList);
        this.mSportMainSettingFace.postInvalidate();
    }

    public void onMonthTargetClicked() {
        startActivity(new Intent(getActivity(), MonthSettingsActivity.class));
    }
}
