package com.huami.watch.newsport.ui.history.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;

public class MoreOperationFragment extends BaseDetailFragment {
    private static final String TAG = MoreOperationFragment.class.getName();
    private LinearLayout mContainer = null;
    private boolean mIsFromEndSport = false;
    private IOperationListener mListener = null;

    public interface IOperationListener {
        void deleteRecord();

        void exportRouteTrail();

        boolean hasRouteTrail();
    }

    private class ViewHolder {
        private View view;
        private int viewType = 0;

        public ViewHolder(final int type, View view) {
            this.viewType = type;
            this.view = view;
            ((HmHaloButton) view.findViewById(C0532R.id.item_view)).setWithHalo(false);
            switch (type) {
                case 0:
                    ((HmHaloButton) view.findViewById(C0532R.id.item_view)).setText(MoreOperationFragment.this.getString(C0532R.string.export_route_trail));
                    break;
                case 1:
                    ((HmHaloButton) view.findViewById(C0532R.id.item_view)).setText(MoreOperationFragment.this.getString(C0532R.string.delete_sport_summary));
                    break;
            }
            ((HmHaloButton) view.findViewById(C0532R.id.item_view)).setOnClickListener(new OnClickListener(MoreOperationFragment.this) {
                public void onClick(View v) {
                    if (MoreOperationFragment.this.mListener != null) {
                        switch (type) {
                            case 0:
                                MoreOperationFragment.this.mListener.exportRouteTrail();
                                return;
                            case 1:
                                MoreOperationFragment.this.mListener.deleteRecord();
                                return;
                            default:
                                return;
                        }
                    }
                }
            });
        }
    }

    public static MoreOperationFragment newInstance(SportSummary sportSummary, boolean isFromEnd) {
        MoreOperationFragment fragment = new MoreOperationFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", sportSummary);
        args.putBoolean("is_from_end_sport", isFromEnd);
        fragment.setArguments(args);
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IOperationListener) {
            this.mListener = (IOperationListener) activity;
        }
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mIsFromEndSport = getArguments().getBoolean("is_from_end_sport", false);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_more_operation, container, false);
        this.mContainer = (LinearLayout) root.findViewById(C0532R.id.content_container);
        init();
        return root;
    }

    private void init() {
        if (this.mListener != null) {
            if (!(!this.mListener.hasRouteTrail() || SportType.isMixedSport(this.mSportSummary.getSportType()) || 18 == this.mSportSummary.getSportType())) {
                this.mContainer.addView(newItem(0).view);
            }
            this.mContainer.addView(newItem(1).view);
        }
    }

    private ViewHolder newItem(int type) {
        return new ViewHolder(type, getActivity().getLayoutInflater().inflate(C0532R.layout.item_more_operation, null, false));
    }
}
