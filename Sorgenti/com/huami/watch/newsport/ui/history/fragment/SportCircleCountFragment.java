package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.widget.HmKeyEventListView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import java.util.ArrayList;
import java.util.List;

public class SportCircleCountFragment extends BaseDetailFragment {
    private static final String TAG = SportCircleCountFragment.class.getSimpleName();
    private HmTextView lapNumView = null;
    private BaseLapAdapter mLapAdapter = null;
    private HmTextView mLapDisView = null;
    private HmTextView mLapPaceView = null;
    private int mLapType = 0;
    private HmKeyEventListView mListView = null;
    private SyncDatabaseManager<RunningInfoPerKM> mPerKmSyncDBManager = null;
    private SyncDatabaseManager<RunningInfoPerLap> mSyncDBManager = null;

    class C08411 implements Runnable {
        C08411() {
        }

        public void run() {
            if (!SportCircleCountFragment.this.isDetached() && SportCircleCountFragment.this.isAdded()) {
                final List<? extends RunningInfoPerLap> laps = SportCircleCountFragment.this.mSyncDBManager.selectAll(SportCircleCountFragment.this.getActivity(), "track_id=? AND lap_type=?", new String[]{"" + SportCircleCountFragment.this.mSportSummary.getTrackId(), "" + SportCircleCountFragment.this.mLapType}, null, null);
                if (laps != null) {
                    Global.getGlobalUIHandler().post(new Runnable() {
                        public void run() {
                            if (!SportCircleCountFragment.this.isDetached() && SportCircleCountFragment.this.isAdded()) {
                                SportCircleCountFragment.this.mLapAdapter = new LapAdapter(laps);
                                SportCircleCountFragment.this.mListView.setAdapter(SportCircleCountFragment.this.mLapAdapter);
                                SportCircleCountFragment.this.mLapAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                }
            }
        }
    }

    private abstract class BaseLapAdapter extends BaseAdapter {
        public abstract void onViewUpdate(ItemViewHolder itemViewHolder, int i);

        public int getCount() {
            return 0;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ItemViewHolder viewHolder;
            if (convertView == null) {
                convertView = SportCircleCountFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.item_detail_lap_info, null);
                viewHolder = new ItemViewHolder();
                viewHolder.container = convertView;
                viewHolder.lapNumView = (TextView) convertView.findViewById(C0532R.id.lap_num);
                viewHolder.lapTimeView = (TextView) convertView.findViewById(C0532R.id.lap_time);
                viewHolder.lapDisView = (TextView) convertView.findViewById(C0532R.id.lap_dis);
                viewHolder.lapPaceView = (TextView) convertView.findViewById(C0532R.id.lap_pace);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ItemViewHolder) convertView.getTag();
            }
            onViewUpdate(viewHolder, position);
            return convertView;
        }

        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            }
            return 1;
        }
    }

    private class ItemViewHolder {
        View container;
        TextView lapDisView;
        TextView lapNumView;
        TextView lapPaceView;
        TextView lapTimeView;

        private ItemViewHolder() {
        }
    }

    private class LapAdapter extends BaseLapAdapter {
        List<? extends RunningInfoPerLap> mLaps = new ArrayList();

        public LapAdapter(List<? extends RunningInfoPerLap> laps) {
            super();
            if (laps != null && !laps.isEmpty()) {
                this.mLaps = laps;
            }
        }

        public void onViewUpdate(ItemViewHolder viewHolder, int position) {
            RunningInfoPerLap lap = (RunningInfoPerLap) this.mLaps.get(position);
            if (position % 2 == 0) {
                viewHolder.container.setBackgroundColor(-16777216);
            } else {
                viewHolder.container.setBackgroundColor(SportCircleCountFragment.this.getActivity().getResources().getColor(C0532R.color.item_detail_lap_info_bg_color));
            }
            viewHolder.lapNumView.setText("" + (lap.getLapNum() + 1));
            viewHolder.lapTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime(lap.getCostTime()));
            if (SportCircleCountFragment.this.mSportSummary.getSportType() == 10) {
                if (((OutdoorSportSummary) SportCircleCountFragment.this.mSportSummary).getDeviceType() != -1) {
                    viewHolder.lapDisView.setText("" + lap.getLapCadence());
                } else {
                    viewHolder.lapDisView.setText(SportCircleCountFragment.this.getString(C0532R.string.sport_pause_heart_shown));
                }
            } else if (SportCircleCountFragment.this.mSportSummary.getSportType() == 12) {
                viewHolder.lapDisView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) lap.getLapCalories()))}));
            } else if (!SportType.isSwimMode(SportCircleCountFragment.this.mSportSummary.getSportType())) {
                viewHolder.lapDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(String.valueOf(lap.getDistance() / 1000.0f))), false));
            } else if (((OutdoorSportSummary) SportCircleCountFragment.this.mSportSummary).getUnit() == 0) {
                viewHolder.lapDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) lap.getDistance(), ((OutdoorSportSummary) SportCircleCountFragment.this.mSportSummary).getUnit())));
            } else {
                viewHolder.lapDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) lap.getDistance(), ((OutdoorSportSummary) SportCircleCountFragment.this.mSportSummary).getUnit())));
            }
            if (SportCircleCountFragment.this.mSportSummary.getSportType() == 12) {
                viewHolder.lapPaceView.setVisibility(8);
            } else if (SportType.isSwimMode(SportCircleCountFragment.this.mSportSummary.getSportType())) {
                viewHolder.lapPaceView.setText(DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter((float) lap.getPace(), ((OutdoorSportSummary) SportCircleCountFragment.this.mSportSummary).getUnit())));
            } else if (SportCircleCountFragment.this.mSportSummary.getSportType() == 10) {
                viewHolder.lapPaceView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) lap.getLapCalories()))}));
            } else if (SportCircleCountFragment.this.mSportSummary.getSportType() == 9) {
                viewHolder.lapPaceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed((float) (1.0d / lap.getPace()))), true));
            } else if (SportCircleCountFragment.this.mSportSummary.getSportType() == 11) {
                viewHolder.lapPaceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) (Double.compare(0.0d, lap.getMaxPace()) == 0 ? 0.0f : (float) (1.0d / lap.getMaxPace()))), true));
            } else {
                viewHolder.lapPaceView.setText(DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace((float) lap.getPace())));
            }
        }

        public int getCount() {
            return this.mLaps.size();
        }
    }

    private class PerKmAdapter extends BaseLapAdapter {
        List<? extends RunningInfoPerKM> mLaps;

        public void onViewUpdate(ItemViewHolder viewHolder, int position) {
            if (viewHolder != null) {
                RunningInfoPerKM lap = (RunningInfoPerKM) this.mLaps.get(position - 1);
                viewHolder.lapNumView.setText("" + position);
                viewHolder.lapTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime(lap.getCostTime()));
                viewHolder.lapDisView.setText("1");
                viewHolder.lapPaceView.setText(DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace((float) lap.getPace())));
            }
        }

        public int getCount() {
            return this.mLaps.size() + 1;
        }
    }

    public static SportCircleCountFragment newInstance(SportSummary sportSummary, int lapType) {
        SportCircleCountFragment fragment = new SportCircleCountFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", sportSummary);
        args.putInt("is_auto_lap", lapType);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSyncDBManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(getActivity()));
        this.mPerKmSyncDBManager = new SyncDatabaseManager(RunningInfoPerKMDao.getInstance(getActivity()));
        this.mLapType = getArguments().getInt("is_auto_lap", 0);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(C0532R.layout.fragment_history_detail_lap, container, false);
        this.mListView = (HmKeyEventListView) viewRoot.findViewById(C0532R.id.lap_list_view);
        this.mLapDisView = (HmTextView) viewRoot.findViewById(C0532R.id.lap_dis);
        this.mLapPaceView = (HmTextView) viewRoot.findViewById(C0532R.id.lap_pace);
        this.lapNumView = (HmTextView) viewRoot.findViewById(C0532R.id.lap_num);
        ArcScrollbarHelper.setArcScrollBarDrawable(this.mListView);
        switch (this.mLapType) {
            case 0:
                ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(getString(C0532R.string.detail_route_detail_manual_lap_title));
                break;
            case 1:
                ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(getString(C0532R.string.detail_route_detail_auto_lap_title));
                break;
            case 2:
                ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(getString(C0532R.string.detail_route_detail_train_lap_title));
                ((HmTextView) viewRoot.findViewById(C0532R.id.lap_num)).setText(getString(C0532R.string.detail_lap_serial_number));
                break;
            case 3:
                ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(getString(C0532R.string.skiing_lap_title));
                ((HmTextView) viewRoot.findViewById(C0532R.id.lap_num)).setText(getString(C0532R.string.detail_lap_serial_number));
                break;
            default:
                ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(getString(C0532R.string.detail_route_detail_manual_lap_title));
                break;
        }
        initTitle();
        TextView emptyHead = new TextView(getActivity());
        emptyHead.setHeight((int) TypedValue.applyDimension(0, 120.0f, getResources().getDisplayMetrics()));
        this.mListView.addHeaderView(emptyHead);
        this.mListView.setDivider(null);
        loadLapInfos();
        return viewRoot;
    }

    private void initTitle() {
        int sportType = this.mSportSummary.getSportType();
        if (sportType == 10) {
            this.mLapDisView.setText(getString(C0532R.string.outdoor_cadence));
            this.mLapPaceView.setText(getString(C0532R.string.running_calorie_desc));
            this.lapNumView.setText(getString(C0532R.string.detail_lap_serial_number));
        } else if (sportType == 12) {
            this.mLapPaceView.setVisibility(8);
            this.mLapDisView.setText(getString(C0532R.string.running_calorie_desc));
            this.lapNumView.setText(getString(C0532R.string.detail_lap_serial_number));
        } else if (sportType == 9) {
            this.mLapPaceView.setText(getString(C0532R.string.running_avg_speed_desc_simple));
        } else if (sportType == 11) {
            this.mLapDisView.setText(getString(C0532R.string.skiing_lap_downhill_dis_title));
            this.mLapPaceView.setText(getString(C0532R.string.skiing_downhill_max_speed));
        }
    }

    private void loadLapInfos() {
        Global.getGlobalWorkHandler().post(new C08411());
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
