package com.huami.watch.newsport.ui.history.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmKeyEventListView;
import com.huami.watch.common.widget.HmKeyEventListView.ItemFocusedListener;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.ui.view.ExerciseLoadProgressView;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.List;

public class SportHistoryFragment extends Fragment {
    private static SportStatistic mSportStatistic = null;
    private NumberTextView exerciseLoadNumberView;
    private HmTextView exerciseLoadSuggessView;
    private Calendar mCalendar;
    private DataManager mDataManager;
    private View mFootView;
    private View mHeadView;
    boolean mIsImperial;
    private boolean mIsUpdatingSportSummary;
    private SportHistoryAdapter mSportHistoryAdapter;
    private ISportHistoryListener mSportHistoryListener;
    private HmKeyEventListView mSportListView;
    private volatile List<SportSummary> mSportSummaries;
    private SyncDatabaseManager<SportSummary> mSportSummaryDatabaseManager;
    private HmTextView mTotalCalorie;
    private HmTextView mTotalCount;
    private TextView mTotalDistance;
    private ExerciseLoadProgressView trainLoadProgressView;
    private HmTextView trainLoadTitleView;

    public interface ISportHistoryListener {
        void onItemSelected(SportSummary sportSummary);
    }

    class C08501 implements OnClickListener {
        C08501() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(SportHistoryFragment.this.getActivity(), 2);
        }
    }

    class C08532 implements Runnable {

        class C08522 implements Runnable {
            C08522() {
            }

            public void run() {
                SportHistoryFragment.this.exerciseLoadNumberView.setText("--");
                SportHistoryFragment.this.exerciseLoadSuggessView.setText(FirstBeatDataUtils.getSuggessTextByCurrentLoad(SportHistoryFragment.this.getActivity(), -1, 0, 0, 0));
                SportHistoryFragment.this.trainLoadProgressView.setTraingLoadProgress(0, 0, 0, 0);
            }
        }

        C08532() {
        }

        public void run() {
            if (SportHistoryFragment.this.getActivity() == null) {
                Log.i("SportHistoryFragment", " getTrainLoadData getActivity is null ");
                return;
            }
            SyncDatabaseManager<SportThaInfo> mSportThaInfoDataManager = new SyncDatabaseManager(SportThaInfoDao.getmInstance(SportHistoryFragment.this.getActivity()));
            Log.i("SportHistoryFragment", " currnetDayId:" + FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis()));
            final SportThaInfo sportThaInfo = (SportThaInfo) mSportThaInfoDataManager.select(SportHistoryFragment.this.getActivity(), "select * from sport_tha_info where dayId=? ; ", new String[]{String.valueOf(currentDayId)});
            if (sportThaInfo != null) {
                Log.i("SportHistoryFragment", " getLastSportThaInfo:" + sportThaInfo.toString());
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        Log.i("SportHistoryFragment", " trainLoadData isDetached:" + SportHistoryFragment.this.isDetached() + ",isAdded:" + SportHistoryFragment.this.isAdded());
                        if (!SportHistoryFragment.this.isDetached() && SportHistoryFragment.this.isAdded()) {
                            SportHistoryFragment.this.exerciseLoadNumberView.setText(String.valueOf(sportThaInfo.getWtlSum() < 0 ? "--" : Integer.valueOf(sportThaInfo.getWtlSum())));
                            SportHistoryFragment.this.exerciseLoadSuggessView.setText(FirstBeatDataUtils.getSuggessTextByCurrentLoad(SportHistoryFragment.this.getActivity(), sportThaInfo.getWtlSum(), sportThaInfo.getWtlSumOptimalMin(), sportThaInfo.getWtlSumOptimalMax(), sportThaInfo.getWtlSumOverreaching()));
                            SportHistoryFragment.this.trainLoadProgressView.setTraingLoadProgress(sportThaInfo.getWtlSum(), sportThaInfo.getWtlSumOptimalMin(), sportThaInfo.getWtlSumOptimalMax(), sportThaInfo.getWtlSumOverreaching());
                        }
                    }
                });
                return;
            }
            Log.i("SportHistoryFragment", " getLastSportThaInfo: is null ");
            Global.getGlobalUIHandler().post(new C08522());
        }
    }

    class C08543 implements ItemFocusedListener {
        C08543() {
        }

        public void hasItemFocused(boolean b) {
            Log.i("SportHistoryFragment", " hasItemFocused_status:" + b);
        }
    }

    private class GetSportHistoryTask extends AsyncTask<Void, Void, List<SportSummary>> {
        private WeakReference<Context> mContextRef = null;

        public GetSportHistoryTask(Context context) {
            this.mContextRef = new WeakReference(context);
        }

        protected List<SportSummary> doInBackground(Void... params) {
            Context context = (Context) this.mContextRef.get();
            if (context == null) {
                return null;
            }
            if (SportHistoryFragment.this.mSportSummaryDatabaseManager == null) {
                SportHistoryFragment.this.mSportSummaryDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context));
            }
            List<SportSummary> sportSummariesAvailable = SportHistoryFragment.this.mSportSummaryDatabaseManager.selectAll(SportHistoryFragment.this.getActivity(), "current_status  not in (?,?,?,?)  and  type  not in (?,?,?) ", new String[]{String.valueOf(0), String.valueOf(1), String.valueOf(7), String.valueOf(9), String.valueOf(1015), String.valueOf(1009), String.valueOf(1001)}, "start_time DESC", null);
            SportHistoryFragment.mSportStatistic = SportHistoryFragment.this.mDataManager.getSportStatistic(SportApplication.getInstance());
            return sportSummariesAvailable;
        }

        protected void onPostExecute(List<SportSummary> summaries) {
            SportHistoryFragment.this.mSportSummaries = summaries;
            Log.i("SportHistoryFragment", " Post Execute Size:" + (SportHistoryFragment.this.mSportSummaries != null ? SportHistoryFragment.this.mSportSummaries.size() : 0) + " ,isAdd:" + SportHistoryFragment.this.isAdded());
            if (SportHistoryFragment.this.isAdded()) {
                SportHistoryFragment.this.updateStatisticInfo();
                if (SportHistoryFragment.this.mSportSummaries != null && SportHistoryFragment.this.mSportSummaries.size() > 0 && SportHistoryFragment.this.mFootView != null && SportHistoryFragment.this.mSportListView != null) {
                    Log.i("SportHistoryFragment", " addFooterListView:Count:" + SportHistoryFragment.this.mSportListView.getFooterViewsCount());
                    if (SportHistoryFragment.this.mSportSummaries.size() < 7) {
                        SportHistoryFragment.this.mFootView.findViewById(C0532R.id.list_bottom_txt).setVisibility(8);
                    } else if (SportHistoryFragment.this.mSportListView.getFooterViewsCount() < 1) {
                        SportHistoryFragment.this.mFootView.findViewById(C0532R.id.list_bottom_txt).setVisibility(0);
                        SportHistoryFragment.this.mSportListView.addFooterView(SportHistoryFragment.this.mFootView, null, false);
                    }
                } else if (!(SportHistoryFragment.this.mSportListView == null && SportHistoryFragment.this.mFootView == null)) {
                    Log.i("SportHistoryFragment", " removeFooterView ");
                    SportHistoryFragment.this.mFootView.findViewById(C0532R.id.list_bottom_txt).setVisibility(8);
                }
                Log.i("SportHistoryFragment", " adapter data Size:" + SportHistoryFragment.this.mSportHistoryAdapter.getCount());
                SportHistoryFragment.this.mSportHistoryAdapter.notifyDataSetChanged();
                SportHistoryFragment.this.mIsUpdatingSportSummary = false;
                SportHistoryFragment.this.getTrainLoadData();
            }
        }
    }

    private class OnSportItemClickListener implements OnItemClickListener {
        private OnSportItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportHistoryFragment", "onItemClick : " + position + ", " + id);
            }
            if (id >= 0 && !SportHistoryFragment.this.mIsUpdatingSportSummary && SportHistoryFragment.this.mSportHistoryListener != null) {
                int index = (int) id;
                if (index < SportHistoryFragment.this.mSportSummaries.size()) {
                    SportHistoryFragment.this.mSportHistoryListener.onItemSelected((SportSummary) SportHistoryFragment.this.mSportSummaries.get(index));
                } else if (Global.DEBUG_LEVEL_3) {
                    Debug.m4e("SportHistoryFragment", "click index out of range. current index : " + index + ". current summary : " + SportHistoryFragment.this.mSportSummaries);
                }
            }
        }
    }

    private class SportHistoryAdapter extends BaseAdapter {

        private class ViewHolder {
            TextView mCostTimeView;
            TextView mDateView;
            TextView mDistanceDescView;
            TextView mDistanceView;
            ImageView mImageView;

            private ViewHolder() {
                this.mImageView = null;
                this.mDateView = null;
                this.mDistanceView = null;
                this.mDistanceDescView = null;
                this.mCostTimeView = null;
            }
        }

        private SportHistoryAdapter() {
        }

        public int getCount() {
            if (SportHistoryFragment.this.mSportSummaries == null) {
                return 0;
            }
            return SportHistoryFragment.this.mSportSummaries.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("SportHistoryFragment", "getItemId : " + position);
            }
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = SportHistoryFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.list_item_sport_history, parent, false);
                SportHistoryAdapter sportHistoryAdapter = this;
                holder = new ViewHolder();
                holder.mCostTimeView = (TextView) convertView.findViewById(C0532R.id.sport_cost_time);
                holder.mImageView = (ImageView) convertView.findViewById(C0532R.id.sport_icon);
                holder.mDateView = (TextView) convertView.findViewById(C0532R.id.sport_date);
                holder.mDistanceView = (TextView) convertView.findViewById(C0532R.id.sport_head_value);
                holder.mDistanceDescView = (TextView) convertView.findViewById(C0532R.id.sport_head_unit);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            OutdoorSportSummary summary = (OutdoorSportSummary) SportHistoryFragment.this.mSportSummaries.get(position);
            int resourceId = -1;
            if (summary.getSportType() == 1) {
                resourceId = C0532R.drawable.sport_history_icon_running;
            } else if (summary.getSportType() == 6) {
                resourceId = C0532R.drawable.sport_history_icon_walk;
            } else if (summary.getSportType() == 7) {
                resourceId = C0532R.drawable.sport_history_icon_trail_running;
            } else if (summary.getSportType() == 8) {
                resourceId = C0532R.drawable.sport_history_icon_treadmill;
            } else if (summary.getSportType() == 9) {
                resourceId = C0532R.drawable.sport_history_icon_outdoor_riding;
            } else if (summary.getSportType() == 10) {
                resourceId = C0532R.drawable.sport_history_icon_indoor_riding;
            } else if (summary.getSportType() == 12) {
                resourceId = C0532R.drawable.sport_history_icon_ellipticals;
            } else if (summary.getSportType() == 2001) {
                resourceId = C0532R.drawable.sport_history_icon_triathlon;
            } else if (summary.getSportType() == 14) {
                resourceId = C0532R.drawable.sport_history_icon_indoor_swimming;
            } else if (summary.getSportType() == 15) {
                resourceId = C0532R.drawable.sport_history_icon_outdoor_swimming;
            } else if (summary.getSportType() == 13) {
                resourceId = C0532R.drawable.sport_history_icon_climb_mountain;
            } else if (summary.getSportType() == 11) {
                resourceId = C0532R.drawable.sport_history_icon_skiing;
            } else if (summary.getSportType() == 17) {
                resourceId = C0532R.drawable.sport_history_icon_tennis;
            } else if (summary.getSportType() == 18) {
                resourceId = C0532R.drawable.sport_history_icon_football;
            } else if (summary.getSportType() == 2002) {
                resourceId = C0532R.drawable.sport_history_icon_complex_sport;
            }
            if (resourceId != -1) {
                holder.mImageView.setImageDrawable(SportHistoryFragment.this.getResources().getDrawable(resourceId));
            }
            if (!SportType.isSportTypeNeedDis(summary.getSportType()) && 17 != summary.getSportType()) {
                holder.mDistanceView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) summary.getCalorie()))}));
                holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.running_calorie_unit));
            } else if (SportType.isSwimMode(summary.getSportType())) {
                holder.mDistanceView.setText(String.valueOf((int) UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) summary.getDistance()), summary.getUnit())));
                if (summary.getUnit() == 0) {
                    holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.metre_metric));
                } else {
                    holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.swim_unit));
                }
            } else if (17 == summary.getSportType()) {
                holder.mDistanceView.setText(String.valueOf(summary.getmStrokes()));
                holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.sport_tennis_strokes));
            } else {
                if (summary.getSportType() == 11) {
                    holder.mDistanceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) summary.getClimbdisDescend()), false));
                } else {
                    holder.mDistanceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) summary.getDistance()), false));
                }
                if (UnitConvertUtils.isImperial()) {
                    holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.km_imperial));
                } else {
                    holder.mDistanceDescView.setText(SportHistoryFragment.this.getString(C0532R.string.km_metric));
                }
            }
            if (DateFormat.is24HourFormat(SportHistoryFragment.this.getActivity())) {
                holder.mDateView.setText(DataFormatUtils.parseMilliSecondToFormattedTime(summary.getStartTime(), "yy-MM-dd HH:mm", true));
            } else {
                String amOrPm;
                SportHistoryFragment.this.mCalendar.setTimeInMillis(summary.getStartTime());
                if (SportHistoryFragment.this.mCalendar.get(9) == 0) {
                    amOrPm = SportHistoryFragment.this.getString(C0532R.string.date_am);
                } else {
                    amOrPm = SportHistoryFragment.this.getString(C0532R.string.date_pm);
                }
                holder.mDateView.setText(DataFormatUtils.parseMilliSecondToFormattedTime(summary.getStartTime(), "yy-MM-dd " + amOrPm + " hh:mm", true));
            }
            holder.mCostTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime((long) summary.getSportDuration()));
            return convertView;
        }
    }

    public SportHistoryFragment() {
        this.mSportSummaries = null;
        this.mSportHistoryAdapter = null;
        this.mSportListView = null;
        this.mHeadView = null;
        this.mFootView = null;
        this.mTotalDistance = null;
        this.mTotalCount = null;
        this.mTotalCalorie = null;
        this.mSportHistoryListener = null;
        this.mDataManager = DataManager.getInstance();
        this.mSportSummaryDatabaseManager = null;
        this.mIsUpdatingSportSummary = false;
        this.mCalendar = null;
        this.mIsImperial = false;
        this.mCalendar = Calendar.getInstance();
    }

    public static SportHistoryFragment newInstance() {
        return new SportHistoryFragment();
    }

    public void onDetach() {
        super.onDetach();
        this.mSportHistoryListener = null;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ISportHistoryListener) {
            this.mSportSummaryDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(activity));
            this.mSportHistoryListener = (ISportHistoryListener) activity;
            return;
        }
        throw new IllegalStateException(activity.getClass().getSimpleName() + " must implements ISportHistoryListener");
    }

    public void onCreate(Bundle savedInstanceState) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportHistoryFragment", "onCreate");
        }
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SportHistoryFragment", "onCreateView");
        }
        View view = inflater.inflate(C0532R.layout.fragment_sport_history, container, false);
        view.setTag("tag_history");
        this.mSportListView = (HmKeyEventListView) view.findViewById(C0532R.id.sport_history_list_view);
        ArcScrollbarHelper.setArcScrollBarDrawable(this.mSportListView);
        this.mHeadView = inflater.inflate(C0532R.layout.list_header_sport_history, this.mSportListView, false);
        if (UnitConvertUtils.isHuangheMode()) {
            this.mHeadView.findViewById(C0532R.id.vo2_max_container).setVisibility(8);
        }
        this.mTotalDistance = (TextView) this.mHeadView.findViewById(C0532R.id.history_total_distance);
        this.mTotalCount = (HmTextView) this.mHeadView.findViewById(C0532R.id.history_total_count);
        this.mTotalCalorie = (HmTextView) this.mHeadView.findViewById(C0532R.id.history_total_calorie);
        this.trainLoadTitleView = (HmTextView) this.mHeadView.findViewById(C0532R.id.train_load_title);
        this.trainLoadProgressView = (ExerciseLoadProgressView) this.mHeadView.findViewById(C0532R.id.exercise_load_progress);
        this.exerciseLoadNumberView = (NumberTextView) this.mHeadView.findViewById(C0532R.id.exercise_load_number);
        this.exerciseLoadSuggessView = (HmTextView) this.mHeadView.findViewById(C0532R.id.exercise_load_suggess);
        HmTextView historyTotalCountDescView = (HmTextView) this.mHeadView.findViewById(C0532R.id.history_total_count_desc);
        historyTotalCountDescView.setTypeface(Typeface.defaultFromStyle(1));
        historyTotalCountDescView.getPaint().setFakeBoldText(true);
        HmTextView historyTotalCalorieDescView = (HmTextView) this.mHeadView.findViewById(C0532R.id.history_total_calorie_desc);
        historyTotalCalorieDescView.setTypeface(Typeface.defaultFromStyle(1));
        historyTotalCalorieDescView.getPaint().setFakeBoldText(true);
        HmTextView historyToatalDistanceDescView = (HmTextView) this.mHeadView.findViewById(C0532R.id.history_total_distance_desc);
        historyToatalDistanceDescView.setTypeface(Typeface.defaultFromStyle(1));
        historyToatalDistanceDescView.getPaint().setFakeBoldText(true);
        this.mSportListView.setHasBottomView(false);
        this.mFootView = inflater.inflate(C0532R.layout.list_footer_sport_history, this.mSportListView, false);
        this.mSportListView.addHeaderView(this.mHeadView, null, false);
        this.mSportListView.addFooterView(this.mFootView, null, false);
        ((HmTextView) this.mFootView.findViewById(C0532R.id.list_bottom_txt)).setText(getResources().getString(C0532R.string.sport_history_bottom_remind));
        initImperial(this.mHeadView);
        this.mSportHistoryAdapter = new SportHistoryAdapter();
        this.mSportListView.setAdapter(this.mSportHistoryAdapter);
        this.mSportListView.setOnItemClickListener(new OnSportItemClickListener());
        setkeyListViewListener();
        initTrainLoadTitleClickEvent();
        initImperial(this.mHeadView);
        updateData(getActivity());
        this.mSportListView.setFocusable(false);
        return view;
    }

    private void initTrainLoadTitleClickEvent() {
        this.trainLoadTitleView.setOnClickListener(new C08501());
    }

    private void initImperial(View view) {
        this.mIsImperial = UnitConvertUtils.isImperial();
        if (this.mIsImperial) {
            ((TextView) view.findViewById(C0532R.id.history_total_distance_unit)).setText(C0532R.string.km_imperial);
        } else {
            ((TextView) view.findViewById(C0532R.id.history_total_distance_unit)).setText(C0532R.string.km_metric);
        }
    }

    private void getTrainLoadData() {
        Global.getGlobalWorkHandler().post(new C08532());
    }

    public void onResume() {
        super.onResume();
        if (this.mIsImperial != UnitConvertUtils.isImperial()) {
            initImperial(this.mHeadView);
            updateData(getActivity());
        }
    }

    public void updateData(Context context) {
        if (!this.mIsUpdatingSportSummary) {
            this.mIsUpdatingSportSummary = true;
            new GetSportHistoryTask(context).execute(new Void[0]);
        }
    }

    private void updateStatisticInfo() {
        if (isAdded() && !isDetached()) {
            if (this.mTotalDistance == null) {
                Debug.m6w("SportHistoryFragment", "total distance view is null, abort update statistic info");
            } else if (this.mTotalCount == null) {
                Debug.m6w("SportHistoryFragment", "total count view is null, abort update statistic info");
            } else if (this.mTotalCalorie == null) {
                Debug.m6w("SportHistoryFragment", "total calorie view is null, abort update statistic info");
            } else {
                if (mSportStatistic == null) {
                    Debug.m6w("SportHistoryFragment", "mSportStatistic is null");
                    mSportStatistic = new SportStatistic();
                }
                this.mTotalDistance.setText(DataFormatUtils.parseTotalDistanceFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) mSportStatistic.getTotalDistance()), false));
                this.mTotalCount.setText("" + getMaxLengthString((double) mSportStatistic.getTotalCount()));
                this.mTotalCalorie.setText(getMaxLengthString((double) Math.round(UnitConvertUtils.convertCalorieToKilocalorie((double) mSportStatistic.getTotalCalorie()))));
            }
        }
    }

    private String getMaxLengthString(double param) {
        StringBuffer stringBuffer = new StringBuffer();
        if (param / 1000000.0d > 1.0d) {
            stringBuffer.append(String.format("%f", new Object[]{Double.valueOf(param)}).substring(0, 6));
            stringBuffer.append("..");
        } else {
            stringBuffer.append(String.format("%.0f", new Object[]{Double.valueOf(param)}));
        }
        return stringBuffer.toString();
    }

    public boolean getKeyEventStatus() {
        if (this.mSportListView != null) {
            return this.mSportListView.hasFocusedItems();
        }
        return false;
    }

    public void setkeyListViewListener() {
        if (this.mSportListView != null) {
            this.mSportListView.setItemFocusedListener(new C08543());
        }
    }
}
