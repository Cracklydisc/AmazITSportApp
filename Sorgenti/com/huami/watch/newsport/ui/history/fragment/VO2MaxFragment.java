package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.huami.watch.common.widget.HmRelativeLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.Vo2MaxLevelFullView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;

public class VO2MaxFragment extends Fragment {
    private HmTextView leftDescView;
    private HmRelativeLayout llVo2MaxTitleView;
    private NumberTextView ridingVo2MaxView;
    private HmTextView rightDescView;
    private NumberTextView sportVo2MaxView;
    private Vo2MaxLevelFullView vo2MaxLevelView;

    class C08652 implements OnClickListener {
        C08652() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(VO2MaxFragment.this.getActivity(), 0);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_vo2_max, container, false);
        this.sportVo2MaxView = (NumberTextView) root.findViewById(C0532R.id.left_number);
        this.ridingVo2MaxView = (NumberTextView) root.findViewById(C0532R.id.right_number);
        this.llVo2MaxTitleView = (HmRelativeLayout) root.findViewById(C0532R.id.ll_vo2_max_title);
        this.vo2MaxLevelView = (Vo2MaxLevelFullView) root.findViewById(C0532R.id.vo2_max_level_view);
        this.rightDescView = (HmTextView) root.findViewById(C0532R.id.right_desc);
        this.leftDescView = (HmTextView) root.findViewById(C0532R.id.left_desc);
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(getActivity());
        final int runVo2MaxValue = FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max());
        final int walkingVo2MaxValue = FirstBeatDataUtils.getRoundingValue(config.getCurrnetWalkingVo2max());
        this.sportVo2MaxView.setText(runVo2MaxValue <= 0 ? "--" : String.valueOf(runVo2MaxValue));
        this.ridingVo2MaxView.setText(walkingVo2MaxValue <= 0 ? "--" : String.valueOf(walkingVo2MaxValue));
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                final int sportLevel = FirstBeatDataUtils.getFitnessLevelByVo2Max(VO2MaxFragment.this.getActivity(), runVo2MaxValue);
                final int rideLevle = FirstBeatDataUtils.getFitnessLevelByVo2Max(VO2MaxFragment.this.getActivity(), walkingVo2MaxValue);
                final String[] levelStrStatus = VO2MaxFragment.this.getActivity().getResources().getStringArray(C0532R.array.vo2_max_level_status);
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        boolean z;
                        boolean z2 = false;
                        int runViewLevel = sportLevel;
                        int walkViewlevel = rideLevle;
                        if (sportLevel < 1 || sportLevel > 7 || runVo2MaxValue <= 0) {
                            VO2MaxFragment.this.leftDescView.setText("--");
                            runViewLevel = 0;
                        } else {
                            VO2MaxFragment.this.leftDescView.setText(levelStrStatus[sportLevel - 1]);
                        }
                        if (rideLevle < 1 || rideLevle > 7 || walkingVo2MaxValue <= 0) {
                            walkViewlevel = 0;
                            VO2MaxFragment.this.rightDescView.setText("--");
                        } else {
                            VO2MaxFragment.this.rightDescView.setText(levelStrStatus[rideLevle - 1]);
                        }
                        Vo2MaxLevelFullView access$200 = VO2MaxFragment.this.vo2MaxLevelView;
                        if (runViewLevel == 0) {
                            z = false;
                        } else {
                            z = true;
                        }
                        if (walkViewlevel != 0) {
                            z2 = true;
                        }
                        access$200.setLeftAndRightLevel(runViewLevel, z, walkViewlevel, z2);
                    }
                });
            }
        });
        this.llVo2MaxTitleView.setOnClickListener(new C08652());
        return root;
    }
}
