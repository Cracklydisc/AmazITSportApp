package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.huami.watch.common.widget.HmRelativeLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.Vo2maxStatusView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class RunVo2maxFragment extends Fragment {
    private static final String TAG = RunVo2maxFragment.class.getSimpleName();
    HmRelativeLayout llVo2MaxTitleView;
    Vo2maxStatusView vo2maxChart;
    HmTextView vo2maxLevelStatus;
    private HmTextView vo2maxSimpleView;
    NumberTextView vo2maxValueView;

    class C08392 implements OnClickListener {
        C08392() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(RunVo2maxFragment.this.getActivity(), 0);
        }
    }

    public static RunVo2maxFragment newInstance() {
        return new RunVo2maxFragment();
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_run_vo2max, container, false);
        this.vo2maxValueView = (NumberTextView) root.findViewById(C0532R.id.vo2max_value);
        this.vo2maxLevelStatus = (HmTextView) root.findViewById(C0532R.id.vo2_max_level_status);
        this.vo2maxChart = (Vo2maxStatusView) root.findViewById(C0532R.id.vo2_max_chart);
        this.llVo2MaxTitleView = (HmRelativeLayout) root.findViewById(C0532R.id.ll_vo2_max_title);
        this.vo2maxSimpleView = (HmTextView) root.findViewById(C0532R.id.vo2_max_simple);
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(getActivity());
        final int runVo2MaxValue = config.getCurrnetRunVo2max() < 0.0f ? -1 : FirstBeatDataUtils.getRoundingValue(config.getCurrnetRunVo2max());
        Log.i(TAG, " runVo2maxValue:" + runVo2MaxValue);
        StringBuffer sb = new StringBuffer();
        if (((float) runVo2MaxValue) < 21.0f || ((float) runVo2MaxValue) > 87.99f) {
            sb.append("--");
        } else {
            sb.append(runVo2MaxValue);
        }
        this.vo2maxValueView.setText(sb.toString());
        Log.i(TAG, " isImperial:" + UnitConvertUtils.isOverSeaVersion());
        if (UnitConvertUtils.isOverSeaVersion()) {
            this.vo2maxSimpleView.setVisibility(8);
        }
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                if (RunVo2maxFragment.this.getActivity() == null) {
                    Log.i(RunVo2maxFragment.TAG, " getActivity is null ");
                    return;
                }
                final int sportLevel = FirstBeatDataUtils.getFitnessLevelByVo2Max(RunVo2maxFragment.this.getActivity(), runVo2MaxValue);
                final String[] levelStrStatus = RunVo2maxFragment.this.getActivity().getResources().getStringArray(C0532R.array.vo2_max_level_status);
                final int[] vo2maxLevelValue = FirstBeatDataUtils.getFitnessLevelArrayValueByVo2maxAge(RunVo2maxFragment.this.getActivity(), runVo2MaxValue);
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        int runViewLevel = sportLevel;
                        if (sportLevel < 1 || sportLevel > 7 || ((float) runVo2MaxValue) < 21.0f || ((float) runVo2MaxValue) > 87.99f) {
                            RunVo2maxFragment.this.vo2maxLevelStatus.setText(RunVo2maxFragment.this.getString(C0532R.string.vo2_max_has_no_data));
                            RunVo2maxFragment.this.vo2maxChart.setCurrentVo2maxValue(0, vo2maxLevelValue, 1);
                            return;
                        }
                        RunVo2maxFragment.this.vo2maxLevelStatus.setText(levelStrStatus[sportLevel - 1]);
                        RunVo2maxFragment.this.vo2maxChart.setCurrentVo2maxValue(runVo2MaxValue, vo2maxLevelValue, sportLevel);
                    }
                });
            }
        });
        this.llVo2MaxTitleView.setOnClickListener(new C08392());
        return root;
    }
}
