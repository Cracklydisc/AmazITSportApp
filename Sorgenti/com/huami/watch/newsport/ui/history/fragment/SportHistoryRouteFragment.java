package com.huami.watch.newsport.ui.history.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.common.widget.HmLinearLayout;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.gpxexport.ExportGPXActivity;
import com.huami.watch.newsport.recordcache.controller.RecordGraphManager;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.route.utils.TrailDrawer;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryRequest;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.SportDataFilterUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SportHistoryRouteFragment extends BaseDetailFragment implements IRecordGraphCache, ISportHistoryDetailData {
    private static final String TAG = SportHistoryRouteFragment.class.getSimpleName();
    private View mAutoLapContainer = null;
    private HmHaloButton mExportBtn = null;
    private boolean mHasTrail = false;
    private boolean mIsDestroyed = false;
    private boolean mIsLoactionPrepared = false;
    private HmLinearLayout mListView = null;
    private SyncDatabaseManager<SportLocationData> mLocationDataDaoSyncDatabaseManager = null;
    private Lock mLock = new ReentrantLock(true);
    private ISportHistoryRequest mRequestListener = null;
    private View mRouteContainer = null;
    private ImageView mRouteImgBg = null;
    private SyncDatabaseManager<RunningInfoPerLap> mSyncDBManager = null;
    private NumberTextView mTotalDisView = null;
    private Bitmap mTrailBitmap = null;
    private TrailDrawer mTrailDrawer = null;
    private HmTextView totalDistanceUnit;

    class C08551 implements OnClickListener {
        C08551() {
        }

        public void onClick(View v) {
            SportHistoryRouteFragment.this.startSaveGPX(String.valueOf(SportHistoryRouteFragment.this.mSportSummary.getTrackId()), "");
        }
    }

    class C08593 implements Runnable {
        C08593() {
        }

        public void run() {
            if (!SportHistoryRouteFragment.this.isDetached() && SportHistoryRouteFragment.this.isAdded()) {
                int autoType = 1;
                if (SportHistoryRouteFragment.this.mSportSummary.getSportType() == 11) {
                    autoType = 3;
                }
                final List<? extends RunningInfoPerLap> laps = SportHistoryRouteFragment.this.mSyncDBManager.selectAll(SportHistoryRouteFragment.this.getActivity(), "track_id=? AND lap_type=?", new String[]{"" + SportHistoryRouteFragment.this.mSportSummary.getTrackId(), "" + autoType}, null, null);
                Global.getGlobalUIHandler().post(new Runnable() {
                    public void run() {
                        if (laps == null || laps.size() == 0) {
                            SportHistoryRouteFragment.this.mAutoLapContainer.setVisibility(8);
                        } else if (!SportHistoryRouteFragment.this.isDetached() && SportHistoryRouteFragment.this.isAdded()) {
                            for (int i = 0; i < laps.size(); i++) {
                                SportHistoryRouteFragment.this.addChildView(i, (RunningInfoPerLap) laps.get(i));
                            }
                        }
                    }
                });
            }
        }
    }

    public static SportHistoryRouteFragment newInstance(SportSummary sportSummary, boolean hasTrail) {
        SportHistoryRouteFragment fragment = new SportHistoryRouteFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", sportSummary);
        args.putBoolean("key_has_trail", hasTrail);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHasTrail = getArguments().getBoolean("key_has_trail", false);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ISportHistoryRequest) {
            this.mRequestListener = (ISportHistoryRequest) activity;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Debug.m5i(TAG, "onCreateView");
        this.mIsDestroyed = false;
        View viewRoot = inflater.inflate(C0532R.layout.fragment_history_detail_route_section_container, container, false);
        this.mRouteContainer = viewRoot.findViewById(C0532R.id.detail_route_container);
        this.mAutoLapContainer = viewRoot.findViewById(C0532R.id.auto_lap_container);
        this.mAutoLapContainer.setVisibility(8);
        this.mExportBtn = (HmHaloButton) this.mRouteContainer.findViewById(C0532R.id.export_btn);
        this.mExportBtn.setWithHalo(false);
        this.mExportBtn.setOnClickListener(new C08551());
        this.mExportBtn.setVisibility(8);
        ArcScrollbarHelper.setArcScrollBarDrawable(viewRoot.findViewById(C0532R.id.scroll_container));
        initRouteView(viewRoot);
        if (this.mHasTrail && SportType.isSportTypeNeedGps(this.mSportSummary.getSportType())) {
            buildTrailDrawer();
        } else {
            this.mRouteContainer.setVisibility(8);
        }
        initAutoLapView(viewRoot);
        return viewRoot;
    }

    private void startSaveGPX(String trackId, String name) {
        Log.i("gpx", "oncreate : " + trackId + " , name: " + name);
        Intent intent = new Intent(getActivity(), ExportGPXActivity.class);
        intent.putExtra("trackid", trackId);
        intent.putExtra("name", name);
        intent.addFlags(268435456);
        startActivity(intent);
    }

    public void onDestroy() {
        Debug.m5i(TAG, "onDestroy");
        super.onDestroy();
        this.mIsDestroyed = true;
        if (this.mTrailDrawer != null) {
            this.mTrailDrawer.finishDraw();
        }
        if (this.mTrailBitmap != null && !this.mTrailBitmap.isRecycled()) {
            this.mTrailBitmap.recycle();
        }
    }

    private void initRouteView(View viewRoot) {
        this.mRouteImgBg = (ImageView) viewRoot.findViewById(C0532R.id.route_bg);
        this.mTotalDisView = (NumberTextView) viewRoot.findViewById(C0532R.id.route_dis);
        this.totalDistanceUnit = (HmTextView) viewRoot.findViewById(C0532R.id.total_distance_unit);
        if (this.mSportSummary != null && 15 != this.mSportSummary.getSportType()) {
            this.totalDistanceUnit.setText(getResources().getString(UnitConvertUtils.isImperial() ? C0532R.string.km_imperial : C0532R.string.km_metric));
            if (this.mSportSummary.getSportType() == 11) {
                this.mTotalDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) ((OutdoorSportSummary) this.mSportSummary).getClimbdisDescend()), false));
            } else {
                this.mTotalDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm((double) this.mSportSummary.getDistance()), false));
            }
        } else if (this.mSportSummary != null && 15 == this.mSportSummary.getSportType()) {
            this.mTotalDisView.setText(String.valueOf(Math.round(UnitConvertUtils.convertDistance2YDOrMeter((double) ((int) this.mSportSummary.getDistance()), ((OutdoorSportSummary) this.mSportSummary).getUnit()))));
            if (((OutdoorSportSummary) this.mSportSummary).getUnit() == 0) {
                this.totalDistanceUnit.setText(getResources().getString(C0532R.string.metre_metric));
            } else {
                this.totalDistanceUnit.setText(getResources().getString(C0532R.string.swim_unit));
            }
        }
        int drawWidth = getResources().getDimensionPixelSize(C0532R.dimen.sport_history_detail_trail_width);
        int drawHeight = getResources().getDimensionPixelSize(C0532R.dimen.sport_history_detail_trail_height);
        this.mLock.lock();
        try {
            this.mTrailDrawer = new TrailDrawer(getActivity(), drawWidth, drawHeight, true);
            this.mTrailDrawer.startDraw();
            this.mTrailDrawer.setStart(C0532R.drawable.his_route_sport_map_icon_start);
            this.mTrailDrawer.setEnd(C0532R.drawable.his_route_sport_map_icon_end);
        } finally {
            this.mLock.unlock();
        }
    }

    private void initAutoLapView(final View viewRoot) {
        this.mSyncDBManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(getActivity()));
        Global.getGlobalWorkHandler().post(new Runnable() {

            class C08561 implements Runnable {
                C08561() {
                }

                public void run() {
                    if (!SportHistoryRouteFragment.this.isDetached() && SportHistoryRouteFragment.this.isAdded()) {
                        SportHistoryRouteFragment.this.mAutoLapContainer.setVisibility(0);
                        SportHistoryRouteFragment.this.mListView = (HmLinearLayout) viewRoot.findViewById(C0532R.id.lap_list_view);
                        View headView = SportHistoryRouteFragment.this.getActivity().getLayoutInflater().inflate(C0532R.layout.item_detail_head_lap_info, SportHistoryRouteFragment.this.mListView, false);
                        LayoutParams lp = new LayoutParams(-1, -2);
                        lp.width = 320;
                        headView.setLayoutParams(lp);
                        int sportType = SportHistoryRouteFragment.this.mSportSummary.getSportType();
                        if (sportType == 10) {
                            ((HmTextView) headView.findViewById(C0532R.id.lap_dis)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.outdoor_cadence));
                            ((HmTextView) headView.findViewById(C0532R.id.lap_pace)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.running_calorie_desc));
                        } else if (sportType == 12) {
                            ((HmTextView) headView.findViewById(C0532R.id.lap_pace)).setVisibility(8);
                            ((HmTextView) headView.findViewById(C0532R.id.lap_dis)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.running_calorie_desc));
                        } else if (sportType == 9) {
                            ((HmTextView) headView.findViewById(C0532R.id.lap_pace)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.running_avg_speed_desc_simple));
                        } else if (sportType == 11) {
                            ((HmTextView) headView.findViewById(C0532R.id.lap_dis)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.skiing_lap_downhill_dis_title));
                            ((HmTextView) headView.findViewById(C0532R.id.lap_pace)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.skiing_downhill_max_speed));
                        }
                        SportHistoryRouteFragment.this.mListView.addView(headView);
                        if (SportHistoryRouteFragment.this.mSportSummary.getSportType() == 11) {
                            ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.skiing_lap_title));
                        } else if (((OutdoorSportSummary) SportHistoryRouteFragment.this.mSportSummary).getIntervalType() == 1) {
                            ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.detail_route_detail_train_lap_title));
                        } else {
                            ((TextView) viewRoot.findViewById(C0532R.id.lap_title)).setText(SportHistoryRouteFragment.this.getString(C0532R.string.detail_route_detail_auto_lap_title));
                        }
                        SportHistoryRouteFragment.this.loadLapInfos();
                    }
                }
            }

            public void run() {
                if (!SportHistoryRouteFragment.this.isDetached() && SportHistoryRouteFragment.this.isAdded()) {
                    Global.getGlobalUIHandler().post(new C08561());
                }
            }
        });
    }

    private void loadLapInfos() {
        Global.getGlobalWorkHandler().post(new C08593());
    }

    private void addChildView(int position, RunningInfoPerLap lap) {
        View child = getActivity().getLayoutInflater().inflate(C0532R.layout.item_detail_lap_info, null);
        LayoutParams lp = new LayoutParams(-1, -2);
        lp.width = 320;
        child.setLayoutParams(lp);
        NumberTextView lapNumView = (NumberTextView) child.findViewById(C0532R.id.lap_num);
        NumberTextView lapTimeView = (NumberTextView) child.findViewById(C0532R.id.lap_time);
        NumberTextView lapDisView = (NumberTextView) child.findViewById(C0532R.id.lap_dis);
        NumberTextView lapPaceView = (NumberTextView) child.findViewById(C0532R.id.lap_pace);
        if (position % 2 == 0) {
            child.setBackgroundColor(getActivity().getResources().getColor(C0532R.color.default_background_black));
        } else {
            child.setBackgroundColor(getActivity().getResources().getColor(C0532R.color.sport_history_item_title_bg));
        }
        lapNumView.setText("" + (lap.getLapNum() + 1));
        lapTimeView.setText(DataFormatUtils.parseMillSecondToDefaultFormattedTime(lap.getCostTime()));
        if (this.mSportSummary.getSportType() == 10) {
            if (((OutdoorSportSummary) this.mSportSummary).getDeviceType() != -1) {
                lapDisView.setText("" + lap.getLapCadence());
            } else {
                lapDisView.setText(getString(C0532R.string.sport_pause_heart_shown));
            }
        } else if (this.mSportSummary.getSportType() == 12) {
            lapDisView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) lap.getLapCalories()))}));
        } else if (!SportType.isSwimMode(this.mSportSummary.getSportType())) {
            lapDisView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToMileOrKm(NumeriConversionUtils.getDoubleValue(String.valueOf(lap.getDistance() / 1000.0f))), false));
        } else if (((OutdoorSportSummary) this.mSportSummary).getUnit() == 0) {
            lapDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) lap.getDistance(), ((OutdoorSportSummary) this.mSportSummary).getUnit())));
        } else {
            lapDisView.setText("" + ((int) UnitConvertUtils.convertDistance2YDOrMeter((double) lap.getDistance(), ((OutdoorSportSummary) this.mSportSummary).getUnit())));
        }
        if (this.mSportSummary.getSportType() == 12) {
            lapPaceView.setVisibility(8);
        } else if (SportType.isSwimMode(this.mSportSummary.getSportType())) {
            lapPaceView.setText(DataFormatUtils.parseSecondPerMeterToFormatted100meterPace(SportDataFilterUtils.parsePacePer100Meter((float) lap.getPace(), ((OutdoorSportSummary) this.mSportSummary).getUnit())));
        } else if (this.mSportSummary.getSportType() == 10) {
            lapPaceView.setText(String.format("%.0f", new Object[]{Double.valueOf(UnitConvertUtils.convertCalorieToKilocalorie((double) lap.getLapCalories()))}));
        } else if (this.mSportSummary.getSportType() == 9) {
            lapPaceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) SportDataFilterUtils.parseSpeed((float) (1.0d / lap.getPace()))), true));
        } else if (this.mSportSummary.getSportType() == 11) {
            lapPaceView.setText(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertSpeedToKmPerHour((double) (Double.compare(0.0d, lap.getMaxPace()) == 0 ? 0.0f : (float) (1.0d / lap.getMaxPace()))), true));
        } else {
            lapPaceView.setText(DataFormatUtils.parseSecondPerMeterToFormattedPace(SportDataFilterUtils.parsePace((float) lap.getPace())));
        }
        this.mListView.addView(child);
    }

    private void buildTrailDrawer() {
        if (RecordGraphManager.getInstance(getActivity()).isCacheGraphData(this.mSportSummary.getTrackId(), 0)) {
            RecordGraphManager.getInstance(getActivity()).postToSearchGraph(this.mSportSummary.getTrackId(), 0);
        } else if (this.mRequestListener != null) {
            this.mRequestListener.doSportHistoryDataRequest(1);
        }
    }

    private void reCallLoadGPS(final List<TrailLocations> trailLocationsList) {
        Global.getGlobalWorkHandler().postDelayed(new Runnable() {
            public void run() {
                SportHistoryRouteFragment.this.loadGPSRoute(trailLocationsList);
            }
        }, 500);
    }

    private void loadGPSRoute(List<TrailLocations> trailLocationsList) {
        if (this.mLock.tryLock()) {
            try {
                if (this.mTrailDrawer == null) {
                    Debug.m5i(TAG, "onLocationDataReady, data is null");
                    reCallLoadGPS(trailLocationsList);
                    return;
                }
                this.mLock.unlock();
                if (!this.mIsDestroyed) {
                    this.mTrailDrawer.addLocationDatasToBitmapOfZhufeng(trailLocationsList);
                    LogUtils.print(TAG, "onLocationDataReady: rate:" + this.mTrailDrawer.getRate());
                    final Bitmap bitmap = this.mTrailDrawer.newTrailBitmap();
                    Global.getGlobalUIHandler().post(new Runnable() {
                        public void run() {
                            if (!SportHistoryRouteFragment.this.isDetached() && SportHistoryRouteFragment.this.isAdded()) {
                                SportHistoryRouteFragment.this.mRouteImgBg.setVisibility(0);
                                SportHistoryRouteFragment.this.mRouteImgBg.setImageDrawable(new BitmapDrawable(SportHistoryRouteFragment.this.getResources(), bitmap));
                            }
                        }
                    });
                    if (this.mSportSummary == null || this.mSportSummary.getCurrentStatus() == 4) {
                        Debug.m5i(TAG, "not saved the bitmap, sport status:" + (this.mSportSummary != null ? Integer.valueOf(this.mSportSummary.getCurrentStatus()) : "summary is null"));
                    } else {
                        RecordGraphManager.getInstance(getActivity()).saveGraphData2Cache(bitmap, this.mSportSummary.getTrackId(), 0, false);
                    }
                }
            } finally {
                this.mLock.unlock();
            }
        } else {
            Debug.m5i(TAG, "onLocationDataReady, data is null");
            reCallLoadGPS(trailLocationsList);
        }
    }

    public void onDrawableFound(long trackId, Bitmap bitmap, int graphType) {
        if (graphType == 0) {
            this.mTrailBitmap = bitmap;
            this.mRouteImgBg.setVisibility(0);
            this.mRouteImgBg.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
        }
    }

    public void onSaveCacheResult(int graphType, boolean isSuccess) {
        Debug.m5i(TAG, "onSaveCacheResult, graph type:" + graphType + ", " + isSuccess);
    }

    public void onDeleteCacheResult(int graphType, boolean isSuccess) {
        Debug.m5i(TAG, "onDeleteCacheResult, graph type:" + graphType + ", " + isSuccess);
    }

    public synchronized void onLocationDataReady(List<TrailLocations> trailLocationsList) {
        if (!this.mIsLoactionPrepared) {
            this.mIsLoactionPrepared = true;
            loadGPSRoute(trailLocationsList);
        }
    }

    public void onHeartDataReady(List<? extends HeartRate> list) {
    }

    public void onDailyPropermenceDataReady(int[] data) {
    }
}
