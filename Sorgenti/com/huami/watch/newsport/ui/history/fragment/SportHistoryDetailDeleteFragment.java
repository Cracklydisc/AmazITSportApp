package com.huami.watch.newsport.ui.history.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.view.HmSportLayout;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnDismissedListener;
import com.huami.watch.newsport.ui.view.HmSportLayout.OnSwipeProgressChangedListener;

public class SportHistoryDetailDeleteFragment extends Fragment {
    private TextView mDialogMsg;
    private View mDialogNegativeButton;
    private View mDialogPositiveButton;
    private IDeleteDataListener mListener;
    private HmSportLayout mSportDialog = null;

    public interface IDeleteDataListener {
        void onDeleteConfirm();

        void onDeleteDismiss();
    }

    class C08461 implements OnClickListener {
        C08461() {
        }

        public void onClick(View v) {
            if (SportHistoryDetailDeleteFragment.this.mListener != null) {
                SportHistoryDetailDeleteFragment.this.mListener.onDeleteConfirm();
            }
        }
    }

    class C08472 implements OnClickListener {
        C08472() {
        }

        public void onClick(View v) {
            if (SportHistoryDetailDeleteFragment.this.mListener != null) {
                SportHistoryDetailDeleteFragment.this.mListener.onDeleteDismiss();
            }
        }
    }

    class C08483 implements OnDismissedListener {
        C08483() {
        }

        public void onDismissed(HmSportLayout view) {
            if (SportHistoryDetailDeleteFragment.this.mListener != null) {
                SportHistoryDetailDeleteFragment.this.mListener.onDeleteDismiss();
            }
        }
    }

    class C08494 implements OnSwipeProgressChangedListener {
        C08494() {
        }

        public void onSwipeProgressChanged(HmSportLayout view, float progress, float deltx) {
            if (deltx >= 0.0f && SportHistoryDetailDeleteFragment.this.mSportDialog.getVisibility() == 0) {
                SportHistoryDetailDeleteFragment.this.mSportDialog.setAlpha(1.0f - progress);
            }
        }

        public void onSwipeCancelled(HmSportLayout view) {
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IDeleteDataListener) {
            this.mListener = (IDeleteDataListener) activity;
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_delete_detail, container, false);
        this.mSportDialog = (HmSportLayout) root.findViewById(C0532R.id.dialog_delete_sport_summary);
        this.mDialogMsg = (TextView) root.findViewById(C0532R.id.message);
        this.mDialogMsg.setText(getString(C0532R.string.sport_history_delete_dialog_title));
        this.mDialogPositiveButton = root.findViewById(C0532R.id.button1);
        this.mDialogPositiveButton.setOnClickListener(new C08461());
        this.mDialogNegativeButton = root.findViewById(C0532R.id.button2);
        this.mDialogNegativeButton.setOnClickListener(new C08472());
        this.mSportDialog.setOnDismissedListener(new C08483());
        this.mSportDialog.setAppOnSwipeProgressChangedListener(new C08494());
        return root;
    }

    public void onConfirmUI() {
        this.mDialogPositiveButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
        this.mDialogNegativeButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
    }

    public void onDismissUi() {
        this.mDialogNegativeButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_positive));
        this.mDialogPositiveButton.setBackground(getResources().getDrawable(C0532R.drawable.system_dialog_button_bg_negative));
    }
}
