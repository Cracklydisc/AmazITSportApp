package com.huami.watch.newsport.ui.history.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.ui.delegate.ChildRidingHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.ChildRunningHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.ChildSwimHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.CompoundHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.CrossingHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.EllipticalHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.IndoorRidingHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.IndoorRunningHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.MountaineerHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.OpenWaterSwimHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.OutdoorRidingHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.RunningHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.SkiingHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.SoccerHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.SwimHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.TennisHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.TriathlonHistoryDelegate;
import com.huami.watch.newsport.ui.delegate.WalkingHistoryDetailDelegate;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryRequest;
import com.huami.watch.newsport.ui.uiinterface.IHistoryDetailDelegate;
import java.util.ArrayList;
import java.util.List;

public class SportHistoryDetailFragment extends Fragment implements IRecordGraphCache, ISportHistoryDetailData, ISportHistoryRequest {
    private static final String TAG = SportHistoryDetailFragment.class.getSimpleName();
    private List<SportSummary> mChildLists = null;
    private IHistoryDetailDelegate mDetailDelegate = null;
    private boolean mIsChildSport = false;
    private boolean mIsEndFromSport = false;
    private boolean mIsShownAnimation = false;
    private ISportHistoryRequest mRequestListener = null;
    private SportSummary mSportSummary = null;

    public static SportHistoryDetailFragment newInstance(SportSummary sportSummary, boolean isShownAnimation, boolean isChildSport, boolean isFromEndSport, List<SportSummary> childSportSummary) {
        if (sportSummary == null) {
            throw new IllegalArgumentException("newInstance sport summary should not be null");
        }
        SportHistoryDetailFragment fragment = new SportHistoryDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", sportSummary);
        args.putBoolean("sport_show_end_animation", isShownAnimation);
        args.putBoolean("is_child_sport", isChildSport);
        args.putBoolean("is_from_end_sport", isFromEndSport);
        if (childSportSummary != null) {
            args.putParcelableArrayList("child_list", (ArrayList) childSportSummary);
        }
        fragment.setArguments(args);
        return fragment;
    }

    public static SportHistoryDetailFragment newInstance(SportSummary sportSummary, boolean isShownAnimation, boolean isChildSport, boolean isFromEndSport) {
        return newInstance(sportSummary, isShownAnimation, isChildSport, isFromEndSport, null);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ISportHistoryRequest) {
            this.mRequestListener = (ISportHistoryRequest) activity;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mSportSummary = (SportSummary) getArguments().getParcelable("sport_summary");
            this.mIsShownAnimation = getArguments().getBoolean("sport_show_end_animation");
            this.mIsChildSport = getArguments().getBoolean("is_child_sport", false);
            this.mIsEndFromSport = getArguments().getBoolean("is_from_end_sport", false);
            this.mChildLists = getArguments().getParcelableArrayList("child_list");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.mSportSummary.getSportType() == 1) {
            this.mDetailDelegate = new RunningHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 6) {
            this.mDetailDelegate = new WalkingHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 7) {
            this.mDetailDelegate = new CrossingHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 8) {
            this.mDetailDelegate = new IndoorRunningHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 9) {
            this.mDetailDelegate = new OutdoorRidingHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 10) {
            this.mDetailDelegate = new IndoorRidingHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 12) {
            this.mDetailDelegate = new EllipticalHistoryDetailDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 14) {
            this.mDetailDelegate = new SwimHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 2001) {
            this.mDetailDelegate = new TriathlonHistoryDelegate(this.mIsEndFromSport, this.mChildLists);
        } else if (this.mSportSummary.getSportType() == 15) {
            this.mDetailDelegate = new OpenWaterSwimHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 1001) {
            this.mDetailDelegate = new ChildRunningHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 1009) {
            this.mDetailDelegate = new ChildRidingHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 1015) {
            this.mDetailDelegate = new ChildSwimHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 13) {
            this.mDetailDelegate = new MountaineerHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 11) {
            this.mDetailDelegate = new SkiingHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 17) {
            this.mDetailDelegate = new TennisHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 18) {
            this.mDetailDelegate = new SoccerHistoryDelegate(this.mIsEndFromSport);
        } else if (this.mSportSummary.getSportType() == 2002) {
            this.mDetailDelegate = new CompoundHistoryDelegate(this.mIsEndFromSport, this.mChildLists);
        }
        if (this.mDetailDelegate == null) {
            throw new IllegalStateException("unknow sport type : " + this.mSportSummary.getSportType());
        }
        this.mDetailDelegate.setActivity(getActivity());
        return this.mDetailDelegate.inflateView(this.mSportSummary, inflater, container, savedInstanceState);
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.destroyView();
        }
    }

    public void onDrawableFound(long trackId, Bitmap bitmap, int graphType) {
        if (this.mSportSummary == null) {
            Debug.m5i(TAG, "onDrawableFound, summary is null");
        } else if (this.mSportSummary.getTrackId() != trackId) {
            Debug.m5i(TAG, "onDrawableFound, not the corresponding record, cur:" + this.mSportSummary.getTrackId() + ", need:" + trackId);
        } else if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onDrawableFound(trackId, bitmap, graphType);
        }
    }

    public void onSaveCacheResult(int graphType, boolean isSuccess) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onSaveCacheResult(graphType, isSuccess);
        }
    }

    public void onDeleteCacheResult(int graphType, boolean isSuccess) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onDeleteCacheResult(graphType, isSuccess);
        }
    }

    public void onLocationDataReady(List<TrailLocations> trailLocationsList) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onLocationDataReady(trailLocationsList);
        }
    }

    public void onHeartDataReady(List<? extends HeartRate> heartRates) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onHeartDataReady(heartRates);
        }
    }

    public void onDailyPropermenceDataReady(int[] data) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.onDailyPerpormentceDataReady(data);
        }
    }

    public void doSportHistoryDataRequest(int type) {
        if (this.mRequestListener != null) {
            this.mRequestListener.doSportHistoryDataRequest(type);
        }
    }

    public void updateSummary(SportSummary summary) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.updateSummary(summary);
        }
    }

    public void updateChildList(List<SportSummary> childSummaryList) {
        if (this.mDetailDelegate != null) {
            this.mDetailDelegate.updateChildList(childSummaryList);
        }
    }
}
