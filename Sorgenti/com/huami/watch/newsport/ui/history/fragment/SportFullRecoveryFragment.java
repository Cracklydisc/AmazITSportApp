package com.huami.watch.newsport.ui.history.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.RecoveryTimeCircleView;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;

public class SportFullRecoveryFragment extends Fragment {
    private static final String TAG = SportFullRecoveryFragment.class.getSimpleName();
    private RecoveryTimeCircleView numberTextView;
    private NumberTextView numberTextViewHour;
    private OutdoorSportSummary outdoorSportSummary;
    private HmTextView sportRecoveyTimeHourView;

    class C08451 implements OnClickListener {
        C08451() {
        }

        public void onClick(View v) {
            FirstBeatDataUtils.jumpToFirstBeatHelpPage(SportFullRecoveryFragment.this.getActivity(), 5);
        }
    }

    public static SportFullRecoveryFragment newInstance(SportSummary summary) {
        SportFullRecoveryFragment fragment = new SportFullRecoveryFragment();
        Bundle args = new Bundle();
        args.putParcelable("sport_summary", summary);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.outdoorSportSummary = (OutdoorSportSummary) getArguments().getParcelable("sport_summary");
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(C0532R.layout.fragment_history_recovery_time_layout, container, false);
        this.numberTextView = (RecoveryTimeCircleView) view.findViewById(C0532R.id.recovery_time);
        this.sportRecoveyTimeHourView = (HmTextView) view.findViewById(C0532R.id.sport_title);
        this.numberTextViewHour = (NumberTextView) view.findViewById(C0532R.id.hour_number);
        FirstBeatConfig config = DataManager.getInstance().getFirstBeatConfig(getActivity());
        LogUtils.print(TAG, " firstBeatConfig:" + config.toString());
        float recoveryTime = (float) (((((long) config.getRecoveryTime()) - (((System.currentTimeMillis() - config.getSportFinishTime()) / 1000) / 60)) + 30) / 60);
        if (recoveryTime > 97.0f || recoveryTime < 0.0f) {
            recoveryTime = 0.0f;
        }
        Log.i(TAG, " recovery time:" + recoveryTime);
        this.numberTextView.setReceryHourTime(recoveryTime, 97.0f);
        this.numberTextViewHour.setText(String.valueOf((int) recoveryTime));
        this.sportRecoveyTimeHourView.setOnClickListener(new C08451());
        return view;
    }
}
