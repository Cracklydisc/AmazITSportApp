package com.huami.watch.newsport.ui.uiinterface;

public interface IDeleteSportSummaryListener {
    void onModifyDistance(double d);

    void onSportSummaryDelete();
}
