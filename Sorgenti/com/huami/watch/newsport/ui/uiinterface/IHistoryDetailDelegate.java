package com.huami.watch.newsport.ui.uiinterface;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import com.huami.watch.newsport.ui.delegate.listener.ISportHistoryDetailData.TrailLocations;
import java.util.List;

public interface IHistoryDetailDelegate extends IRecordGraphCache {
    void destroyView();

    View inflateView(SportSummary sportSummary, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    void onDailyPerpormentceDataReady(int[] iArr);

    void onHeartDataReady(List<? extends HeartRate> list);

    void onLocationDataReady(List<TrailLocations> list);

    void setActivity(Activity activity);

    void updateChildList(List<SportSummary> list);

    void updateSummary(SportSummary sportSummary);
}
