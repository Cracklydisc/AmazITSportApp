package com.huami.watch.newsport.ui.uiinterface;

public interface ILoadingAnimListener {
    void onLoadingAnimEnd();
}
