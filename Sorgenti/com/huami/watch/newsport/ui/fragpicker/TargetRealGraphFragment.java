package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetRealGraphFragment extends AbsSportPickerFragment {
    private static final int[] CROSSING_CONFIG_GRAPH = new int[]{0, 2};
    private static final int[] INDOOR_RUNNING_CONFIG_GRAPH = new int[]{0, 1};
    private static final int[] OPEN_WATER_CONFIG_GRAPH = new int[]{0, 5, 3};
    private static final int[] OUTDOOR_RIDING_CONFIG_GRAPH = new int[]{0, 2, 4};
    private static final int[] RUNNING_CONFIG_GRAPH = new int[]{0, 1};
    private static final int[] SWIM_CONFIG_GRAPH = new int[]{0, 5, 3};
    private SourceAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private int[] mGraphArray = null;
    private int mSource = 0;

    private class SourceAdapter extends AbsPickerViewAdapter {
        public SourceAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int column) {
            return TargetRealGraphFragment.this.mSource;
        }

        public int rowCount(int column) {
            return TargetRealGraphFragment.this.mGraphArray.length;
        }

        public String getDisplayString(int row, int column) {
            if (row < 0 || row > 2) {
                throw new IllegalArgumentException("row is err: " + row);
            }
            int resId;
            switch (TargetRealGraphFragment.this.mGraphArray[row]) {
                case 0:
                    resId = C0532R.string.sport_main_heart_info_title;
                    break;
                case 1:
                    resId = C0532R.string.sport_main_pace_info_title;
                    break;
                case 2:
                    resId = C0532R.string.crossing_history_detail_absolute_altitude;
                    break;
                case 3:
                    resId = C0532R.string.swim_stroke_speed;
                    break;
                case 4:
                    resId = C0532R.string.running_speed_desc;
                    break;
                case 5:
                    resId = C0532R.string.sport_main_pace_info_title;
                    break;
                default:
                    resId = C0532R.string.sport_main_heart_info_title;
                    break;
            }
            return TargetRealGraphFragment.this.getActivity().getString(resId);
        }

        public float getColumnWidth(int column) {
            return 200.0f;
        }

        public float getTextSize() {
            return 32.0f;
        }

        public boolean hideNegativeButton() {
            return true;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int graphType = this.mConfig.getRealGraphShownType();
        int sportType = this.mConfig.getSportType();
        switch (sportType) {
            case 1:
                this.mGraphArray = RUNNING_CONFIG_GRAPH;
                break;
            case 7:
                this.mGraphArray = CROSSING_CONFIG_GRAPH;
                break;
            case 8:
                this.mGraphArray = INDOOR_RUNNING_CONFIG_GRAPH;
                break;
            case 9:
                this.mGraphArray = OUTDOOR_RIDING_CONFIG_GRAPH;
                break;
            case 14:
                this.mGraphArray = SWIM_CONFIG_GRAPH;
                break;
            case 15:
                this.mGraphArray = OPEN_WATER_CONFIG_GRAPH;
                break;
            default:
                throw new IllegalArgumentException("err sport type:" + sportType);
        }
        for (int i = 0; i < this.mGraphArray.length; i++) {
            if (this.mGraphArray[i] == graphType) {
                this.mSource = i;
                return;
            }
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new SourceAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setRealGraphShownType(this.mGraphArray[selectedRow[0]]);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
