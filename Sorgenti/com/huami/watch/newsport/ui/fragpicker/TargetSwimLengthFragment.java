package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class TargetSwimLengthFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    private class TargetTEPickerAdatper extends AbsPickerViewAdapter {
        public TargetTEPickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return 41;
        }

        public float getColumnWidth(int column) {
            return 135.0f;
        }

        public String getDisplayString(int row, int column) {
            return "" + (row + 10);
        }

        public String getUnit(int column) {
            if (UnitConvertUtils.isImperial()) {
                return this.mContext.getString(C0532R.string.metre_imperial);
            }
            return this.mContext.getString(C0532R.string.metre_metric);
        }

        public int getInitRow(int column) {
            return TargetSwimLengthFragment.this.mInitIndex;
        }

        public boolean hideNegativeButton() {
            return true;
        }

        public float getTextSize() {
            return 50.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mInitIndex = this.mConfig.getTargetSwimLength() - 10;
    }

    protected AbsPickerViewAdapter getAdapter() {
        return new TargetTEPickerAdatper(getActivity());
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setTargetSwimLength(selectedRow[0] + 10);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
