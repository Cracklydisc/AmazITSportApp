package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetTEFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    private class TargetTEPickerAdatper extends AbsPickerViewAdapter {
        public TargetTEPickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return 40;
        }

        public float getColumnWidth(int column) {
            return 135.0f;
        }

        public String getDisplayString(int row, int column) {
            return "" + (1.0f + (((float) row) * 0.1f));
        }

        public int getInitRow(int column) {
            return TargetTEFragment.this.mInitIndex;
        }

        public boolean hideNegativeButton() {
            return false;
        }

        public float getTextSize() {
            return 50.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        boolean isOpenTE = this.mConfig.isRemindTE();
        float targetTE = this.mConfig.getTargetTE();
        if (isOpenTE) {
            this.mInitIndex = Math.round((targetTE - 1.0f) / 0.1f);
        } else {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        return new TargetTEPickerAdatper(getActivity());
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindTE(true);
        this.mConfig.setTargetTE((((float) selectedRow[0]) * 0.1f) + 1.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onClose() {
        this.mConfig.setIsRemindTE(false);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
