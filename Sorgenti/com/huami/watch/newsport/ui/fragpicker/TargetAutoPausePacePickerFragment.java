package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetAutoPausePacePickerFragment extends AbsSportPickerFragment {
    private boolean isSpeedFlag = false;
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitMinuteIndex = 0;
    private int mInitSecondIndex = 0;

    class PacePickerAdapter extends AbsPickerViewAdapter {
        public PacePickerAdapter(Context context) {
            super(context);
        }

        public int getInitRow(int column) {
            switch (column) {
                case 0:
                    return TargetAutoPausePacePickerFragment.this.mInitMinuteIndex;
                case 1:
                    return TargetAutoPausePacePickerFragment.this.mInitSecondIndex;
                default:
                    return 0;
            }
        }

        public int columnCount() {
            return TargetAutoPausePacePickerFragment.this.isSpeedFlag ? 1 : 2;
        }

        public int rowCount(int column) {
            if (TargetAutoPausePacePickerFragment.this.isSpeedFlag) {
                return 30;
            }
            if (column == 0) {
                return 35;
            }
            return 51;
        }

        public String getDisplayString(int row, int column) {
            if (TargetAutoPausePacePickerFragment.this.isSpeedFlag) {
                return String.valueOf(row + 1);
            }
            if (column == 0) {
                return String.format("%02d'", new Object[]{Integer.valueOf(row + 2)});
            }
            return String.format("%02d\"", new Object[]{Integer.valueOf(row)});
        }

        public float getTextSize() {
            return 50.0f;
        }

        public float getNegativeButtonContentTextSize() {
            return 30.0f;
        }

        public float getColumnWidth(int column) {
            return 160.0f;
        }

        public String getNegativeButtonContent() {
            return TargetAutoPausePacePickerFragment.this.getString(C0532R.string.settings_content_stand_still);
        }

        public String getUnit(int column) {
            if (TargetAutoPausePacePickerFragment.this.isSpeedFlag) {
                return "km/h";
            }
            return "";
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.isSpeedFlag = this.mConfig.getSportType() == 9;
        if (this.isSpeedFlag) {
            this.mInitMinuteIndex = Math.round(this.mConfig.getAutoPausePace() * 3.6f) - 1;
            if (this.mInitMinuteIndex >= 30) {
                this.mInitMinuteIndex = 29;
                return;
            }
            return;
        }
        float secondPerKm = (float) Math.round(1000.0f * this.mConfig.getAutoPausePace());
        int minute = (int) (secondPerKm / 60.0f);
        int second = (int) (secondPerKm % 60.0f);
        if (minute < 2) {
            this.mInitMinuteIndex = -1;
            this.mInitSecondIndex = 0;
            return;
        }
        this.mInitMinuteIndex = minute - 2;
        this.mInitSecondIndex = second;
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new PacePickerAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onClose() {
        this.mConfig.setAutoPausePace(0.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onSave(int[] selectedRow) {
        if (this.isSpeedFlag) {
            this.mConfig.setAutoPausePace(((float) (selectedRow[0] + 1)) / 3.6f);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setAutoPausePace(((float) (((selectedRow[0] + 2) * 60) + selectedRow[1])) / 1000.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }
}
