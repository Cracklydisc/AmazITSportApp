package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetTimePickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitHourIndex = 0;
    private int mInitMinuteIndex = 0;

    class TimePickerAdapter extends AbsPickerViewAdapter {
        public TimePickerAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 2;
        }

        public int rowCount(int column) {
            if (column == 0) {
                return 24;
            }
            return 60;
        }

        public String getDisplayString(int row, int column) {
            return String.format("%02d", new Object[]{Integer.valueOf(row)});
        }

        public int getInitRow(int column) {
            switch (column) {
                case 0:
                    return TargetTimePickerFragment.this.mInitHourIndex;
                case 1:
                    return TargetTimePickerFragment.this.mInitMinuteIndex;
                default:
                    return 0;
            }
        }

        public String getUnit(int column) {
            if (column == 0) {
                return this.mContext.getString(C0532R.string.time_picker_hour);
            }
            return this.mContext.getString(C0532R.string.time_picker_minute);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        long targetTime = this.mConfig.getTargetTimeCost();
        int totalMinute = (int) (targetTime / 60000);
        if (targetTime < 0) {
            this.mInitHourIndex = 0;
            this.mInitMinuteIndex = 30;
        } else if (this.mConfig.isRemindTargetTimeCost()) {
            this.mInitHourIndex = totalMinute / 60;
            this.mInitMinuteIndex = totalMinute % 60;
        } else {
            this.mInitHourIndex = -1;
            this.mInitMinuteIndex = 0;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new TimePickerAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindTargetTimeCost(true);
        this.mConfig.setTargetTimeCost((long) ((selectedRow[0] * 3600000) + (selectedRow[1] * 60000)));
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected void onPositionChanged(int centerIndex) {
        if (centerIndex == 1) {
            int[] row = getSelected();
            if (row != null && row.length >= 2 && row[0] == 0 && row[1] == 0) {
                scrollToCorrectPosition();
            }
        }
    }

    protected void onClose() {
        this.mConfig.setIsRemindTargetTimeCost(false);
        if (this.mConfig.getTargetTimeCost() < 0) {
            this.mConfig.setTargetTimeCost(60000);
        }
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
