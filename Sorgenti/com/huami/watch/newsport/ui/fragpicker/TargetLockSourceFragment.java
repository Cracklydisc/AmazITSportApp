package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings.System;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetLockSourceFragment extends AbsSportPickerFragment {
    private static final int[] mRes = new int[]{C0532R.string.settings_sport_six_data, C0532R.string.settings_sport_four_data};
    private SourceAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private int mSource = 0;

    private class SourceAdapter extends AbsPickerViewAdapter {
        public SourceAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int column) {
            return TargetLockSourceFragment.this.mSource;
        }

        public int rowCount(int column) {
            return 2;
        }

        public String getDisplayString(int row, int column) {
            if (row >= 0 && row <= 2) {
                return TargetLockSourceFragment.this.getActivity().getString(TargetLockSourceFragment.mRes[row]);
            }
            throw new IllegalArgumentException("row is err: " + row);
        }

        public float getColumnWidth(int column) {
            return 200.0f;
        }

        public float getTextSize() {
            return 32.0f;
        }

        public boolean hideNegativeButton() {
            return true;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mSource = this.mConfig.getDefaultDataShown() == 6 ? 0 : 1;
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new SourceAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        int value = selectedRow[0] == 0 ? 6 : 4;
        this.mConfig.setDefaultDataShown(value);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        System.putInt(getActivity().getContentResolver(), "key_sport_lock_source", value);
        this.mListener.onSelectFinish();
    }
}
