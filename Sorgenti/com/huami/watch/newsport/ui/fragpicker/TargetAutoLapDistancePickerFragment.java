package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class TargetAutoLapDistancePickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mDefaultRowCount = 100;
    private int mInitIndex = 0;

    private class TargetDistancePickerAdatper extends AbsPickerViewAdapter {
        public TargetDistancePickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return TargetAutoLapDistancePickerFragment.this.mDefaultRowCount;
        }

        public float getColumnWidth(int column) {
            return 135.0f;
        }

        public String getDisplayString(int row, int column) {
            if (TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 1 || TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 8 || TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 6) {
                float km = ((float) (row + 1)) * 0.1f;
                return "" + String.format("%.1f", new Object[]{Float.valueOf(km)});
            } else if (TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 14) {
                return "" + (TargetAutoLapDistancePickerFragment.this.mConfig.getTargetSwimLength() + (TargetAutoLapDistancePickerFragment.this.mConfig.getTargetSwimLength() * row));
            } else {
                int km2 = row + 1;
                if (km2 < 10) {
                    return "0" + km2;
                }
                return "" + km2;
            }
        }

        public String getUnit(int column) {
            if (TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 14) {
                if (UnitConvertUtils.isImperial()) {
                    return this.mContext.getString(C0532R.string.metre_imperial);
                }
                return this.mContext.getString(C0532R.string.metre_metric);
            } else if (UnitConvertUtils.isImperial()) {
                return this.mContext.getString(C0532R.string.km_imperial);
            } else {
                return this.mContext.getString(C0532R.string.km_metric);
            }
        }

        public int getInitRow(int column) {
            return TargetAutoLapDistancePickerFragment.this.mInitIndex;
        }

        public boolean hideNegativeButton() {
            return false;
        }

        public float getTextSize() {
            if (TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 14 || TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 1 || TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 8 || TargetAutoLapDistancePickerFragment.this.mConfig.getSportType() == 6) {
                return 40.0f;
            }
            return 50.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int sportType = this.mConfig.getSportType();
        if (sportType == 1 || sportType == 8 || sportType == 6) {
            this.mDefaultRowCount = 1000;
        } else if (this.mConfig.getSportType() == 9) {
            this.mDefaultRowCount = 100;
        } else if (this.mConfig.getSportType() == 14) {
            this.mDefaultRowCount = 1000 / this.mConfig.getTargetSwimLength();
        } else if (this.mConfig.getSportType() == 7) {
            this.mDefaultRowCount = 100;
        }
        int targetDis = (int) this.mConfig.getDistanceAutoLap();
        if (this.mConfig.isOpenAutoLapRecord()) {
            if (targetDis < 0) {
                this.mInitIndex = 0;
            } else if (sportType == 14) {
                this.mInitIndex = (targetDis / this.mConfig.getTargetSwimLength()) - 1;
            } else if (sportType == 1 || sportType == 8 || sportType == 6) {
                this.mInitIndex = (targetDis / 100) - 1;
            } else {
                this.mInitIndex = Math.round(this.mConfig.getDistanceAutoLap() / 1000.0f) - 1;
            }
        } else if (sportType == 1 || sportType == 8 || sportType == 6) {
            this.mInitIndex = 3;
        } else if (sportType == 9) {
            this.mInitIndex = 4;
        } else {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new TargetDistancePickerAdatper(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsOpenAutoLapRecord(true);
        if (this.mConfig.getSportType() == 1 || this.mConfig.getSportType() == 8 || this.mConfig.getSportType() == 6) {
            this.mConfig.setDistanceAutoLap((float) ((selectedRow[0] + 1) * 100));
        } else if (this.mConfig.getSportType() == 14) {
            this.mConfig.setDistanceAutoLap((float) ((selectedRow[0] + 1) * this.mConfig.getTargetSwimLength()));
        } else {
            this.mConfig.setDistanceAutoLap((float) ((selectedRow[0] + 1) * 1000));
        }
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onClose() {
        this.mConfig.setIsOpenAutoLapRecord(false);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
