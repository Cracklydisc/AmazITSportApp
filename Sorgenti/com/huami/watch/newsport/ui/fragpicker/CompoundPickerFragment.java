package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import java.util.List;

public class CompoundPickerFragment extends AbsMultiPageSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private MixedBaseConfig mConfig = null;
    private int mInitFirstIndex = 0;
    private int mSequence = 0;

    private class ComPoundPickerAdpter extends AbsPickerViewAdapter {
        public ComPoundPickerAdpter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int column) {
            return CompoundPickerFragment.this.mInitFirstIndex;
        }

        public int rowCount(int column) {
            if (CompoundPickerFragment.this.mSequence == 2) {
                return 4;
            }
            return 3;
        }

        public String getDisplayString(int row, int column) {
            String str = "";
            if (CompoundPickerFragment.this.mSequence == 2) {
                if (row == 0) {
                    return CompoundPickerFragment.this.getString(C0532R.string.settings_item_value_closed);
                }
                if (row == 1) {
                    return CompoundPickerFragment.this.getString(C0532R.string.sport_child_swim);
                }
                if (row == 2) {
                    return CompoundPickerFragment.this.getString(C0532R.string.sport_child_riding);
                }
                if (row == 3) {
                    return CompoundPickerFragment.this.getString(C0532R.string.sport_child_running);
                }
                return str;
            } else if (row == 0) {
                return CompoundPickerFragment.this.getString(C0532R.string.sport_child_swim);
            } else {
                if (row == 1) {
                    return CompoundPickerFragment.this.getString(C0532R.string.sport_child_riding);
                }
                if (row == 2) {
                    return CompoundPickerFragment.this.getString(C0532R.string.sport_child_running);
                }
                return str;
            }
        }

        public boolean hideNegativeButton() {
            return true;
        }

        public float getTextSize() {
            return 26.0f;
        }

        public boolean hasDefaultButton() {
            if (CompoundPickerFragment.this.mSequence == 2) {
                return true;
            }
            return false;
        }

        public float getColumnWidth(int column) {
            return 300.0f;
        }

        public String getTitle() {
            if (CompoundPickerFragment.this.mSequence == 0) {
                return CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport, CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport_first));
            } else if (CompoundPickerFragment.this.mSequence == 1) {
                return CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport, CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport_second));
            } else if (CompoundPickerFragment.this.mSequence == 2) {
                return CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport, CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport_three));
            } else {
                return CompoundPickerFragment.this.getString(C0532R.string.select_compound_sport, String.valueOf(CompoundPickerFragment.this.mSequence + 1));
            }
        }
    }

    public static CompoundPickerFragment newInstance(MixedBaseConfig config, int sequence) {
        CompoundPickerFragment fragment = new CompoundPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        argument.putInt("key_sequence", sequence);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (MixedBaseConfig) getArguments().getParcelable("key_config");
        this.mSequence = getArguments().getInt("key_sequence", 0);
        List<Integer> childSports = this.mConfig.getChildSports();
        if (childSports.size() > this.mSequence) {
            this.mInitFirstIndex = getIndex(((Integer) childSports.get(this.mSequence)).intValue(), this.mSequence);
        }
    }

    public void onClickConfirmButton() {
        if (this.mListener != null) {
            this.mListener.onClickConfirmButton();
        } else {
            getActivity().finish();
        }
    }

    public boolean needIntercept() {
        if (this.mSequence == 2) {
            return true;
        }
        return false;
    }

    public int getItemPostion() {
        return this.mPickerView.getSelectedRow(0);
    }

    public AbsPickerViewAdapter getAdapter() {
        return new ComPoundPickerAdpter(getActivity());
    }

    private int getIndex(int sportType, int column) {
        if (column == 2) {
            if (sportType == 1009) {
                return 2;
            }
            if (sportType == 1001) {
                return 3;
            }
            if (sportType == 1015) {
                return 1;
            }
        } else if (sportType == 1009) {
            return 1;
        } else {
            if (sportType == 1001) {
                return 2;
            }
            if (sportType == 1015) {
                return 0;
            }
        }
        return 0;
    }
}
