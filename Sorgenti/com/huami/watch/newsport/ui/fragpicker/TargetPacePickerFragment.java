package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.Arrays;

public class TargetPacePickerFragment extends AbsSportPickerFragment {
    private static final int[] MINUTE_ROW = new int[]{4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    private static final int[] SECOND_ROW = new int[]{0, 15, 30, 45};
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitMinuteIndex = 0;
    private int mInitSecondIndex = 0;

    class PacePickerAdapter extends AbsPickerViewAdapter {
        public PacePickerAdapter(Context context) {
            super(context);
        }

        public int getInitRow(int column) {
            switch (column) {
                case 0:
                    return TargetPacePickerFragment.this.mInitMinuteIndex;
                case 1:
                    return TargetPacePickerFragment.this.mInitSecondIndex;
                default:
                    return 0;
            }
        }

        public int columnCount() {
            return 2;
        }

        public int rowCount(int column) {
            if (column == 0) {
                return TargetPacePickerFragment.MINUTE_ROW.length;
            }
            return TargetPacePickerFragment.SECOND_ROW.length;
        }

        public String getDisplayString(int row, int column) {
            if (column == 0) {
                return String.format("%02d'", new Object[]{Integer.valueOf(TargetPacePickerFragment.MINUTE_ROW[row])});
            }
            return String.format("%02d\"", new Object[]{Integer.valueOf(TargetPacePickerFragment.SECOND_ROW[row])});
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        int i = 0;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        float pace = this.mConfig.getTargetPace();
        float secondPerKm = (float) Math.round(1000.0f * pace);
        int minute = (int) (secondPerKm / 60.0f);
        int second = (int) (secondPerKm % 60.0f);
        if (pace < 0.0f) {
            this.mInitMinuteIndex = 2;
            this.mInitSecondIndex = 0;
        } else if (this.mConfig.isRemindTargetPace()) {
            int i2;
            this.mInitMinuteIndex = Arrays.binarySearch(MINUTE_ROW, minute);
            this.mInitSecondIndex = Arrays.binarySearch(SECOND_ROW, second);
            if (this.mInitMinuteIndex > 0) {
                i2 = this.mInitMinuteIndex;
            } else {
                i2 = 0;
            }
            this.mInitMinuteIndex = i2;
            if (this.mInitSecondIndex > 0) {
                i = this.mInitSecondIndex;
            }
            this.mInitSecondIndex = i;
        } else {
            this.mInitMinuteIndex = -1;
            this.mInitSecondIndex = 0;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new PacePickerAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onClose() {
        this.mConfig.setIsRemindTargetPace(false);
        if (this.mConfig.getTargetPace() < 0.0f) {
            this.mConfig.setTargetPace(((float) ((MINUTE_ROW[2] * 60) + SECOND_ROW[0])) / 1000.0f);
        }
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onSave(int[] selectedRow) {
        int secondPerKm = (MINUTE_ROW[selectedRow[0]] * 60) + SECOND_ROW[selectedRow[1]];
        this.mConfig.setIsRemindTargetPace(true);
        this.mConfig.setTargetPace(((float) secondPerKm) / 1000.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }
}
