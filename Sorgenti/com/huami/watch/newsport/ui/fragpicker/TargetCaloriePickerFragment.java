package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetCaloriePickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    private class TargetCaloriePickerAdatper extends AbsPickerViewAdapter {
        public TargetCaloriePickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return 80;
        }

        public float getColumnWidth(int column) {
            return 160.0f;
        }

        public String getDisplayString(int row, int column) {
            return "" + ((row + 1) * 25);
        }

        public String getUnit(int column) {
            return this.mContext.getString(C0532R.string.settings_cal_unit);
        }

        public int getInitRow(int column) {
            return TargetCaloriePickerFragment.this.mInitIndex;
        }

        public float getTextSize() {
            return 30.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        if (this.mConfig.getTargetCalorie() < 0) {
            this.mInitIndex = 2;
        } else if (this.mConfig.isRemindCalorie()) {
            this.mInitIndex = Math.round((float) (this.mConfig.getTargetCalorie() / 25)) - 1;
        } else {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new TargetCaloriePickerAdatper(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindCalorie(true);
        this.mConfig.setTargetCalorie((selectedRow[0] + 1) * 25);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected void onClose() {
        this.mConfig.setIsRemindCalorie(false);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
