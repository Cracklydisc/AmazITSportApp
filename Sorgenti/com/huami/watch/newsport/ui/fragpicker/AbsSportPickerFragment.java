package com.huami.watch.newsport.ui.fragpicker;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.widget.HmPickerView;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.common.widget.HmPickerView.IOnButtonClickListener;
import com.huami.watch.common.widget.HmPickerView.IOnSelectChangeListener;
import com.huami.watch.extendsapi.SwipeDismissUtil;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.newsport.C0532R;

public abstract class AbsSportPickerFragment extends Fragment {
    private String mAdapterClassName;
    protected OnPickerSelectedListener mListener;
    private HmPickerView mPickerView = null;
    private int[] mSelectedRow = null;

    public interface OnPickerSelectedListener {
        void onSelectFinish();
    }

    class C07481 implements IOnSelectChangeListener {
        C07481() {
        }

        public void onChange(int centerIndex, int[] positions, boolean isNeedUpdate) {
            AbsSportPickerFragment.this.mSelectedRow = positions;
            if (isNeedUpdate) {
                AbsSportPickerFragment.this.onPositionChanged(centerIndex);
            }
        }
    }

    class C07492 implements IOnButtonClickListener {
        C07492() {
        }

        public void onClose() {
            AbsSportPickerFragment.this.onClose();
        }

        public void onSelectedConfirm() {
            if (AbsSportPickerFragment.this.mSelectedRow == null) {
                AbsSportPickerFragment.this.mSelectedRow = AbsSportPickerFragment.this.mPickerView.getSelected();
            }
            AbsSportPickerFragment.this.onSave(AbsSportPickerFragment.this.mSelectedRow);
            AbsSportPickerFragment.this.mListener.onSelectFinish();
        }
    }

    protected abstract AbsPickerViewAdapter getAdapter();

    protected abstract void onSave(int[] iArr);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mAdapterClassName = getArguments().getString("adapter_name");
        }
    }

    protected void onClose() {
    }

    protected int[] getSelected() {
        if (this.mPickerView != null) {
            return this.mPickerView.getSelected();
        }
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(C0532R.layout.fragment_sport_picker, container, false);
        this.mPickerView = (HmPickerView) contentView.findViewById(C0532R.id.picker);
        this.mPickerView.setHasFocusedLine(true);
        ViewPagerPageIndicator viewpagerpageindicator = (ViewPagerPageIndicator) contentView.findViewById(C0532R.id.indicator);
        viewpagerpageindicator.setPagerIndicatorAdapter(this.mPickerView);
        viewpagerpageindicator.showIndicator(true);
        SwipeDismissUtil.requestSwipeDismissAlphaBackground(getActivity(), this.mPickerView);
        this.mPickerView.setAdapter(getAdapter());
        this.mPickerView.setOnSelectChangeListener(new C07481());
        this.mPickerView.setOnButtonClickListener(new C07492());
        return contentView;
    }

    protected void scrollToCorrectPosition() {
        if (this.mPickerView != null) {
            this.mPickerView.scrollToCorrectPosition();
        }
    }

    protected void scrollToCorrectPosition(int pos) {
        if (this.mPickerView != null) {
            this.mPickerView.scrollToCorrectPosition(pos);
        }
    }

    protected void onPositionChanged(int centerIndex) {
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnPickerSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnPickerSelectedListener");
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }
}
