package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.MixedBaseConfig;
import java.util.ArrayList;
import java.util.List;

public class SelectCompoundPickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private MixedBaseConfig mConfig = null;
    private int mInitFirstIndex = 0;
    private int mInitSecIndex = 0;
    private int mInitThrIndex = 0;

    class CompoundPickerAdapter extends AbsPickerViewAdapter {
        public CompoundPickerAdapter(Context context) {
            super(context);
        }

        public int getInitRow(int column) {
            switch (column) {
                case 0:
                    return SelectCompoundPickerFragment.this.mInitFirstIndex;
                case 1:
                    return SelectCompoundPickerFragment.this.mInitSecIndex;
                case 2:
                    return SelectCompoundPickerFragment.this.mInitThrIndex;
                default:
                    return 0;
            }
        }

        public int columnCount() {
            return 3;
        }

        public int rowCount(int column) {
            if (column == 2) {
                return 4;
            }
            return 3;
        }

        public String getDisplayString(int row, int column) {
            String str = "";
            if (column == 2) {
                if (row == 0) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.settings_item_value_closed);
                }
                if (row == 1) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_swim);
                }
                if (row == 2) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_riding);
                }
                if (row == 3) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_running);
                }
                return str;
            } else if (row == 0) {
                return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_swim);
            } else {
                if (row == 1) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_riding);
                }
                if (row == 2) {
                    return SelectCompoundPickerFragment.this.getString(C0532R.string.sport_child_running);
                }
                return str;
            }
        }

        public boolean hideNegativeButton() {
            return true;
        }

        public float getTextSize() {
            return 26.0f;
        }
    }

    public static SelectCompoundPickerFragment newInstance(MixedBaseConfig config) {
        SelectCompoundPickerFragment fragment = new SelectCompoundPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (MixedBaseConfig) getArguments().getParcelable("key_config");
        List<Integer> childSports = this.mConfig.getChildSports();
        if (childSports.size() >= 1) {
            this.mInitFirstIndex = getIndex(((Integer) childSports.get(0)).intValue(), 0);
            if (childSports.size() >= 2) {
                this.mInitSecIndex = getIndex(((Integer) childSports.get(1)).intValue(), 1);
                if (childSports.size() >= 3) {
                    this.mInitThrIndex = getIndex(((Integer) childSports.get(2)).intValue(), 2);
                }
            }
        }
    }

    private int getIndex(int sportType, int column) {
        if (column == 2) {
            if (sportType == 1009) {
                return 2;
            }
            if (sportType == 1001) {
                return 3;
            }
            if (sportType == 1015) {
                return 1;
            }
        } else if (sportType == 1009) {
            return 1;
        } else {
            if (sportType == 1001) {
                return 2;
            }
            if (sportType == 1015) {
                return 0;
            }
        }
        return 0;
    }

    private int getSportType(int index, int column) {
        if (column == 2) {
            if (index == 1) {
                return 1015;
            }
            if (index == 2) {
                return 1009;
            }
            if (index == 3) {
                return 1001;
            }
        } else if (index == 0) {
            return 1015;
        } else {
            if (index == 1) {
                return 1009;
            }
            if (index == 2) {
                return 1001;
            }
        }
        return -1;
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new CompoundPickerAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onClose() {
    }

    protected void onSave(int[] selectedRow) {
        List<Integer> childSports = new ArrayList();
        for (int i = 0; i < selectedRow.length; i++) {
            int sportType = getSportType(selectedRow[i], i);
            if (SportType.isSportTypeValid(sportType)) {
                childSports.add(Integer.valueOf(sportType));
            }
        }
        this.mConfig.setChildSports(childSports);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }
}
