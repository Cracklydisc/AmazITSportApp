package com.huami.watch.newsport.ui.fragpicker;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.common.widget.HmPickerView.IOnButtonClickListener;
import com.huami.watch.newsport.ui.view.CompoundPickerView;

public abstract class AbsMultiPageSportPickerFragment extends Fragment {
    protected IResultConfirmLisener mListener = null;
    protected CompoundPickerView mPickerView;

    public interface IResultConfirmLisener {
        void initViewCompleted();

        void onClickConfirmButton();
    }

    class C07471 implements IOnButtonClickListener {
        C07471() {
        }

        public void onClose() {
        }

        public void onSelectedConfirm() {
            AbsMultiPageSportPickerFragment.this.onClickConfirmButton();
        }
    }

    public abstract AbsPickerViewAdapter getAdapter();

    public abstract int getItemPostion();

    public abstract boolean needIntercept();

    public abstract void onClickConfirmButton();

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IResultConfirmLisener) {
            this.mListener = (IResultConfirmLisener) activity;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mPickerView = new CompoundPickerView(getActivity());
        this.mPickerView.setNeedIntercept(needIntercept());
        this.mPickerView.setItemTextSingleLine(true);
        this.mPickerView.setHasFocusedLine(true, getLineMargin());
        this.mPickerView.setAdapter(getAdapter());
        this.mPickerView.setOnButtonClickListener(new C07471());
        if (this.mListener != null) {
            this.mListener.initViewCompleted();
        }
        return this.mPickerView;
    }

    public void setCenterIndex(int index) {
        this.mPickerView.setCenterIndex(index, true);
    }

    public int getCurIndex() {
        return this.mPickerView.getCurrentIndex();
    }

    protected int getLineMargin() {
        return 100;
    }

    public void setCanAccept(boolean canAccept) {
        if (this.mPickerView != null && this.mPickerView.getCurrentIndex() == 0) {
            this.mPickerView.setChildViewCanAccept(0, canAccept);
        }
    }
}
