package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class IndoorPickerFragment extends AbsSportPickerFragment {
    private int initCount;
    private IndoorAdapter mAdapter;
    private double mMinScale = 0.0d;
    private double mMinValue = 0.0d;
    private int rowCount;
    private double sportData;

    class IndoorAdapter extends AbsPickerViewAdapter {
        public IndoorAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return IndoorPickerFragment.this.rowCount;
        }

        public String getDisplayString(int row, int column) {
            return DataFormatUtils.parseFormattedRealNumber(IndoorPickerFragment.this.mMinValue + (((double) row) * IndoorPickerFragment.this.mMinScale), true);
        }

        public int getInitRow(int column) {
            return IndoorPickerFragment.this.initCount;
        }

        public String getUnit(int column) {
            if (UnitConvertUtils.isImperial()) {
                return IndoorPickerFragment.this.getString(C0532R.string.km_imperial);
            }
            return IndoorPickerFragment.this.getString(C0532R.string.km_metric);
        }

        public boolean hideNegativeButton() {
            return true;
        }

        public boolean hasDefaultButton() {
            return true;
        }

        public float getColumnWidth(int column) {
            return 160.0f;
        }

        public float getTextSize() {
            return 45.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.sportData = getArguments().getDouble("distance_picker", 0.0d);
        this.sportData = NumeriConversionUtils.getDoubleValue(String.valueOf(this.sportData), 2);
        if (Double.compare(this.sportData, 0.01d) == 0) {
            this.rowCount = 2;
            this.mMinValue = 0.01d;
            this.mMinScale = 0.01d;
            this.initCount = 0;
            return;
        }
        this.rowCount = getRowCount(this.sportData);
        this.mMinValue = getMinValue(this.rowCount);
        this.initCount = getInitRow(this.rowCount);
    }

    private double getMinValue(int rowCount) {
        return NumeriConversionUtils.getDoubleValue(String.valueOf(this.sportData - (((double) ((rowCount - 1) / 2)) * this.mMinScale)));
    }

    private double getInterval(double number) {
        int integer = (int) number;
        if (integer < 1) {
            return 0.01d;
        }
        return 0.01d * ((double) integer);
    }

    private int getRowCount(double number) {
        double end = NumeriConversionUtils.getMulResult(String.valueOf(number), String.valueOf(1.5d));
        this.mMinScale = getInterval(number);
        int count = (int) NumeriConversionUtils.getDivResult(String.valueOf(NumeriConversionUtils.getSubtractResult(String.valueOf(end), String.valueOf(number))), String.valueOf(this.mMinScale));
        if (count < 1) {
            count = 1;
        }
        return (count * 2) + 1;
    }

    private int getInitRow(int count) {
        return (count - 1) / 2;
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new IndoorAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        double dis = NumeriConversionUtils.getDoubleValue(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceFromMileOrKmtokm(this.mMinValue + (((double) selectedRow[0]) * this.mMinScale)), true));
        Intent result = new Intent();
        Log.d("sportCorrect", "  dis = " + dis);
        result.putExtra("sportNumber", dis);
        getActivity().setResult(-1, result);
        getActivity().finish();
        getActivity().overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
    }
}
