package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetClapThaHandsFragment extends AbsV2SportPickerFragment {
    private static final String TAG = V2TargetClapThaHandsFragment.class.getSimpleName();
    private BaseConfig mConfig;
    private int mInitIndex = -1;

    public static V2TargetClapThaHandsFragment newInstance(BaseConfig config) {
        V2TargetClapThaHandsFragment fragment = new V2TargetClapThaHandsFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        Log.i(TAG, " isLeftRightHandle:" + this.mConfig.getTennicClapThaHands());
        if (this.mConfig.getTennicClapThaHands() >= 0) {
            int i;
            if (this.mConfig.getTennicClapThaHands() == 0) {
                i = 1;
            } else {
                i = 0;
            }
            this.mInitIndex = i;
            switch (this.mConfig.getTennicClapThaHands()) {
                case 0:
                    Log.i(TAG, " CLAP_LEFT_HANDS ");
                    this.mInitIndex = 1;
                    break;
                case 1:
                    Log.i(TAG, " CLAP_RIGHT_HANDS ");
                    this.mInitIndex = 0;
                    break;
                default:
                    break;
            }
        }
        Log.i(TAG, " mInitIndex < 0 ");
        this.mInitIndex = 0;
        Log.i(TAG, " mInitIndex:" + this.mInitIndex);
    }

    protected void onSave(int[] selectedRow) {
        int i = 0;
        Log.i(TAG, "selectedRow[0]:" + selectedRow[0]);
        BaseConfig baseConfig = this.mConfig;
        if (selectedRow[0] == 0) {
            i = 1;
        }
        baseConfig.setTennicClapThaHands(i);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        Log.i(TAG, " getArrayContent:" + column);
        array.add(getString(C0532R.string.sport_setting_right_hand));
        array.add(getString(C0532R.string.sport_setting_left_hand));
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.settings_sport_control);
    }
}
