package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetHeartRegionRemindFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig;
    private int mInitHeartRegionMax = 0;
    private int mInitHeartRegionMin = 0;

    public static V2TargetHeartRegionRemindFragment newInstance(BaseConfig config) {
        V2TargetHeartRegionRemindFragment fragment = new V2TargetHeartRegionRemindFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        int i = 0;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int age = UserInfoManager.getUserAge(getActivity());
        if (this.mConfig.ismHeartRegionRemind()) {
            int i2;
            BaseConfig baseConfig = this.mConfig;
            if (BaseConfig.getCustomMinHeartRate() - 97 < 0) {
                i2 = 0;
            } else {
                baseConfig = this.mConfig;
                i2 = BaseConfig.getCustomMinHeartRate() - 97;
            }
            this.mInitHeartRegionMin = i2;
            baseConfig = this.mConfig;
            if (BaseConfig.getCustomMaxHeartRate() - 99 >= 0) {
                baseConfig = this.mConfig;
                i = BaseConfig.getCustomMaxHeartRate() - 99;
            }
            this.mInitHeartRegionMax = i;
            return;
        }
        int heartMin = ((220 - age) * 50) / 100;
        int heartMax = ((220 - age) * 100) / 100;
        this.mInitHeartRegionMin = heartMin + -97 < 0 ? 0 : heartMin - 97;
        if (heartMax - 99 >= 0) {
            i = heartMax - 99;
        }
        this.mInitHeartRegionMax = i;
    }

    protected void onSave(int[] selectedRow) {
        int leftHeartRegion = selectedRow[0] + 97;
        int rigthHeartRegion = selectedRow[1] + 99;
        if (rigthHeartRegion > leftHeartRegion) {
            if (leftHeartRegion == rigthHeartRegion || rigthHeartRegion - leftHeartRegion < 2) {
                leftHeartRegion = rigthHeartRegion - 2;
            }
        } else if (rigthHeartRegion < leftHeartRegion || rigthHeartRegion == leftHeartRegion) {
            leftHeartRegion = rigthHeartRegion - 2;
        }
        this.mConfig.setmHeartRegionValueMin(leftHeartRegion);
        this.mConfig.setmHeartRegionValueMax(rigthHeartRegion);
        BaseConfig baseConfig = this.mConfig;
        BaseConfig.setCustomMinHeartRate(leftHeartRegion);
        baseConfig = this.mConfig;
        BaseConfig.setCustomMaxHeartRate(rigthHeartRegion);
        this.mConfig.setmHeartRegionRemind(true);
        this.mConfig.setmHeartRegionType(5);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected String[] getArrayContent(int column) {
        List<String> array;
        int row;
        if (column == 0) {
            array = new ArrayList();
            for (row = 0; row < 106; row++) {
                array.add(String.valueOf(row + 97));
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column != 1) {
            return null;
        } else {
            array = new ArrayList();
            for (row = 0; row < 106; row++) {
                array.add(String.valueOf(row + 99));
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (column == 0) {
            return this.mInitHeartRegionMin;
        }
        if (column == 1) {
            return this.mInitHeartRegionMax;
        }
        return 0;
    }

    protected String getTitle() {
        return getString(C0532R.string.setting_sport_heart_region_custom);
    }
}
