package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import android.util.Log;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class V2TargetHeartRegionSimplePickFragment extends AbsV2SportPickerFragment {
    private static final String TAG = V2TargetHeartRegionSimplePickFragment.class.getSimpleName();
    private BaseConfig mConfig;
    private int mInitIndex = -1;
    private int[] maxList = new int[5];
    private int[] minList = new int[5];
    private int userAge;
    private UserInfo userInfo;

    public static V2TargetHeartRegionSimplePickFragment newInstance(BaseConfig config) {
        V2TargetHeartRegionSimplePickFragment fragment = new V2TargetHeartRegionSimplePickFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.userInfo = UserInfoManager.getInstance().getUserInfo(getActivity());
        int currentYear = Calendar.getInstance().get(1);
        if (this.userInfo == null || this.userInfo.getYear() <= 0) {
            this.userAge = 25;
        } else {
            this.userAge = currentYear - this.userInfo.getYear();
        }
        if (this.mConfig.ismHeartRegionRemind()) {
            this.mInitIndex = this.mConfig.getmHeartRegionType();
        } else {
            this.mInitIndex = 0;
        }
        Log.i(TAG, " isHeartRegionRemind:" + this.mConfig.ismHeartRegionRemind() + ",mInitIndex:" + this.mInitIndex);
        this.minList[0] = ((220 - this.userAge) * 50) / 100;
        this.maxList[0] = ((220 - this.userAge) * 60) / 100;
        this.minList[1] = ((220 - this.userAge) * 60) / 100;
        this.maxList[1] = ((220 - this.userAge) * 70) / 100;
        this.minList[2] = ((220 - this.userAge) * 70) / 100;
        this.maxList[2] = ((220 - this.userAge) * 80) / 100;
        this.minList[3] = ((220 - this.userAge) * 80) / 100;
        this.maxList[3] = ((220 - this.userAge) * 90) / 100;
        this.minList[4] = ((220 - this.userAge) * 90) / 100;
        this.maxList[4] = ((220 - this.userAge) * 100) / 100;
        for (int i = 0; i < 5; i++) {
            LogUtils.print(TAG, "run minList index:" + i + " , min:" + this.minList[i] + ",max:" + this.maxList[i]);
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            LogUtils.print(TAG, "onClose");
            this.mConfig.setmHeartRegionRemind(false);
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
            return;
        }
        LogUtils.print(TAG, "onSave： minIndex:" + selectedRow[0]);
        if (this.mConfig != null) {
            this.mConfig.setmHeartRegionValueMin(this.minList[selectedRow[0] - 1]);
            this.mConfig.setmHeartRegionValueMax(this.maxList[selectedRow[0] - 1]);
            this.mConfig.setmHeartRegionRemind(true);
            this.mConfig.setmHeartRegionType(selectedRow[0]);
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        }
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 0; i < 5; i++) {
            array.add(getString(C0532R.string.picker_heart_region_content, new Object[]{Integer.valueOf(i + 1)}));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add("");
        for (int i = 0; i < 5; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(this.minList[i]).append("-").append(this.maxList[i]).append(" bpm");
            array.add(sb.toString());
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.picker_title_target_heart_rate);
    }
}
