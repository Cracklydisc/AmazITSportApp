package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetSwimLengthFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;
    private int mInitUnitIndex = 0;
    private boolean mIsFirstConfig = false;

    public static V2TargetSwimLengthFragment newInstance(BaseConfig config, boolean isFirst) {
        V2TargetSwimLengthFragment fragment = new V2TargetSwimLengthFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        argument.putBoolean("key_is_first_config", isFirst);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mIsFirstConfig = getArguments().getBoolean("key_is_first_config", false);
        this.mInitIndex = this.mConfig.getTargetSwimLength() - 10;
        this.mInitUnitIndex = this.mConfig.getUnit();
    }

    protected void onSave(int[] selectedRow) {
        int unit = 0;
        if (isOverSea()) {
            unit = selectedRow[1];
        }
        this.mConfig.setUnit(unit);
        this.mConfig.setTargetSwimLength(selectedRow[0] + 10);
        this.mConfig.setDistanceAutoLap((float) this.mConfig.getTargetSwimLength());
        DataManager.getInstance().setLastSwimLength(this.mConfig.getTargetSwimLength());
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    public void onDetach() {
        super.onDetach();
        if (this.mIsFirstConfig) {
            this.mConfig.setTargetSwimLength(this.mConfig.getTargetSwimLength());
            this.mConfig.setDistanceAutoLap((float) this.mConfig.getTargetSwimLength());
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
            DataManager.getInstance().setIsFirstOpenSwim(false);
            DataManager.getInstance().setLastSwimLength(this.mConfig.getTargetSwimLength());
        }
    }

    protected String[] getArrayContent(int column) {
        List<String> array;
        int i;
        if (isOverSea()) {
            if (column == 0) {
                array = new ArrayList();
                for (i = 0; i < 191; i++) {
                    array.add("" + (i + 10));
                }
                return (String[]) array.toArray(new String[array.size()]);
            } else if (column == 1) {
                array = new ArrayList();
                array.add(getString(C0532R.string.metre_metric));
                array.add(getString(C0532R.string.swim_unit));
                return (String[]) array.toArray(new String[array.size()]);
            }
        } else if (column == 0) {
            array = new ArrayList();
            for (i = 0; i < 41; i++) {
                array.add("" + (i + 10));
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
        return null;
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (!isOverSea()) {
            return this.mInitIndex;
        }
        if (column == 0) {
            return this.mInitIndex;
        }
        return this.mInitUnitIndex;
    }

    protected String getTitle() {
        if (isOverSea()) {
            return getString(C0532R.string.title_custom_swim_length);
        }
        if (this.mConfig.getUnit() == 0) {
            return getString(C0532R.string.picker_title_swim_length, new Object[]{getString(C0532R.string.metre_metric)});
        }
        return getString(C0532R.string.picker_title_swim_length, new Object[]{getString(C0532R.string.swim_unit)});
    }

    private boolean isOverSea() {
        return true;
    }
}
