package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import android.provider.Settings.System;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2TargetLockSourceFragment extends AbsV2SportPickerFragment {
    private static final int[] mRes = new int[]{C0532R.string.settings_sport_six_data, C0532R.string.settings_sport_four_data};
    private BaseConfig mConfig = null;
    private int mSource = 0;

    public static V2TargetLockSourceFragment newInstance(BaseConfig config) {
        V2TargetLockSourceFragment fragment = new V2TargetLockSourceFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mSource = this.mConfig.getDefaultDataShown() == 6 ? 0 : 1;
    }

    protected void onSave(int[] selectedRow) {
        int value = selectedRow[0] == 0 ? 6 : 4;
        this.mConfig.setDefaultDataShown(value);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        System.putInt(getActivity().getContentResolver(), "key_sport_lock_source", value);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int string : mRes) {
            array.add(getActivity().getString(string));
        }
        return (String[]) array.toArray(new String[mRes.length]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mSource;
    }

    protected String getTitle() {
        return getString(C0532R.string.settings_sport_items);
    }

    protected int getColumnTextSize(int column) {
        if (UnitConvertUtils.isZh()) {
            return super.getColumnTextSize(column);
        }
        return 26;
    }
}
