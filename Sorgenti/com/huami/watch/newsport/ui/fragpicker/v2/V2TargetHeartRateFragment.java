package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetHeartRateFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private int mInitIndex = 0;

    public static V2TargetHeartRateFragment newInstance(BaseConfig config) {
        V2TargetHeartRateFragment fragment = new V2TargetHeartRateFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        boolean isFirst;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int heartRate = (int) this.mConfig.getSafeHeartRateHigh();
        if (heartRate <= 0) {
            isFirst = true;
        } else {
            isFirst = false;
        }
        if (heartRate < 0) {
            heartRate = UserInfoManager.getDefaultSafeHeart(getActivity());
            this.mConfig.setSafeHeartRateHigh((float) heartRate);
        } else if (heartRate < 97 || heartRate > 202) {
            heartRate = UserInfoManager.getDefaultSafeHeart(getActivity());
            this.mConfig.setSafeHeartRateHigh((float) heartRate);
        }
        if (this.mConfig.isRemindSafeHeartRate()) {
            this.mInitIndex = (heartRate - 97) + 1;
        } else if (isFirst) {
            this.mInitIndex = (heartRate - 97) + 1;
        } else {
            this.mInitIndex = 0;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindSafeHeartRate(false);
            if (this.mConfig.getSafeHeartRateHigh() < 0.0f) {
                this.mConfig.setSafeHeartRateHigh(180.0f);
            }
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindSafeHeartRate(true);
        this.mConfig.setSafeHeartRateHigh((float) ((selectedRow[0] - 1) + 97));
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 0; i < 106; i++) {
            array.add(String.valueOf("" + (i + 97)));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        if (column != 0) {
            return null;
        }
        int safeHeartIndex = UserInfoManager.getDefaultSafeHeart(getActivity()) - 97;
        List<String> array = new ArrayList();
        array.add("");
        for (int i = 0; i < 106; i++) {
            if (i == safeHeartIndex) {
                array.add(getString(C0532R.string.settings_safe_heart_rate_hint));
            } else {
                array.add("");
            }
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.picker_title_target_heart_rate);
    }
}
