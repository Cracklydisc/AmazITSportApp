package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class V2TargetPacePickerFragment extends AbsV2SportPickerFragment {
    private static final int[] MINUTE_ROW = new int[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    private static final int[] SECOND_ROW = new int[]{0, 15, 30, 45};
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitMinuteIndex = 0;
    private int mInitSecondIndex = 0;

    public static V2TargetPacePickerFragment newInstance(BaseConfig config) {
        V2TargetPacePickerFragment fragment = new V2TargetPacePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        float speed = 0.0f;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        float pace = this.mConfig.getTargetPace();
        if (isNeedSpeed()) {
            if (Float.compare(this.mConfig.getTargetPace(), 0.0f) != 0) {
                speed = 1.0f / this.mConfig.getTargetPace();
            }
            int speedKmPerH = Math.round(3.6f * speed);
            if (this.mConfig.isRemindTargetPace()) {
                this.mInitMinuteIndex = ((speedKmPerH - 10) / 2) + 1;
                return;
            } else {
                this.mInitMinuteIndex = 0;
                return;
            }
        }
        float secondPerKm = (float) Math.round(1000.0f * pace);
        int minute = (int) (secondPerKm / 60.0f);
        int second = (int) (secondPerKm % 60.0f);
        if (pace < 0.0f) {
            this.mInitMinuteIndex = 3;
            this.mInitSecondIndex = 0;
        } else if (this.mConfig.isRemindTargetPace()) {
            int i;
            this.mInitMinuteIndex = Arrays.binarySearch(MINUTE_ROW, minute) + 1;
            this.mInitSecondIndex = Arrays.binarySearch(SECOND_ROW, second);
            if (this.mInitMinuteIndex > 1) {
                i = this.mInitMinuteIndex;
            } else {
                i = 1;
            }
            this.mInitMinuteIndex = i;
            if (this.mInitSecondIndex > 0) {
                i = this.mInitSecondIndex;
            } else {
                i = 0;
            }
            this.mInitSecondIndex = i;
        } else {
            this.mInitMinuteIndex = 0;
            this.mInitSecondIndex = 0;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (isNeedSpeed()) {
            if (selectedRow[0] == 0) {
                this.mConfig.setIsRemindTargetPace(false);
                this.mDataManager.setSportConfig(getActivity(), this.mConfig);
                return;
            }
            float pace = 1.0f / (((float) (((selectedRow[0] - 1) * 2) + 10)) / 3.6f);
            this.mConfig.setIsRemindTargetPace(true);
            this.mConfig.setTargetPace(pace);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        } else if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindTargetPace(false);
            if (this.mConfig.getTargetPace() < 0.0f) {
                this.mConfig.setTargetPace(((float) ((MINUTE_ROW[2] * 60) + SECOND_ROW[0])) / 1000.0f);
            }
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        } else {
            int secondPerKm = (MINUTE_ROW[selectedRow[0] - 1] * 60) + SECOND_ROW[selectedRow[1]];
            this.mConfig.setIsRemindTargetPace(true);
            this.mConfig.setTargetPace(((float) secondPerKm) / 1000.0f);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        }
    }

    protected String[] getArrayContent(int column) {
        List<String> array = new ArrayList();
        int i;
        if (isNeedSpeed()) {
            if (column != 0) {
                return null;
            }
            array.add(getString(C0532R.string.picker_default_nagetive_text));
            for (i = 0; i < 96; i++) {
                array.add(String.valueOf("" + ((i * 2) + 10)));
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column == 0) {
            array.add(getString(C0532R.string.picker_default_nagetive_text));
            for (int i2 : MINUTE_ROW) {
                array.add(String.valueOf("" + i2));
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column != 1) {
            return null;
        } else {
            for (int i22 : SECOND_ROW) {
                array.add(String.valueOf("" + i22));
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (column == 0) {
            return this.mInitMinuteIndex;
        }
        if (column == 1) {
            return this.mInitSecondIndex;
        }
        return 0;
    }

    protected String getTitle() {
        if (isNeedSpeed()) {
            if (UnitConvertUtils.isImperial()) {
                return getString(C0532R.string.picker_title_auto_pause_speed, new Object[]{getString(C0532R.string.km_imperial)});
            }
            return getString(C0532R.string.picker_title_auto_pause_speed, new Object[]{getString(C0532R.string.km_metric)});
        } else if (UnitConvertUtils.isImperial()) {
            return getString(C0532R.string.picker_title_auto_pause, new Object[]{getString(C0532R.string.km_imperial)});
        } else {
            return getString(C0532R.string.picker_title_auto_pause, new Object[]{getString(C0532R.string.km_metric)});
        }
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        return true;
    }

    private boolean isNeedSpeed() {
        return this.mConfig.getSportType() == 11;
    }
}
