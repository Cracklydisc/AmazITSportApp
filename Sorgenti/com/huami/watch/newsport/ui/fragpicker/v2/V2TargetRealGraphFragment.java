package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetRealGraphFragment extends AbsV2SportPickerFragment {
    private static final int[] CROSSING_CONFIG_GRAPH = new int[]{0, 2};
    private static final int[] INDOOR_RUNNING_CONFIG_GRAPH = new int[]{0, 1};
    private static final int[] MOUNTAINEER_CONFIG_GRAPH = new int[]{2, 0};
    private static final int[] OPEN_WATER_CONFIG_GRAPH = new int[]{5, 3};
    private static final int[] OUTDOOR_RIDING_CONFIG_GRAPH = new int[]{0, 2, 4};
    private static final int[] RUNNING_CONFIG_GRAPH = new int[]{0, 1};
    private static final int[] SKIING_CONFIG_GRAPH = new int[]{4, 2, 0};
    private static final int[] SWIM_CONFIG_GRAPH = new int[]{5, 3};
    private BaseConfig mConfig = null;
    private int[] mGraphArray = null;
    private int mSource = 0;

    public static V2TargetRealGraphFragment newInstance(BaseConfig config) {
        V2TargetRealGraphFragment fragment = new V2TargetRealGraphFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int graphType = this.mConfig.getRealGraphShownType();
        int sportType = this.mConfig.getSportType();
        switch (sportType) {
            case 1:
                this.mGraphArray = RUNNING_CONFIG_GRAPH;
                break;
            case 7:
                this.mGraphArray = CROSSING_CONFIG_GRAPH;
                break;
            case 8:
                this.mGraphArray = INDOOR_RUNNING_CONFIG_GRAPH;
                break;
            case 9:
                this.mGraphArray = OUTDOOR_RIDING_CONFIG_GRAPH;
                break;
            case 11:
                this.mGraphArray = SKIING_CONFIG_GRAPH;
                break;
            case 13:
                this.mGraphArray = MOUNTAINEER_CONFIG_GRAPH;
                break;
            case 14:
                this.mGraphArray = SWIM_CONFIG_GRAPH;
                break;
            case 15:
                this.mGraphArray = OPEN_WATER_CONFIG_GRAPH;
                break;
            default:
                throw new IllegalArgumentException("err sport type:" + sportType);
        }
        for (int i = 0; i < this.mGraphArray.length; i++) {
            if (this.mGraphArray[i] == graphType) {
                this.mSource = i;
                return;
            }
        }
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setRealGraphShownType(this.mGraphArray[selectedRow[0]]);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int i : this.mGraphArray) {
            int resId;
            switch (i) {
                case 0:
                    resId = C0532R.string.sport_main_heart_info_title;
                    break;
                case 1:
                    resId = C0532R.string.sport_main_pace_info_title;
                    break;
                case 2:
                    resId = C0532R.string.crossing_history_detail_absolute_altitude;
                    break;
                case 3:
                    resId = C0532R.string.swim_stroke_speed;
                    break;
                case 4:
                    resId = C0532R.string.running_speed_desc;
                    break;
                case 5:
                    resId = C0532R.string.sport_main_pace_info_title;
                    break;
                default:
                    resId = C0532R.string.sport_main_heart_info_title;
                    break;
            }
            array.add(getActivity().getString(resId));
        }
        return (String[]) array.toArray(new String[this.mGraphArray.length]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mSource;
    }

    protected String getTitle() {
        return getString(C0532R.string.settings_sport_real_graph);
    }
}
