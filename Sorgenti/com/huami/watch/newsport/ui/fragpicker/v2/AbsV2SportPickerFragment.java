package com.huami.watch.newsport.ui.fragpicker.v2;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.common.widget.NumberPickerView;
import com.huami.watch.common.widget.NumberPickerView.IOnButtonClickListener;
import com.huami.watch.common.widget.NumberView;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.ui.fragpicker.AbsSportPickerFragment.OnPickerSelectedListener;

public abstract class AbsV2SportPickerFragment extends Fragment {
    protected NumberView mColumnView1;
    protected NumberView mColumnView2;
    protected NumberView mColumnView3;
    protected OnPickerSelectedListener mListener;
    protected NumberPickerView mPickerView = null;

    class C07501 implements OnValueChangeListener {
        C07501() {
        }

        public void onValueChange(NumberView picker, int oldVal, int newVal) {
            AbsV2SportPickerFragment.this.hideOrShowOtherColumn(false);
            AbsV2SportPickerFragment.this.onPickerValueChange(0, picker, oldVal, newVal);
        }
    }

    class C07512 implements OnValueChangeListener {
        C07512() {
        }

        public void onValueChange(NumberView picker, int oldVal, int newVal) {
            AbsV2SportPickerFragment.this.onPickerValueChange(1, picker, oldVal, newVal);
        }
    }

    class C07523 implements OnValueChangeListener {
        C07523() {
        }

        public void onValueChange(NumberView picker, int oldVal, int newVal) {
            AbsV2SportPickerFragment.this.onPickerValueChange(2, picker, oldVal, newVal);
        }
    }

    class C07534 implements IOnButtonClickListener {
        C07534() {
        }

        public void onSelectedConfirm(NumberPickerView pickerView) {
            AbsV2SportPickerFragment.this.onSave(pickerView.getSelectIndex());
            AbsV2SportPickerFragment.this.mListener.onSelectFinish();
        }
    }

    protected abstract String[] getArrayContent(int i);

    protected abstract String[] getArraySubContent(int i);

    protected abstract int getInitIndex(int i);

    protected abstract String getTitle();

    protected abstract void onSave(int[] iArr);

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void onPickerValueChange(int column, NumberView picker, int oldVal, int newVal) {
    }

    protected int getWidthColumn(int column) {
        return 0;
    }

    protected boolean isSetWidthColumn() {
        return false;
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        return false;
    }

    protected int getColumnTextSize(int column) {
        return 30;
    }

    protected boolean isNeedHide(int column, int row) {
        if (column == 0 && row == 0) {
            return true;
        }
        return false;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(C0532R.layout.fragment_multi_sport_picker, container, false);
        this.mPickerView = (NumberPickerView) root.findViewById(C0532R.id.picker);
        this.mPickerView.setTitle(getTitle());
        this.mPickerView.setPickerData(getArrayContent(0), getArrayContent(1), getArrayContent(2), getArraySubContent(0), getArraySubContent(1), getArraySubContent(2));
        this.mPickerView.setCurrentIndex(getInitIndex(0), getInitIndex(1), getInitIndex(2));
        this.mColumnView1 = this.mPickerView.get1ColumnView();
        this.mColumnView2 = this.mPickerView.get2ColumnView();
        this.mColumnView3 = this.mPickerView.get3ColumnView();
        hideOrShowOtherColumn(true);
        this.mPickerView.setTextSizeSelectColumn1(getColumnTextSize(0));
        this.mPickerView.setTextSizeSelectColumn2(getColumnTextSize(1));
        this.mPickerView.setTextSizeSelectColumn3(getColumnTextSize(2));
        this.mPickerView.set1OnchangedLisenter(new C07501());
        this.mPickerView.set2OnchangedLisenter(new C07512());
        this.mPickerView.set3OnchangedLisenter(new C07523());
        if (isSetWidthColumn()) {
            this.mPickerView.setWidthColumn1(getWidthColumn(0));
            this.mPickerView.setWidthColumn2(getWidthColumn(1));
            this.mPickerView.setWidthColumn3(getWidthColumn(2));
        }
        this.mPickerView.setOnButtonClickListener(new C07534());
        return root;
    }

    private void hideOrShowOtherColumn(final boolean isFirst) {
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (AbsV2SportPickerFragment.this.mPickerView != null && AbsV2SportPickerFragment.this.hasClosedBtnAndMoreColumns()) {
                    int firstSelected;
                    if (isFirst) {
                        firstSelected = AbsV2SportPickerFragment.this.getInitIndex(0);
                    } else {
                        firstSelected = AbsV2SportPickerFragment.this.mPickerView.getSelectIndex()[0];
                    }
                    if (AbsV2SportPickerFragment.this.isNeedHide(0, firstSelected)) {
                        if (AbsV2SportPickerFragment.this.mColumnView2 != null) {
                            AbsV2SportPickerFragment.this.mColumnView2.setVisibility(8);
                        }
                        if (AbsV2SportPickerFragment.this.mColumnView3 != null) {
                            AbsV2SportPickerFragment.this.mColumnView3.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    if (AbsV2SportPickerFragment.this.mColumnView2 != null) {
                        AbsV2SportPickerFragment.this.mColumnView2.setVisibility(0);
                    }
                    if (AbsV2SportPickerFragment.this.mColumnView3 != null) {
                        AbsV2SportPickerFragment.this.mColumnView3.setVisibility(0);
                    }
                }
            }
        });
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnPickerSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnPickerSelectedListener");
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mListener = null;
        this.mPickerView.set1OnchangedLisenter(null);
        this.mPickerView.set2OnchangedLisenter(null);
        this.mPickerView.set3OnchangedLisenter(null);
    }
}
