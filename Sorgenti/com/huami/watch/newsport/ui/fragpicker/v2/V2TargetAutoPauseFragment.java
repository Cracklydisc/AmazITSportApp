package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2TargetAutoPauseFragment extends AbsV2SportPickerFragment {
    private boolean isSpeedFlag = false;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitMinuteIndex = 0;
    private int mInitSecondIndex = 0;

    public static V2TargetAutoPauseFragment newInstance(BaseConfig config) {
        V2TargetAutoPauseFragment fragment = new V2TargetAutoPauseFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.isSpeedFlag = this.mConfig.getSportType() == 9;
        if (!this.mConfig.isAutoPause()) {
            this.mInitMinuteIndex = 0;
            this.mInitSecondIndex = 0;
        } else if (this.isSpeedFlag) {
            this.mInitMinuteIndex = Math.round(this.mConfig.getAutoPausePace() * 3.6f) + 1;
            if (this.mInitMinuteIndex >= 12) {
                this.mInitMinuteIndex = 11;
            }
        } else {
            float secondPerKm = (float) Math.round(1000.0f * this.mConfig.getAutoPausePace());
            int minute = (int) (secondPerKm / 60.0f);
            int second = (int) (secondPerKm % 60.0f);
            if (minute < 5) {
                this.mInitMinuteIndex = 1;
                this.mInitSecondIndex = 0;
                return;
            }
            this.mInitMinuteIndex = minute - 3;
            this.mInitSecondIndex = second;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsAutoPause(false);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        } else if (selectedRow[0] == 1) {
            this.mConfig.setIsAutoPause(true);
            this.mConfig.setAutoPausePace(0.0f);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        } else {
            this.mConfig.setIsAutoPause(true);
            if (this.isSpeedFlag) {
                this.mConfig.setAutoPausePace(((float) (selectedRow[0] - 1)) / 3.6f);
                this.mDataManager.setSportConfig(getActivity(), this.mConfig);
                return;
            }
            this.mConfig.setAutoPausePace(((float) (((selectedRow[0] + 3) * 60) + selectedRow[1])) / 1000.0f);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        }
    }

    protected String[] getArrayContent(int column) {
        List<String> content = new ArrayList();
        int i;
        if (this.isSpeedFlag) {
            if (column != 0) {
                return null;
            }
            content.add(getString(C0532R.string.picker_default_nagetive_text));
            content.add(getString(C0532R.string.settings_content_stand_still));
            for (i = 0; i < 10; i++) {
                content.add(String.valueOf("" + (i + 1)));
            }
            return (String[]) content.toArray(new String[content.size()]);
        } else if (column == 0) {
            content.add(getString(C0532R.string.picker_default_nagetive_text));
            content.add(getString(C0532R.string.settings_content_stand_still));
            for (i = 0; i < 10; i++) {
                content.add(String.valueOf("" + (i + 5)));
            }
            return (String[]) content.toArray(new String[content.size()]);
        } else if (column != 1) {
            return null;
        } else {
            for (i = 0; i < 60; i++) {
                content.add(String.valueOf("" + i));
            }
            return (String[]) content.toArray(new String[content.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        switch (column) {
            case 0:
                return this.mInitMinuteIndex;
            case 1:
                return this.mInitSecondIndex;
            default:
                return 0;
        }
    }

    protected boolean isSetWidthColumn() {
        if (this.isSpeedFlag) {
            return false;
        }
        return true;
    }

    protected int getWidthColumn(int column) {
        if (column == 0) {
            return 130;
        }
        if (column == 1) {
            return 120;
        }
        return 0;
    }

    protected String getTitle() {
        if (UnitConvertUtils.isImperial()) {
            if (this.isSpeedFlag) {
                return getString(C0532R.string.picker_title_auto_pause_speed, new Object[]{getString(C0532R.string.km_imperial)});
            }
            return getString(C0532R.string.picker_title_auto_pause, new Object[]{getString(C0532R.string.km_imperial)});
        } else if (this.isSpeedFlag) {
            return getString(C0532R.string.picker_title_auto_pause_speed, new Object[]{getString(C0532R.string.km_metric)});
        } else {
            return getString(C0532R.string.picker_title_auto_pause, new Object[]{getString(C0532R.string.km_metric)});
        }
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        return true;
    }

    protected boolean isNeedHide(int column, int row) {
        if (column == 0 && (row == 0 || row == 1)) {
            return true;
        }
        return false;
    }
}
