package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2RemindKmDistancePickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    public static V2RemindKmDistancePickerFragment newInstance(BaseConfig config) {
        V2RemindKmDistancePickerFragment fragment = new V2RemindKmDistancePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mInitIndex = this.mConfig.getRemindKm();
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindPerKM(false);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindPerKM(true);
        this.mConfig.setRemindKm(selectedRow[0]);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column > 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 1; i <= 50; i++) {
            array.add(String.valueOf("" + i));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        if (UnitConvertUtils.isImperial()) {
            return getString(C0532R.string.settings_item_title_mi_remind);
        }
        return getString(C0532R.string.settings_item_title_km_remind);
    }
}
