package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2BackgroundPickerFragment extends AbsV2SportPickerFragment {
    private static final int[] mRes = new int[]{C0532R.string.settings_sport_bg_type_normal, C0532R.string.settings_sport_bg_type_white};
    private int mBgType = 0;
    private BaseConfig mConfig = null;

    public static V2BackgroundPickerFragment newInstance(BaseConfig config) {
        V2BackgroundPickerFragment fragment = new V2BackgroundPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mBgType = this.mConfig.getBgType();
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setBgType(selectedRow[0]);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int string : mRes) {
            array.add(getString(string));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mBgType;
    }

    protected String getTitle() {
        return getString(C0532R.string.settings_sport_bg_type);
    }
}
