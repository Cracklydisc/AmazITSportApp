package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetCaloriePickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    public static V2TargetCaloriePickerFragment newInstance(BaseConfig config) {
        V2TargetCaloriePickerFragment fragment = new V2TargetCaloriePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        if (this.mConfig.getTargetCalorie() < 0) {
            this.mInitIndex = 3;
        } else if (this.mConfig.isRemindCalorie()) {
            this.mInitIndex = Math.round((float) (this.mConfig.getTargetCalorie() / 25));
        } else {
            this.mInitIndex = 0;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindCalorie(false);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindCalorie(true);
        this.mConfig.setTargetCalorie(selectedRow[0] * 25);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        List<String> array = new ArrayList();
        if (column != 0) {
            return null;
        }
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 0; i < 80; i++) {
            array.add(String.valueOf("" + ((i + 1) * 25)));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.picker_title_target_calorie);
    }
}
