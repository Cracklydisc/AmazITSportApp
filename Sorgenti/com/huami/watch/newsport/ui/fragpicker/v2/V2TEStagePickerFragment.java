package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import android.util.SparseArray;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.xmlparser.bean.TERemindStage;
import com.huami.watch.newsport.xmlparser.utils.SAXUtils;
import java.util.ArrayList;
import java.util.List;

public class V2TEStagePickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;
    private SparseArray<TERemindStage> mTESettingSparseArray;

    public static V2TEStagePickerFragment newInstance(BaseConfig config) {
        V2TEStagePickerFragment fragment = new V2TEStagePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        if (this.mConfig.getTargetTEStage() <= 0) {
            this.mInitIndex = 0;
        }
        this.mTESettingSparseArray = SAXUtils.getTERemindSettingsFromXml(getActivity());
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindTE(true);
        this.mConfig.setTargetTEStage(selectedRow[0]);
        this.mConfig.setTargetTE(((float) ((TERemindStage) this.mTESettingSparseArray.get(selectedRow[0])).getTe()) / 10.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int i = 0; i < 8; i++) {
            array.add(((TERemindStage) this.mTESettingSparseArray.get(i)).getDesc());
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int i = 0; i < 8; i++) {
            r5 = new Object[2];
            r5[0] = String.format("%.1f", new Object[]{Float.valueOf(((float) ((TERemindStage) this.mTESettingSparseArray.get(i)).getTe()) / 10.0f)});
            r5[1] = String.valueOf(((TERemindStage) this.mTESettingSparseArray.get(i)).getTimeCost() / 60);
            array.add(getString(C0532R.string.sport_setting_te_stage_content, r5));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.sport_setting_select_te_stage_title);
    }

    protected int getColumnTextSize(int column) {
        if (UnitConvertUtils.isZh()) {
            return 24;
        }
        return 20;
    }
}
