package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2TargetDistancePickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;
    private boolean mIsSwimMode = false;

    public static V2TargetDistancePickerFragment newInstance(BaseConfig config) {
        V2TargetDistancePickerFragment fragment = new V2TargetDistancePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mIsSwimMode = SportType.isSwimMode(this.mConfig.getSportType());
        if (this.mConfig.getTargetDistance() < 0) {
            this.mInitIndex = 4;
        } else if (!this.mConfig.isRemindTargetDistance()) {
            this.mInitIndex = 0;
        } else if (this.mIsSwimMode) {
            this.mInitIndex = ((this.mConfig.getTargetDistance() - 500) / 100) + 1;
            if (this.mInitIndex < 0) {
                this.mInitIndex = 0;
            }
        } else {
            this.mInitIndex = Math.round((float) (this.mConfig.getTargetDistance() / 1000));
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindTargetDistance(false);
            if (this.mConfig.getTargetDistance() < 0) {
                if (this.mIsSwimMode) {
                    this.mConfig.setTargetDistance(4000);
                } else {
                    this.mConfig.setTargetDistance(1000);
                }
            }
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindTargetDistance(true);
        if (this.mIsSwimMode) {
            this.mConfig.setTargetDistance(((selectedRow[0] - 1) * 100) + 500);
        } else {
            this.mConfig.setTargetDistance(selectedRow[0] * 1000);
        }
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column > 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        int i;
        if (this.mIsSwimMode) {
            for (i = 0; i <= 95; i++) {
                array.add(String.valueOf("" + ((i * 100) + 500)));
            }
        } else {
            for (i = 1; i <= 100; i++) {
                array.add(String.valueOf("" + i));
            }
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        if (this.mIsSwimMode) {
            if (this.mConfig.getUnit() == 0) {
                return getString(C0532R.string.picker_title_target_dis, new Object[]{getString(C0532R.string.metre_metric)});
            }
            return getString(C0532R.string.picker_title_target_dis, new Object[]{getString(C0532R.string.swim_unit)});
        } else if (UnitConvertUtils.isImperial()) {
            return getString(C0532R.string.picker_title_target_dis, new Object[]{getString(C0532R.string.km_imperial)});
        } else {
            return getString(C0532R.string.picker_title_target_dis, new Object[]{getString(C0532R.string.km_metric)});
        }
    }
}
