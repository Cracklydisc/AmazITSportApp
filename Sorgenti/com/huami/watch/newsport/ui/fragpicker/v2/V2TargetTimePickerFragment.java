package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.common.widget.NumberView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetTimePickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitHourIndex = 0;
    private int mInitMinuteIndex = 0;

    public static V2TargetTimePickerFragment newInstance(BaseConfig config) {
        V2TargetTimePickerFragment fragment = new V2TargetTimePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        long targetTime = this.mConfig.getTargetTimeCost();
        int totalMinute = (int) (targetTime / 60000);
        if (targetTime < 0) {
            this.mInitHourIndex = 1;
            this.mInitMinuteIndex = 30;
        } else if (this.mConfig.isRemindTargetTimeCost()) {
            this.mInitHourIndex = (totalMinute / 60) + 1;
            this.mInitMinuteIndex = totalMinute % 60;
        } else {
            this.mInitHourIndex = 0;
            this.mInitMinuteIndex = 1;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindTargetTimeCost(false);
            if (this.mConfig.getTargetTimeCost() < 0) {
                this.mConfig.setTargetTimeCost(60000);
            }
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindTargetTimeCost(true);
        this.mConfig.setTargetTimeCost((long) (((selectedRow[0] - 1) * 3600000) + (selectedRow[1] * 60000)));
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        List<String> array = new ArrayList();
        int i;
        if (column == 0) {
            array.add(getString(C0532R.string.picker_default_nagetive_text));
            for (i = 0; i < 31; i++) {
                array.add(String.valueOf("" + i));
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column != 1) {
            return null;
        } else {
            for (i = 0; i < 60; i++) {
                array.add(String.valueOf("" + i));
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (column == 0) {
            return this.mInitHourIndex;
        }
        if (column == 1) {
            return this.mInitMinuteIndex;
        }
        return 0;
    }

    protected String getTitle() {
        return getString(C0532R.string.picker_title_target_time);
    }

    protected void onPickerValueChange(final int column, final NumberView picker, int oldVal, final int newVal) {
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (column == 1) {
                    if (newVal == 0 && V2TargetTimePickerFragment.this.mPickerView.getSelectIndex()[0] == 1) {
                        picker.smoothScrollToValue(1);
                    }
                } else if (column == 0 && newVal == 1 && V2TargetTimePickerFragment.this.mPickerView.getSelectIndex()[1] == 0) {
                    V2TargetTimePickerFragment.this.mPickerView.get2ColumnView().smoothScrollToValue(1);
                }
            }
        });
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        return true;
    }
}
