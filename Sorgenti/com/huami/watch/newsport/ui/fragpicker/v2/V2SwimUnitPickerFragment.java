package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2SwimUnitPickerFragment extends AbsV2SportPickerFragment {
    private static final int[] mRes = new int[]{C0532R.string.metre_metric, C0532R.string.swim_unit};
    private BaseConfig mConfig = null;
    private int mUnit = 0;

    public static V2SwimUnitPickerFragment newInstance(BaseConfig config) {
        V2SwimUnitPickerFragment fragment = new V2SwimUnitPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mUnit = this.mConfig.getUnit();
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setUnit(selectedRow[0]);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        for (int string : mRes) {
            array.add(getString(string));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mUnit;
    }

    protected String getTitle() {
        return null;
    }
}
