package com.huami.watch.newsport.ui.fragpicker.v2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.widget.NumberView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2IndoorPickerFragment extends AbsV2SportPickerFragment {
    private int firstRowCount;
    private int initFirstCount;
    private int initSecCount;
    private int mMinValue = 0;
    private double sportData;

    public static V2IndoorPickerFragment newInstance(double distance) {
        V2IndoorPickerFragment fragment = new V2IndoorPickerFragment();
        Bundle argument = new Bundle();
        argument.putDouble("distance_picker", distance);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.sportData = getArguments().getDouble("distance_picker", 0.0d);
        this.sportData = NumeriConversionUtils.getDoubleValue(String.valueOf(this.sportData), 2);
        this.firstRowCount = getRowCount(this.sportData);
        this.initFirstCount = (((int) this.sportData) - this.mMinValue) + 1;
        this.initSecCount = (int) Math.round((this.sportData % 1.0d) / 0.01d);
    }

    private int getRowCount(double number) {
        int end = (int) NumeriConversionUtils.getMulResult(String.valueOf(number), String.valueOf(1.5d));
        int start = (int) NumeriConversionUtils.getMulResult(String.valueOf(number), String.valueOf(0.5d));
        this.mMinValue = start;
        return (end - start) + 1;
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            Intent result = new Intent();
            Log.d("sportCorrect", "close,  dis = " + this.sportData);
            result.putExtra("sportNumber", this.sportData);
            getActivity().setResult(-1, result);
            getActivity().finish();
            getActivity().overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
            return;
        }
        double dis = ((double) ((this.mMinValue + selectedRow[0]) - 1)) + NumeriConversionUtils.getDoubleValue(DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceFromMileOrKmtokm(((double) selectedRow[1]) * 0.01d), true));
        result = new Intent();
        Log.d("sportCorrect", "  dis = " + dis);
        result.putExtra("sportNumber", dis);
        getActivity().setResult(-1, result);
        getActivity().finish();
        getActivity().overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
    }

    protected String[] getArrayContent(int column) {
        List<String> array;
        int i;
        if (column == 0) {
            array = new ArrayList();
            array.add(getString(C0532R.string.picker_default_nagetive_text));
            for (i = 0; i < this.firstRowCount; i++) {
                array.add(String.valueOf(this.mMinValue + i));
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column != 1) {
            return null;
        } else {
            array = new ArrayList();
            for (i = 0; i < 100; i++) {
                StringBuilder builder = new StringBuilder();
                builder.append(".").append(String.format("%02d", new Object[]{Integer.valueOf(i)}));
                array.add(builder.toString());
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (column == 0) {
            return this.initFirstCount;
        }
        if (column == 1) {
            return this.initSecCount;
        }
        return this.initSecCount;
    }

    protected String getTitle() {
        if (UnitConvertUtils.isImperial()) {
            return getString(C0532R.string.picker_title_fix_dis, new Object[]{getString(C0532R.string.km_imperial)});
        }
        return getString(C0532R.string.picker_title_fix_dis, new Object[]{getString(C0532R.string.km_metric)});
    }

    protected void onPickerValueChange(final int column, final NumberView picker, int oldVal, final int newVal) {
        Global.getGlobalUIHandler().post(new Runnable() {
            public void run() {
                if (V2IndoorPickerFragment.this.mMinValue != 0) {
                    return;
                }
                if (column == 1) {
                    if (newVal == 0 && V2IndoorPickerFragment.this.mPickerView.getSelectIndex()[0] == 1) {
                        picker.smoothScrollToValue(1);
                    }
                } else if (column == 0 && newVal == 1 && V2IndoorPickerFragment.this.mPickerView.getSelectIndex()[1] == 0) {
                    V2IndoorPickerFragment.this.mPickerView.get2ColumnView().smoothScrollToValue(1);
                }
            }
        });
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        return true;
    }
}
