package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetTripsPickerFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    public static V2TargetTripsPickerFragment newInstance(BaseConfig config) {
        V2TargetTripsPickerFragment fragment = new V2TargetTripsPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mInitIndex = this.mConfig.getTargetTrips();
        if (!this.mConfig.isRemindTargetTrips()) {
            this.mInitIndex = 0;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindTargetTrips(false);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindTargetTrips(true);
        this.mConfig.setTargetTrips(selectedRow[0]);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        if (column != 0) {
            return null;
        }
        List<String> array = new ArrayList();
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 0; i < 100; i++) {
            array.add("" + (i + 1));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return null;
    }
}
