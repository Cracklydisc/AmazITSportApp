package com.huami.watch.newsport.ui.fragpicker.v2;

import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import com.huami.watch.common.widget.NumberView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.List;

public class V2TargetAutoLapDistancePickerFragment extends AbsV2SportPickerFragment {
    private static final String TAG = V2TargetAutoLapDistancePickerFragment.class.getName();
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mDefaultFirstRowCount = 102;
    private int mDefaultSecRowCount = 10;
    private int mInitFirstIndex = 0;
    private int mInitSecIndex = 0;

    public static V2TargetAutoLapDistancePickerFragment newInstance(BaseConfig config) {
        V2TargetAutoLapDistancePickerFragment fragment = new V2TargetAutoLapDistancePickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int sportType = this.mConfig.getSportType();
        if (isModeSameWithRunning()) {
            this.mDefaultFirstRowCount = 101;
            this.mDefaultSecRowCount = 10;
        } else if (this.mConfig.getSportType() == 9) {
            this.mDefaultFirstRowCount = 100;
        } else if (this.mConfig.getSportType() == 14) {
            this.mDefaultFirstRowCount = 1000 / this.mConfig.getTargetSwimLength();
        }
        int targetDis = (int) this.mConfig.getDistanceAutoLap();
        if (this.mConfig.isOpenAutoLapRecord()) {
            if (targetDis < 0) {
                this.mInitFirstIndex = 1;
                this.mInitSecIndex = 1;
            } else if (sportType == 14) {
                this.mInitFirstIndex = targetDis / this.mConfig.getTargetSwimLength();
            } else if (isModeSameWithRunning()) {
                this.mInitFirstIndex = (targetDis / 1000) + 1;
                this.mInitSecIndex = (targetDis % 1000) / 100;
            } else {
                this.mInitFirstIndex = Math.round(this.mConfig.getDistanceAutoLap() / 1000.0f);
            }
        } else if (!isFirstSettings()) {
            this.mInitFirstIndex = 0;
        } else if (isModeSameWithRunning()) {
            this.mInitFirstIndex = 1;
            this.mInitSecIndex = 4;
        } else if (sportType == 9) {
            this.mInitFirstIndex = 5;
        } else {
            this.mInitFirstIndex = 0;
        }
    }

    private boolean isModeSameWithRunning() {
        return !SportType.isSwimMode(this.mConfig.getSportType());
    }

    protected void onSave(int[] selectedRow) {
        setFirstSetDefault(false);
        if (selectedRow[0] == 0) {
            this.mConfig.setIsOpenAutoLapRecord(false);
            this.mDataManager.setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsOpenAutoLapRecord(true);
        if (isModeSameWithRunning()) {
            this.mConfig.setDistanceAutoLap((float) (((selectedRow[0] - 1) * 1000) + (selectedRow[1] * 100)));
        } else if (this.mConfig.getSportType() == 14) {
            this.mConfig.setDistanceAutoLap((float) (selectedRow[0] * this.mConfig.getTargetSwimLength()));
        } else {
            this.mConfig.setDistanceAutoLap((float) (selectedRow[0] * 1000));
        }
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected String[] getArrayContent(int column) {
        List<String> array;
        int i;
        int km;
        if (column == 0) {
            array = new ArrayList();
            array.add(getString(C0532R.string.picker_default_nagetive_text));
            for (i = 0; i < this.mDefaultFirstRowCount; i++) {
                if (isModeSameWithRunning()) {
                    km = i;
                    if (km < 10) {
                        array.add("0" + km);
                    } else {
                        array.add("" + km);
                    }
                } else if (this.mConfig.getSportType() == 14) {
                    array.add("" + (this.mConfig.getTargetSwimLength() + (this.mConfig.getTargetSwimLength() * i)));
                } else {
                    km = i + 1;
                    if (km < 10) {
                        array.add("0" + km);
                    } else {
                        array.add("" + km);
                    }
                }
            }
            return (String[]) array.toArray(new String[array.size()]);
        } else if (column != 1 || !isModeSameWithRunning()) {
            return null;
        } else {
            array = new ArrayList();
            for (i = 0; i < this.mDefaultSecRowCount; i++) {
                km = i;
                StringBuilder builder = new StringBuilder();
                builder.append(".");
                builder.append(i);
                array.add(builder.toString());
            }
            return (String[]) array.toArray(new String[array.size()]);
        }
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        if (column == 0) {
            return this.mInitFirstIndex;
        }
        if (column == 1) {
            return this.mInitSecIndex;
        }
        return 0;
    }

    protected String getTitle() {
        if (this.mConfig.getUnit() != 0) {
            return getString(C0532R.string.picker_title_auto_lap, new Object[]{getString(C0532R.string.swim_unit)});
        } else if (this.mConfig.getSportType() == 14) {
            return getString(C0532R.string.picker_title_auto_lap, new Object[]{getString(C0532R.string.metre_metric)});
        } else if (UnitConvertUtils.isImperial()) {
            return getString(C0532R.string.picker_title_auto_lap, new Object[]{getString(C0532R.string.km_imperial)});
        } else {
            return getString(C0532R.string.picker_title_auto_lap, new Object[]{getString(C0532R.string.km_metric)});
        }
    }

    protected void onPickerValueChange(final int column, final NumberView picker, int oldVal, final int newVal) {
        if (isModeSameWithRunning()) {
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    if (column == 1) {
                        if (newVal == 0 && V2TargetAutoLapDistancePickerFragment.this.mPickerView.getSelectIndex()[0] == 1) {
                            picker.smoothScrollToValue(1);
                        }
                    } else if (column == 0 && newVal == 1 && V2TargetAutoLapDistancePickerFragment.this.mPickerView.getSelectIndex()[1] == 0) {
                        V2TargetAutoLapDistancePickerFragment.this.mPickerView.get2ColumnView().smoothScrollToValue(1);
                    }
                }
            });
        }
    }

    protected boolean hasClosedBtnAndMoreColumns() {
        if (isModeSameWithRunning()) {
            return true;
        }
        return false;
    }

    private boolean isFirstSettings() {
        boolean isShowTips = getActivity().getPreferences(0).getBoolean("is_first_shown_default_auto_lap", true);
        LogUtil.m9i(true, TAG, "isFirstSettings, " + isShowTips);
        return isShowTips;
    }

    private void setFirstSetDefault(boolean isShowTips) {
        LogUtil.m9i(true, TAG, "setFirstSetDefault, " + isShowTips);
        Editor editor = getActivity().getPreferences(0).edit();
        editor.putBoolean("is_first_shown_default_auto_lap", isShowTips);
        editor.commit();
    }
}
