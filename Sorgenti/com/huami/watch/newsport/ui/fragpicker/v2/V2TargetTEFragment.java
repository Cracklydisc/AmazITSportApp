package com.huami.watch.newsport.ui.fragpicker.v2;

import android.os.Bundle;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.ArrayList;
import java.util.List;

public class V2TargetTEFragment extends AbsV2SportPickerFragment {
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    public static V2TargetTEFragment newInstance(BaseConfig config) {
        V2TargetTEFragment fragment = new V2TargetTEFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        boolean isOpenTE = this.mConfig.isRemindTE();
        float targetTE = this.mConfig.getTargetTE();
        if (isOpenTE) {
            this.mInitIndex = Math.round((targetTE - 1.0f) / 0.1f) + 1;
        } else {
            this.mInitIndex = 0;
        }
    }

    protected void onSave(int[] selectedRow) {
        if (selectedRow[0] == 0) {
            this.mConfig.setIsRemindTE(false);
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
            return;
        }
        this.mConfig.setIsRemindTE(true);
        this.mConfig.setTargetTE((((float) (selectedRow[0] - 1)) * 0.1f) + 1.0f);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected String[] getArrayContent(int column) {
        List<String> array = new ArrayList();
        if (column != 0) {
            return null;
        }
        array.add(getString(C0532R.string.picker_default_nagetive_text));
        for (int i = 0; i < 40; i++) {
            array.add(String.format("%.1f", new Object[]{Float.valueOf(1.0f + (((float) i) * 0.1f))}));
        }
        return (String[]) array.toArray(new String[array.size()]);
    }

    protected String[] getArraySubContent(int column) {
        return null;
    }

    protected int getInitIndex(int column) {
        return this.mInitIndex;
    }

    protected String getTitle() {
        return getString(C0532R.string.picker_title_target_te);
    }
}
