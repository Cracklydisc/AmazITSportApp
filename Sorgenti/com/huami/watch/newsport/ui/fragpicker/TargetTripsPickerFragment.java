package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class TargetTripsPickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    private class TargetTripsPickerAdatper extends AbsPickerViewAdapter {
        public TargetTripsPickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return 100;
        }

        public float getColumnWidth(int column) {
            return 200.0f;
        }

        public String getDisplayString(int row, int column) {
            return "" + (row + 1);
        }

        public int getInitRow(int column) {
            return TargetTripsPickerFragment.this.mInitIndex;
        }

        public float getTextSize() {
            return 50.0f;
        }

        public boolean hideNegativeButton() {
            return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mInitIndex = this.mConfig.getTargetTrips() - 1;
        if (!this.mConfig.isRemindTargetTrips()) {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new TargetTripsPickerAdatper(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindTargetTrips(true);
        this.mConfig.setTargetTrips(selectedRow[0] + 1);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
    }

    protected void onClose() {
        this.mConfig.setIsRemindTargetTrips(false);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
