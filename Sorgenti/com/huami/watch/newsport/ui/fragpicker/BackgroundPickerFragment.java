package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class BackgroundPickerFragment extends AbsSportPickerFragment {
    private static final int[] mRes = new int[]{C0532R.string.settings_sport_bg_type_normal, C0532R.string.settings_sport_bg_type_white};
    private SourceAdapter mAdapter = null;
    private int mBgType = 0;
    private BaseConfig mConfig = null;

    private class SourceAdapter extends AbsPickerViewAdapter {
        public SourceAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int column) {
            return BackgroundPickerFragment.this.mBgType;
        }

        public int rowCount(int column) {
            return 2;
        }

        public String getDisplayString(int row, int column) {
            if (row >= 0 && row <= 2) {
                return BackgroundPickerFragment.this.getActivity().getString(BackgroundPickerFragment.mRes[row]);
            }
            throw new IllegalArgumentException("row is err: " + row);
        }

        public float getColumnWidth(int column) {
            return 200.0f;
        }

        public float getTextSize() {
            return 32.0f;
        }

        public boolean hideNegativeButton() {
            return true;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.mBgType = this.mConfig.getBgType();
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new SourceAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setBgType(selectedRow[0]);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
