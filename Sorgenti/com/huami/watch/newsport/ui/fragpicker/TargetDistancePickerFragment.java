package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class TargetDistancePickerFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter = null;
    private BaseConfig mConfig = null;
    private DataManager mDataManager = DataManager.getInstance();
    private int mInitIndex = 0;

    private class TargetDistancePickerAdatper extends AbsPickerViewAdapter {
        public TargetDistancePickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int rowCount(int column) {
            return 100;
        }

        public float getColumnWidth(int column) {
            return 135.0f;
        }

        public String getDisplayString(int row, int column) {
            int km = row + 1;
            if (km < 10) {
                return "0" + km;
            }
            return "" + km;
        }

        public float getTextSize() {
            return (super.getTextSize() * 3.0f) / 5.0f;
        }

        public String getUnit(int column) {
            if (UnitConvertUtils.isImperial()) {
                return this.mContext.getString(C0532R.string.km_imperial);
            }
            return this.mContext.getString(C0532R.string.km_metric);
        }

        public int getInitRow(int column) {
            return TargetDistancePickerFragment.this.mInitIndex;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        if (this.mConfig.getTargetDistance() < 0) {
            this.mInitIndex = 3;
        } else if (this.mConfig.isRemindTargetDistance()) {
            this.mInitIndex = Math.round((float) (this.mConfig.getTargetDistance() / 1000)) - 1;
        } else {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new TargetDistancePickerAdatper(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindTargetDistance(true);
        this.mConfig.setTargetDistance((selectedRow[0] + 1) * 1000);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onClose() {
        this.mConfig.setIsRemindTargetDistance(false);
        this.mDataManager.setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
