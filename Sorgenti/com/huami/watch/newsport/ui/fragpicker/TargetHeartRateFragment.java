package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import java.util.Calendar;

public class TargetHeartRateFragment extends AbsSportPickerFragment {
    private AbsPickerViewAdapter mAdapter;
    private BaseConfig mConfig = null;
    private int mInitIndex = 0;

    class HeartRatePickerAdapter extends AbsPickerViewAdapter {
        public HeartRatePickerAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int column) {
            return TargetHeartRateFragment.this.mInitIndex;
        }

        public int rowCount(int column) {
            return 106;
        }

        public float getColumnWidth(int column) {
            return 135.0f;
        }

        public String getDisplayString(int row, int column) {
            return String.valueOf(row + 97);
        }

        public boolean showItemHint() {
            return true;
        }

        public int[] getItemHintDisplayPosition() {
            return new int[]{UserInfoManager.getDefaultSafeHeart(this.mContext) - 97, 0};
        }

        public String getItemHintDisplayString() {
            return TargetHeartRateFragment.this.getActivity().getString(C0532R.string.settings_safe_heart_rate_hint);
        }

        public String getUnit(int column) {
            return TargetHeartRateFragment.this.getActivity().getString(C0532R.string.settings_safe_heart_rate_unit);
        }

        public float getHintTextSize() {
            return 12.0f;
        }

        public float getTextSize() {
            return 54.0f;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        int userAge;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int heartRate = (int) this.mConfig.getSafeHeartRateHigh();
        boolean isFirst = heartRate <= 0;
        if (heartRate < 0) {
            heartRate = UserInfoManager.getDefaultSafeHeart(getActivity());
            this.mConfig.setSafeHeartRateHigh((float) heartRate);
        } else if (heartRate < 97 || heartRate > 202) {
            heartRate = 180;
            this.mConfig.setSafeHeartRateHigh((float) 180);
        }
        UserInfo userInfo = UserInfoManager.getInstance().getUserInfo(getActivity());
        int currentYear = Calendar.getInstance().get(1);
        if (userInfo == null || userInfo.getYear() < 0) {
            userAge = 26;
        } else {
            userAge = currentYear - userInfo.getYear();
        }
        if (this.mConfig.isRemindSafeHeartRate()) {
            this.mInitIndex = heartRate - 97;
        } else if (isFirst) {
            this.mInitIndex = (220 - userAge) - 97;
        } else {
            this.mInitIndex = -1;
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new HeartRatePickerAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        this.mConfig.setIsRemindSafeHeartRate(true);
        this.mConfig.setSafeHeartRateHigh((float) (selectedRow[0] + 97));
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onClose() {
        this.mConfig.setIsRemindSafeHeartRate(false);
        if (this.mConfig.getSafeHeartRateHigh() < 0.0f) {
            this.mConfig.setSafeHeartRateHigh(180.0f);
        }
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
