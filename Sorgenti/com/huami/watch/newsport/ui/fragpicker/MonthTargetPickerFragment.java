package com.huami.watch.newsport.ui.fragpicker;

import android.content.Context;
import android.os.Bundle;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.utils.UnitConvertUtils;

public class MonthTargetPickerFragment extends AbsSportPickerFragment {
    private BaseConfig mConfig = null;
    private int mIndexDis = -1;
    private int mIndexHour = -1;
    private int mIndexMin = -1;
    private int mSportType = -1;

    private class TargetMonthPickerAdatper extends AbsPickerViewAdapter {
        public TargetMonthPickerAdatper(Context context) {
            super(context);
        }

        public int columnCount() {
            return 3;
        }

        public int rowCount(int column) {
            if (column == 0) {
                return 300;
            }
            if (column == 1) {
                return 24;
            }
            return 60;
        }

        public float getColumnWidth(int column) {
            return 150.0f;
        }

        public String getDisplayString(int row, int column) {
            if (column == 0) {
                return "" + (row + 1);
            }
            if (column == 1) {
                return "" + row;
            }
            return "" + row;
        }

        public String getUnit(int column) {
            if (column == 0) {
                if (UnitConvertUtils.isImperial()) {
                    return this.mContext.getString(C0532R.string.km_imperial);
                }
                return this.mContext.getString(C0532R.string.km_metric);
            } else if (column == 1) {
                return this.mContext.getString(C0532R.string.time_picker_hour);
            } else {
                return this.mContext.getString(C0532R.string.time_picker_minute);
            }
        }

        public int getInitRow(int column) {
            if (column == 0) {
                return MonthTargetPickerFragment.this.mIndexDis;
            }
            if (column == 1) {
                return MonthTargetPickerFragment.this.mIndexHour;
            }
            return MonthTargetPickerFragment.this.mIndexMin;
        }
    }

    public static MonthTargetPickerFragment newInstance(BaseConfig config) {
        MonthTargetPickerFragment fragment = new MonthTargetPickerFragment();
        Bundle argument = new Bundle();
        argument.putParcelable("key_config", config);
        fragment.setArguments(argument);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        int dis = this.mConfig.getMonthTargetDistance();
        int time = this.mConfig.getMonthTargetCostTime();
        this.mIndexDis = ((int) (((float) dis) / 1000.0f)) - 1;
        this.mIndexHour = (int) ((((long) time) / 1000) / 3600);
        this.mIndexMin = ((int) ((((long) time) / 1000) / 60)) % 60;
    }

    protected AbsPickerViewAdapter getAdapter() {
        return new TargetMonthPickerAdatper(getActivity());
    }

    protected void onSave(int[] selectedRow) {
        int dis = (int) (((float) (selectedRow[0] + 1)) * 1000.0f);
        this.mConfig.setMonthTargetCostTime((3600000 * selectedRow[1]) + (60000 * selectedRow[2]));
        this.mConfig.setMonthTargetDistance(dis);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onPositionChanged(int centerIndex) {
        if (centerIndex == 1) {
            int[] row = getSelected();
            if (row != null && row.length >= 3 && row[1] == 0 && row[2] == 0) {
                scrollToCorrectPosition(2);
            }
        }
    }
}
