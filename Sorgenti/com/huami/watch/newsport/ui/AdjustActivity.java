package com.huami.watch.newsport.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;

public class AdjustActivity extends BaseActivity implements OnTouchListener, EventCallBack {
    private static final String TAG = AdjustActivity.class.getName();
    private View mAdjustContainer;
    private BaseConfig mConfig = null;
    private TextView mContent;
    private int mDefaultValue = 25;
    private long mDelayTime = 1000;
    private Handler mHandler = null;
    private boolean mIsConfirm = false;
    private boolean mIsFirstSettings = false;
    private boolean mIsOnLongClick = false;
    private int mLastSwimLength = -1;
    private int mMaxRange = 50;
    private int mMinRange = 10;
    private int mMode = 0;
    private View mPlugBtn;
    private View mReduceBtn;
    private int mSaveSwimPoolLength = 25;
    private int mSportType = -1;
    private View mTipsContainer;
    private TextView mTitle;
    private WakeLock mWakeLock;

    class C06721 implements OnClickListener {
        C06721() {
        }

        public void onClick(View v) {
            AdjustActivity.this.onConfirmClick();
        }
    }

    private class MyHandler extends Handler {
        public MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (AdjustActivity.this.mIsOnLongClick) {
                        AdjustActivity.this.plugAdjustContent();
                        AdjustActivity.this.reSendDelayTime(1);
                        return;
                    }
                    return;
                case 2:
                    if (AdjustActivity.this.mIsOnLongClick) {
                        AdjustActivity.this.reduceAdjustContent();
                        AdjustActivity.this.reSendDelayTime(2);
                        return;
                    }
                    return;
                case 4:
                    AdjustActivity.this.acquireWakeLock();
                    AdjustActivity.this.mHandler.sendEmptyMessageDelayed(4, 4000);
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        int msg = -1;
        if (id == C0532R.id.reduce) {
            msg = 2;
        } else if (id == C0532R.id.plug) {
            msg = 1;
        }
        switch (event.getAction()) {
            case 0:
                this.mIsOnLongClick = true;
                this.mHandler.sendEmptyMessageDelayed(msg, this.mDelayTime);
                break;
            case 1:
                this.mIsOnLongClick = false;
                this.mHandler.removeMessages(1);
                this.mHandler.removeMessages(2);
                if (this.mDelayTime == 1000) {
                    if (id == C0532R.id.reduce) {
                        reduceAdjustContent();
                    } else if (id == C0532R.id.plug) {
                        plugAdjustContent();
                    }
                }
                this.mDelayTime = 1000;
                break;
        }
        return true;
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_UP) {
            if (this.mAdjustContainer.getVisibility() == 0) {
                plugAdjustContent();
            }
        } else if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            onConfirmClick();
        } else if (hmKeyEvent == HMKeyEvent.KEY_DOWN && this.mAdjustContainer.getVisibility() == 0) {
            reduceAdjustContent();
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        setKeyEventListener(this);
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        this.mMaxRange = getIntent().getIntExtra("swim_max_range", 50);
        this.mMinRange = getIntent().getIntExtra("swim_min_range", 10);
        this.mIsFirstSettings = getIntent().getBooleanExtra("sport_first_setting", false);
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.swim_length_setting_layout);
            this.mReduceBtn = findViewById(C0532R.id.reduce);
            this.mPlugBtn = findViewById(C0532R.id.plug);
            this.mContent = (TextView) findViewById(C0532R.id.value);
            this.mTitle = (TextView) findViewById(C0532R.id.title);
            this.mTipsContainer = findViewById(C0532R.id.tips_container);
            this.mAdjustContainer = findViewById(C0532R.id.adjust_container);
            this.mHandler = new MyHandler(getMainLooper());
            this.mReduceBtn.setOnTouchListener(this);
            this.mPlugBtn.setOnTouchListener(this);
            this.mLastSwimLength = DataManager.getInstance().getLastSwimLength();
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            this.mDefaultValue = this.mConfig.getTargetSwimLength();
            this.mSaveSwimPoolLength = this.mDefaultValue;
            this.mContent.setText("" + this.mDefaultValue);
            findViewById(C0532R.id.confirm).setOnClickListener(new C06721());
            if (this.mIsFirstSettings) {
                this.mWakeLock = ((PowerManager) getSystemService("power")).newWakeLock(10, "adjust_swim_length_wake");
                acquireWakeLock();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("err sport type:" + this.mSportType);
    }

    private void onConfirmClick() {
        switch (this.mMode) {
            case 0:
                if (this.mLastSwimLength != this.mDefaultValue) {
                    this.mAdjustContainer.setVisibility(4);
                    this.mTipsContainer.setVisibility(0);
                    findViewById(C0532R.id.title_tips).setVisibility(4);
                    this.mTitle.setText(getString(C0532R.string.adjust_tips_title));
                    this.mMode = 1;
                    return;
                }
                this.mIsConfirm = true;
                this.mSaveSwimPoolLength = this.mDefaultValue;
                saveConfig();
                setResult(-1);
                finish();
                return;
            case 1:
                this.mIsConfirm = true;
                this.mSaveSwimPoolLength = this.mDefaultValue;
                saveConfig();
                setResult(-1);
                finish();
                return;
            default:
                return;
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mHandler != null) {
            this.mHandler.removeCallbacksAndMessages(null);
        }
        releaseWakeLock();
        if (!this.mIsConfirm) {
            saveConfig();
        }
    }

    private void saveConfig() {
        this.mConfig.setTargetSwimLength(this.mSaveSwimPoolLength);
        this.mConfig.setDistanceAutoLap((float) this.mSaveSwimPoolLength);
        DataManager.getInstance().setSportConfig(this, this.mConfig);
        DataManager.getInstance().setIsFirstOpenSwim(false);
        DataManager.getInstance().setLastSwimLength(this.mSaveSwimPoolLength);
    }

    private void reSendDelayTime(int msg) {
        this.mDelayTime -= 300;
        if (this.mDelayTime < 300) {
            this.mDelayTime = 25;
        }
        this.mHandler.sendEmptyMessageDelayed(msg, this.mDelayTime);
    }

    private synchronized void plugAdjustContent() {
        this.mDefaultValue++;
        if (this.mDefaultValue > this.mMaxRange) {
            this.mDefaultValue = this.mMaxRange;
        }
        this.mContent.setText("" + this.mDefaultValue);
    }

    private synchronized void reduceAdjustContent() {
        this.mDefaultValue--;
        if (this.mDefaultValue < this.mMinRange) {
            this.mDefaultValue = this.mMinRange;
        }
        this.mContent.setText("" + this.mDefaultValue);
    }

    private void acquireWakeLock() {
        Debug.m5i(TAG, "acquireWakeLock");
        if (this.mWakeLock != null) {
            this.mWakeLock.acquire();
        }
    }

    private void releaseWakeLock() {
        Debug.m5i(TAG, "releaseWakeLock");
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
