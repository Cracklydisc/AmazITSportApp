package com.huami.watch.newsport.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.battery.BatteryManager;
import com.huami.watch.newsport.battery.BatteryManager.IBatteryChangedListener;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SortInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.sportcenter.controller.SportDataManager;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter;
import com.huami.watch.newsport.ui.adapter.SportLauncherItemAdapter.ISportItemClickListener;
import com.huami.watch.newsport.ui.view.NumberTextView;
import com.huami.watch.newsport.ui.view.SportMainSettingFace;
import com.huami.watch.newsport.ui.view.SportSettingWheelList;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.newsport.widget.SportWidget;
import java.util.HashMap;
import java.util.Map;

public class SportLauncherActivity extends BaseActivity implements IBatteryChangedListener, ISportItemClickListener {
    private static final String TAG = SportLauncherActivity.class.getName();
    private SportLauncherItemAdapter mAdapter = null;
    private ImageView mBatteryImg;
    private BatteryManager mBatteryManager = null;
    private TextView mBatteryPercent = null;
    private NumberTextView mContinueDisView;
    private NumberTextView mContinueTimeView;
    private TextView mDisUnit = null;
    private boolean mFirstActive = true;
    private BroadcastReceiver mHomeKeyEventReceiver = new C07124();
    private LayoutInflater mInflater = null;
    private boolean mIsActive = false;
    private int mLastSportType = -1;
    private SportMainSettingFace mSportMainSettingFace = null;
    private int mSportType = -1;
    private TextView mSuggestTimeView = null;
    private SportSettingWheelList mWheelList = null;

    class C07091 implements Runnable {
        C07091() {
        }

        public void run() {
            final Map<Integer, SortInfo> sortInfoMap = new HashMap();
            for (int sportType : SportLauncherItemAdapter.SPORT_TYPE_ARRAYS) {
                SortInfo sortInfo = DataManager.getInstance().getSpecificSportNum(sportType);
                sortInfoMap.put(Integer.valueOf(sortInfo.getType()), sortInfo);
            }
            Global.getGlobalUIHandler().post(new Runnable() {
                public void run() {
                    int index = SportLauncherActivity.this.mAdapter.refreshSportSortInfo(sortInfoMap);
                    if (index != -1) {
                        SportLauncherActivity.this.mWheelList.scroll((float) (index - SportLauncherActivity.this.mWheelList.getCurrentItem()), 100);
                    }
                }
            });
        }
    }

    class C07102 implements OnDismissListener {
        C07102() {
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
        }
    }

    class C07113 implements OnClickListener {
        C07113() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C07124 extends BroadcastReceiver {
        String SYSTEM_HOME_KEY = "homekey";
        String SYSTEM_HOME_KEY_LONG = "recentapps";
        String SYSTEM_REASON = "reason";

        C07124() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && TextUtils.equals(intent.getStringExtra(this.SYSTEM_REASON), this.SYSTEM_HOME_KEY)) {
                Debug.m5i("test_home", "SYSTEM_HOME_KEY");
                SportLauncherActivity.this.mFirstActive = true;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_launcher_view);
        this.mDisUnit = (TextView) findViewById(C0532R.id.dis_unit);
        this.mSportMainSettingFace = (SportMainSettingFace) findViewById(C0532R.id.main_list);
        this.mContinueDisView = (NumberTextView) findViewById(C0532R.id.battery_distance_time);
        this.mContinueTimeView = (NumberTextView) findViewById(C0532R.id.battery_continue_time);
        this.mBatteryPercent = (TextView) findViewById(C0532R.id.battery_percent);
        this.mBatteryImg = (ImageView) findViewById(C0532R.id.battery_icon);
        this.mSuggestTimeView = (TextView) findViewById(C0532R.id.suggest_reset_time);
        this.mBatteryManager = new BatteryManager(this);
        this.mSportMainSettingFace.setDismissAllow(true);
        int marginTop = getResources().getDimensionPixelSize(C0532R.dimen.settings_item_layout_drawing_top);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        this.mWheelList = new SportSettingWheelList(this.mSportMainSettingFace, new RectF(0.0f, 0.0f, (float) dm.widthPixels, (float) dm.widthPixels));
        this.mWheelList.setMarginTop(marginTop);
        this.mWheelList.getXContext().setParentNeedTouchEvent(true);
        this.mAdapter = new SportLauncherItemAdapter(this.mSportMainSettingFace);
        this.mAdapter.setSportItemClickListener(this);
        this.mWheelList.setViewAdapter(this.mAdapter);
        this.mWheelList.standup(false, true);
        this.mSportMainSettingFace.addDrawingItem(this.mWheelList);
        this.mSportMainSettingFace.postInvalidate();
        updateTitles(SportDataManager.getInstance().getLastSportType(), isSportRunningInBackground());
    }

    private boolean isSportRunningInBackground() {
        int sportState = SportDataManager.getInstance().getSportStatus();
        if (sportState == 6 || sportState == 0) {
            return false;
        }
        return true;
    }

    public void onSportClicked(int sportType) {
        if (!this.mIsActive) {
            return;
        }
        if (this.mLastSportType != -1) {
            Debug.m5i(TAG, "onSportClicked, sport:" + sportType);
            if (this.mLastSportType != sportType) {
                showNoStopSportDialog(this.mLastSportType);
                return;
            }
            BaseConfig config = DataManager.getInstance().getSportConfig(this, sportType);
            VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
            Intent intent = new Intent(this, SportActivity.class);
            intent.putExtra("sport_type", sportType);
            intent.putExtra("sport_gps_status", SensorHubManagerWrapper.getInstance(this).isGpsAvailable());
            intent.putExtra("sport_config", config);
            intent.putExtra("is_resume_sport", true);
            startActivity(intent);
            return;
        }
        this.mSportType = sportType;
        if (sportType == 11) {
            Intent sportIntent = new Intent("com.huami.watch.sport.action.SPORT_HISTORY");
            sportIntent.setPackage("com.huami.watch.newsport");
            sportIntent.addFlags(805339136);
            startActivity(sportIntent);
            return;
        }
        sportIntent = new Intent("com.huami.watch.sport.action.SPORT_GPS_SEARCH");
        sportIntent.setPackage("com.huami.watch.newsport");
        sportIntent.putExtra("sport_type", this.mSportType);
        sportIntent.addFlags(268435456);
        startActivity(sportIntent);
    }

    protected void onResume() {
        super.onResume();
        this.mIsActive = true;
        if (this.mWheelList != null) {
            this.mWheelList.showScrollBar();
        }
        this.mBatteryManager.setBatterListener(this);
        this.mBatteryManager.registerBatteryReceiver();
        if (UnitConvertUtils.isImperial()) {
            this.mDisUnit.setText(getString(C0532R.string.battery_continue_imperial_distance_unit));
        } else {
            this.mDisUnit.setText(getString(C0532R.string.battery_continue_distance_unit));
        }
        registerHomeKeyReceiver();
        if (this.mFirstActive) {
            this.mFirstActive = false;
            Global.getGlobalWorkHandler().post(new C07091());
        }
    }

    protected void onPause() {
        super.onPause();
        this.mIsActive = false;
        this.mBatteryManager.unRegisterBatteryReceiver();
        this.mBatteryManager.setBatterListener(null);
        unRegisterHomeKeyReceiver();
    }

    protected void onDestroy() {
        super.onDestroy();
        this.mIsActive = false;
    }

    public void onBatteryChanged(int level, int continueHour, float continueDis, int status) {
        Debug.m5i(TAG, "level:" + level + ", hour:" + continueHour + ", dis:" + continueDis + ", status:" + status);
        if (status == 2) {
            this.mBatteryImg.setImageResource(C0532R.drawable.widget_battery_charging);
        } else {
            updateBattery(level);
        }
        if (UnitConvertUtils.isImperial()) {
            this.mDisUnit.setText(getString(C0532R.string.battery_continue_imperial_distance_unit));
        } else {
            this.mDisUnit.setText(getString(C0532R.string.battery_continue_distance_unit));
        }
        this.mBatteryPercent.setText(getString(C0532R.string.hill_default_percent, new Object[]{Integer.valueOf(level)}));
        this.mContinueDisView.setText(String.valueOf(getFormatNumber((float) UnitConvertUtils.convertDistanceToMileOrKm((double) continueDis))));
        this.mContinueTimeView.setText(String.valueOf(continueHour));
    }

    public void doBatteryChangedRequest() {
    }

    public void updateBattery(int level) {
        int pos = 13;
        if (this.mBatteryImg != null) {
            if (level / 7 <= 13) {
                pos = level / 7;
            }
            this.mBatteryImg.setImageResource(SportWidget.BATTERY_RES[pos]);
        }
    }

    private void showNoStopSportDialog(int sportType) {
        new Builder(this).setMessageTextSize(16.0f).setMessageTextColor(-1).setMessage(getString(C0532R.string.end_last_sport_tips, new Object[]{this.mAdapter.getCorrespondingSportStr(sportType)})).setPositiveButton(getString(C0532R.string.confirm), new C07113()).setOnDismissListener(new C07102()).create().show();
    }

    private void updateTitles(int sportType, boolean isStarted) {
        Debug.m5i(TAG, "sport, updateTitles: " + sportType + ", isStarted: " + isStarted);
        if (isStarted) {
            this.mLastSportType = sportType;
        }
        this.mAdapter.refreshViewStatus(sportType, isStarted);
    }

    private String getFormatNumber(float f) {
        if (f >= 100.0f) {
            return String.valueOf((int) f);
        }
        if (f >= 10.0f) {
            return String.format("%.1f", new Object[]{Float.valueOf(f)});
        }
        return String.format("%.2f", new Object[]{Float.valueOf(f)});
    }

    private void registerHomeKeyReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        registerReceiver(this.mHomeKeyEventReceiver, filter);
    }

    private void unRegisterHomeKeyReceiver() {
        try {
            unregisterReceiver(this.mHomeKeyEventReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
