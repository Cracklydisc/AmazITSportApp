package com.huami.watch.newsport.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.huami.watch.common.widget.HmHaloButton;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.adapter.ConfigMenuAdapter;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.newsport.xmlparser.bean.TERemindStage;
import com.huami.watch.newsport.xmlparser.utils.SAXUtils;

public class RunningTargetSettingActivity extends BaseActivity implements EventCallBack {
    private BaseConfig config = null;
    private LinearLayout mContentLinear = null;
    private HmHaloButton mMoreBtn = null;
    private int mSportType = -1;
    private SparseArray<TERemindStage> mTESettingSparseArray;

    class C06911 implements OnClickListener {
        C06911() {
        }

        public void onClick(View v) {
            RunningTargetSettingActivity.this.startSettingActivity();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        setKeyEventListener(this);
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            this.mSportType = getIntent().getIntExtra("sport_type", -1);
        }
        setContentView(C0532R.layout.te_setting_layout);
        this.mMoreBtn = (HmHaloButton) findViewById(C0532R.id.select_btn);
        this.mMoreBtn.setWithHalo(false);
        this.mContentLinear = (LinearLayout) findViewById(C0532R.id.content_container);
        this.config = DataManager.getInstance().getSportConfig(this, this.mSportType);
        this.mMoreBtn.setOnClickListener(new C06911());
        this.mTESettingSparseArray = SAXUtils.getTERemindSettingsFromXml(this);
        loadContent();
    }

    private void startSettingActivity() {
        Intent intent = new Intent(this, SportSettingsActivity.class);
        if (ConfigMenuAdapter.isNeedTERemind(this.mSportType)) {
            intent.putExtra("setting_type", 4);
        } else {
            intent.putExtra("setting_type", 0);
        }
        intent.putExtra("sport_type", this.mSportType);
        startActivityForResult(intent, 32769);
    }

    private void updateView(String content) {
        updateView(content, false);
    }

    private void updateView(String content, boolean isRemindTe) {
        TextView textView = new TextView(this);
        textView.setText(content);
        textView.setTextColor(-1);
        if (UnitConvertUtils.isZh()) {
            textView.setTextSize(18.67f);
        } else {
            textView.setTextSize(15.0f);
        }
        LayoutParams lp = new LayoutParams(-1, 0);
        lp.weight = 1.0f;
        lp.gravity = 17;
        if (isRemindTe) {
            textView.setLayoutParams(lp);
            textView.setGravity(17);
        } else {
            lp.leftMargin = 50;
            textView.setLayoutParams(lp);
            textView.setGravity(16);
        }
        this.mContentLinear.addView(textView);
    }

    private void loadContent() {
        if (this.config.isRemindTE()) {
            updateView("");
            TERemindStage curTEStage = (TERemindStage) this.mTESettingSparseArray.get(this.config.getTargetTEStage());
            updateView(curTEStage.getDesc(), true);
            r12 = new Object[2];
            r12[0] = String.format("%.1f", new Object[]{Float.valueOf(((float) curTEStage.getTe()) / 10.0f)});
            r12[1] = String.valueOf(curTEStage.getTimeCost() / 60);
            updateView(getString(C0532R.string.sport_setting_te_stage_content, r12), true);
            updateView("");
            return;
        }
        if (this.config.isRemindTargetDistance()) {
            if (!SportType.isSwimMode(this.mSportType)) {
                updateView(getString(UnitConvertUtils.isImperial() ? C0532R.string.sport_cur_tar_dis_mi : C0532R.string.sport_cur_tar_dis_km, new Object[]{String.valueOf((int) (((float) this.config.getTargetDistance()) / 1000.0f))}));
            } else if (this.config.getUnit() == 0) {
                updateView(getString(C0532R.string.sport_cur_tar_dis_meter, new Object[]{String.valueOf(this.config.getTargetDistance())}));
            } else {
                updateView(getString(C0532R.string.sport_cur_tar_dis_yd, new Object[]{String.valueOf(this.config.getTargetDistance())}));
            }
        }
        if (this.config.isRemindTargetTimeCost()) {
            long timeCost = this.config.getTargetTimeCost();
            int h = (int) (timeCost / 3600000);
            int min = (int) ((timeCost / 60000) % 60);
            updateView(getString(C0532R.string.sport_cur_tar_time, new Object[]{Integer.valueOf(h), Integer.valueOf(min)}));
        }
        if (this.config.isRemindCalorie()) {
            updateView(getString(C0532R.string.sport_cur_tar_cal, new Object[]{String.valueOf((long) this.config.getTargetCalorie())}));
        }
        if (this.config.isRemindTargetTrips()) {
            updateView(getString(C0532R.string.sport_cur_tar_trips, new Object[]{String.valueOf(this.config.getTargetTrips())}));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32769) {
            this.config = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (ConfigMenuAdapter.isOpenTarget(this.config)) {
                this.mContentLinear.removeAllViews();
                loadContent();
                return;
            }
            setResult(-1);
            finish();
        }
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_CENTER) {
            startSettingActivity();
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }
}
