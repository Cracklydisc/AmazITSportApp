package com.huami.watch.newsport.ui.firstbeathelp;

import android.os.Bundle;
import android.text.Html;
import com.huami.watch.common.widget.HmKeyEventScrollView;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.scrollbar.ArcScrollbarHelper;

public class FirstBeatHelpActivity extends BaseActivity {
    private static final String TAG = FirstBeatHelpActivity.class.getSimpleName();
    private HmTextView helpContentView;
    private HmTextView helpTitleView;
    private int helpTypeCategory = -1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_first_beat_help);
        this.helpTypeCategory = getIntent().getIntExtra("help_type_category", -1);
        if (this.helpTypeCategory >= 0) {
            ArcScrollbarHelper.setArcScrollBarDrawable((HmKeyEventScrollView) findViewById(C0532R.id.scroll_view));
            this.helpTitleView = (HmTextView) findViewById(C0532R.id.help_title);
            this.helpContentView = (HmTextView) findViewById(C0532R.id.help_content);
            initContentData(this.helpTypeCategory);
        }
    }

    private void initContentData(int helpTypeCategory) {
        switch (helpTypeCategory) {
            case 0:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_vo2_max));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_vo2_max)));
                return;
            case 1:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_train_efffect));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_te)));
                return;
            case 2:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_train_load));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_train_load)));
                return;
            case 3:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_sport_target_or_real_time_guide));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_run_target_and_real_time_guide)));
                return;
            case 4:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_daily_perpormence));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_daily_perpormence)));
                return;
            case 5:
                this.helpTitleView.setText(getResources().getString(C0532R.string.first_beat_help_title_recovery_time));
                this.helpContentView.setText(Html.fromHtml(getResources().getString(C0532R.string.first_beat_help_recovery_time)));
                return;
            default:
                return;
        }
    }
}
