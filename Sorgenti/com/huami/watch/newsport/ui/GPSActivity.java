package com.huami.watch.newsport.ui;

import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.log.Debug;
import com.huami.watch.common.widget.HmTextView;
import com.huami.watch.common.widget.HmViewPager;
import com.huami.watch.common.widget.SystemDialog.Builder;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportTargetRecommandActivity;
import com.huami.watch.newsport.bleconn.BleConnManager;
import com.huami.watch.newsport.bleconn.BleConnManager.IBleCoreConnCallback;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.klvp.SensorHubManagerWrapper;
import com.huami.watch.newsport.setting.swimlength.SwimLengthActivity;
import com.huami.watch.newsport.setting.tennicclaphand.TennicSelectHandActivity;
import com.huami.watch.newsport.train.TrainingInfoManager;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.ui.adapter.SportFragmentAdapter;
import com.huami.watch.newsport.ui.fragbase.BaseFragment;
import com.huami.watch.newsport.ui.fragps.GPSearchFragment;
import com.huami.watch.newsport.ui.fragps.GPSearchFragment.IGPSearchListener;
import com.huami.watch.newsport.ui.fragps.GPStatusFragment;
import com.huami.watch.newsport.ui.fragps.GPStatusFragment.IGPSearchFailedListener;
import com.huami.watch.newsport.ui.fragps.SettingFragment;
import com.huami.watch.newsport.ui.fragps.SportTargetRemindFragment;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.PointUtils;
import com.huami.watch.newsport.utils.VibratorUtil;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensorhub.SensorHubProtos.SportThaWorkout;
import com.huami.watch.watchmanager.HmWatchManager;

public class GPSActivity extends BaseActivity implements EventCallBack, IGPSearchListener, IGPSearchFailedListener {
    private static final String TAG = GPSActivity.class.getName();
    private int currnetPageIndex = 0;
    private boolean isStartedLoadingService = false;
    private boolean isStartedSport = false;
    private SportFragmentAdapter mAdapter;
    private IBleCoreConnCallback mBleCoreConnCallback = new C06859();
    private BluetoothDevice mBluetootoDevice = null;
    private BaseConfig mConfig = null;
    private int mCurrentGpsStatus = 2;
    private GPStatusFragment mFailedFragment = null;
    private View mGpsFoundContainer = null;
    private BaseFragment mGpsSearchFragment;
    private HmTextView mGpsTextView = null;
    private ViewPagerPageIndicator mIndicator;
    private boolean mIsDestroy = false;
    private BaseFragment mSettingsFragment;
    private BaseFragment mSportTargetRemindFragment;
    private int mSportType = -1;
    private int mTrainingMode = -1;
    private HmViewPager mViewPager;

    class C06761 implements Runnable {

        class C06751 extends Callback {
            C06751() {
            }

            protected void doCallback(int i, Object o) {
                Debug.m5i("notify_heart_rate", "start notify callback come back, success");
            }
        }

        C06761() {
        }

        public void run() {
            if (!GPSActivity.this.isDestroyed() && !GPSActivity.this.isFinishing()) {
                BaseConfig baseConfig = DataManager.getInstance().getSportConfig(GPSActivity.this, GPSActivity.this.mSportType);
                if (!SportType.isSwimMode(GPSActivity.this.mSportType) && baseConfig.isMeasureHeart()) {
                    SensorHubManagerWrapper.getInstance(GPSActivity.this).notifySensorOpenHeartRate(new C06751());
                }
            }
        }
    }

    class C06782 implements Runnable {

        class C06771 extends Callback {
            C06771() {
            }

            protected void doCallback(int i, Object o) {
                Debug.m5i("notify_heart_rate", "stop notify callback come back, success");
            }
        }

        C06782() {
        }

        public void run() {
            SensorHubManagerWrapper.getInstance(GPSActivity.this).notifySensorCloseHeartRate(new C06771());
        }
    }

    class C06793 implements OnClickListener {
        final /* synthetic */ GPSActivity this$0;
        final /* synthetic */ BaseConfig val$config;

        public void onClick(DialogInterface dialog, int which) {
            this.val$config.setTennicClapThaHands(1);
            DataManager.getInstance().setSportConfig(this.this$0, this.val$config);
            DataManager.getInstance().setIsFirstTennicClapHand(false);
            dialog.dismiss();
        }
    }

    class C06804 implements OnClickListener {
        final /* synthetic */ GPSActivity this$0;
        final /* synthetic */ BaseConfig val$config;

        public void onClick(DialogInterface dialog, int which) {
            this.val$config.setTennicClapThaHands(0);
            DataManager.getInstance().setSportConfig(this.this$0, this.val$config);
            DataManager.getInstance().setIsFirstTennicClapHand(false);
            dialog.dismiss();
        }
    }

    class C06815 implements Runnable {
        C06815() {
        }

        public void run() {
            GPSActivity.this.finish();
        }
    }

    class C06826 implements OnDismissListener {
        C06826() {
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
            DataManager.getInstance().saveBindDeviceState(GPSActivity.this, false);
            Fragment fragment = GPSActivity.this.mAdapter.getItem(0);
            if (fragment instanceof GPSearchFragment) {
                ((GPSearchFragment) fragment).handleBtnEvent();
            }
        }
    }

    class C06837 implements OnClickListener {
        C06837() {
        }

        public void onClick(DialogInterface dialog, int which) {
            GPSActivity.this.startBindDeviceActivity();
        }
    }

    class C06848 implements OnClickListener {
        C06848() {
        }

        public void onClick(DialogInterface dialog, int which) {
            DataManager.getInstance().saveBindDeviceState(GPSActivity.this, false);
            Fragment fragment = GPSActivity.this.mAdapter.getItem(0);
            if (fragment instanceof GPSearchFragment) {
                ((GPSearchFragment) fragment).handleBtnEvent();
            }
        }
    }

    class C06859 implements IBleCoreConnCallback {
        C06859() {
        }

        public void onConnectStateChange(int id, int code) {
            BleConnManager.getInstance().unRegisterBleConnListener(this);
            if (!GPSActivity.this.mIsDestroy && code == 100 && id == 0) {
                BleConnManager.getInstance().notifyBleSportStart(GPSActivity.this.mSportType);
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            this.mSportType = getIntent().getIntExtra("sport_type", -1);
            this.mTrainingMode = getIntent().getIntExtra("entrance_type", -1);
            if (TrainingInfoManager.isInTrainingMode(this.mTrainingMode)) {
                this.mSportType = TrainingInfoManager.getInstance(this).changeTrainTypeToSportType(TrainingInfoManager.getInstance(this).getTrainingInfo().getTrainType());
            }
        }
        if (SportType.isSportTypeValid(this.mSportType)) {
            setContentView(C0532R.layout.activity_gps_layout);
            this.mViewPager = (HmViewPager) findViewById(C0532R.id.pager);
            this.mIndicator = (ViewPagerPageIndicator) findViewById(C0532R.id.indicator);
            this.mGpsFoundContainer = findViewById(C0532R.id.alert_main_container);
            this.mGpsTextView = (HmTextView) findViewById(C0532R.id.remind_type_text);
            initPager();
            setKeyEventListener(this);
            this.mGpsTextView.setText(getString(C0532R.string.sport_gps_found_success_tips));
            Secure.putInt(getContentResolver(), "sport_background_mode", 0);
            HmSensorHubConfigManager.getHmSensorHubConfigManager(this).setIsUseThaWorkoutSet(false);
            BleConnManager.getInstance().registerBleConnListener(this.mBleCoreConnCallback);
            BleConnManager.getInstance().bindBleService(this, 0);
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (this.mSportType == 14 && DataManager.getInstance().isFirstOpenSwim()) {
                Intent swimIntent = new Intent(this, SwimLengthActivity.class);
                swimIntent.putExtra("key_is_first_config", true);
                swimIntent.putExtra("sport_type", this.mSportType);
                startActivityForResult(swimIntent, 1024);
            }
            if (this.mSportType == 17 && DataManager.getInstance().isFirstTennicClapHand()) {
                Intent tennicSelectHandIntent = new Intent(this, TennicSelectHandActivity.class);
                tennicSelectHandIntent.putExtra("sport_type", this.mSportType);
                startActivityForResult(tennicSelectHandIntent, 5381);
            }
            notifySensorOpenHeartRate();
            HmWatchManager.getHmWatchManager(this).disableFeature(0, true);
            return;
        }
        Debug.m5i(TAG, "err sport type: " + this.mSportType);
        finish();
    }

    protected void onStop() {
        super.onStop();
        HmWatchManager.getHmWatchManager(this).disableFeature(0, false);
    }

    public void notifySensorOpenHeartRate() {
        Global.getGlobalWorkHandler().post(new C06761());
    }

    private void notifySensorCloseHeartRate() {
        Global.getGlobalWorkHandler().post(new C06782());
    }

    protected void onDestroy() {
        super.onDestroy();
        LogUtils.print(TAG, "onDestroy, " + this.isStartedSport);
        this.mIsDestroy = true;
        if (!this.isStartedSport) {
            notifySensorCloseHeartRate();
            BleConnManager.getInstance().notifyBleSportStop(this.mSportType);
            BleConnManager.getInstance().unbindBleService(0);
        }
        BleConnManager.getInstance().unRegisterBleConnListener(this.mBleCoreConnCallback);
    }

    private void hideFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commitAllowingStateLoss();
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(C0532R.id.fragment_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    private void initPager() {
        this.mAdapter = new SportFragmentAdapter(getSupportFragmentManager());
        this.mFailedFragment = GPStatusFragment.newInstance();
        this.mGpsSearchFragment = BaseFragment.newInstance(this.mSportType, 0);
        this.mSportTargetRemindFragment = BaseFragment.newInstance(this.mSportType, 2);
        this.mSettingsFragment = BaseFragment.newInstance(this.mSportType, 1);
        this.mAdapter.addFragment(this.mGpsSearchFragment);
        this.mAdapter.notifyDataSetChanged();
        this.mViewPager.setAdapter(this.mAdapter);
        this.mIndicator.setViewPager(this.mViewPager);
        this.mIndicator.showIndicator(true);
        this.mIndicator.setPageIndicatorDrawable(1, BitmapFactory.decodeResource(getResources(), C0532R.drawable.sport_indicator_set));
    }

    public void startSport(int gpsStatus) {
        startSportIntent(null);
    }

    private void startSportIntent(Intent intent) {
        if (this.mCurrentGpsStatus == 2) {
            PointUtils.recordEventProperty("0002", this.mSportType);
        } else {
            PointUtils.recordEventProperty("0003", this.mSportType);
        }
        notifySlptSportDataInit();
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
        if (intent == null) {
            intent = new Intent(this, SportActivity.class);
            intent.putExtra("sport_type", this.mSportType);
            intent.putExtra("sport_gps_status", this.mCurrentGpsStatus);
            intent.putExtra("le_qi_device", this.mBluetootoDevice);
            intent.putExtra("sport_config", this.mConfig);
            intent.putExtra("entrance_type", this.mTrainingMode);
        }
        startActivity(intent);
        this.isStartedSport = true;
        Global.getGlobalWorkHandler().postDelayed(new C06815(), 200);
    }

    public void onAgpsExpired() {
        this.mFailedFragment.setFailedType(1);
        showFragment(this.mFailedFragment);
    }

    public void onGpsTimeOut(boolean isTimeOut) {
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        this.mFailedFragment.setFailedType(isTimeOut ? 3 : 0);
        showFragment(this.mFailedFragment);
        if (isTimeOut) {
            SensorHubManagerWrapper.getInstance(this).notifySensorCloseHeartRate(null);
        }
    }

    public void onGPSClosedNotificationShow() {
        LogUtil.m9i(true, TAG, "onGPSClosedNotificationShow");
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        this.mFailedFragment.setFailedType(4);
        showFragment(this.mFailedFragment);
    }

    public BaseConfig getSportConfig() {
        return this.mConfig;
    }

    public void onGpsFoundSuccess() {
        VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        hideFragment(this.mFailedFragment);
    }

    public void onGpsChanged(int gpsStatus) {
        boolean z = true;
        this.mCurrentGpsStatus = gpsStatus;
        if (this.mSportTargetRemindFragment != null && this.mSportTargetRemindFragment.isVisible()) {
            SportTargetRemindFragment sportTargetRemindFragment = (SportTargetRemindFragment) this.mSportTargetRemindFragment;
            if (this.mCurrentGpsStatus != 1) {
                z = false;
            }
            sportTargetRemindFragment.updateGpsStatus(z);
        }
    }

    public void onGPSearchBtnClick(int type) {
        LogUtil.m9i(true, TAG, "onGPSearchBtnClick:" + type);
        hideFragment(this.mFailedFragment);
        if (type != 2 && type != 4 && type != 3) {
            Fragment fragment = this.mAdapter.getItem(0);
            if (fragment instanceof GPSearchFragment) {
                ((GPSearchFragment) fragment).handleBtnEvent();
            }
        }
    }

    public void onItemClick(int pos, boolean isHasSportRemind) {
        LogUtils.print(TAG, "onKeyClick key center key center :" + this.currnetPageIndex + " showOrHide:" + this.mFailedFragment.isVisible() + " isResume:" + this.mFailedFragment.isResumed());
        Fragment fragment;
        if (this.mFailedFragment.isVisible()) {
            int type = this.mFailedFragment.getGPStatusType();
            hideFragment(this.mFailedFragment);
            if (type != 2 && type != 4) {
                fragment = this.mAdapter.getItem(0);
                if (fragment instanceof GPSearchFragment) {
                    ((GPSearchFragment) fragment).handleBtnEvent();
                }
            }
        } else if (this.mSportTargetRemindFragment == null || !this.mSportTargetRemindFragment.isVisible()) {
            switch (this.currnetPageIndex) {
                case 0:
                    fragment = this.mAdapter.getItem(0);
                    if (fragment instanceof GPSearchFragment) {
                        switch (pos) {
                            case 0:
                                ((GPSearchFragment) fragment).handleBtnEvent();
                                return;
                            case 1:
                                if (!isHasSportRemind) {
                                    startSettingActivity();
                                    return;
                                } else if (this.mSportType == 2002) {
                                    startSelectCompoundActivity();
                                    return;
                                } else {
                                    startIntermittentTraining();
                                    return;
                                }
                            case 2:
                                startSettingActivity();
                                return;
                            default:
                                return;
                        }
                    }
                    return;
                default:
                    return;
            }
        } else {
            PointUtils.recordEventProperty("0005", this.mSportType);
            onShowSportTargetTipsView(false, null, true);
        }
    }

    private void startSelectCompoundActivity() {
        startActivityForResult(new Intent(this, CompoundSelectActivity.class), 293);
    }

    public boolean isSportStart() {
        return this.isStartedSport;
    }

    private void startSettingActivity() {
        PointUtils.recordEventProperty("0004", this.mSportType);
        Intent sportIntent = new Intent("com.huami.watch.sport.action.SPORT_SETTINGS");
        sportIntent.setPackage("com.huami.watch.newsport");
        sportIntent.addFlags(268435456);
        sportIntent.putExtra("widget_settings_type", this.mSportType);
        startActivity(sportIntent);
    }

    private void startIntermittentTraining() {
        Log.i(TAG, " start intermittent training ");
        Intent intermittentTrainingIntent = new Intent(this, IntermittentTrainingActivity.class);
        intermittentTrainingIntent.addFlags(536870912);
        intermittentTrainingIntent.putExtra("widget_settings_type", this.mSportType);
        startActivityForResult(intermittentTrainingIntent, 8705);
    }

    public boolean onKeyClick(HMKeyEvent hmKeyEvent) {
        if (hmKeyEvent == HMKeyEvent.KEY_UP) {
            LogUtils.print(TAG, "onKeyClick key center key up ");
        } else if (hmKeyEvent == HMKeyEvent.KEY_DOWN) {
            LogUtils.print(TAG, "onKeyClick key center key down ");
        }
        return false;
    }

    public boolean onKeyLongOneSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecond(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongOneSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent hmKeyEvent) {
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.print(TAG, "onActivityResult requestCode:" + requestCode + ",resultCode:" + resultCode);
        if (requestCode == 5381) {
            Log.i(TAG, "TennicConfig:onActivityResult");
        }
        if (requestCode == 291) {
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (data != null) {
                if (!(data.getBooleanExtra("has_no_gpx_file", false) || this.mSettingsFragment == null)) {
                    ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
                }
            } else if (this.mSettingsFragment != null) {
                ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
            }
        }
        if (requestCode == 291) {
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (this.mSettingsFragment != null) {
                ((SettingFragment) this.mSettingsFragment).onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == 102) {
            if (data == null) {
                LogUtils.print(TAG, "onActivityResult data is  null ");
                return;
            }
            this.mBluetootoDevice = (BluetoothDevice) data.getParcelableExtra("ble-mac");
            if (this.mBluetootoDevice != null) {
                LogUtils.print(TAG, "onActivityResult: device for mac:" + this.mBluetootoDevice.getAddress());
            }
        } else if (requestCode == 1024) {
            VibratorUtil.vibrateOnceWithStrongAttributes(this, new long[]{0, 1000});
        } else if (requestCode == 103) {
            DataManager.getInstance().saveBindDeviceState(this, false);
            Fragment fragment = this.mAdapter.getItem(0);
            if (fragment instanceof GPSearchFragment) {
                ((GPSearchFragment) fragment).handleBtnEvent();
            }
        } else if (requestCode == 8705) {
            if (resultCode == 8705) {
                if (data != null && data.getBooleanExtra("is_start_sport", false)) {
                    startSport(HmSensorHubConfigManager.getHmSensorHubConfigManager(this).getGpsState() ? 1 : 2);
                }
            } else if (resultCode == 8706 && data != null && data.getBooleanExtra("is_start_sport", false)) {
                this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
                Intent intent = new Intent(this, SportActivity.class);
                int trainType = data.getIntExtra("intermittent_train_type", 0);
                int firstUnitType = data.getIntExtra("intermittent_remind_type", TrainTargetType.DIATANCE.getType());
                boolean isSingleIntervalTrain = data.getBooleanExtra("only_single_train_type", false);
                intent.putExtra("sport_type", this.mSportType);
                intent.putExtra("sport_gps_status", this.mCurrentGpsStatus);
                intent.putExtra("le_qi_device", this.mBluetootoDevice);
                intent.putExtra("sport_config", this.mConfig);
                intent.putExtra("entrance_type", this.mTrainingMode);
                intent.putExtra("intermittent_train_type", trainType);
                intent.putExtra("intermittent_remind_type", firstUnitType);
                intent.putExtra("only_single_train_type", isSingleIntervalTrain);
                startSportIntent(intent);
            }
        } else if (requestCode == 293) {
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (this.mGpsSearchFragment != null) {
                ((GPSearchFragment) this.mGpsSearchFragment).refreshCompoundView();
            }
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }

    public void onFirstExperienceStartSport() {
        showExperienceMode();
    }

    public void onShowSportTargetTipsView(boolean isShown, SportThaWorkout sportThaWorkout, boolean isHandleBtnEvent) {
        Intent intent = new Intent(this, SportTargetRecommandActivity.class);
        intent.addFlags(536870912);
        intent.putExtra("sport_type", this.mSportType);
        startActivityForResult(intent, 8705);
    }

    private void showExperienceMode() {
        new Builder(this).setMessage((int) C0532R.string.experience_title).setMessageTextSize(16.0f).setMessageTextColor(-1).setNegativeButton(getString(C0532R.string.decision_no), new C06848()).setPositiveButton(getString(C0532R.string.decision_yes), new C06837()).setOnDismissListener(new C06826()).create().show();
    }

    private void startBindDeviceActivity() {
        try {
            Intent bindIntent = new Intent("com.huami.mobile.watchsettings.action.init.personalinfo");
            bindIntent.setPackage("com.huami.mobile.watchsettings");
            bindIntent.addCategory("android.intent.category.DEFAULT");
            startActivityForResult(bindIntent, 103);
            overridePendingTransition(C0532R.anim.scale_in, C0532R.anim.slide_out_to_bottom);
        } catch (ActivityNotFoundException e) {
            Debug.m3d(TAG, "bluetooth activity not found");
        }
    }

    private void notifySlptSportDataInit() {
        if (!this.isStartedLoadingService) {
            Intent intentLoading = new Intent();
            intentLoading.setAction("com.huami.watch.watchface.sportface.SportLoadingService");
            intentLoading.setPackage("com.huami.watch.watchface.analogyellow");
            startService(intentLoading);
        }
        this.isStartedLoadingService = true;
    }
}
