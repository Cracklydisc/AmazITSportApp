package com.huami.watch.newsport.ui.animationcenter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.huami.watch.newsport.ui.uiinterface.IHistoryAnimationListener;
import com.huami.watch.newsport.ui.uiinterface.ILoadingAnimListener;
import com.huami.watch.utils.QuadInterpolator;
import java.util.List;

public class SportHistoryAnimationManager implements IHistoryAnimationListener {
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private static SportHistoryAnimationManager sInstance = null;
    private View mDialogAnimButton;
    private View mDialogNegativeButton;
    private View mDialogTextView;
    private View mDialogView;
    private List<ILoadingAnimListener> mLoadingAnimListener;
    private ValueAnimator mTrailLoadingAnim;
    private View mTrailLoadingView;

    class C07241 implements AnimatorUpdateListener {
        final /* synthetic */ SportHistoryAnimationManager this$0;
        final /* synthetic */ float val$translationX;
        final /* synthetic */ float val$translationY;

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            float alpha = 1.0f - input;
            this.this$0.mDialogTextView.setAlpha(alpha);
            this.this$0.mDialogNegativeButton.setAlpha(alpha);
            this.this$0.mDialogView.setBackgroundColor((((int) alpha) * 255) << 24);
            this.this$0.mDialogAnimButton.setTranslationX(this.val$translationX * input);
            this.this$0.mDialogAnimButton.setTranslationY(this.val$translationY * input);
            float scale = 1.0f - (0.8119f * input);
            this.this$0.mDialogAnimButton.setScaleX(scale);
            this.this$0.mDialogAnimButton.setScaleY(scale);
        }
    }

    class C07252 implements AnimatorUpdateListener {
        final /* synthetic */ SportHistoryAnimationManager this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            this.this$0.mDialogAnimButton.setAlpha(1.0f - input);
            if (this.this$0.mTrailLoadingView != null) {
                this.this$0.mTrailLoadingView.setAlpha(input);
            }
        }
    }

    class C07263 extends AnimatorListenerAdapter {
        final /* synthetic */ SportHistoryAnimationManager this$0;

        public void onAnimationEnd(Animator animation) {
            this.this$0.mDialogView.setVisibility(8);
            this.this$0.startTrailLoadingAnim();
        }
    }

    class C07274 extends AnimatorListenerAdapter {
        final /* synthetic */ SportHistoryAnimationManager this$0;

        public void onAnimationEnd(Animator animation) {
            this.this$0.onLoadingAnimEnd();
        }
    }

    class C07285 implements AnimatorUpdateListener {
        final /* synthetic */ View val$target;

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            this.val$target.setTranslationX(20.0f * input);
            this.val$target.setAlpha(1.0f - input);
        }
    }

    class C07296 implements AnimatorUpdateListener {
        C07296() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            if (SportHistoryAnimationManager.this.mTrailLoadingView != null) {
                SportHistoryAnimationManager.this.mTrailLoadingView.setRotation((float) ((Integer) animation.getAnimatedValue()).intValue());
            }
        }
    }

    class C07307 implements AnimatorUpdateListener {
        final /* synthetic */ SportHistoryAnimationManager this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            if (this.this$0.mTrailLoadingView != null) {
                this.this$0.mTrailLoadingView.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        }
    }

    class C07318 extends AnimatorListenerAdapter {
        final /* synthetic */ SportHistoryAnimationManager this$0;

        public void onAnimationEnd(Animator animation) {
            if (this.this$0.mTrailLoadingAnim != null && this.this$0.mTrailLoadingAnim.isStarted()) {
                this.this$0.mTrailLoadingAnim.cancel();
            }
            if (this.this$0.mTrailLoadingView != null) {
                this.this$0.mTrailLoadingView.setVisibility(8);
            }
        }
    }

    public void startTrailLoadingAnim() {
        if (this.mTrailLoadingAnim == null || !this.mTrailLoadingAnim.isStarted()) {
            this.mTrailLoadingAnim = ValueAnimator.ofInt(new int[]{0, 360});
            this.mTrailLoadingAnim.setDuration(1000);
            this.mTrailLoadingAnim.setInterpolator(new LinearInterpolator());
            this.mTrailLoadingAnim.setRepeatCount(-1);
            this.mTrailLoadingAnim.addUpdateListener(new C07296());
            this.mTrailLoadingAnim.start();
        }
    }

    public void onLoadingAnimEnd() {
        if (this.mLoadingAnimListener != null && this.mLoadingAnimListener.size() > 0) {
            for (ILoadingAnimListener listener : this.mLoadingAnimListener) {
                listener.onLoadingAnimEnd();
            }
        }
    }
}
