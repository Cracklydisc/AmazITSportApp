package com.huami.watch.newsport.customtrain.utils;

import android.content.Context;
import android.content.SharedPreferences.Editor;

public class TrainSpUtils {
    public static void setCustomIntervttentTrain(Context mContext, String fileName) {
        Editor editor = mContext.getApplicationContext().getSharedPreferences("intermittent_train_file_name", 0).edit();
        editor.putString("currnet_selected_intervmittent_train_file", fileName);
        editor.commit();
    }

    public static String getCustomIntervttentTrainFileName(Context mContext) {
        return mContext.getApplicationContext().getSharedPreferences("intermittent_train_file_name", 0).getString("currnet_selected_intervmittent_train_file", "");
    }
}
