package com.huami.watch.newsport.customtrain.utils;

import android.content.Context;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.Transporter.DataListener;

public class IntervalRunDataReceiver implements DataListener {
    public IntervalRunDataReceiver(Context mContext) {
        Transporter tor = Transporter.get(mContext, "com.huami.watch.newsport");
        tor.connectTransportService();
        if (tor != null) {
            tor.addDataListener(this);
        }
    }

    public void onDataReceived(TransportDataItem transportDataItem) {
    }
}
