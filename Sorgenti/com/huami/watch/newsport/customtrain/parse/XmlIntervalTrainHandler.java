package com.huami.watch.newsport.customtrain.parse;

import android.util.Log;
import com.huami.watch.newsport.customtrain.model.CustomIntervalTrain;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlIntervalTrainHandler extends DefaultHandler {
    private static final String TAG = XmlIntervalTrainHandler.class.getSimpleName();
    private CustomIntervalTrain customIntervalTrain;
    private String tagName = null;

    public void startDocument() throws SAXException {
        super.startDocument();
        Log.i(TAG, "startDocument: ");
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        Log.i(TAG, "startElement: ");
        if (localName.equals("intermittent_train")) {
            this.customIntervalTrain = new CustomIntervalTrain();
        }
        this.tagName = localName;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        Log.i(TAG, "characters: ");
        if (this.tagName != null) {
            String data = new String(ch, start, length);
            Log.i(TAG, " data:" + data);
            if (this.tagName.equalsIgnoreCase("title_en")) {
                this.customIntervalTrain.setTitleEn(data);
            } else if (this.tagName.equalsIgnoreCase("title_zh")) {
                this.customIntervalTrain.setTitleZh(data);
            } else if (this.tagName.equalsIgnoreCase("train_type")) {
                this.customIntervalTrain.setTrainType(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("repeat_time")) {
                this.customIntervalTrain.setRepeatTime(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("mileage")) {
                this.customIntervalTrain.setMileage(Double.parseDouble(data));
            } else if (this.tagName.equalsIgnoreCase("intermittent_time")) {
                this.customIntervalTrain.setIntermittentTime(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("remind_type")) {
                this.customIntervalTrain.setRemindType(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("heart_region_remind_type")) {
                this.customIntervalTrain.setHeartRegionRemindType(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("remind_min")) {
                this.customIntervalTrain.setRemindMin(Integer.parseInt(data));
            } else if (this.tagName.equalsIgnoreCase("remind_max")) {
                this.customIntervalTrain.setRemindMax(Integer.parseInt(data));
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        Log.i(TAG, "endElement: ");
        this.tagName = null;
    }

    public void endDocument() throws SAXException {
        super.endDocument();
        Log.i(TAG, "endDocument: ");
    }
}
