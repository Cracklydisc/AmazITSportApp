package com.huami.watch.newsport.customtrain.model;

public class CustomIntervalTrain {
    private int heartRegionRemindType;
    private int intermittentTime;
    private double mileage;
    private int remindMax;
    private int remindMin;
    private int remindType;
    private int repeatTime;
    private String titleEn;
    private String titleZh;
    private int trainType;

    public void setTitleZh(String titleZh) {
        this.titleZh = titleZh;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public void setTrainType(int trainType) {
        this.trainType = trainType;
    }

    public void setRepeatTime(int repeatTime) {
        this.repeatTime = repeatTime;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public void setIntermittentTime(int intermittentTime) {
        this.intermittentTime = intermittentTime;
    }

    public void setRemindType(int remindType) {
        this.remindType = remindType;
    }

    public void setHeartRegionRemindType(int heartRegionRemindType) {
        this.heartRegionRemindType = heartRegionRemindType;
    }

    public void setRemindMin(int remindMin) {
        this.remindMin = remindMin;
    }

    public void setRemindMax(int remindMax) {
        this.remindMax = remindMax;
    }

    public String toString() {
        return "CustomIntervalTrain{titleZh='" + this.titleZh + '\'' + ", titleEn='" + this.titleEn + '\'' + ", trainType=" + this.trainType + ", repeatTime=" + this.repeatTime + ", mileage=" + this.mileage + ", intermittentTime=" + this.intermittentTime + ", remindType=" + this.remindType + ", heartRegionRemindType=" + this.heartRegionRemindType + ", remindMin=" + this.remindMin + ", remindMax=" + this.remindMax + '}';
    }
}
