package com.huami.watch.newsport.recordcache.listener;

import android.graphics.Bitmap;

public interface IRecordGraphCache {
    void onDeleteCacheResult(int i, boolean z);

    void onDrawableFound(long j, Bitmap bitmap, int i);

    void onSaveCacheResult(int i, boolean z);
}
