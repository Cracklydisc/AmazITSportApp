package com.huami.watch.newsport.recordcache.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.View;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.recordcache.listener.IRecordGraphCache;
import java.io.File;
import java.io.IOException;

public class RecordGraphManager {
    private static final String TAG = RecordGraphManager.class.getName();
    private static RecordGraphManager sInstance;
    private Context mContext;
    private IRecordGraphCache mRecordCache;

    public static synchronized RecordGraphManager getInstance(Context context) {
        RecordGraphManager recordGraphManager;
        synchronized (RecordGraphManager.class) {
            synchronized (RecordGraphManager.class) {
                if (sInstance == null) {
                    sInstance = new RecordGraphManager(context.getApplicationContext());
                }
            }
            recordGraphManager = sInstance;
        }
        return recordGraphManager;
    }

    private RecordGraphManager(Context context) {
        this.mContext = context;
    }

    public void setRecordCache(IRecordGraphCache iCache) {
        this.mRecordCache = iCache;
    }

    public boolean isCacheGraphData(long trackId, int graphType) {
        if (this.mContext == null) {
            return false;
        }
        String filePath = getDataFilePath(trackId, graphType);
        if (!DataManager.getInstance().isSportRecordAvaiable(filePath)) {
            Debug.m5i(TAG, "isCacheGraphData, sp not contain sport record");
            return false;
        } else if (filePath == null || filePath.isEmpty()) {
            Debug.m5i(TAG, "isCacheGraphData, filePath is not exist");
            return false;
        } else {
            File file = new File(filePath);
            if (file != null && file.exists() && !file.isDirectory()) {
                return true;
            }
            Debug.m5i(TAG, "isCacheGraphData, file is not exist");
            return false;
        }
    }

    public void saveGraphData2Cache(View view, long trackId, int graphType) {
        if (view != null && this.mContext != null) {
            final View view2 = view;
            final long j = trackId;
            final int i = graphType;
            Global.getGlobalWorkHandler().post(new Runnable() {
                public void run() {
                    int width = view2.getMeasuredWidth();
                    int height = view2.getMeasuredHeight();
                    if (width == 0 || height == 0 || width > 320 || height > 320) {
                        Debug.m5i(RecordGraphManager.TAG, "saveGraphData2Cache failed, width or height is 0, width:" + width + ", height:" + height);
                        return;
                    }
                    Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
                    view2.draw(new Canvas(bitmap));
                    RecordGraphManager.this.saveGraphData2Cache(bitmap, j, i);
                }
            });
        } else if (this.mRecordCache != null) {
            this.mRecordCache.onSaveCacheResult(graphType, false);
        }
    }

    private void saveGraphData2Cache(Bitmap bitmap, long trackId, int graphType) {
        saveGraphData2Cache(bitmap, trackId, graphType, true);
    }

    public synchronized void saveGraphData2Cache(Bitmap bitmap, long trackId, int graphType, boolean isRecycle) {
        if (bitmap != null) {
            if (!(this.mContext == null || isCacheGraphData(trackId, graphType) || bitmap.isRecycled())) {
                Bitmap tmpBitmap = null;
                if (!bitmap.isRecycled()) {
                    tmpBitmap = isRecycle ? bitmap : Bitmap.createBitmap(bitmap);
                }
                final Bitmap finalTmpBitmap = tmpBitmap;
                final long j = trackId;
                final int i = graphType;
                Global.getGlobalWorkHandler().post(new Runnable() {

                    class C05901 implements Runnable {
                        final /* synthetic */ boolean val$isSaveSuccess;

                        C05901(boolean z) {
                            this.val$isSaveSuccess = z;
                        }

                        public void run() {
                            if (RecordGraphManager.this.mRecordCache != null) {
                                RecordGraphManager.this.mRecordCache.onSaveCacheResult(i, this.val$isSaveSuccess);
                            }
                        }
                    }

                    class C05912 implements Runnable {
                        C05912() {
                        }

                        public void run() {
                            if (RecordGraphManager.this.mRecordCache != null) {
                                RecordGraphManager.this.mRecordCache.onSaveCacheResult(i, false);
                            }
                        }
                    }

                    class C05923 implements Runnable {
                        C05923() {
                        }

                        public void run() {
                            if (RecordGraphManager.this.mRecordCache != null) {
                                RecordGraphManager.this.mRecordCache.onSaveCacheResult(i, false);
                            }
                        }
                    }

                    public void run() {
                        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
                        /*
                        r8 = this;
                        r4 = r3;
                        r4 = r4.isRecycled();
                        if (r4 == 0) goto L_0x0009;
                    L_0x0008:
                        return;
                    L_0x0009:
                        r4 = com.huami.watch.newsport.recordcache.controller.RecordGraphManager.this;
                        r6 = r4;
                        r5 = r6;
                        r1 = r4.createDataFileIfNecessary(r6, r5);
                        if (r1 == 0) goto L_0x0093;
                    L_0x0015:
                        r4 = r1.exists();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r4 == 0) goto L_0x0093;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x001b:
                        r3 = new java.io.FileOutputStream;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r3.<init>(r1);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r3;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5 = android.graphics.Bitmap.CompressFormat.PNG;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r6 = 100;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r2 = r4.compress(r5, r6, r3);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r3 == 0) goto L_0x0032;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x002c:
                        r3.flush();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r3.close();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x0032:
                        r5 = com.huami.watch.newsport.recordcache.controller.RecordGraphManager.TAG;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4.<init>();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r6 = "save graph ";	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r4.append(r6);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r6 = r6;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r4.append(r6);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r6 = " is ";	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r6 = r4.append(r6);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r2 == 0) goto L_0x0090;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x004f:
                        r4 = "success";	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x0051:
                        r4 = r6.append(r4);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r4.toString();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        com.huami.watch.common.log.Debug.m5i(r5, r4);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r2 == 0) goto L_0x0069;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x005e:
                        r4 = com.huami.watch.newsport.common.manager.DataManager.getInstance();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5 = r1.getAbsolutePath();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4.saveSportRecord(r5);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x0069:
                        r4 = com.huami.watch.newsport.recordcache.controller.RecordGraphManager.this;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r4.mRecordCache;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r4 == 0) goto L_0x007d;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x0071:
                        r4 = com.huami.watch.newsport.Global.getGlobalUIHandler();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5 = new com.huami.watch.newsport.recordcache.controller.RecordGraphManager$2$1;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5.<init>(r2);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4.post(r5);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x007d:
                        r4 = r3;
                        if (r4 == 0) goto L_0x0008;
                    L_0x0081:
                        r4 = r3;
                        r4 = r4.isRecycled();
                        if (r4 != 0) goto L_0x0008;
                    L_0x0089:
                        r4 = r3;
                        r4.recycle();
                        goto L_0x0008;
                    L_0x0090:
                        r4 = "failed";	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        goto L_0x0051;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x0093:
                        r4 = com.huami.watch.newsport.recordcache.controller.RecordGraphManager.this;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r4.mRecordCache;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        if (r4 == 0) goto L_0x007d;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                    L_0x009b:
                        r4 = com.huami.watch.newsport.Global.getGlobalUIHandler();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5 = new com.huami.watch.newsport.recordcache.controller.RecordGraphManager$2$2;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5.<init>();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4.post(r5);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        goto L_0x007d;
                    L_0x00a8:
                        r0 = move-exception;
                        r4 = com.huami.watch.newsport.Global.getGlobalUIHandler();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5 = new com.huami.watch.newsport.recordcache.controller.RecordGraphManager$2$3;	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r5.<init>();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4.post(r5);	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r0.printStackTrace();	 Catch:{ IOException -> 0x00a8, all -> 0x00cb }
                        r4 = r3;
                        if (r4 == 0) goto L_0x0008;
                    L_0x00bc:
                        r4 = r3;
                        r4 = r4.isRecycled();
                        if (r4 != 0) goto L_0x0008;
                    L_0x00c4:
                        r4 = r3;
                        r4.recycle();
                        goto L_0x0008;
                    L_0x00cb:
                        r4 = move-exception;
                        r5 = r3;
                        if (r5 == 0) goto L_0x00dd;
                    L_0x00d0:
                        r5 = r3;
                        r5 = r5.isRecycled();
                        if (r5 != 0) goto L_0x00dd;
                    L_0x00d8:
                        r5 = r3;
                        r5.recycle();
                    L_0x00dd:
                        throw r4;
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.recordcache.controller.RecordGraphManager.2.run():void");
                    }
                });
            }
        }
        if (this.mRecordCache != null) {
            this.mRecordCache.onSaveCacheResult(graphType, false);
        }
    }

    public void postToSearchGraph(final long trackId, final int graphType) {
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                if (RecordGraphManager.this.mContext != null) {
                    String filepath = RecordGraphManager.this.getDataFilePath(trackId, graphType);
                    File file = new File(filepath);
                    if (file.exists() && file.isFile()) {
                        final Bitmap bitmap = BitmapFactory.decodeFile(filepath);
                        if (RecordGraphManager.this.mRecordCache != null) {
                            Global.getGlobalUIHandler().post(new Runnable() {
                                public void run() {
                                    if (RecordGraphManager.this.mRecordCache != null) {
                                        RecordGraphManager.this.mRecordCache.onDrawableFound(trackId, bitmap, graphType);
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    public void deleteCache(final long trackId) {
        Global.getGlobalWorkHandler().post(new Runnable() {
            public void run() {
                if (RecordGraphManager.this.mContext != null) {
                    boolean isSuccess = false;
                    File file = new File(RecordGraphManager.this.getDataDirectoryPath(trackId));
                    if (file.exists() && file.isDirectory()) {
                        isSuccess = RecordGraphManager.this.delelteRecordDirectory(file);
                    }
                    final boolean finalIsSuccess = isSuccess;
                    Global.getGlobalUIHandler().post(new Runnable() {
                        public void run() {
                            if (RecordGraphManager.this.mRecordCache != null) {
                                RecordGraphManager.this.mRecordCache.onDeleteCacheResult(-1, finalIsSuccess);
                            }
                        }
                    });
                } else if (RecordGraphManager.this.mRecordCache != null) {
                    RecordGraphManager.this.mRecordCache.onDeleteCacheResult(-1, false);
                }
            }
        });
    }

    private boolean delelteRecordDirectory(File file) {
        if (file != null && file.exists() && file.isDirectory()) {
            for (File child : file.listFiles()) {
                if (child.isFile() && !child.delete()) {
                    return false;
                }
                if (child.isDirectory() && !delelteRecordDirectory(child)) {
                    return false;
                }
            }
            if (file.list().length == 0 && !file.delete()) {
                return false;
            }
        }
        return true;
    }

    private File createDataFileIfNecessary(long trackId, int graphType) {
        if (this.mContext == null) {
            Debug.m5i(TAG, "createDataFileIfNecessary, context is null");
            return null;
        }
        String directoryStr = getDataDirectoryPath(trackId);
        Debug.m5i(TAG, "createDataFileIfNecessary, directory:" + directoryStr);
        File directory = new File(directoryStr);
        if (!directory.exists()) {
            Debug.m5i(TAG, "createDataFileIfNecessary, isDirectory created Success:" + directory.mkdirs());
        }
        String dataPath = getDataFilePath(trackId, graphType);
        Debug.m5i(TAG, "createDataFileIfNecessary, dataFile:" + dataPath);
        File dataFile = new File(dataPath);
        if (dataFile.exists()) {
            return dataFile;
        }
        try {
            Debug.m5i(TAG, "createDataFileIfNecessary, isSuccess: " + dataFile.createNewFile());
            return dataFile;
        } catch (IOException e) {
            e.printStackTrace();
            return dataFile;
        }
    }

    private String getDataFilePath(long trackId, int graphType) {
        if (this.mContext == null) {
            return null;
        }
        return this.mContext.getCacheDir() + File.separator + trackId + File.separator + graphType + "_10";
    }

    private String getDataDirectoryPath(long trackId) {
        if (this.mContext == null) {
            return null;
        }
        return this.mContext.getCacheDir() + File.separator + trackId;
    }

    public boolean clearCache() {
        try {
            Runtime.getRuntime().exec("rm -rf " + this.mContext.getCacheDir() + File.separator);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
