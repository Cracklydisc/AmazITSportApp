package com.huami.watch.newsport.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FitnessLevelDBManager {
    private static final String TAG = FitnessLevelDBManager.class.getSimpleName();
    private String DB_NAME = "fitness_level_table.db";
    private Context mContext;

    public int getFitnessLevel(SQLiteDatabase db, int sex, int age, int vo2max) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" select * from fitness_level_table  where ");
        stringBuilder.append("gender=?");
        stringBuilder.append(" and  start_age<=?");
        stringBuilder.append(" and  end_age>=?");
        stringBuilder.append(" and start_vo2_max<=?");
        stringBuilder.append(" and end_vo2_max>=?");
        Cursor cursor = db.rawQuery(stringBuilder.toString(), new String[]{String.valueOf(sex), String.valueOf(age), String.valueOf(age), String.valueOf(vo2max), String.valueOf(vo2max)});
        int fitNessLevel = 0;
        if (cursor.moveToFirst()) {
            fitNessLevel = cursor.getInt(cursor.getColumnIndex("fitness_level"));
        }
        if (cursor != null) {
            cursor.close();
        }
        Log.i(TAG, "-- fitNessLevel:" + fitNessLevel);
        return fitNessLevel;
    }

    public int[] getFitnessLevelArrayByAge(SQLiteDatabase db, int age, int sex) {
        Log.i(TAG, " age:" + age + ",sex:" + sex);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" select * from fitness_level_table  where ");
        stringBuffer.append("start_age <= ?  and ");
        stringBuffer.append("end_age >= ?  and ");
        stringBuffer.append("gender= ?  and ");
        stringBuffer.append("fitness_level<=7  ");
        stringBuffer.append(" order by end_vo2_max asc ");
        Cursor cursor = db.rawQuery(stringBuffer.toString(), new String[]{String.valueOf(age), String.valueOf(age), String.valueOf(sex)});
        int[] results = new int[6];
        int currnetIndex = 0;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int currnetVo2maxValue = cursor.getInt(cursor.getColumnIndex("end_vo2_max"));
                Log.i(TAG, " currnetVo2maxValue:" + currnetVo2maxValue + ",currnetIndex:" + currnetIndex);
                if (currnetIndex < 6) {
                    results[currnetIndex] = currnetVo2maxValue;
                }
                currnetIndex++;
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return results;
    }

    public FitnessLevelDBManager(Context mContext) {
        this.mContext = mContext;
    }

    public SQLiteDatabase initDBManager(String packName) {
        String dbPath = "/data/data/" + packName + "/databases/" + this.DB_NAME;
        Log.i(TAG, " dbPath:" + dbPath);
        if (!new File(dbPath).exists()) {
            try {
                FileOutputStream out = new FileOutputStream(dbPath);
                InputStream in = this.mContext.getAssets().open("db/" + this.DB_NAME);
                byte[] buffer = new byte[1024];
                while (true) {
                    int readBytes = in.read(buffer);
                    if (readBytes == -1) {
                        break;
                    }
                    Log.i(TAG, " db  insert batch ");
                    out.write(buffer, 0, readBytes);
                }
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SQLiteDatabase.openOrCreateDatabase(dbPath, null);
    }
}
