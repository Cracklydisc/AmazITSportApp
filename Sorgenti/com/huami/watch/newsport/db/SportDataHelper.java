package com.huami.watch.newsport.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.BaseColumns;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SportDataHelper extends SQLiteOpenHelper {
    private static final String TAG = SportDataHelper.class.getSimpleName();
    private static SportDataHelper sInstance = null;
    private int WAKE_LOCK_TIMEOUT = 300000;
    private Context mContext = null;
    private WakeLock mWakeLock = null;

    public static class CadenceDetailEntry implements BaseColumns {
    }

    public static class DailyPerpormanceInfoEntity implements BaseColumns {
    }

    public static class FitNessLevelEntity implements BaseColumns {
    }

    public static class HeartRateEntry implements BaseColumns {
    }

    public static class MedalEntry implements BaseColumns {
    }

    public static class RealTimeGuideEntity implements BaseColumns {
    }

    public static class RequestEntry implements BaseColumns {
    }

    public static class RunningInfoPerKMEntry implements BaseColumns {
    }

    public static class RunningInfoPerLapEntry implements BaseColumns {
    }

    public static class SportLocationDataEntry implements BaseColumns {
    }

    public static class SportSummaryEntry implements BaseColumns {
    }

    public static class SportThaInfoEntity implements BaseColumns {
    }

    public static class Vo2MaxDayInfoEntity implements BaseColumns {
    }

    public static synchronized SportDataHelper getInstance(Context context) {
        SportDataHelper sportDataHelper;
        synchronized (SportDataHelper.class) {
            if (sInstance == null) {
                synchronized (SportDataHelper.class) {
                    sInstance = new SportDataHelper(context);
                }
            }
            sportDataHelper = sInstance;
        }
        return sportDataHelper;
    }

    public SportDataHelper(Context context) {
        super(context, "sport_data.db", null, 12);
        this.mContext = context.getApplicationContext();
        this.mWakeLock = ((PowerManager) this.mContext.getSystemService("power")).newWakeLock(1, "update_db");
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE sport_summary(track_id INTEGER PRIMARY KEY, type INTEGER, parent_trackid INTEGER, start_time INTEGER, end_time INTEGER, calorie REAL, current_status INTEGER, content TEXT)");
        db.execSQL("CREATE TABLE location_data(track_id INTEGER, point_index INTEGER, latitude REAL, longitude REAL, timestamp INTEGER, bar INTEGER, altitude REAL, accuracy REAL, speed REAL, algo_point_type INTEGER, course REAL, extra TEXT, point_type INTEGER)");
        db.execSQL("CREATE TABLE running_info_per_km(track_id INTEGER, pace REAL, lat REAL, lng REAL, cost_time INTEGER, total_cost_time INTEGER, unit INTEGER, speed REAL ,tx_value INTEGER, daily_perpormence REAL )");
        db.execSQL("CREATE TABLE heart_rate(track_id INTEGER, rate REAL, step_freq REAL, altitude REAL, disdiff REAL, pace INTEGER, heart_quality INTEGER, step_count INTEGER, stride INTEGER, run_time INTEGER, heart_range INTEGER, stroke_speed REAL, time INTEGER)");
        db.execSQL("CREATE TABLE request(request_stat INTEGER, track_id INTEGER, request_action TEXT, id TEXT)");
        db.execSQL("CREATE TABLE medal(id INTEGER, is_active INTEGER)");
        db.execSQL("CREATE TABLE running_info_per_lap(track_id INTEGER, pace REAL, cost_time INTEGER,distance REAL, lap_num INTEGER,latitude REAL,longitude REAL,avg_heart REAL,total_cost_time INTEGER,absolute_altitude REAL,climb_up REAL,max_pace REAL,climb_distance REAL,lap_stroke_speed REAL,lap_strokes INTEGER,lap_swolf INTEGER,lap_calories INTEGER,lap_step_freq INTEGER,lap_cadence INTEGER,lap_type INTEGER,climb_down REAL)");
        db.execSQL("CREATE TABLE cadence_detail(track_id INTEGER, cur_time INTEGER, cur_speed REAL, cur_distance REAL, duration REAL, cur_cadence REAL, temperature INTEGER )");
        db.execSQL("CREATE TABLE IF NOT EXISTS  sport_tha_info(dayId INTEGER PRIMARY KEY, wtlSum INTEGER, wtlSumOptimalMax INTEGER, wtlSumOptimalMin INTEGER, wtlSumOverreaching INTEGER, vo2MaxTread INTEGER, trainingLoadTrend INTEGER, wtlStatus INTEGER, fitnessClass INTEGER, fitnessLevelIncrease INTEGER, updateTime INTEGER, currnetDayTrainLoad  INTEGER ,sportVo2Max REAL ,rideVo2Max  REAL,upload_status INTEGER, dateStr TEXT  )");
        db.execSQL("CREATE TABLE  IF NOT EXISTS  daily_perpormance_info(track_id INTEGER, te_value INTEGER, daily_performance INTEGER, create_time INTEGER, currnet_kilos REAL, save_data_type INTEGER)");
        db.execSQL("CREATE TABLE  IF NOT EXISTS  vo2_max_day_info(dayId INTEGER  PRIMARY KEY , vo2_max_run INTEGER, vo2_max_walking INTEGER, upload_status INTEGER, update_time INTEGER)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Debug.m3d(TAG, "data base upgrade from " + oldVersion + " to " + newVersion);
        this.mWakeLock.acquire((long) this.WAKE_LOCK_TIMEOUT);
        upgrade(db, oldVersion, newVersion);
        this.mWakeLock.release();
        Debug.m3d(TAG, "data base upgrade finished");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void upgrade(android.database.sqlite.SQLiteDatabase r1, int r2, int r3) {
        /*
        r0 = this;
        switch(r2) {
            case 1: goto L_0x0004;
            case 2: goto L_0x0007;
            case 3: goto L_0x000a;
            case 4: goto L_0x000d;
            case 5: goto L_0x0010;
            case 6: goto L_0x0013;
            case 7: goto L_0x0016;
            case 8: goto L_0x0019;
            case 9: goto L_0x001c;
            case 10: goto L_0x001f;
            case 11: goto L_0x0025;
            default: goto L_0x0003;
        };
    L_0x0003:
        return;
    L_0x0004:
        r0.updateFrom1To2(r1);
    L_0x0007:
        r0.addColumnInSportThaInfoTableFrom2To3(r1);
    L_0x000a:
        r0.addColumnSportOrRideVO2Max(r1);
    L_0x000d:
        r0.updateSummaryFrom4To5(r1);
    L_0x0010:
        r0.updateSummaryFrom5To6(r1);
    L_0x0013:
        r0.updateSummaryFrom6To7(r1);
    L_0x0016:
        r0.updatePerKmFrom7To8(r1);
    L_0x0019:
        r0.updateDailyPropermanceInfoTableFrom8To9(r1);
    L_0x001c:
        r0.updateVo2MaxInfoTableFrom9To10(r1);
    L_0x001f:
        r0.addColumnUpdateStatusInVo2MaxDayInfoFrom10To11(r1);
        r0.addColumnUpdateStatusInSportThaInfoFrom10To11(r1);
    L_0x0025:
        r0.addColumnCurrnetKilosInDailyPerformaneceTableFrom11To12(r1);
        goto L_0x0003;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.SportDataHelper.upgrade(android.database.sqlite.SQLiteDatabase, int, int):void");
    }

    private void addColumnCurrnetKilosInDailyPerformaneceTableFrom11To12(SQLiteDatabase db) {
        if (!checkColumnExist(db, "daily_perpormance_info", "currnet_kilos")) {
            db.execSQL(" alter table daily_perpormance_info  add currnet_kilos  REAL ");
        }
    }

    private void updateVo2MaxInfoTableFrom9To10(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE  IF NOT EXISTS  vo2_max_day_info(dayId INTEGER  PRIMARY KEY , vo2_max_run INTEGER, vo2_max_walking INTEGER, upload_status INTEGER, update_time INTEGER)");
    }

    private void updateDailyPropermanceInfoTableFrom8To9(SQLiteDatabase db) {
        Debug.m5i(TAG, "updateDailyPropermanceInfoTableFrom8To9");
        db.execSQL("CREATE TABLE  IF NOT EXISTS  daily_perpormance_info(track_id INTEGER, te_value INTEGER, daily_performance INTEGER, create_time INTEGER, currnet_kilos REAL, save_data_type INTEGER)");
    }

    private void updatePerKmFrom7To8(SQLiteDatabase db) {
        Debug.m5i(TAG, "updatePerKmFrom7To8");
        db.execSQL(" update running_info_per_km  set lat=10000, lng=10000");
    }

    private void updateSummaryFrom6To7(SQLiteDatabase db) {
        Debug.m5i(TAG, "updateSummaryFrom6To7");
        List<SportSummary> sportSummaryList = new ArrayList();
        Cursor cursor = db.query("sport_summary", null, null, null, null, null, null);
        while (cursor != null && cursor.moveToNext()) {
            String content = cursor.getString(cursor.getColumnIndex("content"));
            int currentStatus = cursor.getInt(cursor.getColumnIndex("current_status"));
            SportSummary summary = OutdoorSportSummary.createFromJSON(content);
            summary.setCurrentStatus(currentStatus);
            try {
                JSONObject childListJSON = new JSONObject(content).optJSONObject("child_list");
                List<Long> trackIds = new ArrayList();
                List<Integer> types = new ArrayList();
                if (childListJSON != null) {
                    int i;
                    JSONArray childTrackIdsJSON = childListJSON.optJSONArray("trackids");
                    JSONArray childTypesJSON = childListJSON.optJSONArray("types");
                    if (childTrackIdsJSON != null) {
                        for (i = 0; i < childTrackIdsJSON.length(); i++) {
                            trackIds.add(Long.valueOf(childTrackIdsJSON.optLong(i)));
                        }
                    }
                    if (childTypesJSON != null) {
                        for (i = 0; i < childTypesJSON.length(); i++) {
                            types.add(Integer.valueOf(childTypesJSON.optInt(i)));
                        }
                    }
                    summary.clearChildTrackIdsAndTypes();
                    for (i = 0; i < trackIds.size(); i++) {
                        summary.addChildSportSummary(((Long) trackIds.get(i)).longValue(), ((Integer) types.get(i)).intValue());
                    }
                } else {
                    Debug.m6w(TAG, "child summarys info is null");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (summary != null) {
                summary.setCurrentStatus(currentStatus);
                sportSummaryList.add(summary);
            }
        }
        cursor.close();
        SportSummaryDao sportSummaryDao = SportSummaryDao.getInstance(this.mContext);
        for (SportSummary sportSummary : sportSummaryList) {
            Debug.m5i(TAG, "isSuccess:" + sportSummaryDao.update(this.mContext, db, sportSummary) + ", trackId:" + sportSummary.getTrackId());
        }
    }

    private void updateSummaryFrom5To6(SQLiteDatabase db) {
        Debug.m5i(TAG, "updateSummaryFrom5To6");
        if (!checkColumnExist(db, "sport_summary", "parent_trackid")) {
            db.execSQL(" alter table sport_summary  add parent_trackid  INTEGER DEFAULT -1");
            SportSummaryDao sportSummaryDao = SportSummaryDao.getInstance(this.mContext);
            List<SportSummary> sportSummaryList = sportSummaryDao.selectAll(this.mContext, db, null, null, null, null);
            if (sportSummaryList != null) {
                Map<Long, Long> childSummaryMap = new HashMap();
                List<SportSummary> childSumaryList = new ArrayList();
                for (SportSummary sportSummary : sportSummaryList) {
                    if (SportType.isMixedSport(sportSummary.getSportType())) {
                        List<Long> childTrackIdList = sportSummary.getChildSportSummrys();
                        if (childTrackIdList != null) {
                            for (Long longValue : childTrackIdList) {
                                childSummaryMap.put(Long.valueOf(longValue.longValue()), Long.valueOf(sportSummary.getTrackId()));
                            }
                        }
                    } else if (SportType.isChildSport(sportSummary.getSportType())) {
                        childSumaryList.add(sportSummary);
                    }
                }
                if (childSumaryList != null) {
                    for (SportSummary sportSummary2 : childSumaryList) {
                        Long parentTrackId = (Long) childSummaryMap.get(Long.valueOf(sportSummary2.getTrackId()));
                        if (parentTrackId != null) {
                            sportSummary2.setParentTrackId(parentTrackId.longValue());
                            Debug.m5i(TAG, "updateSummaryFrom5To6, trackId:" + sportSummary2.getTrackId() + ", isSuccess:" + sportSummaryDao.update(this.mContext, db, sportSummary2));
                        }
                    }
                }
            }
        }
    }

    public void addColumnSportOrRideVO2Max(SQLiteDatabase db) {
        Debug.m5i(TAG, "addColumnSportOrRideVO2Max");
        if (!checkColumnExist(db, "sport_tha_info", "sportVo2Max")) {
            db.execSQL(" alter table sport_tha_info  add sportVo2Max  REAL ");
        }
        if (!checkColumnExist(db, "sport_tha_info", "rideVo2Max")) {
            db.execSQL(" alter table sport_tha_info  add rideVo2Max  REAL ");
        }
    }

    public void addColumnUpdateStatusInVo2MaxDayInfoFrom10To11(SQLiteDatabase db) {
        if (!checkColumnExist(db, "vo2_max_day_info", "upload_status")) {
            db.execSQL(" alter table vo2_max_day_info  add upload_status  INTEGER ");
        }
    }

    public void addColumnUpdateStatusInSportThaInfoFrom10To11(SQLiteDatabase db) {
        if (!checkColumnExist(db, "sport_tha_info", "upload_status")) {
            db.execSQL(" alter table sport_tha_info  add upload_status  INTEGER ");
        }
    }

    private void updateFrom1To2(SQLiteDatabase db) {
        Debug.m5i(TAG, "updateFrom1To2");
        db.execSQL("CREATE TABLE IF NOT EXISTS  sport_tha_info(dayId INTEGER PRIMARY KEY, wtlSum INTEGER, wtlSumOptimalMax INTEGER, wtlSumOptimalMin INTEGER, wtlSumOverreaching INTEGER, vo2MaxTread INTEGER, trainingLoadTrend INTEGER, wtlStatus INTEGER, fitnessClass INTEGER, fitnessLevelIncrease INTEGER, updateTime INTEGER, currnetDayTrainLoad  INTEGER ,sportVo2Max REAL ,rideVo2Max  REAL,upload_status INTEGER, dateStr TEXT  )");
    }

    private void addColumnInSportThaInfoTableFrom2To3(SQLiteDatabase db) {
        Debug.m5i(TAG, "addColumnInSportThaInfoTableFrom2To3");
        if (!checkColumnExist(db, "sport_tha_info", "currnetDayTrainLoad")) {
            db.execSQL(" alter table sport_tha_info  add currnetDayTrainLoad  INTEGER ");
        }
    }

    private void updateSummaryFrom4To5(SQLiteDatabase db) {
        Debug.m5i(TAG, "updateSummaryFrom4To5");
        List<SportSummary> sportSummaryList = new ArrayList();
        Cursor cursor = db.query("sport_summary", null, null, null, null, null, null);
        while (cursor != null && cursor.moveToNext()) {
            String content = cursor.getString(cursor.getColumnIndex("content"));
            int currentStatus = cursor.getInt(cursor.getColumnIndex("current_status"));
            SportSummary summary = OutdoorSportSummary.createFromJSON(content);
            try {
                JSONObject jSONObject = new JSONObject(content);
                int flag = jSONObject.optInt("sport_flag", 0);
                int sportType = summary.getSportType();
                if (sportType == 16) {
                    sportType = 2001;
                }
                if (flag == 1) {
                    if (sportType == 1) {
                        sportType = 1001;
                    } else if (sportType == 9) {
                        sportType = 1009;
                    } else {
                        sportType = 1015;
                    }
                }
                summary.setSportType(sportType);
                JSONArray childSummarys = jSONObject.optJSONArray("child_summary");
                if (childSummarys != null) {
                    summary.clearChildTrackIdsAndTypes();
                    for (int i = 0; i < childSummarys.length(); i++) {
                        try {
                            int childSportType;
                            Long trackId = Long.valueOf(childSummarys.getLong(i));
                            if (i == 0) {
                                childSportType = 1015;
                            } else if (i == 1) {
                                childSportType = 1009;
                            } else {
                                childSportType = 1001;
                            }
                            summary.addChildSportSummary(trackId.longValue(), childSportType);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (summary != null) {
                        summary.setCurrentStatus(currentStatus);
                        sportSummaryList.add(summary);
                    }
                } else {
                    Debug.m6w(TAG, "child summarys info is null");
                    if (summary != null) {
                        summary.setCurrentStatus(currentStatus);
                        sportSummaryList.add(summary);
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        cursor.close();
        db.execSQL("DROP TABLE IF EXISTS sport_summary");
        db.execSQL("CREATE TABLE sport_summary(track_id INTEGER PRIMARY KEY, type INTEGER, parent_trackid INTEGER, start_time INTEGER, end_time INTEGER, calorie REAL, current_status INTEGER, content TEXT)");
        Debug.m5i(TAG, "isSuccess:" + SportSummaryDao.getInstance(this.mContext).insertAll(this.mContext, db, sportSummaryList));
    }

    private boolean checkColumnExist(SQLiteDatabase db, String tableName, String columnName) {
        boolean result = false;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("SELECT * FROM " + tableName + " LIMIT 0", null);
            result = (cursor == null || cursor.getColumnIndex(columnName) == -1) ? false : true;
            if (!(cursor == null || cursor.isClosed())) {
                cursor.close();
            }
        } catch (Exception e) {
            Debug.m4e(TAG, "checkColumnExists..." + e.getMessage());
            if (!(cursor == null || cursor.isClosed())) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (!(cursor == null || cursor.isClosed())) {
                cursor.close();
            }
        }
        Debug.m4e(TAG, "checkColumnExist, isExist:" + result);
        return result;
    }
}
