package com.huami.watch.newsport.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.huami.watch.newsport.common.model.RealTimeGuidanceModel;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class RealTimeGuideDBManager {
    private static final String TAG = RealTimeGuideDBManager.class.getSimpleName();
    private Context mContext;

    public RealTimeGuideDBManager(Context mContext) {
        this.mContext = mContext;
    }

    public SQLiteDatabase initDBManager(String packName) {
        String dbPath = "/data/data/" + packName + "/databases/" + "real_time_guidance_table.db";
        Log.i(TAG, " dbPath:" + dbPath);
        if (!new File(dbPath).exists()) {
            try {
                FileOutputStream out = new FileOutputStream(dbPath);
                InputStream in = this.mContext.getAssets().open("db/real_time_guidance_table.db");
                byte[] buffer = new byte[1024];
                while (true) {
                    int readBytes = in.read(buffer);
                    if (readBytes == -1) {
                        break;
                    }
                    Log.i(TAG, " db  insert batch ");
                    out.write(buffer, 0, readBytes);
                }
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SQLiteDatabase.openOrCreateDatabase(dbPath, null);
    }

    public RealTimeGuidanceModel selectItemByPhraseNumber(SQLiteDatabase db, int phraseNumber) {
        RealTimeGuidanceModel model = null;
        Cursor cursor = db.rawQuery(" select *  from real_time_guidance_table where phrase_number = ?", new String[]{String.valueOf(phraseNumber)});
        if (cursor.moveToFirst()) {
            model = new RealTimeGuidanceModel();
            int index = cursor.getColumnIndex("phrase_number");
            if (index > -1) {
                model.setPhraseNumber(cursor.getInt(index));
            }
            index = cursor.getColumnIndex("guide_desc");
            if (index > -1) {
                model.setGuideDesc(cursor.getString(index));
            }
            index = cursor.getColumnIndex("guide_title");
            if (index > -1) {
                model.setGuideTitle(cursor.getString(index));
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        if (db != null) {
            db.close();
        }
        return model;
    }
}
