package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class RunningInfoPerKMDao extends Dao<RunningInfoPerKM> {
    private static RunningInfoPerKMDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    public java.util.List<com.huami.watch.newsport.common.model.RunningInfoPerKM> selectAll(android.content.Context r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String r18) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:18:0x0104]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r1 = "RunningInfoPerKMDao";
        r2 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r12 = 0;
    L_0x000a:
        return r12;
    L_0x000b:
        r1 = r13.mDBHelper;
        r0 = r1.getReadableDatabase();
        r9 = 0;
        r1 = "running_info_per_km";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = 10;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 0;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "cost_time";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 1;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "pace";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 2;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "speed";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 3;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "track_id";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "total_cost_time";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 5;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "lat";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 6;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "lng";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 7;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "unit";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 8;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "tx_value";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = 9;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = "daily_perpormence";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r5 = 0;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r6 = 0;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r3 = r15;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r4 = r16;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r7 = r17;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r8 = r18;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r12 = new java.util.LinkedList;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r12.<init>();	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
    L_0x005e:
        r1 = r9.moveToNext();	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        if (r1 == 0) goto L_0x0102;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
    L_0x0064:
        r11 = new com.huami.watch.newsport.common.model.RunningInfoPerKM;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.<init>();	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "cost_time";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setCostTime(r2);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "pace";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = r9.getDouble(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setPace(r2);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "speed";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = r9.getDouble(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setSpeed(r2);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "track_id";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setTrackId(r2);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "total_cost_time";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setTotalCostTime(r2);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "lat";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setLatitude(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "lng";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setLongitude(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "unit";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setUnit(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "tx_value";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setTeValue(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = "daily_perpormence";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r11.setDailyPorpermence(r1);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        r12.add(r11);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        goto L_0x005e;
    L_0x00f0:
        r10 = move-exception;
        r1 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        if (r1 == 0) goto L_0x00fa;	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
    L_0x00f5:
        r1 = "RunningInfoPerKMDao";	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
        com.huami.watch.common.log.Debug.printException(r1, r10);	 Catch:{ Exception -> 0x00f0, all -> 0x0109 }
    L_0x00fa:
        if (r9 == 0) goto L_0x00ff;
    L_0x00fc:
        r9.close();
    L_0x00ff:
        r12 = 0;
        goto L_0x000a;
    L_0x0102:
        if (r9 == 0) goto L_0x000a;
    L_0x0104:
        r9.close();
        goto L_0x000a;
    L_0x0109:
        r1 = move-exception;
        if (r9 == 0) goto L_0x010f;
    L_0x010c:
        r9.close();
    L_0x010f:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.RunningInfoPerKMDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.RunningInfoPerKM>");
    }

    private RunningInfoPerKMDao(Context applicationContext) {
        this.mDBHelper = SportDataHelper.getInstance(applicationContext);
    }

    public static synchronized RunningInfoPerKMDao getInstance(Context context) {
        RunningInfoPerKMDao runningInfoPerKMDao;
        synchronized (RunningInfoPerKMDao.class) {
            if (sInstance == null) {
                sInstance = new RunningInfoPerKMDao(context.getApplicationContext());
            }
            runningInfoPerKMDao = sInstance;
        }
        return runningInfoPerKMDao;
    }

    public boolean insert(Context context, RunningInfoPerKM runningInfoPerKM) {
        if (context == null) {
            Debug.m6w("RunningInfoPerKMDao", "context should not be null while insert");
            return false;
        } else if (runningInfoPerKM == null) {
            Debug.m6w("RunningInfoPerKMDao", "running info per km is null while insert");
            return false;
        } else {
            if (this.mDBHelper.getWritableDatabase().insert("running_info_per_km", null, createContentValues(runningInfoPerKM)) >= 0) {
                return true;
            }
            return false;
        }
    }

    private ContentValues createContentValues(RunningInfoPerKM runningInfoPerKM) {
        ContentValues values = new ContentValues(4);
        values.put("track_id", Long.valueOf(runningInfoPerKM.getTrackId()));
        values.put("cost_time", Long.valueOf(runningInfoPerKM.getCostTime()));
        values.put("pace", Double.valueOf(runningInfoPerKM.getPace()));
        values.put("speed", Double.valueOf(runningInfoPerKM.getSpeed()));
        values.put("total_cost_time", Long.valueOf(runningInfoPerKM.getTotalCostTime()));
        values.put("lat", Float.valueOf(runningInfoPerKM.getLatitude()));
        values.put("lng", Float.valueOf(runningInfoPerKM.getLongitude()));
        values.put("unit", Integer.valueOf(runningInfoPerKM.getUnit()));
        values.put("tx_value", Integer.valueOf(runningInfoPerKM.getTeValue()));
        values.put("daily_perpormence", Float.valueOf(runningInfoPerKM.getDailyPorpermence()));
        return values;
    }

    public boolean insertAll(Context context, List<? extends RunningInfoPerKM> runningInfoPerKMs) {
        if (context == null) {
            Debug.m6w("RunningInfoPerKMDao", "context should not be null while insert all");
            return false;
        } else if (runningInfoPerKMs == null) {
            Debug.m6w("RunningInfoPerKMDao", "running info per km is null while insert all");
            return false;
        } else {
            SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
            long rowId = -1;
            db.beginTransaction();
            for (RunningInfoPerKM runningInfoPerKM : runningInfoPerKMs) {
                rowId = db.insert("running_info_per_km", null, createContentValues(runningInfoPerKM));
                if (rowId < 0) {
                    break;
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId >= 0) {
                return true;
            }
            return false;
        }
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("RunningInfoPerKMDao", "context should not be null while delete");
            return false;
        }
        Debug.m3d("RunningInfoPerKMDao", "" + this.mDBHelper.getWritableDatabase().delete("running_info_per_km", whereClause, whereClauseArg) + " deleted in running info table");
        return true;
    }

    protected Cursor selectAsCursor(String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        return this.mDBHelper.getReadableDatabase().query("running_info_per_km", null, whereClause, whereClauseArg, null, null, orderBy, limit);
    }

    public Cursor selectAsCursorBySql(String sql, String[] selectionArgs) {
        return this.mDBHelper.getReadableDatabase().rawQuery(sql, selectionArgs);
    }
}
