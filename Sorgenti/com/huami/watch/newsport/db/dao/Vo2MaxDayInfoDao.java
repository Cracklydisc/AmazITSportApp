package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class Vo2MaxDayInfoDao extends Dao<Vo2maxDayInfo> {
    private static final String TAG = Vo2MaxDayInfoDao.class.getSimpleName();
    private static Vo2MaxDayInfoDao sInstance;
    private SportDataHelper mDBHelper = null;

    protected java.util.List<? extends com.huami.watch.newsport.common.model.Vo2maxDayInfo> selectAll(android.content.Context r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String r18) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r1 = TAG;
        r2 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r12 = 0;
    L_0x000a:
        return r12;
    L_0x000b:
        r1 = r13.mDBHelper;
        r0 = r1.getReadableDatabase();
        r10 = 0;
        r1 = "vo2_max_day_info";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2 = 5;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = 0;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = "dayId";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = 1;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = "vo2_max_run";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = 2;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = "vo2_max_walking";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = 3;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = "update_time";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = 4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = "upload_status";	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r5 = 0;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r6 = 0;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r3 = r15;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r4 = r16;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r7 = r17;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r8 = r18;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r10 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r12 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r12.<init>();	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r9 = 0;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
    L_0x0043:
        r1 = r10.moveToNext();	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        if (r1 == 0) goto L_0x005c;	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
    L_0x0049:
        r9 = r13.getVo2maxDayInfoByCursor(r10);	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        r12.add(r9);	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        goto L_0x0043;
    L_0x0051:
        r11 = move-exception;
        r11.printStackTrace();	 Catch:{ Exception -> 0x0051, all -> 0x0062 }
        if (r10 == 0) goto L_0x005a;
    L_0x0057:
        r10.close();
    L_0x005a:
        r12 = 0;
        goto L_0x000a;
    L_0x005c:
        if (r10 == 0) goto L_0x000a;
    L_0x005e:
        r10.close();
        goto L_0x000a;
    L_0x0062:
        r1 = move-exception;
        if (r10 == 0) goto L_0x0068;
    L_0x0065:
        r10.close();
    L_0x0068:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.Vo2MaxDayInfoDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<? extends com.huami.watch.newsport.common.model.Vo2maxDayInfo>");
    }

    public Vo2MaxDayInfoDao(Context context) {
        this.mDBHelper = SportDataHelper.getInstance(context);
    }

    public static synchronized Vo2MaxDayInfoDao getInstance(Context context) {
        Vo2MaxDayInfoDao vo2MaxDayInfoDao;
        synchronized (Vo2MaxDayInfoDao.class) {
            if (sInstance == null) {
                sInstance = new Vo2MaxDayInfoDao(context.getApplicationContext());
            }
            vo2MaxDayInfoDao = sInstance;
        }
        return vo2MaxDayInfoDao;
    }

    protected boolean insert(Context context, Vo2maxDayInfo vo2maxDayInfo) {
        if (vo2maxDayInfo == null) {
            return false;
        }
        ContentValues values = createContentValues(vo2maxDayInfo);
        if (values == null) {
            return false;
        }
        SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
        if (db.insertWithOnConflict("vo2_max_day_info", null, values, 4) == -1) {
            Vo2maxDayInfo dataInDB = getCurrnetVo2MaxDayInfo(String.valueOf(vo2maxDayInfo.getDayId()));
            if (dataInDB == null) {
                return false;
            }
            if (dataInDB.getVo2maxRun() > 0.0f && vo2maxDayInfo.getVo2maxRun() <= 0.0f) {
                vo2maxDayInfo.setVo2maxRun(dataInDB.getVo2maxRun());
            }
            if (dataInDB.getVo2MaxWalking() > 0.0f && vo2maxDayInfo.getVo2MaxWalking() <= 0.0f) {
                vo2maxDayInfo.setVo2MaxWalking(dataInDB.getVo2MaxWalking());
            }
            Log.i(TAG, " update rowOffset: " + db.update("vo2_max_day_info", createContentValues(vo2maxDayInfo), "dayId=?", new String[]{String.valueOf(vo2maxDayInfo.getDayId())}));
        } else {
            Log.i(TAG, " insert ");
        }
        return true;
    }

    protected boolean update(Context context, Vo2maxDayInfo vo2maxDayInfo) {
        return update(context, this.mDBHelper.getWritableDatabase(), vo2maxDayInfo);
    }

    protected boolean insertAll(Context context, List<? extends Vo2maxDayInfo> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list != null) {
            return insertAll(context, this.mDBHelper.getWritableDatabase(), list);
        } else {
            Debug.m6w(TAG, "sport summary is null while insert all");
            return false;
        }
    }

    private boolean insertAll(Context context, SQLiteDatabase db, List<? extends Vo2maxDayInfo> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list == null) {
            Debug.m6w(TAG, " Vo2maxDayInfo is null while insert all");
            return false;
        } else {
            db.beginTransaction();
            long rowId = -1;
            for (Vo2maxDayInfo vo2maxDayInfo : list) {
                ContentValues values = createContentValues(vo2maxDayInfo);
                if (values != null) {
                    rowId = db.replace("vo2_max_day_info", null, values);
                    if (rowId < 0) {
                        break;
                    }
                }
                return false;
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId > 0) {
                return true;
            }
            return false;
        }
    }

    protected Vo2maxDayInfo select(Context context, String whereClause, String[] whereClauseArg) {
        return selectOneItemRawQuery(whereClause, whereClauseArg);
    }

    private boolean update(Context context, SQLiteDatabase db, Vo2maxDayInfo vo2maxDayInfo) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while update");
            return false;
        } else if (vo2maxDayInfo == null) {
            Debug.m6w(TAG, " vo2maxDayInfo is null while update");
            return false;
        } else {
            Debug.m3d(TAG, "" + db.update("vo2_max_day_info", createContentValues(vo2maxDayInfo), "dayId=?", new String[]{"" + vo2maxDayInfo.getDayId()}) + " rows affect");
            return true;
        }
    }

    private Vo2maxDayInfo getCurrnetVo2MaxDayInfo(String currentDayId) {
        return selectOneItemRawQuery("select * from vo2_max_day_info where dayId=? ; ", new String[]{currentDayId});
    }

    private Vo2maxDayInfo selectOneItemRawQuery(String sqlStr, String[] selectionArgs) {
        Cursor cursor = this.mDBHelper.getReadableDatabase().rawQuery(sqlStr, selectionArgs);
        Vo2maxDayInfo vo2MaxDayInfo = null;
        if (cursor.moveToNext()) {
            vo2MaxDayInfo = new Vo2maxDayInfo();
            int index = cursor.getColumnIndex("dayId");
            if (index >= 0) {
                vo2MaxDayInfo.setDayId(cursor.getLong(index));
            }
            index = cursor.getColumnIndex("vo2_max_run");
            if (index >= 0) {
                vo2MaxDayInfo.setVo2maxRun(cursor.getFloat(index));
            }
            index = cursor.getColumnIndex("vo2_max_walking");
            if (index >= 0) {
                vo2MaxDayInfo.setVo2MaxWalking(cursor.getFloat(index));
            }
            index = cursor.getColumnIndex("update_time");
            if (index >= 0) {
                vo2MaxDayInfo.setUpdateTime(cursor.getLong(index));
            }
            index = cursor.getColumnIndex("upload_status");
            if (index >= 0) {
                vo2MaxDayInfo.setUploadStatus(cursor.getInt(index));
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return vo2MaxDayInfo;
    }

    private Vo2maxDayInfo getVo2maxDayInfoByCursor(Cursor cursor) {
        Vo2maxDayInfo vo2MaxDayInfo = new Vo2maxDayInfo();
        int index = cursor.getColumnIndex("dayId");
        if (index >= 0) {
            vo2MaxDayInfo.setDayId(cursor.getLong(index));
        }
        index = cursor.getColumnIndex("vo2_max_run");
        if (index >= 0) {
            vo2MaxDayInfo.setVo2maxRun(cursor.getFloat(index));
        }
        index = cursor.getColumnIndex("vo2_max_walking");
        if (index >= 0) {
            vo2MaxDayInfo.setVo2MaxWalking(cursor.getFloat(index));
        }
        index = cursor.getColumnIndex("update_time");
        if (index >= 0) {
            vo2MaxDayInfo.setUpdateTime(cursor.getLong(index));
        }
        index = cursor.getColumnIndex("upload_status");
        if (index >= 0) {
            vo2MaxDayInfo.setUploadStatus(cursor.getInt(index));
        }
        return vo2MaxDayInfo;
    }

    private ContentValues createContentValues(Vo2maxDayInfo vo2maxDayInfo) {
        ContentValues values = new ContentValues();
        values.put("dayId", Long.valueOf(vo2maxDayInfo.getDayId()));
        values.put("vo2_max_run", Float.valueOf(vo2maxDayInfo.getVo2maxRun()));
        values.put("vo2_max_walking", Float.valueOf(vo2maxDayInfo.getVo2MaxWalking()));
        values.put("update_time", Long.valueOf(vo2maxDayInfo.getUpdateTime()));
        values.put("upload_status", Integer.valueOf(vo2maxDayInfo.getUploadStatus()));
        return values;
    }
}
