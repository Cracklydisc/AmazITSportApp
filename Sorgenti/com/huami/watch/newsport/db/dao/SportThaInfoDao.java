package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class SportThaInfoDao extends Dao<SportThaInfo> {
    private static final String TAG = SportThaInfoDao.class.getSimpleName();
    private static SportThaInfoDao mInstance;
    private SportDataHelper mDBHelper = null;

    protected java.util.List<? extends com.huami.watch.newsport.common.model.SportThaInfo> selectAll(android.content.Context r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String r18) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r1 = TAG;
        r2 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r11 = 0;
    L_0x000a:
        return r11;
    L_0x000b:
        r1 = r13.mDBHelper;
        r0 = r1.getReadableDatabase();
        r9 = 0;
        r1 = "sport_tha_info";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2 = 16;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 0;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "dayId";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 1;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "wtlSum";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 2;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "wtlSumOptimalMax";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 3;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "wtlSumOptimalMin";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "wtlSumOverreaching";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 5;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "vo2MaxTread";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 6;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "trainingLoadTrend";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 7;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "wtlStatus";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 8;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "fitnessClass";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 9;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "fitnessLevelIncrease";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 10;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "updateTime";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 11;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "dateStr";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 12;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "currnetDayTrainLoad";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 13;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "sportVo2Max";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 14;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "rideVo2Max";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = 15;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = "upload_status";	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r2[r3] = r4;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r5 = 0;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r6 = 0;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r3 = r15;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r4 = r16;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r7 = r17;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r8 = r18;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r11 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r11.<init>();	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r12 = 0;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
    L_0x0083:
        r1 = r9.moveToNext();	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        if (r1 == 0) goto L_0x009d;	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
    L_0x0089:
        r12 = r13.getSportThaInfoByCursor(r9);	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        r11.add(r12);	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        goto L_0x0083;
    L_0x0091:
        r10 = move-exception;
        r10.printStackTrace();	 Catch:{ Exception -> 0x0091, all -> 0x00a4 }
        if (r9 == 0) goto L_0x009a;
    L_0x0097:
        r9.close();
    L_0x009a:
        r11 = 0;
        goto L_0x000a;
    L_0x009d:
        if (r9 == 0) goto L_0x000a;
    L_0x009f:
        r9.close();
        goto L_0x000a;
    L_0x00a4:
        r1 = move-exception;
        if (r9 == 0) goto L_0x00aa;
    L_0x00a7:
        r9.close();
    L_0x00aa:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.SportThaInfoDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<? extends com.huami.watch.newsport.common.model.SportThaInfo>");
    }

    public static SportThaInfoDao getmInstance(Context mContext) {
        if (mInstance == null) {
            synchronized (SportThaInfoDao.class) {
                if (mInstance == null) {
                    mInstance = new SportThaInfoDao(mContext.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    public SportThaInfoDao(Context context) {
        this.mDBHelper = SportDataHelper.getInstance(context);
    }

    protected SportThaInfo select(Context context, String whereClause, String[] whereClauseArg) {
        return selectOneItemRawQuery(whereClause, whereClauseArg);
    }

    protected boolean insertAll(Context context, List<? extends SportThaInfo> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list != null) {
            return insertAll(context, this.mDBHelper.getWritableDatabase(), list);
        } else {
            Debug.m6w(TAG, " SportThaInfo is null while insert all");
            return false;
        }
    }

    private boolean insertAll(Context context, SQLiteDatabase db, List<? extends SportThaInfo> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list == null) {
            Debug.m6w(TAG, " SportThaInfo is null while insert all");
            return false;
        } else {
            db.beginTransaction();
            long rowId = -1;
            for (SportThaInfo sportThaInfo : list) {
                ContentValues values = createContentValues(sportThaInfo);
                if (values != null) {
                    rowId = db.replace("sport_tha_info", null, values);
                    if (rowId < 0) {
                        break;
                    }
                }
                return false;
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId > 0) {
                return true;
            }
            return false;
        }
    }

    protected boolean insert(Context context, SportThaInfo sportThaInfo) {
        if (sportThaInfo == null) {
            return false;
        }
        ContentValues values = createContentValues(sportThaInfo);
        if (values == null) {
            return false;
        }
        SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
        if (db.insertWithOnConflict("sport_tha_info", null, values, 4) == -1) {
            SportThaInfo dataInDB = getCurrnetDaySportThaInfo(sportThaInfo.getDayId());
            if (dataInDB == null) {
                return false;
            }
            Log.i(TAG, " dataInDB:" + dataInDB.toString());
            Log.i(TAG, " update Before:sportThaInfo:" + sportThaInfo.toString());
            sportThaInfo.setCurrnetDayTrainLoad(sportThaInfo.getCurrnetDayTrainLoad() + dataInDB.getCurrnetDayTrainLoad());
            if (sportThaInfo.getSportVo2Max() < 0.0f) {
                sportThaInfo.setSportVo2Max(dataInDB.getSportVo2Max());
            } else if (sportThaInfo.getRideVo2max() < 0.0f) {
                sportThaInfo.setRideVo2max(dataInDB.getRideVo2max());
            }
            Log.i(TAG, " need to update:" + sportThaInfo.toString());
            Log.i(TAG, " update rowOffset: " + db.update("sport_tha_info", createContentValues(sportThaInfo), "dayId=?", new String[]{String.valueOf(sportThaInfo.getDayId())}));
        } else {
            Log.i(TAG, " insert ");
        }
        return true;
    }

    private ContentValues createContentValues(SportThaInfo sportThaInfo) {
        ContentValues values = new ContentValues();
        values.put("dayId", Long.valueOf(sportThaInfo.getDayId()));
        values.put("wtlSum", Integer.valueOf(sportThaInfo.getWtlSum()));
        values.put("wtlSumOptimalMin", Integer.valueOf(sportThaInfo.getWtlSumOptimalMin()));
        values.put("wtlSumOptimalMax", Integer.valueOf(sportThaInfo.getWtlSumOptimalMax()));
        values.put("wtlSumOverreaching", Integer.valueOf(sportThaInfo.getWtlSumOverreaching()));
        values.put("updateTime", Long.valueOf(sportThaInfo.getUpdateTime()));
        values.put("dateStr", sportThaInfo.getDateStr());
        values.put("vo2MaxTread", Integer.valueOf(sportThaInfo.getVo2MaxTread()));
        values.put("trainingLoadTrend", Integer.valueOf(sportThaInfo.getTrainingLoadTrend()));
        values.put("wtlStatus", Integer.valueOf(sportThaInfo.getWtlStatus()));
        values.put("fitnessClass", Integer.valueOf(sportThaInfo.getFitnessClass()));
        values.put("fitnessLevelIncrease", Integer.valueOf(sportThaInfo.getFitnessLevelIncrease()));
        values.put("currnetDayTrainLoad", Integer.valueOf(sportThaInfo.getCurrnetDayTrainLoad()));
        values.put("sportVo2Max", Float.valueOf(sportThaInfo.getSportVo2Max()));
        values.put("rideVo2Max", Float.valueOf(sportThaInfo.getRideVo2max()));
        values.put("upload_status", Integer.valueOf(sportThaInfo.getUploadStatus()));
        return values;
    }

    protected boolean update(Context context, SportThaInfo sportThaInfo) {
        return update(context, this.mDBHelper.getWritableDatabase(), sportThaInfo);
    }

    public boolean isHasDataInDb(Context mContext, SportThaInfo sportThaInfo) {
        ContentValues values = createContentValues(sportThaInfo);
        if (values != null && this.mDBHelper.getWritableDatabase().insertWithOnConflict("sport_tha_info", null, values, 4) == -1) {
            return true;
        }
        return false;
    }

    private boolean update(Context context, SQLiteDatabase db, SportThaInfo sportThaInfo) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while update");
            return false;
        } else if (sportThaInfo == null) {
            Debug.m6w(TAG, " sportThaInfo is null while update");
            return false;
        } else {
            Debug.m3d(TAG, "" + db.update("sport_tha_info", createContentValues(sportThaInfo), "dayId=?", new String[]{"" + sportThaInfo.getDayId()}) + " rows affect");
            return true;
        }
    }

    public SportThaInfo selectOneItemRawQuery(String sqlStr, String[] selectionArgs) {
        Cursor cursor = this.mDBHelper.getReadableDatabase().rawQuery(sqlStr, selectionArgs);
        SportThaInfo trainLoadInfo = null;
        if (cursor.moveToNext()) {
            trainLoadInfo = getSportThaInfoByCursor(cursor);
        }
        if (cursor != null) {
            cursor.close();
        }
        return trainLoadInfo;
    }

    private SportThaInfo getSportThaInfoByCursor(Cursor cursor) {
        SportThaInfo trainLoadInfo = new SportThaInfo();
        int index = cursor.getColumnIndex("dayId");
        if (index >= 0) {
            trainLoadInfo.setDayId(cursor.getLong(index));
        }
        index = cursor.getColumnIndex("wtlSum");
        if (index >= 0) {
            trainLoadInfo.setWtlSum(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("wtlSumOptimalMax");
        if (index >= 0) {
            trainLoadInfo.setWtlSumOptimalMax(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("wtlSumOptimalMin");
        if (index >= 0) {
            trainLoadInfo.setWtlSumOptimalMin(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("wtlSumOverreaching");
        if (index >= 0) {
            trainLoadInfo.setWtlSumOverreaching(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("updateTime");
        if (index >= 0) {
            trainLoadInfo.setUpdateTime(cursor.getLong(index));
        }
        index = cursor.getColumnIndex("dateStr");
        if (index >= 0) {
            trainLoadInfo.setDateStr(cursor.getString(index));
        }
        index = cursor.getColumnIndex("vo2MaxTread");
        if (index >= 0) {
            trainLoadInfo.setVo2MaxTread(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("trainingLoadTrend");
        if (index >= 0) {
            trainLoadInfo.setTrainingLoadTrend(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("wtlStatus");
        if (index >= 0) {
            trainLoadInfo.setWtlStatus(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("fitnessClass");
        if (index >= 0) {
            trainLoadInfo.setFitnessClass(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("fitnessLevelIncrease");
        if (index >= 0) {
            trainLoadInfo.setFitnessLevelIncrease(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("currnetDayTrainLoad");
        if (index >= 0) {
            trainLoadInfo.setCurrnetDayTrainLoad(cursor.getInt(index));
        }
        index = cursor.getColumnIndex("sportVo2Max");
        if (index >= 0) {
            trainLoadInfo.setSportVo2Max(cursor.getFloat(index));
        }
        index = cursor.getColumnIndex("rideVo2Max");
        if (index >= 0) {
            trainLoadInfo.setRideVo2max(cursor.getFloat(index));
        }
        index = cursor.getColumnIndex("upload_status");
        if (index >= 0) {
            trainLoadInfo.setUploadStatus(cursor.getInt(index));
        }
        return trainLoadInfo;
    }

    public SportThaInfo getCurrnetDaySportThaInfo(long dayId) {
        return selectOneItemRawQuery("select * from sport_tha_info where dayId=? ; ", new String[]{String.valueOf(dayId)});
    }
}
