package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class CadenceDetailDao extends Dao<CyclingDetail> {
    private static final String TAG = CadenceDetailDao.class.getSimpleName();
    private static CadenceDetailDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    java.util.List<com.huami.watch.newsport.cadence.model.CyclingDetail> selectAll(android.database.sqlite.SQLiteDatabase r15, java.lang.String r16, java.lang.String[] r17, java.lang.String r18, java.lang.String r19) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r14 = this;
        r10 = 0;
        r1 = "cadence_detail";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 7;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2 = new java.lang.String[r0];	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 0;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "track_id";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 1;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "cur_cadence";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 2;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "cur_distance";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "cur_speed";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 4;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "cur_time";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 5;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "temperature";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = 6;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = "duration";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r2[r0] = r3;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r5 = 0;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r6 = 0;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = r15;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r3 = r16;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r4 = r17;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r7 = r18;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r8 = r19;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r10 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r13 = new java.util.LinkedList;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r13.<init>();	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x003d:
        r0 = r10.moveToNext();	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r0 == 0) goto L_0x00c6;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0043:
        r12 = new com.huami.watch.newsport.cadence.model.CyclingDetail;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.<init>();	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r0 = "track_id";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x0057;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0050:
        r0 = r10.getLong(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setTrackId(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0057:
        r0 = "cur_time";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x0066;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x005f:
        r0 = r10.getLong(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setCurTime(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0066:
        r0 = "cur_speed";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x0075;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x006e:
        r0 = r10.getFloat(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setCurSpeed(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0075:
        r0 = "cur_distance";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x0084;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x007d:
        r0 = r10.getFloat(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setCurDistance(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0084:
        r0 = "duration";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x0093;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x008c:
        r0 = r10.getLong(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setDuration(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x0093:
        r0 = "cur_cadence";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x00a2;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x009b:
        r0 = r10.getInt(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setCurCadence(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x00a2:
        r0 = "temperature";	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r9 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r9 < 0) goto L_0x00b1;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x00aa:
        r0 = r10.getInt(r9);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r12.setCurCadence(r0);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
    L_0x00b1:
        r13.add(r12);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        goto L_0x003d;
    L_0x00b5:
        r11 = move-exception;
        r0 = TAG;	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        r1 = r11.getLocalizedMessage();	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        com.huami.watch.common.log.Debug.m6w(r0, r1);	 Catch:{ Exception -> 0x00b5, all -> 0x00cc }
        if (r10 == 0) goto L_0x00c4;
    L_0x00c1:
        r10.close();
    L_0x00c4:
        r13 = 0;
    L_0x00c5:
        return r13;
    L_0x00c6:
        if (r10 == 0) goto L_0x00c5;
    L_0x00c8:
        r10.close();
        goto L_0x00c5;
    L_0x00cc:
        r0 = move-exception;
        if (r10 == 0) goto L_0x00d2;
    L_0x00cf:
        r10.close();
    L_0x00d2:
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.CadenceDetailDao.selectAll(android.database.sqlite.SQLiteDatabase, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.cadence.model.CyclingDetail>");
    }

    private CadenceDetailDao(Context applicationContext) {
        this.mDBHelper = SportDataHelper.getInstance(applicationContext);
    }

    public static synchronized CadenceDetailDao getInstance(Context context) {
        CadenceDetailDao cadenceDetailDao;
        synchronized (CadenceDetailDao.class) {
            if (sInstance == null) {
                sInstance = new CadenceDetailDao(context.getApplicationContext());
            }
            cadenceDetailDao = sInstance;
        }
        return cadenceDetailDao;
    }

    protected CyclingDetail select(Context context, String whereClause, String[] whereClauseArg) {
        LogUtils.print(TAG, "select method: not to  Realization ");
        return (CyclingDetail) super.select(context, whereClause, whereClauseArg);
    }

    protected List<? extends CyclingDetail> selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return null;
        }
        return selectAll(this.mDBHelper.getReadableDatabase(), whereClause, whereClauseArg, orderBy, limit);
    }

    protected boolean insert(Context context, CyclingDetail cyclingDetail) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert");
            return false;
        } else if (cyclingDetail == null) {
            Debug.m6w(TAG, "heart rate data is null while insert");
            return false;
        } else {
            if (this.mDBHelper.getWritableDatabase().insert("heart_rate", null, createContentValues(cyclingDetail)) >= 0) {
                return true;
            }
            return false;
        }
    }

    protected boolean insertAll(Context context, List<? extends CyclingDetail> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list != null) {
            return insertAll(this.mDBHelper.getWritableDatabase(), (List) list);
        } else {
            Debug.m6w(TAG, "location data should not be null while insert all");
            return false;
        }
    }

    protected boolean update(Context context, CyclingDetail cyclingDetail) {
        LogUtils.print(TAG, "update method : not to Realization ");
        return super.update(context, cyclingDetail);
    }

    protected boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while delete");
            return false;
        }
        Debug.m5i(TAG, "" + this.mDBHelper.getWritableDatabase().delete("cadence_detail", whereClause, whereClauseArg) + " rows deleted in location table");
        return true;
    }

    public boolean insertAll(SQLiteDatabase db, List<? extends CyclingDetail> locationDatas) {
        long rowId = -1;
        db.beginTransaction();
        for (CyclingDetail sportStatus : locationDatas) {
            rowId = db.insert("cadence_detail", null, createContentValues(sportStatus));
            if (rowId < 0) {
                break;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        if (rowId >= 0) {
            return true;
        }
        Debug.m6w(TAG, "error occured. rowId : " + rowId);
        return false;
    }

    private ContentValues createContentValues(CyclingDetail cyclingDetail) {
        ContentValues values = new ContentValues(7);
        values.put("track_id", Long.valueOf(cyclingDetail.getTrackId()));
        values.put("cur_time", Long.valueOf(cyclingDetail.getCurTime()));
        values.put("cur_speed", Float.valueOf(cyclingDetail.getCurSpeed()));
        values.put("cur_distance", Float.valueOf(cyclingDetail.getCurDistance()));
        values.put("duration", Long.valueOf(cyclingDetail.getDuration()));
        values.put("cur_cadence", Integer.valueOf(cyclingDetail.getCurCadence()));
        values.put("temperature", Integer.valueOf(cyclingDetail.getTemperature()));
        return values;
    }
}
