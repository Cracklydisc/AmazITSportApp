package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.db.SportDataHelper;
import com.huami.watch.newsport.gps.model.SportLocationData;
import java.util.List;

public class LocationDataDao extends Dao<SportLocationData> {
    private static LocationDataDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    public java.util.List<com.huami.watch.newsport.gps.model.SportLocationData> selectAll(android.content.Context r28, java.lang.String r29, java.lang.String[] r30, java.lang.String r31, java.lang.String r32) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r27 = this;
        if (r28 != 0) goto L_0x000c;
    L_0x0002:
        r3 = "LocationDataDao";
        r4 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r3, r4);
        r26 = 0;
    L_0x000b:
        return r26;
    L_0x000c:
        r0 = r27;
        r3 = r0.mDBHelper;
        r2 = r3.getReadableDatabase();
        r11 = 0;
        r3 = "location_data";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4 = 12;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4 = new java.lang.String[r4];	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 0;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "track_id";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 1;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "latitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 2;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "longitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "accuracy";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 4;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "altitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 5;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "timestamp";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "point_type";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 7;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "speed";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 8;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "point_index";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 9;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "bar";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 10;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "course";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = 11;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = "algo_point_type";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r7 = 0;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r8 = 0;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r5 = r29;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r6 = r30;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r9 = r31;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r10 = r32;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r11 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "track_id";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r25 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "latitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r19 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "longitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r20 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "accuracy";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r14 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "altitude";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r16 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "timestamp";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r24 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "point_type";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r22 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "speed";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r23 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "point_index";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r21 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "bar";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r17 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "course";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r18 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = "algo_point_type";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r15 = r11.getColumnIndex(r3);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r26 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r26.<init>();	 Catch:{ Exception -> 0x0123, all -> 0x013d }
    L_0x00b6:
        r3 = r11.moveToNext();	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        if (r3 == 0) goto L_0x0136;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
    L_0x00bc:
        r12 = new com.huami.watch.newsport.gps.model.SportLocationData;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.<init>();	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r25;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4 = r11.getLong(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mTrackId = r4;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r19;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mLatitude = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r20;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mLongitude = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r14);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mGPSAccuracy = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r16;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mAltitude = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r24;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4 = r11.getLong(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mTimestamp = r4;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r22;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getInt(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mPointType = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r23;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mSpeed = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r21;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getInt(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mPointIndex = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r17;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getInt(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mBar = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r18;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mCourse = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r3 = r11.getInt(r15);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r12.mAlgoPointType = r3;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0 = r26;	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r0.add(r12);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        goto L_0x00b6;
    L_0x0123:
        r13 = move-exception;
        r3 = "LocationDataDao";	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        r4 = r13.getLocalizedMessage();	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        com.huami.watch.common.log.Debug.m6w(r3, r4);	 Catch:{ Exception -> 0x0123, all -> 0x013d }
        if (r11 == 0) goto L_0x0132;
    L_0x012f:
        r11.close();
    L_0x0132:
        r26 = 0;
        goto L_0x000b;
    L_0x0136:
        if (r11 == 0) goto L_0x000b;
    L_0x0138:
        r11.close();
        goto L_0x000b;
    L_0x013d:
        r3 = move-exception;
        if (r11 == 0) goto L_0x0143;
    L_0x0140:
        r11.close();
    L_0x0143:
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.LocationDataDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.gps.model.SportLocationData>");
    }

    private LocationDataDao(Context context) {
        this.mDBHelper = SportDataHelper.getInstance(context);
    }

    public static synchronized LocationDataDao getInstance(Context context) {
        LocationDataDao locationDataDao;
        synchronized (LocationDataDao.class) {
            if (sInstance == null) {
                sInstance = new LocationDataDao(context.getApplicationContext());
            }
            locationDataDao = sInstance;
        }
        return locationDataDao;
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("LocationDataDao", "context should not be null while delete");
            return false;
        }
        Debug.m5i("LocationDataDao", "" + this.mDBHelper.getWritableDatabase().delete("location_data", whereClause, whereClauseArg) + " rows deleted in location table");
        return true;
    }

    public boolean insert(Context context, SportLocationData locationData) {
        if (context == null) {
            Debug.m6w("LocationDataDao", "context should not be null while insert");
            return false;
        } else if (locationData == null) {
            Debug.m6w("LocationDataDao", "sport status is null while insert");
            return false;
        } else {
            SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
            ContentValues values = createContentValues(locationData);
            if (db.insert("location_data", null, values) >= 0) {
                return true;
            }
            Debug.m6w("LocationDataDao", "error occured while insert : " + values);
            return false;
        }
    }

    private ContentValues createContentValues(SportLocationData locationData) {
        ContentValues values = new ContentValues(9);
        values.put("track_id", Long.valueOf(locationData.mTrackId));
        values.put("timestamp", Long.valueOf(locationData.mTimestamp));
        values.put("latitude", Float.valueOf(locationData.mLatitude));
        values.put("longitude", Float.valueOf(locationData.mLongitude));
        values.put("altitude", Float.valueOf(locationData.mAltitude));
        values.put("accuracy", Float.valueOf(locationData.mGPSAccuracy));
        values.put("point_type", Integer.valueOf(locationData.mPointType));
        values.put("speed", Float.valueOf(locationData.mSpeed));
        values.put("point_index", Integer.valueOf(locationData.mPointIndex));
        values.put("bar", Integer.valueOf(locationData.mBar));
        values.put("course", Float.valueOf(locationData.mCourse));
        values.put("algo_point_type", Integer.valueOf(locationData.mAlgoPointType));
        return values;
    }

    public boolean insertAll(SQLiteDatabase db, List<? extends SportLocationData> locationDatas) {
        long rowId = -1;
        db.beginTransaction();
        for (SportLocationData sportStatus : locationDatas) {
            rowId = db.insert("location_data", null, createContentValues(sportStatus));
            if (rowId < 0) {
                break;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        if (rowId >= 0) {
            return true;
        }
        Debug.m6w("LocationDataDao", "error occured. rowId : " + rowId);
        return false;
    }

    public boolean insertAll(Context context, List<? extends SportLocationData> locationDatas) {
        if (context == null) {
            Debug.m6w("LocationDataDao", "context should not be null while insert all");
            return false;
        } else if (locationDatas != null) {
            return insertAll(this.mDBHelper.getWritableDatabase(), (List) locationDatas);
        } else {
            Debug.m6w("LocationDataDao", "location data should not be null while insert all");
            return false;
        }
    }

    protected Cursor selectAsCursor(String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        Cursor cursor = null;
        try {
            cursor = this.mDBHelper.getReadableDatabase().query("location_data", new String[]{"latitude", "longitude", "altitude", "timestamp"}, whereClause, whereClauseArg, null, null, orderBy, limit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }
}
