package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class HeartRateDao extends Dao<HeartRate> {
    private static HeartRateDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    java.util.List<com.huami.watch.newsport.common.model.HeartRate> selectAll(android.database.sqlite.SQLiteDatabase r29, java.lang.String r30, java.lang.String[] r31, java.lang.String r32, java.lang.String r33) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r28 = this;
        r11 = 0;
        r3 = "heart_rate";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 13;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 0;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "track_id";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 1;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "time";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 2;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "rate";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 3;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "step_freq";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 4;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "altitude";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "pace";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 6;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "heart_quality";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 7;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "step_count";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 8;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "run_time";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 9;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "stride";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 10;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "disdiff";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 11;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "heart_range";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = 12;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = "stroke_speed";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r4[r2] = r5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r7 = 0;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r8 = 0;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r29;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r5 = r30;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r6 = r31;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r9 = r32;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r10 = r33;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r11 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "time";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r24 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "track_id";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r25 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "rate";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r15 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "step_freq";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r21 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "altitude";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r13 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "pace";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r18 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "heart_quality";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r16 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "step_count";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r20 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "stride";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r22 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "run_time";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r19 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "disdiff";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r14 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "heart_range";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r17 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = "stroke_speed";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r23 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r27 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r27.<init>();	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00b0:
        r2 = r11.moveToNext();	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        if (r2 == 0) goto L_0x017a;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00b6:
        r26 = new com.huami.watch.newsport.common.model.HeartRate;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r26.<init>();	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        if (r24 < 0) goto L_0x00c8;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00bd:
        r0 = r24;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setTimestamp(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00c8:
        if (r25 < 0) goto L_0x00d5;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00ca:
        r0 = r25;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setTrackId(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00d5:
        if (r15 < 0) goto L_0x00e1;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00d7:
        r2 = r11.getFloat(r15);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = (int) r2;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setHeartRate(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00e1:
        if (r21 < 0) goto L_0x00ee;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00e3:
        r0 = r21;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setStepFreq(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00ee:
        if (r13 < 0) goto L_0x00f9;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00f0:
        r2 = r11.getInt(r13);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setAltitude(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00f9:
        if (r18 < 0) goto L_0x0106;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x00fb:
        r0 = r18;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setPace(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0106:
        if (r16 < 0) goto L_0x0113;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0108:
        r0 = r16;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setHeartQuality(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0113:
        if (r20 < 0) goto L_0x0120;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0115:
        r0 = r20;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setStepDiff(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0120:
        if (r22 < 0) goto L_0x012d;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0122:
        r0 = r22;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setStride(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x012d:
        if (r19 < 0) goto L_0x013a;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x012f:
        r0 = r19;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setRunTime(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x013a:
        if (r14 < 0) goto L_0x0145;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x013c:
        r2 = r11.getFloat(r14);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setDistance(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0145:
        if (r17 < 0) goto L_0x0152;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0147:
        r0 = r17;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setHeartRange(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0152:
        if (r23 < 0) goto L_0x015f;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x0154:
        r0 = r23;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.setStrokeSpeed(r2);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
    L_0x015f:
        r0 = r27;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r1 = r26;	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r0.add(r1);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        goto L_0x00b0;
    L_0x0168:
        r12 = move-exception;
        r2 = "HeartRateDao";	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        r3 = r12.getLocalizedMessage();	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        com.huami.watch.common.log.Debug.m6w(r2, r3);	 Catch:{ Exception -> 0x0168, all -> 0x0180 }
        if (r11 == 0) goto L_0x0177;
    L_0x0174:
        r11.close();
    L_0x0177:
        r27 = 0;
    L_0x0179:
        return r27;
    L_0x017a:
        if (r11 == 0) goto L_0x0179;
    L_0x017c:
        r11.close();
        goto L_0x0179;
    L_0x0180:
        r2 = move-exception;
        if (r11 == 0) goto L_0x0186;
    L_0x0183:
        r11.close();
    L_0x0186:
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.HeartRateDao.selectAll(android.database.sqlite.SQLiteDatabase, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.HeartRate>");
    }

    public java.util.List<com.huami.watch.newsport.common.model.HeartRate> selectRawQuery(java.lang.String r23, java.lang.String[] r24) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r22 = this;
        r5 = new java.util.ArrayList;
        r5.<init>();
        r0 = r22;
        r0 = r0.mDBHelper;
        r20 = r0;
        r3 = r20.getReadableDatabase();
        r0 = r23;
        r1 = r24;
        r2 = r3.rawQuery(r0, r1);
        r20 = "time";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r17 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "track_id";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r18 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "rate";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r8 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "step_freq";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r14 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "altitude";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r6 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "pace";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r11 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "heart_quality";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r9 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "step_count";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r13 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "stride";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r15 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "run_time";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r12 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "disdiff";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r7 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "heart_range";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r10 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = "stroke_speed";	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r16 = r2.getColumnIndex(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x007f:
        r20 = r2.moveToNext();	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        if (r20 == 0) goto L_0x011c;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x0085:
        r19 = new com.huami.watch.newsport.common.model.HeartRate;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.<init>();	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        if (r17 < 0) goto L_0x0095;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x008c:
        r0 = r17;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = r2.getLong(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setTimestamp(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x0095:
        if (r18 < 0) goto L_0x00a0;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x0097:
        r0 = r18;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = r2.getLong(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setTrackId(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00a0:
        if (r8 < 0) goto L_0x00ae;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00a2:
        r20 = r2.getFloat(r8);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = r20;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r0 = (int) r0;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = r0;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setHeartRate(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00ae:
        if (r14 < 0) goto L_0x00b7;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00b0:
        r20 = r2.getFloat(r14);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setStepFreq(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00b7:
        if (r6 < 0) goto L_0x00c0;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00b9:
        r20 = r2.getInt(r6);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setAltitude(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00c0:
        if (r11 < 0) goto L_0x00c9;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00c2:
        r20 = r2.getFloat(r11);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setPace(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00c9:
        if (r9 < 0) goto L_0x00d2;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00cb:
        r20 = r2.getInt(r9);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setHeartQuality(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00d2:
        if (r13 < 0) goto L_0x00db;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00d4:
        r20 = r2.getInt(r13);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setStepDiff(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00db:
        if (r15 < 0) goto L_0x00e4;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00dd:
        r20 = r2.getInt(r15);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setStride(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00e4:
        if (r12 < 0) goto L_0x00ed;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00e6:
        r20 = r2.getInt(r12);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setRunTime(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00ed:
        if (r7 < 0) goto L_0x00f6;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00ef:
        r20 = r2.getFloat(r7);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setDistance(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00f6:
        if (r10 < 0) goto L_0x00ff;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00f8:
        r20 = r2.getInt(r10);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setHeartRange(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x00ff:
        if (r16 < 0) goto L_0x010a;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x0101:
        r0 = r16;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r20 = r2.getFloat(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r19.setStrokeSpeed(r20);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
    L_0x010a:
        r0 = r19;	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        r5.add(r0);	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        goto L_0x007f;
    L_0x0111:
        r4 = move-exception;
        r4.printStackTrace();	 Catch:{ Exception -> 0x0111, all -> 0x0122 }
        if (r2 == 0) goto L_0x011a;
    L_0x0117:
        r2.close();
    L_0x011a:
        r5 = 0;
    L_0x011b:
        return r5;
    L_0x011c:
        if (r2 == 0) goto L_0x011b;
    L_0x011e:
        r2.close();
        goto L_0x011b;
    L_0x0122:
        r20 = move-exception;
        if (r2 == 0) goto L_0x0128;
    L_0x0125:
        r2.close();
    L_0x0128:
        throw r20;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.HeartRateDao.selectRawQuery(java.lang.String, java.lang.String[]):java.util.List<com.huami.watch.newsport.common.model.HeartRate>");
    }

    private HeartRateDao(Context applicationContext) {
        this.mDBHelper = SportDataHelper.getInstance(applicationContext);
    }

    public static synchronized HeartRateDao getInstance(Context context) {
        HeartRateDao heartRateDao;
        synchronized (HeartRateDao.class) {
            if (sInstance == null) {
                sInstance = new HeartRateDao(context.getApplicationContext());
            }
            heartRateDao = sInstance;
        }
        return heartRateDao;
    }

    private ContentValues createContentValues(HeartRate heartRate) {
        ContentValues values = new ContentValues(12);
        values.put("time", Long.valueOf(heartRate.getTimestamp()));
        values.put("track_id", Long.valueOf(heartRate.getTrackId()));
        values.put("rate", Integer.valueOf(heartRate.getHeartRate()));
        values.put("altitude", Integer.valueOf(heartRate.getAltitude()));
        values.put("step_freq", Float.valueOf(heartRate.getStepFreq()));
        values.put("pace", Float.valueOf(heartRate.getPace()));
        values.put("heart_quality", Integer.valueOf(heartRate.getHeartQuality()));
        values.put("step_count", Integer.valueOf(heartRate.getStepDiff()));
        values.put("stride", Integer.valueOf(heartRate.getStride()));
        values.put("run_time", Integer.valueOf(heartRate.getRunTime()));
        values.put("disdiff", Float.valueOf(heartRate.getDistance()));
        values.put("heart_range", Integer.valueOf(heartRate.getHeartRange()));
        values.put("stroke_speed", Float.valueOf(heartRate.getStrokeSpeed()));
        return values;
    }

    public boolean insert(Context context, HeartRate heartRateData) {
        if (context == null) {
            Debug.m6w("HeartRateDao", "context should not be null while insert");
            return false;
        } else if (heartRateData == null) {
            Debug.m6w("HeartRateDao", "heart rate data is null while insert");
            return false;
        } else {
            if (this.mDBHelper.getWritableDatabase().insert("heart_rate", null, createContentValues(heartRateData)) >= 0) {
                return true;
            }
            return false;
        }
    }

    boolean insertAll(SQLiteDatabase db, List<? extends HeartRate> heartRates) {
        long rowId = -1;
        db.beginTransaction();
        for (HeartRate heartRateData : heartRates) {
            rowId = db.insert("heart_rate", null, createContentValues(heartRateData));
            if (rowId < 0) {
                break;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        if (rowId < 0) {
            return false;
        }
        return true;
    }

    public boolean insertAll(Context context, List<? extends HeartRate> heartRates) {
        if (context == null) {
            Debug.m6w("HeartRateDao", "context should not be null while insert all");
            return false;
        } else if (heartRates != null) {
            return insertAll(this.mDBHelper.getWritableDatabase(), (List) heartRates);
        } else {
            Debug.m6w("HeartRateDao", "heart rate data is null while insert all");
            return false;
        }
    }

    public List<HeartRate> selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        if (context == null) {
            Debug.m6w("HeartRateDao", "context should not be null while insert all");
            return null;
        }
        return selectAll(this.mDBHelper.getReadableDatabase(), whereClause, whereClauseArg, orderBy, limit);
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("HeartRateDao", "context should not be null while delete");
            return false;
        }
        Debug.m3d("HeartRateDao", "" + this.mDBHelper.getWritableDatabase().delete("heart_rate", whereClause, whereClauseArg) + " deleted in heart rate table");
        return true;
    }

    protected Cursor selectAsCursor(String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        Cursor cursor = null;
        try {
            cursor = this.mDBHelper.getReadableDatabase().query("heart_rate", new String[]{"run_time", "rate"}, whereClause, whereClauseArg, null, null, orderBy, limit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }
}
