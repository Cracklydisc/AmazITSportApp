package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class SportSummaryDao extends Dao<SportSummary> {
    private static SportSummaryDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    public com.huami.watch.newsport.common.model.SportSummary select(android.content.Context r14, android.database.sqlite.SQLiteDatabase r15, java.lang.String r16, java.lang.String[] r17) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:8:0x0056]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r0 = "SportSummaryDao";
        r1 = "context should not be null while select";
        com.huami.watch.common.log.Debug.m6w(r0, r1);
        r12 = 0;
    L_0x000a:
        return r12;
    L_0x000b:
        r10 = 0;
        r1 = "sport_summary";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = 4;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r2 = new java.lang.String[r0];	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r3 = "track_id";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r2[r0] = r3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = 1;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r3 = "type";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r2[r0] = r3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = 2;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r3 = "content";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r2[r0] = r3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = 3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r3 = "current_status";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r2[r0] = r3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r5 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r6 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r7 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r8 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = r15;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r3 = r16;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r4 = r17;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r10 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r12 = 0;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = r10.moveToNext();	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        if (r0 == 0) goto L_0x0054;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
    L_0x0039:
        r0 = "content";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r9 = r10.getString(r0);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r12 = com.huami.watch.newsport.common.model.OutdoorSportSummary.createFromJSON(r9);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = "current_status";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = r10.getColumnIndex(r0);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r0 = r10.getInt(r0);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        r12.setCurrentStatus(r0);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
    L_0x0054:
        if (r10 == 0) goto L_0x000a;
    L_0x0056:
        r10.close();
        goto L_0x000a;
    L_0x005a:
        r11 = move-exception;
        r0 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        if (r0 == 0) goto L_0x0064;	 Catch:{ Exception -> 0x005a, all -> 0x006b }
    L_0x005f:
        r0 = "SportSummaryDao";	 Catch:{ Exception -> 0x005a, all -> 0x006b }
        com.huami.watch.common.log.Debug.printException(r0, r11);	 Catch:{ Exception -> 0x005a, all -> 0x006b }
    L_0x0064:
        if (r10 == 0) goto L_0x0069;
    L_0x0066:
        r10.close();
    L_0x0069:
        r12 = 0;
        goto L_0x000a;
    L_0x006b:
        r0 = move-exception;
        if (r10 == 0) goto L_0x0071;
    L_0x006e:
        r10.close();
    L_0x0071:
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.SportSummaryDao.select(android.content.Context, android.database.sqlite.SQLiteDatabase, java.lang.String, java.lang.String[]):com.huami.watch.newsport.common.model.SportSummary");
    }

    public java.util.List<com.huami.watch.newsport.common.model.SportSummary> selectAll(android.content.Context r16, android.database.sqlite.SQLiteDatabase r17, java.lang.String r18, java.lang.String[] r19, java.lang.String r20, java.lang.String r21) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:20:0x0075]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r15 = this;
        if (r16 != 0) goto L_0x000b;
    L_0x0002:
        r0 = "SportSummaryDao";
        r1 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r0, r1);
        r13 = 0;
    L_0x000a:
        return r13;
    L_0x000b:
        r11 = 0;
        r1 = "sport_summary";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = 4;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r2 = new java.lang.String[r0];	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = 0;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r3 = "track_id";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r2[r0] = r3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = 1;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r3 = "type";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r2[r0] = r3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = 2;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r3 = "content";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r2[r0] = r3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = 3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r3 = "current_status";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r2[r0] = r3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r5 = 0;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r6 = 0;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = r17;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r3 = r18;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r4 = r19;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r7 = r20;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r8 = r21;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r11 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r13 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r13.<init>();	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
    L_0x003a:
        r0 = r11.moveToNext();	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        if (r0 == 0) goto L_0x0073;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
    L_0x0040:
        r0 = "content";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = r11.getColumnIndex(r0);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r9 = r11.getString(r0);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = "current_status";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r0 = r11.getColumnIndex(r0);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r10 = r11.getInt(r0);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r14 = 0;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r14 = com.huami.watch.newsport.common.model.OutdoorSportSummary.createFromJSON(r9);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        if (r14 == 0) goto L_0x003a;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
    L_0x005b:
        r14.setCurrentStatus(r10);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        r13.add(r14);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        goto L_0x003a;
    L_0x0062:
        r12 = move-exception;
        r0 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        if (r0 == 0) goto L_0x006c;	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
    L_0x0067:
        r0 = "SportSummaryDao";	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
        com.huami.watch.common.log.Debug.printException(r0, r12);	 Catch:{ Exception -> 0x0062, all -> 0x0079 }
    L_0x006c:
        if (r11 == 0) goto L_0x0071;
    L_0x006e:
        r11.close();
    L_0x0071:
        r13 = 0;
        goto L_0x000a;
    L_0x0073:
        if (r11 == 0) goto L_0x000a;
    L_0x0075:
        r11.close();
        goto L_0x000a;
    L_0x0079:
        r0 = move-exception;
        if (r11 == 0) goto L_0x007f;
    L_0x007c:
        r11.close();
    L_0x007f:
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.SportSummaryDao.selectAll(android.content.Context, android.database.sqlite.SQLiteDatabase, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.SportSummary>");
    }

    private SportSummaryDao(Context context) {
        this.mDBHelper = SportDataHelper.getInstance(context);
    }

    public static synchronized SportSummaryDao getInstance(Context context) {
        SportSummaryDao sportSummaryDao;
        synchronized (SportSummaryDao.class) {
            if (sInstance == null) {
                sInstance = new SportSummaryDao(context.getApplicationContext());
            }
            sportSummaryDao = sInstance;
        }
        return sportSummaryDao;
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("SportSummaryDao", "context should not be null while delete");
            return false;
        }
        Debug.m5i("SportSummaryDao", "" + this.mDBHelper.getWritableDatabase().delete("sport_summary", whereClause, whereClauseArg) + " rows deleted in summary table");
        return true;
    }

    protected SportSummary select(Context context, String whereClause, String[] whereClauseArg) {
        return select(context, this.mDBHelper.getReadableDatabase(), whereClause, whereClauseArg);
    }

    public boolean insert(Context context, SportSummary sportSummary) {
        if (context == null) {
            Debug.m6w("SportSummaryDao", "context should not be null while insert");
            return false;
        } else if (sportSummary == null) {
            Debug.m6w("SportSummaryDao", "sport summary is null while insert");
            return false;
        } else if (-1 == sportSummary.getSportType()) {
            throw new IllegalArgumentException("Invalid sport type");
        } else {
            ContentValues values = createContentValues(sportSummary);
            if (values == null) {
                return false;
            }
            long colId = this.mDBHelper.getWritableDatabase().insert("sport_summary", null, values);
            if (colId < 0) {
                Debug.m6w("SportSummaryDao", "insert " + sportSummary + " failed. col id : " + colId);
                return false;
            }
            Debug.m6w("SportSummaryDao", "insert summary trackId:" + sportSummary.getTrackId() + " success. col id : " + colId);
            return true;
        }
    }

    public boolean insertAll(Context context, List<? extends SportSummary> sportSummaries) {
        if (context == null) {
            Debug.m6w("SportSummaryDao", "context should not be null while insert all");
            return false;
        } else if (sportSummaries != null) {
            return insertAll(context, this.mDBHelper.getWritableDatabase(), sportSummaries);
        } else {
            Debug.m6w("SportSummaryDao", "sport summary is null while insert all");
            return false;
        }
    }

    public boolean insertAll(Context context, SQLiteDatabase db, List<? extends SportSummary> sportSummaries) {
        if (context == null) {
            Debug.m6w("SportSummaryDao", "context should not be null while insert all");
            return false;
        } else if (sportSummaries == null) {
            Debug.m6w("SportSummaryDao", "sport summary is null while insert all");
            return false;
        } else {
            db.beginTransaction();
            long rowId = -1;
            for (SportSummary sportSummary : sportSummaries) {
                if (-1 == sportSummary.getSportType()) {
                    throw new IllegalArgumentException("Invalid sport type");
                }
                ContentValues values = createContentValues(sportSummary);
                if (values != null) {
                    rowId = db.insert("sport_summary", null, values);
                    if (rowId < 0) {
                        break;
                    }
                }
                return false;
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId > 0) {
                return true;
            }
            return false;
        }
    }

    private ContentValues createContentValues(SportSummary sportSummary) {
        String content = sportSummary.toJSON();
        if (content == null) {
            return null;
        }
        ContentValues values = new ContentValues(6);
        values.put("track_id", Long.valueOf(sportSummary.getTrackId()));
        values.put("start_time", Long.valueOf(sportSummary.getStartTime()));
        values.put("end_time", Long.valueOf(sportSummary.getEndTime()));
        values.put("type", Integer.valueOf(sportSummary.getSportType()));
        values.put("calorie", Float.valueOf(sportSummary.getCalorie()));
        values.put("current_status", Integer.valueOf(sportSummary.getCurrentStatus()));
        values.put("parent_trackid", Long.valueOf(sportSummary.getParentTrackId()));
        values.put("content", content);
        return values;
    }

    public boolean update(Context context, SportSummary sportSummary) {
        return update(context, this.mDBHelper.getWritableDatabase(), sportSummary);
    }

    public boolean update(Context context, SQLiteDatabase db, SportSummary sportSummary) {
        if (context == null) {
            Debug.m6w("SportSummaryDao", "context should not be null while update");
            return false;
        } else if (sportSummary == null) {
            Debug.m6w("SportSummaryDao", "sport summary is null while update");
            return false;
        } else {
            Debug.m3d("SportSummaryDao", "" + db.update("sport_summary", createContentValues(sportSummary), "track_id=?", new String[]{"" + sportSummary.getTrackId()}) + " rows affect");
            return true;
        }
    }

    public List<SportSummary> selectAll(Context context, String whereClause, String[] whereClauseArg, String orderBy, String limit) {
        return selectAll(context, this.mDBHelper.getReadableDatabase(), whereClause, whereClauseArg, orderBy, limit);
    }
}
