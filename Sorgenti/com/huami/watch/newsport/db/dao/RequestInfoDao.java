package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.RequestInfo;
import com.huami.watch.newsport.db.SportDataHelper;

public class RequestInfoDao extends Dao<RequestInfo> {
    private static RequestInfoDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    protected com.huami.watch.newsport.common.model.RequestInfo select(android.content.Context r21, java.lang.String r22, java.lang.String[] r23) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:8:0x0079]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r20 = this;
        if (r21 != 0) goto L_0x000b;
    L_0x0002:
        r3 = "RequestInfoDao";
        r4 = "context should not be null while select";
        com.huami.watch.common.log.Debug.m6w(r3, r4);
        r15 = 0;
    L_0x000a:
        return r15;
    L_0x000b:
        r0 = r20;
        r3 = r0.mDBHelper;
        r2 = r3.getReadableDatabase();
        r12 = 0;
        r3 = "request";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4 = 4;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4 = new java.lang.String[r4];	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r5 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r6 = "id";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4[r5] = r6;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r5 = 1;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r6 = "track_id";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4[r5] = r6;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r5 = 2;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r6 = "request_action";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4[r5] = r6;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r5 = 3;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r6 = "request_stat";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r4[r5] = r6;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r7 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r8 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r9 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r10 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r5 = r22;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r6 = r23;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r12 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r15 = 0;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = r12.moveToNext();	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        if (r3 == 0) goto L_0x0077;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
    L_0x0040:
        r3 = "id";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r14 = r12.getString(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = "track_id";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r18 = r12.getLong(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = "request_action";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r11 = r12.getString(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r15 = com.huami.watch.newsport.common.model.RequestInfo.createRequest(r14, r11);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = "request_stat";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r16 = r12.getInt(r3);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r0 = r18;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r15.setTrackId(r0);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r15.setAction(r11);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        r15.setRequestStat(r16);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
    L_0x0077:
        if (r12 == 0) goto L_0x000a;
    L_0x0079:
        r12.close();
        goto L_0x000a;
    L_0x007d:
        r13 = move-exception;
        r3 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        if (r3 == 0) goto L_0x0087;	 Catch:{ Exception -> 0x007d, all -> 0x008f }
    L_0x0082:
        r3 = "RequestInfoDao";	 Catch:{ Exception -> 0x007d, all -> 0x008f }
        com.huami.watch.common.log.Debug.printException(r3, r13);	 Catch:{ Exception -> 0x007d, all -> 0x008f }
    L_0x0087:
        if (r12 == 0) goto L_0x008c;
    L_0x0089:
        r12.close();
    L_0x008c:
        r15 = 0;
        goto L_0x000a;
    L_0x008f:
        r3 = move-exception;
        if (r12 == 0) goto L_0x0095;
    L_0x0092:
        r12.close();
    L_0x0095:
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.RequestInfoDao.select(android.content.Context, java.lang.String, java.lang.String[]):com.huami.watch.newsport.common.model.RequestInfo");
    }

    protected java.util.List<com.huami.watch.newsport.common.model.RequestInfo> selectAll(android.content.Context r21, java.lang.String r22, java.lang.String[] r23, java.lang.String r24, java.lang.String r25) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000b in list [B:18:0x009b]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r20 = this;
        if (r21 != 0) goto L_0x000c;
    L_0x0002:
        r3 = "RequestInfoDao";
        r4 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r3, r4);
        r16 = 0;
    L_0x000b:
        return r16;
    L_0x000c:
        r0 = r20;
        r3 = r0.mDBHelper;
        r2 = r3.getReadableDatabase();
        r12 = 0;
        r3 = "request";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4 = 4;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4 = new java.lang.String[r4];	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r5 = 0;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r6 = "id";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r5 = 1;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r6 = "track_id";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r5 = 2;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r6 = "request_action";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r5 = 3;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r6 = "request_stat";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r4[r5] = r6;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r7 = 0;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r8 = 0;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r5 = r22;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r6 = r23;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r9 = r24;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r10 = r25;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r12 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r16 = new java.util.LinkedList;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r16.<init>();	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
    L_0x0041:
        r3 = r12.moveToNext();	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        if (r3 == 0) goto L_0x0099;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
    L_0x0047:
        r3 = "id";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r14 = r12.getString(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = "track_id";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r18 = r12.getLong(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = "request_action";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r11 = r12.getString(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r15 = com.huami.watch.newsport.common.model.RequestInfo.createRequest(r14, r11);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = "request_stat";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r3 = r12.getColumnIndex(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r17 = r12.getInt(r3);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r0 = r18;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r15.setTrackId(r0);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r15.setAction(r11);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r0 = r17;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r15.setRequestStat(r0);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r0 = r16;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        r0.add(r15);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        goto L_0x0041;
    L_0x0086:
        r13 = move-exception;
        r3 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        if (r3 == 0) goto L_0x0090;	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
    L_0x008b:
        r3 = "RequestInfoDao";	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
        com.huami.watch.common.log.Debug.printException(r3, r13);	 Catch:{ Exception -> 0x0086, all -> 0x00a0 }
    L_0x0090:
        if (r12 == 0) goto L_0x0095;
    L_0x0092:
        r12.close();
    L_0x0095:
        r16 = 0;
        goto L_0x000b;
    L_0x0099:
        if (r12 == 0) goto L_0x000b;
    L_0x009b:
        r12.close();
        goto L_0x000b;
    L_0x00a0:
        r3 = move-exception;
        if (r12 == 0) goto L_0x00a6;
    L_0x00a3:
        r12.close();
    L_0x00a6:
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.RequestInfoDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.RequestInfo>");
    }

    public RequestInfoDao(Context context) {
        this.mDBHelper = SportDataHelper.getInstance(context);
    }

    public static synchronized RequestInfoDao getInstance(Context context) {
        RequestInfoDao requestInfoDao;
        synchronized (RequestInfoDao.class) {
            if (sInstance == null) {
                sInstance = new RequestInfoDao(context.getApplicationContext());
            }
            requestInfoDao = sInstance;
        }
        return requestInfoDao;
    }

    protected boolean insert(Context context, RequestInfo requestInfo) {
        if (context == null) {
            Debug.m6w("RequestInfoDao", "context should not be null while insert");
            return false;
        } else if (requestInfo == null) {
            Debug.m6w("RequestInfoDao", "request info is null while insert");
            return false;
        } else {
            ContentValues values = createContentValues(requestInfo);
            if (values == null) {
                return false;
            }
            long colId = this.mDBHelper.getWritableDatabase().insert("request", null, values);
            if (colId >= 0) {
                return true;
            }
            Debug.m6w("RequestInfoDao", "insert " + requestInfo + " failed. col id : " + colId);
            return false;
        }
    }

    private ContentValues createContentValues(RequestInfo requestInfo) {
        ContentValues values = new ContentValues(6);
        values.put("id", requestInfo.getIdentifier());
        values.put("track_id", Long.valueOf(requestInfo.getTrackId()));
        values.put("request_stat", Integer.valueOf(requestInfo.getRequestStat()));
        values.put("request_action", requestInfo.getAction());
        return values;
    }

    protected boolean update(Context context, RequestInfo requestInfo) {
        if (context == null) {
            Debug.m6w("RequestInfoDao", "context should not be null while update");
            return false;
        } else if (requestInfo == null) {
            Debug.m6w("RequestInfoDao", "request is null while update");
            return false;
        } else {
            int rowNum = this.mDBHelper.getWritableDatabase().update("request", createContentValues(requestInfo), "id=?", new String[]{"" + requestInfo.getIdentifier()});
            if (Global.DEBUG_LEVEL_3) {
                Debug.m3d("RequestInfoDao", "" + rowNum + " rows affect");
            }
            return true;
        }
    }

    protected boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("RequestInfoDao", "context should not be null while delete");
            return false;
        }
        Debug.m5i("RequestInfoDao", "" + this.mDBHelper.getWritableDatabase().delete("request", whereClause, whereClauseArg) + " rows deleted");
        return true;
    }
}
