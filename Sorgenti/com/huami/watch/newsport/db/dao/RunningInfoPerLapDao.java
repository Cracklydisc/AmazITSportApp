package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class RunningInfoPerLapDao extends Dao<RunningInfoPerLap> {
    private static RunningInfoPerLapDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    public java.util.List<com.huami.watch.newsport.common.model.RunningInfoPerLap> selectAll(android.content.Context r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String r18) {
        /* JADX: method processing error */
/*
Error: java.util.NoSuchElementException
	at java.util.HashMap$HashIterator.nextNode(HashMap.java:1431)
	at java.util.HashMap$KeyIterator.next(HashMap.java:1453)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.applyRemove(BlockFinallyExtract.java:535)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.extractFinally(BlockFinallyExtract.java:175)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.processExceptionHandler(BlockFinallyExtract.java:79)
	at jadx.core.dex.visitors.blocksmaker.BlockFinallyExtract.visit(BlockFinallyExtract.java:51)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r1 = "RunningInfoPerLapDao";
        r2 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r12 = 0;
    L_0x000a:
        return r12;
    L_0x000b:
        r1 = r13.mDBHelper;
        r0 = r1.getReadableDatabase();
        r9 = 0;
        r1 = "running_info_per_lap";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = 0;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r5 = 0;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r6 = 0;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r3 = r15;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r4 = r16;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r7 = r17;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r8 = r18;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r12 = new java.util.LinkedList;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r12.<init>();	 Catch:{ Exception -> 0x014a, all -> 0x015f }
    L_0x0027:
        r1 = r9.moveToNext();	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        if (r1 == 0) goto L_0x0158;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
    L_0x002d:
        r11 = new com.huami.watch.newsport.common.model.RunningInfoPerLap;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.<init>();	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "cost_time";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setCostTime(r2);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "pace";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = r9.getDouble(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setPace(r2);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "track_id";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setTrackId(r2);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "distance";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setDistance(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_num";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapNum(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "latitude";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLatitude(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "longitude";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLongitude(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "avg_heart";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setAvgHeart(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "total_cost_time";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setTotalCostTime(r2);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "absolute_altitude";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setAbsoluteAltitude(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "climb_up";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setClimbUp(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "climb_down";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setClimbDown(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "max_pace";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r2 = (double) r1;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setMaxPace(r2);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "climb_distance";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setClimbDistance(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_strokes";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapStrokes(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_stroke_speed";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapStrokeSpeed(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_swolf";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapSwolf(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_calories";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapCalories(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_step_freq";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = (float) r1;	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapStepFreq(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_cadence";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapCadence(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = "lap_type";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r11.setLapType(r1);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        r12.add(r11);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        goto L_0x0027;
    L_0x014a:
        r10 = move-exception;
        r1 = "RunningInfoPerLapDao";	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        com.huami.watch.common.log.Debug.printException(r1, r10);	 Catch:{ Exception -> 0x014a, all -> 0x015f }
        if (r9 == 0) goto L_0x0155;
    L_0x0152:
        r9.close();
    L_0x0155:
        r12 = 0;
        goto L_0x000a;
    L_0x0158:
        if (r9 == 0) goto L_0x000a;
    L_0x015a:
        r9.close();
        goto L_0x000a;
    L_0x015f:
        r1 = move-exception;
        if (r9 == 0) goto L_0x0165;
    L_0x0162:
        r9.close();
    L_0x0165:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.RunningInfoPerLapDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.RunningInfoPerLap>");
    }

    private RunningInfoPerLapDao(Context applicationContext) {
        this.mDBHelper = SportDataHelper.getInstance(applicationContext);
    }

    public static synchronized RunningInfoPerLapDao getInstance(Context context) {
        RunningInfoPerLapDao runningInfoPerLapDao;
        synchronized (RunningInfoPerLapDao.class) {
            if (sInstance == null) {
                sInstance = new RunningInfoPerLapDao(context.getApplicationContext());
            }
            runningInfoPerLapDao = sInstance;
        }
        return runningInfoPerLapDao;
    }

    public boolean insert(Context context, RunningInfoPerLap runningInfoPerIndividual) {
        if (context == null) {
            Debug.m6w("RunningInfoPerLapDao", "context should not be null while insert");
            return false;
        } else if (runningInfoPerIndividual == null) {
            Debug.m6w("RunningInfoPerLapDao", "running info per km is null while insert");
            return false;
        } else {
            if (this.mDBHelper.getWritableDatabase().insert("running_info_per_lap", null, createContentValues(runningInfoPerIndividual)) >= 0) {
                return true;
            }
            return false;
        }
    }

    private ContentValues createContentValues(RunningInfoPerLap runningInfoPerIndividual) {
        ContentValues values = new ContentValues();
        values.put("track_id", Long.valueOf(runningInfoPerIndividual.getTrackId()));
        values.put("cost_time", Long.valueOf(runningInfoPerIndividual.getCostTime()));
        values.put("pace", Double.valueOf(runningInfoPerIndividual.getPace()));
        values.put("distance", Float.valueOf(runningInfoPerIndividual.getDistance()));
        values.put("lap_num", Integer.valueOf(runningInfoPerIndividual.getLapNum()));
        values.put("latitude", Float.valueOf(runningInfoPerIndividual.getLatitude()));
        values.put("longitude", Float.valueOf(runningInfoPerIndividual.getLongitude()));
        values.put("avg_heart", Float.valueOf(runningInfoPerIndividual.getAvgHeart()));
        values.put("total_cost_time", Long.valueOf(runningInfoPerIndividual.getTotalCostTime()));
        values.put("absolute_altitude", Float.valueOf(runningInfoPerIndividual.getAbsoluteAltitude()));
        values.put("climb_up", Float.valueOf(runningInfoPerIndividual.getClimbUp()));
        values.put("climb_down", Float.valueOf(runningInfoPerIndividual.getClimbDown()));
        values.put("max_pace", Double.valueOf(runningInfoPerIndividual.getMaxPace()));
        values.put("climb_distance", Float.valueOf(runningInfoPerIndividual.getClimbDistance()));
        values.put("lap_strokes", Integer.valueOf(runningInfoPerIndividual.getLapStrokes()));
        values.put("lap_stroke_speed", Float.valueOf(runningInfoPerIndividual.getLapStrokeSpeed()));
        values.put("lap_swolf", Integer.valueOf(runningInfoPerIndividual.getLapSwolf()));
        values.put("lap_calories", Integer.valueOf(runningInfoPerIndividual.getLapCalories()));
        values.put("lap_step_freq", Float.valueOf(runningInfoPerIndividual.getLapStepFreq()));
        values.put("lap_cadence", Integer.valueOf(runningInfoPerIndividual.getLapCadence()));
        values.put("lap_type", Integer.valueOf(runningInfoPerIndividual.getLapType()));
        return values;
    }

    public boolean insertAll(Context context, List<? extends RunningInfoPerLap> runningInfoPerIndividuals) {
        if (context == null) {
            Debug.m6w("RunningInfoPerLapDao", "context should not be null while insert all");
            return false;
        } else if (runningInfoPerIndividuals == null) {
            Debug.m6w("RunningInfoPerLapDao", "running info per km is null while insert all");
            return false;
        } else {
            SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
            long rowId = -1;
            db.beginTransaction();
            for (RunningInfoPerLap runningInfoPerIndividual : runningInfoPerIndividuals) {
                rowId = db.insert("running_info_per_lap", null, createContentValues(runningInfoPerIndividual));
                if (rowId < 0) {
                    break;
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId >= 0) {
                return true;
            }
            return false;
        }
    }

    public boolean delete(Context context, String whereClause, String[] whereClauseArg) {
        if (context == null) {
            Debug.m6w("RunningInfoPerLapDao", "context should not be null while delete");
            return false;
        }
        Debug.m3d("RunningInfoPerLapDao", "" + this.mDBHelper.getWritableDatabase().delete("running_info_per_lap", whereClause, whereClauseArg) + " deleted in running info table");
        return true;
    }
}
