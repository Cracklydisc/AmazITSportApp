package com.huami.watch.newsport.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.huami.watch.common.db.Dao;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.db.SportDataHelper;
import java.util.List;

public class DailyPerpormanceInfoDao extends Dao<DailyPerformanceInfo> {
    private static final String TAG = DailyPerpormanceInfoDao.class.getSimpleName();
    private static DailyPerpormanceInfoDao sInstance = null;
    private SportDataHelper mDBHelper = null;

    public java.util.List<com.huami.watch.newsport.common.model.DailyPerformanceInfo> selectAll(android.content.Context r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String r18) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000a in list [B:18:0x00b8]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r13 = this;
        if (r14 != 0) goto L_0x000b;
    L_0x0002:
        r1 = TAG;
        r2 = "context should not be null while select all";
        com.huami.watch.common.log.Debug.m6w(r1, r2);
        r12 = 0;
    L_0x000a:
        return r12;
    L_0x000b:
        r1 = r13.mDBHelper;
        r0 = r1.getReadableDatabase();
        r9 = 0;
        r1 = "daily_perpormance_info";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2 = 6;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2 = new java.lang.String[r2];	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 0;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "track_id";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 1;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "te_value";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 2;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "daily_performance";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 3;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "save_data_type";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "create_time";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = 5;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = "currnet_kilos";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2[r3] = r4;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r5 = 0;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r6 = 0;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r3 = r15;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r4 = r16;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r7 = r17;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r8 = r18;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r9 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r12 = new java.util.LinkedList;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r12.<init>();	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
    L_0x0047:
        r1 = r9.moveToNext();	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        if (r1 == 0) goto L_0x00b6;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
    L_0x004d:
        r11 = new com.huami.watch.newsport.common.model.DailyPerformanceInfo;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.<init>();	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "track_id";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setmTrackId(r2);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "save_data_type";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setSaveDataType(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "daily_performance";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setDailyPorpermence(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "te_value";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getInt(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setTeValue(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "create_time";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r2 = r9.getLong(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setCreateTime(r2);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = "currnet_kilos";	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getColumnIndex(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r1 = r9.getFloat(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r11.setCurrnetKilos(r1);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        r12.add(r11);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        goto L_0x0047;
    L_0x00a4:
        r10 = move-exception;
        r1 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        if (r1 == 0) goto L_0x00ae;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
    L_0x00a9:
        r1 = TAG;	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
        com.huami.watch.common.log.Debug.printException(r1, r10);	 Catch:{ Exception -> 0x00a4, all -> 0x00bd }
    L_0x00ae:
        if (r9 == 0) goto L_0x00b3;
    L_0x00b0:
        r9.close();
    L_0x00b3:
        r12 = 0;
        goto L_0x000a;
    L_0x00b6:
        if (r9 == 0) goto L_0x000a;
    L_0x00b8:
        r9.close();
        goto L_0x000a;
    L_0x00bd:
        r1 = move-exception;
        if (r9 == 0) goto L_0x00c3;
    L_0x00c0:
        r9.close();
    L_0x00c3:
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao.selectAll(android.content.Context, java.lang.String, java.lang.String[], java.lang.String, java.lang.String):java.util.List<com.huami.watch.newsport.common.model.DailyPerformanceInfo>");
    }

    private DailyPerpormanceInfoDao(Context applicationContext) {
        this.mDBHelper = SportDataHelper.getInstance(applicationContext);
    }

    public static synchronized DailyPerpormanceInfoDao getInstance(Context context) {
        DailyPerpormanceInfoDao dailyPerpormanceInfoDao;
        synchronized (DailyPerpormanceInfoDao.class) {
            if (sInstance == null) {
                sInstance = new DailyPerpormanceInfoDao(context.getApplicationContext());
            }
            dailyPerpormanceInfoDao = sInstance;
        }
        return dailyPerpormanceInfoDao;
    }

    protected boolean insertAll(Context context, List<? extends DailyPerformanceInfo> list) {
        if (context == null) {
            Debug.m6w(TAG, "context should not be null while insert all");
            return false;
        } else if (list == null) {
            Debug.m6w(TAG, "running info per km is null while insert all");
            return false;
        } else {
            SQLiteDatabase db = this.mDBHelper.getWritableDatabase();
            long rowId = -1;
            db.beginTransaction();
            for (DailyPerformanceInfo dailyPerformanceInfo : list) {
                rowId = db.insert("daily_perpormance_info", null, createContentValues(dailyPerformanceInfo));
                if (rowId < 0) {
                    break;
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            if (rowId >= 0) {
                return true;
            }
            return false;
        }
    }

    private ContentValues createContentValues(DailyPerformanceInfo dailyPerformanceInfo) {
        ContentValues values = new ContentValues();
        values.put("track_id", Long.valueOf(dailyPerformanceInfo.getmTrackId()));
        values.put("te_value", Integer.valueOf(dailyPerformanceInfo.getTeValue()));
        values.put("daily_performance", Float.valueOf(dailyPerformanceInfo.getDailyPorpermence()));
        values.put("save_data_type", Integer.valueOf(dailyPerformanceInfo.getSaveDataType()));
        values.put("create_time", Long.valueOf(dailyPerformanceInfo.getCreateTime()));
        values.put("currnet_kilos", Double.valueOf(dailyPerformanceInfo.getCurrnetKilos()));
        return values;
    }
}
