package com.huami.watch.newsport.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.huami.watch.common.db.AsyncDatabaseManager;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.db.dao.Vo2MaxDayInfoDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.utils.LogUtil;
import java.util.List;

public class SportDatabaseControlCenter {
    private static final String TAG = SportDatabaseControlCenter.class.getName();
    private Context mContext;
    private SyncDatabaseManager<CyclingDetail> mCyclingDetailDataManager = null;
    private SyncDatabaseManager<DailyPerformanceInfo> mDailyPerformanceInfoManager = null;
    private AsyncDatabaseManager<HeartRate> mHeartRateAsyncDatabaseManager = null;
    private SyncDatabaseManager<HeartRate> mHeartRateSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerLap> mRunningInfoPerIndividualSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerKM> mRunningInfoPerKMSyncDatabaseManager = null;
    private AsyncDatabaseManager<SportLocationData> mSportAsyncLocationDataManager = null;
    private SyncDatabaseManager<SportLocationData> mSportLocationDataSyncDatabaseManager = null;
    private AsyncDatabaseManager<SportSummary> mSportSummaryAsyncDatabaseManager = null;
    private SyncDatabaseManager<SportSummary> mSportSummarySyncDatabaseManager = null;
    private SyncDatabaseManager<SportThaInfo> mSportThaInfoDataManager = null;
    private Handler mStorageHandler;
    private HandlerThread mStorageHandlerThread = new HandlerThread("storage-thread");
    private SyncDatabaseManager<Vo2maxDayInfo> mVo2MaxDayInfoManager = null;

    public SportDatabaseControlCenter(Context context) {
        this.mStorageHandlerThread.start();
        this.mStorageHandler = new Handler(this.mStorageHandlerThread.getLooper());
        this.mContext = context.getApplicationContext();
        this.mSportSummarySyncDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(this.mContext));
        this.mSportLocationDataSyncDatabaseManager = new SyncDatabaseManager(LocationDataDao.getInstance(this.mContext));
        this.mHeartRateSyncDatabaseManager = new SyncDatabaseManager(HeartRateDao.getInstance(this.mContext));
        this.mRunningInfoPerKMSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerKMDao.getInstance(this.mContext));
        this.mRunningInfoPerIndividualSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(this.mContext));
        this.mCyclingDetailDataManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(this.mContext));
        this.mSportThaInfoDataManager = new SyncDatabaseManager(SportThaInfoDao.getmInstance(this.mContext));
        this.mDailyPerformanceInfoManager = new SyncDatabaseManager(DailyPerpormanceInfoDao.getInstance(this.mContext));
        this.mVo2MaxDayInfoManager = new SyncDatabaseManager(Vo2MaxDayInfoDao.getInstance(this.mContext));
        this.mSportSummaryAsyncDatabaseManager = new AsyncDatabaseManager(SportSummaryDao.getInstance(this.mContext), Global.getGlobalHeartHandler());
        this.mHeartRateAsyncDatabaseManager = new AsyncDatabaseManager(HeartRateDao.getInstance(this.mContext), Global.getGlobalHeartHandler());
        this.mSportAsyncLocationDataManager = new AsyncDatabaseManager(LocationDataDao.getInstance(this.mContext), Global.getGlobalHeartHandler());
    }

    public List<? extends SportSummary> getSportSummaryList(String whereClause, String[] whereClauseArgs, String orderby, String limit) {
        return this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, whereClause, whereClauseArgs, orderby, limit);
    }

    public boolean updateSportSummary(OutdoorSportSummary sportSummary) {
        boolean isSuccess = this.mSportSummarySyncDatabaseManager.update(this.mContext, sportSummary);
        Debug.m5i(TAG, "updateSportSummary, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public boolean deleteSportSummary(String whereClause, String[] whereClauseArgs) {
        boolean isDeleteSportSummarySuccess = this.mSportSummarySyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs);
        boolean isDeleteSportLocationSuccess = this.mSportLocationDataSyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs);
        boolean isDeleteHeartRateSuccess = this.mHeartRateSyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs);
        boolean isDeleteKmInfoSuccess = this.mRunningInfoPerKMSyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs);
        boolean isDeletePerLapSuccess = this.mRunningInfoPerIndividualSyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs);
        Debug.m5i(TAG, "deleteSportSummary, isDeleteSportSummarySuccess:" + isDeleteSportSummarySuccess + ", isDeleteSportLocationSuccess:" + isDeleteSportLocationSuccess + ", isDeleteHeartRateSuccess:" + isDeleteHeartRateSuccess + ", isDeleteKmInfoSuccess:" + isDeleteKmInfoSuccess + ", isDeletePerLapSuccess:" + isDeletePerLapSuccess);
        return isDeletePerLapSuccess && isDeleteKmInfoSuccess && isDeleteHeartRateSuccess && isDeleteSportLocationSuccess && isDeleteSportSummarySuccess;
    }

    public boolean addKmInfs(List<? extends RunningInfoPerKM> runningInfoPerKMs) {
        boolean isSuccess = this.mRunningInfoPerKMSyncDatabaseManager.addAll(this.mContext, runningInfoPerKMs);
        Debug.m5i(TAG, "addKmInfs, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public boolean addDailyPerpormanceInfoList(List<? extends DailyPerformanceInfo> list) {
        boolean isSuccess = this.mDailyPerformanceInfoManager.addAll(this.mContext, list);
        Debug.m5i(TAG, "addDailyPerpormanceInfoList, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public boolean addVo2MaxDayInfo(Vo2maxDayInfo vo2maxDayInfo) {
        boolean isSuccess = this.mVo2MaxDayInfoManager.add(this.mContext, vo2maxDayInfo);
        Debug.m5i(TAG, "addVo2MaxDayInfo, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public float getLapMaxPace(long startTime, long endTime) {
        Debug.m5i(TAG, "getLapMaxPace, startTime:" + startTime + ", endTime:" + endTime);
        List<HeartRate> heartRates = HeartRateDao.getInstance(this.mContext).selectRawQuery("select * from heart_rate where pace=(select max(pace) from heart_rate where time>=? and time<=?);", new String[]{"" + startTime, "" + endTime});
        if (heartRates == null || heartRates.isEmpty()) {
            Debug.m5i(TAG, "getLapMaxPace, heart rate is empty," + heartRates);
            return 0.0f;
        }
        Debug.m5i(TAG, "getLapMaxPace, pace:" + ((HeartRate) heartRates.get(0)).getPace());
        return ((HeartRate) heartRates.get(0)).getPace();
    }

    public float getLapMaxPace(long startTime, long endTime, long trackId) {
        Debug.m5i(TAG, "getLapMaxPace, startTime:" + startTime + ", endTime:" + endTime);
        List<HeartRate> heartRates = HeartRateDao.getInstance(this.mContext).selectRawQuery("select * from heart_rate where track_id=? AND pace=(select max(pace) from heart_rate where time>=? and time<=?);", new String[]{"" + trackId, "" + startTime, "" + endTime});
        if (heartRates == null || heartRates.isEmpty()) {
            Debug.m5i(TAG, "getLapMaxPace, heart rate is empty," + heartRates);
            return 0.0f;
        }
        Debug.m5i(TAG, "getLapMaxPace, pace:" + ((HeartRate) heartRates.get(0)).getPace());
        return ((HeartRate) heartRates.get(0)).getPace();
    }

    public boolean addLaps(List<? extends RunningInfoPerLap> laps) {
        boolean isSuccess = this.mRunningInfoPerIndividualSyncDatabaseManager.addAll(this.mContext, laps);
        Debug.m5i(TAG, "addLaps, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public boolean addCadenceDetailList(List<? extends CyclingDetail> list) {
        boolean isSuccess = this.mCyclingDetailDataManager.addAll(this.mContext, list);
        Debug.m5i(TAG, "addCadenceDetailList, isSuccess:" + isSuccess);
        return isSuccess;
    }

    public void asyncUpdateSportSummary(SportSummary summary, Callback callback) {
        this.mSportSummaryAsyncDatabaseManager.update(this.mContext, summary, callback);
    }

    public void asyncAddSportSummary(SportSummary summary, Callback callback) {
        this.mSportSummaryAsyncDatabaseManager.add(this.mContext, summary, callback);
    }

    public void asyncgetAllSportSummary(String whereClause, String[] whereClauseArgs, String orderby, String limit, Callback callback) {
        this.mSportSummaryAsyncDatabaseManager.selectAll(this.mContext, whereClause, whereClauseArgs, orderby, limit, callback);
    }

    public void asyncDeleteSportSummary(final String whereClause, final String[] whereClauseArgs, final Callback callback) {
        this.mSportSummaryAsyncDatabaseManager.remove(this.mContext, whereClause, whereClauseArgs, new Callback() {
            protected void doCallback(int code, Object params) {
                SportDatabaseControlCenter.this.mSportLocationDataSyncDatabaseManager.remove(SportDatabaseControlCenter.this.mContext, whereClause, whereClauseArgs);
                SportDatabaseControlCenter.this.mHeartRateSyncDatabaseManager.remove(SportDatabaseControlCenter.this.mContext, whereClause, whereClauseArgs);
                SportDatabaseControlCenter.this.mRunningInfoPerKMSyncDatabaseManager.remove(SportDatabaseControlCenter.this.mContext, whereClause, whereClauseArgs);
                SportDatabaseControlCenter.this.mRunningInfoPerIndividualSyncDatabaseManager.remove(SportDatabaseControlCenter.this.mContext, whereClause, whereClauseArgs);
                callback.notifyCallback(code, params);
            }
        });
    }

    public void asyncAddHeartData(List<HeartRate> heartRates, Callback callback) {
        this.mHeartRateAsyncDatabaseManager.addAll(this.mContext, heartRates, callback);
    }

    public boolean insertOrUpdateThaInfo(SportThaInfo sportThaInfo) {
        boolean result = this.mSportThaInfoDataManager.add(this.mContext, sportThaInfo);
        Log.i(TAG, "insertOrUpdateThaInfo : " + result);
        return result;
    }

    public void updateLocationAltitude(long trackId, float offsetAltitude) {
        StringBuilder locBuilder = new StringBuilder();
        locBuilder.append("update location_data set altitude=altitude").append(offsetAltitude < 0.0f ? "-" : "+").append(String.valueOf(Math.abs(offsetAltitude))).append(" where track_id=").append(String.valueOf(trackId)).append(" and altitude>-20000");
        LogUtil.m9i(true, TAG, "updateLocationAltitude, sql:" + locBuilder.toString());
        StringBuilder hrBuilder = new StringBuilder();
        hrBuilder.append("update heart_rate set altitude=altitude").append(offsetAltitude < 0.0f ? "-" : "+").append(String.valueOf(Math.abs(offsetAltitude))).append(" where track_id=").append(String.valueOf(trackId)).append(" and altitude>-20000");
        LogUtil.m9i(true, TAG, "updateLocationAltitude, sql:" + hrBuilder.toString());
        if (Float.compare(0.0f, offsetAltitude) != 0) {
            long costTime = System.currentTimeMillis();
            SQLiteDatabase db = SportDataHelper.getInstance(this.mContext).getWritableDatabase();
            db.beginTransaction();
            db.execSQL(locBuilder.toString());
            db.execSQL(hrBuilder.toString());
            db.setTransactionSuccessful();
            db.endTransaction();
            LogUtil.m9i(true, TAG, "updateLocationAltitude, costTime:" + (System.currentTimeMillis() - costTime));
        }
    }
}
