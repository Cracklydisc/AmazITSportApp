package com.huami.watch.newsport.syncservice;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.StepConfig;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.db.dao.Vo2MaxDayInfoDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.syncservice.parser.SportDetailParser;
import com.huami.watch.newsport.syncservice.parser.SportSummaryParser;
import com.huami.watch.newsport.syncservice.parser.StepConfigParser;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import com.huami.watch.wifitrans.control.client.AbstractWifiTransClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class WifiSyncClient extends AbstractWifiTransClient {
    private static final String TAG = WifiSyncClient.class.getName();
    private static WifiSyncClient sInstance = null;
    private SyncDatabaseManager<HeartRate> mHeartRateSyncDatabaseManager = null;
    private SyncDatabaseManager<SportLocationData> mLocationDataSyncDatabaseManager = null;
    private SyncDatabaseManager<DailyPerformanceInfo> mRunningDailyformanceInfoManager = null;
    private SyncDatabaseManager<CyclingDetail> mRunningInfoCadenceSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerKM> mRunningInfoPerKMSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerLap> mRunningInfoPerLapSyncDatabaseManager = null;
    private SportDetailParser mSportDetailParser = new SportDetailParser();
    private SyncDatabaseManager<SportSummary> mSportSummaryDatabaseManager = null;
    private SportSummaryParser mSportSummaryParser = new SportSummaryParser();
    private SyncDatabaseManager<SportThaInfo> mSportThaInfoDataBaseManager = null;
    private StepConfigParser mStepConfigParser = new StepConfigParser();
    private int mSyncCount = 0;
    private int mUploadCount = 0;
    private SyncDatabaseManager<Vo2maxDayInfo> mVo2maxDataBaseManager = null;

    public static WifiSyncClient newInstance(Context context) {
        if (sInstance == null) {
            sInstance = new WifiSyncClient(context.getApplicationContext());
        }
        return sInstance;
    }

    public WifiSyncClient(Context context) {
        super(context);
        this.mSportSummaryDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(context));
        this.mLocationDataSyncDatabaseManager = new SyncDatabaseManager(LocationDataDao.getInstance(context));
        this.mRunningInfoPerKMSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerKMDao.getInstance(context));
        this.mHeartRateSyncDatabaseManager = new SyncDatabaseManager(HeartRateDao.getInstance(context));
        this.mRunningInfoPerLapSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(context));
        this.mRunningInfoCadenceSyncDatabaseManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(context));
        this.mRunningDailyformanceInfoManager = new SyncDatabaseManager(DailyPerpormanceInfoDao.getInstance(context));
        this.mVo2maxDataBaseManager = new SyncDatabaseManager(Vo2MaxDayInfoDao.getInstance(this.mContext));
        this.mSportThaInfoDataBaseManager = new SyncDatabaseManager(SportThaInfoDao.getmInstance(this.mContext));
    }

    protected int requestCount() {
        this.mSyncCount = getSyncCount();
        this.mSyncCount += getFirstBeatDataUploadCount();
        Debug.m3d(TAG, "requesCount " + this.mSyncCount);
        if (this.mSyncCount == 0) {
            tellServerFinishSelf(137, 0);
        }
        return this.mSyncCount;
    }

    protected int onStartWifiTrans() {
        Debug.m3d(TAG, "=============begin, upload alll unsync sport data ================");
        startUploadAllUnSyncSportDatas();
        Debug.m3d(TAG, "=============end, upload alll unsync sport data ================");
        return 0;
    }

    private void uploadSportDataFailed(SportSummary summary) {
        Debug.m5i(TAG, "send summary id:" + summary.getTrackId() + " failed");
        Debug.m5i(TAG, "upload success sport item count: " + this.mUploadCount);
        summary.setCurrentStatus(2);
        Debug.m6w(TAG, "isUpdateFailedSuccess, isSuccess :" + this.mSportSummaryDatabaseManager.update(this.mContext, summary) + "trackId:" + summary.getTrackId());
        tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
    }

    public void startUploadAllUnSyncSportDatas() {
        Debug.m3d(TAG, "upload alll unsync sport data");
        List<? extends SportSummary> sportSummaries = sortSportSummarys(this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=2 OR current_status=3 OR current_status=8 OR current_status=5", null, null, null));
        this.mUploadCount = 0;
        if (sportSummaries != null) {
            Debug.m3d(TAG, "upload alll unsync sport data, size: " + sportSummaries.size());
            for (SportSummary summary : sportSummaries) {
                if (startUploadSportData(summary)) {
                    this.mUploadCount += 2;
                } else {
                    uploadSportDataFailed(summary);
                    return;
                }
            }
        }
        if (startUploadDeleteSummary()) {
            if (isNeedUploadStepConfig() && startUploadStepConfig()) {
                this.mUploadCount++;
            }
            Debug.m5i(TAG, "upload success sport item count: " + this.mUploadCount);
            if (!UnitConvertUtils.isHuangheMode()) {
                sendSportThaInfoDataByWifi();
                sendVo2maxDataByWifi();
                sendFistBeatConfigByWifi();
            }
            if (this.mUploadCount == this.mSyncCount) {
                tellServerFinishSelf(137, this.mUploadCount);
                return;
            } else {
                tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
                return;
            }
        }
        Debug.m5i(TAG, "upload delete summary failed");
        Debug.m5i(TAG, "upload success sport item count: " + this.mUploadCount);
        tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
    }

    private boolean startUploadDeleteSummary() {
        List<? extends SportSummary> sportSummaries = this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=7", null, null, null);
        if (sportSummaries == null || sportSummaries.isEmpty()) {
            return true;
        }
        for (SportSummary summary : sportSummaries) {
            if (!sendDeleteSportSummaryRequest(summary)) {
                return false;
            }
            this.mUploadCount++;
        }
        return true;
    }

    private boolean sendDeleteSportSummaryRequest(SportSummary sportSummary) {
        if (sportSummary == null) {
            Debug.m6w(TAG, "cannot find sport summary");
            return false;
        }
        Debug.m3d(TAG, "sendDeleteSportSummaryRequest, track id : " + sportSummary.getTrackId());
        if (this.mSportSummaryParser.parseTrackIdToMap((OutdoorSportSummary) sportSummary) == null) {
            Debug.m6w(TAG, "cannot parse summary : " + sportSummary);
            return false;
        }
        return sendRequest(new DataItem(String.format(Config.getUrlDeleteSummary(), new Object[]{Long.valueOf(sportSummary.getTrackId() / 1000)}), "com.huami.watch.sport.delete_summary", "com.huami.watch.newsport", "get", ""), sportSummary);
    }

    private void receiveDeleteSportSummary(long trackId) {
        if (this.mSportSummaryDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId})) {
            this.mLocationDataSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
            this.mRunningInfoPerKMSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
            this.mRunningInfoPerLapSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
            this.mHeartRateSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
            return;
        }
        Debug.m6w(TAG, "delete sport summary failed");
    }

    private boolean startUploadSportData(SportSummary sportSummary) {
        Debug.m3d(TAG, "upload sport data " + sportSummary);
        if (sportSummary == null) {
            Debug.m6w(TAG, "sport summary is null while upload summary");
            return false;
        }
        sportSummary.addSyncCount();
        sportSummary.setCurrentStatus(3);
        Debug.m6w(TAG, "start upload sport summary, isSuccess :" + this.mSportSummaryDatabaseManager.update(this.mContext, sportSummary) + "trackId:" + sportSummary.getTrackId());
        return sendUploadSportDetailRequest(sportSummary);
    }

    private boolean sendUploadSportDetailRequest(SportSummary sportSummary) {
        if (sportSummary == null) {
            Debug.m3d(TAG, "sport summary is null");
            return false;
        }
        long trackId = sportSummary.getTrackId();
        Debug.m3d(TAG, "sendUploadSportDetailRequest, track id " + trackId);
        if (DataManager.getInstance().isUploadDetailSportCached(trackId)) {
            String detail = UploadCacheManager.getInstance(this.mContext).newSaver(trackId).getAllDetailData();
            LogUtil.m9i(true, TAG, "sendUploadSportDetailRequest, detail:" + detail);
            if (detail != null) {
                DataItem dataItem = new DataItem(Config.getUrlUploadDetail(), "com.huami.watch.sport.upload_detail", "com.huami.watch.newsport", "post", detail);
                packetPostDataItem(dataItem);
                return sendRequest(dataItem, sportSummary);
            }
        }
        DetailInfoSaver saver = UploadCacheManager.getInstance(this.mContext).getSingleSportSaver(trackId);
        if (saver == null) {
            UploadCacheManager.getInstance(this.mContext).addSportInfo(trackId);
            saver = UploadCacheManager.getInstance(this.mContext).getSingleSportSaver(trackId);
        }
        List<? extends SportLocationData> locationDatas = this.mLocationDataSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (locationDatas == null || locationDatas.isEmpty()) {
            Debug.m6w(TAG, "location is null or empty. track id : " + trackId);
        }
        List<? extends RunningInfoPerKM> runningInfoPerKMs = this.mRunningInfoPerKMSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerKMs == null || runningInfoPerKMs.isEmpty()) {
            Debug.m6w(TAG, "running info is null or empty. track id : " + trackId);
        }
        List<? extends HeartRate> heartRates = this.mHeartRateSyncDatabaseManager.selectAll(this.mContext, "time>=? AND time<=?", new String[]{"" + sportSummary.getStartTime(), "" + sportSummary.getEndTime()}, null, null);
        List<? extends RunningInfoPerLap> runningInfoPerIndividuals = this.mRunningInfoPerLapSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerIndividuals == null || runningInfoPerIndividuals.isEmpty()) {
            Debug.m6w(TAG, "running laps info is null or empty. track id : " + trackId);
        }
        List<? extends CyclingDetail> cyclingDetails = this.mRunningInfoCadenceSyncDatabaseManager.selectAll(this.mContext, "track_id>=?", new String[]{"" + trackId}, null, null);
        if (cyclingDetails == null || cyclingDetails.isEmpty()) {
            Debug.m6w(TAG, "running cadence info is null or empty. track id : " + trackId);
        }
        List<? extends DailyPerformanceInfo> dailyPerformanceInfos = this.mRunningDailyformanceInfoManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (dailyPerformanceInfos == null || dailyPerformanceInfos.isEmpty()) {
            Debug.m6w(TAG, "running daily info is null or empty. track id : " + trackId);
        }
        Map<String, String> params = this.mSportDetailParser.parseDetailToMap((OutdoorSportSummary) sportSummary, locationDatas, runningInfoPerKMs, runningInfoPerIndividuals, heartRates, cyclingDetails, dailyPerformanceInfos);
        if (params == null) {
            Debug.m6w(TAG, "cannot parse detail. location : " + locationDatas + ", running info : " + runningInfoPerKMs);
            return false;
        }
        final String content = parseMapToData(params);
        DataItem dataItem2 = new DataItem(Config.getUrlUploadDetail(), "com.huami.watch.sport.upload_detail", "com.huami.watch.newsport", "post", content);
        packetPostDataItem(dataItem2);
        boolean sendRequest = sendRequest(dataItem2, sportSummary);
        final DetailInfoSaver detailInfoSaver = saver;
        Global.getGlobalHeartHandler().post(new Runnable() {
            public void run() {
                detailInfoSaver.saveAllUploadDetailDataFromOldUpload(content);
            }
        });
        return sendRequest;
    }

    private boolean sendRequest(DataItem dataItem, SportSummary sportSummary) {
        Debug.m3d(TAG, "send request " + dataItem + ", track id : " + (sportSummary == null ? -1 : sportSummary.getTrackId()));
        DataItem dataResponse = request(dataItem);
        Debug.m3d(TAG, new StringBuilder().append("data response").append(dataResponse).toString() == null ? "NULL" : dataResponse.toString());
        if (dataResponse != null) {
            String action = dataResponse.getAction();
            String jsonData = parseServerResponse(dataResponse);
            if (jsonData == null) {
                Debug.m3d(TAG, "cannot get server detail response content");
                return false;
            }
            try {
                int code = new JSONObject(jsonData).optInt("code", -1);
                if (code != 1) {
                    Debug.m3d(TAG, "cannot upload data to server, response code is " + code);
                    return false;
                } else if ("com.huami.watch.sport.upload_detail".equals(action)) {
                    return sendUploadSportSummaryRequest(sportSummary);
                } else {
                    if ("com.huami.watch.sport.upload_summary".equals(action)) {
                        if (sportSummary == null) {
                            Debug.m3d(TAG, "receive action: com.huami.watch.sport.upload_summary    sport summary is null");
                        } else {
                            uploadSportSummaryFinished(sportSummary.getTrackId(), true);
                            Debug.m5i(TAG, "upload summary success, a whole process is over, track id:" + sportSummary.getTrackId());
                        }
                    } else if ("com.huami.watch.sport.upload_step_config".equals(action)) {
                        Debug.m5i(TAG, "upload Step_config success, a whole process is over");
                    } else if ("com.huami.watch.sport.delete_summary".equals(action)) {
                        Debug.m5i(TAG, "upload deleteSummary success trackId:" + sportSummary.getTrackId());
                        receiveDeleteSportSummary(sportSummary.getTrackId());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public void uploadSportSummaryFinished(long trackId, boolean success) {
        Debug.m3d(TAG, "uploadSportSummaryFinished. track id : " + trackId + ", success : " + success);
        SportSummary sportSummary = (SportSummary) this.mSportSummaryDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
        if (sportSummary == null) {
            Debug.m3d(TAG, "uploadSportSummaryFinished. failed summary is null");
            return;
        }
        sportSummary.setCurrentStatus(6);
        this.mSportSummaryDatabaseManager.update(this.mContext, sportSummary);
    }

    private boolean sendUploadSportSummaryRequest(SportSummary sportSummary) {
        if (sportSummary == null) {
            Debug.m6w(TAG, "cannot find sport summary");
            return false;
        }
        Debug.m3d(TAG, "sendUploadSportSummaryRequest, track id : " + sportSummary.getTrackId());
        if (sportSummary.getCurrentStatus() == 7) {
            Debug.m6w(TAG, "cannot send sport summary request, the record is already deleted");
            return false;
        }
        Map<String, String> params = this.mSportSummaryParser.parseSummaryToMap((OutdoorSportSummary) sportSummary);
        if (params == null) {
            Debug.m6w(TAG, "cannot parse summary : " + sportSummary);
            return false;
        }
        DataItem dataItem = new DataItem(Config.getUrlUploadSummary(), "com.huami.watch.sport.upload_summary", "com.huami.watch.newsport", "post", parseMapToData(params));
        packetPostDataItem(dataItem);
        return sendRequest(dataItem, sportSummary);
    }

    private String parseServerResponse(DataItem response) {
        if (response.getState() != 0) {
            Debug.m6w(TAG, "request has not been processed by server!, state:" + response.getState() + ", code:" + response.getCode());
            return null;
        }
        String data = response.getData();
        if (data != null && !data.isEmpty()) {
            return DataUtils.drillUnzipData(response);
        }
        Debug.m6w(TAG, "data is null or empty");
        return null;
    }

    private String parseMapToData(Map<String, String> params) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "parseMapToData. map : " + params);
        }
        if (params == null) {
            Debug.m6w(TAG, "params is null");
            return null;
        }
        StringBuilder dataBuilder = new StringBuilder();
        Iterator iterator = params.keySet().iterator();
        if (!iterator.hasNext()) {
            return "";
        }
        String key = (String) iterator.next();
        dataBuilder.append(key).append("=").append((String) params.get(key));
        while (iterator.hasNext()) {
            String k = (String) iterator.next();
            dataBuilder.append("&").append(k).append("=").append((String) params.get(k));
        }
        String data = dataBuilder.toString();
        if (!Global.DEBUG_LEVEL_3) {
            return data;
        }
        Debug.m3d(TAG, "parsed data : " + data);
        return data;
    }

    private void packetPostDataItem(DataItem dataItem) {
        dataItem.addFlags(16);
        dataItem.addFlags(8);
        dataItem.addHeader("Content-Type", "application/x-www-form-urlencoded");
        dataItem.addHeader("Accept", "*/*");
    }

    private boolean startUploadStepConfig() {
        Debug.m3d(TAG, "sendUploadSportStepConfigRequest");
        StepConfig config = DataManager.getInstance().getStepConfig(this.mContext);
        if (config == null || config.isEmpty()) {
            Debug.m6w("TAG", "cannot find sport step config");
            return false;
        }
        Map<String, String> params = this.mStepConfigParser.parseStepConfigToMap(config);
        if (params == null) {
            Debug.m6w("TAG", "cannot parse step config : " + config.toString());
            return false;
        }
        DataItem dataItem = new DataItem(Config.getUrlUploadConfig(), "com.huami.watch.sport.upload_step_config", "com.huami.watch.newsport", "post", parseMapToData(params));
        packetPostDataItem(dataItem);
        sendRequest(dataItem, null);
        return true;
    }

    private int getSyncCount() {
        Debug.m3d(TAG, "getSyncCount");
        List<? extends SportSummary> sportSummaries = this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=2 OR current_status=3 OR current_status=8 OR current_status=5", null, null, null);
        int count = 0;
        if (sportSummaries != null) {
            count = sportSummaries.size() * 2;
        }
        count += getDeleteSummaryCount();
        return isNeedUploadStepConfig() ? count + 1 : count;
    }

    private boolean isNeedUploadStepConfig() {
        Debug.m3d(TAG, "isNeedUploadStepConfig");
        StepConfig config = DataManager.getInstance().getStepConfig(this.mContext);
        if (config != null && !config.isEmpty()) {
            return true;
        }
        Debug.m6w("TAG", "cannot find sport step config");
        return false;
    }

    private int getDeleteSummaryCount() {
        Debug.m3d(TAG, "isNeedDeleteSummary");
        List<? extends SportSummary> sportSummaries = this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=7", null, null, null);
        if (sportSummaries == null || sportSummaries.isEmpty()) {
            return 0;
        }
        return sportSummaries.size();
    }

    private List<? extends SportSummary> sortSportSummarys(List<? extends SportSummary> sportSummaryList) {
        if (sportSummaryList == null) {
            return null;
        }
        List<SportSummary> mixSummarys = new ArrayList();
        List<? extends SportSummary> availAbleSummarys = new ArrayList();
        for (int i = 0; i < sportSummaryList.size(); i++) {
            SportSummary sportSummary = (SportSummary) sportSummaryList.get(i);
            if (SportType.isMixedSport(sportSummary.getSportType())) {
                mixSummarys.add(sportSummary);
            } else {
                availAbleSummarys.add(sportSummary);
            }
        }
        availAbleSummarys.addAll(mixSummarys);
        return availAbleSummarys;
    }

    private int getFirstBeatDataUploadCount() {
        if (UnitConvertUtils.isHuangheMode()) {
            return 0;
        }
        int totalCount = (0 + this.mSportThaInfoDataBaseManager.selectAll(this.mContext, "upload_status=? or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null).size()) + this.mVo2maxDataBaseManager.selectAll(this.mContext, "upload_status=?  or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null).size();
        if (DataManager.getInstance().getFirstBeatConfig(this.mContext) != null) {
            totalCount++;
        }
        return totalCount;
    }

    private void sendSportThaInfoDataByWifi() {
        if (!ModelUtil.isModelHuanghe(null)) {
            List<SportThaInfo> sportThaInfoList = this.mSportThaInfoDataBaseManager.selectAll(this.mContext, "upload_status=? or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
            if (sportThaInfoList == null || sportThaInfoList.size() != 0) {
                Log.i(TAG, "sendSportThaInfoDataByWifi:" + sportThaInfoList.size());
                int i = 0;
                while (i < sportThaInfoList.size()) {
                    boolean isUploadResult = sendSportThaInfoDataItem((SportThaInfo) sportThaInfoList.get(i));
                    Log.i(TAG, " sendSportThaInfoDataItemResult:" + isUploadResult);
                    if (isUploadResult) {
                        this.mUploadCount++;
                        i++;
                    } else {
                        tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
                        return;
                    }
                }
            }
        }
    }

    private void sendVo2maxDataByWifi() {
        List<Vo2maxDayInfo> vo2maxCountList = this.mVo2maxDataBaseManager.selectAll(this.mContext, "upload_status=?  or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
        if (vo2maxCountList == null || vo2maxCountList.size() != 0) {
            Log.i(TAG, "sendVo2maxDataByWifi:" + vo2maxCountList.size());
            int i = 0;
            while (i < vo2maxCountList.size()) {
                boolean isUploadResult = sendVo2maxDataItem((Vo2maxDayInfo) vo2maxCountList.get(i));
                Log.i(TAG, "  sendVo2maxDataItemResult:" + isUploadResult);
                if (isUploadResult) {
                    this.mUploadCount++;
                    i++;
                } else {
                    tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
                    return;
                }
            }
        }
    }

    private void sendFistBeatConfigByWifi() {
        FirstBeatConfig firstBeatConfig = DataManager.getInstance().getFirstBeatConfig(this.mContext);
        if (firstBeatConfig != null) {
            Log.i(TAG, " sendFirstBeatConfigRequest:" + firstBeatConfig.toString());
            DataItem dataItem = new DataItem(Config.getURLUploadFirstBeatConfig(), "com.huami.watch.sport.upload_first_beat_config", "com.huami.watch.newsport", "post", firstBeatConfig.toJSON());
            dataItem.addFlags(16);
            dataItem.addFlags(8);
            dataItem.addHeader("Content-Type", "application/json");
            dataItem.addHeader("Accept", "application/json");
            DataItem response = request(dataItem);
            if (response != null) {
                String result = DataUtils.drillUnzipData(response);
                Log.i(TAG, " sendFirstBeatConfigResult:" + result);
                if (result == null || TextUtils.isEmpty(result)) {
                    tellServerFinishSelf(153, this.mSyncCount - this.mUploadCount);
                } else {
                    this.mUploadCount++;
                }
            }
        }
    }

    private boolean sendVo2maxDataItem(Vo2maxDayInfo currentVo2maxDayInfo) {
        String result = currentVo2maxDayInfo.modelToJson();
        Log.i(TAG, " FLAG_DECODE_BE_SEND sendVO2maxThaInfoRequest[" + result + "]");
        DataItem dataItem = new DataItem(Config.getUrlUploadVo2MaxConfig(), "com.huami.watch.sport.upload_vo2_max_config", "com.huami.watch.newsport", "post", result);
        dataItem.addFlags(16);
        dataItem.addFlags(8);
        dataItem.addHeader("Content-Type", "application/json");
        dataItem.addHeader("Accept", "application/json");
        DataItem responseDataItem = request(dataItem);
        if (responseDataItem != null) {
            String responseResult = DataUtils.drillUnzipData(responseDataItem);
            if (responseResult == null || TextUtils.isEmpty(responseResult)) {
                return false;
            }
            Vo2maxDayInfo vo2maxDayInfo = new Vo2maxDayInfo();
            try {
                vo2maxDayInfo.initModelWithJsonObject(new JSONObject(result));
                vo2maxDayInfo.setUploadStatus(1);
                return this.mVo2maxDataBaseManager.add(this.mContext, vo2maxDayInfo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean sendSportThaInfoDataItem(SportThaInfo currentSportThainfo) {
        String result = currentSportThainfo.modelToJson();
        Log.i(TAG, " FLAG_DECODE_BE_SEND sendSportThaInfoRequest[" + result + "]");
        DataItem dataItem = new DataItem(Config.getUrlUploadSportThaInfoConfig(), "com.huami.watch.sport.upload_sport_tha_info_config", "com.huami.watch.newsport", "post", result);
        dataItem.addFlags(16);
        dataItem.addFlags(8);
        dataItem.addHeader("Content-Type", "application/json");
        dataItem.addHeader("Accept", "application/json");
        dataItem.addHeader("Connection", "keep-alive");
        DataItem response = request(dataItem);
        if (response != null) {
            String responseResult = DataUtils.drillUnzipData(response);
            if (responseResult == null || TextUtils.isEmpty(responseResult)) {
                return false;
            }
            SportThaInfo sportThaInfo = new SportThaInfo();
            try {
                sportThaInfo.initModelWithJsonObject(new JSONObject(responseResult));
                sportThaInfo.setUploadStatus(1);
                return this.mSportThaInfoDataBaseManager.add(this.mContext, sportThaInfo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
