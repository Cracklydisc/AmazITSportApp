package com.huami.watch.newsport.syncservice;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.PauseInfo;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.syncservice.parser.SportSummaryParser;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.transdata.ZipUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailInfoSaver {
    private static final String[] FILE_ARRAY = new String[]{"version", "longitude_latitude", "altitude", "accuracy", "time", "gait", "pace", "pause", "kilo_pace", "mile_pace", "lap", "trackid", "source", "heart_rate", "distance", "air_pressure_altitude", "speed", "correct_altitude", "stroke_speed", "cadence", "daily_performance_info", "course", "flag"};
    private static final String TAG = DetailInfoSaver.class.getName();
    boolean isHeartSaveBegin;
    private List<Float> mAltitudeArray;
    private Context mContext;
    private List<CorrectAltitude> mCorrectAltitudeArray;
    long mCurrentSaveLat;
    long mCurrentSaveLng;
    long mCurrentSaveLocationTime;
    private boolean mIsCompletedSaveRecord;
    int mLastAvailableHeartValue;
    long mLastBarSaveTimestamp;
    private long mLastCadenceSaveTime;
    private long mLastSaveAvailableHeartTime;
    long mLastSaveBar;
    private long mLastSaveDisDiffTime;
    int mLastSaveFlag;
    private long mLastSaveHeartTime;
    long mLastSaveLat;
    long mLastSaveLng;
    long mLastSaveLocationTime;
    private SportSummaryParser mSummaryParser;
    private long mTrackId;
    private int mUserVersion;

    private class CorrectAltitude {
        float altitude;
        int timediff;

        public CorrectAltitude(float altitude, int timediff) {
            this.altitude = altitude;
            this.timediff = timediff;
        }
    }

    public DetailInfoSaver(Context context, long trackId) {
        this(context, trackId, 15);
    }

    public DetailInfoSaver(Context context, long trackId, int userVersion) {
        this.mAltitudeArray = new ArrayList();
        this.mCorrectAltitudeArray = new ArrayList();
        this.mSummaryParser = new SportSummaryParser();
        this.mContext = null;
        this.mTrackId = -1;
        this.mLastSaveAvailableHeartTime = -1;
        this.mLastSaveHeartTime = -1;
        this.mLastSaveDisDiffTime = -1;
        this.mLastCadenceSaveTime = -1;
        this.mUserVersion = 12;
        this.mIsCompletedSaveRecord = false;
        this.mCurrentSaveLat = Long.MAX_VALUE;
        this.mCurrentSaveLng = Long.MAX_VALUE;
        this.mCurrentSaveLocationTime = 0;
        this.mLastSaveLat = Long.MAX_VALUE;
        this.mLastSaveLng = Long.MAX_VALUE;
        this.mLastSaveLocationTime = -1;
        this.mLastBarSaveTimestamp = 0;
        this.mLastSaveBar = -1;
        this.mLastSaveFlag = 1;
        this.mLastAvailableHeartValue = 0;
        this.isHeartSaveBegin = true;
        this.mContext = context;
        this.mTrackId = trackId;
        long j = this.mTrackId / 1000;
        this.mLastCadenceSaveTime = j;
        this.mLastSaveDisDiffTime = j;
        this.mLastSaveHeartTime = j;
        this.mLastSaveAvailableHeartTime = j;
        this.mUserVersion = userVersion;
    }

    public void initFile() {
        for (String fileName : FILE_ARRAY) {
            File file = initSavePath(fileName);
            if (file == null) {
                throw new IllegalArgumentException("init file failed");
            }
            StringBuilder builder = new StringBuilder();
            builder.append(fileName).append("=");
            if ("version".equals(fileName)) {
                builder.append(this.mUserVersion);
            } else if ("trackid".equals(fileName)) {
                builder.append(this.mTrackId / 1000);
            } else if ("source".equals(fileName)) {
                builder.append(Config.SOURCE);
            }
            saveDetailInfo2Cache(file, builder.toString());
        }
        initSavePath("summary_loc");
        initSavePath("all_data");
        initSavePath("temp_data");
    }

    private void deleteListInfo() {
        File file;
        for (String fileName : FILE_ARRAY) {
            file = new File(this.mContext.getFilesDir().getAbsoluteFile() + File.separator + this.mTrackId + File.separator + fileName);
            if (file != null && file.exists()) {
                file.delete();
            }
        }
        file = new File(this.mContext.getFilesDir().getAbsoluteFile() + File.separator + this.mTrackId + File.separator + "summary_loc");
        if (file != null && file.exists()) {
            file.delete();
        }
    }

    private void deleteTemp(long trackId) {
        File file = new File(this.mContext.getFilesDir().getAbsoluteFile() + File.separator + trackId + File.separator + "temp_data");
        if (file != null && file.exists()) {
            file.delete();
        }
    }

    private File initSavePath(String fileName) {
        File parentFolder = new File(this.mContext.getFilesDir().getAbsolutePath() + File.separator + this.mTrackId);
        if (!(parentFolder.exists() && parentFolder.isDirectory())) {
            parentFolder.mkdirs();
        }
        File file = new File(parentFolder.getAbsolutePath() + File.separator + fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private File getDetailFile(String fileName) {
        File file = new File(this.mContext.getFilesDir().getAbsoluteFile() + File.separator + this.mTrackId + File.separator + fileName);
        if (file != null && !file.isDirectory() && file.exists()) {
            return file;
        }
        throw new IllegalArgumentException("err data file, " + file);
    }

    public void saveLocationData(List<? extends SportLocationData> locationDatas, boolean isLast) {
        if (locationDatas != null && !locationDatas.isEmpty()) {
            StringBuilder barStringBuilder = new StringBuilder();
            StringBuilder longLatStringBuilder = new StringBuilder();
            StringBuilder altitudeStringBuilder = new StringBuilder();
            StringBuilder accuracyStringBuilder = new StringBuilder();
            StringBuilder timeStringBuilder = new StringBuilder();
            StringBuilder paceStringBuilder = new StringBuilder();
            StringBuilder courseStringBuilder = new StringBuilder();
            StringBuilder pointTypeStringBuilder = new StringBuilder();
            StringBuilder flagStringBuilder = new StringBuilder();
            for (SportLocationData locationData : locationDatas) {
                float pace;
                if (this.mLastSaveBar < 0) {
                    barStringBuilder.append(locationData.mTimestamp / 1000).append(",").append(locationData.mBar).append(";");
                    this.mLastBarSaveTimestamp = locationData.mTimestamp;
                    this.mLastSaveBar = (long) locationData.mBar;
                } else if (locationData.mBar > 0) {
                    barStringBuilder.append((locationData.mTimestamp / 1000) - (this.mLastBarSaveTimestamp / 1000)).append(",").append(((long) locationData.mBar) - this.mLastSaveBar).append(";");
                    this.mLastSaveBar = (long) locationData.mBar;
                    this.mLastBarSaveTimestamp = locationData.mTimestamp;
                }
                this.mCurrentSaveLat = (long) (locationData.mLatitude * 1.0E8f);
                this.mCurrentSaveLng = (long) (locationData.mLongitude * 1.0E8f);
                if (this.mLastSaveLat == Long.MAX_VALUE) {
                    longLatStringBuilder.append(this.mCurrentSaveLat).append(",").append(this.mCurrentSaveLng).append(";");
                } else {
                    longLatStringBuilder.append(this.mCurrentSaveLat - this.mLastSaveLat).append(",").append(this.mCurrentSaveLng - this.mLastSaveLng).append(";");
                }
                this.mAltitudeArray.add(Float.valueOf(locationData.mAltitude));
                accuracyStringBuilder.append((int) locationData.mGPSAccuracy).append(";");
                this.mCurrentSaveLocationTime = locationData.mTimestamp / 1000;
                if (this.mLastSaveLocationTime < 0) {
                    timeStringBuilder.append(locationData.mTimestamp / 1000).append(";");
                } else {
                    timeStringBuilder.append(this.mCurrentSaveLocationTime - this.mLastSaveLocationTime).append(";");
                }
                if (locationData.mSpeed == 0.0f) {
                    pace = 0.0f;
                } else {
                    pace = 1.0f / locationData.mSpeed;
                }
                paceStringBuilder.append(pace).append(";");
                courseStringBuilder.append(locationData.mCourse).append(";");
                pointTypeStringBuilder.append(locationData.mAlgoPointType).append(";");
                if ((locationData.mPointType & 2) == 0) {
                    this.mLastSaveFlag = 1;
                } else if ((locationData.mPointType & 4) != 0) {
                    this.mLastSaveFlag = 3;
                } else {
                    this.mLastSaveFlag = 2;
                }
                flagStringBuilder.append(this.mLastSaveFlag).append(";");
                this.mLastSaveLat = this.mCurrentSaveLat;
                this.mLastSaveLng = this.mCurrentSaveLng;
                this.mLastSaveLocationTime = locationData.mTimestamp / 1000;
            }
            flagStringBuilder.append(this.mLastSaveFlag).append(";");
            if (isLast) {
                if (barStringBuilder.lastIndexOf(";") != -1) {
                    barStringBuilder.delete(barStringBuilder.lastIndexOf(";"), barStringBuilder.length());
                }
                if (longLatStringBuilder.lastIndexOf(";") != -1) {
                    longLatStringBuilder.delete(longLatStringBuilder.lastIndexOf(";"), longLatStringBuilder.length());
                }
                if (altitudeStringBuilder.lastIndexOf(";") != -1) {
                    altitudeStringBuilder.delete(altitudeStringBuilder.lastIndexOf(";"), altitudeStringBuilder.length());
                }
                if (accuracyStringBuilder.lastIndexOf(";") != -1) {
                    accuracyStringBuilder.delete(accuracyStringBuilder.lastIndexOf(";"), accuracyStringBuilder.length());
                }
                if (timeStringBuilder.lastIndexOf(";") != -1) {
                    timeStringBuilder.delete(timeStringBuilder.lastIndexOf(";"), timeStringBuilder.length());
                }
                if (paceStringBuilder.lastIndexOf(";") != -1) {
                    paceStringBuilder.delete(paceStringBuilder.lastIndexOf(";"), paceStringBuilder.length());
                }
                if (courseStringBuilder.lastIndexOf(";") != -1) {
                    courseStringBuilder.delete(courseStringBuilder.lastIndexOf(";"), courseStringBuilder.length());
                }
                if (pointTypeStringBuilder.lastIndexOf(";") != -1) {
                    pointTypeStringBuilder.delete(pointTypeStringBuilder.lastIndexOf(";"), pointTypeStringBuilder.length());
                }
                if (flagStringBuilder.lastIndexOf(";") != -1) {
                    flagStringBuilder.delete(flagStringBuilder.lastIndexOf(";"), flagStringBuilder.length());
                }
            }
            saveDetailInfo2Cache(getDetailFile("air_pressure_altitude"), barStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("longitude_latitude"), longLatStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("accuracy"), accuracyStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("time"), timeStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("pace"), paceStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("course"), courseStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("flag"), pointTypeStringBuilder.toString());
        }
    }

    public void savePauseInfoData(List<? extends PauseInfo> pauseInfos, boolean isLast) {
        if (pauseInfos != null && !pauseInfos.isEmpty()) {
            LogUtil.m9i(true, TAG, "parseDetailToMap, parse info:" + pauseInfos.toString());
            StringBuilder pauseStringBuilder = new StringBuilder();
            for (PauseInfo info : pauseInfos) {
                pauseStringBuilder.append(info.getStart() / 1000).append(",").append((info.getEnd() - info.getStart()) / 1000).append(",").append(-1).append(",").append(-1).append(",").append(info.getPauseType()).append(";");
            }
            if (isLast && pauseStringBuilder.lastIndexOf(";") != -1) {
                pauseStringBuilder.delete(pauseStringBuilder.lastIndexOf(";"), pauseStringBuilder.length());
            }
            saveDetailInfo2Cache(getDetailFile("pause"), pauseStringBuilder.toString());
        }
    }

    public void savePerKmOrMiData(List<? extends RunningInfoPerKM> runningInfoPerKMs, boolean isLast) {
        if (runningInfoPerKMs == null || runningInfoPerKMs.isEmpty()) {
            LogUtil.m9i(true, TAG, "cannot find kilo info");
            return;
        }
        StringBuilder kiloPaceStringBuilder = new StringBuilder();
        StringBuilder miloPaceStringBuilder = new StringBuilder();
        RunningInfoPerKMDao dao = RunningInfoPerKMDao.getInstance(this.mContext);
        String sql = "select count(*) as count, sum(rate) as sum from heart_rate where track_id=? and time>? and time<=? and heart_quality<=?";
        long lastTime = this.mTrackId - 1000;
        double totalCostTime = 0.0d;
        for (int i = 0; i < runningInfoPerKMs.size(); i++) {
            RunningInfoPerKM currentKiloInfo = (RunningInfoPerKM) runningInfoPerKMs.get(i);
            totalCostTime += (double) currentKiloInfo.getCostTime();
            int totalHeartRate = 0;
            int heartRateCount = 0;
            Cursor cursor = dao.selectAsCursorBySql(sql, new String[]{String.valueOf(this.mTrackId), String.valueOf(lastTime), String.valueOf(currentKiloInfo.getTotalCostTime() + currentKiloInfo.getTrackId()), String.valueOf(1)});
            if (cursor != null && cursor.moveToFirst()) {
                heartRateCount = cursor.getInt(0);
                totalHeartRate = cursor.getInt(1);
                cursor.close();
            }
            if (currentKiloInfo.getUnit() == 0) {
                String str;
                int i2;
                StringBuilder append = kiloPaceStringBuilder.append(i).append(",").append(((long) ((int) currentKiloInfo.getCostTime())) / 1000).append(",");
                if (currentKiloInfo.getLongitude() >= 360.0f) {
                    str = "";
                } else {
                    str = Geohash.encode((double) currentKiloInfo.getLatitude(), (double) currentKiloInfo.getLongitude());
                }
                append = append.append(str).append(",").append("-1").append(",");
                if (heartRateCount == 0) {
                    i2 = -1;
                } else {
                    i2 = totalHeartRate / heartRateCount;
                }
                append.append(i2).append(",").append(((long) ((int) totalCostTime)) / 1000).append(";");
            } else {
                miloPaceStringBuilder.append(i).append(",").append(((long) ((int) currentKiloInfo.getCostTime())) / 1000).append(",").append(currentKiloInfo.getLongitude() >= 360.0f ? "" : Geohash.encode((double) currentKiloInfo.getLatitude(), (double) currentKiloInfo.getLongitude())).append(",").append("-1").append(",").append(heartRateCount == 0 ? -1 : totalHeartRate / heartRateCount).append(",").append(((long) ((int) totalCostTime)) / 1000).append(";");
            }
            lastTime = currentKiloInfo.getTotalCostTime() + currentKiloInfo.getTrackId();
        }
        if (isLast) {
            if (kiloPaceStringBuilder.lastIndexOf(";") != -1) {
                kiloPaceStringBuilder.delete(kiloPaceStringBuilder.lastIndexOf(";"), kiloPaceStringBuilder.length());
            }
            if (miloPaceStringBuilder.lastIndexOf(";") != -1) {
                miloPaceStringBuilder.delete(miloPaceStringBuilder.lastIndexOf(";"), miloPaceStringBuilder.length());
            }
        }
        saveDetailInfo2Cache(getDetailFile("kilo_pace"), kiloPaceStringBuilder.toString());
        saveDetailInfo2Cache(getDetailFile("mile_pace"), miloPaceStringBuilder.toString());
    }

    public void saveLapData(List<? extends RunningInfoPerLap> runningInfoPerIndividuals) {
        if (runningInfoPerIndividuals == null || runningInfoPerIndividuals.isEmpty()) {
            LogUtil.m9i(true, TAG, "cannot find per lap info");
            return;
        }
        String str;
        StringBuilder perLapsStringBuilder = new StringBuilder();
        RunningInfoPerLap firstLapInfo = (RunningInfoPerLap) runningInfoPerIndividuals.get(0);
        StringBuilder append = perLapsStringBuilder.append(firstLapInfo.getLapNum()).append(",").append((int) (firstLapInfo.getCostTime() / 1000)).append(",").append((int) firstLapInfo.getDistance()).append(",");
        if (firstLapInfo.getLongitude() >= 360.0f) {
            str = "";
        } else {
            str = Geohash.encode((double) firstLapInfo.getLatitude(), (double) firstLapInfo.getLongitude());
        }
        append.append(str).append(",").append((int) firstLapInfo.getAvgHeart()).append(",").append((int) (firstLapInfo.getTotalCostTime() / 1000)).append(",").append((int) firstLapInfo.getAbsoluteAltitude()).append(",").append((int) firstLapInfo.getClimbUp()).append(",").append((int) firstLapInfo.getClimbDown()).append(",").append(firstLapInfo.getPace()).append(",").append(firstLapInfo.getMaxPace()).append(",").append((int) firstLapInfo.getClimbDistance()).append(",").append(firstLapInfo.getLapStrokeSpeed()).append(",").append(firstLapInfo.getLapStrokes()).append(",").append(firstLapInfo.getLapSwolf()).append(",").append(firstLapInfo.getLapCalories() / 1000).append(",").append((int) (firstLapInfo.getLapStepFreq() * 60.0f)).append(",").append(firstLapInfo.getLapCadence()).append(",").append(firstLapInfo.getLapType());
        for (int i = 1; i < runningInfoPerIndividuals.size(); i++) {
            RunningInfoPerLap currentLapInfo = (RunningInfoPerLap) runningInfoPerIndividuals.get(i);
            append = perLapsStringBuilder.append(";").append(currentLapInfo.getLapNum()).append(",").append((int) (currentLapInfo.getCostTime() / 1000)).append(",").append((int) currentLapInfo.getDistance()).append(",");
            if (currentLapInfo.getLongitude() >= 360.0f) {
                str = "";
            } else {
                str = Geohash.encode((double) currentLapInfo.getLatitude(), (double) currentLapInfo.getLongitude());
            }
            append.append(str).append(",").append((int) currentLapInfo.getAvgHeart()).append(",").append(((long) ((int) currentLapInfo.getTotalCostTime())) / 1000).append(",").append((int) currentLapInfo.getAbsoluteAltitude()).append(",").append((int) currentLapInfo.getClimbUp()).append(",").append((int) currentLapInfo.getClimbDown()).append(",").append(currentLapInfo.getPace()).append(",").append(currentLapInfo.getMaxPace()).append(",").append((int) currentLapInfo.getClimbDistance()).append(",").append(currentLapInfo.getLapStrokeSpeed()).append(",").append(currentLapInfo.getLapStrokes()).append(",").append(currentLapInfo.getLapSwolf()).append(",").append(currentLapInfo.getLapCalories() / 1000).append(",").append((int) (currentLapInfo.getLapStepFreq() * 60.0f)).append(",").append(currentLapInfo.getLapCadence()).append(",").append(currentLapInfo.getLapType());
        }
        saveDetailInfo2Cache(getDetailFile("lap"), perLapsStringBuilder.toString());
    }

    public void saveDailyPerformanceData(List<? extends DailyPerformanceInfo> dailyPerformanceInfos, boolean isLast) {
        if (dailyPerformanceInfos != null && !dailyPerformanceInfos.isEmpty()) {
            StringBuilder dailyBuilder = new StringBuilder();
            for (DailyPerformanceInfo daily : dailyPerformanceInfos) {
                dailyBuilder.append(daily.getSaveDataType() == 0 ? Integer.valueOf((int) daily.getCurrnetKilos()) : DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm(daily.getCurrnetKilos() * 1000.0d), false)).append(",").append((int) daily.getDailyPorpermence()).append(";");
            }
            Log.i(TAG, " upload dailyPerpormanceInfosContent:" + dailyBuilder.toString());
            if (isLast && dailyBuilder.lastIndexOf(";") != -1) {
                dailyBuilder.delete(dailyBuilder.lastIndexOf(";"), dailyBuilder.length());
            }
            saveDetailInfo2Cache(getDetailFile("daily_performance_info"), dailyBuilder.toString());
        }
    }

    public void saveCadenceData(List<? extends CyclingDetail> cadenceList, boolean isLast) {
        if (cadenceList != null && !cadenceList.isEmpty()) {
            StringBuilder cadenceBuilder = new StringBuilder();
            for (CyclingDetail cyclingDetail : cadenceList) {
                long curTime = cyclingDetail.getCurTime() / 1000;
                cadenceBuilder.append((int) (curTime - this.mLastCadenceSaveTime)).append(",").append(cyclingDetail.getCurCadence()).append(";");
                this.mLastCadenceSaveTime = curTime;
            }
            if (isLast && cadenceBuilder.lastIndexOf(";") != -1) {
                cadenceBuilder.delete(cadenceBuilder.lastIndexOf(";"), cadenceBuilder.length());
            }
            saveDetailInfo2Cache(getDetailFile("cadence"), cadenceBuilder.toString());
        }
    }

    public void saveHistoryHeartData(List<? extends HeartRate> heartRates, boolean isLast) {
        if (heartRates != null && !heartRates.isEmpty()) {
            StringBuilder heartRateStringBuilder = new StringBuilder();
            StringBuilder gaitStringBuilder = new StringBuilder();
            StringBuilder distanceStringBuilder = new StringBuilder();
            StringBuilder speedStringBuilder = new StringBuilder();
            StringBuilder correctAltitudeBuilder = new StringBuilder();
            StringBuilder strokeSpeedBuilder = new StringBuilder();
            LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "rate_start");
            for (HeartRate rate : heartRates) {
                long curTime = rate.getTimestamp() / 1000;
                if (rate.getHeartQuality() <= 1) {
                    int heartDiff = rate.getHeartRate() - this.mLastAvailableHeartValue;
                    heartRateStringBuilder.append((int) (curTime - this.mLastSaveAvailableHeartTime)).append(",").append(heartDiff).append(";");
                    this.mLastSaveAvailableHeartTime = curTime;
                    this.mLastAvailableHeartValue = rate.getHeartRate();
                }
                int timeDiff = (int) (curTime - this.mLastSaveHeartTime);
                int disDiff = (int) rate.getDistance();
                int timeDisDiff = (int) (curTime - this.mLastSaveDisDiffTime);
                gaitStringBuilder.append(timeDiff).append(",").append(rate.getStepDiff()).append(",").append(rate.getStride()).append(",").append((int) (rate.getStepFreq() * 60.0f)).append(";");
                if (this.isHeartSaveBegin || disDiff > 0) {
                    distanceStringBuilder.append(timeDisDiff).append(",").append(disDiff).append(";");
                    this.mLastSaveDisDiffTime = curTime;
                }
                if (this.mUserVersion >= 14) {
                    float speed;
                    if (Float.compare(rate.getPace(), 0.0f) == 0) {
                        speed = 0.0f;
                    } else {
                        speed = NumeriConversionUtils.convertStringToFloat(String.valueOf(1.0f / rate.getPace()), 2);
                    }
                    speedStringBuilder.append(timeDiff).append(",").append(speed).append(";");
                }
                if (this.mUserVersion > 14) {
                    this.mCorrectAltitudeArray.add(new CorrectAltitude((float) rate.getAltitude(), timeDiff));
                    strokeSpeedBuilder.append(timeDiff).append(",").append(rate.getStrokeSpeed()).append(";");
                }
                this.mLastSaveHeartTime = curTime;
                this.isHeartSaveBegin = false;
            }
            LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "rate_end");
            if (isLast) {
                if (heartRateStringBuilder.lastIndexOf(";") != -1) {
                    heartRateStringBuilder.delete(heartRateStringBuilder.lastIndexOf(";"), heartRateStringBuilder.length());
                }
                if (gaitStringBuilder.lastIndexOf(";") != -1) {
                    gaitStringBuilder.delete(gaitStringBuilder.lastIndexOf(";"), gaitStringBuilder.length());
                }
                if (distanceStringBuilder.lastIndexOf(";") != -1) {
                    distanceStringBuilder.delete(distanceStringBuilder.lastIndexOf(";"), distanceStringBuilder.length());
                }
                if (speedStringBuilder.lastIndexOf(";") != -1) {
                    speedStringBuilder.delete(speedStringBuilder.lastIndexOf(";"), speedStringBuilder.length());
                }
                if (correctAltitudeBuilder.lastIndexOf(";") != -1) {
                    correctAltitudeBuilder.delete(correctAltitudeBuilder.lastIndexOf(";"), correctAltitudeBuilder.length());
                }
                if (strokeSpeedBuilder.lastIndexOf(";") != -1) {
                    strokeSpeedBuilder.delete(strokeSpeedBuilder.lastIndexOf(";"), strokeSpeedBuilder.length());
                }
            }
            saveDetailInfo2Cache(getDetailFile("heart_rate"), heartRateStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("gait"), gaitStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("distance"), distanceStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("speed"), speedStringBuilder.toString());
            saveDetailInfo2Cache(getDetailFile("stroke_speed"), strokeSpeedBuilder.toString());
        }
    }

    private boolean saveDetailInfo2Cache(String filePath, String content) {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "save file begin," + filePath);
        try {
            FileWriter writer = new FileWriter(filePath, true);
            writer.write(content);
            writer.flush();
            writer.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "save file end," + filePath);
        return false;
    }

    private boolean saveDetailInfo2Cache(File file, String content) {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "save file begin," + file.getName());
        try {
            FileWriter writer = new FileWriter(file, true);
            writer.write(content);
            writer.flush();
            writer.close();
            LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "save file end," + file.getName());
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return false;
    }

    private boolean saveDetailInfo2Cache(File file, byte[] datas) {
        LogUtil.m9i(true, TAG, "save bytes file begin," + file.getName());
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(datas, 0, datas.length);
            outputStream.flush();
            outputStream.close();
            LogUtil.m9i(true, TAG, "save bytes file end," + file.getName());
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return false;
    }

    private String getDetailDataFromCache(String path) {
        LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "getDetailDataFromCache," + path);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            StringBuilder builder = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                builder.append(line);
            }
            reader.close();
            if (builder.lastIndexOf(";") == builder.length() - 1) {
                builder.replace(builder.length() - 1, builder.length(), "");
            }
            LogUtil.m9i(true, TAG, "getDetailDataFromCache, content:" + builder.toString());
            return builder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private byte[] getWholeDataFromCache(String path) {
        IOException e;
        Throwable th;
        byte[] bArr = null;
        LogUtil.m9i(true, TAG, "getWholeDataFromCache," + path);
        File f = new File(path);
        if (f.exists()) {
            FileChannel channel = null;
            FileInputStream fs = null;
            try {
                FileInputStream fs2 = new FileInputStream(f);
                try {
                    channel = fs2.getChannel();
                    ByteBuffer byteBuffer = ByteBuffer.allocate((int) channel.size());
                    do {
                    } while (channel.read(byteBuffer) > 0);
                    bArr = byteBuffer.array();
                    try {
                        channel.close();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    try {
                        fs2.close();
                    } catch (IOException e22) {
                        e22.printStackTrace();
                    }
                } catch (IOException e3) {
                    e22 = e3;
                    fs = fs2;
                    try {
                        e22.printStackTrace();
                        try {
                            channel.close();
                        } catch (IOException e222) {
                            e222.printStackTrace();
                        }
                        try {
                            fs.close();
                        } catch (IOException e2222) {
                            e2222.printStackTrace();
                        }
                        return bArr;
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            channel.close();
                        } catch (IOException e22222) {
                            e22222.printStackTrace();
                        }
                        try {
                            fs.close();
                        } catch (IOException e222222) {
                            e222222.printStackTrace();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fs = fs2;
                    channel.close();
                    fs.close();
                    throw th;
                }
            } catch (IOException e4) {
                e222222 = e4;
                e222222.printStackTrace();
                channel.close();
                fs.close();
                return bArr;
            }
        }
        return bArr;
    }

    private String getSummayData(long trackId) {
        StringBuilder builder = new StringBuilder();
        builder.append(getDetailDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + trackId + File.separator + "summary_loc"));
        return builder.toString();
    }

    private String getAllDetailData(long trackId) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < FILE_ARRAY.length; i++) {
            builder.append(getDetailDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + trackId + File.separator + FILE_ARRAY[i]));
            if (i != FILE_ARRAY.length - 1) {
                builder.append("&");
            }
        }
        return builder.toString();
    }

    public void saveAllDetailData() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < FILE_ARRAY.length; i++) {
            builder.append(getDetailDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + this.mTrackId + File.separator + FILE_ARRAY[i]));
            if (i != FILE_ARRAY.length - 1) {
                builder.append("&");
            }
        }
        this.mIsCompletedSaveRecord = saveDetailInfo2Cache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + this.mTrackId + File.separator + "all_detail_data", builder.toString());
        if (this.mIsCompletedSaveRecord) {
            DataManager.getInstance().setIsUploadDetailDataCached(this.mTrackId, true);
        }
    }

    public void saveSportDataInRun(OutdoorSportSummary sportSummary) {
        LogUtil.m9i(true, TAG, "saveSportDataInRun, summary start");
        saveSummaryInfo(sportSummary);
        LogUtil.m9i(true, TAG, "saveSportDataInRun, summary end");
        LogUtil.m9i(true, TAG, "saveSportDataInRun, detail start");
        if (!DataManager.getInstance().isUploadDetailSportCached(this.mTrackId)) {
            LogUtil.m9i(true, TAG, "saveSportDataInRun, detail in progress");
            saveAllDetailData();
        }
        LogUtil.m9i(true, TAG, "saveSportDataInRun, detail end");
        LogUtil.m9i(true, TAG, "saveSportDataInRun, temp start");
        saveTempSportDataInRun();
        LogUtil.m9i(true, TAG, "saveSportDataInRun, temp end");
        deleteListInfo();
    }

    public void updateAltitudeOffset(float offset) {
        StringBuilder altitudeStringBuilder = new StringBuilder();
        for (Float altitude : this.mAltitudeArray) {
            int altitudeCm = (int) ((altitude.floatValue() + offset) * 100.0f);
            if (Float.compare(-20000.0f, altitude.floatValue()) == 0) {
                altitudeCm = (int) (altitude.floatValue() * 100.0f);
            }
            altitudeStringBuilder.append(altitudeCm).append(";");
        }
        saveDetailInfo2Cache(getDetailFile("altitude"), altitudeStringBuilder.toString());
        StringBuilder correctAltitudeBuilder = new StringBuilder();
        for (CorrectAltitude correctAltitude : this.mCorrectAltitudeArray) {
            int timeDiff = correctAltitude.timediff;
            int altitude2 = (int) (correctAltitude.altitude + offset);
            if (Float.compare(-20000.0f, correctAltitude.altitude) == 0) {
                altitude2 = -20000;
            }
            correctAltitudeBuilder.append(timeDiff).append(",").append(altitude2).append(";");
        }
        saveDetailInfo2Cache(getDetailFile("correct_altitude"), correctAltitudeBuilder.toString());
    }

    public void saveSummaryInfo(OutdoorSportSummary mOutdoorSummary) {
        saveDetailInfo2Cache(getDetailFile("summary_loc"), parseMapToData(this.mSummaryParser.parseSummaryToMap(mOutdoorSummary)));
    }

    public synchronized void saveAllUploadWholeData(byte[] bytes, long trackId) {
        LogUtil.m9i(true, TAG, "#################saveAllUploadWholeData start#################");
        this.mIsCompletedSaveRecord = saveDetailInfo2Cache(initSavePath("all_data"), bytes);
        markingThisCacheRecord(trackId);
        LogUtil.m9i(true, TAG, "#################saveAllUploadWholeData end#################");
    }

    public synchronized String getAllDetailData() {
        String content;
        LogUtil.m9i(true, TAG, "#################getAllDetailData start#################");
        content = getDetailDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + this.mTrackId + File.separator + "all_detail_data");
        LogUtil.m9i(true, TAG, "#################getAllDetailData end#################");
        return content;
    }

    public synchronized byte[] getAllUploadWholeData() {
        byte[] content;
        LogUtil.m9i(true, TAG, "#################getAllUploadWholeData start#################");
        content = getWholeDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + this.mTrackId + File.separator + "all_data");
        LogUtil.m9i(true, TAG, "#################getAllUploadWholeData end#################");
        return content;
    }

    private boolean isTempSportDataCached(long trackId) {
        return DataManager.getInstance().isUploadSportTempCached(trackId);
    }

    private void markingThisTempCacheRecord(long trackId) {
        deleteListInfo();
        DataManager.getInstance().setIsUploadDataTempCached(trackId, true);
    }

    private void markingThisCacheRecord(long trackId) {
        if (this.mIsCompletedSaveRecord) {
            deleteListInfo();
            DataManager.getInstance().setIsUploadDataCached(trackId, true);
        }
    }

    public synchronized void saveAllUploadDetailDataFromOldUpload(String content) {
        LogUtil.m9i(true, TAG, "#################saveAllUploadDetailDataFromOldUpload start#################");
        if (!DataManager.getInstance().isUploadDetailSportCached(this.mTrackId)) {
            if (saveDetailInfo2Cache(initSavePath("all_detail_data"), content)) {
                DataManager.getInstance().setIsUploadDetailDataCached(this.mTrackId, true);
            }
            LogUtil.m9i(true, TAG, "#################saveAllUploadDetailDataFromOldUpload end#################");
        }
    }

    public void saveAllSportDataInRun(SportSummary sportSummary) {
        JSONException e;
        if (sportSummary != null) {
            if (isTempSportDataCached(sportSummary.getTrackId())) {
                JSONObject sportUploadJson = new JSONObject();
                JSONObject dataJson = new JSONObject();
                List<Long> deleteList = new ArrayList();
                deleteList.add(Long.valueOf(sportSummary.getTrackId()));
                try {
                    sportUploadJson.put("dataType", "SPORT");
                    if (SportType.isMixedSport(sportSummary.getSportType())) {
                        dataJson.put("isSingle", false);
                        List<Long> childTrackIdList = sportSummary.getChildSportSummrys();
                        for (Long longValue : childTrackIdList) {
                            if (!isTempSportDataCached(longValue.longValue())) {
                                LogUtil.m9i(true, TAG, "saveAllSportDataInRun failed, cached failed");
                                return;
                            }
                        }
                        String parentData = getTempData(sportSummary.getTrackId());
                        JSONArray jsonArray = new JSONArray();
                        int i = 0;
                        for (Long longValue2 : childTrackIdList) {
                            long childId = longValue2.longValue();
                            String data = getTempData(childId);
                            if (TextUtils.isEmpty(data)) {
                                LogUtil.m9i(true, TAG, "saveAllSportDataInRun failed, empty data," + childId);
                                return;
                            }
                            jsonArray.put(i, new JSONObject(data));
                            i++;
                            deleteList.add(Long.valueOf(childId));
                        }
                        dataJson.put("parent", parentData);
                        dataJson.put("children", jsonArray.toString());
                    } else {
                        JSONObject dataJson2 = new JSONObject(getTempData(sportSummary.getTrackId()));
                        try {
                            dataJson2.put("isSingle", true);
                            dataJson = dataJson2;
                        } catch (JSONException e2) {
                            e = e2;
                            dataJson = dataJson2;
                            e.printStackTrace();
                            saveAllUploadWholeData(ZipUtil.gZip(sportUploadJson.toString().getBytes()), sportSummary.getTrackId());
                            for (Long longValue22 : deleteList) {
                                deleteTemp(longValue22.longValue());
                            }
                            return;
                        }
                    }
                    sportUploadJson.put("data", dataJson.toString());
                } catch (JSONException e3) {
                    e = e3;
                    e.printStackTrace();
                    saveAllUploadWholeData(ZipUtil.gZip(sportUploadJson.toString().getBytes()), sportSummary.getTrackId());
                    while (i$.hasNext()) {
                        deleteTemp(longValue22.longValue());
                    }
                    return;
                }
                saveAllUploadWholeData(ZipUtil.gZip(sportUploadJson.toString().getBytes()), sportSummary.getTrackId());
                while (i$.hasNext()) {
                    deleteTemp(longValue22.longValue());
                }
                return;
            }
            LogUtil.m9i(true, TAG, "saveAllSportDataInRun ,err no temp data");
        }
    }

    private String getTempData(long trackId) {
        if (DataManager.getInstance().isUploadSportTempCached(trackId)) {
            return getDetailDataFromCache(this.mContext.getFilesDir().getAbsolutePath() + File.separator + trackId + File.separator + "temp_data");
        }
        return null;
    }

    private void saveTempSportDataInRun() {
        LogUtil.m9i(true, TAG, "####saveTempSportData start####");
        saveDetailInfo2Cache(initSavePath("temp_data"), parseSportData2JsonInRun(this.mTrackId));
        markingThisTempCacheRecord(this.mTrackId);
        LogUtil.m9i(true, TAG, "####saveTempSportData end####");
    }

    private String parseSportData2JsonInRun(long trackId) {
        JSONObject jsonObject = new JSONObject();
        String summaryContent = getSummayData(trackId);
        String detailContent = getAllDetailData(trackId);
        LogUtil.m9i(true, TAG, "json start");
        try {
            jsonObject.put("trackid", trackId / 1000);
            jsonObject.put("detail", detailContent);
            jsonObject.put("summary", summaryContent);
            LogUtil.m9i(true, TAG, "json end");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private String parseMapToData(Map<String, String> params) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "parseMapToData. map : " + params);
        }
        if (params == null) {
            Debug.m6w(TAG, "params is null");
            return null;
        }
        StringBuilder dataBuilder = new StringBuilder();
        Iterator iterator = params.keySet().iterator();
        if (!iterator.hasNext()) {
            return "";
        }
        String key = (String) iterator.next();
        dataBuilder.append(key).append("=").append((String) params.get(key));
        while (iterator.hasNext()) {
            String k = (String) iterator.next();
            dataBuilder.append("&").append(k).append("=").append((String) params.get(k));
        }
        String data = dataBuilder.toString();
        if (!Global.DEBUG_LEVEL_3) {
            return data;
        }
        Debug.m3d(TAG, "parsed data : " + data);
        return data;
    }
}
