package com.huami.watch.newsport.syncservice;

import android.os.Build;
import android.os.SystemProperties;
import android.provider.Settings.Secure;
import android.util.Log;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.transport.httpsupport.transporter.http.URLCompleter.URLSeat;

public class Config {
    private static String DEFAULT_BASE_SERVER = "https://api-watch.huami.com";
    private static String DEFAULT_FIRSTBEAT_BASE_SERVER = "https://api-watch.huami.com";
    private static String DEFAULT_TEST_BASE_SERVER = "https://api-watch-staging.huami.com/";
    private static boolean ENABLE_CUSTOM_SERVER = true;
    public static int SERVER_LOCATION = 1;
    public static final String SOURCE = getDeviceSource();
    private static final String URL_DELETE_SUMMARY = ("/v1/sport/run/del_summary.json?trackid=%d&source=" + SOURCE);
    private static final String URL_GET_CONFIG = ("/v1/sport/config.json?source=" + SOURCE + "&type=" + "run");
    private static final String URL_GET_DETAIL = ("/v1/sport/run/detail.json?trackid=%d&source=" + SOURCE);
    private static final String URL_GET_HISTORY = ("/v1/sport/run/history.json?source=" + SOURCE + "&cv=1&count=" + 7 + "&need_sub_data=0");
    private static final String URL_GET_STATISTIC = ("/v1/sport/run/stat.json?type=all&source=" + SOURCE);
    private static final String URL_GET_SUMMARY = ("/v1/sport/run/summary.json?trackid=%d&source=" + SOURCE);
    private static final String URL_UPLOAD_FIRST_BEAT_CONFIG = ("/watch/users/" + URLSeat.USER_ID + "/WatchSportStatistics/NEWEST_FIRSTBEAT_DATA");
    private static final String URL_UPLOAD_SPORT_THA_INFO_CONFIG = ("/watch/users/" + URLSeat.USER_ID + "/WatchSportStatistics/SPORT_LOAD");
    private static final String URL_UPLOAD_VO2_MAX_CONFIG = ("/watch/users/" + URLSeat.USER_ID + "/WatchSportStatistics/VO2_MAX");
    public static boolean USE_CUSTOM_SERVER = false;

    public static String getBaseServer() {
        boolean z = true;
        int loc = Secure.getInt(SportApplication.getInstance().getContentResolver(), "UserIsOversea", -1);
        LogUtil.m10w(true, "Config", "get loc value : " + loc + ", from user : " + Secure.getString(SportApplication.getInstance().getContentResolver(), "UserMIID"));
        if (loc == 0) {
            DEFAULT_BASE_SERVER = "https://api-watch.huami.com";
        } else if (loc == 1) {
            DEFAULT_BASE_SERVER = "https://api-watch.huami.com";
        } else {
            LogUtil.m10w(true, "Config", "unknown loc value : " + loc);
            DEFAULT_BASE_SERVER = "https://api-watch.huami.com";
        }
        if (!ENABLE_CUSTOM_SERVER) {
            return DEFAULT_BASE_SERVER;
        }
        String baseServer = SystemProperties.get("sport_base_server", DEFAULT_BASE_SERVER);
        if (baseServer.equals(DEFAULT_BASE_SERVER)) {
            z = false;
        }
        USE_CUSTOM_SERVER = z;
        while (!baseServer.isEmpty() && baseServer.endsWith("/")) {
            baseServer = baseServer.substring(0, baseServer.length() - 1);
        }
        return baseServer;
    }

    public static String getFirstBeatBaseServer() {
        boolean z = true;
        int loc = Secure.getInt(SportApplication.getInstance().getContentResolver(), "UserIsOversea", -1);
        LogUtil.m10w(true, "Config", "get loc value : " + loc + ", from user : " + Secure.getString(SportApplication.getInstance().getContentResolver(), "UserMIID"));
        if (loc == 0) {
            DEFAULT_FIRSTBEAT_BASE_SERVER = "https://api-watch.huami.com";
        } else if (loc == 1) {
            DEFAULT_FIRSTBEAT_BASE_SERVER = "https://api-watch-us.huami.com";
        } else {
            LogUtil.m10w(true, "Config", "unknown loc value : " + loc);
            DEFAULT_FIRSTBEAT_BASE_SERVER = "https://api-watch.huami.com";
        }
        if (!ENABLE_CUSTOM_SERVER) {
            return DEFAULT_FIRSTBEAT_BASE_SERVER;
        }
        String baseServer = SystemProperties.get("sport_firstbeat_base_server", DEFAULT_FIRSTBEAT_BASE_SERVER);
        if (baseServer.equals("https://api-watch.huami.com")) {
            z = false;
        }
        USE_CUSTOM_SERVER = z;
        while (!baseServer.isEmpty() && baseServer.endsWith("/")) {
            baseServer = baseServer.substring(0, baseServer.length() - 1);
        }
        return baseServer;
    }

    public static String getUrlGetHistory() {
        return getBaseServer() + URL_GET_HISTORY;
    }

    public static String getUrlGetDetail() {
        return getBaseServer() + URL_GET_DETAIL;
    }

    public static String getUrlGetSummary() {
        return getBaseServer() + URL_GET_SUMMARY;
    }

    public static String getUrlGetStatistic() {
        return getBaseServer() + URL_GET_STATISTIC;
    }

    public static String getUrlUploadSummary() {
        return getBaseServer() + "/v1/sport/run/summary.json";
    }

    public static String getUrlDeleteSummary() {
        return getBaseServer() + URL_DELETE_SUMMARY;
    }

    public static String getUrlUploadDetail() {
        return getBaseServer() + "/v1/sport/run/detail.json";
    }

    public static String getUrlGetConfig() {
        return getBaseServer() + URL_GET_CONFIG;
    }

    public static String getUrlUploadConfig() {
        return getBaseServer() + "/v1/sport/config.json";
    }

    public static String getUrlUploadSportThaInfoConfig() {
        Log.i("Config", "getUrlUploadSportThaInfoConfig:" + getFirstBeatBaseServer() + URL_UPLOAD_SPORT_THA_INFO_CONFIG);
        return getFirstBeatBaseServer() + URL_UPLOAD_SPORT_THA_INFO_CONFIG;
    }

    public static String getUrlUploadVo2MaxConfig() {
        Log.i("Config", "getUrlUploadVo2MaxConfig:" + getFirstBeatBaseServer() + URL_UPLOAD_SPORT_THA_INFO_CONFIG);
        return getFirstBeatBaseServer() + URL_UPLOAD_VO2_MAX_CONFIG;
    }

    public static String getUrlDownloadSportThaInfoConfig() {
        return getFirstBeatBaseServer() + URL_UPLOAD_SPORT_THA_INFO_CONFIG;
    }

    public static String getUrlDownloadVo2maxConfig() {
        return getFirstBeatBaseServer() + URL_UPLOAD_VO2_MAX_CONFIG;
    }

    public static String getURLUploadFirstBeatConfig() {
        return getFirstBeatBaseServer() + URL_UPLOAD_FIRST_BEAT_CONFIG;
    }

    public static String getURLDownloadFirstBeatConfig() {
        return getFirstBeatBaseServer() + URL_UPLOAD_FIRST_BEAT_CONFIG;
    }

    private static String getDeviceSource() {
        LogUtil.m9i(true, "Config", "getDeviceSource");
        if (UnitConvertUtils.isHuangheMode()) {
            return "run.watch.huami.com";
        }
        if ("Amazfit Sports Watch 2S".equals(Build.MODEL) || "Amazfit Stratos +".equals(Build.MODEL)) {
            LogUtil.m9i(true, "Config", "MODEL_EVEREST_PLUS");
            return "run.watch.everests.huami.com";
        }
        LogUtil.m9i(true, "Config", "MODEL_EVEREST");
        return "run.watch.everest.huami.com";
    }
}
