package com.huami.watch.newsport.syncservice;

import android.content.Context;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.utils.LogUtil;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UploadCacheManager {
    private static final String TAG = UploadCacheManager.class.getName();
    private static UploadCacheManager sInstance = null;
    private Context mContext = null;
    private Map<Long, DetailInfoSaver> mDetailSaverMap = new HashMap();

    public static UploadCacheManager getInstance(Context context) {
        synchronized (UploadCacheManager.class) {
            if (sInstance == null) {
                sInstance = new UploadCacheManager(context.getApplicationContext());
            }
        }
        return sInstance;
    }

    private UploadCacheManager(Context context) {
        this.mContext = context;
    }

    public synchronized boolean addSportInfo(long trackId) {
        return addSportInfo(trackId, 15);
    }

    public synchronized boolean addSportInfo(long trackId, int userVerion) {
        boolean z;
        if (this.mDetailSaverMap.get(Long.valueOf(trackId)) != null) {
            z = false;
        } else {
            DetailInfoSaver detailInfoSaver = new DetailInfoSaver(this.mContext, trackId, userVerion);
            detailInfoSaver.initFile();
            this.mDetailSaverMap.put(Long.valueOf(trackId), detailInfoSaver);
            z = true;
        }
        return z;
    }

    public synchronized DetailInfoSaver getSingleSportSaver(long trackId) {
        return (DetailInfoSaver) this.mDetailSaverMap.get(Long.valueOf(trackId));
    }

    public synchronized DetailInfoSaver newSaver(long trackId) {
        return new DetailInfoSaver(this.mContext, trackId);
    }

    public boolean clearCache() {
        File file = new File(this.mContext.getFilesDir().getAbsolutePath());
        if (file == null || !file.exists() || !file.isDirectory()) {
            return false;
        }
        for (File child : file.listFiles()) {
            LogUtil.m9i(true, TAG, "clearCache, name:" + child.getName());
            DataManager.getInstance().clearUploadSp();
            clearCache(child.getName());
        }
        return true;
    }

    public boolean clearCache(long trackId) {
        return clearCache(String.valueOf(trackId));
    }

    public boolean clearCache(String trackId) {
        try {
            Runtime.getRuntime().exec("rm -rf " + this.mContext.getFilesDir().getAbsolutePath() + File.separator + trackId);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
