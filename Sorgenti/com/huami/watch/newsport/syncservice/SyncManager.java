package com.huami.watch.newsport.syncservice;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.huami.watch.common.db.AsyncDatabaseManager;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.StepConfig;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.syncservice.SyncHandler.ISyncResponseListener;
import com.huami.watch.newsport.ui.history.SportHistoryDetailActivity.IDeleteState;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SyncManager implements ISyncResponseListener {
    private static SyncManager sInstance = null;
    private SportApplication mContext;
    private List<IHistoryDownloadListener> mHistoryDownloadListener;
    private List<IHistoryUploadListener> mHistoryUploadListener;
    private AsyncDatabaseManager<SportSummary> mSportSummaryDatabaseManager;
    private SyncDatabaseManager<SportSummary> mSportSummarySyncDatabaseManager;
    private SyncClient mSyncClient;
    private SyncHandler mSyncHandler;

    public interface IHistoryDownloadListener {
        void downloadDetailFinished(long j, boolean z);

        void downloadHistoryFinished(List<SportSummary> list, boolean z);
    }

    class C06511 extends Callback {
        final /* synthetic */ SyncManager this$0;
        final /* synthetic */ long val$trackId;

        protected void doCallback(int resultCode, Object params) {
            SportSummary summary = (SportSummary) params;
            for (IHistoryUploadListener l : new LinkedList(this.this$0.mHistoryUploadListener)) {
                l.startUploadSportData(this.val$trackId);
            }
            this.this$0.startUploadSportData(summary, null);
        }
    }

    class C06554 extends Callback {
        C06554() {
        }

        protected void doCallback(int resultCode, Object params) {
            List<IHistoryUploadListener> tmp = new LinkedList(SyncManager.this.mHistoryUploadListener);
            List<? extends SportSummary> unsyncSummaries = SyncManager.this.sortSportSummarys((List) params);
            if (unsyncSummaries == null) {
                Debug.m6w("SyncManager", "unsync summaries is null");
                for (IHistoryUploadListener l : tmp) {
                    l.uploadCancelled();
                }
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
            } else if (unsyncSummaries.isEmpty()) {
                Debug.m6w("SyncManager", "there is not any unsync summary");
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
                for (IHistoryUploadListener l2 : tmp) {
                    l2.uploadCancelled();
                }
            } else {
                final int unsyncSize = unsyncSummaries.size();
                for (int i = 0; i < unsyncSummaries.size(); i++) {
                    final int finalI = i;
                    SyncManager.this.startUploadSportData((SportSummary) unsyncSummaries.get(i), new Callback() {
                        protected void doCallback(int code, Object o) {
                            if (finalI == unsyncSize - 1) {
                                SyncManager.this.notifyPhoneSyncData();
                            }
                        }
                    });
                }
            }
        }
    }

    class C06575 extends Callback {
        C06575() {
        }

        protected void doCallback(int resultCode, Object params) {
            List<? extends SportSummary> unsyncSummaries = SyncManager.this.sortSportSummarys((List) params);
            List<IHistoryUploadListener> tmp = new LinkedList(SyncManager.this.mHistoryUploadListener);
            if (unsyncSummaries == null) {
                Debug.m6w("SyncManager", "unsync summaries is null");
                for (IHistoryUploadListener l : tmp) {
                    l.uploadCancelled();
                }
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
            } else if (unsyncSummaries.isEmpty()) {
                Debug.m6w("SyncManager", "there is not any unsync summary");
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
                for (IHistoryUploadListener l2 : tmp) {
                    l2.uploadCancelled();
                }
            } else {
                final int unsyncSize = unsyncSummaries.size();
                for (int i = 0; i < unsyncSummaries.size(); i++) {
                    final int finalI = i;
                    SyncManager.this.startUploadSportData((SportSummary) unsyncSummaries.get(i), new Callback() {
                        protected void doCallback(int code, Object o) {
                            if (finalI == unsyncSize - 1) {
                                SyncManager.this.notifyPhoneSyncData();
                            }
                        }
                    });
                }
            }
        }
    }

    class C06596 extends Callback {
        C06596() {
        }

        protected void doCallback(int resultCode, Object params) {
            List<? extends SportSummary> unsyncSummaries = SyncManager.this.sortSportSummarys((List) params);
            List<IHistoryUploadListener> tmp = new LinkedList(SyncManager.this.mHistoryUploadListener);
            if (unsyncSummaries == null) {
                Debug.m6w("SyncManager", "unsync summaries is null");
                for (IHistoryUploadListener l : tmp) {
                    l.uploadCancelled();
                }
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
            } else if (unsyncSummaries.isEmpty()) {
                Debug.m6w("SyncManager", "there is not any unsync summary");
                SyncManager.this.mSyncHandler.tellUploadResultForme(true);
                for (IHistoryUploadListener l2 : tmp) {
                    l2.uploadCancelled();
                }
            } else {
                final int unsyncSize = unsyncSummaries.size();
                for (int i = 0; i < unsyncSummaries.size(); i++) {
                    final int finalI = i;
                    SyncManager.this.startUploadSportData((SportSummary) unsyncSummaries.get(i), new Callback() {
                        protected void doCallback(int code, Object o) {
                            if (finalI == unsyncSize - 1) {
                                SyncManager.this.notifyPhoneSyncData();
                            }
                        }
                    });
                }
            }
        }
    }

    public interface IHistoryUploadListener {
        void startUploadSportData(long j);

        void uploadCancelled();

        void uploadDetailFinished(long j, boolean z);

        void uploadSummaryFinished(long j, boolean z);
    }

    private SyncManager() {
        this.mContext = SportApplication.getInstance();
        this.mHistoryDownloadListener = new LinkedList();
        this.mHistoryUploadListener = new LinkedList();
        this.mSportSummaryDatabaseManager = null;
        this.mSportSummarySyncDatabaseManager = null;
        this.mSyncHandler = null;
        this.mSyncClient = null;
        this.mSportSummaryDatabaseManager = new AsyncDatabaseManager(SportSummaryDao.getInstance(this.mContext), Global.getGlobalHeartHandler());
        this.mSportSummarySyncDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(this.mContext));
        this.mSyncHandler = new SyncHandler(Global.getGlobalHeartHandler());
        this.mSyncHandler.setSyncResponseListener(this);
        this.mSyncClient = SyncClient.getInstance(Global.getApplicationContext());
    }

    public void initNewUploadClient() {
        if (this.mSyncClient == null) {
            this.mSyncClient = SyncClient.getInstance(Global.getApplicationContext());
        }
    }

    public void registerHistoryUploadListener(IHistoryUploadListener historyUploadListener) {
        if (historyUploadListener != null) {
            this.mHistoryUploadListener.add(historyUploadListener);
        }
    }

    public void unregisterHistoryUploadListener(IHistoryUploadListener historyUploadListener) {
        if (historyUploadListener != null) {
            this.mHistoryUploadListener.remove(historyUploadListener);
        }
    }

    public void registerHistoryDownloadListener(IHistoryDownloadListener historyDownloadListener) {
        if (historyDownloadListener != null) {
            this.mHistoryDownloadListener.add(historyDownloadListener);
        }
    }

    public void unregisterHistoryDownloadListener(IHistoryDownloadListener historyDownloadListener) {
        if (historyDownloadListener != null) {
            this.mHistoryDownloadListener.remove(historyDownloadListener);
        }
    }

    public static synchronized SyncManager getInstance() {
        SyncManager syncManager;
        synchronized (SyncManager.class) {
            if (sInstance == null) {
                sInstance = new SyncManager();
            }
            syncManager = sInstance;
        }
        return syncManager;
    }

    public void startDownloadHistory() {
        Debug.m3d("SyncManager", "start download history");
        this.mSyncHandler.obtainMessage(1).sendToTarget();
        this.mSyncHandler.obtainMessage(4).sendToTarget();
        this.mSyncHandler.obtainMessage(5).sendToTarget();
        if (!UnitConvertUtils.isHuangheMode()) {
            this.mSyncHandler.obtainMessage(115).sendToTarget();
            this.mSyncHandler.obtainMessage(114).sendToTarget();
            this.mSyncHandler.obtainMessage(118).sendToTarget();
        }
    }

    public void startDownloadSummary(long trackId) {
        Debug.m3d("SyncManager", "start download child summary " + trackId);
        Message msg = this.mSyncHandler.obtainMessage(2);
        Bundle data = new Bundle();
        data.putLong("track_id", trackId);
        msg.setData(data);
        msg.sendToTarget();
    }

    public void startDownloadDetail(long trackId) {
        Debug.m3d("SyncManager", "start download detail " + trackId);
        Message msg = this.mSyncHandler.obtainMessage(3);
        Bundle data = new Bundle();
        data.putLong("track_id", trackId);
        msg.setData(data);
        msg.sendToTarget();
    }

    public void startUploadSportSummary(long trackId) {
        Debug.m3d("SyncManager", "start upload summary " + trackId);
        Message msg = this.mSyncHandler.obtainMessage(6);
        Bundle data = new Bundle();
        data.putLong("track_id", trackId);
        msg.setData(data);
        msg.sendToTarget();
    }

    public void startDeleteSportData(final SportSummary sportSummary, final IDeleteState listener) {
        Debug.m3d("SyncManager", "delete sport data " + sportSummary);
        if (sportSummary == null) {
            Debug.m6w("SyncManager", "sport summary is null while delete summary");
            return;
        }
        sportSummary.addSyncCount();
        final boolean isDelete = sportSummary.getCurrentStatus() == 7;
        sportSummary.setCurrentStatus(7);
        this.mSportSummaryDatabaseManager.update(this.mContext, sportSummary, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                List<IHistoryUploadListener> tmp = new LinkedList(SyncManager.this.mHistoryUploadListener);
                if (!isDelete) {
                    DataManager.getInstance().deleteSummaryFromStatistic(SyncManager.this.mContext, sportSummary);
                }
                Message msg = SyncManager.this.mSyncHandler.obtainMessage(9);
                Bundle data = new Bundle();
                data.putLong("track_id", sportSummary.getTrackId());
                msg.setData(data);
                msg.sendToTarget();
                if (listener != null) {
                    listener.onSwitchDeleteStateSuccess();
                }
            }
        });
    }

    public void startUploadSportData(final SportSummary sportSummary, final Callback callback) {
        Debug.m5i("SyncManager", "upload detail_id:" + sportSummary.getTrackId() + ", detail_type:" + sportSummary.getSportType() + ", detail_parent:" + sportSummary.getParentTrackId());
        Debug.m3d("SyncManager", "upload sport data " + sportSummary);
        if (sportSummary == null) {
            Debug.m6w("SyncManager", "sport summary is null while upload summary");
            return;
        }
        sportSummary.addSyncCount();
        sportSummary.setCurrentStatus(3);
        this.mSportSummaryDatabaseManager.update(this.mContext, sportSummary, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                for (IHistoryUploadListener l : new LinkedList(SyncManager.this.mHistoryUploadListener)) {
                    l.startUploadSportData(sportSummary.getTrackId());
                }
                Message msg = SyncManager.this.mSyncHandler.obtainMessage(7);
                Bundle data = new Bundle();
                data.putLong("track_id", sportSummary.getTrackId());
                msg.setData(data);
                msg.sendToTarget();
                if (callback != null) {
                    callback.notifyCallback(0, null);
                }
            }
        });
    }

    public void startUploadAllUnSyncSportDatas() {
        Debug.m3d("SyncManager", "upload alll unsync sport data");
        this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=2 OR current_status=3 OR current_status=8 OR current_status=5", null, null, null, new C06554());
        startUploadStepConfig();
        startUploadAllSportThaInfo(1);
    }

    private void notifyPhoneSyncData() {
        LogUtil.m9i(true, "SyncManager", "notifyPhoneSyncData");
        this.mSyncHandler.obtainMessage(121).sendToTarget();
    }

    public void startUploadAllSportThaInfo(int type) {
        Debug.m3d("SyncManager", "start upload step_config");
        Message msg = this.mSyncHandler.obtainMessage(120);
        msg.arg1 = type;
        msg.sendToTarget();
    }

    public void startUploadStepConfig() {
        Debug.m3d("SyncManager", "start upload step_config");
        this.mSyncHandler.obtainMessage(8).sendToTarget();
    }

    public void startUploadNotModifyAndSyncSportDatas() {
        Debug.m3d("SyncManager", "upload not sync and not modify of indoor run sport data");
        this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=? OR current_status=?", new String[]{"2", "8"}, null, null, new C06575());
        startUploadAllSportThaInfo(0);
    }

    public void startUploadNotSyncSportDatas() {
        Debug.m3d("SyncManager", "upload not sync sport data");
        this.mSportSummaryDatabaseManager.selectAll(this.mContext, "current_status=?", new String[]{"2"}, null, null, new C06596());
        startUploadAllSportThaInfo(0);
    }

    public void downloadSportHistoryFinished(List<SportSummary> summaries, List<Long> childSummaryIds, boolean success) {
        Debug.m3d("SyncManager", "downloadSportHistoryFinished. summaries : " + summaries + ", success : " + success);
        for (IHistoryDownloadListener l : new LinkedList(this.mHistoryDownloadListener)) {
            l.downloadHistoryFinished(summaries, success);
        }
        if (summaries != null) {
            int recoveryCount = summaries.size();
            if (childSummaryIds != null) {
                recoveryCount += childSummaryIds.size();
                for (Long longValue : childSummaryIds) {
                    startDownloadSummary(longValue.longValue());
                }
            }
            DataManager.getInstance().recordDownloadSportRecord(recoveryCount);
            for (SportSummary summary : summaries) {
                startDownloadDetail(summary.getTrackId());
            }
        }
    }

    public void downloadSportSummaryFinished(SportSummary summary, boolean success) {
        if (summary != null) {
            if (success) {
                long trackId = summary.getTrackId();
                boolean isSuccess = checkWhetherParentSummaryDownloadComplete(summary);
                startDownloadDetail(trackId);
                Debug.m5i("SyncManager", "update parent summary:" + isSuccess);
                return;
            }
            Debug.m5i("SyncManager", "downloadSportSummaryFinished failed");
        }
    }

    private void tellSportRecordFinishedDownload() {
        Debug.m3d("SyncManager", "tellSportRecordFinishedDownload");
        this.mContext.sendBroadcast(new Intent("com.huami.watch.sport.DOWNLOAD_SPORT_RECORD_COMPLETE"));
    }

    public void downloadSportDetailFinished(long trackId, boolean success) {
        DataManager.getInstance().recordCompleteDownloadHistory();
        int totalRecord = DataManager.getInstance().getDownloadSportRecord();
        int completeNum = DataManager.getInstance().getDownloadNums();
        Debug.m3d("SyncManager", "downloadSportDetailFinished, download count:" + totalRecord + ", complete num:" + completeNum);
        if (totalRecord != 0 && completeNum == totalRecord) {
            tellSportRecordFinishedDownload();
        }
        for (IHistoryDownloadListener l : new LinkedList(this.mHistoryDownloadListener)) {
            l.downloadDetailFinished(trackId, success);
        }
    }

    public void downloadSportStatisticFinished(SportStatistic statistic, boolean success) {
        Debug.m3d("SyncManager", "downloadSportStatisticFinished " + statistic);
    }

    public void downloadSportStepConfigFinished(StepConfig config, boolean success) {
        Debug.m3d("SyncManager", "downloadSportStepConfigFinished " + config.toString() + ", isSuccess:" + success);
    }

    public void uploadSportStepConfigFinished(boolean success) {
        Debug.m3d("SyncManager", "uploadSportStepConfigFinished, isSuccess:" + success);
    }

    public void uploadSportSummaryFinished(final long trackId, final boolean success) {
        Debug.m3d("SyncManager", "uploadSportSummaryFinished. track id : " + trackId + ", success : " + success);
        for (IHistoryUploadListener l : new LinkedList(this.mHistoryUploadListener)) {
            l.uploadSummaryFinished(trackId, success);
        }
        this.mSportSummaryDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId}, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                SportSummary sportSummary = (SportSummary) params;
                if (sportSummary == null) {
                    Debug.m6w("SyncManager", "sport summary is null whose track id is " + trackId);
                } else if (success) {
                    sportSummary.setCurrentStatus(6);
                    SyncManager.this.mSportSummarySyncDatabaseManager.update(SyncManager.this.mContext, sportSummary);
                } else {
                    SyncManager.this.checkReuploadSportData(sportSummary);
                }
            }
        });
    }

    private void checkReuploadSportData(SportSummary sportSummary) {
        Debug.m3d("SyncManager", "check reupload sport data " + sportSummary);
        if (sportSummary == null) {
            Debug.m6w("SyncManager", "sport summary is null while check reupload sport data");
            return;
        }
        Debug.m3d("SyncManager", "retry count is " + sportSummary.getSyncCount());
        if (sportSummary.getSyncCount() <= 1) {
            startUploadSportData(sportSummary, null);
            return;
        }
        sportSummary.setCurrentStatus(2);
        this.mSportSummaryDatabaseManager.update(this.mContext, sportSummary, null);
    }

    public void uploadSportDetailFinished(final long trackId, boolean success) {
        Debug.m3d("SyncManager", "uploadSportDetailFinished. trackId:" + trackId + ", success:" + success);
        for (IHistoryUploadListener l : new LinkedList(this.mHistoryUploadListener)) {
            l.uploadDetailFinished(trackId, success);
        }
        if (success) {
            startUploadSportSummary(trackId);
            return;
        }
        Debug.m6w("SyncManager", "upload sport detail failed");
        this.mSportSummaryDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId}, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                SportSummary sportSummary = (SportSummary) params;
                if (sportSummary == null) {
                    Debug.m6w("SyncManager", "sport summary is null whose track id is " + trackId);
                } else {
                    SyncManager.this.checkReuploadSportData(sportSummary);
                }
            }
        });
    }

    public void deleteSportSummaryFinished(final long trackId, boolean success) {
        Debug.m3d("SyncManager", "deleteSportSummaryFinished. trackId:" + trackId + ", success:" + success);
        if (!success) {
            Debug.m6w("SyncManager", "delete sport summary failed");
            this.mSportSummaryDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId}, new Callback() {
                protected void doCallback(int resultCode, Object params) {
                    SportSummary sportSummary = (SportSummary) params;
                    if (sportSummary == null) {
                        Debug.m6w("SyncManager", "sport summary is null whose track id is " + trackId);
                    } else {
                        SyncManager.this.checkReDeleteSportData(sportSummary);
                    }
                }
            });
        }
    }

    private void checkReDeleteSportData(SportSummary sportSummary) {
        Debug.m3d("SyncManager", "check redelete sport data " + sportSummary);
        if (sportSummary == null) {
            Debug.m6w("SyncManager", "sport summary is null while check redelete sport data");
            return;
        }
        Debug.m3d("SyncManager", "retry count is " + sportSummary.getSyncCount());
        startDeleteSportData(sportSummary, null);
    }

    private boolean checkWhetherParentSummaryDownloadComplete(SportSummary sportSummary) {
        if (sportSummary == null) {
            Debug.m5i("SyncManager", "checkWhetherParentSummaryDownloadComplete, summary is null");
            return false;
        }
        SportSummary parentSummary = (SportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + sportSummary.getParentTrackId()});
        if (parentSummary == null) {
            return false;
        }
        List<? extends SportSummary> childSummaryList = this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, "parent_trackid=? AND current_status!=?", new String[]{"" + parentTrackId, "6"}, null, null);
        if (childSummaryList == null || childSummaryList.size() <= 0) {
            return false;
        }
        if (childSummaryList.size() != parentSummary.getChildSportSummrys().size()) {
            return false;
        }
        if (parentSummary.getCurrentStatus() == 9) {
            parentSummary.setCurrentStatus(4);
        } else {
            parentSummary.setCurrentStatus(6);
        }
        this.mSportSummarySyncDatabaseManager.update(this.mContext, parentSummary);
        Debug.m5i("SyncManager", "checkWhetherParentSummaryDownloadComplete, trackId:" + parentTrackId);
        return true;
    }

    private List<? extends SportSummary> sortSportSummarys(List<? extends SportSummary> sportSummaryList) {
        if (sportSummaryList == null) {
            return null;
        }
        List<SportSummary> mixSummarys = new ArrayList();
        List<? extends SportSummary> availAbleSummarys = new ArrayList();
        for (int i = 0; i < sportSummaryList.size(); i++) {
            SportSummary sportSummary = (SportSummary) sportSummaryList.get(i);
            if (SportType.isMixedSport(sportSummary.getSportType())) {
                mixSummarys.add(sportSummary);
            } else {
                availAbleSummarys.add(sportSummary);
            }
        }
        availAbleSummarys.addAll(mixSummarys);
        return availAbleSummarys;
    }

    public void changeNotModifyData2NotSync() {
        List<? extends SportSummary> sportSummaryList = this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, "current_status=? OR current_status=?", new String[]{String.valueOf(8), String.valueOf(3)}, null, null);
        if (sportSummaryList != null && sportSummaryList.size() > 0) {
            for (SportSummary sportSummary : sportSummaryList) {
                sportSummary.setCurrentStatus(2);
                this.mSportSummarySyncDatabaseManager.update(this.mContext, sportSummary);
            }
        }
    }

    public void triggerUploadSportData() {
        if (this.mSyncClient != null) {
            this.mSyncClient.trigger();
        }
    }
}
