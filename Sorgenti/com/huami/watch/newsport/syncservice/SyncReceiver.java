package com.huami.watch.newsport.syncservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.common.log.Debug;

public class SyncReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Debug.m3d("SyncReceiver", "receive sync broadcast " + intent);
        if ("com.huami.watch.broadcast.SYNC_DATA".equals(intent.getAction())) {
            GetHistoryService.startActionGetHistory(context);
        } else if ("com.huami.watch.companion.action.DataSyncRequestApps".equals(intent.getAction())) {
            int syncFlow = intent.getIntExtra("DataSyncFlow", -1);
            Debug.m3d("SyncReceiver", "sync flow " + syncFlow);
            if (syncFlow == 1) {
                GetHistoryService.startActionGetHistory(context);
            } else if (syncFlow == 2) {
                UploadUnsyncDataService.startActionUploadUnsyncDataWithFinishBroadcast(context);
            }
        }
    }
}
