package com.huami.watch.newsport.syncservice;

import java.util.BitSet;
import java.util.HashMap;

public class Geohash {
    static final char[] digits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    static final HashMap<Character, Integer> lookup = new HashMap();
    private static int numbits = 30;

    static {
        char[] arr$ = digits;
        int len$ = arr$.length;
        int i$ = 0;
        int i = 0;
        while (i$ < len$) {
            int i2 = i + 1;
            lookup.put(Character.valueOf(arr$[i$]), Integer.valueOf(i));
            i$++;
            i = i2;
        }
    }

    public static String base32(long i) {
        int charPos;
        int charPos2;
        char[] buf = new char[65];
        boolean negative = i < 0;
        if (negative) {
            charPos = 64;
        } else {
            i = -i;
            charPos = 64;
        }
        while (i <= -32) {
            charPos2 = charPos - 1;
            buf[charPos] = digits[(int) (-(i % 32))];
            i /= 32;
            charPos = charPos2;
        }
        buf[charPos] = digits[(int) (-i)];
        if (negative) {
            charPos2 = charPos - 1;
            buf[charPos2] = '-';
        } else {
            charPos2 = charPos;
        }
        return new String(buf, charPos2, 65 - charPos2);
    }

    public static double[] decode(String geohash) {
        StringBuilder buffer = new StringBuilder();
        for (char c : geohash.toCharArray()) {
            buffer.append(Integer.toString(((Integer) lookup.get(Character.valueOf(c))).intValue() + 32, 2).substring(1));
        }
        BitSet lonset = new BitSet();
        BitSet latset = new BitSet();
        int j = 0;
        int i = 0;
        while (i < numbits * 2) {
            boolean isSet = false;
            if (i < buffer.length()) {
                isSet = buffer.charAt(i) == '1';
            }
            int j2 = j + 1;
            lonset.set(j, isSet);
            i += 2;
            j = j2;
        }
        j = 0;
        i = 1;
        while (i < numbits * 2) {
            isSet = false;
            if (i < buffer.length()) {
                isSet = buffer.charAt(i) == '1';
            }
            j2 = j + 1;
            latset.set(j, isSet);
            i += 2;
            j = j2;
        }
        double lon = decode(lonset, -180.0d, 180.0d);
        double lat = decode(latset, -90.0d, 90.0d);
        return new double[]{lat, lon};
    }

    public static String encode(double lat, double lon) {
        BitSet latbits = getBits(lat, -90.0d, 90.0d);
        BitSet lonbits = getBits(lon, -180.0d, 180.0d);
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < numbits; i++) {
            char c;
            if (lonbits.get(i)) {
                c = '1';
            } else {
                c = '0';
            }
            buffer.append(c);
            if (latbits.get(i)) {
                c = '1';
            } else {
                c = '0';
            }
            buffer.append(c);
        }
        return base32(Long.parseLong(buffer.toString(), 2));
    }

    private static double decode(BitSet bs, double floor, double ceiling) {
        double mid = 0.0d;
        for (int i = 0; i < bs.length(); i++) {
            mid = (floor + ceiling) / 2.0d;
            if (bs.get(i)) {
                floor = mid;
            } else {
                ceiling = mid;
            }
        }
        return mid;
    }

    private static BitSet getBits(double lat, double floor, double ceiling) {
        BitSet buffer = new BitSet(numbits);
        for (int i = 0; i < numbits; i++) {
            double mid = (floor + ceiling) / 2.0d;
            if (lat >= mid) {
                buffer.set(i);
                floor = mid;
            } else {
                ceiling = mid;
            }
        }
        return buffer;
    }
}
