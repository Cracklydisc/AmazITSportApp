package com.huami.watch.newsport.syncservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LongSparseArray;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.syncservice.SyncManager.IHistoryDownloadListener;
import java.util.List;

public class GetHistoryService extends Service implements IHistoryDownloadListener {
    private SyncManager mSyncManager = SyncManager.getInstance();
    private LongSparseArray<Boolean> mTrackIdArray = new LongSparseArray();

    class C06431 implements Runnable {
        C06431() {
        }

        public void run() {
            Debug.m5i("GetHistoryService", "GetHistoryService is first download:" + DataManager.getInstance().isFirstDownloadHistory(Global.getApplicationContext()));
            if (DataManager.getInstance().isFirstDownloadHistory(Global.getApplicationContext())) {
                GetHistoryService.this.startDownloadHistory();
                DataManager.getInstance().setIsFirstDownloadHistory(Global.getApplicationContext(), false);
            }
        }
    }

    public static void startActionGetHistory(Context context) {
        Intent intent = new Intent(context, GetHistoryService.class);
        intent.setAction("com.huami.watch.sport.syncservice.action.get_history");
        context.startService(intent);
    }

    public synchronized void downloadHistoryFinished(List<SportSummary> summaries, boolean success) {
        Debug.m3d("GetHistoryService", "downloadHistoryFinished : " + summaries + ", success : " + success);
        if (!success) {
            stopDownloadHistory(false);
        } else if (summaries == null || summaries.isEmpty()) {
            stopDownloadHistory(true);
        } else {
            for (SportSummary summary : summaries) {
                this.mTrackIdArray.put(summary.getTrackId(), Boolean.TRUE);
            }
        }
    }

    public synchronized void downloadDetailFinished(long trackId, boolean success) {
        Debug.m3d("GetHistoryService", "downloadDetailFinished : " + trackId + ", success : " + success);
        this.mTrackIdArray.delete(trackId);
        if (this.mTrackIdArray.size() == 0) {
            stopDownloadHistory(true);
        }
    }

    private void startDownloadHistory() {
        SportTransHelper.setDataRestored(false);
        Log.i("LOG_TRANS_*", "SportTransHelper.setDataRestored(false)");
        Debug.m3d("GetHistoryService", "start download history");
        this.mSyncManager.registerHistoryDownloadListener(this);
        this.mSyncManager.startDownloadHistory();
    }

    private void stopDownloadHistory(boolean success) {
        Debug.m3d("GetHistoryService", "stop download history " + success);
        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("DataSyncAppPkgName", getPackageName());
        if (success) {
            broadcastIntent.setAction("com.huami.watch.companion.action.DataSyncAppOK");
        } else {
            broadcastIntent.setAction("com.huami.watch.companion.action.DataSyncAppFail");
        }
        sendBroadcast(broadcastIntent);
        this.mSyncManager.unregisterHistoryDownloadListener(this);
        this.mTrackIdArray.clear();
        stopSelf();
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Global.getGlobalWorkHandler().post(new C06431());
        return 2;
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }
}
