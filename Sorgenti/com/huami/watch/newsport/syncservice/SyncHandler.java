package com.huami.watch.newsport.syncservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.LongSparseArray;
import com.huami.watch.common.db.AsyncDatabaseManager;
import com.huami.watch.common.db.Callback;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.PauseInfo;
import com.huami.watch.newsport.common.model.RequestInfo;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportStatistic;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.StepConfig;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.common.model.config.FirstBeatConfig;
import com.huami.watch.newsport.customtrain.utils.IntervalRunDataReceiver;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.RequestInfoDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.db.dao.Vo2MaxDayInfoDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.syncservice.parser.SportDetailParser;
import com.huami.watch.newsport.syncservice.parser.SportSummaryParser;
import com.huami.watch.newsport.syncservice.parser.StatisticParser;
import com.huami.watch.newsport.syncservice.parser.StepConfigParser;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.IntertelRunDataParse;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import com.huami.watch.sensor.HmSensorHubConfigManager.OnKlvpDataListener;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import com.huami.watch.transport.DataBundle;
import com.huami.watch.transport.TransportDataItem;
import com.huami.watch.transport.Transporter;
import com.huami.watch.transport.httpsupport.client.TransportManager;
import com.huami.watch.transport.httpsupport.client.TransportManager.OnDataArrivalListener;
import com.huami.watch.transport.httpsupport.model.DataItem;
import com.huami.watch.transport.httpsupport.model.DataUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SyncHandler extends Handler implements OnDataArrivalListener {
    private static final String SOURCE = Config.SOURCE;
    public static TransportManager mTransportManager = null;
    private SportApplication mContext;
    private DataManager mDataManager;
    private Runnable mDelayTriggerFromClient;
    private SyncDatabaseManager<HeartRate> mHeartRateSyncDatabaseManager;
    private SyncDatabaseManager<SportLocationData> mLocationDataSyncDatabaseManager;
    private AsyncDatabaseManager<RequestInfo> mRequestInfoDatabaseManager;
    private SyncDatabaseManager<DailyPerformanceInfo> mRunningDailyformanceInfoManager;
    private SyncDatabaseManager<CyclingDetail> mRunningInfoCadenceSyncDatabaseManager;
    private SyncDatabaseManager<RunningInfoPerKM> mRunningInfoPerKMSyncDatabaseManager;
    private SyncDatabaseManager<RunningInfoPerLap> mRunningInfoPerLapSyncDatabaseManager;
    private SportDetailParser mSportDetailParser;
    private SportSummaryParser mSportSummaryParser;
    private SyncDatabaseManager<SportSummary> mSportSummarySyncDatabaseManager;
    private SyncDatabaseManager<SportThaInfo> mSportThaInfoDataBaseManager;
    private StatisticParser mStatisticParser;
    private StepConfigParser mStepConfigParser;
    private ISyncResponseListener mSyncResponseListener;
    private SyncDatabaseManager<Vo2maxDayInfo> mVo2maxDataBaseManager;

    class C06461 implements Runnable {
        C06461() {
        }

        public void run() {
            try {
                SyncHandler.mTransportManager.triggerUploadManual("*");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C06494 implements OnKlvpDataListener {
        C06494() {
        }

        public void onHealthDataReady(int i, float v) {
        }

        public void onSportDataReady(SportStatistics sportData) {
            if (sportData != null && sportData.mSportEteInfo != null) {
                StringBuffer sb = new StringBuffer();
                sb.append("getVo2MaxTrend:" + sportData.mSportThaInfo.getVo2MaxTrend() + ",");
                sb.append("getTrainingLoadTrend:" + sportData.mSportThaInfo.getTrainingLoadTrend() + ",");
                sb.append("getFitnessClass:" + sportData.mSportThaInfo.getFitnessClass() + ",");
                sb.append("getFitnessLevelIncrease:" + sportData.mSportThaInfo.getFitnessLevelIncrease() + ",");
                sb.append("getWtlStatus:" + sportData.mSportThaInfo.getWtlStatus() + ",");
                sb.append("getWtlSum:" + sportData.mSportThaInfo.getWtlSum() + ",");
                sb.append("getWtlSumOptimalMax:" + sportData.mSportThaInfo.getWtlSumOptimalMax() + ",");
                sb.append("getWtlSumOptimalMin:" + sportData.mSportThaInfo.getWtlSumOptimalMin() + ",");
                sb.append("getWtlSumOverreaching:" + sportData.mSportThaInfo.getWtlSumOverreaching());
                Log.i("SyncHandler", "getThisDaySportThaInfoData mSportThaInfo:" + sb.toString());
                SportThaInfo sportThaInfo = new SportThaInfo();
                sportThaInfo.setCurrnetDayTrainLoad(sportData.mSportEteInfo.getETEtrainingLoadPeak());
                sportThaInfo.setVo2MaxTread(sportData.mSportThaInfo.getVo2MaxTrend());
                sportThaInfo.setTrainingLoadTrend(sportData.mSportThaInfo.getTrainingLoadTrend());
                sportThaInfo.setFitnessClass(sportData.mSportThaInfo.getFitnessClass());
                sportThaInfo.setFitnessLevelIncrease(sportData.mSportThaInfo.getFitnessLevelIncrease());
                sportThaInfo.setWtlStatus(sportData.mSportThaInfo.getWtlStatus());
                sportThaInfo.setWtlSum(sportData.mSportThaInfo.getWtlSum());
                sportThaInfo.setWtlSumOptimalMax(sportData.mSportThaInfo.getWtlSumOptimalMax());
                sportThaInfo.setWtlSumOptimalMin(sportData.mSportThaInfo.getWtlSumOptimalMin());
                sportThaInfo.setWtlSumOverreaching(sportData.mSportThaInfo.getWtlSumOverreaching());
                sportThaInfo.setDayId(FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis()));
                sportThaInfo.setUpdateTime(System.currentTimeMillis());
                sportThaInfo.setDateStr(FirstBeatDataUtils.getDayTimeFormat(System.currentTimeMillis()));
                Log.i("SyncHandler", "sportThaInfo dayEnd:" + sportThaInfo.toString());
                Log.i("SyncHandler", "insertOrUpdateThaInfo : " + new SyncDatabaseManager(SportThaInfoDao.getmInstance(Global.getApplicationContext())).add(Global.getApplicationContext(), sportThaInfo));
            } else if (sportData == null) {
                Log.i("SyncHandler", " updateTrainLoadData sportData is null ");
            } else if (sportData.mSportEteInfo == null) {
                Log.i("SyncHandler", " updateTrainLoadData sportData.mSportEteInfo is null ");
            }
        }
    }

    public interface ISyncResponseListener {
        void deleteSportSummaryFinished(long j, boolean z);

        void downloadSportDetailFinished(long j, boolean z);

        void downloadSportHistoryFinished(List<SportSummary> list, List<Long> list2, boolean z);

        void downloadSportStatisticFinished(SportStatistic sportStatistic, boolean z);

        void downloadSportStepConfigFinished(StepConfig stepConfig, boolean z);

        void downloadSportSummaryFinished(SportSummary sportSummary, boolean z);

        void uploadSportDetailFinished(long j, boolean z);

        void uploadSportStepConfigFinished(boolean z);

        void uploadSportSummaryFinished(long j, boolean z);
    }

    public SyncHandler(Handler handler) {
        super(handler.getLooper());
        this.mRequestInfoDatabaseManager = null;
        this.mSportSummarySyncDatabaseManager = null;
        this.mLocationDataSyncDatabaseManager = null;
        this.mRunningInfoPerKMSyncDatabaseManager = null;
        this.mRunningInfoPerLapSyncDatabaseManager = null;
        this.mRunningInfoCadenceSyncDatabaseManager = null;
        this.mRunningDailyformanceInfoManager = null;
        this.mHeartRateSyncDatabaseManager = null;
        this.mSportThaInfoDataBaseManager = null;
        this.mVo2maxDataBaseManager = null;
        this.mContext = SportApplication.getInstance();
        this.mSportSummaryParser = new SportSummaryParser();
        this.mSportDetailParser = new SportDetailParser();
        this.mStatisticParser = new StatisticParser();
        this.mStepConfigParser = new StepConfigParser();
        this.mSyncResponseListener = null;
        this.mDataManager = null;
        this.mDelayTriggerFromClient = new C06461();
        this.mRequestInfoDatabaseManager = new AsyncDatabaseManager(RequestInfoDao.getInstance(this.mContext), Global.getGlobalHeartHandler());
        this.mSportSummarySyncDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(this.mContext));
        this.mLocationDataSyncDatabaseManager = new SyncDatabaseManager(LocationDataDao.getInstance(this.mContext));
        this.mRunningInfoPerKMSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerKMDao.getInstance(this.mContext));
        this.mHeartRateSyncDatabaseManager = new SyncDatabaseManager(HeartRateDao.getInstance(this.mContext));
        this.mRunningInfoPerLapSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(this.mContext));
        this.mRunningInfoCadenceSyncDatabaseManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(this.mContext));
        this.mRunningDailyformanceInfoManager = new SyncDatabaseManager(DailyPerpormanceInfoDao.getInstance(this.mContext));
        this.mSportThaInfoDataBaseManager = new SyncDatabaseManager(SportThaInfoDao.getmInstance(this.mContext));
        this.mVo2maxDataBaseManager = new SyncDatabaseManager(Vo2MaxDayInfoDao.getInstance(this.mContext));
        this.mDataManager = DataManager.getInstance();
        mTransportManager = new TransportManager(this.mContext) {
            protected boolean doRemainsDataTrans() {
                List<SportSummary> summaries = SyncHandler.this.mSportSummarySyncDatabaseManager.selectAll(SyncHandler.this.mContext, "current_status=2 OR current_status=3 OR current_status=8 OR current_status=5", null, null, null);
                if (summaries == null || summaries.isEmpty()) {
                    Debug.m5i("SyncHandler", "there is no not sync data");
                    if (!UnitConvertUtils.isUseNewUploadMethod(SyncHandler.this.mContext)) {
                        SyncHandler.this.sendSportThaInfoSimpleRequest(0);
                    }
                    return false;
                }
                if (!UnitConvertUtils.isUseNewUploadMethod(SyncHandler.this.mContext)) {
                    SyncManager.getInstance().startUploadNotSyncSportDatas();
                }
                return true;
            }

            public void onQueryCommand(DataItem commandItem) {
                super.onQueryCommand(commandItem);
                Debug.m5i("SyncHandler", "onQueryCommand come in");
                if (commandItem == null) {
                    Debug.m5i("SyncHandler", "onQueryCommand: commandItem = null");
                    return;
                }
                boolean isClear = Boolean.parseBoolean(commandItem.getExtraValByKey("is_clear"));
                Debug.m5i("SyncHandler", "onQueryCommand isClear:" + isClear);
                if (isClear && !UnitConvertUtils.isUseNewUploadMethod(SyncHandler.this.mContext)) {
                    SyncManager.getInstance().startUploadAllUnSyncSportDatas();
                }
            }
        };
        mTransportManager.setOnDataArrivalListener(this);
        mTransportManager.setAsSerialMode(true);
        C06483 c06483 = new IntervalRunDataReceiver(this.mContext) {
            public void onDataReceived(TransportDataItem transportDataItem) {
                super.onDataReceived(transportDataItem);
                if (transportDataItem == null) {
                    Log.i("SyncHandler", "IntervalRunDataReceiver dataItem is null ");
                    return;
                }
                Log.i("SyncHandler", "IntervalRunDataReceiver:" + transportDataItem.getAction());
                if (transportDataItem.getAction().equalsIgnoreCase("com.huami.watch.sport.intervalrun")) {
                    DataBundle dataBundle = transportDataItem.getData();
                    if (dataBundle != null) {
                        String result = dataBundle.getString("com.huami.watch.sport.intervalrun");
                        Log.i("SyncHandler", "intervalRunResult:" + result);
                        if (result != null && result.length() > 0) {
                            IntertelRunDataParse.getInstance().productTrainProgramFile(SyncHandler.this.mContext, result);
                            return;
                        }
                        return;
                    }
                    Log.i("SyncHandler", "intervalRun databundle is null ");
                }
            }
        };
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 1:
                sendDownloadHistorySportRequest();
                return;
            case 2:
                sendDownloadSportSummaryRequest(msg.getData().getLong("track_id"));
                return;
            case 3:
                sendDownloadSportDetailRequest(msg.getData().getLong("track_id"));
                return;
            case 4:
                sendDownloadSportStatisticRequest();
                return;
            case 5:
                sendDownloadSportStepConfigRequest();
                return;
            case 6:
                sendUploadSportSummaryRequest(msg.getData().getLong("track_id"));
                return;
            case 7:
                sendUploadSportDetailRequest(msg.getData().getLong("track_id"));
                return;
            case 8:
                sendUploadSportStepConfigRequest();
                return;
            case 9:
                sendDeleteSportSummaryRequest(msg.getData().getLong("track_id"));
                return;
            case 101:
                receiveDownloadHistorySportResponse(msg.obj);
                return;
            case 102:
                receiveDownloadSportDetailResponse(msg.getData().getLong("track_id"), msg.obj);
                return;
            case 103:
                receiveDownloadSportStatistic(msg.obj);
                return;
            case 104:
                receiveDownloadSportStepConfigResponse(msg.obj);
                return;
            case 105:
                receiveUploadSportSummary(msg.getData().getLong("track_id"), msg.obj);
                return;
            case 106:
                receiveUploadSportDetail(msg.getData().getLong("track_id"), msg.obj);
                return;
            case 107:
                receiveUploadSportStepConfig(msg.obj);
                return;
            case 108:
                receiveDeleteSportSummary(msg.getData().getLong("track_id"), msg.obj);
                return;
            case 109:
                receiveDownloadSummarySportResponse(msg.obj);
                return;
            case 110:
                receiveUploadVo2maxThaInfoResponse(msg.obj);
                return;
            case 111:
                receiveUploadSportThaInfoResponse(msg.obj);
                return;
            case 112:
                receiveDownVo2maxDataResponse(msg.obj);
                return;
            case 113:
                receiveDownloadSportThaInfoResponse(msg.obj);
                return;
            case 114:
                sendDownloadVo2maxDataRequest();
                return;
            case 115:
                sendDownloadSportThaInfoRequest();
                return;
            case 116:
                receiveUploadFirstBeatConfigInfoResponse(msg.obj);
                return;
            case 117:
                DataItem response = msg.obj;
                Log.i("SyncHandler", " receiveDownloadFirstBeatResponse [" + response.getData() + "]");
                receiveDownloadFirstBeatConfigResponse(response);
                return;
            case 118:
                sendDownloadFistBeatConfigRequest();
                return;
            case 120:
                sendSportThaInfoSimpleRequest(msg.arg1);
                return;
            case 121:
                notifyPhoneSyncData();
                return;
            default:
                return;
        }
    }

    private void sendDownloadVo2maxDataRequest() {
        Log.i("SyncHandler", " sendDownloadVo2maxDataRequest ");
        String url = Config.getUrlDownloadVo2maxConfig();
        sendRequest(new DataItem(url + "?startDay=" + Utils.getDayTimeFormat(System.currentTimeMillis() - 604800000) + "&endDay=" + Utils.getDayTimeFormat(System.currentTimeMillis()), "com.huami.watch.sport.download_vo2_max_config", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void sendDownloadFistBeatConfigRequest() {
        Log.i("SyncHandler", " sendDownloadFistBeatConfigRequest ");
        String url = Config.getURLDownloadFirstBeatConfig();
        sendRequest(new DataItem(url + "?startDay=" + Utils.getDayTimeFormat(System.currentTimeMillis() - 604800000) + "&endDay=" + Utils.getDayTimeFormat(System.currentTimeMillis()), "com.huami.watch.sport.download_first_beat_config", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void sendDownloadSportThaInfoRequest() {
        Log.i("SyncHandler", " sendDownloadSportThaInfoRequest ");
        String url = Config.getUrlDownloadSportThaInfoConfig();
        sendRequest(new DataItem(url + "?startDay=" + Utils.getDayTimeFormat(System.currentTimeMillis() - 518400000) + "&endDay=" + Utils.getDayTimeFormat(System.currentTimeMillis()), "com.huami.watch.sport.download_sport_tha_info_config", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void receiveDownVo2maxDataResponse(DataItem response) {
        JSONException e;
        if (response != null) {
            String result = DataUtils.drillUnzipData(response);
            Log.i("SyncHandler", "receiveDownVo2maxDataResponse:[" + result + "]");
            if (result != null && !TextUtils.isEmpty(result)) {
                List<Vo2maxDayInfo> list = new ArrayList();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    try {
                        JSONArray jsonArray = jsonObject.optJSONArray("items");
                        if (jsonArray != null && jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                int uploadStatusInt;
                                JSONObject item = (JSONObject) jsonArray.get(i);
                                Vo2maxDayInfo vo2maxDayInfo = new Vo2maxDayInfo();
                                vo2maxDayInfo.initModelWithJsonObject(item);
                                if (Utils.getDayTimeFormat(System.currentTimeMillis()).equalsIgnoreCase(Utils.getDayTimeFormat(vo2maxDayInfo.getDayId()))) {
                                    uploadStatusInt = 0;
                                } else {
                                    uploadStatusInt = 1;
                                }
                                vo2maxDayInfo.setUpdateTime((long) uploadStatusInt);
                                list.add(vo2maxDayInfo);
                            }
                            if (list.size() > 0) {
                                this.mVo2maxDataBaseManager.addAll(this.mContext, list);
                            }
                        }
                    } catch (JSONException e2) {
                        e = e2;
                        JSONObject jSONObject = jsonObject;
                        e.printStackTrace();
                    }
                } catch (JSONException e3) {
                    e = e3;
                    e.printStackTrace();
                }
            }
        }
    }

    private void receiveDownloadSportThaInfoResponse(DataItem response) {
        if (response != null) {
            String result = DataUtils.drillUnzipData(response);
            Log.i("SyncHandler", "receiveDownloadSportThaInfoResponse:[" + result + "]");
            if (result != null && !TextUtils.isEmpty(result)) {
                try {
                    List<SportThaInfo> list = new ArrayList();
                    JSONArray jsonArray = new JSONObject(result).optJSONArray("items");
                    if (jsonArray != null && jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            int uploadStatusInt;
                            JSONObject item = (JSONObject) jsonArray.get(i);
                            SportThaInfo sportThaInfo = new SportThaInfo();
                            sportThaInfo.initModelWithJsonObject(item);
                            if (Utils.getDayTimeFormat(System.currentTimeMillis()).equalsIgnoreCase(Utils.getDayTimeFormat(sportThaInfo.getDayId()))) {
                                uploadStatusInt = 0;
                            } else {
                                uploadStatusInt = 1;
                            }
                            sportThaInfo.setUploadStatus(uploadStatusInt);
                            list.add(sportThaInfo);
                        }
                        FirstBeatDataUtils.setTrainLoadToSensorhub(this.mContext, list);
                        if (list.size() > 0) {
                            SportThaInfo lastSportThaInfoRecord = (SportThaInfo) list.get(list.size() - 1);
                            long lastDayId = lastSportThaInfoRecord.getDayId();
                            long thisDayId = FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis());
                            Log.i("SyncHandler", " thisDayId:" + thisDayId + " , lastDayId:" + lastDayId);
                            if (lastDayId < thisDayId) {
                                getThisDaySportThaInfoData();
                            } else if (lastDayId == thisDayId) {
                                boolean isHasDataInDbResult = SportThaInfoDao.getmInstance(this.mContext).isHasDataInDb(this.mContext, lastSportThaInfoRecord);
                                Log.i("SyncHandler", " isHasDataInDbResult:" + isHasDataInDbResult);
                                if (!isHasDataInDbResult) {
                                    this.mSportThaInfoDataBaseManager.add(this.mContext, lastSportThaInfoRecord);
                                }
                            } else {
                                Log.i("SyncHandler", " do nothing");
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getThisDaySportThaInfoData() {
        Log.i("SyncHandler", "getThisDaySportThaInfoData: ");
        HmSensorHubConfigManager.getHmSensorHubConfigManager(Global.getApplicationContext()).requestRealtimeData(2, new C06494());
    }

    private void receiveDownloadFirstBeatConfigResponse(DataItem response) {
        JSONException e;
        if (response != null) {
            String result = DataUtils.drillUnzipData(response);
            if (result == null || TextUtils.isEmpty(result)) {
                Log.i("SyncHandler", " receiveDownloadFirstBeatConfig is null ");
                return;
            }
            Log.i("SyncHandler", " receiveDownloadFirstbeat result:" + result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                try {
                    JSONArray jsonArray = jsonObject.optJSONArray("items");
                    if (jsonArray == null || jsonArray.length() == 0) {
                        Log.i("SyncHandler", " firstbeatConfig jsonArray is null ");
                        return;
                    }
                    FirstBeatConfig firstBeatConfig = FirstBeatConfig.newFirstBeatConfig();
                    firstBeatConfig.initObjectFromJSON((JSONObject) jsonArray.get(0));
                    DataManager.getInstance().setFirstBeatConfig(this.mContext, firstBeatConfig);
                    Log.i("SyncHandler", " setVo2maxToKernel:" + firstBeatConfig.toString());
                    FirstBeatDataUtils.setVo2maxToSensorhub(this.mContext, firstBeatConfig.getCurrnetRunVo2max(), firstBeatConfig.getCurrnetWalkingVo2max());
                } catch (JSONException e2) {
                    e = e2;
                    JSONObject jSONObject = jsonObject;
                    e.printStackTrace();
                }
            } catch (JSONException e3) {
                e = e3;
                e.printStackTrace();
            }
        }
    }

    private void receiveUploadSportThaInfoResponse(DataItem response) {
        if (response != null) {
            String result = DataUtils.drillUnzipData(response);
            Log.i("SyncHandler", " receiveUploadSportThaInfoResponse  [" + result + "]");
            if (result != null && !TextUtils.isEmpty(result)) {
                SportThaInfo sportThaInfo = new SportThaInfo();
                try {
                    sportThaInfo.initModelWithJsonObject(new JSONObject(result));
                    Log.i("SyncHandler", " currnetDayId:" + sportThaInfo.getDayId());
                    SportThaInfo sportInfoInDb = (SportThaInfo) this.mSportThaInfoDataBaseManager.select(this.mContext, "select * from sport_tha_info where dayId=? ; ", new String[]{String.valueOf(currentDayId)});
                    if (sportInfoInDb != null) {
                        if (sportInfoInDb.getDayId() == FirstBeatDataUtils.getEveryDayTrainLoadIdByCurrnetTime(System.currentTimeMillis())) {
                            sportInfoInDb.setUploadStatus(0);
                        } else {
                            sportInfoInDb.setUploadStatus(1);
                        }
                        this.mSportThaInfoDataBaseManager.update(this.mContext, sportInfoInDb);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void receiveUploadVo2maxThaInfoResponse(DataItem response) {
        if (response != null) {
            String result = DataUtils.drillUnzipData(response);
            Log.i("SyncHandler", " receiveUploadVO2maxThaInfoResponse  [" + result + "]");
            if (result != null && !TextUtils.isEmpty(result)) {
                Vo2maxDayInfo vo2maxDayInfo = new Vo2maxDayInfo();
                try {
                    vo2maxDayInfo.initModelWithJsonObject(new JSONObject(result));
                    vo2maxDayInfo.setUploadStatus(1);
                    this.mVo2maxDataBaseManager.add(this.mContext, vo2maxDayInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void receiveUploadFirstBeatConfigInfoResponse(DataItem response) {
        if (response != null) {
            Log.i("SyncHandler", " receiveUploadFirstBeatConfigResponse:" + DataUtils.drillUnzipData(response));
        }
    }

    private void receiveDeleteSportSummary(long trackId, DataItem response) {
        Debug.m3d("SyncHandler", "receiveDeleteSportSummary, " + trackId);
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server detail response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
                return;
            }
            return;
        }
        try {
            int code = new JSONObject(jsonData).optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot delete from server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
                    return;
                }
                return;
            }
            if (this.mSportSummarySyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId})) {
                this.mLocationDataSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
                this.mRunningInfoPerKMSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
                this.mRunningInfoPerLapSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
                this.mHeartRateSyncDatabaseManager.remove(this.mContext, "track_id=?", new String[]{"" + trackId});
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.deleteSportSummaryFinished(trackId, true);
                    return;
                }
                return;
            }
            Debug.m6w("SyncHandler", "delete sport summary failed");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
            }
        }
    }

    private void receiveDownloadSportDetailResponse(long trackId, DataItem response) {
        Debug.m3d("SyncHandler", "receiveDownloadSportDetailResponse");
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server detail response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportDetailFinished(-1, false);
                return;
            }
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            int code = jsonObject.optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot download detail from server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportDetailFinished(-1, false);
                    return;
                }
                return;
            }
            List<SportLocationData> locationDatas = new LinkedList();
            List<RunningInfoPerKM> runningInfoPerKMs = new LinkedList();
            List<RunningInfoPerLap> runningInfoPerIndividuals = new LinkedList();
            List<HeartRate> heartRates = new LinkedList();
            List<PauseInfo> pauseInfoList = new LinkedList();
            List<CyclingDetail> cyclingDetailList = new LinkedList();
            List<DailyPerformanceInfo> dailyPerformanceInfos = new LinkedList();
            SportSummary sportSummary = (SportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
            if (sportSummary == null) {
                Debug.m6w("SyncHandler", "cannot find summary while download detail " + trackId);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportDetailFinished(trackId, false);
                }
            } else if (this.mSportDetailParser.parseSportDetailData(jsonObject, locationDatas, runningInfoPerKMs, runningInfoPerIndividuals, heartRates, pauseInfoList, cyclingDetailList, dailyPerformanceInfos)) {
                this.mLocationDataSyncDatabaseManager.addAll(this.mContext, locationDatas);
                this.mRunningInfoPerKMSyncDatabaseManager.addAll(this.mContext, runningInfoPerKMs);
                this.mRunningInfoPerLapSyncDatabaseManager.addAll(this.mContext, runningInfoPerIndividuals);
                this.mHeartRateSyncDatabaseManager.addAll(this.mContext, heartRates);
                this.mRunningInfoCadenceSyncDatabaseManager.addAll(this.mContext, cyclingDetailList);
                this.mRunningDailyformanceInfoManager.addAll(this.mContext, dailyPerformanceInfos);
                if (sportSummary.getCurrentStatus() == 9) {
                    sportSummary.setCurrentStatus(10);
                } else {
                    sportSummary.setCurrentStatus(6);
                }
                if (!(pauseInfoList == null || pauseInfoList.isEmpty())) {
                    for (PauseInfo ps : pauseInfoList) {
                        sportSummary.addPauseInfo(ps);
                    }
                }
                this.mSportSummarySyncDatabaseManager.update(this.mContext, sportSummary);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportDetailFinished(trackId, true);
                }
            } else {
                Debug.m6w("SyncHandler", "parse sport detail failed");
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportDetailFinished(-1, false);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportDetailFinished(-1, false);
            }
        }
    }

    private String parseServerResponse(DataItem response) {
        if (response.getState() != 0) {
            Debug.m6w("SyncHandler", "request has not been processed by server!, state: " + response.getState() + ", code:" + response.getCode());
            return null;
        }
        String data = response.getData();
        if (data != null && !data.isEmpty()) {
            return DataUtils.drillUnzipData(response);
        }
        Debug.m6w("SyncHandler", "data is null or empty");
        return null;
    }

    private void receiveDownloadSummarySportResponse(DataItem response) {
        Debug.m3d("SyncHandler", "receiveDownloadSummarySportResponse " + response);
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server history response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportSummaryFinished(null, false);
                return;
            }
            return;
        }
        Debug.m3d("SyncHandler", "received data : " + jsonData);
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            int code = jsonObject.optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot download history from server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportSummaryFinished(null, false);
                    return;
                }
                return;
            }
            OutdoorSportSummary childSummary = this.mSportSummaryParser.parseChildJsonToSummary(jsonObject);
            if (childSummary != null) {
                childSummary.setCurrentStatus(4);
                this.mSportSummarySyncDatabaseManager.add(this.mContext, childSummary);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportSummaryFinished(childSummary, true);
                    return;
                }
                return;
            }
            Debug.m6w("SyncHandler", "cannot parse json to summaries");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportSummaryFinished(null, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportSummaryFinished(null, false);
            }
        }
    }

    private void receiveDownloadHistorySportResponse(DataItem response) {
        Debug.m3d("SyncHandler", "receiveDownloadHistorySportResponse " + response);
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server history response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportHistoryFinished(null, null, false);
                return;
            }
            return;
        }
        Debug.m3d("SyncHandler", "received data : " + jsonData);
        try {
            JSONObject jSONObject = new JSONObject(jsonData);
            int code = jSONObject.optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot download history from server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportHistoryFinished(null, null, false);
                    return;
                }
                return;
            }
            List<OutdoorSportSummary> summaries = this.mSportSummaryParser.parseHistoryJsonToSummaries(jSONObject);
            List<? extends SportSummary> localSummaries = this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, null, null, null, null);
            LongSparseArray<SportSummary> longSparseArray = null;
            if (!(localSummaries == null || localSummaries.isEmpty())) {
                LongSparseArray<SportSummary> longSparseArray2 = new LongSparseArray(localSummaries.size());
                for (SportSummary summary : localSummaries) {
                    longSparseArray2.put(summary.getTrackId(), summary);
                }
            }
            if (longSparseArray == null) {
                longSparseArray = new LongSparseArray();
            }
            if (summaries != null) {
                List<Long> needSyncTrackIds = new ArrayList();
                List<SportSummary> parentSummarys = new ArrayList();
                Map<Long, SportSummary> childSummarysMap = new HashMap();
                List<SportSummary> updatedSummaries = new LinkedList();
                for (SportSummary summary2 : summaries) {
                    SportSummary existSummary = (SportSummary) longSparseArray.get(summary2.getTrackId());
                    if (existSummary == null) {
                        summary2.setCurrentStatus(4);
                        updatedSummaries.add(summary2);
                        if (SportType.isMixedSport(summary2.getSportType())) {
                            parentSummarys.add(summary2);
                        } else if (SportType.isChildSport(summary2.getSportType())) {
                            childSummarysMap.put(Long.valueOf(summary2.getTrackId()), summary2);
                        }
                    } else if (existSummary.getCurrentStatus() == 4) {
                        updatedSummaries.add(summary2);
                        if (SportType.isMixedSport(summary2.getSportType())) {
                            parentSummarys.add(summary2);
                        } else if (SportType.isChildSport(summary2.getSportType())) {
                            childSummarysMap.put(Long.valueOf(summary2.getTrackId()), summary2);
                        }
                    } else {
                        LogUtil.m9i(true, "SyncHandler", "summary has exist " + summary2.getTrackId());
                    }
                }
                for (SportSummary summary22 : parentSummarys) {
                    for (Long longValue : summary22.getChildSportSummrys()) {
                        long childTrackId = longValue.longValue();
                        if (((SportSummary) childSummarysMap.get(Long.valueOf(childTrackId))) == null) {
                            summary22.setCurrentStatus(9);
                            needSyncTrackIds.add(Long.valueOf(childTrackId));
                        }
                    }
                }
                Debug.m5i("SyncHandler", "there is not download child summaryIds:" + Arrays.toString(needSyncTrackIds.toArray()));
                this.mSportSummarySyncDatabaseManager.addAll(this.mContext, updatedSummaries);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.downloadSportHistoryFinished(updatedSummaries, needSyncTrackIds, true);
                    return;
                }
                return;
            }
            Debug.m6w("SyncHandler", "cannot parse json to summaries");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportHistoryFinished(null, null, false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportHistoryFinished(null, null, false);
            }
        }
    }

    private void receiveUploadSportDetail(long trackId, DataItem response) {
        Debug.m3d("SyncHandler", "receiveUploadSportDetail");
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server upload detail response content");
            return;
        }
        try {
            int code = new JSONObject(jsonData).optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot upload detail to server, response code is " + code);
            } else {
                Debug.m3d("SyncHandler", "receiveUploadSportDetail, upload detail success trackId:" + trackId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void receiveDownloadSportStatistic(DataItem response) {
        Debug.m3d("SyncHandler", "receiveDownloadSportStatistic " + response);
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server upload summary response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStatisticFinished(null, false);
                return;
            }
            return;
        }
        try {
            SportStatistic sportStatistic = this.mStatisticParser.parseJsonToSportStatistic(new JSONObject(jsonData));
            if (sportStatistic == null) {
                Debug.m6w("SyncHandler", "cannot parse sport statistic " + jsonData);
                return;
            }
            this.mDataManager.addStatistic(this.mContext, sportStatistic);
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStatisticFinished(sportStatistic, true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStatisticFinished(null, false);
            }
        }
    }

    private void receiveDownloadSportStepConfigResponse(DataItem response) {
        Debug.m3d("SyncHandler", "receiveDownloadSportStepConfigResponse " + response);
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server download sport step config response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStepConfigFinished(null, false);
                return;
            }
            return;
        }
        try {
            StepConfig stepConfig = this.mStepConfigParser.parseJsonToStepConfig(new JSONObject(jsonData));
            if (stepConfig == null) {
                Debug.m6w("SyncHandler", "cannot parse sport step config " + jsonData);
                return;
            }
            this.mDataManager.updateStepConfig(this.mContext, stepConfig);
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStepConfigFinished(stepConfig, true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.downloadSportStepConfigFinished(null, false);
            }
        }
    }

    private void receiveUploadSportSummary(long trackId, DataItem response) {
        Debug.m3d("SyncHandler", "receiveUploadSportSummary");
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("SyncHandler", "cannot get server upload summary response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportSummaryFinished(trackId, false);
                tellUploadResultForme(false);
                return;
            }
            return;
        }
        try {
            int code = new JSONObject(jsonData).optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot upload summary to server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    tellUploadResultForme(false);
                    this.mSyncResponseListener.uploadSportSummaryFinished(trackId, false);
                    return;
                }
                return;
            }
            if (this.mSyncResponseListener != null) {
                tellUploadResultForme(true);
                this.mSyncResponseListener.uploadSportSummaryFinished(trackId, true);
            }
            OutdoorSportSummary sportSummary = (OutdoorSportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
            if (sportSummary == null || SportType.isChildSport(sportSummary.getSportType())) {
                Debug.m6w("SyncHandler", "child summary skip");
                return;
            }
            if (SportType.isMixedSport(sportSummary.getSportType())) {
                for (Long longValue : sportSummary.getChildSportSummrys()) {
                    long childId = longValue.longValue();
                    DataManager.getInstance().clearUploadCached(childId);
                    UploadCacheManager.getInstance(this.mContext).clearCache(childId);
                }
            }
            DataManager.getInstance().clearUploadCached(trackId);
            UploadCacheManager.getInstance(this.mContext).clearCache(trackId);
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportSummaryFinished(trackId, false);
                tellUploadResultForme(false);
            }
        }
    }

    private void receiveUploadSportStepConfig(DataItem response) {
        Debug.m3d("SyncHandler", "receiveUploadSportStepConfig");
        String jsonData = parseServerResponse(response);
        if (jsonData == null) {
            Debug.m3d("TAG", "cannot get server upload step config response content");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportStepConfigFinished(false);
                return;
            }
            return;
        }
        try {
            int code = new JSONObject(jsonData).optInt("code", -1);
            if (code != 1) {
                Debug.m3d("SyncHandler", "cannot upload step config to server, response code is " + code);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.uploadSportStepConfigFinished(false);
                }
            } else if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportStepConfigFinished(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportStepConfigFinished(false);
            }
        }
    }

    public void tellUploadResultForme(boolean isSuccess) {
        if (mTransportManager != null) {
            mTransportManager.tellUploadResultForMe(isSuccess, String.valueOf(System.currentTimeMillis()), "com.huami.watch.sport.upload_summary");
        }
    }

    public void setSyncResponseListener(ISyncResponseListener syncResponseListener) {
        this.mSyncResponseListener = syncResponseListener;
    }

    private void sendDownloadSportStatisticRequest() {
        Debug.m3d("SyncHandler", "sendDownloadSportStatisticRequest");
        sendRequest(new DataItem(Config.getUrlGetStatistic(), "com.huami.watch.sport.download_statistic", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void sendDownloadSportStepConfigRequest() {
        Debug.m3d("TAG", "sendDownloadSportStepConfigRequest");
        sendRequest(new DataItem(Config.getUrlGetConfig(), "com.huami.watch.sport.download_step_config", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void sendDownloadSportSummaryRequest(long trackId) {
        Debug.m3d("SyncHandler", "sendDownloadSportSummaryRequest, track id : " + trackId);
        sendRequest(new DataItem(String.format(Config.getUrlGetSummary(), new Object[]{Long.valueOf(trackId / 1000)}), "com.huami.watch.sport.download_child_summary", "com.huami.watch.newsport", "get", ""), trackId);
    }

    private void sendDownloadSportDetailRequest(long trackId) {
        Debug.m3d("SyncHandler", "sendDownloadSportDetailRequest, track id : " + trackId);
        sendRequest(new DataItem(String.format(Config.getUrlGetDetail(), new Object[]{Long.valueOf(trackId / 1000)}), "com.huami.watch.sport.download_detail", "com.huami.watch.newsport", "get", ""), trackId);
    }

    private void sendRequest(DataItem dataItem, long trackId) {
        Debug.m3d("SyncHandler", "send request " + dataItem + ", track id : " + trackId);
        mTransportManager.sendSync(dataItem);
        RequestInfo requestInfo = RequestInfo.createRequest(trackId, dataItem.getIdentifier(), dataItem.getAction());
        requestInfo.setRequestStat(12);
        this.mRequestInfoDatabaseManager.add(this.mContext, requestInfo, null);
        Global.getGlobalWorkHandler().removeCallbacks(this.mDelayTriggerFromClient);
        Global.getGlobalWorkHandler().postDelayed(this.mDelayTriggerFromClient, 70000);
    }

    private void sendDeleteSportSummaryRequest(long trackId) {
        Debug.m3d("SyncHandler", "sendDeleteSportSummaryRequest, track id : " + trackId);
        OutdoorSportSummary sportSummary = (OutdoorSportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
        if (sportSummary == null) {
            Debug.m6w("SyncHandler", "cannot find sport summary");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
            }
        } else if (this.mSportSummaryParser.parseTrackIdToMap(sportSummary) == null) {
            Debug.m6w("SyncHandler", "cannot parse summary : " + sportSummary);
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.deleteSportSummaryFinished(trackId, false);
            }
        } else {
            sendRequest(new DataItem(String.format(Config.getUrlDeleteSummary(), new Object[]{Long.valueOf(trackId / 1000)}), "com.huami.watch.sport.delete_summary", "com.huami.watch.newsport", "get", ""), trackId);
        }
    }

    private void sendUploadSportStepConfigRequest() {
        Debug.m3d("SyncHandler", "sendUploadSportStepConfigRequest");
        StepConfig config = DataManager.getInstance().getStepConfig(this.mContext);
        if (config == null || (config != null && config.isEmpty())) {
            Debug.m6w("TAG", "cannot find sport step config");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportStepConfigFinished(false);
                return;
            }
            return;
        }
        Map<String, String> params = this.mStepConfigParser.parseStepConfigToMap(config);
        if (params == null) {
            Debug.m6w("TAG", "cannot parse step config : " + config.toString());
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportStepConfigFinished(false);
                return;
            }
            return;
        }
        DataItem dataItem = new DataItem(Config.getUrlUploadConfig(), "com.huami.watch.sport.upload_step_config", "com.huami.watch.newsport", "post", parseMapToData(params));
        packetPostDataItem(dataItem);
        sendRequest(dataItem, -1);
    }

    private void sendUploadSportSummaryRequest(long trackId) {
        Debug.m3d("SyncHandler", "sendUploadSportSummaryRequest, track id : " + trackId);
        OutdoorSportSummary sportSummary = (OutdoorSportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
        if (sportSummary == null) {
            Debug.m6w("SyncHandler", "cannot find sport summary");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportSummaryFinished(trackId, false);
            }
        } else if (sportSummary.getCurrentStatus() == 7) {
            Debug.m6w("SyncHandler", "cannot send sport summary request, the record is already deleted");
        } else {
            Map<String, String> params = this.mSportSummaryParser.parseSummaryToMap(sportSummary);
            if (params == null) {
                Debug.m6w("SyncHandler", "cannot parse summary : " + sportSummary);
                if (this.mSyncResponseListener != null) {
                    this.mSyncResponseListener.uploadSportSummaryFinished(trackId, false);
                    return;
                }
                return;
            }
            DataItem dataItem = new DataItem(Config.getUrlUploadSummary(), "com.huami.watch.sport.upload_summary", "com.huami.watch.newsport", "post", parseMapToData(params));
            packetPostDataItem(dataItem);
            sendRequest(dataItem, trackId);
        }
    }

    private void sendSportThaInfoSimpleRequest(int type) {
        Log.i("SyncHandler", " sendSportThaInfoRequest  start ");
        if (!ModelUtil.isModelHuanghe(null)) {
            List<SportThaInfo> list = null;
            if (type == 0) {
                list = this.mSportThaInfoDataBaseManager.selectAll(this.mContext, "upload_status=? and  updateTime > ?", new String[]{"0", String.valueOf(DataManager.getInstance().getLastSportThaInfoUploadTime())}, "dayId ASC", null);
                if (list != null) {
                    Log.i("SyncHandler", "sendSportThaInfoSimpleRequest: size:" + list.size() + ",uploadTime:" + lastUploadSportThaInfoTime);
                }
            } else if (1 == type) {
                list = this.mSportThaInfoDataBaseManager.selectAll(this.mContext, "upload_status=? or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
            }
            if (list != null && list.size() > 0) {
                int i;
                for (i = 0; i < list.size(); i++) {
                    String result = ((SportThaInfo) list.get(i)).modelToJson();
                    Log.i("SyncHandler", " FLAG_DECODE_BE_SEND sendSportThaInfoRequest[" + result + "]");
                    DataItem dataItem = new DataItem(Config.getUrlUploadSportThaInfoConfig(), "com.huami.watch.sport.upload_sport_tha_info_config", "com.huami.watch.newsport", "post", result);
                    dataItem.addFlags(16);
                    dataItem.addFlags(8);
                    dataItem.addHeader("Content-Type", "application/json");
                    dataItem.addHeader("Accept", "application/json");
                    dataItem.addHeader("Connection", "keep-alive");
                    mTransportManager.sendSync(dataItem);
                }
                Log.i("SyncHandler", " sendSportThaInfoRequest  over ");
                DataManager.getInstance().setLastSportThaInfoUploadTime(((SportThaInfo) list.get(list.size() - 1)).getUpdateTime());
                for (i = 0; i < list.size(); i++) {
                    SportThaInfo updateSportThaInfo = (SportThaInfo) list.get(i);
                    updateSportThaInfo.setUploadStatus(2);
                    this.mSportThaInfoDataBaseManager.update(this.mContext, updateSportThaInfo);
                }
            }
            sendVO2maxInfoSimpleRequest(type);
        }
    }

    private void sendVO2maxInfoSimpleRequest(int type) {
        Log.i("SyncHandler", " sendVO2maxThaInfoRequest  start ");
        List<Vo2maxDayInfo> list = null;
        if (type == 0) {
            list = this.mVo2maxDataBaseManager.selectAll(this.mContext, "upload_status=? and update_time>?", new String[]{"0", String.valueOf(DataManager.getInstance().getLastVo2maxUploadTime())}, "dayId ASC", null);
            if (list != null) {
                Log.i("SyncHandler", "sendVO2maxInfoSimpleRequest: size:" + list.size() + ",uploadTime:" + uploadTime);
            }
        } else if (1 == type) {
            list = this.mVo2maxDataBaseManager.selectAll(this.mContext, "upload_status=?  or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
        }
        if (list != null && list.size() > 0) {
            int i;
            for (i = 0; i < list.size(); i++) {
                String result = ((Vo2maxDayInfo) list.get(i)).modelToJson();
                Log.i("SyncHandler", " FLAG_DECODE_BE_SEND sendVO2maxThaInfoRequest[" + result + "]");
                DataItem dataItem = new DataItem(Config.getUrlUploadVo2MaxConfig(), "com.huami.watch.sport.upload_vo2_max_config", "com.huami.watch.newsport", "post", result);
                dataItem.addFlags(16);
                dataItem.addFlags(8);
                dataItem.addHeader("Content-Type", "application/json");
                dataItem.addHeader("Accept", "application/json");
                mTransportManager.sendSync(dataItem);
            }
            Log.i("SyncHandler", " sendVO2maxThaInfoRequest  over ");
            DataManager.getInstance().setLastVo2maxUploadTime(((Vo2maxDayInfo) list.get(list.size() - 1)).getUpdateTime());
            for (i = 0; i < list.size(); i++) {
                Vo2maxDayInfo currentVo2maxDayInfo = (Vo2maxDayInfo) list.get(i);
                currentVo2maxDayInfo.setUploadStatus(2);
                this.mVo2maxDataBaseManager.update(this.mContext, currentVo2maxDayInfo);
            }
        }
        List<SportThaInfo> checkSportThaInfoList = this.mSportThaInfoDataBaseManager.selectAll(this.mContext, "upload_status=? or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
        List<Vo2maxDayInfo> checkVo2maxList = this.mVo2maxDataBaseManager.selectAll(this.mContext, "upload_status=?  or upload_status=?", new String[]{String.valueOf(0), String.valueOf(2)}, "dayId ASC", null);
        if (checkSportThaInfoList != null && checkSportThaInfoList.size() > 0 && checkVo2maxList != null && checkVo2maxList.size() > 0) {
            sendFirstBeatConfigRequest();
        }
    }

    private void sendFirstBeatConfigRequest() {
        Log.i("SyncHandler", " sendFirstBeatConfigRequest ");
        FirstBeatConfig firstBeatConfig = DataManager.getInstance().getFirstBeatConfig(this.mContext);
        if (firstBeatConfig == null || firstBeatConfig.getCurrnetRunVo2max() < 0.0f) {
            Log.i("SyncHandler", new StringBuilder().append(" firstBeat need not to send ").append(firstBeatConfig).toString() != null ? firstBeatConfig.toString() : "null");
            return;
        }
        Log.i("SyncHandler", " sendFirstBeatConfigRequest:" + firstBeatConfig.toString());
        DataItem dataItem = new DataItem(Config.getURLUploadFirstBeatConfig(), "com.huami.watch.sport.upload_first_beat_config", "com.huami.watch.newsport", "post", firstBeatConfig.toJSON());
        dataItem.addFlags(16);
        dataItem.addFlags(8);
        dataItem.addHeader("Content-Type", "application/json");
        dataItem.addHeader("Accept", "application/json");
        mTransportManager.sendSync(dataItem);
    }

    private String parseMapToData(Map<String, String> params) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("SyncHandler", "parseMapToData. map : " + params);
        }
        if (params == null) {
            Debug.m6w("SyncHandler", "params is null");
            return null;
        }
        StringBuilder dataBuilder = new StringBuilder();
        Iterator iterator = params.keySet().iterator();
        if (!iterator.hasNext()) {
            return "";
        }
        String key = (String) iterator.next();
        dataBuilder.append(key).append("=").append((String) params.get(key));
        while (iterator.hasNext()) {
            String k = (String) iterator.next();
            dataBuilder.append("&").append(k).append("=").append((String) params.get(k));
        }
        String data = dataBuilder.toString();
        if (!Global.DEBUG_LEVEL_3) {
            return data;
        }
        Debug.m3d("SyncHandler", "parsed data : " + data);
        return data;
    }

    private void sendUploadSportDetailRequest(long trackId) {
        Debug.m3d("SyncHandler", "sendUploadSportDetailRequest, track id " + trackId);
        if (DataManager.getInstance().isUploadDetailSportCached(trackId)) {
            String detail = UploadCacheManager.getInstance(this.mContext).newSaver(trackId).getAllDetailData();
            LogUtil.m9i(true, "SyncHandler", "sendUploadSportDetailRequest, detail:" + detail);
            if (detail != null) {
                DataItem dataItem = new DataItem(Config.getUrlUploadDetail(), "com.huami.watch.sport.upload_detail", "com.huami.watch.newsport", "post", detail);
                packetPostDataItem(dataItem);
                sendRequest(dataItem, trackId);
                sendUploadSportSummaryRequest(trackId);
                return;
            }
        }
        DetailInfoSaver saver = UploadCacheManager.getInstance(this.mContext).getSingleSportSaver(trackId);
        if (saver == null) {
            UploadCacheManager.getInstance(this.mContext).addSportInfo(trackId);
            saver = UploadCacheManager.getInstance(this.mContext).getSingleSportSaver(trackId);
        }
        OutdoorSportSummary sportSummary = (OutdoorSportSummary) this.mSportSummarySyncDatabaseManager.select(this.mContext, "track_id=?", new String[]{"" + trackId});
        if (sportSummary == null) {
            Debug.m3d("SyncHandler", "sport summary is null");
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportDetailFinished(trackId, false);
                return;
            }
            return;
        }
        List<? extends SportLocationData> locationDatas = this.mLocationDataSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (locationDatas == null || locationDatas.isEmpty()) {
            Debug.m6w("SyncHandler", "location is null or empty. track id : " + trackId);
        }
        List<? extends RunningInfoPerKM> runningInfoPerKMs = this.mRunningInfoPerKMSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerKMs == null || runningInfoPerKMs.isEmpty()) {
            Debug.m6w("SyncHandler", "running info is null or empty. track id : " + trackId);
        }
        List<? extends HeartRate> heartRates = this.mHeartRateSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        LogUtil.m9i(true, "SyncHandler", "upload select heart info end");
        List<? extends RunningInfoPerLap> runningInfoPerIndividuals = this.mRunningInfoPerLapSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerIndividuals == null || runningInfoPerIndividuals.isEmpty()) {
            Debug.m6w("SyncHandler", "running laps info is null or empty. track id : " + trackId);
        }
        List<? extends CyclingDetail> cyclingDetails = this.mRunningInfoCadenceSyncDatabaseManager.selectAll(this.mContext, "track_id>=?", new String[]{"" + trackId}, null, null);
        if (cyclingDetails == null || cyclingDetails.isEmpty()) {
            Debug.m6w("SyncHandler", "running cadence info is null or empty. track id : " + trackId);
        }
        List<? extends DailyPerformanceInfo> dailyPerformanceInfos = this.mRunningDailyformanceInfoManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (dailyPerformanceInfos == null || dailyPerformanceInfos.isEmpty()) {
            Debug.m6w("SyncHandler", "running daily info is null or empty. track id : " + trackId);
        } else {
            Log.i("SyncHandler", " upload dailyPerformanceInfos size:" + dailyPerformanceInfos.size());
        }
        Map<String, String> params = this.mSportDetailParser.parseDetailToMap(sportSummary, locationDatas, runningInfoPerKMs, runningInfoPerIndividuals, heartRates, cyclingDetails, dailyPerformanceInfos);
        if (params == null) {
            Debug.m6w("SyncHandler", "cannot parse detail. location : " + locationDatas + ", running info : " + runningInfoPerKMs);
            if (this.mSyncResponseListener != null) {
                this.mSyncResponseListener.uploadSportDetailFinished(trackId, false);
                return;
            }
            return;
        }
        DataItem dataItem2 = new DataItem(Config.getUrlUploadDetail(), "com.huami.watch.sport.upload_detail", "com.huami.watch.newsport", "post", parseMapToData(params));
        packetPostDataItem(dataItem2);
        sendRequest(dataItem2, trackId);
        saver.saveAllUploadDetailDataFromOldUpload(parseMapToData(params));
        sendUploadSportSummaryRequest(trackId);
    }

    private void packetPostDataItem(DataItem dataItem) {
        dataItem.addFlags(16);
        dataItem.addFlags(8);
        dataItem.addHeader("Content-Type", "application/x-www-form-urlencoded");
        dataItem.addHeader("Accept", "*/*");
    }

    private void sendDownloadHistorySportRequest() {
        Debug.m3d("SyncHandler", "sendDownloadHistorySportRequest");
        sendRequest(new DataItem(Config.getUrlGetHistory(), "com.huami.watch.sport.download_summary", "com.huami.watch.newsport", "get", ""), -1);
    }

    private void notifyPhoneSyncData() {
        LogUtil.m9i(true, "SyncHandler", "notifyPhoneSyncData");
        DataBundle dataBundle = new DataBundle();
        dataBundle.putString("TargetPackage", "com.huami.watch.newsport");
        Transporter.get(this.mContext, "com.huami.watch.companion").send("com.huami.watch.companion.RequestSyncSportHealthData", dataBundle);
    }

    public void onDataArrival(final DataItem dataItem) {
        Debug.m3d("SyncHandler", "on data arrival: " + dataItem);
        Debug.m3d("SyncHandler", "receive data : " + DataUtils.drillUnzipData(dataItem));
        if (dataItem != null) {
            if ("com.huami.watch.sport.upload_vo2_max_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(110, dataItem).sendToTarget();
            } else if ("com.huami.watch.sport.upload_sport_tha_info_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(111, dataItem).sendToTarget();
            } else if ("com.huami.watch.sport.download_vo2_max_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(112, dataItem).sendToTarget();
            } else if ("com.huami.watch.sport.download_sport_tha_info_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(113, dataItem).sendToTarget();
            } else if ("com.huami.watch.sport.upload_first_beat_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(116, dataItem).sendToTarget();
            } else if ("com.huami.watch.sport.download_first_beat_config".equalsIgnoreCase(dataItem.getAction())) {
                obtainMessage(117, dataItem).sendToTarget();
            }
        }
        this.mRequestInfoDatabaseManager.select(this.mContext, "id=?", new String[]{dataItem.getIdentifier()}, new Callback() {
            protected void doCallback(int resultCode, Object params) {
                RequestInfo requestInfo = (RequestInfo) params;
                if (requestInfo == null) {
                    Debug.m6w("SyncHandler", "there is no request with identifier : " + dataItem.getIdentifier());
                    return;
                }
                SyncHandler.this.mRequestInfoDatabaseManager.remove(SyncHandler.this.mContext, "id=?", new String[]{dataItem.getIdentifier()}, null);
                if (SyncHandler.this.mSyncResponseListener == null) {
                    return;
                }
                Message msg;
                Bundle data;
                if ("com.huami.watch.sport.download_detail".equals(requestInfo.getAction())) {
                    msg = SyncHandler.this.obtainMessage(102, dataItem);
                    data = new Bundle();
                    data.putLong("track_id", requestInfo.getTrackId());
                    msg.setData(data);
                    msg.sendToTarget();
                } else if ("com.huami.watch.sport.download_summary".equals(requestInfo.getAction())) {
                    SyncHandler.this.obtainMessage(101, dataItem).sendToTarget();
                    Log.i("LOG_TRANS_*", "onDataArrival_" + dataItem.getAction());
                    Log.i("LOG_TRANS_*", "onDataArrival:" + SportTransHelper.isRestored());
                    if (SportTransHelper.isRestored()) {
                        SportTransHelper.setDataRestored(false);
                    } else {
                        SportTransHelper.setDataRestored(true);
                    }
                } else if ("com.huami.watch.sport.upload_detail".equals(requestInfo.getAction())) {
                    msg = SyncHandler.this.obtainMessage(106, dataItem);
                    data = new Bundle();
                    data.putLong("track_id", requestInfo.getTrackId());
                    msg.setData(data);
                    msg.sendToTarget();
                } else if ("com.huami.watch.sport.upload_summary".equals(requestInfo.getAction())) {
                    msg = SyncHandler.this.obtainMessage(105, dataItem);
                    data = new Bundle();
                    data.putLong("track_id", requestInfo.getTrackId());
                    msg.setData(data);
                    msg.sendToTarget();
                } else if ("com.huami.watch.sport.download_statistic".equals(requestInfo.getAction())) {
                    SyncHandler.this.obtainMessage(103, dataItem).sendToTarget();
                } else if ("com.huami.watch.sport.delete_summary".equals(requestInfo.getAction())) {
                    msg = SyncHandler.this.obtainMessage(108, dataItem);
                    data = new Bundle();
                    data.putLong("track_id", requestInfo.getTrackId());
                    msg.setData(data);
                    msg.sendToTarget();
                } else if ("com.huami.watch.sport.download_step_config".equals(requestInfo.getAction())) {
                    SyncHandler.this.obtainMessage(104, dataItem).sendToTarget();
                } else if ("com.huami.watch.sport.upload_step_config".equals(requestInfo.getAction())) {
                    SyncHandler.this.obtainMessage(107, dataItem).sendToTarget();
                } else if ("com.huami.watch.sport.download_child_summary".equals(requestInfo.getAction())) {
                    SyncHandler.this.obtainMessage(109, dataItem).sendToTarget();
                }
            }
        });
    }
}
