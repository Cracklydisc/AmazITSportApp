package com.huami.watch.newsport.syncservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.LongSparseArray;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.syncservice.SyncManager.IHistoryUploadListener;

public class UploadUnsyncDataService extends Service implements IHistoryUploadListener {
    private boolean mBroadcastWhenFinished;
    private SyncManager mSyncManager = SyncManager.getInstance();
    private LongSparseArray<Boolean> mTrackIds = new LongSparseArray();

    class C06631 implements Runnable {
        C06631() {
        }

        public void run() {
            UploadUnsyncDataService.this.uploadUnsyncData();
        }
    }

    public static void startActionUploadUnsyncDataWithFinishBroadcast(Context context) {
        Intent intent = new Intent(context, UploadUnsyncDataService.class);
        intent.putExtra("broadcast_when_finished", true);
        context.startService(intent);
    }

    public static void startActionUploadUnsyncDataWithoutFinishBroadcast(Context context) {
        Intent intent = new Intent(context, UploadUnsyncDataService.class);
        intent.putExtra("broadcast_when_finished", false);
        context.startService(intent);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            this.mBroadcastWhenFinished = intent.getBooleanExtra("broadcast_when_finished", false);
            Global.getGlobalWorkHandler().post(new C06631());
        }
        return 2;
    }

    private void uploadUnsyncData() {
        Debug.m3d("UploadUnsyncDataService", "upload unsync data");
        this.mSyncManager.registerHistoryUploadListener(this);
    }

    private void uploadFinished(boolean success) {
        Debug.m3d("UploadUnsyncDataService", "upload finished : " + success + ", send broadcast : " + this.mBroadcastWhenFinished);
        Intent broadcastIntent = new Intent();
        broadcastIntent.putExtra("DataSyncAppPkgName", getPackageName());
        if (this.mBroadcastWhenFinished) {
            if (success) {
                broadcastIntent.setAction("com.huami.watch.companion.action.DataSyncAppOK");
            } else {
                broadcastIntent.setAction("com.huami.watch.companion.action.DataSyncAppFail");
            }
        }
        sendBroadcast(broadcastIntent);
        this.mSyncManager.unregisterHistoryUploadListener(this);
        this.mTrackIds.clear();
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void startUploadSportData(long trackId) {
        this.mTrackIds.put(trackId, Boolean.TRUE);
    }

    public void uploadDetailFinished(long trackId, boolean success) {
        Debug.m3d("UploadUnsyncDataService", "upload detail finished. track id " + trackId + ", success " + success);
        if (!success) {
            uploadFinished(false);
        }
    }

    public void uploadSummaryFinished(long trackId, boolean success) {
        Debug.m3d("UploadUnsyncDataService", "upload summary finished. track id " + trackId + ", success " + success);
        if (success) {
            this.mTrackIds.delete(trackId);
            if (this.mTrackIds.size() == 0) {
                uploadFinished(true);
                return;
            }
            return;
        }
        uploadFinished(false);
    }

    public void uploadCancelled() {
        uploadFinished(true);
    }
}
