package com.huami.watch.newsport.syncservice.parser;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.SportStatistic;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StatisticParser {
    public SportStatistic parseJsonToSportStatistic(JSONObject jsonObject) {
        Debug.m6w("StatisticParser", "parse json to sport statistic. response : " + jsonObject);
        if (jsonObject == null) {
            Debug.m6w("StatisticParser", "json is null while parse history json to statistic");
            return null;
        } else if (jsonObject.optInt("code", 0) != 1) {
            Debug.m6w("StatisticParser", "request failed. response : " + jsonObject);
            return null;
        } else {
            JSONArray dataJsonArr = jsonObject.optJSONArray("data");
            if (dataJsonArr == null || dataJsonArr.isNull(0)) {
                Debug.m5i("StatisticParser", "cannot find data json array in " + jsonObject);
                return null;
            }
            JSONObject dataJson = null;
            try {
                dataJson = dataJsonArr.getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (dataJson == null) {
                Debug.m6w("StatisticParser", "data json is null while find data in " + jsonObject);
                return null;
            }
            SportStatistic sportStatistic = new SportStatistic();
            sportStatistic.setTotalCount(dataJson.optInt("count", 0));
            sportStatistic.setTotalCalorie(dataJson.optInt("calorie", 0) * 1000);
            sportStatistic.setTotalDistance(dataJson.optInt("dis", 0));
            return sportStatistic;
        }
    }
}
