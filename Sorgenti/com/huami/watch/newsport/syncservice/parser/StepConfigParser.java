package com.huami.watch.newsport.syncservice.parser;

import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.common.model.StepConfig;
import com.huami.watch.newsport.syncservice.Config;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONException;
import org.json.JSONObject;

public class StepConfigParser {
    public StepConfig parseJsonToStepConfig(JSONObject jsonObject) {
        Debug.m6w("StepConfigParser", "parse json to sport step config. response : " + jsonObject);
        if (jsonObject == null) {
            Debug.m6w("StepConfigParser", "json is null while parse history json to step config");
            return null;
        } else if (jsonObject.optInt("code", 0) != 1) {
            Debug.m6w("StepConfigParser", "request failed. response : " + jsonObject);
            return null;
        } else {
            JSONObject dataJson = jsonObject.optJSONObject("data");
            if (dataJson == null) {
                Debug.m5i("StepConfigParser", "cannot find data json array in " + jsonObject);
                return null;
            }
            Debug.m5i("StepConfigParser", "dataJson:" + dataJson);
            String configStr = dataJson.optString("config", "");
            Debug.m5i("StepConfigParser", "configStr:" + configStr);
            JSONObject configJson = null;
            try {
                configJson = new JSONObject(configStr);
            } catch (JSONException e) {
                e.printStackTrace();
                Debug.m5i("StepConfigParser", "exception has occurred," + e.getLocalizedMessage());
            }
            if (configJson == null) {
                Debug.m5i("StepConfigParser", "cannot find config in " + dataJson);
                return null;
            }
            String learnArray = configJson.optString("indoorLearnArray", "");
            String stepArray = configJson.optString("indoorStepLArray", "");
            Debug.m5i("StepConfigParser", "learn array:" + learnArray + "/n stepArray:" + stepArray);
            StepConfig config = new StepConfig();
            config.setLearnArray(learnArray);
            config.setStepArray(stepArray);
            return config;
        }
    }

    public Map<String, String> parseStepConfigToMap(StepConfig config) {
        Debug.m3d("StepConfigParser", "parse step config config to map " + config);
        if (config == null) {
            Debug.m6w("StepConfigParser", "step config is null while parse trackId to map");
            return null;
        }
        Map<String, String> result = new TreeMap();
        result.put("type", "run");
        result.put("source", Config.SOURCE);
        JSONObject configJson = new JSONObject();
        try {
            configJson.put("indoorLearnArray", "" + config.getLearnArray());
            configJson.put("indoorStepLArray", "" + config.getStepArray());
        } catch (JSONException e) {
            e.printStackTrace();
            Debug.m5i("StepConfigParser", "exception occured, " + e.getLocalizedMessage());
        }
        result.put("config", configJson.toString());
        return result;
    }
}
