package com.huami.watch.newsport.syncservice.parser;

import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.common.ArrayUtils;
import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.PauseInfo;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.gps.utils.SportPointTypeParser;
import com.huami.watch.newsport.syncservice.Config;
import com.huami.watch.newsport.syncservice.Geohash;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONObject;

public class SportDetailParser {
    private static final String JSON_VALUE_SUMMARY_DATA_SOURCE = Config.SOURCE;
    private SportPointTypeParser mPointTypeParser = new SportPointTypeParser();

    public Map<String, String> parseDetailToMap(OutdoorSportSummary summary, List<? extends SportLocationData> locationDatas, List<? extends RunningInfoPerKM> runningInfoPerKMs, List<? extends RunningInfoPerLap> runningInfoPerIndividuals, List<? extends HeartRate> heartRates, List<? extends CyclingDetail> cadenceList, List<? extends DailyPerformanceInfo> dailyPerformanceInfos) {
        long lastTime;
        StringBuilder append;
        String str;
        int i;
        long curTime;
        Map<String, String> result = new TreeMap();
        long trackId = summary.getTrackId();
        List<PauseInfo> pauseInfos = summary.getPauseInfos();
        StringBuilder longLatStringBuilder = new StringBuilder();
        StringBuilder altitudeStringBuilder = new StringBuilder();
        StringBuilder accuracyStringBuilder = new StringBuilder();
        StringBuilder timeStringBuilder = new StringBuilder();
        StringBuilder gaitStringBuilder = new StringBuilder();
        StringBuilder paceStringBuilder = new StringBuilder();
        StringBuilder pauseStringBuilder = new StringBuilder();
        StringBuilder flagStringBuilder = new StringBuilder();
        StringBuilder kiloPaceStringBuilder = new StringBuilder();
        StringBuilder miloPaceStringBuilder = new StringBuilder();
        StringBuilder perLapsStringBuilder = new StringBuilder();
        StringBuilder heartRateStringBuilder = new StringBuilder();
        StringBuilder barStringBuilder = new StringBuilder();
        StringBuilder courseStringBuilder = new StringBuilder();
        StringBuilder pointTypeStringBuilder = new StringBuilder();
        StringBuilder distanceStringBuilder = new StringBuilder();
        StringBuilder speedStringBuilder = new StringBuilder();
        StringBuilder correctAltitudeBuilder = new StringBuilder();
        StringBuilder strokeSpeedBuilder = new StringBuilder();
        StringBuilder cadenceBuilder = new StringBuilder();
        StringBuilder dailyBuilder = new StringBuilder();
        if (!(locationDatas == null || locationDatas.isEmpty())) {
            float pace;
            Iterator iterator = locationDatas.iterator();
            SportLocationData firstLocationData = (SportLocationData) iterator.next();
            barStringBuilder.append(firstLocationData.mTimestamp / 1000).append(",").append(firstLocationData.mBar);
            SportLocationData lastLocationData = firstLocationData;
            long lastLat = (long) (firstLocationData.mLatitude * 1.0E8f);
            long lastLng = (long) (firstLocationData.mLongitude * 1.0E8f);
            lastTime = firstLocationData.mTimestamp / 1000;
            longLatStringBuilder.append((long) (firstLocationData.mLatitude * 1.0E8f)).append(",").append((long) (firstLocationData.mLongitude * 1.0E8f));
            altitudeStringBuilder.append((int) (firstLocationData.mAltitude * 100.0f));
            accuracyStringBuilder.append((int) firstLocationData.mGPSAccuracy);
            timeStringBuilder.append(firstLocationData.mTimestamp / 1000);
            if (firstLocationData.mSpeed == 0.0f) {
                pace = 0.0f;
            } else {
                pace = 1.0f / firstLocationData.mSpeed;
            }
            paceStringBuilder.append(pace);
            courseStringBuilder.append(firstLocationData.mCourse);
            pointTypeStringBuilder.append(firstLocationData.mAlgoPointType);
            int lastFlag = 1;
            int index = 1;
            long lastBarTimestamp = firstLocationData.mTimestamp;
            int lastBar = firstLocationData.mBar;
            while (iterator.hasNext()) {
                SportLocationData locationData = (SportLocationData) iterator.next();
                if (locationData.mBar > 0) {
                    barStringBuilder.append(";").append((locationData.mTimestamp / 1000) - (lastBarTimestamp / 1000)).append(",").append(locationData.mBar - lastBar);
                    lastBar = locationData.mBar;
                    lastBarTimestamp = locationData.mTimestamp;
                }
                long currentLat = (long) (locationData.mLatitude * 1.0E8f);
                long currentLng = (long) (locationData.mLongitude * 1.0E8f);
                longLatStringBuilder.append(";").append(currentLat - lastLat).append(",").append(currentLng - lastLng);
                courseStringBuilder.append(";").append(locationData.mCourse);
                pointTypeStringBuilder.append(";").append(locationData.mAlgoPointType);
                altitudeStringBuilder.append(";").append((int) (locationData.mAltitude * 100.0f));
                accuracyStringBuilder.append(";").append((int) locationData.mGPSAccuracy);
                long currentTime = locationData.mTimestamp / 1000;
                timeStringBuilder.append(";").append(currentTime - lastTime);
                if (locationData.mSpeed == 0.0f) {
                    pace = 0.0f;
                } else {
                    pace = 1.0f / locationData.mSpeed;
                }
                paceStringBuilder.append(";").append(pace);
                lastLat = currentLat;
                lastLng = currentLng;
                lastTime = currentTime;
                if ((locationData.mPointType & 2) != 0) {
                    SportLocationData lastPausedLocationData = lastLocationData;
                    int lastPausedIndex = index - 1;
                    if ((locationData.mPointType & 4) != 0) {
                        lastFlag = 3;
                    } else {
                        lastFlag = 2;
                    }
                } else {
                    lastFlag = 1;
                }
                if (flagStringBuilder.length() == 0) {
                    flagStringBuilder.append(lastFlag);
                } else {
                    flagStringBuilder.append(";").append(lastFlag);
                }
                lastLocationData = locationData;
                index++;
            }
            if (flagStringBuilder.length() != 0) {
                flagStringBuilder.append(";");
            }
            flagStringBuilder.append(lastFlag);
        }
        if (!(pauseInfos == null || pauseInfos.isEmpty())) {
            Debug.m5i("SportDetailParser", "parseDetailToMap, parse info:" + pauseInfos.toString());
            for (PauseInfo info : pauseInfos) {
                if (pauseStringBuilder.length() != 0) {
                    pauseStringBuilder.append(";");
                }
                pauseStringBuilder.append(info.getStart() / 1000).append(",").append((info.getEnd() - info.getStart()) / 1000).append(",").append(-1).append(",").append(-1).append(",").append(info.getPauseType());
            }
        }
        if (runningInfoPerKMs == null || runningInfoPerKMs.isEmpty()) {
            Debug.m5i("SportDetailParser", "cannot find kilo info");
        } else {
            int i2;
            Iterator hrIterator = heartRates.iterator();
            RunningInfoPerKM firstKiloInfo = (RunningInfoPerKM) runningInfoPerKMs.get(0);
            double totalCostTime = (double) firstKiloInfo.getCostTime();
            int totalHeartRate = 0;
            int heartRateCount = 0;
            HeartRate heartRate = null;
            if (hrIterator.hasNext()) {
                heartRate = (HeartRate) hrIterator.next();
            }
            while (heartRate != null && heartRate.getTimestamp() <= firstKiloInfo.getTotalCostTime() + firstKiloInfo.getTrackId()) {
                if (heartRate.getHeartQuality() <= 1) {
                    totalHeartRate += heartRate.getHeartRate();
                    heartRateCount++;
                }
                if (!hrIterator.hasNext()) {
                    break;
                }
                heartRate = (HeartRate) hrIterator.next();
            }
            if (firstKiloInfo.getUnit() == 0) {
                append = kiloPaceStringBuilder.append(0).append(",").append(((int) firstKiloInfo.getCostTime()) / 1000).append(",");
                if (firstKiloInfo.getLongitude() >= 360.0f) {
                    str = "";
                } else {
                    str = Geohash.encode((double) firstKiloInfo.getLatitude(), (double) firstKiloInfo.getLongitude());
                }
                append = append.append(str).append(",").append("-1").append(",");
                if (heartRateCount == 0) {
                    i2 = -1;
                } else {
                    i2 = totalHeartRate / heartRateCount;
                }
                append.append(i2).append(",").append(((int) firstKiloInfo.getTotalCostTime()) / 1000);
            } else {
                miloPaceStringBuilder.append(0).append(",").append(((int) firstKiloInfo.getCostTime()) / 1000).append(",").append(firstKiloInfo.getLongitude() >= 360.0f ? "" : Geohash.encode((double) firstKiloInfo.getLatitude(), (double) firstKiloInfo.getLongitude())).append(",").append("-1").append(",").append(heartRateCount == 0 ? -1 : totalHeartRate / heartRateCount).append(",").append(((int) firstKiloInfo.getTotalCostTime()) / 1000);
            }
            for (i = 1; i < runningInfoPerKMs.size(); i++) {
                RunningInfoPerKM currentKiloInfo = (RunningInfoPerKM) runningInfoPerKMs.get(i);
                totalCostTime += (double) currentKiloInfo.getCostTime();
                totalHeartRate = 0;
                heartRateCount = 0;
                while (heartRate != null && heartRate.getTimestamp() <= currentKiloInfo.getTotalCostTime() + currentKiloInfo.getTrackId()) {
                    if (heartRate.getHeartQuality() <= 1) {
                        totalHeartRate += heartRate.getHeartRate();
                        heartRateCount++;
                    }
                    if (!hrIterator.hasNext()) {
                        break;
                    }
                    heartRate = (HeartRate) hrIterator.next();
                }
                if (firstKiloInfo.getUnit() == 0) {
                    append = kiloPaceStringBuilder.append(";").append(i).append(",").append(((int) currentKiloInfo.getCostTime()) / 1000).append(",");
                    if (currentKiloInfo.getLongitude() >= 360.0f) {
                        str = "";
                    } else {
                        str = Geohash.encode((double) currentKiloInfo.getLatitude(), (double) currentKiloInfo.getLongitude());
                    }
                    append = append.append(str).append(",").append("-1").append(",");
                    if (heartRateCount == 0) {
                        i2 = -1;
                    } else {
                        i2 = totalHeartRate / heartRateCount;
                    }
                    append.append(i2).append(",").append(((int) currentKiloInfo.getTotalCostTime()) / 1000);
                } else {
                    miloPaceStringBuilder.append(";").append(i).append(",").append(((int) currentKiloInfo.getCostTime()) / 1000).append(",").append(currentKiloInfo.getLongitude() >= 360.0f ? "" : Geohash.encode((double) currentKiloInfo.getLatitude(), (double) currentKiloInfo.getLongitude())).append(",").append("-1").append(",").append(heartRateCount == 0 ? -1 : totalHeartRate / heartRateCount).append(",").append(((int) currentKiloInfo.getTotalCostTime()) / 1000);
                }
            }
        }
        if (runningInfoPerIndividuals == null || runningInfoPerIndividuals.isEmpty()) {
            Debug.m5i("SportDetailParser", "cannot find per lap info");
        } else {
            RunningInfoPerLap firstLapInfo = (RunningInfoPerLap) runningInfoPerIndividuals.get(0);
            append = perLapsStringBuilder.append(firstLapInfo.getLapNum()).append(",").append((int) (firstLapInfo.getCostTime() / 1000)).append(",").append((int) firstLapInfo.getDistance()).append(",");
            if (firstLapInfo.getLongitude() >= 360.0f) {
                str = "";
            } else {
                str = Geohash.encode((double) firstLapInfo.getLatitude(), (double) firstLapInfo.getLongitude());
            }
            append.append(str).append(",").append((int) firstLapInfo.getAvgHeart()).append(",").append((int) (firstLapInfo.getTotalCostTime() / 1000)).append(",").append((int) firstLapInfo.getAbsoluteAltitude()).append(",").append((int) firstLapInfo.getClimbUp()).append(",").append((int) firstLapInfo.getClimbDown()).append(",").append(firstLapInfo.getPace()).append(",").append(firstLapInfo.getMaxPace()).append(",").append((int) firstLapInfo.getClimbDistance()).append(",").append(firstLapInfo.getLapStrokeSpeed()).append(",").append(firstLapInfo.getLapStrokes()).append(",").append(firstLapInfo.getLapSwolf()).append(",").append(firstLapInfo.getLapCalories() / 1000).append(",").append((int) (firstLapInfo.getLapStepFreq() * 60.0f)).append(",").append(firstLapInfo.getLapCadence()).append(",").append(firstLapInfo.getLapType());
            for (i = 1; i < runningInfoPerIndividuals.size(); i++) {
                RunningInfoPerLap currentLapInfo = (RunningInfoPerLap) runningInfoPerIndividuals.get(i);
                append = perLapsStringBuilder.append(";").append(currentLapInfo.getLapNum()).append(",").append((int) (currentLapInfo.getCostTime() / 1000)).append(",").append((int) currentLapInfo.getDistance()).append(",");
                if (currentLapInfo.getLongitude() >= 360.0f) {
                    str = "";
                } else {
                    str = Geohash.encode((double) currentLapInfo.getLatitude(), (double) currentLapInfo.getLongitude());
                }
                append.append(str).append(",").append((int) currentLapInfo.getAvgHeart()).append(",").append(((int) currentLapInfo.getTotalCostTime()) / 1000).append(",").append((int) currentLapInfo.getAbsoluteAltitude()).append(",").append((int) currentLapInfo.getClimbUp()).append(",").append((int) currentLapInfo.getClimbDown()).append(",").append(currentLapInfo.getPace()).append(",").append(currentLapInfo.getMaxPace()).append(",").append((int) currentLapInfo.getClimbDistance()).append(",").append(currentLapInfo.getLapStrokeSpeed()).append(",").append(currentLapInfo.getLapStrokes()).append(",").append(currentLapInfo.getLapSwolf()).append(",").append(currentLapInfo.getLapCalories() / 1000).append(",").append((int) (currentLapInfo.getLapStepFreq() * 60.0f)).append(",").append(currentLapInfo.getLapCadence()).append(",").append(currentLapInfo.getLapType());
            }
        }
        if (!(heartRates == null || heartRates.isEmpty())) {
            long lastAvailableTime = trackId / 1000;
            int lastAvailableHeartValue = 0;
            lastTime = trackId / 1000;
            long lastDisDiffTime = trackId / 1000;
            i = 0;
            LogUtil.m9i(Global.DEBUG_LEVEL_3, "SportDetailParser", "rate_start");
            for (HeartRate rate : heartRates) {
                curTime = rate.getTimestamp() / 1000;
                if (rate.getHeartQuality() <= 1) {
                    heartRateStringBuilder.append((int) (curTime - lastAvailableTime)).append(",").append(rate.getHeartRate() - lastAvailableHeartValue).append(";");
                    lastAvailableTime = curTime;
                    lastAvailableHeartValue = rate.getHeartRate();
                }
                int timeDiff = (int) (curTime - lastTime);
                int disDiff = (int) rate.getDistance();
                int timeDisDiff = (int) (curTime - lastDisDiffTime);
                gaitStringBuilder.append(timeDiff).append(",").append(rate.getStepDiff()).append(",").append(rate.getStride()).append(",").append((int) (rate.getStepFreq() * 60.0f)).append(";");
                if (i == 0 || disDiff > 0) {
                    distanceStringBuilder.append(timeDisDiff).append(",").append(disDiff).append(";");
                    lastDisDiffTime = curTime;
                }
                if (15 >= 14) {
                    float speed;
                    if (Float.compare(rate.getPace(), 0.0f) == 0) {
                        speed = 0.0f;
                    } else {
                        speed = NumeriConversionUtils.convertStringToFloat(String.valueOf(1.0f / rate.getPace()), 2);
                    }
                    speedStringBuilder.append(timeDiff).append(",").append(speed).append(";");
                }
                if (15 > 14) {
                    correctAltitudeBuilder.append(timeDiff).append(",").append(rate.getAltitude()).append(";");
                    strokeSpeedBuilder.append(timeDiff).append(",").append(rate.getStrokeSpeed()).append(";");
                }
                lastTime = curTime;
                i++;
            }
            LogUtil.m9i(Global.DEBUG_LEVEL_3, "SportDetailParser", "rate_end");
            if (heartRateStringBuilder.lastIndexOf(";") != -1) {
                heartRateStringBuilder.delete(heartRateStringBuilder.lastIndexOf(";"), heartRateStringBuilder.length());
            }
            if (gaitStringBuilder.lastIndexOf(";") != -1) {
                gaitStringBuilder.delete(gaitStringBuilder.lastIndexOf(";"), gaitStringBuilder.length());
            }
            if (distanceStringBuilder.lastIndexOf(";") != -1) {
                distanceStringBuilder.delete(distanceStringBuilder.lastIndexOf(";"), distanceStringBuilder.length());
            }
            if (speedStringBuilder.lastIndexOf(";") != -1) {
                speedStringBuilder.delete(speedStringBuilder.lastIndexOf(";"), speedStringBuilder.length());
            }
            if (correctAltitudeBuilder.lastIndexOf(";") != -1) {
                correctAltitudeBuilder.delete(correctAltitudeBuilder.lastIndexOf(";"), correctAltitudeBuilder.length());
            }
            if (strokeSpeedBuilder.lastIndexOf(";") != -1) {
                strokeSpeedBuilder.delete(strokeSpeedBuilder.lastIndexOf(";"), strokeSpeedBuilder.length());
            }
        }
        if (!(cadenceList == null || cadenceList.isEmpty())) {
            lastTime = trackId / 1000;
            for (CyclingDetail cyclingDetail : cadenceList) {
                curTime = cyclingDetail.getCurTime() / 1000;
                cadenceBuilder.append((int) (curTime - lastTime)).append(",").append(cyclingDetail.getCurCadence()).append(";");
                lastTime = curTime;
            }
            if (cadenceBuilder.lastIndexOf(";") != -1) {
                cadenceBuilder.delete(cadenceBuilder.lastIndexOf(";"), cadenceBuilder.length());
            }
        }
        if (dailyPerformanceInfos != null) {
            for (DailyPerformanceInfo daily : dailyPerformanceInfos) {
                dailyBuilder.append(daily.getSaveDataType() == 0 ? Integer.valueOf((int) daily.getCurrnetKilos()) : DataFormatUtils.parseFormattedRealNumber(UnitConvertUtils.convertDistanceToKm(daily.getCurrnetKilos() * 1000.0d), false)).append(",").append((int) daily.getDailyPorpermence()).append(";");
            }
            Log.i("SportDetailParser", " upload dailyPerpormanceInfosContent:" + dailyBuilder.toString());
            if (dailyBuilder.lastIndexOf(";") != -1) {
                dailyBuilder.delete(dailyBuilder.lastIndexOf(";"), dailyBuilder.length());
            }
        }
        result.put("version", "" + 15);
        result.put("longitude_latitude", longLatStringBuilder.toString().isEmpty() ? "" : longLatStringBuilder.toString());
        result.put("altitude", altitudeStringBuilder.toString().isEmpty() ? "" : altitudeStringBuilder.toString());
        result.put("accuracy", accuracyStringBuilder.toString().isEmpty() ? "" : accuracyStringBuilder.toString());
        result.put("time", timeStringBuilder.toString().isEmpty() ? "" : timeStringBuilder.toString());
        str = "gait";
        String gaitStr = gaitStringBuilder.toString();
        if (gaitStr.isEmpty()) {
            gaitStr = "";
        }
        result.put(str, gaitStr);
        result.put("pace", paceStringBuilder.toString().isEmpty() ? "" : paceStringBuilder.toString());
        result.put("pause", pauseStringBuilder.toString().isEmpty() ? "" : pauseStringBuilder.toString());
        result.put("kilo_pace", kiloPaceStringBuilder.toString().isEmpty() ? "" : kiloPaceStringBuilder.toString());
        result.put("mile_pace", miloPaceStringBuilder.toString().isEmpty() ? "" : miloPaceStringBuilder.toString());
        result.put("lap", perLapsStringBuilder.toString().isEmpty() ? "" : perLapsStringBuilder.toString());
        result.put("trackid", "" + (trackId / 1000));
        result.put("source", JSON_VALUE_SUMMARY_DATA_SOURCE);
        result.put("heart_rate", heartRateStringBuilder.toString().isEmpty() ? "" : heartRateStringBuilder.toString());
        result.put("distance", distanceStringBuilder.toString().isEmpty() ? "" : distanceStringBuilder.toString());
        result.put("air_pressure_altitude", barStringBuilder.toString().isEmpty() ? "" : barStringBuilder.toString());
        if (15 >= 14) {
            result.put("speed", speedStringBuilder.toString().isEmpty() ? "" : speedStringBuilder.toString());
        }
        if (15 > 14) {
            result.put("correct_altitude", correctAltitudeBuilder.toString().isEmpty() ? "" : correctAltitudeBuilder.toString());
            result.put("stroke_speed", strokeSpeedBuilder.toString().isEmpty() ? "" : strokeSpeedBuilder.toString());
            result.put("cadence", cadenceBuilder.toString().isEmpty() ? "" : cadenceBuilder.toString());
            result.put("daily_performance_info", dailyBuilder.toString().isEmpty() ? "" : dailyBuilder.toString());
            Log.i("SportDetailParser", " upload dailyPropormance info put ");
        }
        if (15 >= 13) {
            Object course = courseStringBuilder.toString();
            str = "course";
            if (course.isEmpty()) {
                course = "";
            }
            result.put(str, course);
            String pointType = pointTypeStringBuilder.toString();
            str = "flag";
            if (pointType.isEmpty()) {
                pointType = "";
            }
            result.put(str, pointType);
        } else {
            result.put("flag", flagStringBuilder.toString().isEmpty() ? "" : flagStringBuilder.toString());
        }
        return result;
    }

    public boolean parseSportDetailData(JSONObject json, List<SportLocationData> outLocationDatas, List<RunningInfoPerKM> outRunningInfoPerKMs, List<RunningInfoPerLap> outRunningInfoPerIndividuals, List<HeartRate> outHeartRates, List<PauseInfo> pauseInfos, List<CyclingDetail> cyclingDetailList, List<DailyPerformanceInfo> dailyPerformanceInfos) {
        Debug.m3d("SportDetailParser", "parseSportDetailData " + json);
        if (json.optInt("code", -1) == -1) {
            Debug.m3d("SportDetailParser", "response code is failed " + json);
            return false;
        }
        JSONObject dataJson = json.optJSONObject("data");
        if (dataJson == null) {
            Debug.m3d("SportDetailParser", "sport detail data is empty " + json);
            return true;
        }
        String dataStr = dataJson.toString();
        if (dataStr != null) {
            Debug.m5i("SportDetailParser", "xxxxxxxxxxxx parse data length xxxxxxxxxxxx");
            Debug.m5i("SportDetailParser", "xxxxxxxxxxxx " + dataStr.length() + " xxxxxxxxxxxx");
            Debug.m5i("SportDetailParser", "xxxxxxxxxxxx parse data length xxxxxxxxxxxx");
        }
        long trackId = dataJson.optLong("trackid", -1);
        if (trackId < 0) {
            Debug.m6w("SportDetailParser", "cannot find track id : " + json);
            return false;
        }
        int i$;
        int len$;
        int i;
        String[] arr$;
        String ps;
        RunningInfoPerKM runningInfoPerKM;
        String kiloLatLngStr;
        double[] latLng;
        long totalCostTime;
        int version = dataJson.optInt("version", 12);
        String latLngStr = dataJson.optString("longitude_latitude", "");
        if ("".equals(latLngStr)) {
            latLngStr = "";
        }
        String altitudeStr = dataJson.optString("altitude", "");
        if ("".equals(altitudeStr)) {
            altitudeStr = "";
        }
        String accuracyStr = dataJson.optString("accuracy", "");
        if ("".equals(accuracyStr)) {
            accuracyStr = "";
        }
        String timeStr = dataJson.optString("time", "");
        if ("".equals(timeStr)) {
            timeStr = "";
        }
        String paceStr = dataJson.optString("pace", "");
        if ("".equals(paceStr)) {
            paceStr = "";
        }
        String pauseStr = dataJson.optString("pause", "");
        if ("".equals(pauseStr)) {
            pauseStr = "";
        }
        String courseStr = dataJson.optString("course", "");
        if ("".equals(courseStr)) {
            courseStr = "";
        }
        String flagStr = dataJson.optString("flag", "");
        if ("".equals(flagStr)) {
            flagStr = "";
        }
        String kiloPace = dataJson.optString("kilo_pace", "");
        if ("".equals(kiloPace)) {
            kiloPace = "";
        }
        String miloPace = dataJson.optString("mile_pace", "");
        if ("".equals(miloPace)) {
            miloPace = "";
        }
        String perLap = dataJson.optString("lap", "");
        if ("".equals(perLap)) {
            perLap = "";
        }
        String heartrate = dataJson.optString("heart_rate", "");
        if ("".equals(heartrate)) {
            heartrate = "";
        }
        String gait = dataJson.optString("gait", "");
        if ("".equals(gait)) {
            gait = "";
        }
        String barStr = dataJson.optString("air_pressure_altitude", "");
        String disStr = dataJson.optString("distance", "");
        String speedStr = dataJson.optString("speed", "");
        String correctAltitudeStr = dataJson.optString("correct_altitude", "");
        String strokeStr = dataJson.optString("stroke_speed", "");
        String cadenceStr = dataJson.optString("cadence", "");
        String dailyStr = dataJson.optString("daily_performance_info", "");
        Debug.m5i("SportDetailParser", "daily:" + dailyStr);
        trackId *= 1000;
        if (latLngStr.isEmpty()) {
            Debug.m6w("SportDetailParser", "latitude and longitude is empty. json " + json);
        } else if (outLocationDatas == null) {
            Debug.m6w("SportDetailParser", "location data is null");
        } else {
            String[] latLngSplitStr = latLngStr.split(";");
            int size = latLngSplitStr.length;
            if (size == 0) {
                Debug.m6w("SportDetailParser", "latitude and longitude size is 0. json " + json);
                return false;
            }
            SportPointTypeParser sportPointTypeParser;
            float[] lats = new float[size];
            float[] lngs = new float[size];
            float currentLat = 0.0f;
            float currentLng = 0.0f;
            int index = 0;
            for (String split : latLngSplitStr) {
                String[] llSplitStr = split.split(",");
                if (llSplitStr.length != 2) {
                    Debug.m6w("SportDetailParser", "cannot split string : " + ArrayUtils.arrayToString(llSplitStr));
                } else {
                    try {
                        currentLat += ((float) Long.parseLong(llSplitStr[0])) / 1.0E8f;
                        currentLng += ((float) Long.parseLong(llSplitStr[1])) / 1.0E8f;
                        lats[index] = currentLat;
                        lngs[index] = currentLng;
                        index++;
                    } catch (NumberFormatException e) {
                        Debug.m6w("SportDetailParser", "cannot parse lat : " + llSplitStr[0] + ", lng : " + llSplitStr[1]);
                        return false;
                    }
                }
            }
            float[] altitudes = new float[size];
            index = 0;
            for (String as : altitudeStr.split(";")) {
                if (as.isEmpty()) {
                    Debug.m6w("SportDetailParser", "altitude is empty. json " + json);
                } else {
                    try {
                        altitudes[index] = Float.parseFloat(as) / 100.0f;
                        index++;
                        if (index >= size) {
                            break;
                        }
                    } catch (NumberFormatException e2) {
                        Debug.m6w("SportDetailParser", "cannot parse altitude : " + as);
                        return false;
                    }
                }
            }
            long[] barTimestamp = null;
            int[] barValue = null;
            String[] barSplitStr = barStr.split(";");
            if (barSplitStr != null && barSplitStr.length > 0) {
                barTimestamp = new long[barSplitStr.length];
                barValue = new int[barSplitStr.length];
                long lastBarTimestamp = 0;
                int lastBarValue = 0;
                for (i = 0; i < barSplitStr.length; i++) {
                    String bar = barSplitStr[i];
                    String[] item = bar.split(",");
                    long barTime = 0;
                    int value = 0;
                    if (item.length < 2) {
                        LogUtil.m10w(true, "SportDetailParser", "parse bar error : " + bar);
                    } else {
                        barTime = Long.parseLong(item[0]) * 1000;
                        value = Integer.parseInt(item[1]);
                    }
                    barTimestamp[i] = lastBarTimestamp + barTime;
                    barValue[i] = lastBarValue + value;
                    lastBarTimestamp = barTimestamp[i];
                    lastBarValue = barValue[i];
                }
            }
            index = 0;
            float[] accuracies = new float[size];
            for (String as2 : accuracyStr.split(";")) {
                if (as2.isEmpty()) {
                    Debug.m6w("SportDetailParser", "accuracy is empty. json " + json);
                } else {
                    try {
                        accuracies[index] = Float.parseFloat(as2);
                        index++;
                        if (index >= size) {
                            break;
                        }
                    } catch (NumberFormatException e3) {
                        Debug.m6w("SportDetailParser", "cannot parse accuracy : " + as2);
                        return false;
                    }
                }
            }
            index = 0;
            long currentTime = 0;
            long[] time = new long[size];
            for (String ts : timeStr.split(";")) {
                if (ts.isEmpty()) {
                    Debug.m6w("SportDetailParser", "time is empty. json " + json);
                } else {
                    try {
                        currentTime += 1000 * Long.parseLong(ts);
                        time[index] = currentTime;
                        index++;
                        if (index >= size) {
                            break;
                        }
                    } catch (NumberFormatException e4) {
                        Debug.m6w("SportDetailParser", "cannot parse diff time : " + ts);
                        return false;
                    }
                }
            }
            index = 0;
            float[] paces = new float[size];
            arr$ = paceStr.split(";");
            len$ = arr$.length;
            i$ = 0;
            while (i$ < len$) {
                ps = arr$[i$];
                try {
                    if (ps.isEmpty()) {
                        Debug.m6w("SportDetailParser", "pace is empty. json " + json);
                    } else {
                        paces[index] = Float.parseFloat(ps);
                        index++;
                        if (index >= size) {
                            break;
                        }
                    }
                    i$++;
                } catch (NumberFormatException e5) {
                    Debug.m6w("SportDetailParser", "cannot parse pace : " + ps);
                    return false;
                }
            }
            String[] courseSplitStr = courseStr.split(";");
            float[] courses = new float[size];
            Arrays.fill(courses, 0.0f);
            index = 0;
            for (String c : courseSplitStr) {
                if (c.isEmpty()) {
                    Debug.m6w("SportDetailParser", "course size is 0. json " + json);
                } else {
                    try {
                        courses[index] = Float.parseFloat(c);
                        index++;
                        if (index >= size) {
                            break;
                        }
                    } catch (NumberFormatException e6) {
                        Debug.m6w("SportDetailParser", "cannot parse course : " + c);
                        return false;
                    }
                }
            }
            String[] flagSplitStr = flagStr.split(";");
            int[] flags = new int[size];
            int[] pointTypes = new int[size];
            Arrays.fill(pointTypes, 0);
            index = 0;
            for (String fs : flagSplitStr) {
                if (fs.isEmpty()) {
                    Debug.m6w("SportDetailParser", "latitude and longitude size is 0. json " + json);
                } else {
                    try {
                        int flag = Integer.parseInt(fs);
                        if (version >= 13) {
                            pointTypes[index] = flag;
                            sportPointTypeParser = this.mPointTypeParser;
                            flag = SportPointTypeParser.parseAlgoPointType(flag);
                        }
                        flags[index] = flag;
                        index++;
                        if (index >= size) {
                            break;
                        }
                    } catch (NumberFormatException e7) {
                        Debug.m6w("SportDetailParser", "cannot parse flag : " + fs);
                        return false;
                    }
                }
            }
            SportLocationData lastLocationData = null;
            int barIndex = 0;
            if (outLocationDatas != null) {
                for (i = 0; i < size; i++) {
                    SportLocationData sportLocationData = new SportLocationData();
                    sportLocationData.mTrackId = trackId;
                    sportLocationData.mLatitude = lats[i];
                    sportLocationData.mLongitude = lngs[i];
                    sportLocationData.mAltitude = altitudes[i];
                    sportLocationData.mSpeed = paces[i] > 0.0f ? 1.0f / paces[i] : 0.0f;
                    sportLocationData.mGPSAccuracy = accuracies[i];
                    sportLocationData.mCourse = courses[i];
                    sportLocationData.mAlgoPointType = pointTypes[i];
                    sportLocationData.mTimestamp = time[i];
                    if (barTimestamp == null || barIndex >= barTimestamp.length || barTimestamp[barIndex] > sportLocationData.mTimestamp) {
                        sportLocationData.mBar = 0;
                    } else {
                        sportLocationData.mBar = barValue[barIndex];
                        while (barIndex < barTimestamp.length && barTimestamp[barIndex] <= sportLocationData.mTimestamp) {
                            barIndex++;
                        }
                    }
                    sportLocationData.mPointType |= 1;
                    if (version > 12) {
                        sportPointTypeParser = this.mPointTypeParser;
                        sportLocationData.mPointType = SportPointTypeParser.parseAlgoPointType(pointTypes[i]);
                    } else if (lastLocationData == null) {
                        sportLocationData.mPointType |= 2;
                    } else if (flags[i - 1] == 2) {
                        sportLocationData.mPointType |= 2;
                        sportLocationData.mIsAutoPause = false;
                    } else if (flags[i - 1] == 3) {
                        sportLocationData.mPointType |= 2;
                        sportLocationData.mPointType |= 4;
                        sportLocationData.mIsAutoPause = true;
                    } else if (flags[i - 1] != 1) {
                        Debug.m6w("SportDetailParser", "invalid flag " + flags[i - 1]);
                        outLocationDatas.clear();
                        outRunningInfoPerKMs.clear();
                        return false;
                    }
                    outLocationDatas.add(sportLocationData);
                    lastLocationData = sportLocationData;
                }
            }
        }
        if (pauseInfos != null) {
            String[] pauseSplitStr = pauseStr.split(";");
            if (pauseSplitStr != null && pauseSplitStr.length > 0) {
                for (String ps2 : pauseSplitStr) {
                    String[] psSplit = ps2.split(",");
                    if (psSplit.length < 5) {
                        Debug.m6w("SportDetailParser", "cannot parse pause info " + ps2);
                    } else {
                        long startTime = Long.parseLong(psSplit[0]) * 1000;
                        pauseInfos.add(new PauseInfo(startTime, startTime + (Long.parseLong(psSplit[1]) * 1000), (byte) Integer.parseInt(psSplit[4])));
                    }
                }
            }
            Debug.m5i("SportDetailParser", "parseSportDetailData, pause info:" + pauseInfos.toString());
        }
        if (outRunningInfoPerKMs == null || kiloPace.isEmpty()) {
            Debug.m6w("SportDetailParser", "running info is null");
        } else {
            outRunningInfoPerKMs.clear();
            for (String ks : kiloPace.split(";")) {
                runningInfoPerKM = new RunningInfoPerKM();
                runningInfoPerKM.setTrackId(trackId);
                String[] kiloPaceInfoStr = ks.split(",");
                if (kiloPaceInfoStr.length >= 2) {
                    try {
                        float second = Float.parseFloat(kiloPaceInfoStr[1]);
                        runningInfoPerKM.setCostTime((long) (1000.0f * second));
                        float pace = second / 1000.0f;
                        runningInfoPerKM.setPace((double) pace);
                        runningInfoPerKM.setSpeed(pace != 0.0f ? (double) (1.0f / pace) : 0.0d);
                        if (kiloPaceInfoStr.length > 2) {
                            kiloLatLngStr = kiloPaceInfoStr[2];
                            if (TextUtils.isEmpty(kiloLatLngStr)) {
                                runningInfoPerKM.setLatitude(10000.0f);
                                runningInfoPerKM.setLongitude(10000.0f);
                            } else {
                                latLng = Geohash.decode(kiloLatLngStr);
                                if (latLng != null && latLng.length == 2) {
                                    runningInfoPerKM.setLatitude((float) latLng[0]);
                                    runningInfoPerKM.setLongitude((float) latLng[1]);
                                }
                            }
                        }
                        if (kiloPaceInfoStr.length > 5) {
                            totalCostTime = Long.parseLong(kiloPaceInfoStr[5], 10);
                            if (totalCostTime != -1) {
                                runningInfoPerKM.setTotalCostTime(1000 * totalCostTime);
                            }
                        }
                        outRunningInfoPerKMs.add(runningInfoPerKM);
                    } catch (NumberFormatException e8) {
                        Debug.m6w("SportDetailParser", "cannot parse kilo pace : " + ks);
                        if (outLocationDatas != null) {
                            outLocationDatas.clear();
                        }
                        outRunningInfoPerKMs.clear();
                        return false;
                    }
                }
            }
        }
        if (outRunningInfoPerKMs == null || miloPace.isEmpty()) {
            Debug.m6w("SportDetailParser", "running info is null");
        } else {
            outRunningInfoPerKMs.clear();
            for (String ks2 : miloPace.split(";")) {
                runningInfoPerKM = new RunningInfoPerKM();
                runningInfoPerKM.setTrackId(trackId);
                runningInfoPerKM.setUnit(1);
                String[] miloPaceInfoStr = ks2.split(",");
                if (miloPaceInfoStr.length >= 2) {
                    second = Float.parseFloat(miloPaceInfoStr[1]);
                    runningInfoPerKM.setCostTime((long) ((int) (1000.0f * second)));
                    pace = second / 1000.0f;
                    runningInfoPerKM.setPace((double) pace);
                    runningInfoPerKM.setSpeed(pace != 0.0f ? (double) (1.0f / pace) : 0.0d);
                    if (miloPaceInfoStr.length > 2) {
                        kiloLatLngStr = miloPaceInfoStr[2];
                        if (TextUtils.isEmpty(kiloLatLngStr)) {
                            runningInfoPerKM.setLatitude(10000.0f);
                            runningInfoPerKM.setLongitude(10000.0f);
                        } else {
                            try {
                                latLng = Geohash.decode(kiloLatLngStr);
                                if (latLng != null && latLng.length == 2) {
                                    runningInfoPerKM.setLatitude((float) latLng[0]);
                                    runningInfoPerKM.setLongitude((float) latLng[1]);
                                }
                            } catch (NumberFormatException e9) {
                                Debug.m6w("SportDetailParser", "cannot parse kilo pace : " + ks2);
                                if (outLocationDatas != null) {
                                    outLocationDatas.clear();
                                }
                                outRunningInfoPerKMs.clear();
                                return false;
                            }
                        }
                    }
                    if (miloPaceInfoStr.length > 5) {
                        totalCostTime = Long.parseLong(miloPaceInfoStr[5], 10);
                        if (totalCostTime != -1) {
                            runningInfoPerKM.setTotalCostTime(1000 * totalCostTime);
                        }
                    }
                    outRunningInfoPerKMs.add(runningInfoPerKM);
                }
            }
        }
        if (outRunningInfoPerIndividuals == null || perLap.isEmpty()) {
            Debug.m6w("SportDetailParser", "running per lap info is null");
        } else {
            outRunningInfoPerIndividuals.clear();
            for (String lap : perLap.split(";")) {
                RunningInfoPerLap runningInfoPerIndividual = new RunningInfoPerLap();
                runningInfoPerIndividual.setTrackId(trackId);
                String[] perLapInfoStr = lap.split(",");
                if (perLapInfoStr.length >= 9) {
                    try {
                        int lapNum = Integer.parseInt(perLapInfoStr[0]);
                        second = Float.parseFloat(perLapInfoStr[1]);
                        float distance = Float.parseFloat(perLapInfoStr[2]);
                        latLng = Geohash.decode(perLapInfoStr[3]);
                        if (latLng != null && latLng.length == 2) {
                            runningInfoPerIndividual.setLatitude((float) latLng[0]);
                            runningInfoPerIndividual.setLongitude((float) latLng[1]);
                        }
                        float avgHeart = Float.parseFloat(perLapInfoStr[4]);
                        float totalCostTime2 = Float.parseFloat(perLapInfoStr[5]);
                        float absoluteAltitude = Float.parseFloat(perLapInfoStr[6]);
                        float climbUp = Float.parseFloat(perLapInfoStr[7]);
                        float climbDown = Float.parseFloat(perLapInfoStr[8]);
                        pace = second / distance;
                        if (perLapInfoStr.length >= 19) {
                            pace = Float.parseFloat(perLapInfoStr[9]);
                            float maxPace = Float.parseFloat(perLapInfoStr[10]);
                            int climbDis = Integer.parseInt(perLapInfoStr[11]);
                            float avgStrokeSpeed = Float.parseFloat(perLapInfoStr[12]);
                            int strokes = Integer.parseInt(perLapInfoStr[13]);
                            int swolf = Integer.parseInt(perLapInfoStr[14]);
                            int calorie = Integer.parseInt(perLapInfoStr[15]) * 1000;
                            float avgStepFreq = ((float) Integer.parseInt(perLapInfoStr[16])) / 60.0f;
                            int avgCadence = Integer.parseInt(perLapInfoStr[17]);
                            int lapType = Integer.parseInt(perLapInfoStr[18]);
                            runningInfoPerIndividual.setMaxPace((double) maxPace);
                            runningInfoPerIndividual.setLapStrokeSpeed(avgStrokeSpeed);
                            runningInfoPerIndividual.setClimbDistance((float) climbDis);
                            runningInfoPerIndividual.setLapStrokes(strokes);
                            runningInfoPerIndividual.setLapSwolf(swolf);
                            runningInfoPerIndividual.setLapCalories(calorie);
                            runningInfoPerIndividual.setLapStepFreq(avgStepFreq);
                            runningInfoPerIndividual.setLapCadence(avgCadence);
                            runningInfoPerIndividual.setLapType(lapType);
                        }
                        runningInfoPerIndividual.setCostTime((long) (1000.0f * second));
                        runningInfoPerIndividual.setPace((double) pace);
                        runningInfoPerIndividual.setDistance(distance);
                        runningInfoPerIndividual.setLapNum(lapNum);
                        runningInfoPerIndividual.setAvgHeart(avgHeart);
                        runningInfoPerIndividual.setTotalCostTime((long) (1000.0f * totalCostTime2));
                        runningInfoPerIndividual.setAbsoluteAltitude(absoluteAltitude);
                        runningInfoPerIndividual.setClimbUp(climbUp);
                        runningInfoPerIndividual.setClimbDown(climbDown);
                        outRunningInfoPerIndividuals.add(runningInfoPerIndividual);
                    } catch (NumberFormatException e10) {
                        Debug.m6w("SportDetailParser", "cannot parse per lap : " + lap);
                        if (outLocationDatas != null) {
                            outLocationDatas.clear();
                        }
                        outRunningInfoPerIndividuals.clear();
                        return false;
                    }
                }
            }
        }
        long[] disTimestamps = null;
        int[] disValue = null;
        long lastDistanceTimestamp = trackId;
        if (disStr.isEmpty()) {
            Debug.m5i("SportDetailParser", "dis is empty " + disStr);
        } else {
            String[] disSplitStr = disStr.split(";");
            if (disSplitStr != null && disSplitStr.length != 0) {
                disTimestamps = new long[disSplitStr.length];
                disValue = new int[disSplitStr.length];
                i = 0;
                while (i < disSplitStr.length) {
                    String[] distanceStr = disSplitStr[i].split(",");
                    if (distanceStr.length < 2) {
                        Debug.m5i("SportDetailParser", "distance info is not complete! " + Arrays.toString(distanceStr));
                        disTimestamps = null;
                        disValue = null;
                        break;
                    }
                    try {
                        disTimestamps[i] = (Long.parseLong(distanceStr[0]) * 1000) + lastDistanceTimestamp;
                        disValue[i] = Integer.parseInt(distanceStr[1]);
                        lastDistanceTimestamp = disTimestamps[i];
                        i++;
                    } catch (NumberFormatException e11) {
                        disTimestamps = null;
                        disValue = null;
                    }
                }
            }
        }
        long[] heartTimes = null;
        int[] heartValues = null;
        long lastTimestamp;
        if (!heartrate.isEmpty()) {
            String[] heartrateSplitStr = heartrate.split(";");
            lastTimestamp = trackId;
            int lastHeartRate = 0;
            i = 0;
            heartTimes = new long[heartrateSplitStr.length];
            heartValues = new int[heartrateSplitStr.length];
            arr$ = heartrateSplitStr;
            len$ = arr$.length;
            i$ = 0;
            while (i$ < len$) {
                String hss = arr$[i$];
                String[] heartrateInfoStr = hss.split(",");
                if (heartrateInfoStr.length < 2) {
                    Debug.m3d("SportDetailParser", "heart rate format incorrect! heart rate string " + hss);
                    i++;
                    break;
                }
                try {
                    heartTimes[i] = (Long.parseLong(heartrateInfoStr[0]) * 1000) + lastTimestamp;
                    heartValues[i] = Integer.parseInt(heartrateInfoStr[1]) + lastHeartRate;
                    lastTimestamp = heartTimes[i];
                    lastHeartRate = heartValues[i];
                    i++;
                    i$++;
                } catch (NumberFormatException e12) {
                    Debug.m6w("SportDetailParser", "cannot parse heart info : " + hss);
                    return false;
                }
            }
        }
        long[] speedTimeStamps = null;
        float[] speedArrays = null;
        String[] speedSplitStr = speedStr.split(";");
        if (speedSplitStr != null && speedSplitStr.length > 0) {
            speedTimeStamps = new long[speedSplitStr.length];
            speedArrays = new float[speedSplitStr.length];
            long lastSpeedTimeStamp = trackId;
            i = 0;
            while (i < speedSplitStr.length) {
                String[] singleSpeedInfo = speedSplitStr[i].split(",");
                if (singleSpeedInfo.length < 2) {
                    Debug.m5i("SportDetailParser", "speed info is not complete! " + Arrays.toString(singleSpeedInfo));
                    break;
                }
                try {
                    speedTimeStamps[i] = ((long) (Integer.parseInt(singleSpeedInfo[0]) * 1000)) + lastSpeedTimeStamp;
                    speedArrays[i] = Float.parseFloat(singleSpeedInfo[1]);
                    lastSpeedTimeStamp = speedTimeStamps[i];
                    i++;
                } catch (NumberFormatException e13) {
                    e13.printStackTrace();
                    if (outHeartRates != null) {
                        outHeartRates.clear();
                    }
                }
            }
        }
        long[] correctAltitudeTimeStamps = null;
        int[] correctAltitudeArrays = null;
        String[] correctAltitudeSplitStr = correctAltitudeStr.split(";");
        if (correctAltitudeSplitStr != null && correctAltitudeSplitStr.length > 0) {
            correctAltitudeTimeStamps = new long[correctAltitudeSplitStr.length];
            correctAltitudeArrays = new int[correctAltitudeSplitStr.length];
            long lastAltitudeTimeStamp = trackId;
            i = 0;
            while (i < correctAltitudeSplitStr.length) {
                String[] singleAltitudeInfo = correctAltitudeSplitStr[i].split(",");
                if (singleAltitudeInfo.length < 2) {
                    Debug.m5i("SportDetailParser", "correct altitude info is not complete! " + Arrays.toString(singleAltitudeInfo));
                    break;
                }
                try {
                    correctAltitudeTimeStamps[i] = ((long) (Integer.parseInt(singleAltitudeInfo[0]) * 1000)) + lastAltitudeTimeStamp;
                    correctAltitudeArrays[i] = Integer.parseInt(singleAltitudeInfo[1]);
                    lastAltitudeTimeStamp = correctAltitudeTimeStamps[i];
                    i++;
                } catch (NumberFormatException e132) {
                    e132.printStackTrace();
                    if (outHeartRates != null) {
                        outHeartRates.clear();
                    }
                }
            }
        }
        long[] strokeTimeStamps = null;
        float[] strokeArrays = null;
        String[] strokeSplitStr = strokeStr.split(";");
        if (strokeSplitStr != null && strokeSplitStr.length > 0) {
            strokeTimeStamps = new long[strokeSplitStr.length];
            strokeArrays = new float[strokeSplitStr.length];
            long lastStrokeTimeStamp = trackId;
            i = 0;
            while (i < strokeSplitStr.length) {
                String[] singleStrokeInfo = strokeSplitStr[i].split(",");
                if (singleStrokeInfo.length < 2) {
                    Debug.m5i("SportDetailParser", "correct altitude info is not complete! " + Arrays.toString(singleStrokeInfo));
                    break;
                }
                try {
                    strokeTimeStamps[i] = ((long) (Integer.parseInt(singleStrokeInfo[0]) * 1000)) + lastStrokeTimeStamp;
                    strokeArrays[i] = Float.parseFloat(singleStrokeInfo[1]);
                    lastStrokeTimeStamp = strokeTimeStamps[i];
                    i++;
                } catch (NumberFormatException e1322) {
                    e1322.printStackTrace();
                    if (outHeartRates != null) {
                        outHeartRates.clear();
                    }
                }
            }
        }
        if (outHeartRates != null) {
            outHeartRates.clear();
            if (gait.isEmpty()) {
                Debug.m5i("SportDetailParser", "gait is empty " + gait);
            } else {
                lastTimestamp = trackId;
                String[] gaitSplitStr = gait.split(";");
                int disIndex = 0;
                int disLength = 0;
                if (disTimestamps != null) {
                    disLength = disTimestamps.length;
                }
                long curDisTimeStamps = -1;
                int curDistance = -1;
                if (0 < disLength) {
                    curDisTimeStamps = disTimestamps[0];
                    curDistance = disValue[0];
                }
                int heartRateIndex = 0;
                int heartRateLength = 0;
                if (heartTimes != null) {
                    heartRateLength = heartTimes.length;
                }
                long curHeartRateTimeStamps = -1;
                int curHeartRate = -1;
                if (0 < heartRateLength) {
                    curHeartRateTimeStamps = heartTimes[0];
                    curHeartRate = heartValues[0];
                }
                int speedIndex = 0;
                int speedLength = 0;
                if (speedTimeStamps != null) {
                    speedLength = speedTimeStamps.length;
                }
                long curSpeedTimeStamp = -1;
                float curSpeed = -1.0f;
                if (0 < speedLength) {
                    curSpeedTimeStamp = speedTimeStamps[0];
                    curSpeed = speedArrays[0];
                }
                int correctAltitudeIndex = 0;
                int correctAltitudeLength = 0;
                if (correctAltitudeTimeStamps != null) {
                    correctAltitudeLength = correctAltitudeTimeStamps.length;
                }
                long curCorrectAltitudeTimeStamp = -1;
                int curCorrectAltitude = -1;
                if (0 < correctAltitudeLength) {
                    curCorrectAltitudeTimeStamp = correctAltitudeTimeStamps[0];
                    curCorrectAltitude = correctAltitudeArrays[0];
                }
                int strokeIndex = 0;
                int strokeLength = 0;
                if (strokeTimeStamps != null) {
                    strokeLength = strokeTimeStamps.length;
                }
                long curStrokeTimeStamp = -1;
                float curStroke = -1.0f;
                if (0 < strokeLength) {
                    curStrokeTimeStamp = strokeTimeStamps[0];
                    curStroke = strokeArrays[0];
                }
                if (gaitSplitStr != null && gaitSplitStr.length != 0) {
                    i = 0;
                    while (i < gaitSplitStr.length) {
                        String[] gaitStr = gaitSplitStr[i].split(",");
                        if (gaitStr.length < 3) {
                            Debug.m5i("SportDetailParser", "gait info is not complete! " + Arrays.toString(gaitStr));
                            break;
                        }
                        try {
                            long timeStamp = lastTimestamp + ((long) (Integer.parseInt(gaitStr[0]) * 1000));
                            int stepDiff = Integer.parseInt(gaitStr[1]);
                            int stride = Integer.parseInt(gaitStr[2]);
                            int stepFreq = 0;
                            if (gaitStr.length > 3) {
                                stepFreq = Integer.parseInt(gaitStr[3]);
                            }
                            HeartRate heartRate = new HeartRate();
                            heartRate.setTrackId(trackId);
                            heartRate.setTimestamp(timeStamp);
                            heartRate.setRunTime((int) (timeStamp - trackId));
                            heartRate.setStepDiff(stepDiff);
                            heartRate.setStride(stride);
                            heartRate.setStepFreq(((float) stepFreq) / 60.0f);
                            if (timeStamp == curDisTimeStamps) {
                                heartRate.setDistance((float) curDistance);
                                disIndex++;
                                if (disIndex < disLength) {
                                    curDisTimeStamps = disTimestamps[disIndex];
                                    curDistance = disValue[disIndex];
                                }
                            }
                            if (timeStamp == curHeartRateTimeStamps) {
                                heartRate.setHeartRate(curHeartRate);
                                heartRateIndex++;
                                if (heartRateIndex < heartRateLength) {
                                    curHeartRateTimeStamps = heartTimes[heartRateIndex];
                                    curHeartRate = heartValues[heartRateIndex];
                                }
                            } else {
                                heartRate.setHeartQuality(20000);
                            }
                            if (timeStamp == curSpeedTimeStamp) {
                                if (Float.compare(curSpeed, 0.0f) == 0) {
                                    pace = 0.0f;
                                } else {
                                    pace = 1.0f / curSpeed;
                                }
                                heartRate.setPace(pace);
                                speedIndex++;
                                if (speedIndex < speedLength) {
                                    curSpeedTimeStamp = speedTimeStamps[speedIndex];
                                    curSpeed = speedArrays[speedIndex];
                                }
                            }
                            if (timeStamp == curCorrectAltitudeTimeStamp) {
                                heartRate.setAltitude(curCorrectAltitude);
                                correctAltitudeIndex++;
                                if (correctAltitudeIndex < correctAltitudeLength) {
                                    curCorrectAltitudeTimeStamp = correctAltitudeTimeStamps[correctAltitudeIndex];
                                    curCorrectAltitude = correctAltitudeArrays[correctAltitudeIndex];
                                }
                            }
                            if (timeStamp == curStrokeTimeStamp) {
                                heartRate.setStrokeSpeed(curStroke);
                                strokeIndex++;
                                if (strokeIndex < strokeLength) {
                                    curStrokeTimeStamp = strokeTimeStamps[strokeIndex];
                                    curStroke = strokeArrays[strokeIndex];
                                }
                            }
                            outHeartRates.add(heartRate);
                            lastTimestamp = timeStamp;
                            i++;
                        } catch (NumberFormatException e13222) {
                            e13222.printStackTrace();
                            if (outHeartRates != null) {
                                outHeartRates.clear();
                            }
                        }
                    }
                }
            }
        } else {
            Debug.m6w("SportDetailParser", "heart rate is null");
        }
        if (cyclingDetailList == null || cadenceStr.isEmpty()) {
            Debug.m6w("SportDetailParser", "cadence info is null");
        } else {
            cyclingDetailList.clear();
            long lastTimeStamps = trackId;
            for (String ks22 : cadenceStr.split(";")) {
                CyclingDetail cyclingDetail = new CyclingDetail();
                cyclingDetail.setTrackId(trackId);
                String[] cadenceInfoStr = ks22.split(",");
                if (cadenceInfoStr.length >= 2) {
                    try {
                        long timeStamps = lastTimeStamps + (((long) Integer.parseInt(cadenceInfoStr[0])) * 1000);
                        int cadence = Integer.parseInt(cadenceInfoStr[1]);
                        cyclingDetail.setCurTime(timeStamps);
                        cyclingDetail.setCurCadence(cadence);
                        cyclingDetailList.add(cyclingDetail);
                        lastTimeStamps = timeStamps;
                    } catch (NumberFormatException e14) {
                        Debug.m6w("SportDetailParser", "cannot parse cadence info : " + ks22);
                        if (cyclingDetailList != null) {
                            cyclingDetailList.clear();
                        }
                        cyclingDetailList.clear();
                        return false;
                    }
                }
            }
        }
        if (dailyPerformanceInfos == null || dailyStr.isEmpty()) {
            Debug.m6w("SportDetailParser", "daily info is null");
        } else {
            dailyPerformanceInfos.clear();
            String[] dailySplitStr = dailyStr.split(";");
            for (i = 0; i < dailySplitStr.length; i++) {
                String ds = dailySplitStr[i];
                DailyPerformanceInfo daily = new DailyPerformanceInfo();
                daily.setmTrackId(trackId);
                String[] dailyInfoStr = ds.split(",");
                if (dailyInfoStr.length >= 2) {
                    float dis = Float.parseFloat(dailyInfoStr[0]);
                    daily.setDailyPorpermence((float) Integer.parseInt(dailyInfoStr[1]));
                    if (i != dailyInfoStr.length - 1) {
                        daily.setSaveDataType(0);
                    } else {
                        try {
                            daily.setSaveDataType(1);
                        } catch (NumberFormatException e15) {
                            Debug.m6w("SportDetailParser", "cannot parse daily info : " + ds);
                            if (dailyPerformanceInfos != null) {
                                dailyPerformanceInfos.clear();
                            }
                            dailyPerformanceInfos.clear();
                            return false;
                        }
                    }
                    daily.setCurrnetKilos(dis);
                    dailyPerformanceInfos.add(daily);
                }
            }
        }
        return true;
    }
}
