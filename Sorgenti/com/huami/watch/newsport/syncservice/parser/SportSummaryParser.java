package com.huami.watch.newsport.syncservice.parser;

import com.huami.watch.common.DataFormatUtils;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.syncservice.Config;
import com.huami.watch.newsport.syncservice.Geohash;
import com.huami.watch.newsport.utils.DateUtils;
import com.huami.watch.newsport.utils.NumeriConversionUtils;
import com.huami.watch.newsport.utils.UnitConvertUtils;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class SportSummaryParser {
    private static final String JSON_VALUE_SUMMARY_DATA_SOURCE = Config.SOURCE;

    public Map<String, String> parseTrackIdToMap(OutdoorSportSummary summary) {
        Debug.m3d("SportSummaryParser", "parse trackId to map " + summary);
        if (summary == null) {
            Debug.m6w("SportSummaryParser", "summary is null while parse trackId to map");
            return null;
        }
        Map<String, String> result = new TreeMap();
        result.put("trackid", "" + (summary.getTrackId() / 1000));
        return result;
    }

    public Map<String, String> parseSummaryToMap(OutdoorSportSummary summary) {
        Debug.m3d("SportSummaryParser", "parse summary to map " + summary);
        if (summary == null) {
            Debug.m6w("SportSummaryParser", "summary is null while parse summary to map");
            return null;
        }
        Map<String, String> result = new TreeMap();
        result.put("trackid", "" + (summary.getTrackId() / 1000));
        result.put("source", JSON_VALUE_SUMMARY_DATA_SOURCE);
        if (summary.getSportType() == 14 || summary.getSportType() == 15 || summary.getSportType() == 1015 || summary.getSportType() == 2001) {
            result.put("dis", "" + ((int) summary.getDistance()));
        } else {
            result.put("dis", "" + ((int) summary.getDistance()));
        }
        result.put("calorie", "" + (summary.getCalorie() / 1000.0f));
        result.put("end_time", "" + (summary.getEndTime() / 1000));
        result.put("run_time", "" + (summary.getSportDuration() / 1000));
        result.put("avg_pace", "" + summary.getTotalPace());
        result.put("avg_frequency", "" + UnitConvertUtils.convertStepFreqToTimesPerMin((double) summary.getStepFreq()));
        result.put("avg_heart_rate", "" + summary.getAvgHeartRate());
        int sportType = summary.getSportType();
        if (SportType.isSportTypeValid(sportType)) {
            result.put("total_step", "" + summary.getStepCount());
            result.put("avg_stride_length", "" + ((int) (summary.getAvgStride() * 100.0f)));
            result.put("type", "" + sportType);
            result.put("location", summary.isLocationValid() ? Geohash.encode(summary.getLatitude(), summary.getLongitude()) : "");
            result.put("city", "");
            result.put("forefoot_ratio", "-1");
            result.put("max_pace", "" + (summary.getBestPace() >= 0.0f ? summary.getBestPace() : 0.0f));
            result.put("min_pace", "" + (summary.getMinPace() >= 0.0f ? summary.getMinPace() : 0.0f));
            result.put("version", "15");
            result.put("max_frequency", "" + UnitConvertUtils.convertStepFreqToTimesPerMin((double) summary.getMaxStepFreq()));
            result.put("altitude_ascend", "" + (summary.getClimbUp() < 0.0f ? -1 : Math.round(summary.getClimbUp())));
            result.put("altitude_descend", "" + (summary.getClimbDown() < 0.0f ? -1 : Math.round(summary.getClimbDown())));
            result.put("max_altitude", "" + Math.round(summary.getHighestAltitude()));
            result.put("min_altitude", "" + Math.round(summary.getLowestAltitude()));
            result.put("lap_distance", "" + ((int) summary.getDistancePerLap()));
            result.put("distance_ascend", "" + (summary.getClimbdisascend() < 0.0f ? -1 : (int) summary.getClimbdisascend()));
            if (summary.getDeviceType() < 0) {
                result.put("bind_device", "");
                result.put("avg_cadence", "-1");
                result.put("max_cadence", "-1");
            } else {
                result.put("bind_device", summary.getDeviceType() + ":" + "CADENCE" + ":" + summary.getDeviceBrand());
                result.put("avg_cadence", "" + ((int) summary.getAveCadence()));
                result.put("max_cadence", "" + ((int) summary.getMaxCadence()));
            }
            result.put("climb_dis_descend", "" + (summary.getClimbdisDescend() < 0.0f ? -1 : (int) summary.getClimbdisDescend()));
            result.put("climb_dis_ascend_time", "" + (summary.getAscendTime() < 0 ? -1 : summary.getAscendTime() / 1000));
            result.put("climb_dis_descend_time", "" + ((summary.getDescendTime() < 0 ? -1 : summary.getDescendTime()) / 1000));
            Debug.m5i("test_summary", "childList:" + summary.parseChildListToJson());
            result.put("child_list", summary.parseChildListToJson());
            result.put("parent_trackid", "" + (summary.getParentTrackId() == -1 ? -1 : summary.getParentTrackId() / 1000));
            result.put("max_heart_rate", "" + summary.getMaxHeartRate());
            result.put("min_heart_rate", "" + summary.getMinHeartRate());
            result.put("swolf", "" + summary.getSwolfPerFixedMeters());
            result.put("total_strokes", "" + summary.getTotalStrokes());
            result.put("total_trips", "" + summary.getTotalTrips());
            result.put("avg_stroke_speed", "" + summary.getAvgStrokeSpeed());
            result.put("max_stroke_speed", "" + summary.getMaxStrokeSpeed());
            result.put("avg_distance_per_stroke", "" + summary.getAvgDistancePerStroke());
            result.put("swim_pool_length", "" + summary.getSwimPoolLength());
            result.put("te", "" + summary.getTE());
            result.put("swim_style", "" + summary.getSwimStyle());
            result.put("unit", "" + summary.getUnit());
            result.put("sport_mode", "" + summary.getIntervalType());
            result.put("downhill_num", "" + summary.getDownHillNum());
            result.put("downhill_max_altitude_desend", "" + Math.round(summary.getDownhillMaxAltitudeDescend()));
            result.put("second_half_start_time", "" + (summary.getSecHalfStartTime() / 1000));
            result.put("strokes", "" + summary.getmStrokes());
            result.put("fore_hand", "" + summary.getmForeHand());
            result.put("back_hand", "" + summary.getmBackHand());
            result.put("serve", "" + summary.getmServe());
            return result;
        }
        throw new IllegalStateException("Illegal sport type " + sportType);
    }

    public List<OutdoorSportSummary> parseHistoryJsonToSummaries(JSONObject json) {
        List<OutdoorSportSummary> list = null;
        Debug.m6w("SportSummaryParser", "parse json to summaries. response : " + json);
        if (json == null) {
            Debug.m6w("SportSummaryParser", "json is null while parse history json to sumnaries");
        } else if (json.optInt("code", 0) != 1) {
            Debug.m6w("SportSummaryParser", "request failed. response : " + json);
        } else {
            JSONObject dataJson = json.optJSONObject("data");
            if (dataJson == null) {
                Debug.m6w("SportSummaryParser", "data json is null while find data in " + json);
            } else {
                list = new LinkedList();
                JSONArray summaries = dataJson.optJSONArray("summary");
                if (summaries == null) {
                    Debug.m6w("SportSummaryParser", "cannot found any summary : " + json);
                } else {
                    int length = summaries.length();
                    for (int i = 0; i < length; i++) {
                        OutdoorSportSummary sportSummary = parseSingleJson2Summary(summaries.optJSONObject(i));
                        if (sportSummary != null) {
                            list.add(sportSummary);
                        }
                    }
                    updateTodayDistance(list);
                }
            }
        }
        return list;
    }

    public OutdoorSportSummary parseChildJsonToSummary(JSONObject json) {
        Debug.m6w("SportSummaryParser", "parse json to summary. response : " + json);
        if (json == null) {
            Debug.m6w("SportSummaryParser", "json is null while parse history json to summary");
            return null;
        } else if (json.optInt("code", 0) != 1) {
            Debug.m6w("SportSummaryParser", "request failed. response : " + json);
            return null;
        } else {
            JSONObject dataJson = json.optJSONObject("data");
            if (dataJson != null) {
                return parseSingleJson2Summary(dataJson);
            }
            Debug.m6w("SportSummaryParser", "data json is null while find data in " + json);
            return null;
        }
    }

    private OutdoorSportSummary parseSingleJson2Summary(JSONObject jsonObject) {
        int sportType = jsonObject.optInt("type", -1);
        OutdoorSportSummary outdoorSportSummary = null;
        if (SportType.isSportTypeValid(sportType)) {
            outdoorSportSummary = OutdoorSportSummary.createOutdoorSportSummary(sportType);
        }
        if (outdoorSportSummary == null) {
            Debug.m6w("SportSummaryParser", "unknown sport type : " + jsonObject + ", type:" + sportType);
            return null;
        }
        long trackId = jsonObject.optLong("trackid", -1);
        if (trackId == -1) {
            Debug.m6w("SportSummaryParser", "invalid track id " + jsonObject);
            return null;
        }
        trackId *= 1000;
        outdoorSportSummary.setTrackId(trackId);
        outdoorSportSummary.setVersion(jsonObject.optInt("version", 12));
        float distance = (float) jsonObject.optDouble("dis", -1.0d);
        if (distance == -1.0f) {
            Debug.m6w("SportSummaryParser", "invalid distance " + jsonObject);
        } else {
            outdoorSportSummary.setDistance(distance);
        }
        if (SportType.isMixedSport(sportType)) {
            outdoorSportSummary.setStartTime(1000 + trackId);
        } else {
            outdoorSportSummary.setStartTime(trackId);
        }
        long endTime = jsonObject.optLong("end_time", -1);
        if (endTime == -1) {
            Debug.m6w("SportSummaryParser", "invalid end time " + jsonObject);
        } else {
            endTime *= 1000;
            outdoorSportSummary.setEndTime(endTime);
        }
        float calorie = (float) (jsonObject.optDouble("calorie", -1.0d) * 1000.0d);
        if (calorie == -1.0f) {
            Debug.m6w("SportSummaryParser", "invalid calorie " + jsonObject);
        } else {
            outdoorSportSummary.setCalorie(calorie);
        }
        int sportDuration = jsonObject.optInt("run_time", -1);
        if (sportDuration == -1) {
            Debug.m6w("SportSummaryParser", "invalid sport duration " + jsonObject);
            return null;
        }
        float speed;
        float maxSpeed;
        sportDuration *= 1000;
        outdoorSportSummary.setSportDuration(sportDuration);
        outdoorSportSummary.setTotalPausedTime((int) ((endTime - trackId) - ((long) sportDuration)));
        double[] latlng = Geohash.decode(jsonObject.optString("location"));
        if (latlng == null || latlng.length != 2) {
            Debug.m6w("SportSummaryParser", "cannot decode location : " + jsonObject);
        } else {
            outdoorSportSummary.setLatitude(latlng[0]);
            outdoorSportSummary.setLongitude(latlng[1]);
        }
        outdoorSportSummary.setStepFreq(((float) jsonObject.optDouble("avg_frequency")) / 60.0f);
        outdoorSportSummary.setStepCount(jsonObject.optInt("total_step", 0));
        outdoorSportSummary.setAvgHeartRate(jsonObject.optInt("avg_heart_rate"));
        float pace = (float) jsonObject.optDouble("avg_pace");
        outdoorSportSummary.setPace(pace);
        outdoorSportSummary.setTotalPace(pace);
        if (pace == 0.0f) {
            speed = 0.0f;
        } else {
            speed = 1.0f / pace;
        }
        outdoorSportSummary.setSpeed(speed);
        outdoorSportSummary.setTotalSpeed(speed);
        float maxPace = (float) jsonObject.optDouble("max_pace", 0.0d);
        outdoorSportSummary.setBestPace(maxPace);
        outdoorSportSummary.setMinPace((float) jsonObject.optDouble("min_pace", 0.0d));
        if (maxPace == 0.0f) {
            maxSpeed = 0.0f;
        } else {
            maxSpeed = 1.0f / maxPace;
        }
        outdoorSportSummary.setMaxSpeed(maxSpeed);
        outdoorSportSummary.setMaxStepFreq(((float) jsonObject.optInt("max_frequency", 0)) / 60.0f);
        outdoorSportSummary.setClimbUp((float) jsonObject.optInt("altitude_ascend", -1));
        outdoorSportSummary.setClimbDown((float) jsonObject.optInt("altitude_descend", -1));
        outdoorSportSummary.setHighestAltitude((float) jsonObject.optInt("max_altitude", -20000));
        outdoorSportSummary.setLowestAltitude((float) jsonObject.optInt("min_altitude", -20000));
        outdoorSportSummary.setDisPerLap((float) jsonObject.optInt("lap_distance", 0));
        String deviceInfo = jsonObject.optString("bind_device", "");
        if (deviceInfo != null) {
            String[] deviceArray = deviceInfo.split(":");
            if (deviceArray == null || deviceArray.length < 3) {
                Debug.m5i("SportSummaryParser", "device info is error");
            } else {
                int deviceType = Integer.parseInt(deviceArray[0]);
                int deviceBrand = Integer.parseInt(deviceArray[2]);
                outdoorSportSummary.setDeviceType(deviceType);
                outdoorSportSummary.setDeviceBrand(deviceBrand);
            }
        }
        outdoorSportSummary.setClimbdisascend((float) jsonObject.optInt("distance_ascend", -1));
        outdoorSportSummary.setClimbdisDescend((float) jsonObject.optInt("climb_dis_descend", -1));
        int ascTime = jsonObject.optInt("climb_dis_ascend_time", -1);
        if (ascTime > 0) {
            ascTime *= 1000;
        }
        outdoorSportSummary.setAscendTime((long) ascTime);
        int descendTime = jsonObject.optInt("climb_dis_descend_time", -1);
        if (descendTime > 0) {
            descendTime *= 1000;
        }
        outdoorSportSummary.setDescendTime((long) descendTime);
        outdoorSportSummary.setMaxCadence((float) jsonObject.optInt("max_cadence", -1));
        outdoorSportSummary.setAveCadence((float) jsonObject.optInt("avg_cadence", -1));
        String childListStr = jsonObject.optString("child_list", "");
        Debug.m5i("test_summary", "childList:" + childListStr);
        outdoorSportSummary.parseStrToChildList(childListStr);
        int parentTrackId = jsonObject.optInt("parent_trackid", -1);
        outdoorSportSummary.setParentTrackId(parentTrackId == -1 ? -1 : ((long) parentTrackId) * 1000);
        outdoorSportSummary.setMaxHeartRate(jsonObject.optInt("max_heart_rate", -1));
        outdoorSportSummary.setMinHeartRate(jsonObject.optInt("min_heart_rate", -1));
        outdoorSportSummary.setSwolfPerFixedMeters(jsonObject.optInt("swolf", -1));
        outdoorSportSummary.setTotalStrokes(jsonObject.optInt("total_strokes", -1));
        outdoorSportSummary.setTotalTrips(jsonObject.optInt("total_trips", -1));
        outdoorSportSummary.setAvgStrokeSpeed((float) jsonObject.optDouble("avg_stroke_speed", -1.0d));
        outdoorSportSummary.setMaxStrokeSpeed((float) jsonObject.optDouble("max_stroke_speed", -1.0d));
        outdoorSportSummary.setAvgDistancePerStroke((float) jsonObject.optDouble("avg_distance_per_stroke", -1.0d));
        outdoorSportSummary.setSwimPoolLength(jsonObject.optInt("swim_pool_length", -1));
        outdoorSportSummary.setTE(jsonObject.optInt("te", -1));
        outdoorSportSummary.setmSwimStyle(jsonObject.optInt("swim_style", -1));
        outdoorSportSummary.setUnit(jsonObject.optInt("unit", 0));
        outdoorSportSummary.setAvgStride(((float) jsonObject.optInt("avg_stride_length", 0)) / 100.0f);
        outdoorSportSummary.setIntervalType(jsonObject.optInt("sport_mode", 0));
        outdoorSportSummary.setDownHillNum(jsonObject.optInt("downhill_num", 0));
        outdoorSportSummary.setDownhillMaxAltitudeDescend((float) jsonObject.optInt("downhill_max_altitude_desend", -1));
        long secHalfStartTime = (long) jsonObject.optInt("second_half_start_time", -1);
        if (secHalfStartTime != -1) {
            outdoorSportSummary.setSecHalfStartTime(1000 * secHalfStartTime);
        }
        outdoorSportSummary.setmStrokes(jsonObject.optInt("strokes", 0));
        outdoorSportSummary.setmForeHand(jsonObject.optInt("fore_hand", 0));
        outdoorSportSummary.setmBackHand(jsonObject.optInt("back_hand", 0));
        outdoorSportSummary.setmServe(jsonObject.optInt("serve", 0));
        return outdoorSportSummary;
    }

    private void updateTodayDistance(List<OutdoorSportSummary> result) {
        SyncDatabaseManager<SportSummary> mSportSummaryManager = new SyncDatabaseManager(SportSummaryDao.getInstance(Global.getApplicationContext()));
        if (result == null || result.isEmpty()) {
            Debug.m3d("SportSummaryParser", "the sport summary list is null");
            return;
        }
        float todayDis = 0.0f;
        for (OutdoorSportSummary summary : result) {
            if (!(!DateUtils.isSameDate(summary.getTrackId()) || summary == null || SportType.isChildSport(summary.getSportType()))) {
                todayDis = NumeriConversionUtils.getAddResult(todayDis, NumeriConversionUtils.convertStringToFloat(DataFormatUtils.parseFormattedRealNumber((double) (summary.getDistance() / 1000.0f), false)));
            }
        }
        Debug.m3d("SportSummaryParser", "totay distance:" + todayDis);
        DataManager.getInstance().setTodayDistance(SportApplication.getInstance(), todayDis);
    }
}
