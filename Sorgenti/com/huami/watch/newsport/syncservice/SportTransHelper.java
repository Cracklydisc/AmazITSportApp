package com.huami.watch.newsport.syncservice;

import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.huami.watch.newsport.SportApplication;
import java.io.File;

public class SportTransHelper {
    private static int count;
    private static final Handler sHandler = new Handler(Looper.getMainLooper());
    private static Runnable triggerOfRestoreData = new C06441();

    static class C06441 implements Runnable {
        C06441() {
        }

        public void run() {
            try {
                if (!SportTransHelper.isRestored() && SyncHandler.mTransportManager != null && SportTransHelper.count <= 50) {
                    SyncHandler.mTransportManager.triggerUploadManual("*");
                    Log.i("LOG_TRANS_*", "sport::triggerUploadManual(*)");
                    SportTransHelper.setDataRestored(false);
                    SportTransHelper.access$008();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static /* synthetic */ int access$008() {
        int i = count;
        count = i + 1;
        return i;
    }

    public static boolean isRestored() {
        if (new File(pathOfSharedPref()).exists()) {
            SharedPreferences pref = SportApplication.getInstance().getSharedPreferences("pref_sport_data_restore_in_flag", 0);
            boolean hasRestore = pref.getBoolean("key_sport_data_restore_in_flag", false);
            pref.edit().putBoolean("key_sport_data_restore_in_flag", hasRestore).commit();
            Log.i("LOG_TRANS_*", "文件存在hasRestore:" + hasRestore);
            return hasRestore;
        }
        SportApplication.getInstance().getSharedPreferences("pref_sport_data_restore_in_flag", 0).edit().putBoolean("key_sport_data_restore_in_flag", true).commit();
        Log.i("LOG_TRANS_*", "文件不存在");
        return true;
    }

    private static String pathOfSharedPref() {
        return new StringBuffer().append(Environment.getDataDirectory().getPath()).append(File.separator).append("data").append(File.separator).append(SportApplication.getInstance().getApplicationContext().getPackageName()).append(File.separator).append("shared_prefs").append(File.separator).append("pref_sport_data_restore_in_flag").append(".xml").toString();
    }

    public static synchronized void setDataRestored(boolean restored) {
        synchronized (SportTransHelper.class) {
            if (restored) {
                sHandler.removeCallbacks(triggerOfRestoreData);
                SportApplication.getInstance().getSharedPreferences("pref_sport_data_restore_in_flag", 0).edit().putBoolean("key_sport_data_restore_in_flag", true).commit();
                Log.i("LOG_TRANS_*", "put true");
            } else {
                SportApplication.getInstance().getSharedPreferences("pref_sport_data_restore_in_flag", 0).edit().putBoolean("key_sport_data_restore_in_flag", false).commit();
                Log.i("LOG_TRANS_*", "set false");
                sHandler.removeCallbacks(triggerOfRestoreData);
                sHandler.postDelayed(triggerOfRestoreData, 15000);
            }
        }
    }
}
