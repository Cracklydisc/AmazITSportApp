package com.huami.watch.newsport.syncservice;

import android.content.Context;
import android.util.Log;
import com.huami.watch.common.db.SyncDatabaseManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.DailyPerformanceInfo;
import com.huami.watch.newsport.common.model.HeartRate;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.RunningInfoPerKM;
import com.huami.watch.newsport.common.model.RunningInfoPerLap;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.SportType;
import com.huami.watch.newsport.common.model.Vo2maxDayInfo;
import com.huami.watch.newsport.db.dao.CadenceDetailDao;
import com.huami.watch.newsport.db.dao.DailyPerpormanceInfoDao;
import com.huami.watch.newsport.db.dao.HeartRateDao;
import com.huami.watch.newsport.db.dao.LocationDataDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerKMDao;
import com.huami.watch.newsport.db.dao.RunningInfoPerLapDao;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.db.dao.SportThaInfoDao;
import com.huami.watch.newsport.db.dao.Vo2MaxDayInfoDao;
import com.huami.watch.newsport.gps.model.SportLocationData;
import com.huami.watch.newsport.syncservice.parser.SportDetailParser;
import com.huami.watch.newsport.syncservice.parser.SportSummaryParser;
import com.huami.watch.newsport.utils.FirstBeatDataUtils;
import com.huami.watch.newsport.utils.LogUtil;
import com.huami.watch.transdata.DataService.ClientListener;
import com.huami.watch.transdata.DataTrans;
import com.huami.watch.transdata.ZipUtil;
import com.huami.watch.transdata.transport.DataClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SyncClient implements ClientListener {
    private static final String TAG = SyncClient.class.getName();
    private static SyncClient sInstance = null;
    private UploadCacheManager mCacheManager = null;
    private Context mContext = null;
    private DataClient mDataClient = null;
    private SyncDatabaseManager<HeartRate> mHeartRateSyncDatabaseManager = null;
    private SyncDatabaseManager<SportLocationData> mLocationDataSyncDatabaseManager = null;
    private ReentrantLock mLock = new ReentrantLock();
    private SyncDatabaseManager<DailyPerformanceInfo> mRunningDailyformanceInfoManager = null;
    private SyncDatabaseManager<CyclingDetail> mRunningInfoCadenceSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerKM> mRunningInfoPerKMSyncDatabaseManager = null;
    private SyncDatabaseManager<RunningInfoPerLap> mRunningInfoPerLapSyncDatabaseManager = null;
    private SportDetailParser mSportDetailParser = new SportDetailParser();
    private SportSummaryParser mSportSummaryParser = new SportSummaryParser();
    private SyncDatabaseManager<SportSummary> mSportSummarySyncDatabaseManager = null;
    private SyncDatabaseManager<SportThaInfo> mSportThaInfoDataBaseManager = null;
    private SyncDatabaseManager<Vo2maxDayInfo> mVo2maxDataBaseManager = null;

    private SyncClient(Context context) {
        this.mContext = context.getApplicationContext();
        this.mSportSummarySyncDatabaseManager = new SyncDatabaseManager(SportSummaryDao.getInstance(this.mContext));
        this.mLocationDataSyncDatabaseManager = new SyncDatabaseManager(LocationDataDao.getInstance(this.mContext));
        this.mRunningInfoPerKMSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerKMDao.getInstance(this.mContext));
        this.mHeartRateSyncDatabaseManager = new SyncDatabaseManager(HeartRateDao.getInstance(this.mContext));
        this.mRunningInfoPerLapSyncDatabaseManager = new SyncDatabaseManager(RunningInfoPerLapDao.getInstance(this.mContext));
        this.mRunningInfoCadenceSyncDatabaseManager = new SyncDatabaseManager(CadenceDetailDao.getInstance(this.mContext));
        this.mRunningDailyformanceInfoManager = new SyncDatabaseManager(DailyPerpormanceInfoDao.getInstance(this.mContext));
        this.mSportThaInfoDataBaseManager = new SyncDatabaseManager(SportThaInfoDao.getmInstance(this.mContext));
        this.mVo2maxDataBaseManager = new SyncDatabaseManager(Vo2MaxDayInfoDao.getInstance(this.mContext));
        this.mCacheManager = UploadCacheManager.getInstance(this.mContext);
        init();
    }

    public static SyncClient getInstance(Context context) {
        synchronized (SyncClient.class) {
            if (sInstance == null) {
                sInstance = new SyncClient(context);
            }
        }
        return sInstance;
    }

    private void init() {
        this.mDataClient = new DataClient(this.mContext);
        this.mDataClient.registerListener(this);
    }

    public void trigger() {
        if (this.mDataClient != null) {
            this.mDataClient.triggerSync(null);
        }
    }

    public void onDataResult(String sessionId, String dataType, String dataTrans) {
        LogUtil.m9i(true, TAG, "onDataResult, " + dataTrans + ", sessionId:" + sessionId + ",dataType:" + dataType);
        if ("SPORT".equalsIgnoreCase(dataType)) {
            try {
                JSONObject jsonObject = new JSONObject(dataTrans);
                long trackId = -1;
                if (jsonObject.optBoolean("isSingle", true)) {
                    trackId = jsonObject.optLong("trackid", -1) * 1000;
                } else {
                    JSONObject parentObject = jsonObject.optJSONObject("parent");
                    if (parentObject != null) {
                        trackId = ((long) parentObject.optInt("trackid")) * 1000;
                    }
                }
                List<? extends SportSummary> summaryList = this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, "parent_trackid=? OR track_id=?", new String[]{String.valueOf(trackId), String.valueOf(trackId)}, null, null);
                if (summaryList == null || summaryList.isEmpty()) {
                    LogUtil.m9i(true, TAG, "onDataResult, summary list is null");
                    return;
                }
                for (SportSummary sportSummary : summaryList) {
                    sportSummary.setCurrentStatus(6);
                    LogUtil.m9i(true, TAG, "onDataResult, update trackId:" + sportSummary.getTrackId() + " " + this.mSportSummarySyncDatabaseManager.update(this.mContext, sportSummary));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (!"FIRSTBEAT".equalsIgnoreCase(dataType)) {
        } else {
            if (ModelUtil.isModelHuanghe(null)) {
                Log.i(TAG, "firstBeat is huanghe model ");
            } else {
                parseFistBeatPackDataByResponse(dataTrans);
            }
        }
    }

    public DataTrans getData(String sessionId, String dataType, String s1) {
        Log.i(TAG, "dataType:" + dataType);
        if (dataType.equalsIgnoreCase("SPORT")) {
            LogUtil.m9i(true, TAG, "getData, sessionId:" + sessionId);
            List<? extends SportSummary> sportSummaries = this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, "(current_status=? OR current_status=?) AND type IN (SELECT type FROM sport_summary WHERE type >=2000 OR type <= 1000)", new String[]{String.valueOf(2), String.valueOf(3)}, "start_time DESC", null);
            if (sportSummaries == null || sportSummaries.isEmpty()) {
                LogUtil.m9i(true, TAG, "getData, summary is null");
                return getEmptyData();
            }
            OutdoorSportSummary sportSummary = null;
            for (SportSummary summary : sportSummaries) {
                if (!DataManager.getInstance().getUploadSportSessionId(summary.getTrackId()).equals(sessionId)) {
                    sportSummary = (OutdoorSportSummary) summary;
                    break;
                }
            }
            if (sportSummary == null) {
                LogUtil.m9i(true, TAG, "getData, there has uploaded int one sessionId");
                return getEmptyData();
            }
            DataTrans dataTrans = new DataTrans("SPORT", sportSummary.getSportType());
            dataTrans.addHeader("trackId", String.valueOf(sportSummary.getTrackId() / 1000));
            byte[] datas;
            if (DataManager.getInstance().isUploadSportCached(sportSummary.getTrackId())) {
                DetailInfoSaver saver = this.mCacheManager.getSingleSportSaver(sportSummary.getTrackId());
                if (saver == null) {
                    saver = this.mCacheManager.newSaver(sportSummary.getTrackId());
                }
                LogUtil.m9i(true, TAG, "getData, ###load cache start###");
                datas = saver.getAllUploadWholeData();
                LogUtil.m9i(true, TAG, "getData, ###load cache end###");
                dataTrans.setData(datas, false);
            } else {
                LogUtil.m9i(true, TAG, "getData, ###save data to cache start###");
                datas = saveSportData2Cache(sportSummary);
                LogUtil.m9i(true, TAG, "getData, ###save data to cache end###");
                dataTrans.setData(datas, false);
            }
            LogUtil.m9i(true, TAG, "getData, data trans is ended");
            DataManager.getInstance().setUploadSportSessionId(sportSummary.getTrackId(), sessionId);
            return dataTrans;
        } else if (!"FIRSTBEAT".equalsIgnoreCase(dataType)) {
            return null;
        } else {
            if (!ModelUtil.isModelHuanghe(null)) {
                return postFirstBeatDataToCloud();
            }
            Log.i(TAG, "firstBeat is huanghe model ");
            return null;
        }
    }

    private DataTrans getEmptyData() {
        LogUtil.m9i(true, TAG, "getEmptyData");
        DataTrans dataTrans = new DataTrans("SPORT", -1);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dataType", "SPORT");
            jsonObject.put("data", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dataTrans.setData(jsonObject.toString().getBytes());
        return dataTrans;
    }

    private String parseSportSummary2Data(OutdoorSportSummary sportSummary) {
        return parseMapToData(this.mSportSummaryParser.parseSummaryToMap(sportSummary));
    }

    private String parseMapToData(Map<String, String> params) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d(TAG, "parseMapToData. map : " + params);
        }
        if (params == null) {
            Debug.m6w(TAG, "params is null");
            return null;
        }
        StringBuilder dataBuilder = new StringBuilder();
        Iterator iterator = params.keySet().iterator();
        if (!iterator.hasNext()) {
            return "";
        }
        String key = (String) iterator.next();
        dataBuilder.append(key).append("=").append((String) params.get(key));
        while (iterator.hasNext()) {
            String k = (String) iterator.next();
            dataBuilder.append("&").append(k).append("=").append((String) params.get(k));
        }
        String data = dataBuilder.toString();
        if (!Global.DEBUG_LEVEL_3) {
            return data;
        }
        Debug.m3d(TAG, "parsed data : " + data);
        return data;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.huami.watch.transdata.DataTrans postFirstBeatDataToCloud() {
        /*
        r22 = this;
        r1 = TAG;
        r2 = "postFirstBeatDataToCloud:";
        android.util.Log.i(r1, r2);
        r0 = r22;
        r1 = r0.mSportThaInfoDataBaseManager;
        r0 = r22;
        r2 = r0.mContext;
        r3 = "upload_status=? ";
        r4 = 1;
        r4 = new java.lang.String[r4];
        r5 = 0;
        r6 = "0";
        r4[r5] = r6;
        r5 = "dayId ASC";
        r6 = 0;
        r19 = r1.selectAll(r2, r3, r4, r5, r6);
        r18 = new org.json.JSONArray;
        r18.<init>();
        r7 = 0;
        r17 = r19.iterator();
    L_0x002a:
        r1 = r17.hasNext();
        if (r1 == 0) goto L_0x0040;
    L_0x0030:
        r8 = r17.next();
        r8 = (com.huami.watch.newsport.common.model.SportThaInfo) r8;
        r7 = r8.modelToJsonObject();
        r0 = r18;
        r0.put(r7);
        goto L_0x002a;
    L_0x0040:
        r0 = r22;
        r1 = r0.mVo2maxDataBaseManager;
        r0 = r22;
        r2 = r0.mContext;
        r3 = "upload_status=?";
        r4 = 1;
        r4 = new java.lang.String[r4];
        r5 = 0;
        r6 = "0";
        r4[r5] = r6;
        r5 = "dayId ASC";
        r6 = 0;
        r21 = r1.selectAll(r2, r3, r4, r5, r6);
        r20 = new org.json.JSONArray;
        r20.<init>();
        r17 = r21.iterator();
    L_0x0062:
        r1 = r17.hasNext();
        if (r1 == 0) goto L_0x0078;
    L_0x0068:
        r9 = r17.next();
        r9 = (com.huami.watch.newsport.common.model.Vo2maxDayInfo) r9;
        r7 = r9.modelToJsonObject();
        r0 = r20;
        r0.put(r7);
        goto L_0x0062;
    L_0x0078:
        r1 = com.huami.watch.newsport.common.manager.DataManager.getInstance();
        r0 = r22;
        r2 = r0.mContext;
        r14 = r1.getFirstBeatConfig(r2);
        r14.toJSON();
        r12 = 0;
        if (r19 == 0) goto L_0x0090;
    L_0x008a:
        r1 = r19.size();	 Catch:{ JSONException -> 0x0106 }
        if (r1 != 0) goto L_0x00fb;
    L_0x0090:
        if (r21 == 0) goto L_0x0098;
    L_0x0092:
        r1 = r21.size();	 Catch:{ JSONException -> 0x0106 }
        if (r1 != 0) goto L_0x00fb;
    L_0x0098:
        r13 = new org.json.JSONObject;	 Catch:{ JSONException -> 0x0106 }
        r13.<init>();	 Catch:{ JSONException -> 0x0106 }
        r12 = r13;
    L_0x009e:
        r16 = new org.json.JSONObject;
        r16.<init>();
        r15 = new org.json.JSONObject;	 Catch:{ JSONException -> 0x010b }
        r15.<init>();	 Catch:{ JSONException -> 0x010b }
        r1 = "sportThaInfo";
        r0 = r18;
        r15.put(r1, r0);	 Catch:{ JSONException -> 0x010b }
        r1 = "vo2max";
        r0 = r20;
        r15.put(r1, r0);	 Catch:{ JSONException -> 0x010b }
        r1 = "firstbeatconfig";
        r15.put(r1, r12);	 Catch:{ JSONException -> 0x010b }
        r1 = "dataType";
        r2 = "FIRSTBEAT";
        r0 = r16;
        r0.put(r1, r2);	 Catch:{ JSONException -> 0x010b }
        r1 = "data";
        r0 = r16;
        r0.put(r1, r15);	 Catch:{ JSONException -> 0x010b }
    L_0x00cb:
        r1 = TAG;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "postFirstBeatDataToCloud:";
        r2 = r2.append(r3);
        r3 = r16.toString();
        r2 = r2.append(r3);
        r2 = r2.toString();
        android.util.Log.i(r1, r2);
        r10 = new com.huami.watch.transdata.DataTrans;
        r1 = "FIRSTBEAT";
        r2 = 0;
        r10.<init>(r1, r2);
        r1 = r16.toString();
        r1 = r1.getBytes();
        r10.setData(r1);
        return r10;
    L_0x00fb:
        r13 = new org.json.JSONObject;	 Catch:{ JSONException -> 0x0106 }
        r1 = r14.toJSON();	 Catch:{ JSONException -> 0x0106 }
        r13.<init>(r1);	 Catch:{ JSONException -> 0x0106 }
        r12 = r13;
        goto L_0x009e;
    L_0x0106:
        r11 = move-exception;
        r11.printStackTrace();
        goto L_0x009e;
    L_0x010b:
        r11 = move-exception;
        r11.printStackTrace();
        goto L_0x00cb;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.syncservice.SyncClient.postFirstBeatDataToCloud():com.huami.watch.transdata.DataTrans");
    }

    private void parseFistBeatPackDataByResponse(String reponse) {
        if (reponse != null) {
            try {
                Log.i(TAG, "parseFistBeatPackDataByResponse:" + reponse);
                JSONObject responseObject = new JSONObject(reponse);
                if (responseObject != null) {
                    int i;
                    long currentDayId;
                    JSONArray thaArray = (JSONArray) responseObject.opt("sportThaInfo");
                    JSONArray vo2maxArray = (JSONArray) responseObject.opt("vo2max");
                    JSONObject firstBeatConfig = (JSONObject) responseObject.opt("firstbeatconfig");
                    if (thaArray != null && thaArray.length() > 0) {
                        for (i = 0; i < thaArray.length(); i++) {
                            String dayId = ((JSONObject) thaArray.get(i)).optString("dayId");
                            Log.i(TAG, " currnetDayId:" + FirstBeatDataUtils.getDayIdByDataStr(dayId));
                            SportThaInfo sportThaInfo = (SportThaInfo) this.mSportThaInfoDataBaseManager.select(this.mContext, "select * from sport_tha_info where dayId=? ; ", new String[]{String.valueOf(currentDayId)});
                            sportThaInfo.setUploadStatus(1);
                            Log.i(TAG, " update SportThaInfo " + this.mSportThaInfoDataBaseManager.update(this.mContext, sportThaInfo) + ":" + dayId);
                        }
                    }
                    if (vo2maxArray != null && vo2maxArray.length() > 0) {
                        for (i = 0; i < vo2maxArray.length(); i++) {
                            currentDayId = FirstBeatDataUtils.getDayIdByDataStr(((JSONObject) vo2maxArray.get(i)).optString("dayId"));
                            Vo2maxDayInfo vo2maxDayInfo = (Vo2maxDayInfo) this.mVo2maxDataBaseManager.select(this.mContext, "select * from vo2_max_day_info where dayId=? ; ", new String[]{String.valueOf(currentDayId)});
                            vo2maxDayInfo.setUploadStatus(1);
                            this.mVo2maxDataBaseManager.update(this.mContext, vo2maxDayInfo);
                        }
                        return;
                    }
                    return;
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        Log.i(TAG, "parseFistBeatPackDataByResponse is null ");
    }

    private byte[] saveSportData2Cache(SportSummary sportSummary) {
        if (sportSummary == null) {
            LogUtil.m9i(true, TAG, "getData, there has uploaded int one sessionId");
            return null;
        }
        boolean isSingle = true;
        List<SportSummary> uploadSummarys = new ArrayList();
        uploadSummarys.add(sportSummary);
        if (SportType.isMixedSport(sportSummary.getSportType())) {
            isSingle = false;
            uploadSummarys.addAll(this.mSportSummarySyncDatabaseManager.selectAll(this.mContext, "parent_trackid=?", new String[]{String.valueOf(sportSummary.getTrackId())}, "start_time DESC", "1"));
        }
        String datas = parseSportData2Json(uploadSummarys, isSingle);
        byte[] originDatas = datas.getBytes();
        LogUtil.m9i(true, TAG, "gzip start");
        final byte[] gZipDatas = ZipUtil.gZip(originDatas);
        LogUtil.m9i(true, TAG, "gzip end");
        this.mLock.lock();
        if (this.mCacheManager.getSingleSportSaver(sportSummary.getTrackId()) == null) {
            boolean isSuccess = this.mCacheManager.addSportInfo(sportSummary.getTrackId());
            if (this.mLock.isLocked()) {
                this.mLock.unlock();
            }
            if (isSuccess) {
                final SportSummary sportSummary2 = sportSummary;
                Global.getGlobalCloudHandler().post(new Runnable() {
                    public void run() {
                        SyncClient.this.mCacheManager.getSingleSportSaver(sportSummary2.getTrackId()).saveAllUploadWholeData(gZipDatas, sportSummary2.getTrackId());
                    }
                });
            }
        }
        if (this.mLock.isLocked()) {
            this.mLock.unlock();
        }
        LogUtil.m9i(true, TAG, "getData, datas, " + datas);
        return gZipDatas;
    }

    private String parseSportData2Json(List<SportSummary> sportSummaries, boolean isSingle) {
        if (sportSummaries == null) {
            return "";
        }
        JSONObject sportUploadJson = new JSONObject();
        try {
            sportUploadJson.put("dataType", "SPORT");
            JSONObject dataJson = new JSONObject();
            dataJson.put("isSingle", isSingle);
            SportSummary sportSummary;
            long trackId;
            String summaryContent;
            String detailContent;
            if (isSingle) {
                sportSummary = (SportSummary) sportSummaries.get(0);
                trackId = sportSummary.getTrackId();
                summaryContent = parseSportSummary2Data((OutdoorSportSummary) sportSummary);
                detailContent = V2ParseSportDetail2Data((OutdoorSportSummary) sportSummary);
                LogUtil.m9i(true, TAG, "json start");
                dataJson.put("trackid", trackId / 1000);
                dataJson.put("detail", detailContent);
                dataJson.put("summary", summaryContent);
                LogUtil.m9i(true, TAG, "json end");
            } else {
                JSONObject parentJson = new JSONObject();
                JSONArray childJsonArray = new JSONArray();
                int i = 0;
                for (SportSummary sportSummary2 : sportSummaries) {
                    trackId = sportSummary2.getTrackId();
                    summaryContent = parseSportSummary2Data((OutdoorSportSummary) sportSummary2);
                    detailContent = V2ParseSportDetail2Data((OutdoorSportSummary) sportSummary2);
                    if (SportType.isMixedSport(sportSummary2.getSportType())) {
                        parentJson.put("trackid", trackId / 1000);
                        parentJson.put("detail", detailContent);
                        parentJson.put("summary", summaryContent);
                    } else {
                        JSONObject childJson = new JSONObject();
                        childJson.put("trackid", trackId / 1000);
                        childJson.put("detail", detailContent);
                        childJson.put("summary", summaryContent);
                        childJsonArray.put(i, childJson);
                        i++;
                    }
                }
                dataJson.put("parent", parentJson.toString());
                dataJson.put("children", childJsonArray.toString());
            }
            sportUploadJson.put("data", dataJson.toString());
            return sportUploadJson.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    private String V2ParseSportDetail2Data(OutdoorSportSummary sportSummary) {
        LogUtil.m9i(true, TAG, "parseSportDetail2Data start");
        long trackId = sportSummary.getTrackId();
        List<? extends SportLocationData> locationDatas = this.mLocationDataSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (locationDatas == null || locationDatas.isEmpty()) {
            Debug.m6w(TAG, "location is null or empty. track id : " + trackId);
        }
        List<? extends RunningInfoPerKM> runningInfoPerKMs = this.mRunningInfoPerKMSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerKMs == null || runningInfoPerKMs.isEmpty()) {
            Debug.m6w(TAG, "running info is null or empty. track id : " + trackId);
        }
        List<? extends HeartRate> heartRates = this.mHeartRateSyncDatabaseManager.selectAll(this.mContext, "time>=? AND time<=?", new String[]{"" + sportSummary.getStartTime(), "" + sportSummary.getEndTime()}, null, null);
        List<? extends RunningInfoPerLap> runningInfoPerIndividuals = this.mRunningInfoPerLapSyncDatabaseManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (runningInfoPerIndividuals == null || runningInfoPerIndividuals.isEmpty()) {
            Debug.m6w(TAG, "running laps info is null or empty. track id : " + trackId);
        }
        List<? extends CyclingDetail> cyclingDetails = this.mRunningInfoCadenceSyncDatabaseManager.selectAll(this.mContext, "track_id>=?", new String[]{"" + trackId}, null, null);
        if (cyclingDetails == null || cyclingDetails.isEmpty()) {
            Debug.m6w(TAG, "running cadence info is null or empty. track id : " + trackId);
        }
        List<? extends DailyPerformanceInfo> dailyPerformanceInfos = this.mRunningDailyformanceInfoManager.selectAll(this.mContext, "track_id=?", new String[]{"" + trackId}, null, null);
        if (dailyPerformanceInfos == null || dailyPerformanceInfos.isEmpty()) {
            Debug.m6w(TAG, "running daily info is null or empty. track id : " + trackId);
        } else {
            Log.i(TAG, " upload dailyPerformanceInfos size:" + dailyPerformanceInfos.size());
        }
        String datas = parseMapToData(this.mSportDetailParser.parseDetailToMap(sportSummary, locationDatas, runningInfoPerKMs, runningInfoPerIndividuals, heartRates, cyclingDetails, dailyPerformanceInfos));
        LogUtil.m9i(true, TAG, "parseSportDetail2Data end");
        return datas;
    }
}
