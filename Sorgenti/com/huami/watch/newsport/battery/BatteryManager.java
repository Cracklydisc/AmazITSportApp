package com.huami.watch.newsport.battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.huami.watch.common.log.Debug;

public class BatteryManager {
    private static final String TAG = BatteryManager.class.getName();
    private float batteryToDistance = 3.21f;
    private final BroadcastReceiver mBatteryReceiver = new C05401();
    private Context mContext;
    private boolean mIsRegisterReceiver = false;
    private IBatteryChangedListener mListener;
    private int mRawlevel;
    private int mScale;
    private int mStatus;

    class C05401 extends BroadcastReceiver {
        C05401() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equalsIgnoreCase(intent.getAction())) {
                BatteryManager.this.updateBattery(intent);
            }
        }
    }

    public interface IBatteryChangedListener {
        void doBatteryChangedRequest();

        void onBatteryChanged(int i, int i2, float f, int i3);
    }

    public BatteryManager(Context context) {
        this.mContext = context;
    }

    public void registerBatteryReceiver() {
        Debug.m5i(TAG, "registerBatteryReceiver, mIsRegisterReceiver:" + this.mIsRegisterReceiver);
        if (!this.mIsRegisterReceiver) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            this.mContext.registerReceiver(this.mBatteryReceiver, intentFilter);
            this.mIsRegisterReceiver = true;
        }
    }

    public void unRegisterBatteryReceiver() {
        Debug.m5i(TAG, "unRegisterBatteryReceiver");
        try {
            this.mContext.unregisterReceiver(this.mBatteryReceiver);
            this.mIsRegisterReceiver = false;
        } catch (Exception e) {
            e.printStackTrace();
            this.mIsRegisterReceiver = false;
        }
    }

    public void setBatterListener(IBatteryChangedListener listener) {
        this.mListener = listener;
    }

    private void updateBattery(Intent intent) {
        this.mRawlevel = intent.getIntExtra("level", -1);
        this.mScale = intent.getIntExtra("scale", -1);
        this.mStatus = intent.getIntExtra("status", -1);
        getBatteryLevel();
    }

    private void getBatteryLevel() {
        int level = (this.mRawlevel * 100) / this.mScale;
        int continueTime = (int) getContinueHour(level);
        float continueDis = getContinueDistance(level);
        if (this.mListener != null) {
            this.mListener.onBatteryChanged(level, continueTime, continueDis, this.mStatus);
        }
    }

    private float getContinueHour(int level) {
        return ((float) level) * 0.35f;
    }

    private float getContinueDistance(int level) {
        return this.batteryToDistance * ((float) level);
    }
}
