package com.huami.watch.newsport.utils;

import android.content.Context;
import android.os.PowerManager;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;

public class PowerUtils {
    public static void wakeupForMilliSecond(Context context, long milliSecond) {
        if (Global.DEBUG_LEVEL_3) {
            Debug.m3d("PowerUtils", "requestList : " + milliSecond);
        }
        ((PowerManager) context.getSystemService("power")).newWakeLock(268435466, "TEST").acquire(milliSecond);
    }
}
