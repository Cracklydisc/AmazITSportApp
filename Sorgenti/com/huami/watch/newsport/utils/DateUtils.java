package com.huami.watch.newsport.utils;

import java.util.Calendar;

public class DateUtils {
    public static boolean isSameDate(long trackId) {
        boolean isSameYear;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(trackId);
        if (cal1.get(1) == cal2.get(1)) {
            isSameYear = true;
        } else {
            isSameYear = false;
        }
        boolean isSameMonth;
        if (cal1.get(2) == cal2.get(2)) {
            isSameMonth = true;
        } else {
            isSameMonth = false;
        }
        boolean isSameDay;
        if (cal1.get(5) == cal2.get(5)) {
            isSameDay = true;
        } else {
            isSameDay = false;
        }
        if (isSameYear && isSameMonth && isSameDay) {
            return true;
        }
        return false;
    }

    public static long getDayStartTime(long trackId) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(trackId);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar.getTimeInMillis();
    }

    public static long getDayendTime(long trackId) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(trackId);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        calendar.add(6, 1);
        return calendar.getTimeInMillis();
    }

    public static String getTimeFormatDefaultOnlySecond(long second) {
        int sec = (int) (second % 60);
        StringBuilder sb = new StringBuilder();
        if (sec > 9) {
            sb.append(sec);
        } else {
            sb.append("0").append(sec);
        }
        return sb.toString();
    }

    public static String getTimeFormatDefaultOnlyMinute(long second) {
        int min = (int) (second / 60);
        StringBuilder sb = new StringBuilder();
        if (min > 9) {
            sb.append(min);
        } else {
            sb.append("0").append(min);
        }
        return sb.toString();
    }
}
