package com.huami.watch.newsport.utils;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import com.huami.watch.common.log.Debug;
import com.huami.watch.wearubc.UbcInterface;

public class PointUtils {
    public static final String TAG = PointUtils.class.getName();

    public static String getApplicationVersion(Context context) {
        if (context.getApplicationContext() == null) {
            return "NULL";
        }
        try {
            return String.valueOf(context.getApplicationContext().getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "NULL";
        }
    }

    public static void recordEventProperty(String eventType, int sportType) {
        String property = null;
        if (sportType == 1) {
            property = "running";
        } else if (sportType == 6) {
            property = "walking";
        } else if (sportType == 7) {
            property = "cross-country";
        } else if (sportType == 8) {
            property = "indoor-running";
        } else if (sportType == 9) {
            property = "riding";
        } else if (sportType == 10) {
            property = "riding-indoor";
        } else if (sportType == 12) {
            property = "elliptical-machine";
        } else if (sportType == 13) {
            property = "mountain";
        } else if (sportType == 14) {
            property = "pool-swimming";
        } else if (sportType == 15) {
            property = "open-water-swimming";
        } else if (sportType == 2001) {
            property = "triathlon";
        } else if (sportType == 11) {
            property = "skiing";
        } else if (sportType == 17) {
            property = "tennis";
        } else if (sportType == 18) {
            property = "soccer";
        } else if (sportType == 2002) {
            property = "compound";
        }
        if (property == null) {
            Debug.m5i(TAG, "invalid sport type:" + sportType);
            return;
        }
        UbcInterface.recordPropertyEvent(eventType, property);
    }
}
