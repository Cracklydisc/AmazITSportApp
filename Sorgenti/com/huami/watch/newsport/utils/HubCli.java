package com.huami.watch.newsport.utils;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HubCli {
    private static final String TAG = HubCli.class.getName();
    private LocalSocket Client = null;
    private InputStream in = null;
    private OutputStream out = null;
    private int timeout = 30000;

    class C08821 implements Comparator<File> {
        C08821() {
        }

        public int compare(File o1, File o2) {
            return (int) ((o1.lastModified() / 1000) - (o2.lastModified() / 1000));
        }
    }

    private void connect() {
        try {
            this.Client = new LocalSocket();
            this.Client.connect(new LocalSocketAddress("sensorhubd", Namespace.RESERVED));
            this.Client.setSoTimeout(this.timeout);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close() {
        try {
            this.out.close();
            this.in.close();
            this.Client.close();
            this.Client = null;
            this.in = null;
            this.out = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int sendCommand(String cmd) {
        byte[] data = cmd.getBytes();
        try {
            this.out = this.Client.getOutputStream();
            this.out.write(data);
            this.out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private String recvAck() {
        byte[] ack = new byte[1024];
        try {
            this.in = this.Client.getInputStream();
            return new String(ack, 0, this.in.read(ack));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int startCapture() {
        Debug.m5i(TAG, "startCapture");
        connect();
        sendCommand("CTRL-REQ-CAPTURE-START");
        String ack = recvAck();
        if (ack == null || !ack.equals("OK")) {
            close();
            return -1;
        }
        close();
        return 0;
    }

    public String stopCapture() {
        Debug.m5i(TAG, "stopCapture");
        connect();
        sendCommand("CTRL-REQ-CAPTURE-STOP");
        String ack = recvAck();
        if (ack == null) {
            close();
            return null;
        }
        String[] result = ack.split(" ");
        if (result.length != 2) {
            close();
            return null;
        } else if (result[0].equals("OK")) {
            close();
            return result[1];
        } else {
            close();
            return null;
        }
    }

    public void reduceCaptureData() {
        File file = new File("/data/misc/sensor/");
        if (file != null && file.exists() && file.isDirectory()) {
            File[] childFiles = file.listFiles();
            if (childFiles == null || childFiles.length <= 10) {
                LogUtil.m9i(true, TAG, "reduceCaptureData failed, file length below 10: " + (childFiles == null ? 0 : childFiles.length));
                return;
            }
            File cf;
            List<File> childList = Arrays.asList(childFiles);
            Collections.sort(childList, new C08821());
            for (File cf2 : childList) {
                LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "reduceCaptureData, file name:" + cf2.getName() + ", last modify time:" + cf2.lastModified());
            }
            for (int i = 0; i < childList.size() - 10; i++) {
                cf2 = (File) childList.get(i);
                LogUtil.m9i(Global.DEBUG_LEVEL_3, TAG, "reduceCaptureData, delete file name:" + cf2.getAbsolutePath() + ", last modify time:" + cf2.lastModified() + ", isSuccess:" + clearFile(cf2.getAbsolutePath()));
            }
            return;
        }
        LogUtil.m9i(true, TAG, "reduceCaptureData failed, file:" + file);
    }

    public boolean clearFile(String filePath) {
        try {
            Runtime.getRuntime().exec("rm -rf " + filePath + File.separator);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
