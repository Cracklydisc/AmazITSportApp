package com.huami.watch.newsport.utils;

import android.os.SystemClock;
import android.os.SystemProperties;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.route.controller.RouteTrailManager;

public class SlptRouteManager {
    int debugPngCounter = 0;
    private double mCurrentMapCenterLatitude = 0.0d;
    private double mCurrentMapCenterLongitude = 0.0d;
    int mCurrentPointIndex = 0;
    private double mCurrentRotationDegree = 0.0d;
    private int mCurrentZoomLevel = 14;
    private boolean mForceUpdate = false;
    private double mLastSaveMapLatitude = 0.0d;
    private double mLastSaveMapLongitude = 0.0d;
    private long mLastSaveTimestamp = 0;
    private double mUpdateBitmapDistance = 1000.0d;
    private int moveMapCount = 0;

    public void onPause() {
        Debug.m3d("SlptRouteManager", "onPause");
        setCurrentLoLa2Property();
    }

    public SlptRouteManager() {
        Debug.m3d("SlptRouteManager", "init SlptRouteManager ---");
    }

    public void setZoomLevel(int level) {
        Debug.m3d("SlptRouteManager", "setZoomLevel :" + level);
        this.mCurrentZoomLevel = level;
        SystemProperties.set("sport.map.meterPerPixel", String.valueOf(300000.0d * (((double) RouteTrailManager.ROUTE_SCALE_RANGE[level]) / 30.0d)));
    }

    public void updateCurrentLocation(double longitude, double latitude) {
        Debug.m3d("SlptRouteManager", "updateCurrentLocation to " + longitude + "," + latitude);
        this.mCurrentMapCenterLongitude = longitude;
        this.mCurrentMapCenterLatitude = latitude;
    }

    private void setCurrentLoLa2Property() {
        SystemProperties.set("sport.map.longitude", String.valueOf((int) (this.mCurrentMapCenterLongitude * 300000.0d)));
        SystemProperties.set("sport.map.latitude", String.valueOf((int) (this.mCurrentMapCenterLatitude * 300000.0d)));
        long timestamp = SystemClock.uptimeMillis() / 1000;
        SystemProperties.set("sport.map.timestamp", String.valueOf(timestamp));
        SystemProperties.set("sport.map.rotation", String.valueOf(this.mCurrentRotationDegree));
        Debug.m3d("SlptRouteManager", "property sport.map.timestamp : " + timestamp + " location:[" + this.mCurrentMapCenterLongitude + ", " + this.mCurrentMapCenterLatitude + "] " + "sport.map.rotation:" + this.mCurrentRotationDegree);
    }

    public void rotateMap(double degree, long animDuration) {
        this.mCurrentRotationDegree = degree;
    }

    public void hideMapView() {
        Debug.m3d("SlptRouteManager", "hideMapView");
        SystemProperties.set("sport.show.mapview", String.valueOf(0));
    }

    public void showMapView() {
        Debug.m3d("SlptRouteManager", "showMapView");
        SystemProperties.set("sport.show.mapview", String.valueOf(1));
    }
}
