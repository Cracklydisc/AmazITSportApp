package com.huami.watch.newsport.utils;

public class SportDataItemInfo {
    public static final int[] mCrossingInfoOrder = new int[]{1, 2, 5, 3, 4, 10, 11, 7, 13, 8, 9, 6};
    public static final int[] mEppiticalInfoOrder = new int[]{1, 5, 6};
    public static final int[] mIndoorRidingInfoOrder = new int[]{1, 5, 6};
    public static final int[] mIndoorRunningInfoOrder = new int[]{1, 2, 3, 5, 4, 6, 10, 11, 12};
    public static final int[] mMixedOpenSwimInfoOrder = new int[]{1, 22, 20};
    public static final int[] mMixedRidingInfoOrder = new int[]{1, 2, 10, 5};
    public static final int[] mMixedRunningInfoOrder = new int[]{1, 2, 3, 5};
    public static final int[] mMountaineerInfoOrder = new int[]{1, 2, 7, 5, 8, 13, 9, 10, 11, 6, 24};
    public static final int[] mOpenWaterSwimingInfoOrder = new int[]{1, 22, 18, 19, 20, 21, 6};
    public static final int[] mOutdoorRidingInfoOrder = new int[]{1, 2, 5, 10, 11, 7, 13, 8, 9, 6};
    public static final int[] mRunningInfoOrder = new int[]{1, 2, 5, 3, 4, 12, 10, 11, 7, 13, 8, 9, 6, 23};
    public static final int[] mSkiingInfoOrder = new int[]{1, 10, 26, 30, 27, 28, 29, 7, 5, 6, 25};
    public static final int[] mSoccerInfoOrder = new int[]{1, 2, 6, 5};
    public static final int[] mSwimingInfoOrder = new int[]{1, 22, 16, 17, 18, 19, 20, 21, 6};
    public static final int[] mTennisInfoOrder = new int[]{1, 31, 6, 5};
    public static final int[] mWalkingInfoOrder = new int[]{14, 1, 2, 5, 6, 12, 3, 4, 10, 11};
}
