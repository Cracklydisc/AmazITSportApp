package com.huami.watch.newsport.utils;

import com.huami.watch.extendsapi.PowerkeyUtils;

public class ScreenUtils {
    private static final String TAG = ScreenUtils.class.getName();

    public static synchronized void setHomeKeyLock(boolean lock) {
        synchronized (ScreenUtils.class) {
            try {
                PowerkeyUtils.disablePowerKey(lock);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
