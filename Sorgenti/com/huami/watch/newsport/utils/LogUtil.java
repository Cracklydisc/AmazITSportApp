package com.huami.watch.newsport.utils;

import com.huami.watch.common.log.Debug;

public class LogUtil {
    public static void m9i(boolean level, String tag, String msg) {
        if (level) {
            Debug.m5i(tag, msg);
        }
    }

    public static void m7d(boolean level, String tag, String msg) {
        if (level) {
            Debug.m3d(tag, msg);
        }
    }

    public static void m10w(boolean level, String tag, String msg) {
        if (level) {
            Debug.m6w(tag, msg);
        }
    }

    public static void m8e(boolean level, String tag, String msg) {
        if (level) {
            Debug.m4e(tag, msg);
        }
    }
}
