package com.huami.watch.newsport.utils;

import com.huami.watch.common.log.Debug;
import java.util.List;

public class ParseUtils {
    private static final String TAG = ParseUtils.class.getName();

    public static String parseNumberArrayToString(List<? extends Number> order, String split) {
        StringBuilder sb = new StringBuilder();
        if (order != null && order.size() > 0) {
            sb.append(order.get(0));
            for (int i = 1; i < order.size(); i++) {
                sb.append(split).append(order.get(i));
            }
        }
        return sb.toString();
    }

    public static int[] parseStringToIntArray(String orderStr) {
        if (orderStr == null || orderStr.isEmpty()) {
            return null;
        }
        try {
            String[] orders = orderStr.split(",");
            int[] result = new int[orders.length];
            for (int i = 0; i < orders.length; i++) {
                result[i] = Integer.parseInt(orders[i]);
            }
            return result;
        } catch (Exception e) {
            Debug.m6w(TAG, "parse order string " + orderStr + " failed, return order as null");
            return null;
        }
    }

    public static float[] parseStringToFloatArray(String orderStr) {
        if (orderStr == null || orderStr.isEmpty()) {
            return null;
        }
        try {
            String[] orders = orderStr.split(",");
            float[] result = new float[orders.length];
            for (int i = 0; i < orders.length; i++) {
                result[i] = Float.parseFloat(orders[i]);
            }
            return result;
        } catch (Exception e) {
            Debug.m6w(TAG, "parse order string " + orderStr + " failed, return order as null");
            return null;
        }
    }
}
