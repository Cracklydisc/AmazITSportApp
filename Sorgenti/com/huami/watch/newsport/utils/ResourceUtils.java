package com.huami.watch.newsport.utils;

import android.content.Context;

public class ResourceUtils {
    public static String[] getStringArray(Context mContext, int resultId) {
        return mContext.getResources().getStringArray(resultId);
    }

    public static int getStringId(Context paramContext, String paramString) {
        return paramContext.getResources().getIdentifier(paramString, "string", paramContext.getPackageName());
    }

    public static int getDrawableId(Context paramContext, String paramString) {
        return paramContext.getResources().getIdentifier(paramString, "drawable", paramContext.getPackageName());
    }
}
