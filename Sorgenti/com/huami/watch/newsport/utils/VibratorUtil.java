package com.huami.watch.newsport.utils;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.os.Vibrator;

public class VibratorUtil {
    public static AudioAttributes getMostStrongAttributes() {
        return new Builder().setUsage(22).build();
    }

    public static void vibrate(Context context, long[] pattern, int repeat, AudioAttributes attributes) {
        ((Vibrator) context.getSystemService("vibrator")).vibrate(pattern, repeat, attributes);
    }

    public static void vibrateOnceWithStrongAttributes(Context context, long[] pattern) {
        vibrate(context, pattern, -1, getMostStrongAttributes());
    }
}
