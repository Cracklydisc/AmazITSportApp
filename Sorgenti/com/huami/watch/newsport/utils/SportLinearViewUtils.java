package com.huami.watch.newsport.utils;

import android.content.Context;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.HmImageView;
import com.huami.watch.newsport.utils.Constants.BackgroundUtils;

public class SportLinearViewUtils {
    public static final void initView(Context context, LinearLayout layout, String value, int type, boolean isBar) {
        if (layout != null && value != null) {
            int[] resNums;
            if (isBar) {
                resNums = BackgroundUtils.getBarResArray(type);
            } else {
                resNums = BackgroundUtils.getRealNumResArray(type);
            }
            char c;
            HmImageView imageView;
            byte asc;
            int pos;
            if (layout.getChildCount() == value.length()) {
                char[] items = value.toCharArray();
                for (int i = 0; i < items.length; i++) {
                    c = items[i];
                    imageView = (HmImageView) layout.getChildAt(i);
                    asc = (byte) c;
                    pos = -1;
                    if (asc >= (byte) 48 && asc <= (byte) 57) {
                        pos = asc - 48;
                    } else if (c == ':') {
                        pos = 10;
                    } else if (c == '-') {
                        pos = 13;
                    } else if (c == '.') {
                        pos = 12;
                    } else if (c == '-') {
                        pos = 13;
                    } else if (c == '\'') {
                        pos = 14;
                    } else if (c == '\"') {
                        pos = 15;
                    } else if (c == 'u') {
                        pos = 16;
                    } else if (c == 'd') {
                        pos = 17;
                    }
                    if (pos >= 0) {
                        imageView.setBackground(context.getDrawable(resNums[pos]));
                    }
                }
                return;
            }
            layout.removeAllViews();
            for (char c2 : value.toCharArray()) {
                asc = (byte) c2;
                pos = -1;
                if (asc >= (byte) 48 && asc <= (byte) 57) {
                    pos = asc - 48;
                } else if (c2 == ':') {
                    pos = 10;
                } else if (c2 == '-') {
                    pos = 13;
                } else if (c2 == '.') {
                    pos = 12;
                } else if (c2 == '-') {
                    pos = 13;
                } else if (c2 == '\'') {
                    pos = 14;
                } else if (c2 == '\"') {
                    pos = 15;
                } else if (c2 == 'u') {
                    pos = 16;
                } else if (c2 == 'd') {
                    pos = 17;
                }
                if (pos >= 0) {
                    imageView = new HmImageView(context);
                    imageView.setBackground(context.getDrawable(resNums[pos]));
                    layout.addView(imageView);
                }
            }
        }
    }
}
