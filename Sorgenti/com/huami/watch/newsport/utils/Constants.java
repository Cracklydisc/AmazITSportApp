package com.huami.watch.newsport.utils;

import android.content.Context;
import android.provider.Settings.Secure;
import com.hs.gpxparser.utils.GPSSPUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;

public class Constants {

    public static class BackgroundUtils {
        private static final int[] BAR_NUM_RES = new int[]{C0532R.drawable.bar_0, C0532R.drawable.bar_1, C0532R.drawable.bar_2, C0532R.drawable.bar_3, C0532R.drawable.bar_4, C0532R.drawable.bar_5, C0532R.drawable.bar_6, C0532R.drawable.bar_7, C0532R.drawable.bar_8, C0532R.drawable.bar_9, C0532R.drawable.bar_colon};
        public static final int[] BATTERY_RES = new int[]{C0532R.drawable.widget_battery_00, C0532R.drawable.widget_battery_01, C0532R.drawable.widget_battery_02, C0532R.drawable.widget_battery_03, C0532R.drawable.widget_battery_04, C0532R.drawable.widget_battery_05, C0532R.drawable.widget_battery_06, C0532R.drawable.widget_battery_07, C0532R.drawable.widget_battery_08, C0532R.drawable.widget_battery_09, C0532R.drawable.widget_battery_10, C0532R.drawable.widget_battery_11, C0532R.drawable.widget_battery_12, C0532R.drawable.widget_battery_13};
        private static final int[] BG_WHITE_BAR_NUM_RES = new int[]{C0532R.drawable.bg_white_bar_0, C0532R.drawable.bg_white_bar_1, C0532R.drawable.bg_white_bar_2, C0532R.drawable.bg_white_bar_3, C0532R.drawable.bg_white_bar_4, C0532R.drawable.bg_white_bar_5, C0532R.drawable.bg_white_bar_6, C0532R.drawable.bg_white_bar_7, C0532R.drawable.bg_white_bar_8, C0532R.drawable.bg_white_bar_9, C0532R.drawable.bg_white_bar_colon};
        public static final int[] BG_WHITE_BATTERY_RES = new int[]{C0532R.drawable.bg_white_battery_00, C0532R.drawable.bg_white_battery_01, C0532R.drawable.bg_white_battery_02, C0532R.drawable.bg_white_battery_03, C0532R.drawable.bg_white_battery_04, C0532R.drawable.bg_white_battery_05, C0532R.drawable.bg_white_battery_06, C0532R.drawable.bg_white_battery_07, C0532R.drawable.bg_white_battery_08, C0532R.drawable.bg_white_battery_09, C0532R.drawable.bg_white_battery_10, C0532R.drawable.bg_white_battery_11, C0532R.drawable.bg_white_battery_12, C0532R.drawable.bg_white_battery_13};
        private static final int[] BG_WHITE_NUM_RES = new int[]{C0532R.drawable.bg_white_num_0, C0532R.drawable.bg_white_num_1, C0532R.drawable.bg_white_num_2, C0532R.drawable.bg_white_num_3, C0532R.drawable.bg_white_num_4, C0532R.drawable.bg_white_num_5, C0532R.drawable.bg_white_num_6, C0532R.drawable.bg_white_num_7, C0532R.drawable.bg_white_num_8, C0532R.drawable.bg_white_num_9, C0532R.drawable.bg_white_colon, C0532R.drawable.bg_white_default, C0532R.drawable.bg_white_dot, C0532R.drawable.bg_white_minus, C0532R.drawable.bg_white_minute, C0532R.drawable.bg_white_second, C0532R.drawable.bg_white_up, C0532R.drawable.bg_white_down};
        private static final int[] NUM_RES = new int[]{C0532R.drawable.num_0, C0532R.drawable.num_1, C0532R.drawable.num_2, C0532R.drawable.num_3, C0532R.drawable.num_4, C0532R.drawable.num_5, C0532R.drawable.num_6, C0532R.drawable.num_7, C0532R.drawable.num_8, C0532R.drawable.num_9, C0532R.drawable.colon, C0532R.drawable.default_value, C0532R.drawable.dot, C0532R.drawable.minus, C0532R.drawable.minute, C0532R.drawable.second, C0532R.drawable.up, C0532R.drawable.down};

        public static int getCustomBgType(Context context) {
            return Secure.getInt(context.getContentResolver(), "sport_background_mode", 0);
        }

        public static int[] getBarResArray(int type) {
            if (type == 1) {
                return BG_WHITE_BAR_NUM_RES;
            }
            return BAR_NUM_RES;
        }

        public static int[] getRealNumResArray(int type) {
            if (type == 1) {
                return BG_WHITE_NUM_RES;
            }
            return NUM_RES;
        }

        public static int[] getBatteryResArray(int type) {
            if (type == 1) {
                return BG_WHITE_BATTERY_RES;
            }
            return BATTERY_RES;
        }
    }

    public static class GPSUtils {
        public static boolean isGPSRouteAvailable(int sportType) {
            String[] gpxRouteInfo = GPSSPUtils.getCurrentSelectedGPXRouteBySportType(Global.getApplicationContext(), sportType);
            if (gpxRouteInfo == null || gpxRouteInfo.length < 2) {
                Debug.m5i("test_route", "getTrackInfo, gpx route is null or length less than 2: " + gpxRouteInfo);
                return false;
            } else if (gpxRouteInfo[0] == null || gpxRouteInfo[1] == null) {
                return false;
            } else {
                return true;
            }
        }
    }
}
