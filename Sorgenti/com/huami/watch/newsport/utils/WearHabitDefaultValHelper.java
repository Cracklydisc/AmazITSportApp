package com.huami.watch.newsport.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class WearHabitDefaultValHelper {
    public static int getWearHabitDefVal(Context context) {
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://com.huami.watch.setup.usersettings"), null, null, null, null);
        int wearHabit = 0;
        if (cursor != null && cursor.moveToFirst()) {
            wearHabit = cursor.getInt(6);
        }
        switch (wearHabit) {
            case 0:
                return 1;
            case 1:
                return 0;
            default:
                return -1;
        }
    }
}
