package com.huami.watch.newsport.utils;

import com.huami.watch.common.log.Debug;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TPUtils {
    private static final String TAG = TPUtils.class.getSimpleName();

    public static void notifyKernelBgColor(int bgType) {
        File file = new File("/sys/slpt/apps/slpt-app/res/setting/key/sport_background/data");
        if (!file.exists()) {
            Debug.m4e(TAG, "notifyKernelBgColor, can't find bg file node:" + file.getAbsolutePath());
        } else if (file.exists() && file.canWrite()) {
            try {
                FileInputStream inputStream = new FileInputStream(file);
                byte[] readContent = new byte[1];
                inputStream.read(readContent);
                String content = new String(readContent);
                String bgStr = bgType == 1 ? "1" : "0";
                Debug.m3d(TAG, "notifyKernelBgColor, read bg type " + content + ", going into bg:" + bgStr);
                if (bgStr.equalsIgnoreCase(content)) {
                    Debug.m3d(TAG, "notifyKernelBgColor, same with last result");
                    inputStream.close();
                    return;
                }
                Debug.m3d(TAG, "write bg type: " + bgType);
                FileOutputStream outputStream = new FileOutputStream(file);
                outputStream.write(String.valueOf(bgType).getBytes());
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            } catch (FileNotFoundException e) {
                Debug.m3d(TAG, "bg error :" + e.toString());
            } catch (IOException e2) {
                Debug.m3d(TAG, "bg error :" + e2.toString());
            }
        } else {
            Debug.m3d(TAG, "can not write bg flag" + file.canWrite() + "   file.exists()  " + file.exists());
        }
    }
}
