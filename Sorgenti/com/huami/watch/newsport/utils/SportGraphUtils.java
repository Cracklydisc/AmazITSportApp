package com.huami.watch.newsport.utils;

import android.content.Context;
import android.provider.Settings.System;
import com.huami.watch.newsport.common.model.SportType;

public class SportGraphUtils {

    public final class GraphType {
    }

    public static boolean isNeedShowRealGraphy(int sportType) {
        if (SportType.isMixedSport(sportType)) {
            return false;
        }
        return true;
    }

    public static void setSportDisplayType(Context context, int sportType, int graph) {
        if (SportType.isSwimMode(sportType) && graph == 0) {
            graph = 5;
        }
        System.putInt(context.getContentResolver(), "sport_display_graph_type_" + sportType, graph);
    }
}
