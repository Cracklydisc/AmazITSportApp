package com.huami.watch.newsport.utils;

import android.content.Context;
import android.util.Log;
import com.huami.watch.newsport.customtrain.utils.TrainSpUtils;
import com.huami.watch.newsport.train.model.TrainConfig;
import com.huami.watch.newsport.train.model.TrainGroup;
import com.huami.watch.newsport.train.model.TrainProgram;
import com.huami.watch.newsport.train.model.TrainRemindType;
import com.huami.watch.newsport.train.model.TrainTargetType;
import com.huami.watch.newsport.train.model.TrainUnit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IntertelRunDataParse {
    private static final String TAG = IntertelRunDataParse.class.getSimpleName();
    private static IntertelRunDataParse instance;

    public static IntertelRunDataParse getInstance() {
        if (instance == null) {
            instance = new IntertelRunDataParse();
        }
        return instance;
    }

    public void productTrainProgramFile(Context mContext, String jsonConfig) {
        Log.i(TAG, "productTrainProgramFile: jsonConfig:" + jsonConfig);
        try {
            JSONArray jsonArray = new JSONArray(jsonConfig);
            String beforeFilePath = "/data/data/" + mContext.getPackageName() + "/databases/" + "interval_run";
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject currnetObject = (JSONObject) jsonArray.get(i);
                String title = currnetObject.optString("title");
                if (title != null && title.length() > 0) {
                    String filePath = beforeFilePath + File.separator + title + ".json";
                    Log.i(TAG, " currnetSepeFile:" + filePath);
                    try {
                        File file = new File(filePath);
                        if (!file.exists()) {
                            new File(file.getParent()).mkdirs();
                            file.createNewFile();
                        }
                        FileOutputStream outStream = new FileOutputStream(file);
                        outStream.write(currnetObject.toString().getBytes());
                        outStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public TrainProgram parsetJson(Context mContext) {
        IOException e;
        TrainProgram currentTrainProgram;
        JSONObject jsonObject;
        String title;
        JSONArray groupList;
        int j;
        TrainGroup currnetTrainGroup;
        TrainGroup currnetTrainGroup2;
        JSONObject groupObject;
        String beforeFilePath = "/data/data/" + mContext.getPackageName() + "/databases/" + "interval_run" + File.separator + TrainSpUtils.getCustomIntervttentTrainFileName(mContext);
        StringBuffer buffer = new StringBuffer("");
        try {
            Log.i(TAG, "getLoadFilePath:" + beforeFilePath);
            InputStream inputStream = new FileInputStream(new File(beforeFilePath));
            InputStream inputStream2;
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                while (true) {
                    String str = bufferedReader.readLine();
                    if (str == null) {
                        break;
                    }
                    buffer.append(str);
                }
                inputStream.close();
                inputStream2 = inputStream;
            } catch (IOException e2) {
                e = e2;
                inputStream2 = inputStream;
                e.printStackTrace();
                if (buffer.length() != 0) {
                    return null;
                }
                currentTrainProgram = new TrainProgram();
                try {
                    jsonObject = new JSONObject(buffer.toString());
                    try {
                        title = jsonObject.optString("title");
                        groupList = (JSONArray) jsonObject.opt("groupList");
                        j = 0;
                        currnetTrainGroup = null;
                        while (j < groupList.length()) {
                            try {
                                currnetTrainGroup2 = new TrainGroup();
                                groupObject = (JSONObject) groupList.get(j);
                                currnetTrainGroup2.setRepeatCounts(groupObject.optInt("repeatCount") + 1);
                                currnetTrainGroup2.setTrainUnitList(getTrainUnitByJsonObject(groupObject));
                                currentTrainProgram.addTrainGroup(currnetTrainGroup2);
                                j++;
                                currnetTrainGroup = currnetTrainGroup2;
                            } catch (JSONException e3) {
                                e = e3;
                                currnetTrainGroup2 = currnetTrainGroup;
                                JSONObject jSONObject = jsonObject;
                            }
                        }
                        currnetTrainGroup2 = currnetTrainGroup;
                        return currentTrainProgram;
                    } catch (JSONException e4) {
                        e = e4;
                        jSONObject = jsonObject;
                    }
                } catch (JSONException e5) {
                    JSONException e6;
                    e6 = e5;
                    e6.printStackTrace();
                    return currentTrainProgram;
                }
            }
        } catch (IOException e7) {
            e = e7;
            e.printStackTrace();
            if (buffer.length() != 0) {
                return null;
            }
            currentTrainProgram = new TrainProgram();
            jsonObject = new JSONObject(buffer.toString());
            title = jsonObject.optString("title");
            groupList = (JSONArray) jsonObject.opt("groupList");
            j = 0;
            currnetTrainGroup = null;
            while (j < groupList.length()) {
                currnetTrainGroup2 = new TrainGroup();
                groupObject = (JSONObject) groupList.get(j);
                currnetTrainGroup2.setRepeatCounts(groupObject.optInt("repeatCount") + 1);
                currnetTrainGroup2.setTrainUnitList(getTrainUnitByJsonObject(groupObject));
                currentTrainProgram.addTrainGroup(currnetTrainGroup2);
                j++;
                currnetTrainGroup = currnetTrainGroup2;
            }
            currnetTrainGroup2 = currnetTrainGroup;
            return currentTrainProgram;
        }
        if (buffer.length() != 0) {
            return null;
        }
        currentTrainProgram = new TrainProgram();
        jsonObject = new JSONObject(buffer.toString());
        title = jsonObject.optString("title");
        groupList = (JSONArray) jsonObject.opt("groupList");
        j = 0;
        currnetTrainGroup = null;
        while (j < groupList.length()) {
            currnetTrainGroup2 = new TrainGroup();
            groupObject = (JSONObject) groupList.get(j);
            currnetTrainGroup2.setRepeatCounts(groupObject.optInt("repeatCount") + 1);
            currnetTrainGroup2.setTrainUnitList(getTrainUnitByJsonObject(groupObject));
            currentTrainProgram.addTrainGroup(currnetTrainGroup2);
            j++;
            currnetTrainGroup = currnetTrainGroup2;
        }
        currnetTrainGroup2 = currnetTrainGroup;
        return currentTrainProgram;
    }

    public List<TrainUnit> getTrainUnitByJsonObject(JSONObject jsonObject) {
        JSONException e;
        List<TrainUnit> trainUnitList = new ArrayList();
        JSONArray jsonArray = (JSONArray) jsonObject.opt("itemInfoList");
        int i = 0;
        TrainUnit currentTrainUnit = null;
        while (i < jsonArray.length()) {
            TrainUnit currentTrainUnit2;
            try {
                currentTrainUnit2 = new TrainUnit();
                JSONObject currentObject = (JSONObject) jsonArray.get(i);
                int lengthType = currentObject.optInt("lengthType");
                if (lengthType != -1) {
                    currentTrainUnit2.setUnitType(TrainTargetType.values()[lengthType]);
                    if (lengthType == TrainTargetType.DIATANCE.getType()) {
                        currentTrainUnit2.setTargetDistance(currentObject.optInt("lengthValue"));
                    } else {
                        try {
                            if (lengthType == TrainTargetType.TIME.getType()) {
                                currentTrainUnit2.setTargetTime(currentObject.optLong("lengthValue") * 1000);
                            }
                        } catch (JSONException e2) {
                            e = e2;
                        }
                    }
                    int remindType = currentObject.optInt("reminderType");
                    if (remindType == -1 || remindType == 5) {
                        currentTrainUnit2.addTrainConfig(new TrainConfig(TrainRemindType.NONE));
                    } else if (remindType == 3) {
                        trainConfig = new TrainConfig(TrainRemindType.PACE);
                        JSONArray lengthValues = (JSONArray) currentObject.opt("reminderValue");
                        trainConfig.setMinPace((float) lengthValues.getInt(0));
                        trainConfig.setMaxPace((float) lengthValues.getInt(1));
                        currentTrainUnit2.addTrainConfig(trainConfig);
                    } else if (remindType == 4) {
                        trainConfig = new TrainConfig(TrainRemindType.HEART);
                        JSONArray heartValues = (JSONArray) currentObject.opt("reminderValue");
                        trainConfig.setMinHeart(heartValues.getInt(0));
                        trainConfig.setMaxHeart(heartValues.getInt(1));
                        currentTrainUnit2.addTrainConfig(trainConfig);
                    }
                    trainUnitList.add(currentTrainUnit2);
                }
                i++;
                currentTrainUnit = currentTrainUnit2;
            } catch (JSONException e3) {
                e = e3;
                currentTrainUnit2 = currentTrainUnit;
            }
        }
        return trainUnitList;
        e.printStackTrace();
        return trainUnitList;
    }
}
