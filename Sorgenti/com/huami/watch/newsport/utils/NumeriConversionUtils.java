package com.huami.watch.newsport.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumeriConversionUtils {
    public static float convertStringToFloat(String value) {
        if (value.contains("NaN") || value.contains("Inf")) {
            value = "0";
        }
        return new BigDecimal(value).floatValue();
    }

    public static float convertStringToFloat(String valueA, int digit) {
        if (valueA.contains("NaN") || valueA.contains("Inf")) {
            valueA = "0";
        }
        return new BigDecimal(valueA).setScale(digit, RoundingMode.HALF_UP).floatValue();
    }

    public static float getSubtractResult(float valueA, float valueB) {
        if (Float.isNaN(valueA) || Float.isInfinite(valueA)) {
            valueA = 0.0f;
        }
        if (Float.isNaN(valueB) || Float.isInfinite(valueB)) {
            valueA = 0.0f;
        }
        return new BigDecimal(String.valueOf(valueA)).subtract(new BigDecimal(String.valueOf(valueB))).setScale(2, 4).floatValue();
    }

    public static double getSubtractResult(String valueA, String valueB) {
        if (valueA.contains("NaN") || valueA.contains("Inf")) {
            valueA = "0";
        }
        if (valueB.contains("NaN") || valueB.contains("Inf")) {
            valueA = "0";
        }
        return new BigDecimal(valueA).subtract(new BigDecimal(valueB)).doubleValue();
    }

    public static double getDoubleValue(String value, int digit) {
        if (value.contains("NaN") || value.contains("Inf")) {
            value = "0";
        }
        return new BigDecimal(value).setScale(digit, 4).doubleValue();
    }

    public static float getFloatValue(String value, int digit) {
        if (value.contains("NaN") || value.contains("Inf")) {
            value = "0";
        }
        return new BigDecimal(value).setScale(digit, 4).floatValue();
    }

    public static double getDoubleValue(String value) {
        if (value.contains("NaN") || value.contains("Inf")) {
            value = "0";
        }
        return new BigDecimal(value).doubleValue();
    }

    public static float getAddResult(float valueA, float valueB) {
        if (Float.isNaN(valueA) || Float.isInfinite(valueA)) {
            valueA = 0.0f;
        }
        if (Float.isNaN(valueB) || Float.isInfinite(valueB)) {
            valueA = 0.0f;
        }
        return new BigDecimal((double) valueA).add(new BigDecimal((double) valueB)).setScale(2, 4).floatValue();
    }

    public static double getMulResult(String valueA, String valueB) {
        if (valueA.contains("NaN") || valueA.contains("Inf")) {
            valueA = "0";
        }
        if (valueB.contains("NaN") || valueB.contains("Inf")) {
            valueA = "0";
        }
        return new BigDecimal(valueA).multiply(new BigDecimal(valueB)).doubleValue();
    }

    public static double getDivResult(String valueA, String valueB) {
        if (valueA.contains("NaN") || valueA.contains("Inf")) {
            valueA = "0";
        }
        if (valueB.contains("NaN") || valueB.contains("Inf")) {
            valueA = "0";
        }
        return new BigDecimal(valueA).divide(new BigDecimal(valueB), 2, RoundingMode.FLOOR).doubleValue();
    }
}
