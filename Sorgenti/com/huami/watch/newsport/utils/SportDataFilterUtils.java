package com.huami.watch.newsport.utils;

public class SportDataFilterUtils {
    public static float parsePace(float pace) {
        return parsePace(pace, true);
    }

    public static float parsePace(float pace, boolean isAvoidMinPace) {
        float secondPerKm = (float) UnitConvertUtils.convertPaceToMinOrKmPerMile((double) (pace * 1000.0f));
        if (secondPerKm >= 5999.0f || secondPerKm <= 0.0f) {
            return ((secondPerKm >= 5999.0f || secondPerKm <= 0.0f) && isAvoidMinPace) ? 5.999f : 0.0f;
        } else {
            if (UnitConvertUtils.isImperial()) {
                return secondPerKm / 1000.0f;
            }
            return pace;
        }
    }

    public static float parsePacePer100Meter(float pace) {
        return parsePacePer100Meter(pace, 0);
    }

    public static float parsePacePer100Meter(float pace, int unit) {
        float secondPer100Meter;
        if (unit == 0) {
            secondPer100Meter = pace * 100.0f;
        } else {
            secondPer100Meter = (float) (UnitConvertUtils.convertpace2MeterOrYD((double) pace, unit) * 100.0d);
        }
        if (secondPer100Meter >= 5999.0f || secondPer100Meter <= 0.0f) {
            return 0.0f;
        }
        if (unit != 0) {
            return secondPer100Meter / 100.0f;
        }
        if (UnitConvertUtils.isImperial()) {
            return secondPer100Meter / 100.0f;
        }
        return pace;
    }

    public static float parseHeartRate(float heartRate) {
        return heartRate;
    }

    public static float parseSpeed(float speed) {
        return speed;
    }

    public static float parseStepFreq(float stepFreq) {
        return stepFreq;
    }
}
