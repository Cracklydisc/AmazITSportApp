package com.huami.watch.newsport.utils;

import android.content.Context;
import android.os.SystemProperties;
import android.text.TextUtils;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.SportApplication;
import com.huami.watch.newsport.common.model.SportType;

public class UnitConvertUtils {
    public static double convertSpeedToKmPerHour(double speed) {
        return convertSpeedToMileOrKmPerHour(3.6d * speed);
    }

    public static double convertSpeedToMeterPerMin(double speed) {
        return convertSpeedToFtOrMeter(60.0d * speed);
    }

    public static double convertDistanceToKm(double distance) {
        return convertDistanceToMileOrKm(distance / 1000.0d);
    }

    public static int convertStepFreqToTimesPerMin(double stepFreq) {
        return (int) Math.round(60.0d * stepFreq);
    }

    public static double convertCalorieToKilocalorie(double calorie) {
        return calorie / 1000.0d;
    }

    public static double convertSpeedToMileOrKmPerHour(double speed) {
        if (isImperial()) {
            return speed * 0.6213712d;
        }
        return speed;
    }

    public static double convertPaceToMinOrKmPerMile(double pace) {
        if (isImperial()) {
            return pace / 0.6213712d;
        }
        return pace;
    }

    public static double convertDistanceToMileOrKm(double distance) {
        if (isImperial()) {
            return distance * 0.6213712d;
        }
        return distance;
    }

    public static double convertSpeedToFtOrMeter(double speed) {
        if (isImperial()) {
            return speed * 3.28084d;
        }
        return speed;
    }

    public static double convertSpeedToMeter(double speed) {
        if (isImperial()) {
            return speed / 3.28084d;
        }
        return speed;
    }

    public static double convertDistanceToFtOrMeter(double distance, int sportType) {
        if (isImperial() && SportType.isSupportImperial(sportType)) {
            return distance * 3.28084d;
        }
        return distance;
    }

    public static double convertDistanceToMeter(double distance, int sportType) {
        if (isImperial() && SportType.isSupportImperial(sportType)) {
            return distance / 3.28084d;
        }
        return distance;
    }

    public static double convertDistanceFromMileOrKmtokm(double distance) {
        if (isImperial()) {
            return distance / 0.6213712d;
        }
        return distance;
    }

    public static double convertDistanceFromMileOrKmtokm(double distance, boolean isImperial) {
        if (isImperial) {
            return distance / 0.6213712d;
        }
        return distance;
    }

    public static double convertPaceFromMileOrKmTokm(double pace) {
        if (isImperial()) {
            return pace * 0.6213712d;
        }
        return pace;
    }

    public static double convertDistanceToKm(double distance, boolean isMetric) {
        return convertDistanceToMileOrKm(distance / 1000.0d, isMetric);
    }

    public static double convertDistanceToMileOrKm(double distance, boolean isMetric) {
        if (isMetric) {
            return distance;
        }
        return distance * 0.6213712d;
    }

    public static double convertDistanceToFtOrMeter(double distance, boolean isMetric) {
        if (isMetric) {
            return distance;
        }
        return distance * 3.28084d;
    }

    public static double convertDistance2YDOrMeter(double distance, int unit) {
        if (unit == 0) {
            return distance;
        }
        if (unit == 1) {
            return distance * 1.0936d;
        }
        return 0.0d;
    }

    public static double convertpace2MeterOrYD(double pace, int unit) {
        if (unit != 0 && unit == 1) {
            return pace / 1.0936d;
        }
        return pace;
    }

    public static double convertYDOrMeter2Meter(double distance, int unit) {
        if (unit != 0 && unit == 1) {
            return distance / 1.0936d;
        }
        return distance;
    }

    public static double convertCm2InOrCm(double distance) {
        if (isImperial()) {
            return distance * 0.39370079d;
        }
        return distance;
    }

    public static boolean isImperial() {
        return !SportApplication.mIsMetric;
    }

    public static boolean isOverSeaVersion() {
        return TextUtils.equals(SystemProperties.get("ro.build.oversea", "0"), "1");
    }

    public static boolean isZh() {
        if (Global.getApplicationContext().getResources().getConfiguration().locale.getLanguage().endsWith("zh")) {
            return true;
        }
        return false;
    }

    public static boolean isUseNewUploadMethod(Context context) {
        return SportApplication.mIsUseNewUploadMethod && ModelUtil.isBoudType_iOS(context);
    }

    public static boolean isHuangheMode() {
        return ModelUtil.isModelHuanghe(Global.getApplicationContext());
    }
}
