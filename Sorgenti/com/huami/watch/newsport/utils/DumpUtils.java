package com.huami.watch.newsport.utils;

public class DumpUtils {
    private static final String TAG = DumpUtils.class.getSimpleName();

    public static String println(int[] array) {
        StringBuilder sb = new StringBuilder();
        if (array == null || array.length == 0) {
            sb.append(" has not data ");
            return sb.toString();
        }
        sb.append("[");
        for (int current : array) {
            sb.append(current).append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        sb.append("]");
        return sb.toString();
    }
}
