package com.huami.watch.newsport.utils;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.hs.gpxparser.utils.ByteUtils;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.SportThaInfo;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.db.FitnessLevelDBManager;
import com.huami.watch.newsport.ui.firstbeathelp.FirstBeatHelpActivity;
import com.huami.watch.sensor.HmSensorHubConfigManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class FirstBeatDataUtils {
    private static final String TAG = FirstBeatDataUtils.class.getSimpleName();

    static class C08811 implements Comparator<SportThaInfo> {
        C08811() {
        }

        public int compare(SportThaInfo arg0, SportThaInfo arg1) {
            return arg0.getDayId() > arg1.getDayId() ? -1 : 1;
        }
    }

    public static int getTELevelByValue(float teFloatValue) {
        return (int) Math.abs(Math.floor((double) teFloatValue));
    }

    public static String getRealTimePerPormanceLevel(Context mContext, float realTimeValue) {
        if (realTimeValue >= -20.0f && realTimeValue < -10.0f) {
            return mContext.getResources().getString(C0532R.string.real_time_perpormance_level_fatigued);
        }
        if (realTimeValue >= -10.0f && realTimeValue < -5.0f) {
            return mContext.getResources().getString(C0532R.string.real_time_perpormance_level_fatigued);
        }
        if (realTimeValue >= -5.0f && realTimeValue < 5.0f) {
            return mContext.getResources().getString(C0532R.string.real_time_perpormance_level_average);
        }
        if (realTimeValue >= 5.0f && realTimeValue < 10.0f) {
            return mContext.getResources().getString(C0532R.string.real_time_perpormance_level_good);
        }
        if (realTimeValue < 10.0f || realTimeValue > 20.0f) {
            return null;
        }
        return mContext.getResources().getString(C0532R.string.real_time_perpormance_level_good);
    }

    public static int getFitnessLevelByVo2Max(Context mContext, int vo2maxValue) {
        UserInfo userInfo = UserInfoManager.getInstance().getUserInfo(mContext);
        FitnessLevelDBManager dbManager = new FitnessLevelDBManager(mContext.getApplicationContext());
        SQLiteDatabase sqLiteDatabase = dbManager.initDBManager(mContext.getPackageName());
        int gender = userInfo.getGender() == 0 ? 0 : 1;
        int age = UserInfoManager.getUserAgeByUserInfo(userInfo);
        if (age < 20) {
            age = 20;
        } else if (age > 65) {
            age = 65;
        }
        int level = dbManager.getFitnessLevel(sqLiteDatabase, gender, age, vo2maxValue);
        Log.i(TAG, "-- fitNessLevel:" + level + ",gender:" + gender + ",age:" + age + ",level:" + level + ",vo2Max:" + vo2maxValue);
        if (sqLiteDatabase != null) {
            sqLiteDatabase.close();
        }
        return level;
    }

    public static int[] getFitnessLevelArrayValueByVo2maxAge(Context mContext, int vo2maxValue) {
        UserInfo userInfo = UserInfoManager.getInstance().getUserInfo(mContext);
        FitnessLevelDBManager dbManager = new FitnessLevelDBManager(mContext.getApplicationContext());
        SQLiteDatabase sqLiteDatabase = dbManager.initDBManager(mContext.getPackageName());
        int gender = userInfo.getGender() == 0 ? 0 : 1;
        int age = UserInfoManager.getUserAgeByUserInfo(userInfo);
        if (age < 20) {
            age = 20;
        } else if (age > 65) {
            age = 65;
        }
        return dbManager.getFitnessLevelArrayByAge(sqLiteDatabase, age, gender);
    }

    public static long getEveryDayTrainLoadIdByCurrnetTime(long currnetTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(currnetTime);
        cal.set(11, 12);
        cal.set(13, 0);
        cal.set(12, 0);
        cal.set(14, 0);
        Log.i(TAG, "currentTime:" + currnetTime + ",calendarTime:" + cal.getTimeInMillis());
        return cal.getTimeInMillis();
    }

    public static String getDayTimeFormat(long currentTime) {
        return new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(currentTime));
    }

    public static long getDayIdByDataStr(String dataStr) {
        try {
            Date date = new SimpleDateFormat(" yyyy-MM-dd ").parse(new StringBuffer().append(" ").append(dataStr).append(" ").toString());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(date.getTime());
            cal.set(11, 12);
            cal.set(13, 0);
            cal.set(12, 0);
            cal.set(14, 0);
            return cal.getTimeInMillis();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void jumpToFirstBeatHelpPage(Context mContext, int categoryType) {
        Intent intent = new Intent(mContext, FirstBeatHelpActivity.class);
        intent.putExtra("help_type_category", categoryType);
        mContext.startActivity(intent);
    }

    public static String getSuggessTextByCurrentLoad(Context mCOntext, int weeklyTrainLoadSum, int minValue, int maxValue, int overReachValue) {
        String result = "";
        if (weeklyTrainLoadSum < 0) {
            return mCOntext.getResources().getString(C0532R.string.train_load_suggess_status_no_record);
        }
        if (weeklyTrainLoadSum <= minValue) {
            return mCOntext.getResources().getString(C0532R.string.train_load_suggess_status_less);
        }
        if (weeklyTrainLoadSum <= maxValue) {
            return mCOntext.getResources().getString(C0532R.string.train_load_suggess_status_good);
        }
        if (weeklyTrainLoadSum < overReachValue) {
            return mCOntext.getResources().getString(C0532R.string.train_load_suggess_status_recovery);
        }
        if (weeklyTrainLoadSum > overReachValue) {
            return mCOntext.getResources().getString(C0532R.string.train_load_suggess_status_over);
        }
        return result;
    }

    public static int getRoundingValue(float value) {
        return (int) (((double) value) + 0.5d);
    }

    public static void setTrainLoadToSensorhub(Context mContext, List<SportThaInfo> sportThaInfos) {
        if (sportThaInfos == null || sportThaInfos.size() == 0) {
            Log.i(TAG, " setTrainLoadToSensorhub  data is empty ");
            return;
        }
        Log.i(TAG, " setTrainLoadToSensorhub size:" + sportThaInfos.size());
        List<SportThaInfo> list = new ArrayList();
        list.addAll(sportThaInfos);
        Collections.sort(list, new C08811());
        int sportThaDataSize = list.size();
        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
        outBuffer.write(4);
        int currnetTime = (int) (((SportThaInfo) list.get(0)).getDayId() / 1000);
        ByteArrayOutputStream wtlSumBytes = new ByteArrayOutputStream();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        stringBuffer.append(4).append(",");
        stringBuffer.append(currnetTime).append("][");
        StringBuilder wtlSumBuffer = new StringBuilder();
        wtlSumBuffer.append("[");
        try {
            outBuffer.write(ByteUtils.intToBytes4(currnetTime));
            for (int i = 0; i < 7; i++) {
                if (i <= sportThaDataSize - 1) {
                    SportThaInfo currnetSportThaInfo = (SportThaInfo) list.get(i);
                    outBuffer.write(ByteUtils.intToBytes4(currnetSportThaInfo.getCurrnetDayTrainLoad()));
                    wtlSumBytes.write(ByteUtils.intToBytes2(currnetSportThaInfo.getWtlSum()));
                    stringBuffer.append(currnetSportThaInfo.getCurrnetDayTrainLoad()).append(",");
                    wtlSumBuffer.append(currnetSportThaInfo.getWtlSum()).append(",");
                } else {
                    outBuffer.write(ByteUtils.intToBytes4(0));
                    wtlSumBytes.write(ByteUtils.intToBytes2(0));
                    stringBuffer.append("0").append(",");
                    wtlSumBuffer.append("0").append(",");
                }
            }
            outBuffer.write(wtlSumBytes.toByteArray());
            stringBuffer.append("]").append(wtlSumBuffer.toString()).append("]");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, " sportThaInfo to sensorhub:" + stringBuffer.toString());
        HmSensorHubConfigManager.getHmSensorHubConfigManager(mContext).syncGpxTrailData(outBuffer.toByteArray(), outBuffer.toByteArray().length);
    }

    public static void setVo2maxToSensorhub(Context mContext, float currnetRunVo2max, float currnetWalkingVo2max) {
        ByteArrayOutputStream vo2maxBytes = new ByteArrayOutputStream();
        vo2maxBytes.write(3);
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(3).append(",");
        if (currnetRunVo2max < 0.0f) {
            currnetRunVo2max = 0.0f;
        }
        if (currnetWalkingVo2max < 0.0f) {
            currnetWalkingVo2max = 0.0f;
        }
        try {
            vo2maxBytes.write(ByteUtils.intToBytes2((int) (currnetRunVo2max * 100.0f)));
            vo2maxBytes.write(ByteUtils.intToBytes2((int) (currnetWalkingVo2max * 100.0f)));
            sb.append((int) (currnetRunVo2max * 100.0f)).append(",");
            sb.append((int) (currnetWalkingVo2max * 100.0f)).append("]");
            Log.i(TAG, " setVo2maxToSensorHub:" + sb.toString());
            HmSensorHubConfigManager.getHmSensorHubConfigManager(mContext).syncGpxTrailData(vo2maxBytes.toByteArray(), vo2maxBytes.toByteArray().length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
