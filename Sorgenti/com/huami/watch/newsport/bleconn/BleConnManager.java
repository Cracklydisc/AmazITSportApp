package com.huami.watch.newsport.bleconn;

import android.content.Context;
import com.huami.ble.core.manager.BleLocalAdapter;
import com.huami.ble.core.manager.BluetoothLeProfile;
import com.huami.ble.core.manager.BluetoothLeProfile.LeServiceListener;
import com.huami.ble.core.manager.HmBlecoreManager;
import com.huami.watch.common.log.Debug;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BleConnManager {
    private static final String TAG = BleConnManager.class.getName();
    private static BleConnManager sInstance = null;
    private List<IBleCoreConnCallback> mBleListners = new ArrayList();
    private boolean mIsSportStarted = false;
    private Map<Integer, Boolean> mManagerBindStateMap = new HashMap();
    private Map<Integer, BluetoothLeProfile> mManagerMap = new HashMap();

    private final class BlecoreServiceListener implements LeServiceListener {
        private BlecoreServiceListener() {
        }

        public void onServiceConnected(int id, BluetoothLeProfile proxy) {
            Debug.m3d(BleConnManager.TAG, "Bluetooth service connected, " + proxy);
            BluetoothLeProfile profile = (BluetoothLeProfile) BleConnManager.this.mManagerMap.get(Integer.valueOf(id));
            if (profile != null) {
                profile.cleanup();
                BleConnManager.this.mManagerMap.remove(Integer.valueOf(id));
            }
            BleConnManager.this.mManagerMap.put(Integer.valueOf(id), proxy);
            BleConnManager.this.notifyBleConnState(id, 100);
        }

        public void onServiceDisconnected(int id) {
            Debug.m3d(BleConnManager.TAG, "Bluetooth service disconnected");
            BluetoothLeProfile profile = (BluetoothLeProfile) BleConnManager.this.mManagerMap.remove(Integer.valueOf(id));
            BleConnManager.this.notifyBleConnState(id, -1);
        }
    }

    public interface IBleCoreConnCallback {
        void onConnectStateChange(int i, int i2);
    }

    public static synchronized BleConnManager getInstance() {
        BleConnManager bleConnManager;
        synchronized (BleConnManager.class) {
            if (sInstance == null) {
                sInstance = new BleConnManager();
            }
            bleConnManager = sInstance;
        }
        return bleConnManager;
    }

    private BleConnManager() {
    }

    private boolean getDeviceState(int id) {
        Boolean isState = (Boolean) this.mManagerBindStateMap.get(Integer.valueOf(id));
        if (isState == null) {
            return false;
        }
        return isState.booleanValue();
    }

    public boolean isBound(int id) {
        return getDeviceState(id) && this.mManagerMap.get(Integer.valueOf(id)) != null;
    }

    public int getDeviceConnState(int id, int profile) {
        Debug.m5i(TAG, "getDeviceConnState, id:" + id + ", profile:" + profile);
        if (isBound(0)) {
            switch (id) {
                case 0:
                    return ((HmBlecoreManager) this.mManagerMap.get(Integer.valueOf(id))).getProfileConnectionState(profile);
                default:
                    return 0;
            }
        }
        Debug.m5i(TAG, "getDeviceConnState, not bound");
        return 0;
    }

    public void notifyBleSportStart(int sportType) {
        Debug.m5i(TAG, "notifyBleSportStart, init：" + isBound(0));
        if (!isBound(0)) {
            Debug.m5i(TAG, "notifyBleSportStart, not bound");
        }
        if (this.mIsSportStarted) {
            Debug.m5i(TAG, "notifyBleSportStart aborted, already started");
            return;
        }
        ((HmBlecoreManager) this.mManagerMap.get(Integer.valueOf(0))).sportReady(sportType);
        this.mIsSportStarted = true;
    }

    public void notifyBleSportStop(int sportType) {
        Debug.m5i(TAG, "notifyBleSportStop, init：" + isBound(0));
        if (!isBound(0)) {
            Debug.m5i(TAG, "notifyBleSportStop, not bound");
        }
        if (this.mIsSportStarted) {
            HmBlecoreManager profile = (HmBlecoreManager) this.mManagerMap.get(Integer.valueOf(0));
            if (profile != null) {
                profile.sportUnready(sportType);
            }
            this.mIsSportStarted = false;
            return;
        }
        Debug.m5i(TAG, "notifyBleSportStop aborted, not yet start");
    }

    public synchronized void registerBleConnListener(IBleCoreConnCallback callback) {
        this.mBleListners.add(callback);
    }

    public synchronized void unRegisterBleConnListener(IBleCoreConnCallback callback) {
        this.mBleListners.remove(callback);
    }

    public void bindBleService(Context context, int id) {
        BluetoothLeProfile profile = (BluetoothLeProfile) this.mManagerMap.get(Integer.valueOf(id));
        if (!getDeviceState(id) || profile == null) {
            if (profile != null) {
                profile.cleanup();
                this.mManagerMap.remove(Integer.valueOf(id));
                this.mManagerBindStateMap.put(Integer.valueOf(id), Boolean.valueOf(false));
            }
            this.mManagerBindStateMap.put(Integer.valueOf(id), Boolean.valueOf(true));
            BleLocalAdapter.getAdapter().getManager(context.getApplicationContext(), new BlecoreServiceListener(), id);
            return;
        }
        notifyBleConnState(id, 100);
    }

    public void unbindBleService(int id) {
        this.mManagerBindStateMap.put(Integer.valueOf(id), Boolean.valueOf(false));
        BluetoothLeProfile profile = (BluetoothLeProfile) this.mManagerMap.remove(Integer.valueOf(id));
        if (profile != null) {
            profile.cleanup();
        }
    }

    private synchronized void notifyBleConnState(int id, int state) {
        List<IBleCoreConnCallback> tempList = new ArrayList();
        tempList.addAll(this.mBleListners);
        Iterator<IBleCoreConnCallback> iterator = tempList.iterator();
        while (iterator != null && iterator.hasNext()) {
            IBleCoreConnCallback callback = (IBleCoreConnCallback) iterator.next();
            if (callback != null) {
                callback.onConnectStateChange(id, state);
            }
        }
    }
}
