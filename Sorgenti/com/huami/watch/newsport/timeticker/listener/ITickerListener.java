package com.huami.watch.newsport.timeticker.listener;

public interface ITickerListener {
    void onTicker(int i);
}
