package com.huami.watch.newsport.timeticker.action;

import com.huami.watch.newsport.timeticker.listener.ITickerListener;
import java.util.concurrent.atomic.AtomicInteger;

public class SportTickerImpl implements ISportTicker, ITickerRemind {
    private boolean isMillStop;
    private long mMillSecIntervalTime;
    private BaseTicker mMillTicker;
    private AtomicInteger mMillValue;
    private BaseTicker mPauseTicker;
    private long mSecIntervalTime;
    private BaseTicker mSecTicker;
    private ITickerRemind mSpotTickerListener;
    private TICKER_STATE mTickerState;
    private final ITickerListener onMillTickerListener;
    private final ITickerListener onPauseTickerListener;
    private final ITickerListener onSecTickerListener;

    class C06661 implements ITickerListener {
        C06661() {
        }

        public void onTicker(int millSecond) {
            SportTickerImpl.this.onTicker((int) (((long) millSecond) / 1000));
            if (((long) millSecond) >= 5000 && !SportTickerImpl.this.isMillStop) {
                SportTickerImpl.this.mMillTicker.stopTicker();
                SportTickerImpl.this.isMillStop = true;
            }
        }
    }

    class C06672 implements ITickerListener {
        C06672() {
        }

        public void onTicker(int millSecond) {
            if (SportTickerImpl.this.mMillValue.incrementAndGet() >= 100) {
                SportTickerImpl.this.mMillValue.set(0);
            }
            SportTickerImpl.this.onMillTicker(millSecond);
        }
    }

    class C06683 implements ITickerListener {
        C06683() {
        }

        public void onTicker(int millSecond) {
            SportTickerImpl.this.onPauseTicker((int) (((long) millSecond) / 1000));
        }
    }

    private enum TICKER_STATE {
        START,
        PAUSE,
        CONTINUE,
        STOP
    }

    public SportTickerImpl() {
        this.mSecTicker = null;
        this.mMillTicker = null;
        this.mPauseTicker = null;
        this.mTickerState = TICKER_STATE.STOP;
        this.mSecIntervalTime = 1000;
        this.mMillSecIntervalTime = 10;
        this.isMillStop = false;
        this.onSecTickerListener = new C06661();
        this.onMillTickerListener = new C06672();
        this.onPauseTickerListener = new C06683();
        this.mSecTicker = new BaseTicker();
        this.mMillTicker = new BaseTicker();
        this.mPauseTicker = new BaseTicker();
        this.mSecTicker.setTickerListener(this.onSecTickerListener);
        this.mMillTicker.setTickerListener(this.onMillTickerListener);
        this.mPauseTicker.setTickerListener(this.onPauseTickerListener);
        this.mMillValue = new AtomicInteger(0);
    }

    public void startTicker(long startTime) {
        this.mSecTicker.setIntervalTime(this.mSecIntervalTime);
        this.mMillTicker.setIntervalTime(this.mMillSecIntervalTime);
        this.mPauseTicker.setIntervalTime(this.mSecIntervalTime);
        this.mSecTicker.startTicker(startTime);
        this.mMillTicker.startTicker(startTime);
        this.mPauseTicker.stopTicker();
        this.mTickerState = TICKER_STATE.START;
        this.isMillStop = false;
    }

    public void pauseTicker() {
        this.mSecTicker.pauseTicker();
        this.mMillTicker.pauseTicker();
        this.mPauseTicker.startTicker();
        this.mTickerState = TICKER_STATE.PAUSE;
    }

    public void continueTicker() {
        this.mSecTicker.continueTicker();
        this.mMillTicker.continueTicker();
        this.mPauseTicker.pauseTicker();
        this.mTickerState = TICKER_STATE.CONTINUE;
    }

    public void stopTicker() {
        this.mSecTicker.stopTicker();
        this.mMillTicker.stopTicker();
        this.mPauseTicker.stopTicker();
        this.mTickerState = TICKER_STATE.STOP;
    }

    public void setSportTickerListener(ITickerRemind listener) {
        this.mSpotTickerListener = listener;
    }

    public void notifySportDisplayStatus(boolean isForeground) {
        if (isForeground) {
            this.mSecTicker.setIntervalTime(this.mSecIntervalTime);
        } else {
            this.mSecTicker.setIntervalTime(5000);
        }
        int status = this.mSecTicker.getTimeTaskStatus();
        if (status == 3 || status == 1) {
            this.mSecTicker.restartTicker();
        }
    }

    public void onTicker(int sec) {
        if (this.mSpotTickerListener != null) {
            this.mSpotTickerListener.onTicker(sec);
            if (!this.isMillStop) {
                this.mMillValue.set(0);
            }
        }
    }

    public void onMillTicker(int millSec) {
        if (this.mSpotTickerListener != null) {
            this.mSpotTickerListener.onMillTicker(this.mMillValue.intValue());
        }
    }

    public void onPauseTicker(int sec) {
        if (this.mSpotTickerListener != null) {
            this.mSpotTickerListener.onPauseTicker(sec);
        }
    }
}
