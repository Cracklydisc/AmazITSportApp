package com.huami.watch.newsport.timeticker.action;

public interface ISportTicker extends ITickerAction {
    void notifySportDisplayStatus(boolean z);

    void setSportTickerListener(ITickerRemind iTickerRemind);
}
