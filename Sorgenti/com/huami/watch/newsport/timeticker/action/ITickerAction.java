package com.huami.watch.newsport.timeticker.action;

public interface ITickerAction {
    void continueTicker();

    void pauseTicker();

    void startTicker(long j);

    void stopTicker();
}
