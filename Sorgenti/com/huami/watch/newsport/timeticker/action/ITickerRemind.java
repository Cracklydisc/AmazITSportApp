package com.huami.watch.newsport.timeticker.action;

public interface ITickerRemind {
    void onMillTicker(int i);

    void onPauseTicker(int i);

    void onTicker(int i);
}
