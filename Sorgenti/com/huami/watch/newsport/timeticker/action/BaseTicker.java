package com.huami.watch.newsport.timeticker.action;

import android.os.SystemClock;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.timeticker.listener.ITickerListener;
import java.util.Timer;
import java.util.TimerTask;

public class BaseTicker implements ITickerAction {
    private long lastFreezingTime = 0;
    private long mIntervalTime = 0;
    private boolean mIsPaused = false;
    private boolean mIsStopped = true;
    private long mPauseTime = 0;
    private long mStartTime = 0;
    private int mStatus = 0;
    private TimerTask mTasks;
    private ITickerListener mTickListener;
    private Timer mTimer;
    private int timeElapsed = 0;

    class C06651 implements Runnable {
        C06651() {
        }

        public void run() {
            Debug.m5i("edward", "restartTicker");
            long time = SystemClock.elapsedRealtime();
            BaseTicker.this.lastFreezingTime = SystemClock.elapsedRealtime();
            BaseTicker.this.timeElapsed = (int) ((time - BaseTicker.this.mStartTime) - BaseTicker.this.mPauseTime);
            if (BaseTicker.this.mTickListener != null) {
                Debug.m5i("edward", "restartTicker1");
                BaseTicker.this.mTickListener.onTicker(BaseTicker.this.timeElapsed);
            }
        }
    }

    private class SportTimerTask extends TimerTask {
        private SportTimerTask() {
        }

        public void run() {
            if (!BaseTicker.this.mIsPaused && !BaseTicker.this.mIsStopped) {
                long time = SystemClock.elapsedRealtime();
                BaseTicker.this.lastFreezingTime = SystemClock.elapsedRealtime();
                BaseTicker.this.timeElapsed = (int) ((time - BaseTicker.this.mStartTime) - BaseTicker.this.mPauseTime);
                if (BaseTicker.this.mTickListener != null) {
                    BaseTicker.this.mTickListener.onTicker(BaseTicker.this.timeElapsed);
                }
            }
        }
    }

    public long getDelayTime() {
        return 0;
    }

    public void startTicker() {
        startTicker(SystemClock.elapsedRealtime());
    }

    public void startTicker(long startTime) {
        this.mStatus = 1;
        if (isRunning()) {
            stopTicker();
        }
        this.mTimer = new Timer();
        this.mTasks = new SportTimerTask();
        this.mIsStopped = false;
        this.mIsPaused = false;
        if (startTime == -1) {
            startTime = SystemClock.elapsedRealtime();
        }
        this.lastFreezingTime = startTime;
        this.mStartTime = startTime;
        this.mTimer.schedule(this.mTasks, getDelayTime(), this.mIntervalTime);
    }

    public void pauseTicker() {
        if (!this.mIsStopped && !this.mIsPaused) {
            this.mStatus = 2;
            this.lastFreezingTime = SystemClock.elapsedRealtime();
            this.mIsPaused = true;
            this.mTasks.cancel();
        }
    }

    public void continueTicker() {
        if (!this.mIsStopped) {
            this.mStatus = 3;
            this.mIsPaused = false;
            long pauseTime = SystemClock.elapsedRealtime();
            this.mPauseTime += pauseTime - this.lastFreezingTime;
            this.lastFreezingTime = pauseTime;
            this.mTasks = new SportTimerTask();
            this.mTimer.schedule(this.mTasks, getDelayTime(), this.mIntervalTime);
        }
    }

    public void stopTicker() {
        if (!this.mIsStopped) {
            this.mStatus = 0;
            this.mIsStopped = true;
            this.mTimer.cancel();
            this.mTimer.purge();
            this.mTimer = null;
            this.mTasks = null;
            this.timeElapsed = 0;
            this.lastFreezingTime = 0;
            this.mPauseTime = 0;
            this.mStartTime = 0;
        }
    }

    public void setTickerListener(ITickerListener listener) {
        this.mTickListener = listener;
    }

    public void setIntervalTime(long intervalTime) {
        this.mIntervalTime = intervalTime;
    }

    public boolean isRunning() {
        if (this.mIsStopped) {
            return false;
        }
        return true;
    }

    public void restartTicker() {
        if (this.mTasks != null) {
            this.mTasks.cancel();
        }
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer.purge();
        }
        this.mTimer = new Timer();
        this.mTasks = new SportTimerTask();
        this.mIsStopped = false;
        this.mIsPaused = false;
        this.mTimer.schedule(this.mTasks, getDelayTime(), this.mIntervalTime);
        Global.getGlobalWorkHandler().post(new C06651());
    }

    public int getTimeTaskStatus() {
        return this.mStatus;
    }
}
