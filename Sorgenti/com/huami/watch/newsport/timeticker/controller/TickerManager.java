package com.huami.watch.newsport.timeticker.controller;

import com.huami.watch.newsport.timeticker.action.ISportTicker;
import com.huami.watch.newsport.timeticker.action.ITickerRemind;
import com.huami.watch.newsport.timeticker.action.SportTickerImpl;

public class TickerManager implements ISportTicker {
    private static ISportTicker mSportTicker;

    public static synchronized ISportTicker getInstance() {
        ISportTicker iSportTicker;
        synchronized (TickerManager.class) {
            if (mSportTicker == null) {
                mSportTicker = new SportTickerImpl();
            }
            iSportTicker = mSportTicker;
        }
        return iSportTicker;
    }

    public void startTicker(long startTime) {
        mSportTicker.startTicker(startTime);
    }

    public void pauseTicker() {
        mSportTicker.pauseTicker();
    }

    public void continueTicker() {
        mSportTicker.continueTicker();
    }

    public void stopTicker() {
        mSportTicker.stopTicker();
    }

    public void setSportTickerListener(ITickerRemind listener) {
        mSportTicker.setSportTickerListener(listener);
    }

    public void notifySportDisplayStatus(boolean isForeground) {
        mSportTicker.notifySportDisplayStatus(isForeground);
    }
}
