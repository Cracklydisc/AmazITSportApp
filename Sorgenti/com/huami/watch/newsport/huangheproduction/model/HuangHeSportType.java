package com.huami.watch.newsport.huangheproduction.model;

public class HuangHeSportType {
    public static int transferSportTypeFromHuangHe2Zhufeng(int sportType) {
        switch (sportType) {
            case 1:
                return 1;
            case 2:
                return 6;
            case 3:
                return 7;
            case 4:
                return 8;
            case 5:
                return 9;
            case 6:
                return 10;
            case 7:
                return 12;
            case 8:
                return 13;
            default:
                return -1;
        }
    }

    public static int transferSportTypeFromZhuFeng2Huanghe(int sportType) {
        switch (sportType) {
            case 1:
                return 1;
            case 6:
                return 2;
            case 7:
                return 3;
            case 8:
                return 4;
            case 9:
                return 5;
            case 10:
                return 6;
            case 12:
                return 7;
            case 13:
                return 8;
            default:
                return -1;
        }
    }
}
