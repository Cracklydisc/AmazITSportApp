package com.huami.watch.newsport.huangheproduction.model;

import com.huami.watch.newsport.utils.LogUtil;
import java.util.ArrayList;
import java.util.List;

public class HuangHeSportInfoOrderManagerWrapper {
    private static final String TAG = HuangHeSportInfoOrderManagerWrapper.class.getName();

    public interface IHuangHeSportSortTransfer {
        int[] getTransferSportOrder(int[] iArr);
    }

    private static class CrossingInfo implements IHuangHeSportSortTransfer {
        private CrossingInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(3));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(4));
                        break;
                    case 5:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(6));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(7));
                        break;
                    case 8:
                        orders.add(Integer.valueOf(8));
                        break;
                    case 9:
                        orders.add(Integer.valueOf(9));
                        break;
                    case 10:
                        orders.add(Integer.valueOf(24));
                        break;
                    case 11:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 12:
                        orders.add(Integer.valueOf(11));
                        break;
                    case 13:
                        orders.add(Integer.valueOf(12));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class EppiticalInfo implements IHuangHeSportSortTransfer {
        private EppiticalInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(6));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class IndoorRidingInfo implements IHuangHeSportSortTransfer {
        private IndoorRidingInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(6));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class IndoorRunningInfo implements IHuangHeSportSortTransfer {
        private IndoorRunningInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(3));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 5:
                        orders.add(Integer.valueOf(6));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(12));
                        break;
                    case 8:
                        orders.add(Integer.valueOf(4));
                        break;
                    case 9:
                        orders.add(Integer.valueOf(11));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class MountaineerInfo implements IHuangHeSportSortTransfer {
        private MountaineerInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(7));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(8));
                        break;
                    case 5:
                        orders.add(Integer.valueOf(9));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(13));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(24));
                        break;
                    case 8:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 9:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 10:
                        orders.add(Integer.valueOf(11));
                        break;
                    case 11:
                        orders.add(Integer.valueOf(6));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static final class OutdoorRidingInfo implements IHuangHeSportSortTransfer {
        private OutdoorRidingInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(11));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(7));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(8));
                        break;
                    case 8:
                        orders.add(Integer.valueOf(13));
                        break;
                    case 9:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 10:
                        orders.add(Integer.valueOf(6));
                        break;
                    case 11:
                        orders.add(Integer.valueOf(9));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class RunningInfo implements IHuangHeSportSortTransfer {
        private RunningInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(3));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 5:
                        orders.add(Integer.valueOf(6));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(12));
                        break;
                    case 8:
                        orders.add(Integer.valueOf(4));
                        break;
                    case 9:
                        orders.add(Integer.valueOf(11));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    private static class WalkingInfo implements IHuangHeSportSortTransfer {
        private WalkingInfo() {
        }

        public int[] getTransferSportOrder(int[] older) {
            if (older == null) {
                return null;
            }
            List<Integer> orders = new ArrayList();
            for (int valueOf : older) {
                Integer number = Integer.valueOf(valueOf);
                switch (number.intValue()) {
                    case 1:
                        orders.add(Integer.valueOf(2));
                        break;
                    case 2:
                        orders.add(Integer.valueOf(1));
                        break;
                    case 3:
                        orders.add(Integer.valueOf(6));
                        break;
                    case 4:
                        orders.add(Integer.valueOf(5));
                        break;
                    case 5:
                        orders.add(Integer.valueOf(10));
                        break;
                    case 6:
                        orders.add(Integer.valueOf(11));
                        break;
                    case 7:
                        orders.add(Integer.valueOf(14));
                        break;
                    default:
                        LogUtil.m9i(true, HuangHeSportInfoOrderManagerWrapper.TAG, "no this type:" + number);
                        break;
                }
            }
            int[] orderArray = new int[orders.size()];
            for (int i = 0; i < orders.size(); i++) {
                orderArray[i] = ((Integer) orders.get(i)).intValue();
            }
            return orderArray;
        }
    }

    public static int[] getTransferSportOrder(int sportType, int[] older) {
        IHuangHeSportSortTransfer transfer = null;
        switch (HuangHeSportType.transferSportTypeFromZhuFeng2Huanghe(sportType)) {
            case 1:
                transfer = new RunningInfo();
                break;
            case 2:
                transfer = new WalkingInfo();
                break;
            case 3:
                transfer = new CrossingInfo();
                break;
            case 4:
                transfer = new IndoorRunningInfo();
                break;
            case 5:
                transfer = new OutdoorRidingInfo();
                break;
            case 6:
                transfer = new IndoorRidingInfo();
                break;
            case 7:
                transfer = new EppiticalInfo();
                break;
            case 8:
                transfer = new MountaineerInfo();
                break;
            default:
                LogUtil.m9i(true, TAG, "err sport_type:" + sportType);
                break;
        }
        if (transfer != null) {
            return transfer.getTransferSportOrder(older);
        }
        return null;
    }
}
