package com.huami.watch.newsport.huangheproduction.dbtransfer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.model.OutdoorSportSummary;
import com.huami.watch.newsport.common.model.SportSummary;
import com.huami.watch.newsport.db.dao.SportSummaryDao;
import com.huami.watch.newsport.huangheproduction.model.HuangHeSportType;
import com.huami.watch.newsport.utils.LogUtil;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class HuangHeDBTransferManager {
    private static final String TAG = HuangHeDBTransferManager.class.getName();
    private static HuangHeDBTransferManager sInstance = null;
    private Context mContext = null;

    private boolean transferSportHeartRateData(android.database.sqlite.SQLiteDatabase r31) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x012e in list []
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r30 = this;
        r11 = 0;
        r29 = new java.util.LinkedList;
        r29.<init>();
        r3 = "heart_rate";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r5 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r6 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r7 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r8 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r9 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r10 = 0;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r31;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r11 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "time";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r25 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "track_id";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r26 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "rate";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r16 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "step_freq";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r22 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "altitude";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r14 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "pace";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r19 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "heart_quality";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r17 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "step_count";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r21 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "stride";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r23 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "run_time";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r20 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "disdiff";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r15 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "heart_range";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r18 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = "stroke_speed";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r24 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0063:
        r2 = r11.moveToNext();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        if (r2 == 0) goto L_0x012f;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0069:
        r28 = new com.huami.watch.newsport.common.model.HeartRate;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r28.<init>();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        if (r25 < 0) goto L_0x007b;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0070:
        r0 = r25;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setTimestamp(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x007b:
        if (r26 < 0) goto L_0x0088;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x007d:
        r0 = r26;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setTrackId(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0088:
        if (r16 < 0) goto L_0x0096;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x008a:
        r0 = r16;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = (int) r2;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setHeartRate(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0096:
        if (r22 < 0) goto L_0x00a3;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0098:
        r0 = r22;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setStepFreq(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00a3:
        if (r14 < 0) goto L_0x00ae;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00a5:
        r2 = r11.getInt(r14);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setAltitude(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00ae:
        if (r19 < 0) goto L_0x00bb;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00b0:
        r0 = r19;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setPace(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00bb:
        if (r17 < 0) goto L_0x00c8;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00bd:
        r0 = r17;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setHeartQuality(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00c8:
        if (r21 < 0) goto L_0x00d5;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00ca:
        r0 = r21;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setStepDiff(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00d5:
        if (r23 < 0) goto L_0x00e2;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00d7:
        r0 = r23;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setStride(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00e2:
        if (r20 < 0) goto L_0x00ef;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00e4:
        r0 = r20;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setRunTime(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00ef:
        if (r15 < 0) goto L_0x00fa;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00f1:
        r2 = r11.getFloat(r15);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setDistance(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00fa:
        if (r18 < 0) goto L_0x0107;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x00fc:
        r0 = r18;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setHeartRange(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0107:
        if (r24 < 0) goto L_0x0114;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0109:
        r0 = r24;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.setStrokeSpeed(r2);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
    L_0x0114:
        r0 = r29;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r1 = r28;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r0.add(r1);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        goto L_0x0063;
    L_0x011d:
        r12 = move-exception;
        r2 = TAG;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r3 = r12.getLocalizedMessage();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        com.huami.watch.common.log.Debug.m6w(r2, r3);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r27 = 0;
        if (r11 == 0) goto L_0x012e;
    L_0x012b:
        r11.close();
    L_0x012e:
        return r27;
    L_0x012f:
        r2 = 1;
        r3 = TAG;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4.<init>();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r5 = "heart rate size:";	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r5 = r29.size();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        r4 = r4.toString();	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);	 Catch:{ Exception -> 0x011d, all -> 0x017f }
        if (r11 == 0) goto L_0x0151;
    L_0x014e:
        r11.close();
    L_0x0151:
        r0 = r30;
        r2 = r0.mContext;
        r13 = com.huami.watch.newsport.db.dao.HeartRateDao.getInstance(r2);
        r0 = r30;
        r2 = r0.mContext;
        r0 = r29;
        r27 = r13.insertAll(r2, r0);
        r2 = 1;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "transferSportHeartRateData, isSuccess:";
        r4 = r4.append(r5);
        r0 = r27;
        r4 = r4.append(r0);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        goto L_0x012e;
    L_0x017f:
        r2 = move-exception;
        if (r11 == 0) goto L_0x0185;
    L_0x0182:
        r11.close();
    L_0x0185:
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.huangheproduction.dbtransfer.HuangHeDBTransferManager.transferSportHeartRateData(android.database.sqlite.SQLiteDatabase):boolean");
    }

    private boolean transferSportKmInfoData(android.database.sqlite.SQLiteDatabase r19) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x00ce in list []
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r18 = this;
        r12 = 0;
        r16 = new java.util.LinkedList;
        r16.<init>();
        r3 = "running_info_per_km";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r5 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r6 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r7 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r8 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r9 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r10 = 0;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r2 = r19;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r12 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = -1;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0016:
        r2 = r12.moveToNext();	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r2 == 0) goto L_0x00cf;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x001c:
        r15 = new com.huami.watch.newsport.common.model.RunningInfoPerKM;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.<init>();	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r2 = "cost_time";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x0030;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0029:
        r2 = r12.getLong(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setCostTime(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0030:
        r2 = "pace";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x003f;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0038:
        r2 = r12.getDouble(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setPace(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x003f:
        r2 = "speed";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x004e;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0047:
        r2 = r12.getDouble(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setSpeed(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x004e:
        r2 = "track_id";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x005d;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0056:
        r2 = r12.getLong(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setTrackId(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x005d:
        r2 = "total_cost_time";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x006c;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0065:
        r2 = r12.getLong(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setTotalCostTime(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x006c:
        r2 = "lat";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x007b;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0074:
        r2 = r12.getFloat(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setLatitude(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x007b:
        r2 = "lng";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x008a;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0083:
        r2 = r12.getFloat(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setLongitude(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x008a:
        r2 = "unit";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x0099;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0092:
        r2 = r12.getInt(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setUnit(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x0099:
        r2 = "tx_value";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x00a8;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00a1:
        r2 = r12.getInt(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setTeValue(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00a8:
        r2 = "daily_perpormence";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r11 = r12.getColumnIndex(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r11 < 0) goto L_0x00b7;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00b0:
        r2 = r12.getFloat(r11);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r15.setDailyPorpermence(r2);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00b7:
        r0 = r16;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r0.add(r15);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        goto L_0x0016;
    L_0x00be:
        r13 = move-exception;
        r2 = com.huami.watch.newsport.Global.DEBUG_LEVEL_3;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r2 == 0) goto L_0x00c8;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00c3:
        r2 = TAG;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        com.huami.watch.common.log.Debug.printException(r2, r13);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
    L_0x00c8:
        r14 = 0;
        if (r12 == 0) goto L_0x00ce;
    L_0x00cb:
        r12.close();
    L_0x00ce:
        return r14;
    L_0x00cf:
        r2 = 1;
        r3 = TAG;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4.<init>();	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r5 = "per km info size:";	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r5 = r16.size();	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        r4 = r4.toString();	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);	 Catch:{ Exception -> 0x00be, all -> 0x011f }
        if (r12 == 0) goto L_0x00f1;
    L_0x00ee:
        r12.close();
    L_0x00f1:
        r0 = r18;
        r2 = r0.mContext;
        r17 = com.huami.watch.newsport.db.dao.RunningInfoPerKMDao.getInstance(r2);
        r0 = r18;
        r2 = r0.mContext;
        r0 = r17;
        r1 = r16;
        r14 = r0.insertAll(r2, r1);
        r2 = 1;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "transferSportKmInfoData, isSuccess:";
        r4 = r4.append(r5);
        r4 = r4.append(r14);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        goto L_0x00ce;
    L_0x011f:
        r2 = move-exception;
        if (r12 == 0) goto L_0x0125;
    L_0x0122:
        r12.close();
    L_0x0125:
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.huangheproduction.dbtransfer.HuangHeDBTransferManager.transferSportKmInfoData(android.database.sqlite.SQLiteDatabase):boolean");
    }

    private boolean transferSportLocationData(android.database.sqlite.SQLiteDatabase r30) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0114 in list []
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:42)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:42)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r29 = this;
        r11 = 0;
        r28 = new java.util.LinkedList;
        r28.<init>();
        r3 = "location_data";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r5 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r6 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r7 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r8 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r9 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r10 = 0;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r30;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r11 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "track_id";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r25 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "latitude";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r19 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "longitude";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r20 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "accuracy";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r14 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "altitude";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r16 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "timestamp";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r24 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "point_type";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r22 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "speed";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r23 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "point_index";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r21 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "bar";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r17 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "course";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r18 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = "algo_point_type";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r15 = r11.getColumnIndex(r2);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x005d:
        r2 = r11.moveToNext();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r2 == 0) goto L_0x0115;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0063:
        r12 = new com.huami.watch.newsport.gps.model.SportLocationData;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.<init>();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r25;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x0075;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x006d:
        r0 = r25;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mTrackId = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0075:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r19;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x0082;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x007a:
        r0 = r19;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mLatitude = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0082:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r20;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x008f;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0087:
        r0 = r20;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mLongitude = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x008f:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r14 == r2) goto L_0x0098;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0092:
        r2 = r11.getFloat(r14);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mGPSAccuracy = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x0098:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r16;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00a5;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x009d:
        r0 = r16;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mAltitude = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00a5:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r24;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00b2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00aa:
        r0 = r24;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getLong(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mTimestamp = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00b2:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r22;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00bf;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00b7:
        r0 = r22;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mPointType = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00bf:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r23;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00cc;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00c4:
        r0 = r23;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mSpeed = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00cc:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r21;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00d9;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00d1:
        r0 = r21;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mPointIndex = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00d9:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r17;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00e6;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00de:
        r0 = r17;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getInt(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mBar = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00e6:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0 = r18;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r0 == r2) goto L_0x00f3;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00eb:
        r0 = r18;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r2 = r11.getFloat(r0);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mCourse = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00f3:
        r2 = -1;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r15 == r2) goto L_0x00fc;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00f6:
        r2 = r11.getInt(r15);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r12.mAlgoPointType = r2;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
    L_0x00fc:
        r0 = r28;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r0.add(r12);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        goto L_0x005d;
    L_0x0103:
        r13 = move-exception;
        r2 = TAG;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r3 = r13.getLocalizedMessage();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        com.huami.watch.common.log.Debug.m6w(r2, r3);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r26 = 0;
        if (r11 == 0) goto L_0x0114;
    L_0x0111:
        r11.close();
    L_0x0114:
        return r26;
    L_0x0115:
        r2 = 1;
        r3 = TAG;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4.<init>();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r5 = "location size:";	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r5 = r28.size();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4 = r4.append(r5);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        r4 = r4.toString();	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);	 Catch:{ Exception -> 0x0103, all -> 0x0167 }
        if (r11 == 0) goto L_0x0137;
    L_0x0134:
        r11.close();
    L_0x0137:
        r0 = r29;
        r2 = r0.mContext;
        r27 = com.huami.watch.newsport.db.dao.LocationDataDao.getInstance(r2);
        r0 = r29;
        r2 = r0.mContext;
        r0 = r27;
        r1 = r28;
        r26 = r0.insertAll(r2, r1);
        r2 = 1;
        r3 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "transferSportLocationData, isSuccess:";
        r4 = r4.append(r5);
        r0 = r26;
        r4 = r4.append(r0);
        r4 = r4.toString();
        com.huami.watch.newsport.utils.LogUtil.m9i(r2, r3, r4);
        goto L_0x0114;
    L_0x0167:
        r2 = move-exception;
        if (r11 == 0) goto L_0x016d;
    L_0x016a:
        r11.close();
    L_0x016d:
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.newsport.huangheproduction.dbtransfer.HuangHeDBTransferManager.transferSportLocationData(android.database.sqlite.SQLiteDatabase):boolean");
    }

    private HuangHeDBTransferManager(Context applicationContext) {
        this.mContext = applicationContext;
    }

    public static synchronized HuangHeDBTransferManager getInstance(Context context) {
        HuangHeDBTransferManager huangHeDBTransferManager;
        synchronized (HuangHeDBTransferManager.class) {
            if (sInstance == null) {
                sInstance = new HuangHeDBTransferManager(context.getApplicationContext());
            }
            huangHeDBTransferManager = sInstance;
        }
        return huangHeDBTransferManager;
    }

    public void beginDBTransfer() {
        if (Environment.getExternalStorageState().equalsIgnoreCase("mounted")) {
            File sdcardDiretory = Environment.getExternalStorageDirectory();
            if (sdcardDiretory == null || !sdcardDiretory.exists()) {
                LogUtil.m9i(true, TAG, "beginDBTransfer sdcard is not exist," + sdcardDiretory);
                return;
            }
            String path = sdcardDiretory.getAbsolutePath() + File.separator + "sport_data.db";
            File file = new File(path);
            if (file == null || !file.exists() || file.isDirectory()) {
                LogUtil.m9i(true, TAG, "beginDBTransfer db is not exist," + file);
                return;
            }
            SQLiteDatabase database = SQLiteDatabase.openDatabase(path, null, 0);
            if (database == null) {
                LogUtil.m9i(true, TAG, "beginDBTransfer database is null");
                return;
            }
            LogUtil.m9i(true, TAG, "beginDBTransfer, db:" + database.isReadOnly());
            int version = database.getVersion();
            LogUtil.m9i(true, TAG, "beginDBTransfer, version:" + version);
            if (version < 17) {
                file.delete();
                return;
            }
            boolean isSummaryTransferSuccess = transferSummaryData(database);
            boolean isHeartTransferSuccess = transferSportHeartRateData(database);
            boolean isLocationTransferSuccess = transferSportLocationData(database);
            boolean isKmInfoTransferSuccess = transferSportKmInfoData(database);
            if (isSummaryTransferSuccess && isHeartTransferSuccess && isLocationTransferSuccess && isKmInfoTransferSuccess) {
                LogUtil.m9i(true, TAG, "beginDBTransfer, delete db:" + file.delete());
                return;
            }
            return;
        }
        LogUtil.m9i(true, TAG, "beginDBTransfer database is not mount");
    }

    private boolean transferSummaryData(SQLiteDatabase db) {
        List<SportSummary> result = new LinkedList();
        Cursor cursor = null;
        try {
            SportSummary summary;
            cursor = db.query("sport_summary", null, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                String content = "";
                int columnIndex = cursor.getColumnIndex("content");
                if (columnIndex != -1) {
                    content = cursor.getString(columnIndex);
                }
                int currentStatus = 2;
                columnIndex = cursor.getColumnIndex("current_status");
                if (columnIndex != -1) {
                    currentStatus = cursor.getInt(columnIndex);
                }
                summary = OutdoorSportSummary.createFromJSON(content);
                if (summary != null) {
                    summary.setCurrentStatus(currentStatus);
                    result.add(summary);
                }
            }
            for (SportSummary summary2 : result) {
                summary2.setSportType(HuangHeSportType.transferSportTypeFromHuangHe2Zhufeng(summary2.getSportType()));
                LogUtil.m9i(true, TAG, "summary:" + summary2.toString());
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            if (Global.DEBUG_LEVEL_3) {
                Debug.printException(TAG, e);
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
        boolean isSuccess = SportSummaryDao.getInstance(this.mContext).insertAll(this.mContext, result);
        LogUtil.m9i(true, TAG, "transferSummaryData, isSuccess:" + isSuccess);
        return isSuccess;
    }
}
