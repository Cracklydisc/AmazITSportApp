package com.huami.watch.newsport.setting.tennicclaphand;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.BaseActivity;
import com.huami.watch.utils.ClickScaleAnimation;

public class TennicSelectHandActivity extends BaseActivity implements OnClickListener {
    private static final String TAG = TennicSelectHandActivity.class.getSimpleName();
    private BaseConfig config = null;
    private LinearLayout llLeftHandLayout;
    private LinearLayout llRightHandLayout;
    private int mSportType = -1;

    class C06171 extends AnimatorListenerAdapter {
        C06171() {
        }

        public void onAnimationEnd(Animator animation) {
            TennicSelectHandActivity.this.config.setTennicClapThaHands(0);
            DataManager.getInstance().setSportConfig(TennicSelectHandActivity.this, TennicSelectHandActivity.this.config);
            DataManager.getInstance().setIsFirstTennicClapHand(false);
            Intent tennicWearIntent = new Intent(TennicSelectHandActivity.this, TennicWearGuideActivity.class);
            tennicWearIntent.setFlags(268435456);
            tennicWearIntent.putExtra("tennic_hand_type", 0);
            TennicSelectHandActivity.this.startActivityForResult(tennicWearIntent, 5381);
        }
    }

    class C06182 extends AnimatorListenerAdapter {
        C06182() {
        }

        public void onAnimationEnd(Animator animation) {
            TennicSelectHandActivity.this.config.setTennicClapThaHands(1);
            DataManager.getInstance().setSportConfig(TennicSelectHandActivity.this, TennicSelectHandActivity.this.config);
            DataManager.getInstance().setIsFirstTennicClapHand(false);
            Intent rightHandIntent = new Intent(TennicSelectHandActivity.this, TennicWearGuideActivity.class);
            rightHandIntent.setFlags(268435456);
            rightHandIntent.putExtra("tennic_hand_type", 1);
            TennicSelectHandActivity.this.startActivityForResult(rightHandIntent, 5381);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.dialog_tennic_select_hand_remind);
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        this.llLeftHandLayout = (LinearLayout) findViewById(C0532R.id.ll_left_hand);
        this.llRightHandLayout = (LinearLayout) findViewById(C0532R.id.ll_right_hand);
        this.llLeftHandLayout.setOnClickListener(this);
        this.llRightHandLayout.setOnClickListener(this);
        this.config = DataManager.getInstance().getSportConfig(this, this.mSportType);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case C0532R.id.ll_left_hand:
                new ClickScaleAnimation(v, new C06171()).startClickAnimation();
                return;
            case C0532R.id.ll_right_hand:
                new ClickScaleAnimation(v, new C06182()).startClickAnimation();
                return;
            default:
                return;
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5381) {
            Log.i(TAG, "TennicConfig:onActivityResult");
            setResult(-1);
            finish();
        }
    }
}
