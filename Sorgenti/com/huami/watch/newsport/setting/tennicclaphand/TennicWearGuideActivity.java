package com.huami.watch.newsport.setting.tennicclaphand;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.BaseActivity;

public class TennicWearGuideActivity extends BaseActivity {
    private static final String TAG = TennicWearGuideActivity.class.getSimpleName();
    private AlphaAnimation alphaAnimation;
    private ImageView bgImagView;
    private int currnetHandType = -1;
    private ImageView handImageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0532R.layout.activity_tennic_wear_guide);
        this.bgImagView = (ImageView) findViewById(C0532R.id.bg_img);
        this.handImageView = (ImageView) findViewById(C0532R.id.hand_img);
        this.currnetHandType = getIntent().getIntExtra("tennic_hand_type", -1);
        this.alphaAnimation = null;
        switch (this.currnetHandType) {
            case 0:
                this.bgImagView.setBackgroundResource(C0532R.drawable.sport_tennis_animate_left_2);
                this.handImageView.setBackgroundResource(C0532R.drawable.sport_tennis_animate_left);
                this.alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(this, C0532R.anim.tennic_wear_hand_alpha);
                this.handImageView.startAnimation(this.alphaAnimation);
                return;
            case 1:
                this.bgImagView.setBackgroundResource(C0532R.drawable.sport_tennis_animate_right_2);
                this.handImageView.setBackgroundResource(C0532R.drawable.sport_tennis_animate_right);
                this.alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(this, C0532R.anim.tennic_wear_hand_alpha);
                this.handImageView.startAnimation(this.alphaAnimation);
                return;
            default:
                return;
        }
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
        if (this.alphaAnimation != null) {
            this.alphaAnimation.cancel();
        }
        Intent dataIntent = new Intent();
        dataIntent.putExtra("tennic_hand_type", this.currnetHandType);
        setResult(-1, dataIntent);
        finish();
    }

    protected void onDestroy() {
        super.onDestroy();
    }
}
