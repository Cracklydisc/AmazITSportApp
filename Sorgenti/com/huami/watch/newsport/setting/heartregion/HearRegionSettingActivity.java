package com.huami.watch.newsport.setting.heartregion;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.ui.BaseActivity;

public class HearRegionSettingActivity extends BaseActivity {
    private static final String TAG = HearRegionSettingActivity.class.getSimpleName();
    private int mSportType = -1;
    private HearRegionSettingAdapter settingAdapter;
    private HmMenuView view = null;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        if (this.mSportType != -1) {
            this.view = new HmMenuView(this, null, C0532R.string.setting_sport_heart_region_remind);
            this.settingAdapter = new HearRegionSettingAdapter(this, null, this.mSportType);
            this.view.setSimpleMenuAdapter(this.settingAdapter);
            setContentView(this.view);
            this.view.scrollToTop();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                LogUtils.print(TAG, "onTouchEvent ACTION_DOWN ");
                break;
            case 1:
                LogUtils.print(TAG, "onTouchEvent ACTION UP ");
                break;
            case 2:
                LogUtils.print(TAG, "onTouchEvent ACTION MOVE ");
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.settingAdapter != null && requestCode == 304) {
            LogUtils.print(TAG, "onActivityResult:" + requestCode + "," + resultCode);
            this.settingAdapter.onDataChanged();
            setResult(-1);
        }
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
