package com.huami.watch.newsport.setting.heartregion;

import android.content.Context;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.sensor.HmSensorHubConfigManager;

public class HeartRegionUtils {
    private static final String TAG = HeartRegionUtils.class.getSimpleName();
    private static HeartRegionUtils instance;

    public static HeartRegionUtils getInstance() {
        if (instance == null) {
            instance = new HeartRegionUtils();
        }
        return instance;
    }

    public static void setSensorHubInfo(Context mContext, boolean isOpen, int minHeartRegion, int maxHeartRegion) {
        LogUtils.print(TAG, "setSensorHubInfo:isOpen:" + isOpen + ",minHeartRegion:" + minHeartRegion + ",maxHeartRegion:" + maxHeartRegion);
        HmSensorHubConfigManager.getHmSensorHubConfigManager(mContext).setEnable(11, isOpen, (float) minHeartRegion, (float) maxHeartRegion);
    }
}
