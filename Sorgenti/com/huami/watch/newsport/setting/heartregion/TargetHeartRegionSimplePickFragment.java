package com.huami.watch.newsport.setting.heartregion;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.fragpicker.AbsSportPickerFragment;
import java.util.Calendar;

public class TargetHeartRegionSimplePickFragment extends AbsSportPickerFragment {
    private static final String TAG = TargetHeartRegionSimplePickFragment.class.getSimpleName();
    private HeartRegionAdapter mAdapter;
    private BaseConfig mConfig;
    private int mInitIndex = -1;
    private int[] maxList = new int[5];
    private int[] minList = new int[5];
    private int userAge;
    private UserInfo userInfo;

    class HeartRegionAdapter extends AbsPickerViewAdapter {
        public HeartRegionAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 1;
        }

        public int getInitRow(int i) {
            return TargetHeartRegionSimplePickFragment.this.mInitIndex;
        }

        public float getTextSize() {
            return super.getTextSize() / 3.0f;
        }

        public float getColumnWidth(int column) {
            return 125.0f;
        }

        public int rowCount(int i) {
            return TargetHeartRegionSimplePickFragment.this.minList.length;
        }

        public boolean showItemHint() {
            return super.showItemHint();
        }

        public boolean showItemBottom() {
            return true;
        }

        public String getItemBottomStringByPosition(int position) {
            StringBuffer sb = new StringBuffer();
            sb.append(TargetHeartRegionSimplePickFragment.this.minList[position]).append("-").append(TargetHeartRegionSimplePickFragment.this.maxList[position]).append(" bpm");
            LogUtils.print(TargetHeartRegionSimplePickFragment.TAG, "getItemBottomStringByPosition:" + sb.toString());
            return sb.toString();
        }

        public String getUnit(int column) {
            return "";
        }

        public String getDisplayString(int row, int column) {
            return "区间" + (row + 1);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        this.userInfo = UserInfoManager.getInstance().getUserInfo(getActivity());
        int currentYear = Calendar.getInstance().get(1);
        if (this.userInfo == null || this.userInfo.getYear() <= 0) {
            this.userAge = 25;
        } else {
            this.userAge = currentYear - this.userInfo.getYear();
        }
        if (this.mConfig.ismHeartRegionRemind()) {
            this.mInitIndex = this.mConfig.getmHeartRegionType() - 1;
        } else {
            this.mInitIndex = -1;
        }
        Log.i(TAG, " isHeartRegionRemind:" + this.mConfig.ismHeartRegionRemind() + ",mInitIndex:" + this.mInitIndex);
        this.minList[0] = ((220 - this.userAge) * 50) / 100;
        this.maxList[0] = ((220 - this.userAge) * 60) / 100;
        this.minList[1] = ((220 - this.userAge) * 60) / 100;
        this.maxList[1] = ((220 - this.userAge) * 70) / 100;
        this.minList[2] = ((220 - this.userAge) * 70) / 100;
        this.maxList[2] = ((220 - this.userAge) * 80) / 100;
        this.minList[3] = ((220 - this.userAge) * 80) / 100;
        this.maxList[3] = ((220 - this.userAge) * 90) / 100;
        this.minList[4] = ((220 - this.userAge) * 90) / 100;
        this.maxList[4] = ((220 - this.userAge) * 100) / 100;
        for (int i = 0; i < 5; i++) {
            LogUtils.print(TAG, "run minList index:" + i + " , min:" + this.minList[i] + ",max:" + this.maxList[i]);
        }
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new HeartRegionAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onSave(int[] selectedRow) {
        LogUtils.print(TAG, "onSave： minIndex:" + selectedRow[0]);
        if (this.mConfig != null) {
            this.mConfig.setmHeartRegionValueMin(this.minList[selectedRow[0]]);
            this.mConfig.setmHeartRegionValueMax(this.maxList[selectedRow[0]]);
            this.mConfig.setmHeartRegionRemind(true);
            this.mConfig.setmHeartRegionType(selectedRow[0] + 1);
            DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
            this.mListener.onSelectFinish();
        }
    }

    protected void onClose() {
        super.onClose();
        this.mConfig.setmHeartRegionRemind(false);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
