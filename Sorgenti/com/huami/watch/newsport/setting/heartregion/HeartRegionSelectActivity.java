package com.huami.watch.newsport.setting.heartregion;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.ui.BaseActivity;

public class HeartRegionSelectActivity extends BaseActivity implements IConfigListener {
    static int RESULT_CODE = 256;
    private static final String TAG = HeartRegionSelectActivity.class.getSimpleName();
    private int currnetSportType = -1;
    private HeartRegionSelectAdapter selectAdapter;
    private HmMenuView view;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.currnetSportType = getIntent().getIntExtra("sport_type", -1);
        if (this.currnetSportType != -1) {
            this.view = new HmMenuView(this, null);
            this.selectAdapter = new HeartRegionSelectAdapter(this, this.currnetSportType);
            this.selectAdapter.setListener(this);
            this.view.setSimpleMenuAdapter(this.selectAdapter);
            setContentView(this.view);
            this.view.scrollToTop();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.print(TAG, "onActivityResult");
        if (requestCode == RESULT_CODE) {
            setResult(-1);
            this.selectAdapter.updateCustomRegion();
        }
    }

    public void onConfigChange() {
        LogUtils.print(TAG, "onConfigChange");
        setResult(-1);
        finish();
    }

    protected void onResume() {
        super.onResume();
        LogUtils.print(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        LogUtils.print(TAG, "onPause");
    }
}
