package com.huami.watch.newsport.setting.heartregion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HearRegionSettingAdapter extends BaseConfigMenuAdapter {
    private static final String TAG = HearRegionSettingAdapter.class.getSimpleName();
    private int currentYear = 0;
    private List<MenuInfo> setttingMenuInfo = new ArrayList();
    private int userAge;
    private UserInfo userInfo;

    public HearRegionSettingAdapter(Activity context, IConfigListener listener, int sportType) {
        super(context, listener, sportType);
    }

    private void initHeartRegion() {
        this.userInfo = UserInfoManager.getInstance().getUserInfo(this.mMenuActivity);
        this.currentYear = Calendar.getInstance().get(1);
        if (this.userInfo == null || this.userInfo.getYear() < 0) {
            this.userAge = 25;
        } else {
            this.userAge = this.currentYear - this.userInfo.getYear();
        }
        Log.i(TAG, " isHeartRegionRemind:" + this.mConfig.ismHeartRegionRemind());
        this.minList[0] = ((220 - this.userAge) * 50) / 100;
        this.maxList[0] = ((220 - this.userAge) * 60) / 100;
        this.minList[1] = ((220 - this.userAge) * 60) / 100;
        this.maxList[1] = ((220 - this.userAge) * 70) / 100;
        this.minList[2] = ((220 - this.userAge) * 70) / 100;
        this.maxList[2] = ((220 - this.userAge) * 80) / 100;
        this.minList[3] = ((220 - this.userAge) * 80) / 100;
        this.maxList[3] = ((220 - this.userAge) * 90) / 100;
        this.minList[4] = ((220 - this.userAge) * 90) / 100;
        this.maxList[4] = ((220 - this.userAge) * 100) / 100;
        for (int i = 0; i < 5; i++) {
            LogUtils.print(TAG, "run minList index:" + i + " , min:" + this.minList[i] + ",max:" + this.maxList[i]);
        }
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        initHeartRegion();
        this.setttingMenuInfo = new ArrayList();
        MenuInfo info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_value_closed);
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.heart_region_warm_up_and_relax);
        StringBuilder sb = new StringBuilder();
        sb.append(this.minList[0]).append("-").append(this.maxList[0]).append(" bpm");
        info.content = sb.toString();
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(1);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.heart_region_fat_burning);
        sb = new StringBuilder();
        sb.append(this.minList[1]).append("-").append(this.maxList[1]).append(" bpm");
        info.content = sb.toString();
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(2);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.heart_region_heart_and_lung_enhancement);
        sb = new StringBuilder();
        sb.append(this.minList[2]).append("-").append(this.maxList[2]).append(" bpm");
        info.content = sb.toString();
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(3);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.heart_region_endurance_enhancement);
        sb = new StringBuilder();
        sb.append(this.minList[3]).append("-").append(this.maxList[3]).append(" bpm");
        info.content = sb.toString();
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(4);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.heart_region_anaerobic＿limit);
        sb = new StringBuilder();
        sb.append(this.minList[4]).append("-").append(this.maxList[4]).append(" bpm");
        info.content = sb.toString();
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(5);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.title = this.mMenuActivity.getString(C0532R.string.setting_sport_heart_region_custom);
        if (this.mConfig.getmHeartRegionType() == 5 && this.mConfig.ismHeartRegionRemind()) {
            sb = new StringBuilder();
            BaseConfig baseConfig = this.mConfig;
            sb.append(BaseConfig.getCustomMinHeartRate()).append("-").append(this.mConfig.getmHeartRegionValueMax()).append(" bpm");
            info.content = sb.toString();
        } else {
            info.content = null;
        }
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(6);
        this.setttingMenuInfo.add(info);
        return this.setttingMenuInfo;
    }

    public void omMenuItemClick(int position) {
        clickMenuItem(position);
    }

    private void clickMenuItem(int position) {
        switch (position) {
            case 0:
                this.mConfig.setmHeartRegionRemind(false);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                this.mConfig.setmHeartRegionRemind(true);
                this.mConfig.setmHeartRegionValueMin(this.minList[position - 1]);
                this.mConfig.setmHeartRegionValueMax(this.maxList[position - 1]);
                this.mConfig.setmHeartRegionType(position - 1);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 6:
                Intent graphIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                graphIntent.putExtra("selector_type", "select_heart_region_remind");
                graphIntent.putExtra("sport_type", this.mSportType);
                this.mMenuActivity.startActivityForResult(graphIntent, 304);
                return;
            default:
                return;
        }
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < this.mMenuList.size(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 6:
                    info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
                    info.title = this.mMenuActivity.getString(C0532R.string.setting_sport_heart_region_custom);
                    if (this.mConfig.getmHeartRegionType() != 5 || !this.mConfig.ismHeartRegionRemind()) {
                        info.content = null;
                        break;
                    }
                    StringBuilder sb = new StringBuilder();
                    BaseConfig baseConfig = this.mConfig;
                    sb.append(BaseConfig.getCustomMinHeartRate()).append("-").append(this.mConfig.getmHeartRegionValueMax()).append(" bpm");
                    info.content = sb.toString();
                    break;
                    break;
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
