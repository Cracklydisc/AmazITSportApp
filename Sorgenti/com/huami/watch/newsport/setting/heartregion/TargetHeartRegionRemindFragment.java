package com.huami.watch.newsport.setting.heartregion;

import android.content.Context;
import android.os.Bundle;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.widget.HmPickerView.AbsPickerViewAdapter;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.fragpicker.AbsSportPickerFragment;

public class TargetHeartRegionRemindFragment extends AbsSportPickerFragment {
    private static final String TAG = TargetHeartRegionRemindFragment.class.getSimpleName();
    private static int mInitHeartRegionMax = 0;
    private static int mInitHeartRegionMin = 0;
    private HeartRegionRemindAdapter mAdapter;
    private BaseConfig mConfig;

    class HeartRegionRemindAdapter extends AbsPickerViewAdapter {
        public HeartRegionRemindAdapter(Context context) {
            super(context);
        }

        public int columnCount() {
            return 2;
        }

        public int rowCount(int column) {
            return 106;
        }

        public float getColumnWidth(int column) {
            return 125.0f;
        }

        public float getTextSize() {
            return super.getTextSize() / 2.0f;
        }

        public String getDisplayString(int row, int column) {
            switch (column) {
                case 0:
                    return String.valueOf(row + 97);
                case 1:
                    return String.valueOf(row + 99);
                default:
                    return null;
            }
        }

        public int getInitRow(int column) {
            switch (column) {
                case 0:
                    return TargetHeartRegionRemindFragment.mInitHeartRegionMin;
                case 1:
                    return TargetHeartRegionRemindFragment.mInitHeartRegionMax;
                default:
                    return 0;
            }
        }

        public boolean showItemHint() {
            return true;
        }

        public boolean hideNegativeButton() {
            return true;
        }

        public String getUnit(int column) {
            if (column == 0) {
                return "bpm至";
            }
            return "bpm";
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        int i = 0;
        super.onCreate(savedInstanceState);
        this.mConfig = (BaseConfig) getArguments().getParcelable("key_config");
        if (this.mConfig.ismHeartRegionRemind()) {
            int i2;
            BaseConfig baseConfig = this.mConfig;
            if (BaseConfig.getCustomMinHeartRate() - 97 < 0) {
                i2 = 0;
            } else {
                baseConfig = this.mConfig;
                i2 = BaseConfig.getCustomMinHeartRate() - 97;
            }
            mInitHeartRegionMin = i2;
            baseConfig = this.mConfig;
            if (BaseConfig.getCustomMaxHeartRate() - 99 >= 0) {
                baseConfig = this.mConfig;
                i = BaseConfig.getCustomMaxHeartRate() - 99;
            }
            mInitHeartRegionMax = i;
            return;
        }
        mInitHeartRegionMin = 0;
        mInitHeartRegionMax = 0;
    }

    protected AbsPickerViewAdapter getAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new HeartRegionRemindAdapter(getActivity());
        }
        return this.mAdapter;
    }

    protected void onPositionChanged(int centerIndex) {
        super.onPositionChanged(centerIndex);
        LogUtils.print(TAG, "onPositionChanged centerIndex:" + centerIndex);
    }

    protected void onSave(int[] selectedRow) {
        LogUtils.print(TAG, "onSave：" + (selectedRow[0] + 97) + ":" + (selectedRow[1] + 99));
        int leftHeartRegion = selectedRow[0] + 97;
        int rigthHeartRegion = selectedRow[1] + 99;
        if (rigthHeartRegion > leftHeartRegion) {
            if (leftHeartRegion == rigthHeartRegion || rigthHeartRegion - leftHeartRegion < 2) {
                leftHeartRegion = rigthHeartRegion - 2;
            }
        } else if (rigthHeartRegion < leftHeartRegion || rigthHeartRegion == leftHeartRegion) {
            leftHeartRegion = rigthHeartRegion - 2;
        }
        this.mConfig.setmHeartRegionValueMin(leftHeartRegion);
        this.mConfig.setmHeartRegionValueMax(rigthHeartRegion);
        BaseConfig baseConfig = this.mConfig;
        BaseConfig.setCustomMinHeartRate(leftHeartRegion);
        baseConfig = this.mConfig;
        BaseConfig.setCustomMaxHeartRate(rigthHeartRegion);
        this.mConfig.setCurrentHeartRegionType(5);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }

    protected void onClose() {
        super.onClose();
        LogUtils.print(TAG, "onClose");
        this.mConfig.setmHeartRegionRemind(false);
        DataManager.getInstance().setSportConfig(getActivity(), this.mConfig);
        this.mListener.onSelectFinish();
    }
}
