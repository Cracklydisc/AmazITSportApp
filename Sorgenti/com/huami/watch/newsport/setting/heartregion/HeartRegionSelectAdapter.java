package com.huami.watch.newsport.setting.heartregion;

import android.content.Context;
import android.content.Intent;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.HmMenuActivity.HmMeunAdaper;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.manager.UserInfoManager;
import com.huami.watch.newsport.common.model.UserInfo;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.SportPickerActivity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HeartRegionSelectAdapter extends HmMeunAdaper {
    private static final String TAG = HeartRegionSelectAdapter.class.getSimpleName();
    private volatile int STATE_CURRENT = 1;
    private int currnetSportType = -1;
    private IConfigListener iconfigListener;
    private BaseConfig mConfig;
    private Context mContext;
    private DataManager mDataManager = DataManager.getInstance();
    private List<MenuInfo> selectMenuInfo = new ArrayList();
    private int userAge;
    private UserInfo userInfo;

    interface IConfigListener {
        void onConfigChange();
    }

    public HeartRegionSelectAdapter(Context mContext, int sportType) {
        this.mContext = mContext;
        this.currnetSportType = sportType;
        initMenu();
    }

    public void setListener(IConfigListener listener) {
        this.iconfigListener = listener;
    }

    private void initMenu() {
        final int currentYear;
        LogUtils.print(TAG, "initMenu");
        this.mConfig = this.mDataManager.getSportConfig(this.mContext, this.currnetSportType);
        MenuInfo info = new MenuInfo();
        info.title = this.mContext.getString(C0532R.string.setting_sport_heart_region_fat_burning);
        info.content = getHeartRegionContent(0);
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_burning;
        info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        this.selectMenuInfo.add(info);
        info = new MenuInfo();
        info.title = this.mContext.getString(C0532R.string.setting_sport_heart_region_aerobic);
        info.content = getHeartRegionContent(1);
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_aerobic;
        info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(1);
        this.selectMenuInfo.add(info);
        info = new MenuInfo();
        info.title = this.mContext.getString(C0532R.string.setting_sport_heart_region_high＿intensity_exercise);
        info.content = getHeartRegionContent(2);
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_strength;
        info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(2);
        this.selectMenuInfo.add(info);
        info = new MenuInfo();
        info.title = this.mContext.getString(C0532R.string.setting_sport_heart_region_custom);
        BaseConfig baseConfig = this.mConfig;
        if (BaseConfig.getCustomMinHeartRate() != 0) {
            baseConfig = this.mConfig;
            if (BaseConfig.getCustomMaxHeartRate() != 0) {
                this.STATE_CURRENT = 1;
                baseConfig = this.mConfig;
                int customMinHeartRate = BaseConfig.getCustomMinHeartRate();
                BaseConfig baseConfig2 = this.mConfig;
                info.content = getHeartRegionContent(customMinHeartRate, BaseConfig.getCustomMaxHeartRate());
                info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
                info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
                info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
                info.tag = Integer.valueOf(3);
                this.selectMenuInfo.add(info);
                this.userInfo = UserInfoManager.getInstance().getUserInfo(this.mContext);
                currentYear = Calendar.getInstance().get(1);
                Global.getGlobalWorkHandler().postDelayed(new Runnable() {

                    class C06151 implements Runnable {
                        C06151() {
                        }

                        public void run() {
                            HeartRegionSelectAdapter.this.notifyDataChanged();
                        }
                    }

                    public void run() {
                        if (HeartRegionSelectAdapter.this.userInfo == null || HeartRegionSelectAdapter.this.userInfo.getYear() <= 0) {
                            HeartRegionSelectAdapter.this.userAge = 25;
                        } else {
                            HeartRegionSelectAdapter.this.userAge = currentYear - HeartRegionSelectAdapter.this.userInfo.getYear();
                        }
                        boolean isPreViewSetting = HeartRegionSelectAdapter.this.STATE_CURRENT == 0;
                        int i = 0;
                        while (i < HeartRegionSelectAdapter.this.selectMenuInfo.size() && (isPreViewSetting || i != HeartRegionSelectAdapter.this.selectMenuInfo.size() - 1)) {
                            ((MenuInfo) HeartRegionSelectAdapter.this.selectMenuInfo.get(i)).content = HeartRegionSelectAdapter.this.getHeartRegionContent(i);
                            i++;
                        }
                        Global.getGlobalUIHandler().post(new C06151());
                    }
                }, 100);
            }
        }
        this.STATE_CURRENT = 0;
        info.iconResId = C0532R.drawable.lan_quicksettings_icon_hr_section;
        info.titleSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mContext.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(3);
        this.selectMenuInfo.add(info);
        this.userInfo = UserInfoManager.getInstance().getUserInfo(this.mContext);
        currentYear = Calendar.getInstance().get(1);
        Global.getGlobalWorkHandler().postDelayed(/* anonymous class already generated */, 100);
    }

    public void updateCustomRegion() {
        this.mConfig = this.mDataManager.getSportConfig(this.mContext, this.currnetSportType);
        if (this.mConfig.getCurrentHeartRegionType() == 5) {
            MenuInfo menuInfo = (MenuInfo) this.selectMenuInfo.get(3);
            BaseConfig baseConfig = this.mConfig;
            int customMinHeartRate = BaseConfig.getCustomMinHeartRate();
            BaseConfig baseConfig2 = this.mConfig;
            menuInfo.content = getHeartRegionContent(customMinHeartRate, BaseConfig.getCustomMaxHeartRate());
            notifyDataChanged();
            HeartRegionUtils.getInstance();
            HeartRegionUtils.setSensorHubInfo(this.mContext, this.mConfig.ismHeartRegionRemind(), this.mConfig.getmHeartRegionValueMin(), this.mConfig.getmHeartRegionValueMax());
        }
    }

    public int getCount() {
        return 4;
    }

    public MenuInfo getItemMenu(int position) {
        return (MenuInfo) this.selectMenuInfo.get(position);
    }

    public void omMenuItemClick(int position) {
        int[] result;
        switch (position) {
            case 0:
                result = getHeartRegion(0);
                this.mConfig.setmHeartRegionValueMin(result[0]);
                this.mConfig.setmHeartRegionValueMax(result[1]);
                LogUtils.print(TAG, "omMenuItemClick FAT_BURNING save:" + result[0] + "--" + result[1]);
                this.mDataManager.setSportConfig(this.mContext, this.mConfig);
                this.iconfigListener.onConfigChange();
                HeartRegionUtils.getInstance();
                HeartRegionUtils.setSensorHubInfo(this.mContext, true, result[0], result[1]);
                return;
            case 1:
                result = getHeartRegion(1);
                this.mConfig.setmHeartRegionValueMin(result[0]);
                this.mConfig.setmHeartRegionValueMax(result[1]);
                this.mDataManager.setSportConfig(this.mContext, this.mConfig);
                LogUtils.print(TAG, "omMenuItemClick aeribic save:" + result[0] + "--" + result[1]);
                this.iconfigListener.onConfigChange();
                HeartRegionUtils.getInstance();
                HeartRegionUtils.setSensorHubInfo(this.mContext, true, result[0], result[1]);
                return;
            case 2:
                result = getHeartRegion(2);
                this.mConfig.setmHeartRegionValueMin(result[0]);
                this.mConfig.setmHeartRegionValueMax(result[1]);
                LogUtils.print(TAG, "omMenuItemClick high intensity save:" + result[0] + "--" + result[1]);
                this.mDataManager.setSportConfig(this.mContext, this.mConfig);
                this.iconfigListener.onConfigChange();
                HeartRegionUtils.getInstance();
                HeartRegionUtils.setSensorHubInfo(this.mContext, true, result[0], result[1]);
                return;
            case 3:
                jumpToSettingPage();
                return;
            default:
                return;
        }
    }

    private void jumpToSettingPage() {
        Intent heartRemindRegionAcitivity = new Intent(this.mContext, SportPickerActivity.class);
        heartRemindRegionAcitivity.putExtra("selector_type", "select_heart_region_remind");
        heartRemindRegionAcitivity.putExtra("sport_type", this.currnetSportType);
        ((HeartRegionSelectActivity) this.mContext).startActivityForResult(heartRemindRegionAcitivity, HeartRegionSelectActivity.RESULT_CODE);
    }

    private int[] getHeartRegion(int type) {
        int[] result = new int[2];
        int min = 0;
        int max = 0;
        LogUtils.print(TAG, "getHeartRegion age:" + this.userAge);
        if (this.userAge == 0) {
            result[0] = 0;
            result[1] = 0;
        } else {
            switch (type) {
                case 0:
                    min = ((220 - this.userAge) * 50) / 100;
                    max = ((220 - this.userAge) * 69) / 100;
                    break;
                case 1:
                    min = ((220 - this.userAge) * 70) / 100;
                    max = ((220 - this.userAge) * 84) / 100;
                    break;
                case 2:
                    min = ((220 - this.userAge) * 85) / 100;
                    max = 220 - this.userAge;
                    break;
                case 3:
                    min = ((220 - this.userAge) * 80) / 100;
                    max = ((220 - this.userAge) * 90) / 100;
                    break;
            }
            result[0] = min;
            result[1] = max;
        }
        return result;
    }

    private String getHeartRegionContent(int type) {
        int[] heartRegion = getHeartRegion(type);
        return getHeartRegionContent(heartRegion[0], heartRegion[1]);
    }

    private String getHeartRegionContent(int min, int max) {
        StringBuilder sb = new StringBuilder();
        sb.append(min);
        sb.append("-");
        sb.append(max);
        sb.append(" bpm");
        return sb.toString();
    }
}
