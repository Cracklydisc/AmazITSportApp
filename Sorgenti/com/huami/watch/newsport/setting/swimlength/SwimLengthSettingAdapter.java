package com.huami.watch.newsport.setting.swimlength;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.huami.watch.menu.NewMenuLayout.MenuInfo;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.ui.SportPickerActivity;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter;
import com.huami.watch.newsport.ui.adapter.BaseConfigMenuAdapter.IConfigListener;
import java.util.ArrayList;
import java.util.List;

public class SwimLengthSettingAdapter extends BaseConfigMenuAdapter {
    private static final String TAG = SwimLengthSettingAdapter.class.getSimpleName();
    private boolean mIsFirstSetting = false;
    private List<MenuInfo> setttingMenuInfo = new ArrayList();

    public SwimLengthSettingAdapter(Activity context, IConfigListener listener, int sportType, boolean isFirstSetting) {
        super(context, listener, sportType);
        this.mIsFirstSetting = isFirstSetting;
    }

    public List<MenuInfo> getMenuInfos(Context context) {
        this.setttingMenuInfo = new ArrayList();
        MenuInfo info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(25)});
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(0);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(50)});
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(1);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(25)});
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(2);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
        info.title = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(50)});
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(3);
        this.setttingMenuInfo.add(info);
        info = new MenuInfo();
        info.iconResId = C0532R.drawable.sport_set_icon_lane_length;
        info.title = this.mMenuActivity.getString(C0532R.string.setting_sport_heart_region_text_custom);
        if (this.mConfig.getUnit() == 0) {
            info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
        } else {
            info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
        }
        info.titleSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_title_txt_size);
        info.valueSize = this.mMenuActivity.getResources().getDimension(C0532R.dimen.setting_content_txt_size);
        info.tag = Integer.valueOf(4);
        this.setttingMenuInfo.add(info);
        return this.setttingMenuInfo;
    }

    public void omMenuItemClick(int position) {
        clickMenuItem(position);
    }

    private void clickMenuItem(int position) {
        switch (((Integer) ((MenuInfo) this.mMenuList.get(position)).tag).intValue()) {
            case 0:
                this.mConfig.setUnit(0);
                this.mConfig.setTargetSwimLength(25);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 1:
                this.mConfig.setUnit(0);
                this.mConfig.setTargetSwimLength(50);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 2:
                this.mConfig.setUnit(1);
                this.mConfig.setTargetSwimLength(25);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 3:
                this.mConfig.setUnit(1);
                this.mConfig.setTargetSwimLength(50);
                DataManager.getInstance().setSportConfig(this.mMenuActivity, this.mConfig);
                this.mMenuActivity.finish();
                return;
            case 4:
                Intent graphIntent = new Intent(this.mMenuActivity, SportPickerActivity.class);
                graphIntent.putExtra("selector_type", "select_target_swim_length");
                graphIntent.putExtra("sport_type", this.mSportType);
                graphIntent.putExtra("key_is_first_config", this.mIsFirstSetting);
                this.mMenuActivity.startActivityForResult(graphIntent, 291);
                return;
            default:
                return;
        }
    }

    public void onDataChanged() {
        super.onDataChanged();
        for (int i = 0; i < this.mMenuList.size(); i++) {
            MenuInfo info = (MenuInfo) this.mMenuList.get(i);
            switch (((Integer) info.tag).intValue()) {
                case 4:
                    if (this.mConfig.getUnit() != 0) {
                        info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length_yd, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                        break;
                    }
                    info.content = this.mMenuActivity.getString(C0532R.string.settings_item_value_swim_length, new Object[]{String.valueOf(this.mConfig.getTargetSwimLength())});
                    break;
                default:
                    break;
            }
        }
        notifyDataChanged();
    }
}
