package com.huami.watch.newsport.setting.swimlength;

import android.content.Intent;
import android.os.Bundle;
import com.huami.watch.menu.HmMenuView;
import com.huami.watch.newsport.C0532R;
import com.huami.watch.newsport.common.manager.DataManager;
import com.huami.watch.newsport.common.model.config.BaseConfig;
import com.huami.watch.newsport.ui.BaseActivity;

public class SwimLengthActivity extends BaseActivity {
    private static final String TAG = SwimLengthActivity.class.getSimpleName();
    private BaseConfig mConfig = null;
    private boolean mIsFirstSetting = false;
    private int mSportType = -1;
    private SwimLengthSettingAdapter settingAdapter;
    private HmMenuView view = null;

    protected void onCreate(Bundle savedInstanceState) {
        setIsReceiveHomeKey(true);
        super.onCreate(savedInstanceState);
        this.mSportType = getIntent().getIntExtra("sport_type", -1);
        this.mIsFirstSetting = getIntent().getBooleanExtra("key_is_first_config", false);
        if (this.mSportType != -1) {
            this.view = new HmMenuView(this, null, C0532R.string.settings_sport_swim_length);
            this.settingAdapter = new SwimLengthSettingAdapter(this, null, this.mSportType, this.mIsFirstSetting);
            this.view.setSimpleMenuAdapter(this.settingAdapter);
            setContentView(this.view);
            this.view.scrollToTop();
            this.mConfig = DataManager.getInstance().getSportConfig(this, this.mSportType);
            if (this.mIsFirstSetting) {
                this.mConfig.setTargetSwimLength(this.mConfig.getTargetSwimLength());
                this.mConfig.setDistanceAutoLap((float) this.mConfig.getTargetSwimLength());
                DataManager.getInstance().setSportConfig(this, this.mConfig);
                DataManager.getInstance().setIsFirstOpenSwim(false);
                DataManager.getInstance().setLastSwimLength(this.mConfig.getTargetSwimLength());
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.settingAdapter != null) {
            this.settingAdapter.onDataChanged();
        }
    }
}
