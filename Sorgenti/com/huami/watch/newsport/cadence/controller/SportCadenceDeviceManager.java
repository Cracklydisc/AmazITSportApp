package com.huami.watch.newsport.cadence.controller;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.hs.gpxparser.utils.LogUtils;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.Global;
import com.huami.watch.newsport.cadence.callback.IBluetoothGattCallback;
import com.huami.watch.newsport.cadence.model.CyclingDataSnapshot;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import com.huami.watch.newsport.cadence.model.LeqiGattAttributes;
import com.huami.watch.newsport.cadence.model.SampleGattAttributes;
import com.huami.watch.newsport.cadence.service.BluetoothLeService;
import com.huami.watch.newsport.cadence.service.BluetoothLeService.LocalBinder;
import com.huami.watch.newsport.cadence.utils.Utils;
import com.huami.watch.newsport.utils.VibratorUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SportCadenceDeviceManager implements IBluetoothGattCallback {
    private static final String TAG = SportCadenceDeviceManager.class.getName();
    private static SportCadenceDeviceManager sInstance = null;
    private List<CyclingDataSnapshot> detailSnapList = new ArrayList();
    private boolean isStartSyncDetail = false;
    private boolean isStartSyncSummary = false;
    private BluetoothGattCharacteristic mActionCharacteristic;
    private HashMap<String, BluetoothGattCharacteristic> mBluetoothGattCharacteristic = new HashMap();
    private int mConnStatus = 0;
    private ICyclingDataListener mCyclingDataListener = null;
    private int mDeatailLength = 0;
    private byte[] mDetailBytes;
    private int mDetailPosition;
    private final Runnable mDetailRunable = new C05432();
    private CyclingDetail mDetails = null;
    private BluetoothDevice mDevice = null;
    private BluetoothGattCharacteristic mDeviceInfoCharacteristic;
    private int mFirmwareVersion = -1;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics = new ArrayList();
    private boolean mIsBindService = false;
    private boolean mIsStartedRequestLastSummaryInfo = false;
    private CyclingDataSummary mLastSummary = null;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private final Runnable mReConnectRunnable = new C05454();
    private BluetoothLeService mService = null;
    private CyclingDataSnapshot mSnapshot = null;
    private long mStartTime = -1;
    List<IBluetoothGattState> mStateListener = new LinkedList();
    private CyclingDataSummary mSummary = null;
    private byte[] mSummaryBytes = null;
    private int mSummaryLength = 29;
    private int mSummaryPosition = 0;
    private final Runnable mSummaryRunable = new C05443();
    private ServiceConnection serviceConnection = new C05421();

    class C05421 implements ServiceConnection {
        C05421() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            SportCadenceDeviceManager.this.mService = ((LocalBinder) service).getService();
            SportCadenceDeviceManager.this.mIsBindService = true;
            LogUtils.print(SportCadenceDeviceManager.TAG, "onServiceConnected serviceConnected ");
            if (SportCadenceDeviceManager.this.mService.initialize()) {
                SportCadenceDeviceManager.this.registBluetoothGattCallback();
                if (SportCadenceDeviceManager.this.mDevice != null) {
                    Debug.m5i(SportCadenceDeviceManager.TAG, "service address:" + SportCadenceDeviceManager.this.mDevice.getAddress());
                    SportCadenceDeviceManager.this.mService.connect(SportCadenceDeviceManager.this.mDevice);
                    return;
                }
                return;
            }
            Log.e(SportCadenceDeviceManager.TAG, "Unable to initialize Bluetooth");
        }

        public void onServiceDisconnected(ComponentName name) {
            SportCadenceDeviceManager.this.mService = null;
            SportCadenceDeviceManager.this.mIsBindService = false;
        }
    }

    class C05432 implements Runnable {
        C05432() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService != null) {
                Debug.m5i(SportCadenceDeviceManager.TAG, "========================sendDetail=============================");
                LeqiCmdAdapter.CmdStartSync(SportCadenceDeviceManager.this.mActionCharacteristic, 2, SportCadenceDeviceManager.this.mStartTime);
                SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, true);
                Global.getGlobalHeartHandler().postDelayed(SportCadenceDeviceManager.this.mDetailRunable, 9000);
            }
        }
    }

    class C05443 implements Runnable {
        C05443() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService == null || SportCadenceDeviceManager.this.mActionCharacteristic == null) {
                Debug.m5i(SportCadenceDeviceManager.TAG, "can not request leqi summary info, mService:" + SportCadenceDeviceManager.this.mService + ", mActionCharacteristic:" + SportCadenceDeviceManager.this.mActionCharacteristic);
                if (SportCadenceDeviceManager.this.mIsStartedRequestLastSummaryInfo && SportCadenceDeviceManager.this.mCyclingDataListener != null) {
                    SportCadenceDeviceManager.this.mCyclingDataListener.onLastSummaryQueryEnd();
                    return;
                }
                return;
            }
            Debug.m5i(SportCadenceDeviceManager.TAG, "========================sendSummary=============================");
            SportCadenceDeviceManager.this.mStartTime = System.currentTimeMillis() - LeqiCmdAdapter.get2000To1970();
            LeqiCmdAdapter.CmdStartSync(SportCadenceDeviceManager.this.mActionCharacteristic, 0, SportCadenceDeviceManager.this.mStartTime);
            SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, true);
            Global.getGlobalHeartHandler().postDelayed(SportCadenceDeviceManager.this.mSummaryRunable, 20000);
        }
    }

    class C05454 implements Runnable {
        C05454() {
        }

        public void run() {
            SportCadenceDeviceManager.this.reConnectLEDevice();
        }
    }

    class C05465 implements Runnable {
        C05465() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService != null && SportCadenceDeviceManager.this.mActionCharacteristic != null) {
                Debug.m5i(SportCadenceDeviceManager.TAG, "StartAuthenticateUser");
                LeqiCmdAdapter.CmdAuth(SportCadenceDeviceManager.this.mActionCharacteristic, 1000001, SportCadenceDeviceManager.this.mFirmwareVersion);
                SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, false);
            }
        }
    }

    class C05476 implements Runnable {
        C05476() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService != null) {
                SportCadenceDeviceManager.this.mStartTime = System.currentTimeMillis() - LeqiCmdAdapter.get2000To1970();
                LeqiCmdAdapter.CmdPhoneStart(SportCadenceDeviceManager.this.mActionCharacteristic, SportCadenceDeviceManager.this.mStartTime);
                SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, false);
            }
        }
    }

    class C05487 implements Runnable {
        C05487() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService != null && SportCadenceDeviceManager.this.mActionCharacteristic != null) {
                Debug.m5i(SportCadenceDeviceManager.TAG, "===================start sync real time data===================");
                LeqiCmdAdapter.CmdStartSyncRealTimeNotify(SportCadenceDeviceManager.this.mActionCharacteristic);
                SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, true);
            }
        }
    }

    class C05498 implements Runnable {
        C05498() {
        }

        public void run() {
            if (SportCadenceDeviceManager.this.mService != null && SportCadenceDeviceManager.this.mActionCharacteristic != null) {
                Debug.m5i(SportCadenceDeviceManager.TAG, "===================end sync real time data===================");
                LeqiCmdAdapter.CmdEndSyncRealTimeNotify(SportCadenceDeviceManager.this.mActionCharacteristic);
                SportCadenceDeviceManager.this.mService.writeCharacteristic(SportCadenceDeviceManager.this.mActionCharacteristic, true);
            }
        }
    }

    public interface IBluetoothGattState {
        void onConnectStateChanged(int i, int i2);
    }

    public interface ICyclingDataListener {
        void onDetailChangedList(List<CyclingDataSnapshot> list);

        void onLastSummaryQueryEnd();

        void onSummaryChanged(CyclingDataSummary cyclingDataSummary);
    }

    private SportCadenceDeviceManager() {
    }

    public static synchronized SportCadenceDeviceManager getInstance() {
        SportCadenceDeviceManager sportCadenceDeviceManager;
        synchronized (SportCadenceDeviceManager.class) {
            if (sInstance == null) {
                sInstance = new SportCadenceDeviceManager();
            }
            sportCadenceDeviceManager = sInstance;
        }
        return sportCadenceDeviceManager;
    }

    public void setDeviceAddress(BluetoothDevice device) {
        this.mDevice = device;
    }

    public void registeBlueStateChangeListener(IBluetoothGattState listener) {
        this.mStateListener.add(listener);
    }

    public void unRegisteBlueStateChangeListener(IBluetoothGattState listener) {
        if (this.mStateListener.contains(listener)) {
            this.mStateListener.remove(listener);
        }
    }

    public void stopCadenceService(Context context) {
        this.mConnStatus = 0;
        this.mDevice = null;
        Global.getGlobalHeartHandler().removeCallbacks(this.mReConnectRunnable);
        context.stopService(new Intent(context, BluetoothLeService.class));
    }

    public void bindCadenceService(Context context) {
        if (this.mService != null || this.mIsBindService) {
            Debug.m5i(TAG, "bind service failed, service is not null, is bind:" + this.mIsBindService);
        } else {
            context.bindService(new Intent(context.getApplicationContext(), BluetoothLeService.class), this.serviceConnection, 1);
        }
    }

    public void unBindCadenceService(Context context) {
        try {
            this.mIsBindService = false;
            this.mDevice = null;
            this.mConnStatus = 0;
            if (!(this.mService == null || this.mActionCharacteristic == null)) {
                LeqiCmdAdapter.CmdPhoneEnd(this.mActionCharacteristic, this.mStartTime);
                this.mService.writeCharacteristic(this.mActionCharacteristic, false);
            }
            context.getApplicationContext().unbindService(this.serviceConnection);
            Global.getGlobalHeartHandler().removeCallbacks(this.mReConnectRunnable);
        } catch (Exception e) {
            e.printStackTrace();
            Debug.m4e(TAG, "unbind service err, " + e.getLocalizedMessage());
        } finally {
            this.mService = null;
        }
    }

    public void registBluetoothGattCallback() {
        if (this.mService == null) {
            Debug.m5i(TAG, "service is null");
        } else {
            this.mService.registBluetoothGattCallback(this);
        }
    }

    public void setCyclingDataListener(ICyclingDataListener listener) {
        this.mCyclingDataListener = listener;
    }

    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        LogUtils.print(TAG, "onConnectionStateChange status:" + status + "---->newStatus:" + newState);
        if (this.mConnStatus != newState) {
            this.mConnStatus = newState;
            if (newState == 2) {
                if (this.mSummary == null) {
                    this.mSummary = new CyclingDataSummary();
                    this.mLastSummary = new CyclingDataSummary();
                    this.mDetails = new CyclingDetail();
                    this.mSnapshot = new CyclingDataSnapshot();
                }
                VibratorUtil.vibrateOnceWithStrongAttributes(Global.getApplicationContext(), new long[]{0, 800});
            } else if (newState == 0) {
                this.mSummary = null;
                this.mLastSummary = null;
                this.mDetails = null;
                this.mSnapshot = null;
                if (this.mService == null || this.mDevice != null) {
                    VibratorUtil.vibrateOnceWithStrongAttributes(Global.getApplicationContext(), new long[]{0, 500, 200, 500});
                } else {
                    VibratorUtil.vibrateOnceWithStrongAttributes(Global.getApplicationContext(), new long[]{0, 500, 200, 500});
                }
            }
            if (this.mStateListener != null) {
                for (IBluetoothGattState state : this.mStateListener) {
                    state.onConnectStateChanged(status, newState);
                }
            }
        }
    }

    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        LogUtils.print(TAG, "onServicesDiscovered: status: " + status);
        if (status == 0) {
            displayGattServices(this.mService.getSupportedGattServices());
        }
    }

    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        LogUtils.print(TAG, "onCharacteristicRead");
    }

    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        LogUtils.print(TAG, "onCharacteristicChanged");
    }

    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        LogUtils.print(TAG, "onCharacteristicWrite");
    }

    public synchronized void OnDataArrival(byte[] bytes, BluetoothGattCharacteristic characteristic) {
        if (characteristic.getUuid().equals(LeqiGattAttributes.UUID_CHARACTERISTIC_NOTIFY_CYCLING_DATA)) {
            Log.d(TAG, "action dump sync data");
            if (LeqiCmdAdapter.isResponseForSummary(bytes) || this.isStartSyncSummary) {
                this.isStartSyncSummary = true;
                if (LeqiCmdAdapter.isResponseForSummary(bytes)) {
                    this.mSummaryBytes = new byte[this.mSummaryLength];
                    System.arraycopy(bytes, 6, this.mSummaryBytes, 0, bytes.length - 6);
                    this.mSummaryPosition = bytes.length - 6;
                } else {
                    System.arraycopy(bytes, 0, this.mSummaryBytes, this.mSummaryPosition, this.mSummaryLength - this.mSummaryPosition);
                    Utils.dump(this.mSummaryBytes);
                    if (!(this.mSummary == null || this.mLastSummary == null)) {
                        this.mStartTime = LeqiCmdAdapter.parseX1ProSummaryData(this.mSummaryBytes, this.mSummary, this.mLastSummary);
                        if (this.mStartTime == -1) {
                            Debug.m5i(TAG, "the starttime is not correct");
                        } else {
                            this.mLastSummary = this.mLastSummary.copy(this.mSummary);
                            if (this.mCyclingDataListener != null) {
                                this.mCyclingDataListener.onSummaryChanged(this.mSummary);
                                if (this.mIsStartedRequestLastSummaryInfo) {
                                    this.mCyclingDataListener.onLastSummaryQueryEnd();
                                }
                            }
                        }
                    }
                    LeqiCmdAdapter.CmdEndSync(this.mActionCharacteristic, 0, this.mStartTime);
                    this.mService.writeCharacteristic(this.mActionCharacteristic, false);
                    this.isStartSyncSummary = false;
                    Global.getGlobalHeartHandler().removeCallbacks(this.mDetailRunable);
                    Global.getGlobalHeartHandler().postDelayed(this.mDetailRunable, 5000);
                }
            } else {
                this.detailSnapList.clear();
                if (LeqiCmdAdapter.isResponseForStart(bytes)) {
                    this.mDeatailLength = (bytes[2] & 255) + ((bytes[3] << 8) & 65280);
                    Debug.m5i(TAG, "coming next detail byte data count:" + this.mDeatailLength);
                    if (this.mDeatailLength > LeqiCmdAdapter.getSingleDetailLength(this.mFirmwareVersion) && this.mDeatailLength % LeqiCmdAdapter.getSingleDetailLength(this.mFirmwareVersion) == 0) {
                        this.isStartSyncDetail = true;
                        this.mDetailPosition = 0;
                        this.mDetailBytes = new byte[this.mDeatailLength];
                        System.arraycopy(bytes, 6, this.mDetailBytes, this.mDetailPosition, bytes.length - 6);
                        this.mDetailPosition = bytes.length - 6;
                    } else if (this.mDeatailLength == 10) {
                        byte[] snapBytes = new byte[10];
                        System.arraycopy(bytes, 6, snapBytes, 0, 10);
                        LeqiCmdAdapter.parseX1ProDetailSnapShot(snapBytes, this.mSnapshot);
                        if (this.mCyclingDataListener != null) {
                            this.detailSnapList.add(this.mSnapshot);
                            this.mCyclingDataListener.onDetailChangedList(this.detailSnapList);
                        }
                    }
                } else if (this.isStartSyncDetail) {
                    if (bytes.length < this.mDeatailLength - this.mDetailPosition) {
                        System.arraycopy(bytes, 0, this.mDetailBytes, this.mDetailPosition, bytes.length);
                        this.mDetailPosition += bytes.length;
                    } else {
                        System.arraycopy(bytes, 0, this.mDetailBytes, this.mDetailPosition, this.mDeatailLength - this.mDetailPosition);
                        Utils.dump(this.mDetailBytes);
                        this.isStartSyncDetail = false;
                        if (this.mDetails != null) {
                            LeqiCmdAdapter.parseX1ProDetailData(this.mDetailBytes, this.mDetails, this.mFirmwareVersion, this.mSummary);
                            if (this.mFirmwareVersion < 28 && this.mCyclingDataListener != null) {
                                this.mSnapshot.setCurTime(this.mDetails.getCurTime());
                                this.mSnapshot.setCurDistance(this.mDetails.getCurDistance());
                                this.mSnapshot.setCurSpeed(this.mDetails.getCurSpeed());
                                this.mSnapshot.setCurTemperature(this.mDetails.getTemperature());
                                this.mSnapshot.setCurCadence(this.mDetails.getCurCadence());
                                this.mSnapshot.setCurDuration(this.mDetails.getDuration());
                                this.detailSnapList.add(this.mSnapshot);
                                this.mCyclingDataListener.onDetailChangedList(this.detailSnapList);
                            }
                        }
                    }
                }
            }
        }
    }

    private void StartAuthenticateUser() {
        Global.getGlobalHeartHandler().postDelayed(new C05465(), 3000);
    }

    public void startRequestDataInfo() {
        Debug.m5i(TAG, "========================startRequestDatainfo============================= service:" + this.mService);
        if (this.mService == null || this.mActionCharacteristic == null) {
            Debug.m5i(TAG, "service: " + this.mService + ", mActionCharacteristic: " + this.mActionCharacteristic);
            return;
        }
        this.mService.setReceivePackageData(17);
        Global.getGlobalHeartHandler().postDelayed(new C05476(), 3000);
        Global.getGlobalHeartHandler().postDelayed(this.mSummaryRunable, 5000);
        if (this.mFirmwareVersion >= 28) {
            Global.getGlobalHeartHandler().postDelayed(new C05487(), 5000);
        }
    }

    public void endRequestDataInfo() {
        Debug.m5i(TAG, "========================endRequestDataInfo=============================");
        this.mIsStartedRequestLastSummaryInfo = false;
        Global.getGlobalHeartHandler().removeCallbacks(this.mSummaryRunable);
        Global.getGlobalHeartHandler().removeCallbacks(this.mDetailRunable);
        if (this.mFirmwareVersion >= 28) {
            Global.getGlobalHeartHandler().post(new C05498());
        }
        disConnGatt();
    }

    public void requestSummaryInfo() {
        Debug.m5i(TAG, "========================requestSummaryInfo=============================");
        this.mIsStartedRequestLastSummaryInfo = true;
        Global.getGlobalHeartHandler().removeCallbacks(this.mSummaryRunable);
        Global.getGlobalHeartHandler().removeCallbacks(this.mDetailRunable);
        Global.getGlobalHeartHandler().post(this.mSummaryRunable);
    }

    private void disConnGatt() {
        if (this.mService == null) {
            Debug.m5i(TAG, "disConnGatt, service is null");
        } else {
            this.mService.disconnect();
        }
    }

    private synchronized void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices != null) {
            ArrayList<HashMap<String, String>> gattServiceData = new ArrayList();
            ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData = new ArrayList();
            this.mGattCharacteristics = new ArrayList();
            for (BluetoothGattService gattService : gattServices) {
                HashMap<String, String> currentServiceData = new HashMap();
                if (!SampleGattAttributes.lookup(gattService.getUuid().toString(), "unknown").equals("unknown")) {
                    ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList();
                    List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                    ArrayList<BluetoothGattCharacteristic> charas = new ArrayList();
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        String uuid = gattCharacteristic.getUuid().toString();
                        int charaProp = gattCharacteristic.getProperties();
                        if (!SampleGattAttributes.lookup(uuid, "unknown").equals("unknown")) {
                            if (gattCharacteristic.getUuid().equals(LeqiGattAttributes.UUID_CHARACTERISTIC_NOTIFY_CYCLING_DATA)) {
                                this.mNotifyCharacteristic = gattCharacteristic;
                                if ((charaProp | 16) > 0) {
                                    this.mService.enableCharacteristicNotification(gattCharacteristic);
                                }
                            }
                            this.mBluetoothGattCharacteristic.put(uuid, gattCharacteristic);
                        }
                    }
                }
            }
            this.mActionCharacteristic = (BluetoothGattCharacteristic) this.mBluetoothGattCharacteristic.get(SampleGattAttributes.CHARACTERISTIC_ACTION);
            this.mDeviceInfoCharacteristic = (BluetoothGattCharacteristic) this.mBluetoothGattCharacteristic.get(SampleGattAttributes.CHARACTERISTIC_DEVICE_INFO);
            StartAuthenticateUser();
        }
    }

    public boolean reConnectLEDevice() {
        if (this.mDevice == null) {
            return false;
        }
        this.mService.reConnect(this.mDevice);
        return true;
    }
}
