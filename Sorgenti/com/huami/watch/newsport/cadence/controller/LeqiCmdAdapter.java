package com.huami.watch.newsport.cadence.controller;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.cadence.action.LeqiAction;
import com.huami.watch.newsport.cadence.action.LeqiActionBind;
import com.huami.watch.newsport.cadence.action.LeqiActionCurrentDataSync;
import com.huami.watch.newsport.cadence.action.LeqiActionPhoneSync;
import com.huami.watch.newsport.cadence.action.LeqiActionSync;
import com.huami.watch.newsport.cadence.model.CyclingDataSnapshot;
import com.huami.watch.newsport.cadence.model.CyclingDataSummary;
import com.huami.watch.newsport.cadence.model.CyclingDetail;
import java.text.SimpleDateFormat;

public class LeqiCmdAdapter {
    private static String TAG = "LeqiCmdAdapter";

    public static void CmdAuth(BluetoothGattCharacteristic chars, int id, int firmVersion) {
        LeqiAction action = new LeqiAction(3);
        LeqiActionBind bind = new LeqiActionBind(id, firmVersion);
        bind.Bind();
        action.setData(bind.getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdStartSync(BluetoothGattCharacteristic chars, int type, long startTime) {
        LeqiAction action = new LeqiAction(2);
        LeqiActionSync sync = new LeqiActionSync(startTime);
        sync.setValueType(type);
        sync.setSyncAction(1);
        action.setData(sync.getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdEndSync(BluetoothGattCharacteristic chars, int type, long startTime) {
        LeqiAction action = new LeqiAction(2);
        LeqiActionSync sync = new LeqiActionSync(startTime);
        sync.setValueType(type);
        sync.setSyncAction(0);
        action.setData(sync.getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdStartSyncRealTimeNotify(BluetoothGattCharacteristic chars) {
        LeqiAction action = new LeqiAction(6);
        action.setData(new LeqiActionCurrentDataSync(1).getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdEndSyncRealTimeNotify(BluetoothGattCharacteristic chars) {
        LeqiAction action = new LeqiAction(6);
        action.setData(new LeqiActionCurrentDataSync(0).getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdPhoneStart(BluetoothGattCharacteristic chars, long startTime) {
        LeqiAction action = new LeqiAction(1);
        LeqiActionPhoneSync sync = new LeqiActionPhoneSync(startTime);
        sync.setSyncAction(1);
        action.setData(sync.getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static void CmdPhoneEnd(BluetoothGattCharacteristic chars, long startTime) {
        LeqiAction action = new LeqiAction(1);
        LeqiActionPhoneSync sync = new LeqiActionPhoneSync(startTime);
        sync.setSyncAction(0);
        action.setData(sync.getCmdBytes());
        chars.setValue(action.getActionBytes());
    }

    public static long parseX1ProSummaryData(byte[] data, CyclingDataSummary summary, CyclingDataSummary lastSummary) {
        if (summary == null) {
            Debug.m5i(TAG, "summary is null");
            return -1;
        }
        long endTime;
        long utcStartTime = (long) getInt(getBytes(data, 0, 4));
        long utcEndTime = (long) getInt(getBytes(data, 4, 4));
        if (utcEndTime == 0) {
            endTime = System.currentTimeMillis();
        } else {
            endTime = (1000 * utcEndTime) + get2000To1970();
        }
        float distance = getFloat(getBytes(data, 8, 4));
        int duration = getInt(getBytes(data, 12, 4));
        float avgSpeed = getAvgSpeed(distance, duration);
        float maxSpeed = getFloat(getBytes(data, 16, 4));
        int circle = getInt(getBytes(data, 20, 4));
        int sumCadence = getInt(getBytes(data, 24, 4));
        int maxCadence = byte1Toint(data[28]);
        int avgCadence = getAvgCadence(circle, sumCadence);
        summary.setStartTime(getValidStartTime(utcStartTime, (long) duration, endTime));
        summary.setEndTime(endTime);
        summary.setDistance(1000.0f * distance);
        summary.setMaxSpeed(maxSpeed);
        summary.setTotalCircle(circle);
        summary.setSumCadence(sumCadence);
        summary.setMaxCandence(maxCadence);
        summary.setAveSpeed(avgSpeed);
        summary.setAveCadence((float) avgCadence);
        Log.d(TAG, "summary: " + summary.toString());
        if (summary.getTotalCircle() != lastSummary.getTotalCircle()) {
            summary.setCurCandence((int) (((float) (summary.getSumCadence() - lastSummary.getSumCadence())) / ((float) (summary.getTotalCircle() - lastSummary.getTotalCircle()))));
            return utcStartTime;
        }
        summary.setCurCandence(0);
        return utcStartTime;
    }

    public static int getSingleDetailLength(int version) {
        if (version < 28) {
            return 17;
        }
        return 18;
    }

    public static void parseX1ProDetailData(byte[] data, CyclingDetail detail, int version, CyclingDataSummary summary) {
        if (detail == null || data == null || data.length < getSingleDetailLength(version) || summary == null) {
            Debug.m5i(TAG, "detail is null");
            return;
        }
        Debug.m5i(TAG, "detail length: " + (data.length / getSingleDetailLength(version)));
        int lastPosition = ((data.length / getSingleDetailLength(version)) - 1) * getSingleDetailLength(version);
        long startTime = (1000 * ((long) getInt(getBytes(data, lastPosition, 4)))) + get2000To1970();
        float curSpeed = getFloat(getBytes(data, lastPosition + 4, 4));
        float distance = getFloat(getBytes(data, lastPosition + 8, 4));
        long duration = (long) getInt(getBytes(data, lastPosition + 12, 4));
        int cadence = byte1Toint(data[lastPosition + 16]);
        int temperature = 0;
        if (version >= 28) {
            temperature = byte1Toint(data[lastPosition + 17]);
        }
        detail.setCurTime(startTime - summary.getStartTime());
        detail.setCurSpeed(curSpeed);
        detail.setCurDistance(distance);
        detail.setDuration(duration);
        detail.setCurCadence(cadence);
        detail.setTemperature(temperature);
        Log.d(TAG, "detail: " + detail.toString());
    }

    public static void parseX1ProDetailSnapShot(byte[] data, CyclingDataSnapshot snapshot) {
        if (snapshot == null || data == null || data.length < 10) {
            Debug.m5i(TAG, "snap shot is null");
            return;
        }
        Debug.m5i(TAG, "snap shot length: 10");
        for (int i = 0; i < data.length / 10; i++) {
            int initPos = i * 10;
            float curSpeed = getFloat(getBytes(data, initPos, 4));
            float distance = getFloat(getBytes(data, initPos + 4, 4));
            int cadence = byte1Toint(data[initPos + 8]);
            int temperature = byte1Toint(data[initPos + 9]);
            snapshot.setCurSpeed(curSpeed);
            snapshot.setCurDistance(distance);
            snapshot.setCurCadence(cadence);
            snapshot.setCurTemperature(temperature);
            Log.d(TAG, "snap shot: " + snapshot.toString());
        }
    }

    private static long getValidStartTime(long utcStartTime, long duration, long endTime) {
        if (utcStartTime == 0) {
            return endTime - (duration * 1000);
        }
        return (utcStartTime * 1000) + get2000To1970();
    }

    public static long get2000To1970() {
        try {
            return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("01/01/2000 00:00:00").getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    private static byte[] getBytes(byte[] data, int offset, int size) {
        byte[] buff = new byte[size];
        int i = offset;
        int n = 0;
        while (i < offset + size) {
            int n2 = n + 1;
            buff[n] = data[i];
            i++;
            n = n2;
        }
        return buff;
    }

    private static int getInt(byte[] bytes) {
        return (((bytes[0] & 255) | (65280 & (bytes[1] << 8))) | (16711680 & (bytes[2] << 16))) | (-16777216 & (bytes[3] << 24));
    }

    private static int byte1Toint(byte data) {
        return data & 255;
    }

    private static float getFloat(byte[] bytes) {
        return Float.intBitsToFloat(getInt(bytes));
    }

    private static float getAvgSpeed(float distance, int time) {
        if (time == 0) {
            return 0.0f;
        }
        return distance / ((float) time);
    }

    private static int getAvgCadence(int circle, int sumCandence) {
        if (circle == 0) {
            return 0;
        }
        return sumCandence / circle;
    }

    public static boolean isResponseForSummary(byte[] bytes) {
        if (bytes == null || bytes.length < 4) {
            return false;
        }
        int length = (bytes[2] & 255) + ((bytes[3] << 8) & 65280);
        if (bytes[0] == (byte) 1 && bytes[1] == (byte) 0 && length == 29) {
            return true;
        }
        return false;
    }

    public static boolean isResponseForStart(byte[] bytes) {
        if (bytes == null || bytes.length < 3) {
            return false;
        }
        if (bytes[0] == (byte) 1 && bytes[1] == (byte) 0) {
            return true;
        }
        return false;
    }
}
