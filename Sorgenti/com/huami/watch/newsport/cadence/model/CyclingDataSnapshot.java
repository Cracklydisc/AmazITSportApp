package com.huami.watch.newsport.cadence.model;

public class CyclingDataSnapshot {
    private int curCadence;
    private float curDistance;
    private long curDuration;
    private float curSpeed;
    private int curTemperature;
    private long curTime;

    public long getCurTime() {
        return this.curTime;
    }

    public void setCurTime(long curTime) {
        this.curTime = curTime;
    }

    public long getCurDuration() {
        return this.curDuration;
    }

    public void setCurDuration(long curDuration) {
        this.curDuration = curDuration;
    }

    public float getCurSpeed() {
        return this.curSpeed;
    }

    public void setCurSpeed(float curSpeed) {
        this.curSpeed = curSpeed;
    }

    public float getCurDistance() {
        return this.curDistance;
    }

    public void setCurDistance(float curDistance) {
        this.curDistance = curDistance;
    }

    public int getCurCadence() {
        return this.curCadence;
    }

    public void setCurCadence(int curCadence) {
        this.curCadence = curCadence;
    }

    public int getCurTemperature() {
        return this.curTemperature;
    }

    public void setCurTemperature(int curTemperature) {
        this.curTemperature = curTemperature;
    }

    public String toString() {
        return "CyclingDataSnapshot{curSpeed=" + this.curSpeed + ", curDistance=" + this.curDistance + ", curCadence=" + this.curCadence + ", curTemperature=" + this.curTemperature + ", curTime=" + this.curTime + ", curDuration=" + this.curDuration + '}';
    }
}
