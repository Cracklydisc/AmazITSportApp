package com.huami.watch.newsport.cadence.model;

import java.util.HashMap;
import java.util.UUID;

public class LeqiGattAttributes {
    private static String CHARACTERISTIC_ACTION = "0000fff3-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_CONNECT_PARAMS = "0000fff7-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_DEVICE_INFO = "0000fff8-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_NOTIFY_CYCLING_DATA = "0000fff2-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_NOTIFY_W1 = "0000fff6-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_REALTIME_DATA = "0000fff5-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_SETTINGS = "0000fff4-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_TMP = "0000ffff-0000-1000-8000-00805f9b34fb";
    private static String CHARACTERISTIC_TOTAL_CYCLING_DATA = "0000fff1-0000-1000-8000-00805f9b34fb";
    private static String LEQI_GATT_SERVICE = "0000fff0-0000-1000-8000-00805f9b34fb";
    public static final UUID UUID_CHARACTERISTIC_ACTION = UUID.fromString(CHARACTERISTIC_ACTION);
    public static final UUID UUID_CHARACTERISTIC_CONNECT_PARAMS = UUID.fromString(CHARACTERISTIC_CONNECT_PARAMS);
    public static final UUID UUID_CHARACTERISTIC_DEVICE_INFO = UUID.fromString(CHARACTERISTIC_DEVICE_INFO);
    public static final UUID UUID_CHARACTERISTIC_NOTIFY_CYCLING_DATA = UUID.fromString(CHARACTERISTIC_NOTIFY_CYCLING_DATA);
    public static final UUID UUID_CHARACTERISTIC_NOTIFY_W1 = UUID.fromString(CHARACTERISTIC_NOTIFY_W1);
    public static final UUID UUID_CHARACTERISTIC_REALTIME_DATA = UUID.fromString(CHARACTERISTIC_REALTIME_DATA);
    public static final UUID UUID_CHARACTERISTIC_SETTINGS = UUID.fromString(CHARACTERISTIC_SETTINGS);
    public static final UUID UUID_CHARACTERISTIC_TMP = UUID.fromString(CHARACTERISTIC_TMP);
    public static final UUID UUID_CHARACTERISTIC_TOTAL_CYCLING_DATA = UUID.fromString(CHARACTERISTIC_TOTAL_CYCLING_DATA);
    public static final UUID UUID_LEQI_GATT_SERVICE = UUID.fromString(LEQI_GATT_SERVICE);
    private static HashMap<String, String> attributes = new HashMap();

    static {
        attributes.put(UUID_LEQI_GATT_SERVICE.toString(), "X1 pro Service");
        attributes.put(UUID_CHARACTERISTIC_NOTIFY_CYCLING_DATA.toString(), "notify cycling data");
        attributes.put(UUID_CHARACTERISTIC_ACTION.toString(), "action");
    }
}
