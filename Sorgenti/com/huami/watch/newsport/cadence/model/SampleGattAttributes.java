package com.huami.watch.newsport.cadence.model;

import java.util.HashMap;

public class SampleGattAttributes {
    public static String CHARACTERISTIC_ACTION = "0000fff3-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_CONNECT_PARAMS = "0000fff7-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DEVICE_INFO = "0000fff8-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_NOTIFY_CYCLING_DATA = "0000fff2-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_NOTIFY_W1 = "0000fff6-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_REALTIME_DATA = "0000fff5-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_SETTINGS = "0000fff4-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_TOTAL_CYCLING_DATA = "0000fff1-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    private static HashMap<String, String> attributes = new HashMap();

    static {
        attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        attributes.put("0000fff0-0000-1000-8000-00805f9b34fb", "X1 pro Service");
        attributes.put("0000fff1-0000-1000-8000-00805f9b34fb", "total cycling data");
        attributes.put("0000fff2-0000-1000-8000-00805f9b34fb", "notify cycling data");
        attributes.put("0000fff3-0000-1000-8000-00805f9b34fb", "action");
        attributes.put("0000fff4-0000-1000-8000-00805f9b34fb", "settings");
        attributes.put("0000fff5-0000-1000-8000-00805f9b34fb", "readtime data");
        attributes.put("0000fff6-0000-1000-8000-00805f9b34fb", "notify w1 connection");
        attributes.put("0000fff7-0000-1000-8000-00805f9b34fb", "connect params");
        attributes.put("0000fff8-0000-1000-8000-00805f9b34fb", "devices info");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = (String) attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
