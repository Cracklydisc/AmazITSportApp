package com.huami.watch.newsport.cadence.model;

public class CyclingDetail {
    private int curCadence;
    private float curDistance;
    private float curSpeed;
    private long curTime;
    private long duration;
    private int temperature;
    private long trackId;

    public int getTemperature() {
        return this.temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public long getCurTime() {
        return this.curTime;
    }

    public void setCurTime(long curTime) {
        this.curTime = curTime;
    }

    public float getCurSpeed() {
        return this.curSpeed;
    }

    public void setCurSpeed(float curSpeed) {
        this.curSpeed = curSpeed;
    }

    public float getCurDistance() {
        return this.curDistance;
    }

    public void setCurDistance(float curDistance) {
        this.curDistance = curDistance;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getCurCadence() {
        return this.curCadence;
    }

    public void setCurCadence(int curCadence) {
        this.curCadence = curCadence;
    }

    public long getTrackId() {
        return this.trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }

    public String toString() {
        return "CyclingDetail{trackId=" + this.trackId + "curTime=" + this.curTime + ", curSpeed=" + this.curSpeed + ", curDistance=" + this.curDistance + ", duration=" + this.duration + ", curCadence=" + this.curCadence + ", temperature=" + this.temperature + '}';
    }
}
