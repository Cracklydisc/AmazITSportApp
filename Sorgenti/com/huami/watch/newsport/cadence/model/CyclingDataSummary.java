package com.huami.watch.newsport.cadence.model;

public class CyclingDataSummary {
    private float mAveCadence;
    private float mAveSpeed;
    private int mCurCandence;
    private long mCurrentSpeed;
    private long mCurrentTime;
    private float mDistance;
    private long mDuration;
    private long mEndTime;
    private int mMaxCandence;
    private float mMaxSpeed;
    private long mStartTime;
    private int mSumCadence;
    private int mTotalCircle;

    public long getStartTime() {
        return this.mStartTime;
    }

    public void setStartTime(long startTime) {
        this.mStartTime = startTime;
    }

    public long getEndTime() {
        return this.mEndTime;
    }

    public void setEndTime(long endTime) {
        this.mEndTime = endTime;
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
    }

    public long getDuration() {
        return this.mDuration;
    }

    public void setDuration(long duration) {
        this.mDuration = duration;
    }

    public float getMaxSpeed() {
        return this.mMaxSpeed;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.mMaxSpeed = maxSpeed;
    }

    public int getTotalCircle() {
        return this.mTotalCircle;
    }

    public void setTotalCircle(int totalCadenceCircle) {
        this.mTotalCircle = totalCadenceCircle;
    }

    public int getSumCadence() {
        return this.mSumCadence;
    }

    public void setSumCadence(int totalCadence) {
        this.mSumCadence = totalCadence;
    }

    public int getMaxCandence() {
        return this.mMaxCandence;
    }

    public void setMaxCandence(int maxCandence) {
        this.mMaxCandence = maxCandence;
    }

    public int getCurCandence() {
        return this.mCurCandence;
    }

    public void setCurCandence(int curCandence) {
        this.mCurCandence = curCandence;
    }

    public long getCurrentTime() {
        return this.mCurrentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.mCurrentTime = currentTime;
    }

    public long getCurrentSpeed() {
        return this.mCurrentSpeed;
    }

    public void setCurrentSpeed(long currentSpeed) {
        this.mCurrentSpeed = currentSpeed;
    }

    public float getAveSpeed() {
        return this.mAveSpeed;
    }

    public void setAveSpeed(float aveSpeed) {
        this.mAveSpeed = aveSpeed;
    }

    public float getAveCadence() {
        return this.mAveCadence;
    }

    public void setAveCadence(float aveCadence) {
        this.mAveCadence = aveCadence;
    }

    public String toString() {
        return "CyclingDataSummary{mStartTime=" + this.mStartTime + ", mEndTime=" + this.mEndTime + ", mDistance=" + this.mDistance + ", mDuration=" + this.mDuration + ", mMaxSpeed=" + this.mMaxSpeed + ", mTotalCadenceCircle=" + this.mTotalCircle + ", mTotalCadence=" + this.mSumCadence + ", mMaxCandence=" + this.mMaxCandence + ", mCurCandence=" + this.mCurCandence + ", mCurrentTime=" + this.mCurrentTime + ", mCurrentSpeed=" + this.mCurrentSpeed + '}';
    }

    public CyclingDataSummary copy(CyclingDataSummary summary) {
        CyclingDataSummary lastSummary = new CyclingDataSummary();
        lastSummary.setStartTime(summary.getStartTime());
        lastSummary.setEndTime(summary.getEndTime());
        lastSummary.setDistance(summary.getDistance());
        lastSummary.setDuration(summary.getDuration());
        lastSummary.setMaxSpeed(summary.getMaxSpeed());
        lastSummary.setAveSpeed(summary.getAveSpeed());
        lastSummary.setAveCadence(summary.getAveCadence());
        lastSummary.setTotalCircle(summary.getTotalCircle());
        lastSummary.setSumCadence(summary.getSumCadence());
        lastSummary.setMaxCandence(summary.getMaxCandence());
        lastSummary.setCurCandence(summary.getCurCandence());
        lastSummary.setCurrentTime(summary.getCurrentTime());
        lastSummary.setCurrentSpeed(summary.getCurrentSpeed());
        return lastSummary;
    }
}
