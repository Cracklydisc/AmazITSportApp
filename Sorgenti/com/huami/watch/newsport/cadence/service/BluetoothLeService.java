package com.huami.watch.newsport.cadence.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.huami.watch.common.log.Debug;
import com.huami.watch.newsport.cadence.callback.IBluetoothGattCallback;
import com.huami.watch.newsport.cadence.model.LeqiGattAttributes;
import com.huami.watch.newsport.cadence.model.SampleGattAttributes;
import com.huami.watch.newsport.cadence.utils.Utils;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class BluetoothLeService extends Service {
    private static final String TAG = BluetoothLeService.class.getSimpleName();
    public static final UUID UUID_HEART_RATE_MEASUREMENT = UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
    private byte[] bytesArray;
    private final IBinder mBinder = new LocalBinder();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothManager mBluetoothManager;
    private int mConnectionState = 0;
    private final BluetoothGattCallback mGattCallback = new C05501();
    private List<IBluetoothGattCallback> mListener = new LinkedList();
    private int packageCount = 0;
    private int packageCountMax = 0;
    private int packageLength;

    class C05501 extends BluetoothGattCallback {
        C05501() {
        }

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == 2) {
                intentAction = "com.huami.cyclingcounter.bluetooth.le.ACTION_GATT_CONNECTED";
                BluetoothLeService.this.mConnectionState = 2;
                Log.i(BluetoothLeService.TAG, "Connected to GATT server.");
                Log.i(BluetoothLeService.TAG, "Attempting to start service discovery:" + BluetoothLeService.this.mBluetoothGatt.discoverServices());
            } else if (newState == 0) {
                intentAction = "com.huami.cyclingcounter.bluetooth.le.ACTION_GATT_DISCONNECTED";
                BluetoothLeService.this.mConnectionState = 0;
                Log.i(BluetoothLeService.TAG, "Disconnected from GATT server.");
            }
            if (BluetoothLeService.this.mListener != null) {
                for (IBluetoothGattCallback callback : BluetoothLeService.this.mListener) {
                    callback.onConnectionStateChange(gatt, status, newState);
                }
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status != 0) {
                Log.w(BluetoothLeService.TAG, "onServicesDiscovered received: " + status);
            }
            if (BluetoothLeService.this.mListener != null) {
                for (IBluetoothGattCallback callback : BluetoothLeService.this.mListener) {
                    callback.onServicesDiscovered(gatt, status);
                }
            }
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == 0) {
                BluetoothLeService.this.broadcastUpdate("com.huami.cyclingcounter.bluetooth.le.ACTION_DATA_AVAILABLE", characteristic);
            } else {
                Log.w(BluetoothLeService.TAG, "onServicesDiscovered received: " + status);
            }
            if (BluetoothLeService.this.mListener != null) {
                for (IBluetoothGattCallback callback : BluetoothLeService.this.mListener) {
                    callback.onCharacteristicRead(gatt, characteristic, status);
                }
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            BluetoothLeService.this.broadcastUpdate("com.huami.cyclingcounter.bluetooth.le.ACTION_DATA_AVAILABLE", characteristic);
            if (BluetoothLeService.this.mListener != null) {
                for (IBluetoothGattCallback callback : BluetoothLeService.this.mListener) {
                    callback.onCharacteristicChanged(gatt, characteristic);
                }
            }
        }

        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == 0) {
                BluetoothLeService.this.broadcastUpdate("com.huami.cyclingcounter.bluetooth.le.ACTION_DATA_AVAILABLE", characteristic);
                Log.w(BluetoothLeService.TAG, "onCharacteristicWrite write success: " + characteristic.getUuid().toString());
            }
            if (BluetoothLeService.this.mListener != null) {
                for (IBluetoothGattCallback callback : BluetoothLeService.this.mListener) {
                    callback.onCharacteristicWrite(gatt, characteristic, status);
                }
            }
        }
    }

    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 2;
    }

    private synchronized void broadcastUpdate(String action, BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "broadcastUpadate, action: " + characteristic.getUuid());
        if (LeqiGattAttributes.UUID_CHARACTERISTIC_NOTIFY_CYCLING_DATA.equals(characteristic.getUuid())) {
            if (this.packageCountMax == 0) {
                Debug.m5i(TAG, "max count: " + this.packageCount + ", page count: " + this.packageCount);
                setReceivePackageData(17);
            }
            byte[] bArr = new byte[20];
            bArr = characteristic.getValue();
            Utils.dump(bArr);
            if (this.packageCount != this.packageCountMax) {
                Log.d(TAG, "data size " + characteristic.getValue().length + " packageCounxt=" + this.packageCount + " max=" + this.packageCountMax);
                System.arraycopy(bArr, 0, this.bytesArray, (this.packageCount - 1) * 20, 20);
                Utils.dump(this.bytesArray);
                this.packageCount++;
            } else {
                Log.d(TAG, "data size " + characteristic.getValue().length + " packageCounxt=" + this.packageCount + " packageend=" + this.packageCountMax);
                System.arraycopy(bArr, 0, this.bytesArray, (this.packageCount - 1) * 20, 20);
                Utils.dump(this.bytesArray);
                this.packageCount = 1;
                if (this.mListener != null) {
                    for (IBluetoothGattCallback callback : this.mListener) {
                        callback.OnDataArrival(this.bytesArray, characteristic);
                    }
                }
            }
        } else {
            byte[] data = characteristic.getValue();
            if (data != null && data.length > 3) {
                int key = characteristic.getIntValue(17, 2).intValue();
                Log.d(TAG, "data size " + data.length + " key = " + key + " value = " + characteristic.getIntValue(17, 3).intValue());
            }
        }
    }

    public synchronized void registBluetoothGattCallback(IBluetoothGattCallback callback) {
        this.mListener.add(callback);
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public boolean initialize() {
        if (this.mBluetoothManager == null) {
            this.mBluetoothManager = (BluetoothManager) getSystemService("bluetooth");
            if (this.mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter != null) {
            return true;
        }
        Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
        return false;
    }

    public boolean connect(BluetoothDevice device) {
        if (this.mBluetoothAdapter == null || device == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } else if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        } else if (this.mBluetoothDevice == null || !this.mBluetoothDevice.getAddress().equals(device.getAddress()) || this.mBluetoothGatt == null) {
            this.mBluetoothGatt = device.connectGatt(this, false, this.mGattCallback, 2);
            Log.d(TAG, "Trying to create a new connection.");
            this.mConnectionState = 1;
            return true;
        } else {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (!this.mBluetoothGatt.connect()) {
                return false;
            }
            this.mConnectionState = 1;
            return true;
        }
    }

    public boolean reConnect(BluetoothDevice device) {
        if (this.mBluetoothAdapter == null || device == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } else if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        } else {
            this.mBluetoothGatt = device.connectGatt(this, false, this.mGattCallback);
            Log.d(TAG, "Trying to create a new connection.");
            this.mConnectionState = 1;
            return true;
        }
    }

    public void disconnect() {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            this.mBluetoothGatt.disconnect();
        }
    }

    public void close() {
        if (this.mBluetoothGatt != null) {
            this.mBluetoothGatt.close();
            this.mBluetoothGatt = null;
        }
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic, boolean enableNotify) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        if (enableNotify) {
            this.mBluetoothGatt.setCharacteristicNotification(characteristic, enableNotify);
            List<BluetoothGattDescriptor> descriptorList = characteristic.getDescriptors();
            if (descriptorList != null && descriptorList.size() > 0) {
                for (BluetoothGattDescriptor descriptor : descriptorList) {
                    Log.e(TAG, "scan descriptorList uuid " + descriptor.getUuid());
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    this.mBluetoothGatt.writeDescriptor(descriptor);
                }
            }
        }
        return this.mBluetoothGatt.writeCharacteristic(characteristic);
    }

    public boolean enableCharacteristicNotification(BluetoothGattCharacteristic characteristic) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        Log.w(TAG, "enableCharacteristicNotification, ffff2");
        boolean isEnableNotification = this.mBluetoothGatt.setCharacteristicNotification(characteristic, true);
        if (!isEnableNotification) {
            return isEnableNotification;
        }
        List<BluetoothGattDescriptor> descriptorList = characteristic.getDescriptors();
        if (descriptorList == null || descriptorList.size() <= 0) {
            return isEnableNotification;
        }
        for (BluetoothGattDescriptor descriptor : descriptorList) {
            Log.e(TAG, "scan descriptorList uuid " + descriptor.getUuid());
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            this.mBluetoothGatt.writeDescriptor(descriptor);
        }
        return isEnableNotification;
    }

    public List<BluetoothGattService> getSupportedGattServices() {
        if (this.mBluetoothGatt == null) {
            return null;
        }
        return this.mBluetoothGatt.getServices();
    }

    public synchronized void setReceivePackageData(int packLength) {
        int i = 1;
        synchronized (this) {
            Debug.m5i(TAG, "setReceivePackageData, packLength: " + packLength);
            this.packageLength = packLength;
            this.packageCount = 1;
            int i2 = packLength / 20;
            if (packLength % 20 <= 1) {
                i = 0;
            }
            this.packageCountMax = i + i2;
            this.bytesArray = new byte[(this.packageCountMax * 20)];
        }
    }
}
