package com.huami.watch.newsport.cadence.action;

import com.huami.watch.newsport.cadence.utils.Utils;

public class LeqiActionSync {
    private byte[] action = new byte[1];
    private byte[] randomId;
    private byte[] type = new byte[1];

    public LeqiActionSync(long startTime) {
        this.randomId = Utils.toByteArray((int) startTime, 4);
    }

    public void setValueType(int type) {
        this.type[0] = (byte) type;
    }

    public void setSyncAction(int action) {
        this.action[0] = (byte) action;
    }

    public byte[] getCmdBytes() {
        return Utils.byteMerger(Utils.byteMerger(this.randomId, this.type), this.action);
    }
}
