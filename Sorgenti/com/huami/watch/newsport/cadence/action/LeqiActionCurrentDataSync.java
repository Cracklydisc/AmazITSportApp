package com.huami.watch.newsport.cadence.action;

public class LeqiActionCurrentDataSync {
    private byte[] mSyncCmd = new byte[1];

    public LeqiActionCurrentDataSync(int type) {
        this.mSyncCmd[0] = (byte) type;
    }

    public byte[] getCmdBytes() {
        return this.mSyncCmd;
    }
}
