package com.huami.watch.newsport.cadence.action;

import com.huami.watch.newsport.cadence.utils.LeqiCrc;
import com.huami.watch.newsport.cadence.utils.Utils;

public class LeqiAction {
    private byte[] data = null;
    public byte[] headData = new byte[2];
    private byte[] magic_key = new byte[]{(byte) -18, (byte) -103};
    private byte[] valueData = null;

    public LeqiAction(int key) {
        this.headData[0] = (byte) key;
    }

    public void setData(byte[] data) {
        if (this.data == null) {
            this.data = new byte[data.length];
        }
        this.headData[1] = (byte) data.length;
        for (int i = 0; i < data.length; i++) {
            this.data[i] = data[i];
        }
    }

    public void buildPackage() {
        this.valueData = Utils.byteMerger(Utils.byteMerger(Utils.byteMerger(this.magic_key, this.headData), this.data), Utils.toByteArray(LeqiCrc.calcCrc(this.data, this.data.length), 2));
    }

    public byte[] getActionBytes() {
        if (this.valueData == null) {
            buildPackage();
        }
        byte[] retValue = new byte[20];
        for (int i = 0; i < this.valueData.length; i++) {
            retValue[i] = this.valueData[i];
        }
        return retValue;
    }
}
