package com.huami.watch.newsport.cadence.action;

import com.huami.watch.newsport.cadence.utils.Utils;

public class LeqiActionBind {
    private byte[] byteId;
    private byte[] channel;
    private byte[] cmd;

    public LeqiActionBind(int id, int firmVersion) {
        this.byteId = Utils.toByteArray(id, 4);
        if (firmVersion >= 28) {
            this.channel = Utils.toByteArray(65534, 2);
        }
    }

    public void Bind() {
        this.cmd = Utils.toByteArray(1, 1);
    }

    public byte[] getCmdBytes() {
        return this.channel == null ? Utils.byteMerger(this.byteId, this.cmd) : Utils.byteMerger(Utils.byteMerger(this.byteId, this.cmd), this.channel);
    }
}
