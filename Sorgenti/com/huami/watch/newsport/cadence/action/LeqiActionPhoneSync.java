package com.huami.watch.newsport.cadence.action;

import com.huami.watch.newsport.cadence.utils.Utils;

public class LeqiActionPhoneSync {
    private byte[] action = new byte[1];
    private byte[] randomId;

    public LeqiActionPhoneSync(long startTime) {
        this.randomId = Utils.toByteArray((int) startTime, 4);
    }

    public void setSyncAction(int action) {
        this.action[0] = (byte) action;
    }

    public byte[] getCmdBytes() {
        return Utils.byteMerger(this.randomId, this.action);
    }
}
