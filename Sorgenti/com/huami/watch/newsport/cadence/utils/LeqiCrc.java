package com.huami.watch.newsport.cadence.utils;

public class LeqiCrc {
    public static short calcCrc(byte[] value, int length) {
        short crc = (short) 0;
        for (int i = 0; i < length; i++) {
            crc = crc16(crc, value[i]);
        }
        return crc;
    }

    static short crc16(short crc, byte val) {
        byte cnt = (byte) 0;
        while (cnt < (byte) 8) {
            byte msb;
            if ((crc & 32768) == 32768) {
                msb = (byte) 1;
            } else {
                msb = (byte) 0;
            }
            crc = (short) (crc << 1);
            if ((val & 128) == 128) {
                crc = (short) (crc | 1);
            }
            if (msb == (byte) 1) {
                crc = (short) (crc ^ 4129);
            }
            cnt = (byte) (cnt + 1);
            val = (byte) (val << 1);
        }
        return crc;
    }
}
