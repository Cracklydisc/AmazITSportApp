package com.huami.watch.newsport.cadence.utils;

import android.util.Log;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import java.text.SimpleDateFormat;

public class Utils {
    private static String TAG = "Utils";

    public static byte[] toByteArray(int iSource, int iArrayLen) {
        byte[] bLocalArr = new byte[iArrayLen];
        int i = 0;
        while (i < 4 && i < iArrayLen) {
            bLocalArr[i] = (byte) ((iSource >> (i * 8)) & 255);
            i++;
        }
        return bLocalArr;
    }

    public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
        byte[] byte_3 = new byte[(byte_1.length + byte_2.length)];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }

    public static void dump(byte[] value) {
        StringBuilder stringBuilder = new StringBuilder(value.length);
        for (int i = 0; i < value.length; i++) {
            stringBuilder.append(String.format("%02X ", new Object[]{Byte.valueOf(value[i])}));
        }
        Log.e(TAG, "value:" + stringBuilder.toString());
    }

    public static String dump(int[] value) {
        StringBuilder stringBuilder = new StringBuilder(value.length);
        for (int valueOf : value) {
            stringBuilder.append(String.valueOf(valueOf));
        }
        return stringBuilder.toString();
    }

    public static void printlnSportStatistics(SportStatistics sportStatistics) {
        if (sportStatistics != null && sportStatistics.mSportThaWorkout != null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" distance:" + sportStatistics.mSportThaWorkout.getDistance());
            stringBuilder.append(" ,duration:" + sportStatistics.mSportThaWorkout.getDuration());
            stringBuilder.append(" ,internsity:" + sportStatistics.mSportThaWorkout.getIntensity());
            stringBuilder.append(" ,hearRate:" + sportStatistics.mSportThaWorkout.getHeartRate());
            stringBuilder.append(" ,trainingEffect:" + sportStatistics.mSportThaWorkout.getTrainingEffect());
            stringBuilder.append(" ,runningSpeed:" + sportStatistics.mSportThaWorkout.getRunningSpeed());
            stringBuilder.append(" ,phrase:" + sportStatistics.mSportThaWorkout.getPhrase());
            Log.i(TAG, " mSportThaWorkout:" + stringBuilder.toString());
        } else if (sportStatistics == null) {
            Log.i(TAG, "sportStatistics is null");
        } else if (sportStatistics.mSportThaWorkout == null) {
            Log.i(TAG, " sportStatistics.mSportThaWorkout is null");
        }
    }

    public static String getDayTimeFormat(long currentTime) {
        return new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(currentTime));
    }
}
