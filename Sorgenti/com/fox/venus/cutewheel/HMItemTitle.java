package com.fox.venus.cutewheel;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.IconDrawable;
import clc.utils.venus.widgets.common.XTextView;

public class HMItemTitle extends BaseDrawableGroup {
    private IconDrawable mLoadingView;
    private XTextView mTextView;

    class C03131 implements AnimatorUpdateListener {
        final /* synthetic */ HMItemTitle this$0;

        public void onAnimationUpdate(ValueAnimator arg0) {
            float proc = -((Float) arg0.getAnimatedValue()).floatValue();
            if (this.this$0.mLoadingView != null && this.this$0.mLoadingView.getParent() != null && this.this$0.mLoadingView.isVisible()) {
                this.this$0.mLoadingView.getMatrix().setRotate(proc, this.this$0.mLoadingView.getRelativeX() + (this.this$0.mLoadingView.getWidth() / 2.0f), this.this$0.mLoadingView.getRelativeY() + (this.this$0.mLoadingView.getHeight() / 2.0f));
                this.this$0.mLoadingView.invalidate();
            }
        }
    }

    public HMItemTitle(XViewWrapper context, int iconResId, String text, RectF textRegion, Align align) {
        super(context);
        if (iconResId != -1) {
            this.mLoadingView = new IconDrawable(context, iconResId);
        }
        this.mTextView = new XTextView(context, text, textRegion, align);
        this.mTextView.resize(textRegion);
        addItem(this.mTextView);
        addItem(this.mLoadingView);
        float halfH = this.localRect.height() / 2.0f;
        this.mTextView.setRelativeX((this.mTextView.getWidth() / 2.0f) - (this.mTextView.getWidth() / 2.0f));
        this.mTextView.setRelativeY(((halfH - (this.mTextView.getHeight() / 2.0f)) + 25.0f) - 25.0f);
    }

    public void setTextColor(int color) {
        if (this.mTextView != null) {
            this.mTextView.setTextColor(color);
        }
    }

    public void setTextSize(int sizePx) {
        if (this.mTextView != null) {
            this.mTextView.setTextSize((float) sizePx);
        }
    }
}
