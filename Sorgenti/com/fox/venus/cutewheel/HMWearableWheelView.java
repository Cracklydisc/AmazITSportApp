package com.fox.venus.cutewheel;

import android.graphics.RectF;
import android.util.AttributeSet;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.wheel.WheelVerticalLayout;
import clc.utils.venus.widgets.wheel.WheelView;
import clc.utils.venus.widgets.wheel.WheelViewItemsLayout;

public class HMWearableWheelView extends WheelView {
    public HMWearableWheelView(XViewWrapper context, AttributeSet attr) {
        this(context, new RectF(0.0f, 0.0f, 320.0f, 800.0f), 0);
    }

    public HMWearableWheelView(XViewWrapper context, RectF regionF) {
        this(context, regionF, 90);
    }

    public HMWearableWheelView(XViewWrapper context, RectF regionF, int itemH) {
        super(context);
        resize(regionF);
        setAnchorForAnimListenerEnable(true);
    }

    protected WheelViewItemsLayout onCreateItemsLayout(XViewWrapper wrapper) {
        WheelVerticalLayout defaultLayout = new WheelVerticalLayout((WheelView) this, new RectF(0.0f, 0.0f, this.localRect.width(), this.localRect.height()), this.localRect.width(), (float) 100);
        calculateGlobalTouchRect();
        return defaultLayout;
    }
}
