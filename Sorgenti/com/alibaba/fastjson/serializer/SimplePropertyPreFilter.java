package com.alibaba.fastjson.serializer;

import java.util.Set;

public class SimplePropertyPreFilter implements PropertyPreFilter {
    private final Class<?> clazz;
    private final Set<String> excludes;
    private final Set<String> includes;
    private int maxLevel;

    public boolean apply(JSONSerializer serializer, Object source, String name) {
        if (source == null) {
            return true;
        }
        if (this.clazz != null && !this.clazz.isInstance(source)) {
            return true;
        }
        if (this.excludes.contains(name)) {
            return false;
        }
        if (this.maxLevel > 0) {
            int level = 0;
            for (SerialContext context = serializer.context; context != null; context = context.parent) {
                level++;
                if (level > this.maxLevel) {
                    return false;
                }
            }
        }
        if (this.includes.size() == 0 || this.includes.contains(name)) {
            return true;
        }
        return false;
    }
}
