package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.IOUtils;
import java.io.IOException;
import java.io.Writer;

public final class SerializeWriter extends Writer {
    private static final ThreadLocal<char[]> bufLocal = new ThreadLocal();
    private static final ThreadLocal<byte[]> bytesBufLocal = new ThreadLocal();
    static final int nonDirectFeautres = (((((((((SerializerFeature.UseSingleQuotes.mask | 0) | SerializerFeature.BrowserCompatible.mask) | SerializerFeature.PrettyFormat.mask) | SerializerFeature.WriteEnumUsingToString.mask) | SerializerFeature.WriteNonStringValueAsString.mask) | SerializerFeature.WriteSlashAsSpecial.mask) | SerializerFeature.IgnoreErrorGetter.mask) | SerializerFeature.WriteClassName.mask) | SerializerFeature.NotWriteDefaultValue.mask);
    protected boolean beanToArray;
    protected boolean browserSecure;
    protected char[] buf;
    protected int count;
    protected boolean disableCircularReferenceDetect;
    protected int features;
    protected char keySeperator;
    protected int maxBufSize;
    protected boolean notWriteDefaultValue;
    protected boolean quoteFieldNames;
    protected long sepcialBits;
    protected boolean sortField;
    protected boolean useSingleQuotes;
    protected boolean writeDirect;
    protected boolean writeEnumUsingName;
    protected boolean writeEnumUsingToString;
    protected boolean writeNonStringValueAsString;
    private final Writer writer;

    public SerializeWriter() {
        this((Writer) null);
    }

    public SerializeWriter(Writer writer) {
        this(writer, JSON.DEFAULT_GENERATE_FEATURE, SerializerFeature.EMPTY);
    }

    public SerializeWriter(Writer writer, int defaultFeatures, SerializerFeature... features) {
        this.maxBufSize = -1;
        this.writer = writer;
        this.buf = (char[]) bufLocal.get();
        if (this.buf != null) {
            bufLocal.set(null);
        } else {
            this.buf = new char[2048];
        }
        int featuresValue = defaultFeatures;
        for (SerializerFeature feature : features) {
            featuresValue |= feature.getMask();
        }
        this.features = featuresValue;
        computeFeatures();
    }

    public void config(SerializerFeature feature, boolean state) {
        if (state) {
            this.features |= feature.getMask();
            if (feature == SerializerFeature.WriteEnumUsingToString) {
                this.features &= SerializerFeature.WriteEnumUsingName.getMask() ^ -1;
            } else if (feature == SerializerFeature.WriteEnumUsingName) {
                this.features &= SerializerFeature.WriteEnumUsingToString.getMask() ^ -1;
            }
        } else {
            this.features &= feature.getMask() ^ -1;
        }
        computeFeatures();
    }

    protected void computeFeatures() {
        boolean z;
        boolean z2 = true;
        if ((this.features & SerializerFeature.QuoteFieldNames.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.quoteFieldNames = z;
        if ((this.features & SerializerFeature.UseSingleQuotes.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.useSingleQuotes = z;
        if ((this.features & SerializerFeature.SortField.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.sortField = z;
        if ((this.features & SerializerFeature.DisableCircularReferenceDetect.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.disableCircularReferenceDetect = z;
        if ((this.features & SerializerFeature.BeanToArray.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.beanToArray = z;
        if ((this.features & SerializerFeature.WriteNonStringValueAsString.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.writeNonStringValueAsString = z;
        if ((this.features & SerializerFeature.NotWriteDefaultValue.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.notWriteDefaultValue = z;
        if ((this.features & SerializerFeature.WriteEnumUsingName.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.writeEnumUsingName = z;
        if ((this.features & SerializerFeature.WriteEnumUsingToString.mask) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.writeEnumUsingToString = z;
        if (this.quoteFieldNames && (this.features & nonDirectFeautres) == 0 && (this.beanToArray || this.writeEnumUsingName)) {
            z = true;
        } else {
            z = false;
        }
        this.writeDirect = z;
        this.keySeperator = this.useSingleQuotes ? '\'' : '\"';
        if ((this.features & SerializerFeature.BrowserSecure.mask) == 0) {
            z2 = false;
        }
        this.browserSecure = z2;
        long j = this.browserSecure ? 5764610843043954687L : (this.features & SerializerFeature.WriteSlashAsSpecial.mask) != 0 ? 140758963191807L : 21474836479L;
        this.sepcialBits = j;
    }

    public boolean isEnabled(SerializerFeature feature) {
        return (this.features & feature.mask) != 0;
    }

    public boolean isEnabled(int feature) {
        return (this.features & feature) != 0;
    }

    public void write(int c) {
        int newcount = this.count + 1;
        if (newcount > this.buf.length) {
            if (this.writer == null) {
                expandCapacity(newcount);
            } else {
                flush();
                newcount = 1;
            }
        }
        this.buf[this.count] = (char) c;
        this.count = newcount;
    }

    public void write(char[] c, int off, int len) {
        if (off < 0 || off > c.length || len < 0 || off + len > c.length || off + len < 0) {
            throw new IndexOutOfBoundsException();
        } else if (len != 0) {
            int newcount = this.count + len;
            if (newcount > this.buf.length) {
                if (this.writer == null) {
                    expandCapacity(newcount);
                } else {
                    do {
                        int rest = this.buf.length - this.count;
                        System.arraycopy(c, off, this.buf, this.count, rest);
                        this.count = this.buf.length;
                        flush();
                        len -= rest;
                        off += rest;
                    } while (len > this.buf.length);
                    newcount = len;
                }
            }
            System.arraycopy(c, off, this.buf, this.count, len);
            this.count = newcount;
        }
    }

    public void expandCapacity(int minimumCapacity) {
        if (this.maxBufSize == -1 || minimumCapacity < this.maxBufSize) {
            int newCapacity = (this.buf.length + (this.buf.length >> 1)) + 1;
            if (newCapacity < minimumCapacity) {
                newCapacity = minimumCapacity;
            }
            char[] newValue = new char[newCapacity];
            System.arraycopy(this.buf, 0, newValue, 0, this.count);
            this.buf = newValue;
            return;
        }
        throw new JSONException("serialize exceeded MAX_OUTPUT_LENGTH=" + this.maxBufSize + ", minimumCapacity=" + minimumCapacity);
    }

    public SerializeWriter append(CharSequence csq) {
        String s = csq == null ? "null" : csq.toString();
        write(s, 0, s.length());
        return this;
    }

    public SerializeWriter append(CharSequence csq, int start, int end) {
        if (csq == null) {
            csq = "null";
        }
        String s = csq.subSequence(start, end).toString();
        write(s, 0, s.length());
        return this;
    }

    public SerializeWriter append(char c) {
        write((int) c);
        return this;
    }

    public void write(String str, int off, int len) {
        int newcount = this.count + len;
        if (newcount > this.buf.length) {
            if (this.writer == null) {
                expandCapacity(newcount);
            } else {
                do {
                    int rest = this.buf.length - this.count;
                    str.getChars(off, off + rest, this.buf, this.count);
                    this.count = this.buf.length;
                    flush();
                    len -= rest;
                    off += rest;
                } while (len > this.buf.length);
                newcount = len;
            }
        }
        str.getChars(off, off + len, this.buf, this.count);
        this.count = newcount;
    }

    public String toString() {
        return new String(this.buf, 0, this.count);
    }

    public void close() {
        if (this.writer != null && this.count > 0) {
            flush();
        }
        if (this.buf.length <= 65536) {
            bufLocal.set(this.buf);
        }
        this.buf = null;
    }

    public void write(String text) {
        if (text == null) {
            writeNull();
        } else {
            write(text, 0, text.length());
        }
    }

    public void writeInt(int i) {
        if (i == Integer.MIN_VALUE) {
            write("-2147483648");
            return;
        }
        int size = i < 0 ? IOUtils.stringSize(-i) + 1 : IOUtils.stringSize(i);
        int newcount = this.count + size;
        if (newcount > this.buf.length) {
            if (this.writer == null) {
                expandCapacity(newcount);
            } else {
                char[] chars = new char[size];
                IOUtils.getChars(i, size, chars);
                write(chars, 0, chars.length);
                return;
            }
        }
        IOUtils.getChars(i, newcount, this.buf);
        this.count = newcount;
    }

    public void writeByteArray(byte[] bytes) {
        if (isEnabled(SerializerFeature.WriteClassName.mask)) {
            writeHex(bytes);
            return;
        }
        int bytesLen = bytes.length;
        int quote = this.useSingleQuotes ? '\'' : '\"';
        if (bytesLen == 0) {
            write(this.useSingleQuotes ? "''" : "\"\"");
            return;
        }
        int s;
        int i;
        int left;
        char[] CA = IOUtils.CA;
        int eLen = (bytesLen / 3) * 3;
        int charsLen = (((bytesLen - 1) / 3) + 1) << 2;
        int offset = this.count;
        int newcount = (this.count + charsLen) + 2;
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write(quote);
                s = 0;
                while (s < eLen) {
                    int s2 = s + 1;
                    s = s2 + 1;
                    s2 = s + 1;
                    i = (((bytes[s] & 255) << 16) | ((bytes[s2] & 255) << 8)) | (bytes[s] & 255);
                    write((int) CA[(i >>> 18) & 63]);
                    write((int) CA[(i >>> 12) & 63]);
                    write((int) CA[(i >>> 6) & 63]);
                    write((int) CA[i & 63]);
                    s = s2;
                }
                left = bytesLen - eLen;
                if (left > 0) {
                    i = ((bytes[eLen] & 255) << 10) | (left == 2 ? (bytes[bytesLen - 1] & 255) << 2 : 0);
                    write((int) CA[i >> 12]);
                    write((int) CA[(i >>> 6) & 63]);
                    write((int) left == 2 ? CA[i & 63] : '=');
                    write(61);
                }
                write(quote);
                return;
            }
            expandCapacity(newcount);
        }
        this.count = newcount;
        int offset2 = offset + 1;
        this.buf[offset] = quote;
        int d = offset2;
        s = 0;
        while (s < eLen) {
            s2 = s + 1;
            s = s2 + 1;
            s2 = s + 1;
            i = (((bytes[s] & 255) << 16) | ((bytes[s2] & 255) << 8)) | (bytes[s] & 255);
            int i2 = d + 1;
            this.buf[d] = CA[(i >>> 18) & 63];
            d = i2 + 1;
            this.buf[i2] = CA[(i >>> 12) & 63];
            i2 = d + 1;
            this.buf[d] = CA[(i >>> 6) & 63];
            d = i2 + 1;
            this.buf[i2] = CA[i & 63];
            s = s2;
        }
        left = bytesLen - eLen;
        if (left > 0) {
            i = ((bytes[eLen] & 255) << 10) | (left == 2 ? (bytes[bytesLen - 1] & 255) << 2 : 0);
            this.buf[newcount - 5] = CA[i >> 12];
            this.buf[newcount - 4] = CA[(i >>> 6) & 63];
            this.buf[newcount - 3] = left == 2 ? CA[i & 63] : '=';
            this.buf[newcount - 2] = '=';
        }
        this.buf[newcount - 1] = quote;
    }

    public void writeHex(byte[] bytes) {
        int b1;
        int i;
        int newcount = (this.count + (bytes.length * 2)) + 3;
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                char[] chars = new char[(bytes.length + 3)];
                int i2 = 0 + 1;
                chars[0] = 'x';
                int i3 = i2 + 1;
                chars[i2] = '\'';
                for (byte b : bytes) {
                    int a = b & 255;
                    int b0 = a >> 4;
                    b1 = a & 15;
                    i2 = i3 + 1;
                    chars[i3] = (char) ((b0 < 10 ? 48 : 55) + b0);
                    i3 = i2 + 1;
                    if (b1 < 10) {
                        i = 48;
                    } else {
                        i = 55;
                    }
                    chars[i2] = (char) (i + b1);
                }
                i2 = i3 + 1;
                chars[i3] = '\'';
                try {
                    this.writer.write(chars);
                    return;
                } catch (IOException ex) {
                    throw new JSONException("writeBytes error.", ex);
                }
            }
            expandCapacity(newcount);
        }
        char[] cArr = this.buf;
        int i4 = this.count;
        this.count = i4 + 1;
        cArr[i4] = 'x';
        cArr = this.buf;
        i4 = this.count;
        this.count = i4 + 1;
        cArr[i4] = '\'';
        for (byte b2 : bytes) {
            a = b2 & 255;
            b0 = a >> 4;
            b1 = a & 15;
            char[] cArr2 = this.buf;
            int i5 = this.count;
            this.count = i5 + 1;
            cArr2[i5] = (char) ((b0 < 10 ? 48 : 55) + b0);
            cArr2 = this.buf;
            i5 = this.count;
            this.count = i5 + 1;
            if (b1 < 10) {
                i = 48;
            } else {
                i = 55;
            }
            cArr2[i5] = (char) (i + b1);
        }
        cArr = this.buf;
        i4 = this.count;
        this.count = i4 + 1;
        cArr[i4] = '\'';
    }

    public void writeFloat(float value, boolean checkWriteClassName) {
        if (Float.isNaN(value) || Float.isInfinite(value)) {
            writeNull();
            return;
        }
        String floatText = Float.toString(value);
        if (isEnabled(SerializerFeature.WriteNullNumberAsZero) && floatText.endsWith(".0")) {
            floatText = floatText.substring(0, floatText.length() - 2);
        }
        write(floatText);
        if (checkWriteClassName && isEnabled(SerializerFeature.WriteClassName)) {
            write(70);
        }
    }

    public void writeDouble(double doubleValue, boolean checkWriteClassName) {
        if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
            writeNull();
            return;
        }
        String doubleText = Double.toString(doubleValue);
        if (isEnabled(SerializerFeature.WriteNullNumberAsZero) && doubleText.endsWith(".0")) {
            doubleText = doubleText.substring(0, doubleText.length() - 2);
        }
        write(doubleText);
        if (checkWriteClassName && isEnabled(SerializerFeature.WriteClassName)) {
            write(68);
        }
    }

    public void writeEnum(Enum<?> value) {
        if (value == null) {
            writeNull();
            return;
        }
        String strVal = null;
        if (this.writeEnumUsingName && !this.writeEnumUsingToString) {
            strVal = value.name();
        } else if (this.writeEnumUsingToString) {
            strVal = value.toString();
        }
        if (strVal != null) {
            int quote = isEnabled(SerializerFeature.UseSingleQuotes) ? '\'' : '\"';
            write(quote);
            write(strVal);
            write(quote);
            return;
        }
        writeInt(value.ordinal());
    }

    public void writeLong(long i) {
        boolean needQuotationMark;
        if (!isEnabled(SerializerFeature.BrowserCompatible) || isEnabled(SerializerFeature.WriteClassName) || (i <= 9007199254740991L && i >= -9007199254740991L)) {
            needQuotationMark = false;
        } else {
            needQuotationMark = true;
        }
        if (i != Long.MIN_VALUE) {
            int size = i < 0 ? IOUtils.stringSize(-i) + 1 : IOUtils.stringSize(i);
            int newcount = this.count + size;
            if (needQuotationMark) {
                newcount += 2;
            }
            if (newcount > this.buf.length) {
                if (this.writer == null) {
                    expandCapacity(newcount);
                } else {
                    char[] chars = new char[size];
                    IOUtils.getChars(i, size, chars);
                    if (needQuotationMark) {
                        write(34);
                        write(chars, 0, chars.length);
                        write(34);
                        return;
                    }
                    write(chars, 0, chars.length);
                    return;
                }
            }
            if (needQuotationMark) {
                this.buf[this.count] = '\"';
                IOUtils.getChars(i, newcount - 1, this.buf);
                this.buf[newcount - 1] = '\"';
            } else {
                IOUtils.getChars(i, newcount, this.buf);
            }
            this.count = newcount;
        } else if (needQuotationMark) {
            write("\"-9223372036854775808\"");
        } else {
            write("-9223372036854775808");
        }
    }

    public void writeNull() {
        write("null");
    }

    public void writeNull(SerializerFeature feature) {
        writeNull(0, feature.mask);
    }

    public void writeNull(int beanFeatures, int feature) {
        if ((beanFeatures & feature) == 0 && (this.features & feature) == 0) {
            writeNull();
        } else if (feature == SerializerFeature.WriteNullListAsEmpty.mask) {
            write("[]");
        } else if (feature == SerializerFeature.WriteNullStringAsEmpty.mask) {
            writeString("");
        } else if (feature == SerializerFeature.WriteNullBooleanAsFalse.mask) {
            write("false");
        } else if (feature == SerializerFeature.WriteNullNumberAsZero.mask) {
            write(48);
        } else {
            writeNull();
        }
    }

    public void writeStringWithDoubleQuote(String text, char seperator) {
        if (text == null) {
            writeNull();
            if (seperator != '\u0000') {
                write((int) seperator);
                return;
            }
            return;
        }
        int i;
        int len = text.length();
        int newcount = (this.count + len) + 2;
        if (seperator != '\u0000') {
            newcount++;
        }
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write(34);
                for (i = 0; i < text.length(); i++) {
                    int ch = text.charAt(i);
                    if (isEnabled(SerializerFeature.BrowserSecure) && (ch == '(' || ch == ')' || ch == '<' || ch == '>')) {
                        write(92);
                        write(117);
                        write((int) IOUtils.DIGITS[(ch >>> 12) & 15]);
                        write((int) IOUtils.DIGITS[(ch >>> 8) & 15]);
                        write((int) IOUtils.DIGITS[(ch >>> 4) & 15]);
                        write((int) IOUtils.DIGITS[ch & 15]);
                    } else {
                        if (isEnabled(SerializerFeature.BrowserCompatible)) {
                            if (ch == '\b' || ch == '\f' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\"' || ch == '/' || ch == '\\') {
                                write(92);
                                write((int) IOUtils.replaceChars[ch]);
                            } else if (ch < ' ') {
                                write(92);
                                write(117);
                                write(48);
                                write(48);
                                write((int) IOUtils.ASCII_CHARS[ch * 2]);
                                write((int) IOUtils.ASCII_CHARS[(ch * 2) + 1]);
                            } else if (ch >= '') {
                                write(92);
                                write(117);
                                write((int) IOUtils.DIGITS[(ch >>> 12) & 15]);
                                write((int) IOUtils.DIGITS[(ch >>> 8) & 15]);
                                write((int) IOUtils.DIGITS[(ch >>> 4) & 15]);
                                write((int) IOUtils.DIGITS[ch & 15]);
                            }
                        } else if ((ch < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[ch] != (byte) 0) || (ch == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                            write(92);
                            if (IOUtils.specicalFlags_doubleQuotes[ch] == (byte) 4) {
                                write(117);
                                write((int) IOUtils.DIGITS[(ch >>> 12) & 15]);
                                write((int) IOUtils.DIGITS[(ch >>> 8) & 15]);
                                write((int) IOUtils.DIGITS[(ch >>> 4) & 15]);
                                write((int) IOUtils.DIGITS[ch & 15]);
                            } else {
                                write((int) IOUtils.replaceChars[ch]);
                            }
                        }
                        write(ch);
                    }
                }
                write(34);
                if (seperator != '\u0000') {
                    write((int) seperator);
                    return;
                }
                return;
            }
            expandCapacity(newcount);
        }
        int start = this.count + 1;
        int end = start + len;
        this.buf[this.count] = '\"';
        text.getChars(0, len, this.buf, start);
        this.count = newcount;
        int lastSpecialIndex;
        char ch2;
        if (isEnabled(SerializerFeature.BrowserCompatible)) {
            lastSpecialIndex = -1;
            for (i = start; i < end; i++) {
                ch2 = this.buf[i];
                if (ch2 == '\"' || ch2 == '/' || ch2 == '\\') {
                    lastSpecialIndex = i;
                    newcount++;
                } else if (ch2 == '\b' || ch2 == '\f' || ch2 == '\n' || ch2 == '\r' || ch2 == '\t') {
                    lastSpecialIndex = i;
                    newcount++;
                } else if (ch2 < ' ') {
                    lastSpecialIndex = i;
                    newcount += 5;
                } else if (ch2 >= '') {
                    lastSpecialIndex = i;
                    newcount += 5;
                }
            }
            if (newcount > this.buf.length) {
                expandCapacity(newcount);
            }
            this.count = newcount;
            for (i = lastSpecialIndex; i >= start; i--) {
                ch2 = this.buf[i];
                if (ch2 == '\b' || ch2 == '\f' || ch2 == '\n' || ch2 == '\r' || ch2 == '\t') {
                    System.arraycopy(this.buf, i + 1, this.buf, i + 2, (end - i) - 1);
                    this.buf[i] = '\\';
                    this.buf[i + 1] = IOUtils.replaceChars[ch2];
                    end++;
                } else if (ch2 == '\"' || ch2 == '/' || ch2 == '\\') {
                    System.arraycopy(this.buf, i + 1, this.buf, i + 2, (end - i) - 1);
                    this.buf[i] = '\\';
                    this.buf[i + 1] = ch2;
                    end++;
                } else if (ch2 < ' ') {
                    System.arraycopy(this.buf, i + 1, this.buf, i + 6, (end - i) - 1);
                    this.buf[i] = '\\';
                    this.buf[i + 1] = 'u';
                    this.buf[i + 2] = '0';
                    this.buf[i + 3] = '0';
                    this.buf[i + 4] = IOUtils.ASCII_CHARS[ch2 * 2];
                    this.buf[i + 5] = IOUtils.ASCII_CHARS[(ch2 * 2) + 1];
                    end += 5;
                } else if (ch2 >= '') {
                    System.arraycopy(this.buf, i + 1, this.buf, i + 6, (end - i) - 1);
                    this.buf[i] = '\\';
                    this.buf[i + 1] = 'u';
                    this.buf[i + 2] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                    this.buf[i + 3] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                    this.buf[i + 4] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                    this.buf[i + 5] = IOUtils.DIGITS[ch2 & 15];
                    end += 5;
                }
            }
            if (seperator != '\u0000') {
                this.buf[this.count - 2] = '\"';
                this.buf[this.count - 1] = seperator;
                return;
            }
            this.buf[this.count - 1] = '\"';
            return;
        }
        int specialCount = 0;
        lastSpecialIndex = -1;
        int firstSpecialIndex = -1;
        char lastSpecial = '\u0000';
        for (i = start; i < end; i++) {
            ch2 = this.buf[i];
            if (ch2 < ']') {
                boolean special = (ch2 < '@' && (this.sepcialBits & (1 << ch2)) != 0) || ch2 == '\\';
                if (special) {
                    specialCount++;
                    lastSpecialIndex = i;
                    lastSpecial = ch2;
                    if (ch2 == '(' || ch2 == ')' || ch2 == '<' || ch2 == '>' || (ch2 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[ch2] == (byte) 4)) {
                        newcount += 4;
                    }
                    if (firstSpecialIndex == -1) {
                        firstSpecialIndex = i;
                    }
                }
            } else if (ch2 >= '' && (ch2 == ' ' || ch2 == ' ' || ch2 < ' ')) {
                if (firstSpecialIndex == -1) {
                    firstSpecialIndex = i;
                }
                specialCount++;
                lastSpecialIndex = i;
                lastSpecial = ch2;
                newcount += 4;
            }
        }
        if (specialCount > 0) {
            newcount += specialCount;
            if (newcount > this.buf.length) {
                expandCapacity(newcount);
            }
            this.count = newcount;
            int i2;
            int i3;
            if (specialCount == 1) {
                if (lastSpecial == ' ') {
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (end - lastSpecialIndex) - 1);
                    this.buf[lastSpecialIndex] = '\\';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = 'u';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '0';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    this.buf[lastSpecialIndex + 1] = '8';
                } else if (lastSpecial == ' ') {
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (end - lastSpecialIndex) - 1);
                    this.buf[lastSpecialIndex] = '\\';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = 'u';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '0';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    this.buf[lastSpecialIndex + 1] = '9';
                } else if (lastSpecial == '(' || lastSpecial == ')' || lastSpecial == '<' || lastSpecial == '>') {
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (end - lastSpecialIndex) - 1);
                    this.buf[lastSpecialIndex] = '\\';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = 'u';
                    ch2 = lastSpecial;
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                    this.buf[lastSpecialIndex + 1] = IOUtils.DIGITS[ch2 & 15];
                } else {
                    ch2 = lastSpecial;
                    if (ch2 >= IOUtils.specicalFlags_doubleQuotes.length || IOUtils.specicalFlags_doubleQuotes[ch2] != (byte) 4) {
                        System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 2, (end - lastSpecialIndex) - 1);
                        this.buf[lastSpecialIndex] = '\\';
                        this.buf[lastSpecialIndex + 1] = IOUtils.replaceChars[ch2];
                    } else {
                        System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (end - lastSpecialIndex) - 1);
                        i2 = lastSpecialIndex;
                        i3 = i2 + 1;
                        this.buf[i2] = '\\';
                        i2 = i3 + 1;
                        this.buf[i3] = 'u';
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[ch2 & 15];
                    }
                }
            } else if (specialCount > 1) {
                i2 = firstSpecialIndex;
                for (i = firstSpecialIndex - start; i < text.length(); i++) {
                    ch2 = text.charAt(i);
                    if (this.browserSecure && (ch2 == '(' || ch2 == ')' || ch2 == '<' || ch2 == '>')) {
                        i3 = i2 + 1;
                        this.buf[i2] = '\\';
                        i2 = i3 + 1;
                        this.buf[i3] = 'u';
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[ch2 & 15];
                        end += 5;
                    } else if ((ch2 < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[ch2] != (byte) 0) || (ch2 == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                        i3 = i2 + 1;
                        this.buf[i2] = '\\';
                        if (IOUtils.specicalFlags_doubleQuotes[ch2] == (byte) 4) {
                            i2 = i3 + 1;
                            this.buf[i3] = 'u';
                            i3 = i2 + 1;
                            this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                            i2 = i3 + 1;
                            this.buf[i3] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                            i3 = i2 + 1;
                            this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                            i2 = i3 + 1;
                            this.buf[i3] = IOUtils.DIGITS[ch2 & 15];
                            end += 5;
                        } else {
                            i2 = i3 + 1;
                            this.buf[i3] = IOUtils.replaceChars[ch2];
                            end++;
                        }
                    } else if (ch2 == ' ' || ch2 == ' ') {
                        i3 = i2 + 1;
                        this.buf[i2] = '\\';
                        i2 = i3 + 1;
                        this.buf[i3] = 'u';
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 12) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[(ch2 >>> 8) & 15];
                        i3 = i2 + 1;
                        this.buf[i2] = IOUtils.DIGITS[(ch2 >>> 4) & 15];
                        i2 = i3 + 1;
                        this.buf[i3] = IOUtils.DIGITS[ch2 & 15];
                        end += 5;
                    } else {
                        i3 = i2 + 1;
                        this.buf[i2] = ch2;
                        i2 = i3;
                    }
                }
            }
        }
        if (seperator != '\u0000') {
            this.buf[this.count - 2] = '\"';
            this.buf[this.count - 1] = seperator;
            return;
        }
        this.buf[this.count - 1] = '\"';
    }

    public void write(boolean value) {
        if (value) {
            write("true");
        } else {
            write("false");
        }
    }

    public void writeFieldValue(char seperator, String name, int value) {
        if (value == Integer.MIN_VALUE || !this.quoteFieldNames) {
            write((int) seperator);
            writeFieldName(name);
            writeInt(value);
            return;
        }
        int intSize = value < 0 ? IOUtils.stringSize(-value) + 1 : IOUtils.stringSize(value);
        int nameLen = name.length();
        int newcount = ((this.count + nameLen) + 4) + intSize;
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write((int) seperator);
                writeFieldName(name);
                writeInt(value);
                return;
            }
            expandCapacity(newcount);
        }
        int start = this.count;
        this.count = newcount;
        this.buf[start] = seperator;
        int nameEnd = (start + nameLen) + 1;
        this.buf[start + 1] = this.keySeperator;
        name.getChars(0, nameLen, this.buf, start + 2);
        this.buf[nameEnd + 1] = this.keySeperator;
        this.buf[nameEnd + 2] = ':';
        IOUtils.getChars(value, this.count, this.buf);
    }

    public void writeFieldValue(char seperator, String name, long value) {
        if (value == Long.MIN_VALUE || !this.quoteFieldNames) {
            write((int) seperator);
            writeFieldName(name);
            writeLong(value);
            return;
        }
        int intSize = value < 0 ? IOUtils.stringSize(-value) + 1 : IOUtils.stringSize(value);
        int nameLen = name.length();
        int newcount = ((this.count + nameLen) + 4) + intSize;
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write((int) seperator);
                writeFieldName(name);
                writeLong(value);
                return;
            }
            expandCapacity(newcount);
        }
        int start = this.count;
        this.count = newcount;
        this.buf[start] = seperator;
        int nameEnd = (start + nameLen) + 1;
        this.buf[start + 1] = this.keySeperator;
        name.getChars(0, nameLen, this.buf, start + 2);
        this.buf[nameEnd + 1] = this.keySeperator;
        this.buf[nameEnd + 2] = ':';
        IOUtils.getChars(value, this.count, this.buf);
    }

    public void writeFieldValue(char seperator, String name, double value) {
        write((int) seperator);
        writeFieldName(name);
        writeDouble(value, false);
    }

    public void writeFieldValue(char seperator, String name, String value) {
        if (!this.quoteFieldNames) {
            write((int) seperator);
            writeFieldName(name);
            if (value == null) {
                writeNull();
            } else {
                writeString(value);
            }
        } else if (this.useSingleQuotes) {
            write((int) seperator);
            writeFieldName(name);
            if (value == null) {
                writeNull();
            } else {
                writeString(value);
            }
        } else if (isEnabled(SerializerFeature.BrowserCompatible)) {
            write((int) seperator);
            writeStringWithDoubleQuote(name, ':');
            writeStringWithDoubleQuote(value, '\u0000');
        } else {
            writeFieldValueStringWithDoubleQuoteCheck(seperator, name, value);
        }
    }

    public void writeFieldValueStringWithDoubleQuoteCheck(char seperator, String name, String value) {
        int valueLen;
        int nameLen = name.length();
        int newcount = this.count;
        if (value == null) {
            valueLen = 4;
            newcount += nameLen + 8;
        } else {
            valueLen = value.length();
            newcount += (nameLen + valueLen) + 6;
        }
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write((int) seperator);
                writeStringWithDoubleQuote(name, ':');
                writeStringWithDoubleQuote(value, '\u0000');
                return;
            }
            expandCapacity(newcount);
        }
        this.buf[this.count] = seperator;
        int nameStart = this.count + 2;
        int nameEnd = nameStart + nameLen;
        this.buf[this.count + 1] = '\"';
        name.getChars(0, nameLen, this.buf, nameStart);
        this.count = newcount;
        this.buf[nameEnd] = '\"';
        int i = nameEnd + 1;
        int i2 = i + 1;
        this.buf[i] = ':';
        if (value == null) {
            i = i2 + 1;
            this.buf[i2] = 'n';
            i2 = i + 1;
            this.buf[i] = 'u';
            i = i2 + 1;
            this.buf[i2] = 'l';
            i2 = i + 1;
            this.buf[i] = 'l';
            return;
        }
        int i3;
        i = i2 + 1;
        this.buf[i2] = '\"';
        int valueStart = i;
        int valueEnd = valueStart + valueLen;
        value.getChars(0, valueLen, this.buf, valueStart);
        int specialCount = 0;
        int lastSpecialIndex = -1;
        int firstSpecialIndex = -1;
        char lastSpecial = '\u0000';
        for (i3 = valueStart; i3 < valueEnd; i3++) {
            char ch = this.buf[i3];
            if (ch < ']') {
                boolean special = (ch < '@' && (this.sepcialBits & (1 << ch)) != 0) || ch == '\\';
                if (special) {
                    specialCount++;
                    lastSpecialIndex = i3;
                    lastSpecial = ch;
                    if (ch == '(' || ch == ')' || ch == '<' || ch == '>' || (ch < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[ch] == (byte) 4)) {
                        newcount += 4;
                    }
                    if (firstSpecialIndex == -1) {
                        firstSpecialIndex = i3;
                    }
                }
            } else if (ch >= '' && (ch == ' ' || ch == ' ' || ch < ' ')) {
                if (firstSpecialIndex == -1) {
                    firstSpecialIndex = i3;
                }
                specialCount++;
                lastSpecialIndex = i3;
                lastSpecial = ch;
                newcount += 4;
            }
        }
        if (specialCount > 0) {
            newcount += specialCount;
            if (newcount > this.buf.length) {
                expandCapacity(newcount);
            }
            this.count = newcount;
            int i4;
            int i5;
            if (specialCount == 1) {
                if (lastSpecial == ' ') {
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (valueEnd - lastSpecialIndex) - 1);
                    this.buf[lastSpecialIndex] = '\\';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = 'u';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '0';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    this.buf[lastSpecialIndex + 1] = '8';
                } else if (lastSpecial == ' ') {
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (valueEnd - lastSpecialIndex) - 1);
                    this.buf[lastSpecialIndex] = '\\';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = 'u';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '0';
                    lastSpecialIndex++;
                    this.buf[lastSpecialIndex] = '2';
                    this.buf[lastSpecialIndex + 1] = '9';
                } else if (lastSpecial == '(' || lastSpecial == ')' || lastSpecial == '<' || lastSpecial == '>') {
                    ch = lastSpecial;
                    System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (valueEnd - lastSpecialIndex) - 1);
                    i4 = lastSpecialIndex;
                    i5 = i4 + 1;
                    this.buf[i4] = '\\';
                    i4 = i5 + 1;
                    this.buf[i5] = 'u';
                    i5 = i4 + 1;
                    this.buf[i4] = IOUtils.DIGITS[(ch >>> 12) & 15];
                    i4 = i5 + 1;
                    this.buf[i5] = IOUtils.DIGITS[(ch >>> 8) & 15];
                    i5 = i4 + 1;
                    this.buf[i4] = IOUtils.DIGITS[(ch >>> 4) & 15];
                    i4 = i5 + 1;
                    this.buf[i5] = IOUtils.DIGITS[ch & 15];
                } else {
                    ch = lastSpecial;
                    if (ch >= IOUtils.specicalFlags_doubleQuotes.length || IOUtils.specicalFlags_doubleQuotes[ch] != (byte) 4) {
                        System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 2, (valueEnd - lastSpecialIndex) - 1);
                        this.buf[lastSpecialIndex] = '\\';
                        this.buf[lastSpecialIndex + 1] = IOUtils.replaceChars[ch];
                    } else {
                        System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 6, (valueEnd - lastSpecialIndex) - 1);
                        i4 = lastSpecialIndex;
                        i5 = i4 + 1;
                        this.buf[i4] = '\\';
                        i4 = i5 + 1;
                        this.buf[i5] = 'u';
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 12) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[(ch >>> 8) & 15];
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 4) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[ch & 15];
                    }
                }
            } else if (specialCount > 1) {
                i4 = firstSpecialIndex;
                for (i3 = firstSpecialIndex - valueStart; i3 < value.length(); i3++) {
                    ch = value.charAt(i3);
                    if (this.browserSecure && (ch == '(' || ch == ')' || ch == '<' || ch == '>')) {
                        i5 = i4 + 1;
                        this.buf[i4] = '\\';
                        i4 = i5 + 1;
                        this.buf[i5] = 'u';
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 12) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[(ch >>> 8) & 15];
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 4) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[ch & 15];
                        valueEnd += 5;
                    } else if ((ch < IOUtils.specicalFlags_doubleQuotes.length && IOUtils.specicalFlags_doubleQuotes[ch] != (byte) 0) || (ch == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                        i5 = i4 + 1;
                        this.buf[i4] = '\\';
                        if (IOUtils.specicalFlags_doubleQuotes[ch] == (byte) 4) {
                            i4 = i5 + 1;
                            this.buf[i5] = 'u';
                            i5 = i4 + 1;
                            this.buf[i4] = IOUtils.DIGITS[(ch >>> 12) & 15];
                            i4 = i5 + 1;
                            this.buf[i5] = IOUtils.DIGITS[(ch >>> 8) & 15];
                            i5 = i4 + 1;
                            this.buf[i4] = IOUtils.DIGITS[(ch >>> 4) & 15];
                            i4 = i5 + 1;
                            this.buf[i5] = IOUtils.DIGITS[ch & 15];
                            valueEnd += 5;
                        } else {
                            i4 = i5 + 1;
                            this.buf[i5] = IOUtils.replaceChars[ch];
                            valueEnd++;
                        }
                    } else if (ch == ' ' || ch == ' ') {
                        i5 = i4 + 1;
                        this.buf[i4] = '\\';
                        i4 = i5 + 1;
                        this.buf[i5] = 'u';
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 12) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[(ch >>> 8) & 15];
                        i5 = i4 + 1;
                        this.buf[i4] = IOUtils.DIGITS[(ch >>> 4) & 15];
                        i4 = i5 + 1;
                        this.buf[i5] = IOUtils.DIGITS[ch & 15];
                        valueEnd += 5;
                    } else {
                        i5 = i4 + 1;
                        this.buf[i4] = ch;
                        i4 = i5;
                    }
                }
            }
        }
        this.buf[this.count - 1] = '\"';
    }

    public void writeString(String text) {
        if (this.useSingleQuotes) {
            writeStringWithSingleQuote(text);
        } else {
            writeStringWithDoubleQuote(text, '\u0000');
        }
    }

    protected void writeStringWithSingleQuote(String text) {
        int newcount;
        if (text == null) {
            newcount = this.count + 4;
            if (newcount > this.buf.length) {
                expandCapacity(newcount);
            }
            "null".getChars(0, 4, this.buf, this.count);
            this.count = newcount;
            return;
        }
        int i;
        int len = text.length();
        newcount = (this.count + len) + 2;
        if (newcount > this.buf.length) {
            if (this.writer != null) {
                write(39);
                for (i = 0; i < text.length(); i++) {
                    int ch = text.charAt(i);
                    if (ch <= '\r' || ch == '\\' || ch == '\'' || (ch == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                        write(92);
                        write(IOUtils.replaceChars[ch]);
                    } else {
                        write(ch);
                    }
                }
                write(39);
                return;
            }
            expandCapacity(newcount);
        }
        int start = this.count + 1;
        int end = start + len;
        this.buf[this.count] = '\'';
        text.getChars(0, len, this.buf, start);
        this.count = newcount;
        int specialCount = 0;
        int lastSpecialIndex = -1;
        char lastSpecial = '\u0000';
        for (i = start; i < end; i++) {
            char ch2 = this.buf[i];
            if (ch2 <= '\r' || ch2 == '\\' || ch2 == '\'' || (ch2 == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                specialCount++;
                lastSpecialIndex = i;
                lastSpecial = ch2;
            }
        }
        newcount += specialCount;
        if (newcount > this.buf.length) {
            expandCapacity(newcount);
        }
        this.count = newcount;
        if (specialCount == 1) {
            System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 2, (end - lastSpecialIndex) - 1);
            this.buf[lastSpecialIndex] = '\\';
            this.buf[lastSpecialIndex + 1] = IOUtils.replaceChars[lastSpecial];
        } else if (specialCount > 1) {
            System.arraycopy(this.buf, lastSpecialIndex + 1, this.buf, lastSpecialIndex + 2, (end - lastSpecialIndex) - 1);
            this.buf[lastSpecialIndex] = '\\';
            lastSpecialIndex++;
            this.buf[lastSpecialIndex] = IOUtils.replaceChars[lastSpecial];
            end++;
            for (i = lastSpecialIndex - 2; i >= start; i--) {
                ch2 = this.buf[i];
                if (ch2 <= '\r' || ch2 == '\\' || ch2 == '\'' || (ch2 == '/' && isEnabled(SerializerFeature.WriteSlashAsSpecial))) {
                    System.arraycopy(this.buf, i + 1, this.buf, i + 2, (end - i) - 1);
                    this.buf[i] = '\\';
                    this.buf[i + 1] = IOUtils.replaceChars[ch2];
                    end++;
                }
            }
        }
        this.buf[this.count - 1] = '\'';
    }

    public void writeFieldName(String key) {
        writeFieldName(key, false);
    }

    public void writeFieldName(String key, boolean checkSpecial) {
        if (key == null) {
            write("null:");
        } else if (this.useSingleQuotes) {
            if (this.quoteFieldNames) {
                writeStringWithSingleQuote(key);
                write(58);
                return;
            }
            writeKeyWithSingleQuoteIfHasSpecial(key);
        } else if (this.quoteFieldNames) {
            writeStringWithDoubleQuote(key, ':');
        } else {
            boolean hashSpecial;
            if (key.length() == 0) {
                hashSpecial = true;
            } else {
                hashSpecial = false;
            }
            for (int i = 0; i < key.length(); i++) {
                boolean special;
                char ch = key.charAt(i);
                if ((ch >= '@' || (this.sepcialBits & (1 << ch)) == 0) && ch != '\\') {
                    special = false;
                } else {
                    special = true;
                }
                if (special) {
                    hashSpecial = true;
                    break;
                }
            }
            if (hashSpecial) {
                writeStringWithDoubleQuote(key, ':');
                return;
            }
            write(key);
            write(58);
        }
    }

    private void writeKeyWithSingleQuoteIfHasSpecial(String text) {
        boolean hasSpecial;
        int i;
        char ch;
        byte[] specicalFlags_singleQuotes = IOUtils.specicalFlags_singleQuotes;
        int len = text.length();
        int newcount = (this.count + len) + 1;
        if (newcount > this.buf.length) {
            if (this.writer == null) {
                expandCapacity(newcount);
            } else if (len == 0) {
                write(39);
                write(39);
                write(58);
                return;
            } else {
                hasSpecial = false;
                for (i = 0; i < len; i++) {
                    ch = text.charAt(i);
                    if (ch < specicalFlags_singleQuotes.length && specicalFlags_singleQuotes[ch] != (byte) 0) {
                        hasSpecial = true;
                        break;
                    }
                }
                if (hasSpecial) {
                    write(39);
                }
                for (i = 0; i < len; i++) {
                    int ch2 = text.charAt(i);
                    if (ch2 >= specicalFlags_singleQuotes.length || specicalFlags_singleQuotes[ch2] == (byte) 0) {
                        write(ch2);
                    } else {
                        write(92);
                        write(IOUtils.replaceChars[ch2]);
                    }
                }
                if (hasSpecial) {
                    write(39);
                }
                write(58);
                return;
            }
        }
        if (len == 0) {
            if (this.count + 3 > this.buf.length) {
                expandCapacity(this.count + 3);
            }
            char[] cArr = this.buf;
            int i2 = this.count;
            this.count = i2 + 1;
            cArr[i2] = '\'';
            cArr = this.buf;
            i2 = this.count;
            this.count = i2 + 1;
            cArr[i2] = '\'';
            cArr = this.buf;
            i2 = this.count;
            this.count = i2 + 1;
            cArr[i2] = ':';
            return;
        }
        int start = this.count;
        int end = start + len;
        text.getChars(0, len, this.buf, start);
        this.count = newcount;
        hasSpecial = false;
        i = start;
        while (i < end) {
            ch = this.buf[i];
            if (ch < specicalFlags_singleQuotes.length && specicalFlags_singleQuotes[ch] != (byte) 0) {
                if (hasSpecial) {
                    newcount++;
                    if (newcount > this.buf.length) {
                        expandCapacity(newcount);
                    }
                    this.count = newcount;
                    System.arraycopy(this.buf, i + 1, this.buf, i + 2, end - i);
                    this.buf[i] = '\\';
                    i++;
                    this.buf[i] = IOUtils.replaceChars[ch];
                    end++;
                } else {
                    newcount += 3;
                    if (newcount > this.buf.length) {
                        expandCapacity(newcount);
                    }
                    this.count = newcount;
                    System.arraycopy(this.buf, i + 1, this.buf, i + 3, (end - i) - 1);
                    System.arraycopy(this.buf, 0, this.buf, 1, i);
                    this.buf[start] = '\'';
                    i++;
                    this.buf[i] = '\\';
                    i++;
                    this.buf[i] = IOUtils.replaceChars[ch];
                    end += 2;
                    this.buf[this.count - 2] = '\'';
                    hasSpecial = true;
                }
            }
            i++;
        }
        this.buf[newcount - 1] = ':';
    }

    public void flush() {
        if (this.writer != null) {
            try {
                this.writer.write(this.buf, 0, this.count);
                this.writer.flush();
                this.count = 0;
            } catch (IOException e) {
                throw new JSONException(e.getMessage(), e);
            }
        }
    }
}
