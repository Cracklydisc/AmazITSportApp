package com.alibaba.fastjson.serializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONStreamAware;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.parser.deserializer.Jdk8DateCodec;
import com.alibaba.fastjson.parser.deserializer.OptionalCodec;
import com.alibaba.fastjson.support.springfox.SwaggerJsonSerializer;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.FieldInfo;
import com.alibaba.fastjson.util.IdentityHashMap;
import com.alibaba.fastjson.util.ServiceLoader;
import com.alibaba.fastjson.util.TypeUtils;
import java.io.File;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import javax.xml.datatype.XMLGregorianCalendar;

public class SerializeConfig {
    private static boolean awtError = false;
    public static final SerializeConfig globalInstance = new SerializeConfig();
    private static boolean guavaError = false;
    private static boolean jdk8Error = false;
    private static boolean jsonnullError = false;
    private static boolean oracleJdbcError = false;
    private static boolean springfoxError = false;
    private boolean asm;
    private ASMSerializerFactory asmFactory;
    private final boolean fieldBased;
    public PropertyNamingStrategy propertyNamingStrategy;
    private final IdentityHashMap<Type, ObjectSerializer> serializers;
    protected String typeKey;

    private final JavaBeanSerializer createASMSerializer(SerializeBeanInfo beanInfo) throws Exception {
        JavaBeanSerializer serializer = this.asmFactory.createJavaBeanSerializer(beanInfo);
        for (FieldSerializer fieldDeser : serializer.sortedGetters) {
            Class<?> fieldClass = fieldDeser.fieldInfo.fieldClass;
            if (fieldClass.isEnum() && !(getObjectWriter(fieldClass) instanceof EnumSerializer)) {
                serializer.writeDirect = false;
            }
        }
        return serializer;
    }

    public final ObjectSerializer createJavaBeanSerializer(Class<?> clazz) {
        SerializeBeanInfo beanInfo = TypeUtils.buildBeanInfo(clazz, null, this.propertyNamingStrategy, this.fieldBased);
        if (beanInfo.fields.length == 0 && Iterable.class.isAssignableFrom(clazz)) {
            return MiscCodec.instance;
        }
        return createJavaBeanSerializer(beanInfo);
    }

    public ObjectSerializer createJavaBeanSerializer(SerializeBeanInfo beanInfo) {
        JSONType jsonType = beanInfo.jsonType;
        if (jsonType != null) {
            Class<?> serializerClass = jsonType.serializer();
            if (serializerClass != Void.class) {
                try {
                    Object seralizer = serializerClass.newInstance();
                    if (seralizer instanceof ObjectSerializer) {
                        return (ObjectSerializer) seralizer;
                    }
                } catch (Throwable th) {
                }
            }
            if (!jsonType.asm()) {
                this.asm = false;
            }
            for (SerializerFeature feature : jsonType.serialzeFeatures()) {
                if (SerializerFeature.WriteNonStringValueAsString == feature || SerializerFeature.WriteEnumUsingToString == feature || SerializerFeature.NotWriteDefaultValue == feature) {
                    this.asm = false;
                    break;
                }
            }
        }
        Class<?> clazz = beanInfo.beanType;
        if (!Modifier.isPublic(beanInfo.beanType.getModifiers())) {
            return new JavaBeanSerializer(beanInfo);
        }
        boolean asm = this.asm && !this.fieldBased;
        if ((asm && this.asmFactory.classLoader.isExternalClass(clazz)) || clazz == Serializable.class || clazz == Object.class) {
            asm = false;
        }
        if (asm && !ASMUtils.checkName(clazz.getSimpleName())) {
            asm = false;
        }
        if (asm && beanInfo.beanType.isInterface()) {
            asm = false;
        }
        if (asm) {
            for (FieldInfo fieldInfo : beanInfo.fields) {
                Field field = fieldInfo.field;
                if (field != null && !field.getType().equals(fieldInfo.fieldClass)) {
                    asm = false;
                    break;
                }
                Method method = fieldInfo.method;
                if (method != null && !method.getReturnType().equals(fieldInfo.fieldClass)) {
                    asm = false;
                    break;
                }
                JSONField annotation = fieldInfo.getAnnotation();
                if (annotation != null) {
                    String format = annotation.format();
                    if (format.length() != 0 && (fieldInfo.fieldClass != String.class || !"trim".equals(format))) {
                        asm = false;
                        break;
                    } else if (!ASMUtils.checkName(annotation.name()) || annotation.jsonDirect() || annotation.serializeUsing() != Void.class || annotation.unwrapped()) {
                        asm = false;
                        break;
                    } else {
                        for (SerializerFeature feature2 : annotation.serialzeFeatures()) {
                            if (SerializerFeature.WriteNonStringValueAsString == feature2 || SerializerFeature.WriteEnumUsingToString == feature2 || SerializerFeature.NotWriteDefaultValue == feature2 || SerializerFeature.WriteClassName == feature2) {
                                asm = false;
                                break;
                            }
                        }
                        if (TypeUtils.isAnnotationPresentOneToMany(method)) {
                            asm = true;
                            break;
                        }
                    }
                }
            }
        }
        if (asm) {
            try {
                ObjectSerializer asmSerializer = createASMSerializer(beanInfo);
                if (asmSerializer != null) {
                    return asmSerializer;
                }
            } catch (ClassNotFoundException e) {
            } catch (ClassFormatError e2) {
            } catch (ClassCastException e3) {
            } catch (Throwable e4) {
                JSONException jSONException = new JSONException("create asm serializer error, class " + clazz, e4);
            }
        }
        return new JavaBeanSerializer(beanInfo);
    }

    public static SerializeConfig getGlobalInstance() {
        return globalInstance;
    }

    public SerializeConfig() {
        this(1024);
    }

    public SerializeConfig(int tableSize) {
        this(tableSize, false);
    }

    public SerializeConfig(int tableSize, boolean fieldBase) {
        this.asm = !ASMUtils.IS_ANDROID;
        this.typeKey = JSON.DEFAULT_TYPE_KEY;
        this.fieldBased = fieldBase;
        this.serializers = new IdentityHashMap(tableSize);
        try {
            if (this.asm) {
                this.asmFactory = new ASMSerializerFactory();
            }
        } catch (Throwable th) {
            this.asm = false;
        }
        put(Boolean.class, BooleanCodec.instance);
        put(Character.class, CharacterCodec.instance);
        put(Byte.class, IntegerCodec.instance);
        put(Short.class, IntegerCodec.instance);
        put(Integer.class, IntegerCodec.instance);
        put(Long.class, LongCodec.instance);
        put(Float.class, FloatCodec.instance);
        put(Double.class, DoubleSerializer.instance);
        put(BigDecimal.class, BigDecimalCodec.instance);
        put(BigInteger.class, BigIntegerCodec.instance);
        put(String.class, StringCodec.instance);
        put(byte[].class, PrimitiveArraySerializer.instance);
        put(short[].class, PrimitiveArraySerializer.instance);
        put(int[].class, PrimitiveArraySerializer.instance);
        put(long[].class, PrimitiveArraySerializer.instance);
        put(float[].class, PrimitiveArraySerializer.instance);
        put(double[].class, PrimitiveArraySerializer.instance);
        put(boolean[].class, PrimitiveArraySerializer.instance);
        put(char[].class, PrimitiveArraySerializer.instance);
        put(Object[].class, ObjectArrayCodec.instance);
        put(Class.class, MiscCodec.instance);
        put(SimpleDateFormat.class, MiscCodec.instance);
        put(Currency.class, new MiscCodec());
        put(TimeZone.class, MiscCodec.instance);
        put(InetAddress.class, MiscCodec.instance);
        put(Inet4Address.class, MiscCodec.instance);
        put(Inet6Address.class, MiscCodec.instance);
        put(InetSocketAddress.class, MiscCodec.instance);
        put(File.class, MiscCodec.instance);
        put(Appendable.class, AppendableSerializer.instance);
        put(StringBuffer.class, AppendableSerializer.instance);
        put(StringBuilder.class, AppendableSerializer.instance);
        put(Charset.class, ToStringSerializer.instance);
        put(Pattern.class, ToStringSerializer.instance);
        put(Locale.class, ToStringSerializer.instance);
        put(URI.class, ToStringSerializer.instance);
        put(URL.class, ToStringSerializer.instance);
        put(UUID.class, ToStringSerializer.instance);
        put(AtomicBoolean.class, AtomicCodec.instance);
        put(AtomicInteger.class, AtomicCodec.instance);
        put(AtomicLong.class, AtomicCodec.instance);
        put(AtomicReference.class, ReferenceCodec.instance);
        put(AtomicIntegerArray.class, AtomicCodec.instance);
        put(AtomicLongArray.class, AtomicCodec.instance);
        put(WeakReference.class, ReferenceCodec.instance);
        put(SoftReference.class, ReferenceCodec.instance);
        put(LinkedList.class, CollectionCodec.instance);
    }

    public ObjectSerializer getObjectWriter(Class<?> clazz) {
        return getObjectWriter(clazz, true);
    }

    private ObjectSerializer getObjectWriter(Class<?> clazz, boolean create) {
        AutowiredObjectSerializer autowired;
        ObjectSerializer objectSerializer = (ObjectSerializer) this.serializers.get(clazz);
        if (objectSerializer == null) {
            try {
                for (Object o : ServiceLoader.load(AutowiredObjectSerializer.class, Thread.currentThread().getContextClassLoader())) {
                    if (o instanceof AutowiredObjectSerializer) {
                        autowired = (AutowiredObjectSerializer) o;
                        for (Type forType : autowired.getAutowiredFor()) {
                            put(forType, autowired);
                        }
                    }
                }
            } catch (ClassCastException e) {
            }
            objectSerializer = (ObjectSerializer) this.serializers.get(clazz);
        }
        if (objectSerializer == null) {
            ClassLoader classLoader = JSON.class.getClassLoader();
            if (classLoader != Thread.currentThread().getContextClassLoader()) {
                try {
                    for (Object o2 : ServiceLoader.load(AutowiredObjectSerializer.class, classLoader)) {
                        if (o2 instanceof AutowiredObjectSerializer) {
                            autowired = (AutowiredObjectSerializer) o2;
                            for (Type forType2 : autowired.getAutowiredFor()) {
                                put(forType2, autowired);
                            }
                        }
                    }
                } catch (ClassCastException e2) {
                }
                objectSerializer = (ObjectSerializer) this.serializers.get(clazz);
            }
        }
        if (objectSerializer == null) {
            String className = clazz.getName();
            if (Map.class.isAssignableFrom(clazz)) {
                objectSerializer = MapSerializer.instance;
                put(clazz, objectSerializer);
            } else if (List.class.isAssignableFrom(clazz)) {
                objectSerializer = ListSerializer.instance;
                put(clazz, objectSerializer);
            } else if (Collection.class.isAssignableFrom(clazz)) {
                objectSerializer = CollectionCodec.instance;
                put(clazz, objectSerializer);
            } else if (Date.class.isAssignableFrom(clazz)) {
                objectSerializer = DateCodec.instance;
                put(clazz, objectSerializer);
            } else if (JSONAware.class.isAssignableFrom(clazz)) {
                objectSerializer = JSONAwareSerializer.instance;
                put(clazz, objectSerializer);
            } else if (JSONSerializable.class.isAssignableFrom(clazz)) {
                objectSerializer = JSONSerializableSerializer.instance;
                put(clazz, objectSerializer);
            } else if (JSONStreamAware.class.isAssignableFrom(clazz)) {
                objectSerializer = MiscCodec.instance;
                put(clazz, objectSerializer);
            } else if (clazz.isEnum() || (clazz.getSuperclass() != null && clazz.getSuperclass().isEnum())) {
                JSONType jsonType = (JSONType) TypeUtils.getAnnotation(clazz, JSONType.class);
                if (jsonType == null || !jsonType.serializeEnumAsJavaBean()) {
                    objectSerializer = EnumSerializer.instance;
                    put(clazz, objectSerializer);
                } else {
                    objectSerializer = createJavaBeanSerializer((Class) clazz);
                    put(clazz, objectSerializer);
                }
            } else if (clazz.isArray()) {
                Class<?> componentType = clazz.getComponentType();
                r0 = new ArraySerializer(componentType, getObjectWriter(componentType));
                put(clazz, r0);
            } else if (Throwable.class.isAssignableFrom(clazz)) {
                SerializeBeanInfo beanInfo = TypeUtils.buildBeanInfo(clazz, null, this.propertyNamingStrategy);
                beanInfo.features |= SerializerFeature.WriteClassName.mask;
                r0 = new JavaBeanSerializer(beanInfo);
                put(clazz, r0);
            } else if (TimeZone.class.isAssignableFrom(clazz) || Entry.class.isAssignableFrom(clazz)) {
                objectSerializer = MiscCodec.instance;
                put(clazz, objectSerializer);
            } else if (Appendable.class.isAssignableFrom(clazz)) {
                objectSerializer = AppendableSerializer.instance;
                put(clazz, objectSerializer);
            } else if (Charset.class.isAssignableFrom(clazz)) {
                objectSerializer = ToStringSerializer.instance;
                put(clazz, objectSerializer);
            } else if (Enumeration.class.isAssignableFrom(clazz)) {
                objectSerializer = EnumerationSerializer.instance;
                put(clazz, objectSerializer);
            } else if (Calendar.class.isAssignableFrom(clazz) || XMLGregorianCalendar.class.isAssignableFrom(clazz)) {
                objectSerializer = CalendarCodec.instance;
                put(clazz, objectSerializer);
            } else if (Clob.class.isAssignableFrom(clazz)) {
                objectSerializer = ClobSeriliazer.instance;
                put(clazz, objectSerializer);
            } else if (TypeUtils.isPath(clazz)) {
                objectSerializer = ToStringSerializer.instance;
                put(clazz, objectSerializer);
            } else if (Iterator.class.isAssignableFrom(clazz)) {
                objectSerializer = MiscCodec.instance;
                put(clazz, objectSerializer);
            } else {
                Class cls;
                if (className.startsWith("java.awt.") && AwtCodec.support(clazz) && !awtError) {
                    try {
                        for (String name : new String[]{"java.awt.Color", "java.awt.Font", "java.awt.Point", "java.awt.Rectangle"}) {
                            if (name.equals(className)) {
                                cls = Class.forName(name);
                                objectSerializer = AwtCodec.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                    } catch (Throwable th) {
                        awtError = true;
                    }
                }
                if (!jdk8Error && (className.startsWith("java.time.") || className.startsWith("java.util.Optional") || className.equals("java.util.concurrent.atomic.LongAdder") || className.equals("java.util.concurrent.atomic.DoubleAdder"))) {
                    try {
                        for (String name2 : new String[]{"java.time.LocalDateTime", "java.time.LocalDate", "java.time.LocalTime", "java.time.ZonedDateTime", "java.time.OffsetDateTime", "java.time.OffsetTime", "java.time.ZoneOffset", "java.time.ZoneRegion", "java.time.Period", "java.time.Duration", "java.time.Instant"}) {
                            if (name2.equals(className)) {
                                cls = Class.forName(name2);
                                objectSerializer = Jdk8DateCodec.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                        for (String name22 : new String[]{"java.util.Optional", "java.util.OptionalDouble", "java.util.OptionalInt", "java.util.OptionalLong"}) {
                            if (name22.equals(className)) {
                                cls = Class.forName(name22);
                                objectSerializer = OptionalCodec.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                        for (String name222 : new String[]{"java.util.concurrent.atomic.LongAdder", "java.util.concurrent.atomic.DoubleAdder"}) {
                            if (name222.equals(className)) {
                                cls = Class.forName(name222);
                                objectSerializer = AdderSerializer.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                    } catch (Throwable th2) {
                        jdk8Error = true;
                    }
                }
                if (!oracleJdbcError && className.startsWith("oracle.sql.")) {
                    try {
                        for (String name2222 : new String[]{"oracle.sql.DATE", "oracle.sql.TIMESTAMP"}) {
                            if (name2222.equals(className)) {
                                cls = Class.forName(name2222);
                                objectSerializer = DateCodec.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                    } catch (Throwable th3) {
                        oracleJdbcError = true;
                    }
                }
                if (!springfoxError && className.equals("springfox.documentation.spring.web.json.Json")) {
                    try {
                        cls = Class.forName("springfox.documentation.spring.web.json.Json");
                        objectSerializer = SwaggerJsonSerializer.instance;
                        put(cls, objectSerializer);
                        return objectSerializer;
                    } catch (ClassNotFoundException e3) {
                        springfoxError = true;
                    }
                }
                if (!guavaError && className.startsWith("com.google.common.collect.")) {
                    try {
                        for (String name22222 : new String[]{"com.google.common.collect.HashMultimap", "com.google.common.collect.LinkedListMultimap", "com.google.common.collect.ArrayListMultimap", "com.google.common.collect.TreeMultimap"}) {
                            if (name22222.equals(className)) {
                                cls = Class.forName(name22222);
                                objectSerializer = GuavaCodec.instance;
                                put(cls, objectSerializer);
                                return objectSerializer;
                            }
                        }
                    } catch (ClassNotFoundException e4) {
                        guavaError = true;
                    }
                }
                if (!jsonnullError && className.equals("net.sf.json.JSONNull")) {
                    try {
                        cls = Class.forName("net.sf.json.JSONNull");
                        objectSerializer = MiscCodec.instance;
                        put(cls, objectSerializer);
                        return objectSerializer;
                    } catch (ClassNotFoundException e5) {
                        jsonnullError = true;
                    }
                }
                Class[] interfaces = clazz.getInterfaces();
                if (interfaces.length == 1 && interfaces[0].isAnnotation()) {
                    return AnnotationSerializer.instance;
                }
                ObjectSerializer superWriter;
                if (TypeUtils.isProxy(clazz)) {
                    superWriter = getObjectWriter(clazz.getSuperclass());
                    put(clazz, superWriter);
                    return superWriter;
                }
                if (Proxy.isProxyClass(clazz)) {
                    Class handlerClass = null;
                    if (interfaces.length == 2) {
                        handlerClass = interfaces[1];
                    } else {
                        for (Class proxiedInterface : interfaces) {
                            if (!proxiedInterface.getName().startsWith("org.springframework.aop.")) {
                                if (handlerClass != null) {
                                    handlerClass = null;
                                    break;
                                }
                                handlerClass = proxiedInterface;
                            }
                        }
                    }
                    if (handlerClass != null) {
                        superWriter = getObjectWriter(handlerClass);
                        put(clazz, superWriter);
                        return superWriter;
                    }
                }
                if (create) {
                    objectSerializer = createJavaBeanSerializer((Class) clazz);
                    put(clazz, objectSerializer);
                }
            }
            if (objectSerializer == null) {
                objectSerializer = (ObjectSerializer) this.serializers.get(clazz);
            }
        }
        return objectSerializer;
    }

    public boolean put(Type type, ObjectSerializer value) {
        return this.serializers.put(type, value);
    }
}
