package com.alibaba.fastjson.serializer;

import java.util.Arrays;

public class Labels {

    private static class DefaultLabelFilter implements LabelFilter {
        private String[] excludes;
        private String[] includes;

        public boolean apply(String label) {
            if (this.excludes != null) {
                if (Arrays.binarySearch(this.excludes, label) == -1) {
                    return true;
                }
                return false;
            } else if (this.includes == null || Arrays.binarySearch(this.includes, label) < 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
