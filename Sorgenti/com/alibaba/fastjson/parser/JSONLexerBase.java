package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.IOUtils;
import java.io.Closeable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public abstract class JSONLexerBase implements JSONLexer, Closeable {
    private static final ThreadLocal<char[]> SBUF_LOCAL = new ThreadLocal();
    protected static final int[] digits = new int[103];
    protected static final char[] typeFieldName = ("\"" + JSON.DEFAULT_TYPE_KEY + "\":\"").toCharArray();
    protected int bp;
    protected Calendar calendar = null;
    protected char ch;
    protected int eofPos;
    protected int features;
    protected boolean hasSpecial;
    protected Locale locale = JSON.defaultLocale;
    public int matchStat = 0;
    protected int np;
    protected int pos;
    protected char[] sbuf;
    protected int sp;
    protected String stringDefaultValue = null;
    protected TimeZone timeZone = JSON.defaultTimeZone;
    protected int token;

    public abstract String addSymbol(int i, int i2, int i3, SymbolTable symbolTable);

    protected abstract void arrayCopy(int i, char[] cArr, int i2, int i3);

    protected abstract boolean charArrayCompare(char[] cArr);

    public abstract char charAt(int i);

    protected abstract void copyTo(int i, int i2, char[] cArr);

    public abstract BigDecimal decimalValue();

    public abstract int indexOf(char c, int i);

    public abstract boolean isEOF();

    public abstract char next();

    public abstract String numberString();

    public abstract String stringVal();

    public abstract String subString(int i, int i2);

    protected abstract char[] sub_chars(int i, int i2);

    protected void lexError(String key, Object... args) {
        this.token = 1;
    }

    static {
        int i;
        for (i = 48; i <= 57; i++) {
            digits[i] = i - 48;
        }
        for (i = 97; i <= 102; i++) {
            digits[i] = (i - 97) + 10;
        }
        for (i = 65; i <= 70; i++) {
            digits[i] = (i - 65) + 10;
        }
    }

    public JSONLexerBase(int features) {
        this.features = features;
        if ((Feature.InitStringFieldAsEmpty.mask & features) != 0) {
            this.stringDefaultValue = "";
        }
        this.sbuf = (char[]) SBUF_LOCAL.get();
        if (this.sbuf == null) {
            this.sbuf = new char[512];
        }
    }

    public final void nextToken() {
        this.sp = 0;
        while (true) {
            this.pos = this.bp;
            if (this.ch == '/') {
                skipComment();
            } else if (this.ch == '\"') {
                scanString();
                return;
            } else if (this.ch == ',') {
                next();
                this.token = 16;
                return;
            } else if (this.ch >= '0' && this.ch <= '9') {
                scanNumber();
                return;
            } else if (this.ch == '-') {
                scanNumber();
                return;
            } else {
                switch (this.ch) {
                    case '\b':
                    case '\t':
                    case '\n':
                    case '\f':
                    case '\r':
                    case ' ':
                        next();
                        break;
                    case '\'':
                        if (isEnabled(Feature.AllowSingleQuotes)) {
                            scanStringSingleQuote();
                            return;
                        }
                        throw new JSONException("Feature.AllowSingleQuotes is false");
                    case '(':
                        next();
                        this.token = 10;
                        return;
                    case ')':
                        next();
                        this.token = 11;
                        return;
                    case '+':
                        next();
                        scanNumber();
                        return;
                    case '.':
                        next();
                        this.token = 25;
                        return;
                    case ':':
                        next();
                        this.token = 17;
                        return;
                    case ';':
                        next();
                        this.token = 24;
                        return;
                    case 'N':
                    case 'S':
                    case 'T':
                    case 'u':
                        scanIdent();
                        return;
                    case '[':
                        next();
                        this.token = 14;
                        return;
                    case ']':
                        next();
                        this.token = 15;
                        return;
                    case 'f':
                        scanFalse();
                        return;
                    case 'n':
                        scanNullOrNew();
                        return;
                    case 't':
                        scanTrue();
                        return;
                    case 'x':
                        scanHex();
                        return;
                    case '{':
                        next();
                        this.token = 12;
                        return;
                    case '}':
                        next();
                        this.token = 13;
                        return;
                    default:
                        if (isEOF()) {
                            if (this.token == 20) {
                                throw new JSONException("EOF error");
                            }
                            this.token = 20;
                            int i = this.eofPos;
                            this.bp = i;
                            this.pos = i;
                            return;
                        } else if (this.ch <= '\u001f' || this.ch == '') {
                            next();
                            break;
                        } else {
                            lexError("illegal.char", String.valueOf(this.ch));
                            next();
                            return;
                        }
                }
            }
        }
    }

    public final void nextToken(int expect) {
        this.sp = 0;
        while (true) {
            switch (expect) {
                case 2:
                    if (this.ch >= '0' && this.ch <= '9') {
                        this.pos = this.bp;
                        scanNumber();
                        return;
                    } else if (this.ch == '\"') {
                        this.pos = this.bp;
                        scanString();
                        return;
                    } else if (this.ch == '[') {
                        this.token = 14;
                        next();
                        return;
                    } else if (this.ch == '{') {
                        this.token = 12;
                        next();
                        return;
                    }
                    break;
                case 4:
                    if (this.ch == '\"') {
                        this.pos = this.bp;
                        scanString();
                        return;
                    } else if (this.ch >= '0' && this.ch <= '9') {
                        this.pos = this.bp;
                        scanNumber();
                        return;
                    } else if (this.ch == '[') {
                        this.token = 14;
                        next();
                        return;
                    } else if (this.ch == '{') {
                        this.token = 12;
                        next();
                        return;
                    }
                    break;
                case 12:
                    if (this.ch == '{') {
                        this.token = 12;
                        next();
                        return;
                    } else if (this.ch == '[') {
                        this.token = 14;
                        next();
                        return;
                    }
                    break;
                case 14:
                    if (this.ch == '[') {
                        this.token = 14;
                        next();
                        return;
                    } else if (this.ch == '{') {
                        this.token = 12;
                        next();
                        return;
                    }
                    break;
                case 15:
                    if (this.ch == ']') {
                        this.token = 15;
                        next();
                        return;
                    }
                    break;
                case 16:
                    if (this.ch == ',') {
                        this.token = 16;
                        next();
                        return;
                    } else if (this.ch == '}') {
                        this.token = 13;
                        next();
                        return;
                    } else if (this.ch == ']') {
                        this.token = 15;
                        next();
                        return;
                    } else if (this.ch == '\u001a') {
                        this.token = 20;
                        return;
                    }
                    break;
                case 18:
                    nextIdent();
                    return;
                case 20:
                    break;
            }
            if (this.ch == '\u001a') {
                this.token = 20;
                return;
            }
            if (this.ch == ' ' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\f' || this.ch == '\b') {
                next();
            } else {
                nextToken();
                return;
            }
        }
    }

    public final void nextIdent() {
        while (isWhitespace(this.ch)) {
            next();
        }
        if (this.ch == '_' || Character.isLetter(this.ch)) {
            scanIdent();
        } else {
            nextToken();
        }
    }

    public final void nextTokenWithColon() {
        nextTokenWithChar(':');
    }

    public final void nextTokenWithChar(char expect) {
        this.sp = 0;
        while (this.ch != expect) {
            if (this.ch == ' ' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\f' || this.ch == '\b') {
                next();
            } else {
                throw new JSONException("not match " + expect + " - " + this.ch + ", info : " + info());
            }
        }
        next();
        nextToken();
    }

    public final int token() {
        return this.token;
    }

    public final String tokenName() {
        return JSONToken.name(this.token);
    }

    public final int pos() {
        return this.pos;
    }

    public final String stringDefaultValue() {
        return this.stringDefaultValue;
    }

    public final Number integerValue() throws NumberFormatException {
        long limit;
        int i;
        long result = 0;
        boolean negative = false;
        if (this.np == -1) {
            this.np = 0;
        }
        int i2 = this.np;
        int max = this.np + this.sp;
        char type = ' ';
        switch (charAt(max - 1)) {
            case 'B':
                max--;
                type = 'B';
                break;
            case 'L':
                max--;
                type = 'L';
                break;
            case 'S':
                max--;
                type = 'S';
                break;
        }
        if (charAt(this.np) == '-') {
            negative = true;
            limit = Long.MIN_VALUE;
            i = i2 + 1;
        } else {
            limit = -9223372036854775807L;
            i = i2;
        }
        if (i < max) {
            result = (long) (-(charAt(i) - 48));
            i++;
        }
        while (i < max) {
            i2 = i + 1;
            int digit = charAt(i) - 48;
            if (result < -922337203685477580L) {
                return new BigInteger(numberString());
            }
            result *= 10;
            if (result < ((long) digit) + limit) {
                return new BigInteger(numberString());
            }
            result -= (long) digit;
            i = i2;
        }
        if (!negative) {
            result = -result;
            if (result > 2147483647L || type == 'L') {
                i2 = i;
                return Long.valueOf(result);
            } else if (type == 'S') {
                i2 = i;
                return Short.valueOf((short) ((int) result));
            } else if (type == 'B') {
                i2 = i;
                return Byte.valueOf((byte) ((int) result));
            } else {
                i2 = i;
                return Integer.valueOf((int) result);
            }
        } else if (i <= this.np + 1) {
            throw new NumberFormatException(numberString());
        } else if (result < -2147483648L || type == 'L') {
            i2 = i;
            return Long.valueOf(result);
        } else if (type == 'S') {
            i2 = i;
            return Short.valueOf((short) ((int) result));
        } else if (type == 'B') {
            i2 = i;
            return Byte.valueOf((byte) ((int) result));
        } else {
            i2 = i;
            return Integer.valueOf((int) result);
        }
    }

    public final void nextTokenWithColon(int expect) {
        nextTokenWithChar(':');
    }

    public float floatValue() {
        String strVal = numberString();
        float floatValue = Float.parseFloat(strVal);
        if (floatValue == 0.0f || floatValue == Float.POSITIVE_INFINITY) {
            char c0 = strVal.charAt(0);
            if (c0 > '0' && c0 <= '9') {
                throw new JSONException("float overflow : " + strVal);
            }
        }
        return floatValue;
    }

    public double doubleValue() {
        return Double.parseDouble(numberString());
    }

    public final boolean isEnabled(Feature feature) {
        return isEnabled(feature.mask);
    }

    public final boolean isEnabled(int feature) {
        return (this.features & feature) != 0;
    }

    public final char getCurrent() {
        return this.ch;
    }

    protected void skipComment() {
        next();
        if (this.ch == '/') {
            do {
                next();
                if (this.ch == '\n') {
                    next();
                    return;
                }
            } while (this.ch != '\u001a');
        } else if (this.ch == '*') {
            next();
            while (this.ch != '\u001a') {
                if (this.ch == '*') {
                    next();
                    if (this.ch == '/') {
                        next();
                        return;
                    }
                } else {
                    next();
                }
            }
        } else {
            throw new JSONException("invalid comment");
        }
    }

    public final String scanSymbol(SymbolTable symbolTable) {
        skipWhitespace();
        if (this.ch == '\"') {
            return scanSymbol(symbolTable, '\"');
        }
        if (this.ch == '\'') {
            if (isEnabled(Feature.AllowSingleQuotes)) {
                return scanSymbol(symbolTable, '\'');
            }
            throw new JSONException("syntax error");
        } else if (this.ch == '}') {
            next();
            this.token = 13;
            return null;
        } else if (this.ch == ',') {
            next();
            this.token = 16;
            return null;
        } else if (this.ch == '\u001a') {
            this.token = 20;
            return null;
        } else if (isEnabled(Feature.AllowUnQuotedFieldNames)) {
            return scanSymbolUnQuoted(symbolTable);
        } else {
            throw new JSONException("syntax error");
        }
    }

    public final String scanSymbol(SymbolTable symbolTable, char quote) {
        int hash = 0;
        this.np = this.bp;
        this.sp = 0;
        boolean hasSpecial = false;
        while (true) {
            char chLocal = next();
            if (chLocal == quote) {
                String value;
                this.token = 4;
                if (hasSpecial) {
                    value = symbolTable.addSymbol(this.sbuf, 0, this.sp, hash);
                } else {
                    int offset;
                    if (this.np == -1) {
                        offset = 0;
                    } else {
                        offset = this.np + 1;
                    }
                    value = addSymbol(offset, this.sp, hash, symbolTable);
                }
                this.sp = 0;
                next();
                return value;
            } else if (chLocal == '\u001a') {
                throw new JSONException("unclosed.str");
            } else if (chLocal == '\\') {
                if (!hasSpecial) {
                    hasSpecial = true;
                    if (this.sp >= this.sbuf.length) {
                        int newCapcity = this.sbuf.length * 2;
                        if (this.sp > newCapcity) {
                            newCapcity = this.sp;
                        }
                        char[] newsbuf = new char[newCapcity];
                        System.arraycopy(this.sbuf, 0, newsbuf, 0, this.sbuf.length);
                        this.sbuf = newsbuf;
                    }
                    arrayCopy(this.np + 1, this.sbuf, 0, this.sp);
                }
                chLocal = next();
                switch (chLocal) {
                    case '\"':
                        hash = (hash * 31) + 34;
                        putChar('\"');
                        break;
                    case '\'':
                        hash = (hash * 31) + 39;
                        putChar('\'');
                        break;
                    case '/':
                        hash = (hash * 31) + 47;
                        putChar('/');
                        break;
                    case '0':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0000');
                        break;
                    case '1':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0001');
                        break;
                    case '2':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0002');
                        break;
                    case '3':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0003');
                        break;
                    case '4':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0004');
                        break;
                    case '5':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0005');
                        break;
                    case '6':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0006');
                        break;
                    case '7':
                        hash = (hash * 31) + chLocal;
                        putChar('\u0007');
                        break;
                    case 'F':
                    case 'f':
                        hash = (hash * 31) + 12;
                        putChar('\f');
                        break;
                    case '\\':
                        hash = (hash * 31) + 92;
                        putChar('\\');
                        break;
                    case 'b':
                        hash = (hash * 31) + 8;
                        putChar('\b');
                        break;
                    case 'n':
                        hash = (hash * 31) + 10;
                        putChar('\n');
                        break;
                    case 'r':
                        hash = (hash * 31) + 13;
                        putChar('\r');
                        break;
                    case 't':
                        hash = (hash * 31) + 9;
                        putChar('\t');
                        break;
                    case 'u':
                        char c1 = next();
                        char c2 = next();
                        char c3 = next();
                        char c4 = next();
                        int val = Integer.parseInt(new String(new char[]{c1, c2, c3, c4}), 16);
                        hash = (hash * 31) + val;
                        putChar((char) val);
                        break;
                    case 'v':
                        hash = (hash * 31) + 11;
                        putChar('\u000b');
                        break;
                    case 'x':
                        char x1 = next();
                        this.ch = x1;
                        char x2 = next();
                        this.ch = x2;
                        char x_char = (char) ((digits[x1] * 16) + digits[x2]);
                        hash = (hash * 31) + x_char;
                        putChar(x_char);
                        break;
                    default:
                        this.ch = chLocal;
                        throw new JSONException("unclosed.str.lit");
                }
            } else {
                hash = (hash * 31) + chLocal;
                if (hasSpecial) {
                    if (this.sp == this.sbuf.length) {
                        putChar(chLocal);
                    } else {
                        char[] cArr = this.sbuf;
                        int i = this.sp;
                        this.sp = i + 1;
                        cArr[i] = chLocal;
                    }
                } else {
                    this.sp++;
                }
            }
        }
    }

    public final void resetStringPosition() {
        this.sp = 0;
    }

    public String info() {
        return "";
    }

    public final String scanSymbolUnQuoted(SymbolTable symbolTable) {
        boolean firstFlag = false;
        if (this.token == 1 && this.pos == 0 && this.bp == 1) {
            this.bp = 0;
        }
        boolean[] firstIdentifierFlags = IOUtils.firstIdentifierFlags;
        char first = this.ch;
        if (this.ch >= firstIdentifierFlags.length || firstIdentifierFlags[first]) {
            firstFlag = true;
        }
        if (firstFlag) {
            boolean[] identifierFlags = IOUtils.identifierFlags;
            int hash = first;
            this.np = this.bp;
            this.sp = 1;
            while (true) {
                char chLocal = next();
                if (chLocal < identifierFlags.length && !identifierFlags[chLocal]) {
                    break;
                }
                hash = (hash * 31) + chLocal;
                this.sp++;
            }
            this.ch = charAt(this.bp);
            this.token = 18;
            if (this.sp == 4 && hash == 3392903 && charAt(this.np) == 'n' && charAt(this.np + 1) == 'u' && charAt(this.np + 2) == 'l' && charAt(this.np + 3) == 'l') {
                return null;
            }
            if (symbolTable == null) {
                return subString(this.np, this.sp);
            }
            return addSymbol(this.np, this.sp, hash, symbolTable);
        }
        throw new JSONException("illegal identifier : " + this.ch + info());
    }

    public final void scanString() {
        this.np = this.bp;
        this.hasSpecial = false;
        while (true) {
            char ch = next();
            if (ch == '\"') {
                this.token = 4;
                this.ch = next();
                return;
            } else if (ch == '\u001a') {
                if (isEOF()) {
                    throw new JSONException("unclosed string : " + ch);
                }
                putChar('\u001a');
            } else if (ch == '\\') {
                if (!this.hasSpecial) {
                    this.hasSpecial = true;
                    if (this.sp >= this.sbuf.length) {
                        int newCapcity = this.sbuf.length * 2;
                        if (this.sp > newCapcity) {
                            newCapcity = this.sp;
                        }
                        char[] newsbuf = new char[newCapcity];
                        System.arraycopy(this.sbuf, 0, newsbuf, 0, this.sbuf.length);
                        this.sbuf = newsbuf;
                    }
                    copyTo(this.np + 1, this.sp, this.sbuf);
                }
                ch = next();
                switch (ch) {
                    case '\"':
                        putChar('\"');
                        break;
                    case '\'':
                        putChar('\'');
                        break;
                    case '/':
                        putChar('/');
                        break;
                    case '0':
                        putChar('\u0000');
                        break;
                    case '1':
                        putChar('\u0001');
                        break;
                    case '2':
                        putChar('\u0002');
                        break;
                    case '3':
                        putChar('\u0003');
                        break;
                    case '4':
                        putChar('\u0004');
                        break;
                    case '5':
                        putChar('\u0005');
                        break;
                    case '6':
                        putChar('\u0006');
                        break;
                    case '7':
                        putChar('\u0007');
                        break;
                    case 'F':
                    case 'f':
                        putChar('\f');
                        break;
                    case '\\':
                        putChar('\\');
                        break;
                    case 'b':
                        putChar('\b');
                        break;
                    case 'n':
                        putChar('\n');
                        break;
                    case 'r':
                        putChar('\r');
                        break;
                    case 't':
                        putChar('\t');
                        break;
                    case 'u':
                        char u1 = next();
                        char u2 = next();
                        char u3 = next();
                        char u4 = next();
                        putChar((char) Integer.parseInt(new String(new char[]{u1, u2, u3, u4}), 16));
                        break;
                    case 'v':
                        putChar('\u000b');
                        break;
                    case 'x':
                        putChar((char) ((digits[next()] * 16) + digits[next()]));
                        break;
                    default:
                        this.ch = ch;
                        throw new JSONException("unclosed string : " + ch);
                }
            } else if (!this.hasSpecial) {
                this.sp++;
            } else if (this.sp == this.sbuf.length) {
                putChar(ch);
            } else {
                char[] cArr = this.sbuf;
                int i = this.sp;
                this.sp = i + 1;
                cArr[i] = ch;
            }
        }
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public final int intValue() {
        int limit;
        int i;
        if (this.np == -1) {
            this.np = 0;
        }
        int result = 0;
        boolean negative = false;
        int i2 = this.np;
        int max = this.np + this.sp;
        if (charAt(this.np) == '-') {
            negative = true;
            limit = Integer.MIN_VALUE;
            i = i2 + 1;
        } else {
            limit = -2147483647;
            i = i2;
        }
        if (i < max) {
            result = -(charAt(i) - 48);
            i++;
        }
        while (i < max) {
            i2 = i + 1;
            char chLocal = charAt(i);
            if (chLocal == 'L' || chLocal == 'S' || chLocal == 'B') {
                break;
            }
            int digit = chLocal - 48;
            if (((long) result) < -214748364) {
                throw new NumberFormatException(numberString());
            }
            result *= 10;
            if (result < limit + digit) {
                throw new NumberFormatException(numberString());
            }
            result -= digit;
            i = i2;
        }
        i2 = i;
        if (!negative) {
            return -result;
        }
        if (i2 > this.np + 1) {
            return result;
        }
        throw new NumberFormatException(numberString());
    }

    public void close() {
        if (this.sbuf.length <= 8192) {
            SBUF_LOCAL.set(this.sbuf);
        }
        this.sbuf = null;
    }

    public final boolean isRef() {
        if (this.sp == 4 && charAt(this.np + 1) == '$' && charAt(this.np + 2) == 'r' && charAt(this.np + 3) == 'e' && charAt(this.np + 4) == 'f') {
            return true;
        }
        return false;
    }

    public final boolean matchField(char[] fieldName) {
        while (!charArrayCompare(fieldName)) {
            if (!isWhitespace(this.ch)) {
                return false;
            }
            next();
        }
        this.bp += fieldName.length;
        this.ch = charAt(this.bp);
        if (this.ch == '{') {
            next();
            this.token = 12;
        } else if (this.ch == '[') {
            next();
            this.token = 14;
        } else if (this.ch == 'S' && charAt(this.bp + 1) == 'e' && charAt(this.bp + 2) == 't' && charAt(this.bp + 3) == '[') {
            this.bp += 3;
            this.ch = charAt(this.bp);
            this.token = 21;
        } else {
            nextToken();
        }
        return true;
    }

    public String scanFieldString(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            if (charAt(this.bp + offset) != '\"') {
                this.matchStat = -1;
                return stringDefaultValue();
            }
            int endIndex = indexOf('\"', (this.bp + fieldName.length) + 1);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }
            int startIndex2 = (this.bp + fieldName.length) + 1;
            String stringVal = subString(startIndex2, endIndex - startIndex2);
            if (stringVal.indexOf(92) != -1) {
                while (true) {
                    int slashCount = 0;
                    int i = endIndex - 1;
                    while (i >= 0 && charAt(i) == '\\') {
                        slashCount++;
                        i--;
                    }
                    if (slashCount % 2 == 0) {
                        break;
                    }
                    endIndex = indexOf('\"', endIndex + 1);
                }
                int chars_len = endIndex - ((this.bp + fieldName.length) + 1);
                stringVal = readString(sub_chars((this.bp + fieldName.length) + 1, chars_len), chars_len);
            }
            offset = offset2 + ((endIndex - ((this.bp + fieldName.length) + 1)) + 1);
            offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            String strVal = stringVal;
            if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                return strVal;
            } else if (chLocal == '}') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return stringDefaultValue();
                }
                this.matchStat = 4;
                return strVal;
            } else {
                this.matchStat = -1;
                return stringDefaultValue();
            }
        }
        this.matchStat = -2;
        return stringDefaultValue();
    }

    public String scanString(char expectNextChar) {
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        int offset2;
        if (chLocal != 'n') {
            while (chLocal != '\"') {
                if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return stringDefaultValue();
                }
            }
            int startIndex = this.bp + offset;
            int endIndex = indexOf('\"', startIndex);
            if (endIndex == -1) {
                throw new JSONException("unclosed str");
            }
            String stringVal = subString(this.bp + offset, endIndex - startIndex);
            if (stringVal.indexOf(92) != -1) {
                while (true) {
                    int slashCount = 0;
                    int i = endIndex - 1;
                    while (i >= 0 && charAt(i) == '\\') {
                        slashCount++;
                        i--;
                    }
                    if (slashCount % 2 == 0) {
                        break;
                    }
                    endIndex = indexOf('\"', endIndex + 1);
                }
                int chars_len = endIndex - startIndex;
                stringVal = readString(sub_chars(this.bp + 1, chars_len), chars_len);
            }
            offset2 = offset + ((endIndex - startIndex) + 1);
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            String strVal = stringVal;
            while (chLocal != expectNextChar) {
                if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return strVal;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 3;
            offset2 = offset;
            return strVal;
        } else if (charAt(this.bp + 1) == 'u' && charAt((this.bp + 1) + 1) == 'l' && charAt((this.bp + 1) + 2) == 'l') {
            offset2 = (offset + 3) + 1;
            if (charAt(this.bp + 4) == expectNextChar) {
                this.bp += 5;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                return null;
            }
            this.matchStat = -1;
            return null;
        } else {
            this.matchStat = -1;
            offset2 = offset;
            return null;
        }
    }

    public long scanFieldSymbol(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            if (charAt(this.bp + offset) != '\"') {
                this.matchStat = -1;
                return 0;
            }
            char chLocal;
            long hash = -3750763034362895579L;
            offset = offset2;
            while (true) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal == '\"') {
                    break;
                }
                hash = (hash ^ ((long) chLocal)) * 1099511628211L;
                if (chLocal == '\\') {
                    this.matchStat = -1;
                    return 0;
                }
                offset = offset2;
            }
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (chLocal == ',') {
                this.bp += offset;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                return hash;
            } else if (chLocal == '}') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset2 - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return 0;
                }
                this.matchStat = 4;
                return hash;
            } else {
                this.matchStat = -1;
                return 0;
            }
        }
        this.matchStat = -2;
        return 0;
    }

    public Enum<?> scanEnum(Class<?> enumClass, SymbolTable symbolTable, char serperator) {
        String name = scanSymbolWithSeperator(symbolTable, serperator);
        if (name == null) {
            return null;
        }
        return Enum.valueOf(enumClass, name);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String scanSymbolWithSeperator(com.alibaba.fastjson.parser.SymbolTable r13, char r14) {
        /*
        r12 = this;
        r11 = 108; // 0x6c float:1.51E-43 double:5.34E-322;
        r8 = 34;
        r10 = 3;
        r6 = 0;
        r9 = -1;
        r7 = 0;
        r12.matchStat = r7;
        r3 = 0;
        r7 = r12.bp;
        r4 = r3 + 1;
        r7 = r7 + r3;
        r0 = r12.charAt(r7);
        r7 = 110; // 0x6e float:1.54E-43 double:5.43E-322;
        if (r0 != r7) goto L_0x0062;
    L_0x0018:
        r7 = r12.bp;
        r7 = r7 + 1;
        r7 = r12.charAt(r7);
        r8 = 117; // 0x75 float:1.64E-43 double:5.8E-322;
        if (r7 != r8) goto L_0x005b;
    L_0x0024:
        r7 = r12.bp;
        r7 = r7 + 1;
        r7 = r7 + 1;
        r7 = r12.charAt(r7);
        if (r7 != r11) goto L_0x005b;
    L_0x0030:
        r7 = r12.bp;
        r7 = r7 + 1;
        r7 = r7 + 2;
        r7 = r12.charAt(r7);
        if (r7 != r11) goto L_0x005b;
    L_0x003c:
        r3 = r4 + 3;
        r7 = r12.bp;
        r3 = r3 + 1;
        r7 = r7 + 4;
        r0 = r12.charAt(r7);
        if (r0 != r14) goto L_0x005f;
    L_0x004a:
        r7 = r12.bp;
        r7 = r7 + 5;
        r12.bp = r7;
        r7 = r12.bp;
        r7 = r12.charAt(r7);
        r12.ch = r7;
        r12.matchStat = r10;
    L_0x005a:
        return r6;
    L_0x005b:
        r12.matchStat = r9;
        r3 = r4;
        goto L_0x005a;
    L_0x005f:
        r12.matchStat = r9;
        goto L_0x005a;
    L_0x0062:
        if (r0 == r8) goto L_0x0068;
    L_0x0064:
        r12.matchStat = r9;
        r3 = r4;
        goto L_0x005a;
    L_0x0068:
        r1 = 0;
        r3 = r4;
    L_0x006a:
        r7 = r12.bp;
        r4 = r3 + 1;
        r7 = r7 + r3;
        r0 = r12.charAt(r7);
        if (r0 != r8) goto L_0x00a2;
    L_0x0075:
        r7 = r12.bp;
        r7 = r7 + 0;
        r5 = r7 + 1;
        r7 = r12.bp;
        r7 = r7 + r4;
        r7 = r7 - r5;
        r2 = r7 + -1;
        r6 = r12.addSymbol(r5, r2, r1, r13);
        r7 = r12.bp;
        r3 = r4 + 1;
        r7 = r7 + r4;
        r0 = r12.charAt(r7);
        r4 = r3;
    L_0x008f:
        if (r0 != r14) goto L_0x00ae;
    L_0x0091:
        r7 = r12.bp;
        r7 = r7 + r4;
        r12.bp = r7;
        r7 = r12.bp;
        r7 = r12.charAt(r7);
        r12.ch = r7;
        r12.matchStat = r10;
        r3 = r4;
        goto L_0x005a;
    L_0x00a2:
        r7 = r1 * 31;
        r1 = r7 + r0;
        r7 = 92;
        if (r0 != r7) goto L_0x00c3;
    L_0x00aa:
        r12.matchStat = r9;
        r3 = r4;
        goto L_0x005a;
    L_0x00ae:
        r7 = isWhitespace(r0);
        if (r7 == 0) goto L_0x00bf;
    L_0x00b4:
        r7 = r12.bp;
        r3 = r4 + 1;
        r7 = r7 + r4;
        r0 = r12.charAt(r7);
        r4 = r3;
        goto L_0x008f;
    L_0x00bf:
        r12.matchStat = r9;
        r3 = r4;
        goto L_0x005a;
    L_0x00c3:
        r3 = r4;
        goto L_0x006a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.JSONLexerBase.scanSymbolWithSeperator(com.alibaba.fastjson.parser.SymbolTable, char):java.lang.String");
    }

    public int scanFieldInt(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            boolean negative;
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            if (chLocal == '-') {
                negative = true;
            } else {
                negative = false;
            }
            if (negative) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
            } else {
                offset = offset2;
            }
            if (chLocal < '0' || chLocal > '9') {
                this.matchStat = -1;
                return 0;
            }
            int value = chLocal - 48;
            while (true) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal >= '0' && chLocal <= '9') {
                    value = (value * 10) + (chLocal - 48);
                    offset = offset2;
                }
            }
            if (chLocal == '.') {
                this.matchStat = -1;
                return 0;
            } else if ((value < 0 || offset2 > fieldName.length + 14) && !(value == Integer.MIN_VALUE && offset2 == 17 && negative)) {
                this.matchStat = -1;
                return 0;
            } else if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                if (negative) {
                    return -value;
                }
                return value;
            } else if (chLocal == '}') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return 0;
                }
                this.matchStat = 4;
                if (negative) {
                    return -value;
                }
                return value;
            } else {
                this.matchStat = -1;
                return 0;
            }
        }
        this.matchStat = -2;
        return 0;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int[] scanFieldIntArray(char[] r12) {
        /*
        r11 = this;
        r9 = 0;
        r11.matchStat = r9;
        r9 = r11.charArrayCompare(r12);
        if (r9 != 0) goto L_0x000e;
    L_0x0009:
        r9 = -2;
        r11.matchStat = r9;
        r0 = 0;
    L_0x000d:
        return r0;
    L_0x000e:
        r5 = r12.length;
        r9 = r11.bp;
        r6 = r5 + 1;
        r9 = r9 + r5;
        r3 = r11.charAt(r9);
        r9 = 91;
        if (r3 == r9) goto L_0x0021;
    L_0x001c:
        r9 = -2;
        r11.matchStat = r9;
        r0 = 0;
        goto L_0x000d;
    L_0x0021:
        r9 = r11.bp;
        r5 = r6 + 1;
        r9 = r9 + r6;
        r3 = r11.charAt(r9);
        r9 = 16;
        r0 = new int[r9];
        r1 = 0;
        r9 = 93;
        if (r3 != r9) goto L_0x013b;
    L_0x0033:
        r9 = r11.bp;
        r6 = r5 + 1;
        r9 = r9 + r5;
        r3 = r11.charAt(r9);
        r5 = r6;
    L_0x003d:
        r9 = r0.length;
        if (r1 == r9) goto L_0x0048;
    L_0x0040:
        r7 = new int[r1];
        r9 = 0;
        r10 = 0;
        java.lang.System.arraycopy(r0, r9, r7, r10, r1);
        r0 = r7;
    L_0x0048:
        r9 = 44;
        if (r3 != r9) goto L_0x00cb;
    L_0x004c:
        r9 = r11.bp;
        r10 = r5 + -1;
        r9 = r9 + r10;
        r11.bp = r9;
        r11.next();
        r9 = 3;
        r11.matchStat = r9;
        r9 = 16;
        r11.token = r9;
        goto L_0x000d;
    L_0x005e:
        r9 = r0.length;
        if (r2 < r9) goto L_0x006e;
    L_0x0061:
        r9 = r0.length;
        r9 = r9 * 3;
        r9 = r9 / 2;
        r7 = new int[r9];
        r9 = 0;
        r10 = 0;
        java.lang.System.arraycopy(r0, r9, r7, r10, r2);
        r0 = r7;
    L_0x006e:
        r1 = r2 + 1;
        if (r4 == 0) goto L_0x0073;
    L_0x0072:
        r8 = -r8;
    L_0x0073:
        r0[r2] = r8;
        r9 = 44;
        if (r3 != r9) goto L_0x00b6;
    L_0x0079:
        r9 = r11.bp;
        r5 = r6 + 1;
        r9 = r9 + r6;
        r3 = r11.charAt(r9);
    L_0x0082:
        r2 = r1;
        r6 = r5;
    L_0x0084:
        r4 = 0;
        r9 = 45;
        if (r3 != r9) goto L_0x0138;
    L_0x0089:
        r9 = r11.bp;
        r5 = r6 + 1;
        r9 = r9 + r6;
        r3 = r11.charAt(r9);
        r4 = 1;
    L_0x0093:
        r9 = 48;
        if (r3 < r9) goto L_0x00c5;
    L_0x0097:
        r9 = 57;
        if (r3 > r9) goto L_0x00c5;
    L_0x009b:
        r8 = r3 + -48;
    L_0x009d:
        r9 = r11.bp;
        r6 = r5 + 1;
        r9 = r9 + r5;
        r3 = r11.charAt(r9);
        r9 = 48;
        if (r3 < r9) goto L_0x005e;
    L_0x00aa:
        r9 = 57;
        if (r3 > r9) goto L_0x005e;
    L_0x00ae:
        r9 = r8 * 10;
        r10 = r3 + -48;
        r8 = r9 + r10;
        r5 = r6;
        goto L_0x009d;
    L_0x00b6:
        r9 = 93;
        if (r3 != r9) goto L_0x0135;
    L_0x00ba:
        r9 = r11.bp;
        r5 = r6 + 1;
        r9 = r9 + r6;
        r3 = r11.charAt(r9);
        goto L_0x003d;
    L_0x00c5:
        r9 = -1;
        r11.matchStat = r9;
        r0 = 0;
        goto L_0x000d;
    L_0x00cb:
        r9 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        if (r3 != r9) goto L_0x012f;
    L_0x00cf:
        r9 = r11.bp;
        r6 = r5 + 1;
        r9 = r9 + r5;
        r3 = r11.charAt(r9);
        r9 = 44;
        if (r3 != r9) goto L_0x00ef;
    L_0x00dc:
        r9 = 16;
        r11.token = r9;
        r9 = r11.bp;
        r10 = r6 + -1;
        r9 = r9 + r10;
        r11.bp = r9;
        r11.next();
    L_0x00ea:
        r9 = 4;
        r11.matchStat = r9;
        goto L_0x000d;
    L_0x00ef:
        r9 = 93;
        if (r3 != r9) goto L_0x0102;
    L_0x00f3:
        r9 = 15;
        r11.token = r9;
        r9 = r11.bp;
        r10 = r6 + -1;
        r9 = r9 + r10;
        r11.bp = r9;
        r11.next();
        goto L_0x00ea;
    L_0x0102:
        r9 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        if (r3 != r9) goto L_0x0115;
    L_0x0106:
        r9 = 13;
        r11.token = r9;
        r9 = r11.bp;
        r10 = r6 + -1;
        r9 = r9 + r10;
        r11.bp = r9;
        r11.next();
        goto L_0x00ea;
    L_0x0115:
        r9 = 26;
        if (r3 != r9) goto L_0x0129;
    L_0x0119:
        r9 = r11.bp;
        r10 = r6 + -1;
        r9 = r9 + r10;
        r11.bp = r9;
        r9 = 20;
        r11.token = r9;
        r9 = 26;
        r11.ch = r9;
        goto L_0x00ea;
    L_0x0129:
        r9 = -1;
        r11.matchStat = r9;
        r0 = 0;
        goto L_0x000d;
    L_0x012f:
        r9 = -1;
        r11.matchStat = r9;
        r0 = 0;
        goto L_0x000d;
    L_0x0135:
        r5 = r6;
        goto L_0x0082;
    L_0x0138:
        r5 = r6;
        goto L_0x0093;
    L_0x013b:
        r2 = r1;
        r6 = r5;
        goto L_0x0084;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.JSONLexerBase.scanFieldIntArray(char[]):int[]");
    }

    public boolean scanBoolean(char expectNext) {
        int offset;
        this.matchStat = 0;
        int offset2 = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        boolean value = false;
        if (chLocal == 't') {
            if (charAt(this.bp + 1) == 'r' && charAt((this.bp + 1) + 1) == 'u' && charAt((this.bp + 1) + 2) == 'e') {
                offset = (offset2 + 3) + 1;
                chLocal = charAt(this.bp + 4);
                value = true;
                offset2 = offset;
            } else {
                this.matchStat = -1;
                offset = offset2;
                return false;
            }
        } else if (chLocal == 'f') {
            if (charAt(this.bp + 1) == 'a' && charAt((this.bp + 1) + 1) == 'l' && charAt((this.bp + 1) + 2) == 's' && charAt((this.bp + 1) + 3) == 'e') {
                offset = (offset2 + 4) + 1;
                chLocal = charAt(this.bp + 5);
                value = false;
                offset2 = offset;
            } else {
                this.matchStat = -1;
                offset = offset2;
                return false;
            }
        } else if (chLocal == '1') {
            offset = offset2 + 1;
            chLocal = charAt(this.bp + 1);
            value = true;
            offset2 = offset;
        } else if (chLocal == '0') {
            offset = offset2 + 1;
            chLocal = charAt(this.bp + 1);
            value = false;
            offset2 = offset;
        }
        while (chLocal != expectNext) {
            if (isWhitespace(chLocal)) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                offset2 = offset;
            } else {
                this.matchStat = -1;
                offset = offset2;
                return value;
            }
        }
        this.bp += offset2;
        this.ch = charAt(this.bp);
        this.matchStat = 3;
        offset = offset2;
        return value;
    }

    public int scanInt(char expectNext) {
        boolean quote;
        boolean negative = true;
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        if (chLocal == '\"') {
            quote = true;
        } else {
            quote = false;
        }
        if (quote) {
            int offset2 = offset + 1;
            chLocal = charAt(this.bp + 1);
            offset = offset2;
        }
        if (chLocal != '-') {
            negative = false;
        }
        if (negative) {
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
        } else {
            offset2 = offset;
        }
        if (chLocal >= '0' && chLocal <= '9') {
            int value = chLocal - 48;
            while (true) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal >= '0' && chLocal <= '9') {
                    value = (value * 10) + (chLocal - 48);
                    offset2 = offset;
                }
            }
            if (chLocal == '.') {
                this.matchStat = -1;
                offset2 = offset;
                return 0;
            } else if (value < 0) {
                this.matchStat = -1;
                offset2 = offset;
                return 0;
            } else {
                while (chLocal != expectNext) {
                    if (isWhitespace(chLocal)) {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                        offset = offset2;
                    } else {
                        this.matchStat = -1;
                        if (negative) {
                            value = -value;
                        }
                        offset2 = offset;
                        return value;
                    }
                }
                this.bp += offset;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                if (negative) {
                    value = -value;
                }
                offset2 = offset;
                return value;
            }
        } else if (chLocal == 'n' && charAt(this.bp + offset2) == 'u' && charAt((this.bp + offset2) + 1) == 'l' && charAt((this.bp + offset2) + 2) == 'l') {
            this.matchStat = 5;
            offset2 += 3;
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (quote && chLocal == '\"') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                offset = offset2;
            }
            while (chLocal != ',') {
                if (chLocal == ']') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 5;
                    this.token = 15;
                    offset2 = offset;
                    return 0;
                } else if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return 0;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 5;
            this.token = 16;
            offset2 = offset;
            return 0;
        } else {
            this.matchStat = -1;
            return 0;
        }
    }

    public boolean scanFieldBoolean(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            boolean value;
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            if (chLocal == 't') {
                offset = offset2 + 1;
                if (charAt(this.bp + offset2) != 'r') {
                    this.matchStat = -1;
                    return false;
                }
                offset2 = offset + 1;
                if (charAt(this.bp + offset) != 'u') {
                    this.matchStat = -1;
                    return false;
                }
                offset = offset2 + 1;
                if (charAt(this.bp + offset2) != 'e') {
                    this.matchStat = -1;
                    return false;
                }
                value = true;
            } else if (chLocal == 'f') {
                offset = offset2 + 1;
                if (charAt(this.bp + offset2) != 'a') {
                    this.matchStat = -1;
                    return false;
                }
                offset2 = offset + 1;
                if (charAt(this.bp + offset) != 'l') {
                    this.matchStat = -1;
                    return false;
                }
                offset = offset2 + 1;
                if (charAt(this.bp + offset2) != 's') {
                    this.matchStat = -1;
                    return false;
                }
                offset2 = offset + 1;
                if (charAt(this.bp + offset) != 'e') {
                    this.matchStat = -1;
                    return false;
                }
                value = false;
                offset = offset2;
            } else {
                this.matchStat = -1;
                return false;
            }
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
            if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                return value;
            } else if (chLocal == '}') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return false;
                }
                this.matchStat = 4;
                return value;
            } else {
                this.matchStat = -1;
                return false;
            }
        }
        this.matchStat = -2;
        return false;
    }

    public long scanFieldLong(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            boolean negative = false;
            if (chLocal == '-') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                negative = true;
            } else {
                offset = offset2;
            }
            if (chLocal < '0' || chLocal > '9') {
                this.matchStat = -1;
                return 0;
            }
            long value = (long) (chLocal - 48);
            while (true) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal >= '0' && chLocal <= '9') {
                    value = (10 * value) + ((long) (chLocal - 48));
                    offset = offset2;
                }
            }
            if (chLocal == '.') {
                this.matchStat = -1;
                return 0;
            }
            boolean valid = offset2 - fieldName.length < 21 && (value >= 0 || (value == Long.MIN_VALUE && negative));
            if (!valid) {
                this.matchStat = -1;
                return 0;
            } else if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                if (negative) {
                    return -value;
                }
                return value;
            } else if (chLocal == '}') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return 0;
                }
                this.matchStat = 4;
                if (negative) {
                    return -value;
                }
                return value;
            } else {
                this.matchStat = -1;
                return 0;
            }
        }
        this.matchStat = -2;
        return 0;
    }

    public long scanLong(char expectNextChar) {
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        boolean quote = chLocal == '\"';
        if (quote) {
            int offset2 = offset + 1;
            chLocal = charAt(this.bp + 1);
            offset = offset2;
        }
        boolean negative = chLocal == '-';
        if (negative) {
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
        } else {
            offset2 = offset;
        }
        if (chLocal >= '0' && chLocal <= '9') {
            long value = (long) (chLocal - 48);
            while (true) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal >= '0' && chLocal <= '9') {
                    value = (10 * value) + ((long) (chLocal - 48));
                    offset2 = offset;
                }
            }
            if (chLocal == '.') {
                this.matchStat = -1;
                offset2 = offset;
                return 0;
            }
            boolean valid = value >= 0 || (value == Long.MIN_VALUE && negative);
            if (valid) {
                if (quote) {
                    if (chLocal != '\"') {
                        this.matchStat = -1;
                        offset2 = offset;
                        return 0;
                    }
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                }
                while (chLocal != expectNextChar) {
                    if (isWhitespace(chLocal)) {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                        offset = offset2;
                    } else {
                        this.matchStat = -1;
                        offset2 = offset;
                        return value;
                    }
                }
                this.bp += offset;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                if (negative) {
                    value = -value;
                }
                offset2 = offset;
                return value;
            }
            throw new NumberFormatException(subString(this.bp, offset - 1));
        } else if (chLocal == 'n' && charAt(this.bp + offset2) == 'u' && charAt((this.bp + offset2) + 1) == 'l' && charAt((this.bp + offset2) + 2) == 'l') {
            this.matchStat = 5;
            offset2 += 3;
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (quote && chLocal == '\"') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                offset = offset2;
            }
            while (chLocal != ',') {
                if (chLocal == ']') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 5;
                    this.token = 15;
                    offset2 = offset;
                    return 0;
                } else if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return 0;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 5;
            this.token = 16;
            offset2 = offset;
            return 0;
        } else {
            this.matchStat = -1;
            return 0;
        }
    }

    public final float scanFieldFloat(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            boolean quote = chLocal == '\"';
            if (quote) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                offset2 = offset;
            }
            boolean negative = chLocal == '-';
            if (negative) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
            } else {
                offset = offset2;
            }
            if (chLocal >= '0' && chLocal <= '9') {
                int power;
                int start;
                int count;
                float parseFloat;
                int intVal = chLocal - 48;
                while (true) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    if (chLocal < '0' || chLocal > '9') {
                        power = 1;
                    } else {
                        intVal = (intVal * 10) + (chLocal - 48);
                        offset = offset2;
                    }
                }
                power = 1;
                if (chLocal == '.') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal >= '0' && chLocal <= '9') {
                        intVal = (intVal * 10) + (chLocal - 48);
                        power = 10;
                        while (true) {
                            offset2 = offset + 1;
                            chLocal = charAt(this.bp + offset);
                            if (chLocal < '0' || chLocal > '9') {
                                break;
                            }
                            intVal = (intVal * 10) + (chLocal - 48);
                            power *= 10;
                            offset = offset2;
                        }
                    } else {
                        this.matchStat = -1;
                        return 0.0f;
                    }
                }
                boolean exp = chLocal == 'e' || chLocal == 'E';
                if (exp) {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == '+' || chLocal == '-') {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                    } else {
                        offset2 = offset;
                    }
                    while (chLocal >= '0' && chLocal <= '9') {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    }
                }
                if (!quote) {
                    start = this.bp + fieldName.length;
                    count = ((this.bp + offset2) - start) - 1;
                } else if (chLocal != '\"') {
                    this.matchStat = -1;
                    return 0.0f;
                } else {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    start = (this.bp + fieldName.length) + 1;
                    count = ((this.bp + offset) - start) - 2;
                    offset2 = offset;
                }
                if (exp || count >= 20) {
                    parseFloat = Float.parseFloat(subString(start, count));
                } else {
                    parseFloat = ((float) intVal) / ((float) power);
                    if (negative) {
                        parseFloat = -parseFloat;
                    }
                }
                if (chLocal == ',') {
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                    this.matchStat = 3;
                    this.token = 16;
                    return parseFloat;
                } else if (chLocal == '}') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == ',') {
                        this.token = 16;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == ']') {
                        this.token = 15;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '}') {
                        this.token = 13;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '\u001a') {
                        this.bp += offset - 1;
                        this.token = 20;
                        this.ch = '\u001a';
                    } else {
                        this.matchStat = -1;
                        return 0.0f;
                    }
                    this.matchStat = 4;
                    return parseFloat;
                } else {
                    this.matchStat = -1;
                    return 0.0f;
                }
            } else if (chLocal == 'n' && charAt(this.bp + offset) == 'u' && charAt((this.bp + offset) + 1) == 'l' && charAt((this.bp + offset) + 2) == 'l') {
                this.matchStat = 5;
                offset += 3;
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (quote && chLocal == '\"') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    offset2 = offset;
                }
                while (chLocal != ',') {
                    if (chLocal == '}') {
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                        this.matchStat = 5;
                        this.token = 13;
                        return 0.0f;
                    } else if (isWhitespace(chLocal)) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    } else {
                        this.matchStat = -1;
                        return 0.0f;
                    }
                }
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 5;
                this.token = 16;
                return 0.0f;
            } else {
                this.matchStat = -1;
                return 0.0f;
            }
        }
        this.matchStat = -2;
        return 0.0f;
    }

    public final float scanFloat(char seperator) {
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        boolean quote = chLocal == '\"';
        if (quote) {
            int offset2 = offset + 1;
            chLocal = charAt(this.bp + 1);
            offset = offset2;
        }
        boolean negative = chLocal == '-';
        if (negative) {
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
        } else {
            offset2 = offset;
        }
        if (chLocal >= '0' && chLocal <= '9') {
            long power;
            int start;
            int count;
            float parseFloat;
            long intVal = (long) (chLocal - 48);
            while (true) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal < '0' || chLocal > '9') {
                    power = 1;
                } else {
                    intVal = (10 * intVal) + ((long) (chLocal - 48));
                    offset2 = offset;
                }
            }
            power = 1;
            if (chLocal == '.') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal >= '0' && chLocal <= '9') {
                    intVal = (10 * intVal) + ((long) (chLocal - 48));
                    power = 10;
                    while (true) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        if (chLocal < '0' || chLocal > '9') {
                            break;
                        }
                        intVal = (10 * intVal) + ((long) (chLocal - 48));
                        power *= 10;
                        offset2 = offset;
                    }
                } else {
                    this.matchStat = -1;
                    return 0.0f;
                }
            }
            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal == '+' || chLocal == '-') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                } else {
                    offset = offset2;
                }
                while (chLocal >= '0' && chLocal <= '9') {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                }
            }
            if (!quote) {
                start = this.bp;
                count = ((this.bp + offset) - start) - 1;
                offset2 = offset;
            } else if (chLocal != '\"') {
                this.matchStat = -1;
                offset2 = offset;
                return 0.0f;
            } else {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                start = this.bp + 1;
                count = ((this.bp + offset2) - start) - 2;
            }
            if (exp || count >= 20) {
                parseFloat = Float.parseFloat(subString(start, count));
            } else {
                parseFloat = ((float) intVal) / ((float) power);
                if (negative) {
                    parseFloat = -parseFloat;
                }
            }
            if (chLocal == seperator) {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                return parseFloat;
            }
            this.matchStat = -1;
            return parseFloat;
        } else if (chLocal == 'n' && charAt(this.bp + offset2) == 'u' && charAt((this.bp + offset2) + 1) == 'l' && charAt((this.bp + offset2) + 2) == 'l') {
            this.matchStat = 5;
            offset2 += 3;
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (quote && chLocal == '\"') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                offset = offset2;
            }
            while (chLocal != ',') {
                if (chLocal == ']') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 5;
                    this.token = 15;
                    offset2 = offset;
                    return 0.0f;
                } else if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return 0.0f;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 5;
            this.token = 16;
            offset2 = offset;
            return 0.0f;
        } else {
            this.matchStat = -1;
            return 0.0f;
        }
    }

    public double scanDouble(char seperator) {
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        boolean quote = chLocal == '\"';
        if (quote) {
            int offset2 = offset + 1;
            chLocal = charAt(this.bp + 1);
            offset = offset2;
        }
        boolean negative = chLocal == '-';
        if (negative) {
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
        } else {
            offset2 = offset;
        }
        if (chLocal >= '0' && chLocal <= '9') {
            long power;
            int start;
            int count;
            double parseDouble;
            long intVal = (long) (chLocal - 48);
            while (true) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal < '0' || chLocal > '9') {
                    power = 1;
                } else {
                    intVal = (10 * intVal) + ((long) (chLocal - 48));
                    offset2 = offset;
                }
            }
            power = 1;
            if (chLocal == '.') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal >= '0' && chLocal <= '9') {
                    intVal = (10 * intVal) + ((long) (chLocal - 48));
                    power = 10;
                    while (true) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        if (chLocal < '0' || chLocal > '9') {
                            break;
                        }
                        intVal = (10 * intVal) + ((long) (chLocal - 48));
                        power *= 10;
                        offset2 = offset;
                    }
                } else {
                    this.matchStat = -1;
                    return 0.0d;
                }
            }
            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal == '+' || chLocal == '-') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                } else {
                    offset = offset2;
                }
                while (chLocal >= '0' && chLocal <= '9') {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                }
            }
            if (!quote) {
                start = this.bp;
                count = ((this.bp + offset) - start) - 1;
                offset2 = offset;
            } else if (chLocal != '\"') {
                this.matchStat = -1;
                offset2 = offset;
                return 0.0d;
            } else {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                start = this.bp + 1;
                count = ((this.bp + offset2) - start) - 2;
            }
            if (exp || count >= 20) {
                parseDouble = Double.parseDouble(subString(start, count));
            } else {
                parseDouble = ((double) intVal) / ((double) power);
                if (negative) {
                    parseDouble = -parseDouble;
                }
            }
            if (chLocal == seperator) {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                return parseDouble;
            }
            this.matchStat = -1;
            return parseDouble;
        } else if (chLocal == 'n' && charAt(this.bp + offset2) == 'u' && charAt((this.bp + offset2) + 1) == 'l' && charAt((this.bp + offset2) + 2) == 'l') {
            this.matchStat = 5;
            offset2 += 3;
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (quote && chLocal == '\"') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                offset = offset2;
            }
            while (chLocal != ',') {
                if (chLocal == ']') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 5;
                    this.token = 15;
                    offset2 = offset;
                    return 0.0d;
                } else if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return 0.0d;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 5;
            this.token = 16;
            offset2 = offset;
            return 0.0d;
        } else {
            this.matchStat = -1;
            return 0.0d;
        }
    }

    public BigDecimal scanDecimal(char seperator) {
        this.matchStat = 0;
        int offset = 0 + 1;
        char chLocal = charAt(this.bp + 0);
        boolean quote = chLocal == '\"';
        if (quote) {
            int offset2 = offset + 1;
            chLocal = charAt(this.bp + 1);
            offset = offset2;
        }
        if (chLocal == '-') {
            offset2 = offset + 1;
            chLocal = charAt(this.bp + offset);
        } else {
            offset2 = offset;
        }
        if (chLocal >= '0' && chLocal <= '9') {
            int start;
            int count;
            while (true) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal >= '0' && chLocal <= '9') {
                    offset2 = offset;
                }
            }
            if (chLocal == '.') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal >= '0' && chLocal <= '9') {
                    while (true) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        if (chLocal < '0' || chLocal > '9') {
                            break;
                        }
                        offset2 = offset;
                    }
                } else {
                    this.matchStat = -1;
                    return null;
                }
            }
            boolean exp = chLocal == 'e' || chLocal == 'E';
            if (exp) {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (chLocal == '+' || chLocal == '-') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                } else {
                    offset = offset2;
                }
                while (chLocal >= '0' && chLocal <= '9') {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                }
            }
            if (!quote) {
                start = this.bp;
                count = ((this.bp + offset) - start) - 1;
                offset2 = offset;
            } else if (chLocal != '\"') {
                this.matchStat = -1;
                offset2 = offset;
                return null;
            } else {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                start = this.bp + 1;
                count = ((this.bp + offset2) - start) - 2;
            }
            BigDecimal value = new BigDecimal(sub_chars(start, count));
            if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                this.token = 16;
                return value;
            } else if (chLocal == ']') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return null;
                }
                this.matchStat = 4;
                offset2 = offset;
                return value;
            } else {
                this.matchStat = -1;
                return null;
            }
        } else if (chLocal == 'n' && charAt(this.bp + offset2) == 'u' && charAt((this.bp + offset2) + 1) == 'l' && charAt((this.bp + offset2) + 2) == 'l') {
            this.matchStat = 5;
            offset2 += 3;
            offset = offset2 + 1;
            chLocal = charAt(this.bp + offset2);
            if (quote && chLocal == '\"') {
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                offset = offset2;
            }
            while (chLocal != ',') {
                if (chLocal == '}') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 5;
                    this.token = 13;
                    offset2 = offset;
                    return null;
                } else if (isWhitespace(chLocal)) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    offset = offset2;
                } else {
                    this.matchStat = -1;
                    offset2 = offset;
                    return null;
                }
            }
            this.bp += offset;
            this.ch = charAt(this.bp);
            this.matchStat = 5;
            this.token = 16;
            offset2 = offset;
            return null;
        } else {
            this.matchStat = -1;
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final float[] scanFieldFloatArray(char[] r22) {
        /*
        r21 = this;
        r19 = 0;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r19 = r21.charArrayCompare(r22);
        if (r19 != 0) goto L_0x0018;
    L_0x000e:
        r19 = -2;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
    L_0x0017:
        return r3;
    L_0x0018:
        r0 = r22;
        r11 = r0.length;
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 91;
        r0 = r19;
        if (r6 == r0) goto L_0x003d;
    L_0x0033:
        r19 = -2;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
        goto L_0x0017;
    L_0x003d:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r11 = r12 + 1;
        r19 = r19 + r12;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 16;
        r0 = r19;
        r3 = new float[r0];
        r4 = 0;
    L_0x0056:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r19 = r19 + r11;
        r15 = r19 + -1;
        r19 = 45;
        r0 = r19;
        if (r6 != r0) goto L_0x00b0;
    L_0x0066:
        r10 = 1;
    L_0x0067:
        if (r10 == 0) goto L_0x007c;
    L_0x0069:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r11 = r12;
    L_0x007c:
        r19 = 48;
        r0 = r19;
        if (r6 < r0) goto L_0x0247;
    L_0x0082:
        r19 = 57;
        r0 = r19;
        if (r6 > r0) goto L_0x0247;
    L_0x0088:
        r9 = r6 + -48;
    L_0x008a:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 48;
        r0 = r19;
        if (r6 < r0) goto L_0x00b2;
    L_0x00a2:
        r19 = 57;
        r0 = r19;
        if (r6 > r0) goto L_0x00b2;
    L_0x00a8:
        r19 = r9 * 10;
        r20 = r6 + -48;
        r9 = r19 + r20;
        r11 = r12;
        goto L_0x008a;
    L_0x00b0:
        r10 = 0;
        goto L_0x0067;
    L_0x00b2:
        r13 = 1;
        r19 = 46;
        r0 = r19;
        if (r6 != r0) goto L_0x010a;
    L_0x00b9:
        r14 = 1;
    L_0x00ba:
        if (r14 == 0) goto L_0x0117;
    L_0x00bc:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r11 = r12 + 1;
        r19 = r19 + r12;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r13 = 10;
        r19 = 48;
        r0 = r19;
        if (r6 < r0) goto L_0x010c;
    L_0x00d6:
        r19 = 57;
        r0 = r19;
        if (r6 > r0) goto L_0x010c;
    L_0x00dc:
        r19 = r9 * 10;
        r20 = r6 + -48;
        r9 = r19 + r20;
    L_0x00e2:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 48;
        r0 = r19;
        if (r6 < r0) goto L_0x0117;
    L_0x00fa:
        r19 = 57;
        r0 = r19;
        if (r6 > r0) goto L_0x0117;
    L_0x0100:
        r19 = r9 * 10;
        r20 = r6 + -48;
        r9 = r19 + r20;
        r13 = r13 * 10;
        r11 = r12;
        goto L_0x00e2;
    L_0x010a:
        r14 = 0;
        goto L_0x00ba;
    L_0x010c:
        r19 = -1;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
        goto L_0x0017;
    L_0x0117:
        r19 = 101; // 0x65 float:1.42E-43 double:5.0E-322;
        r0 = r19;
        if (r6 == r0) goto L_0x0123;
    L_0x011d:
        r19 = 69;
        r0 = r19;
        if (r6 != r0) goto L_0x0176;
    L_0x0123:
        r8 = 1;
    L_0x0124:
        if (r8 == 0) goto L_0x0178;
    L_0x0126:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r11 = r12 + 1;
        r19 = r19 + r12;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 43;
        r0 = r19;
        if (r6 == r0) goto L_0x0144;
    L_0x013e:
        r19 = 45;
        r0 = r19;
        if (r6 != r0) goto L_0x0316;
    L_0x0144:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
    L_0x0156:
        r19 = 48;
        r0 = r19;
        if (r6 < r0) goto L_0x0178;
    L_0x015c:
        r19 = 57;
        r0 = r19;
        if (r6 > r0) goto L_0x0178;
    L_0x0162:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r11 = r12 + 1;
        r19 = r19 + r12;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r12 = r11;
        goto L_0x0156;
    L_0x0176:
        r8 = 0;
        goto L_0x0124;
    L_0x0178:
        r11 = r12;
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r19 = r19 + r11;
        r19 = r19 - r15;
        r7 = r19 + -1;
        if (r8 != 0) goto L_0x01df;
    L_0x0187:
        r19 = 10;
        r0 = r19;
        if (r7 >= r0) goto L_0x01df;
    L_0x018d:
        r0 = (float) r9;
        r19 = r0;
        r0 = (float) r13;
        r20 = r0;
        r18 = r19 / r20;
        if (r10 == 0) goto L_0x019c;
    L_0x0197:
        r0 = r18;
        r0 = -r0;
        r18 = r0;
    L_0x019c:
        r0 = r3.length;
        r19 = r0;
        r0 = r19;
        if (r4 < r0) goto L_0x01bf;
    L_0x01a3:
        r0 = r3.length;
        r19 = r0;
        r19 = r19 * 3;
        r19 = r19 / 2;
        r0 = r19;
        r0 = new float[r0];
        r17 = r0;
        r19 = 0;
        r20 = 0;
        r0 = r19;
        r1 = r17;
        r2 = r20;
        java.lang.System.arraycopy(r3, r0, r1, r2, r4);
        r3 = r17;
    L_0x01bf:
        r5 = r4 + 1;
        r3[r4] = r18;
        r19 = 44;
        r0 = r19;
        if (r6 != r0) goto L_0x01ea;
    L_0x01c9:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r11 = r12;
    L_0x01dc:
        r4 = r5;
        goto L_0x0056;
    L_0x01df:
        r0 = r21;
        r16 = r0.subString(r15, r7);
        r18 = java.lang.Float.parseFloat(r16);
        goto L_0x019c;
    L_0x01ea:
        r19 = 93;
        r0 = r19;
        if (r6 != r0) goto L_0x01dc;
    L_0x01f0:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r12 = r11 + 1;
        r19 = r19 + r11;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r0 = r3.length;
        r19 = r0;
        r0 = r19;
        if (r5 == r0) goto L_0x021c;
    L_0x0209:
        r0 = new float[r5];
        r17 = r0;
        r19 = 0;
        r20 = 0;
        r0 = r19;
        r1 = r17;
        r2 = r20;
        java.lang.System.arraycopy(r3, r0, r1, r2, r5);
        r3 = r17;
    L_0x021c:
        r19 = 44;
        r0 = r19;
        if (r6 != r0) goto L_0x0252;
    L_0x0222:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r20 = r12 + -1;
        r19 = r19 + r20;
        r0 = r19;
        r1 = r21;
        r1.bp = r0;
        r21.next();
        r19 = 3;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r19 = 16;
        r0 = r19;
        r1 = r21;
        r1.token = r0;
        goto L_0x0017;
    L_0x0247:
        r19 = -1;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
        goto L_0x0017;
    L_0x0252:
        r19 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r19;
        if (r6 != r0) goto L_0x030b;
    L_0x0258:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r11 = r12 + 1;
        r19 = r19 + r12;
        r0 = r21;
        r1 = r19;
        r6 = r0.charAt(r1);
        r19 = 44;
        r0 = r19;
        if (r6 != r0) goto L_0x0295;
    L_0x0270:
        r19 = 16;
        r0 = r19;
        r1 = r21;
        r1.token = r0;
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r20 = r11 + -1;
        r19 = r19 + r20;
        r0 = r19;
        r1 = r21;
        r1.bp = r0;
        r21.next();
    L_0x028b:
        r19 = 4;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        goto L_0x0017;
    L_0x0295:
        r19 = 93;
        r0 = r19;
        if (r6 != r0) goto L_0x02b7;
    L_0x029b:
        r19 = 15;
        r0 = r19;
        r1 = r21;
        r1.token = r0;
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r20 = r11 + -1;
        r19 = r19 + r20;
        r0 = r19;
        r1 = r21;
        r1.bp = r0;
        r21.next();
        goto L_0x028b;
    L_0x02b7:
        r19 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r19;
        if (r6 != r0) goto L_0x02d9;
    L_0x02bd:
        r19 = 13;
        r0 = r19;
        r1 = r21;
        r1.token = r0;
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r20 = r11 + -1;
        r19 = r19 + r20;
        r0 = r19;
        r1 = r21;
        r1.bp = r0;
        r21.next();
        goto L_0x028b;
    L_0x02d9:
        r19 = 26;
        r0 = r19;
        if (r6 != r0) goto L_0x0300;
    L_0x02df:
        r0 = r21;
        r0 = r0.bp;
        r19 = r0;
        r20 = r11 + -1;
        r19 = r19 + r20;
        r0 = r19;
        r1 = r21;
        r1.bp = r0;
        r19 = 20;
        r0 = r19;
        r1 = r21;
        r1.token = r0;
        r19 = 26;
        r0 = r19;
        r1 = r21;
        r1.ch = r0;
        goto L_0x028b;
    L_0x0300:
        r19 = -1;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
        goto L_0x0017;
    L_0x030b:
        r19 = -1;
        r0 = r19;
        r1 = r21;
        r1.matchStat = r0;
        r3 = 0;
        goto L_0x0017;
    L_0x0316:
        r12 = r11;
        goto L_0x0156;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.JSONLexerBase.scanFieldFloatArray(char[]):float[]");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final float[][] scanFieldFloatArray2(char[] r24) {
        /*
        r23 = this;
        r21 = 0;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = r23.charArrayCompare(r24);
        if (r21 != 0) goto L_0x001b;
    L_0x000e:
        r21 = -2;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
    L_0x001a:
        return r21;
    L_0x001b:
        r0 = r24;
        r14 = r0.length;
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 91;
        r0 = r21;
        if (r9 == r0) goto L_0x0043;
    L_0x0036:
        r21 = -2;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
        goto L_0x001a;
    L_0x0043:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 16;
        r0 = r21;
        r6 = new float[r0][];
        r7 = 0;
        r8 = r7;
        r15 = r14;
    L_0x005e:
        r21 = 91;
        r0 = r21;
        if (r9 != r0) goto L_0x005e;
    L_0x0064:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 16;
        r0 = r21;
        r3 = new float[r0];
        r4 = 0;
    L_0x007d:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r21 = r21 + r14;
        r17 = r21 + -1;
        r21 = 45;
        r0 = r21;
        if (r9 != r0) goto L_0x00d7;
    L_0x008d:
        r13 = 1;
    L_0x008e:
        if (r13 == 0) goto L_0x00a3;
    L_0x0090:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r14 = r15;
    L_0x00a3:
        r21 = 48;
        r0 = r21;
        if (r9 < r0) goto L_0x0289;
    L_0x00a9:
        r21 = 57;
        r0 = r21;
        if (r9 > r0) goto L_0x0289;
    L_0x00af:
        r12 = r9 + -48;
    L_0x00b1:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 48;
        r0 = r21;
        if (r9 < r0) goto L_0x00d9;
    L_0x00c9:
        r21 = 57;
        r0 = r21;
        if (r9 > r0) goto L_0x00d9;
    L_0x00cf:
        r21 = r12 * 10;
        r22 = r9 + -48;
        r12 = r21 + r22;
        r14 = r15;
        goto L_0x00b1;
    L_0x00d7:
        r13 = 0;
        goto L_0x008e;
    L_0x00d9:
        r16 = 1;
        r21 = 46;
        r0 = r21;
        if (r9 != r0) goto L_0x013d;
    L_0x00e1:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 48;
        r0 = r21;
        if (r9 < r0) goto L_0x012f;
    L_0x00f9:
        r21 = 57;
        r0 = r21;
        if (r9 > r0) goto L_0x012f;
    L_0x00ff:
        r21 = r12 * 10;
        r22 = r9 + -48;
        r12 = r21 + r22;
        r16 = 10;
    L_0x0107:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 48;
        r0 = r21;
        if (r9 < r0) goto L_0x013d;
    L_0x011f:
        r21 = 57;
        r0 = r21;
        if (r9 > r0) goto L_0x013d;
    L_0x0125:
        r21 = r12 * 10;
        r22 = r9 + -48;
        r12 = r21 + r22;
        r16 = r16 * 10;
        r14 = r15;
        goto L_0x0107;
    L_0x012f:
        r21 = -1;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
        goto L_0x001a;
    L_0x013d:
        r21 = 101; // 0x65 float:1.42E-43 double:5.0E-322;
        r0 = r21;
        if (r9 == r0) goto L_0x0149;
    L_0x0143:
        r21 = 69;
        r0 = r21;
        if (r9 != r0) goto L_0x019c;
    L_0x0149:
        r11 = 1;
    L_0x014a:
        if (r11 == 0) goto L_0x019e;
    L_0x014c:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 43;
        r0 = r21;
        if (r9 == r0) goto L_0x016a;
    L_0x0164:
        r21 = 45;
        r0 = r21;
        if (r9 != r0) goto L_0x03c5;
    L_0x016a:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
    L_0x017c:
        r21 = 48;
        r0 = r21;
        if (r9 < r0) goto L_0x019e;
    L_0x0182:
        r21 = 57;
        r0 = r21;
        if (r9 > r0) goto L_0x019e;
    L_0x0188:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r15 = r14;
        goto L_0x017c;
    L_0x019c:
        r11 = 0;
        goto L_0x014a;
    L_0x019e:
        r14 = r15;
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r21 = r21 + r14;
        r21 = r21 - r17;
        r10 = r21 + -1;
        if (r11 != 0) goto L_0x0207;
    L_0x01ad:
        r21 = 10;
        r0 = r21;
        if (r10 >= r0) goto L_0x0207;
    L_0x01b3:
        r0 = (float) r12;
        r21 = r0;
        r0 = r16;
        r0 = (float) r0;
        r22 = r0;
        r20 = r21 / r22;
        if (r13 == 0) goto L_0x01c4;
    L_0x01bf:
        r0 = r20;
        r0 = -r0;
        r20 = r0;
    L_0x01c4:
        r0 = r3.length;
        r21 = r0;
        r0 = r21;
        if (r4 < r0) goto L_0x01e7;
    L_0x01cb:
        r0 = r3.length;
        r21 = r0;
        r21 = r21 * 3;
        r21 = r21 / 2;
        r0 = r21;
        r0 = new float[r0];
        r19 = r0;
        r21 = 0;
        r22 = 0;
        r0 = r21;
        r1 = r19;
        r2 = r22;
        java.lang.System.arraycopy(r3, r0, r1, r2, r4);
        r3 = r19;
    L_0x01e7:
        r5 = r4 + 1;
        r3[r4] = r20;
        r21 = 44;
        r0 = r21;
        if (r9 != r0) goto L_0x0214;
    L_0x01f1:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r14 = r15;
    L_0x0204:
        r4 = r5;
        goto L_0x007d;
    L_0x0207:
        r0 = r23;
        r1 = r17;
        r18 = r0.subString(r1, r10);
        r20 = java.lang.Float.parseFloat(r18);
        goto L_0x01c4;
    L_0x0214:
        r21 = 93;
        r0 = r21;
        if (r9 != r0) goto L_0x0204;
    L_0x021a:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r0 = r3.length;
        r21 = r0;
        r0 = r21;
        if (r5 == r0) goto L_0x0246;
    L_0x0233:
        r0 = new float[r5];
        r19 = r0;
        r21 = 0;
        r22 = 0;
        r0 = r21;
        r1 = r19;
        r2 = r22;
        java.lang.System.arraycopy(r3, r0, r1, r2, r5);
        r3 = r19;
    L_0x0246:
        r0 = r6.length;
        r21 = r0;
        r0 = r21;
        if (r8 < r0) goto L_0x0269;
    L_0x024d:
        r0 = r6.length;
        r21 = r0;
        r21 = r21 * 3;
        r21 = r21 / 2;
        r0 = r21;
        r0 = new float[r0][];
        r19 = r0;
        r21 = 0;
        r22 = 0;
        r0 = r21;
        r1 = r19;
        r2 = r22;
        java.lang.System.arraycopy(r3, r0, r1, r2, r5);
        r6 = r19;
    L_0x0269:
        r7 = r8 + 1;
        r6[r8] = r3;
        r21 = 44;
        r0 = r21;
        if (r9 != r0) goto L_0x0297;
    L_0x0273:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
    L_0x0285:
        r8 = r7;
        r15 = r14;
        goto L_0x005e;
    L_0x0289:
        r21 = -1;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
        goto L_0x001a;
    L_0x0297:
        r21 = 93;
        r0 = r21;
        if (r9 != r0) goto L_0x03c2;
    L_0x029d:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r14 = r15 + 1;
        r21 = r21 + r15;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r0 = r6.length;
        r21 = r0;
        r0 = r21;
        if (r7 == r0) goto L_0x02c9;
    L_0x02b6:
        r0 = new float[r7][];
        r19 = r0;
        r21 = 0;
        r22 = 0;
        r0 = r21;
        r1 = r19;
        r2 = r22;
        java.lang.System.arraycopy(r6, r0, r1, r2, r7);
        r6 = r19;
    L_0x02c9:
        r21 = 44;
        r0 = r21;
        if (r9 != r0) goto L_0x02f6;
    L_0x02cf:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r22 = r14 + -1;
        r21 = r21 + r22;
        r0 = r21;
        r1 = r23;
        r1.bp = r0;
        r23.next();
        r21 = 3;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 16;
        r0 = r21;
        r1 = r23;
        r1.token = r0;
        r21 = r6;
        goto L_0x001a;
    L_0x02f6:
        r21 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r21;
        if (r9 != r0) goto L_0x03b4;
    L_0x02fc:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r15 = r14 + 1;
        r21 = r21 + r14;
        r0 = r23;
        r1 = r21;
        r9 = r0.charAt(r1);
        r21 = 44;
        r0 = r21;
        if (r9 != r0) goto L_0x033b;
    L_0x0314:
        r21 = 16;
        r0 = r21;
        r1 = r23;
        r1.token = r0;
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r22 = r15 + -1;
        r21 = r21 + r22;
        r0 = r21;
        r1 = r23;
        r1.bp = r0;
        r23.next();
    L_0x032f:
        r21 = 4;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = r6;
        goto L_0x001a;
    L_0x033b:
        r21 = 93;
        r0 = r21;
        if (r9 != r0) goto L_0x035d;
    L_0x0341:
        r21 = 15;
        r0 = r21;
        r1 = r23;
        r1.token = r0;
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r22 = r15 + -1;
        r21 = r21 + r22;
        r0 = r21;
        r1 = r23;
        r1.bp = r0;
        r23.next();
        goto L_0x032f;
    L_0x035d:
        r21 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r21;
        if (r9 != r0) goto L_0x037f;
    L_0x0363:
        r21 = 13;
        r0 = r21;
        r1 = r23;
        r1.token = r0;
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r22 = r15 + -1;
        r21 = r21 + r22;
        r0 = r21;
        r1 = r23;
        r1.bp = r0;
        r23.next();
        goto L_0x032f;
    L_0x037f:
        r21 = 26;
        r0 = r21;
        if (r9 != r0) goto L_0x03a6;
    L_0x0385:
        r0 = r23;
        r0 = r0.bp;
        r21 = r0;
        r22 = r15 + -1;
        r21 = r21 + r22;
        r0 = r21;
        r1 = r23;
        r1.bp = r0;
        r21 = 20;
        r0 = r21;
        r1 = r23;
        r1.token = r0;
        r21 = 26;
        r0 = r21;
        r1 = r23;
        r1.ch = r0;
        goto L_0x032f;
    L_0x03a6:
        r21 = -1;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
        goto L_0x001a;
    L_0x03b4:
        r21 = -1;
        r0 = r21;
        r1 = r23;
        r1.matchStat = r0;
        r21 = 0;
        r21 = (float[][]) r21;
        goto L_0x001a;
    L_0x03c2:
        r14 = r15;
        goto L_0x0285;
    L_0x03c5:
        r15 = r14;
        goto L_0x017c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.JSONLexerBase.scanFieldFloatArray2(char[]):float[][]");
    }

    public final double scanFieldDouble(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            boolean quote = chLocal == '\"';
            if (quote) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                offset2 = offset;
            }
            boolean negative = chLocal == '-';
            if (negative) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
            } else {
                offset = offset2;
            }
            if (chLocal >= '0' && chLocal <= '9') {
                long power;
                int start;
                int count;
                double parseDouble;
                long intVal = (long) (chLocal - 48);
                while (true) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    if (chLocal < '0' || chLocal > '9') {
                        power = 1;
                    } else {
                        intVal = (10 * intVal) + ((long) (chLocal - 48));
                        offset = offset2;
                    }
                }
                power = 1;
                if (chLocal == '.') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal >= '0' && chLocal <= '9') {
                        intVal = (10 * intVal) + ((long) (chLocal - 48));
                        power = 10;
                        while (true) {
                            offset2 = offset + 1;
                            chLocal = charAt(this.bp + offset);
                            if (chLocal < '0' || chLocal > '9') {
                                break;
                            }
                            intVal = (10 * intVal) + ((long) (chLocal - 48));
                            power *= 10;
                            offset = offset2;
                        }
                    } else {
                        this.matchStat = -1;
                        return 0.0d;
                    }
                }
                boolean exp = chLocal == 'e' || chLocal == 'E';
                if (exp) {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == '+' || chLocal == '-') {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                    } else {
                        offset2 = offset;
                    }
                    while (chLocal >= '0' && chLocal <= '9') {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    }
                }
                if (!quote) {
                    start = this.bp + fieldName.length;
                    count = ((this.bp + offset2) - start) - 1;
                } else if (chLocal != '\"') {
                    this.matchStat = -1;
                    return 0.0d;
                } else {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    start = (this.bp + fieldName.length) + 1;
                    count = ((this.bp + offset) - start) - 2;
                    offset2 = offset;
                }
                if (exp || count >= 20) {
                    parseDouble = Double.parseDouble(subString(start, count));
                } else {
                    parseDouble = ((double) intVal) / ((double) power);
                    if (negative) {
                        parseDouble = -parseDouble;
                    }
                }
                if (chLocal == ',') {
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                    this.matchStat = 3;
                    this.token = 16;
                    return parseDouble;
                } else if (chLocal == '}') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == ',') {
                        this.token = 16;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == ']') {
                        this.token = 15;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '}') {
                        this.token = 13;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '\u001a') {
                        this.token = 20;
                        this.bp += offset - 1;
                        this.ch = '\u001a';
                    } else {
                        this.matchStat = -1;
                        return 0.0d;
                    }
                    this.matchStat = 4;
                    return parseDouble;
                } else {
                    this.matchStat = -1;
                    return 0.0d;
                }
            } else if (chLocal == 'n' && charAt(this.bp + offset) == 'u' && charAt((this.bp + offset) + 1) == 'l' && charAt((this.bp + offset) + 2) == 'l') {
                this.matchStat = 5;
                offset += 3;
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (quote && chLocal == '\"') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    offset2 = offset;
                }
                while (chLocal != ',') {
                    if (chLocal == '}') {
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                        this.matchStat = 5;
                        this.token = 13;
                        return 0.0d;
                    } else if (isWhitespace(chLocal)) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    } else {
                        this.matchStat = -1;
                        return 0.0d;
                    }
                }
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 5;
                this.token = 16;
                return 0.0d;
            } else {
                this.matchStat = -1;
                return 0.0d;
            }
        }
        this.matchStat = -2;
        return 0.0d;
    }

    public BigDecimal scanFieldDecimal(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            boolean quote = chLocal == '\"';
            if (quote) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                offset2 = offset;
            }
            if (chLocal == '-') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
            } else {
                offset = offset2;
            }
            if (chLocal >= '0' && chLocal <= '9') {
                int start;
                int count;
                while (true) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    if (chLocal >= '0' && chLocal <= '9') {
                        offset = offset2;
                    }
                }
                if (chLocal == '.') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal >= '0' && chLocal <= '9') {
                        while (true) {
                            offset2 = offset + 1;
                            chLocal = charAt(this.bp + offset);
                            if (chLocal < '0' || chLocal > '9') {
                                break;
                            }
                            offset = offset2;
                        }
                    } else {
                        this.matchStat = -1;
                        return null;
                    }
                }
                boolean exp = chLocal == 'e' || chLocal == 'E';
                if (exp) {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == '+' || chLocal == '-') {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                    } else {
                        offset2 = offset;
                    }
                    while (chLocal >= '0' && chLocal <= '9') {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    }
                }
                if (!quote) {
                    start = this.bp + fieldName.length;
                    count = ((this.bp + offset2) - start) - 1;
                    offset = offset2;
                } else if (chLocal != '\"') {
                    this.matchStat = -1;
                    return null;
                } else {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    start = (this.bp + fieldName.length) + 1;
                    count = ((this.bp + offset) - start) - 2;
                }
                BigDecimal value = new BigDecimal(sub_chars(start, count));
                if (chLocal == ',') {
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                    this.matchStat = 3;
                    this.token = 16;
                    return value;
                } else if (chLocal == '}') {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    if (chLocal == ',') {
                        this.token = 16;
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == ']') {
                        this.token = 15;
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '}') {
                        this.token = 13;
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '\u001a') {
                        this.token = 20;
                        this.bp += offset2 - 1;
                        this.ch = '\u001a';
                    } else {
                        this.matchStat = -1;
                        return null;
                    }
                    this.matchStat = 4;
                    return value;
                } else {
                    this.matchStat = -1;
                    return null;
                }
            } else if (chLocal == 'n' && charAt(this.bp + offset) == 'u' && charAt((this.bp + offset) + 1) == 'l' && charAt((this.bp + offset) + 2) == 'l') {
                this.matchStat = 5;
                offset += 3;
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (quote && chLocal == '\"') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    offset2 = offset;
                }
                while (chLocal != ',') {
                    if (chLocal == '}') {
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                        this.matchStat = 5;
                        this.token = 13;
                        return null;
                    } else if (isWhitespace(chLocal)) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    } else {
                        this.matchStat = -1;
                        return null;
                    }
                }
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 5;
                this.token = 16;
                return null;
            } else {
                this.matchStat = -1;
                return null;
            }
        }
        this.matchStat = -2;
        return null;
    }

    public BigInteger scanFieldBigInteger(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            boolean quote = chLocal == '\"';
            if (quote) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                offset2 = offset;
            }
            boolean negative = chLocal == '-';
            if (negative) {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
            } else {
                offset = offset2;
            }
            if (chLocal >= '0' && chLocal <= '9') {
                int start;
                int count;
                BigInteger value;
                long intVal = (long) (chLocal - 48);
                while (true) {
                    offset2 = offset + 1;
                    chLocal = charAt(this.bp + offset);
                    if (chLocal >= '0' && chLocal <= '9') {
                        intVal = (10 * intVal) + ((long) (chLocal - 48));
                        offset = offset2;
                    } else if (quote) {
                        start = this.bp + fieldName.length;
                        count = ((this.bp + offset2) - start) - 1;
                    } else if (chLocal == '\"') {
                        this.matchStat = -1;
                        return null;
                    } else {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        start = (this.bp + fieldName.length) + 1;
                        count = ((this.bp + offset) - start) - 2;
                        offset2 = offset;
                    }
                }
                if (quote) {
                    start = this.bp + fieldName.length;
                    count = ((this.bp + offset2) - start) - 1;
                } else if (chLocal == '\"') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    start = (this.bp + fieldName.length) + 1;
                    count = ((this.bp + offset) - start) - 2;
                    offset2 = offset;
                } else {
                    this.matchStat = -1;
                    return null;
                }
                if (count < 20 || (negative && count < 21)) {
                    if (negative) {
                        intVal = -intVal;
                    }
                    value = BigInteger.valueOf(intVal);
                } else {
                    value = new BigInteger(subString(start, count));
                }
                if (chLocal == ',') {
                    this.bp += offset2;
                    this.ch = charAt(this.bp);
                    this.matchStat = 3;
                    this.token = 16;
                    return value;
                } else if (chLocal == '}') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    if (chLocal == ',') {
                        this.token = 16;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == ']') {
                        this.token = 15;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '}') {
                        this.token = 13;
                        this.bp += offset;
                        this.ch = charAt(this.bp);
                    } else if (chLocal == '\u001a') {
                        this.token = 20;
                        this.bp += offset - 1;
                        this.ch = '\u001a';
                    } else {
                        this.matchStat = -1;
                        return null;
                    }
                    this.matchStat = 4;
                    return value;
                } else {
                    this.matchStat = -1;
                    return null;
                }
            } else if (chLocal == 'n' && charAt(this.bp + offset) == 'u' && charAt((this.bp + offset) + 1) == 'l' && charAt((this.bp + offset) + 2) == 'l') {
                this.matchStat = 5;
                offset += 3;
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                if (quote && chLocal == '\"') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    offset2 = offset;
                }
                while (chLocal != ',') {
                    if (chLocal == '}') {
                        this.bp += offset2;
                        this.ch = charAt(this.bp);
                        this.matchStat = 5;
                        this.token = 13;
                        return null;
                    } else if (isWhitespace(chLocal)) {
                        offset = offset2 + 1;
                        chLocal = charAt(this.bp + offset2);
                        offset2 = offset;
                    } else {
                        this.matchStat = -1;
                        return null;
                    }
                }
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 5;
                this.token = 16;
                return null;
            } else {
                this.matchStat = -1;
                return null;
            }
        }
        this.matchStat = -2;
        return null;
    }

    public Date scanFieldDate(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(fieldName)) {
            Date dateVal;
            int offset = fieldName.length;
            int offset2 = offset + 1;
            char chLocal = charAt(this.bp + offset);
            if (chLocal == '\"') {
                int endIndex = indexOf('\"', (this.bp + fieldName.length) + 1);
                if (endIndex == -1) {
                    throw new JSONException("unclosed str");
                }
                int startIndex2 = (this.bp + fieldName.length) + 1;
                String stringVal = subString(startIndex2, endIndex - startIndex2);
                if (stringVal.indexOf(92) != -1) {
                    while (true) {
                        int slashCount = 0;
                        int i = endIndex - 1;
                        while (i >= 0 && charAt(i) == '\\') {
                            slashCount++;
                            i--;
                        }
                        if (slashCount % 2 == 0) {
                            break;
                        }
                        endIndex = indexOf('\"', endIndex + 1);
                    }
                    int chars_len = endIndex - ((this.bp + fieldName.length) + 1);
                    stringVal = readString(sub_chars((this.bp + fieldName.length) + 1, chars_len), chars_len);
                }
                offset = offset2 + ((endIndex - ((this.bp + fieldName.length) + 1)) + 1);
                offset2 = offset + 1;
                chLocal = charAt(this.bp + offset);
                JSONScanner dateLexer = new JSONScanner(stringVal);
                try {
                    if (dateLexer.scanISO8601DateIfMatch(false)) {
                        dateVal = dateLexer.getCalendar().getTime();
                    } else {
                        this.matchStat = -1;
                        dateLexer.close();
                        return null;
                    }
                } finally {
                    dateLexer.close();
                }
            } else if (chLocal == '-' || (chLocal >= '0' && chLocal <= '9')) {
                long millis = 0;
                boolean negative = false;
                if (chLocal == '-') {
                    offset = offset2 + 1;
                    chLocal = charAt(this.bp + offset2);
                    negative = true;
                } else {
                    offset = offset2;
                }
                if (chLocal >= '0' && chLocal <= '9') {
                    millis = (long) (chLocal - 48);
                    while (true) {
                        offset2 = offset + 1;
                        chLocal = charAt(this.bp + offset);
                        if (chLocal < '0' || chLocal > '9') {
                            offset = offset2;
                        } else {
                            millis = (10 * millis) + ((long) (chLocal - 48));
                            offset = offset2;
                        }
                    }
                    offset = offset2;
                }
                if (millis < 0) {
                    this.matchStat = -1;
                    return null;
                }
                if (negative) {
                    millis = -millis;
                }
                dateVal = new Date(millis);
                offset2 = offset;
            } else {
                this.matchStat = -1;
                return null;
            }
            if (chLocal == ',') {
                this.bp += offset2;
                this.ch = charAt(this.bp);
                this.matchStat = 3;
                return dateVal;
            } else if (chLocal == '}') {
                offset = offset2 + 1;
                chLocal = charAt(this.bp + offset2);
                if (chLocal == ',') {
                    this.token = 16;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == ']') {
                    this.token = 15;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '}') {
                    this.token = 13;
                    this.bp += offset;
                    this.ch = charAt(this.bp);
                } else if (chLocal == '\u001a') {
                    this.token = 20;
                    this.bp += offset - 1;
                    this.ch = '\u001a';
                } else {
                    this.matchStat = -1;
                    return null;
                }
                this.matchStat = 4;
                return dateVal;
            } else {
                this.matchStat = -1;
                return null;
            }
        }
        this.matchStat = -2;
        return null;
    }

    public final void scanTrue() {
        if (this.ch != 't') {
            throw new JSONException("error parse true");
        }
        next();
        if (this.ch != 'r') {
            throw new JSONException("error parse true");
        }
        next();
        if (this.ch != 'u') {
            throw new JSONException("error parse true");
        }
        next();
        if (this.ch != 'e') {
            throw new JSONException("error parse true");
        }
        next();
        if (this.ch == ' ' || this.ch == ',' || this.ch == '}' || this.ch == ']' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\u001a' || this.ch == '\f' || this.ch == '\b' || this.ch == ':' || this.ch == '/') {
            this.token = 6;
            return;
        }
        throw new JSONException("scan true error");
    }

    public final void scanNullOrNew() {
        if (this.ch != 'n') {
            throw new JSONException("error parse null or new");
        }
        next();
        if (this.ch == 'u') {
            next();
            if (this.ch != 'l') {
                throw new JSONException("error parse null");
            }
            next();
            if (this.ch != 'l') {
                throw new JSONException("error parse null");
            }
            next();
            if (this.ch == ' ' || this.ch == ',' || this.ch == '}' || this.ch == ']' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\u001a' || this.ch == '\f' || this.ch == '\b') {
                this.token = 8;
                return;
            }
            throw new JSONException("scan null error");
        } else if (this.ch != 'e') {
            throw new JSONException("error parse new");
        } else {
            next();
            if (this.ch != 'w') {
                throw new JSONException("error parse new");
            }
            next();
            if (this.ch == ' ' || this.ch == ',' || this.ch == '}' || this.ch == ']' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\u001a' || this.ch == '\f' || this.ch == '\b') {
                this.token = 9;
                return;
            }
            throw new JSONException("scan new error");
        }
    }

    public final void scanFalse() {
        if (this.ch != 'f') {
            throw new JSONException("error parse false");
        }
        next();
        if (this.ch != 'a') {
            throw new JSONException("error parse false");
        }
        next();
        if (this.ch != 'l') {
            throw new JSONException("error parse false");
        }
        next();
        if (this.ch != 's') {
            throw new JSONException("error parse false");
        }
        next();
        if (this.ch != 'e') {
            throw new JSONException("error parse false");
        }
        next();
        if (this.ch == ' ' || this.ch == ',' || this.ch == '}' || this.ch == ']' || this.ch == '\n' || this.ch == '\r' || this.ch == '\t' || this.ch == '\u001a' || this.ch == '\f' || this.ch == '\b' || this.ch == ':' || this.ch == '/') {
            this.token = 7;
            return;
        }
        throw new JSONException("scan false error");
    }

    public final void scanIdent() {
        this.np = this.bp - 1;
        this.hasSpecial = false;
        do {
            this.sp++;
            next();
        } while (Character.isLetterOrDigit(this.ch));
        String ident = stringVal();
        if ("null".equalsIgnoreCase(ident)) {
            this.token = 8;
        } else if ("new".equals(ident)) {
            this.token = 9;
        } else if ("true".equals(ident)) {
            this.token = 6;
        } else if ("false".equals(ident)) {
            this.token = 7;
        } else if ("undefined".equals(ident)) {
            this.token = 23;
        } else if ("Set".equals(ident)) {
            this.token = 21;
        } else if ("TreeSet".equals(ident)) {
            this.token = 22;
        } else {
            this.token = 18;
        }
    }

    public static String readString(char[] chars, int chars_len) {
        char[] sbuf = new char[chars_len];
        int i = 0;
        int len = 0;
        while (i < chars_len) {
            int len2;
            char ch = chars[i];
            if (ch != '\\') {
                len2 = len + 1;
                sbuf[len] = ch;
            } else {
                i++;
                switch (chars[i]) {
                    case '\"':
                        len2 = len + 1;
                        sbuf[len] = '\"';
                        break;
                    case '\'':
                        len2 = len + 1;
                        sbuf[len] = '\'';
                        break;
                    case '/':
                        len2 = len + 1;
                        sbuf[len] = '/';
                        break;
                    case '0':
                        len2 = len + 1;
                        sbuf[len] = '\u0000';
                        break;
                    case '1':
                        len2 = len + 1;
                        sbuf[len] = '\u0001';
                        break;
                    case '2':
                        len2 = len + 1;
                        sbuf[len] = '\u0002';
                        break;
                    case '3':
                        len2 = len + 1;
                        sbuf[len] = '\u0003';
                        break;
                    case '4':
                        len2 = len + 1;
                        sbuf[len] = '\u0004';
                        break;
                    case '5':
                        len2 = len + 1;
                        sbuf[len] = '\u0005';
                        break;
                    case '6':
                        len2 = len + 1;
                        sbuf[len] = '\u0006';
                        break;
                    case '7':
                        len2 = len + 1;
                        sbuf[len] = '\u0007';
                        break;
                    case 'F':
                    case 'f':
                        len2 = len + 1;
                        sbuf[len] = '\f';
                        break;
                    case '\\':
                        len2 = len + 1;
                        sbuf[len] = '\\';
                        break;
                    case 'b':
                        len2 = len + 1;
                        sbuf[len] = '\b';
                        break;
                    case 'n':
                        len2 = len + 1;
                        sbuf[len] = '\n';
                        break;
                    case 'r':
                        len2 = len + 1;
                        sbuf[len] = '\r';
                        break;
                    case 't':
                        len2 = len + 1;
                        sbuf[len] = '\t';
                        break;
                    case 'u':
                        len2 = len + 1;
                        r6 = new char[4];
                        i++;
                        r6[0] = chars[i];
                        i++;
                        r6[1] = chars[i];
                        i++;
                        r6[2] = chars[i];
                        i++;
                        r6[3] = chars[i];
                        sbuf[len] = (char) Integer.parseInt(new String(r6), 16);
                        break;
                    case 'v':
                        len2 = len + 1;
                        sbuf[len] = '\u000b';
                        break;
                    case 'x':
                        len2 = len + 1;
                        i++;
                        i++;
                        sbuf[len] = (char) ((digits[chars[i]] * 16) + digits[chars[i]]);
                        break;
                    default:
                        throw new JSONException("unclosed.str.lit");
                }
            }
            i++;
            len = len2;
        }
        return new String(sbuf, 0, len);
    }

    public boolean isBlankInput() {
        int i = 0;
        while (true) {
            char chLocal = charAt(i);
            if (chLocal == '\u001a') {
                this.token = 20;
                return true;
            } else if (!isWhitespace(chLocal)) {
                return false;
            } else {
                i++;
            }
        }
    }

    public final void skipWhitespace() {
        while (this.ch <= '/') {
            if (this.ch == ' ' || this.ch == '\r' || this.ch == '\n' || this.ch == '\t' || this.ch == '\f' || this.ch == '\b') {
                next();
            } else if (this.ch == '/') {
                skipComment();
            } else {
                return;
            }
        }
    }

    private void scanStringSingleQuote() {
        this.np = this.bp;
        this.hasSpecial = false;
        while (true) {
            char chLocal = next();
            if (chLocal == '\'') {
                this.token = 4;
                next();
                return;
            } else if (chLocal == '\u001a') {
                if (isEOF()) {
                    throw new JSONException("unclosed single-quote string");
                }
                putChar('\u001a');
            } else if (chLocal == '\\') {
                if (!this.hasSpecial) {
                    this.hasSpecial = true;
                    if (this.sp > this.sbuf.length) {
                        char[] newsbuf = new char[(this.sp * 2)];
                        System.arraycopy(this.sbuf, 0, newsbuf, 0, this.sbuf.length);
                        this.sbuf = newsbuf;
                    }
                    copyTo(this.np + 1, this.sp, this.sbuf);
                }
                chLocal = next();
                switch (chLocal) {
                    case '\"':
                        putChar('\"');
                        break;
                    case '\'':
                        putChar('\'');
                        break;
                    case '/':
                        putChar('/');
                        break;
                    case '0':
                        putChar('\u0000');
                        break;
                    case '1':
                        putChar('\u0001');
                        break;
                    case '2':
                        putChar('\u0002');
                        break;
                    case '3':
                        putChar('\u0003');
                        break;
                    case '4':
                        putChar('\u0004');
                        break;
                    case '5':
                        putChar('\u0005');
                        break;
                    case '6':
                        putChar('\u0006');
                        break;
                    case '7':
                        putChar('\u0007');
                        break;
                    case 'F':
                    case 'f':
                        putChar('\f');
                        break;
                    case '\\':
                        putChar('\\');
                        break;
                    case 'b':
                        putChar('\b');
                        break;
                    case 'n':
                        putChar('\n');
                        break;
                    case 'r':
                        putChar('\r');
                        break;
                    case 't':
                        putChar('\t');
                        break;
                    case 'u':
                        putChar((char) Integer.parseInt(new String(new char[]{next(), next(), next(), next()}), 16));
                        break;
                    case 'v':
                        putChar('\u000b');
                        break;
                    case 'x':
                        putChar((char) ((digits[next()] * 16) + digits[next()]));
                        break;
                    default:
                        this.ch = chLocal;
                        throw new JSONException("unclosed single-quote string");
                }
            } else if (!this.hasSpecial) {
                this.sp++;
            } else if (this.sp == this.sbuf.length) {
                putChar(chLocal);
            } else {
                char[] cArr = this.sbuf;
                int i = this.sp;
                this.sp = i + 1;
                cArr[i] = chLocal;
            }
        }
    }

    protected final void putChar(char ch) {
        if (this.sp == this.sbuf.length) {
            char[] newsbuf = new char[(this.sbuf.length * 2)];
            System.arraycopy(this.sbuf, 0, newsbuf, 0, this.sbuf.length);
            this.sbuf = newsbuf;
        }
        char[] cArr = this.sbuf;
        int i = this.sp;
        this.sp = i + 1;
        cArr[i] = ch;
    }

    public final void scanHex() {
        if (this.ch != 'x') {
            throw new JSONException("illegal state. " + this.ch);
        }
        next();
        if (this.ch != '\'') {
            throw new JSONException("illegal state. " + this.ch);
        }
        this.np = this.bp;
        next();
        int i = 0;
        while (true) {
            char ch = next();
            if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F')) {
                this.sp++;
                i++;
            } else if (ch != '\'') {
                this.sp++;
                next();
                this.token = 26;
            } else {
                throw new JSONException("illegal state. " + ch);
            }
        }
        if (ch != '\'') {
            throw new JSONException("illegal state. " + ch);
        }
        this.sp++;
        next();
        this.token = 26;
    }

    public final void scanNumber() {
        this.np = this.bp;
        if (this.ch == '-') {
            this.sp++;
            next();
        }
        while (this.ch >= '0' && this.ch <= '9') {
            this.sp++;
            next();
        }
        boolean isDouble = false;
        if (this.ch == '.') {
            this.sp++;
            next();
            isDouble = true;
            while (this.ch >= '0' && this.ch <= '9') {
                this.sp++;
                next();
            }
        }
        if (this.ch == 'L') {
            this.sp++;
            next();
        } else if (this.ch == 'S') {
            this.sp++;
            next();
        } else if (this.ch == 'B') {
            this.sp++;
            next();
        } else if (this.ch == 'F') {
            this.sp++;
            next();
            isDouble = true;
        } else if (this.ch == 'D') {
            this.sp++;
            next();
            isDouble = true;
        } else if (this.ch == 'e' || this.ch == 'E') {
            this.sp++;
            next();
            if (this.ch == '+' || this.ch == '-') {
                this.sp++;
                next();
            }
            while (this.ch >= '0' && this.ch <= '9') {
                this.sp++;
                next();
            }
            if (this.ch == 'D' || this.ch == 'F') {
                this.sp++;
                next();
            }
            isDouble = true;
        }
        if (isDouble) {
            this.token = 3;
        } else {
            this.token = 2;
        }
    }

    public final long longValue() throws NumberFormatException {
        long limit;
        int i;
        long result = 0;
        boolean negative = false;
        if (this.np == -1) {
            this.np = 0;
        }
        int i2 = this.np;
        int max = this.np + this.sp;
        if (charAt(this.np) == '-') {
            negative = true;
            limit = Long.MIN_VALUE;
            i = i2 + 1;
        } else {
            limit = -9223372036854775807L;
            i = i2;
        }
        if (i < max) {
            result = (long) (-(charAt(i) - 48));
            i++;
        }
        while (i < max) {
            i2 = i + 1;
            char chLocal = charAt(i);
            if (chLocal == 'L' || chLocal == 'S' || chLocal == 'B') {
                break;
            }
            int digit = chLocal - 48;
            if (result < -922337203685477580L) {
                throw new NumberFormatException(numberString());
            }
            result *= 10;
            if (result < ((long) digit) + limit) {
                throw new NumberFormatException(numberString());
            }
            result -= (long) digit;
            i = i2;
        }
        i2 = i;
        if (!negative) {
            return -result;
        }
        if (i2 > this.np + 1) {
            return result;
        }
        throw new NumberFormatException(numberString());
    }

    public final Number decimalValue(boolean decimal) {
        char chLocal = charAt((this.np + this.sp) - 1);
        if (chLocal == 'F') {
            try {
                return Float.valueOf(Float.parseFloat(numberString()));
            } catch (NumberFormatException ex) {
                throw new JSONException(ex.getMessage() + ", " + info());
            }
        } else if (chLocal == 'D') {
            return Double.valueOf(Double.parseDouble(numberString()));
        } else {
            if (decimal) {
                return decimalValue();
            }
            return Double.valueOf(doubleValue());
        }
    }

    public static boolean isWhitespace(char ch) {
        return ch <= ' ' && (ch == ' ' || ch == '\n' || ch == '\r' || ch == '\t' || ch == '\f' || ch == '\b');
    }
}
