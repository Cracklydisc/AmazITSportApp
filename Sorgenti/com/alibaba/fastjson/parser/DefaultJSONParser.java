package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.alibaba.fastjson.JSONPathException;
import com.alibaba.fastjson.parser.deserializer.ExtraProcessable;
import com.alibaba.fastjson.parser.deserializer.ExtraProcessor;
import com.alibaba.fastjson.parser.deserializer.ExtraTypeProvider;
import com.alibaba.fastjson.parser.deserializer.FieldDeserializer;
import com.alibaba.fastjson.parser.deserializer.FieldTypeResolver;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.parser.deserializer.ResolveFieldDeserializer;
import com.alibaba.fastjson.serializer.IntegerCodec;
import com.alibaba.fastjson.serializer.LongCodec;
import com.alibaba.fastjson.serializer.StringCodec;
import java.io.Closeable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class DefaultJSONParser implements Closeable {
    private static final Set<Class<?>> primitiveClasses = new HashSet();
    private String[] autoTypeAccept;
    protected ParserConfig config;
    protected ParseContext context;
    private ParseContext[] contextArray;
    private int contextArrayIndex;
    private DateFormat dateFormat;
    private String dateFormatPattern;
    private List<ExtraProcessor> extraProcessors;
    private List<ExtraTypeProvider> extraTypeProviders;
    protected FieldTypeResolver fieldTypeResolver;
    public final Object input;
    public final JSONLexer lexer;
    public int resolveStatus;
    private List<ResolveTask> resolveTaskList;
    public final SymbolTable symbolTable;

    public static class ResolveTask {
        public final ParseContext context;
        public FieldDeserializer fieldDeserializer;
        public ParseContext ownerContext;
        public final String referenceValue;

        public ResolveTask(ParseContext context, String referenceValue) {
            this.context = context;
            this.referenceValue = referenceValue;
        }
    }

    static {
        int i = 0;
        Class<?>[] classes = new Class[]{Boolean.TYPE, Byte.TYPE, Short.TYPE, Integer.TYPE, Long.TYPE, Float.TYPE, Double.TYPE, Boolean.class, Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class, BigInteger.class, BigDecimal.class, String.class};
        int length = classes.length;
        while (i < length) {
            primitiveClasses.add(classes[i]);
            i++;
        }
    }

    public String getDateFomartPattern() {
        return this.dateFormatPattern;
    }

    public DateFormat getDateFormat() {
        if (this.dateFormat == null) {
            this.dateFormat = new SimpleDateFormat(this.dateFormatPattern, this.lexer.getLocale());
            this.dateFormat.setTimeZone(this.lexer.getTimeZone());
        }
        return this.dateFormat;
    }

    public DefaultJSONParser(String input, ParserConfig config, int features) {
        this((Object) input, new JSONScanner(input, features), config);
    }

    public DefaultJSONParser(Object input, JSONLexer lexer, ParserConfig config) {
        this.dateFormatPattern = JSON.DEFFAULT_DATE_FORMAT;
        this.contextArrayIndex = 0;
        this.resolveStatus = 0;
        this.extraTypeProviders = null;
        this.extraProcessors = null;
        this.fieldTypeResolver = null;
        this.autoTypeAccept = null;
        this.lexer = lexer;
        this.input = input;
        this.config = config;
        this.symbolTable = config.symbolTable;
        int ch = lexer.getCurrent();
        if (ch == 123) {
            lexer.next();
            ((JSONLexerBase) lexer).token = 12;
        } else if (ch == 91) {
            lexer.next();
            ((JSONLexerBase) lexer).token = 14;
        } else {
            lexer.nextToken();
        }
    }

    public SymbolTable getSymbolTable() {
        return this.symbolTable;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object parseObject(java.util.Map r43, java.lang.Object r44) {
        /*
        r42 = this;
        r0 = r42;
        r0 = r0.lexer;
        r21 = r0;
        r39 = r21.token();
        r40 = 8;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x0018;
    L_0x0012:
        r21.nextToken();
        r43 = 0;
    L_0x0017:
        return r43;
    L_0x0018:
        r39 = r21.token();
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x0028;
    L_0x0024:
        r21.nextToken();
        goto L_0x0017;
    L_0x0028:
        r39 = r21.token();
        r40 = 12;
        r0 = r39;
        r1 = r40;
        if (r0 == r1) goto L_0x006b;
    L_0x0034:
        r39 = r21.token();
        r40 = 16;
        r0 = r39;
        r1 = r40;
        if (r0 == r1) goto L_0x006b;
    L_0x0040:
        r39 = new com.alibaba.fastjson.JSONException;
        r40 = new java.lang.StringBuilder;
        r40.<init>();
        r41 = "syntax error, expect {, actual ";
        r40 = r40.append(r41);
        r41 = r21.tokenName();
        r40 = r40.append(r41);
        r41 = ", ";
        r40 = r40.append(r41);
        r41 = r21.info();
        r40 = r40.append(r41);
        r40 = r40.toString();
        r39.<init>(r40);
        throw r39;
    L_0x006b:
        r0 = r42;
        r5 = r0.context;
        r0 = r43;
        r0 = r0 instanceof com.alibaba.fastjson.JSONObject;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x00a7;
    L_0x0077:
        r0 = r43;
        r0 = (com.alibaba.fastjson.JSONObject) r0;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r23 = r39.getInnerMap();	 Catch:{ all -> 0x00fc }
    L_0x0081:
        r33 = 0;
    L_0x0083:
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x00aa;
    L_0x0096:
        r39 = 44;
        r0 = r39;
        if (r3 != r0) goto L_0x00aa;
    L_0x009c:
        r21.next();	 Catch:{ all -> 0x00fc }
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        goto L_0x0096;
    L_0x00a7:
        r23 = r43;
        goto L_0x0081;
    L_0x00aa:
        r17 = 0;
        r39 = 34;
        r0 = r39;
        if (r3 != r0) goto L_0x0103;
    L_0x00b2:
        r0 = r42;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r40 = 34;
        r0 = r21;
        r1 = r39;
        r2 = r40;
        r20 = r0.scanSymbol(r1, r2);	 Catch:{ all -> 0x00fc }
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = 58;
        r0 = r39;
        if (r3 == r0) goto L_0x0254;
    L_0x00d1:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "expect ':' at ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.pos();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = ", name ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r0 = r40;
        r1 = r20;
        r40 = r0.append(r1);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x00fc:
        r39 = move-exception;
        r0 = r42;
        r0.setContext(r5);
        throw r39;
    L_0x0103:
        r39 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r39;
        if (r3 != r0) goto L_0x0155;
    L_0x0109:
        r21.next();	 Catch:{ all -> 0x00fc }
        r21.resetStringPosition();	 Catch:{ all -> 0x00fc }
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        if (r33 != 0) goto L_0x0144;
    L_0x0114:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x014b;
    L_0x011c:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.fieldName;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r44;
        r1 = r39;
        if (r0 != r1) goto L_0x014b;
    L_0x012e:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r43;
        r1 = r39;
        if (r0 != r1) goto L_0x014b;
    L_0x0140:
        r0 = r42;
        r5 = r0.context;	 Catch:{ all -> 0x00fc }
    L_0x0144:
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x014b:
        r6 = r42.setContext(r43, r44);	 Catch:{ all -> 0x00fc }
        if (r5 != 0) goto L_0x0152;
    L_0x0151:
        r5 = r6;
    L_0x0152:
        r33 = 1;
        goto L_0x0144;
    L_0x0155:
        r39 = 39;
        r0 = r39;
        if (r3 != r0) goto L_0x01ab;
    L_0x015b:
        r39 = com.alibaba.fastjson.parser.Feature.AllowSingleQuotes;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 != 0) goto L_0x016f;
    L_0x0167:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x016f:
        r0 = r42;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r40 = 39;
        r0 = r21;
        r1 = r39;
        r2 = r40;
        r20 = r0.scanSymbol(r1, r2);	 Catch:{ all -> 0x00fc }
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = 58;
        r0 = r39;
        if (r3 == r0) goto L_0x0254;
    L_0x018e:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "expect ':' at ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.pos();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x01ab:
        r39 = 26;
        r0 = r39;
        if (r3 != r0) goto L_0x01b9;
    L_0x01b1:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x01b9:
        r39 = 44;
        r0 = r39;
        if (r3 != r0) goto L_0x01c7;
    L_0x01bf:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x01c7:
        r39 = 48;
        r0 = r39;
        if (r3 < r0) goto L_0x01d3;
    L_0x01cd:
        r39 = 57;
        r0 = r39;
        if (r3 <= r0) goto L_0x01d9;
    L_0x01d3:
        r39 = 45;
        r0 = r39;
        if (r3 != r0) goto L_0x023f;
    L_0x01d9:
        r21.resetStringPosition();	 Catch:{ all -> 0x00fc }
        r21.scanNumber();	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ NumberFormatException -> 0x0221 }
        r40 = 2;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x0216;
    L_0x01eb:
        r20 = r21.integerValue();	 Catch:{ NumberFormatException -> 0x0221 }
    L_0x01ef:
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = 58;
        r0 = r39;
        if (r3 == r0) goto L_0x0254;
    L_0x01f9:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "parse number key error";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.info();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x0216:
        r39 = 1;
        r0 = r21;
        r1 = r39;
        r20 = r0.decimalValue(r1);	 Catch:{ NumberFormatException -> 0x0221 }
        goto L_0x01ef;
    L_0x0221:
        r9 = move-exception;
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "parse number key error";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.info();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x023f:
        r39 = 123; // 0x7b float:1.72E-43 double:6.1E-322;
        r0 = r39;
        if (r3 == r0) goto L_0x024b;
    L_0x0245:
        r39 = 91;
        r0 = r39;
        if (r3 != r0) goto L_0x02aa;
    L_0x024b:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r20 = r42.parse();	 Catch:{ all -> 0x00fc }
        r17 = 1;
    L_0x0254:
        if (r17 != 0) goto L_0x025c;
    L_0x0256:
        r21.next();	 Catch:{ all -> 0x00fc }
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
    L_0x025c:
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r21.resetStringPosition();	 Catch:{ all -> 0x00fc }
        r39 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;	 Catch:{ all -> 0x00fc }
        r0 = r20;
        r1 = r39;
        if (r0 != r1) goto L_0x0422;
    L_0x026b:
        r39 = com.alibaba.fastjson.parser.Feature.DisableSpecialKeyDetect;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 != 0) goto L_0x0422;
    L_0x0277:
        r0 = r42;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r40 = 34;
        r0 = r21;
        r1 = r39;
        r2 = r40;
        r37 = r0.scanSymbol(r1, r2);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0 = r0.config;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r40 = 0;
        r0 = r39;
        r1 = r37;
        r2 = r40;
        r4 = r0.checkAutoType(r1, r2);	 Catch:{ all -> 0x00fc }
        if (r4 != 0) goto L_0x0302;
    L_0x029d:
        r39 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;	 Catch:{ all -> 0x00fc }
        r0 = r23;
        r1 = r39;
        r2 = r37;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
        goto L_0x0083;
    L_0x02aa:
        r39 = com.alibaba.fastjson.parser.Feature.AllowUnQuotedFieldNames;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 != 0) goto L_0x02be;
    L_0x02b6:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x02be:
        r0 = r42;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r21;
        r1 = r39;
        r20 = r0.scanSymbolUnQuoted(r1);	 Catch:{ all -> 0x00fc }
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = 58;
        r0 = r39;
        if (r3 == r0) goto L_0x0254;
    L_0x02d9:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "expect ':' at ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.pos();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = ", actual ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r0 = r40;
        r40 = r0.append(r3);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x0302:
        r39 = 16;
        r0 = r21;
        r1 = r39;
        r0.nextToken(r1);	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x03b1;
    L_0x0317:
        r39 = 16;
        r0 = r21;
        r1 = r39;
        r0.nextToken(r1);	 Catch:{ all -> 0x00fc }
        r16 = 0;
        r0 = r42;
        r0 = r0.config;	 Catch:{ Exception -> 0x0378 }
        r39 = r0;
        r0 = r39;
        r8 = r0.getDeserializer(r4);	 Catch:{ Exception -> 0x0378 }
        r0 = r8 instanceof com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;	 Catch:{ Exception -> 0x0378 }
        r39 = r0;
        if (r39 == 0) goto L_0x0385;
    L_0x0334:
        r0 = r8;
        r0 = (com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer) r0;	 Catch:{ Exception -> 0x0378 }
        r19 = r0;
        r0 = r19;
        r1 = r42;
        r16 = r0.createInstance(r1, r4);	 Catch:{ Exception -> 0x0378 }
        r39 = r23.entrySet();	 Catch:{ Exception -> 0x0378 }
        r39 = r39.iterator();	 Catch:{ Exception -> 0x0378 }
    L_0x0349:
        r40 = r39.hasNext();	 Catch:{ Exception -> 0x0378 }
        if (r40 == 0) goto L_0x0385;
    L_0x034f:
        r25 = r39.next();	 Catch:{ Exception -> 0x0378 }
        r0 = r25;
        r0 = (java.util.Map.Entry) r0;	 Catch:{ Exception -> 0x0378 }
        r10 = r0;
        r11 = r10.getKey();	 Catch:{ Exception -> 0x0378 }
        r0 = r11 instanceof java.lang.String;	 Catch:{ Exception -> 0x0378 }
        r40 = r0;
        if (r40 == 0) goto L_0x0349;
    L_0x0362:
        r11 = (java.lang.String) r11;	 Catch:{ Exception -> 0x0378 }
        r0 = r19;
        r13 = r0.getFieldDeserializer(r11);	 Catch:{ Exception -> 0x0378 }
        if (r13 == 0) goto L_0x0349;
    L_0x036c:
        r40 = r10.getValue();	 Catch:{ Exception -> 0x0378 }
        r0 = r16;
        r1 = r40;
        r13.setValue(r0, r1);	 Catch:{ Exception -> 0x0378 }
        goto L_0x0349;
    L_0x0378:
        r9 = move-exception;
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "create instance error";
        r0 = r39;
        r1 = r40;
        r0.<init>(r1, r9);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x0385:
        if (r16 != 0) goto L_0x0392;
    L_0x0387:
        r39 = java.lang.Cloneable.class;
        r0 = r39;
        if (r4 != r0) goto L_0x039b;
    L_0x038d:
        r16 = new java.util.HashMap;	 Catch:{ Exception -> 0x0378 }
        r16.<init>();	 Catch:{ Exception -> 0x0378 }
    L_0x0392:
        r0 = r42;
        r0.setContext(r5);
        r43 = r16;
        goto L_0x0017;
    L_0x039b:
        r39 = "java.util.Collections$EmptyMap";
        r0 = r39;
        r1 = r37;
        r39 = r0.equals(r1);	 Catch:{ Exception -> 0x0378 }
        if (r39 == 0) goto L_0x03ac;
    L_0x03a7:
        r16 = java.util.Collections.emptyMap();	 Catch:{ Exception -> 0x0378 }
        goto L_0x0392;
    L_0x03ac:
        r16 = r4.newInstance();	 Catch:{ Exception -> 0x0378 }
        goto L_0x0392;
    L_0x03b1:
        r39 = 2;
        r0 = r42;
        r1 = r39;
        r0.setResolveStatus(r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x03e3;
    L_0x03c2:
        if (r44 == 0) goto L_0x03e3;
    L_0x03c4:
        r0 = r44;
        r0 = r0 instanceof java.lang.Integer;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 != 0) goto L_0x03e3;
    L_0x03cc:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.fieldName;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0 instanceof java.lang.Integer;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 != 0) goto L_0x03e3;
    L_0x03e0:
        r42.popContext();	 Catch:{ all -> 0x00fc }
    L_0x03e3:
        r39 = r43.size();	 Catch:{ all -> 0x00fc }
        if (r39 <= 0) goto L_0x0407;
    L_0x03e9:
        r0 = r42;
        r0 = r0.config;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r43;
        r1 = r39;
        r24 = com.alibaba.fastjson.util.TypeUtils.cast(r0, r4, r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r24;
        r0.parseObject(r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        r43 = r24;
        goto L_0x0017;
    L_0x0407:
        r0 = r42;
        r0 = r0.config;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r8 = r0.getDeserializer(r4);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r44;
        r43 = r8.deserialze(r0, r4, r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x0422:
        r39 = "$ref";
        r0 = r20;
        r1 = r39;
        if (r0 != r1) goto L_0x0587;
    L_0x042a:
        if (r5 == 0) goto L_0x0587;
    L_0x042c:
        r39 = com.alibaba.fastjson.parser.Feature.DisableSpecialKeyDetect;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 != 0) goto L_0x0587;
    L_0x0438:
        r39 = 4;
        r0 = r21;
        r1 = r39;
        r0.nextToken(r1);	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 4;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x0566;
    L_0x044d:
        r29 = r21.stringVal();	 Catch:{ all -> 0x00fc }
        r39 = 13;
        r0 = r21;
        r1 = r39;
        r0.nextToken(r1);	 Catch:{ all -> 0x00fc }
        r30 = 0;
        r39 = "@";
        r0 = r39;
        r1 = r29;
        r39 = r0.equals(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x04b9;
    L_0x0468:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x0552;
    L_0x0470:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r35 = r0;
        r0 = r35;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r36 = r0;
        r0 = r36;
        r0 = r0 instanceof java.lang.Object[];	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 != 0) goto L_0x048c;
    L_0x0484:
        r0 = r36;
        r0 = r0 instanceof java.util.Collection;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x04a4;
    L_0x048c:
        r30 = r36;
    L_0x048e:
        r43 = r30;
    L_0x0490:
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 == r1) goto L_0x0556;
    L_0x049c:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x04a4:
        r0 = r35;
        r0 = r0.parent;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x048e;
    L_0x04ac:
        r0 = r35;
        r0 = r0.parent;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r30 = r0;
        goto L_0x048e;
    L_0x04b9:
        r39 = "..";
        r0 = r39;
        r1 = r29;
        r39 = r0.equals(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x04ee;
    L_0x04c5:
        r0 = r5.object;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x04d2;
    L_0x04cb:
        r0 = r5.object;	 Catch:{ all -> 0x00fc }
        r30 = r0;
        r43 = r30;
        goto L_0x0490;
    L_0x04d2:
        r39 = new com.alibaba.fastjson.parser.DefaultJSONParser$ResolveTask;	 Catch:{ all -> 0x00fc }
        r0 = r39;
        r1 = r29;
        r0.<init>(r5, r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r39;
        r0.addResolveTask(r1);	 Catch:{ all -> 0x00fc }
        r39 = 1;
        r0 = r42;
        r1 = r39;
        r0.setResolveStatus(r1);	 Catch:{ all -> 0x00fc }
        r43 = r30;
        goto L_0x0490;
    L_0x04ee:
        r39 = "$";
        r0 = r39;
        r1 = r29;
        r39 = r0.equals(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x0539;
    L_0x04fa:
        r32 = r5;
    L_0x04fc:
        r0 = r32;
        r0 = r0.parent;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x050b;
    L_0x0504:
        r0 = r32;
        r0 = r0.parent;	 Catch:{ all -> 0x00fc }
        r32 = r0;
        goto L_0x04fc;
    L_0x050b:
        r0 = r32;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x051d;
    L_0x0513:
        r0 = r32;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r30 = r0;
    L_0x0519:
        r43 = r30;
        goto L_0x0490;
    L_0x051d:
        r39 = new com.alibaba.fastjson.parser.DefaultJSONParser$ResolveTask;	 Catch:{ all -> 0x00fc }
        r0 = r39;
        r1 = r32;
        r2 = r29;
        r0.<init>(r1, r2);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r39;
        r0.addResolveTask(r1);	 Catch:{ all -> 0x00fc }
        r39 = 1;
        r0 = r42;
        r1 = r39;
        r0.setResolveStatus(r1);	 Catch:{ all -> 0x00fc }
        goto L_0x0519;
    L_0x0539:
        r39 = new com.alibaba.fastjson.parser.DefaultJSONParser$ResolveTask;	 Catch:{ all -> 0x00fc }
        r0 = r39;
        r1 = r29;
        r0.<init>(r5, r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r39;
        r0.addResolveTask(r1);	 Catch:{ all -> 0x00fc }
        r39 = 1;
        r0 = r42;
        r1 = r39;
        r0.setResolveStatus(r1);	 Catch:{ all -> 0x00fc }
    L_0x0552:
        r43 = r30;
        goto L_0x0490;
    L_0x0556:
        r39 = 16;
        r0 = r21;
        r1 = r39;
        r0.nextToken(r1);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x0566:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "illegal ref, ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.token();	 Catch:{ all -> 0x00fc }
        r41 = com.alibaba.fastjson.parser.JSONToken.name(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x0587:
        if (r33 != 0) goto L_0x05b9;
    L_0x0589:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x0619;
    L_0x0591:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.fieldName;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r44;
        r1 = r39;
        if (r0 != r1) goto L_0x0619;
    L_0x05a3:
        r0 = r42;
        r0 = r0.context;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r0 = r0.object;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r43;
        r1 = r39;
        if (r0 != r1) goto L_0x0619;
    L_0x05b5:
        r0 = r42;
        r5 = r0.context;	 Catch:{ all -> 0x00fc }
    L_0x05b9:
        r39 = r43.getClass();	 Catch:{ all -> 0x00fc }
        r40 = com.alibaba.fastjson.JSONObject.class;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x05c9;
    L_0x05c5:
        if (r20 != 0) goto L_0x05c9;
    L_0x05c7:
        r20 = "null";
    L_0x05c9:
        r39 = 34;
        r0 = r39;
        if (r3 != r0) goto L_0x0623;
    L_0x05cf:
        r21.scanString();	 Catch:{ all -> 0x00fc }
        r34 = r21.stringVal();	 Catch:{ all -> 0x00fc }
        r38 = r34;
        r39 = com.alibaba.fastjson.parser.Feature.AllowISO8601DateFormat;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x05fe;
    L_0x05e4:
        r18 = new com.alibaba.fastjson.parser.JSONScanner;	 Catch:{ all -> 0x00fc }
        r0 = r18;
        r1 = r34;
        r0.<init>(r1);	 Catch:{ all -> 0x00fc }
        r39 = r18.scanISO8601DateIfMatch();	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x05fb;
    L_0x05f3:
        r39 = r18.getCalendar();	 Catch:{ all -> 0x00fc }
        r38 = r39.getTime();	 Catch:{ all -> 0x00fc }
    L_0x05fb:
        r18.close();	 Catch:{ all -> 0x00fc }
    L_0x05fe:
        r0 = r23;
        r1 = r20;
        r2 = r38;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
    L_0x0607:
        r21.skipWhitespace();	 Catch:{ all -> 0x00fc }
        r3 = r21.getCurrent();	 Catch:{ all -> 0x00fc }
        r39 = 44;
        r0 = r39;
        if (r3 != r0) goto L_0x0836;
    L_0x0614:
        r21.next();	 Catch:{ all -> 0x00fc }
        goto L_0x0083;
    L_0x0619:
        r6 = r42.setContext(r43, r44);	 Catch:{ all -> 0x00fc }
        if (r5 != 0) goto L_0x0620;
    L_0x061f:
        r5 = r6;
    L_0x0620:
        r33 = 1;
        goto L_0x05b9;
    L_0x0623:
        r39 = 48;
        r0 = r39;
        if (r3 < r0) goto L_0x062f;
    L_0x0629:
        r39 = 57;
        r0 = r39;
        if (r3 <= r0) goto L_0x0635;
    L_0x062f:
        r39 = 45;
        r0 = r39;
        if (r3 != r0) goto L_0x0665;
    L_0x0635:
        r21.scanNumber();	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 2;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x0652;
    L_0x0644:
        r38 = r21.integerValue();	 Catch:{ all -> 0x00fc }
    L_0x0648:
        r0 = r23;
        r1 = r20;
        r2 = r38;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
        goto L_0x0607;
    L_0x0652:
        r39 = com.alibaba.fastjson.parser.Feature.UseBigDecimal;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r38 = r0.decimalValue(r1);	 Catch:{ all -> 0x00fc }
        goto L_0x0648;
    L_0x0665:
        r39 = 91;
        r0 = r39;
        if (r3 != r0) goto L_0x06dc;
    L_0x066b:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r22 = new com.alibaba.fastjson.JSONArray;	 Catch:{ all -> 0x00fc }
        r22.<init>();	 Catch:{ all -> 0x00fc }
        if (r44 == 0) goto L_0x06c2;
    L_0x0675:
        r39 = r44.getClass();	 Catch:{ all -> 0x00fc }
        r40 = java.lang.Integer.class;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x06c2;
    L_0x0681:
        r28 = 1;
    L_0x0683:
        if (r44 != 0) goto L_0x068a;
    L_0x0685:
        r0 = r42;
        r0.setContext(r5);	 Catch:{ all -> 0x00fc }
    L_0x068a:
        r0 = r42;
        r1 = r22;
        r2 = r20;
        r0.parseArray(r1, r2);	 Catch:{ all -> 0x00fc }
        r39 = com.alibaba.fastjson.parser.Feature.UseObjectArray;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        if (r39 == 0) goto L_0x06c5;
    L_0x069f:
        r38 = r22.toArray();	 Catch:{ all -> 0x00fc }
    L_0x06a3:
        r0 = r23;
        r1 = r20;
        r2 = r38;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x06c8;
    L_0x06b8:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x06c2:
        r28 = 0;
        goto L_0x0683;
    L_0x06c5:
        r38 = r22;
        goto L_0x06a3;
    L_0x06c8:
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 16;
        r0 = r39;
        r1 = r40;
        if (r0 == r1) goto L_0x0083;
    L_0x06d4:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = "syntax error";
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x06dc:
        r39 = 123; // 0x7b float:1.72E-43 double:6.1E-322;
        r0 = r39;
        if (r3 != r0) goto L_0x07d9;
    L_0x06e2:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        if (r44 == 0) goto L_0x079b;
    L_0x06e7:
        r39 = r44.getClass();	 Catch:{ all -> 0x00fc }
        r40 = java.lang.Integer.class;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x079b;
    L_0x06f3:
        r28 = 1;
    L_0x06f5:
        r15 = new com.alibaba.fastjson.JSONObject;	 Catch:{ all -> 0x00fc }
        r39 = com.alibaba.fastjson.parser.Feature.OrderedField;	 Catch:{ all -> 0x00fc }
        r0 = r21;
        r1 = r39;
        r39 = r0.isEnabled(r1);	 Catch:{ all -> 0x00fc }
        r0 = r39;
        r15.<init>(r0);	 Catch:{ all -> 0x00fc }
        r7 = 0;
        if (r28 != 0) goto L_0x0711;
    L_0x0709:
        r0 = r42;
        r1 = r20;
        r7 = r0.setContext(r5, r15, r1);	 Catch:{ all -> 0x00fc }
    L_0x0711:
        r26 = 0;
        r27 = 0;
        r0 = r42;
        r0 = r0.fieldTypeResolver;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        if (r39 == 0) goto L_0x074b;
    L_0x071d:
        if (r20 == 0) goto L_0x079f;
    L_0x071f:
        r31 = r20.toString();	 Catch:{ all -> 0x00fc }
    L_0x0723:
        r0 = r42;
        r0 = r0.fieldTypeResolver;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r1 = r43;
        r2 = r31;
        r14 = r0.resolve(r1, r2);	 Catch:{ all -> 0x00fc }
        if (r14 == 0) goto L_0x074b;
    L_0x0735:
        r0 = r42;
        r0 = r0.config;	 Catch:{ all -> 0x00fc }
        r39 = r0;
        r0 = r39;
        r12 = r0.getDeserializer(r14);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r20;
        r26 = r12.deserialze(r0, r14, r1);	 Catch:{ all -> 0x00fc }
        r27 = 1;
    L_0x074b:
        if (r27 != 0) goto L_0x0755;
    L_0x074d:
        r0 = r42;
        r1 = r20;
        r26 = r0.parseObject(r15, r1);	 Catch:{ all -> 0x00fc }
    L_0x0755:
        if (r7 == 0) goto L_0x075f;
    L_0x0757:
        r0 = r26;
        if (r15 == r0) goto L_0x075f;
    L_0x075b:
        r0 = r43;
        r7.object = r0;	 Catch:{ all -> 0x00fc }
    L_0x075f:
        r39 = r20.toString();	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r43;
        r2 = r39;
        r0.checkMapResolve(r1, r2);	 Catch:{ all -> 0x00fc }
        r0 = r23;
        r1 = r20;
        r2 = r26;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
        if (r28 == 0) goto L_0x0780;
    L_0x0777:
        r0 = r42;
        r1 = r26;
        r2 = r20;
        r0.setContext(r1, r2);	 Catch:{ all -> 0x00fc }
    L_0x0780:
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x07a2;
    L_0x078c:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x079b:
        r28 = 0;
        goto L_0x06f5;
    L_0x079f:
        r31 = 0;
        goto L_0x0723;
    L_0x07a2:
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 16;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x07bc;
    L_0x07ae:
        if (r28 == 0) goto L_0x07b5;
    L_0x07b0:
        r42.popContext();	 Catch:{ all -> 0x00fc }
        goto L_0x0083;
    L_0x07b5:
        r0 = r42;
        r0.setContext(r5);	 Catch:{ all -> 0x00fc }
        goto L_0x0083;
    L_0x07bc:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "syntax error, ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.tokenName();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x07d9:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r38 = r42.parse();	 Catch:{ all -> 0x00fc }
        r0 = r23;
        r1 = r20;
        r2 = r38;
        r0.put(r1, r2);	 Catch:{ all -> 0x00fc }
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 13;
        r0 = r39;
        r1 = r40;
        if (r0 != r1) goto L_0x07ff;
    L_0x07f5:
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x07ff:
        r39 = r21.token();	 Catch:{ all -> 0x00fc }
        r40 = 16;
        r0 = r39;
        r1 = r40;
        if (r0 == r1) goto L_0x0083;
    L_0x080b:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "syntax error, position at ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.pos();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = ", name ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r0 = r40;
        r1 = r20;
        r40 = r0.append(r1);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
    L_0x0836:
        r39 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r39;
        if (r3 != r0) goto L_0x0855;
    L_0x083c:
        r21.next();	 Catch:{ all -> 0x00fc }
        r21.resetStringPosition();	 Catch:{ all -> 0x00fc }
        r21.nextToken();	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r1 = r38;
        r2 = r20;
        r0.setContext(r1, r2);	 Catch:{ all -> 0x00fc }
        r0 = r42;
        r0.setContext(r5);
        goto L_0x0017;
    L_0x0855:
        r39 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x00fc }
        r40 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00fc }
        r40.<init>();	 Catch:{ all -> 0x00fc }
        r41 = "syntax error, position at ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = r21.pos();	 Catch:{ all -> 0x00fc }
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r41 = ", name ";
        r40 = r40.append(r41);	 Catch:{ all -> 0x00fc }
        r0 = r40;
        r1 = r20;
        r40 = r0.append(r1);	 Catch:{ all -> 0x00fc }
        r40 = r40.toString();	 Catch:{ all -> 0x00fc }
        r39.<init>(r40);	 Catch:{ all -> 0x00fc }
        throw r39;	 Catch:{ all -> 0x00fc }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.DefaultJSONParser.parseObject(java.util.Map, java.lang.Object):java.lang.Object");
    }

    public ParserConfig getConfig() {
        return this.config;
    }

    public <T> T parseObject(Class<T> clazz) {
        return parseObject((Type) clazz, null);
    }

    public <T> T parseObject(Type type) {
        return parseObject(type, null);
    }

    public <T> T parseObject(Type type, Object fieldName) {
        int token = this.lexer.token();
        if (token == 8) {
            this.lexer.nextToken();
            return null;
        }
        if (token == 4) {
            if (type == byte[].class) {
                T bytes = this.lexer.bytesValue();
                this.lexer.nextToken();
                return bytes;
            } else if (type == char[].class) {
                String strVal = this.lexer.stringVal();
                this.lexer.nextToken();
                return strVal.toCharArray();
            }
        }
        try {
            return this.config.getDeserializer(type).deserialze(this, type, fieldName);
        } catch (JSONException e) {
            throw e;
        } catch (Throwable e2) {
            JSONException jSONException = new JSONException(e2.getMessage(), e2);
        }
    }

    public void parseArray(Type type, Collection array) {
        parseArray(type, array, null);
    }

    public void parseArray(Type type, Collection array, Object fieldName) {
        int token = this.lexer.token();
        if (token == 21 || token == 22) {
            this.lexer.nextToken();
            token = this.lexer.token();
        }
        if (token != 14) {
            throw new JSONException("exepct '[', but " + JSONToken.name(token) + ", " + this.lexer.info());
        }
        ObjectDeserializer deserializer;
        if (Integer.TYPE == type) {
            deserializer = IntegerCodec.instance;
            this.lexer.nextToken(2);
        } else if (String.class == type) {
            deserializer = StringCodec.instance;
            this.lexer.nextToken(4);
        } else {
            deserializer = this.config.getDeserializer(type);
            this.lexer.nextToken(deserializer.getFastMatchToken());
        }
        ParseContext context = this.context;
        setContext(array, fieldName);
        int i = 0;
        while (true) {
            try {
                if (this.lexer.isEnabled(Feature.AllowArbitraryCommas)) {
                    while (this.lexer.token() == 16) {
                        this.lexer.nextToken();
                    }
                }
                if (this.lexer.token() == 15) {
                    break;
                }
                if (Integer.TYPE == type) {
                    array.add(IntegerCodec.instance.deserialze(this, null, null));
                } else if (String.class == type) {
                    String value;
                    if (this.lexer.token() == 4) {
                        value = this.lexer.stringVal();
                        this.lexer.nextToken(16);
                    } else {
                        Object obj = parse();
                        if (obj == null) {
                            value = null;
                        } else {
                            value = obj.toString();
                        }
                    }
                    array.add(value);
                } else {
                    Object obj2;
                    if (this.lexer.token() == 8) {
                        this.lexer.nextToken();
                        obj2 = null;
                    } else {
                        obj2 = deserializer.deserialze(this, type, Integer.valueOf(i));
                    }
                    array.add(obj2);
                    checkListResolve(array);
                }
                if (this.lexer.token() == 16) {
                    this.lexer.nextToken(deserializer.getFastMatchToken());
                }
                i++;
            } finally {
                setContext(context);
            }
        }
        this.lexer.nextToken(16);
    }

    public void parseObject(Object object) {
        Class<?> clazz = object.getClass();
        JavaBeanDeserializer beanDeser = null;
        ObjectDeserializer deserizer = this.config.getDeserializer(clazz);
        if (deserizer instanceof JavaBeanDeserializer) {
            beanDeser = (JavaBeanDeserializer) deserizer;
        }
        if (this.lexer.token() == 12 || this.lexer.token() == 16) {
            while (true) {
                String key = this.lexer.scanSymbol(this.symbolTable);
                if (key == null) {
                    if (this.lexer.token() == 13) {
                        this.lexer.nextToken(16);
                        return;
                    } else if (this.lexer.token() == 16 && this.lexer.isEnabled(Feature.AllowArbitraryCommas)) {
                    }
                }
                FieldDeserializer fieldDeser = null;
                if (beanDeser != null) {
                    fieldDeser = beanDeser.getFieldDeserializer(key);
                }
                if (fieldDeser != null) {
                    Object fieldValue;
                    Class<?> fieldClass = fieldDeser.fieldInfo.fieldClass;
                    Type fieldType = fieldDeser.fieldInfo.fieldType;
                    if (fieldClass == Integer.TYPE) {
                        this.lexer.nextTokenWithColon(2);
                        fieldValue = IntegerCodec.instance.deserialze(this, fieldType, null);
                    } else if (fieldClass == String.class) {
                        this.lexer.nextTokenWithColon(4);
                        fieldValue = StringCodec.deserialze(this);
                    } else if (fieldClass == Long.TYPE) {
                        this.lexer.nextTokenWithColon(2);
                        fieldValue = LongCodec.instance.deserialze(this, fieldType, null);
                    } else {
                        ObjectDeserializer fieldValueDeserializer = this.config.getDeserializer(fieldClass, fieldType);
                        this.lexer.nextTokenWithColon(fieldValueDeserializer.getFastMatchToken());
                        fieldValue = fieldValueDeserializer.deserialze(this, fieldType, null);
                    }
                    fieldDeser.setValue(object, fieldValue);
                    if (this.lexer.token() != 16 && this.lexer.token() == 13) {
                        this.lexer.nextToken(16);
                        return;
                    }
                } else if (this.lexer.isEnabled(Feature.IgnoreNotMatch)) {
                    this.lexer.nextTokenWithColon();
                    parse();
                    if (this.lexer.token() == 13) {
                        this.lexer.nextToken();
                        return;
                    }
                } else {
                    throw new JSONException("setter not found, class " + clazz.getName() + ", property " + key);
                }
            }
        }
        throw new JSONException("syntax error, expect {, actual " + this.lexer.tokenName());
    }

    public void acceptType(String typeName) {
        JSONLexer lexer = this.lexer;
        lexer.nextTokenWithColon();
        if (lexer.token() != 4) {
            throw new JSONException("type not match error");
        } else if (typeName.equals(lexer.stringVal())) {
            lexer.nextToken();
            if (lexer.token() == 16) {
                lexer.nextToken();
            }
        } else {
            throw new JSONException("type not match error");
        }
    }

    public int getResolveStatus() {
        return this.resolveStatus;
    }

    public void setResolveStatus(int resolveStatus) {
        this.resolveStatus = resolveStatus;
    }

    public Object getObject(String path) {
        for (int i = 0; i < this.contextArrayIndex; i++) {
            if (path.equals(this.contextArray[i].toString())) {
                return this.contextArray[i].object;
            }
        }
        return null;
    }

    public void checkListResolve(Collection array) {
        if (this.resolveStatus != 1) {
            return;
        }
        if (array instanceof List) {
            int index = array.size() - 1;
            List list = (List) array;
            ResolveTask task = getLastResolveTask();
            task.fieldDeserializer = new ResolveFieldDeserializer(this, list, index);
            task.ownerContext = this.context;
            setResolveStatus(0);
            return;
        }
        task = getLastResolveTask();
        task.fieldDeserializer = new ResolveFieldDeserializer(array);
        task.ownerContext = this.context;
        setResolveStatus(0);
    }

    public void checkMapResolve(Map object, Object fieldName) {
        if (this.resolveStatus == 1) {
            ResolveFieldDeserializer fieldResolver = new ResolveFieldDeserializer(object, fieldName);
            ResolveTask task = getLastResolveTask();
            task.fieldDeserializer = fieldResolver;
            task.ownerContext = this.context;
            setResolveStatus(0);
        }
    }

    public Object parseObject(Map object) {
        return parseObject(object, null);
    }

    public JSONObject parseObject() {
        return (JSONObject) parseObject(new JSONObject(this.lexer.isEnabled(Feature.OrderedField)));
    }

    public final void parseArray(Collection array) {
        parseArray(array, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void parseArray(java.util.Collection r13, java.lang.Object r14) {
        /*
        r12 = this;
        r11 = 4;
        r10 = 16;
        r4 = r12.lexer;
        r8 = r4.token();
        r9 = 21;
        if (r8 == r9) goto L_0x0015;
    L_0x000d:
        r8 = r4.token();
        r9 = 22;
        if (r8 != r9) goto L_0x0018;
    L_0x0015:
        r4.nextToken();
    L_0x0018:
        r8 = r4.token();
        r9 = 14;
        if (r8 == r9) goto L_0x0059;
    L_0x0020:
        r8 = new com.alibaba.fastjson.JSONException;
        r9 = new java.lang.StringBuilder;
        r9.<init>();
        r10 = "syntax error, expect [, actual ";
        r9 = r9.append(r10);
        r10 = r4.token();
        r10 = com.alibaba.fastjson.parser.JSONToken.name(r10);
        r9 = r9.append(r10);
        r10 = ", pos ";
        r9 = r9.append(r10);
        r10 = r4.pos();
        r9 = r9.append(r10);
        r10 = ", fieldName ";
        r9 = r9.append(r10);
        r9 = r9.append(r14);
        r9 = r9.toString();
        r8.<init>(r9);
        throw r8;
    L_0x0059:
        r4.nextToken(r11);
        r0 = r12.context;
        r12.setContext(r13, r14);
        r1 = 0;
    L_0x0062:
        r8 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;	 Catch:{ all -> 0x0074 }
        r8 = r4.isEnabled(r8);	 Catch:{ all -> 0x0074 }
        if (r8 == 0) goto L_0x0079;
    L_0x006a:
        r8 = r4.token();	 Catch:{ all -> 0x0074 }
        if (r8 != r10) goto L_0x0079;
    L_0x0070:
        r4.nextToken();	 Catch:{ all -> 0x0074 }
        goto L_0x006a;
    L_0x0074:
        r8 = move-exception;
        r12.setContext(r0);
        throw r8;
    L_0x0079:
        r8 = r4.token();	 Catch:{ all -> 0x0074 }
        switch(r8) {
            case 2: goto L_0x0097;
            case 3: goto L_0x00a1;
            case 4: goto L_0x00ba;
            case 5: goto L_0x0080;
            case 6: goto L_0x00e6;
            case 7: goto L_0x00ee;
            case 8: goto L_0x0128;
            case 9: goto L_0x0080;
            case 10: goto L_0x0080;
            case 11: goto L_0x0080;
            case 12: goto L_0x00f6;
            case 13: goto L_0x0080;
            case 14: goto L_0x010b;
            case 15: goto L_0x0136;
            case 16: goto L_0x0080;
            case 17: goto L_0x0080;
            case 18: goto L_0x0080;
            case 19: goto L_0x0080;
            case 20: goto L_0x013f;
            case 21: goto L_0x0080;
            case 22: goto L_0x0080;
            case 23: goto L_0x012f;
            default: goto L_0x0080;
        };	 Catch:{ all -> 0x0074 }
    L_0x0080:
        r7 = r12.parse();	 Catch:{ all -> 0x0074 }
    L_0x0084:
        r13.add(r7);	 Catch:{ all -> 0x0074 }
        r12.checkListResolve(r13);	 Catch:{ all -> 0x0074 }
        r8 = r4.token();	 Catch:{ all -> 0x0074 }
        if (r8 != r10) goto L_0x0094;
    L_0x0090:
        r8 = 4;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
    L_0x0094:
        r1 = r1 + 1;
        goto L_0x0062;
    L_0x0097:
        r7 = r4.integerValue();	 Catch:{ all -> 0x0074 }
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x00a1:
        r8 = com.alibaba.fastjson.parser.Feature.UseBigDecimal;	 Catch:{ all -> 0x0074 }
        r8 = r4.isEnabled(r8);	 Catch:{ all -> 0x0074 }
        if (r8 == 0) goto L_0x00b4;
    L_0x00a9:
        r8 = 1;
        r7 = r4.decimalValue(r8);	 Catch:{ all -> 0x0074 }
    L_0x00ae:
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x00b4:
        r8 = 0;
        r7 = r4.decimalValue(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x00ae;
    L_0x00ba:
        r6 = r4.stringVal();	 Catch:{ all -> 0x0074 }
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        r8 = com.alibaba.fastjson.parser.Feature.AllowISO8601DateFormat;	 Catch:{ all -> 0x0074 }
        r8 = r4.isEnabled(r8);	 Catch:{ all -> 0x0074 }
        if (r8 == 0) goto L_0x00e4;
    L_0x00cb:
        r2 = new com.alibaba.fastjson.parser.JSONScanner;	 Catch:{ all -> 0x0074 }
        r2.<init>(r6);	 Catch:{ all -> 0x0074 }
        r8 = r2.scanISO8601DateIfMatch();	 Catch:{ all -> 0x0074 }
        if (r8 == 0) goto L_0x00e2;
    L_0x00d6:
        r8 = r2.getCalendar();	 Catch:{ all -> 0x0074 }
        r7 = r8.getTime();	 Catch:{ all -> 0x0074 }
    L_0x00de:
        r2.close();	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x00e2:
        r7 = r6;
        goto L_0x00de;
    L_0x00e4:
        r7 = r6;
        goto L_0x0084;
    L_0x00e6:
        r7 = java.lang.Boolean.TRUE;	 Catch:{ all -> 0x0074 }
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x00ee:
        r7 = java.lang.Boolean.FALSE;	 Catch:{ all -> 0x0074 }
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x00f6:
        r5 = new com.alibaba.fastjson.JSONObject;	 Catch:{ all -> 0x0074 }
        r8 = com.alibaba.fastjson.parser.Feature.OrderedField;	 Catch:{ all -> 0x0074 }
        r8 = r4.isEnabled(r8);	 Catch:{ all -> 0x0074 }
        r5.<init>(r8);	 Catch:{ all -> 0x0074 }
        r8 = java.lang.Integer.valueOf(r1);	 Catch:{ all -> 0x0074 }
        r7 = r12.parseObject(r5, r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x010b:
        r3 = new com.alibaba.fastjson.JSONArray;	 Catch:{ all -> 0x0074 }
        r3.<init>();	 Catch:{ all -> 0x0074 }
        r8 = java.lang.Integer.valueOf(r1);	 Catch:{ all -> 0x0074 }
        r12.parseArray(r3, r8);	 Catch:{ all -> 0x0074 }
        r8 = com.alibaba.fastjson.parser.Feature.UseObjectArray;	 Catch:{ all -> 0x0074 }
        r8 = r4.isEnabled(r8);	 Catch:{ all -> 0x0074 }
        if (r8 == 0) goto L_0x0125;
    L_0x011f:
        r7 = r3.toArray();	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x0125:
        r7 = r3;
        goto L_0x0084;
    L_0x0128:
        r7 = 0;
        r8 = 4;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x012f:
        r7 = 0;
        r8 = 4;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        goto L_0x0084;
    L_0x0136:
        r8 = 16;
        r4.nextToken(r8);	 Catch:{ all -> 0x0074 }
        r12.setContext(r0);
        return;
    L_0x013f:
        r8 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0074 }
        r9 = "unclosed jsonArray";
        r8.<init>(r9);	 Catch:{ all -> 0x0074 }
        throw r8;	 Catch:{ all -> 0x0074 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.DefaultJSONParser.parseArray(java.util.Collection, java.lang.Object):void");
    }

    public ParseContext getContext() {
        return this.context;
    }

    public void addResolveTask(ResolveTask task) {
        if (this.resolveTaskList == null) {
            this.resolveTaskList = new ArrayList(2);
        }
        this.resolveTaskList.add(task);
    }

    public ResolveTask getLastResolveTask() {
        return (ResolveTask) this.resolveTaskList.get(this.resolveTaskList.size() - 1);
    }

    public List<ExtraProcessor> getExtraProcessors() {
        if (this.extraProcessors == null) {
            this.extraProcessors = new ArrayList(2);
        }
        return this.extraProcessors;
    }

    public List<ExtraTypeProvider> getExtraTypeProviders() {
        if (this.extraTypeProviders == null) {
            this.extraTypeProviders = new ArrayList(2);
        }
        return this.extraTypeProviders;
    }

    public FieldTypeResolver getFieldTypeResolver() {
        return this.fieldTypeResolver;
    }

    public void setFieldTypeResolver(FieldTypeResolver fieldTypeResolver) {
        this.fieldTypeResolver = fieldTypeResolver;
    }

    public void setContext(ParseContext context) {
        if (!this.lexer.isEnabled(Feature.DisableCircularReferenceDetect)) {
            this.context = context;
        }
    }

    public void popContext() {
        if (!this.lexer.isEnabled(Feature.DisableCircularReferenceDetect)) {
            this.context = this.context.parent;
            if (this.contextArrayIndex > 0) {
                this.contextArrayIndex--;
                this.contextArray[this.contextArrayIndex] = null;
            }
        }
    }

    public ParseContext setContext(Object object, Object fieldName) {
        if (this.lexer.isEnabled(Feature.DisableCircularReferenceDetect)) {
            return null;
        }
        return setContext(this.context, object, fieldName);
    }

    public ParseContext setContext(ParseContext parent, Object object, Object fieldName) {
        if (this.lexer.isEnabled(Feature.DisableCircularReferenceDetect)) {
            return null;
        }
        this.context = new ParseContext(parent, object, fieldName);
        addContext(this.context);
        return this.context;
    }

    private void addContext(ParseContext context) {
        int i = this.contextArrayIndex;
        this.contextArrayIndex = i + 1;
        if (this.contextArray == null) {
            this.contextArray = new ParseContext[8];
        } else if (i >= this.contextArray.length) {
            ParseContext[] newArray = new ParseContext[((this.contextArray.length * 3) / 2)];
            System.arraycopy(this.contextArray, 0, newArray, 0, this.contextArray.length);
            this.contextArray = newArray;
        }
        this.contextArray[i] = context;
    }

    public Object parse() {
        return parse(null);
    }

    public Object parse(Object fieldName) {
        JSONLexer lexer = this.lexer;
        switch (lexer.token()) {
            case 2:
                Number intValue = lexer.integerValue();
                lexer.nextToken();
                return intValue;
            case 3:
                Number value = lexer.decimalValue(lexer.isEnabled(Feature.UseBigDecimal));
                lexer.nextToken();
                return value;
            case 4:
                String stringLiteral = lexer.stringVal();
                lexer.nextToken(16);
                if (lexer.isEnabled(Feature.AllowISO8601DateFormat)) {
                    JSONScanner iso8601Lexer = new JSONScanner(stringLiteral);
                    try {
                        if (iso8601Lexer.scanISO8601DateIfMatch()) {
                            HashSet<Object> set = iso8601Lexer.getCalendar().getTime();
                            return set;
                        }
                        iso8601Lexer.close();
                    } finally {
                        iso8601Lexer.close();
                    }
                }
                return stringLiteral;
            case 6:
                lexer.nextToken();
                return Boolean.TRUE;
            case 7:
                lexer.nextToken();
                return Boolean.FALSE;
            case 8:
                lexer.nextToken();
                return null;
            case 9:
                lexer.nextToken(18);
                if (lexer.token() != 18) {
                    throw new JSONException("syntax error");
                }
                lexer.nextToken(10);
                accept(10);
                long time = lexer.integerValue().longValue();
                accept(2);
                accept(11);
                return new Date(time);
            case 12:
                return parseObject(new JSONObject(lexer.isEnabled(Feature.OrderedField)), fieldName);
            case 14:
                Collection array = new JSONArray();
                parseArray(array, fieldName);
                return lexer.isEnabled(Feature.UseObjectArray) ? array.toArray() : array;
            case 20:
                if (lexer.isBlankInput()) {
                    return null;
                }
                throw new JSONException("unterminated json string, " + lexer.info());
            case 21:
                lexer.nextToken();
                Collection set2 = new HashSet();
                parseArray(set2, fieldName);
                return set2;
            case 22:
                lexer.nextToken();
                Collection treeSet = new TreeSet();
                parseArray(treeSet, fieldName);
                return treeSet;
            case 23:
                lexer.nextToken();
                return null;
            case 26:
                byte[] bytes = lexer.bytesValue();
                lexer.nextToken();
                return bytes;
            default:
                throw new JSONException("syntax error, " + lexer.info());
        }
    }

    public boolean isEnabled(Feature feature) {
        return this.lexer.isEnabled(feature);
    }

    public JSONLexer getLexer() {
        return this.lexer;
    }

    public final void accept(int token) {
        JSONLexer lexer = this.lexer;
        if (lexer.token() == token) {
            lexer.nextToken();
            return;
        }
        throw new JSONException("syntax error, expect " + JSONToken.name(token) + ", actual " + JSONToken.name(lexer.token()));
    }

    public void close() {
        JSONLexer lexer = this.lexer;
        try {
            if (!lexer.isEnabled(Feature.AutoCloseSource) || lexer.token() == 20) {
                lexer.close();
                return;
            }
            throw new JSONException("not close json text, token : " + JSONToken.name(lexer.token()));
        } catch (Throwable th) {
            lexer.close();
        }
    }

    public Object resolveReference(String ref) {
        if (this.contextArray == null) {
            return null;
        }
        int i = 0;
        while (i < this.contextArray.length && i < this.contextArrayIndex) {
            ParseContext context = this.contextArray[i];
            if (context.toString().equals(ref)) {
                return context.object;
            }
            i++;
        }
        return null;
    }

    public void handleResovleTask(Object value) {
        if (this.resolveTaskList != null) {
            int size = this.resolveTaskList.size();
            for (int i = 0; i < size; i++) {
                Object refValue;
                ResolveTask task = (ResolveTask) this.resolveTaskList.get(i);
                String ref = task.referenceValue;
                Object object = null;
                if (task.ownerContext != null) {
                    object = task.ownerContext.object;
                }
                if (ref.startsWith("$")) {
                    refValue = getObject(ref);
                    if (refValue == null) {
                        try {
                            refValue = JSONPath.eval(value, ref);
                        } catch (JSONPathException e) {
                        }
                    }
                } else {
                    refValue = task.context.object;
                }
                FieldDeserializer fieldDeser = task.fieldDeserializer;
                if (fieldDeser != null) {
                    if (!(refValue == null || refValue.getClass() != JSONObject.class || fieldDeser.fieldInfo == null || Map.class.isAssignableFrom(fieldDeser.fieldInfo.fieldClass))) {
                        refValue = JSONPath.eval(this.contextArray[0].object, ref);
                    }
                    fieldDeser.setValue(object, refValue);
                }
            }
        }
    }

    public void parseExtra(Object object, String key) {
        Object value;
        this.lexer.nextTokenWithColon();
        Type type = null;
        if (this.extraTypeProviders != null) {
            for (ExtraTypeProvider extraProvider : this.extraTypeProviders) {
                type = extraProvider.getExtraType(object, key);
            }
        }
        if (type == null) {
            value = parse();
        } else {
            value = parseObject(type);
        }
        if (object instanceof ExtraProcessable) {
            ((ExtraProcessable) object).processExtra(key, value);
            return;
        }
        if (this.extraProcessors != null) {
            for (ExtraProcessor process : this.extraProcessors) {
                process.processExtra(object, key, value);
            }
        }
        if (this.resolveStatus == 1) {
            this.resolveStatus = 0;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object parse(com.alibaba.fastjson.parser.deserializer.PropertyProcessable r19, java.lang.Object r20) {
        /*
        r18 = this;
        r0 = r18;
        r15 = r0.lexer;
        r15 = r15.token();
        r16 = 12;
        r0 = r16;
        if (r15 == r0) goto L_0x00af;
    L_0x000e:
        r15 = new java.lang.StringBuilder;
        r15.<init>();
        r16 = "syntax error, expect {, actual ";
        r15 = r15.append(r16);
        r0 = r18;
        r0 = r0.lexer;
        r16 = r0;
        r16 = r16.tokenName();
        r15 = r15.append(r16);
        r10 = r15.toString();
        r0 = r20;
        r15 = r0 instanceof java.lang.String;
        if (r15 == 0) goto L_0x0057;
    L_0x0031:
        r15 = new java.lang.StringBuilder;
        r15.<init>();
        r15 = r15.append(r10);
        r16 = ", fieldName ";
        r15 = r15.append(r16);
        r10 = r15.toString();
        r15 = new java.lang.StringBuilder;
        r15.<init>();
        r15 = r15.append(r10);
        r0 = r20;
        r15 = r15.append(r0);
        r10 = r15.toString();
    L_0x0057:
        r15 = new java.lang.StringBuilder;
        r15.<init>();
        r15 = r15.append(r10);
        r16 = ", ";
        r15 = r15.append(r16);
        r10 = r15.toString();
        r15 = new java.lang.StringBuilder;
        r15.<init>();
        r15 = r15.append(r10);
        r0 = r18;
        r0 = r0.lexer;
        r16 = r0;
        r16 = r16.info();
        r15 = r15.append(r16);
        r10 = r15.toString();
        r2 = new com.alibaba.fastjson.JSONArray;
        r2.<init>();
        r0 = r18;
        r1 = r20;
        r0.parseArray(r2, r1);
        r15 = r2.size();
        r16 = 1;
        r0 = r16;
        if (r15 != r0) goto L_0x00a9;
    L_0x009b:
        r15 = 0;
        r7 = r2.get(r15);
        r15 = r7 instanceof com.alibaba.fastjson.JSONObject;
        if (r15 == 0) goto L_0x00a9;
    L_0x00a4:
        r7 = (com.alibaba.fastjson.JSONObject) r7;
        r19 = r7;
    L_0x00a8:
        return r19;
    L_0x00a9:
        r15 = new com.alibaba.fastjson.JSONException;
        r15.<init>(r10);
        throw r15;
    L_0x00af:
        r0 = r18;
        r5 = r0.context;
        r8 = 0;
    L_0x00b4:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;	 Catch:{ all -> 0x0134 }
        r15 = r15.isEnabled(r16);	 Catch:{ all -> 0x0134 }
        if (r15 == 0) goto L_0x00ea;
    L_0x00cf:
        r15 = 44;
        if (r3 != r15) goto L_0x00ea;
    L_0x00d3:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.next();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        goto L_0x00cf;
    L_0x00ea:
        r15 = 34;
        if (r3 != r15) goto L_0x013b;
    L_0x00ee:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x0134 }
        r16 = r0;
        r17 = 34;
        r9 = r15.scanSymbol(r16, r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        r15 = 58;
        if (r3 == r15) goto L_0x021f;
    L_0x0111:
        r15 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0134 }
        r16 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0134 }
        r16.<init>();	 Catch:{ all -> 0x0134 }
        r17 = "expect ':' at ";
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r17 = r0;
        r17 = r17.pos();	 Catch:{ all -> 0x0134 }
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r16 = r16.toString();	 Catch:{ all -> 0x0134 }
        r15.<init>(r16);	 Catch:{ all -> 0x0134 }
        throw r15;	 Catch:{ all -> 0x0134 }
    L_0x0134:
        r15 = move-exception;
        r0 = r18;
        r0.setContext(r5);
        throw r15;
    L_0x013b:
        r15 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        if (r3 != r15) goto L_0x015d;
    L_0x013f:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.next();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.resetStringPosition();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = 16;
        r15.nextToken(r16);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5);
        goto L_0x00a8;
    L_0x015d:
        r15 = 39;
        if (r3 != r15) goto L_0x01bb;
    L_0x0161:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = com.alibaba.fastjson.parser.Feature.AllowSingleQuotes;	 Catch:{ all -> 0x0134 }
        r15 = r15.isEnabled(r16);	 Catch:{ all -> 0x0134 }
        if (r15 != 0) goto L_0x0175;
    L_0x016d:
        r15 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0134 }
        r16 = "syntax error";
        r15.<init>(r16);	 Catch:{ all -> 0x0134 }
        throw r15;	 Catch:{ all -> 0x0134 }
    L_0x0175:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x0134 }
        r16 = r0;
        r17 = 39;
        r9 = r15.scanSymbol(r16, r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        r15 = 58;
        if (r3 == r15) goto L_0x021f;
    L_0x0198:
        r15 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0134 }
        r16 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0134 }
        r16.<init>();	 Catch:{ all -> 0x0134 }
        r17 = "expect ':' at ";
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r17 = r0;
        r17 = r17.pos();	 Catch:{ all -> 0x0134 }
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r16 = r16.toString();	 Catch:{ all -> 0x0134 }
        r15.<init>(r16);	 Catch:{ all -> 0x0134 }
        throw r15;	 Catch:{ all -> 0x0134 }
    L_0x01bb:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = com.alibaba.fastjson.parser.Feature.AllowUnQuotedFieldNames;	 Catch:{ all -> 0x0134 }
        r15 = r15.isEnabled(r16);	 Catch:{ all -> 0x0134 }
        if (r15 != 0) goto L_0x01cf;
    L_0x01c7:
        r15 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0134 }
        r16 = "syntax error";
        r15.<init>(r16);	 Catch:{ all -> 0x0134 }
        throw r15;	 Catch:{ all -> 0x0134 }
    L_0x01cf:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x0134 }
        r16 = r0;
        r9 = r15.scanSymbolUnQuoted(r16);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        r15 = 58;
        if (r3 == r15) goto L_0x021f;
    L_0x01f0:
        r15 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0134 }
        r16 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0134 }
        r16.<init>();	 Catch:{ all -> 0x0134 }
        r17 = "expect ':' at ";
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r17 = r0;
        r17 = r17.pos();	 Catch:{ all -> 0x0134 }
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r17 = ", actual ";
        r16 = r16.append(r17);	 Catch:{ all -> 0x0134 }
        r0 = r16;
        r16 = r0.append(r3);	 Catch:{ all -> 0x0134 }
        r16 = r16.toString();	 Catch:{ all -> 0x0134 }
        r15.<init>(r16);	 Catch:{ all -> 0x0134 }
        throw r15;	 Catch:{ all -> 0x0134 }
    L_0x021f:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.next();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.skipWhitespace();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r3 = r15.getCurrent();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.resetStringPosition();	 Catch:{ all -> 0x0134 }
        r15 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;	 Catch:{ all -> 0x0134 }
        if (r9 != r15) goto L_0x02cc;
    L_0x0240:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = com.alibaba.fastjson.parser.Feature.DisableSpecialKeyDetect;	 Catch:{ all -> 0x0134 }
        r15 = r15.isEnabled(r16);	 Catch:{ all -> 0x0134 }
        if (r15 != 0) goto L_0x02cc;
    L_0x024c:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0 = r0.symbolTable;	 Catch:{ all -> 0x0134 }
        r16 = r0;
        r17 = 34;
        r12 = r15.scanSymbol(r16, r17);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.config;	 Catch:{ all -> 0x0134 }
        r16 = 0;
        r0 = r16;
        r4 = r15.checkAutoType(r12, r0);	 Catch:{ all -> 0x0134 }
        r15 = java.util.Map.class;
        r15 = r15.isAssignableFrom(r4);	 Catch:{ all -> 0x0134 }
        if (r15 == 0) goto L_0x0297;
    L_0x0270:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = 16;
        r15.nextToken(r16);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15 = r15.token();	 Catch:{ all -> 0x0134 }
        r16 = 13;
        r0 = r16;
        if (r15 != r0) goto L_0x0335;
    L_0x0287:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = 16;
        r15.nextToken(r16);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5);
        goto L_0x00a8;
    L_0x0297:
        r0 = r18;
        r15 = r0.config;	 Catch:{ all -> 0x0134 }
        r6 = r15.getDeserializer(r4);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r16 = 16;
        r15.nextToken(r16);	 Catch:{ all -> 0x0134 }
        r15 = 2;
        r0 = r18;
        r0.setResolveStatus(r15);	 Catch:{ all -> 0x0134 }
        if (r5 == 0) goto L_0x02b9;
    L_0x02b0:
        r0 = r20;
        r15 = r0 instanceof java.lang.Integer;	 Catch:{ all -> 0x0134 }
        if (r15 != 0) goto L_0x02b9;
    L_0x02b6:
        r18.popContext();	 Catch:{ all -> 0x0134 }
    L_0x02b9:
        r0 = r18;
        r1 = r20;
        r15 = r6.deserialze(r0, r4, r1);	 Catch:{ all -> 0x0134 }
        r15 = (java.util.Map) r15;	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5);
        r19 = r15;
        goto L_0x00a8;
    L_0x02cc:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.nextToken();	 Catch:{ all -> 0x0134 }
        if (r8 == 0) goto L_0x02da;
    L_0x02d5:
        r0 = r18;
        r0.setContext(r5);	 Catch:{ all -> 0x0134 }
    L_0x02da:
        r0 = r19;
        r14 = r0.getType(r9);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15 = r15.token();	 Catch:{ all -> 0x0134 }
        r16 = 8;
        r0 = r16;
        if (r15 != r0) goto L_0x031c;
    L_0x02ee:
        r13 = 0;
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.nextToken();	 Catch:{ all -> 0x0134 }
    L_0x02f6:
        r0 = r19;
        r0.apply(r9, r13);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5, r13, r9);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5);	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r11 = r15.token();	 Catch:{ all -> 0x0134 }
        r15 = 20;
        if (r11 == r15) goto L_0x0315;
    L_0x0311:
        r15 = 15;
        if (r11 != r15) goto L_0x0323;
    L_0x0315:
        r0 = r18;
        r0.setContext(r5);
        goto L_0x00a8;
    L_0x031c:
        r0 = r18;
        r13 = r0.parseObject(r14, r9);	 Catch:{ all -> 0x0134 }
        goto L_0x02f6;
    L_0x0323:
        r15 = 13;
        if (r11 != r15) goto L_0x0335;
    L_0x0327:
        r0 = r18;
        r15 = r0.lexer;	 Catch:{ all -> 0x0134 }
        r15.nextToken();	 Catch:{ all -> 0x0134 }
        r0 = r18;
        r0.setContext(r5);
        goto L_0x00a8;
    L_0x0335:
        r8 = r8 + 1;
        goto L_0x00b4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.DefaultJSONParser.parse(com.alibaba.fastjson.parser.deserializer.PropertyProcessable, java.lang.Object):java.lang.Object");
    }
}
