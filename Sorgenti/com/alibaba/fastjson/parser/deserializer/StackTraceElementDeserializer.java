package com.alibaba.fastjson.parser.deserializer;

public class StackTraceElementDeserializer implements ObjectDeserializer {
    public static final StackTraceElementDeserializer instance = new StackTraceElementDeserializer();

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T deserialze(com.alibaba.fastjson.parser.DefaultJSONParser r13, java.lang.reflect.Type r14, java.lang.Object r15) {
        /*
        r12 = this;
        r4 = r13.lexer;
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x000f;
    L_0x000a:
        r4.nextToken();
        r9 = 0;
    L_0x000e:
        return r9;
    L_0x000f:
        r9 = r4.token();
        r10 = 12;
        if (r9 == r10) goto L_0x0040;
    L_0x0017:
        r9 = r4.token();
        r10 = 16;
        if (r9 == r10) goto L_0x0040;
    L_0x001f:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = new java.lang.StringBuilder;
        r10.<init>();
        r11 = "syntax error: ";
        r10 = r10.append(r11);
        r11 = r4.token();
        r11 = com.alibaba.fastjson.parser.JSONToken.name(r11);
        r10 = r10.append(r11);
        r10 = r10.toString();
        r9.<init>(r10);
        throw r9;
    L_0x0040:
        r0 = 0;
        r6 = 0;
        r2 = 0;
        r5 = 0;
        r7 = 0;
        r8 = 0;
    L_0x0046:
        r9 = r13.getSymbolTable();
        r3 = r4.scanSymbol(r9);
        if (r3 != 0) goto L_0x0073;
    L_0x0050:
        r9 = r4.token();
        r10 = 13;
        if (r9 != r10) goto L_0x0063;
    L_0x0058:
        r9 = 16;
        r4.nextToken(r9);
    L_0x005d:
        r9 = new java.lang.StackTraceElement;
        r9.<init>(r0, r6, r2, r5);
        goto L_0x000e;
    L_0x0063:
        r9 = r4.token();
        r10 = 16;
        if (r9 != r10) goto L_0x0073;
    L_0x006b:
        r9 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;
        r9 = r4.isEnabled(r9);
        if (r9 != 0) goto L_0x0046;
    L_0x0073:
        r9 = 4;
        r4.nextTokenWithColon(r9);
        r9 = "className";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x00aa;
    L_0x007f:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x0096;
    L_0x0087:
        r0 = 0;
    L_0x0088:
        r9 = r4.token();
        r10 = 13;
        if (r9 != r10) goto L_0x0046;
    L_0x0090:
        r9 = 16;
        r4.nextToken(r9);
        goto L_0x005d;
    L_0x0096:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x00a2;
    L_0x009d:
        r0 = r4.stringVal();
        goto L_0x0088;
    L_0x00a2:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x00aa:
        r9 = "methodName";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x00d0;
    L_0x00b2:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x00bc;
    L_0x00ba:
        r6 = 0;
        goto L_0x0088;
    L_0x00bc:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x00c8;
    L_0x00c3:
        r6 = r4.stringVal();
        goto L_0x0088;
    L_0x00c8:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x00d0:
        r9 = "fileName";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x00f6;
    L_0x00d8:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x00e2;
    L_0x00e0:
        r2 = 0;
        goto L_0x0088;
    L_0x00e2:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x00ee;
    L_0x00e9:
        r2 = r4.stringVal();
        goto L_0x0088;
    L_0x00ee:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x00f6:
        r9 = "lineNumber";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x011d;
    L_0x00fe:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x0108;
    L_0x0106:
        r5 = 0;
        goto L_0x0088;
    L_0x0108:
        r9 = r4.token();
        r10 = 2;
        if (r9 != r10) goto L_0x0115;
    L_0x010f:
        r5 = r4.intValue();
        goto L_0x0088;
    L_0x0115:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x011d:
        r9 = "nativeMethod";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x0158;
    L_0x0125:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x0134;
    L_0x012d:
        r9 = 16;
        r4.nextToken(r9);
        goto L_0x0088;
    L_0x0134:
        r9 = r4.token();
        r10 = 6;
        if (r9 != r10) goto L_0x0142;
    L_0x013b:
        r9 = 16;
        r4.nextToken(r9);
        goto L_0x0088;
    L_0x0142:
        r9 = r4.token();
        r10 = 7;
        if (r9 != r10) goto L_0x0150;
    L_0x0149:
        r9 = 16;
        r4.nextToken(r9);
        goto L_0x0088;
    L_0x0150:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x0158:
        r9 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;
        if (r3 != r9) goto L_0x0198;
    L_0x015c:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x0188;
    L_0x0163:
        r1 = r4.stringVal();
        r9 = "java.lang.StackTraceElement";
        r9 = r1.equals(r9);
        if (r9 != 0) goto L_0x0088;
    L_0x016f:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = new java.lang.StringBuilder;
        r10.<init>();
        r11 = "syntax error : ";
        r10 = r10.append(r11);
        r10 = r10.append(r1);
        r10 = r10.toString();
        r9.<init>(r10);
        throw r9;
    L_0x0188:
        r9 = r4.token();
        r10 = 8;
        if (r9 == r10) goto L_0x0088;
    L_0x0190:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x0198:
        r9 = "moduleName";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x01c0;
    L_0x01a0:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x01ab;
    L_0x01a8:
        r7 = 0;
        goto L_0x0088;
    L_0x01ab:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x01b8;
    L_0x01b2:
        r7 = r4.stringVal();
        goto L_0x0088;
    L_0x01b8:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x01c0:
        r9 = "moduleVersion";
        r9 = r9.equals(r3);
        if (r9 == 0) goto L_0x01e8;
    L_0x01c8:
        r9 = r4.token();
        r10 = 8;
        if (r9 != r10) goto L_0x01d3;
    L_0x01d0:
        r8 = 0;
        goto L_0x0088;
    L_0x01d3:
        r9 = r4.token();
        r10 = 4;
        if (r9 != r10) goto L_0x01e0;
    L_0x01da:
        r8 = r4.stringVal();
        goto L_0x0088;
    L_0x01e0:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = "syntax error";
        r9.<init>(r10);
        throw r9;
    L_0x01e8:
        r9 = new com.alibaba.fastjson.JSONException;
        r10 = new java.lang.StringBuilder;
        r10.<init>();
        r11 = "syntax error : ";
        r10 = r10.append(r11);
        r10 = r10.append(r3);
        r10 = r10.toString();
        r9.<init>(r10);
        throw r9;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.deserializer.StackTraceElementDeserializer.deserialze(com.alibaba.fastjson.parser.DefaultJSONParser, java.lang.reflect.Type, java.lang.Object):T");
    }

    public int getFastMatchToken() {
        return 12;
    }
}
