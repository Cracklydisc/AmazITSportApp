package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.DefaultJSONParser.ResolveTask;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.JSONLexer;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.ParseContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MapDeserializer implements ObjectDeserializer {
    public static MapDeserializer instance = new MapDeserializer();

    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        if (type == JSONObject.class && parser.getFieldTypeResolver() == null) {
            return parser.parseObject();
        }
        JSONLexer lexer = parser.lexer;
        if (lexer.token() == 8) {
            lexer.nextToken(16);
            return null;
        }
        Map<Object, Object> map = createMap(type);
        ParseContext context = parser.getContext();
        try {
            parser.setContext(context, map, fieldName);
            T deserialze = deserialze(parser, type, fieldName, map);
            return deserialze;
        } finally {
            parser.setContext(context);
        }
    }

    protected Object deserialze(DefaultJSONParser parser, Type type, Object fieldName, Map map) {
        if (!(type instanceof ParameterizedType)) {
            return parser.parseObject(map, fieldName);
        }
        Type valueType;
        ParameterizedType parameterizedType = (ParameterizedType) type;
        Class keyType = parameterizedType.getActualTypeArguments()[0];
        if (map.getClass().getName().equals("org.springframework.util.LinkedMultiValueMap")) {
            valueType = List.class;
        } else {
            valueType = parameterizedType.getActualTypeArguments()[1];
        }
        if (String.class == keyType) {
            return parseMap(parser, map, valueType, fieldName);
        }
        return parseMap(parser, map, keyType, valueType, fieldName);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map parseMap(com.alibaba.fastjson.parser.DefaultJSONParser r20, java.util.Map<java.lang.String, java.lang.Object> r21, java.lang.reflect.Type r22, java.lang.Object r23) {
        /*
        r0 = r20;
        r11 = r0.lexer;
        r14 = r11.token();
        r17 = 12;
        r0 = r17;
        if (r14 == r0) goto L_0x00be;
    L_0x000e:
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r18 = "syntax error, expect {, actual ";
        r17 = r17.append(r18);
        r18 = r11.tokenName();
        r17 = r17.append(r18);
        r12 = r17.toString();
        r0 = r23;
        r0 = r0 instanceof java.lang.String;
        r17 = r0;
        if (r17 == 0) goto L_0x0059;
    L_0x002d:
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r0 = r17;
        r17 = r0.append(r12);
        r18 = ", fieldName ";
        r17 = r17.append(r18);
        r12 = r17.toString();
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r0 = r17;
        r17 = r0.append(r12);
        r0 = r17;
        r1 = r23;
        r17 = r0.append(r1);
        r12 = r17.toString();
    L_0x0059:
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r0 = r17;
        r17 = r0.append(r12);
        r18 = ", ";
        r17 = r17.append(r18);
        r12 = r17.toString();
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r0 = r17;
        r17 = r0.append(r12);
        r18 = r11.info();
        r17 = r17.append(r18);
        r12 = r17.toString();
        r17 = 4;
        r0 = r17;
        if (r14 == r0) goto L_0x00b6;
    L_0x008b:
        r2 = new com.alibaba.fastjson.JSONArray;
        r2.<init>();
        r0 = r20;
        r1 = r23;
        r0.parseArray(r2, r1);
        r17 = r2.size();
        r18 = 1;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x00b6;
    L_0x00a3:
        r17 = 0;
        r0 = r17;
        r8 = r2.get(r0);
        r0 = r8 instanceof com.alibaba.fastjson.JSONObject;
        r17 = r0;
        if (r17 == 0) goto L_0x00b6;
    L_0x00b1:
        r8 = (com.alibaba.fastjson.JSONObject) r8;
        r21 = r8;
    L_0x00b5:
        return r21;
    L_0x00b6:
        r17 = new com.alibaba.fastjson.JSONException;
        r0 = r17;
        r0.<init>(r12);
        throw r17;
    L_0x00be:
        r6 = r20.getContext();
        r9 = 0;
    L_0x00c3:
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        r17 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;	 Catch:{ all -> 0x0123 }
        r0 = r17;
        r17 = r11.isEnabled(r0);	 Catch:{ all -> 0x0123 }
        if (r17 == 0) goto L_0x00e5;
    L_0x00d4:
        r17 = 44;
        r0 = r17;
        if (r3 != r0) goto L_0x00e5;
    L_0x00da:
        r11.next();	 Catch:{ all -> 0x0123 }
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        goto L_0x00d4;
    L_0x00e5:
        r17 = 34;
        r0 = r17;
        if (r3 != r0) goto L_0x012a;
    L_0x00eb:
        r17 = r20.getSymbolTable();	 Catch:{ all -> 0x0123 }
        r18 = 34;
        r0 = r17;
        r1 = r18;
        r10 = r11.scanSymbol(r0, r1);	 Catch:{ all -> 0x0123 }
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        r17 = 58;
        r0 = r17;
        if (r3 == r0) goto L_0x01e6;
    L_0x0106:
        r17 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0123 }
        r18 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0123 }
        r18.<init>();	 Catch:{ all -> 0x0123 }
        r19 = "expect ':' at ";
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r19 = r11.pos();	 Catch:{ all -> 0x0123 }
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r18 = r18.toString();	 Catch:{ all -> 0x0123 }
        r17.<init>(r18);	 Catch:{ all -> 0x0123 }
        throw r17;	 Catch:{ all -> 0x0123 }
    L_0x0123:
        r17 = move-exception;
        r0 = r20;
        r0.setContext(r6);
        throw r17;
    L_0x012a:
        r17 = 125; // 0x7d float:1.75E-43 double:6.2E-322;
        r0 = r17;
        if (r3 != r0) goto L_0x0144;
    L_0x0130:
        r11.next();	 Catch:{ all -> 0x0123 }
        r11.resetStringPosition();	 Catch:{ all -> 0x0123 }
        r17 = 16;
        r0 = r17;
        r11.nextToken(r0);	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r0.setContext(r6);
        goto L_0x00b5;
    L_0x0144:
        r17 = 39;
        r0 = r17;
        if (r3 != r0) goto L_0x0194;
    L_0x014a:
        r17 = com.alibaba.fastjson.parser.Feature.AllowSingleQuotes;	 Catch:{ all -> 0x0123 }
        r0 = r17;
        r17 = r11.isEnabled(r0);	 Catch:{ all -> 0x0123 }
        if (r17 != 0) goto L_0x015c;
    L_0x0154:
        r17 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0123 }
        r18 = "syntax error";
        r17.<init>(r18);	 Catch:{ all -> 0x0123 }
        throw r17;	 Catch:{ all -> 0x0123 }
    L_0x015c:
        r17 = r20.getSymbolTable();	 Catch:{ all -> 0x0123 }
        r18 = 39;
        r0 = r17;
        r1 = r18;
        r10 = r11.scanSymbol(r0, r1);	 Catch:{ all -> 0x0123 }
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        r17 = 58;
        r0 = r17;
        if (r3 == r0) goto L_0x01e6;
    L_0x0177:
        r17 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0123 }
        r18 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0123 }
        r18.<init>();	 Catch:{ all -> 0x0123 }
        r19 = "expect ':' at ";
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r19 = r11.pos();	 Catch:{ all -> 0x0123 }
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r18 = r18.toString();	 Catch:{ all -> 0x0123 }
        r17.<init>(r18);	 Catch:{ all -> 0x0123 }
        throw r17;	 Catch:{ all -> 0x0123 }
    L_0x0194:
        r17 = com.alibaba.fastjson.parser.Feature.AllowUnQuotedFieldNames;	 Catch:{ all -> 0x0123 }
        r0 = r17;
        r17 = r11.isEnabled(r0);	 Catch:{ all -> 0x0123 }
        if (r17 != 0) goto L_0x01a6;
    L_0x019e:
        r17 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0123 }
        r18 = "syntax error";
        r17.<init>(r18);	 Catch:{ all -> 0x0123 }
        throw r17;	 Catch:{ all -> 0x0123 }
    L_0x01a6:
        r17 = r20.getSymbolTable();	 Catch:{ all -> 0x0123 }
        r0 = r17;
        r10 = r11.scanSymbolUnQuoted(r0);	 Catch:{ all -> 0x0123 }
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        r17 = 58;
        r0 = r17;
        if (r3 == r0) goto L_0x01e6;
    L_0x01bd:
        r17 = new com.alibaba.fastjson.JSONException;	 Catch:{ all -> 0x0123 }
        r18 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0123 }
        r18.<init>();	 Catch:{ all -> 0x0123 }
        r19 = "expect ':' at ";
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r19 = r11.pos();	 Catch:{ all -> 0x0123 }
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r19 = ", actual ";
        r18 = r18.append(r19);	 Catch:{ all -> 0x0123 }
        r0 = r18;
        r18 = r0.append(r3);	 Catch:{ all -> 0x0123 }
        r18 = r18.toString();	 Catch:{ all -> 0x0123 }
        r17.<init>(r18);	 Catch:{ all -> 0x0123 }
        throw r17;	 Catch:{ all -> 0x0123 }
    L_0x01e6:
        r11.next();	 Catch:{ all -> 0x0123 }
        r11.skipWhitespace();	 Catch:{ all -> 0x0123 }
        r3 = r11.getCurrent();	 Catch:{ all -> 0x0123 }
        r11.resetStringPosition();	 Catch:{ all -> 0x0123 }
        r17 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;	 Catch:{ all -> 0x0123 }
        r0 = r17;
        if (r10 != r0) goto L_0x027c;
    L_0x01f9:
        r17 = com.alibaba.fastjson.parser.Feature.DisableSpecialKeyDetect;	 Catch:{ all -> 0x0123 }
        r0 = r17;
        r17 = r11.isEnabled(r0);	 Catch:{ all -> 0x0123 }
        if (r17 != 0) goto L_0x027c;
    L_0x0203:
        r17 = r20.getSymbolTable();	 Catch:{ all -> 0x0123 }
        r18 = 34;
        r0 = r17;
        r1 = r18;
        r15 = r11.scanSymbol(r0, r1);	 Catch:{ all -> 0x0123 }
        r5 = r20.getConfig();	 Catch:{ all -> 0x0123 }
        r17 = 0;
        r0 = r17;
        r4 = r5.checkAutoType(r15, r0);	 Catch:{ all -> 0x0123 }
        r17 = java.util.Map.class;
        r0 = r17;
        r17 = r0.isAssignableFrom(r4);	 Catch:{ all -> 0x0123 }
        if (r17 == 0) goto L_0x0248;
    L_0x0227:
        r17 = 16;
        r0 = r17;
        r11.nextToken(r0);	 Catch:{ all -> 0x0123 }
        r17 = r11.token();	 Catch:{ all -> 0x0123 }
        r18 = 13;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x02e1;
    L_0x023a:
        r17 = 16;
        r0 = r17;
        r11.nextToken(r0);	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r0.setContext(r6);
        goto L_0x00b5;
    L_0x0248:
        r7 = r5.getDeserializer(r4);	 Catch:{ all -> 0x0123 }
        r17 = 16;
        r0 = r17;
        r11.nextToken(r0);	 Catch:{ all -> 0x0123 }
        r17 = 2;
        r0 = r20;
        r1 = r17;
        r0.setResolveStatus(r1);	 Catch:{ all -> 0x0123 }
        if (r6 == 0) goto L_0x0269;
    L_0x025e:
        r0 = r23;
        r0 = r0 instanceof java.lang.Integer;	 Catch:{ all -> 0x0123 }
        r17 = r0;
        if (r17 != 0) goto L_0x0269;
    L_0x0266:
        r20.popContext();	 Catch:{ all -> 0x0123 }
    L_0x0269:
        r0 = r20;
        r1 = r23;
        r17 = r7.deserialze(r0, r4, r1);	 Catch:{ all -> 0x0123 }
        r17 = (java.util.Map) r17;	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r0.setContext(r6);
        r21 = r17;
        goto L_0x00b5;
    L_0x027c:
        r11.nextToken();	 Catch:{ all -> 0x0123 }
        if (r9 == 0) goto L_0x0286;
    L_0x0281:
        r0 = r20;
        r0.setContext(r6);	 Catch:{ all -> 0x0123 }
    L_0x0286:
        r17 = r11.token();	 Catch:{ all -> 0x0123 }
        r18 = 8;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x02c8;
    L_0x0292:
        r16 = 0;
        r11.nextToken();	 Catch:{ all -> 0x0123 }
    L_0x0297:
        r0 = r21;
        r1 = r16;
        r0.put(r10, r1);	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r1 = r21;
        r0.checkMapResolve(r1, r10);	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r1 = r16;
        r0.setContext(r6, r1, r10);	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r0.setContext(r6);	 Catch:{ all -> 0x0123 }
        r13 = r11.token();	 Catch:{ all -> 0x0123 }
        r17 = 20;
        r0 = r17;
        if (r13 == r0) goto L_0x02c1;
    L_0x02bb:
        r17 = 15;
        r0 = r17;
        if (r13 != r0) goto L_0x02d1;
    L_0x02c1:
        r0 = r20;
        r0.setContext(r6);
        goto L_0x00b5;
    L_0x02c8:
        r0 = r20;
        r1 = r22;
        r16 = r0.parseObject(r1, r10);	 Catch:{ all -> 0x0123 }
        goto L_0x0297;
    L_0x02d1:
        r17 = 13;
        r0 = r17;
        if (r13 != r0) goto L_0x02e1;
    L_0x02d7:
        r11.nextToken();	 Catch:{ all -> 0x0123 }
        r0 = r20;
        r0.setContext(r6);
        goto L_0x00b5;
    L_0x02e1:
        r9 = r9 + 1;
        goto L_0x00c3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.deserializer.MapDeserializer.parseMap(com.alibaba.fastjson.parser.DefaultJSONParser, java.util.Map, java.lang.reflect.Type, java.lang.Object):java.util.Map");
    }

    public static Object parseMap(DefaultJSONParser parser, Map<Object, Object> map, Type keyType, Type valueType, Object fieldName) {
        JSONLexer lexer = parser.lexer;
        if (lexer.token() == 12 || lexer.token() == 16) {
            ObjectDeserializer keyDeserializer = parser.getConfig().getDeserializer(keyType);
            ObjectDeserializer valueDeserializer = parser.getConfig().getDeserializer(valueType);
            lexer.nextToken(keyDeserializer.getFastMatchToken());
            ParseContext context = parser.getContext();
            while (lexer.token() != 13) {
                if (lexer.token() == 4 && lexer.isRef() && !lexer.isEnabled(Feature.DisableSpecialKeyDetect)) {
                    Map<Object, Object> object = null;
                    lexer.nextTokenWithColon(4);
                    if (lexer.token() == 4) {
                        String ref = lexer.stringVal();
                        if ("..".equals(ref)) {
                            object = context.parent.object;
                        } else if ("$".equals(ref)) {
                            ParseContext rootContext = context;
                            while (rootContext.parent != null) {
                                rootContext = rootContext.parent;
                            }
                            object = rootContext.object;
                        } else {
                            parser.addResolveTask(new ResolveTask(context, ref));
                            parser.setResolveStatus(1);
                        }
                        lexer.nextToken(13);
                        if (lexer.token() != 13) {
                            throw new JSONException("illegal ref");
                        }
                        lexer.nextToken(16);
                        parser.setContext(context);
                        return object;
                    }
                    throw new JSONException("illegal ref, " + JSONToken.name(lexer.token()));
                }
                if (map.size() == 0 && lexer.token() == 4 && JSON.DEFAULT_TYPE_KEY.equals(lexer.stringVal()) && !lexer.isEnabled(Feature.DisableSpecialKeyDetect)) {
                    lexer.nextTokenWithColon(4);
                    lexer.nextToken(16);
                    if (lexer.token() == 13) {
                        lexer.nextToken();
                        return map;
                    }
                    try {
                        lexer.nextToken(keyDeserializer.getFastMatchToken());
                    } finally {
                        parser.setContext(context);
                    }
                }
                Object key = keyDeserializer.deserialze(parser, keyType, null);
                if (lexer.token() != 17) {
                    throw new JSONException("syntax error, expect :, actual " + lexer.token());
                }
                lexer.nextToken(valueDeserializer.getFastMatchToken());
                Object value = valueDeserializer.deserialze(parser, valueType, key);
                parser.checkMapResolve(map, key);
                map.put(key, value);
                if (lexer.token() == 16) {
                    lexer.nextToken(keyDeserializer.getFastMatchToken());
                }
            }
            lexer.nextToken(16);
            parser.setContext(context);
            return map;
        }
        throw new JSONException("syntax error, expect {, actual " + lexer.tokenName());
    }

    protected Map<Object, Object> createMap(Type type) {
        if (type == Properties.class) {
            return new Properties();
        }
        if (type == Hashtable.class) {
            return new Hashtable();
        }
        if (type == IdentityHashMap.class) {
            return new IdentityHashMap();
        }
        if (type == SortedMap.class || type == TreeMap.class) {
            return new TreeMap();
        }
        if (type == ConcurrentMap.class || type == ConcurrentHashMap.class) {
            return new ConcurrentHashMap();
        }
        if (type == Map.class || type == HashMap.class) {
            return new HashMap();
        }
        if (type == LinkedHashMap.class) {
            return new LinkedHashMap();
        }
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();
            if (EnumMap.class.equals(rawType)) {
                return new EnumMap((Class) parameterizedType.getActualTypeArguments()[0]);
            }
            return createMap(rawType);
        }
        Class<?> clazz = (Class) type;
        if (clazz.isInterface()) {
            throw new JSONException("unsupport type " + type);
        }
        try {
            return (Map) clazz.newInstance();
        } catch (Exception e) {
            throw new JSONException("unsupport type " + type, e);
        }
    }

    public int getFastMatchToken() {
        return 12;
    }
}
