package com.alibaba.fastjson.parser.deserializer;

import com.alibaba.fastjson.parser.ParserConfig;
import java.lang.reflect.Constructor;

public class ThrowableDeserializer extends JavaBeanDeserializer {
    public ThrowableDeserializer(ParserConfig mapping, Class<?> clazz) {
        super(mapping, clazz, clazz);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T deserialze(com.alibaba.fastjson.parser.DefaultJSONParser r23, java.lang.reflect.Type r24, java.lang.Object r25) {
        /*
        r22 = this;
        r0 = r23;
        r14 = r0.lexer;
        r19 = r14.token();
        r20 = 8;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x0015;
    L_0x0010:
        r14.nextToken();
        r8 = 0;
    L_0x0014:
        return r8;
    L_0x0015:
        r19 = r23.getResolveStatus();
        r20 = 2;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x00a7;
    L_0x0021:
        r19 = 0;
        r0 = r23;
        r1 = r19;
        r0.setResolveStatus(r1);
    L_0x002a:
        r4 = 0;
        r10 = 0;
        if (r24 == 0) goto L_0x0045;
    L_0x002e:
        r0 = r24;
        r0 = r0 instanceof java.lang.Class;
        r19 = r0;
        if (r19 == 0) goto L_0x0045;
    L_0x0036:
        r5 = r24;
        r5 = (java.lang.Class) r5;
        r19 = java.lang.Throwable.class;
        r0 = r19;
        r19 = r0.isAssignableFrom(r5);
        if (r19 == 0) goto L_0x0045;
    L_0x0044:
        r10 = r5;
    L_0x0045:
        r15 = 0;
        r17 = 0;
        r16 = new java.util.HashMap;
        r16.<init>();
    L_0x004d:
        r19 = r23.getSymbolTable();
        r0 = r19;
        r13 = r14.scanSymbol(r0);
        if (r13 != 0) goto L_0x00d1;
    L_0x0059:
        r19 = r14.token();
        r20 = 13;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x00bb;
    L_0x0065:
        r19 = 16;
        r0 = r19;
        r14.nextToken(r0);
    L_0x006c:
        r8 = 0;
        if (r10 != 0) goto L_0x019a;
    L_0x006f:
        r8 = new java.lang.Exception;
        r8.<init>(r15, r4);
    L_0x0074:
        if (r17 == 0) goto L_0x007b;
    L_0x0076:
        r0 = r17;
        r8.setStackTrace(r0);
    L_0x007b:
        r19 = r16.entrySet();
        r19 = r19.iterator();
    L_0x0083:
        r20 = r19.hasNext();
        if (r20 == 0) goto L_0x0014;
    L_0x0089:
        r7 = r19.next();
        r7 = (java.util.Map.Entry) r7;
        r13 = r7.getKey();
        r13 = (java.lang.String) r13;
        r18 = r7.getValue();
        r0 = r22;
        r12 = r0.getFieldDeserializer(r13);
        if (r12 == 0) goto L_0x0083;
    L_0x00a1:
        r0 = r18;
        r12.setValue(r8, r0);
        goto L_0x0083;
    L_0x00a7:
        r19 = r14.token();
        r20 = 12;
        r0 = r19;
        r1 = r20;
        if (r0 == r1) goto L_0x002a;
    L_0x00b3:
        r19 = new com.alibaba.fastjson.JSONException;
        r20 = "syntax error";
        r19.<init>(r20);
        throw r19;
    L_0x00bb:
        r19 = r14.token();
        r20 = 16;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x00d1;
    L_0x00c7:
        r19 = com.alibaba.fastjson.parser.Feature.AllowArbitraryCommas;
        r0 = r19;
        r19 = r14.isEnabled(r0);
        if (r19 != 0) goto L_0x004d;
    L_0x00d1:
        r19 = 4;
        r0 = r19;
        r14.nextTokenWithColon(r0);
        r19 = com.alibaba.fastjson.JSON.DEFAULT_TYPE_KEY;
        r0 = r19;
        r19 = r0.equals(r13);
        if (r19 == 0) goto L_0x0124;
    L_0x00e2:
        r19 = r14.token();
        r20 = 4;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x011c;
    L_0x00ee:
        r11 = r14.stringVal();
        r19 = r23.getConfig();
        r20 = java.lang.Throwable.class;
        r0 = r19;
        r1 = r20;
        r10 = r0.checkAutoType(r11, r1);
        r19 = 16;
        r0 = r19;
        r14.nextToken(r0);
    L_0x0107:
        r19 = r14.token();
        r20 = 13;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x004d;
    L_0x0113:
        r19 = 16;
        r0 = r19;
        r14.nextToken(r0);
        goto L_0x006c;
    L_0x011c:
        r19 = new com.alibaba.fastjson.JSONException;
        r20 = "syntax error";
        r19.<init>(r20);
        throw r19;
    L_0x0124:
        r19 = "message";
        r0 = r19;
        r19 = r0.equals(r13);
        if (r19 == 0) goto L_0x0158;
    L_0x012e:
        r19 = r14.token();
        r20 = 8;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x013f;
    L_0x013a:
        r15 = 0;
    L_0x013b:
        r14.nextToken();
        goto L_0x0107;
    L_0x013f:
        r19 = r14.token();
        r20 = 4;
        r0 = r19;
        r1 = r20;
        if (r0 != r1) goto L_0x0150;
    L_0x014b:
        r15 = r14.stringVal();
        goto L_0x013b;
    L_0x0150:
        r19 = new com.alibaba.fastjson.JSONException;
        r20 = "syntax error";
        r19.<init>(r20);
        throw r19;
    L_0x0158:
        r19 = "cause";
        r0 = r19;
        r19 = r0.equals(r13);
        if (r19 == 0) goto L_0x0175;
    L_0x0162:
        r19 = 0;
        r20 = "cause";
        r0 = r22;
        r1 = r23;
        r2 = r19;
        r3 = r20;
        r4 = r0.deserialze(r1, r2, r3);
        r4 = (java.lang.Throwable) r4;
        goto L_0x0107;
    L_0x0175:
        r19 = "stackTrace";
        r0 = r19;
        r19 = r0.equals(r13);
        if (r19 == 0) goto L_0x018d;
    L_0x017f:
        r19 = java.lang.StackTraceElement[].class;
        r0 = r23;
        r1 = r19;
        r17 = r0.parseObject(r1);
        r17 = (java.lang.StackTraceElement[]) r17;
        goto L_0x0107;
    L_0x018d:
        r19 = r23.parse();
        r0 = r16;
        r1 = r19;
        r0.put(r13, r1);
        goto L_0x0107;
    L_0x019a:
        r19 = java.lang.Throwable.class;
        r0 = r19;
        r19 = r0.isAssignableFrom(r10);
        if (r19 != 0) goto L_0x01c1;
    L_0x01a4:
        r19 = new com.alibaba.fastjson.JSONException;
        r20 = new java.lang.StringBuilder;
        r20.<init>();
        r21 = "type not match, not Throwable. ";
        r20 = r20.append(r21);
        r21 = r10.getName();
        r20 = r20.append(r21);
        r20 = r20.toString();
        r19.<init>(r20);
        throw r19;
    L_0x01c1:
        r0 = r22;
        r8 = r0.createException(r15, r4, r10);	 Catch:{ Exception -> 0x01d1 }
        if (r8 != 0) goto L_0x0074;
    L_0x01c9:
        r9 = new java.lang.Exception;	 Catch:{ Exception -> 0x01d1 }
        r9.<init>(r15, r4);	 Catch:{ Exception -> 0x01d1 }
        r8 = r9;
        goto L_0x0074;
    L_0x01d1:
        r6 = move-exception;
        r19 = new com.alibaba.fastjson.JSONException;
        r20 = "create instance error";
        r0 = r19;
        r1 = r20;
        r0.<init>(r1, r6);
        throw r19;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.deserializer.ThrowableDeserializer.deserialze(com.alibaba.fastjson.parser.DefaultJSONParser, java.lang.reflect.Type, java.lang.Object):T");
    }

    private Throwable createException(String message, Throwable cause, Class<?> exClass) throws Exception {
        Constructor<?> defaultConstructor = null;
        Constructor<?> messageConstructor = null;
        Constructor<?> causeConstructor = null;
        for (Constructor<?> constructor : exClass.getConstructors()) {
            Class<?>[] types = constructor.getParameterTypes();
            if (types.length == 0) {
                defaultConstructor = constructor;
            } else if (types.length == 1 && types[0] == String.class) {
                messageConstructor = constructor;
            } else if (types.length == 2 && types[0] == String.class && types[1] == Throwable.class) {
                causeConstructor = constructor;
            }
        }
        if (causeConstructor != null) {
            return (Throwable) causeConstructor.newInstance(new Object[]{message, cause});
        } else if (messageConstructor != null) {
            return (Throwable) messageConstructor.newInstance(new Object[]{message});
        } else if (defaultConstructor != null) {
            return (Throwable) defaultConstructor.newInstance(new Object[0]);
        } else {
            return null;
        }
    }

    public int getFastMatchToken() {
        return 12;
    }
}
