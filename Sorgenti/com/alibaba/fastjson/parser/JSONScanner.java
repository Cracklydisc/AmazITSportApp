package com.alibaba.fastjson.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.util.ASMUtils;
import com.alibaba.fastjson.util.IOUtils;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class JSONScanner extends JSONLexerBase {
    private final int len;
    private final String text;

    public JSONScanner(String input) {
        this(input, JSON.DEFAULT_PARSER_FEATURE);
    }

    public JSONScanner(String input, int features) {
        super(features);
        this.text = input;
        this.len = this.text.length();
        this.bp = -1;
        next();
        if (this.ch == '﻿') {
            next();
        }
    }

    public final char charAt(int index) {
        if (index >= this.len) {
            return '\u001a';
        }
        return this.text.charAt(index);
    }

    public final char next() {
        char c;
        int index = this.bp + 1;
        this.bp = index;
        if (index >= this.len) {
            c = '\u001a';
        } else {
            c = this.text.charAt(index);
        }
        this.ch = c;
        return c;
    }

    protected final void copyTo(int offset, int count, char[] dest) {
        this.text.getChars(offset, offset + count, dest, 0);
    }

    static boolean charArrayCompare(String src, int offset, char[] dest) {
        int destLen = dest.length;
        if (destLen + offset > src.length()) {
            return false;
        }
        for (int i = 0; i < destLen; i++) {
            if (dest[i] != src.charAt(offset + i)) {
                return false;
            }
        }
        return true;
    }

    public final boolean charArrayCompare(char[] chars) {
        return charArrayCompare(this.text, this.bp, chars);
    }

    public final int indexOf(char ch, int startIndex) {
        return this.text.indexOf(ch, startIndex);
    }

    public final String addSymbol(int offset, int len, int hash, SymbolTable symbolTable) {
        return symbolTable.addSymbol(this.text, offset, len, hash);
    }

    public byte[] bytesValue() {
        if (this.token != 26) {
            return IOUtils.decodeBase64(this.text, this.np + 1, this.sp);
        }
        int start = this.np + 1;
        int len = this.sp;
        if (len % 2 != 0) {
            throw new JSONException("illegal state. " + len);
        }
        byte[] bArr = new byte[(len / 2)];
        for (int i = 0; i < bArr.length; i++) {
            int i2;
            char c0 = this.text.charAt((i * 2) + start);
            char c1 = this.text.charAt(((i * 2) + start) + 1);
            if (c0 <= '9') {
                i2 = 48;
            } else {
                i2 = 55;
            }
            int b0 = c0 - i2;
            if (c1 <= '9') {
                i2 = 48;
            } else {
                i2 = 55;
            }
            bArr[i] = (byte) ((b0 << 4) | (c1 - i2));
        }
        return bArr;
    }

    public final String stringVal() {
        if (this.hasSpecial) {
            return new String(this.sbuf, 0, this.sp);
        }
        return subString(this.np + 1, this.sp);
    }

    public final String subString(int offset, int count) {
        if (!ASMUtils.IS_ANDROID) {
            return this.text.substring(offset, offset + count);
        }
        if (count < this.sbuf.length) {
            this.text.getChars(offset, offset + count, this.sbuf, 0);
            return new String(this.sbuf, 0, count);
        }
        char[] chars = new char[count];
        this.text.getChars(offset, offset + count, chars, 0);
        return new String(chars);
    }

    public final char[] sub_chars(int offset, int count) {
        if (!ASMUtils.IS_ANDROID || count >= this.sbuf.length) {
            char[] chars = new char[count];
            this.text.getChars(offset, offset + count, chars, 0);
            return chars;
        }
        this.text.getChars(offset, offset + count, this.sbuf, 0);
        return this.sbuf;
    }

    public final String numberString() {
        char chLocal = charAt((this.np + this.sp) - 1);
        int sp = this.sp;
        if (chLocal == 'L' || chLocal == 'S' || chLocal == 'B' || chLocal == 'F' || chLocal == 'D') {
            sp--;
        }
        return subString(this.np, sp);
    }

    public final BigDecimal decimalValue() {
        char chLocal = charAt((this.np + this.sp) - 1);
        int sp = this.sp;
        if (chLocal == 'L' || chLocal == 'S' || chLocal == 'B' || chLocal == 'F' || chLocal == 'D') {
            sp--;
        }
        int offset = this.np;
        int count = sp;
        if (count < this.sbuf.length) {
            this.text.getChars(offset, offset + count, this.sbuf, 0);
            return new BigDecimal(this.sbuf, 0, count);
        }
        char[] chars = new char[count];
        this.text.getChars(offset, offset + count, chars, 0);
        return new BigDecimal(chars);
    }

    public boolean scanISO8601DateIfMatch() {
        return scanISO8601DateIfMatch(true);
    }

    public boolean scanISO8601DateIfMatch(boolean strict) {
        return scanISO8601DateIfMatch(strict, this.len - this.bp);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean scanISO8601DateIfMatch(boolean r75, int r76) {
        /*
        r74 = this;
        r12 = 8;
        r0 = r76;
        if (r0 >= r12) goto L_0x0008;
    L_0x0006:
        r12 = 0;
    L_0x0007:
        return r12;
    L_0x0008:
        r0 = r74;
        r12 = r0.bp;
        r0 = r74;
        r32 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 1;
        r0 = r74;
        r33 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 2;
        r0 = r74;
        r38 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 3;
        r0 = r74;
        r39 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 4;
        r0 = r74;
        r40 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 5;
        r0 = r74;
        r41 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 6;
        r0 = r74;
        r42 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 7;
        r0 = r74;
        r43 = r0.charAt(r12);
        if (r75 != 0) goto L_0x012e;
    L_0x0068:
        r12 = 13;
        r0 = r76;
        if (r0 <= r12) goto L_0x012e;
    L_0x006e:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r76;
        r12 = r12 + -1;
        r0 = r74;
        r47 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r76;
        r12 = r12 + -2;
        r0 = r74;
        r48 = r0.charAt(r12);
        r12 = 47;
        r0 = r32;
        if (r0 != r12) goto L_0x012e;
    L_0x0090:
        r12 = 68;
        r0 = r33;
        if (r0 != r12) goto L_0x012e;
    L_0x0096:
        r12 = 97;
        r0 = r38;
        if (r0 != r12) goto L_0x012e;
    L_0x009c:
        r12 = 116; // 0x74 float:1.63E-43 double:5.73E-322;
        r0 = r39;
        if (r0 != r12) goto L_0x012e;
    L_0x00a2:
        r12 = 101; // 0x65 float:1.42E-43 double:5.0E-322;
        r0 = r40;
        if (r0 != r12) goto L_0x012e;
    L_0x00a8:
        r12 = 40;
        r0 = r41;
        if (r0 != r12) goto L_0x012e;
    L_0x00ae:
        r12 = 47;
        r0 = r47;
        if (r0 != r12) goto L_0x012e;
    L_0x00b4:
        r12 = 41;
        r0 = r48;
        if (r0 != r12) goto L_0x012e;
    L_0x00ba:
        r60 = -1;
        r53 = 6;
    L_0x00be:
        r0 = r53;
        r1 = r76;
        if (r0 >= r1) goto L_0x00e7;
    L_0x00c4:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r53;
        r0 = r74;
        r31 = r0.charAt(r12);
        r12 = 43;
        r0 = r31;
        if (r0 != r12) goto L_0x00db;
    L_0x00d6:
        r60 = r53;
    L_0x00d8:
        r53 = r53 + 1;
        goto L_0x00be;
    L_0x00db:
        r12 = 48;
        r0 = r31;
        if (r0 < r12) goto L_0x00e7;
    L_0x00e1:
        r12 = 57;
        r0 = r31;
        if (r0 <= r12) goto L_0x00d8;
    L_0x00e7:
        r12 = -1;
        r0 = r60;
        if (r0 != r12) goto L_0x00ef;
    L_0x00ec:
        r12 = 0;
        goto L_0x0007;
    L_0x00ef:
        r0 = r74;
        r12 = r0.bp;
        r59 = r12 + 6;
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r60;
        r12 = r12 - r59;
        r0 = r74;
        r1 = r59;
        r58 = r0.subString(r1, r12);
        r54 = java.lang.Long.parseLong(r58);
        r0 = r74;
        r12 = r0.timeZone;
        r0 = r74;
        r0 = r0.locale;
        r19 = r0;
        r0 = r19;
        r12 = java.util.Calendar.getInstance(r12, r0);
        r0 = r74;
        r0.calendar = r12;
        r0 = r74;
        r12 = r0.calendar;
        r0 = r54;
        r12.setTimeInMillis(r0);
        r12 = 5;
        r0 = r74;
        r0.token = r12;
        r12 = 1;
        goto L_0x0007;
    L_0x012e:
        r12 = 8;
        r0 = r76;
        if (r0 == r12) goto L_0x0170;
    L_0x0134:
        r12 = 14;
        r0 = r76;
        if (r0 == r12) goto L_0x0170;
    L_0x013a:
        r12 = 16;
        r0 = r76;
        if (r0 != r12) goto L_0x0158;
    L_0x0140:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 10;
        r0 = r74;
        r34 = r0.charAt(r12);
        r12 = 84;
        r0 = r34;
        if (r0 == r12) goto L_0x0170;
    L_0x0152:
        r12 = 32;
        r0 = r34;
        if (r0 == r12) goto L_0x0170;
    L_0x0158:
        r12 = 17;
        r0 = r76;
        if (r0 != r12) goto L_0x036b;
    L_0x015e:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 6;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 45;
        r0 = r19;
        if (r12 == r0) goto L_0x036b;
    L_0x0170:
        if (r75 == 0) goto L_0x0175;
    L_0x0172:
        r12 = 0;
        goto L_0x0007;
    L_0x0175:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 8;
        r0 = r74;
        r44 = r0.charAt(r12);
        r12 = 45;
        r0 = r40;
        if (r0 != r12) goto L_0x01ca;
    L_0x0187:
        r12 = 45;
        r0 = r43;
        if (r0 != r12) goto L_0x01ca;
    L_0x018d:
        r46 = 1;
    L_0x018f:
        if (r46 == 0) goto L_0x01cd;
    L_0x0191:
        r12 = 16;
        r0 = r76;
        if (r0 != r12) goto L_0x01cd;
    L_0x0197:
        r62 = 1;
    L_0x0199:
        if (r46 == 0) goto L_0x01d0;
    L_0x019b:
        r12 = 17;
        r0 = r76;
        if (r0 != r12) goto L_0x01d0;
    L_0x01a1:
        r63 = 1;
    L_0x01a3:
        if (r63 != 0) goto L_0x01a7;
    L_0x01a5:
        if (r62 == 0) goto L_0x01d3;
    L_0x01a7:
        r4 = r32;
        r5 = r33;
        r6 = r38;
        r7 = r39;
        r8 = r41;
        r9 = r42;
        r10 = r44;
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 9;
        r0 = r74;
        r11 = r0.charAt(r12);
    L_0x01c1:
        r12 = checkDate(r4, r5, r6, r7, r8, r9, r10, r11);
        if (r12 != 0) goto L_0x01e4;
    L_0x01c7:
        r12 = 0;
        goto L_0x0007;
    L_0x01ca:
        r46 = 0;
        goto L_0x018f;
    L_0x01cd:
        r62 = 0;
        goto L_0x0199;
    L_0x01d0:
        r63 = 0;
        goto L_0x01a3;
    L_0x01d3:
        r4 = r32;
        r5 = r33;
        r6 = r38;
        r7 = r39;
        r8 = r40;
        r9 = r41;
        r10 = r42;
        r11 = r43;
        goto L_0x01c1;
    L_0x01e4:
        r12 = r74;
        r13 = r4;
        r14 = r5;
        r15 = r6;
        r16 = r7;
        r17 = r8;
        r18 = r9;
        r19 = r10;
        r20 = r11;
        r12.setCalendar(r13, r14, r15, r16, r17, r18, r19, r20);
        r12 = 8;
        r0 = r76;
        if (r0 == r12) goto L_0x0362;
    L_0x01fc:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 9;
        r0 = r74;
        r45 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 10;
        r0 = r74;
        r34 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 11;
        r0 = r74;
        r35 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 12;
        r0 = r74;
        r36 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 13;
        r0 = r74;
        r37 = r0.charAt(r12);
        if (r63 == 0) goto L_0x0258;
    L_0x023a:
        r12 = 84;
        r0 = r34;
        if (r0 != r12) goto L_0x0258;
    L_0x0240:
        r12 = 58;
        r0 = r37;
        if (r0 != r12) goto L_0x0258;
    L_0x0246:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 16;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 90;
        r0 = r19;
        if (r12 == r0) goto L_0x026c;
    L_0x0258:
        if (r62 == 0) goto L_0x0297;
    L_0x025a:
        r12 = 32;
        r0 = r34;
        if (r0 == r12) goto L_0x0266;
    L_0x0260:
        r12 = 84;
        r0 = r34;
        if (r0 != r12) goto L_0x0297;
    L_0x0266:
        r12 = 58;
        r0 = r37;
        if (r0 != r12) goto L_0x0297;
    L_0x026c:
        r13 = r35;
        r14 = r36;
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 14;
        r0 = r74;
        r15 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 15;
        r0 = r74;
        r16 = r0.charAt(r12);
        r17 = 48;
        r18 = 48;
    L_0x028c:
        r12 = r74;
        r12 = r12.checkTime(r13, r14, r15, r16, r17, r18);
        if (r12 != 0) goto L_0x02a4;
    L_0x0294:
        r12 = 0;
        goto L_0x0007;
    L_0x0297:
        r13 = r44;
        r14 = r45;
        r15 = r34;
        r16 = r35;
        r17 = r36;
        r18 = r37;
        goto L_0x028c;
    L_0x02a4:
        r12 = 17;
        r0 = r76;
        if (r0 != r12) goto L_0x035f;
    L_0x02aa:
        if (r63 != 0) goto L_0x035f;
    L_0x02ac:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 14;
        r0 = r74;
        r28 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 15;
        r0 = r74;
        r29 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 16;
        r0 = r74;
        r30 = r0.charAt(r12);
        r12 = 48;
        r0 = r28;
        if (r0 < r12) goto L_0x02dc;
    L_0x02d6:
        r12 = 57;
        r0 = r28;
        if (r0 <= r12) goto L_0x02df;
    L_0x02dc:
        r12 = 0;
        goto L_0x0007;
    L_0x02df:
        r12 = 48;
        r0 = r29;
        if (r0 < r12) goto L_0x02eb;
    L_0x02e5:
        r12 = 57;
        r0 = r29;
        if (r0 <= r12) goto L_0x02ee;
    L_0x02eb:
        r12 = 0;
        goto L_0x0007;
    L_0x02ee:
        r12 = 48;
        r0 = r30;
        if (r0 < r12) goto L_0x02fa;
    L_0x02f4:
        r12 = 57;
        r0 = r30;
        if (r0 <= r12) goto L_0x02fd;
    L_0x02fa:
        r12 = 0;
        goto L_0x0007;
    L_0x02fd:
        r12 = r28 + -48;
        r12 = r12 * 100;
        r19 = r29 + -48;
        r19 = r19 * 10;
        r12 = r12 + r19;
        r19 = r30 + -48;
        r54 = r12 + r19;
    L_0x030b:
        r12 = r13 + -48;
        r12 = r12 * 10;
        r19 = r14 + -48;
        r52 = r12 + r19;
        r12 = r15 + -48;
        r12 = r12 * 10;
        r19 = r16 + -48;
        r57 = r12 + r19;
        r12 = r17 + -48;
        r12 = r12 * 10;
        r19 = r18 + -48;
        r61 = r12 + r19;
    L_0x0323:
        r0 = r74;
        r12 = r0.calendar;
        r19 = 11;
        r0 = r19;
        r1 = r52;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 12;
        r0 = r19;
        r1 = r57;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 13;
        r0 = r19;
        r1 = r61;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 14;
        r0 = r19;
        r1 = r54;
        r12.set(r0, r1);
        r12 = 5;
        r0 = r74;
        r0.token = r12;
        r12 = 1;
        goto L_0x0007;
    L_0x035f:
        r54 = 0;
        goto L_0x030b;
    L_0x0362:
        r52 = 0;
        r57 = 0;
        r61 = 0;
        r54 = 0;
        goto L_0x0323;
    L_0x036b:
        r12 = 9;
        r0 = r76;
        if (r0 >= r12) goto L_0x0374;
    L_0x0371:
        r12 = 0;
        goto L_0x0007;
    L_0x0374:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 8;
        r0 = r74;
        r44 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 9;
        r0 = r74;
        r45 = r0.charAt(r12);
        r49 = 10;
        r12 = 45;
        r0 = r40;
        if (r0 != r12) goto L_0x039a;
    L_0x0394:
        r12 = 45;
        r0 = r43;
        if (r0 == r12) goto L_0x03a6;
    L_0x039a:
        r12 = 47;
        r0 = r40;
        if (r0 != r12) goto L_0x03bf;
    L_0x03a0:
        r12 = 47;
        r0 = r43;
        if (r0 != r12) goto L_0x03bf;
    L_0x03a6:
        r4 = r32;
        r5 = r33;
        r6 = r38;
        r7 = r39;
        r8 = r41;
        r9 = r42;
        r10 = r44;
        r11 = r45;
    L_0x03b6:
        r12 = checkDate(r4, r5, r6, r7, r8, r9, r10, r11);
        if (r12 != 0) goto L_0x04bd;
    L_0x03bc:
        r12 = 0;
        goto L_0x0007;
    L_0x03bf:
        r12 = 45;
        r0 = r40;
        if (r0 != r12) goto L_0x03eb;
    L_0x03c5:
        r12 = 45;
        r0 = r42;
        if (r0 != r12) goto L_0x03eb;
    L_0x03cb:
        r4 = r32;
        r5 = r33;
        r6 = r38;
        r7 = r39;
        r8 = 48;
        r9 = r41;
        r12 = 32;
        r0 = r44;
        if (r0 != r12) goto L_0x03e4;
    L_0x03dd:
        r10 = 48;
        r11 = r43;
        r49 = 8;
        goto L_0x03b6;
    L_0x03e4:
        r10 = r43;
        r11 = r44;
        r49 = 9;
        goto L_0x03b6;
    L_0x03eb:
        r12 = 46;
        r0 = r38;
        if (r0 != r12) goto L_0x03f7;
    L_0x03f1:
        r12 = 46;
        r0 = r41;
        if (r0 == r12) goto L_0x0403;
    L_0x03f7:
        r12 = 45;
        r0 = r38;
        if (r0 != r12) goto L_0x0414;
    L_0x03fd:
        r12 = 45;
        r0 = r41;
        if (r0 != r12) goto L_0x0414;
    L_0x0403:
        r10 = r32;
        r11 = r33;
        r8 = r39;
        r9 = r40;
        r4 = r42;
        r5 = r43;
        r6 = r44;
        r7 = r45;
        goto L_0x03b6;
    L_0x0414:
        r12 = 24180; // 0x5e74 float:3.3883E-41 double:1.19465E-319;
        r0 = r40;
        if (r0 == r12) goto L_0x0421;
    L_0x041a:
        r12 = 45380; // 0xb144 float:6.3591E-41 double:2.24207E-319;
        r0 = r40;
        if (r0 != r12) goto L_0x04ba;
    L_0x0421:
        r4 = r32;
        r5 = r33;
        r6 = r38;
        r7 = r39;
        r12 = 26376; // 0x6708 float:3.696E-41 double:1.30315E-319;
        r0 = r43;
        if (r0 == r12) goto L_0x0436;
    L_0x042f:
        r12 = 50900; // 0xc6d4 float:7.1326E-41 double:2.5148E-319;
        r0 = r43;
        if (r0 != r12) goto L_0x047d;
    L_0x0436:
        r8 = r41;
        r9 = r42;
        r12 = 26085; // 0x65e5 float:3.6553E-41 double:1.28877E-319;
        r0 = r45;
        if (r0 == r12) goto L_0x0447;
    L_0x0440:
        r12 = 51068; // 0xc77c float:7.1562E-41 double:2.5231E-319;
        r0 = r45;
        if (r0 != r12) goto L_0x044d;
    L_0x0447:
        r10 = 48;
        r11 = r44;
        goto L_0x03b6;
    L_0x044d:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 10;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 26085; // 0x65e5 float:3.6553E-41 double:1.28877E-319;
        r0 = r19;
        if (r12 == r0) goto L_0x0472;
    L_0x045f:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + 10;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 51068; // 0xc77c float:7.1562E-41 double:2.5231E-319;
        r0 = r19;
        if (r12 != r0) goto L_0x047a;
    L_0x0472:
        r10 = r44;
        r11 = r45;
        r49 = 11;
        goto L_0x03b6;
    L_0x047a:
        r12 = 0;
        goto L_0x0007;
    L_0x047d:
        r12 = 26376; // 0x6708 float:3.696E-41 double:1.30315E-319;
        r0 = r42;
        if (r0 == r12) goto L_0x048a;
    L_0x0483:
        r12 = 50900; // 0xc6d4 float:7.1326E-41 double:2.5148E-319;
        r0 = r42;
        if (r0 != r12) goto L_0x04b7;
    L_0x048a:
        r8 = 48;
        r9 = r41;
        r12 = 26085; // 0x65e5 float:3.6553E-41 double:1.28877E-319;
        r0 = r44;
        if (r0 == r12) goto L_0x049b;
    L_0x0494:
        r12 = 51068; // 0xc77c float:7.1562E-41 double:2.5231E-319;
        r0 = r44;
        if (r0 != r12) goto L_0x04a1;
    L_0x049b:
        r10 = 48;
        r11 = r43;
        goto L_0x03b6;
    L_0x04a1:
        r12 = 26085; // 0x65e5 float:3.6553E-41 double:1.28877E-319;
        r0 = r45;
        if (r0 == r12) goto L_0x04ae;
    L_0x04a7:
        r12 = 51068; // 0xc77c float:7.1562E-41 double:2.5231E-319;
        r0 = r45;
        if (r0 != r12) goto L_0x04b4;
    L_0x04ae:
        r10 = r43;
        r11 = r44;
        goto L_0x03b6;
    L_0x04b4:
        r12 = 0;
        goto L_0x0007;
    L_0x04b7:
        r12 = 0;
        goto L_0x0007;
    L_0x04ba:
        r12 = 0;
        goto L_0x0007;
    L_0x04bd:
        r19 = r74;
        r20 = r4;
        r21 = r5;
        r22 = r6;
        r23 = r7;
        r24 = r8;
        r25 = r9;
        r26 = r10;
        r27 = r11;
        r19.setCalendar(r20, r21, r22, r23, r24, r25, r26, r27);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r0 = r74;
        r64 = r0.charAt(r12);
        r12 = 84;
        r0 = r64;
        if (r0 == r12) goto L_0x04ec;
    L_0x04e4:
        r12 = 32;
        r0 = r64;
        if (r0 != r12) goto L_0x04f5;
    L_0x04ea:
        if (r75 != 0) goto L_0x04f5;
    L_0x04ec:
        r12 = r49 + 9;
        r0 = r76;
        if (r0 >= r12) goto L_0x060d;
    L_0x04f2:
        r12 = 0;
        goto L_0x0007;
    L_0x04f5:
        r12 = 34;
        r0 = r64;
        if (r0 == r12) goto L_0x050e;
    L_0x04fb:
        r12 = 26;
        r0 = r64;
        if (r0 == r12) goto L_0x050e;
    L_0x0501:
        r12 = 26085; // 0x65e5 float:3.6553E-41 double:1.28877E-319;
        r0 = r64;
        if (r0 == r12) goto L_0x050e;
    L_0x0507:
        r12 = 51068; // 0xc77c float:7.1562E-41 double:2.5231E-319;
        r0 = r64;
        if (r0 != r12) goto L_0x0566;
    L_0x050e:
        r0 = r74;
        r12 = r0.calendar;
        r19 = 11;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 12;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 13;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 14;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r0 = r74;
        r0.bp = r12;
        r0 = r74;
        r12 = r0.charAt(r12);
        r0 = r74;
        r0.ch = r12;
        r12 = 5;
        r0 = r74;
        r0.token = r12;
        r12 = 1;
        goto L_0x0007;
    L_0x0566:
        r12 = 43;
        r0 = r64;
        if (r0 == r12) goto L_0x0572;
    L_0x056c:
        r12 = 45;
        r0 = r64;
        if (r0 != r12) goto L_0x060a;
    L_0x0572:
        r0 = r74;
        r12 = r0.len;
        r19 = r49 + 6;
        r0 = r19;
        if (r12 != r0) goto L_0x0607;
    L_0x057c:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 3;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 58;
        r0 = r19;
        if (r12 != r0) goto L_0x05b8;
    L_0x0590:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 4;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 48;
        r0 = r19;
        if (r12 != r0) goto L_0x05b8;
    L_0x05a4:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 5;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 48;
        r0 = r19;
        if (r12 == r0) goto L_0x05bb;
    L_0x05b8:
        r12 = 0;
        goto L_0x0007;
    L_0x05bb:
        r20 = 48;
        r21 = 48;
        r22 = 48;
        r23 = 48;
        r24 = 48;
        r25 = 48;
        r19 = r74;
        r19.setTime(r20, r21, r22, r23, r24, r25);
        r0 = r74;
        r12 = r0.calendar;
        r19 = 14;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 1;
        r0 = r74;
        r12 = r0.charAt(r12);
        r0 = r74;
        r0 = r0.bp;
        r19 = r0;
        r19 = r19 + r49;
        r19 = r19 + 2;
        r0 = r74;
        r1 = r19;
        r19 = r0.charAt(r1);
        r0 = r74;
        r1 = r64;
        r2 = r19;
        r0.setTimeZone(r1, r12, r2);
        r12 = 1;
        goto L_0x0007;
    L_0x0607:
        r12 = 0;
        goto L_0x0007;
    L_0x060a:
        r12 = 0;
        goto L_0x0007;
    L_0x060d:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 3;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 58;
        r0 = r19;
        if (r12 == r0) goto L_0x0624;
    L_0x0621:
        r12 = 0;
        goto L_0x0007;
    L_0x0624:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 6;
        r0 = r74;
        r12 = r0.charAt(r12);
        r19 = 58;
        r0 = r19;
        if (r12 == r0) goto L_0x063b;
    L_0x0638:
        r12 = 0;
        goto L_0x0007;
    L_0x063b:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 1;
        r0 = r74;
        r13 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 2;
        r0 = r74;
        r14 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 4;
        r0 = r74;
        r15 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 5;
        r0 = r74;
        r16 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 7;
        r0 = r74;
        r17 = r0.charAt(r12);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 8;
        r0 = r74;
        r18 = r0.charAt(r12);
        r12 = r74;
        r12 = r12.checkTime(r13, r14, r15, r16, r17, r18);
        if (r12 != 0) goto L_0x069a;
    L_0x0697:
        r12 = 0;
        goto L_0x0007;
    L_0x069a:
        r12 = r74;
        r12.setTime(r13, r14, r15, r16, r17, r18);
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 9;
        r0 = r74;
        r50 = r0.charAt(r12);
        r12 = 46;
        r0 = r50;
        if (r0 != r12) goto L_0x06bc;
    L_0x06b3:
        r12 = r49 + 11;
        r0 = r76;
        if (r0 >= r12) goto L_0x0717;
    L_0x06b9:
        r12 = 0;
        goto L_0x0007;
    L_0x06bc:
        r0 = r74;
        r12 = r0.calendar;
        r19 = 14;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r12.set(r0, r1);
        r0 = r74;
        r12 = r0.bp;
        r19 = r49 + 9;
        r12 = r12 + r19;
        r0 = r74;
        r0.bp = r12;
        r0 = r74;
        r12 = r0.charAt(r12);
        r0 = r74;
        r0.ch = r12;
        r12 = 5;
        r0 = r74;
        r0.token = r12;
        r12 = 90;
        r0 = r50;
        if (r0 != r12) goto L_0x0714;
    L_0x06ec:
        r0 = r74;
        r12 = r0.calendar;
        r12 = r12.getTimeZone();
        r12 = r12.getRawOffset();
        if (r12 == 0) goto L_0x0714;
    L_0x06fa:
        r12 = 0;
        r72 = java.util.TimeZone.getAvailableIDs(r12);
        r0 = r72;
        r12 = r0.length;
        if (r12 <= 0) goto L_0x0714;
    L_0x0704:
        r12 = 0;
        r12 = r72[r12];
        r70 = java.util.TimeZone.getTimeZone(r12);
        r0 = r74;
        r12 = r0.calendar;
        r0 = r70;
        r12.setTimeZone(r0);
    L_0x0714:
        r12 = 1;
        goto L_0x0007;
    L_0x0717:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r0 = r74;
        r28 = r0.charAt(r12);
        r12 = 48;
        r0 = r28;
        if (r0 < r12) goto L_0x0731;
    L_0x072b:
        r12 = 57;
        r0 = r28;
        if (r0 <= r12) goto L_0x0734;
    L_0x0731:
        r12 = 0;
        goto L_0x0007;
    L_0x0734:
        r54 = r28 + -48;
        r56 = 1;
        r12 = r49 + 11;
        r0 = r76;
        if (r0 <= r12) goto L_0x0760;
    L_0x073e:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 11;
        r0 = r74;
        r29 = r0.charAt(r12);
        r12 = 48;
        r0 = r29;
        if (r0 < r12) goto L_0x0760;
    L_0x0752:
        r12 = 57;
        r0 = r29;
        if (r0 > r12) goto L_0x0760;
    L_0x0758:
        r12 = r54 * 10;
        r19 = r29 + -48;
        r54 = r12 + r19;
        r56 = 2;
    L_0x0760:
        r12 = 2;
        r0 = r56;
        if (r0 != r12) goto L_0x0787;
    L_0x0765:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 12;
        r0 = r74;
        r30 = r0.charAt(r12);
        r12 = 48;
        r0 = r30;
        if (r0 < r12) goto L_0x0787;
    L_0x0779:
        r12 = 57;
        r0 = r30;
        if (r0 > r12) goto L_0x0787;
    L_0x077f:
        r12 = r54 * 10;
        r19 = r30 + -48;
        r54 = r12 + r19;
        r56 = 3;
    L_0x0787:
        r0 = r74;
        r12 = r0.calendar;
        r19 = 14;
        r0 = r19;
        r1 = r54;
        r12.set(r0, r1);
        r73 = 0;
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r0 = r74;
        r71 = r0.charAt(r12);
        r12 = 43;
        r0 = r71;
        if (r0 == r12) goto L_0x07b2;
    L_0x07ac:
        r12 = 45;
        r0 = r71;
        if (r0 != r12) goto L_0x0897;
    L_0x07b2:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 1;
        r0 = r74;
        r65 = r0.charAt(r12);
        r12 = 48;
        r0 = r65;
        if (r0 < r12) goto L_0x07d0;
    L_0x07ca:
        r12 = 49;
        r0 = r65;
        if (r0 <= r12) goto L_0x07d3;
    L_0x07d0:
        r12 = 0;
        goto L_0x0007;
    L_0x07d3:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 2;
        r0 = r74;
        r66 = r0.charAt(r12);
        r12 = 48;
        r0 = r66;
        if (r0 < r12) goto L_0x07f1;
    L_0x07eb:
        r12 = 57;
        r0 = r66;
        if (r0 <= r12) goto L_0x07f4;
    L_0x07f1:
        r12 = 0;
        goto L_0x0007;
    L_0x07f4:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 3;
        r0 = r74;
        r67 = r0.charAt(r12);
        r12 = 58;
        r0 = r67;
        if (r0 != r12) goto L_0x0870;
    L_0x080c:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 4;
        r0 = r74;
        r68 = r0.charAt(r12);
        r12 = 48;
        r0 = r68;
        if (r0 == r12) goto L_0x0827;
    L_0x0824:
        r12 = 0;
        goto L_0x0007;
    L_0x0827:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 5;
        r0 = r74;
        r69 = r0.charAt(r12);
        r12 = 48;
        r0 = r69;
        if (r0 == r12) goto L_0x0842;
    L_0x083f:
        r12 = 0;
        goto L_0x0007;
    L_0x0842:
        r73 = 6;
    L_0x0844:
        r0 = r74;
        r1 = r71;
        r2 = r65;
        r3 = r66;
        r0.setTimeZone(r1, r2, r3);
    L_0x084f:
        r0 = r74;
        r12 = r0.bp;
        r19 = r49 + 10;
        r19 = r19 + r56;
        r19 = r19 + r73;
        r12 = r12 + r19;
        r0 = r74;
        r51 = r0.charAt(r12);
        r12 = 26;
        r0 = r51;
        if (r0 == r12) goto L_0x08c8;
    L_0x0867:
        r12 = 34;
        r0 = r51;
        if (r0 == r12) goto L_0x08c8;
    L_0x086d:
        r12 = 0;
        goto L_0x0007;
    L_0x0870:
        r12 = 48;
        r0 = r67;
        if (r0 != r12) goto L_0x0894;
    L_0x0876:
        r0 = r74;
        r12 = r0.bp;
        r12 = r12 + r49;
        r12 = r12 + 10;
        r12 = r12 + r56;
        r12 = r12 + 4;
        r0 = r74;
        r68 = r0.charAt(r12);
        r12 = 48;
        r0 = r68;
        if (r0 == r12) goto L_0x0891;
    L_0x088e:
        r12 = 0;
        goto L_0x0007;
    L_0x0891:
        r73 = 5;
        goto L_0x0844;
    L_0x0894:
        r73 = 3;
        goto L_0x0844;
    L_0x0897:
        r12 = 90;
        r0 = r71;
        if (r0 != r12) goto L_0x084f;
    L_0x089d:
        r73 = 1;
        r0 = r74;
        r12 = r0.calendar;
        r12 = r12.getTimeZone();
        r12 = r12.getRawOffset();
        if (r12 == 0) goto L_0x084f;
    L_0x08ad:
        r12 = 0;
        r72 = java.util.TimeZone.getAvailableIDs(r12);
        r0 = r72;
        r12 = r0.length;
        if (r12 <= 0) goto L_0x084f;
    L_0x08b7:
        r12 = 0;
        r12 = r72[r12];
        r70 = java.util.TimeZone.getTimeZone(r12);
        r0 = r74;
        r12 = r0.calendar;
        r0 = r70;
        r12.setTimeZone(r0);
        goto L_0x084f;
    L_0x08c8:
        r0 = r74;
        r12 = r0.bp;
        r19 = r49 + 10;
        r19 = r19 + r56;
        r19 = r19 + r73;
        r12 = r12 + r19;
        r0 = r74;
        r0.bp = r12;
        r0 = r74;
        r12 = r0.charAt(r12);
        r0 = r74;
        r0.ch = r12;
        r12 = 5;
        r0 = r74;
        r0.token = r12;
        r12 = 1;
        goto L_0x0007;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.alibaba.fastjson.parser.JSONScanner.scanISO8601DateIfMatch(boolean, int):boolean");
    }

    protected void setTime(char h0, char h1, char m0, char m1, char s0, char s1) {
        int minute = ((m0 - 48) * 10) + (m1 - 48);
        int seconds = ((s0 - 48) * 10) + (s1 - 48);
        this.calendar.set(11, ((h0 - 48) * 10) + (h1 - 48));
        this.calendar.set(12, minute);
        this.calendar.set(13, seconds);
    }

    protected void setTimeZone(char timeZoneFlag, char t0, char t1) {
        int timeZoneOffset = ((((t0 - 48) * 10) + (t1 - 48)) * 3600) * 1000;
        if (timeZoneFlag == '-') {
            timeZoneOffset = -timeZoneOffset;
        }
        if (this.calendar.getTimeZone().getRawOffset() != timeZoneOffset) {
            String[] timeZoneIDs = TimeZone.getAvailableIDs(timeZoneOffset);
            if (timeZoneIDs.length > 0) {
                this.calendar.setTimeZone(TimeZone.getTimeZone(timeZoneIDs[0]));
            }
        }
    }

    private boolean checkTime(char h0, char h1, char m0, char m1, char s0, char s1) {
        if (h0 == '0') {
            if (h1 < '0' || h1 > '9') {
                return false;
            }
        } else if (h0 == '1') {
            if (h1 < '0' || h1 > '9') {
                return false;
            }
        } else if (h0 != '2' || h1 < '0') {
            return false;
        } else {
            if (h1 > '4') {
                return false;
            }
        }
        if (m0 < '0' || m0 > '5') {
            if (m0 != '6') {
                return false;
            }
            if (m1 != '0') {
                return false;
            }
        } else if (m1 < '0' || m1 > '9') {
            return false;
        }
        if (s0 < '0' || s0 > '5') {
            if (s0 != '6') {
                return false;
            }
            if (s1 != '0') {
                return false;
            }
        } else if (s1 < '0' || s1 > '9') {
            return false;
        }
        return true;
    }

    private void setCalendar(char y0, char y1, char y2, char y3, char M0, char M1, char d0, char d1) {
        this.calendar = Calendar.getInstance(this.timeZone, this.locale);
        int month = (((M0 - 48) * 10) + (M1 - 48)) - 1;
        int day = ((d0 - 48) * 10) + (d1 - 48);
        this.calendar.set(1, ((((y0 - 48) * 1000) + ((y1 - 48) * 100)) + ((y2 - 48) * 10)) + (y3 - 48));
        this.calendar.set(2, month);
        this.calendar.set(5, day);
    }

    static boolean checkDate(char y0, char y1, char y2, char y3, char M0, char M1, int d0, int d1) {
        if (y0 < '1' || y0 > '3' || y1 < '0' || y1 > '9' || y2 < '0' || y2 > '9' || y3 < '0' || y3 > '9') {
            return false;
        }
        if (M0 == '0') {
            if (M1 < '1' || M1 > '9') {
                return false;
            }
        } else if (M0 != '1') {
            return false;
        } else {
            if (!(M1 == '0' || M1 == '1' || M1 == '2')) {
                return false;
            }
        }
        if (d0 == 48) {
            if (d1 < 49 || d1 > 57) {
                return false;
            }
        } else if (d0 == 49 || d0 == 50) {
            if (d1 < 48) {
                return false;
            }
            if (d1 > 57) {
                return false;
            }
        } else if (d0 != 51) {
            return false;
        } else {
            if (!(d1 == 48 || d1 == 49)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEOF() {
        return this.bp == this.len || (this.ch == '\u001a' && this.bp + 1 == this.len);
    }

    public int scanFieldInt(char[] fieldName) {
        boolean negative = true;
        this.matchStat = 0;
        int startPos = this.bp;
        char startChar = this.ch;
        if (charArrayCompare(this.text, this.bp, fieldName)) {
            boolean quote;
            int index = this.bp + fieldName.length;
            int index2 = index + 1;
            char ch = charAt(index);
            if (ch == '\"') {
                quote = true;
            } else {
                quote = false;
            }
            if (quote) {
                index = index2 + 1;
                ch = charAt(index2);
                index2 = index;
            }
            if (ch != '-') {
                negative = false;
            }
            if (negative) {
                index = index2 + 1;
                ch = charAt(index2);
            } else {
                index = index2;
            }
            if (ch < '0' || ch > '9') {
                this.matchStat = -1;
                return 0;
            }
            int value = ch - 48;
            while (true) {
                index2 = index + 1;
                ch = charAt(index);
                if (ch >= '0' && ch <= '9') {
                    value = (value * 10) + (ch - 48);
                    index = index2;
                }
            }
            if (ch == '.') {
                this.matchStat = -1;
                return 0;
            } else if (value < 0) {
                this.matchStat = -1;
                return 0;
            } else {
                if (quote) {
                    if (ch != '\"') {
                        this.matchStat = -1;
                        return 0;
                    }
                    index = index2 + 1;
                    ch = charAt(index2);
                    index2 = index;
                }
                while (ch != ',' && ch != '}') {
                    if (JSONLexerBase.isWhitespace(ch)) {
                        index = index2 + 1;
                        ch = charAt(index2);
                        index2 = index;
                    } else {
                        this.matchStat = -1;
                        return 0;
                    }
                }
                this.bp = index2 - 1;
                int i;
                if (ch == ',') {
                    i = this.bp + 1;
                    this.bp = i;
                    this.ch = charAt(i);
                    this.matchStat = 3;
                    this.token = 16;
                    if (negative) {
                        return -value;
                    }
                    return value;
                }
                if (ch == '}') {
                    this.bp = index2 - 1;
                    int i2 = this.bp + 1;
                    this.bp = i2;
                    ch = charAt(i2);
                    while (ch != ',') {
                        if (ch == ']') {
                            this.token = 15;
                            i = this.bp + 1;
                            this.bp = i;
                            this.ch = charAt(i);
                            break;
                        } else if (ch == '}') {
                            this.token = 13;
                            i = this.bp + 1;
                            this.bp = i;
                            this.ch = charAt(i);
                            break;
                        } else if (ch == '\u001a') {
                            this.token = 20;
                            break;
                        } else if (JSONLexerBase.isWhitespace(ch)) {
                            i2 = this.bp + 1;
                            this.bp = i2;
                            ch = charAt(i2);
                        } else {
                            this.bp = startPos;
                            this.ch = startChar;
                            this.matchStat = -1;
                            return 0;
                        }
                    }
                    this.token = 16;
                    i = this.bp + 1;
                    this.bp = i;
                    this.ch = charAt(i);
                    this.matchStat = 4;
                }
                if (negative) {
                    return -value;
                }
                return value;
            }
        }
        this.matchStat = -2;
        return 0;
    }

    public String scanFieldString(char[] fieldName) {
        this.matchStat = 0;
        int startPos = this.bp;
        char startChar = this.ch;
        while (!charArrayCompare(this.text, this.bp, fieldName)) {
            if (JSONLexerBase.isWhitespace(this.ch)) {
                next();
            } else {
                this.matchStat = -2;
                return stringDefaultValue();
            }
        }
        int length = this.bp + fieldName.length;
        int index = length + 1;
        if (charAt(length) != '\"') {
            this.matchStat = -1;
            return stringDefaultValue();
        }
        int startIndex = index;
        int endIndex = indexOf('\"', startIndex);
        if (endIndex == -1) {
            throw new JSONException("unclosed str");
        }
        String stringVal = subString(startIndex, endIndex - startIndex);
        if (stringVal.indexOf(92) != -1) {
            while (true) {
                int slashCount = 0;
                int i = endIndex - 1;
                while (i >= 0 && charAt(i) == '\\') {
                    slashCount++;
                    i--;
                }
                if (slashCount % 2 == 0) {
                    break;
                }
                endIndex = indexOf('\"', endIndex + 1);
            }
            int chars_len = endIndex - ((this.bp + fieldName.length) + 1);
            stringVal = JSONLexerBase.readString(sub_chars((this.bp + fieldName.length) + 1, chars_len), chars_len);
        }
        char ch = charAt(endIndex + 1);
        while (ch != ',' && ch != '}') {
            if (JSONLexerBase.isWhitespace(ch)) {
                endIndex++;
                ch = charAt(endIndex + 1);
            } else {
                this.matchStat = -1;
                return stringDefaultValue();
            }
        }
        this.bp = endIndex + 1;
        this.ch = ch;
        String strVal = stringVal;
        if (ch == ',') {
            int i2 = this.bp + 1;
            this.bp = i2;
            this.ch = charAt(i2);
            this.matchStat = 3;
            return strVal;
        }
        i2 = this.bp + 1;
        this.bp = i2;
        ch = charAt(i2);
        if (ch == ',') {
            this.token = 16;
            i2 = this.bp + 1;
            this.bp = i2;
            this.ch = charAt(i2);
        } else if (ch == ']') {
            this.token = 15;
            i2 = this.bp + 1;
            this.bp = i2;
            this.ch = charAt(i2);
        } else if (ch == '}') {
            this.token = 13;
            i2 = this.bp + 1;
            this.bp = i2;
            this.ch = charAt(i2);
        } else if (ch == '\u001a') {
            this.token = 20;
        } else {
            this.bp = startPos;
            this.ch = startChar;
            this.matchStat = -1;
            return stringDefaultValue();
        }
        this.matchStat = 4;
        return strVal;
    }

    public Date scanFieldDate(char[] fieldName) {
        this.matchStat = 0;
        int startPos = this.bp;
        char startChar = this.ch;
        if (charArrayCompare(this.text, this.bp, fieldName)) {
            Date dateVal;
            int length = this.bp + fieldName.length;
            int index = length + 1;
            char ch = charAt(length);
            if (ch == '\"') {
                int startIndex = index;
                int endIndex = indexOf('\"', startIndex);
                if (endIndex == -1) {
                    throw new JSONException("unclosed str");
                }
                int rest = endIndex - startIndex;
                this.bp = index;
                if (scanISO8601DateIfMatch(false, rest)) {
                    dateVal = this.calendar.getTime();
                    ch = charAt(endIndex + 1);
                    this.bp = startPos;
                    while (ch != ',' && ch != '}') {
                        if (JSONLexerBase.isWhitespace(ch)) {
                            endIndex++;
                            ch = charAt(endIndex + 1);
                        } else {
                            this.matchStat = -1;
                            return null;
                        }
                    }
                    this.bp = endIndex + 1;
                    this.ch = ch;
                    length = index;
                } else {
                    this.bp = startPos;
                    this.matchStat = -1;
                    return null;
                }
            } else if (ch == '-' || (ch >= '0' && ch <= '9')) {
                long millis = 0;
                boolean negative = false;
                if (ch == '-') {
                    length = index + 1;
                    ch = charAt(index);
                    negative = true;
                } else {
                    length = index;
                }
                if (ch >= '0' && ch <= '9') {
                    millis = (long) (ch - 48);
                    while (true) {
                        index = length + 1;
                        ch = charAt(length);
                        if (ch >= '0' && ch <= '9') {
                            millis = (10 * millis) + ((long) (ch - 48));
                            length = index;
                        }
                    }
                    if (ch == ',' || ch == '}') {
                        this.bp = index - 1;
                        length = index;
                    }
                }
                if (millis < 0) {
                    this.matchStat = -1;
                    return null;
                }
                if (negative) {
                    millis = -millis;
                }
                dateVal = new Date(millis);
            } else {
                this.matchStat = -1;
                return null;
            }
            int i;
            if (ch == ',') {
                i = this.bp + 1;
                this.bp = i;
                this.ch = charAt(i);
                this.matchStat = 3;
                return dateVal;
            }
            i = this.bp + 1;
            this.bp = i;
            ch = charAt(i);
            if (ch == ',') {
                this.token = 16;
                i = this.bp + 1;
                this.bp = i;
                this.ch = charAt(i);
            } else if (ch == ']') {
                this.token = 15;
                i = this.bp + 1;
                this.bp = i;
                this.ch = charAt(i);
            } else if (ch == '}') {
                this.token = 13;
                i = this.bp + 1;
                this.bp = i;
                this.ch = charAt(i);
            } else if (ch == '\u001a') {
                this.token = 20;
            } else {
                this.bp = startPos;
                this.ch = startChar;
                this.matchStat = -1;
                return null;
            }
            this.matchStat = 4;
            return dateVal;
        }
        this.matchStat = -2;
        return null;
    }

    public long scanFieldSymbol(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(this.text, this.bp, fieldName)) {
            int index = this.bp + fieldName.length;
            int index2 = index + 1;
            if (charAt(index) != '\"') {
                this.matchStat = -1;
                return 0;
            }
            char ch;
            int i;
            long hash = -3750763034362895579L;
            index = index2;
            while (true) {
                index2 = index + 1;
                ch = charAt(index);
                if (ch == '\"') {
                    break;
                } else if (index2 > this.len) {
                    this.matchStat = -1;
                    return 0;
                } else {
                    hash = (hash ^ ((long) ch)) * 1099511628211L;
                    index = index2;
                }
            }
            this.bp = index2;
            ch = charAt(this.bp);
            this.ch = ch;
            while (ch != ',') {
                if (ch == '}') {
                    next();
                    skipWhitespace();
                    ch = getCurrent();
                    if (ch == ',') {
                        this.token = 16;
                        i = this.bp + 1;
                        this.bp = i;
                        this.ch = charAt(i);
                    } else if (ch == ']') {
                        this.token = 15;
                        i = this.bp + 1;
                        this.bp = i;
                        this.ch = charAt(i);
                    } else if (ch == '}') {
                        this.token = 13;
                        i = this.bp + 1;
                        this.bp = i;
                        this.ch = charAt(i);
                    } else if (ch == '\u001a') {
                        this.token = 20;
                    } else {
                        this.matchStat = -1;
                        return 0;
                    }
                    this.matchStat = 4;
                    return hash;
                } else if (JSONLexerBase.isWhitespace(ch)) {
                    i = this.bp + 1;
                    this.bp = i;
                    ch = charAt(i);
                } else {
                    this.matchStat = -1;
                    return 0;
                }
            }
            i = this.bp + 1;
            this.bp = i;
            this.ch = charAt(i);
            this.matchStat = 3;
            return hash;
        }
        this.matchStat = -2;
        return 0;
    }

    public long scanFieldLong(char[] fieldName) {
        this.matchStat = 0;
        int startPos = this.bp;
        char startChar = this.ch;
        if (charArrayCompare(this.text, this.bp, fieldName)) {
            int index = this.bp + fieldName.length;
            int index2 = index + 1;
            char ch = charAt(index);
            boolean quote = ch == '\"';
            if (quote) {
                index = index2 + 1;
                ch = charAt(index2);
                index2 = index;
            }
            boolean negative = false;
            if (ch == '-') {
                index = index2 + 1;
                ch = charAt(index2);
                negative = true;
            } else {
                index = index2;
            }
            if (ch < '0' || ch > '9') {
                this.bp = startPos;
                this.ch = startChar;
                this.matchStat = -1;
                return 0;
            }
            long value = (long) (ch - 48);
            while (true) {
                index2 = index + 1;
                ch = charAt(index);
                if (ch >= '0' && ch <= '9') {
                    value = (10 * value) + ((long) (ch - 48));
                    index = index2;
                }
            }
            if (ch == '.') {
                this.matchStat = -1;
                return 0;
            }
            if (!quote) {
                index = index2;
            } else if (ch != '\"') {
                this.matchStat = -1;
                return 0;
            } else {
                index = index2 + 1;
                ch = charAt(index2);
            }
            if (ch == ',' || ch == '}') {
                this.bp = index - 1;
            }
            boolean valid = value >= 0 || (value == Long.MIN_VALUE && negative);
            if (valid) {
                int i;
                index2 = index;
                while (ch != ',') {
                    if (ch == '}') {
                        i = this.bp + 1;
                        this.bp = i;
                        ch = charAt(i);
                        while (ch != ',') {
                            if (ch == ']') {
                                this.token = 15;
                                i = this.bp + 1;
                                this.bp = i;
                                this.ch = charAt(i);
                                break;
                            } else if (ch == '}') {
                                this.token = 13;
                                i = this.bp + 1;
                                this.bp = i;
                                this.ch = charAt(i);
                                break;
                            } else if (ch == '\u001a') {
                                this.token = 20;
                                break;
                            } else if (JSONLexerBase.isWhitespace(ch)) {
                                i = this.bp + 1;
                                this.bp = i;
                                ch = charAt(i);
                            } else {
                                this.bp = startPos;
                                this.ch = startChar;
                                this.matchStat = -1;
                                return 0;
                            }
                        }
                        this.token = 16;
                        i = this.bp + 1;
                        this.bp = i;
                        this.ch = charAt(i);
                        this.matchStat = 4;
                        if (negative) {
                            return -value;
                        }
                        return value;
                    } else if (JSONLexerBase.isWhitespace(ch)) {
                        this.bp = index2;
                        index = index2 + 1;
                        ch = charAt(index2);
                        index2 = index;
                    } else {
                        this.matchStat = -1;
                        return 0;
                    }
                }
                i = this.bp + 1;
                this.bp = i;
                this.ch = charAt(i);
                this.matchStat = 3;
                this.token = 16;
                if (negative) {
                    return -value;
                }
                return value;
            }
            this.bp = startPos;
            this.ch = startChar;
            this.matchStat = -1;
            return 0;
        }
        this.matchStat = -2;
        return 0;
    }

    public boolean scanFieldBoolean(char[] fieldName) {
        this.matchStat = 0;
        if (charArrayCompare(this.text, this.bp, fieldName)) {
            boolean quote;
            boolean value;
            int i;
            int startPos = this.bp;
            int index = this.bp + fieldName.length;
            int index2 = index + 1;
            char ch = charAt(index);
            if (ch == '\"') {
                quote = true;
            } else {
                quote = false;
            }
            if (quote) {
                index = index2 + 1;
                ch = charAt(index2);
                index2 = index;
            }
            if (ch == 't') {
                index = index2 + 1;
                if (charAt(index2) != 'r') {
                    this.matchStat = -1;
                    return false;
                }
                index2 = index + 1;
                if (charAt(index) != 'u') {
                    this.matchStat = -1;
                    return false;
                }
                index = index2 + 1;
                if (charAt(index2) != 'e') {
                    this.matchStat = -1;
                    return false;
                }
                if (quote) {
                    index2 = index + 1;
                    if (charAt(index) != '\"') {
                        this.matchStat = -1;
                        return false;
                    }
                    index = index2;
                }
                this.bp = index;
                ch = charAt(this.bp);
                value = true;
            } else if (ch == 'f') {
                index = index2 + 1;
                if (charAt(index2) != 'a') {
                    this.matchStat = -1;
                    return false;
                }
                index2 = index + 1;
                if (charAt(index) != 'l') {
                    this.matchStat = -1;
                    return false;
                }
                index = index2 + 1;
                if (charAt(index2) != 's') {
                    this.matchStat = -1;
                    return false;
                }
                index2 = index + 1;
                if (charAt(index) != 'e') {
                    this.matchStat = -1;
                    return false;
                }
                if (quote) {
                    index = index2 + 1;
                    if (charAt(index2) != '\"') {
                        this.matchStat = -1;
                        return false;
                    }
                }
                index = index2;
                this.bp = index;
                ch = charAt(this.bp);
                value = false;
            } else if (ch == '1') {
                if (quote) {
                    index = index2 + 1;
                    if (charAt(index2) != '\"') {
                        this.matchStat = -1;
                        return false;
                    }
                }
                index = index2;
                this.bp = index;
                ch = charAt(this.bp);
                value = true;
            } else if (ch == '0') {
                if (quote) {
                    index = index2 + 1;
                    if (charAt(index2) != '\"') {
                        this.matchStat = -1;
                        return false;
                    }
                }
                index = index2;
                this.bp = index;
                ch = charAt(this.bp);
                value = false;
            } else {
                this.matchStat = -1;
                return false;
            }
            while (ch != ',') {
                int i2;
                if (ch == '}') {
                    i2 = this.bp + 1;
                    this.bp = i2;
                    ch = charAt(i2);
                    while (ch != ',') {
                        if (ch == ']') {
                            this.token = 15;
                            i = this.bp + 1;
                            this.bp = i;
                            this.ch = charAt(i);
                            break;
                        } else if (ch == '}') {
                            this.token = 13;
                            i = this.bp + 1;
                            this.bp = i;
                            this.ch = charAt(i);
                            break;
                        } else if (ch == '\u001a') {
                            this.token = 20;
                            break;
                        } else if (JSONLexerBase.isWhitespace(ch)) {
                            i2 = this.bp + 1;
                            this.bp = i2;
                            ch = charAt(i2);
                        } else {
                            this.matchStat = -1;
                            return false;
                        }
                    }
                    this.token = 16;
                    i = this.bp + 1;
                    this.bp = i;
                    this.ch = charAt(i);
                    this.matchStat = 4;
                    return value;
                } else if (JSONLexerBase.isWhitespace(ch)) {
                    i2 = this.bp + 1;
                    this.bp = i2;
                    ch = charAt(i2);
                } else {
                    this.bp = startPos;
                    ch = charAt(this.bp);
                    this.matchStat = -1;
                    return false;
                }
            }
            i = this.bp + 1;
            this.bp = i;
            this.ch = charAt(i);
            this.matchStat = 3;
            this.token = 16;
            return value;
        }
        this.matchStat = -2;
        return false;
    }

    public final int scanInt(char expectNext) {
        boolean quote;
        boolean negative = true;
        this.matchStat = 0;
        int offset = this.bp;
        int offset2 = offset + 1;
        char chLocal = charAt(offset);
        offset = offset2;
        while (JSONLexerBase.isWhitespace(chLocal)) {
            offset2 = offset + 1;
            chLocal = charAt(offset);
            offset = offset2;
        }
        if (chLocal == '\"') {
            quote = true;
        } else {
            quote = false;
        }
        if (quote) {
            offset2 = offset + 1;
            chLocal = charAt(offset);
        } else {
            offset2 = offset;
        }
        if (chLocal != '-') {
            negative = false;
        }
        if (negative) {
            offset = offset2 + 1;
            chLocal = charAt(offset2);
            offset2 = offset;
        }
        if (chLocal < '0' || chLocal > '9') {
            if (chLocal == 'n') {
                offset = offset2 + 1;
                if (charAt(offset2) == 'u') {
                    offset2 = offset + 1;
                    if (charAt(offset) == 'l') {
                        offset = offset2 + 1;
                        if (charAt(offset2) == 'l') {
                            this.matchStat = 5;
                            offset2 = offset + 1;
                            chLocal = charAt(offset);
                            if (quote && chLocal == '\"') {
                                offset = offset2 + 1;
                                chLocal = charAt(offset2);
                                offset2 = offset;
                            }
                            while (chLocal != ',') {
                                if (chLocal == ']') {
                                    this.bp = offset2;
                                    this.ch = charAt(this.bp);
                                    this.matchStat = 5;
                                    this.token = 15;
                                    offset = offset2;
                                    return 0;
                                } else if (JSONLexerBase.isWhitespace(chLocal)) {
                                    offset = offset2 + 1;
                                    chLocal = charAt(offset2);
                                    offset2 = offset;
                                } else {
                                    this.matchStat = -1;
                                    offset = offset2;
                                    return 0;
                                }
                            }
                            this.bp = offset2;
                            this.ch = charAt(this.bp);
                            this.matchStat = 5;
                            this.token = 16;
                            offset = offset2;
                            return 0;
                        }
                    }
                }
                this.matchStat = -1;
                return 0;
            }
            this.matchStat = -1;
            return 0;
        }
        int value = chLocal - 48;
        offset = offset2;
        while (true) {
            offset2 = offset + 1;
            chLocal = charAt(offset);
            if (chLocal >= '0' && chLocal <= '9') {
                value = (value * 10) + (chLocal - 48);
                offset = offset2;
            }
        }
        if (chLocal == '.') {
            this.matchStat = -1;
            offset = offset2;
            return 0;
        }
        if (!quote) {
            offset = offset2;
        } else if (chLocal != '\"') {
            this.matchStat = -1;
            offset = offset2;
            return 0;
        } else {
            offset = offset2 + 1;
            chLocal = charAt(offset2);
        }
        if (value < 0) {
            this.matchStat = -1;
            return 0;
        }
        offset2 = offset;
        while (chLocal != expectNext) {
            if (JSONLexerBase.isWhitespace(chLocal)) {
                offset = offset2 + 1;
                chLocal = charAt(offset2);
                offset2 = offset;
            } else {
                this.matchStat = -1;
                if (negative) {
                    value = -value;
                }
                offset = offset2;
                return value;
            }
        }
        this.bp = offset2;
        this.ch = charAt(this.bp);
        this.matchStat = 3;
        this.token = 16;
        if (negative) {
            value = -value;
        }
        offset = offset2;
        return value;
    }

    public double scanDouble(char seperator) {
        this.matchStat = 0;
        int i = this.bp;
        int i2 = i + 1;
        char chLocal = charAt(i);
        boolean quote = chLocal == '\"';
        if (quote) {
            i = i2 + 1;
            chLocal = charAt(i2);
            i2 = i;
        }
        boolean negative = chLocal == '-';
        if (negative) {
            i = i2 + 1;
            chLocal = charAt(i2);
            i2 = i;
        }
        if (chLocal < '0' || chLocal > '9') {
            if (chLocal == 'n') {
                i = i2 + 1;
                if (charAt(i2) == 'u') {
                    i2 = i + 1;
                    if (charAt(i) == 'l') {
                        i = i2 + 1;
                        if (charAt(i2) == 'l') {
                            this.matchStat = 5;
                            i2 = i + 1;
                            chLocal = charAt(i);
                            if (quote && chLocal == '\"') {
                                i = i2 + 1;
                                chLocal = charAt(i2);
                                i2 = i;
                            }
                            while (chLocal != ',') {
                                if (chLocal == ']') {
                                    this.bp = i2;
                                    this.ch = charAt(this.bp);
                                    this.matchStat = 5;
                                    this.token = 15;
                                    i = i2;
                                    return 0.0d;
                                } else if (JSONLexerBase.isWhitespace(chLocal)) {
                                    i = i2 + 1;
                                    chLocal = charAt(i2);
                                    i2 = i;
                                } else {
                                    this.matchStat = -1;
                                    i = i2;
                                    return 0.0d;
                                }
                            }
                            this.bp = i2;
                            this.ch = charAt(this.bp);
                            this.matchStat = 5;
                            this.token = 16;
                            i = i2;
                            return 0.0d;
                        }
                    }
                }
                this.matchStat = -1;
                return 0.0d;
            }
            this.matchStat = -1;
            return 0.0d;
        }
        long power;
        int start;
        int count;
        double parseDouble;
        long intVal = (long) (chLocal - 48);
        i = i2;
        while (true) {
            i2 = i + 1;
            chLocal = charAt(i);
            if (chLocal < '0' || chLocal > '9') {
                power = 1;
            } else {
                intVal = (10 * intVal) + ((long) (chLocal - 48));
                i = i2;
            }
        }
        power = 1;
        if (chLocal == '.') {
            i = i2 + 1;
            chLocal = charAt(i2);
            if (chLocal >= '0' && chLocal <= '9') {
                intVal = (10 * intVal) + ((long) (chLocal - 48));
                power = 10;
                while (true) {
                    i2 = i + 1;
                    chLocal = charAt(i);
                    if (chLocal < '0' || chLocal > '9') {
                        break;
                    }
                    intVal = (10 * intVal) + ((long) (chLocal - 48));
                    power *= 10;
                    i = i2;
                }
            } else {
                this.matchStat = -1;
                return 0.0d;
            }
        }
        boolean exp = chLocal == 'e' || chLocal == 'E';
        if (exp) {
            i = i2 + 1;
            chLocal = charAt(i2);
            if (chLocal == '+' || chLocal == '-') {
                i2 = i + 1;
                chLocal = charAt(i);
            } else {
                i2 = i;
            }
            while (chLocal >= '0' && chLocal <= '9') {
                i = i2 + 1;
                chLocal = charAt(i2);
                i2 = i;
            }
        }
        if (!quote) {
            start = this.bp;
            count = (i2 - start) - 1;
            i = i2;
        } else if (chLocal != '\"') {
            this.matchStat = -1;
            i = i2;
            return 0.0d;
        } else {
            i = i2 + 1;
            chLocal = charAt(i2);
            start = this.bp + 1;
            count = (i - start) - 2;
        }
        if (exp || count >= 20) {
            parseDouble = Double.parseDouble(subString(start, count));
        } else {
            parseDouble = ((double) intVal) / ((double) power);
            if (negative) {
                parseDouble = -parseDouble;
            }
        }
        if (chLocal == seperator) {
            this.bp = i;
            this.ch = charAt(this.bp);
            this.matchStat = 3;
            this.token = 16;
            return parseDouble;
        }
        this.matchStat = -1;
        return parseDouble;
    }

    public long scanLong(char seperator) {
        this.matchStat = 0;
        int offset = this.bp;
        int i = offset + 1;
        char chLocal = charAt(offset);
        boolean quote = chLocal == '\"';
        if (quote) {
            offset = i + 1;
            chLocal = charAt(i);
            i = offset;
        }
        boolean negative = chLocal == '-';
        if (negative) {
            offset = i + 1;
            chLocal = charAt(i);
            i = offset;
        }
        if (chLocal < '0' || chLocal > '9') {
            if (chLocal == 'n') {
                offset = i + 1;
                if (charAt(i) == 'u') {
                    i = offset + 1;
                    if (charAt(offset) == 'l') {
                        offset = i + 1;
                        if (charAt(i) == 'l') {
                            this.matchStat = 5;
                            i = offset + 1;
                            chLocal = charAt(offset);
                            if (quote && chLocal == '\"') {
                                offset = i + 1;
                                chLocal = charAt(i);
                                i = offset;
                            }
                            while (chLocal != ',') {
                                if (chLocal == ']') {
                                    this.bp = i;
                                    this.ch = charAt(this.bp);
                                    this.matchStat = 5;
                                    this.token = 15;
                                    offset = i;
                                    return 0;
                                } else if (JSONLexerBase.isWhitespace(chLocal)) {
                                    offset = i + 1;
                                    chLocal = charAt(i);
                                    i = offset;
                                } else {
                                    this.matchStat = -1;
                                    offset = i;
                                    return 0;
                                }
                            }
                            this.bp = i;
                            this.ch = charAt(this.bp);
                            this.matchStat = 5;
                            this.token = 16;
                            offset = i;
                            return 0;
                        }
                    }
                }
                this.matchStat = -1;
                return 0;
            }
            this.matchStat = -1;
            return 0;
        }
        long value = (long) (chLocal - 48);
        offset = i;
        while (true) {
            i = offset + 1;
            chLocal = charAt(offset);
            if (chLocal >= '0' && chLocal <= '9') {
                value = (10 * value) + ((long) (chLocal - 48));
                offset = i;
            }
        }
        if (chLocal == '.') {
            this.matchStat = -1;
            offset = i;
            return 0;
        }
        if (!quote) {
            offset = i;
        } else if (chLocal != '\"') {
            this.matchStat = -1;
            offset = i;
            return 0;
        } else {
            offset = i + 1;
            chLocal = charAt(i);
        }
        boolean valid = value >= 0 || (value == Long.MIN_VALUE && negative);
        if (valid) {
            i = offset;
            while (chLocal != seperator) {
                if (JSONLexerBase.isWhitespace(chLocal)) {
                    offset = i + 1;
                    chLocal = charAt(i);
                    i = offset;
                } else {
                    this.matchStat = -1;
                    offset = i;
                    return value;
                }
            }
            this.bp = i;
            this.ch = charAt(this.bp);
            this.matchStat = 3;
            this.token = 16;
            if (negative) {
                value = -value;
            }
            offset = i;
            return value;
        }
        this.matchStat = -1;
        return 0;
    }

    protected final void arrayCopy(int srcPos, char[] dest, int destPos, int length) {
        this.text.getChars(srcPos, srcPos + length, dest, destPos);
    }

    public String info() {
        String str;
        StringBuilder append = new StringBuilder().append("pos ").append(this.bp).append(", json : ");
        if (this.text.length() < 65536) {
            str = this.text;
        } else {
            str = this.text.substring(0, 65536);
        }
        return append.append(str).toString();
    }
}
