package com.alibaba.fastjson;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import java.io.Closeable;

public class JSONReader implements Closeable {
    private final DefaultJSONParser parser;

    public void close() {
        this.parser.close();
    }
}
