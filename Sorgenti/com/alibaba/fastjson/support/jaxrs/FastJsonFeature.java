package com.alibaba.fastjson.support.jaxrs;

import javax.ws.rs.core.Feature;

public class FastJsonFeature implements Feature {
    private static final String JSON_FEATURE = FastJsonFeature.class.getSimpleName();
}
