package com.alibaba.fastjson.support.config;

import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import java.nio.charset.Charset;

public class FastJsonConfig {
    private Charset charset = Charset.forName("UTF-8");
    private Feature[] features = new Feature[0];
    private ParserConfig parserConfig = new ParserConfig();
    private SerializeConfig serializeConfig = SerializeConfig.getGlobalInstance();
    private SerializeFilter[] serializeFilters = new SerializeFilter[0];
    private SerializerFeature[] serializerFeatures = new SerializerFeature[]{SerializerFeature.BrowserSecure};
    protected boolean writeContentLength = true;
}
