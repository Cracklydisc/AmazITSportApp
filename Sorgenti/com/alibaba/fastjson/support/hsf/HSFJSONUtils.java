package com.alibaba.fastjson.support.hsf;

import com.alibaba.fastjson.parser.SymbolTable;

public class HSFJSONUtils {
    static final char[] fieldName_argsObjs = "\"argsObjs\"".toCharArray();
    static final char[] fieldName_argsTypes = "\"argsTypes\"".toCharArray();
    static final char[] fieldName_type = "\"@type\":".toCharArray();
    static final SymbolTable typeSymbolTable = new SymbolTable(1024);
}
