package com.alibaba.fastjson.support.spring;

import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import org.springframework.web.servlet.view.AbstractView;

public class FastJsonJsonView extends AbstractView {
    private static final Pattern CALLBACK_PARAM_PATTERN = Pattern.compile("[0-9A-Za-z_\\.]*");
    @Deprecated
    protected Charset charset = Charset.forName("UTF-8");
    private boolean disableCaching = true;
    private boolean extractValueFromSingleKeyModel = false;
    private FastJsonConfig fastJsonConfig = new FastJsonConfig();
    @Deprecated
    protected SerializerFeature[] features = new SerializerFeature[0];
    @Deprecated
    protected SerializeFilter[] filters = new SerializeFilter[0];
    private String[] jsonpParameterNames = new String[]{"jsonp", "callback"};
    private boolean updateContentLength = true;

    public FastJsonJsonView() {
        setContentType("application/json;charset=UTF-8");
        setExposePathVariables(false);
    }
}
