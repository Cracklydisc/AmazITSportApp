package com.alibaba.fastjson.support.spring;

import java.util.regex.Pattern;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
@Order(Integer.MIN_VALUE)
@Deprecated
public class FastJsonpResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    private static final Pattern CALLBACK_PARAM_PATTERN = Pattern.compile("[0-9A-Za-z_\\.]*");
    public static final String[] DEFAULT_JSONP_QUERY_PARAM_NAMES = new String[]{"callback", "jsonp"};
    private final String[] jsonpQueryParamNames = DEFAULT_JSONP_QUERY_PARAM_NAMES;
}
