package com.alibaba.fastjson.support.spring;

import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
@Order
public class FastJsonViewResponseBodyAdvice implements ResponseBodyAdvice<Object> {
}
