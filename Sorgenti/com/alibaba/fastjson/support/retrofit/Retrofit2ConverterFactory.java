package com.alibaba.fastjson.support.retrofit;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.parser.ParserConfig;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Converter.Factory;

public class Retrofit2ConverterFactory extends Factory {
    private static final Feature[] EMPTY_SERIALIZER_FEATURES = new Feature[0];
    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
    private int featureValues = JSON.DEFAULT_PARSER_FEATURE;
    private ParserConfig parserConfig = ParserConfig.getGlobalInstance();

    final class RequestBodyConverter<T> implements Converter<T, RequestBody> {
    }

    final class ResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    }
}
