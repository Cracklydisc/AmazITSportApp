package com.alibaba.fastjson.util;

import java.lang.ref.SoftReference;
import java.nio.charset.CharsetDecoder;

public class ThreadLocalCache {
    static final /* synthetic */ boolean $assertionsDisabled = (!ThreadLocalCache.class.desiredAssertionStatus());
    private static final ThreadLocal<SoftReference<byte[]>> bytesBufLocal = new ThreadLocal();
    private static final ThreadLocal<SoftReference<char[]>> charsBufLocal = new ThreadLocal();
    private static final ThreadLocal<CharsetDecoder> decoderLocal = new ThreadLocal();
}
