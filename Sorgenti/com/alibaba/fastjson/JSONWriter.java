package com.alibaba.fastjson;

import com.alibaba.fastjson.serializer.SerializeWriter;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public class JSONWriter implements Closeable, Flushable {
    private SerializeWriter writer;

    public void flush() throws IOException {
        this.writer.flush();
    }

    public void close() throws IOException {
        this.writer.close();
    }
}
