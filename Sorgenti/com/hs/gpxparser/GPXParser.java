package com.hs.gpxparser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class GPXParser extends BaseGPX {
    private static final String TAG = GPXParser.class.getName();
    public static DocumentBuilder builder;

    public GPXParser() {
        builder = getDocument();
    }

    private static DocumentBuilder getDocument() {
        if (builder != null) {
            return builder;
        }
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return builder;
    }
}
