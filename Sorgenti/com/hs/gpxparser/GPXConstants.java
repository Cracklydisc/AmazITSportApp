package com.hs.gpxparser;

import java.util.HashMap;

public class GPXConstants {
    private static final HashMap<String, Integer> GPSConstantsMap = new HashMap();

    static {
        GPSConstantsMap.put("gpx", Integer.valueOf(1));
        GPSConstantsMap.put("creator", Integer.valueOf(2));
        GPSConstantsMap.put("version", Integer.valueOf(3));
        GPSConstantsMap.put("metadata", Integer.valueOf(4));
        GPSConstantsMap.put("trk", Integer.valueOf(5));
        GPSConstantsMap.put("rte", Integer.valueOf(6));
        GPSConstantsMap.put("wpt", Integer.valueOf(7));
        GPSConstantsMap.put("extensions", Integer.valueOf(8));
        GPSConstantsMap.put("name", Integer.valueOf(9));
        GPSConstantsMap.put("desc", Integer.valueOf(10));
        GPSConstantsMap.put("author", Integer.valueOf(11));
        GPSConstantsMap.put("copyright", Integer.valueOf(12));
        GPSConstantsMap.put("link", Integer.valueOf(13));
        GPSConstantsMap.put("time", Integer.valueOf(14));
        GPSConstantsMap.put("keywords", Integer.valueOf(15));
        GPSConstantsMap.put("bounds", Integer.valueOf(16));
        GPSConstantsMap.put("lat", Integer.valueOf(17));
        GPSConstantsMap.put("lon", Integer.valueOf(18));
        GPSConstantsMap.put("ele", Integer.valueOf(19));
        GPSConstantsMap.put("magvar", Integer.valueOf(20));
        GPSConstantsMap.put("geoidheight", Integer.valueOf(21));
        GPSConstantsMap.put("cmt", Integer.valueOf(22));
        GPSConstantsMap.put("src", Integer.valueOf(23));
        GPSConstantsMap.put("sym", Integer.valueOf(24));
        GPSConstantsMap.put("type", Integer.valueOf(25));
        GPSConstantsMap.put("fix", Integer.valueOf(26));
        GPSConstantsMap.put("sat", Integer.valueOf(27));
        GPSConstantsMap.put("hdop", Integer.valueOf(28));
        GPSConstantsMap.put("vdop", Integer.valueOf(29));
        GPSConstantsMap.put("pdop", Integer.valueOf(30));
        GPSConstantsMap.put("ageofdgpsdata", Integer.valueOf(31));
        GPSConstantsMap.put("dgpsid", Integer.valueOf(32));
        GPSConstantsMap.put("number", Integer.valueOf(33));
        GPSConstantsMap.put("rtept", Integer.valueOf(34));
        GPSConstantsMap.put("trkseg", Integer.valueOf(35));
        GPSConstantsMap.put("trkpt", Integer.valueOf(36));
        GPSConstantsMap.put("author", Integer.valueOf(37));
        GPSConstantsMap.put("year", Integer.valueOf(38));
        GPSConstantsMap.put("license", Integer.valueOf(39));
        GPSConstantsMap.put("href", Integer.valueOf(40));
        GPSConstantsMap.put("text", Integer.valueOf(41));
        GPSConstantsMap.put("id", Integer.valueOf(42));
        GPSConstantsMap.put("domain", Integer.valueOf(43));
        GPSConstantsMap.put("email", Integer.valueOf(44));
        GPSConstantsMap.put("pt", Integer.valueOf(45));
        GPSConstantsMap.put("minlat", Integer.valueOf(46));
        GPSConstantsMap.put("minlon", Integer.valueOf(47));
        GPSConstantsMap.put("maxlat", Integer.valueOf(48));
        GPSConstantsMap.put("maxlon", Integer.valueOf(49));
    }

    public static int getIndexNode(String nodeName) {
        if (nodeName == null) {
            return -1;
        }
        Object object = GPSConstantsMap.get(nodeName);
        if (object == null || object == null) {
            return -1;
        }
        return ((Integer) object).intValue();
    }
}
