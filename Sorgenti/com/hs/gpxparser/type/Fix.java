package com.hs.gpxparser.type;

public class Fix {
    public static Fix DGPS = new Fix("dgps");
    public static Fix NONE = new Fix("none");
    public static Fix PPS = new Fix("pps");
    public static Fix THREE_D = new Fix("3d");
    public static Fix TWO_D = new Fix("2d");
    private String value;

    private Fix(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }
}
