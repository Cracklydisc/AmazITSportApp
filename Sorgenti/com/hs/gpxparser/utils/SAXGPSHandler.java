package com.hs.gpxparser.utils;

import com.hs.gpxparser.GPXConstants;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Metadata;
import com.hs.gpxparser.modal.Route;
import com.hs.gpxparser.modal.Track;
import com.hs.gpxparser.modal.TrackSegment;
import com.hs.gpxparser.modal.Waypoint;
import java.util.Iterator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXGPSHandler extends DefaultHandler {
    private static boolean IS_DEBUG = false;
    public static final String TAG = SAXGPSHandler.class.getSimpleName();
    private String currentLocalName;
    private TrackSegment currentTrackSeg;
    private Waypoint currentWayPoint;
    private Metadata currnetMetadata;
    private Route currnetRoute;
    private Track currnetTrack;
    private GPX gpx;
    private String lastLocalName;
    private Waypoint routeWayPoint;

    public GPX getGPX() {
        if (this.gpx == null) {
            return new GPX();
        }
        printGPXData(this.gpx);
        return this.gpx;
    }

    private void printGPXData(GPX gpx) {
        if (gpx != null && IS_DEBUG) {
            String str;
            Metadata metadata = gpx.getMetadata();
            if (metadata != null) {
                LogUtils.print(TAG, "printGPXData  metaData:" + metadata.toString());
                LogUtils.print(TAG, "printGPXData  metaDataTitle:" + metadata.getName());
            }
            if (gpx.getTracks() != null) {
                LogUtils.print(TAG, "printGPXData trackSize:" + gpx.getTracks().size());
                Iterator<Track> iterator = gpx.getTracks().iterator();
                while (iterator.hasNext()) {
                    LogUtils.print(TAG, "printGPXData currnetTrackName:" + ((Track) iterator.next()).getName());
                }
            }
            LogUtils.print(TAG, "------------------ getWayPoints--- start----------------");
            if (gpx.getWaypoints() != null) {
                Iterator i$ = gpx.getWaypoints().iterator();
                while (i$.hasNext()) {
                    Waypoint currnetPoint = (Waypoint) i$.next();
                    LogUtils.print(TAG, "printWayPoint:lat:" + currnetPoint.getLatitude() + ",lon:" + currnetPoint.getLongitude() + ",name:" + currnetPoint.getName());
                }
            }
            LogUtils.print(TAG, "------------------ getWayPoints--- end----------------");
            String str2 = TAG;
            StringBuilder append = new StringBuilder().append(" routes: isNull?:");
            if (gpx.getRoutes() == null) {
                str = "null";
            } else {
                str = "" + gpx.getRoutes().size();
            }
            LogUtils.print(str2, append.append(str).toString());
            if (gpx.getRoutes() != null) {
                LogUtils.print(TAG, "RouteSize:" + gpx.getRoutes().size());
                Iterator<Route> iterator2 = gpx.getRoutes().iterator();
                while (iterator2.hasNext()) {
                    Route route = (Route) iterator2.next();
                    LogUtils.print(TAG, "currentRouteWayPointSize:" + route.getRoutePoints().size());
                    for (int i = 0; i < route.getRoutePoints().size(); i++) {
                        LogUtils.print(TAG, "currentRouteWayPoint:" + ((Waypoint) route.getRoutePoints().get(i)).toString());
                    }
                }
            }
        }
    }

    public void startDocument() throws SAXException {
        super.startDocument();
        this.gpx = new GPX();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        switch (GPXConstants.getIndexNode(localName)) {
            case 4:
                this.lastLocalName = "metadata";
                String metaDataName = attributes.getValue("name");
                String metaTxtData = attributes.getValue("link");
                LogUtils.print(TAG, " metaTxtData:" + metaTxtData);
                this.currnetMetadata = new Metadata();
                if (metaDataName == null) {
                    if (metaTxtData != null) {
                        this.currnetMetadata.setName(metaTxtData);
                        break;
                    }
                }
                this.currnetMetadata.setName(metaDataName);
                break;
                break;
            case 5:
                String name = attributes.getValue("name");
                this.lastLocalName = "trk";
                this.currnetTrack = new Track();
                this.currnetTrack.setName(name);
                break;
            case 6:
                this.lastLocalName = "rte";
                this.currnetRoute = new Route();
                break;
            case 7:
                this.currentWayPoint = new Waypoint(Double.parseDouble(attributes.getValue("lat")), Double.parseDouble(attributes.getValue("lon")));
                this.lastLocalName = "wpt";
                break;
            case 34:
                this.routeWayPoint = new Waypoint(Double.parseDouble(attributes.getValue("lat")), Double.parseDouble(attributes.getValue("lon")));
                this.lastLocalName = "rtept";
                break;
            case 35:
                this.lastLocalName = "trkseg";
                this.currentTrackSeg = new TrackSegment();
                break;
            case 36:
                this.lastLocalName = "trkpt";
                this.currentWayPoint = new Waypoint(Double.parseDouble(attributes.getValue("lat")), Double.parseDouble(attributes.getValue("lon")));
                break;
            case 37:
                this.lastLocalName = "author";
                break;
        }
        this.currentLocalName = localName;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (this.lastLocalName != null) {
            String data = new String(ch, start, length);
            if (IS_DEBUG) {
                LogUtils.print(TAG, "characters :localName:" + this.currentLocalName + ",data:" + data);
            }
            if (data != null && data.length() > 0 && data.trim().length() > 0) {
                switch (GPXConstants.getIndexNode(this.lastLocalName)) {
                    case 4:
                        LogUtils.print(TAG, "metaData:" + this.currentLocalName + ",data:" + data);
                        if ("name".equals(this.currentLocalName)) {
                            this.currnetMetadata.setName(data);
                        }
                        if ("text".equals(this.currentLocalName)) {
                            this.currnetMetadata.setName(data);
                            return;
                        }
                        return;
                    case 5:
                        if ("name".equals(this.currentLocalName)) {
                            this.currnetTrack.setName(data);
                            return;
                        }
                        return;
                    case 7:
                        if ("name".equals(this.currentLocalName)) {
                            this.currentWayPoint.setName(data);
                            return;
                        }
                        return;
                    case 34:
                        try {
                            if ("lat".equals(this.currentLocalName)) {
                                this.routeWayPoint.setLatitude(Double.parseDouble(data));
                                return;
                            } else if ("lon".equals(this.currentLocalName)) {
                                this.routeWayPoint.setLongitude(Double.parseDouble(data));
                                return;
                            } else if ("ele".equals(this.currentLocalName)) {
                                this.routeWayPoint.setElevation(Double.parseDouble(data));
                                return;
                            } else if (!"time".equals(this.currentLocalName)) {
                                return;
                            } else {
                                return;
                            }
                        } catch (NumberFormatException e) {
                            return;
                        }
                    case 36:
                        try {
                            if ("lat".equals(this.currentLocalName)) {
                                this.currentWayPoint.setLatitude(Double.parseDouble(data));
                                return;
                            } else if ("lon".equals(this.currentLocalName)) {
                                this.currentWayPoint.setLongitude(Double.parseDouble(data));
                                return;
                            } else if ("ele".equals(this.currentLocalName)) {
                                this.currentWayPoint.setElevation(Double.parseDouble(data));
                                return;
                            } else if (!"time".equals(this.currentLocalName)) {
                                return;
                            } else {
                                return;
                            }
                        } catch (NumberFormatException e2) {
                            return;
                        }
                    default:
                        return;
                }
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        switch (GPXConstants.getIndexNode(localName)) {
            case 4:
                this.gpx.setMetadata(this.currnetMetadata);
                this.currnetMetadata = null;
                return;
            case 5:
                this.gpx.addTrack(this.currnetTrack);
                this.currnetTrack = null;
                return;
            case 6:
                this.gpx.addRoute(this.currnetRoute);
                this.currnetRoute = null;
                return;
            case 7:
                this.gpx.addWaypoint(this.currentWayPoint);
                this.currentWayPoint = null;
                return;
            case 34:
                this.currnetRoute.addRoutePoint(this.routeWayPoint);
                this.routeWayPoint = null;
                return;
            case 35:
                this.currnetTrack.addTrackSegment(this.currentTrackSeg);
                this.currentTrackSeg = null;
                return;
            case 36:
                this.currentTrackSeg.addWaypoint(this.currentWayPoint);
                this.currentWayPoint = null;
                return;
            default:
                return;
        }
    }

    public void endDocument() throws SAXException {
        super.endDocument();
    }
}
