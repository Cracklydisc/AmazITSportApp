package com.hs.gpxparser.utils;

import com.hs.gpxparser.modal.GPX;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SimpleSAXGPXHandler extends DefaultHandler {
    private String currnetLocalName;
    private GPX gpx;

    public void startDocument() throws SAXException {
        super.startDocument();
        this.gpx = new GPX();
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        this.currnetLocalName = localName;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }

    public void endDocument() throws SAXException {
        super.endDocument();
    }
}
