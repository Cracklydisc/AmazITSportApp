package com.hs.gpxparser.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.hs.gpxparser.GPXParser;
import com.hs.gpxparser.appmodel.GPXAppRoute;
import com.hs.gpxparser.modal.GPX;
import com.hs.gpxparser.modal.Waypoint;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SDCardGPSUtils {
    private static boolean GPS_Data_Kernel_DEBUG = false;
    public static int GPX_TO_KERNEL_ENLARGE = 1000;
    private static final String TAG = SDCardGPSUtils.class.getName();
    private static long startGPSDataTime = 0;

    static class C03742 implements Comparator<Waypoint> {
        C03742() {
        }

        public int compare(Waypoint s1, Waypoint s2) {
            return s1.getEnlargeLat() - s2.getEnlargeLat();
        }
    }

    static class C03753 implements Comparator<Waypoint> {
        C03753() {
        }

        public int compare(Waypoint s1, Waypoint s2) {
            if (s1.getEnlargeLat() == s2.getEnlargeLat()) {
                return s1.getEnlargeLon() - s2.getEnlargeLon();
            }
            return s1.getEnlargeLat() - s2.getEnlargeLat();
        }
    }

    public static boolean isSdCardExist() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static File[] getGPSSDCardFiles() {
        File[] fileArr = null;
        if (isSdCardExist()) {
            final String encoding = System.getProperty("file.encoding");
            File currentPath = new File("/sdcard/gpxdata/");
            if (currentPath.exists() && currentPath.isDirectory()) {
                try {
                    for (File file : currentPath.listFiles()) {
                        LogUtils.print(TAG, "getGPSSDCardFiles before :" + file.getName());
                        LogUtils.print(TAG, "getGPSSDCardFiles  after test fileName:" + new String(file.getName().getBytes(encoding), "UTF-8"));
                    }
                    LogUtils.print(TAG, "getGPSSDCardFiles============================");
                    fileArr = currentPath.listFiles(new FilenameFilter() {
                        public boolean accept(File dir, String filename) {
                            LogUtils.print(SDCardGPSUtils.TAG, " getGPSSDCardFiles accept :" + filename);
                            if (filename != null) {
                                try {
                                    if (new String(filename.getBytes(encoding), "UTF-8").toString().endsWith(".gpx")) {
                                        LogUtils.print(SDCardGPSUtils.TAG, "accept add fileName:" + filename);
                                        return true;
                                    }
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                    return false;
                                }
                            }
                            LogUtils.print(SDCardGPSUtils.TAG, "accept remove fileName:" + filename);
                            return false;
                        }
                    });
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                LogUtils.print(TAG, "getGPSSDCardFiles mkdir ");
                currentPath.mkdirs();
            }
        }
        return fileArr;
    }

    public static List<GPXAppRoute> getGPXAppRouteFromSDCard() {
        File[] files = getGPSSDCardFiles();
        if (files == null || files.length == 0) {
            return null;
        }
        List<GPXAppRoute> gpxAppRoutes = new ArrayList();
        GPXParser gpxParser = new GPXParser();
        for (File currentFile : files) {
            GPXAppRoute gpxAppRoute = new GPXAppRoute();
            LogUtils.print(TAG, "getGPXAppRouteFromSDCard fileName:" + currentFile.getName());
            gpxAppRoute.setGpxFileName(currentFile.getName().replace(".gpx", ""));
            gpxAppRoute.setRouteImgPath(getGPXRouteImagePath(currentFile.getName()));
            gpxAppRoute.setRouteLength(0.0f);
            gpxAppRoutes.add(gpxAppRoute);
        }
        return gpxAppRoutes;
    }

    public static GPX getWayPointsFromFileName(String fileName) {
        GPXParser gpxParser = new GPXParser();
        try {
            long startTime = System.currentTimeMillis();
            SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
            InputStream is = new FileInputStream(getGPXRouteFilePath(fileName));
            SAXGPSHandler handle = new SAXGPSHandler();
            saxParser.parse(is, handle);
            is.close();
            GPX gpx = handle.getGPX();
            LogUtils.print(TAG, "  parseGPX File cost time:" + (System.currentTimeMillis() - startTime));
            return gpx;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getGPXRouteImagePath(String fileName) {
        String fileNameStr = fileName.replace(".gpx", "").toString();
        StringBuilder sb = new StringBuilder();
        sb.append("/sdcard/.sportroute/gpximg/");
        sb.append(fileNameStr);
        sb.append(".png");
        LogUtils.print(TAG, "getGPXRouteImagePath : imagaPath:" + fileNameStr);
        return sb.toString();
    }

    public static String getGPXRouteFilePath(String fileName) {
        String fileNameStr = fileName.replace(".gpx", "").toString();
        StringBuilder sb = new StringBuilder();
        sb.append("/sdcard/gpxdata/");
        sb.append(fileNameStr);
        sb.append(".gpx");
        LogUtils.print(TAG, "getGPXRouteFilePath : filePath:" + sb.toString());
        return sb.toString();
    }

    public static void saveBitmapToSDCard(Bitmap bitmap, String fileName) {
        File fileDir = new File("/sdcard/.sportroute/gpximg/");
        boolean fileDirBoolean = fileDir.exists() && fileDir.isDirectory();
        if (!fileDirBoolean) {
            fileDir.mkdirs();
        }
        if (fileDirBoolean) {
            File file = new File(getGPXRouteImagePath(fileName));
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                bitmap.compress(CompressFormat.JPEG, 100, bos);
                bos.flush();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap getGPSBitmapFromGPXFileName(String name) {
        LogUtils.print(TAG, "getGPSBitmapFromGPXFileName");
        String bitmapFilePath = getGPXRouteImagePath(name);
        if (new File(bitmapFilePath).exists()) {
            LogUtils.print(TAG, "getGPSBitmapFromGPXFileName:" + bitmapFilePath + " is exist ");
            return BitmapFactory.decodeFile(bitmapFilePath);
        }
        LogUtils.print(TAG, "getGPSBitmapFromGPXFileName:" + bitmapFilePath + " is not exist ");
        return null;
    }

    public static byte[] getClearGPSToKernelData() {
        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
        try {
            outBuffer.write(1);
            outBuffer.write("GPXD".getBytes());
            outBuffer.write(ByteUtils.intToBytes4(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outBuffer.toByteArray();
    }

    public static byte[] getGPSToKernelData(Context mContext, List<Waypoint> listData) {
        startGPSDataTime = System.currentTimeMillis();
        LogUtils.print(TAG, "getGPSToKernelData　startTime:" + startGPSDataTime);
        String result = "";
        if (listData == null || listData.size() <= 0) {
            LogUtils.print(TAG, "getGPSSimpleDataToDeep has not data ");
            return null;
        }
        List<Waypoint> listNewData = new ArrayList();
        listNewData.addAll(listData);
        List<Waypoint> listInsertPointData = new ArrayList();
        Waypoint firstPoint = (Waypoint) listNewData.get(0);
        firstPoint.setEnlarge(GPX_TO_KERNEL_ENLARGE);
        int preInsertPointLat = firstPoint.getEnlargeLat();
        int preInsertPointLon = firstPoint.getEnlargeLon();
        for (Waypoint point : listNewData) {
            point.setEnlarge(GPX_TO_KERNEL_ENLARGE);
            int currentInsertPointLon = point.getEnlargeLon();
            int currnetInsertPointLat = point.getEnlargeLat();
            int latSize = point.getEnlargeLat() - preInsertPointLat;
            int lonSize = point.getEnlargeLon() - preInsertPointLon;
            int absLat = Math.abs(latSize);
            int absLon = Math.abs(lonSize);
            if (absLat > 1 || absLon > 1) {
                int i;
                double newLat;
                double newLon;
                if (absLat >= absLon) {
                    LogUtils.print(TAG, " getGPSToKernelData insert  lat 　GPXLat:" + currnetInsertPointLat + ",GPXLon:" + currentInsertPointLon + ",preLat:" + preInsertPointLat + ",preLon:" + preInsertPointLon + " ,latSize:" + latSize + ",lonSize:" + lonSize);
                    for (i = 1; i < absLat; i++) {
                        newLat = latSize == 0 ? (double) preInsertPointLat : (double) ((((currnetInsertPointLat - preInsertPointLat) * i) / absLat) + preInsertPointLat);
                        if (latSize == 0) {
                            newLon = (double) preInsertPointLon;
                        } else {
                            newLon = (double) ((((currentInsertPointLon - preInsertPointLon) * i) / absLat) + preInsertPointLon);
                        }
                        if (newLat >= ((double) Math.min(preInsertPointLat, currnetInsertPointLat)) && newLat <= ((double) Math.max(preInsertPointLat, currnetInsertPointLat)) && newLon >= ((double) Math.min(preInsertPointLon, currentInsertPointLon)) && newLon <= ((double) Math.max(preInsertPointLon, currentInsertPointLon))) {
                            LogUtils.print(TAG, "getGPSToKernelData　before insertPoint: newLat:" + newLat + ",newLon:" + newLon + ",i:" + i + ",absLat:" + absLat);
                            listInsertPointData.add(new Waypoint(newLat / ((double) GPX_TO_KERNEL_ENLARGE), newLon / ((double) GPX_TO_KERNEL_ENLARGE)));
                        }
                    }
                } else {
                    LogUtils.print(TAG, " getGPSToKernelData insert  lon 　GPXLat:" + currnetInsertPointLat + ",GPXLon:" + currentInsertPointLon + ",preLat:" + preInsertPointLat + ",preLon:" + preInsertPointLon + " ,latSize:" + latSize + ",lonSize:" + lonSize);
                    for (i = 1; i < absLon; i++) {
                        newLat = lonSize == 0 ? (double) preInsertPointLat : (double) (((currnetInsertPointLat - preInsertPointLat) * (i / absLon)) + preInsertPointLat);
                        if (lonSize == 0) {
                            newLon = (double) preInsertPointLon;
                        } else {
                            newLon = (double) ((((currentInsertPointLon - preInsertPointLon) * i) / absLon) + preInsertPointLon);
                        }
                        if (newLat >= ((double) Math.min(preInsertPointLat, currnetInsertPointLat)) && newLat <= ((double) Math.max(preInsertPointLat, currnetInsertPointLat)) && newLon >= ((double) Math.min(preInsertPointLon, currentInsertPointLon)) && newLon <= ((double) Math.max(preInsertPointLon, currentInsertPointLon))) {
                            LogUtils.print(TAG, "getGPSToKernelData before  insertPoint: newLat:" + newLat + ",newLon:" + newLon);
                            listInsertPointData.add(new Waypoint(newLat / ((double) GPX_TO_KERNEL_ENLARGE), newLon / ((double) GPX_TO_KERNEL_ENLARGE)));
                        }
                    }
                }
            }
            listInsertPointData.add(point);
            preInsertPointLat = currnetInsertPointLat;
            preInsertPointLon = currentInsertPointLon;
        }
        Collections.sort(listInsertPointData, new C03742());
        Collections.sort(listInsertPointData, new C03753());
        LogUtils.print(TAG, "--------------------");
        return pringKernelData(mContext, listInsertPointData, true);
    }

    private static byte[] pringKernelData(Context mContext, List<Waypoint> listNewData, boolean isdebug) {
        List<Object> listDataObject = new ArrayList();
        if (listNewData == null || listNewData.size() == 0) {
            return null;
        }
        Waypoint firstPoint = (Waypoint) listNewData.get(0);
        firstPoint.setEnlarge(GPX_TO_KERNEL_ENLARGE);
        int firstLat = firstPoint.getEnlargeLat();
        int firstLon = firstPoint.getEnlargeLon();
        LogUtils.print(TAG, "pringKernelData size:" + listNewData.size());
        StringBuffer sb = new StringBuffer();
        ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
        try {
            outBuffer.write(ByteUtils.intToBytes4(firstLat));
            outBuffer.write(ByteUtils.intToBytes4(firstLon));
            int preLat = -1;
            int preLon = -1;
            int totalSummary = 0;
            LogUtils.print(TAG, "pringKernelData sortSize:" + listNewData.size());
            for (Waypoint currnetPoint : listNewData) {
                int currnetLat = currnetPoint.getEnlargeLat() - firstLat;
                int currnetLon = currnetPoint.getEnlargeLon() - firstLon;
                boolean isNeedAdd = (preLat == currnetLat && preLon == currnetLon) ? false : true;
                if (isNeedAdd) {
                    outBuffer.write(ByteUtils.intToBytes2(currnetLat));
                    outBuffer.write(ByteUtils.intToBytes2(currnetLon));
                    totalSummary++;
                }
                preLat = currnetLat;
                preLon = currnetLon;
            }
            LogUtils.print(TAG, "pringKernelData cost time:" + (System.currentTimeMillis() - startGPSDataTime));
            ByteArrayOutputStream gpdToKernelBuffer = new ByteArrayOutputStream();
            gpdToKernelBuffer.write(1);
            gpdToKernelBuffer.write("GPXD".getBytes());
            LogUtils.print(TAG, "pringKernelData  totalSummary:" + totalSummary + ",ENLarge:" + GPX_TO_KERNEL_ENLARGE);
            gpdToKernelBuffer.write(ByteUtils.intGPSPointLengthToBytes(totalSummary, GPX_TO_KERNEL_ENLARGE));
            gpdToKernelBuffer.write(outBuffer.toByteArray());
            return gpdToKernelBuffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
