package com.hs.gpxparser.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class GPSSPUtils {
    public static void setCurrentGPXRouteBySportType(Context mContext, String currentGPXName, String selectedGPXFile, int sportType) {
        Editor edit = mContext.getSharedPreferences("sp_gps_route", 0).edit();
        edit.putString("selected_gpx_route_name_" + sportType, currentGPXName);
        edit.putString("select_gpx_route_file_name_" + sportType, selectedGPXFile);
        edit.commit();
    }

    public static String[] getCurrentSelectedGPXRouteBySportType(Context mContext, int sportType) {
        SharedPreferences mPreferenceBlue = mContext.getSharedPreferences("sp_gps_route", 0);
        return new String[]{mPreferenceBlue.getString("selected_gpx_route_name_" + sportType, null), mPreferenceBlue.getString("select_gpx_route_file_name_" + sportType, null)};
    }
}
