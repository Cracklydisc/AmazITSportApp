package com.hs.gpxparser.utils;

public class ByteUtils {
    private static final String TAG = ByteUtils.class.getName();

    public static byte[] intToBytes4(int value) {
        return new byte[]{(byte) (value & 255), (byte) ((value >> 8) & 255), (byte) ((value >> 16) & 255), (byte) ((value >> 24) & 255)};
    }

    public static byte[] intGPSPointLengthToBytes(int value, int enlargeType) {
        byte[] src = new byte[4];
        src[0] = (byte) (value & 255);
        src[1] = (byte) ((value >> 8) & 255);
        src[2] = (byte) ((value >> 16) & 255);
        LogUtils.print(TAG, "intToBytes4 enlargeType:" + enlargeType);
        switch (enlargeType) {
            case 1000:
                src[3] = (byte) 0;
                break;
            case 2000:
                src[3] = (byte) 1;
                break;
            case 4000:
                src[3] = (byte) 2;
                break;
        }
        return src;
    }

    public static byte[] intToBytes2(int value) {
        return new byte[]{(byte) (value & 255), (byte) ((value >> 8) & 255)};
    }
}
