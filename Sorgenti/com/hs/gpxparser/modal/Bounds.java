package com.hs.gpxparser.modal;

import java.io.Serializable;

public class Bounds implements Serializable {
    private double maxLat;
    private double maxLon;
    private double minLat;
    private double minLon;
}
