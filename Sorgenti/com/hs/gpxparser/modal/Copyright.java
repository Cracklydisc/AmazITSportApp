package com.hs.gpxparser.modal;

import java.io.Serializable;

public class Copyright implements Serializable {
    private String author;
    private String license;
    private String year;
}
