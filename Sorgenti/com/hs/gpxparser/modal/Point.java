package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.Date;

public class Point implements Serializable {
    private double elevation;
    private double latitude;
    private double longitude;
    private Date time;
}
