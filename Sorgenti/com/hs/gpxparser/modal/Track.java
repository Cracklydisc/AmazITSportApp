package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

public class Track extends Extension implements Serializable {
    private String comment;
    private String description;
    private HashSet<Link> links;
    private String name;
    private int number;
    private String src;
    private ArrayList<TrackSegment> trackSegments;
    private String type;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("trk[");
        sb.append("name:" + this.name + " ");
        sb.append("]");
        return sb.toString();
    }

    public ArrayList<TrackSegment> getTrackSegments() {
        return this.trackSegments;
    }

    public void addTrackSegment(TrackSegment trackSegment) {
        if (this.trackSegments == null) {
            this.trackSegments = new ArrayList();
        }
        this.trackSegments.add(trackSegment);
    }
}
