package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;

public class Metadata extends Extension implements Serializable {
    private Person author;
    private Bounds bounds;
    private Copyright copyright;
    private String desc;
    private String keywords;
    private HashSet<Link> links;
    private String name;
    private Date time;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
