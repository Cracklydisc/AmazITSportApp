package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class TrackSegment extends Extension implements Serializable {
    private ArrayList<Waypoint> waypoints;

    public ArrayList<Waypoint> getWaypoints() {
        return this.waypoints;
    }

    public void setWaypoints(ArrayList<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    public void addWaypoint(Waypoint wp) {
        if (this.waypoints == null) {
            this.waypoints = new ArrayList();
        }
        this.waypoints.add(wp);
    }
}
