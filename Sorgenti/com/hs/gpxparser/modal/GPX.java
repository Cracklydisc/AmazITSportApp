package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.HashSet;

public class GPX extends Extension implements Serializable {
    private String creator;
    private Metadata metadata;
    private HashSet<Route> routes = new HashSet();
    private HashSet<Track> tracks = new HashSet();
    private String version = "1.1";
    private HashSet<Waypoint> waypoints = new HashSet();

    public void addRoute(Route route) {
        if (this.routes == null) {
            this.routes = new HashSet();
        }
        this.routes.add(route);
    }

    public void addTrack(Track track) {
        if (this.tracks == null) {
            this.tracks = new HashSet();
        }
        this.tracks.add(track);
    }

    public void addWaypoint(Waypoint waypoint) {
        if (this.waypoints == null) {
            this.waypoints = new HashSet();
        }
        this.waypoints.add(waypoint);
    }

    public HashSet<Route> getRoutes() {
        return this.routes;
    }

    public HashSet<Track> getTracks() {
        return this.tracks;
    }

    public HashSet<Waypoint> getWaypoints() {
        return this.waypoints;
    }

    public Metadata getMetadata() {
        return this.metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
