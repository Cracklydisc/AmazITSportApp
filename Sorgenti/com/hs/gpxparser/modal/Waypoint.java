package com.hs.gpxparser.modal;

import com.hs.gpxparser.type.Fix;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

public class Waypoint extends Extension implements Serializable {
    private double Offset = 0.5d;
    private double ageOfGpsData;
    private String comment;
    private int dGpsStationId;
    private String description;
    private double elevation;
    private int enlarge = 1000;
    private Fix fix;
    private double geoIdHeight;
    private double hdop;
    private int intLat;
    private int intLon;
    private double latitude;
    private HashSet<Link> links;
    private double longitude;
    private double magneticVariation;
    private String name;
    private double pdop;
    private int sat;
    private String src;
    private String sym;
    private Date time;
    private String type;
    private double vdop;

    public Waypoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String getName() {
        return this.name;
    }

    public void setElevation(double elevation) {
        this.elevation = elevation;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date = "";
        if (this.time != null) {
            date = sdf.format(this.time);
        }
        sb.append("[");
        sb.append("name:'" + this.name + "' ");
        sb.append("lat:" + this.latitude + " ");
        sb.append("lon:" + this.longitude + " ");
        sb.append("elv:" + this.elevation + " ");
        sb.append("time:" + date + " ");
        sb.append("fix:" + this.fix + " ");
        if (this.extensionData != null) {
            sb.append("extensions:{");
            Iterator<String> it = this.extensionData.keySet().iterator();
            while (it.hasNext()) {
                sb.append((String) it.next());
                if (it.hasNext()) {
                    sb.append(",");
                }
            }
            sb.append("}");
        }
        sb.append("]");
        return sb.toString();
    }

    public int getEnlargeLat() {
        return (int) ((((double) this.enlarge) * this.latitude) + this.Offset);
    }

    public int getEnlargeLon() {
        return (int) ((((double) this.enlarge) * this.longitude) + this.Offset);
    }

    public void setEnlarge(int enlarge) {
        this.enlarge = enlarge;
    }
}
