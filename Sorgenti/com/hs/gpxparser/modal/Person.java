package com.hs.gpxparser.modal;

import java.io.Serializable;

public class Person implements Serializable {
    private Email email;
    private Link link;
    private String name;
}
