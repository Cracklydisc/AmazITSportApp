package com.hs.gpxparser.modal;

import java.io.Serializable;

public class Link implements Serializable {
    private String href;
    private String text;
    private String type;
}
