package com.hs.gpxparser.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

public class Route extends Extension implements Serializable {
    private String comment;
    private String description;
    private HashSet<Link> links;
    private String name;
    private int number;
    private ArrayList<Waypoint> routePoints;
    private String src;
    private String type;

    public ArrayList<Waypoint> getRoutePoints() {
        return this.routePoints;
    }

    public void addRoutePoint(Waypoint waypoint) {
        if (this.routePoints == null) {
            this.routePoints = new ArrayList();
        }
        this.routePoints.add(waypoint);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("rte[");
        sb.append("name:" + this.name + " ");
        int points = 0;
        if (this.routePoints != null) {
            points = this.routePoints.size();
        }
        sb.append("rtepts:" + points + " ");
        sb.append("]");
        return sb.toString();
    }
}
