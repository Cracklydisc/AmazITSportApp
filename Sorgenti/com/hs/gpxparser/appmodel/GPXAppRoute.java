package com.hs.gpxparser.appmodel;

import android.graphics.Bitmap;
import java.io.Serializable;

public class GPXAppRoute implements Serializable {
    private String gpxFileName;
    private Bitmap routeImgBitmap;
    private String routeImgPath;
    private float routeLength;
    private String routeName;

    public void setRouteImgPath(String routeImgPath) {
        this.routeImgPath = routeImgPath;
    }

    public void setRouteLength(float routeLength) {
        this.routeLength = routeLength;
    }

    public String getGpxFileName() {
        return this.gpxFileName;
    }

    public void setGpxFileName(String gpxFileName) {
        this.gpxFileName = gpxFileName;
    }

    public Bitmap getRouteImgBitmap() {
        return this.routeImgBitmap;
    }

    public void setRouteImgBitmap(Bitmap routeImgBitmap) {
        this.routeImgBitmap = routeImgBitmap;
    }
}
