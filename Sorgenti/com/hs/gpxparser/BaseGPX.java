package com.hs.gpxparser;

import com.hs.gpxparser.extension.IExtensionParser;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

class BaseGPX {
    protected final ArrayList<IExtensionParser> extensionParsers = new ArrayList();
    protected final SimpleDateFormat xmlDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    BaseGPX() {
    }
}
