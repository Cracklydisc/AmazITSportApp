package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;

public class LocalBroadcastManager {
    private static final Object mLock = new Object();
    private final Context mAppContext;
    private final ArrayList<BroadcastRecord> mPendingBroadcasts;
    private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> mReceivers;

    class C00331 extends Handler {
        final /* synthetic */ LocalBroadcastManager this$0;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    this.this$0.executePendingBroadcasts();
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
        }
    }

    private static class BroadcastRecord {
        final Intent intent;
        final ArrayList<ReceiverRecord> receivers;
    }

    private static class ReceiverRecord {
        final IntentFilter filter;
        final BroadcastReceiver receiver;

        public String toString() {
            StringBuilder builder = new StringBuilder(128);
            builder.append("Receiver{");
            builder.append(this.receiver);
            builder.append(" filter=");
            builder.append(this.filter);
            builder.append("}");
            return builder.toString();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void executePendingBroadcasts() {
        /*
        r8 = this;
    L_0x0000:
        r2 = 0;
        r6 = r8.mReceivers;
        monitor-enter(r6);
        r5 = r8.mPendingBroadcasts;	 Catch:{ all -> 0x003e }
        r0 = r5.size();	 Catch:{ all -> 0x003e }
        if (r0 > 0) goto L_0x000e;
    L_0x000c:
        monitor-exit(r6);	 Catch:{ all -> 0x003e }
        return;
    L_0x000e:
        r2 = new android.support.v4.content.LocalBroadcastManager.BroadcastRecord[r0];	 Catch:{ all -> 0x003e }
        r5 = r8.mPendingBroadcasts;	 Catch:{ all -> 0x003e }
        r5.toArray(r2);	 Catch:{ all -> 0x003e }
        r5 = r8.mPendingBroadcasts;	 Catch:{ all -> 0x003e }
        r5.clear();	 Catch:{ all -> 0x003e }
        monitor-exit(r6);	 Catch:{ all -> 0x003e }
        r3 = 0;
    L_0x001c:
        r5 = r2.length;
        if (r3 >= r5) goto L_0x0000;
    L_0x001f:
        r1 = r2[r3];
        r4 = 0;
    L_0x0022:
        r5 = r1.receivers;
        r5 = r5.size();
        if (r4 >= r5) goto L_0x0041;
    L_0x002a:
        r5 = r1.receivers;
        r5 = r5.get(r4);
        r5 = (android.support.v4.content.LocalBroadcastManager.ReceiverRecord) r5;
        r5 = r5.receiver;
        r6 = r8.mAppContext;
        r7 = r1.intent;
        r5.onReceive(r6, r7);
        r4 = r4 + 1;
        goto L_0x0022;
    L_0x003e:
        r5 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x003e }
        throw r5;
    L_0x0041:
        r3 = r3 + 1;
        goto L_0x001c;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.content.LocalBroadcastManager.executePendingBroadcasts():void");
    }
}
