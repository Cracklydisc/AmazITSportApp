package android.support.v4.app;

import android.app.Notification;
import android.app.Service;
import android.os.RemoteException;
import android.support.v4.app.INotificationSideChannel.Stub;

public abstract class NotificationCompatSideChannelService extends Service {

    private class NotificationSideChannelStub extends Stub {
        final /* synthetic */ NotificationCompatSideChannelService this$0;

        public void notify(String packageName, int id, String tag, Notification notification) throws RemoteException {
            this.this$0.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                this.this$0.notify(packageName, id, tag, notification);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }

        public void cancel(String packageName, int id, String tag) throws RemoteException {
            this.this$0.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                this.this$0.cancel(packageName, id, tag);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }

        public void cancelAll(String packageName) {
            this.this$0.checkPermission(getCallingUid(), packageName);
            long idToken = clearCallingIdentity();
            try {
                this.this$0.cancelAll(packageName);
            } finally {
                restoreCallingIdentity(idToken);
            }
        }
    }

    public abstract void cancel(String str, int i, String str2);

    public abstract void cancelAll(String str);

    public abstract void notify(String str, int i, String str2, Notification notification);

    private void checkPermission(int callingUid, String packageName) {
        String[] arr$ = getPackageManager().getPackagesForUid(callingUid);
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            if (!arr$[i$].equals(packageName)) {
                i$++;
            } else {
                return;
            }
        }
        throw new SecurityException("NotificationSideChannelService: Uid " + callingUid + " is not authorized for package " + packageName);
    }
}
