package android.support.v4.app;

public abstract class FragmentManager {

    public interface BackStackEntry {
    }

    public interface OnBackStackChangedListener {
        void onBackStackChanged();
    }

    public abstract FragmentTransaction beginTransaction();

    public abstract boolean executePendingTransactions();

    public abstract Fragment findFragmentByTag(String str);

    public abstract void popBackStack(int i, int i2);
}
