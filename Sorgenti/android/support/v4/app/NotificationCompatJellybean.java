package android.support.v4.app;

class NotificationCompatJellybean {
    private static final Object sActionsLock = new Object();
    private static final Object sExtrasLock = new Object();

    public static class Builder implements NotificationBuilderWithActions, NotificationBuilderWithBuilderAccessor {
    }

    NotificationCompatJellybean() {
    }
}
