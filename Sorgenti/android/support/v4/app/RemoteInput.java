package android.support.v4.app;

import android.os.Build.VERSION;
import android.support.v4.app.RemoteInputCompatBase.RemoteInput.Factory;

public class RemoteInput extends android.support.v4.app.RemoteInputCompatBase.RemoteInput {
    public static final Factory FACTORY = new C00321();
    private static final Impl IMPL;

    static class C00321 implements Factory {
        C00321() {
        }
    }

    public static final class Builder {
    }

    interface Impl {
    }

    static class ImplApi20 implements Impl {
        ImplApi20() {
        }
    }

    static class ImplBase implements Impl {
        ImplBase() {
        }
    }

    static class ImplJellybean implements Impl {
        ImplJellybean() {
        }
    }

    static {
        if (VERSION.SDK_INT >= 20) {
            IMPL = new ImplApi20();
        } else if (VERSION.SDK_INT >= 16) {
            IMPL = new ImplJellybean();
        } else {
            IMPL = new ImplBase();
        }
    }
}
