package android.support.v4.app;

import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: FragmentManager */
interface FragmentContainer {
    @Nullable
    View findViewById(int i);

    boolean hasView();
}
