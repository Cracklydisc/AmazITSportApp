package android.support.v4.widget;

import android.widget.ProgressBar;

public class ContentLoadingProgressBar extends ProgressBar {
    private final Runnable mDelayedHide;
    private final Runnable mDelayedShow;
    private boolean mDismissed;
    private boolean mPostedHide;
    private boolean mPostedShow;
    private long mStartTime;

    class C00991 implements Runnable {
        final /* synthetic */ ContentLoadingProgressBar this$0;

        public void run() {
            this.this$0.mPostedHide = false;
            this.this$0.mStartTime = -1;
            this.this$0.setVisibility(8);
        }
    }

    class C01002 implements Runnable {
        final /* synthetic */ ContentLoadingProgressBar this$0;

        public void run() {
            this.this$0.mPostedShow = false;
            if (!this.this$0.mDismissed) {
                this.this$0.mStartTime = System.currentTimeMillis();
                this.this$0.setVisibility(0);
            }
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks();
    }

    private void removeCallbacks() {
        removeCallbacks(this.mDelayedHide);
        removeCallbacks(this.mDelayedShow);
    }
}
