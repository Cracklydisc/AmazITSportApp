package android.support.v4.widget;

import android.widget.SearchView;

class SearchViewCompatIcs {

    public static class MySearchView extends SearchView {
        public void onActionViewCollapsed() {
            setQuery("", false);
            super.onActionViewCollapsed();
        }
    }

    SearchViewCompatIcs() {
    }
}
