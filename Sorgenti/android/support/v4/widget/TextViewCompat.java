package android.support.v4.widget;

import android.os.Build.VERSION;

public class TextViewCompat {
    static final TextViewCompatImpl IMPL;

    interface TextViewCompatImpl {
    }

    static class BaseTextViewCompatImpl implements TextViewCompatImpl {
        BaseTextViewCompatImpl() {
        }
    }

    static class JbMr1TextViewCompatImpl extends BaseTextViewCompatImpl {
        JbMr1TextViewCompatImpl() {
        }
    }

    static class JbMr2TextViewCompatImpl extends JbMr1TextViewCompatImpl {
        JbMr2TextViewCompatImpl() {
        }
    }

    private TextViewCompat() {
    }

    static {
        int version = VERSION.SDK_INT;
        if (version >= 18) {
            IMPL = new JbMr2TextViewCompatImpl();
        } else if (version >= 17) {
            IMPL = new JbMr1TextViewCompatImpl();
        } else {
            IMPL = new BaseTextViewCompatImpl();
        }
    }
}
