package android.support.v4.speech.tts;

import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.speech.tts.UtteranceProgressListener;

class TextToSpeechICSMR1 {

    static class C00681 extends UtteranceProgressListener {
        final /* synthetic */ UtteranceProgressListenerICSMR1 val$listener;

        public void onStart(String utteranceId) {
            this.val$listener.onStart(utteranceId);
        }

        public void onError(String utteranceId) {
            this.val$listener.onError(utteranceId);
        }

        public void onDone(String utteranceId) {
            this.val$listener.onDone(utteranceId);
        }
    }

    static class C00692 implements OnUtteranceCompletedListener {
        final /* synthetic */ UtteranceProgressListenerICSMR1 val$listener;

        public void onUtteranceCompleted(String utteranceId) {
            this.val$listener.onStart(utteranceId);
            this.val$listener.onDone(utteranceId);
        }
    }

    interface UtteranceProgressListenerICSMR1 {
        void onDone(String str);

        void onError(String str);

        void onStart(String str);
    }

    TextToSpeechICSMR1() {
    }
}
