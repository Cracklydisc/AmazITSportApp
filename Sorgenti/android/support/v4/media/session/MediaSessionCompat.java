package android.support.v4.media.session;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.IMediaSession.Stub;
import android.support.v4.media.session.PlaybackStateCompat.Builder;
import android.view.KeyEvent;
import java.util.List;

public class MediaSessionCompat {

    public static abstract class Callback {
        final Object mCallbackObj;

        private class StubApi21 implements android.support.v4.media.session.MediaSessionCompatApi21.Callback {
            private StubApi21() {
            }

            public void onCommand(String command, Bundle extras, ResultReceiver cb) {
                Callback.this.onCommand(command, extras, cb);
            }

            public boolean onMediaButtonEvent(Intent mediaButtonIntent) {
                return Callback.this.onMediaButtonEvent(mediaButtonIntent);
            }

            public void onPlay() {
                Callback.this.onPlay();
            }

            public void onPlayFromMediaId(String mediaId, Bundle extras) {
                Callback.this.onPlayFromMediaId(mediaId, extras);
            }

            public void onPlayFromSearch(String search, Bundle extras) {
                Callback.this.onPlayFromSearch(search, extras);
            }

            public void onSkipToQueueItem(long id) {
                Callback.this.onSkipToQueueItem(id);
            }

            public void onPause() {
                Callback.this.onPause();
            }

            public void onSkipToNext() {
                Callback.this.onSkipToNext();
            }

            public void onSkipToPrevious() {
                Callback.this.onSkipToPrevious();
            }

            public void onFastForward() {
                Callback.this.onFastForward();
            }

            public void onRewind() {
                Callback.this.onRewind();
            }

            public void onStop() {
                Callback.this.onStop();
            }

            public void onSeekTo(long pos) {
                Callback.this.onSeekTo(pos);
            }

            public void onSetRating(Object ratingObj) {
                Callback.this.onSetRating(RatingCompat.fromRating(ratingObj));
            }

            public void onCustomAction(String action, Bundle extras) {
                Callback.this.onCustomAction(action, extras);
            }
        }

        public Callback() {
            if (VERSION.SDK_INT >= 21) {
                this.mCallbackObj = MediaSessionCompatApi21.createCallback(new StubApi21());
            } else {
                this.mCallbackObj = null;
            }
        }

        public void onCommand(String command, Bundle extras, ResultReceiver cb) {
        }

        public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
            return false;
        }

        public void onPlay() {
        }

        public void onPlayFromMediaId(String mediaId, Bundle extras) {
        }

        public void onPlayFromSearch(String query, Bundle extras) {
        }

        public void onSkipToQueueItem(long id) {
        }

        public void onPause() {
        }

        public void onSkipToNext() {
        }

        public void onSkipToPrevious() {
        }

        public void onFastForward() {
        }

        public void onRewind() {
        }

        public void onStop() {
        }

        public void onSeekTo(long pos) {
        }

        public void onSetRating(RatingCompat rating) {
        }

        public void onCustomAction(String action, Bundle extras) {
        }
    }

    interface MediaSessionImpl {
    }

    static class MediaSessionImplApi21 implements MediaSessionImpl {
    }

    static class MediaSessionImplBase implements MediaSessionImpl {
        private final AudioManager mAudioManager;
        private Callback mCallback;
        private final RemoteCallbackList<IMediaControllerCallback> mControllerCallbacks;
        private boolean mDestroyed;
        private Bundle mExtras;
        private int mFlags;
        private final MessageHandler mHandler;
        private int mLocalStream;
        private final Object mLock;
        private MediaMetadataCompat mMetadata;
        private final String mPackageName;
        private List<QueueItem> mQueue;
        private CharSequence mQueueTitle;
        private int mRatingType;
        private PendingIntent mSessionActivity;
        private PlaybackStateCompat mState;
        private final String mTag;
        private VolumeProviderCompat mVolumeProvider;
        private int mVolumeType;

        class C00511 extends android.support.v4.media.VolumeProviderCompat.Callback {
        }

        class C00522 implements android.support.v4.media.session.MediaSessionCompatApi14.Callback {
            final /* synthetic */ Callback val$callback;

            public void onSetRating(Object ratingObj) {
                this.val$callback.onSetRating(RatingCompat.fromRating(ratingObj));
            }

            public void onSeekTo(long pos) {
                this.val$callback.onSeekTo(pos);
            }
        }

        private static final class Command {
            public final String command;
            public final Bundle extras;
            public final ResultReceiver stub;

            public Command(String command, Bundle extras, ResultReceiver stub) {
                this.command = command;
                this.extras = extras;
                this.stub = stub;
            }
        }

        class MediaSessionStub extends Stub {
            final /* synthetic */ MediaSessionImplBase this$0;

            public void sendCommand(String command, Bundle args, ResultReceiverWrapper cb) {
                this.this$0.mHandler.post(15, new Command(command, args, cb.mResultReceiver));
            }

            public boolean sendMediaButton(KeyEvent mediaButton) {
                boolean handlesMediaButtons = (this.this$0.mFlags & 1) != 0;
                if (handlesMediaButtons) {
                    this.this$0.mHandler.post(14, mediaButton);
                }
                return handlesMediaButtons;
            }

            public void registerCallbackListener(IMediaControllerCallback cb) {
                if (this.this$0.mDestroyed) {
                    try {
                        cb.onSessionDestroyed();
                        return;
                    } catch (Exception e) {
                        return;
                    }
                }
                this.this$0.mControllerCallbacks.register(cb);
            }

            public void unregisterCallbackListener(IMediaControllerCallback cb) {
                this.this$0.mControllerCallbacks.unregister(cb);
            }

            public String getPackageName() {
                return this.this$0.mPackageName;
            }

            public String getTag() {
                return this.this$0.mTag;
            }

            public PendingIntent getLaunchPendingIntent() {
                PendingIntent access$1400;
                synchronized (this.this$0.mLock) {
                    access$1400 = this.this$0.mSessionActivity;
                }
                return access$1400;
            }

            public long getFlags() {
                long access$800;
                synchronized (this.this$0.mLock) {
                    access$800 = (long) this.this$0.mFlags;
                }
                return access$800;
            }

            public ParcelableVolumeInfo getVolumeAttributes() {
                int volumeType;
                int stream;
                int controlType;
                int max;
                int current;
                synchronized (this.this$0.mLock) {
                    volumeType = this.this$0.mVolumeType;
                    stream = this.this$0.mLocalStream;
                    VolumeProviderCompat vp = this.this$0.mVolumeProvider;
                    if (volumeType == 2) {
                        controlType = vp.getVolumeControl();
                        max = vp.getMaxVolume();
                        current = vp.getCurrentVolume();
                    } else {
                        controlType = 2;
                        max = this.this$0.mAudioManager.getStreamMaxVolume(stream);
                        current = this.this$0.mAudioManager.getStreamVolume(stream);
                    }
                }
                return new ParcelableVolumeInfo(volumeType, stream, controlType, max, current);
            }

            public void adjustVolume(int direction, int flags, String packageName) {
                this.this$0.adjustVolume(direction, flags);
            }

            public void setVolumeTo(int value, int flags, String packageName) {
                this.this$0.setVolumeTo(value, flags);
            }

            public void play() throws RemoteException {
                this.this$0.mHandler.post(1);
            }

            public void playFromMediaId(String mediaId, Bundle extras) throws RemoteException {
                this.this$0.mHandler.post(2, mediaId, extras);
            }

            public void playFromSearch(String query, Bundle extras) throws RemoteException {
                this.this$0.mHandler.post(3, query, extras);
            }

            public void skipToQueueItem(long id) {
                this.this$0.mHandler.post(4, Long.valueOf(id));
            }

            public void pause() throws RemoteException {
                this.this$0.mHandler.post(5);
            }

            public void stop() throws RemoteException {
                this.this$0.mHandler.post(6);
            }

            public void next() throws RemoteException {
                this.this$0.mHandler.post(7);
            }

            public void previous() throws RemoteException {
                this.this$0.mHandler.post(8);
            }

            public void fastForward() throws RemoteException {
                this.this$0.mHandler.post(9);
            }

            public void rewind() throws RemoteException {
                this.this$0.mHandler.post(10);
            }

            public void seekTo(long pos) throws RemoteException {
                this.this$0.mHandler.post(11, Long.valueOf(pos));
            }

            public void rate(RatingCompat rating) throws RemoteException {
                this.this$0.mHandler.post(12, rating);
            }

            public void sendCustomAction(String action, Bundle args) throws RemoteException {
                this.this$0.mHandler.post(13, action, args);
            }

            public MediaMetadataCompat getMetadata() {
                return this.this$0.mMetadata;
            }

            public PlaybackStateCompat getPlaybackState() {
                return this.this$0.getStateWithUpdatedPosition();
            }

            public List<QueueItem> getQueue() {
                List<QueueItem> access$2000;
                synchronized (this.this$0.mLock) {
                    access$2000 = this.this$0.mQueue;
                }
                return access$2000;
            }

            public CharSequence getQueueTitle() {
                return this.this$0.mQueueTitle;
            }

            public Bundle getExtras() {
                Bundle access$2200;
                synchronized (this.this$0.mLock) {
                    access$2200 = this.this$0.mExtras;
                }
                return access$2200;
            }

            public int getRatingType() {
                return this.this$0.mRatingType;
            }

            public boolean isTransportControlEnabled() {
                return (this.this$0.mFlags & 2) != 0;
            }
        }

        private class MessageHandler extends Handler {
            final /* synthetic */ MediaSessionImplBase this$0;

            public void post(int what, Object obj, Bundle bundle) {
                Message msg = obtainMessage(what, obj);
                msg.setData(bundle);
                msg.sendToTarget();
            }

            public void post(int what, Object obj) {
                obtainMessage(what, obj).sendToTarget();
            }

            public void post(int what) {
                post(what, null);
            }

            public void handleMessage(Message msg) {
                if (this.this$0.mCallback != null) {
                    switch (msg.what) {
                        case 1:
                            this.this$0.mCallback.onPlay();
                            return;
                        case 2:
                            this.this$0.mCallback.onPlayFromMediaId((String) msg.obj, msg.getData());
                            return;
                        case 3:
                            this.this$0.mCallback.onPlayFromSearch((String) msg.obj, msg.getData());
                            return;
                        case 4:
                            this.this$0.mCallback.onSkipToQueueItem(((Long) msg.obj).longValue());
                            return;
                        case 5:
                            this.this$0.mCallback.onPause();
                            return;
                        case 6:
                            this.this$0.mCallback.onStop();
                            return;
                        case 7:
                            this.this$0.mCallback.onSkipToNext();
                            return;
                        case 8:
                            this.this$0.mCallback.onSkipToPrevious();
                            return;
                        case 9:
                            this.this$0.mCallback.onFastForward();
                            return;
                        case 10:
                            this.this$0.mCallback.onRewind();
                            return;
                        case 11:
                            this.this$0.mCallback.onSeekTo(((Long) msg.obj).longValue());
                            return;
                        case 12:
                            this.this$0.mCallback.onSetRating((RatingCompat) msg.obj);
                            return;
                        case 13:
                            this.this$0.mCallback.onCustomAction((String) msg.obj, msg.getData());
                            return;
                        case 14:
                            KeyEvent keyEvent = msg.obj;
                            Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                            intent.putExtra("android.intent.extra.KEY_EVENT", keyEvent);
                            this.this$0.mCallback.onMediaButtonEvent(intent);
                            return;
                        case 15:
                            Command cmd = msg.obj;
                            this.this$0.mCallback.onCommand(cmd.command, cmd.extras, cmd.stub);
                            return;
                        case 16:
                            this.this$0.adjustVolume(((Integer) msg.obj).intValue(), 0);
                            return;
                        case 17:
                            this.this$0.setVolumeTo(((Integer) msg.obj).intValue(), 0);
                            return;
                        default:
                            return;
                    }
                }
            }
        }

        private void adjustVolume(int direction, int flags) {
            if (this.mVolumeType != 2) {
                this.mAudioManager.adjustStreamVolume(direction, this.mLocalStream, flags);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onAdjustVolume(direction);
            }
        }

        private void setVolumeTo(int value, int flags) {
            if (this.mVolumeType != 2) {
                this.mAudioManager.setStreamVolume(this.mLocalStream, value, flags);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onSetVolumeTo(value);
            }
        }

        private PlaybackStateCompat getStateWithUpdatedPosition() {
            long duration = -1;
            synchronized (this.mLock) {
                PlaybackStateCompat state = this.mState;
                if (this.mMetadata != null && this.mMetadata.containsKey("android.media.metadata.DURATION")) {
                    duration = this.mMetadata.getLong("android.media.metadata.DURATION");
                }
            }
            PlaybackStateCompat result = null;
            if (state != null && (state.getState() == 3 || state.getState() == 4 || state.getState() == 5)) {
                long updateTime = state.getLastPositionUpdateTime();
                long currentTime = SystemClock.elapsedRealtime();
                if (updateTime > 0) {
                    long position = ((long) (state.getPlaybackSpeed() * ((float) (currentTime - updateTime)))) + state.getPosition();
                    if (duration >= 0 && position > duration) {
                        position = duration;
                    } else if (position < 0) {
                        position = 0;
                    }
                    Builder builder = new Builder(state);
                    builder.setState(state.getState(), position, state.getPlaybackSpeed(), currentTime);
                    result = builder.build();
                }
            }
            if (result == null) {
                return state;
            }
            return result;
        }
    }

    public interface OnActiveChangeListener {
    }

    public static final class QueueItem implements Parcelable {
        public static final Creator<QueueItem> CREATOR = new C00531();
        private final MediaDescriptionCompat mDescription;
        private final long mId;

        static class C00531 implements Creator<QueueItem> {
            C00531() {
            }

            public QueueItem createFromParcel(Parcel p) {
                return new QueueItem(p);
            }

            public QueueItem[] newArray(int size) {
                return new QueueItem[size];
            }
        }

        private QueueItem(Parcel in) {
            this.mDescription = (MediaDescriptionCompat) MediaDescriptionCompat.CREATOR.createFromParcel(in);
            this.mId = in.readLong();
        }

        public void writeToParcel(Parcel dest, int flags) {
            this.mDescription.writeToParcel(dest, flags);
            dest.writeLong(this.mId);
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaSession.QueueItem {Description=" + this.mDescription + ", Id=" + this.mId + " }";
        }
    }

    static final class ResultReceiverWrapper implements Parcelable {
        public static final Creator<ResultReceiverWrapper> CREATOR = new C00541();
        private ResultReceiver mResultReceiver;

        static class C00541 implements Creator<ResultReceiverWrapper> {
            C00541() {
            }

            public ResultReceiverWrapper createFromParcel(Parcel p) {
                return new ResultReceiverWrapper(p);
            }

            public ResultReceiverWrapper[] newArray(int size) {
                return new ResultReceiverWrapper[size];
            }
        }

        ResultReceiverWrapper(Parcel in) {
            this.mResultReceiver = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(in);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            this.mResultReceiver.writeToParcel(dest, flags);
        }
    }

    public static final class Token implements Parcelable {
        public static final Creator<Token> CREATOR = new C00551();
        private final Object mInner;

        static class C00551 implements Creator<Token> {
            C00551() {
            }

            public Token createFromParcel(Parcel in) {
                Object readParcelable;
                if (VERSION.SDK_INT >= 21) {
                    readParcelable = in.readParcelable(null);
                } else {
                    readParcelable = in.readStrongBinder();
                }
                return new Token(readParcelable);
            }

            public Token[] newArray(int size) {
                return new Token[size];
            }
        }

        Token(Object inner) {
            this.mInner = inner;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            if (VERSION.SDK_INT >= 21) {
                dest.writeParcelable((Parcelable) this.mInner, flags);
            } else {
                dest.writeStrongBinder((IBinder) this.mInner);
            }
        }
    }
}
