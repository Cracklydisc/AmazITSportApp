package android.support.v4.media.session;

public class MediaSessionCompatApi14 {

    public interface Callback {
        void onSeekTo(long j);

        void onSetRating(Object obj);
    }
}
