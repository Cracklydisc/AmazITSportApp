package android.support.v4.media.session;

import android.media.Rating;
import android.support.v4.media.session.MediaSessionCompatApi14.Callback;

public class MediaSessionCompatApi19 {

    static class OnMetadataUpdateListener<T extends Callback> implements android.media.RemoteControlClient.OnMetadataUpdateListener {
        protected final T mCallback;

        public void onMetadataUpdate(int key, Object newValue) {
            if (key == 268435457 && (newValue instanceof Rating)) {
                this.mCallback.onSetRating(newValue);
            }
        }
    }
}
