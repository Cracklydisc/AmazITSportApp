package android.support.v4.media;

import android.support.v4.media.VolumeProviderCompatApi21.Delegate;

public abstract class VolumeProviderCompat {
    private final int mControlType;
    private int mCurrentVolume;
    private final int mMaxVolume;

    class C00471 implements Delegate {
        final /* synthetic */ VolumeProviderCompat this$0;

        public void onSetVolumeTo(int volume) {
            this.this$0.onSetVolumeTo(volume);
        }

        public void onAdjustVolume(int direction) {
            this.this$0.onAdjustVolume(direction);
        }
    }

    public static abstract class Callback {
    }

    public final int getCurrentVolume() {
        return this.mCurrentVolume;
    }

    public final int getVolumeControl() {
        return this.mControlType;
    }

    public final int getMaxVolume() {
        return this.mMaxVolume;
    }

    public void onSetVolumeTo(int volume) {
    }

    public void onAdjustVolume(int direction) {
    }
}
