package android.support.v4.media.routing;

import android.media.MediaRouter;
import android.os.Build.VERSION;
import java.lang.reflect.Method;

class MediaRouterJellybean {

    public interface Callback {
        void onRouteAdded(Object obj);

        void onRouteChanged(Object obj);

        void onRouteGrouped(Object obj, Object obj2, int i);

        void onRouteRemoved(Object obj);

        void onRouteSelected(int i, Object obj);

        void onRouteUngrouped(Object obj, Object obj2);

        void onRouteUnselected(int i, Object obj);

        void onRouteVolumeChanged(Object obj);
    }

    static class CallbackProxy<T extends Callback> extends android.media.MediaRouter.Callback {
        protected final T mCallback;

        public void onRouteSelected(MediaRouter router, int type, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteSelected(type, route);
        }

        public void onRouteUnselected(MediaRouter router, int type, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteUnselected(type, route);
        }

        public void onRouteAdded(MediaRouter router, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteAdded(route);
        }

        public void onRouteRemoved(MediaRouter router, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteRemoved(route);
        }

        public void onRouteChanged(MediaRouter router, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteChanged(route);
        }

        public void onRouteGrouped(MediaRouter router, android.media.MediaRouter.RouteInfo route, android.media.MediaRouter.RouteGroup group, int index) {
            this.mCallback.onRouteGrouped(route, group, index);
        }

        public void onRouteUngrouped(MediaRouter router, android.media.MediaRouter.RouteInfo route, android.media.MediaRouter.RouteGroup group) {
            this.mCallback.onRouteUngrouped(route, group);
        }

        public void onRouteVolumeChanged(MediaRouter router, android.media.MediaRouter.RouteInfo route) {
            this.mCallback.onRouteVolumeChanged(route);
        }
    }

    public static final class GetDefaultRouteWorkaround {
        private Method mGetSystemAudioRouteMethod;

        public GetDefaultRouteWorkaround() {
            if (VERSION.SDK_INT < 16 || VERSION.SDK_INT > 17) {
                throw new UnsupportedOperationException();
            }
            try {
                this.mGetSystemAudioRouteMethod = MediaRouter.class.getMethod("getSystemAudioRoute", new Class[0]);
            } catch (NoSuchMethodException e) {
            }
        }
    }

    public static final class RouteCategory {
    }

    public static final class RouteGroup {
    }

    public static final class RouteInfo {
    }

    public static final class SelectRouteWorkaround {
        private Method mSelectRouteIntMethod;

        public SelectRouteWorkaround() {
            if (VERSION.SDK_INT < 16 || VERSION.SDK_INT > 17) {
                throw new UnsupportedOperationException();
            }
            try {
                this.mSelectRouteIntMethod = MediaRouter.class.getMethod("selectRouteInt", new Class[]{Integer.TYPE, android.media.MediaRouter.RouteInfo.class});
            } catch (NoSuchMethodException e) {
            }
        }
    }

    public static final class UserRouteInfo {
    }

    public interface VolumeCallback {
        void onVolumeSetRequest(Object obj, int i);

        void onVolumeUpdateRequest(Object obj, int i);
    }

    static class VolumeCallbackProxy<T extends VolumeCallback> extends android.media.MediaRouter.VolumeCallback {
        protected final T mCallback;

        public void onVolumeSetRequest(android.media.MediaRouter.RouteInfo route, int volume) {
            this.mCallback.onVolumeSetRequest(route, volume);
        }

        public void onVolumeUpdateRequest(android.media.MediaRouter.RouteInfo route, int direction) {
            this.mCallback.onVolumeUpdateRequest(route, direction);
        }
    }

    MediaRouterJellybean() {
    }
}
