package android.support.v4.media;

import android.view.KeyEvent;
import android.view.KeyEvent.Callback;

public class TransportMediator extends TransportController {
    final TransportPerformer mCallbacks;
    final Callback mKeyEventCallback;

    class C00411 implements TransportMediatorCallback {
        final /* synthetic */ TransportMediator this$0;

        public void handleKey(KeyEvent key) {
            key.dispatch(this.this$0.mKeyEventCallback);
        }

        public void handleAudioFocusChange(int focusChange) {
            this.this$0.mCallbacks.onAudioFocusChange(focusChange);
        }

        public long getPlaybackPosition() {
            return this.this$0.mCallbacks.onGetCurrentPosition();
        }

        public void playbackPositionUpdate(long newPositionMs) {
            this.this$0.mCallbacks.onSeekTo(newPositionMs);
        }
    }

    class C00422 implements Callback {
        final /* synthetic */ TransportMediator this$0;

        public boolean onKeyDown(int keyCode, KeyEvent event) {
            return TransportMediator.isMediaKey(keyCode) ? this.this$0.mCallbacks.onMediaButtonDown(keyCode, event) : false;
        }

        public boolean onKeyLongPress(int keyCode, KeyEvent event) {
            return false;
        }

        public boolean onKeyUp(int keyCode, KeyEvent event) {
            return TransportMediator.isMediaKey(keyCode) ? this.this$0.mCallbacks.onMediaButtonUp(keyCode, event) : false;
        }

        public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
            return false;
        }
    }

    static boolean isMediaKey(int keyCode) {
        switch (keyCode) {
            case 79:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 130:
                return true;
            default:
                return false;
        }
    }
}
