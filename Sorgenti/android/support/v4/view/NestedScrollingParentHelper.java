package android.support.v4.view;

import android.view.View;

public class NestedScrollingParentHelper {
    private int mNestedScrollAxes;

    public void onNestedScrollAccepted(View child, View target, int axes) {
        this.mNestedScrollAxes = axes;
    }

    public int getNestedScrollAxes() {
        return this.mNestedScrollAxes;
    }
}
