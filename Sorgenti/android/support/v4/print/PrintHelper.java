package android.support.v4.print;

public final class PrintHelper {

    public interface OnPrintFinishCallback {
        void onFinish();
    }

    interface PrintHelperVersionImpl {
    }

    private static final class PrintHelperKitkatImpl implements PrintHelperVersionImpl {

        class C00621 implements android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback {
            final /* synthetic */ OnPrintFinishCallback val$callback;

            public void onFinish() {
                this.val$callback.onFinish();
            }
        }

        class C00632 implements android.support.v4.print.PrintHelperKitkat.OnPrintFinishCallback {
            final /* synthetic */ OnPrintFinishCallback val$callback;

            public void onFinish() {
                this.val$callback.onFinish();
            }
        }
    }

    private static final class PrintHelperStubImpl implements PrintHelperVersionImpl {
        int mColorMode = 2;
        int mOrientation = 1;
        int mScaleMode = 2;

        private PrintHelperStubImpl() {
        }
    }
}
