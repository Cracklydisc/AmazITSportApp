package android.support.v7.util;

import java.util.Comparator;

public class SortedList<T> {

    public static abstract class Callback<T2> implements Comparator<T2> {
        public abstract int compare(T2 t2, T2 t22);
    }

    public static class BatchedCallback<T2> extends Callback<T2> {
        private final Callback<T2> mWrappedCallback;

        public int compare(T2 o1, T2 o2) {
            return this.mWrappedCallback.compare(o1, o2);
        }
    }
}
