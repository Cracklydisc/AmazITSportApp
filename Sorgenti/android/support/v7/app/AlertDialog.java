package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertController.AlertParams;
import android.support.v7.appcompat.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

public class AlertDialog extends AppCompatDialog implements DialogInterface {
    private AlertController mAlert = new AlertController(getContext(), this, getWindow());

    public static class Builder {
        private final AlertParams f0P;
        private int mTheme;

        public Builder(Context context) {
            this(context, AlertDialog.resolveDialogTheme(context, 0));
        }

        public Builder(Context context, int theme) {
            this.f0P = new AlertParams(new ContextThemeWrapper(context, AlertDialog.resolveDialogTheme(context, theme)));
            this.mTheme = theme;
        }

        public Context getContext() {
            return this.f0P.mContext;
        }

        public Builder setTitle(CharSequence title) {
            this.f0P.mTitle = title;
            return this;
        }

        public Builder setCustomTitle(View customTitleView) {
            this.f0P.mCustomTitleView = customTitleView;
            return this;
        }

        public Builder setIcon(Drawable icon) {
            this.f0P.mIcon = icon;
            return this;
        }

        public Builder setOnKeyListener(OnKeyListener onKeyListener) {
            this.f0P.mOnKeyListener = onKeyListener;
            return this;
        }

        public Builder setAdapter(ListAdapter adapter, OnClickListener listener) {
            this.f0P.mAdapter = adapter;
            this.f0P.mOnClickListener = listener;
            return this;
        }

        public AlertDialog create() {
            AlertDialog dialog = new AlertDialog(this.f0P.mContext, this.mTheme, false);
            this.f0P.apply(dialog.mAlert);
            dialog.setCancelable(this.f0P.mCancelable);
            if (this.f0P.mCancelable) {
                dialog.setCanceledOnTouchOutside(true);
            }
            dialog.setOnCancelListener(this.f0P.mOnCancelListener);
            dialog.setOnDismissListener(this.f0P.mOnDismissListener);
            if (this.f0P.mOnKeyListener != null) {
                dialog.setOnKeyListener(this.f0P.mOnKeyListener);
            }
            return dialog;
        }
    }

    AlertDialog(Context context, int theme, boolean createThemeContextWrapper) {
        super(context, resolveDialogTheme(context, theme));
    }

    static int resolveDialogTheme(Context context, int resid) {
        if (resid >= 16777216) {
            return resid;
        }
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.alertDialogTheme, outValue, true);
        return outValue.resourceId;
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
        this.mAlert.setTitle(title);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAlert.installContent();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mAlert.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (this.mAlert.onKeyUp(keyCode, event)) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
