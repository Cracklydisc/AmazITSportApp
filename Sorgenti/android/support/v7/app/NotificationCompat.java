package android.support.v7.app;

import android.support.v4.app.NotificationCompat.Style;

public class NotificationCompat extends android.support.v4.app.NotificationCompat {

    public static class Builder extends android.support.v4.app.NotificationCompat.Builder {
    }

    private static class IceCreamSandwichExtender extends BuilderExtender {
        private IceCreamSandwichExtender() {
        }
    }

    private static class JellybeanExtender extends BuilderExtender {
        private JellybeanExtender() {
        }
    }

    private static class LollipopExtender extends BuilderExtender {
        private LollipopExtender() {
        }
    }

    public static class MediaStyle extends Style {
        int[] mActionsToShowInCompact = null;
    }
}
