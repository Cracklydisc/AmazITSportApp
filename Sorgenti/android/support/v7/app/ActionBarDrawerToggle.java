package android.support.v7.app;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;

public class ActionBarDrawerToggle implements DrawerListener {
    private final Delegate mActivityImpl;
    private final int mCloseDrawerContentDescRes;
    private boolean mDrawerIndicatorEnabled;
    private final DrawerLayout mDrawerLayout;
    private final int mOpenDrawerContentDescRes;
    private DrawerToggle mSlider;
    private OnClickListener mToolbarNavigationClickListener;

    public interface DelegateProvider {
    }

    class C01251 implements OnClickListener {
        final /* synthetic */ ActionBarDrawerToggle this$0;

        public void onClick(View v) {
            if (this.this$0.mDrawerIndicatorEnabled) {
                this.this$0.toggle();
            } else if (this.this$0.mToolbarNavigationClickListener != null) {
                this.this$0.mToolbarNavigationClickListener.onClick(v);
            }
        }
    }

    public interface Delegate {
        void setActionBarDescription(int i);
    }

    interface DrawerToggle {
        void setPosition(float f);
    }

    static class DrawerArrowDrawableToggle extends DrawerArrowDrawable implements DrawerToggle {
        private final Activity mActivity;

        public void setPosition(float position) {
            if (position == 1.0f) {
                setVerticalMirror(true);
            } else if (position == 0.0f) {
                setVerticalMirror(false);
            }
            super.setProgress(position);
        }

        boolean isLayoutRtl() {
            return ViewCompat.getLayoutDirection(this.mActivity.getWindow().getDecorView()) == 1;
        }
    }

    static class DummyDelegate implements Delegate {
        public void setActionBarDescription(int contentDescRes) {
        }
    }

    private static class HoneycombDelegate implements Delegate {
        final Activity mActivity;
        SetIndicatorInfo mSetIndicatorInfo;

        public void setActionBarDescription(int contentDescRes) {
            this.mSetIndicatorInfo = ActionBarDrawerToggleHoneycomb.setActionBarDescription(this.mSetIndicatorInfo, this.mActivity, contentDescRes);
        }
    }

    private static class JellybeanMr2Delegate implements Delegate {
        final Activity mActivity;

        public void setActionBarDescription(int contentDescRes) {
            ActionBar actionBar = this.mActivity.getActionBar();
            if (actionBar != null) {
                actionBar.setHomeActionContentDescription(contentDescRes);
            }
        }
    }

    static class ToolbarCompatDelegate implements Delegate {
        final CharSequence mDefaultContentDescription;
        final Toolbar mToolbar;

        public void setActionBarDescription(int contentDescRes) {
            if (contentDescRes == 0) {
                this.mToolbar.setNavigationContentDescription(this.mDefaultContentDescription);
            } else {
                this.mToolbar.setNavigationContentDescription(contentDescRes);
            }
        }
    }

    private void toggle() {
        if (this.mDrawerLayout.isDrawerVisible(8388611)) {
            this.mDrawerLayout.closeDrawer(8388611);
        } else {
            this.mDrawerLayout.openDrawer(8388611);
        }
    }

    public void onDrawerSlide(View drawerView, float slideOffset) {
        this.mSlider.setPosition(Math.min(1.0f, Math.max(0.0f, slideOffset)));
    }

    public void onDrawerOpened(View drawerView) {
        this.mSlider.setPosition(1.0f);
        if (this.mDrawerIndicatorEnabled) {
            setActionBarDescription(this.mCloseDrawerContentDescRes);
        }
    }

    public void onDrawerClosed(View drawerView) {
        this.mSlider.setPosition(0.0f);
        if (this.mDrawerIndicatorEnabled) {
            setActionBarDescription(this.mOpenDrawerContentDescRes);
        }
    }

    public void onDrawerStateChanged(int newState) {
    }

    void setActionBarDescription(int contentDescRes) {
        this.mActivityImpl.setActionBarDescription(contentDescRes);
    }
}
