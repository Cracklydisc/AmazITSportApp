package android.support.wearable.view;

import android.annotation.TargetApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowInsets;
import android.widget.FrameLayout;

@TargetApi(20)
public class WatchViewStub extends FrameLayout {
    private boolean mInflateNeeded;
    private boolean mLastKnownRound;
    private OnLayoutInflatedListener mListener;
    private int mRectLayout;
    private int mRoundLayout;
    private boolean mWindowInsetsApplied;
    private boolean mWindowOverscan;

    public interface OnLayoutInflatedListener {
        void onLayoutInflated(WatchViewStub watchViewStub);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mWindowOverscan = Func.getWindowOverscan(this);
        this.mWindowInsetsApplied = false;
        requestApplyInsets();
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mInflateNeeded && !this.mWindowOverscan) {
            inflate();
            this.mInflateNeeded = false;
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        this.mWindowInsetsApplied = true;
        boolean round = insets.isRound();
        if (round != this.mLastKnownRound) {
            this.mLastKnownRound = round;
            this.mInflateNeeded = true;
        }
        if (this.mInflateNeeded) {
            inflate();
            this.mInflateNeeded = false;
        }
        return insets;
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (this.mWindowOverscan && !this.mWindowInsetsApplied) {
            Log.w("WatchViewStub", "onApplyWindowInsets was not called. WatchViewStub should be the the root of your layout. If an OnApplyWindowInsetsListener was attached to this view, it must forward the insets on by calling view.onApplyWindowInsets.");
        }
        super.onLayout(changed, left, top, right, bottom);
    }

    public void inflate() {
        removeAllViews();
        if (this.mRoundLayout == 0 && !isInEditMode()) {
            throw new IllegalStateException("You must supply a roundLayout resource");
        } else if (this.mRectLayout != 0 || isInEditMode()) {
            LayoutInflater.from(getContext()).inflate(this.mLastKnownRound ? this.mRoundLayout : this.mRectLayout, this);
            if (this.mListener != null) {
                this.mListener.onLayoutInflated(this);
            }
        } else {
            throw new IllegalStateException("You must supply a rectLayout resource");
        }
    }
}
