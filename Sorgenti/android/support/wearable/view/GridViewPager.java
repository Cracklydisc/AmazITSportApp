package android.support.wearable.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v4.util.SimpleArrayMap;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.MeasureSpec;
import android.view.View.OnApplyWindowInsetsListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ScrollView;
import android.widget.Scroller;

@TargetApi(20)
public class GridViewPager extends ViewGroup {
    private static final int[] LAYOUT_ATTRS = new int[]{16842931};
    private static final Interpolator OVERSCROLL_INTERPOLATOR = new DragFrictionInterpolator();
    private static final Interpolator SLIDE_INTERPOLATOR = new DecelerateInterpolator(2.5f);
    private int mActivePointerId;
    private GridPagerAdapter mAdapter;
    private OnAdapterChangeListener mAdapterChangeListener;
    private boolean mAdapterChangeNotificationPending;
    private BackgroundController mBackgroundController;
    private boolean mCalledSuper;
    private final int mCloseEnough;
    private int mColMargin;
    private final Point mCurItem;
    private boolean mDatasetChangePending;
    private boolean mDelayPopulate;
    private final Runnable mEndScrollRunnable;
    private int mExpectedCurrentColumnCount;
    private int mExpectedRowCount;
    private boolean mFirstLayout;
    private int mGestureInitialScrollY;
    private float mGestureInitialX;
    private float mGestureInitialY;
    private boolean mInLayout;
    private boolean mIsAbleToDrag;
    private boolean mIsBeingDragged;
    private final SimpleArrayMap<Point, ItemInfo> mItems;
    private final int mMinFlingDistance;
    private final int mMinFlingVelocity;
    private final int mMinUsableVelocity;
    private int mOffscreenPageCount;
    private GridPagerAdapter mOldAdapter;
    private OnApplyWindowInsetsListener mOnApplyWindowInsetsListener;
    private OnPageChangeListener mOnPageChangeListener;
    private float mPointerLastX;
    private float mPointerLastY;
    private final Rect mPopulatedPageBounds;
    private final Rect mPopulatedPages;
    private final SimpleArrayMap<Point, ItemInfo> mRecycledItems;
    private Point mRestoredCurItem;
    private int mRowMargin;
    private SparseIntArray mRowScrollX;
    private int mScrollAxis;
    private int mScrollState;
    private final Scroller mScroller;
    private View mScrollingContent;
    private final Point mTempPoint1;
    private final int mTouchSlop;
    private final int mTouchSlopSquared;
    private VelocityTracker mVelocityTracker;
    private WindowInsets mWindowInsets;

    public interface OnAdapterChangeListener {
        void onAdapterChanged(GridPagerAdapter gridPagerAdapter, GridPagerAdapter gridPagerAdapter2);

        void onDataSetChanged();
    }

    public interface OnPageChangeListener {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, int i2, float f, float f2, int i3, int i4);

        void onPageSelected(int i, int i2);
    }

    class C02381 implements Runnable {
        final /* synthetic */ GridViewPager this$0;

        public void run() {
            this.this$0.setScrollState(0);
            this.this$0.populate();
        }
    }

    private static final class DragFrictionInterpolator implements Interpolator {
        private final float falloffRate;

        public DragFrictionInterpolator() {
            this(4.0f);
        }

        public DragFrictionInterpolator(float falloffRate) {
            this.falloffRate = falloffRate;
        }

        public float getInterpolation(float input) {
            double e = Math.exp((double) ((2.0f * input) * this.falloffRate));
            return ((float) ((e - 1.0d) / (1.0d + e))) * (1.0f / this.falloffRate);
        }
    }

    static class ItemInfo {
        Object object;
        int positionX;
        int positionY;

        ItemInfo() {
        }

        public String toString() {
            return this.positionX + "," + this.positionY + " => " + this.object;
        }
    }

    public static class LayoutParams extends MarginLayoutParams {
        public int gravity;
        public boolean needsMeasure;

        public LayoutParams() {
            super(-1, -1);
        }

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray a = context.obtainStyledAttributes(attrs, GridViewPager.LAYOUT_ATTRS);
            this.gravity = a.getInteger(0, 48);
            a.recycle();
        }
    }

    private class PagerObserver extends DataSetObserver {
        final /* synthetic */ GridViewPager this$0;

        public void onChanged() {
            this.this$0.dataSetChanged();
        }

        public void onInvalidated() {
            this.this$0.dataSetChanged();
        }
    }

    private static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR = new C02391();
        int currentX;
        int currentY;

        static class C02391 implements Creator<SavedState> {
            C02391() {
            }

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.currentX);
            out.writeInt(this.currentY);
        }

        private SavedState(Parcel in) {
            super(in);
            this.currentX = in.readInt();
            this.currentY = in.readInt();
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
        getParent().requestFitSystemWindows();
    }

    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).dispatchApplyWindowInsets(insets);
        }
        this.mWindowInsets = insets;
        return insets.consumeSystemWindowInsets();
    }

    public void setOnApplyWindowInsetsListener(OnApplyWindowInsetsListener listener) {
        this.mOnApplyWindowInsetsListener = listener;
    }

    public WindowInsets dispatchApplyWindowInsets(WindowInsets insets) {
        insets = onApplyWindowInsets(insets);
        if (this.mOnApplyWindowInsetsListener != null) {
            this.mOnApplyWindowInsetsListener.onApplyWindowInsets(this, insets);
        }
        return insets;
    }

    public void requestFitSystemWindows() {
    }

    protected void onDetachedFromWindow() {
        removeCallbacks(this.mEndScrollRunnable);
        super.onDetachedFromWindow();
    }

    private void adapterChanged(GridPagerAdapter oldAdapter, GridPagerAdapter newAdapter) {
        if (this.mAdapterChangeListener != null) {
            this.mAdapterChangeListener.onAdapterChanged(oldAdapter, newAdapter);
        }
        if (this.mBackgroundController != null) {
            this.mBackgroundController.onAdapterChanged(oldAdapter, newAdapter);
        }
    }

    public void scrollTo(int x, int y) {
        if (this.mScrollState == 2 && this.mScrollAxis == 1) {
            x = getRowScrollX(this.mCurItem.y);
        }
        super.scrollTo(0, y);
        scrollCurrentRowTo(x);
    }

    private void setScrollState(int newState) {
        if (this.mScrollState != newState) {
            this.mScrollState = newState;
            if (this.mOnPageChangeListener != null) {
                this.mOnPageChangeListener.onPageScrollStateChanged(newState);
            }
            if (this.mBackgroundController != null) {
                this.mBackgroundController.onPageScrollStateChanged(newState);
            }
        }
    }

    private int getRowScrollX(int row) {
        return this.mRowScrollX.get(row, 0);
    }

    private void setRowScrollX(int row, int scrollX) {
        this.mRowScrollX.put(row, scrollX);
    }

    private void scrollRowTo(int row, int x) {
        if (getRowScrollX(row) != x) {
            int size = getChildCount();
            int scrollAmount = x - getRowScrollX(row);
            for (int i = 0; i < size; i++) {
                View child = getChildAt(i);
                ItemInfo ii = infoForChild(child);
                if (ii != null && ii.positionY == row) {
                    child.offsetLeftAndRight(-scrollAmount);
                    postInvalidateOnAnimation();
                }
            }
            setRowScrollX(row, x);
        }
    }

    private void scrollCurrentRowTo(int x) {
        scrollRowTo(this.mCurItem.y, x);
    }

    private int getContentWidth() {
        return getMeasuredWidth() - (getPaddingLeft() + getPaddingRight());
    }

    private int getContentHeight() {
        return getMeasuredHeight() - (getPaddingTop() + getPaddingBottom());
    }

    public void setCurrentItem(int row, int column, boolean smoothScroll) {
        this.mDelayPopulate = false;
        setCurrentItemInternal(row, column, smoothScroll, false);
    }

    void setCurrentItemInternal(int row, int column, boolean smoothScroll, boolean always) {
        setCurrentItemInternal(row, column, smoothScroll, always, 0);
    }

    void setCurrentItemInternal(int row, int column, boolean smoothScroll, boolean always, int velocity) {
        if (this.mAdapter != null && this.mAdapter.getRowCount() > 0) {
            if (always || !this.mCurItem.equals(column, row) || this.mItems.size() == 0) {
                boolean dispatchSelected;
                row = limit(row, 0, this.mAdapter.getRowCount() - 1);
                column = limit(column, 0, this.mAdapter.getColumnCount(row) - 1);
                if (this.mCurItem.equals(column, row)) {
                    dispatchSelected = false;
                } else {
                    dispatchSelected = true;
                }
                if (this.mFirstLayout) {
                    this.mCurItem.set(0, 0);
                    this.mAdapter.setCurrentColumnForRow(row, column);
                    if (dispatchSelected) {
                        if (this.mOnPageChangeListener != null) {
                            this.mOnPageChangeListener.onPageSelected(row, column);
                        }
                        if (this.mBackgroundController != null) {
                            this.mBackgroundController.onPageSelected(row, column);
                        }
                    }
                    requestLayout();
                    return;
                }
                populate(column, row);
                scrollToItem(column, row, smoothScroll, velocity, dispatchSelected);
            }
        }
    }

    private void scrollToItem(int x, int y, boolean smoothScroll, int velocity, boolean dispatchSelected) {
        ItemInfo curInfo = infoForPosition(x, y);
        int destX = 0;
        int destY = 0;
        if (curInfo != null) {
            destX = computePageLeft(curInfo.positionX) - getPaddingLeft();
            destY = computePageTop(curInfo.positionY) - getPaddingTop();
        }
        this.mAdapter.setCurrentColumnForRow(y, x);
        if (dispatchSelected) {
            if (this.mOnPageChangeListener != null) {
                this.mOnPageChangeListener.onPageSelected(y, x);
            }
            if (this.mBackgroundController != null) {
                this.mBackgroundController.onPageSelected(y, x);
            }
        }
        if (smoothScroll) {
            smoothScrollTo(destX, destY, velocity);
            return;
        }
        completeScroll(false);
        scrollTo(destX, destY);
        pageScrolled(destX, destY);
    }

    void smoothScrollTo(int x, int y, int velocity) {
        if (getChildCount() != 0) {
            int sx = getRowScrollX(this.mCurItem.y);
            int sy = getScrollY();
            int dx = x - sx;
            int dy = y - sy;
            if (dx == 0 && dy == 0) {
                completeScroll(false);
                populate();
                setScrollState(0);
                return;
            }
            setScrollState(2);
            this.mScroller.startScroll(sx, sy, dx, dy, 300);
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    void flingContent(int limitX, int limitY, int velocityX, int velocityY) {
        if (this.mScrollingContent != null) {
            if (velocityX == 0 && velocityY == 0) {
                completeScroll(false);
                setScrollState(0);
                return;
            }
            int minX;
            int maxX;
            int minY;
            int maxY;
            int sx = this.mScrollingContent.getScrollX();
            int sy = this.mScrollingContent.getScrollY();
            setScrollState(3);
            if (velocityX > 0) {
                minX = sx;
                maxX = sx + limitX;
            } else {
                minX = sx + limitX;
                maxX = sx;
            }
            if (velocityY > 0) {
                minY = sy;
                maxY = sy + limitY;
            } else {
                minY = sy + limitY;
                maxY = sy;
            }
            this.mScroller.fling(sx, sy, velocityX, velocityY, minX, maxX, minY, maxY);
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    private ItemInfo addNewItem(int positionX, int positionY) {
        Point key = new Point(positionX, positionY);
        ItemInfo ii = (ItemInfo) this.mRecycledItems.remove(key);
        if (ii == null) {
            ii = new ItemInfo();
            ii.object = this.mAdapter.instantiateItem(this, positionY, positionX);
            ii.positionX = positionX;
            ii.positionY = positionY;
        }
        key.set(positionX, positionY);
        ii.positionX = positionX;
        ii.positionY = positionY;
        this.mItems.put(key, ii);
        return ii;
    }

    private void dataSetChanged() {
        int adapterRowCount = this.mAdapter.getRowCount();
        this.mExpectedRowCount = adapterRowCount;
        Point newCurrItem = new Point(this.mCurItem);
        boolean isUpdating = false;
        SimpleArrayMap<Point, ItemInfo> newItemMap = new SimpleArrayMap();
        for (int i = this.mItems.size() - 1; i >= 0; i--) {
            Point itemKey = (Point) this.mItems.keyAt(i);
            ItemInfo itemInfo = (ItemInfo) this.mItems.valueAt(i);
            Point newItemPos = this.mAdapter.getItemPosition(itemInfo.object);
            if (newItemPos == GridPagerAdapter.POSITION_UNCHANGED) {
                newItemMap.put(itemKey, itemInfo);
            } else if (newItemPos == GridPagerAdapter.POSITION_NONE) {
                if (!isUpdating) {
                    this.mAdapter.startUpdate(this);
                    isUpdating = true;
                }
                this.mAdapter.destroyItem(this, itemInfo.positionY, itemInfo.positionX, itemInfo.object);
                if (this.mCurItem.equals(itemInfo.positionX, itemInfo.positionY)) {
                    newCurrItem.y = limit(this.mCurItem.y, 0, Math.max(0, adapterRowCount - 1));
                    if (newCurrItem.y < adapterRowCount) {
                        newCurrItem.x = limit(this.mCurItem.x, 0, this.mAdapter.getColumnCount(newCurrItem.y) - 1);
                    } else {
                        newCurrItem.x = 0;
                    }
                }
            } else if (!newItemPos.equals(itemInfo.positionX, itemInfo.positionY)) {
                if (this.mCurItem.equals(itemInfo.positionX, itemInfo.positionY)) {
                    newCurrItem.set(newItemPos.x, newItemPos.y);
                }
                itemInfo.positionX = newItemPos.x;
                itemInfo.positionY = newItemPos.y;
                newItemMap.put(new Point(newItemPos), itemInfo);
            }
        }
        this.mItems.clear();
        this.mItems.putAll(newItemMap);
        if (isUpdating) {
            this.mAdapter.finishUpdate(this);
        }
        if (this.mExpectedRowCount > 0) {
            this.mExpectedCurrentColumnCount = this.mAdapter.getColumnCount(newCurrItem.y);
        } else {
            this.mExpectedCurrentColumnCount = 0;
        }
        dispatchOnDataSetChanged();
        setCurrentItemInternal(newCurrItem.y, newCurrItem.x, false, true);
        requestLayout();
    }

    private void dispatchOnDataSetChanged() {
        if (this.mAdapterChangeListener != null) {
            this.mAdapterChangeListener.onDataSetChanged();
        }
        if (this.mBackgroundController != null) {
            this.mBackgroundController.onDataSetChanged();
        }
    }

    private void populate() {
        if (this.mAdapter != null && this.mAdapter.getRowCount() > 0) {
            populate(this.mCurItem.x, this.mCurItem.y);
        }
    }

    private void populate(int newX, int newY) {
        Point oldCurItem = new Point();
        if (!(this.mCurItem.x == newX && this.mCurItem.y == newY)) {
            oldCurItem.set(this.mCurItem.x, this.mCurItem.y);
            this.mCurItem.set(newX, newY);
            for (int row = 0; row < this.mExpectedRowCount; row++) {
                if (row != this.mCurItem.y || this.mScrollState == 0) {
                    setRowScrollX(row, computePageLeft(this.mAdapter.getCurrentColumnForRow(row, this.mCurItem.x)) - getPaddingLeft());
                }
            }
        }
        if (!this.mDelayPopulate && getWindowToken() != null) {
            this.mAdapter.startUpdate(this);
            this.mPopulatedPageBounds.setEmpty();
            int rowCount = this.mAdapter.getRowCount();
            if (this.mExpectedRowCount != rowCount) {
                throw new IllegalStateException("Adapter row count changed without a call to notifyDataSetChanged()");
            }
            int colCount = this.mAdapter.getColumnCount(newY);
            if (colCount < 1) {
                throw new IllegalStateException("All rows must have at least 1 column");
            }
            int i;
            ItemInfo ii;
            Point point;
            this.mExpectedRowCount = rowCount;
            this.mExpectedCurrentColumnCount = colCount;
            int offscreenPages = Math.max(1, this.mOffscreenPageCount);
            int startPosY = Math.max(0, newY - offscreenPages);
            int endPosY = Math.min(rowCount - 1, newY + offscreenPages);
            int startPosX = Math.max(0, newX - offscreenPages);
            int endPosX = Math.min(colCount - 1, newX + offscreenPages);
            for (i = this.mItems.size() - 1; i >= 0; i--) {
                ii = (ItemInfo) this.mItems.valueAt(i);
                Point key;
                if (ii.positionY == newY) {
                    if (ii.positionX >= startPosX && ii.positionX <= endPosX) {
                    }
                    key = (Point) this.mItems.keyAt(i);
                    this.mItems.removeAt(i);
                    key.set(ii.positionX, ii.positionY);
                    this.mRecycledItems.put(key, ii);
                } else {
                    if (ii.positionX == this.mAdapter.getCurrentColumnForRow(ii.positionY, this.mCurItem.x) && ii.positionY >= startPosY && ii.positionY <= endPosY) {
                    }
                    key = (Point) this.mItems.keyAt(i);
                    this.mItems.removeAt(i);
                    key.set(ii.positionX, ii.positionY);
                    this.mRecycledItems.put(key, ii);
                }
            }
            this.mTempPoint1.y = newY;
            this.mTempPoint1.x = startPosX;
            while (this.mTempPoint1.x <= endPosX) {
                if (!this.mItems.containsKey(this.mTempPoint1)) {
                    addNewItem(this.mTempPoint1.x, this.mTempPoint1.y);
                }
                point = this.mTempPoint1;
                point.x++;
            }
            this.mTempPoint1.y = startPosY;
            while (this.mTempPoint1.y <= endPosY) {
                this.mTempPoint1.x = this.mAdapter.getCurrentColumnForRow(this.mTempPoint1.y, newX);
                if (!this.mItems.containsKey(this.mTempPoint1)) {
                    addNewItem(this.mTempPoint1.x, this.mTempPoint1.y);
                }
                point = this.mTempPoint1;
                point.y++;
            }
            for (i = this.mRecycledItems.size() - 1; i >= 0; i--) {
                ii = (ItemInfo) this.mRecycledItems.removeAt(i);
                this.mAdapter.destroyItem(this, ii.positionY, ii.positionX, ii.object);
            }
            this.mRecycledItems.clear();
            this.mAdapter.finishUpdate(this);
            this.mPopulatedPages.set(startPosX, startPosY, endPosX, endPosY);
            this.mPopulatedPageBounds.set(computePageLeft(startPosX) - getPaddingLeft(), computePageTop(startPosY) - getPaddingTop(), computePageLeft(endPosX + 1) - getPaddingRight(), computePageTop(endPosY + 1) + getPaddingBottom());
            if (this.mAdapterChangeNotificationPending) {
                this.mAdapterChangeNotificationPending = false;
                adapterChanged(this.mOldAdapter, this.mAdapter);
                this.mOldAdapter = null;
            }
            if (this.mDatasetChangePending) {
                this.mDatasetChangePending = false;
                dispatchOnDataSetChanged();
            }
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState state = new SavedState(super.onSaveInstanceState());
        state.currentX = this.mCurItem.x;
        state.currentY = this.mCurItem.y;
        return state;
    }

    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedState) {
            SavedState ss = (SavedState) state;
            super.onRestoreInstanceState(ss.getSuperState());
            if (pointInRange(ss.currentX, ss.currentY)) {
                this.mRestoredCurItem = new Point(ss.currentX, ss.currentY);
                return;
            }
            this.mCurItem.set(0, 0);
            scrollTo(0, 0);
            return;
        }
        super.onRestoreInstanceState(state);
    }

    public void addView(View child, int index, android.view.ViewGroup.LayoutParams params) {
        ItemInfo ii = infoForChild(child);
        if (!checkLayoutParams(params)) {
            params = generateLayoutParams(params);
        }
        LayoutParams lp = (LayoutParams) params;
        if (this.mInLayout) {
            lp.needsMeasure = true;
            addViewInLayout(child, index, params);
        } else {
            super.addView(child, index, params);
        }
        if (this.mWindowInsets != null) {
            child.onApplyWindowInsets(this.mWindowInsets);
        }
    }

    public void removeView(View view) {
        ItemInfo ii = infoForChild(view);
        if (this.mInLayout) {
            removeViewInLayout(view);
        } else {
            super.removeView(view);
        }
    }

    private ItemInfo infoForChild(View child) {
        for (int i = 0; i < this.mItems.size(); i++) {
            ItemInfo ii = (ItemInfo) this.mItems.valueAt(i);
            if (ii != null && this.mAdapter.isViewFromObject(child, ii.object)) {
                return ii;
            }
        }
        return null;
    }

    private ItemInfo infoForPosition(Point p) {
        return (ItemInfo) this.mItems.get(p);
    }

    private ItemInfo infoForPosition(int x, int y) {
        this.mTempPoint1.set(x, y);
        return (ItemInfo) this.mItems.get(this.mTempPoint1);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(0, widthMeasureSpec), getDefaultSize(0, heightMeasureSpec));
        this.mInLayout = true;
        populate();
        this.mInLayout = false;
        int size = getChildCount();
        for (int i = 0; i < size; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                LayoutParams lp = (LayoutParams) child.getLayoutParams();
                if (lp != null) {
                    measureChild(child, lp);
                }
            }
        }
    }

    protected void measureChild(View child, LayoutParams lp) {
        int widthMode;
        int heightMode;
        int childDefaultWidth = getContentWidth();
        int childDefaultHeight = getContentHeight();
        if (lp.width == -2) {
            widthMode = 0;
        } else {
            widthMode = 1073741824;
        }
        if (lp.height == -2) {
            heightMode = 0;
        } else {
            heightMode = 1073741824;
        }
        child.measure(getChildMeasureSpec(MeasureSpec.makeMeasureSpec(childDefaultWidth, widthMode), lp.leftMargin + lp.rightMargin, lp.width), getChildMeasureSpec(MeasureSpec.makeMeasureSpec(childDefaultHeight, heightMode), lp.topMargin + lp.bottomMargin, lp.height));
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (!this.mItems.isEmpty()) {
            recomputeScrollPosition(w, oldw, h, oldh, this.mColMargin, this.mColMargin, this.mRowMargin, this.mRowMargin);
        }
    }

    private int computePageLeft(int column) {
        return ((getContentWidth() + this.mColMargin) * column) + getPaddingLeft();
    }

    private int computePageTop(int row) {
        return ((getContentHeight() + this.mRowMargin) * row) + getPaddingTop();
    }

    private void recomputeScrollPosition(int width, int oldWidth, int height, int oldHeight, int colMargin, int oldColMargin, int rowMargin, int oldRowMargin) {
        if (oldWidth <= 0 || oldHeight <= 0) {
            ItemInfo ii = infoForPosition(this.mCurItem);
            if (ii != null) {
                int targetX = computePageLeft(ii.positionX) - getPaddingLeft();
                int targetY = computePageTop(ii.positionY) - getPaddingTop();
                if (targetX != getRowScrollX(ii.positionY) || targetY != getScrollY()) {
                    completeScroll(false);
                    scrollTo(targetX, targetY);
                    return;
                }
                return;
            }
            return;
        }
        float pageOffset = ((float) getRowScrollX(this.mCurItem.y)) / ((float) (((oldWidth - getPaddingLeft()) - getPaddingRight()) + oldColMargin));
        int newOffsetXPixels = (int) (((float) (((width - getPaddingLeft()) - getPaddingRight()) + colMargin)) * pageOffset);
        float pageOffsetY = ((float) getScrollY()) / ((float) (((oldHeight - getPaddingTop()) - getPaddingBottom()) + oldRowMargin));
        int newOffsetYPixels = (int) (((float) (((height - getPaddingTop()) - getPaddingBottom()) + rowMargin)) * pageOffsetY);
        scrollTo(newOffsetXPixels, newOffsetYPixels);
        if (!this.mScroller.isFinished()) {
            ItemInfo targetInfo = infoForPosition(this.mCurItem);
            this.mScroller.startScroll(newOffsetXPixels, newOffsetYPixels, computePageLeft(targetInfo.positionX) - getPaddingLeft(), computePageTop(targetInfo.positionY) - getPaddingTop(), this.mScroller.getDuration() - this.mScroller.timePassed());
        }
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int children = getChildCount();
        for (int i = 0; i < children; i++) {
            View view = getChildAt(i);
            LayoutParams lp = (LayoutParams) view.getLayoutParams();
            if (lp == null) {
                Log.w("GridViewPager", "Got null layout params for child: " + view);
            } else {
                ItemInfo ii = infoForChild(view);
                if (ii == null) {
                    Log.w("GridViewPager", "Unknown child view, not claimed by adapter: " + view);
                } else {
                    if (lp.needsMeasure) {
                        lp.needsMeasure = false;
                        measureChild(view, lp);
                    }
                    int left = computePageLeft(ii.positionX);
                    left = (left - getRowScrollX(ii.positionY)) + lp.leftMargin;
                    int top = computePageTop(ii.positionY) + lp.topMargin;
                    view.layout(left, top, view.getMeasuredWidth() + left, view.getMeasuredHeight() + top);
                }
            }
        }
        if (this.mFirstLayout && !this.mItems.isEmpty()) {
            scrollToItem(this.mCurItem.x, this.mCurItem.y, false, 0, false);
        }
        this.mFirstLayout = false;
    }

    public void computeScroll() {
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            completeScroll(true);
            return;
        }
        if (this.mScrollState != 3) {
            int oldX = getRowScrollX(this.mCurItem.y);
            int oldY = getScrollY();
            int x = this.mScroller.getCurrX();
            int y = this.mScroller.getCurrY();
            if (!(oldX == x && oldY == y)) {
                scrollTo(x, y);
                if (!pageScrolled(x, y)) {
                    this.mScroller.abortAnimation();
                    scrollTo(0, 0);
                }
            }
        } else if (this.mScrollingContent == null) {
            this.mScroller.abortAnimation();
        } else {
            this.mScrollingContent.scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
        }
        ViewCompat.postInvalidateOnAnimation(this);
    }

    private boolean pageScrolled(int xpos, int ypos) {
        if (this.mItems.size() == 0) {
            this.mCalledSuper = false;
            onPageScrolled(0, 0, 0.0f, 0.0f, 0, 0);
            if (this.mCalledSuper) {
                return false;
            }
            throw new IllegalStateException("onPageScrolled did not call superclass implementation");
        }
        ItemInfo ii = infoForCurrentScrollPosition();
        int pageLeft = computePageLeft(ii.positionX);
        int offsetLeftPx = (getPaddingLeft() + xpos) - pageLeft;
        int offsetTopPx = (getPaddingTop() + ypos) - computePageTop(ii.positionY);
        float offsetLeft = ((float) offsetLeftPx) / ((float) (getContentWidth() + this.mColMargin));
        float offsetTop = ((float) offsetTopPx) / ((float) (getContentHeight() + this.mRowMargin));
        this.mCalledSuper = false;
        onPageScrolled(ii.positionX, ii.positionY, offsetLeft, offsetTop, offsetLeftPx, offsetTopPx);
        if (this.mCalledSuper) {
            return true;
        }
        throw new IllegalStateException("onPageScrolled did not call superclass implementation");
    }

    protected void onPageScrolled(int positionX, int positionY, float offsetX, float offsetY, int offsetLeftPx, int offsetTopPx) {
        this.mCalledSuper = true;
        if (this.mOnPageChangeListener != null) {
            this.mOnPageChangeListener.onPageScrolled(positionY, positionX, offsetY, offsetX, offsetTopPx, offsetLeftPx);
        }
        if (this.mBackgroundController != null) {
            this.mBackgroundController.onPageScrolled(positionY, positionX, offsetY, offsetX, offsetTopPx, offsetLeftPx);
        }
    }

    private void completeScroll(boolean postEvents) {
        boolean needPopulate;
        if (this.mScrollState == 2) {
            needPopulate = true;
        } else {
            needPopulate = false;
        }
        if (needPopulate) {
            this.mScroller.abortAnimation();
            int oldX = getRowScrollX(this.mCurItem.y);
            int oldY = getScrollY();
            int x = this.mScroller.getCurrX();
            int y = this.mScroller.getCurrY();
            if (!(oldX == x && oldY == y)) {
                scrollTo(x, y);
            }
        }
        this.mScrollingContent = null;
        this.mDelayPopulate = false;
        if (!needPopulate) {
            return;
        }
        if (postEvents) {
            ViewCompat.postOnAnimation(this, this.mEndScrollRunnable);
        } else {
            this.mEndScrollRunnable.run();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int action = ev.getAction() & 255;
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            this.mIsAbleToDrag = false;
            this.mActivePointerId = -1;
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
            }
            return false;
        }
        if (action != 0) {
            if (this.mIsBeingDragged) {
                return true;
            }
            if (!this.mIsAbleToDrag) {
                return false;
            }
        }
        switch (action) {
            case 0:
                handlePointerDown(ev);
                break;
            case 2:
                handlePointerMove(ev);
                break;
            case 6:
                onSecondaryPointerUp(ev);
                break;
        }
        return this.mIsBeingDragged;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mAdapter == null) {
            return false;
        }
        int action = ev.getAction();
        switch (action & 255) {
            case 0:
                handlePointerDown(ev);
                break;
            case 1:
            case 3:
                handlePointerUp(ev);
                break;
            case 2:
                handlePointerMove(ev);
                break;
            case 6:
                onSecondaryPointerUp(ev);
                break;
            default:
                Log.e("GridViewPager", "Unknown action type: " + action);
                break;
        }
        return true;
    }

    private void requestParentDisallowInterceptTouchEvent(boolean disallowIntercept) {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(disallowIntercept);
        }
    }

    private static float limit(float input, int limit) {
        if (limit > 0) {
            return Math.max(0.0f, Math.min(input, (float) limit));
        }
        return Math.min(0.0f, Math.max(input, (float) limit));
    }

    private boolean performDrag(float x, float y) {
        float deltaX = this.mPointerLastX - x;
        float deltaY = this.mPointerLastY - y;
        this.mPointerLastX = x;
        this.mPointerLastY = y;
        Rect pages = this.mPopulatedPages;
        int leftBound = computePageLeft(pages.left) - getPaddingLeft();
        int rightBound = computePageLeft(pages.right) - getPaddingLeft();
        int topBound = computePageTop(pages.top) - getPaddingTop();
        int bottomBound = computePageTop(pages.bottom) - getPaddingTop();
        float scrollX = (float) getRowScrollX(this.mCurItem.y);
        float scrollY = (float) getScrollY();
        if (this.mScrollAxis == 1) {
            float distanceToFocusPoint;
            int pageSpacingY = getContentHeight() + this.mRowMargin;
            if (deltaY < 0.0f) {
                distanceToFocusPoint = -(scrollY % ((float) pageSpacingY));
            } else {
                distanceToFocusPoint = (((float) pageSpacingY) - (scrollY % ((float) pageSpacingY))) % ((float) pageSpacingY);
            }
            boolean focalPointCrossed = false;
            if (Math.abs(distanceToFocusPoint) <= Math.abs(deltaY)) {
                deltaY -= distanceToFocusPoint;
                scrollY += distanceToFocusPoint;
                focalPointCrossed = true;
            }
            if (focalPointCrossed) {
                View child = getChildForInfo(infoForScrollPosition((int) scrollX, (int) scrollY));
                if (child != null) {
                    float consumed = limit(deltaY, getScrollableDistance(child, (int) Math.signum(deltaY)));
                    child.scrollBy(0, (int) consumed);
                    deltaY -= consumed;
                    this.mPointerLastY += consumed - ((float) ((int) consumed));
                }
            }
        }
        int targetX = (int) (((float) ((int) deltaX)) + scrollX);
        int targetY = (int) (((float) ((int) deltaY)) + scrollY);
        boolean wouldOverscroll = targetX < leftBound || targetX > rightBound || targetY < topBound || targetY > bottomBound;
        if (wouldOverscroll) {
            int mode = getOverScrollMode();
            boolean couldScroll = (this.mScrollAxis == 0 && leftBound < rightBound) || (this.mScrollAxis == 1 && topBound < bottomBound);
            if (mode == 0 || (couldScroll && mode == 1)) {
                float overscrollX = scrollX > ((float) rightBound) ? scrollX - ((float) rightBound) : scrollX < ((float) leftBound) ? scrollX - ((float) leftBound) : 0.0f;
                float overscrollY = scrollY > ((float) bottomBound) ? scrollY - ((float) bottomBound) : scrollY < ((float) topBound) ? scrollY - ((float) topBound) : 0.0f;
                if (Math.abs(overscrollX) > 0.0f && Math.signum(overscrollX) == Math.signum(deltaX)) {
                    deltaX *= OVERSCROLL_INTERPOLATOR.getInterpolation(1.0f - (Math.abs(overscrollX) / ((float) getContentWidth())));
                }
                if (Math.abs(overscrollY) > 0.0f && Math.signum(overscrollY) == Math.signum(deltaY)) {
                    deltaY *= OVERSCROLL_INTERPOLATOR.getInterpolation(1.0f - (Math.abs(overscrollY) / ((float) getContentHeight())));
                }
            } else {
                deltaX = limit(deltaX, ((float) leftBound) - scrollX, ((float) rightBound) - scrollX);
                deltaY = limit(deltaY, ((float) topBound) - scrollY, ((float) bottomBound) - scrollY);
            }
        }
        scrollX += deltaX;
        scrollY += deltaY;
        this.mPointerLastX += scrollX - ((float) ((int) scrollX));
        this.mPointerLastY += scrollY - ((float) ((int) scrollY));
        scrollTo((int) scrollX, (int) scrollY);
        pageScrolled((int) scrollX, (int) scrollY);
        return true;
    }

    private int getScrollableDistance(View child, int dir) {
        if (child instanceof CardScrollView) {
            return ((CardScrollView) child).getAvailableScrollDelta(dir);
        }
        if (child instanceof ScrollView) {
            return getScrollableDistance((ScrollView) child, dir);
        }
        return 0;
    }

    private int getScrollableDistance(ScrollView view, int direction) {
        if (view.getChildCount() <= 0) {
            return 0;
        }
        View content = view.getChildAt(0);
        int height = view.getHeight();
        int contentHeight = content.getHeight();
        int extra = contentHeight - height;
        if (contentHeight <= height) {
            return 0;
        }
        if (direction > 0) {
            return Math.min(extra - view.getScrollY(), 0);
        }
        if (direction < 0) {
            return -view.getScrollY();
        }
        return 0;
    }

    private View getChildForInfo(ItemInfo ii) {
        if (ii.object != null) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = getChildAt(i);
                if (this.mAdapter.isViewFromObject(child, ii.object)) {
                    return child;
                }
            }
        }
        return null;
    }

    private ItemInfo infoForCurrentScrollPosition() {
        return infoForScrollPosition(getRowScrollX(getScrollY() / (getContentHeight() + this.mRowMargin)), getScrollY());
    }

    private ItemInfo infoForScrollPosition(int scrollX, int scrollY) {
        int y = scrollY / (getContentHeight() + this.mRowMargin);
        int x = scrollX / (getContentWidth() + this.mColMargin);
        ItemInfo ii = infoForPosition(x, y);
        if (ii != null) {
            return ii;
        }
        ii = new ItemInfo();
        ii.positionX = x;
        ii.positionY = y;
        return ii;
    }

    private void onSecondaryPointerUp(MotionEvent ev) {
        int pointerIndex = MotionEventCompat.getActionIndex(ev);
        if (MotionEventCompat.getPointerId(ev, pointerIndex) == this.mActivePointerId) {
            int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            this.mPointerLastX = MotionEventCompat.getX(ev, newPointerIndex);
            this.mPointerLastY = MotionEventCompat.getY(ev, newPointerIndex);
            this.mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
            if (this.mVelocityTracker != null) {
                this.mVelocityTracker.clear();
            }
        }
    }

    private void endDrag() {
        this.mIsBeingDragged = false;
        this.mIsAbleToDrag = false;
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    public boolean canScrollHorizontally(int direction) {
        if (getVisibility() != 0 || this.mAdapter == null || this.mItems.isEmpty()) {
            return false;
        }
        int scrollX = getRowScrollX(this.mCurItem.y);
        int lastColumnIndex = this.mExpectedCurrentColumnCount - 1;
        if (direction > 0) {
            if (getPaddingLeft() + scrollX >= computePageLeft(lastColumnIndex)) {
                return false;
            }
            return true;
        } else if (scrollX <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean canScrollVertically(int direction) {
        if (getVisibility() != 0 || this.mAdapter == null || this.mItems.isEmpty()) {
            return false;
        }
        int scrollY = getScrollY();
        int lastRowIndex = this.mExpectedRowCount - 1;
        if (direction > 0) {
            if (getPaddingTop() + scrollY >= computePageTop(lastRowIndex)) {
                return false;
            }
            return true;
        } else if (scrollY <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event) || executeKeyEvent(event);
    }

    private boolean executeKeyEvent(KeyEvent event) {
        boolean handled = false;
        switch (event.getKeyCode()) {
            case 19:
                handled = pageUp();
                break;
            case 20:
                handled = pageDown();
                break;
            case 21:
                handled = pageLeft();
                break;
            case 22:
                handled = pageRight();
                break;
            case 62:
                debug();
                return true;
        }
        return handled;
    }

    private boolean pageLeft() {
        if (this.mCurItem.x <= 0) {
            return false;
        }
        setCurrentItem(this.mCurItem.x - 1, this.mCurItem.y, true);
        return true;
    }

    private boolean pageRight() {
        if (this.mAdapter == null || this.mCurItem.x >= this.mAdapter.getColumnCount(this.mCurItem.y) - 1) {
            return false;
        }
        setCurrentItem(this.mCurItem.x + 1, this.mCurItem.y, true);
        return true;
    }

    private boolean pageUp() {
        if (this.mCurItem.y <= 0) {
            return false;
        }
        setCurrentItem(this.mCurItem.x, this.mCurItem.y - 1, true);
        return true;
    }

    private boolean pageDown() {
        if (this.mAdapter == null || this.mCurItem.y >= this.mAdapter.getRowCount() - 1) {
            return false;
        }
        setCurrentItem(this.mCurItem.x, this.mCurItem.y + 1, true);
        return true;
    }

    private boolean handlePointerDown(MotionEvent ev) {
        this.mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
        this.mGestureInitialX = ev.getX();
        this.mGestureInitialY = ev.getY();
        this.mGestureInitialScrollY = getScrollY();
        this.mPointerLastX = this.mGestureInitialX;
        this.mPointerLastY = this.mGestureInitialY;
        this.mIsAbleToDrag = true;
        this.mVelocityTracker = VelocityTracker.obtain();
        this.mVelocityTracker.addMovement(ev);
        this.mScroller.computeScrollOffset();
        if (((this.mScrollState == 2 || this.mScrollState == 3) && this.mScrollAxis == 0 && Math.abs(this.mScroller.getFinalX() - this.mScroller.getCurrX()) > this.mCloseEnough) || (this.mScrollAxis == 1 && Math.abs(this.mScroller.getFinalY() - this.mScroller.getCurrY()) > this.mCloseEnough)) {
            this.mScroller.abortAnimation();
            this.mDelayPopulate = false;
            populate();
            this.mIsBeingDragged = true;
            requestParentDisallowInterceptTouchEvent(true);
            setScrollState(1);
        } else {
            completeScroll(false);
            this.mIsBeingDragged = false;
        }
        return false;
    }

    private boolean handlePointerMove(MotionEvent ev) {
        int activePointerId = this.mActivePointerId;
        if (activePointerId == -1) {
            return false;
        }
        int pointerIndex = ev.findPointerIndex(activePointerId);
        if (pointerIndex == -1) {
            return this.mIsBeingDragged;
        }
        float x = MotionEventCompat.getX(ev, pointerIndex);
        float y = MotionEventCompat.getY(ev, pointerIndex);
        float dx = x - this.mPointerLastX;
        float xDiff = Math.abs(dx);
        float dy = y - this.mPointerLastY;
        float yDiff = Math.abs(dy);
        if (this.mIsBeingDragged) {
        }
        if (!this.mIsBeingDragged && (xDiff * xDiff) + (yDiff * yDiff) > ((float) this.mTouchSlopSquared)) {
            float sy;
            float sx;
            this.mIsBeingDragged = true;
            requestParentDisallowInterceptTouchEvent(true);
            setScrollState(1);
            if (yDiff >= xDiff) {
                this.mScrollAxis = 1;
            } else {
                this.mScrollAxis = 0;
            }
            if (yDiff > 0.0f && xDiff > 0.0f) {
                double t = Math.acos(((double) xDiff) / Math.sqrt((double) ((xDiff * xDiff) + (yDiff * yDiff))));
                sy = (float) (Math.sin(t) * ((double) this.mTouchSlop));
                sx = (float) (Math.cos(t) * ((double) this.mTouchSlop));
            } else if (yDiff == 0.0f) {
                sx = (float) this.mTouchSlop;
                sy = 0.0f;
            } else {
                sx = 0.0f;
                sy = (float) this.mTouchSlop;
            }
            this.mPointerLastX = dx > 0.0f ? this.mPointerLastX + sx : this.mPointerLastX - sx;
            this.mPointerLastY = dy > 0.0f ? this.mPointerLastY + sy : this.mPointerLastY - sy;
        }
        if (this.mIsBeingDragged) {
            if (performDrag(this.mScrollAxis == 0 ? x : this.mPointerLastX, this.mScrollAxis == 1 ? y : this.mPointerLastY)) {
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }
        this.mVelocityTracker.addMovement(ev);
        return this.mIsBeingDragged;
    }

    private boolean handlePointerUp(MotionEvent ev) {
        if (!this.mIsBeingDragged || this.mExpectedRowCount == 0) {
            this.mActivePointerId = -1;
            endDrag();
            return false;
        }
        VelocityTracker velocityTracker = this.mVelocityTracker;
        velocityTracker.addMovement(ev);
        velocityTracker.computeCurrentVelocity(1000);
        int activePointerIndex = ev.findPointerIndex(this.mActivePointerId);
        int targetPageX = this.mCurItem.x;
        int targetPageY = this.mCurItem.y;
        int velocity = 0;
        ItemInfo ii = infoForCurrentScrollPosition();
        switch (this.mScrollAxis) {
            case 0:
                int totalDeltaX = (int) (ev.getRawX() - this.mGestureInitialX);
                velocity = (int) velocityTracker.getXVelocity(this.mActivePointerId);
                int currentPageX = ii.positionX;
                float pageOffsetX = ((float) (getRowScrollX(ii.positionY) - computePageLeft(ii.positionX))) / ((float) (getContentWidth() + this.mColMargin));
                targetPageX = determineTargetPage(this.mCurItem.x, currentPageX, pageOffsetX, this.mPopulatedPages.left, this.mPopulatedPages.right, velocity, totalDeltaX);
                break;
            case 1:
                float y = ev.getX(activePointerIndex);
                int totalDeltaY = this.mGestureInitialScrollY - getScrollY();
                velocity = (int) velocityTracker.getYVelocity(this.mActivePointerId);
                int currentPageY = ii.positionY;
                float pageOffsetY = ((float) (getScrollY() - computePageTop(ii.positionY))) / ((float) (getContentHeight() + this.mRowMargin));
                if (pageOffsetY != 0.0f) {
                    targetPageY = determineTargetPage(this.mCurItem.y, currentPageY, pageOffsetY, this.mPopulatedPages.top, this.mPopulatedPages.bottom, velocity, totalDeltaY);
                    break;
                }
                View child = getChildForInfo(infoForCurrentScrollPosition());
                int scrollable = getScrollableDistance(child, -velocity);
                if (scrollable != 0) {
                    this.mScrollingContent = child;
                    if (Math.abs(velocity) >= Math.abs(this.mMinFlingVelocity)) {
                        flingContent(0, scrollable, 0, -velocity);
                        endDrag();
                        break;
                    }
                }
                break;
        }
        if (this.mScrollState != 3) {
            this.mDelayPopulate = true;
            if (targetPageY != this.mCurItem.y) {
                targetPageX = this.mAdapter.getCurrentColumnForRow(targetPageY, this.mCurItem.x);
            }
            setCurrentItemInternal(targetPageY, targetPageX, true, true, velocity);
        }
        this.mActivePointerId = -1;
        endDrag();
        return false;
    }

    private int determineTargetPage(int previousPage, int currentPage, float pageOffset, int firstPage, int lastPage, int velocity, int totalDragDistance) {
        int targetPage;
        if (Math.abs(velocity) < this.mMinUsableVelocity) {
            velocity = (int) Math.copySign((float) velocity, (float) totalDragDistance);
        }
        float flingBoost = (0.5f / Math.max(Math.abs(0.5f - pageOffset), 0.001f)) * 100.0f;
        if (Math.abs(totalDragDistance) <= this.mMinFlingDistance || ((float) Math.abs(velocity)) + flingBoost <= ((float) this.mMinFlingVelocity)) {
            targetPage = Math.round(((float) currentPage) + pageOffset);
        } else if (velocity > 0) {
            targetPage = currentPage;
        } else {
            targetPage = currentPage + 1;
        }
        return limit(targetPage, firstPage, lastPage);
    }

    private static int limit(int val, int min, int max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    }

    private static float limit(float val, float min, float max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    }

    protected android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }

    protected android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return generateDefaultLayoutParams();
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return (p instanceof LayoutParams) && super.checkLayoutParams(p);
    }

    public android.view.ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    public void debug() {
        debug(0);
    }

    protected void debug(int depth) {
        super.debug(depth);
        Log.d("View", debugIndent(depth) + "mCurItem={" + this.mCurItem + "}");
        Log.d("View", debugIndent(depth) + "mAdapter={" + this.mAdapter + "}");
        Log.d("View", debugIndent(depth) + "mRowCount=" + this.mExpectedRowCount);
        Log.d("View", debugIndent(depth) + "mCurrentColumnCount=" + this.mExpectedCurrentColumnCount);
        int count = this.mItems.size();
        if (count != 0) {
            Log.d("View", debugIndent(depth) + "mItems={");
        }
        for (int i = 0; i < count; i++) {
            Log.d("View", debugIndent(depth + 1) + this.mItems.keyAt(i) + " => " + this.mItems.valueAt(i));
        }
        if (count != 0) {
            Log.d("View", debugIndent(depth) + "}");
        }
    }

    protected static String debugIndent(int depth) {
        StringBuilder spaces = new StringBuilder(((depth * 2) + 3) * 2);
        for (int i = 0; i < (depth * 2) + 3; i++) {
            spaces.append(' ').append(' ');
        }
        return spaces.toString();
    }

    private static boolean inRange(int value, int min, int max) {
        return value >= min && value <= max;
    }

    private boolean pointInRange(int x, int y) {
        return inRange(y, 0, this.mExpectedRowCount + -1) && inRange(x, 0, this.mAdapter.getColumnCount(y) - 1);
    }
}
