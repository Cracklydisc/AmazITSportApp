package android.support.wearable.view;

import android.annotation.TargetApi;

@TargetApi(20)
public interface GridPageOptions {

    public interface BackgroundListener {
    }
}
