package android.support.wearable.view;

import android.annotation.TargetApi;
import android.view.WindowInsets;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

@TargetApi(20)
public class WearableHeaderTextView extends TextView {
    private int mCircularLayoutGravity;
    private int mCircularTextSize;

    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        insets = super.onApplyWindowInsets(insets);
        if (insets.isRound()) {
            applyCircularStyles();
            requestLayout();
        }
        return insets;
    }

    private void applyCircularStyles() {
        if (this.mCircularLayoutGravity != 0) {
            LayoutParams lp = (LayoutParams) getLayoutParams();
            lp.gravity = this.mCircularLayoutGravity;
            setLayoutParams(lp);
        }
        if (this.mCircularTextSize != 0) {
            setTextSize((float) this.mCircularTextSize);
        }
    }
}
