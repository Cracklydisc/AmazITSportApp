package android.support.wearable.view;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;

@TargetApi(20)
public class DelayedConfirmationView extends CircledImageView {
    private long mCurrentTimeMs;
    private DelayedConfirmationListener mListener;
    private long mStartTimeMs;
    private Handler mTimerHandler;
    private long mTotalTimeMs;
    private long mUpdateIntervalMs;

    class C02311 extends Handler {
        final /* synthetic */ DelayedConfirmationView this$0;

        public void handleMessage(Message msg) {
            this.this$0.mCurrentTimeMs = SystemClock.elapsedRealtime();
            this.this$0.invalidate();
            if (this.this$0.mCurrentTimeMs - this.this$0.mStartTimeMs < this.this$0.mTotalTimeMs) {
                this.this$0.mTimerHandler.sendEmptyMessageDelayed(0, this.this$0.mUpdateIntervalMs);
            } else if (this.this$0.mStartTimeMs > 0 && this.this$0.mListener != null) {
                this.this$0.mListener.onTimerFinished(this.this$0);
            }
        }
    }

    public interface DelayedConfirmationListener {
        void onTimerFinished(View view);

        void onTimerSelected(View view);
    }

    protected void onDraw(Canvas canvas) {
        if (this.mStartTimeMs > 0) {
            this.mProgress = ((float) (this.mCurrentTimeMs - this.mStartTimeMs)) / ((float) this.mTotalTimeMs);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
            case 2:
                if (this.mListener != null) {
                    this.mListener.onTimerSelected(this);
                    break;
                }
                break;
        }
        return false;
    }
}
