package android.support.wearable.view;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

@TargetApi(20)
public class DismissOverlayView extends FrameLayout {
    private boolean mFirstRun;
    private SharedPreferences mPrefs;

    class C02321 implements OnClickListener {
        final /* synthetic */ Context val$context;

        public void onClick(View v) {
            if (this.val$context instanceof Activity) {
                ((Activity) this.val$context).finish();
            }
        }
    }

    class C02332 implements Runnable {
        final /* synthetic */ DismissOverlayView this$0;

        public void run() {
            this.this$0.hide();
        }
    }

    class C02343 implements Runnable {
        C02343() {
        }

        public void run() {
            DismissOverlayView.this.setVisibility(8);
            DismissOverlayView.this.setAlpha(1.0f);
        }
    }

    private void hide() {
        animate().alpha(0.0f).setDuration(200).withEndAction(new C02343()).start();
        if (this.mFirstRun) {
            this.mFirstRun = false;
            this.mPrefs.edit().putBoolean("first_run", false).apply();
        }
    }

    public boolean performClick() {
        hide();
        return true;
    }
}
