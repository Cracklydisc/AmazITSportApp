package android.support.wearable.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.wearable.C0224R;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.WindowInsets;
import android.widget.FrameLayout;

@TargetApi(20)
public class BoxInsetLayout extends FrameLayout {
    private static float FACTOR = 0.146467f;
    private Rect mForegroundPadding;
    private Rect mInsets;
    private boolean mLastKnownRound;

    public static class LayoutParams extends android.widget.FrameLayout.LayoutParams {
        public int boxedEdges = 0;

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray a = context.obtainStyledAttributes(attrs, C0224R.styleable.BoxInsetLayout_Layout, 0, 0);
            this.boxedEdges = a.getInt(C0224R.styleable.BoxInsetLayout_Layout_layout_box, 0);
            a.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
        }

        public LayoutParams(int width, int height, int gravity, int boxed) {
            super(width, height, gravity);
            this.boxedEdges = boxed;
        }

        public LayoutParams(android.view.ViewGroup.LayoutParams source) {
            super(source);
        }

        public LayoutParams(LayoutParams source) {
            super(source);
            this.boxedEdges = source.boxedEdges;
            this.gravity = source.gravity;
        }
    }

    public BoxInsetLayout(Context context) {
        this(context, null);
    }

    public BoxInsetLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BoxInsetLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (this.mForegroundPadding == null) {
            this.mForegroundPadding = new Rect();
        }
        if (this.mInsets == null) {
            this.mInsets = new Rect();
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        requestApplyInsets();
    }

    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        insets = super.onApplyWindowInsets(insets);
        boolean round = insets.isRound();
        if (round != this.mLastKnownRound) {
            this.mLastKnownRound = round;
            requestLayout();
        }
        this.mInsets.set(insets.getSystemWindowInsetLeft(), insets.getSystemWindowInsetTop(), insets.getSystemWindowInsetRight(), insets.getSystemWindowInsetBottom());
        return insets;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int i;
        int count = getChildCount();
        int maxWidth = 0;
        int maxHeight = 0;
        int childState = 0;
        for (i = 0; i < count; i++) {
            LayoutParams lp;
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                lp = (LayoutParams) child.getLayoutParams();
                int marginLeft = 0;
                int marginRight = 0;
                int marginTop = 0;
                int marginBottom = 0;
                if (this.mLastKnownRound) {
                    if ((lp.boxedEdges & 1) == 0) {
                        marginLeft = lp.leftMargin;
                    }
                    if ((lp.boxedEdges & 4) == 0) {
                        marginRight = lp.rightMargin;
                    }
                    if ((lp.boxedEdges & 2) == 0) {
                        marginTop = lp.topMargin;
                    }
                    if ((lp.boxedEdges & 8) == 0) {
                        marginBottom = lp.bottomMargin;
                    }
                } else {
                    marginLeft = lp.leftMargin;
                    marginTop = lp.topMargin;
                    marginRight = lp.rightMargin;
                    marginBottom = lp.bottomMargin;
                }
                measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
                maxWidth = Math.max(maxWidth, (child.getMeasuredWidth() + marginLeft) + marginRight);
                maxHeight = Math.max(maxHeight, (child.getMeasuredHeight() + marginTop) + marginBottom);
                childState = combineMeasuredStates(childState, child.getMeasuredState());
            }
        }
        maxWidth += ((getPaddingLeft() + this.mForegroundPadding.left) + getPaddingRight()) + this.mForegroundPadding.right;
        maxHeight = Math.max(maxHeight + (((getPaddingTop() + this.mForegroundPadding.top) + getPaddingBottom()) + this.mForegroundPadding.bottom), getSuggestedMinimumHeight());
        maxWidth = Math.max(maxWidth, getSuggestedMinimumWidth());
        Drawable drawable = getForeground();
        if (drawable != null) {
            maxHeight = Math.max(maxHeight, drawable.getMinimumHeight());
            maxWidth = Math.max(maxWidth, drawable.getMinimumWidth());
        }
        setMeasuredDimension(resolveSizeAndState(maxWidth, widthMeasureSpec, childState), resolveSizeAndState(maxHeight, heightMeasureSpec, childState << 16));
        int boxInset = (int) (FACTOR * ((float) Math.max(getMeasuredWidth(), getMeasuredHeight())));
        for (i = 0; i < count; i++) {
            int childWidthMeasureSpec;
            int childHeightMeasureSpec;
            child = getChildAt(i);
            lp = (LayoutParams) child.getLayoutParams();
            int plwf = getPaddingLeft() + this.mForegroundPadding.left;
            int prwf = getPaddingRight() + this.mForegroundPadding.right;
            int ptwf = getPaddingTop() + this.mForegroundPadding.top;
            int pbwf = getPaddingBottom() + this.mForegroundPadding.bottom;
            int totalPadding = 0;
            int totalMargin = 0;
            if (!this.mLastKnownRound || (lp.boxedEdges & 1) == 0) {
                totalMargin = 0 + (lp.leftMargin + plwf);
            } else {
                totalPadding = 0 + boxInset;
            }
            if (!this.mLastKnownRound || (lp.boxedEdges & 4) == 0) {
                totalMargin += lp.rightMargin + prwf;
            } else {
                totalPadding += boxInset;
            }
            if (lp.width == -1) {
                childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth() - totalMargin, 1073741824);
            } else {
                childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, totalPadding + totalMargin, lp.width);
            }
            totalPadding = 0;
            totalMargin = 0;
            if (!this.mLastKnownRound || (lp.boxedEdges & 2) == 0) {
                totalMargin = 0 + (lp.topMargin + ptwf);
            } else {
                totalPadding = 0 + boxInset;
            }
            if (!this.mLastKnownRound || (lp.boxedEdges & 8) == 0) {
                totalMargin += lp.bottomMargin + pbwf;
            } else {
                totalPadding += boxInset;
            }
            if (lp.height == -1) {
                childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight() - totalMargin, 1073741824);
            } else {
                childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, totalPadding + totalMargin, lp.height);
            }
            child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
        }
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        layoutBoxChildren(left, top, right, bottom, false);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void layoutBoxChildren(int r27, int r28, int r29, int r30, boolean r31) {
        /*
        r26 = this;
        r7 = r26.getChildCount();
        r23 = FACTOR;
        r24 = r29 - r27;
        r25 = r30 - r28;
        r24 = java.lang.Math.max(r24, r25);
        r0 = r24;
        r0 = (float) r0;
        r24 = r0;
        r23 = r23 * r24;
        r0 = r23;
        r3 = (int) r0;
        r23 = r26.getPaddingLeft();
        r0 = r26;
        r0 = r0.mForegroundPadding;
        r24 = r0;
        r0 = r24;
        r0 = r0.left;
        r24 = r0;
        r18 = r23 + r24;
        r23 = r29 - r27;
        r24 = r26.getPaddingRight();
        r23 = r23 - r24;
        r0 = r26;
        r0 = r0.mForegroundPadding;
        r24 = r0;
        r0 = r24;
        r0 = r0.right;
        r24 = r0;
        r19 = r23 - r24;
        r23 = r26.getPaddingTop();
        r0 = r26;
        r0 = r0.mForegroundPadding;
        r24 = r0;
        r0 = r24;
        r0 = r0.top;
        r24 = r0;
        r20 = r23 + r24;
        r23 = r30 - r28;
        r24 = r26.getPaddingBottom();
        r23 = r23 - r24;
        r0 = r26;
        r0 = r0.mForegroundPadding;
        r24 = r0;
        r0 = r24;
        r0 = r0.bottom;
        r24 = r0;
        r17 = r23 - r24;
        r10 = 0;
    L_0x0069:
        if (r10 >= r7) goto L_0x01e3;
    L_0x006b:
        r0 = r26;
        r4 = r0.getChildAt(r10);
        r23 = r4.getVisibility();
        r24 = 8;
        r0 = r23;
        r1 = r24;
        if (r0 == r1) goto L_0x0129;
    L_0x007d:
        r12 = r4.getLayoutParams();
        r12 = (android.support.wearable.view.BoxInsetLayout.LayoutParams) r12;
        r22 = r4.getMeasuredWidth();
        r9 = r4.getMeasuredHeight();
        r8 = r12.gravity;
        r23 = -1;
        r0 = r23;
        if (r8 != r0) goto L_0x0096;
    L_0x0093:
        r8 = 8388659; // 0x800033 float:1.1755015E-38 double:4.144548E-317;
    L_0x0096:
        r11 = r26.getLayoutDirection();
        r2 = android.view.Gravity.getAbsoluteGravity(r8, r11);
        r21 = r8 & 112;
        r14 = r4.getPaddingLeft();
        r15 = r4.getPaddingRight();
        r16 = r4.getPaddingTop();
        r13 = r4.getPaddingBottom();
        r0 = r12.width;
        r23 = r0;
        r24 = -1;
        r0 = r23;
        r1 = r24;
        if (r0 != r1) goto L_0x012d;
    L_0x00bc:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x00cd;
    L_0x00c4:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 1;
        if (r23 == 0) goto L_0x00cd;
    L_0x00cc:
        r14 = r3;
    L_0x00cd:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x00de;
    L_0x00d5:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 4;
        if (r23 == 0) goto L_0x00de;
    L_0x00dd:
        r15 = r3;
    L_0x00de:
        r0 = r12.leftMargin;
        r23 = r0;
        r5 = r18 + r23;
    L_0x00e4:
        r0 = r12.height;
        r23 = r0;
        r24 = -1;
        r0 = r23;
        r1 = r24;
        if (r0 != r1) goto L_0x0185;
    L_0x00f0:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x0102;
    L_0x00f8:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 2;
        if (r23 == 0) goto L_0x0102;
    L_0x0100:
        r16 = r3;
    L_0x0102:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x0113;
    L_0x010a:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 8;
        if (r23 == 0) goto L_0x0113;
    L_0x0112:
        r13 = r3;
    L_0x0113:
        r0 = r12.topMargin;
        r23 = r0;
        r6 = r20 + r23;
    L_0x0119:
        r0 = r16;
        r4.setPadding(r14, r0, r15, r13);
        r23 = r5 + r22;
        r24 = r6 + r9;
        r0 = r23;
        r1 = r24;
        r4.layout(r5, r6, r0, r1);
    L_0x0129:
        r10 = r10 + 1;
        goto L_0x0069;
    L_0x012d:
        r23 = r2 & 7;
        switch(r23) {
            case 1: goto L_0x0145;
            case 5: goto L_0x015a;
            default: goto L_0x0132;
        };
    L_0x0132:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x017d;
    L_0x013a:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 1;
        if (r23 == 0) goto L_0x017d;
    L_0x0142:
        r14 = r3;
        r5 = 0;
        goto L_0x00e4;
    L_0x0145:
        r23 = r19 - r18;
        r23 = r23 - r22;
        r23 = r23 / 2;
        r23 = r23 + r18;
        r0 = r12.leftMargin;
        r24 = r0;
        r23 = r23 + r24;
        r0 = r12.rightMargin;
        r24 = r0;
        r5 = r23 - r24;
        goto L_0x00e4;
    L_0x015a:
        if (r31 != 0) goto L_0x0132;
    L_0x015c:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x0173;
    L_0x0164:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 4;
        if (r23 == 0) goto L_0x0173;
    L_0x016c:
        r15 = r3;
        r23 = r29 - r27;
        r5 = r23 - r22;
        goto L_0x00e4;
    L_0x0173:
        r23 = r19 - r22;
        r0 = r12.rightMargin;
        r24 = r0;
        r5 = r23 - r24;
        goto L_0x00e4;
    L_0x017d:
        r0 = r12.leftMargin;
        r23 = r0;
        r5 = r18 + r23;
        goto L_0x00e4;
    L_0x0185:
        switch(r21) {
            case 16: goto L_0x01ac;
            case 48: goto L_0x018f;
            case 80: goto L_0x01c2;
            default: goto L_0x0188;
        };
    L_0x0188:
        r0 = r12.topMargin;
        r23 = r0;
        r6 = r20 + r23;
        goto L_0x0119;
    L_0x018f:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x01a4;
    L_0x0197:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 2;
        if (r23 == 0) goto L_0x01a4;
    L_0x019f:
        r16 = r3;
        r6 = 0;
        goto L_0x0119;
    L_0x01a4:
        r0 = r12.topMargin;
        r23 = r0;
        r6 = r20 + r23;
        goto L_0x0119;
    L_0x01ac:
        r23 = r17 - r20;
        r23 = r23 - r9;
        r23 = r23 / 2;
        r23 = r23 + r20;
        r0 = r12.topMargin;
        r24 = r0;
        r23 = r23 + r24;
        r0 = r12.bottomMargin;
        r24 = r0;
        r6 = r23 - r24;
        goto L_0x0119;
    L_0x01c2:
        r0 = r26;
        r0 = r0.mLastKnownRound;
        r23 = r0;
        if (r23 == 0) goto L_0x01d9;
    L_0x01ca:
        r0 = r12.boxedEdges;
        r23 = r0;
        r23 = r23 & 8;
        if (r23 == 0) goto L_0x01d9;
    L_0x01d2:
        r13 = r3;
        r23 = r30 - r28;
        r6 = r23 - r9;
        goto L_0x0119;
    L_0x01d9:
        r23 = r17 - r9;
        r0 = r12.bottomMargin;
        r24 = r0;
        r6 = r23 - r24;
        goto L_0x0119;
    L_0x01e3:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.wearable.view.BoxInsetLayout.layoutBoxChildren(int, int, int, int, boolean):void");
    }

    public void setForeground(Drawable drawable) {
        super.setForeground(drawable);
        if (this.mForegroundPadding == null) {
            this.mForegroundPadding = new Rect();
        }
        drawable.getPadding(this.mForegroundPadding);
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    protected android.view.ViewGroup.LayoutParams generateLayoutParams(android.view.ViewGroup.LayoutParams p) {
        return new LayoutParams(p);
    }

    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }
}
