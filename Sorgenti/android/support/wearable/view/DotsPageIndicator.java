package android.support.wearable.view;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.wearable.view.GridViewPager.OnAdapterChangeListener;
import android.support.wearable.view.GridViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.MeasureSpec;

@TargetApi(20)
public class DotsPageIndicator extends View implements OnAdapterChangeListener, OnPageChangeListener {
    private GridPagerAdapter mAdapter;
    private OnAdapterChangeListener mAdapterChangeListener;
    private int mColumnCount;
    private int mCurrentState;
    private int mDotFadeInDuration;
    private int mDotFadeOutDelay;
    private int mDotFadeOutDuration;
    private boolean mDotFadeWhenIdle;
    private final Paint mDotPaint;
    private final Paint mDotPaintShadow;
    private final Paint mDotPaintShadowSelected;
    private float mDotRadius;
    private float mDotRadiusSelected;
    private float mDotShadowDx;
    private float mDotShadowDy;
    private float mDotShadowRadius;
    private int mDotSpacing;
    private OnPageChangeListener mPageChangeListener;
    private int mSelectedColumn;
    private int mSelectedRow;
    private boolean mVisible;

    class C02351 extends SimpleAnimatorListener {
        C02351() {
        }

        public void onAnimationComplete(Animator animator) {
            DotsPageIndicator.this.mVisible = false;
            DotsPageIndicator.this.animate().alpha(0.0f).setListener(null).setStartDelay((long) DotsPageIndicator.this.mDotFadeOutDelay).setDuration((long) DotsPageIndicator.this.mDotFadeOutDuration).start();
        }
    }

    private void columnChanged(int column) {
        this.mSelectedColumn = column;
        invalidate();
    }

    private void rowChanged(int row, int column) {
        int count = this.mAdapter.getColumnCount(row);
        if (count != this.mColumnCount) {
            this.mColumnCount = count;
            this.mSelectedColumn = column;
            requestLayout();
        } else if (column != this.mSelectedColumn) {
            this.mSelectedColumn = column;
            invalidate();
        }
    }

    private void fadeIn() {
        this.mVisible = true;
        animate().cancel();
        animate().alpha(1.0f).setStartDelay(0).setDuration((long) this.mDotFadeInDuration).start();
    }

    private void fadeOut() {
        this.mVisible = false;
        animate().cancel();
        animate().alpha(0.0f).setStartDelay(0).setDuration((long) this.mDotFadeOutDuration).start();
    }

    private void fadeInOut() {
        this.mVisible = true;
        animate().cancel();
        animate().alpha(1.0f).setStartDelay(0).setDuration((long) this.mDotFadeInDuration).setListener(new C02351()).start();
    }

    public void onPageScrollStateChanged(int state) {
        if (this.mCurrentState != state) {
            this.mCurrentState = state;
            if (this.mDotFadeWhenIdle && state == 2) {
                if (this.mVisible) {
                    fadeOut();
                } else {
                    fadeInOut();
                }
            }
        }
        if (this.mPageChangeListener != null) {
            this.mPageChangeListener.onPageScrollStateChanged(state);
        }
    }

    public void onPageScrolled(int row, int column, float rowOffset, float columnOffset, int rowOffsetPixels, int columnOffsetPixels) {
        if (this.mDotFadeWhenIdle) {
            if (columnOffset != 0.0f) {
                if (!this.mVisible) {
                    fadeIn();
                }
            } else if (this.mCurrentState == 1 && this.mVisible) {
                fadeOut();
            }
        }
        if (this.mPageChangeListener != null) {
            this.mPageChangeListener.onPageScrolled(row, column, rowOffset, columnOffset, rowOffsetPixels, columnOffsetPixels);
        }
    }

    public void onPageSelected(int row, int column) {
        if (row != this.mSelectedRow) {
            rowChanged(row, column);
        } else if (column != this.mSelectedColumn) {
            columnChanged(column);
        }
        if (this.mPageChangeListener != null) {
            this.mPageChangeListener.onPageSelected(row, column);
        }
    }

    public void onAdapterChanged(GridPagerAdapter oldAdapter, GridPagerAdapter newAdapter) {
        this.mAdapter = newAdapter;
        if (this.mAdapter != null) {
            rowChanged(0, 0);
            fadeInOut();
        }
        if (this.mAdapterChangeListener != null) {
            this.mAdapterChangeListener.onAdapterChanged(oldAdapter, newAdapter);
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int totalWidth;
        int totalHeight;
        if (MeasureSpec.getMode(widthMeasureSpec) == 1073741824) {
            totalWidth = MeasureSpec.getSize(widthMeasureSpec);
            int contentWidth = (totalWidth - getPaddingLeft()) - getPaddingRight();
        } else {
            totalWidth = (getPaddingLeft() + (this.mColumnCount * this.mDotSpacing)) + getPaddingRight();
        }
        if (MeasureSpec.getMode(heightMeasureSpec) == 1073741824) {
            totalHeight = MeasureSpec.getSize(heightMeasureSpec);
            int contentHeight = (totalHeight - getPaddingTop()) - getPaddingBottom();
        } else {
            totalHeight = (getPaddingTop() + ((int) (((float) ((int) Math.ceil((double) (2.0f * Math.max(this.mDotRadius + this.mDotShadowRadius, this.mDotRadiusSelected + this.mDotShadowRadius))))) + this.mDotShadowDy))) + getPaddingBottom();
        }
        setMeasuredDimension(resolveSizeAndState(totalWidth, widthMeasureSpec, 0), resolveSizeAndState(totalHeight, heightMeasureSpec, 0));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mColumnCount > 1) {
            float dotCenterLeft = ((float) getPaddingLeft()) + (((float) this.mDotSpacing) / 2.0f);
            float dotCenterTop = ((float) getHeight()) / 2.0f;
            canvas.save();
            canvas.translate(dotCenterLeft, dotCenterTop);
            for (int i = 0; i < this.mColumnCount; i++) {
                if (i == this.mSelectedColumn) {
                    canvas.drawCircle(this.mDotShadowDx, this.mDotShadowDy, this.mDotRadiusSelected + this.mDotShadowRadius, this.mDotPaintShadowSelected);
                    canvas.drawCircle(0.0f, 0.0f, this.mDotRadiusSelected, this.mDotPaint);
                } else {
                    canvas.drawCircle(this.mDotShadowDx, this.mDotShadowDy, this.mDotRadius + this.mDotShadowRadius, this.mDotPaintShadow);
                    canvas.drawCircle(0.0f, 0.0f, this.mDotRadius, this.mDotPaint);
                }
                canvas.translate((float) this.mDotSpacing, 0.0f);
            }
            canvas.restore();
        }
    }

    public void onDataSetChanged() {
        if (this.mAdapter.getRowCount() > 0) {
            rowChanged(0, 0);
        }
        if (this.mAdapterChangeListener != null) {
            this.mAdapterChangeListener.onDataSetChanged();
        }
    }
}
