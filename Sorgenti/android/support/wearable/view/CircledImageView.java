package android.support.wearable.view;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.ImageView;

@TargetApi(20)
public class CircledImageView extends FrameLayout {
    private int mCircleBorderColor;
    private float mCircleBorderWidth;
    private ColorStateList mCircleColor;
    private boolean mCircleHidden;
    float mCircleRadius;
    private float mCircleRadiusPressed;
    private ImageView mImageView;
    private Rect mIndeterminateBounds;
    private ProgressDrawable mIndeterminateDrawable;
    private final RectF mOval;
    private final Paint mPaint;
    private boolean mPressed;
    protected float mProgress;
    private boolean mProgressIndeterminate;
    private float mShadowVisibility;
    private final float mShadowWidth;

    class C02301 implements Callback {
        final /* synthetic */ CircledImageView this$0;

        public void invalidateDrawable(Drawable drawable) {
            this.this$0.invalidate();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long l) {
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        }
    }

    protected void onDraw(Canvas canvas) {
        float circleRadius;
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        if (this.mPressed) {
            circleRadius = this.mCircleRadiusPressed;
        } else {
            circleRadius = this.mCircleRadius;
        }
        if (this.mShadowWidth > 0.0f && this.mShadowVisibility > 0.0f) {
            this.mOval.set((float) paddingLeft, (float) paddingTop, (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()));
            float radius = (this.mCircleBorderWidth + circleRadius) + (this.mShadowWidth * this.mShadowVisibility);
            this.mPaint.setColor(-16777216);
            this.mPaint.setStyle(Style.FILL);
            this.mPaint.setShader(new RadialGradient(this.mOval.centerX(), this.mOval.centerY(), radius, new int[]{-16777216, 0}, new float[]{0.6f, 1.0f}, TileMode.MIRROR));
            canvas.drawCircle(this.mOval.centerX(), this.mOval.centerY(), radius, this.mPaint);
            this.mPaint.setShader(null);
        }
        if (this.mCircleBorderWidth > 0.0f) {
            this.mOval.set((float) paddingLeft, (float) paddingTop, (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()));
            this.mOval.set(this.mOval.centerX() - circleRadius, this.mOval.centerY() - circleRadius, this.mOval.centerX() + circleRadius, this.mOval.centerY() + circleRadius);
            this.mPaint.setColor(this.mCircleBorderColor);
            this.mPaint.setStyle(Style.STROKE);
            this.mPaint.setStrokeWidth(this.mCircleBorderWidth);
            if (this.mProgressIndeterminate) {
                this.mOval.roundOut(this.mIndeterminateBounds);
                this.mIndeterminateDrawable.setBounds(this.mIndeterminateBounds);
                this.mIndeterminateDrawable.setRingColor(this.mCircleBorderColor);
                this.mIndeterminateDrawable.setRingWidth(this.mCircleBorderWidth);
                this.mIndeterminateDrawable.draw(canvas);
            } else {
                canvas.drawArc(this.mOval, -90.0f, 360.0f * this.mProgress, false, this.mPaint);
            }
        }
        if (!this.mCircleHidden) {
            this.mOval.set((float) paddingLeft, (float) paddingTop, (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()));
            this.mPaint.setColor(this.mCircleColor.getColorForState(getDrawableState(), this.mCircleColor.getDefaultColor()));
            this.mPaint.setStyle(Style.FILL);
            float centerX = this.mOval.centerX();
            float centerY = this.mOval.centerY();
            if (this.mImageView.getDrawable() != null) {
                centerX = (float) Math.round(((float) this.mImageView.getLeft()) + (((float) this.mImageView.getWidth()) / 2.0f));
                centerY = (float) Math.round(((float) this.mImageView.getTop()) + (((float) this.mImageView.getHeight()) / 2.0f));
            }
            canvas.drawCircle(centerX, centerY, circleRadius, this.mPaint);
        }
        super.onDraw(canvas);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width;
        int height;
        float radius = (this.mCircleRadius + this.mCircleBorderWidth) + (this.mShadowWidth * this.mShadowVisibility);
        float desiredWidth = radius * 2.0f;
        float desiredHeight = radius * 2.0f;
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        if (widthMode == 1073741824) {
            width = widthSize;
        } else if (widthMode == Integer.MIN_VALUE) {
            width = (int) Math.min(desiredWidth, (float) widthSize);
        } else {
            width = (int) desiredWidth;
        }
        if (heightMode == 1073741824) {
            height = heightSize;
        } else if (heightMode == Integer.MIN_VALUE) {
            height = (int) Math.min(desiredHeight, (float) heightSize);
        } else {
            height = (int) desiredHeight;
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(width, 1073741824), MeasureSpec.makeMeasureSpec(height, 1073741824));
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    public void showIndeterminateProgress(boolean show) {
        this.mProgressIndeterminate = show;
        if (show) {
            this.mIndeterminateDrawable.startAnimation();
        } else {
            this.mIndeterminateDrawable.stopAnimation();
        }
    }

    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility != 0) {
            showIndeterminateProgress(false);
        } else if (this.mProgressIndeterminate) {
            showIndeterminateProgress(true);
        }
    }

    public void setPressed(boolean pressed) {
        super.setPressed(pressed);
        this.mPressed = pressed;
        invalidate();
    }
}
