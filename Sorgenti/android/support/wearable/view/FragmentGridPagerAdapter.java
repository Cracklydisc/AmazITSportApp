package android.support.wearable.view;

import android.annotation.TargetApi;
import android.support.wearable.view.GridPageOptions.BackgroundListener;

@TargetApi(20)
public abstract class FragmentGridPagerAdapter extends GridPagerAdapter {
    private static final BackgroundListener NOOP_BACKGROUND_OBSERVER = new C02361();

    static class C02361 implements BackgroundListener {
        C02361() {
        }
    }

    private class BackgroundObserver implements BackgroundListener {
    }
}
