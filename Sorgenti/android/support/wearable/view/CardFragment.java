package android.support.wearable.view;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.wearable.C0224R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

@TargetApi(20)
public class CardFragment extends Fragment {
    private boolean mActivityCreated;
    private CardFrame mCard;
    private int mCardGravity = 80;
    private final Rect mCardMargins = new Rect(-1, -1, -1, -1);
    private Rect mCardPadding;
    private CardScrollView mCardScroll;
    private int mExpansionDirection = 1;
    private boolean mExpansionEnabled = true;
    private float mExpansionFactor = 10.0f;
    private boolean mScrollToBottom;
    private boolean mScrollToTop;

    class C02291 implements OnLayoutChangeListener {
        C02291() {
        }

        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            CardFragment.this.mCardScroll.removeOnLayoutChangeListener(this);
            if (CardFragment.this.mScrollToTop) {
                CardFragment.this.mScrollToTop = false;
                CardFragment.this.scrollToTop();
            } else if (CardFragment.this.mScrollToBottom) {
                CardFragment.this.mScrollToBottom = false;
                CardFragment.this.scrollToBottom();
            }
        }
    }

    private void applyCardGravity() {
        LayoutParams lp = (LayoutParams) this.mCard.getLayoutParams();
        lp.gravity = this.mCardGravity;
        this.mCard.setLayoutParams(lp);
    }

    private void applyCardMargins() {
        MarginLayoutParams lp = (MarginLayoutParams) this.mCard.getLayoutParams();
        if (this.mCardMargins.left != -1) {
            lp.leftMargin = this.mCardMargins.left;
        }
        if (this.mCardMargins.top != -1) {
            lp.topMargin = this.mCardMargins.top;
        }
        if (this.mCardMargins.right != -1) {
            lp.rightMargin = this.mCardMargins.right;
        }
        if (this.mCardMargins.bottom != -1) {
            lp.bottomMargin = this.mCardMargins.bottom;
        }
        this.mCard.setLayoutParams(lp);
    }

    public void scrollToTop() {
        if (this.mCardScroll != null) {
            this.mCardScroll.scrollBy(0, this.mCardScroll.getAvailableScrollDelta(-1));
            return;
        }
        this.mScrollToTop = true;
        this.mScrollToBottom = false;
    }

    public void scrollToBottom() {
        if (this.mCardScroll != null) {
            this.mCardScroll.scrollBy(0, this.mCardScroll.getAvailableScrollDelta(1));
            return;
        }
        this.mScrollToTop = true;
        this.mScrollToBottom = false;
    }

    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mCardScroll = new CardScrollView(inflater.getContext());
        this.mCardScroll.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.mCard = new CardFrame(inflater.getContext());
        this.mCard.setLayoutParams(new LayoutParams(-1, -2, this.mCardGravity));
        this.mCard.setExpansionEnabled(this.mExpansionEnabled);
        this.mCard.setExpansionFactor(this.mExpansionFactor);
        this.mCard.setExpansionDirection(this.mExpansionDirection);
        if (this.mCardPadding != null) {
            this.mCard.setContentPadding(this.mCardPadding.left, this.mCardPadding.top, this.mCardPadding.right, this.mCardPadding.bottom);
        }
        this.mCardScroll.addView(this.mCard);
        if (this.mScrollToTop || this.mScrollToBottom) {
            this.mCardScroll.addOnLayoutChangeListener(new C02291());
        }
        Bundle contentSavedState = null;
        if (savedInstanceState != null) {
            contentSavedState = savedInstanceState.getBundle("CardScrollView_content");
        }
        View content = onCreateContentView(inflater, this.mCard, contentSavedState);
        if (content != null) {
            this.mCard.addView(content);
        }
        return this.mCardScroll;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mActivityCreated = true;
        applyCardMargins();
        applyCardGravity();
    }

    public void onDestroy() {
        this.mActivityCreated = false;
        super.onDestroy();
    }

    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(C0224R.layout.watch_card_content, container, false);
        Bundle args = getArguments();
        if (args != null) {
            TextView title = (TextView) view.findViewById(C0224R.id.title);
            if (args.containsKey("CardFragment_title") && title != null) {
                title.setText(args.getCharSequence("CardFragment_title"));
            }
            if (args.containsKey("CardFragment_text")) {
                TextView text = (TextView) view.findViewById(C0224R.id.text);
                if (text != null) {
                    text.setText(args.getCharSequence("CardFragment_text"));
                }
            }
            if (args.containsKey("CardFragment_icon") && title != null) {
                title.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, args.getInt("CardFragment_icon"), 0);
            }
        }
        return view;
    }
}
