package android.support.wearable.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.C0224R;
import android.widget.ImageView;
import android.widget.TextView;

@TargetApi(20)
public class ConfirmationActivity extends Activity {
    private TextView mSuccessMessage;

    class C02251 implements Runnable {
        C02251() {
        }

        public void run() {
            ConfirmationActivity.this.finish();
            ConfirmationActivity.this.overridePendingTransition(0, 17432577);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0224R.layout.confirmation_activity_layout);
        this.mSuccessMessage = (TextView) findViewById(C0224R.id.message);
        String message = null;
        int animationType = 1;
        if (!(getIntent() == null || getIntent().getExtras() == null)) {
            message = getIntent().getStringExtra("message");
            animationType = getIntent().getIntExtra("animation_type", 1);
        }
        if (message != null) {
            this.mSuccessMessage.setText(message);
            ((TextView) findViewById(C0224R.id.error_message)).setText(message);
        }
        boolean shouldRunAnimation = true;
        ImageView view = (ImageView) findViewById(C0224R.id.animation);
        switch (animationType) {
            case 1:
                view.setImageResource(C0224R.drawable.confirmation_animation);
                break;
            case 2:
                view.setImageResource(C0224R.drawable.go_to_phone_animation);
                break;
            case 3:
                shouldRunAnimation = false;
                findViewById(C0224R.id.action_error).setVisibility(0);
                break;
            default:
                throw new IllegalArgumentException("Unknown type of animation: " + animationType);
        }
        if (shouldRunAnimation) {
            AnimationDrawable animation = (AnimationDrawable) view.getDrawable();
            findViewById(C0224R.id.action_success).setVisibility(0);
            animation.start();
            final long duration = getAnimationDuration(animation);
            new Handler().postDelayed(new C02251(), duration);
            this.mSuccessMessage.setAlpha(0.0f);
            this.mSuccessMessage.animate().alpha(1.0f).setStartDelay(50).withEndAction(new Runnable() {
                public void run() {
                    ConfirmationActivity.this.mSuccessMessage.animate().alpha(0.0f).setStartDelay(Math.max(0, duration - (2 * (50 + ConfirmationActivity.this.mSuccessMessage.animate().getDuration()))));
                }
            });
        }
    }

    private static long getAnimationDuration(AnimationDrawable animation) {
        long duration = 0;
        for (int i = 0; i < animation.getNumberOfFrames(); i++) {
            duration += (long) animation.getDuration(i);
        }
        return duration;
    }
}
