package android.support.wearable.watchface;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

@TargetApi(21)
public abstract class Gles2WatchFaceService extends WatchFaceService {
    private static final int[] EGL_CONFIG_ATTRIB_LIST = new int[]{12352, 4, 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12344};
    private static final int[] EGL_CONTEXT_ATTRIB_LIST = new int[]{12440, 2, 12344};
    private static final int[] EGL_SURFACE_ATTRIB_LIST = new int[]{12344};

    public class Engine extends android.support.wearable.watchface.WatchFaceService.Engine {
        private boolean mCalledOnGlContextCreated;
        private final Choreographer mChoreographer;
        private boolean mDestroyed;
        private boolean mDrawRequested;
        private EGLConfig mEglConfig;
        private EGLContext mEglContext;
        private EGLDisplay mEglDisplay;
        private EGLSurface mEglSurface;
        private final FrameCallback mFrameCallback;
        private final Handler mHandler;
        private int mInsetBottom;
        private int mInsetLeft;

        class C02501 implements FrameCallback {
            final /* synthetic */ Engine this$1;

            public void doFrame(long frameTimeNs) {
                if (!this.this$1.mDestroyed && this.this$1.mDrawRequested) {
                    this.this$1.drawFrame();
                }
            }
        }

        class C02512 extends Handler {
            final /* synthetic */ Engine this$1;

            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        this.this$1.invalidate();
                        return;
                    default:
                        return;
                }
            }
        }

        private boolean createEglContext() {
            if (this.mEglDisplay == null) {
                this.mEglDisplay = EGL14.eglGetDisplay(0);
                if (this.mEglDisplay == EGL14.EGL_NO_DISPLAY) {
                    Log.e("Gles2WatchFaceService", "eglGetDisplay returned EGL_NO_DISPLAY");
                    this.mEglDisplay = null;
                    return false;
                }
                int[] version = new int[2];
                if (!EGL14.eglInitialize(this.mEglDisplay, version, 0, version, 1)) {
                    Log.e("Gles2WatchFaceService", "eglInitialize failed");
                    this.mEglDisplay = null;
                    return false;
                }
            }
            if (this.mEglConfig == null) {
                EGLConfig[] eglConfigs = new EGLConfig[1];
                if (EGL14.eglChooseConfig(this.mEglDisplay, Gles2WatchFaceService.EGL_CONFIG_ATTRIB_LIST, 0, eglConfigs, 0, eglConfigs.length, new int[1], 0)) {
                    this.mEglConfig = eglConfigs[0];
                } else {
                    Log.e("Gles2WatchFaceService", "eglChooseConfig failed");
                    return false;
                }
            }
            if (this.mEglContext == null) {
                this.mEglContext = EGL14.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL14.EGL_NO_CONTEXT, Gles2WatchFaceService.EGL_CONTEXT_ATTRIB_LIST, 0);
                if (this.mEglContext == null) {
                    Log.e("Gles2WatchFaceService", "eglCreateContext returning null");
                    return false;
                }
            }
            return true;
        }

        private void makeContextCurrent() {
            EGL14.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            if (Log.isLoggable("Gles2WatchFaceService", 3)) {
                Log.d("Gles2WatchFaceService", "onCreate");
            }
            super.onCreate(surfaceHolder);
            createEglContext();
        }

        public void onDestroy() {
            this.mDestroyed = true;
            this.mHandler.removeMessages(0);
            this.mChoreographer.removeFrameCallback(this.mFrameCallback);
            super.onDestroy();
        }

        public void onGlContextCreated() {
        }

        public void onGlSurfaceCreated(int width, int height) {
        }

        public void onApplyWindowInsets(WindowInsets insets) {
            if (Log.isLoggable("Gles2WatchFaceService", 3)) {
                Log.d("Gles2WatchFaceService", "onApplyWindowInsets: " + insets);
            }
            super.onApplyWindowInsets(insets);
            Rect bounds = getSurfaceHolder().getSurfaceFrame();
            this.mInsetLeft = insets.getSystemWindowInsetLeft();
            this.mInsetBottom = insets.getSystemWindowInsetBottom();
            makeContextCurrent();
            GLES20.glViewport(-this.mInsetLeft, -this.mInsetBottom, bounds.width(), bounds.height());
        }

        public final void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (Log.isLoggable("Gles2WatchFaceService", 3)) {
                Log.d("Gles2WatchFaceService", "onSurfaceChanged");
            }
            super.onSurfaceChanged(holder, format, width, height);
            if (this.mEglSurface != null) {
                EGL14.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = EGL14.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, holder.getSurface(), Gles2WatchFaceService.EGL_SURFACE_ATTRIB_LIST, 0);
            makeContextCurrent();
            GLES20.glViewport(-this.mInsetLeft, -this.mInsetBottom, width, height);
            if (!this.mCalledOnGlContextCreated) {
                this.mCalledOnGlContextCreated = true;
                onGlContextCreated();
            }
            onGlSurfaceCreated(width, height);
            invalidate();
        }

        public final void onSurfaceRedrawNeeded(SurfaceHolder holder) {
            if (Log.isLoggable("Gles2WatchFaceService", 3)) {
                Log.d("Gles2WatchFaceService", "onSurfaceRedrawNeeded");
            }
            super.onSurfaceRedrawNeeded(holder);
            drawFrame();
        }

        public final void onSurfaceDestroyed(SurfaceHolder holder) {
            if (Log.isLoggable("Gles2WatchFaceService", 3)) {
                Log.d("Gles2WatchFaceService", "onSurfaceDestroyed");
            }
            try {
                EGL14.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            } finally {
                super.onSurfaceDestroyed(holder);
            }
        }

        public final void invalidate() {
            if (!this.mDrawRequested) {
                this.mDrawRequested = true;
                this.mChoreographer.postFrameCallback(this.mFrameCallback);
            }
        }

        public void onDraw() {
        }

        private void drawFrame() {
            this.mDrawRequested = false;
            if (this.mEglSurface != null) {
                makeContextCurrent();
                onDraw();
                EGL14.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            }
        }
    }
}
