package android.support.wearable.watchface;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;
import android.view.SurfaceHolder;

@TargetApi(21)
public abstract class CanvasWatchFaceService extends WatchFaceService {

    public class Engine extends android.support.wearable.watchface.WatchFaceService.Engine {
        private Choreographer mChoreographer;
        private boolean mDestroyed;
        private boolean mDrawRequested;
        private final FrameCallback mFrameCallback;
        private final Handler mHandler;

        class C02481 implements FrameCallback {
            final /* synthetic */ Engine this$1;

            public void doFrame(long frameTimeNs) {
                if (!this.this$1.mDestroyed && this.this$1.mDrawRequested) {
                    this.this$1.draw(this.this$1.getSurfaceHolder());
                }
            }
        }

        class C02492 extends Handler {
            final /* synthetic */ Engine this$1;

            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        this.this$1.invalidate();
                        return;
                    default:
                        return;
                }
            }
        }

        public void onDestroy() {
            this.mDestroyed = true;
            this.mHandler.removeMessages(0);
            this.mChoreographer.removeFrameCallback(this.mFrameCallback);
            super.onDestroy();
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (Log.isLoggable("CanvasWatchFaceService", 3)) {
                Log.d("CanvasWatchFaceService", "onSurfaceChanged");
            }
            super.onSurfaceChanged(holder, format, width, height);
            invalidate();
        }

        public void onSurfaceRedrawNeeded(SurfaceHolder holder) {
            if (Log.isLoggable("CanvasWatchFaceService", 3)) {
                Log.d("CanvasWatchFaceService", "onSurfaceRedrawNeeded");
            }
            super.onSurfaceRedrawNeeded(holder);
            draw(holder);
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            if (Log.isLoggable("CanvasWatchFaceService", 3)) {
                Log.d("CanvasWatchFaceService", "onSurfaceCreated");
            }
            super.onSurfaceCreated(holder);
            invalidate();
        }

        public void invalidate() {
            if (!this.mDrawRequested) {
                this.mDrawRequested = true;
                this.mChoreographer.postFrameCallback(this.mFrameCallback);
            }
        }

        public void onDraw(Canvas canvas, Rect bounds) {
        }

        private void draw(SurfaceHolder holder) {
            this.mDrawRequested = false;
            Canvas canvas = holder.lockCanvas();
            if (canvas != null) {
                try {
                    onDraw(canvas, holder.getSurfaceFrame());
                } finally {
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
