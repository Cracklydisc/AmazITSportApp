package clc.component.flower;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import android.text.TextUtils;
import android.util.Log;
import clc.utils.debug.slog.SolidLogger;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BeeClient {
    private String FLOWER_AVALIBLE_ACTION;
    private String UUID;
    private AtomicBoolean mBinded = new AtomicBoolean(false);
    protected ComponentName mBindedServiceComponent = null;
    protected Context mContext;
    private ServiceConnection mCurrConnection = null;
    private BroadcastReceiver mHostReadyReceiver = new C02571();
    private ServiceConnection mReadyOnnection = null;
    private Messenger mRemoteService;
    private Messenger mReplyMessenger;
    private Handler mWorkerHandler;
    private HandlerThread mWorkerThread;

    class C02571 extends BroadcastReceiver {
        C02571() {
        }

        public void onReceive(Context context, Intent intent) {
            String fetchActionForHostAvalible = BeeClient.this.fetchActionForHostAvalible();
            if (!TextUtils.isEmpty(fetchActionForHostAvalible) && fetchActionForHostAvalible.equalsIgnoreCase(intent.getAction())) {
                BeeClient.this.tryToBindService();
            }
        }
    }

    class C02602 implements ServiceConnection {

        class C02581 implements DeathRecipient {
            C02581() {
            }

            public void binderDied() {
                Log.i("WH-APP", "[CLIENT] ==>  HOST DEAD!");
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>  HOST DEAD!");
                BeeClient.this.onFlowerOffline();
                BeeClient.this.rezygote(BeeClient.this.hintRezygoteWithStopServiceFirstly());
            }
        }

        class C02592 implements Runnable {
            C02592() {
            }

            public void run() {
                BeeClient.this.rezygote(BeeClient.this.hintRezygoteWithStopServiceFirstly());
            }
        }

        C02602() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            BeeClient.this.mReadyOnnection = this;
            BeeClient.this.mRemoteService = new Messenger(service);
            try {
                BeeClient.this.mRemoteService.getBinder().linkToDeath(new C02581(), 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            BeeClient.this.onFlowerOnline();
            BeeClient.this.mBinded.set(true);
            BeeClient.this.handshake();
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.i("WH-APP", "[CLIENT] ==> CLIENT onServiceDisconnected : [" + BeeClient.this.mContext.getPackageName() + "] DISCONNECT with host.");
            SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==> CLIENT onServiceDisconnected : [" + BeeClient.this.mContext.getPackageName() + "] DISCONNECT with host.");
            BeeClient.this.mBinded.set(false);
            BeeClient.this.mWorkerHandler.post(new C02592());
        }
    }

    public interface OnBigDataListener {
        void onSendBigData();
    }

    protected abstract String fetchActionForHostAvalible();

    protected abstract String getFlowerAction();

    protected abstract String getFlowerPkg();

    protected BeeClient(Context context) {
        this.mContext = context;
        SolidLogger.withContext(context);
        initHandler();
        this.FLOWER_AVALIBLE_ACTION = fetchActionForHostAvalible();
        if (!TextUtils.isEmpty(this.FLOWER_AVALIBLE_ACTION)) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(this.FLOWER_AVALIBLE_ACTION);
            this.mContext.registerReceiver(this.mHostReadyReceiver, filter);
            tryToBindService();
        }
    }

    private synchronized ServiceConnection generateConnection() {
        return new C02602();
    }

    private void tryToBindService() {
        Log.i("WH-APP", this.mContext.getPackageName() + " try to rebind host data service <><><><>");
        SolidLogger.getInstance().with("WH-APP", this.mContext.getPackageName() + " try to rebind host data service <><><><>");
        try {
            String str;
            Intent intent = new Intent(getFlowerAction());
            intent.setPackage(getFlowerPkg());
            this.mContext.startService(intent);
            Intent service = new Intent(getFlowerAction());
            service.setPackage(getFlowerPkg());
            service.putExtra("<!!KEY&-INNER>[TARGET_PKG]", this.mContext.getPackageName());
            this.mBindedServiceComponent = this.mContext.startService(service);
            if (this.mBindedServiceComponent == null) {
                Log.i("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 失败");
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 失败");
            } else {
                Log.i("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 成功");
                SolidLogger.getInstance().with("WH-APP", "[CLIENT] ==>" + this.mContext.getPackageName() + "start service 成功");
            }
            Context context = this.mContext;
            ServiceConnection generateConnection = generateConnection();
            this.mCurrConnection = generateConnection;
            boolean res = context.bindService(service, generateConnection, 8);
            Log.i("WH-APP", "[CLIENT] ==> [" + this.mContext.getPackageName() + "] client bind service res================> : " + res);
            SolidLogger instance = SolidLogger.getInstance();
            String str2 = "WH-APP";
            StringBuilder append = new StringBuilder().append("[CLIENT] ==> [").append(this.mContext.getPackageName()).append("] client bind service res================> : ");
            if (res) {
                str = "SUCCESS";
            } else {
                str = "FAILED";
            }
            instance.with(str2, append.append(str).toString());
        } catch (Exception e) {
            Log.i("WH-APP", "[CLIENT] ==> bind service failed : ", e);
            SolidLogger.getInstance().with("WH-APP", "[CLIENT]" + this.mContext.getPackageName() + " ==> bind service failed ");
        }
    }

    private void rezygote(boolean simpleStart) {
        Log.i("WH-APP", "[CLIENT] ==> ##########REZYGOTE REMOTE############ :" + this.mBindedServiceComponent);
        Intent i;
        if (!simpleStart) {
            try {
                if (this.mBindedServiceComponent != null) {
                    i = new Intent();
                    i.setComponent(this.mBindedServiceComponent);
                    this.mContext.stopService(i);
                }
                this.mContext.unbindService(this.mReadyOnnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tryToBindService();
        } else if (this.mBindedServiceComponent != null) {
            i = new Intent();
            i.setComponent(this.mBindedServiceComponent);
            this.mContext.startService(i);
        }
    }

    private void initHandler() {
        if (this.mWorkerThread == null) {
            this.mWorkerThread = new HandlerThread("wear-http-client-handler");
            this.mWorkerThread.start();
        }
        if (this.mWorkerHandler == null) {
            this.mWorkerHandler = new Handler(this.mWorkerThread.getLooper()) {
                public void handleMessage(Message msg) {
                    Log.i("WH-APP", "client '" + BeeClient.this.mContext.getPackageName() + "' received msg !" + msg);
                    switch (msg.what) {
                        case Integer.MIN_VALUE:
                            String UUID = msg.getData().getString("<!!KEY&-INNER>[UUID]");
                            if (TextUtils.isEmpty(UUID)) {
                                Log.i("WH-APP", "client '" + BeeClient.this.mContext.getPackageName() + "' UUID IS NULL!!!");
                                throw new IllegalArgumentException("Null UUID Exception !");
                            }
                            Log.i("WH-APP", "client '" + BeeClient.this.mContext.getPackageName() + "' rev uuid : " + UUID);
                            BeeClient.this.UUID = UUID;
                            BeeClient.this.onFlowerReadyToRolling();
                            return;
                        default:
                            BeeClient.this.receiveRemoteMsg(msg);
                            return;
                    }
                }
            };
        }
        if (this.mReplyMessenger == null) {
            this.mReplyMessenger = new Messenger(this.mWorkerHandler);
        }
    }

    private void receiveRemoteMsg(Message msg) {
        Bundle data = msg.getData();
        if (data != null) {
            dataFromFlower(msg.what, data);
        }
    }

    private synchronized void handshake() {
        String pkg = this.mContext.getPackageName();
        Log.i("WH-APP", "\t\t>>>>>> [client] '" + pkg + "' req hand shake ...");
        SolidLogger.getInstance().with("WH-APP", ">>>>>> [client] '" + pkg + "' req hand shake ...");
        Message message = Message.obtain();
        message.what = Integer.MIN_VALUE;
        Bundle b = new Bundle();
        b.putString("<!!KEY&-INNER>[TARGET_PKG]", pkg);
        b.putString("<!!KEY&-INNER>[TARGET_WIDGET_CLAZZ]", getClass().getName());
        message.setData(b);
        message.replyTo = this.mReplyMessenger;
        try {
            this.mRemoteService.send(message);
        } catch (RemoteException e) {
            e.printStackTrace();
            tryToBindService();
        }
    }

    public int request(int reqId, Bundle data, OnBigDataListener listener) {
        if (data == null) {
            data = new Bundle();
        }
        Message message = Message.obtain();
        data.putString("<!!KEY&-INNER>[UUID]", this.UUID);
        message.what = reqId;
        message.setData(data);
        message.replyTo = this.mReplyMessenger;
        try {
            this.mRemoteService.send(message);
        } catch (Exception e) {
            if (e instanceof TransactionTooLargeException) {
                boolean z;
                String str = "wifi_trans";
                if (("BigDataListener = " + listener) == null) {
                    z = true;
                } else {
                    z = false;
                }
                Log.i(str, String.valueOf(z));
                if (listener != null) {
                    listener.onSendBigData();
                }
            } else {
                e.printStackTrace();
                tryToBindService();
            }
        }
        return 0;
    }

    protected void dataFromFlower(int reqId, Bundle dataRev) {
        Log.i("WH-APP", "'" + this.mContext.getPackageName() + "' received data from host! " + dataRev);
    }

    protected void onFlowerOffline() {
    }

    protected void onFlowerOnline() {
    }

    protected void onFlowerReadyToRolling() {
    }

    protected boolean hintRezygoteWithStopServiceFirstly() {
        return false;
    }
}
