package clc.component.flower;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import clc.openlib.utils.ConcurrentDuplexMap;
import clc.utils.debug.slog.SolidLogger;
import de.greenrobot.event.EventBus;
import java.util.Set;
import java.util.UUID;

public abstract class AbstractFlowerService extends Service {
    private Context mContext = null;
    private Messenger mMessenger = null;
    private ConcurrentDuplexMap<String, Messenger> mUUID2ClientMap = new ConcurrentDuplexMap();
    private ConcurrentDuplexMap<String, ComponentName> mUUID2ComponentNameMap = new ConcurrentDuplexMap();
    private Handler mWorkHandler = null;
    private HandlerThread mWorkerThread;

    class C02541 implements Runnable {
        C02541() {
        }

        public void run() {
            Log.i("WH-SRV", "<><><><><>发出 Service Ready..<><><><><>");
            SolidLogger.getInstance().with("WH-ASSIST", "<><><><><>Host Ready notify Service Ready..<><><><><>");
            String speaker = AbstractFlowerService.this.fetchFlowerAvalibleAction();
            if (TextUtils.isEmpty(speaker)) {
                Log.i("WH-SRV", "<><><><><>发出 Service Ready..<><><><><>");
            } else {
                AbstractFlowerService.this.mContext.sendBroadcast(new Intent(speaker));
            }
        }
    }

    protected abstract void dispatchClientReq(int i, Bundle bundle, String str);

    protected abstract String fetchFlowerAvalibleAction();

    public final void onCreate() {
        Log.i("flower-service", "[DataService] == > onCreate now..==========================");
        super.onCreate();
        this.mContext = getApplicationContext();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public final void onStart(Intent intent, int startId) {
        Log.i("flower-service", "[DataService] == > on start now..==========================");
        if (this.mWorkHandler == null) {
            initHandler();
        }
        if (this.mUUID2ClientMap != null) {
            this.mUUID2ClientMap.clear();
        }
        this.mWorkHandler.postDelayed(new C02541(), 388);
    }

    private synchronized void initHandler() {
        if (this.mWorkerThread == null) {
            this.mWorkerThread = new HandlerThread("springboard-host-service");
            this.mWorkerThread.start();
        }
        this.mWorkHandler = new Handler(this.mWorkerThread.getLooper()) {
            public void handleMessage(Message msg) {
                Log.i("flower-service", "[DataService] ==> receive msg." + msg.what + " , data： " + msg.getData());
                int reqId = msg.what;
                switch (reqId) {
                    case Integer.MIN_VALUE:
                        AbstractFlowerService.this.handshake(reqId, msg.replyTo, msg.getData());
                        return;
                    case -2147483646:
                        AbstractFlowerService.this.handleClientDisconnect(reqId, msg.replyTo, msg.getData());
                        return;
                    default:
                        AbstractFlowerService.this.messenger2Target(reqId, msg.replyTo, msg.getData());
                        return;
                }
            }
        };
    }

    private void handleClientDisconnect(int reqId, Messenger replyTo, Bundle data) {
        String uuid = (String) this.mUUID2ClientMap.removeByValue(replyTo);
        if (TextUtils.isEmpty(uuid)) {
            Log.w("flower-service", "uuid is NULL while disconnect with --> " + replyTo);
        } else {
            onBeeDropping((ComponentName) this.mUUID2ComponentNameMap.remove(uuid));
        }
    }

    public final synchronized void postDataToClient(String clientId, int reqId, Bundle data) {
        Messenger client = (Messenger) this.mUUID2ClientMap.getByKey(clientId);
        if (client == null) {
            Log.i("flower-service", "Client is NULL for req : " + reqId + ", ignore!");
        } else {
            postDataToClient(client, reqId, data);
        }
    }

    final synchronized byte postDataToClient(Messenger client, int reqId, Bundle data) {
        byte b;
        if (this.mWorkHandler == null) {
            initHandler();
        }
        if (client != null) {
            Message msg = Message.obtain();
            msg.what = reqId;
            msg.setData(data);
            msg.replyTo = new Messenger(this.mWorkHandler);
            try {
                client.send(msg);
                b = (byte) 100;
            } catch (RemoteException e) {
                e.printStackTrace();
                Log.i("flower-service", "[DataService] ==> failed to send data : " + data);
            }
        }
        b = (byte) 0;
        return b;
    }

    public final IBinder onBind(Intent intent) {
        if (this.mWorkHandler == null) {
            initHandler();
        }
        if (this.mMessenger == null) {
            this.mMessenger = new Messenger(this.mWorkHandler);
        }
        return this.mMessenger.getBinder();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("flower-service", "[DataService] ==> onStartCommand  :: " + intent + " , from : " + (intent == null ? "self-maybe" : intent.getStringExtra("reqId")));
        return 1;
    }

    public void onDestroy() {
        for (String uuid : this.mUUID2ClientMap.keyMap().keySet()) {
            if (!TextUtils.isEmpty(uuid)) {
                Messenger m = (Messenger) this.mUUID2ClientMap.getByKey(uuid);
                if (m != null) {
                    try {
                        m.getBinder().linkToDeath(null, 0);
                    } catch (RemoteException e) {
                    }
                }
            }
        }
        if (this.mWorkHandler != null) {
            if (this.mWorkHandler.getLooper() != null) {
                this.mWorkHandler.getLooper().quit();
            }
            this.mWorkHandler = null;
        }
        this.mMessenger = null;
        this.mWorkerThread = null;
        if (this.mUUID2ClientMap != null) {
            this.mUUID2ClientMap.clear();
        }
        Log.i("flower-service", "[DataService] ==> 尝试onDestroy后重启...");
        this.mContext.startService(new Intent(this.mContext, AbstractFlowerService.class));
    }

    private void messenger2Target(int reqId, Messenger client, Bundle data) {
        if (data == null) {
            throw new RuntimeException("data not with uuid !");
        }
        Log.i("flower-service", "[SRV] now Flower rev request : " + reqId + " , data : " + data);
        String uuid = data.getString("<!!KEY&-INNER>[UUID]");
        if (TextUtils.isEmpty(uuid)) {
            throw new RuntimeException("UUID should not be null !");
        }
        dispatchClientReq(reqId, data, uuid);
    }

    private void handshake(int reqId, Messenger client, Bundle data) {
        if (isServiceReadyToAccept()) {
            Log.i("flower-service", "[DataService] ==> DS hand shake for : " + data + " , and replyTo:" + client);
            if (data == null) {
                Log.i("flower-service", "[DataService] ==> DS no data for hand shake. refused.");
                return;
            }
            String pkg = data.getString("<!!KEY&-INNER>[TARGET_PKG]");
            String clazz = data.getString("<!!KEY&-INNER>[TARGET_WIDGET_CLAZZ]");
            if (TextUtils.isEmpty(pkg) || TextUtils.isEmpty(clazz)) {
                Log.i("flower-service", "wrong handshake pkg and clazz. " + pkg + " , " + clazz + " , refused");
                return;
            }
            ComponentName cname = new ComponentName(pkg, clazz);
            Log.i("flower-service", "[FlowerService] ==> handshake :::: ===> build tranport : " + cname);
            if (this.mUUID2ClientMap == null) {
                this.mUUID2ClientMap = new ConcurrentDuplexMap();
            }
            String uuid = (String) this.mUUID2ClientMap.getbyValue(client);
            if (TextUtils.isEmpty(uuid)) {
                uuid = UUID.randomUUID().toString();
                this.mUUID2ClientMap.put(uuid, client);
                Log.i("flower-service", "Mapping uuid ::::  {" + uuid + "} = " + "[" + cname + "]");
                Log.i("flower-service", "Link client to death : " + cname);
                linkClientToDeath(client, cname);
            }
            this.mUUID2ComponentNameMap.put(uuid, cname);
            data.putString("<!!KEY&-INNER>[UUID]", uuid);
            postDataToClient(client, reqId, data);
            return;
        }
        String pkg1 = "";
        String clazz1 = "";
        if (data != null) {
            pkg1 = data.getString("<!!KEY&-INNER>[TARGET_PKG]");
            clazz1 = data.getString("<!!KEY&-INNER>[TARGET_WIDGET_CLAZZ]");
        }
        Log.i("flower-service", "[DATA_SERVICE] ==> Host NOT Ready , refused handshake !** ==> " + pkg1 + "/" + clazz1);
    }

    private void linkClientToDeath(final Messenger client, final ComponentName cname) {
        IBinder b = client.getBinder();
        if (b != null) {
            try {
                b.linkToDeath(new DeathRecipient() {
                    public void binderDied() {
                        Log.i("flower-service", "@@@@@@ CLIENT  ' " + cname + "' 挂掉了..clean up...");
                        AbstractFlowerService.this.onBeeDropping(cname);
                        AbstractFlowerService.this.mUUID2ClientMap.removeByValue(client);
                    }
                }, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            onBeeFlying(cname);
        }
    }

    public void onTaskRemoved(Intent rootIntent) {
        Log.i("flower-service", "onTaskRemoved" + rootIntent);
    }

    public boolean onUnbind(Intent intent) {
        cleanUp();
        return super.onUnbind(intent);
    }

    private void cleanUp() {
        if (this.mUUID2ClientMap != null) {
            this.mUUID2ClientMap.clear();
        }
        if (this.mUUID2ComponentNameMap != null) {
            this.mUUID2ComponentNameMap.clear();
        }
    }

    public ComponentName findComponentNameByUUID(String uuid) {
        if (this.mUUID2ComponentNameMap == null) {
            return null;
        }
        return (ComponentName) this.mUUID2ComponentNameMap.getByKey(uuid);
    }

    public String findUUIDByComponentName(ComponentName cname) {
        if (this.mUUID2ComponentNameMap == null) {
            return null;
        }
        return (String) this.mUUID2ComponentNameMap.getbyValue(cname);
    }

    protected Set<String> retriveAllClientIds() {
        return this.mUUID2ClientMap.keyMap().keySet();
    }

    protected boolean isServiceReadyToAccept() {
        return true;
    }

    protected void onBeeDropping(ComponentName cname) {
    }

    protected void onBeeFlying(ComponentName cname) {
    }
}
