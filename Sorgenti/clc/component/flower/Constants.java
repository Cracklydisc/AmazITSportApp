package clc.component.flower;

import android.os.Environment;
import java.io.File;

public class Constants {
    public static final boolean GLOBAL_DEBUG = new File(Environment.getExternalStorageDirectory() + "/springboard.ini").exists();
    public static final boolean LOG_ON = GLOBAL_DEBUG;

    public static class ResultCode {
    }
}
