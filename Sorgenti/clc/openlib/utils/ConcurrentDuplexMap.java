package clc.openlib.utils;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentDuplexMap<K, V> {
    private ConcurrentHashMap<K, Entry> kEntyMap = new ConcurrentHashMap();
    private ConcurrentHashMap<V, Entry> vEntyMap = new ConcurrentHashMap();

    class Entry {
        K f5k;
        V f6v;

        public Entry(K k, V v) {
            this.f5k = k;
            this.f6v = v;
        }

        public K getK() {
            return this.f5k;
        }

        public V getV() {
            return this.f6v;
        }
    }

    public boolean contains(K k) {
        return this.kEntyMap.containsKey(k);
    }

    public boolean containsValue(V v) {
        return this.vEntyMap.containsKey(v);
    }

    public V getByKey(K k) {
        Entry e = (Entry) this.kEntyMap.get(k);
        if (e == null) {
            return null;
        }
        return e.getV();
    }

    public K getbyValue(V v) {
        Entry e = (Entry) this.vEntyMap.get(v);
        if (e == null) {
            return null;
        }
        return e.getK();
    }

    public boolean put(K k, V v) {
        if (k == null || v == null) {
            return false;
        }
        Entry e = new Entry(k, v);
        if (contains(k)) {
            remove(k);
        }
        if (containsValue(v)) {
            removeByValue(v);
        }
        this.kEntyMap.put(k, e);
        this.vEntyMap.put(v, e);
        return true;
    }

    public V remove(K k) {
        Entry e = (Entry) this.kEntyMap.remove(k);
        if (e == null) {
            return null;
        }
        this.vEntyMap.remove(e.getV());
        return e.getV();
    }

    public K removeByValue(V v) {
        Entry e = (Entry) this.vEntyMap.remove(v);
        if (e == null) {
            return null;
        }
        this.kEntyMap.remove(e.getK());
        return e.getK();
    }

    public void clear() {
        this.vEntyMap.clear();
        this.kEntyMap.clear();
    }

    public ConcurrentHashMap<K, Entry> keyMap() {
        return this.kEntyMap;
    }
}
