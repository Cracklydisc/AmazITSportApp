package clc.sliteplugin.flowboard;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.atomic.AtomicBoolean;

@NoProguardClient
public abstract class SpringBoardWidgetClient {
    private boolean mAllowToContinue = true;
    private AtomicBoolean mBinded = new AtomicBoolean(false);
    private ComponentName mBindedServiceComponent;
    private Context mContext;
    private BroadcastReceiver mHostStateListener = new C02641();
    private AtomicBoolean mMutedByServer = new AtomicBoolean(false);
    private ServiceConnection mReadyConnection = null;
    private Messenger mRemoteService;
    private Messenger mReplyMessenger;
    private AtomicBoolean mRespawnOnTheWay = new AtomicBoolean(false);
    private Class<?> mTargetWidgetClassToManage;
    private String mUUID;
    private Handler mWorkerHandler;
    private HandlerThread mWorkerThread;

    class C02641 extends BroadcastReceiver {

        class C02631 implements Runnable {
            C02631() {
            }

            public void run() {
                SpringBoardWidgetClient.this.rezygote();
            }
        }

        C02641() {
        }

        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), "com.huami.watch.launcher.springboard.ON_HOST_AVALIBLE")) {
                if (Constants.LOG_ON) {
                    Log.i("springboard", "[CLIENT] ==> [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] , received host avalible. BindingStateNow : isBinded --> " + SpringBoardWidgetClient.this.mBinded.get());
                }
                if (SpringBoardWidgetClient.this.mBinded.get()) {
                    if (Constants.LOG_ON) {
                        Log.i("springboard", "[CLIENT] ==> [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] 重新握手 ===2！");
                    }
                    SpringBoardWidgetClient.this.handshake();
                    return;
                }
                SpringBoardWidgetClient.this.mWorkerHandler.post(new C02631());
            }
        }
    }

    class C02662 implements ServiceConnection {

        class C02651 implements Runnable {
            C02651() {
            }

            public void run() {
                SpringBoardWidgetClient.this.rezygote();
            }
        }

        C02662() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==> CLIENT onServiceConnected: [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] connect established.");
            }
            SpringBoardWidgetClient.this.mRemoteService = new Messenger(service);
            SpringBoardWidgetClient.this.setupLinkToDeath();
            SpringBoardWidgetClient.this.mBinded.set(true);
            SpringBoardWidgetClient.this.handshake();
        }

        public void onServiceDisconnected(ComponentName name) {
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==> CLIENT onServiceDisconnected : [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] DISCONNECT with host.");
            }
            SpringBoardWidgetClient.this.mBinded.set(false);
            try {
                SpringBoardWidgetClient.this.mContext.unbindService(SpringBoardWidgetClient.this.mReadyConnection);
            } catch (Exception e) {
            }
            if (SpringBoardWidgetClient.this.mAllowToContinue) {
                SpringBoardWidgetClient.this.mWorkerHandler.post(new C02651());
            }
        }
    }

    class C02673 implements DeathRecipient {
        C02673() {
        }

        public void binderDied() {
            SpringBoardWidgetClient.this.mBinded.set(false);
            SpringBoardWidgetClient.this.rezygote();
        }
    }

    class C02695 implements Runnable {
        C02695() {
        }

        public void run() {
            Bundle data = new Bundle();
            data.putString("<!!KEY&-INNER>[TARGET_PKG]", SpringBoardWidgetClient.this.mContext.getPackageName());
            data.putString("<!!KEY&-INNER>[TARGET_WIDGET_CLAZZ]", SpringBoardWidgetClient.this.mTargetWidgetClassToManage.getName());
            SpringBoardWidgetClient.this.sendDataToWidget(Integer.MIN_VALUE, data);
        }
    }

    private synchronized ServiceConnection generateConnection() {
        if (this.mReadyConnection != null) {
            try {
                this.mContext.unbindService(this.mReadyConnection);
            } catch (Exception e) {
            }
        }
        this.mReadyConnection = new C02662();
        return this.mReadyConnection;
    }

    private void setupLinkToDeath() {
        try {
            this.mRemoteService.getBinder().linkToDeath(new C02673(), 0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public SpringBoardWidgetClient(Context context, Class<?> targetWidgetClass) {
        if (context == null) {
            throw new IllegalArgumentException("Need the context not be null.");
        } else if (targetWidgetClass == null) {
            throw new IllegalArgumentException("Need specify the target widget class.");
        } else {
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT][===start==" + targetWidgetClass.getName() + "=============client=======init =======]");
            }
            this.mContext = context.getApplicationContext();
            this.mTargetWidgetClassToManage = targetWidgetClass;
            initHandler();
            initListener();
            tryToBindService();
            this.mMutedByServer.set(false);
        }
    }

    @NoProguardClient
    private void initListener() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watch.launcher.springboard.ON_HOST_AVALIBLE");
        this.mContext.registerReceiver(this.mHostStateListener, filter);
    }

    private void initHandler() {
        if (this.mWorkerThread == null) {
            this.mWorkerThread = new HandlerThread("springboard-client-handler");
            this.mWorkerThread.start();
        }
        if (this.mWorkerHandler == null) {
            this.mWorkerHandler = new Handler(this.mWorkerThread.getLooper()) {
                public void handleMessage(Message msg) {
                    if (Constants.LOG_ON) {
                        Log.i("springboard", "[CLIENT] ==>  handle Message ： " + msg.what);
                    }
                    switch (msg.what) {
                        case Integer.MIN_VALUE:
                            Bundle b = msg.getData();
                            if (b != null) {
                                SpringBoardWidgetClient.this.mAllowToContinue = b.getBoolean("<!!KEY&-INNER>[SHOW]", true);
                                if (SpringBoardWidgetClient.this.mAllowToContinue) {
                                    SpringBoardWidgetClient.this.mUUID = b.getString("<!!KEY&-INNER>[UUID]");
                                    if (Constants.LOG_ON) {
                                        Log.i("springboard", "[CLIENT] ==> HANDSHAKE ---> [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] recieved uuid : {" + SpringBoardWidgetClient.this.mUUID + "}");
                                    }
                                    if (Constants.LOG_ON) {
                                        Log.i("springboard", "[CLIENT] ==> 数据通路创建成功: " + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + " , uuid : " + SpringBoardWidgetClient.this.mUUID);
                                        return;
                                    }
                                    return;
                                }
                                SpringBoardWidgetClient.this.disableByHost();
                                return;
                            } else if (Constants.LOG_ON) {
                                Log.i("springboard", "[CLIENT] ==> CLIENT [" + SpringBoardWidgetClient.this.mTargetWidgetClassToManage + "] uuid is null whild handshake..");
                                return;
                            } else {
                                return;
                            }
                        case -2147483646:
                            if (Constants.LOG_ON) {
                                Log.i("springboard", "[CLIENT] ==>  DISABLE self now ! --> " + SpringBoardWidgetClient.this.mTargetWidgetClassToManage);
                            }
                            SpringBoardWidgetClient.this.disableByHost();
                            return;
                        case -2147483645:
                            if (Constants.LOG_ON) {
                                Log.i("springboard", "[CLIENT] ==>  ENABLE self now ! --> " + SpringBoardWidgetClient.this.mTargetWidgetClassToManage);
                            }
                            SpringBoardWidgetClient.this.enableByHost();
                            return;
                        default:
                            int reqId = msg.what;
                            Bundle data = msg.getData();
                            data.setClassLoader(SpringBoardWidgetClient.this.mContext.getClassLoader());
                            SpringBoardWidgetClient.this.onReceiveDataFromWidget(reqId, data);
                            return;
                    }
                }
            };
        }
    }

    private void tryToBindService() {
        if (Constants.LOG_ON) {
            Log.i("springboard", "[CLIENT] ==> try to rebind host data service , [" + this.mTargetWidgetClassToManage + "]");
        }
        try {
            Intent service = new Intent("com.huami.watch.launcher.springboard.SPRING_BOARD_HOST");
            service.setComponent(new ComponentName("com.huami.watch.launcher", "com.huami.watch.launcher.springboard.DataExchangeService"));
            service.putExtra("<!!KEY&-INNER>[TARGET_PKG]", this.mContext.getPackageName());
            service.putExtra("<!!KEY&-INNER>[TARGET_WIDGET_CLAZZ]", this.mTargetWidgetClassToManage.getName());
            this.mBindedServiceComponent = this.mContext.startService(service);
            if (this.mBindedServiceComponent == null) {
                if (Constants.LOG_ON) {
                    Log.i("springboard", "[CLIENT] ==>" + this.mTargetWidgetClassToManage + "start service 失败");
                }
            } else if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==>" + this.mTargetWidgetClassToManage + "start service 成功");
            }
            boolean res = this.mContext.bindService(service, generateConnection(), 8);
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==> [" + this.mTargetWidgetClassToManage + "] client bind service res================> : " + res);
            }
            if (!res) {
                stopRemoteService();
            }
        } catch (Exception e) {
            if (Constants.LOG_ON) {
                e.printStackTrace();
                Log.i("springboard", "[CLIENT] ==> bind service failed : ", e);
            }
            stopRemoteService();
        }
    }

    private void stopRemoteService() {
        if (this.mBindedServiceComponent != null) {
            Intent i = new Intent();
            i.setComponent(this.mBindedServiceComponent);
            this.mContext.stopService(i);
        }
    }

    protected void onReceiveDataFromWidget(int requestId, Bundle data) {
    }

    public final byte sendDataToWidget(int requestId, Bundle data) {
        if (!this.mAllowToContinue) {
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==>  I am Not Allowed To Send Data To Widget For Comp!");
            }
            return (byte) -2;
        } else if (this.mMutedByServer.get() || (this.mBinded.get() && this.mRemoteService != null && this.mRemoteService.getBinder().pingBinder())) {
            if (this.mReplyMessenger == null) {
                this.mReplyMessenger = new Messenger(this.mWorkerHandler);
            }
            Message message = Message.obtain();
            message.what = requestId;
            if (data == null) {
                data = new Bundle();
            }
            data.putString("<!!KEY&-INNER>[UUID]", this.mUUID);
            message.setData(data);
            message.replyTo = this.mReplyMessenger;
            try {
                this.mRemoteService.send(message);
                return (byte) 100;
            } catch (RemoteException e) {
                if (Constants.LOG_ON) {
                    e.printStackTrace();
                    Log.i("springboard", "[CLIENT] ==> msg send failed for : " + message);
                }
                return (byte) 1;
            } catch (NullPointerException e2) {
                if (Constants.LOG_ON) {
                    e2.printStackTrace();
                    Log.i("springboard", "[CLIENT] ==> service was null");
                }
                return (byte) 1;
            }
        } else {
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] ==>  service maybe died now. try TO rezyGOTE");
            }
            rezygote();
            return (byte) -1;
        }
    }

    private synchronized void rezygote() {
        if (this.mRespawnOnTheWay.get() && Constants.LOG_ON) {
            Log.i("springboard", "[CLIENT] " + this.mTargetWidgetClassToManage.getName() + " 正在Respawn， 忽略此次！");
        } else {
            this.mRespawnOnTheWay.set(true);
            if (Constants.LOG_ON) {
                Log.i("springboard", "[CLIENT] " + this.mTargetWidgetClassToManage.getName() + "==> -------REZYGOTE------- :" + this.mBindedServiceComponent);
            }
            try {
                this.mContext.unbindService(this.mReadyConnection);
            } catch (Exception e) {
                if (Constants.LOG_ON) {
                    e.printStackTrace();
                }
            }
            this.mUUID = null;
            tryToBindService();
            this.mRespawnOnTheWay.set(false);
        }
    }

    private void handshake() {
        this.mWorkerHandler.post(new C02695());
    }

    private void destroySelf() {
        try {
            this.mContext.unregisterReceiver(this.mHostStateListener);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        this.mBinded.set(false);
    }

    private void disableByHost() {
        this.mMutedByServer.set(true);
        try {
            onPreDisableByHost();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            disableSelfInternal();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            onPostDisableByHost();
        } catch (Exception e22) {
            e22.printStackTrace();
        }
    }

    private void enableByHost() {
        this.mMutedByServer.set(false);
        try {
            onPreEnableByHost();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            enableSelfInternal();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            onPostEnableByHost();
        } catch (Exception e22) {
            e22.printStackTrace();
        }
    }

    protected void onPreDisableByHost() {
    }

    protected void onPostDisableByHost() {
    }

    protected void onPreEnableByHost() {
    }

    protected void onPostEnableByHost() {
    }

    private void enableSelfInternal() {
        initHandler();
        initListener();
        tryToBindService();
    }

    private void disableSelfInternal() {
        destroySelf();
    }
}
