package clc.sliteplugin.flowboard;

public enum KeyDef {
    TOP(0),
    MIDDLE(1),
    BOTTOM(2),
    UNKOWN(3);
    
    public int code;

    public enum Stage {
        private static final /* synthetic */ Stage[] ENUM$VALUES = null;
        public static final Stage FIRST = null;
        public static final Stage SECOND = null;
        public int code;

        public static Stage valueOf(String str) {
            return (Stage) Enum.valueOf(Stage.class, str);
        }

        public static Stage[] values() {
            Object obj = ENUM$VALUES;
            int length = obj.length;
            Object obj2 = new Stage[length];
            System.arraycopy(obj, 0, obj2, 0, length);
            return obj2;
        }

        static {
            FIRST = new Stage("FIRST", 0, 0);
            SECOND = new Stage("SECOND", 1, 1);
            ENUM$VALUES = new Stage[]{FIRST, SECOND};
        }

        private Stage(String str, int i, int code) {
            this.code = -1;
            this.code = code;
        }
    }

    private KeyDef(int code) {
        this.code = -1;
        this.code = code;
    }
}
