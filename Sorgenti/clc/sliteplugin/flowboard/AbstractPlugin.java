package clc.sliteplugin.flowboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import clc.sliteplugin.flowboard.KeyDef.Stage;
import java.util.concurrent.atomic.AtomicBoolean;

@NoProguardClient
public abstract class AbstractPlugin implements IPluginable {
    private AtomicBoolean mConnectedToProvider = new AtomicBoolean(false);

    public abstract View getView(Context context);

    public abstract void onBindHost(ISpringBoardHostStub iSpringBoardHostStub);

    @NoProguardHost
    @NoProguardClient
    private final void doBindHost(Object host) {
        if (Constants.LOG_ON) {
            Log.i("springboard", "[AbstractPlugin] ==> Plugin do bind host to stub. " + host.hashCode());
        }
        final Object springHostRef = host;
        onBindHost(new ISpringBoardHostStub() {
            public byte postDataToProvider(IPluginable plugin, int reqId, Bundle data) {
                Byte res = (Byte) ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "postDataToProvider", false, new Class[]{Object.class, Integer.TYPE, Bundle.class}, plugin, Integer.valueOf(reqId), data);
                if (res == null || res.byteValue() == (byte) 0) {
                    return (byte) 0;
                }
                return res.byteValue();
            }

            public void saveWidgetData(IPluginable plugin, String key, Object val) {
                ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "saveWidgetData", false, new Class[]{Object.class, String.class, Object.class}, plugin, key, val);
            }

            public Object getWidgetData(IPluginable plugin, String key, Class<?> type) {
                return ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "getWidgetData", false, new Class[]{Object.class, String.class, Class.class}, plugin, key, type);
            }

            public void runTaskOnUI(IPluginable plugin, Runnable taskToRun) {
                ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "runTaskOnUI", false, new Class[]{Object.class, Runnable.class}, plugin, taskToRun);
            }

            public void runTaskOnWorkThread(IPluginable plugin, Runnable taskToRun) {
                ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "runTaskOnWorkThread", false, new Class[]{Object.class, Runnable.class}, plugin, taskToRun);
            }

            public byte postDataToHost(IPluginable plugin, String action, Bundle data) {
                Byte res = (Byte) ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "postDataToHost", false, new Class[]{Object.class, String.class, Bundle.class}, plugin, action, data);
                if (res == null) {
                    return (byte) 0;
                }
                return res.byteValue();
            }

            public int getHostVerCode() {
                Integer ver = (Integer) ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "getHostVerCode", false, new Class[0], new Object[0]);
                if (ver == null) {
                    return 0;
                }
                return ver.intValue();
            }

            public String getHostVerName() {
                return String.valueOf(ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "getHostVerName", false, new Class[0], new Object[0]));
            }

            public Window getHostWindow() {
                Object window = ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "getHostWindow", false, new Class[0], new Object[0]);
                if (window == null) {
                    return null;
                }
                return (Window) window;
            }

            public Window getContainerWindow(IPluginable plugin) {
                Object window = ReflectUtil.invoke(springHostRef.getClass(), springHostRef, "getContainerWindow", false, new Class[]{Object.class}, plugin);
                if (window == null) {
                    return null;
                }
                return (Window) window;
            }
        });
    }

    public void onStop() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onStop");
    }

    public void onActive(Bundle saveInstanceData) {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onActive");
    }

    public void onInactive(Bundle saveInstanceData) {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onInactive");
    }

    public void onDestroy() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onDestroy");
    }

    public void onPause() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onPause");
    }

    public void onResume() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onResume");
    }

    @NoProguardClient
    private void setConnStateWithProvider(boolean connected) {
        this.mConnectedToProvider.set(connected);
    }

    public boolean isConnected() {
        return this.mConnectedToProvider.get();
    }

    public void onReceiveDataFromProvider(int reqId, Bundle data) {
    }

    public void onProviderOnline() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] ON line!");
    }

    public void onProviderOffline() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] OFF line!");
    }

    public void onPreDisableByHost() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onPreDisableByHost");
    }

    public void onPostDisableByHost() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onPostDisableByHost");
    }

    public void onPreEnableByHost() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onPreEnableByHost");
    }

    public void onPostEnableByHost() {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onPostEnableByHost");
    }

    public Bitmap getWidgetIcon(Context context) {
        return null;
    }

    public String getWidgetTitle(Context context) {
        return "--def--";
    }

    public Intent getWidgetIntent() {
        return null;
    }

    public boolean performOnKeyClick(Object key) {
        return onKeyClick((KeyDef) key);
    }

    @NoProguardClient
    private final boolean onKeyClick(int key) {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onKeyClick " + translateCode2Key(key));
        return onKeyClick(translateCode2Key(key));
    }

    @NoProguardClient
    private final boolean onKeyLongClick(int key, int seg) {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onKeyLongClick " + translateCode2Key(key) + " , in seg: " + seg);
        return onKeyLongClick(translateCode2Key(key), translateCode2Stage(seg));
    }

    @NoProguardClient
    private final boolean onKeyLongClickTimeOut(int key, int seg) {
        Log.i("springboard", "plugin [" + getClass().getName() + "] onKeyLongClickTimeOut " + translateCode2Key(key) + " , in seg: " + seg);
        return onKeyLongClickTimeOut(translateCode2Key(key), translateCode2Stage(seg));
    }

    private KeyDef translateCode2Key(int code) {
        for (KeyDef the : KeyDef.values()) {
            if (code == the.code) {
                return the;
            }
        }
        return null;
    }

    private Stage translateCode2Stage(int code) {
        for (Stage the : Stage.values()) {
            if (code == the.code) {
                return the;
            }
        }
        return null;
    }

    public boolean onKeyClick(KeyDef key) {
        return false;
    }

    public boolean onKeyLongClick(KeyDef key, Stage seg) {
        return false;
    }

    public boolean onKeyLongClickTimeOut(KeyDef key, Stage seg) {
        return false;
    }
}
