package clc.sliteplugin.flowboard;

import android.os.Bundle;
import android.view.Window;

@NoProguardHost
@NoProguardClient
public interface ISpringBoardHost {
    Window getContainerWindow(Object obj);

    int getHostVerCode();

    String getHostVerName();

    Window getHostWindow();

    Object getWidgetData(Object obj, String str, Class<?> cls);

    byte postDataToHost(Object obj, String str, Bundle bundle);

    byte postDataToProvider(Object obj, int i, Bundle bundle);

    void runTaskOnUI(Object obj, Runnable runnable);

    void runTaskOnWorkThread(Object obj, Runnable runnable);

    void saveWidgetData(Object obj, String str, Object obj2);
}
