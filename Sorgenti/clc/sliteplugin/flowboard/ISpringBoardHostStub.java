package clc.sliteplugin.flowboard;

import android.os.Bundle;
import android.view.Window;

@NoProguardHost
@NoProguardClient
public interface ISpringBoardHostStub {
    Window getContainerWindow(IPluginable iPluginable);

    int getHostVerCode();

    String getHostVerName();

    Window getHostWindow();

    Object getWidgetData(IPluginable iPluginable, String str, Class<?> cls);

    byte postDataToHost(IPluginable iPluginable, String str, Bundle bundle);

    byte postDataToProvider(IPluginable iPluginable, int i, Bundle bundle);

    void runTaskOnUI(IPluginable iPluginable, Runnable runnable);

    void runTaskOnWorkThread(IPluginable iPluginable, Runnable runnable);

    void saveWidgetData(IPluginable iPluginable, String str, Object obj);
}
