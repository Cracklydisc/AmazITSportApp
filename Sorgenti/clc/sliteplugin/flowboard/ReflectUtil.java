package clc.sliteplugin.flowboard;

import android.text.TextUtils;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;

public final class ReflectUtil {
    private static final String TAG = ReflectUtil.class.getSimpleName();
    private static ConcurrentHashMap<String, Method> mMethodCacheList = new ConcurrentHashMap();

    public static Object invoke(Class<?> theClazz, Object theInstance, String methodName, boolean checkSuper, Class<?>[] methodParamType, Object... params) {
        if (theClazz == null || theInstance == null || TextUtils.isEmpty(methodName)) {
            Log.i(TAG, "INVOKE FAILED : clazz: " + theClazz + " , method: " + methodName + " instance: " + theInstance);
            return null;
        }
        try {
            String key = buildCacheKey(theClazz, methodName, methodParamType, checkSuper);
            Method method = tryGetFromCache(key);
            Log.i("springboard", "[" + theClazz + "] ==> check key: " + key + " , fed : " + methodName);
            if (method != null) {
                return method.invoke(theInstance, params);
            }
            method = getMethod(theClazz, methodName, methodParamType, checkSuper);
            if (method == null) {
                return null;
            }
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            Object returnObj = method.invoke(theInstance, params);
            mMethodCacheList.put(key, method);
            return returnObj;
        } catch (IllegalAccessException e) {
            return null;
        } catch (IllegalArgumentException e2) {
            return null;
        } catch (InvocationTargetException e3) {
            return null;
        }
    }

    public static Method getMethod(Class<?> clazz, String methodName, Class<?>[] methodParamType, boolean checkSuperClass) {
        Method method = null;
        if (checkSuperClass) {
            Class<?> c = clazz;
            while (c != Object.class) {
                try {
                    method = c.getDeclaredMethod(methodName, methodParamType);
                    if (method != null) {
                        Log.i("springboard", "s KEYEVENT  [" + c + "] 内查找 ==> " + " method [" + methodName + "] 已找到！");
                        break;
                    }
                    c = c.getSuperclass();
                } catch (NoSuchMethodException e) {
                    Log.i("springboard", "s KEYEVENT  [" + c + "] 内查找 ==> " + " method [" + methodName + "] 没找到.");
                }
            }
        } else {
            try {
                method = clazz.getDeclaredMethod(methodName, methodParamType);
            } catch (NoSuchMethodException e2) {
                Log.i("springboard", "KEYEVENT  [ " + clazz + " ] 内查找 ==> " + " method [ " + methodName + " ] 没找到. 本类没有OVERRIDE.");
                try {
                    method = clazz.getMethod(methodName, methodParamType);
                } catch (NoSuchMethodException e3) {
                    Log.i("springboard", "KEYEVENT  [ " + clazz + " ] 内查找 ==> " + " all method [ " + methodName + " ] 没找到. 本类没有OVERRIDE.");
                }
            }
        }
        return method;
    }

    private static Method tryGetFromCache(String key) {
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        return (Method) mMethodCacheList.get(key);
    }

    private static String buildCacheKey(Class<?> theClazz, String methodName, Class<?>[] methodParamType, boolean checkSuper) {
        StringBuilder s = new StringBuilder();
        s.append(theClazz.hashCode());
        s.append(methodName.hashCode());
        for (Class o : methodParamType) {
            s.append(o.hashCode());
        }
        return String.valueOf(s.toString().hashCode());
    }
}
