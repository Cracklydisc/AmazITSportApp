package clc.utils.venus;

import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import clc.utils.venus.DrawableItem.OnVisibilityChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class BaseDrawableGroup extends DrawableItem {
    private static final boolean DEBUG = false;
    private static final String TAG = "L2";
    private ArrayList<DrawableItem> itemHited = new ArrayList(5);
    protected ArrayList<DrawableItem> items = new ArrayList();
    protected DrawableItem lastTouchedItem = null;

    public BaseDrawableGroup(XViewWrapper context) {
        super(context);
    }

    public boolean addItem(DrawableItem item) {
        return addItem(item, getChildCount() == -1 ? 0 : getChildCount());
    }

    public boolean addItem(DrawableItem item, int index) {
        if (item == null) {
            return false;
        }
        if (this.items == null) {
            this.items = new ArrayList();
        }
        if (this.items.contains(item) || index < 0 || index > this.items.size()) {
            return false;
        }
        item.setParent(this);
        item.setPaint(getPaint());
        this.items.add(index, item);
        return true;
    }

    public void removeItem(int index) {
        removeItem(index, true);
    }

    public void removeItem(int index, boolean destory) {
        if (this.items != null && index > -1 && index < this.items.size()) {
            DrawableItem item = (DrawableItem) this.items.remove(index);
            if (destory) {
                item.destory();
            } else {
                item.clean();
            }
            if (this.lastTouchedItem == item) {
                this.lastTouchedItem = null;
            }
        }
    }

    public int removeItem(DrawableItem item) {
        return removeItem(item, true);
    }

    public int removeItem(DrawableItem item, boolean destory) {
        if (!(item == null || this.items == null)) {
            int index = this.items.indexOf(item);
            if (index > -1) {
                synchronized (this.items) {
                    item.setParent(null);
                    this.items.remove(index);
                }
                if (destory) {
                    item.destory();
                } else {
                    item.clean();
                }
                if (this.lastTouchedItem != item) {
                    return index;
                }
                this.lastTouchedItem = null;
                return index;
            }
        }
        return -1;
    }

    public void removeItems(Collection<DrawableItem> itemsToRemove) {
        if (this.items != null) {
            if (itemsToRemove.contains(this.lastTouchedItem)) {
                this.lastTouchedItem = null;
            }
            this.items.removeAll(itemsToRemove);
        }
    }

    public int getChildIndex(DrawableItem item) {
        if (this.items != null) {
            int count = this.items.size();
            for (int i = 0; i < count; i++) {
                if (item == this.items.get(i)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public DrawableItem getChildAt(int index) {
        synchronized (this.items) {
            if (index > -1) {
                try {
                    if (index < this.items.size()) {
                        DrawableItem drawableItem = (DrawableItem) this.items.get(index);
                        return drawableItem;
                    }
                } catch (Exception e) {
                }
            }
        }
        return null;
    }

    public boolean moveChildToIndex(DrawableItem child, int toIndex) {
        int fromIndex = getChildIndex(child);
        if (fromIndex == -1 || fromIndex == toIndex) {
            return false;
        }
        DrawableItem[] itemObjects = (DrawableItem[]) this.items.toArray(new DrawableItem[0]);
        DrawableItem tmp = itemObjects[fromIndex];
        itemObjects[fromIndex] = itemObjects[toIndex];
        itemObjects[toIndex] = tmp;
        this.items = new ArrayList(Arrays.asList(itemObjects));
        return true;
    }

    public void bringChildToFront(DrawableItem child) {
        int fromIndex = getChildIndex(child);
        if (fromIndex != -1 && fromIndex < getChildCount() - 1) {
            this.items.remove(fromIndex);
            this.items.add(child);
        }
    }

    public void bringChildToBack(DrawableItem child) {
        int fromIndex = getChildIndex(child);
        if (fromIndex > 0 && fromIndex < getChildCount()) {
            this.items.remove(fromIndex);
            this.items.add(0, child);
        }
    }

    public boolean exchangeChildren(DrawableItem child1, DrawableItem child2) {
        int fromIndex = getChildIndex(child1);
        int toIndex = getChildIndex(child2);
        DrawableItem itemTmp = child1;
        this.items.set(fromIndex, child2);
        this.items.set(toIndex, itemTmp);
        Object tag = child1.getTag();
        child1.setTag(child2.getTag());
        child2.setTag(tag);
        return true;
    }

    public boolean exchangeChildren(int from, int to) {
        if (from == to || from < 0 || from > getChildCount() - 1 || to < 0 || to > getChildCount() - 1) {
            return false;
        }
        DrawableItem fromTmp = getChildAt(from);
        DrawableItem toTmp = getChildAt(to);
        this.items.set(to, fromTmp);
        this.items.set(from, toTmp);
        return true;
    }

    public void clearAllItems() {
        if (this.items != null) {
            synchronized (this.items) {
                try {
                    for (int i = this.items.size() - 1; i >= 0; i--) {
                        ((DrawableItem) this.items.remove(i)).destory();
                    }
                } catch (Exception e) {
                }
                this.lastTouchedItem = null;
            }
        }
    }

    public void setPaint(Paint paint) {
        super.setPaint(paint);
        if (this.items != null && !this.items.isEmpty()) {
            for (int i = this.items.size() - 1; i >= 0; i--) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (item != null) {
                    item.setPaint(paint);
                }
            }
        }
    }

    public void enableCache() {
        super.enableCache();
        if (this.items != null) {
            for (int i = this.items.size() - 1; i >= 0; i--) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (item != null) {
                    item.disableCache();
                }
            }
        }
    }

    public void setInvertMatrixDirty() {
        super.setInvertMatrixDirty();
        if (this.items != null) {
            for (int i = this.items.size() - 1; i >= 0; i--) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (item != null) {
                    item.setInvertMatrixDirty();
                }
            }
        }
    }

    public void onDraw(IDisplayProcess c) {
        if (this.items != null && !this.isRecycled) {
            for (int i = 0; i < this.items.size(); i++) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (!(item == null || item.isRecycled)) {
                    if (isCacheDirty()) {
                        item.setBgAlphaDirty();
                    }
                    item.draw(c);
                }
            }
        }
    }

    public int getChildCount() {
        if (this.items != null) {
            return this.items.size();
        }
        return -1;
    }

    public void calculateGlobalTouchRect() {
        super.calculateGlobalTouchRect();
        if (this.items != null) {
            for (int i = 0; i < this.items.size(); i++) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (item != null) {
                    item.calculateGlobalTouchRect();
                }
            }
        }
    }

    public boolean onDown(MotionEvent e) {
        boolean superRes = super.onDown(e);
        if (superRes) {
            return true;
        }
        if (this.items == null || this.items.isEmpty()) {
            return superRes;
        }
        this.itemHited.clear();
        this.itemHited = checkHitedItem(e);
        for (int i = this.itemHited.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) this.itemHited.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onDown(e)) {
                if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                    this.lastTouchedItem.onTouchCancel(e);
                }
                this.lastTouchedItem = item;
                return true;
            }
        }
        return superRes;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (super.onFling(e1, e2, velocityX, velocityY)) {
            return true;
        }
        if (!(this.items == null || this.items.isEmpty())) {
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item.onFling(e1, e2, velocityX, velocityY)) {
                    if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                        this.lastTouchedItem.onTouchCancel(e2);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onLongPress(MotionEvent e) {
        if (!(this.items == null || this.items.isEmpty())) {
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item.onLongPress(e)) {
                    if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                        this.lastTouchedItem.onTouchCancel(e);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
        }
        return super.onLongPress(e);
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
        if (super.onScroll(e1, e2, distanceX, distanceY, previousX, previousY)) {
            return true;
        }
        if (!(this.items == null || this.items.isEmpty())) {
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item.onScroll(e1, e2, distanceX, distanceY, previousX, previousY)) {
                    if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                        this.lastTouchedItem.onTouchCancel(e2);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onShowPress(MotionEvent e) {
        if (super.onShowPress(e)) {
            return true;
        }
        if (!(this.items == null || this.items.isEmpty())) {
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item.onShowPress(e)) {
                    if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                        this.lastTouchedItem.onTouchCancel(e);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
        }
        return false;
    }

    public boolean onSingleTapUp(MotionEvent e) {
        boolean superRes = super.onSingleTapUp(e);
        if (superRes) {
            return true;
        }
        if (this.items == null || this.items.isEmpty()) {
            return superRes;
        }
        for (int i = this.itemHited.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) this.itemHited.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onSingleTapUp(e)) {
                if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                    this.lastTouchedItem.onTouchCancel(e);
                }
                this.lastTouchedItem = item;
                return true;
            }
        }
        return superRes;
    }

    public boolean onFingerUp(MotionEvent e) {
        boolean superRes = super.onFingerUp(e);
        if (superRes) {
            return true;
        }
        if (!(this.items == null || this.items.isEmpty())) {
            if (this.itemHited.isEmpty()) {
                if (this.lastTouchedItem != null) {
                    this.lastTouchedItem.onTouchCancel(e);
                }
                this.lastTouchedItem = null;
                return true;
            }
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item.onFingerUp(e)) {
                    if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                        this.lastTouchedItem.onTouchCancel(e);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
        }
        this.itemHited.clear();
        return superRes;
    }

    public void onTouchCancel(MotionEvent e) {
        super.onTouchCancel(e);
        if (this.items != null) {
            synchronized (this.items) {
                for (int i = this.items.size() - 1; i > -1; i--) {
                    DrawableItem item = (DrawableItem) this.items.get(i);
                    if (item != null) {
                        item.onTouchCancel(e);
                    }
                }
            }
        }
        this.lastTouchedItem = null;
    }

    public boolean onFingerCancel(MotionEvent e) {
        boolean res = super.onFingerCancel(e);
        if (res) {
            return true;
        }
        if (this.lastTouchedItem != null) {
            res |= this.lastTouchedItem.onFingerCancel(e);
        }
        if (this.items != null && !this.items.isEmpty()) {
            for (int i = this.itemHited.size() - 1; i > -1; i--) {
                DrawableItem item = (DrawableItem) this.itemHited.get(i);
                if (item != null && item.isVisible() && item.isTouchable() && item != this.lastTouchedItem && item.onFingerCancel(e)) {
                    res = true;
                    break;
                }
            }
        }
        this.lastTouchedItem = null;
        return res;
    }

    protected ArrayList<DrawableItem> checkHitedItem(MotionEvent e) {
        ArrayList<DrawableItem> itemRes = new ArrayList();
        if (e != null) {
            ExchangeManager exchange = getXContext().getExchangee();
            for (int i = 0; i < this.items.size(); i++) {
                DrawableItem item = (DrawableItem) this.items.get(i);
                if (item != null && item.isTouchable() && exchange != null && exchange.checkHited(item, e.getX(), e.getY())) {
                    if (item.isDesiredTouchEventItem()) {
                        itemRes.clear();
                        itemRes.add(item);
                        break;
                    }
                    itemRes.add(item);
                }
            }
        }
        return itemRes;
    }

    public static void echo(String msg) {
        Log.d(TAG, msg);
    }

    public int removeItemWithoutClean(DrawableItem item) {
        if (!(item == null || this.items == null)) {
            int index = this.items.indexOf(item);
            if (index > -1) {
                synchronized (this.items) {
                    this.items.remove(index);
                }
                if (this.lastTouchedItem != item) {
                    return index;
                }
                this.lastTouchedItem = null;
                return index;
            }
        }
        return -1;
    }

    public void removeItemWithoutClean(int index) {
        if (this.items != null && index > -1 && index < this.items.size() && this.lastTouchedItem == ((DrawableItem) this.items.remove(index))) {
            this.lastTouchedItem = null;
        }
    }

    public void setVisibility(boolean visibility) {
        super.setVisibility(visibility);
        for (int i = getChildCount() - 1; i >= 0; i--) {
            if (getChildAt(i).wantKnowVisibleState()) {
                OnVisibilityChangeListener listener = getChildAt(i).getOnVisibilityChangeListener();
                if (listener != null) {
                    listener.onVisibilityChange(this, visibility);
                }
            }
        }
    }

    protected void wantKnowVisibleState(boolean wanted) {
        super.wantKnowVisibleState(wanted);
        if (getParent() != null) {
            getParent().wantKnowVisibleState(wanted);
        }
    }

    public String dumpLayoutInfo() {
        StringBuffer toprint = new StringBuffer(super.dumpLayoutInfo()).append("\n{");
        for (int i = 0; i < this.items.size(); i++) {
            DrawableItem child = (DrawableItem) this.items.get(i);
            if (child != null) {
                toprint.append("\n  ").append(i).append(". <").append(child.getClass().getSimpleName()).append(">").append(": ").append(child.dumpLayoutInfo());
            }
        }
        toprint.append("\n}");
        return toprint.toString();
    }

    public boolean onDoubleTapped(MotionEvent e, DrawableItem tappedItem) {
        boolean superRes = super.onDoubleTapped(e, tappedItem);
        if (superRes) {
            return true;
        }
        if (this.items == null || this.items.isEmpty()) {
            return superRes;
        }
        for (int i = this.itemHited.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) this.itemHited.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onDoubleTapped(e, item)) {
                if (!(this.lastTouchedItem == null || this.lastTouchedItem == item)) {
                    this.lastTouchedItem.onTouchCancel(e);
                }
                this.lastTouchedItem = item;
                return true;
            }
        }
        return superRes;
    }

    public boolean addItemWithoutFilter(DrawableItem item, int index) {
        if (item == null) {
            return false;
        }
        if (this.items == null) {
            this.items = new ArrayList();
        }
        if (index < 0 || index > this.items.size()) {
            return false;
        }
        item.setParent(this);
        item.setPaint(getPaint());
        if (index < this.items.size()) {
            this.items.set(index, item);
        } else if (index == this.items.size()) {
            this.items.add(item);
        }
        return true;
    }

    public void clearAllItemsNotDestroy() {
        if (this.items != null) {
            synchronized (this.items) {
                this.items.clear();
                this.lastTouchedItem = null;
            }
        }
    }
}
