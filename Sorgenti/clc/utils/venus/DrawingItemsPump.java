package clc.utils.venus;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DrawingItemsPump {
    private ConcurrentLinkedQueue<DrawableItem> drawingList;
    private ConcurrentLinkedQueue<DrawableItem> itemsToAdd;
    private ConcurrentLinkedQueue<DrawableItem> itemsToRemove;
    private ConcurrentLinkedQueue<DrawableItem> itemsToUpdate;
    private ConcurrentLinkedQueue<IController> mControllerQueue = new ConcurrentLinkedQueue();
    private State mState = new State();
    private Handler mWorker;
    private HandlerThread mWorkerThread;

    public class State {
        public boolean sDataDirty = false;
        public boolean sDataReady = true;
        public boolean sLockedByDataUpdating = false;
        public boolean sLockedByExternalDrawing = false;
    }

    public DrawingItemsPump() {
        init();
    }

    private void init() {
        makeSureListNotNull();
        this.mWorkerThread = new HandlerThread("DrawingPump-Thread");
        this.mWorkerThread.start();
        this.mWorker = new Handler(this.mWorkerThread.getLooper()) {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    DrawingItemsPump.this.performAdd();
                } else if (msg.what == 2) {
                    DrawingItemsPump.this.performRemove();
                } else if (msg.what == 3) {
                    Looper.myLooper().quit();
                }
            }

            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
            }
        };
    }

    private void makeSureListNotNull() {
        if (this.drawingList == null) {
            this.drawingList = new ConcurrentLinkedQueue();
        }
        if (this.itemsToAdd == null) {
            this.itemsToAdd = new ConcurrentLinkedQueue();
        }
        if (this.itemsToRemove == null) {
            this.itemsToRemove = new ConcurrentLinkedQueue();
        }
        if (this.itemsToUpdate == null) {
            this.itemsToUpdate = new ConcurrentLinkedQueue();
        }
    }

    public synchronized void addDrawingItems(ConcurrentLinkedQueue<DrawableItem> itemsToAdd) {
        makeSureListNotNull();
        if (!(itemsToAdd == null || itemsToAdd.isEmpty())) {
            this.itemsToAdd.addAll(itemsToAdd);
        }
        this.mWorker.removeMessages(1);
        this.mWorker.sendEmptyMessage(1);
    }

    public synchronized void addDrawingItem(DrawableItem itemToAdd) {
        ConcurrentLinkedQueue<DrawableItem> tmpList = new ConcurrentLinkedQueue();
        if (itemToAdd != null) {
            tmpList.add(itemToAdd);
        }
        addDrawingItems(tmpList);
    }

    private void performRemove() {
        if (this.drawingList != null) {
            this.drawingList.removeAll(this.itemsToRemove);
            this.itemsToRemove.clear();
        }
    }

    private void performAdd() {
        this.drawingList.addAll(this.itemsToAdd);
        this.itemsToAdd.clear();
        this.mState.sLockedByDataUpdating = false;
    }

    public ConcurrentLinkedQueue<DrawableItem> offerAndLockDrawing() {
        this.mState.sLockedByExternalDrawing = true;
        return this.drawingList;
    }

    public void unLockDrawing() {
        this.mState.sLockedByExternalDrawing = false;
    }

    public void destroy() {
        this.mWorkerThread.getLooper().quit();
        clear();
    }

    public void clear() {
        if (this.drawingList != null) {
            Iterator it = this.drawingList.iterator();
            while (it.hasNext()) {
                ((DrawableItem) it.next()).destory();
            }
            this.drawingList.clear();
        }
        this.drawingList = null;
        if (this.mControllerQueue != null) {
            this.mControllerQueue.clear();
            this.mControllerQueue = null;
        }
        if (this.mWorker != null) {
            performRemove();
            this.mWorker.getLooper().quit();
            this.mWorker = null;
        }
    }

    public ConcurrentLinkedQueue<IController> getControllers() {
        return this.mControllerQueue;
    }

    public void registerIController(IController controller) {
        if (controller != null && !this.mControllerQueue.contains(controller)) {
            this.mControllerQueue.add(controller);
        }
    }

    public void unregisterIController(IController controller) {
        this.mControllerQueue.remove(controller);
    }
}
