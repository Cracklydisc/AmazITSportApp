package clc.utils.venus;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class NormalDisplayProcess implements IDisplayProcess {
    public static PaintFlagsDrawFilter paintFilter = new PaintFlagsDrawFilter(0, 3);
    private Canvas mCanvas;

    public void beginDisplay(Bitmap bitmap) {
        if (this.mCanvas == null) {
            this.mCanvas = new Canvas();
            this.mCanvas.setBitmap(bitmap);
        }
    }

    public void endDisplay() {
        if (this.mCanvas != null) {
            this.mCanvas = null;
        }
    }

    public boolean clipRect(RectF clip) {
        return this.mCanvas.clipRect(clip);
    }

    public void drawBitmap(Bitmap bitmap, float left, float top, Paint p) {
        if ((bitmap == null || !bitmap.isRecycled()) && bitmap != null) {
            if (p != null) {
                p.setDither(true);
            }
            this.mCanvas.drawBitmap(bitmap, left, top, p);
        }
    }

    public int save() {
        return this.mCanvas.save();
    }

    public void restore() {
        this.mCanvas.restore();
    }

    public void concat(Matrix m) {
        this.mCanvas.concat(m);
    }

    public void translate(float left, float top) {
        this.mCanvas.translate(left, top);
    }

    public void drawRoundRect(RectF rectF, float rx, float ry, Paint mPaint) {
        this.mCanvas.drawRoundRect(rectF, rx, ry, mPaint);
    }

    public void drawRect(RectF rectF, Paint mPaint) {
        this.mCanvas.drawRect(rectF, mPaint);
    }

    public void drawRect(float left, float top, float right, float bottom, Paint paint) {
        this.mCanvas.drawRect(left, top, right, bottom, paint);
    }

    public void drawText(String toDraw, float f, float textBaseY, Paint mPaint) {
        this.mCanvas.drawText(toDraw, f, textBaseY, mPaint);
    }

    public void drawDrawable(Drawable d, RectF Rect) {
        d.setBounds(0, 0, (int) Rect.width(), (int) Rect.height());
        this.mCanvas.translate(Rect.left, Rect.top);
        d.draw(this.mCanvas);
        this.mCanvas.translate(-Rect.left, -Rect.top);
    }

    public boolean beginDisplay(Canvas canvas) {
        this.mCanvas = canvas;
        return this.mCanvas != null;
    }

    public void drawBitmap(Bitmap bitmap, Matrix m, Paint p) {
        if ((bitmap == null || !bitmap.isRecycled()) && bitmap != null) {
            this.mCanvas.drawBitmap(bitmap, m, p);
        }
    }

    public void drawCircle(float cx, float cy, float radius, Paint paint) {
        this.mCanvas.drawCircle(cx, cy, radius, paint);
    }

    public Canvas getCanvas() {
        return this.mCanvas;
    }
}
