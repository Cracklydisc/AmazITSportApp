package clc.utils.venus;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public interface IDisplayProcess {
    boolean clipRect(RectF rectF);

    void concat(Matrix matrix);

    void drawBitmap(Bitmap bitmap, float f, float f2, Paint paint);

    void drawBitmap(Bitmap bitmap, Matrix matrix, Paint paint);

    void drawCircle(float f, float f2, float f3, Paint paint);

    void drawDrawable(Drawable drawable, RectF rectF);

    void drawRect(float f, float f2, float f3, float f4, Paint paint);

    void drawRect(RectF rectF, Paint paint);

    void drawRoundRect(RectF rectF, float f, float f2, Paint paint);

    void drawText(String str, float f, float f2, Paint paint);

    Canvas getCanvas();

    void restore();

    int save();

    void translate(float f, float f2);
}
