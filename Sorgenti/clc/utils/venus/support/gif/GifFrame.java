package clc.utils.venus.support.gif;

import android.graphics.Bitmap;

public class GifFrame {
    public int delay;
    public Bitmap image;

    public GifFrame(Bitmap im, int del) {
        this.image = im;
        this.delay = del;
    }
}
