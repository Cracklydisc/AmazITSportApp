package clc.utils.venus.support.gif;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class GifDecoder extends Thread {
    private int[] act;
    private GifAction action;
    private int bgColor;
    private int bgIndex;
    private byte[] block;
    private int blockSize;
    private int delay;
    private int[] dest;
    private int dispose;
    private int fileType;
    private ArrayList<GifFrame> frameCache;
    private int frameCount;
    private Queue<GifFrame> frameQueue;
    private int[] gct;
    private boolean gctFlag;
    private int gctSize;
    private byte[] gifData;
    public int height;
    private int iCurrentFrame;
    private int icacheParse;
    private int ih;
    private Bitmap image;
    private InputStream in;
    private boolean interlace;
    public boolean isDestroy;
    private boolean isLoop;
    private int iw;
    private int ix;
    private int iy;
    private int lastBgColor;
    private int lastDispose;
    private Bitmap lastImage;
    private int[] lct;
    private boolean lctFlag;
    private int lctSize;
    private final ReentrantLock lock;
    private boolean loopCache;
    private int loopCount;
    private boolean loopParse;
    private int lrh;
    private int lrw;
    private int lrx;
    private int lry;
    private int pixelAspect;
    private byte[] pixelStack;
    private byte[] pixels;
    private short[] prefix;
    private final Condition rCondition;
    private Resources res;
    private int resId;
    private int status;
    private String strFileName;
    private byte[] suffix;
    private int[] tab;
    private int transIndex;
    private boolean transparency;
    private final Condition wCondition;
    public int width;

    public clc.utils.venus.support.gif.GifFrame getCurrentFrame() {
        /* JADX: method processing error */
/*
Error: java.lang.NullPointerException
	at jadx.core.dex.visitors.ssa.SSATransform.placePhi(SSATransform.java:82)
	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:50)
	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r6 = this;
        r4 = 0;
        r5 = 0;
        r2 = 0;
        r3 = r6.loopCache;
        if (r3 == 0) goto L_0x002e;
    L_0x0007:
        r3 = r6.frameQueue;
        r3 = r3.size();
        if (r3 != 0) goto L_0x002e;
    L_0x000f:
        r3 = r6.iCurrentFrame;
        r4 = r6.frameCount;
        if (r3 < r4) goto L_0x001c;
    L_0x0015:
        r6.iCurrentFrame = r5;
        r3 = r6.action;
        r3.loopEnd();
    L_0x001c:
        r3 = r6.frameCache;
        r4 = r6.iCurrentFrame;
        r2 = r3.get(r4);
        r2 = (clc.utils.venus.support.gif.GifFrame) r2;
        r3 = r6.iCurrentFrame;
        r3 = r3 + 1;
        r6.iCurrentFrame = r3;
    L_0x002c:
        r3 = r2;
    L_0x002d:
        return r3;
    L_0x002e:
        r3 = r6.lock;	 Catch:{ Exception -> 0x007f, all -> 0x0087 }
        r3.lockInterruptibly();	 Catch:{ Exception -> 0x007f, all -> 0x0087 }
    L_0x0033:
        r3 = r6.loopCache;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        if (r3 != 0) goto L_0x003f;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
    L_0x0037:
        r3 = r6.frameQueue;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r3.size();	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        if (r3 == 0) goto L_0x006c;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
    L_0x003f:
        r3 = r6.frameQueue;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r3.poll();	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r0 = r3;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r0 = (clc.utils.venus.support.gif.GifFrame) r0;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r2 = r0;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r6.wCondition;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3.signal();	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r6.iCurrentFrame;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r3 + 1;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r6.iCurrentFrame = r3;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = r6.loopParse;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        if (r3 == 0) goto L_0x0066;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
    L_0x0058:
        r3 = r6.iCurrentFrame;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r5 = r6.frameCount;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        if (r3 < r5) goto L_0x0066;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
    L_0x005e:
        r3 = r6.action;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3.loopEnd();	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3 = 0;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r6.iCurrentFrame = r3;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
    L_0x0066:
        r3 = r6.lock;
        r3.unlock();
        goto L_0x002c;
    L_0x006c:
        r3 = r6.rCondition;	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        r3.await();	 Catch:{ Exception -> 0x0072, all -> 0x0087 }
        goto L_0x0033;
    L_0x0072:
        r1 = move-exception;
        r3 = r6.rCondition;	 Catch:{ Exception -> 0x007f, all -> 0x0087 }
        r3.signal();	 Catch:{ Exception -> 0x007f, all -> 0x0087 }
        r3 = r6.lock;
        r3.unlock();
        r3 = r4;
        goto L_0x002d;
    L_0x007f:
        r1 = move-exception;
        r3 = r6.lock;
        r3.unlock();
        r3 = r4;
        goto L_0x002d;
    L_0x0087:
        r3 = move-exception;
        r4 = r6.lock;
        r4.unlock();
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: clc.utils.venus.support.gif.GifDecoder.getCurrentFrame():clc.utils.venus.support.gif.GifFrame");
    }

    private void openInputstream() {
        this.in = new ByteArrayInputStream(this.gifData);
    }

    private void openResourceFile() {
        this.in = this.res.openRawResource(this.resId);
    }

    private void openFile() {
        try {
            this.in = new FileInputStream(this.strFileName);
        } catch (Exception ex) {
            Log.e("open failed", ex.toString());
        }
    }

    public void run() {
        try {
            readStream();
        } catch (Exception ex) {
            Log.e("GifView decode run", ex.toString());
            ex.printStackTrace();
        }
    }

    private void free() {
        if (this.in != null) {
            try {
                this.in.close();
            } catch (Exception e) {
            }
            this.in = null;
        }
        this.gifData = null;
        this.status = 0;
        if (this.frameCache != null) {
            this.frameCache.clear();
            this.frameCache = null;
        }
        if (this.frameQueue != null) {
            this.frameQueue.clear();
            this.frameQueue = null;
        }
    }

    public void destroy() {
        this.isDestroy = true;
        free();
        this.action = null;
    }

    private void setPixels() {
        try {
            int c;
            int i;
            int k;
            if (this.dest == null) {
                this.dest = new int[(this.width * this.height)];
            }
            if (this.lastDispose > 0) {
                if (this.lastDispose == 3) {
                    if (this.frameCount - 2 <= 0) {
                        this.lastImage = null;
                    }
                    this.lastImage = null;
                }
                if (this.lastImage != null) {
                    this.lastImage.getPixels(this.dest, 0, this.width, 0, 0, this.width, this.height);
                    if (this.lastDispose == 2) {
                        c = 0;
                        if (!this.transparency) {
                            c = this.lastBgColor;
                        }
                        for (i = 0; i < this.lrh; i++) {
                            int n1 = ((this.lry + i) * this.width) + this.lrx;
                            int n2 = n1 + this.lrw;
                            for (k = n1; k < n2; k++) {
                                this.dest[k] = c;
                            }
                        }
                    }
                }
            }
            int pass = 1;
            int inc = 8;
            int iline = 0;
            for (i = 0; i < this.ih; i++) {
                int line = i;
                if (this.interlace) {
                    if (iline >= this.ih) {
                        pass++;
                        switch (pass) {
                            case 2:
                                iline = 4;
                                break;
                            case 3:
                                iline = 2;
                                inc = 4;
                                break;
                            case 4:
                                iline = 1;
                                inc = 2;
                                break;
                        }
                    }
                    line = iline;
                    iline += inc;
                }
                line += this.iy;
                if (line < this.height) {
                    k = line * this.width;
                    int dx = k + this.ix;
                    int dlim = dx + this.iw;
                    if (this.width + k < dlim) {
                        dlim = k + this.width;
                    }
                    int sx = i * this.iw;
                    while (dx < dlim) {
                        int sx2 = sx + 1;
                        c = this.act[this.pixels[sx] & 255];
                        if (c != 0) {
                            this.dest[dx] = c;
                        }
                        dx++;
                        sx = sx2;
                    }
                }
            }
            this.image = Bitmap.createBitmap(this.dest, this.width, this.height, Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (StackOverflowError ee) {
            ee.printStackTrace();
        } catch (Exception ex) {
            Log.e("GifView decode setpixel", ex.toString());
        }
    }

    public GifFrame next() {
        return getCurrentFrame();
    }

    private int readStream() {
        init();
        if (this.in != null) {
            readHeader();
            if (!err()) {
                readContents();
                if (!this.loopParse && this.frameCount < 0) {
                    this.status = 1;
                    if (this.action != null) {
                        this.action.parseReturn(4);
                    }
                }
            }
            try {
                if (this.in != null) {
                    this.in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.in = null;
            checkLoop();
        } else {
            this.status = 2;
            if (this.action != null) {
                this.action.parseReturn(4);
            }
        }
        return this.status;
    }

    private void checkLoop() {
        if (this.isLoop && !this.loopCache) {
            if (this.frameCount <= 15) {
                try {
                    this.lock.lockInterruptibly();
                    this.loopCache = true;
                    this.status = -1;
                    if (this.action != null) {
                        this.action.parseReturn(2);
                    }
                    this.rCondition.signal();
                } catch (Exception e) {
                } finally {
                    this.lock.unlock();
                }
            } else if (this.frameCache != null) {
                this.frameCache.clear();
            }
            switch (this.fileType) {
                case 1:
                    openResourceFile();
                    break;
                case 2:
                    openFile();
                    break;
                case 3:
                    openInputstream();
                    break;
            }
            this.loopParse = true;
            if (!this.isDestroy) {
                readStream();
            }
        }
    }

    private void decodeImageData() {
        int code;
        int i;
        int npix = this.iw * this.ih;
        if (this.pixels == null || this.pixels.length < npix) {
            this.pixels = new byte[npix];
        }
        if (this.prefix == null) {
            this.prefix = new short[4096];
        }
        if (this.suffix == null) {
            this.suffix = new byte[4096];
        }
        if (this.pixelStack == null) {
            this.pixelStack = new byte[4097];
        }
        int data_size = read();
        int clear = 1 << data_size;
        int end_of_information = clear + 1;
        int available = clear + 2;
        int old_code = -1;
        int code_size = data_size + 1;
        int code_mask = (1 << code_size) - 1;
        for (code = 0; code < clear; code++) {
            this.prefix[code] = (short) 0;
            this.suffix[code] = (byte) code;
        }
        int bi = 0;
        int first = 0;
        int count = 0;
        int bits = 0;
        int datum = 0;
        int i2 = 0;
        int pi = 0;
        int top = 0;
        while (i2 < npix) {
            if (top == 0) {
                if (bits >= code_size) {
                    code = datum & code_mask;
                    datum >>= code_size;
                    bits -= code_size;
                    if (code > available) {
                        break;
                    } else if (code == end_of_information) {
                        i = top;
                        break;
                    } else if (code == clear) {
                        code_size = data_size + 1;
                        code_mask = (1 << code_size) - 1;
                        available = clear + 2;
                        old_code = -1;
                    } else if (old_code == -1) {
                        i = top + 1;
                        this.pixelStack[top] = this.suffix[code];
                        old_code = code;
                        first = code;
                        top = i;
                    } else {
                        int in_code = code;
                        if (code == available) {
                            i = top + 1;
                            this.pixelStack[top] = (byte) first;
                            code = old_code;
                            top = i;
                        }
                        while (code > clear) {
                            i = top + 1;
                            this.pixelStack[top] = this.suffix[code];
                            code = this.prefix[code];
                            top = i;
                        }
                        first = this.suffix[code] & 255;
                        if (available >= 4096) {
                            i = top;
                            break;
                        }
                        i = top + 1;
                        this.pixelStack[top] = (byte) first;
                        this.prefix[available] = (short) old_code;
                        this.suffix[available] = (byte) first;
                        available++;
                        if ((available & code_mask) == 0 && available < 4096) {
                            code_size++;
                            code_mask += available;
                        }
                        old_code = in_code;
                    }
                } else {
                    if (count == 0) {
                        count = readBlock();
                        if (count <= 0) {
                            i = top;
                            break;
                        }
                        bi = 0;
                    }
                    datum += (this.block[bi] & 255) << bits;
                    bits += 8;
                    bi++;
                    count--;
                }
            } else {
                i = top;
            }
            i--;
            int pi2 = pi + 1;
            this.pixels[pi] = this.pixelStack[i];
            i2++;
            pi = pi2;
            top = i;
        }
        i = top;
        for (i2 = pi; i2 < npix; i2++) {
            this.pixels[i2] = (byte) 0;
        }
    }

    private boolean err() {
        return this.status != 0;
    }

    private void init() {
        this.status = 0;
        if (!this.loopParse) {
            this.frameCount = 0;
        }
        this.gct = null;
        this.lct = null;
        this.icacheParse = 0;
    }

    private int read() {
        int curByte = 0;
        try {
            curByte = this.in.read();
        } catch (Exception e) {
            this.status = 1;
        }
        return curByte;
    }

    private int readBlock() {
        this.blockSize = read();
        int n = 0;
        if (this.blockSize > 0) {
            while (n < this.blockSize) {
                try {
                    int count = this.in.read(this.block, n, this.blockSize - n);
                    if (count == -1) {
                        break;
                    }
                    n += count;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (n < this.blockSize) {
                this.status = 1;
            }
        }
        return n;
    }

    private int[] readColorTable(int ncolors) {
        int nbytes = ncolors * 3;
        byte[] c = new byte[nbytes];
        int n = 0;
        try {
            n = this.in.read(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (n < nbytes) {
            this.status = 1;
        } else {
            int j = 0;
            int i = 0;
            while (i < ncolors) {
                int j2 = j + 1;
                int r = c[j] & 255;
                j = j2 + 1;
                int g = c[j2] & 255;
                j2 = j + 1;
                int i2 = i + 1;
                this.tab[i] = ((-16777216 | (r << 16)) | (g << 8)) | (c[j] & 255);
                j = j2;
                i = i2;
            }
        }
        return this.tab;
    }

    private void readContents() {
        boolean done = false;
        while (!done && !err() && !this.isDestroy) {
            switch (read()) {
                case 0:
                    break;
                case 33:
                    switch (read()) {
                        case 249:
                            readGraphicControlExt();
                            break;
                        case 255:
                            readBlock();
                            String app = "";
                            for (int i = 0; i < 11; i++) {
                                app = new StringBuilder(String.valueOf(app)).append((char) this.block[i]).toString();
                            }
                            if (!app.equals("NETSCAPE2.0")) {
                                skip();
                                break;
                            } else {
                                readNetscapeExt();
                                break;
                            }
                        default:
                            skip();
                            break;
                    }
                case 44:
                    readImage();
                    break;
                case 59:
                    done = true;
                    break;
                default:
                    this.status = 1;
                    break;
            }
        }
    }

    private void readGraphicControlExt() {
        boolean z = true;
        read();
        int packed = read();
        this.dispose = (packed & 28) >> 2;
        if (this.dispose == 0) {
            this.dispose = 1;
        }
        if ((packed & 1) == 0) {
            z = false;
        }
        this.transparency = z;
        this.delay = readShort() * 10;
        if (this.delay == 0) {
            this.delay = 100;
        }
        this.transIndex = read();
        read();
    }

    private void readHeader() {
        String id = "";
        for (int i = 0; i < 6; i++) {
            id = new StringBuilder(String.valueOf(id)).append((char) read()).toString();
        }
        if (id.startsWith("GIF")) {
            readLSD();
            if (this.gctFlag && !err()) {
                this.gct = readColorTable(this.gctSize);
                this.bgColor = this.gct[this.bgIndex];
                return;
            }
            return;
        }
        this.status = 1;
    }

    private void readImage() {
        boolean z;
        this.ix = readShort();
        this.iy = readShort();
        this.iw = readShort();
        this.ih = readShort();
        int packed = read();
        if ((packed & 128) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.lctFlag = z;
        if ((packed & 64) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.interlace = z;
        this.lctSize = 2 << (packed & 7);
        if (this.lctFlag) {
            this.lct = readColorTable(this.lctSize);
            this.act = this.lct;
        } else {
            this.act = this.gct;
            if (this.bgIndex == this.transIndex) {
                this.bgColor = 0;
            }
        }
        int save = 0;
        if (this.transparency && this.act != null && this.act.length > 0 && this.act.length > this.transIndex) {
            save = this.act[this.transIndex];
            this.act[this.transIndex] = 0;
        }
        if (this.act == null) {
            this.status = 1;
        }
        if (!err()) {
            decodeImageData();
            skip();
            if (!err()) {
                if (!this.loopParse) {
                    this.frameCount++;
                }
                setPixels();
                try {
                    this.lock.lockInterruptibly();
                    while (this.frameQueue != null && this.frameQueue.size() >= 15) {
                        try {
                            this.wCondition.await();
                        } catch (InterruptedException e) {
                            this.wCondition.signal();
                            if (this.transparency) {
                                this.act[this.transIndex] = save;
                            }
                            resetFrame();
                        } finally {
                            this.lock.unlock();
                        }
                    }
                    if (this.frameQueue != null) {
                        GifFrame gif = new GifFrame(this.image, this.delay);
                        this.frameQueue.add(gif);
                        if (!this.loopParse) {
                            this.frameCache.add(gif);
                        }
                        this.rCondition.signal();
                        if (!this.loopParse && this.icacheParse >= 0) {
                            this.icacheParse++;
                            if (this.icacheParse >= 15) {
                                this.action.parseReturn(3);
                                this.icacheParse = -1;
                            } else if (this.icacheParse == 1) {
                                this.action.parseReturn(1);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (this.transparency) {
                    this.act[this.transIndex] = save;
                }
                resetFrame();
            }
        }
    }

    private void readLSD() {
        this.width = readShort();
        this.height = readShort();
        int packed = read();
        this.gctFlag = (packed & 128) != 0;
        this.gctSize = 2 << (packed & 7);
        this.bgIndex = read();
        this.pixelAspect = read();
    }

    private void readNetscapeExt() {
        do {
            readBlock();
            if (this.block[0] == (byte) 1) {
                this.loopCount = ((this.block[2] & 255) << 8) | (this.block[1] & 255);
            }
            if (this.blockSize <= 0) {
                return;
            }
        } while (!err());
    }

    private int readShort() {
        return read() | (read() << 8);
    }

    private void resetFrame() {
        this.lastDispose = this.dispose;
        this.lrx = this.ix;
        this.lry = this.iy;
        this.lrw = this.iw;
        this.lrh = this.ih;
        this.lastImage = this.image;
        this.lastBgColor = this.bgColor;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.lct = null;
    }

    private void skip() {
        do {
            readBlock();
            if (this.blockSize <= 0) {
                return;
            }
        } while (!err());
    }

    public boolean isLoop() {
        return this.isLoop;
    }
}
