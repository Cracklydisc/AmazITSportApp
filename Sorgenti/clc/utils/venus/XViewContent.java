package clc.utils.venus;

import android.graphics.Canvas;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

public class XViewContent extends View implements KeyeventConsumer {
    private static final boolean DEBUG = false;
    public static final String TAG = "XContext";
    static Handler handler = new C02931();
    private ExchangeManager exchangee;
    private AtomicBoolean mDetached;
    private NormalDisplayProcess mDisProc;
    private boolean mInited;
    private volatile boolean mNeedInvalidate;
    private XViewWrapper mXContext;
    float oldX;
    private XContentMate renderer;
    private boolean reuse;

    class C02931 extends Handler {
        C02931() {
        }
    }

    public XViewContent(XViewWrapper context) {
        this(context, null);
    }

    public XViewContent(XViewWrapper context, AttributeSet attrs) {
        super(context.getContext(), attrs);
        this.mInited = false;
        this.mDetached = new AtomicBoolean(false);
        this.reuse = false;
        this.oldX = 0.0f;
        this.mNeedInvalidate = false;
        this.mDisProc = new NormalDisplayProcess();
        this.mXContext = context;
        init();
    }

    public void init() {
        if (this.mInited) {
            Log.w(TAG, "WARNING : XContext should not be initionlized secondly!");
            return;
        }
        this.renderer = new XContentMate(getClass().getSimpleName());
        this.exchangee = new ExchangeManager(this.mXContext);
        this.renderer.setExchangee(this.exchangee);
        this.renderer.setRenderTarget(this);
        this.renderer.start();
        this.mInited = true;
    }

    public ExchangeManager getExchangee() {
        return this.exchangee;
    }

    public XContentMate getRenderer() {
        return this.renderer;
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetached.get()) {
            return super.onTouchEvent(event);
        }
        if (this.renderer == null) {
            return true;
        }
        switch (event.getAction() & 255) {
            case 0:
                this.exchangee.onTouchEvent(event);
                return true;
            case 1:
            case 3:
                this.exchangee.onTouchEvent(event);
                return true;
            case 2:
                if (this.mXContext.mCurrenDirection == 10 || this.mXContext.mCurrenDirection == 13 || this.mXContext.mCurrenDirection == 14) {
                    return true;
                }
                this.exchangee.onTouchEvent(event);
                return true;
            default:
                this.exchangee.onTouchEvent(event);
                return true;
        }
    }

    public void onTouchCancel(MotionEvent e) {
        if (this.exchangee != null) {
            this.exchangee.onTouchCancel(e);
        }
    }

    protected void onAttachedToWindow() {
        this.mDetached.set(false);
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        this.mDetached.set(true);
        checkIfReuse();
        invalidate();
        Log.i("venus", "Detached from window !" + this);
        if (!this.reuse) {
            if (this.exchangee != null) {
                this.exchangee.clear();
                this.exchangee = null;
            }
            if (this.renderer != null) {
                this.renderer.clearAndDettach();
            }
            this.mXContext = null;
        }
        super.onDetachedFromWindow();
    }

    void requestInvalidate() {
        if (!this.mNeedInvalidate) {
            this.mNeedInvalidate = true;
            super.invalidate();
        }
    }

    void invalidateInmediately() {
        super.invalidate();
    }

    public void invalidate() {
        requestInvalidate();
    }

    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.mDetached.get() || this.exchangee == null) {
            Log.i("venus", "这个view DiapatchDRAW after dettached " + this + " , reuse is : " + this.reuse);
            return;
        }
        this.exchangee.updateItem(16);
        if (this.mDisProc.beginDisplay(canvas)) {
            this.exchangee.draw(this.mDisProc);
            this.mDisProc.endDisplay();
        }
        this.mNeedInvalidate = false;
    }

    public Handler getExtraHandler() {
        return handler;
    }

    public synchronized void makeReuse(boolean reuse) {
        this.reuse = reuse;
    }

    private void checkIfReuse() {
        ViewGroup v = this.mXContext;
        if (v == null) {
            Log.i("venus", "Check If RESUSE IS NULL --- >" + v);
            return;
        }
        do {
            this.reuse = TextUtils.equals((String) v.getTag(1073741823), "widget-excluded");
            if (!this.reuse) {
                if (!(v.getParent() instanceof ViewGroup)) {
                    break;
                }
                v = (ViewGroup) v.getParent();
            } else {
                Log.i("venus", "Find Reuse Tag --- >" + v);
                break;
            }
        } while (v != null);
        if (!this.reuse) {
            Log.i("venus", "Find NONONONO reuse Tag of --- >" + v);
        }
    }

    private void cleanAllDrawingItem() {
        ViewGroup v = this.mXContext;
    }

    public void injectKeyevent(KeyEvent event) {
        XViewContent content = this.mXContext.getContentView();
        if (content != null && content.getRenderer() != null && content.getExchangee() != null) {
            Iterator it = content.getRenderer().getExchangee().getDrawingPass().offerAndLockDrawing().iterator();
            while (it.hasNext()) {
                DrawableItem item = (DrawableItem) it.next();
                if (item instanceof KeyeventConsumer) {
                    KeyeventConsumer consumer = (KeyeventConsumer) item;
                    if (consumer.canAccept()) {
                        consumer.injectKeyevent(event);
                    }
                }
            }
        }
    }

    public boolean canAccept() {
        return true;
    }
}
