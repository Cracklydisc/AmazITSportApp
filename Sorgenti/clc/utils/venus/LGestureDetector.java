package clc.utils.venus;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public class LGestureDetector {
    private static int LONGPRESS_TIMEOUT = 500;
    static boolean mDebug = false;
    private static int mNormalFlingVelocity;
    public static float mScrollYfactor = 1.0f;
    public static int mTouchSlopSquare;
    public static int mToucheSlop;
    private final int DOUBLE_TAP_TIMEOUT;
    private final int TAP_TIMEOUT;
    private boolean mAlwaysInBiggerTapRegion;
    public boolean mAlwaysInTapRegion;
    private int mBiggerTouchSlopSquare;
    private MotionEvent mCurrentDownEvent;
    private OnDoubleTapListener mDoubleTapListener;
    private int mDoubleTapSlopSquare;
    private final Handler mHandler;
    private boolean mIgnoreMultitouch;
    private boolean mInLongPress;
    private boolean mIsDoubleTapping;
    private boolean mIsLongpressEnabled;
    private float mLastMotionX;
    private float mLastMotionY;
    private final OnGestureListener mListener;
    private int mMaximumFlingVelocity;
    private int mMinimumFlingVelocity;
    private float mPreviousMotionX;
    private float mPreviousMotionY;
    private MotionEvent mPreviousUpEvent;
    private boolean mStillDown;
    boolean mTwoFinger;
    private VelocityTracker mVelocityTracker;
    Context xLauncher;

    public interface OnDoubleTapListener {
        boolean onDoubleTap(MotionEvent motionEvent);

        boolean onDoubleTapEvent(MotionEvent motionEvent);

        boolean onSingleTapConfirmed(MotionEvent motionEvent);
    }

    public interface OnGestureListener {
        boolean onDown(MotionEvent motionEvent);

        boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);

        void onLongPress(MotionEvent motionEvent);

        boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2, float f3, float f4);

        void onShowPress(MotionEvent motionEvent);

        boolean onSingleTapUp(MotionEvent motionEvent);
    }

    private class GestureHandler extends Handler {
        GestureHandler() {
        }

        GestureHandler(Handler handler) {
            super(handler.getLooper());
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    LGestureDetector.this.mListener.onShowPress(LGestureDetector.this.mCurrentDownEvent);
                    return;
                case 2:
                    LGestureDetector.this.dispatchLongPress();
                    return;
                case 3:
                    if (LGestureDetector.this.mDoubleTapListener != null && !LGestureDetector.this.mStillDown) {
                        LGestureDetector.this.mDoubleTapListener.onSingleTapConfirmed(LGestureDetector.this.mCurrentDownEvent);
                        return;
                    }
                    return;
                default:
                    throw new RuntimeException("Unknown message " + msg);
            }
        }
    }

    public static class SimpleOnGestureListener implements OnDoubleTapListener, OnGestureListener {
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
            return false;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onDown(MotionEvent e) {
            return false;
        }

        public boolean onDoubleTap(MotionEvent e) {
            return false;
        }

        public boolean onDoubleTapEvent(MotionEvent e) {
            return false;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            return false;
        }
    }

    public LGestureDetector(Context context, OnGestureListener listener) {
        this(context, listener, null);
    }

    public LGestureDetector(Context context, OnGestureListener listener, Handler handler) {
        this(context, listener, handler, false);
    }

    public LGestureDetector(Context context, OnGestureListener listener, Handler handler, boolean ignoreMultitouch) {
        this.mBiggerTouchSlopSquare = 400;
        this.TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
        this.DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
        this.mTwoFinger = false;
        this.xLauncher = context;
        if (handler != null) {
            this.mHandler = new GestureHandler(handler);
        } else {
            this.mHandler = new GestureHandler();
        }
        this.mListener = listener;
        if (listener instanceof OnDoubleTapListener) {
            setOnDoubleTapListener((OnDoubleTapListener) listener);
        }
        init(context, ignoreMultitouch);
    }

    private void init(Context context, boolean ignoreMultitouch) {
        if (this.mListener == null) {
            throw new NullPointerException("OnGestureListener must not be null");
        }
        int doubleTapSlop;
        this.mIsLongpressEnabled = true;
        this.mIgnoreMultitouch = ignoreMultitouch;
        if (context == null) {
            mToucheSlop = 8;
            doubleTapSlop = ViewConfiguration.getDoubleTapTimeout();
            this.mMinimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
            this.mMaximumFlingVelocity = ViewConfiguration.getMaximumFlingVelocity();
            mNormalFlingVelocity = this.mMinimumFlingVelocity * 6;
        } else {
            ViewConfiguration configuration = ViewConfiguration.get(context);
            mToucheSlop = (int) (8.0f * context.getResources().getDisplayMetrics().density);
            doubleTapSlop = configuration.getScaledDoubleTapSlop();
            this.mMinimumFlingVelocity = configuration.getScaledMinimumFlingVelocity();
            this.mMaximumFlingVelocity = configuration.getScaledMaximumFlingVelocity();
            mNormalFlingVelocity = (int) (300.0f * context.getResources().getDisplayMetrics().density);
        }
        mTouchSlopSquare = mToucheSlop * mToucheSlop;
        mScrollYfactor = (float) Math.tan(0.9599310885968813d);
        Log.i("debug", "mScrollYfactor = " + mScrollYfactor);
        this.mDoubleTapSlopSquare = doubleTapSlop * doubleTapSlop;
    }

    public void setOnDoubleTapListener(OnDoubleTapListener onDoubleTapListener) {
        this.mDoubleTapListener = onDoubleTapListener;
    }

    public void setIsLongpressEnabled(boolean isLongpressEnabled) {
        this.mIsLongpressEnabled = isLongpressEnabled;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (mDebug) {
            Log.i("debug", "LGestureDetector onTouchEvent = " + ev.getAction());
        }
        int action = ev.getAction();
        float y = ev.getY();
        float x = ev.getX();
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(ev);
        boolean handled = false;
        switch (action & 255) {
            case 0:
                if (this.mDoubleTapListener != null) {
                    boolean hadTapMessage = this.mHandler.hasMessages(3);
                    if (hadTapMessage) {
                        this.mHandler.removeMessages(3);
                    }
                    if (!(this.mCurrentDownEvent == null || this.mPreviousUpEvent == null || !hadTapMessage)) {
                        if (isConsideredDoubleTap(this.mCurrentDownEvent, this.mPreviousUpEvent, ev)) {
                            this.mHandler.removeMessages(2);
                            this.mIsDoubleTapping = true;
                            handled = (false | this.mDoubleTapListener.onDoubleTap(this.mCurrentDownEvent)) | this.mDoubleTapListener.onDoubleTapEvent(ev);
                        }
                    }
                    this.mHandler.sendEmptyMessageDelayed(3, (long) this.DOUBLE_TAP_TIMEOUT);
                }
                this.mTwoFinger = false;
                this.mLastMotionX = x;
                this.mPreviousMotionX = x;
                this.mLastMotionY = y;
                this.mPreviousMotionY = y;
                if (this.mCurrentDownEvent != null) {
                    this.mCurrentDownEvent.recycle();
                }
                this.mCurrentDownEvent = MotionEvent.obtain(ev);
                this.mAlwaysInTapRegion = true;
                this.mAlwaysInBiggerTapRegion = true;
                this.mStillDown = true;
                this.mInLongPress = false;
                if (this.mIsLongpressEnabled && !this.mIsDoubleTapping) {
                    this.mHandler.removeMessages(2);
                    this.mHandler.sendEmptyMessageDelayed(2, (long) LONGPRESS_TIMEOUT);
                }
                this.mHandler.sendEmptyMessageDelayed(1, (long) this.TAP_TIMEOUT);
                handled |= this.mListener.onDown(ev);
                break;
            case 1:
                this.mStillDown = false;
                MotionEvent currentUpEvent = MotionEvent.obtain(ev);
                checkInTapRegionDistance(x, y, ev);
                if (this.mIsDoubleTapping) {
                    handled = false | this.mDoubleTapListener.onDoubleTapEvent(ev);
                } else if (this.mInLongPress) {
                    this.mHandler.removeMessages(3);
                    this.mInLongPress = false;
                } else if (this.mAlwaysInTapRegion) {
                    if (mDebug) {
                        Log.i("debug", "LGestureDetector onSingleTapUp");
                    }
                    handled = this.mListener.onSingleTapUp(ev);
                } else {
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumFlingVelocity);
                    float velocityY = velocityTracker.getYVelocity();
                    float velocityX = velocityTracker.getXVelocity();
                    if (Math.abs(velocityY) > ((float) this.mMinimumFlingVelocity) || Math.abs(velocityX) > ((float) this.mMinimumFlingVelocity)) {
                        handled = this.mListener.onFling(this.mCurrentDownEvent, ev, velocityX, velocityY);
                    }
                }
                if (this.mPreviousUpEvent != null) {
                    this.mPreviousUpEvent.recycle();
                }
                this.mPreviousUpEvent = currentUpEvent;
                this.mVelocityTracker.recycle();
                this.mVelocityTracker = null;
                this.mIsDoubleTapping = false;
                this.mHandler.removeMessages(1);
                this.mHandler.removeMessages(2);
                break;
            case 2:
                if (this.mInLongPress || ((this.mIgnoreMultitouch && ev.getPointerCount() > 1) || this.mTwoFinger)) {
                    if (mDebug) {
                        Log.i("debug", "mInLongPress = " + this.mInLongPress);
                        break;
                    }
                }
                float scrollX = this.mLastMotionX - x;
                float scrollY = this.mLastMotionY - y;
                if (this.mIsDoubleTapping) {
                    handled = false | this.mDoubleTapListener.onDoubleTapEvent(ev);
                } else if (this.mAlwaysInTapRegion) {
                    int deltaX = (int) (x - this.mCurrentDownEvent.getX());
                    int deltaY = (int) (y - this.mCurrentDownEvent.getY());
                    int distance = (deltaX * deltaX) + (deltaY * deltaY);
                    if (distance > mTouchSlopSquare) {
                        if (mDebug) {
                            Log.i("debug", "deltaX = " + deltaX + "deltaY = " + deltaY + "ev.getPointerCount() = " + ev.getPointerCount());
                        }
                        if (ev.getPointerCount() == 2) {
                            if (mDebug) {
                                Log.i("debug", "two finger");
                            }
                            cancel();
                            this.mTwoFinger = true;
                            return false;
                        } else if (ev.getPointerCount() < 2 || ((float) Math.abs(deltaY)) <= mScrollYfactor * ((float) Math.abs(deltaX))) {
                            handled = this.mListener.onScroll(this.mCurrentDownEvent, ev, scrollX, scrollY, this.mPreviousMotionX - x, this.mPreviousMotionY - y);
                            this.mLastMotionX = x;
                            this.mLastMotionY = y;
                            this.mAlwaysInTapRegion = false;
                            this.mHandler.removeMessages(3);
                            this.mHandler.removeMessages(1);
                            this.mHandler.removeMessages(2);
                        } else {
                            if (mDebug) {
                                Log.i("debug", "two move");
                            }
                            cancel();
                            this.mTwoFinger = true;
                            return false;
                        }
                    }
                    if (distance > this.mBiggerTouchSlopSquare) {
                        this.mAlwaysInBiggerTapRegion = false;
                    }
                } else if (Math.abs(scrollX) >= 0.0f || Math.abs(scrollY) >= 0.0f) {
                    handled = this.mListener.onScroll(this.mCurrentDownEvent, ev, scrollX, scrollY, this.mPreviousMotionX - x, this.mPreviousMotionY - y);
                    this.mLastMotionX = x;
                    this.mLastMotionY = y;
                }
                this.mPreviousMotionX = x;
                this.mPreviousMotionY = y;
                break;
                break;
            case 3:
                cancel();
                break;
            case 5:
                if (this.mIgnoreMultitouch) {
                    cancel();
                    break;
                }
                break;
            case 6:
                if (this.mIgnoreMultitouch && ev.getPointerCount() == 2) {
                    int index;
                    if (((65280 & action) >> 8) == 0) {
                        index = 1;
                    } else {
                        index = 0;
                    }
                    this.mLastMotionX = ev.getX(index);
                    this.mLastMotionY = ev.getY(index);
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = VelocityTracker.obtain();
                    break;
                }
        }
        return handled;
    }

    private boolean checkInTapRegionDistance(float x, float y, MotionEvent ev) {
        if (!this.mAlwaysInTapRegion) {
            return false;
        }
        int deltaX = (int) (x - this.mCurrentDownEvent.getX());
        int deltaY = (int) (y - this.mCurrentDownEvent.getY());
        if ((deltaX * deltaX) + (deltaY * deltaY) <= mTouchSlopSquare) {
            return false;
        }
        if (mDebug) {
            Log.i("debug", "deltaX = " + deltaX + "deltaY = " + deltaY + "ev.getPointerCount() = " + ev.getPointerCount());
        }
        this.mAlwaysInTapRegion = false;
        return true;
    }

    public void cancel() {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
        this.mIsDoubleTapping = false;
        this.mStillDown = false;
        this.mAlwaysInTapRegion = false;
        this.mAlwaysInBiggerTapRegion = false;
        if (this.mInLongPress) {
            this.mInLongPress = false;
        }
    }

    private boolean isConsideredDoubleTap(MotionEvent firstDown, MotionEvent firstUp, MotionEvent secondDown) {
        if (!this.mAlwaysInBiggerTapRegion || secondDown.getEventTime() - firstUp.getEventTime() > ((long) this.DOUBLE_TAP_TIMEOUT)) {
            return false;
        }
        int deltaX = ((int) firstDown.getX()) - ((int) secondDown.getX());
        int deltaY = ((int) firstDown.getY()) - ((int) secondDown.getY());
        if ((deltaX * deltaX) + (deltaY * deltaY) < this.mDoubleTapSlopSquare) {
            return true;
        }
        return false;
    }

    private void dispatchLongPress() {
        this.mHandler.removeMessages(3);
        this.mInLongPress = true;
        this.mListener.onLongPress(this.mCurrentDownEvent);
    }

    public void cancelLongPress() {
        this.mInLongPress = false;
        this.mHandler.removeMessages(2);
    }
}
