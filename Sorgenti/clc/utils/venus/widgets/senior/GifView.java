package clc.utils.venus.widgets.senior;

import android.graphics.RectF;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.support.gif.GifAction;
import clc.utils.venus.support.gif.GifDecoder;
import clc.utils.venus.support.gif.GifFrame;

public class GifView extends DrawableItem implements GifAction {
    private GifFrame mCurrFrame;
    private GifDecoder mDecoder;
    private long mFrameFraction;
    private long mNextDelay;
    private OnFinishParsingListener mParsingFinishListener;
    private boolean mReadyToShow;

    public interface OnFinishParsingListener {
        void onParseReady(float f, float f2);
    }

    public void parseReturn(int iResult) {
        resize(new RectF(0.0f, 0.0f, (float) this.mDecoder.width, (float) this.mDecoder.height));
        this.mReadyToShow = true;
        if (this.mParsingFinishListener != null) {
            this.mParsingFinishListener.onParseReady((float) this.mDecoder.width, (float) this.mDecoder.height);
        }
    }

    public void loopEnd() {
    }

    public void draw(IDisplayProcess c) {
        c.translate(this.localRect.right, this.localRect.top);
        long time = System.currentTimeMillis();
        if (this.mReadyToShow) {
            if (this.mFrameFraction < this.mNextDelay) {
                this.mFrameFraction += System.currentTimeMillis() - time;
            } else {
                this.mCurrFrame = this.mDecoder.next();
                this.mFrameFraction = 0;
            }
            c.drawBitmap(this.mCurrFrame.image, getMatrix(), getPaint());
            this.mNextDelay = (long) this.mCurrFrame.delay;
            if (this.mDecoder.isLoop()) {
                this.mContext.getRenderer().invalidateImmediately();
            }
            this.mFrameFraction += (System.currentTimeMillis() - time) + 16;
        }
    }

    public void clean() {
        this.mCurrFrame = null;
        this.mDecoder.destroy();
        super.clean();
    }
}
