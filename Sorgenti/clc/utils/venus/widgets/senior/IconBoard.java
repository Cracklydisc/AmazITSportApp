package clc.utils.venus.widgets.senior;

import android.graphics.Matrix;
import clc.utils.stuff.Validator;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.IconDrawable;

public class IconBoard extends BaseDrawableGroup {
    private boolean isUp;
    private float mCurrProgress;
    private IconDrawable mIcon;
    private IconDrawable mIconBoard;
    private float mScaleFactor;

    public IconBoard(XViewWrapper context, IconDrawable icon) {
        this(context, icon, null);
    }

    public IconBoard(XViewWrapper context, IconDrawable icon, IconDrawable iconBoard) {
        super(context);
        this.mCurrProgress = 0.0f;
        this.mIcon = null;
        this.mIconBoard = null;
        this.mScaleFactor = 1.0f;
        this.isUp = false;
        Validator.notNull(icon, "Icon should not be null!");
        this.mIcon = icon;
        this.mIconBoard = iconBoard;
        resize(icon.localRect);
    }

    public void setProgress(boolean isCurrPos, float progress) {
        this.isUp = progress > 0.0f;
        this.mCurrProgress = Math.abs(progress);
        if (this.mCurrProgress > 0.4f) {
            setAlpha(0.64f);
        } else {
            setAlpha((-0.90000004f * this.mCurrProgress) + 1.0f);
        }
    }

    public void draw(IDisplayProcess c) {
        c.save();
        updateFinalAlpha();
        c.translate(this.localRect.left, this.localRect.top);
        Matrix m = getMatrix();
        float scale = 1.0f - (this.mCurrProgress * this.mScaleFactor);
        m.setScale(scale, scale, this.mIcon.getWidth(), this.isUp ? 0.0f : getHeight());
        m.postTranslate(20.0f * this.mCurrProgress, 0.0f);
        c.getCanvas().concat(m);
        if (this.mIconBoard != null) {
            this.mIconBoard.setAlpha(getFinalAlpha());
            this.mIconBoard.draw(c);
        }
        this.mIcon.setAlpha(getFinalAlpha());
        this.mIcon.draw(c);
        c.restore();
    }

    public void setScaleFactor(float scaleFactor) {
        this.mScaleFactor = scaleFactor;
    }
}
