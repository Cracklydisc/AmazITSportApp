package clc.utils.venus.widgets.senior;

import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import clc.utils.venus.C0288R;

public class WearableDialogView extends RelativeLayout {
    private boolean enterAnim;
    private Button mCancelBtn;
    private Button mConfirmBtn;
    private TextView mTopHint;

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mTopHint = (TextView) findViewById(C0288R.id.dlg_hint);
        this.mConfirmBtn = (Button) findViewById(C0288R.id.dlg_left_btn);
        this.mCancelBtn = (Button) findViewById(C0288R.id.dlg_right_btn);
    }

    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == 0 && this.enterAnim) {
            startAnimation(AnimationUtils.loadAnimation(getContext(), C0288R.anim.alpha_in));
        }
    }
}
