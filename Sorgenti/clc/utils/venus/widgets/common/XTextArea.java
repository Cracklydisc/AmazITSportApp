package clc.utils.venus.widgets.common;

import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.XViewWrapper;

public class XTextArea extends DrawableItem {
    private static /* synthetic */ int[] $SWITCH_TABLE$android$graphics$Paint$Align;
    protected static final Xfermode mXfermode = new PorterDuffXfermode(Mode.DST_OUT);
    protected float edge = 0.0f;
    private float fadingEdgeLength = 0.0f;
    private FontMetrics fm = new FontMetrics();
    private final RectF mClipRect;
    protected final Paint mFadePaint = new Paint();
    protected final Paint mPaint = new Paint();
    private String mText = "";
    protected boolean needCut = false;
    protected Shader shader;
    float textBaseY = 0.0f;
    protected int textSetColor;
    private float textWidth = 0.0f;
    float textX = 0.0f;
    protected String toDraw = "";
    protected TruncateAt where = TruncateAt.END;

    static /* synthetic */ int[] $SWITCH_TABLE$android$graphics$Paint$Align() {
        int[] iArr = $SWITCH_TABLE$android$graphics$Paint$Align;
        if (iArr == null) {
            iArr = new int[Align.values().length];
            try {
                iArr[Align.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Align.LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Align.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$android$graphics$Paint$Align = iArr;
        }
        return iArr;
    }

    public XTextArea(XViewWrapper context, String text, RectF rect) {
        super(context);
        if (rect == null) {
            rect = new RectF();
        }
        this.mClipRect = new RectF();
        if (rect.width() > 0.0f && rect.height() > 0.0f) {
            resize(rect);
        }
        this.mPaint.setAntiAlias(true);
        if (text == null) {
            text = "";
        }
        setText(text);
    }

    public boolean isEmpty() {
        return this.mText == null || this.mText.equals("");
    }

    public void setTextSize(float size) {
        if (size > 0.0f) {
            this.mPaint.setTextSize(size);
            this.mPaint.getFontMetrics(this.fm);
            float bottom = ((this.localRect.top + ((float) this.mPaddingTop)) + ((float) this.mPaddingBottom)) + ((float) ((int) Math.ceil((double) (this.fm.bottom - this.fm.top))));
            if (bottom > this.localRect.bottom) {
                this.localRect.bottom = bottom;
                this.mClipRect.set(0.0f, 0.0f, getWidth(), getHeight());
            }
            updateText();
            invalidate();
        }
    }

    public void setBackgroundDrawable(Drawable d) {
        super.setBackgroundDrawable(d);
        if (d != null) {
            int width = d.getIntrinsicWidth();
            int height = d.getIntrinsicHeight();
            if (((float) width) > this.localRect.right - this.localRect.left) {
                this.localRect.right = this.localRect.left + ((float) width);
                this.mClipRect.set(0.0f, 0.0f, getWidth(), getHeight());
            }
            if (((float) height) > this.localRect.bottom - this.localRect.top) {
                this.localRect.bottom = this.localRect.top + ((float) height);
                this.mClipRect.set(0.0f, 0.0f, getWidth(), getHeight());
            }
        }
    }

    public float getTextWidth() {
        return this.textWidth;
    }

    public void setEllipsize(TruncateAt where) {
        if (where == TruncateAt.MARQUEE) {
            this.where = where;
            this.fadingEdgeLength = 10.0f;
            this.shader = new LinearGradient(this.edge, 0.0f, this.edge + this.fadingEdgeLength, 0.0f, 0, -16777216, TileMode.CLAMP);
            return;
        }
        this.where = where;
        this.fadingEdgeLength = 0.0f;
        this.shader = null;
    }

    public void setTextColor(int color) {
        this.textSetColor = color;
        invalidate();
    }

    public void setText(String text) {
        if (text == null) {
            text = "";
        }
        this.mText = text;
        updateText();
        invalidate();
    }

    public void resize(RectF rect) {
        super.resize(rect);
        this.mClipRect.set(0.0f, 0.0f, getWidth(), getHeight());
        updateText();
        invalidate();
    }

    private void updateText() {
        this.toDraw = this.mText;
        this.toDraw.trim();
        int maxWidth = (int) ((this.localRect.width() - ((float) this.mPaddingLeft)) - ((float) this.mPaddingRight));
        this.textWidth = this.mPaint.measureText(this.toDraw);
        if (this.textWidth > ((float) maxWidth)) {
            float max;
            if (this.where == TruncateAt.MARQUEE) {
                max = (float) maxWidth;
            } else {
                max = ((float) maxWidth) - this.mPaint.measureText("...");
            }
            float[] widths = new float[this.toDraw.length()];
            int count = this.mPaint.getTextWidths(this.toDraw, widths);
            this.textWidth = 0.0f;
            for (int i = 0; i < count; i++) {
                this.textWidth += widths[i];
                if (this.textWidth >= max) {
                    this.textWidth -= widths[i];
                    count = i;
                    break;
                }
            }
            if (this.where != TruncateAt.MARQUEE) {
                this.toDraw = new StringBuffer(this.toDraw.substring(0, count)).append("...").toString();
                this.textWidth = this.mPaint.measureText(this.toDraw);
            } else if (count < this.toDraw.length()) {
                count++;
                String subStr = this.toDraw.substring(0, count);
                float subWidth = this.mPaint.measureText(subStr);
                while (subWidth == 0.0f && count < this.toDraw.length()) {
                    count++;
                    subStr = this.toDraw.substring(0, count);
                }
                this.toDraw = subStr;
                this.needCut = true;
            } else {
                this.needCut = false;
            }
        } else {
            this.needCut = false;
        }
        this.textBaseY = (this.localRect.height() - ((this.localRect.height() - (this.fm.bottom - this.fm.top)) / 2.0f)) - this.fm.bottom;
        switch ($SWITCH_TABLE$android$graphics$Paint$Align()[this.mPaint.getTextAlign().ordinal()]) {
            case 1:
                if (this.needCut) {
                    this.textX = ((this.localRect.width() - this.textWidth) + this.mPaint.measureText(this.toDraw)) / 2.0f;
                    this.edge = (this.localRect.width() + this.textWidth) / 2.0f;
                    this.shader = new LinearGradient(this.edge, 0.0f, this.edge + this.fadingEdgeLength, 0.0f, 0, -16777216, TileMode.CLAMP);
                    return;
                }
                this.textX = this.localRect.width() / 2.0f;
                return;
            case 2:
                this.textX = (float) this.mPaddingLeft;
                this.edge = ((float) this.mPaddingLeft) + this.textWidth;
                return;
            case 3:
                this.textX = getWidth() - ((float) this.mPaddingRight);
                this.edge = this.textX;
                return;
            default:
                return;
        }
    }

    public void onDraw(IDisplayProcess c) {
        if (!isEmpty()) {
            c.clipRect(this.mClipRect);
            this.mPaint.setColor(this.textSetColor);
            this.mPaint.setAlpha((int) (((float) (this.textSetColor >>> 24)) * getFinalAlpha()));
            c.drawText(this.toDraw, this.textX, this.textBaseY, this.mPaint);
            this.mPaint.setShadowLayer(0.0f, 0.0f, 0.0f, (((int) (255.0f * getFinalAlpha())) << 24) & -1);
            if (this.where == TruncateAt.MARQUEE && this.needCut) {
                this.mFadePaint.setShader(this.shader);
                this.mFadePaint.setXfermode(mXfermode);
                this.mFadePaint.setStyle(Style.FILL);
                c.drawRect(new RectF(this.edge, 0.0f, getWidth(), getHeight()), this.mFadePaint);
            }
        }
    }

    public void setAlpha(float alpha) {
        super.setAlpha(alpha);
        if (alpha >= 0.0f && alpha <= 1.0f) {
            this.mPaint.setAlpha((int) (255.0f * alpha));
        }
    }

    public void updateFinalAlpha() {
        super.updateFinalAlpha();
        this.mPaint.setAlpha((int) (getFinalAlpha() * 255.0f));
    }
}
