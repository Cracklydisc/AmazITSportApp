package clc.utils.venus.widgets.common;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.XViewWrapper;

public class IconDrawable extends DrawableItem {
    private int alignMode = 4369;
    private float drawX = 0.0f;
    private float drawY = 0.0f;
    private Matrix extraMatrix = new Matrix();
    private Matrix extraMatrixCopy = new Matrix();
    private boolean fillMode = true;
    Bitmap iconBitmap;

    public IconDrawable(XViewWrapper context, int iconResId) {
        super(context);
        Drawable d = context.getResources().getDrawable(iconResId);
        if (d instanceof BitmapDrawable) {
            doInit(((BitmapDrawable) d).getBitmap());
            return;
        }
        throw new IllegalArgumentException("Not supported picture format.");
    }

    public IconDrawable(XViewWrapper context, Bitmap icon) {
        super(context);
        doInit(icon);
    }

    private void doInit(Bitmap icon) {
        this.iconBitmap = icon;
        float iconWidth = 0.0f;
        float iconHeight = 0.0f;
        if (this.iconBitmap != null) {
            iconWidth = (float) this.iconBitmap.getWidth();
            iconHeight = (float) this.iconBitmap.getHeight();
        }
        this.localRect = new RectF(0.0f, 0.0f, iconWidth, iconHeight);
        this.mHaloCenterX = iconWidth * 0.5f;
        this.mHaloCenterY = iconHeight * 0.5f;
        setHaloRadius(Math.max(this.mHaloCenterX, this.mHaloCenterY));
        invalidate();
    }

    private void updateAlign() {
        if (this.iconBitmap != null && !this.iconBitmap.isRecycled()) {
            this.drawX = 0.0f;
            this.drawY = 0.0f;
            if (this.fillMode) {
                this.extraMatrix.setScale(getWidth() / ((float) this.iconBitmap.getWidth()), getHeight() / ((float) this.iconBitmap.getHeight()));
                this.mHaloCenterX = getWidth() * 0.5f;
                this.mHaloCenterY = getHeight() * 0.5f;
                setHaloRadius(Math.max(this.mHaloCenterX, this.mHaloCenterY));
            } else {
                switch (this.alignMode & 4352) {
                    case 256:
                        this.drawX = this.localRect.width() - ((float) this.iconBitmap.getWidth());
                        break;
                    case 4096:
                        this.drawX = 0.0f;
                        break;
                    case 4352:
                        this.drawX = (this.localRect.width() - ((float) this.iconBitmap.getWidth())) * 0.5f;
                        break;
                }
                switch (this.alignMode & 17) {
                    case 1:
                        this.drawY = this.localRect.height() - ((float) this.iconBitmap.getHeight());
                        break;
                    case 16:
                        this.drawY = 0.0f;
                        break;
                    case 17:
                        this.drawY = (this.localRect.height() - ((float) this.iconBitmap.getHeight())) * 0.5f;
                        break;
                }
                this.extraMatrix.setTranslate(this.drawX, this.drawY);
                this.mHaloCenterX = this.drawX + (((float) this.iconBitmap.getWidth()) * 0.5f);
                this.mHaloCenterY = this.drawY + (((float) this.iconBitmap.getHeight()) * 0.5f);
                setHaloRadius(((float) Math.max(this.iconBitmap.getWidth(), this.iconBitmap.getHeight())) * 0.5f);
                setPivot(this.mHaloCenterX, this.mHaloCenterY);
            }
            this.extraMatrixCopy.set(this.extraMatrix);
        }
    }

    public void onDraw(IDisplayProcess c) {
        if (this.extraMatrix.isIdentity()) {
            c.drawBitmap(this.iconBitmap, 0.0f, 0.0f, getPaint());
        } else {
            c.drawBitmap(this.iconBitmap, this.extraMatrix, getPaint());
        }
    }

    public void resize(RectF rect) {
        super.resize(rect);
        updateAlign();
        invalidate();
    }

    public boolean onFingerUp(MotionEvent e) {
        super.onFingerUp(e);
        return true;
    }

    public boolean onShowPress(MotionEvent e) {
        super.onShowPress(e);
        return true;
    }

    public boolean onFingerCancel(MotionEvent e) {
        super.onFingerCancel(e);
        return true;
    }

    public boolean onSingleTapUp(MotionEvent e) {
        super.onSingleTapUp(e);
        return true;
    }

    public boolean onDown(MotionEvent e) {
        super.onDown(e);
        return true;
    }
}
