package clc.utils.venus.widgets.common;

import android.graphics.Paint.Style;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.IController;
import clc.utils.venus.IDisplayProcess;

public class BlackBoard extends DrawableItem {
    private float targetAlpha;
    private float targetTempAlpha;

    private class AlphaController implements IController {
        protected boolean alphaAnimActivate;
        final /* synthetic */ BlackBoard this$0;

        protected void stopAlphaAnim() {
            this.alphaAnimActivate = false;
            this.this$0.unregisterIController(this);
        }

        public void update(long timeDelta) {
            if (this.alphaAnimActivate) {
                float delta = this.this$0.targetTempAlpha - this.this$0.getAlpha();
                if (Math.abs(delta) < 1.0E-4f) {
                    this.this$0.setSuperAlpha(this.this$0.targetTempAlpha);
                    stopAlphaAnim();
                } else {
                    this.this$0.setSuperAlpha(this.this$0.getAlpha() + (0.135f * delta));
                }
                this.this$0.invalidate();
            }
        }
    }

    public void setAlpha(float alpha) {
        this.targetAlpha = alpha;
        if (getParent() != null) {
            super.setAlpha(this.targetAlpha);
            invalidate();
        }
    }

    private void setSuperAlpha(float alpha) {
        super.setAlpha(alpha);
    }

    public void onDraw(IDisplayProcess c) {
        if (getAlpha() > 0.0f) {
            getPaint().setColor(-16777216);
            getPaint().setStyle(Style.FILL);
            updateFinalAlpha();
            c.drawRect(0.0f, 0.0f, getWidth(), getHeight(), getPaint());
        }
    }
}
