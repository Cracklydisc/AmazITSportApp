package clc.utils.venus.widgets.common;

import android.content.res.Resources;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Xfermode;
import android.text.TextUtils.TruncateAt;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.XViewWrapper;

public class XTextView extends XTextArea {
    private int bubbleColor;
    private boolean enableBackground = false;
    private float mCornerRadius;
    private Typeface mTextFace;
    private int textview_background_offset;

    public XTextView(XViewWrapper context, String text, RectF rect, Align alignType) {
        super(context, text, rect);
        Resources res = context.getResources();
        setPadding(1, 2, 1, 2);
        RectF rectF = this.localRect;
        rectF.bottom += (float) (this.mPaddingTop + this.mPaddingBottom);
        this.mPaint.setTextAlign(alignType);
        this.mCornerRadius = 8.0f * res.getDisplayMetrics().density;
        setTextColor(-65536);
        this.bubbleColor = -7829368;
        this.textview_background_offset = 20;
        setEllipsize(TruncateAt.MARQUEE);
        enableCache();
    }

    public void onDraw(IDisplayProcess c) {
        if (!isEmpty()) {
            if (this.mTextFace != null) {
                this.mPaint.setTypeface(this.mTextFace);
            }
            this.mPaint.setColor(-1);
            this.mPaint.setAlpha((int) (((float) 255) * getFinalAlpha()));
            this.mPaint.setShadowLayer(1.0f, 0.0f, 0.0f, -16777216 & (((int) (((float) 221) * getFinalAlpha())) << 24));
            super.onDraw(c);
            float textWidth = getTextWidth() + ((float) this.textview_background_offset);
            if (this.enableBackground) {
                this.mPaint.setColor(this.bubbleColor);
                this.mPaint.setAlpha((int) (((float) (this.bubbleColor >>> 24)) * getFinalAlpha()));
                this.mPaint.setStyle(Style.FILL);
                Xfermode temp = this.mPaint.getXfermode();
                this.mPaint.setXfermode(new PorterDuffXfermode(Mode.DST_OVER));
                c.drawRoundRect(new RectF(((this.localRect.width() / 2.0f) - (textWidth / 2.0f)) - ((float) this.mPaddingLeft), 0.0f, ((this.localRect.width() / 2.0f) + (textWidth / 2.0f)) + ((float) this.mPaddingRight), this.localRect.height()), this.mCornerRadius, this.mCornerRadius, this.mPaint);
                this.mPaint.setXfermode(temp);
            }
        }
    }
}
