package clc.utils.venus.widgets.common;

import android.content.Intent;
import android.util.Log;
import android.view.MotionEvent;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.XViewWrapper;

public class IconView extends BaseDrawableGroup {
    Intent intent;
    XViewWrapper mContext;

    class C02951 extends XTextView {
        public boolean onLongPress(MotionEvent e) {
            Log.i("clc", "on long xxx pressed: " + this);
            return super.onLongPress(e);
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
            setAlpha(1.0f);
            return super.onScroll(e1, e2, distanceX, distanceY, previousX, previousY);
        }
    }

    public boolean onFingerCancel(MotionEvent e) {
        return super.onFingerCancel(e);
    }

    public boolean onDown(MotionEvent e) {
        if (!super.onDown(e)) {
            invalidate();
        }
        return true;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
        if (getAlpha() < 1.0f) {
            resetPressedState();
        }
        return false;
    }

    public void resetPressedState() {
        setAlpha(1.0f);
        invalidate();
    }

    public boolean onFingerUp(MotionEvent e) {
        return super.onFingerUp(e);
    }

    public void clean() {
        this.intent = null;
        this.mContext = null;
        super.clean();
    }

    public boolean onLongPress(MotionEvent e) {
        resetPressedState();
        return true;
    }
}
