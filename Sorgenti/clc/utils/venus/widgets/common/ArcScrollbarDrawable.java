package clc.utils.venus.widgets.common;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class ArcScrollbarDrawable extends Drawable {
    private static RectF cachedOvalTrack = new RectF();
    private int mAlpha = 0;
    private float mBlockOverscrollAngle = 0.0f;
    private float mBlockSweepAngle = 9.0f;
    private Rect mBounds;
    private float mMaxOverscrollAngle = (this.mBlockSweepAngle * 0.5f);
    private float mProcOffset = 0.0f;
    private int mThumbAlpha = ((int) (0.64f * ((float) this.mAlpha)));
    private int mThumbColor = -1;
    private int mTrackAlpha = ((int) (0.16f * ((float) this.mAlpha)));
    private int mTrackColor = -1;
    private float mTrackMaxAngle = (45.0f - this.mBlockSweepAngle);
    Paint paint;
    private float startAngle;

    public ArcScrollbarDrawable(Rect bounds) {
        this.mBounds = bounds;
        init();
    }

    public void updateProc(float proc) {
        this.mProcOffset = proc;
    }

    public void init() {
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeCap(Cap.ROUND);
        this.paint.setStrokeWidth(4.0f);
        updateParams();
    }

    public void updateBounds(Rect bounds) {
        this.mBounds = bounds;
        updateParams();
    }

    private void updateParams() {
        cachedOvalTrack.left = 0.0f;
        cachedOvalTrack.top = 0.0f;
        cachedOvalTrack.right = (float) this.mBounds.width();
        cachedOvalTrack.bottom = (float) this.mBounds.height();
    }

    private void drawVerticalTack(Canvas canvas, Rect bounds) {
        this.paint.setColor(this.mTrackColor);
        this.paint.setAlpha(this.mTrackAlpha);
        canvas.drawArc(cachedOvalTrack, 337.5f, 45.0f, false, this.paint);
    }

    private void drawVerticalThumb(Canvas canvas, Rect bounds, float offset) {
        this.paint.setColor(this.mThumbColor);
        this.paint.setAlpha(this.mThumbAlpha);
        this.startAngle = (this.mTrackMaxAngle * offset) + 337.5f;
        if (this.startAngle < 337.5f) {
            this.mBlockOverscrollAngle = Math.min(this.mMaxOverscrollAngle, 337.5f - this.startAngle);
            this.startAngle = 337.5f;
        } else if (this.startAngle > this.mTrackMaxAngle + 337.5f) {
            this.startAngle = Math.min((this.mTrackMaxAngle + 337.5f) + this.mMaxOverscrollAngle, this.startAngle);
            this.mBlockOverscrollAngle = (this.startAngle - 337.5f) - this.mTrackMaxAngle;
        } else {
            this.mBlockOverscrollAngle = 0.0f;
        }
        canvas.drawArc(cachedOvalTrack, this.startAngle, this.mBlockSweepAngle - this.mBlockOverscrollAngle, false, this.paint);
    }

    public void draw(Canvas canvas) {
        if (this.mAlpha > 0 && this.mTrackMaxAngle > 0.0f) {
            drawVerticalTack(canvas, this.mBounds);
            drawVerticalThumb(canvas, this.mBounds, this.mProcOffset);
        }
    }

    public void setAlpha(int alpha) {
        if (this.mAlpha != alpha) {
            this.mAlpha = alpha;
            this.mTrackAlpha = (int) (0.16f * ((float) this.mAlpha));
            this.mThumbAlpha = (int) (0.64f * ((float) this.mAlpha));
        }
    }

    public int getAlpha() {
        return this.mAlpha;
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public int getOpacity() {
        return 0;
    }

    public void setBlockLength(float ratio) {
        this.mBlockSweepAngle = 45.0f * Math.min(Math.max(ratio, 0.2f), 1.0f);
        this.mMaxOverscrollAngle = this.mBlockSweepAngle * 0.5f;
        this.mTrackMaxAngle = 45.0f - this.mBlockSweepAngle;
    }
}
