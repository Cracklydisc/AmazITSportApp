package clc.utils.venus.widgets.common;

import android.graphics.Bitmap;
import android.graphics.RectF;
import clc.utils.venus.BaseDrawableGroup;

public class IconTextView extends BaseDrawableGroup {
    private Bitmap mIconBitmap;

    public void resize(RectF rect) {
        super.resize(rect);
    }

    public void clean() {
        this.mIconBitmap = null;
        clearAllItems();
        super.clean();
    }
}
