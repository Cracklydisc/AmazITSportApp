package clc.utils.venus.widgets.common;

import android.graphics.Paint;
import android.graphics.RectF;

public class GradientDrawable {
    private final Paint mFillPaint = new Paint(1);
    int mHeight = -1;
    private final RectF mRect = new RectF();
    int mWidth = -1;
}
