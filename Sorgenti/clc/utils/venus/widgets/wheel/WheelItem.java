package clc.utils.venus.widgets.wheel;

import android.graphics.RectF;
import android.view.MotionEvent;
import clc.utils.venus.DrawableItem;

public class WheelItem extends WheelViewItemsLayout {
    private int mCellHeight;
    private int mColNum;

    public void resize(RectF rect) {
        super.resize(rect);
        calculateGlobalTouchRect();
    }

    public boolean addItem(DrawableItem item, int index) {
        if (item == null || item.getParent() != null) {
            return false;
        }
        int cellCapacityW = (int) (this.localRect.width() / ((float) this.mColNum));
        item.setRelativeX((float) ((int) (((float) (((getChildCount() % this.mColNum) * cellCapacityW) + (cellCapacityW / 2))) - (item.getWidth() / 2.0f))));
        item.setRelativeY((this.localRect.height() / 2.0f) - (item.getHeight() / 2.0f));
        offsetRelative(this.localRect.left, this.localRect.top);
        calculateGlobalTouchRect();
        return super.addItem(item, index);
    }

    public boolean addItem(DrawableItem item) {
        return addItem(item, getChildCount());
    }

    public float getItemHeight() {
        return (float) this.mCellHeight;
    }

    public boolean onLongPress(MotionEvent e) {
        return super.onLongPress(e);
    }
}
