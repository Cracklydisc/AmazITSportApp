package clc.utils.venus.widgets.wheel;

public interface OnWheelScrollListener {
    void onScrolling(float f, float f2);

    void onScrollingFinished(WheelView wheelView);

    void onScrollingStarted(WheelView wheelView);
}
