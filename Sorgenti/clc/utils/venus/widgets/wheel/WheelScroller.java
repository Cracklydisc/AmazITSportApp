package clc.utils.venus.widgets.wheel;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class WheelScroller {
    private final int MESSAGE_JUSTIFY = 1;
    private final int MESSAGE_SCROLL = 0;
    private Handler animationHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            WheelScroller.this.scroller.computeScrollOffset();
            int currY = WheelScroller.this.scroller.getCurrY();
            float delta = WheelScroller.this.lastScrollY - ((float) currY);
            WheelScroller.this.lastScrollY = (float) currY;
            if (delta != 0.0f) {
                WheelScroller.this.listener.onScroll(delta);
            }
            if (Math.abs(currY - WheelScroller.this.scroller.getFinalY()) < 1) {
                currY = WheelScroller.this.scroller.getFinalY();
                WheelScroller.this.scroller.forceFinished(true);
            }
            if (!WheelScroller.this.scroller.isFinished()) {
                WheelScroller.this.animationHandler.sendEmptyMessage(msg.what);
            } else if (msg.what == 0) {
                WheelScroller.this.justify();
            } else {
                WheelScroller.this.finishScrolling();
            }
        }
    };
    private Context context;
    private GestureDetector gestureDetector;
    private SimpleOnGestureListener gestureListener = new C02961();
    private boolean isScrollingPerformed;
    private float lastScrollY;
    private float lastTouchedY;
    private ScrollingListener listener;
    private Scroller scroller;

    class C02961 extends SimpleOnGestureListener {
        C02961() {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            WheelScroller.this.lastScrollY = 0.0f;
            float targetY = (float) WheelScroller.this.listener.onFling(velocityY, WheelScroller.this.lastScrollY);
            if (velocityY < 0.0f) {
                WheelScroller.this.scroller.fling(0, (int) WheelScroller.this.lastScrollY, 0, (int) (-velocityY), 0, 0, -2147483647, (int) targetY);
            } else if (velocityY > 0.0f) {
                WheelScroller.this.scroller.fling(0, (int) WheelScroller.this.lastScrollY, 0, (int) (-velocityY), 0, 0, (int) targetY, Integer.MAX_VALUE);
            }
            WheelScroller.this.setNextMessage(0);
            return true;
        }
    }

    public interface ScrollingListener {
        void onFinished();

        int onFling(float f, float f2);

        void onJustify();

        void onScroll(float f);

        void onStarted();
    }

    public WheelScroller(Context context, ScrollingListener listener) {
        this.gestureDetector = new GestureDetector(context, this.gestureListener);
        this.gestureDetector.setIsLongpressEnabled(false);
        this.scroller = new Scroller(context);
        this.listener = listener;
        this.context = context;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.scroller.forceFinished(true);
        this.scroller = new Scroller(this.context, interpolator);
    }

    public void scroll(float distance, int time) {
        this.scroller.forceFinished(true);
        this.lastScrollY = 0.0f;
        this.scroller.startScroll(0, 0, 0, (int) distance, time != 0 ? time : 228);
        setNextMessage(0);
        startScrolling();
    }

    public void stopScrolling() {
        this.scroller.forceFinished(true);
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean ret = true;
        switch (event.getAction()) {
            case 0:
                this.lastTouchedY = event.getY();
                this.scroller.forceFinished(true);
                clearMessages();
                ret = false;
                break;
            case 2:
                float distanceY = event.getY() - this.lastTouchedY;
                if (distanceY != 0.0f) {
                    startScrolling();
                    this.listener.onScroll(distanceY);
                    this.lastTouchedY = event.getY();
                    break;
                }
                break;
        }
        if (!this.gestureDetector.onTouchEvent(event) && event.getAction() == 1) {
            justify();
        }
        return ret;
    }

    private void setNextMessage(int message) {
        clearMessages();
        this.animationHandler.sendEmptyMessage(message);
    }

    private void clearMessages() {
        this.animationHandler.removeMessages(0);
        this.animationHandler.removeMessages(1);
    }

    public void justify() {
        this.listener.onJustify();
        setNextMessage(1);
    }

    private void startScrolling() {
        if (!this.isScrollingPerformed) {
            this.isScrollingPerformed = true;
            this.listener.onStarted();
        }
    }

    void finishScrolling() {
        if (this.isScrollingPerformed) {
            this.listener.onFinished();
            this.isScrollingPerformed = false;
        }
    }
}
