package clc.utils.venus.widgets.wheel;

import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.XViewWrapper;

public abstract class WheelViewItemsLayout extends BaseDrawableGroup {
    private boolean canFocus = false;
    private int clickCount = -1;
    private int focusIndex = -1;
    private int lastIndexOfFocus = this.focusIndex;
    private int pointerOfFocusInnerChild = 0;

    public abstract float getItemHeight();

    public WheelViewItemsLayout(XViewWrapper context) {
        super(context);
    }

    public void setOverscroll(boolean top, float input) {
    }

    protected void startFadeInAnimation() {
    }

    public void setFocusIndex(int lastFocus, int focusIndex) {
        this.focusIndex = focusIndex;
        invalidate();
    }

    public int getFocusIndex() {
        return this.focusIndex;
    }

    protected void onLoseFocusedState(DrawableItem itemNeedShowFocusState) {
        clearAllFocusForItem((WheelItem) itemNeedShowFocusState);
    }

    public void clearAllFocusForItem(WheelItem item) {
        if (item != null) {
            for (int i = item.getChildCount() - 1; i >= 0; i--) {
                DrawableItem child = item.getChildAt(i);
                if (child != null) {
                    child.setBackgroundDrawable(null);
                }
            }
        }
    }

    public boolean canSupportFocus() {
        return this.canFocus;
    }

    public void setSupportFocus(boolean canFocus) {
        this.canFocus = canFocus;
    }

    public void resetFocus() {
        for (int i = getChildCount() - 1; i >= 0; i--) {
            WheelItem currFocusWheelItem = (WheelItem) getChildAt(i);
            if (currFocusWheelItem != null) {
                clearAllFocusForItem(currFocusWheelItem);
            }
        }
        this.clickCount = -1;
        this.pointerOfFocusInnerChild = 0;
    }
}
