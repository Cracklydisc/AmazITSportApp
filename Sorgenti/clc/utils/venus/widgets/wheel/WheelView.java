package clc.utils.venus.widgets.wheel;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Interpolator;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.C0288R;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.DrawableItem.OnClickListener;
import clc.utils.venus.IDisplayProcess;
import clc.utils.venus.XViewWrapper;
import clc.utils.venus.widgets.common.ArcScrollbarDrawable;
import clc.utils.venus.widgets.wheel.WheelScroller.ScrollingListener;
import com.huami.watch.keyevent_lib.HMKeyDef.HMKeyEvent;
import com.huami.watch.keyevent_lib.KeyEventHelpers.EventCallBack;
import com.huami.watch.keyevent_lib.KeyeventConsumer;
import com.huami.watch.keyevent_lib.KeyeventProcessor;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WheelView extends BaseDrawableGroup implements KeyeventConsumer {
    public static final boolean DEBUG = false;
    private static final int DEF_VISIBLE_ITEMS = 3;
    private static final int DELAY_HIDE_SCROLLBAR = 200;
    private static final int DELAY_HIDE_SCROLLBAR_PUB = 1000;
    private static final int DURATION_OVERSCROLL = 229;
    private static final int DURATION_SCROLLBAR_HIDE = 200;
    private static final int DURATION_SCROLLBAR_SHOW = 100;
    private static final QuadInterpolator INTER_OUT = new QuadInterpolator((byte) 1);
    private static final float LIMIT_LINE_SLOP = 0.0f;
    private static final int MAX_OVERSCROLL_OFFSET = 75;
    private static final int MSG_HIDE_SCROLLBAR = 101;
    private static final int OVERSCROLL_BOTTOM = 2;
    private static final int OVERSCROLL_NONE = -1;
    private static final int OVERSCROLL_TOP = 1;
    public static final String TAG = "WheelView";
    private static final String TAG_OVER = "overscroll";
    private boolean acceptKeyEvent = true;
    private boolean canSupportFocus = false;
    private List<OnWheelChangedListener> changingListeners = new LinkedList();
    private int currentItem = 0;
    private DataSetObserver dataObserver = new C03012();
    private int firstItem;
    private DrawableItem focusIndicator;
    private boolean hasShow = false;
    private int indexOfFocus = -1;
    boolean isCyclic = false;
    private boolean isScrollingPerformed;
    private WheelViewItemsLayout itemsLayout;
    private EventCallBack keyEventCallback = new C03034();
    private KeyeventProcessor keyEventManager;
    private int lastIndexOfFocus = this.indexOfFocus;
    private float mAllChildrenHeight = LIMIT_LINE_SLOP;
    private ArcScrollbarDrawable mArcScrollBar;
    private int mBaseLine = 1;
    private int mCurrTargetItemIndex;
    private float mFlingVelocityY;
    private Handler mHandler = new C03023();
    private int mMarginBottom = 0;
    private int mMarginTop;
    private ValueAnimator mOverscrollAnim;
    private float mOverscrollOffset = LIMIT_LINE_SLOP;
    private int mOverscrollType = -1;
    private ValueAnimator mScrollbarAnim;
    private boolean mScrollbarEnabled = true;
    private boolean mShowAnchorAnim = false;
    private int mTopAnchor = 1;
    private XViewWrapper mWrapper;
    private float scrollOffsetAbs;
    private HashMap<Integer, ScrollProcMonitor> scrollProcMonitorsMap = new HashMap();
    private int scrollbarOffsetX = 0;
    private int scrollbarOffsetY = 0;
    private WheelScroller scroller;
    ScrollingListener scrollingListener = new C03001();
    private List<OnWheelScrollListener> scrollingListeners = new LinkedList();
    private float scrollingOffset;
    private WheelViewAdapter viewAdapter;
    private int visibleItems = DEF_VISIBLE_ITEMS;

    class C03001 implements ScrollingListener {
        C03001() {
        }

        public void onStarted() {
            WheelView.this.isScrollingPerformed = true;
            WheelView.this.notifyScrollingListenersAboutStart();
        }

        public void onScroll(float distance) {
            WheelView.this.doScroll(distance);
        }

        public void onFinished() {
            if (WheelView.this.isScrollingPerformed) {
                WheelView.this.notifyScrollingListenersAboutEnd();
                WheelView.this.isScrollingPerformed = false;
            }
            WheelView.this.scrollingOffset = WheelView.LIMIT_LINE_SLOP;
            if (WheelView.this.getOverscrollType() > 0) {
                WheelView.this.startOverscrollAnim();
            }
            if (WheelView.this.canSupportFocus) {
                WheelView.this.itemsLayout.setFocusIndex(WheelView.this.lastIndexOfFocus, WheelView.this.currentItem);
                WheelView.this.itemsLayout.invalidate();
            }
            WheelView.this.invalidate(false);
            Log.i("clcc", "finished at : " + WheelView.this.currentItem);
        }

        public void onJustify() {
            if (Math.abs(WheelView.this.scrollingOffset) > 1.0f) {
                if (WheelView.this.currentItem == 0) {
                    float offsetToAnchor = (float) (WheelView.this.getItemHeight() * (WheelView.this.mTopAnchor - WheelView.this.currentItem));
                    WheelView wheelView = WheelView.this;
                    wheelView.scrollingOffset = wheelView.scrollingOffset + offsetToAnchor;
                    WheelView.this.currentItem = WheelView.this.mTopAnchor;
                }
                WheelView.this.scroller.scroll(WheelView.this.scrollingOffset, 0);
            }
        }

        public int onFling(float vY, float lastY) {
            WheelView.this.mFlingVelocityY = vY;
            int delta = (int) (WheelView.this.scrollingOffset % ((float) WheelView.this.getItemHeight()));
            if (WheelView.this.mFlingVelocityY < WheelView.LIMIT_LINE_SLOP) {
                WheelView.this.mCurrTargetItemIndex = WheelView.this.currentItem + Math.max(1, (int) ((-WheelView.this.mFlingVelocityY) / 800.0f));
            } else {
                WheelView.this.mCurrTargetItemIndex = Math.min(-1, (int) ((-WheelView.this.mFlingVelocityY) / 800.0f));
            }
            return (WheelView.this.mCurrTargetItemIndex * WheelView.this.getItemHeight()) + delta;
        }
    }

    class C03012 extends DataSetObserver {
        C03012() {
        }

        public void onChanged() {
            WheelView.this.invalidateWheel(false);
        }

        public void onInvalidated() {
            WheelView.this.invalidateWheel(false);
        }
    }

    class C03023 extends Handler {
        C03023() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WheelView.MSG_HIDE_SCROLLBAR /*101*/:
                    WheelView.this.hideScrollBar();
                    return;
                default:
                    return;
            }
        }
    }

    class C03034 implements EventCallBack {
        C03034() {
        }

        public boolean onKeyClick(HMKeyEvent keyEvent) {
            Log.i(WheelView.TAG, "onKeyClick");
            if (keyEvent == HMKeyEvent.KEY_UP) {
                Log.i(WheelView.TAG, "UPPPP");
                WheelView.this.setCurrentItem(WheelView.this.getCurrentItem() - 1, true, 300);
            } else if (keyEvent == HMKeyEvent.KEY_DOWN || keyEvent.realAndKeyCode() == 25) {
                WheelView.this.setCurrentItem(WheelView.this.getCurrentItem() + 1, true, 300);
                Log.i(WheelView.TAG, "Downnnn");
            } else if (keyEvent == HMKeyEvent.KEY_CENTER || keyEvent.realAndKeyCode() == 24) {
                Log.i(WheelView.TAG, "Center IN");
                DrawableItem itemToClick = null;
                try {
                    itemToClick = WheelView.this.getItemsLayout().getChildAt(WheelView.this.currentItem + 1);
                } catch (Exception e) {
                }
                if (itemToClick != null) {
                    OnClickListener l = itemToClick.getOnClickListener();
                    if (l != null) {
                        boolean isTouchable = itemToClick.isTouchable();
                        itemToClick.setTouchable(true);
                        l.onClick(itemToClick);
                        itemToClick.setTouchable(isTouchable);
                    }
                }
            }
            return true;
        }

        public boolean onKeyLongOneSecond(HMKeyEvent keyCode) {
            Log.i(WheelView.TAG, "onKeyLongOneSecond");
            return false;
        }

        public boolean onKeyLongThreeSecond(HMKeyEvent keyCode) {
            Log.i(WheelView.TAG, "onKeyLongThreeSecond");
            return false;
        }

        public boolean onKeyLongOneSecondTimeOut(HMKeyEvent keyCode) {
            Log.i(WheelView.TAG, "onKeyLongOneSecondTimeOut");
            return false;
        }

        public boolean onKeyLongThreeSecondTimeOut(HMKeyEvent keyCode) {
            Log.i(WheelView.TAG, "onKeyLongThreeSecondTimeOut");
            return false;
        }
    }

    class C03056 implements AnimatorUpdateListener {
        C03056() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            WheelView.this.mArcScrollBar.setAlpha(((Integer) animation.getAnimatedValue()).intValue());
            WheelView.this.invalidate(false);
        }
    }

    class C03067 implements AnimatorUpdateListener {
        C03067() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            WheelView.this.mArcScrollBar.setAlpha(((Integer) animation.getAnimatedValue()).intValue());
            WheelView.this.invalidate(false);
        }
    }

    public interface OnBottomEdageListener {
    }

    public interface ScrollProcMonitor {
        boolean isItemVisible();

        void updateProc(boolean z, float f, int i);
    }

    public WheelView(XViewWrapper context) {
        super(context);
        this.mWrapper = context;
        initData(context.getContext());
    }

    public void setMarginTop(int marginTop) {
        this.mMarginTop = marginTop;
    }

    public void setMarginBottom(int marginBottom) {
        this.mMarginBottom = marginBottom;
    }

    private void initData(Context context) {
        this.scroller = new WheelScroller(context, this.scrollingListener);
        DisplayMetrics dm = this.mWrapper.getResources().getDisplayMetrics();
        Rect region = new Rect(0, 0, dm.widthPixels, dm.widthPixels);
        this.mArcScrollBar = new ArcScrollbarDrawable(region);
        this.mArcScrollBar.updateBounds(region);
        this.scrollbarOffsetX = -this.mWrapper.getResources().getDimensionPixelOffset(C0288R.dimen.arc_margin_right);
        this.scrollbarOffsetY = 0;
    }

    public void standup(boolean withScroll) {
        standup(withScroll, withScroll);
    }

    public void standup(boolean withScroll, boolean needKick) {
        buildViewForMeasuring();
        if (withScroll) {
            this.scrollbarOffsetY = 0;
            this.scrollOffsetAbs = LIMIT_LINE_SLOP;
            if (needKick) {
                doScroll((float) (-getItemHeight()));
            }
        } else if (needKick) {
            doScroll(LIMIT_LINE_SLOP);
        }
    }

    public void standup() {
        standup(true);
    }

    public void setTopAnchor(int anchor) {
        this.mTopAnchor = anchor;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.scroller.setInterpolator(interpolator);
    }

    public int getVisibleItems() {
        return this.visibleItems;
    }

    public void setVisibleItems(int count) {
        this.visibleItems = count;
    }

    public WheelViewAdapter getViewAdapter() {
        return this.viewAdapter;
    }

    public void setViewAdapter(WheelViewAdapter viewAdapter) {
        if (this.viewAdapter != null) {
            this.viewAdapter.unregisterDataSetObserver(this.dataObserver);
        }
        this.viewAdapter = viewAdapter;
        if (this.viewAdapter != null) {
            this.viewAdapter.registerDataSetObserver(this.dataObserver);
        }
        invalidateWheel(true);
    }

    public void addChangingListener(OnWheelChangedListener listener) {
        this.changingListeners.add(listener);
    }

    public void removeChangingListener(OnWheelChangedListener listener) {
        this.changingListeners.remove(listener);
    }

    protected void notifyChangingListeners(int oldValue, int newValue) {
        for (OnWheelChangedListener listener : this.changingListeners) {
            listener.onChanged(this, oldValue, newValue);
        }
    }

    public void addScrollingListener(OnWheelScrollListener listener) {
        this.scrollingListeners.add(listener);
    }

    public void removeScrollingListener(OnWheelScrollListener listener) {
        this.scrollingListeners.remove(listener);
    }

    protected void notifyScrollingListenersAboutStart() {
        for (OnWheelScrollListener listener : this.scrollingListeners) {
            listener.onScrollingStarted(this);
        }
    }

    protected void notifyScrollingListenersAboutEnd() {
        for (OnWheelScrollListener listener : this.scrollingListeners) {
            listener.onScrollingFinished(this);
        }
    }

    protected void notifyScrollingListenersScrolling() {
        for (OnWheelScrollListener listener : this.scrollingListeners) {
            listener.onScrolling(LIMIT_LINE_SLOP, -this.scrollOffsetAbs);
        }
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(int index, boolean animated) {
        setCurrentItem(index, animated, 0);
    }

    public void setCurrentItem(int index, boolean animated, int timeToAnim) {
        if (this.viewAdapter != null && this.viewAdapter.getItemsCount() != 0) {
            int itemCount = this.viewAdapter.getItemsCount();
            if (index < 0 || index >= itemCount) {
                if (this.isCyclic) {
                    while (index < 0) {
                        index += itemCount;
                    }
                    index %= itemCount;
                } else {
                    return;
                }
            }
            if (index == this.currentItem) {
                return;
            }
            if (animated) {
                int itemsToScroll = index - this.currentItem;
                if (this.isCyclic) {
                    int scroll = (Math.min(index, this.currentItem) + itemCount) - Math.max(index, this.currentItem);
                    if (scroll < Math.abs(itemsToScroll)) {
                        itemsToScroll = itemsToScroll < 0 ? scroll : -scroll;
                    }
                }
                scroll((float) itemsToScroll, timeToAnim);
                return;
            }
            this.scrollingOffset = LIMIT_LINE_SLOP;
            int old = this.currentItem;
            this.currentItem = index;
            notifyChangingListeners(old, this.currentItem);
            invalidate(false);
        }
    }

    public void setCurrentItem(int index) {
        setCurrentItem(index, false);
    }

    public boolean isCyclic() {
        return this.isCyclic;
    }

    public void setCyclic(boolean isCyclic) {
        this.isCyclic = isCyclic;
        invalidateWheel(false);
    }

    public void invalidateWheel(boolean clearCaches) {
        invalidate(false);
    }

    protected int getItemHeight() {
        if (this.itemsLayout != null) {
            return (int) this.itemsLayout.getItemHeight();
        }
        return (int) (this.localRect.height() / ((float) this.visibleItems));
    }

    public void draw(IDisplayProcess c) {
        if (this.itemsLayout == null) {
            super.draw(c);
            return;
        }
        Canvas canvas = c.getCanvas();
        canvas.save();
        float top = ((float) ((this.currentItem - this.firstItem) * getItemHeight())) + ((((float) getItemHeight()) - getHeight()) / 2.0f);
        Matrix console = this.itemsLayout.getMatrix();
        console.setTranslate(LIMIT_LINE_SLOP, ((-top) + this.scrollingOffset) + ((float) this.mMarginTop));
        if (this.scrollingOffset == LIMIT_LINE_SLOP) {
            this.itemsLayout.updateMatrix(console);
        }
        this.itemsLayout.draw(c);
        canvas.restore();
        if (this.mScrollbarEnabled) {
            canvas.save();
            canvas.translate((float) this.scrollbarOffsetX, (float) this.scrollbarOffsetY);
            this.mArcScrollBar.draw(canvas);
            canvas.restore();
        }
    }

    public void onDraw(IDisplayProcess c) {
    }

    public boolean onDown(MotionEvent e) {
        this.mCurrTargetItemIndex = 0;
        cancelOverscrollAnim();
        this.scroller.onTouchEvent(e);
        return super.onDown(e);
    }

    public boolean onFingerUp(MotionEvent event) {
        this.itemsLayout.setTouchable(true);
        if (!this.isScrollingPerformed) {
            int distance = (int) (event.getY() - (getHeight() / 2.0f));
            if (distance > 0) {
                distance += getItemHeight() / 2;
            } else {
                int itemHeight = distance - (getItemHeight() / 2);
            }
        }
        this.scroller.onTouchEvent(event);
        startOverscrollAnim();
        return super.onFingerUp(event);
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
        this.scroller.onTouchEvent(e2);
        return super.onScroll(e1, e2, distanceX, distanceY, previousX, previousY);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doScroll(float r22) {
        /*
        r21 = this;
        r0 = r21;
        r0 = r0.scrollingOffset;
        r17 = r0;
        r17 = r17 + r22;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
        r0 = r21;
        r0 = r0.scrollOffsetAbs;
        r17 = r0;
        r17 = r17 + r22;
        r0 = r17;
        r1 = r21;
        r1.scrollOffsetAbs = r0;
        r17 = 0;
        r17 = (r22 > r17 ? 1 : (r22 == r17 ? 0 : -1));
        if (r17 >= 0) goto L_0x0062;
    L_0x0022:
        r0 = r21;
        r0 = r0.scrollOffsetAbs;
        r17 = r0;
        r17 = java.lang.Math.abs(r17);
        r0 = r21;
        r0 = r0.mAllChildrenHeight;
        r18 = r0;
        r19 = r21.getItemHeight();
        r0 = r19;
        r0 = (float) r0;
        r19 = r0;
        r18 = r18 - r19;
        r0 = r21;
        r0 = r0.mMarginTop;
        r19 = r0;
        r0 = r19;
        r0 = (float) r0;
        r19 = r0;
        r18 = r18 + r19;
        r0 = r21;
        r0 = r0.mMarginBottom;
        r19 = r0;
        r0 = r19;
        r0 = (float) r0;
        r19 = r0;
        r18 = r18 - r19;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x0062;
    L_0x005b:
        r17 = r21.getOverscrollType();
        switch(r17) {
            case -1: goto L_0x0173;
            case 0: goto L_0x0062;
            case 1: goto L_0x0225;
            case 2: goto L_0x017c;
            default: goto L_0x0062;
        };
    L_0x0062:
        r17 = 0;
        r17 = (r22 > r17 ? 1 : (r22 == r17 ? 0 : -1));
        if (r17 <= 0) goto L_0x007b;
    L_0x0068:
        r0 = r21;
        r0 = r0.scrollOffsetAbs;
        r17 = r0;
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x007b;
    L_0x0074:
        r17 = r21.getOverscrollType();
        switch(r17) {
            case -1: goto L_0x024e;
            case 0: goto L_0x007b;
            case 1: goto L_0x0257;
            case 2: goto L_0x0267;
            default: goto L_0x007b;
        };
    L_0x007b:
        r17 = r21.getOverscrollType();
        if (r17 <= 0) goto L_0x0088;
    L_0x0081:
        r17 = r21.getOverscrollType();
        switch(r17) {
            case 1: goto L_0x0274;
            case 2: goto L_0x02d3;
            default: goto L_0x0088;
        };
    L_0x0088:
        r0 = r21;
        r0 = r0.mScrollbarEnabled;
        r17 = r0;
        if (r17 == 0) goto L_0x009d;
    L_0x0090:
        r17 = java.lang.Math.abs(r22);
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x009d;
    L_0x009a:
        r21.updateScrollBar();
    L_0x009d:
        r9 = r21.getItemHeight();
        r0 = r21;
        r0 = r0.scrollingOffset;
        r17 = r0;
        r0 = (float) r9;
        r18 = r0;
        r17 = r17 / r18;
        r0 = r17;
        r2 = (int) r0;
        r0 = r21;
        r0 = r0.currentItem;
        r17 = r0;
        r14 = r17 - r2;
        r0 = r21;
        r0 = r0.viewAdapter;
        r17 = r0;
        r7 = r17.getItemsCount();
        r0 = r21;
        r0 = r0.scrollingOffset;
        r17 = r0;
        r0 = (float) r9;
        r18 = r0;
        r4 = r17 % r18;
        r17 = java.lang.Math.abs(r4);
        r18 = r9 / 2;
        r0 = r18;
        r0 = (float) r0;
        r18 = r0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 > 0) goto L_0x00dc;
    L_0x00db:
        r4 = 0;
    L_0x00dc:
        if (r7 <= 0) goto L_0x00e5;
    L_0x00de:
        if (r14 >= 0) goto L_0x0332;
    L_0x00e0:
        r0 = r21;
        r2 = r0.currentItem;
        r14 = 0;
    L_0x00e5:
        r0 = r21;
        r13 = r0.scrollingOffset;
        r0 = r21;
        r0 = r0.currentItem;
        r17 = r0;
        r0 = r17;
        if (r14 == r0) goto L_0x0362;
    L_0x00f3:
        r17 = 0;
        r0 = r21;
        r1 = r17;
        r0.setCurrentItem(r14, r1);
    L_0x00fc:
        r17 = r2 * r9;
        r0 = r17;
        r0 = (float) r0;
        r17 = r0;
        r17 = r13 - r17;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
        r0 = r21;
        r0 = r0.scrollingOffset;
        r17 = r0;
        r18 = r21.getHeight();
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x012b;
    L_0x0119:
        r17 = r21.getHeight();
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 > 0) goto L_0x036d;
    L_0x0123:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
    L_0x012b:
        r0 = r21;
        r0 = r0.mShowAnchorAnim;
        r17 = r0;
        if (r17 == 0) goto L_0x0163;
    L_0x0133:
        r0 = r21;
        r0 = r0.scrollProcMonitorsMap;
        r17 = r0;
        r17 = r17.isEmpty();
        if (r17 != 0) goto L_0x0163;
    L_0x013f:
        r0 = r21;
        r0 = r0.viewAdapter;
        r17 = r0;
        r8 = r17.getItemsCount();
        r0 = r21;
        r0 = r0.mBaseLine;
        r17 = r0;
        r17 = r17 * r9;
        r18 = r9 / 2;
        r17 = r17 + r18;
        r0 = r21;
        r0 = r0.mMarginTop;
        r18 = r0;
        r17 = r17 - r18;
        r0 = r17;
        r10 = (float) r0;
        r6 = r8;
    L_0x0161:
        if (r6 >= 0) goto L_0x0387;
    L_0x0163:
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x03c8;
    L_0x016f:
        r21.stopScrolling();
    L_0x0172:
        return;
    L_0x0173:
        r17 = 2;
        r0 = r21;
        r1 = r17;
        r0.setOverscrollType(r1);
    L_0x017c:
        r17 = r21.getItemHeight();
        r0 = r17;
        r0 = (float) r0;
        r17 = r0;
        r0 = r21;
        r0 = r0.mAllChildrenHeight;
        r18 = r0;
        r17 = r17 - r18;
        r0 = r21;
        r0 = r0.mMarginTop;
        r18 = r0;
        r0 = r18;
        r0 = (float) r0;
        r18 = r0;
        r17 = r17 - r18;
        r0 = r21;
        r0 = r0.mMarginBottom;
        r18 = r0;
        r0 = r18;
        r0 = (float) r0;
        r18 = r0;
        r17 = r17 + r18;
        r0 = r17;
        r1 = r21;
        r1.scrollOffsetAbs = r0;
        r0 = r21;
        r0 = r0.mAllChildrenHeight;
        r17 = r0;
        r18 = r21.getItemHeight();
        r0 = r18;
        r0 = (float) r0;
        r18 = r0;
        r17 = r17 - r18;
        r18 = r21.getHeight();
        r19 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r18 = r18 / r19;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x0245;
    L_0x01ca:
        r0 = r21;
        r0 = r0.mMarginTop;
        r17 = r0;
        if (r17 == 0) goto L_0x0232;
    L_0x01d2:
        r0 = r21;
        r0 = r0.mMarginTop;
        r17 = r0;
        r18 = r21.getItemHeight();
        r11 = r17 / r18;
        r0 = r21;
        r0 = r0.mMarginTop;
        r17 = r0;
        r18 = r21.getItemHeight();
        r17 = r17 % r18;
        r17 = java.lang.Math.abs(r17);
        r18 = r21.getItemHeight();
        r18 = r18 / 2;
        r0 = r17;
        r1 = r18;
        if (r0 <= r1) goto L_0x0211;
    L_0x01fa:
        r0 = (float) r11;
        r17 = r0;
        r0 = r21;
        r0 = r0.mMarginTop;
        r18 = r0;
        r0 = r18;
        r0 = (float) r0;
        r18 = r0;
        r18 = java.lang.Math.signum(r18);
        r17 = r17 + r18;
        r0 = r17;
        r11 = (int) r0;
    L_0x0211:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        r17 = r17.getChildCount();
        r17 = r17 + -1;
        r17 = r17 + r11;
        r0 = r17;
        r1 = r21;
        r1.currentItem = r0;
    L_0x0225:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
        r21.stopScrolling();
        goto L_0x0062;
    L_0x0232:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        r17 = r17.getChildCount();
        r17 = r17 + -1;
        r0 = r17;
        r1 = r21;
        r1.currentItem = r0;
        goto L_0x0225;
    L_0x0245:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.currentItem = r0;
        goto L_0x0225;
    L_0x024e:
        r17 = 1;
        r0 = r21;
        r1 = r17;
        r0.setOverscrollType(r1);
    L_0x0257:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.scrollOffsetAbs = r0;
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.currentItem = r0;
    L_0x0267:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
        r21.stopScrolling();
        goto L_0x007b;
    L_0x0274:
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r17 = r17 + r22;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 >= 0) goto L_0x02be;
    L_0x028e:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
    L_0x0296:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        if (r17 == 0) goto L_0x02b3;
    L_0x029e:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        r18 = 1;
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r19 = r0;
        r20 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r19 = r19 / r20;
        r17.setOverscroll(r18, r19);
    L_0x02b3:
        r17 = 0;
        r0 = r21;
        r1 = r17;
        r0.invalidate(r1);
        goto L_0x0088;
    L_0x02be:
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r18 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x0296;
    L_0x02ca:
        r17 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
        goto L_0x0296;
    L_0x02d3:
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r17 = r17 - r22;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r18 = 0;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 >= 0) goto L_0x031d;
    L_0x02ed:
        r17 = 0;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
    L_0x02f5:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        if (r17 == 0) goto L_0x0312;
    L_0x02fd:
        r0 = r21;
        r0 = r0.itemsLayout;
        r17 = r0;
        r18 = 0;
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r19 = r0;
        r20 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r19 = r19 / r20;
        r17.setOverscroll(r18, r19);
    L_0x0312:
        r17 = 0;
        r0 = r21;
        r1 = r17;
        r0.invalidate(r1);
        goto L_0x0088;
    L_0x031d:
        r0 = r21;
        r0 = r0.mOverscrollOffset;
        r17 = r0;
        r18 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 <= 0) goto L_0x02f5;
    L_0x0329:
        r17 = 1117126656; // 0x42960000 float:75.0 double:5.51933903E-315;
        r0 = r17;
        r1 = r21;
        r1.mOverscrollOffset = r0;
        goto L_0x02f5;
    L_0x0332:
        if (r14 < r7) goto L_0x0342;
    L_0x0334:
        r0 = r21;
        r0 = r0.currentItem;
        r17 = r0;
        r17 = r17 - r7;
        r2 = r17 + 1;
        r14 = r7 + -1;
        goto L_0x00e5;
    L_0x0342:
        if (r14 <= 0) goto L_0x0350;
    L_0x0344:
        r17 = 0;
        r17 = (r4 > r17 ? 1 : (r4 == r17 ? 0 : -1));
        if (r17 <= 0) goto L_0x0350;
    L_0x034a:
        r14 = r14 + -1;
        r2 = r2 + 1;
        goto L_0x00e5;
    L_0x0350:
        r17 = r7 + -1;
        r0 = r17;
        if (r14 >= r0) goto L_0x00e5;
    L_0x0356:
        r17 = 0;
        r17 = (r4 > r17 ? 1 : (r4 == r17 ? 0 : -1));
        if (r17 >= 0) goto L_0x00e5;
    L_0x035c:
        r14 = r14 + 1;
        r2 = r2 + -1;
        goto L_0x00e5;
    L_0x0362:
        r17 = 0;
        r0 = r21;
        r1 = r17;
        r0.invalidate(r1);
        goto L_0x00fc;
    L_0x036d:
        r0 = r21;
        r0 = r0.scrollingOffset;
        r17 = r0;
        r18 = r21.getHeight();
        r17 = r17 % r18;
        r18 = r21.getHeight();
        r17 = r17 + r18;
        r0 = r17;
        r1 = r21;
        r1.scrollingOffset = r0;
        goto L_0x012b;
    L_0x0387:
        r0 = r21;
        r0 = r0.scrollProcMonitorsMap;
        r17 = r0;
        r18 = java.lang.Integer.valueOf(r6);
        r12 = r17.get(r18);
        r12 = (clc.utils.venus.widgets.wheel.WheelView.ScrollProcMonitor) r12;
        if (r12 == 0) goto L_0x039f;
    L_0x0399:
        r17 = r12.isItemVisible();
        if (r17 != 0) goto L_0x03a3;
    L_0x039f:
        r6 = r6 + -1;
        goto L_0x0161;
    L_0x03a3:
        r17 = r8 - r6;
        r17 = r17 * r9;
        r18 = r9 / 2;
        r17 = r17 + r18;
        r0 = r17;
        r0 = (float) r0;
        r16 = r0;
        r0 = r21;
        r0 = r0.scrollOffsetAbs;
        r17 = r0;
        r5 = r16 + r17;
        r3 = r5 - r10;
        r17 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r17 = r17 + r10;
        r15 = r3 / r17;
        r17 = 1;
        r0 = r17;
        r12.updateProc(r0, r15, r6);
        goto L_0x039f;
    L_0x03c8:
        r17 = -1;
        r0 = r21;
        r1 = r17;
        r0.setOverscrollType(r1);
        goto L_0x0172;
        */
        throw new UnsupportedOperationException("Method not decompiled: clc.utils.venus.widgets.wheel.WheelView.doScroll(float):void");
    }

    public void updateScrollBar() {
        showScrollBarInternal();
        switch (getOverscrollType()) {
            case -1:
                this.mArcScrollBar.updateProc(this.scrollOffsetAbs / (((((float) getItemHeight()) - this.mAllChildrenHeight) - ((float) this.mMarginTop)) + ((float) this.mMarginBottom)));
                notifyScrollingListenersScrolling();
                return;
            case 1:
                this.mArcScrollBar.updateProc((this.scrollOffsetAbs + this.mOverscrollOffset) / (((((float) getItemHeight()) - this.mAllChildrenHeight) - ((float) this.mMarginTop)) + ((float) this.mMarginBottom)));
                return;
            case 2:
                this.mArcScrollBar.updateProc((this.scrollOffsetAbs - this.mOverscrollOffset) / (((((float) getItemHeight()) - this.mAllChildrenHeight) - ((float) this.mMarginTop)) + ((float) this.mMarginBottom)));
                return;
            default:
                return;
        }
    }

    public void setBaseLine(int baseIndex) {
        this.mBaseLine = baseIndex;
    }

    public void setAnchorForAnimListenerEnable(boolean enable) {
        this.mShowAnchorAnim = enable;
    }

    public void scroll(float itemsToScroll, int time) {
        this.scroller.scroll((((float) getItemHeight()) * itemsToScroll) - this.scrollingOffset, time);
    }

    public ItemsRange getItemsRange() {
        if (getItemHeight() == 0) {
            return null;
        }
        int first = this.currentItem;
        int count = 1;
        while (((float) (getItemHeight() * count)) < getHeight()) {
            first--;
            count += 2;
        }
        if (this.scrollingOffset != LIMIT_LINE_SLOP) {
            if (this.scrollingOffset > LIMIT_LINE_SLOP) {
                first--;
            }
            int emptyItems = (int) (this.scrollingOffset / ((float) getItemHeight()));
            first -= emptyItems;
            count = (int) (((double) (count + 1)) + Math.asin((double) emptyItems));
        }
        return new ItemsRange(first, count);
    }

    private WheelViewItemsLayout createItemsLayout() {
        if (this.itemsLayout != null) {
            BaseDrawableGroup group = (BaseDrawableGroup) this.itemsLayout.getParent();
            if (group != null) {
                group.removeItem(this.itemsLayout);
            }
            this.itemsLayout.destory();
        }
        this.itemsLayout = onCreateItemsLayout(this.mWrapper);
        return this.itemsLayout;
    }

    protected WheelViewItemsLayout onCreateItemsLayout(XViewWrapper wrapper) {
        WheelVerticalLayout defaultLayout = new WheelVerticalLayout(this, new RectF(LIMIT_LINE_SLOP, LIMIT_LINE_SLOP, this.localRect.width(), this.localRect.height()), this.localRect.width(), this.localRect.height());
        calculateGlobalTouchRect();
        return defaultLayout;
    }

    public boolean onFingerCancel(MotionEvent e) {
        this.scroller.onTouchEvent(e);
        startOverscrollAnim();
        return super.onFingerCancel(e);
    }

    private void buildViewForMeasuring() {
        this.itemsLayout = createItemsLayout();
        for (int i = this.viewAdapter.getItemsCount() - 1; i >= 0; i--) {
            if (addViewItem(i, true)) {
                this.firstItem = i;
            }
        }
        if (this.itemsLayout.getParent() == null) {
            addItem(this.itemsLayout);
        }
    }

    private boolean addViewItem(int index, boolean first) {
        DrawableItem view = getItemView(index);
        if (view instanceof ScrollProcMonitor) {
            this.scrollProcMonitorsMap.put(Integer.valueOf(index), (ScrollProcMonitor) view);
        }
        this.itemsLayout.addItem(view);
        this.mAllChildrenHeight = (float) (this.itemsLayout.getChildCount() * getItemHeight());
        this.mArcScrollBar.setBlockLength(getHeight() / this.mAllChildrenHeight);
        return true;
    }

    public void removeItem() {
    }

    private DrawableItem getItemView(int index) {
        if (this.viewAdapter == null || this.viewAdapter.getItemsCount() == 0) {
            return null;
        }
        int count = this.viewAdapter.getItemsCount();
        while (index < 0) {
            index += count;
        }
        return this.viewAdapter.getItem((count - (index % count)) - 1, null, this.itemsLayout);
    }

    public void stopScrolling() {
        this.scroller.stopScrolling();
    }

    public WheelViewItemsLayout getItemsLayout() {
        return this.itemsLayout;
    }

    public void offsetScrollBarTo(int x, int y) {
        this.scrollbarOffsetX = x;
        this.scrollbarOffsetY = y;
    }

    public void clean() {
        if (this.itemsLayout != null) {
            this.itemsLayout.clearAllItems();
        }
        super.clean();
    }

    public void resetListPosition() {
        setCurrentItem(0);
        this.scrollOffsetAbs = LIMIT_LINE_SLOP;
        this.scrollingOffset = LIMIT_LINE_SLOP;
        this.mArcScrollBar.updateProc(LIMIT_LINE_SLOP);
        doScroll(1.0E-4f);
        notifyScrollingListenersScrolling();
    }

    public boolean isViewBottomEdged() {
        if (Math.abs(this.scrollOffsetAbs) >= (this.mAllChildrenHeight - ((float) getItemHeight())) + ((float) this.mMarginTop)) {
            return true;
        }
        return false;
    }

    public void justify() {
        this.scroller.justify();
    }

    public void setScrollbarEnabled(boolean scrollEnable) {
        this.mScrollbarEnabled = scrollEnable;
        if (this.mScrollbarEnabled) {
            updateScrollBar();
            showScrollBar();
        }
    }

    public float canScrollUp() {
        return Math.max(LIMIT_LINE_SLOP, ((this.mAllChildrenHeight - ((float) getItemHeight())) + ((float) this.mMarginTop)) - Math.abs(this.scrollOffsetAbs));
    }

    private void setOverscrollType(int type) {
        this.mOverscrollType = type;
    }

    public int getOverscrollType() {
        return this.mOverscrollType;
    }

    public void onTouchCancel(MotionEvent e) {
        startOverscrollAnim();
        super.onTouchCancel(e);
    }

    private void startOverscrollAnim() {
        if (this.mOverscrollAnim != null && this.mOverscrollAnim.isStarted()) {
            this.mOverscrollAnim.cancel();
        }
        boolean top = true;
        switch (getOverscrollType()) {
            case -1:
                return;
            case 1:
                top = true;
                break;
            case 2:
                top = false;
                break;
        }
        final boolean t = top;
        this.mOverscrollAnim = ValueAnimator.ofFloat(new float[]{this.mOverscrollOffset, LIMIT_LINE_SLOP});
        this.mOverscrollAnim.setDuration(229);
        this.mOverscrollAnim.setInterpolator(INTER_OUT);
        this.mOverscrollAnim.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                WheelView.this.mOverscrollOffset = ((Float) animation.getAnimatedValue()).floatValue();
                if (WheelView.this.itemsLayout != null) {
                    WheelView.this.itemsLayout.setOverscroll(t, WheelView.this.mOverscrollOffset / 75.0f);
                    if (WheelView.this.mScrollbarEnabled) {
                        WheelView.this.updateScrollBar();
                    }
                    WheelView.this.invalidate(false);
                }
            }
        });
        this.mOverscrollAnim.start();
    }

    private void cancelOverscrollAnim() {
        if (this.mOverscrollAnim != null && this.mOverscrollAnim.isStarted()) {
            this.mOverscrollAnim.cancel();
        }
    }

    private void cancelScrollbarAnim() {
        if (this.mScrollbarAnim != null && this.mScrollbarAnim.isStarted()) {
            this.mScrollbarAnim.cancel();
        }
    }

    public final void showScrollBar() {
        if (this.mScrollbarEnabled) {
            showScrollBar(DELAY_HIDE_SCROLLBAR_PUB);
        }
    }

    private void showScrollBarInternal() {
        showScrollBar(200);
    }

    private void showScrollBar(int hideDelay) {
        if (!this.hasShow) {
            this.hasShow = true;
            cancelScrollbarAnim();
            if (this.mArcScrollBar.getAlpha() != 255) {
                this.mScrollbarAnim = ValueAnimator.ofInt(new int[]{this.mArcScrollBar.getAlpha(), 255});
                this.mScrollbarAnim.setDuration(100);
                this.mScrollbarAnim.setInterpolator(INTER_OUT);
                this.mScrollbarAnim.addUpdateListener(new C03056());
                this.mScrollbarAnim.start();
            }
        }
        this.mHandler.removeMessages(MSG_HIDE_SCROLLBAR);
        if (hideDelay > 0) {
            this.mHandler.sendEmptyMessageDelayed(MSG_HIDE_SCROLLBAR, (long) hideDelay);
        } else {
            this.mHandler.sendEmptyMessageDelayed(MSG_HIDE_SCROLLBAR, 200);
        }
    }

    private void hideScrollBar() {
        if (this.hasShow) {
            this.hasShow = false;
            cancelScrollbarAnim();
            if (this.mArcScrollBar.getAlpha() != 0) {
                this.mScrollbarAnim = ValueAnimator.ofInt(new int[]{this.mArcScrollBar.getAlpha(), 0});
                this.mScrollbarAnim.setDuration(200);
                this.mScrollbarAnim.setInterpolator(INTER_OUT);
                this.mScrollbarAnim.addUpdateListener(new C03067());
                this.mScrollbarAnim.start();
            }
        }
    }

    public void startFadeInAnimation() {
        if (this.itemsLayout != null) {
            this.itemsLayout.startFadeInAnimation();
        }
        showScrollBar(DELAY_HIDE_SCROLLBAR_PUB);
        invalidate(false);
    }

    public void showFocusAt(int itemIndex) {
        this.lastIndexOfFocus = this.indexOfFocus;
        this.indexOfFocus = itemIndex;
        if (this.canSupportFocus) {
            this.itemsLayout.setFocusIndex(this.lastIndexOfFocus, this.indexOfFocus);
        }
    }

    public void scrollToFocusItem() {
        setCurrentItem(this.itemsLayout.getFocusIndex(), true, 500);
    }

    public void injectKeyevent(KeyEvent event) {
        if (!this.acceptKeyEvent) {
            Log.i(TAG, "NOT accept handle keyEvent for : " + this);
        } else if (event.getAction() == 0 || event.getAction() == 1) {
            if (this.keyEventManager == null) {
                this.keyEventManager = new KeyeventProcessor(this.keyEventCallback);
            }
            Log.i(TAG, "Accept handle keyEvent for : " + this);
            this.keyEventManager.injectKeyEvent(event);
        }
    }

    public void setAcceptKeyEvent(boolean enable) {
        this.acceptKeyEvent = enable;
    }

    public void setOnKeyeventListener(EventCallBack callback) {
        if (this.keyEventManager != null) {
            this.keyEventManager.setKeyeventListener(callback);
        }
    }

    public boolean canAccept() {
        return this.acceptKeyEvent;
    }

    public int getLastIndexOfFocus() {
        return this.lastIndexOfFocus;
    }

    public int getCurrFocus() {
        return this.indexOfFocus;
    }

    public boolean canSupportFocus() {
        return this.canSupportFocus;
    }

    public void setCanSupportFocus(boolean support) {
        this.canSupportFocus = support;
        if (this.itemsLayout != null) {
            this.itemsLayout.setSupportFocus(support);
        }
    }

    public void resetFocus() {
        resetListPosition();
        setCurrentItem(0, false, 0);
        showFocusAt(0);
        getItemsLayout().resetFocus();
    }
}
