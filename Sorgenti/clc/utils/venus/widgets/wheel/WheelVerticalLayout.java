package clc.utils.venus.widgets.wheel;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.RectF;
import clc.utils.venus.DrawableItem;
import clc.utils.venus.IDisplayProcess;

public class WheelVerticalLayout extends WheelViewItemsLayout {
    private static final float[] OVERSCROLLS = new float[]{10.0f, 18.0f, 26.0f};
    private int mItemHeight;
    private int mItemWidth;
    WheelView mWheel;

    public WheelVerticalLayout(WheelView wheel, RectF localRect, int itemWidth, int itemHeight) {
        super(wheel.getXContext());
        resize(localRect);
        this.mWheel = wheel;
        this.mItemHeight = itemHeight;
        this.mItemWidth = itemWidth;
    }

    public WheelVerticalLayout(WheelView wheel, RectF localRect, float itemWidth, float itemHeight) {
        this(wheel, localRect, (int) itemWidth, (int) itemHeight);
    }

    public void resize(RectF rect) {
        super.resize(rect);
        calculateGlobalTouchRect();
    }

    public void onDraw(IDisplayProcess c) {
        int currItem = this.mWheel.getCurrentItem();
        int start = Math.max(0, currItem - 1);
        int end = Math.min(getChildCount() - 1, currItem + 2);
        for (int i = start; i <= end; i++) {
            DrawableItem item = (DrawableItem) this.items.get(i);
            if (canSupportFocus()) {
                if (getFocusIndex() == i) {
                    if (!item.isFocused()) {
                        item.setFocused(true);
                    }
                } else if (item.isFocused()) {
                    item.setFocused(false);
                    onLoseFocusedState(item);
                }
            }
            if (item != null) {
                item.draw(c);
            }
        }
    }

    public boolean addItem(DrawableItem item, int index) {
        if (item == null || item.getParent() != null) {
            return false;
        }
        int yStart = (this.mItemHeight * getChildCount()) + ((int) (((float) (this.mItemHeight / 2)) - (item.getHeight() / 2.0f)));
        item.offsetRelative((float) ((int) ((this.localRect.width() / 2.0f) - ((float) (this.mItemWidth / 2)))), (float) yStart);
        item.calculateGlobalTouchRect();
        resize(new RectF(this.localRect.left, this.localRect.top, this.localRect.right, (float) (this.mItemHeight + yStart)));
        return super.addItem(item, index);
    }

    public boolean addItem(DrawableItem item) {
        return addItem(item, getChildCount());
    }

    public float getItemHeight() {
        return (float) this.mItemHeight;
    }

    public void setOverscroll(boolean top, float input) {
        DrawableItem item0;
        DrawableItem item1;
        DrawableItem item2;
        if (top) {
            item0 = getChildAt(0);
            if (item0 != null) {
                item0.setTranslationY(OVERSCROLLS[0] * input);
                item1 = getChildAt(1);
                if (item1 != null) {
                    item1.setTranslationY(OVERSCROLLS[1] * input);
                    item2 = getChildAt(2);
                    if (item2 != null) {
                        item2.setTranslationY(OVERSCROLLS[2] * input);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        item0 = getChildAt(getChildCount() - 1);
        if (item0 != null) {
            item0.setTranslationY(-(OVERSCROLLS[0] * input));
            item1 = getChildAt(getChildCount() - 2);
            if (item1 != null) {
                item1.setTranslationY(-(OVERSCROLLS[1] * input));
                item2 = getChildAt(getChildCount() - 3);
                if (item2 != null) {
                    item2.setTranslationY(-(OVERSCROLLS[2] * input));
                }
            }
        }
    }

    protected void startFadeInAnimation() {
        QuadInterpolator QUAD = new QuadInterpolator((byte) 1);
        AnimatorSet anims = new AnimatorSet();
        ValueAnimator lastA = null;
        for (int i = 0; i < OVERSCROLLS.length; i++) {
            final DrawableItem item = getChildAt(i);
            if (item != null) {
                item.setAlpha(0.0f);
                final float transY = OVERSCROLLS[i];
                item.setTranslationY(transY);
                final ValueAnimator last = lastA;
                ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
                anim.setDuration(500);
                anim.setStartDelay((long) (i * 125));
                anim.setInterpolator(QUAD);
                anim.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float input = ((Float) animation.getAnimatedValue()).floatValue();
                        item.setTranslationY(transY * (1.0f - input));
                        item.setAlpha(input);
                        if (last == null || !last.isStarted()) {
                            WheelVerticalLayout.this.invalidate(false);
                        }
                    }
                });
                if (item.isCached()) {
                    item.disableCache();
                    anim.addListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animation) {
                            item.enableCache();
                        }
                    });
                }
                anims.play(anim);
                lastA = anim;
            }
        }
        anims.start();
    }
}
