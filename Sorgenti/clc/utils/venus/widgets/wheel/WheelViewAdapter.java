package clc.utils.venus.widgets.wheel;

import android.database.DataSetObserver;
import clc.utils.venus.BaseDrawableGroup;
import clc.utils.venus.DrawableItem;

public interface WheelViewAdapter {
    DrawableItem getItem(int i, DrawableItem drawableItem, BaseDrawableGroup baseDrawableGroup);

    int getItemsCount();

    void registerDataSetObserver(DataSetObserver dataSetObserver);

    void unregisterDataSetObserver(DataSetObserver dataSetObserver);
}
