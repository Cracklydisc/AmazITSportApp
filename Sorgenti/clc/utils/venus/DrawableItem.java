package clc.utils.venus;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.LinearInterpolator;
import clc.utils.venus.widgets.wheel.QuadInterpolator;

public class DrawableItem {
    private static final boolean DEBUG = false;
    private static final int HALO_ALPHA_START = 180;
    private static final float NONZERO_EPSILON = 0.001f;
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private static final String TAG = "L3";
    private static final int mDurationHalo = 330;
    private static final int mDurationIconScale = 125;
    static final ThreadLocal<Rect> sThreadLocal = new ThreadLocal();
    protected float alpha = 1.0f;
    private boolean bgAlphaDirty = false;
    protected boolean cacheDirtyTag = true;
    private boolean caculatingRect = false;
    private RectF clipRect = new RectF();
    private boolean desiredTouchEvent = false;
    private RectF drawableBGRect;
    private boolean enableCache = false;
    private boolean enableExtraAlpha = false;
    private float extraAlpha = 1.0f;
    private Bitmap faceToDraw;
    protected float finalAlpha = 1.0f;
    protected int finalAlphaInt = 255;
    private boolean focused = false;
    private Matrix globalToLocalMatrix = new Matrix();
    private RectF globalTouchRect = new RectF();
    private boolean invertMatrixDirty = false;
    private boolean isBackgroundVisible = true;
    protected boolean isRecycled = false;
    private boolean isVisible = true;
    public RectF localRect = new RectF();
    private Matrix f7m;
    private Drawable mBGDrawable;
    private Camera mCamera = null;
    protected final XViewWrapper mContext;
    private int mDebugColor = -65536;
    private boolean mEnableHalo = false;
    private boolean mEnableHaloFeature = false;
    private RectF mExtraTouchBounds;
    private ValueAnimator mHaloAnim;
    protected float mHaloCenterX;
    protected float mHaloCenterY;
    private Paint mHaloPaint;
    private float mHaloRadiiusLoading;
    private float mHaloRadius;
    private float mHaloRadiusEnd;
    private float mHaloRadiusStart;
    private float mHaloScaleEnd = 1.4f;
    private float mHaloScaleLoading = 1.25f;
    private float mHaloScaleStart = 0.9f;
    boolean mMatrixDirty = false;
    private String mName = "";
    private OnClickListener mOnClickListener;
    private OnLongClickListener mOnLongClickListener;
    protected int mPaddingBottom = 0;
    protected int mPaddingLeft = 0;
    protected int mPaddingRight = 0;
    protected int mPaddingTop = 0;
    boolean mPivotSet = false;
    float mPivotX = 240.0f;
    float mPivotY = 400.0f;
    int mPrivateFlags;
    float mRotation = 0.0f;
    public float mRotationX = 0.0f;
    public float mRotationY = 0.0f;
    private ValueAnimator mScaleAnim;
    float mScaleX = 1.0f;
    float mScaleY = 1.0f;
    private SweepGradient mShader;
    private Object mTag;
    float mTranslationX = 0.0f;
    float mTranslationY = 0.0f;
    private OnVisibilityChangeListener mVisibilityListener;
    private Matrix matrix = new Matrix();
    private Matrix matrix3D = null;
    protected int maxHeight = Integer.MAX_VALUE;
    protected int maxWidth = Integer.MAX_VALUE;
    protected int minHeight = 0;
    protected int minWidth = 0;
    private boolean needPromoteDrawingOrder = false;
    private Paint paint;
    private DrawableItem parent;
    protected boolean removeTag = false;
    private final Matrix tempM = new Matrix();
    private boolean touchable = true;
    protected boolean touched;
    private boolean wantKnowVisibleState = false;

    class C02821 implements AnimatorUpdateListener {
        C02821() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            DrawableItem.this.f7m.setRotate(360.0f * ((Float) animation.getAnimatedValue()).floatValue(), DrawableItem.this.mHaloCenterX, DrawableItem.this.mHaloCenterY);
            DrawableItem.this.mShader.setLocalMatrix(DrawableItem.this.f7m);
            DrawableItem.this.mHaloPaint.setShader(DrawableItem.this.mShader);
            DrawableItem.this.invalidate(false);
        }
    }

    class C02832 implements AnimatorUpdateListener {
        C02832() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            DrawableItem.this.mHaloRadius = DrawableItem.this.mHaloRadiusStart + ((DrawableItem.this.mHaloRadiusEnd - DrawableItem.this.mHaloRadiusStart) * input);
            DrawableItem.this.mHaloPaint.setAlpha((int) ((1.0f - input) * 180.0f));
            DrawableItem.this.invalidate();
        }
    }

    class C02854 implements AnimatorUpdateListener {
        C02854() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float scale = 1.0f - (((Float) animation.getAnimatedValue()).floatValue() * 0.1f);
            DrawableItem.this.setScaleX(scale);
            DrawableItem.this.setScaleY(scale);
            if (DrawableItem.this.mHaloAnim != null && !DrawableItem.this.mHaloAnim.isStarted()) {
                DrawableItem.this.invalidate(false);
            }
        }
    }

    class C02865 extends AnimatorListenerAdapter {
        C02865() {
        }

        public void onAnimationRepeat(Animator animation) {
            if (DrawableItem.this.mHaloAnim != null) {
                DrawableItem.this.mHaloAnim.start();
            }
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            DrawableItem.this.setScaleX(1.0f);
            DrawableItem.this.setScaleY(1.0f);
            DrawableItem.this.invalidate(false);
        }
    }

    public interface OnClickListener {
        void onClick(DrawableItem drawableItem);
    }

    public interface OnDragListener {
    }

    public interface OnLongClickListener {
        boolean onLongClick(DrawableItem drawableItem);
    }

    public interface OnScrollListener {
    }

    public interface OnVisibilityChangeListener {
        void onVisibilityChange(DrawableItem drawableItem, boolean z);
    }

    public void setBgAlphaDirty() {
        this.bgAlphaDirty = true;
    }

    public XViewWrapper getXContext() {
        return this.mContext;
    }

    public void setFocused(boolean focused) {
        this.focused = focused;
    }

    public boolean isFocused() {
        return this.focused;
    }

    public void setExtraTouchBounds(RectF extraTouchBounds) {
        this.mExtraTouchBounds = new RectF(extraTouchBounds);
        if (this.parent != null) {
            RectF tmpTouchBounds = new RectF(this.parent.localRect);
            tmpTouchBounds.union(extraTouchBounds);
            this.parent.setExtraTouchBounds(tmpTouchBounds);
        }
    }

    public void resetTouchBounds() {
        if (this.mExtraTouchBounds != null) {
            this.mExtraTouchBounds.setEmpty();
        }
        if (this.parent != null) {
            this.parent.resetTouchBounds();
        }
        this.mExtraTouchBounds = null;
    }

    public RectF getExtraTouchBounds() {
        return this.mExtraTouchBounds;
    }

    public boolean hasExtraTouchBounds() {
        return (this.mExtraTouchBounds == null || this.mExtraTouchBounds.isEmpty()) ? false : true;
    }

    public boolean isDesiredTouchEventItem() {
        return this.desiredTouchEvent;
    }

    public void desireTouchEvent(boolean desire) {
        this.desiredTouchEvent = desire;
        if (this.parent != null) {
            this.parent.desireTouchEvent(desire);
        }
    }

    protected boolean wantKnowVisibleState() {
        return this.wantKnowVisibleState;
    }

    protected void wantKnowVisibleState(boolean wanted) {
        this.wantKnowVisibleState = wanted;
    }

    public DrawableItem(XViewWrapper context) {
        if (context == null) {
            throw new NullPointerException("Context is NULL!");
        }
        this.mContext = context;
        this.paint = new Paint();
        this.paint.setFilterBitmap(true);
    }

    public void promoteDrawingOrder(boolean promote) {
        this.needPromoteDrawingOrder = promote;
    }

    public boolean needPromoteDrawingOrder() {
        return this.needPromoteDrawingOrder;
    }

    public boolean isCached() {
        return this.enableCache;
    }

    public void enableCache() {
        this.enableCache = true;
        setCacheDirty(true);
        updateFinalAlpha();
        setBgAlphaDirty();
    }

    public void disableCache() {
        this.enableCache = false;
        updateFinalAlpha();
        if (this.faceToDraw != null) {
            if (!this.faceToDraw.isRecycled()) {
                this.faceToDraw.recycle();
            }
            this.faceToDraw = null;
        }
        setBgAlphaDirty();
    }

    public boolean isCacheDirty() {
        return this.cacheDirtyTag;
    }

    public void setCacheDirty(boolean dirtyTag) {
        this.cacheDirtyTag = dirtyTag;
    }

    public void setInvertMatrixDirty() {
        this.invertMatrixDirty = true;
    }

    public boolean isInvertMatrixDirty() {
        return this.invertMatrixDirty;
    }

    public Matrix getInvertMatrix() {
        Matrix resMatrix = this.globalToLocalMatrix;
        if (!this.invertMatrixDirty) {
            return resMatrix;
        }
        resMatrix = new Matrix();
        getInvMatrixRecursive(this, resMatrix);
        this.matrix.invert(this.tempM);
        resMatrix.postConcat(this.tempM);
        this.globalToLocalMatrix = resMatrix;
        this.invertMatrixDirty = false;
        return resMatrix;
    }

    private DrawableItem getInvMatrixRecursive(DrawableItem current, Matrix currentMatrix) {
        if (current.getParent() != null) {
            if (current.getParent().isInvertMatrixDirty()) {
                DrawableItem res = getInvMatrixRecursive(current.getParent(), currentMatrix);
                res.getMatrix().invert(this.tempM);
                currentMatrix.postConcat(this.tempM);
                res.globalToLocalMatrix.set(currentMatrix);
                res.invertMatrixDirty = false;
                currentMatrix.postTranslate(-res.getRelativeX(), -res.getRelativeY());
            } else {
                currentMatrix.postConcat(current.getParent().getInvertMatrix());
                currentMatrix.postTranslate(-current.getParent().getRelativeX(), -current.getParent().getRelativeY());
            }
        }
        return current;
    }

    public Matrix getMatrix() {
        return this.matrix;
    }

    public void setRemoveTag(boolean removeTag) {
        this.removeTag = removeTag;
    }

    public boolean isNeedRemove() {
        return this.removeTag;
    }

    public void setFace(Bitmap faceToSet) {
        if (faceToSet != null) {
            enableCache();
            setCacheDirty(false);
            if (!(this.faceToDraw == null || this.faceToDraw.isRecycled())) {
                this.faceToDraw.recycle();
                this.faceToDraw = null;
            }
            this.faceToDraw = faceToSet;
        }
    }

    public void updateMatrix(Matrix matrix) {
        this.matrix.set(matrix);
        this.matrix.invert(this.globalToLocalMatrix);
        setInvertMatrixDirty();
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    public Paint getPaint() {
        return this.paint;
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public void setVisibility(boolean visibility) {
        this.isVisible = visibility;
        if (this.mVisibilityListener != null) {
            this.mVisibilityListener.onVisibilityChange(this, visibility);
        }
    }

    public void setTouchable(boolean touchable) {
        this.touchable = touchable;
    }

    public boolean isTouchable() {
        return this.touchable;
    }

    public boolean isTouched() {
        return this.touched;
    }

    public Bitmap getCurrentFace() {
        return this.faceToDraw;
    }

    public RectF getClipRect() {
        return this.clipRect;
    }

    public void setClipRect(RectF r) {
        this.clipRect = new RectF(r);
    }

    public float getRelativeX() {
        return this.localRect.left;
    }

    public float getRelativeY() {
        return this.localRect.top;
    }

    public float getWidth() {
        return this.localRect.width();
    }

    public float getHeight() {
        return this.localRect.height();
    }

    public void setRelativeX(float x) {
        this.localRect.offsetTo(x, this.localRect.top);
        setInvertMatrixDirty();
    }

    public void setRelativeY(float y) {
        this.localRect.offsetTo(this.localRect.left, y);
        setInvertMatrixDirty();
    }

    public void offsetRelative(float deltaX, float deltaY) {
        this.localRect.offset(deltaX, deltaY);
        setInvertMatrixDirty();
    }

    public RectF getGlobalTouchRect() {
        return this.globalTouchRect;
    }

    public float getGlobalX() {
        return this.globalTouchRect != null ? this.globalTouchRect.left : 0.0f;
    }

    public float getGlobalX2() {
        float x = getRelativeX();
        if (getParent() != null) {
            return x + getParent().getRelativeX();
        }
        return x;
    }

    public float getGlobalY2() {
        float y = getRelativeY();
        if (getParent() != null) {
            return y + getParent().getRelativeY();
        }
        return y;
    }

    public float getGlobalY() {
        return this.globalTouchRect != null ? this.globalTouchRect.top : 0.0f;
    }

    public void calculateGlobalTouchRect() {
        boolean faceNull = true;
        float f = 0.0f;
        if (!this.caculatingRect) {
            float f2;
            this.caculatingRect = true;
            if (this.globalTouchRect == null) {
                this.globalTouchRect = new RectF();
            }
            if (this.faceToDraw != null) {
                faceNull = false;
            }
            RectF rectF = this.globalTouchRect;
            if (this.parent != null) {
                f2 = this.parent.getGlobalTouchRect().left + this.localRect.left;
            } else {
                f2 = 0.0f;
            }
            rectF.left = f2;
            RectF rectF2 = this.globalTouchRect;
            if (this.parent != null) {
                f = this.parent.getGlobalTouchRect().top + this.localRect.top;
            }
            rectF2.top = f;
            RectF rectF3 = this.globalTouchRect;
            if (faceNull) {
                f2 = this.globalTouchRect.left + this.localRect.width();
            } else {
                f2 = this.globalTouchRect.left + ((float) this.faceToDraw.getWidth());
            }
            rectF3.right = f2;
            rectF3 = this.globalTouchRect;
            if (faceNull) {
                f2 = this.globalTouchRect.top + this.localRect.height();
            } else {
                f2 = this.globalTouchRect.top + ((float) this.faceToDraw.getHeight());
            }
            rectF3.bottom = f2;
            if (this.matrix != null) {
                this.matrix.mapRect(this.globalTouchRect);
            }
            this.caculatingRect = false;
        }
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    protected void setParent(DrawableItem prent) {
        this.parent = prent;
        if (this.parent != null) {
            setInvertMatrixDirty();
            calculateGlobalTouchRect();
            this.clipRect = this.parent.getClipRect();
        }
    }

    public DrawableItem getParent() {
        return this.parent;
    }

    protected Bitmap createFace() {
        int width = (int) this.localRect.width();
        int height = (int) this.localRect.height();
        if (width <= 0 || height <= 0) {
            return null;
        }
        Bitmap face = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        NormalDisplayProcess tmpProc = new NormalDisplayProcess();
        tmpProc.beginDisplay(face);
        float alphaTemp = this.alpha;
        this.alpha = 1.0f;
        this.finalAlpha = 1.0f;
        if (this.paint != null) {
            this.paint.setAlpha(255);
        }
        onDraw(tmpProc);
        this.alpha = alphaTemp;
        Bitmap res = Bitmap.createBitmap(face);
        face.recycle();
        setCacheDirty(false);
        return res;
    }

    @SuppressLint({"WrongCall"})
    public Bitmap getSnapshot(float scale) {
        int width = (int) this.localRect.width();
        int height = (int) this.localRect.height();
        if (width <= 0 || height <= 0) {
            return null;
        }
        Bitmap face = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        NormalDisplayProcess tmpProc = new NormalDisplayProcess();
        tmpProc.beginDisplay(face);
        this.paint.setAlpha((int) (255.0f * this.alpha));
        if (this.isBackgroundVisible && this.mBGDrawable != null) {
            this.mBGDrawable.setBounds(0, 0, (int) this.localRect.width(), (int) this.localRect.height());
            this.mBGDrawable.draw(tmpProc.getCanvas());
        }
        onDraw(tmpProc);
        if (((double) scale) <= 0.9999d || ((double) scale) >= 1.0001d) {
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap res = Bitmap.createBitmap(face, 0, 0, width, height, matrix, true);
            tmpProc.getCanvas().setBitmap(null);
            tmpProc.endDisplay();
            face.recycle();
            return res;
        }
        tmpProc.getCanvas().setBitmap(null);
        tmpProc.endDisplay();
        return face;
    }

    public void onDraw(IDisplayProcess c) {
    }

    @SuppressLint({"WrongCall"})
    public void draw(IDisplayProcess c) {
        if (isVisible()) {
            updateMatrix();
            if (this.enableCache) {
                if (this.faceToDraw == null) {
                    this.faceToDraw = createFace();
                } else if (isCacheDirty()) {
                    if (!this.faceToDraw.isRecycled()) {
                        this.faceToDraw.recycle();
                        this.faceToDraw = null;
                    }
                    this.faceToDraw = createFace();
                }
                if (this.faceToDraw != null && this.matrix != null) {
                    c.save();
                    updateFinalAlpha();
                    if (this.isBackgroundVisible && this.mBGDrawable != null) {
                        c.drawDrawable(this.mBGDrawable, this.drawableBGRect);
                    }
                    c.translate(this.localRect.left, this.localRect.top);
                    if (this.mEnableHalo) {
                        c.drawCircle(this.mHaloCenterX, this.mHaloCenterY, this.mHaloRadius, this.mHaloPaint);
                    }
                    c.drawBitmap(this.faceToDraw, 0.0f, 0.0f, this.paint);
                    c.restore();
                    return;
                }
                return;
            }
            c.save();
            if (this.matrix != null) {
                c.concat(this.matrix);
            }
            updateFinalAlpha();
            if (this.mBGDrawable != null) {
                c.drawDrawable(this.mBGDrawable, this.drawableBGRect);
            }
            c.translate(this.localRect.left, this.localRect.top);
            if (this.mEnableHalo) {
                c.drawCircle(this.mHaloCenterX, this.mHaloCenterY, this.mHaloRadius, this.mHaloPaint);
            }
            onDraw(c);
            c.restore();
        }
    }

    public boolean isRecycled() {
        return this.isRecycled;
    }

    public void clean() {
        this.isRecycled = true;
        this.parent = null;
    }

    public void destory() {
        clean();
    }

    public void reuse() {
        this.isRecycled = false;
    }

    public void setAlpha(float alpha) {
        if (alpha >= 0.0f && alpha <= 1.0001f) {
            this.alpha = alpha;
        }
    }

    public float getAlpha() {
        return this.alpha;
    }

    public void setExtraAlphaEnable(boolean enable) {
        this.enableExtraAlpha = enable;
    }

    public void setExtraAlpha(float extraAlpha) {
        this.extraAlpha = extraAlpha;
    }

    public float getParentAlpha() {
        return this.parent == null ? 1.0f : this.parent.getFinalAlpha();
    }

    public void updateFinalAlpha() {
        if (this.enableExtraAlpha) {
            this.finalAlpha = (getParentAlpha() * this.alpha) * this.extraAlpha;
        } else {
            this.finalAlpha = getParentAlpha() * this.alpha;
        }
        int newAlpha = (int) (255.0f * this.finalAlpha);
        if (this.finalAlphaInt != newAlpha || this.bgAlphaDirty) {
            this.finalAlphaInt = newAlpha;
            if (this.mBGDrawable != null) {
                this.mBGDrawable.setAlpha(this.finalAlphaInt);
            }
        }
        this.paint.setAlpha(this.finalAlphaInt);
    }

    public float getFinalAlpha() {
        return this.finalAlpha;
    }

    public boolean onDown(MotionEvent e) {
        return !isTouchable() ? false : false;
    }

    public boolean onShowPress(MotionEvent e) {
        return !isTouchable() ? false : false;
    }

    public boolean onSingleTapUp(MotionEvent e) {
        if (isTouchable()) {
            invokeOnClickListener();
        }
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
        return !isTouchable() ? false : false;
    }

    public boolean onLongPress(MotionEvent e) {
        if (isTouchable()) {
            return invokeOnLongClickListener();
        }
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return !isTouchable() ? false : false;
    }

    public boolean onFingerUp(MotionEvent e) {
        return !isTouchable() ? false : false;
    }

    public void onTouchCancel(MotionEvent e) {
        if (!isTouchable()) {
        }
    }

    public boolean onDoubleTapped(MotionEvent e, DrawableItem item) {
        return false;
    }

    public void invalidate() {
        invalidate(false);
    }

    public void invalidate(boolean cached) {
        if (this.enableCache && cached) {
            setCacheDirty(true);
        }
        if (this.mContext != null) {
            this.mContext.getRenderer().invalidate();
        }
    }

    public void invalidateAtOnce() {
        if (this.enableCache) {
            setCacheDirty(true);
        }
        this.mContext.getRenderer().invalidateAtOnce();
    }

    public void interruptLongPressed() {
        this.mContext.getExchangee().interruptLongPress();
    }

    public static void echo(String msg) {
        Log.d(TAG, msg);
    }

    public void setRotationX(float rotationX) {
        if (this.mRotationX != rotationX) {
            this.mRotationX = rotationX;
            this.mMatrixDirty = true;
        }
    }

    public void setRotationY(float rotationY) {
        if (this.mRotationY != rotationY) {
            this.mRotationY = rotationY;
            this.mMatrixDirty = true;
        }
    }

    public void setRotation(float rotation) {
        if (this.mRotation != rotation) {
            this.mRotation = rotation;
            this.mMatrixDirty = true;
        }
    }

    public void setScaleX(float scaleX) {
        if (this.mScaleX != scaleX) {
            this.mScaleX = scaleX;
            this.mMatrixDirty = true;
        }
    }

    public void setScaleY(float scaleY) {
        if (this.mScaleY != scaleY) {
            this.mScaleY = scaleY;
            this.mMatrixDirty = true;
        }
    }

    public void setTranslationX(float translationX) {
        if (this.mTranslationX != translationX) {
            this.mTranslationX = translationX;
            this.mMatrixDirty = true;
        }
    }

    public void setTranslationY(float translationY) {
        if (this.mTranslationY != translationY) {
            this.mTranslationY = translationY;
            this.mMatrixDirty = true;
        }
    }

    public void setPivot(float pivotX, float pivotY) {
        this.mPivotX = pivotX;
        this.mPivotY = pivotY;
        this.mPivotSet = true;
        this.mMatrixDirty = true;
    }

    public void updateMatrix() {
        if (this.mMatrixDirty) {
            if (!this.mPivotSet) {
                this.mPivotX = (this.localRect.right - this.localRect.left) / 2.0f;
                this.mPivotY = (this.localRect.bottom - this.localRect.top) / 2.0f;
                this.mPivotSet = true;
            }
            this.matrix.reset();
            if (nonzero(this.mRotationX) || nonzero(this.mRotationY)) {
                if (this.mCamera == null) {
                    this.mCamera = new Camera();
                    this.matrix3D = new Matrix();
                }
                this.mCamera.save();
                this.matrix3D.reset();
                this.matrix.preScale(this.mScaleX, this.mScaleY, this.mPivotX, this.mPivotY);
                this.mCamera.rotate(this.mRotationX, this.mRotationY, -this.mRotation);
                this.mCamera.getMatrix(this.matrix3D);
                this.matrix3D.preTranslate(-this.mPivotX, -this.mPivotY);
                this.matrix3D.postTranslate(this.mPivotX + this.mTranslationX, this.mPivotY + this.mTranslationY);
                this.matrix.postConcat(this.matrix3D);
                this.mCamera.restore();
            } else {
                this.matrix.setTranslate(this.mTranslationX, this.mTranslationY);
                this.matrix.preRotate(this.mRotation, this.mPivotX, this.mPivotY);
                this.matrix.preScale(this.mScaleX, this.mScaleY, this.mPivotX, this.mPivotY);
            }
            this.mMatrixDirty = false;
            invalidate(false);
        }
    }

    private static boolean nonzero(float value) {
        return value < -0.001f || value > NONZERO_EPSILON;
    }

    public void setPadding(int left, int top, int right, int bottom) {
        boolean changed = false;
        if (this.mPaddingLeft != left) {
            changed = true;
            this.mPaddingLeft = left;
        }
        if (this.mPaddingTop != top) {
            changed = true;
            this.mPaddingTop = top;
        }
        if (this.mPaddingRight != right) {
            changed = true;
            this.mPaddingRight = right;
        }
        if (this.mPaddingBottom != bottom) {
            changed = true;
            this.mPaddingBottom = bottom;
        }
        if (changed) {
            invalidate();
        }
    }

    public Drawable getBackgroundDrawable() {
        return this.mBGDrawable;
    }

    public void setBackgroundDrawable(Drawable d) {
        setBackgroundDrawable(d, new RectF(0.0f, 0.0f, this.localRect.width(), this.localRect.height()));
    }

    public void setBackgroundDrawable(Drawable d, RectF r) {
        if (d != this.mBGDrawable) {
            if (this.drawableBGRect == null) {
                this.drawableBGRect = new RectF(r);
            }
            if (this.mBGDrawable != null) {
                this.mBGDrawable.setCallback(null);
            }
            if (d != null) {
                Rect padding = (Rect) sThreadLocal.get();
                if (padding == null) {
                    padding = new Rect();
                    sThreadLocal.set(padding);
                }
                if (d.getPadding(padding)) {
                    setPadding(padding.left, padding.top, padding.right, padding.bottom);
                }
                this.mBGDrawable = d;
                this.mBGDrawable.setAlpha(this.finalAlphaInt);
            } else {
                this.mBGDrawable = null;
            }
            invalidate();
        }
    }

    public void setBackgroundVisible(boolean visible) {
        this.isBackgroundVisible = visible;
    }

    public boolean isBackgroundVisible() {
        return this.isBackgroundVisible;
    }

    public void setTag(Object o) {
        this.mTag = o;
    }

    public Object getTag() {
        return this.mTag;
    }

    public void resize(RectF rect) {
        if (rect != null && rect.width() > 0.0f && rect.height() > 0.0f) {
            this.localRect.set(rect);
            if (this.mEnableHaloFeature) {
                this.mHaloCenterX = this.localRect.width() * 0.5f;
                this.mHaloCenterY = this.localRect.height() * 0.5f;
                setHaloRadius(Math.max(this.mHaloCenterX, this.mHaloCenterY));
            }
            setInvertMatrixDirty();
            invalidate();
        }
    }

    public OnClickListener getOnClickListener() {
        return this.mOnClickListener;
    }

    public void setOnClickListener(OnClickListener l) {
        this.mOnClickListener = l;
    }

    private void invokeOnClickListener() {
        if (this.mOnClickListener != null) {
            this.mOnClickListener.onClick(this);
        }
    }

    public void setOnVisibilityChangeListener(OnVisibilityChangeListener listener) {
        this.mVisibilityListener = listener;
    }

    public OnVisibilityChangeListener getOnVisibilityChangeListener() {
        return this.mVisibilityListener;
    }

    public void setOnLongClickListener(OnLongClickListener l) {
        this.mOnLongClickListener = l;
    }

    private boolean invokeOnLongClickListener() {
        if (this.mOnLongClickListener != null) {
            return this.mOnLongClickListener.onLongClick(this);
        }
        return false;
    }

    public boolean onFingerCancel(MotionEvent e) {
        return !isTouchable() ? false : false;
    }

    public void registerIController(IController controller) {
        if (this.mContext != null && this.mContext.getExchangee() != null && this.mContext.getExchangee().getDrawingPass() != null) {
            this.mContext.getExchangee().getDrawingPass().registerIController(controller);
        }
    }

    public void unregisterIController(IController controller) {
        if (this.mContext != null && this.mContext.getExchangee() != null && this.mContext.getExchangee().getDrawingPass() != null) {
            this.mContext.getExchangee().getDrawingPass().unregisterIController(controller);
        }
    }

    public String dumpLayoutInfo() {
        StringBuffer toprint = new StringBuffer();
        toprint.append(" l = ").append(this.localRect.left).append(" t = ").append(this.localRect.top).append(" w = ").append(this.localRect.width()).append(" h = ").append(this.localRect.height());
        return toprint.toString();
    }

    public String dumpMatrix() {
        StringBuffer rt = new StringBuffer("\n");
        StringBuffer dump = new StringBuffer("dumpMatrix {");
        for (DrawableItem target = this; target != null; target = target.getParent()) {
            dump.append(rt).append(rt).append(target.getClass().getSimpleName());
            dump.append(rt).append("xy( ").append(target.getRelativeX()).append(", ").append(target.getRelativeY()).append(" )");
            dump.append(rt).append("width = ").append(target.getWidth()).append(" height = ").append(target.getHeight());
            dump.append(rt).append("matrix = ").append(target.matrix);
            rt.append("  ");
        }
        dump.append("\n}");
        return dump.toString();
    }

    public void setDebugColor(String colorStr) {
        int i = this.mDebugColor;
        try {
            this.mDebugColor = Color.parseColor(colorStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setName(String name) {
        this.mName = String.valueOf(name);
    }

    public String name() {
        return this.mName;
    }

    public void setEnableHaloFeature(boolean enable) {
        this.mEnableHaloFeature = enable;
        if (!this.localRect.isEmpty()) {
            this.mHaloCenterX = getWidth() * 0.5f;
            this.mHaloCenterY = getHeight() * 0.5f;
            setHaloRadius(Math.max(this.mHaloCenterX, this.mHaloCenterY));
        }
    }

    public void setHolaScaleRange(float start, float end, float loading) {
        this.mHaloScaleStart = start;
        this.mHaloScaleEnd = end;
        this.mHaloScaleLoading = loading;
        this.mHaloRadiusStart = this.mHaloRadius * this.mHaloScaleStart;
        this.mHaloRadiusEnd = this.mHaloRadius * this.mHaloScaleEnd;
        this.mHaloRadiiusLoading = this.mHaloRadius * this.mHaloScaleLoading;
    }

    public void setHaloRadius(float radius) {
        this.mHaloRadius = radius;
        this.mHaloRadiusStart = this.mHaloRadius * this.mHaloScaleStart;
        this.mHaloRadiusEnd = this.mHaloRadius * this.mHaloScaleEnd;
        this.mHaloRadiiusLoading = this.mHaloRadius * this.mHaloScaleLoading;
    }

    private void initLoadingPaint() {
        this.mHaloPaint = new Paint(1);
        this.mHaloPaint.setStyle(Style.STROKE);
        this.mHaloPaint.setStrokeWidth(2.0f);
        this.f7m = new Matrix();
        this.mShader = new SweepGradient(this.mHaloCenterX, this.mHaloCenterY, new int[]{16777215, 16777215, -1}, null);
        this.mHaloPaint.setShader(this.mShader);
    }

    public void startLoadingAnimation() {
        if (!this.mEnableHalo && this.mEnableHaloFeature) {
            initLoadingPaint();
            this.mEnableHalo = true;
            this.mHaloRadius = this.mHaloRadiiusLoading;
            this.mHaloAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mHaloAnim.setInterpolator(new LinearInterpolator());
            this.mHaloAnim.setDuration(1000);
            this.mHaloAnim.setRepeatCount(-1);
            this.mHaloAnim.addUpdateListener(new C02821());
            this.mHaloAnim.start();
        }
    }

    public void cancelScaleAnimation() {
        if (this.mScaleAnim != null && this.mScaleAnim.isStarted()) {
            this.mScaleAnim.cancel();
        }
        this.mScaleAnim = null;
    }

    public void cancelHaloAnimation() {
        this.mEnableHalo = false;
        if (this.mHaloAnim != null && this.mHaloAnim.isStarted()) {
            this.mHaloAnim.cancel();
        }
        this.mHaloAnim = null;
    }

    private void initHaloPaint() {
        this.mHaloPaint = new Paint(1);
        this.mHaloPaint.setStyle(Style.STROKE);
        this.mHaloPaint.setColor(-1);
        this.mHaloPaint.setStrokeWidth(2.0f);
        this.mHaloPaint.setAlpha(HALO_ALPHA_START);
    }

    public void startHaloClickAnimation() {
        startHaloClickAnimation(null);
    }

    public void startHaloClickAnimation(Runnable onEndAction) {
        startHaloClickAnimation(false, onEndAction);
    }

    public void startHaloClickAnimation(final boolean loadingAfter, final Runnable onEndAction) {
        if (this.mEnableHaloFeature) {
            cancelHaloAnimation();
            cancelScaleAnimation();
            initHaloPaint();
            this.mHaloRadius = this.mHaloRadiusStart;
            this.mHaloAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mHaloAnim.setDuration(330);
            this.mHaloAnim.setInterpolator(QUAD_OUT);
            this.mHaloAnim.addUpdateListener(new C02832());
            this.mHaloAnim.addListener(new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animation) {
                    DrawableItem.this.mEnableHalo = true;
                }

                public void onAnimationEnd(Animator animation) {
                    DrawableItem.this.mEnableHalo = false;
                    if (onEndAction != null) {
                        onEndAction.run();
                    }
                    if (loadingAfter) {
                        DrawableItem.this.startLoadingAnimation();
                    }
                }
            });
            this.mScaleAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mScaleAnim.setDuration(125);
            this.mScaleAnim.setInterpolator(QUAD_OUT);
            this.mScaleAnim.setRepeatMode(2);
            this.mScaleAnim.setRepeatCount(1);
            this.mScaleAnim.addUpdateListener(new C02854());
            this.mScaleAnim.addListener(new C02865());
            if (this.mScaleAnim != null) {
                this.mScaleAnim.start();
            }
        } else if (onEndAction != null) {
            onEndAction.run();
        }
    }
}
