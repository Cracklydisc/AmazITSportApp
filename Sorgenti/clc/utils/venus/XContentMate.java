package clc.utils.venus;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.view.View;
import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Pattern;

public class XContentMate {
    private static final String[] MODEL_SENIOR = new String[]{"K900", "SCH-N719"};
    public static PorterDuffXfermode duffXfermode = new PorterDuffXfermode(Mode.CLEAR);
    public static boolean mBuildVersionUp403 = isUp403();
    public static PaintFlagsDrawFilter paintFilter = new PaintFlagsDrawFilter(0, 3);
    boolean animatorJustUpdated = false;
    private ExchangeManager exchangee;
    private long gap = 0;
    private long lastRequestTime = 0;
    private boolean mDataReady = false;
    IDisplayProcess mDisProc = null;
    private String mName = "NULL";
    private boolean mNeedDattach;
    private boolean mSeniorDevice;
    private XViewContent mTarget;
    ConcurrentLinkedQueue<Runnable> runnableList;
    boolean sRenderHasStarted = false;
    private byte sState = (byte) 1;

    class C02891 implements AnimatorUpdateListener {
        final /* synthetic */ XContentMate this$0;

        public void onAnimationUpdate(ValueAnimator animation) {
            this.this$0.invalidate();
        }
    }

    class C02902 implements AnimatorListener {
        final /* synthetic */ XContentMate this$0;

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            this.this$0.invalidate();
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    private final class AnimationThread extends HandlerThread {
    }

    final class EventHandler extends HandlerThread {
        private Handler mHandler;
        private Handler mHandler2;

        public void run() {
            Process.setThreadPriority(-4);
            Looper.prepare();
            this.mHandler = new Handler(Looper.myLooper()) {
            };
            this.mHandler2 = new Handler(Looper.myLooper()) {
            };
            Looper.loop();
        }
    }

    public final class RenderMode {
    }

    public final class RenderState {
    }

    private static boolean isUp403() {
        int coreCount;
        try {
            coreCount = new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    if (Pattern.matches("cpu[0-9]", pathname.getName())) {
                        return true;
                    }
                    return false;
                }
            }).length;
        } catch (Exception e) {
            coreCount = 1;
        }
        if (coreCount == 1) {
            return false;
        }
        if (VERSION.SDK_INT > 15) {
            return true;
        }
        if (VERSION.SDK_INT != 15) {
            return false;
        }
        if (VERSION.RELEASE.compareToIgnoreCase("4.0.3") > 0 || coreCount > 1) {
            return true;
        }
        return false;
    }

    public void setRenderTarget(View targetHolder) {
        if (this.mTarget != null) {
            this.mTarget = null;
        }
        if (targetHolder != null) {
            this.mTarget = (XViewContent) targetHolder;
            invalidate();
        }
    }

    public XContentMate(String name) {
        String model = Build.MODEL;
        Log.i("Test", "device model ==" + model);
        this.mSeniorDevice = false;
        for (CharSequence contains : MODEL_SENIOR) {
            if (model.contains(contains)) {
                this.mSeniorDevice = true;
                break;
            }
        }
        this.mName = name;
        init();
    }

    public synchronized void start() {
        if (!this.sRenderHasStarted) {
            run();
            this.sRenderHasStarted = true;
        }
    }

    private void init() {
        this.mNeedDattach = false;
        this.runnableList = new ConcurrentLinkedQueue();
    }

    public void setExchangee(ExchangeManager exchangeManager) {
        this.exchangee = exchangeManager;
    }

    public void clearAndDettach() {
        this.mNeedDattach = true;
        invalidate();
        this.sRenderHasStarted = false;
        this.mTarget = null;
        this.exchangee = null;
        this.runnableList.clear();
    }

    public void invalidate() {
        if (this.mTarget != null) {
            this.mTarget.requestInvalidate();
        }
    }

    public void invalidateImmediately() {
        if (this.mTarget != null) {
            this.mTarget.invalidateInmediately();
        }
    }

    public void invalidateAtOnce() {
        if (this.mTarget != null) {
            this.mTarget.invalidate();
        }
    }

    public ExchangeManager getExchangee() {
        return this.exchangee;
    }

    public void run() {
    }
}
