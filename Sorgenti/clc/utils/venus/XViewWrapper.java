package clc.utils.venus;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import clc.utils.stuff.SlideHelper;
import clc.utils.stuff.SlidingOriAnalysis.SlideOrient;
import clc.utils.stuff.Utils;

public class XViewWrapper extends FrameLayout {
    private boolean hasInited;
    private int mActivePointerId;
    private XViewContent mContentView;
    public int mCurrenDirection;
    private float mInitialTouchTime;
    private float mInitialTouchX;
    private float mInitialTouchY;
    private float mLastTouchX;
    private float mLastTouchY;
    private SlideOrient mNeedReleaseSlideOri;
    private boolean mNeedReleaseTouchEventToParent;
    public boolean mParentNeedTouchEvent;
    private int mReleaseOrit;
    private SlideHelper mSlider;

    class C02941 implements Runnable {
        final /* synthetic */ XViewWrapper this$0;

        public void run() {
            this.this$0.bringChildToFront(this.this$0.mContentView);
            this.this$0.requestLayout();
        }
    }

    public XViewWrapper(Context context) {
        this(context, null);
    }

    public XViewWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.hasInited = false;
        this.mNeedReleaseSlideOri = SlideOrient.NOT_SECIFIED;
        this.mCurrenDirection = 0;
        this.mReleaseOrit = 14;
        this.mParentNeedTouchEvent = false;
        init();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    public void init() {
        setLongClickable(false);
        setHapticFeedbackEnabled(false);
        if (this.mContentView == null) {
            this.mContentView = new XViewContent(this);
            this.mContentView.setLongClickable(false);
            this.mContentView.setHapticFeedbackEnabled(false);
            addView(this.mContentView, new LayoutParams(-1, -1));
            requestLayout();
            this.mSlider = new SlideHelper(getContext());
            this.hasInited = true;
        }
    }

    public ExchangeManager getExchangee() {
        checkInitState();
        return this.mContentView.getExchangee();
    }

    public XContentMate getRenderer() {
        checkInitState();
        return this.mContentView.getRenderer();
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        checkInitState();
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void checkInitState() {
        if (!this.hasInited) {
            throw new RuntimeException("NOT. INIT.");
        }
    }

    public boolean hasReleaseTouchEventToParent() {
        return this.mNeedReleaseTouchEventToParent;
    }

    private boolean handlePointerDown(MotionEvent motionevent) {
        this.mActivePointerId = motionevent.getPointerId(0);
        int i = motionevent.findPointerIndex(this.mActivePointerId);
        this.mInitialTouchX = motionevent.getX(i);
        this.mInitialTouchY = motionevent.getY(i);
        this.mLastTouchX = motionevent.getX(i);
        this.mLastTouchY = motionevent.getY(i);
        this.mInitialTouchTime = (float) motionevent.getEventTime();
        this.mCurrenDirection = 10;
        return false;
    }

    private boolean handlePointerMove(MotionEvent motionevent) {
        int pindex = motionevent.findPointerIndex(this.mActivePointerId);
        if (pindex == -1) {
            return false;
        }
        float pointy = motionevent.getY(pindex);
        float pointx = motionevent.getX(pindex);
        if (pointy == this.mLastTouchY && pointx == this.mLastTouchX) {
            return false;
        }
        float pointy_distance = pointy - this.mLastTouchY;
        this.mLastTouchX = pointx;
        this.mLastTouchY = pointy;
        if (this.mCurrenDirection == 10) {
            this.mCurrenDirection = Utils.cacTouchGesture(this.mInitialTouchX, this.mInitialTouchY, pointx, pointy, this.mInitialTouchTime, (float) motionevent.getEventTime());
        }
        return true;
    }

    public void setReleaseOrientationWhileInteractiveWithParent(int orient) {
        this.mReleaseOrit = orient;
    }

    public void setParentNeedTouchEvent(boolean need) {
        this.mParentNeedTouchEvent = need;
    }

    public boolean dispatchTouchEvent(MotionEvent e) {
        boolean isTouch;
        switch (e.getAction() & 255) {
            case 0:
                isTouch = handlePointerDown(e);
                break;
            case 1:
            case 3:
                break;
            case 2:
                isTouch = handlePointerMove(e);
                break;
            default:
                break;
        }
        if (this.mParentNeedTouchEvent) {
            ((ViewGroup) getParent()).requestDisallowInterceptTouchEvent(false);
        } else {
            boolean change = this.mNeedReleaseTouchEventToParent;
            if (this.mCurrenDirection == this.mReleaseOrit) {
                this.mNeedReleaseTouchEventToParent = true;
            } else {
                this.mNeedReleaseTouchEventToParent = false;
            }
            ViewGroup parent = (ViewGroup) getParent();
            if (parent != null) {
                if (hasReleaseTouchEventToParent()) {
                    parent.requestDisallowInterceptTouchEvent(false);
                } else {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
            }
        }
        return super.dispatchTouchEvent(e);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return !this.mParentNeedTouchEvent && super.onInterceptTouchEvent(ev);
    }

    public void addDrawingItem(DrawableItem itemToAdd) {
        if (itemToAdd == null) {
            throw new NullPointerException();
        }
        this.mContentView.getExchangee().getDrawingPass().addDrawingItem(itemToAdd);
        this.mContentView.getRenderer().invalidate();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public XViewContent getContentView() {
        return this.mContentView;
    }
}
