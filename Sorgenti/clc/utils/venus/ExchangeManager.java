package clc.utils.venus;

import android.graphics.Matrix;
import android.view.MotionEvent;
import clc.utils.venus.LGestureDetector.OnDoubleTapListener;
import clc.utils.venus.LGestureDetector.OnGestureListener;
import java.util.ArrayList;
import java.util.Iterator;

public class ExchangeManager implements OnDoubleTapListener, OnGestureListener {
    int currPointer = 0;
    MotionEvent[] eventsQueque = new MotionEvent[15];
    private LGestureDetector gestureDetector;
    private int lastAction = -1;
    private DrawableItem lastTouchedItem = null;
    private XViewWrapper mContext;
    private ArrayList<DrawableItem> mItemsListHited;
    private float[] mTouchPoints = new float[2];
    private DrawingItemsPump pumper = new DrawingItemsPump();

    public ExchangeManager(XViewWrapper context) {
        init(context);
    }

    public void init(XViewWrapper context) {
        this.mContext = context;
        this.gestureDetector = new LGestureDetector(this.mContext.getContext(), this);
        this.mItemsListHited = new ArrayList();
    }

    public void updateItem(long timeDelta) {
        Iterator it = this.pumper.getControllers().iterator();
        while (it.hasNext()) {
            IController controller = (IController) it.next();
            if (controller != null) {
                controller.update(timeDelta);
            }
        }
    }

    public void draw(IDisplayProcess disProc) {
        Iterator it = this.pumper.offerAndLockDrawing().iterator();
        while (it.hasNext()) {
            ((DrawableItem) it.next()).draw(disProc);
        }
        this.pumper.unLockDrawing();
    }

    public void onTouchEvent(MotionEvent event) {
        if (event != null) {
            if (event.getAction() == 1 && this.lastAction == 1) {
                this.lastAction = -1;
            } else if (event.getAction() == 0 && this.lastAction == 0) {
                this.lastAction = -1;
            } else {
                this.lastAction = event.getAction();
                this.gestureDetector.onTouchEvent(MotionEvent.obtain(event));
                if (event.getAction() == 1) {
                    onFingerUp(event);
                    this.gestureDetector.setIsLongpressEnabled(true);
                } else if (event.getAction() == 3) {
                    onFingerCancel(event);
                    this.gestureDetector.setIsLongpressEnabled(true);
                }
            }
        }
    }

    public boolean onDown(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onDown(e)) {
                this.lastTouchedItem = item;
                return true;
            }
        }
        return false;
    }

    public void onShowPress(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable()) {
                this.lastTouchedItem = item;
                item.onShowPress(e);
            }
        }
    }

    public boolean onSingleTapUp(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onSingleTapUp(e)) {
                if (item.getXContext() != null) {
                    item.getXContext().playSoundEffect(0);
                }
                this.lastTouchedItem = item;
                return true;
            }
        }
        return false;
    }

    public void onTouchCancel(MotionEvent e) {
        if (this.lastTouchedItem != null) {
            this.lastTouchedItem.onTouchCancel(e);
        }
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, float previousX, float previousY) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e2);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onScroll(e1, e2, distanceX, distanceY, previousX, previousY)) {
                this.lastTouchedItem = item;
                return true;
            }
        }
        return false;
    }

    public void onLongPress(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable()) {
                item.onLongPress(e);
            }
        }
    }

    public void interruptLongPress() {
        this.gestureDetector.cancelLongPress();
    }

    private ArrayList<DrawableItem> filterDesireTouchEvent(ArrayList<DrawableItem> itemListToFilter) {
        if (itemListToFilter == null || itemListToFilter.isEmpty()) {
            return itemListToFilter;
        }
        ArrayList<DrawableItem> newList = new ArrayList();
        Iterator it = itemListToFilter.iterator();
        while (it.hasNext()) {
            DrawableItem item = (DrawableItem) it.next();
            if (item.isDesiredTouchEventItem()) {
                newList.add(item);
                return newList;
            }
        }
        return itemListToFilter;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        ArrayList<DrawableItem> itemS = filterDesireTouchEvent(checkHitedItem(e2));
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onFling(e1, e2, velocityX, velocityY)) {
                this.lastTouchedItem = item;
                return true;
            }
        }
        return false;
    }

    private ArrayList<DrawableItem> checkHitedItem(MotionEvent e) {
        this.mItemsListHited.clear();
        if (e == null) {
            return this.mItemsListHited;
        }
        Iterator<DrawableItem> it = this.pumper.offerAndLockDrawing().iterator();
        while (it.hasNext()) {
            DrawableItem item = (DrawableItem) it.next();
            if (checkHited(item, e.getX(), e.getY())) {
                this.mItemsListHited.add(item);
            }
        }
        return this.mItemsListHited;
    }

    public boolean checkHited(DrawableItem item, float x, float y) {
        if (item == null || !item.isVisible() || !item.isTouchable()) {
            return false;
        }
        this.mTouchPoints[0] = x;
        this.mTouchPoints[1] = y;
        Matrix resMatrix = item.getInvertMatrix();
        if (resMatrix == null) {
            return false;
        }
        resMatrix.mapPoints(this.mTouchPoints);
        if (!item.hasExtraTouchBounds()) {
            return item.localRect.contains(this.mTouchPoints[0], this.mTouchPoints[1]);
        }
        boolean resLocal = item.localRect.contains(this.mTouchPoints[0], this.mTouchPoints[1]);
        boolean resExtra = item.getExtraTouchBounds().contains(this.mTouchPoints[0], this.mTouchPoints[1]);
        if (resLocal || resExtra) {
            return true;
        }
        return false;
    }

    public boolean onFingerUp(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        if (itemS.size() >= 1) {
            int i = itemS.size() - 1;
            while (i > -1) {
                DrawableItem item = (DrawableItem) itemS.get(i);
                if (item == null || !item.isVisible() || !item.isTouchable() || !item.onFingerUp(e)) {
                    i--;
                } else if (this.lastTouchedItem == item) {
                    return true;
                } else {
                    if (this.lastTouchedItem != null) {
                        this.lastTouchedItem.onTouchCancel(e);
                    }
                    this.lastTouchedItem = item;
                    return true;
                }
            }
            if (this.lastTouchedItem != null) {
                this.lastTouchedItem.onTouchCancel(e);
                this.lastTouchedItem = null;
            }
        } else if (this.lastTouchedItem != null) {
            this.lastTouchedItem.onTouchCancel(e);
            this.lastTouchedItem = null;
        }
        return false;
    }

    public void clear() {
        synchronized (this.pumper) {
            this.pumper.destroy();
        }
    }

    private void onFingerCancel(MotionEvent e) {
        if (this.lastTouchedItem != null) {
            this.lastTouchedItem.onFingerCancel(e);
        }
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item != this.lastTouchedItem) {
                item.onFingerCancel(e);
            }
        }
        this.lastTouchedItem = null;
    }

    public DrawingItemsPump getDrawingPass() {
        return this.pumper;
    }

    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    public boolean onDoubleTap(MotionEvent e) {
        ArrayList<DrawableItem> itemS = checkHitedItem(e);
        for (int i = itemS.size() - 1; i > -1; i--) {
            DrawableItem item = (DrawableItem) itemS.get(i);
            if (item != null && item.isVisible() && item.isTouchable() && item.onDoubleTapped(e, item)) {
                this.lastTouchedItem = item;
                return true;
            }
        }
        return false;
    }

    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }
}
