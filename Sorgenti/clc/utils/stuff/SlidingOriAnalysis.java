package clc.utils.stuff;

public final class SlidingOriAnalysis {

    public static class MotionInfo {
        public SlideOrient newOri;
        public SlideOrient oldOri;
    }

    public enum SlideOrient {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NOT_SECIFIED
    }
}
