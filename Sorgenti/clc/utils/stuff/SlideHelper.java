package clc.utils.stuff;

import android.content.Context;
import android.view.VelocityTracker;
import clc.utils.stuff.SlidingOriAnalysis.MotionInfo;
import clc.utils.stuff.SlidingOriAnalysis.SlideOrient;

public class SlideHelper {
    private SlideMode mCurrSlideMode = SlideMode.STANDBY;
    private float mCurrVelocityX = 0.0f;
    private float mCurrVelocityY = 0.0f;
    private boolean mOnceTouchCycle = false;
    private MotionInfo mSlidingMotionInfo;
    private VelocityTracker mVelocityTracker = null;

    public enum SlideMode {
        HORIZIONAL,
        VERTICAL,
        STANDBY
    }

    public SlideHelper(Context context) {
        init();
    }

    private void init() {
        this.mSlidingMotionInfo = new MotionInfo();
        this.mSlidingMotionInfo.newOri = SlideOrient.NOT_SECIFIED;
        this.mSlidingMotionInfo.oldOri = SlideOrient.NOT_SECIFIED;
    }
}
