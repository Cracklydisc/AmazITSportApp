package clc.utils.stuff;

public final class Validator {
    public static void notNull(Object obj, String msg) {
        if (obj == null) {
            StringBuilder stringBuilder = new StringBuilder("Validator check: NULL. ");
            if (msg == null) {
                msg = "";
            }
            throw new NullPointerException(stringBuilder.append(msg).toString());
        }
    }
}
