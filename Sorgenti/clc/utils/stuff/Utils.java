package clc.utils.stuff;

public class Utils {
    public static int cacTouchGesture(float sx, float sy, float ex, float ey, float stime, float etime) {
        float x = ex - sx;
        float y = ey - sy;
        float time_dis = etime - stime;
        if ((x * x) + (y * y) <= 200.0f) {
            return 10;
        }
        if (Math.abs(x) >= Math.abs(y)) {
            if (x > 0.0f) {
                return 14;
            }
            if (x <= 0.0f) {
                return 13;
            }
            return 10;
        } else if (y > 0.0f) {
            return 12;
        } else {
            if (y <= 0.0f) {
                return 11;
            }
            return 10;
        }
    }
}
