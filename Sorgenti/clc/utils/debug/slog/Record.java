package clc.utils.debug.slog;

public class Record {
    public String content = "";
    public String tag = "";
    public String timeStamp = "";

    public String toString() {
        return "[SLOG] At [" + this.timeStamp + "] tagged [" + this.tag + "]" + " with [" + this.content + "]";
    }
}
