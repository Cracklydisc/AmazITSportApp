package clc.utils.debug.slog;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SlogFile {
    private static SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.SSS");
    private static Handler sWorkHandler = new C02701(sWorkerThread.getLooper());
    private static HandlerThread sWorkerThread = new HandlerThread("slog-file", 10);
    private String mCurrFilePathAndName = "/sdcard/.springchannel/slog";
    private ConcurrentLinkedQueue<Record> mRecordQueue = new ConcurrentLinkedQueue();

    class C02701 extends Handler {
        C02701(Looper $anonymous0) {
            super($anonymous0);
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    class C02723 implements Runnable {
        C02723() {
        }

        public void run() {
            SlogFile.this.checkAndBackup();
        }
    }

    class C02745 implements Comparator<File> {
        C02745() {
        }

        public int compare(File lhs, File rhs) {
            if (lhs.lastModified() < rhs.lastModified()) {
                return -1;
            }
            return 0;
        }
    }

    static {
        sWorkerThread.start();
    }

    public SlogFile(String filePathAndName) {
        if (!(TextUtils.isEmpty(filePathAndName) || TextUtils.equals(this.mCurrFilePathAndName, filePathAndName))) {
            this.mCurrFilePathAndName = filePathAndName;
            if (new File(this.mCurrFilePathAndName).exists()) {
                Log.i("slog", "已经有文件存在于: " + this.mCurrFilePathAndName + " , 旧文件可能被覆盖或删除呢.");
            }
        }
        String parentDir = this.mCurrFilePathAndName.substring(0, this.mCurrFilePathAndName.lastIndexOf(File.separator));
        File dir = new File(parentDir);
        final File f = new File(this.mCurrFilePathAndName);
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("slog", "文件夹: " + parentDir + " 无法创建呢.");
            }
        }
        if (f.exists()) {
            checkAndBackup();
        } else {
            sWorkHandler.post(new Runnable() {
                public void run() {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i("slog", "文件: " + SlogFile.this.mCurrFilePathAndName + " 无法创建呢.");
                    }
                }
            });
        }
    }

    void addRecord(Record r) {
        this.mRecordQueue.add(r);
        if (checkIfToFile()) {
            forceFlush(new C02723());
        }
    }

    private synchronized void checkAndBackup() {
        int index = this.mCurrFilePathAndName.lastIndexOf(File.separator);
        final File f = new File(this.mCurrFilePathAndName);
        String parentDir = this.mCurrFilePathAndName.substring(0, index);
        if (f.length() > 2097152) {
            String bkName = new StringBuilder(String.valueOf(parentDir)).append(File.separator).append(f.getName()).append("-").append(sFormat.format(Long.valueOf(Calendar.getInstance().getTimeInMillis()))).toString();
            Log.i("slog", "文件 " + this.mCurrFilePathAndName + " 已超出大小范围. 备份为: " + bkName);
            Log.i("slog", "备份结果: " + (f.renameTo(new File(bkName)) ? "成功" : "失败"));
            File[] fs = new File(parentDir).listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    String name = pathname.getName();
                    Log.i("slog", "检查文件: " + name);
                    if (!name.startsWith(f.getName())) {
                        return false;
                    }
                    Log.i("slog", "找到文件: " + name);
                    return true;
                }
            });
            if (fs.length <= 3) {
                Log.i("slog", "不需要删除文件，现在有: " + fs.length + " , limit : " + 3);
            } else {
                Collections.sort(Arrays.asList(fs), new C02745());
                int lenToDel = Math.max(1, fs.length - 3);
                Log.i("slog", "准备删除的文件数目是: " + lenToDel);
                for (int n = 0; n <= lenToDel; n++) {
                    Log.i("slog", "删除了文件: " + fs[n].getAbsolutePath() + " , 结果: " + fs[n].delete());
                }
            }
        }
    }

    synchronized void forceFlush(final Runnable taskToDo) {
        sWorkHandler.post(new Runnable() {
            public void run() {
                SlogFile.this.dumpToFile();
                if (taskToDo != null) {
                    taskToDo.run();
                }
            }
        });
    }

    private boolean checkIfToFile() {
        return this.mRecordQueue.size() > 1;
    }

    private synchronized void dumpToFile() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Exception block dominator not found, method:clc.utils.debug.slog.SlogFile.dumpToFile():void. bs: [B:5:0x000c, B:26:0x0084]
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.searchTryCatchDominators(ProcessTryCatchRegions.java:86)
	at jadx.core.dex.visitors.regions.ProcessTryCatchRegions.process(ProcessTryCatchRegions.java:45)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.postProcessRegions(RegionMakerVisitor.java:63)
	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:58)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:37)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:306)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler$1.run(JadxDecompiler.java:199)
*/
        /*
        r14 = this;
        monitor-enter(r14);
        r8 = 0;
        r9 = new java.io.RandomAccessFile;	 Catch:{ FileNotFoundException -> 0x0039 }
        r10 = r14.mCurrFilePathAndName;	 Catch:{ FileNotFoundException -> 0x0039 }
        r11 = "rw";	 Catch:{ FileNotFoundException -> 0x0039 }
        r9.<init>(r10, r11);	 Catch:{ FileNotFoundException -> 0x0039 }
        r8 = r9;
    L_0x000c:
        r0 = new java.lang.StringBuffer;	 Catch:{ all -> 0x00a0 }
        r0.<init>();	 Catch:{ all -> 0x00a0 }
        r10 = r14.mRecordQueue;	 Catch:{ all -> 0x00a0 }
        r10 = r10.iterator();	 Catch:{ all -> 0x00a0 }
    L_0x0017:
        r11 = r10.hasNext();	 Catch:{ all -> 0x00a0 }
        if (r11 != 0) goto L_0x00dc;	 Catch:{ all -> 0x00a0 }
    L_0x001d:
        r10 = r14.mRecordQueue;	 Catch:{ all -> 0x00a0 }
        r10.clear();	 Catch:{ all -> 0x00a0 }
        r1 = r0.toString();	 Catch:{ all -> 0x00a0 }
        r4 = r8.length();	 Catch:{ IOException -> 0x00e9 }
        r8.seek(r4);	 Catch:{ IOException -> 0x00e9 }
        r10 = r1.getBytes();	 Catch:{ IOException -> 0x00e9 }
        r8.write(r10);	 Catch:{ IOException -> 0x00e9 }
        r8.close();	 Catch:{ IOException -> 0x00e9 }
    L_0x0037:
        monitor-exit(r14);
        return;
    L_0x0039:
        r2 = move-exception;
        r10 = r14.mCurrFilePathAndName;	 Catch:{ IOException -> 0x006b }
        r11 = 0;	 Catch:{ IOException -> 0x006b }
        r12 = r14.mCurrFilePathAndName;	 Catch:{ IOException -> 0x006b }
        r13 = java.io.File.separator;	 Catch:{ IOException -> 0x006b }
        r12 = r12.lastIndexOf(r13);	 Catch:{ IOException -> 0x006b }
        r6 = r10.substring(r11, r12);	 Catch:{ IOException -> 0x006b }
        r10 = new java.io.File;	 Catch:{ IOException -> 0x006b }
        r10.<init>(r6);	 Catch:{ IOException -> 0x006b }
        r10.mkdirs();	 Catch:{ IOException -> 0x006b }
        r10 = new java.io.File;	 Catch:{ IOException -> 0x006b }
        r11 = r14.mCurrFilePathAndName;	 Catch:{ IOException -> 0x006b }
        r10.<init>(r11);	 Catch:{ IOException -> 0x006b }
        r10.createNewFile();	 Catch:{ IOException -> 0x006b }
        r9 = new java.io.RandomAccessFile;	 Catch:{ IOException -> 0x006b }
        r10 = r14.mCurrFilePathAndName;	 Catch:{ IOException -> 0x006b }
        r11 = "rw";	 Catch:{ IOException -> 0x006b }
        r9.<init>(r10, r11);	 Catch:{ IOException -> 0x006b }
        if (r9 == 0) goto L_0x00d9;
    L_0x0066:
        r9.close();	 Catch:{ IOException -> 0x00c2 }
        r8 = r9;
        goto L_0x000c;
    L_0x006b:
        r3 = move-exception;
        r10 = "slog";	 Catch:{ all -> 0x00a3 }
        r11 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a3 }
        r12 = "file write failed : ";	 Catch:{ all -> 0x00a3 }
        r11.<init>(r12);	 Catch:{ all -> 0x00a3 }
        r12 = r14.mCurrFilePathAndName;	 Catch:{ all -> 0x00a3 }
        r11 = r11.append(r12);	 Catch:{ all -> 0x00a3 }
        r11 = r11.toString();	 Catch:{ all -> 0x00a3 }
        android.util.Log.i(r10, r11, r2);	 Catch:{ all -> 0x00a3 }
        if (r8 == 0) goto L_0x0037;
    L_0x0084:
        r8.close();	 Catch:{ IOException -> 0x0088 }
        goto L_0x0037;
    L_0x0088:
        r3 = move-exception;
        r10 = "slog";	 Catch:{ all -> 0x00a0 }
        r11 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a0 }
        r12 = "file close failed : ";	 Catch:{ all -> 0x00a0 }
        r11.<init>(r12);	 Catch:{ all -> 0x00a0 }
        r12 = r14.mCurrFilePathAndName;	 Catch:{ all -> 0x00a0 }
        r11 = r11.append(r12);	 Catch:{ all -> 0x00a0 }
        r11 = r11.toString();	 Catch:{ all -> 0x00a0 }
        android.util.Log.i(r10, r11, r2);	 Catch:{ all -> 0x00a0 }
        goto L_0x0037;
    L_0x00a0:
        r10 = move-exception;
    L_0x00a1:
        monitor-exit(r14);
        throw r10;
    L_0x00a3:
        r10 = move-exception;
        if (r8 == 0) goto L_0x00a9;
    L_0x00a6:
        r8.close();	 Catch:{ IOException -> 0x00aa }
    L_0x00a9:
        throw r10;	 Catch:{ all -> 0x00a0 }
    L_0x00aa:
        r3 = move-exception;	 Catch:{ all -> 0x00a0 }
        r11 = "slog";	 Catch:{ all -> 0x00a0 }
        r12 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a0 }
        r13 = "file close failed : ";	 Catch:{ all -> 0x00a0 }
        r12.<init>(r13);	 Catch:{ all -> 0x00a0 }
        r13 = r14.mCurrFilePathAndName;	 Catch:{ all -> 0x00a0 }
        r12 = r12.append(r13);	 Catch:{ all -> 0x00a0 }
        r12 = r12.toString();	 Catch:{ all -> 0x00a0 }
        android.util.Log.i(r11, r12, r2);	 Catch:{ all -> 0x00a0 }
        goto L_0x00a9;
    L_0x00c2:
        r3 = move-exception;
        r10 = "slog";	 Catch:{ all -> 0x00ef }
        r11 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00ef }
        r12 = "file close failed : ";	 Catch:{ all -> 0x00ef }
        r11.<init>(r12);	 Catch:{ all -> 0x00ef }
        r12 = r14.mCurrFilePathAndName;	 Catch:{ all -> 0x00ef }
        r11 = r11.append(r12);	 Catch:{ all -> 0x00ef }
        r11 = r11.toString();	 Catch:{ all -> 0x00ef }
        android.util.Log.i(r10, r11, r2);	 Catch:{ all -> 0x00ef }
    L_0x00d9:
        r8 = r9;
        goto L_0x000c;
    L_0x00dc:
        r7 = r10.next();	 Catch:{ all -> 0x00a0 }
        r7 = (clc.utils.debug.slog.Record) r7;	 Catch:{ all -> 0x00a0 }
        if (r7 == 0) goto L_0x0017;	 Catch:{ all -> 0x00a0 }
    L_0x00e4:
        r14.appendToFile(r0, r7);	 Catch:{ all -> 0x00a0 }
        goto L_0x0017;	 Catch:{ all -> 0x00a0 }
    L_0x00e9:
        r2 = move-exception;	 Catch:{ all -> 0x00a0 }
        r2.printStackTrace();	 Catch:{ all -> 0x00a0 }
        goto L_0x0037;
    L_0x00ef:
        r10 = move-exception;
        r8 = r9;
        goto L_0x00a1;
        */
        throw new UnsupportedOperationException("Method not decompiled: clc.utils.debug.slog.SlogFile.dumpToFile():void");
    }

    private void appendToFile(StringBuffer buffer, Record r) {
        if (r != null) {
            buffer.append(r.timeStamp + " " + r.tag + ": => " + r.content + "\n");
        }
    }
}
