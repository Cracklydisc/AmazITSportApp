package clc.utils.debug.slog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;

@NoProguard
public final class SolidLogger {
    private static String fileName = "";
    private static boolean mute;
    private static BroadcastReceiver sFlushReceiver = new C02771();
    private static SimpleDateFormat sFormat = new SimpleDateFormat("yy.MM.dd HH:mm:ss.SSS");
    private static AtomicBoolean sInited = new AtomicBoolean(false);
    private static SolidLogger sInstance = null;
    private static SolidLoggerCallback sSolidLoggerCallback;
    private static SlogFile sWorkFile;

    class C02771 extends BroadcastReceiver {
        C02771() {
        }

        public void onReceive(final Context context, Intent intent) {
            Log.i("Slog", "Force flush Slog...");
            if (SolidLogger.mute) {
                Log.i("Slog", "Force flush Slog Refused for muting .");
            } else if (SolidLogger.sWorkFile != null) {
                SolidLogger.sWorkFile.forceFlush(new Runnable() {
                    public void run() {
                        if (context != null) {
                            context.sendBroadcast(new Intent("clc.utils.debug.slog.FORCE_FLUSH_FINISH"));
                        }
                    }
                });
            }
        }
    }

    public interface SolidLoggerCallback {
        void onNewRecord(String str);
    }

    static {
        boolean z = false;
        if (!Build.TYPE.contains("userdebug")) {
            z = true;
        }
        mute = z;
    }

    private SolidLogger() {
    }

    private static void doInit(String pkgName) {
        String path = "/sdcard/.springchannel/slog";
        if (!TextUtils.isEmpty(pkgName)) {
            path = "/sdcard/.springchannel/" + pkgName;
        }
        if (sWorkFile == null) {
            sWorkFile = new SlogFile(path);
        }
    }

    public static synchronized void withContext(Context context) {
        synchronized (SolidLogger.class) {
            withContext(context, null);
        }
    }

    public static synchronized void withContext(Context context, String fName) {
        synchronized (SolidLogger.class) {
            if (mute) {
                sInited.set(true);
            } else if (context == null) {
                throw new IllegalArgumentException("Context is null !");
            } else {
                if (TextUtils.isEmpty(fName)) {
                    fName = context.getPackageName();
                }
                fileName = fName;
                doInit(fileName);
                try {
                    context.getApplicationContext().unregisterReceiver(sFlushReceiver);
                } catch (Exception e) {
                }
                IntentFilter flt = new IntentFilter();
                flt.addAction("clc.utils.debug.slog.FORCE_FLUSH");
                context.getApplicationContext().registerReceiver(sFlushReceiver, flt);
                sInited.set(true);
            }
        }
    }

    public static SolidLogger getInstance() {
        if (sInited.get() || mute) {
            if (sInstance == null) {
                synchronized (SolidLogger.class) {
                    if (sInstance == null) {
                        sInstance = new SolidLogger();
                        if (!mute) {
                            doInit(fileName);
                        }
                    }
                }
            }
            return sInstance;
        }
        throw new IllegalArgumentException("please call withContext() firstly !");
    }

    public void with(String tag, String logMsg) {
        if (!mute) {
            with(tag, logMsg, null);
        }
    }

    public void with2(String tag, String logMsg) {
        if (!mute) {
            Log.i(tag, logMsg);
            if (sInited.get()) {
                with(tag, logMsg);
            }
        }
    }

    public void with(String tag, String logMsg, Throwable e) {
        if (!mute && sWorkFile != null) {
            String time = sFormat.format(Long.valueOf(Calendar.getInstance().getTimeInMillis()));
            Record r = new Record();
            r.timeStamp = time;
            r.tag = tag;
            r.content = logMsg;
            if (sSolidLoggerCallback != null) {
                sSolidLoggerCallback.onNewRecord(r.toString());
            }
            sWorkFile.addRecord(r);
        }
    }
}
