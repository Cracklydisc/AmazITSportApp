package clc.utils.taskmanager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import clc.utils.taskmanager.Task.RunningStatus;
import clc.utils.taskmanager.Task.Status;
import java.util.HashMap;
import java.util.LinkedList;

public class TaskManager {
    private static /* synthetic */ int[] $SWITCH_TABLE$clc$utils$taskmanager$Task$RunningStatus;
    private static HashMap<String, TaskManager> sTaskManagers = new HashMap();
    private boolean mAutoQuit;
    private Task mCurTask;
    private IStateChangeListener mListener;
    private String mName;
    private State mState;
    private LinkedList<Task> mTaskList;
    private TaskOperation mTaskOperation;
    private Handler mThreadHandler;
    private ThreadWorker mThreadWorker;
    private Handler mUIHandler;

    class C02792 implements Runnable {
        C02792() {
        }

        public void run() {
            TaskManager.this.doInBackground();
        }
    }

    class C02803 implements Runnable {
        final /* synthetic */ TaskManager this$0;
        private final /* synthetic */ Task val$runTask;

        public void run() {
            this.this$0.executeTask(this.val$runTask);
        }
    }

    public interface IStateChangeListener {
        void onStateChanged(TaskManager taskManager, State state, State state2);
    }

    public enum State {
        NEW,
        RUNNING,
        PAUSED,
        FINISHED,
        READY
    }

    public enum TaskManagerState {
        CONTINUE,
        PAUSE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$clc$utils$taskmanager$Task$RunningStatus() {
        int[] iArr = $SWITCH_TABLE$clc$utils$taskmanager$Task$RunningStatus;
        if (iArr == null) {
            iArr = new int[RunningStatus.values().length];
            try {
                iArr[RunningStatus.UI_THREAD.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[RunningStatus.WORK_THREAD.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$clc$utils$taskmanager$Task$RunningStatus = iArr;
        }
        return iArr;
    }

    public TaskManager() {
        this.mTaskList = new LinkedList();
        this.mTaskOperation = new TaskOperation();
        this.mThreadWorker = null;
        this.mCurTask = null;
        this.mState = State.NEW;
        this.mName = null;
        this.mListener = null;
        this.mThreadHandler = null;
        this.mAutoQuit = true;
        this.mUIHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        TaskManager.this.executeTask(msg.obj);
                        TaskManager.this.runNextTask();
                        return;
                    case 2:
                        TaskManager.this.postProgress(msg.obj);
                        return;
                    default:
                        return;
                }
            }
        };
    }

    public TaskManager(String name) {
        this(name, true);
    }

    public TaskManager(String name, boolean autoQuit) {
        this.mTaskList = new LinkedList();
        this.mTaskOperation = new TaskOperation();
        this.mThreadWorker = null;
        this.mCurTask = null;
        this.mState = State.NEW;
        this.mName = null;
        this.mListener = null;
        this.mThreadHandler = null;
        this.mAutoQuit = true;
        this.mUIHandler = /* anonymous class already generated */;
        this.mName = name;
        this.mAutoQuit = autoQuit;
    }

    public TaskManager next(Task task) {
        if (task != null) {
            synchronized (this.mTaskList) {
                task.setTaskId(this.mTaskList.size() + 1);
                this.mTaskList.add(task);
            }
            return this;
        }
        throw new NullPointerException("task is null");
    }

    public void execute() {
        if (this.mTaskList.size() > 0) {
            startThread();
            setState(State.RUNNING);
            this.mThreadHandler.post(new C02792());
        } else if (this.mAutoQuit) {
            quitLooper();
        } else {
            setState(State.READY);
        }
    }

    public void execute(TaskOperation operation) {
        if (operation != null) {
            this.mTaskOperation = operation;
        }
        execute();
    }

    public void removeTask(Task task) {
        synchronized (this.mTaskList) {
            this.mTaskList.remove(task);
            if (this.mTaskList.isEmpty()) {
                quitLooper();
            }
        }
    }

    public String getName() {
        return this.mName;
    }

    public void quitLooper() {
        if (this.mThreadWorker != null) {
            this.mThreadWorker.quit();
            this.mThreadWorker = null;
        }
        this.mThreadHandler = null;
        setState(State.FINISHED);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name = ").append(this.mName).append("  ");
        sb.append("State = ").append(this.mState).append("  ");
        sb.append(super.toString());
        return sb.toString();
    }

    protected void printExecuteTaskState(Task task) {
    }

    private void setState(State state) {
        State oldState = this.mState;
        State newState = state;
        this.mState = state;
        if (this.mState == State.FINISHED) {
            popTaskManager(this);
        } else {
            pushTaskManager(this);
        }
        if (oldState != newState) {
            printTaskManagerState(oldState, newState);
            performStateChange(oldState, newState);
        }
    }

    private void startThread() {
        if (this.mThreadWorker == null) {
            this.mThreadWorker = new ThreadWorker("TaskManager_Thread_" + (TextUtils.isEmpty(this.mName) ? toString() : this.mName));
            this.mThreadHandler = new Handler(this.mThreadWorker.getLooper());
            setState(State.READY);
        }
    }

    private void doInBackground() {
        this.mCurTask = null;
        if (!this.mTaskList.isEmpty()) {
            Task task = (Task) this.mTaskList.get(0);
            this.mCurTask = task;
            synchronized (this.mTaskList) {
                this.mTaskList.remove(0);
            }
            switch ($SWITCH_TABLE$clc$utils$taskmanager$Task$RunningStatus()[task.getRunningStatus().ordinal()]) {
                case 1:
                    executeTask(task);
                    runNextTask();
                    return;
                case 2:
                    this.mUIHandler.obtainMessage(1, task).sendToTarget();
                    return;
                default:
                    return;
            }
        }
    }

    private void runNextTask() {
        if (isRunNext()) {
            execute();
        }
    }

    private boolean isRunNext() {
        boolean isRunNext = true;
        boolean hasNext = false;
        if (this.mTaskOperation != null) {
            isRunNext = this.mTaskOperation.getTaskManagerStatus() == TaskManagerState.CONTINUE;
        }
        if (this.mTaskList != null) {
            if (this.mTaskList.size() > 0) {
                hasNext = true;
            } else {
                hasNext = false;
            }
        }
        if (!hasNext) {
            if (this.mAutoQuit) {
                quitLooper();
            } else {
                setState(State.READY);
            }
        }
        if (isRunNext && hasNext) {
            return true;
        }
        return false;
    }

    private void executeTask(Task task) {
        if (task != null) {
            task.setStatus(Status.RUNNING);
            printExecuteTaskState(task);
            try {
                this.mTaskOperation = task.onExecute(this.mTaskOperation);
            } catch (Exception e) {
                e.printStackTrace();
            }
            task.setStatus(Status.FINISHED);
        }
    }

    private void postProgress(Object progresses) {
        if (this.mCurTask != null) {
            this.mCurTask.onProgressUpdate(progresses);
        }
    }

    private void performStateChange(final State oldState, final State newState) {
        if (this.mListener != null) {
            this.mUIHandler.post(new Runnable() {
                public void run() {
                    TaskManager.this.mListener.onStateChanged(TaskManager.this, oldState, newState);
                }
            });
        }
    }

    private void printTaskManagerState(State oldState, State newState) {
    }

    private static void pushTaskManager(TaskManager taskManager) {
        if (taskManager != null) {
            String name = taskManager.getName();
            if (!TextUtils.isEmpty(name)) {
                sTaskManagers.put(name, taskManager);
            }
        }
    }

    private static void popTaskManager(TaskManager taskManager) {
        if (taskManager != null) {
            sTaskManagers.remove(taskManager.getName());
        }
    }
}
