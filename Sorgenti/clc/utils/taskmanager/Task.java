package clc.utils.taskmanager;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Task {
    private AtomicBoolean mCancelled;
    private int mId;
    private String mName;
    private volatile RunningStatus mRunStatus;
    private volatile Status mStatus;

    public enum RunningStatus {
        WORK_THREAD,
        UI_THREAD
    }

    public enum Status {
        PENDING,
        RUNNING,
        FINISHED
    }

    public abstract TaskOperation onExecute(TaskOperation taskOperation);

    public Task(RunningStatus status) {
        this(status, null);
    }

    public Task(RunningStatus status, String name) {
        this.mId = 0;
        this.mName = null;
        this.mCancelled = new AtomicBoolean(false);
        this.mStatus = Status.PENDING;
        this.mRunStatus = RunningStatus.UI_THREAD;
        this.mRunStatus = status;
        this.mName = name;
    }

    public void onProgressUpdate(Object progresses) {
    }

    public RunningStatus getRunningStatus() {
        return this.mRunStatus;
    }

    public void setStatus(Status status) {
        this.mStatus = status;
    }

    public void setTaskId(int id) {
        this.mId = id;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name = ").append(this.mName).append("  ");
        sb.append("id = ").append(this.mId).append("  ");
        sb.append(super.toString());
        return sb.toString();
    }
}
