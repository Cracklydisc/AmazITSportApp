package clc.utils.taskmanager;

import android.os.Looper;
import android.os.Process;

public final class ThreadWorker implements Runnable {
    private final Object mLockObj = new Object();
    private Looper mLooper = null;

    public ThreadWorker(String name) {
        Thread t = new Thread(null, this, name);
        t.setPriority(1);
        t.start();
        synchronized (this.mLockObj) {
            while (this.mLooper == null) {
                try {
                    this.mLockObj.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public Looper getLooper() {
        return this.mLooper;
    }

    public void run() {
        synchronized (this.mLockObj) {
            Process.setThreadPriority(10);
            Looper.prepare();
            this.mLooper = Looper.myLooper();
            this.mLockObj.notifyAll();
        }
        Looper.loop();
    }

    public void quit() {
        this.mLooper.quit();
    }
}
