package clc.utils.taskmanager;

import clc.utils.taskmanager.TaskManager.TaskManagerState;
import java.util.ArrayList;

public class TaskOperation {
    private Object[] mNextTaskParams = null;
    private TaskManagerState mTaskManagerStatus = TaskManagerState.CONTINUE;

    public TaskOperation(Object[] nextTaskParams) {
        this.mNextTaskParams = nextTaskParams;
    }

    public Object[] getTaskParams() {
        return this.mNextTaskParams;
    }

    public void setTaskParamsEmpty() {
        this.mNextTaskParams = null;
    }

    public void setTaskParams(TaskOperation operation) {
        int i = 0;
        if (operation == this) {
            throw new IllegalArgumentException("The argument can NOT be self.");
        } else if (operation != null) {
            Object[] params = operation.getTaskParams();
            if (params != null) {
                ArrayList<Object> paramsList = new ArrayList();
                if (this.mNextTaskParams != null) {
                    for (Object param : this.mNextTaskParams) {
                        paramsList.add(param);
                    }
                }
                int length = params.length;
                while (i < length) {
                    paramsList.add(params[i]);
                    i++;
                }
                this.mNextTaskParams = paramsList.toArray();
            }
        }
    }

    public TaskManagerState getTaskManagerStatus() {
        return this.mTaskManagerStatus;
    }

    public void appendTaskParam(Object param) {
        appendTaskParams(new Object[]{param});
    }

    public void appendTaskParams(Object[] params) {
        if (params != null) {
            setTaskParams(new TaskOperation(params));
        }
    }
}
