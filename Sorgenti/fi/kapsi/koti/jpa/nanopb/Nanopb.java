package fi.kapsi.koti.jpa.nanopb;

import com.google.protobuf.DescriptorProtos.EnumOptions;
import com.google.protobuf.DescriptorProtos.FieldOptions;
import com.google.protobuf.DescriptorProtos.FileOptions;
import com.google.protobuf.DescriptorProtos.MessageOptions;
import com.google.protobuf.nano.CodedInputByteBufferNano;
import com.google.protobuf.nano.CodedOutputByteBufferNano;
import com.google.protobuf.nano.ExtendableMessageNano;
import com.google.protobuf.nano.Extension;
import java.io.IOException;

public interface Nanopb {
    public static final Extension<FieldOptions, NanoPBOptions> nanopb = Extension.createMessageTyped(11, NanoPBOptions.class, 8082);
    public static final Extension<EnumOptions, NanoPBOptions> nanopbEnumopt = Extension.createMessageTyped(11, NanoPBOptions.class, 8082);
    public static final Extension<FileOptions, NanoPBOptions> nanopbFileopt = Extension.createMessageTyped(11, NanoPBOptions.class, 8082);
    public static final Extension<MessageOptions, NanoPBOptions> nanopbMsgopt = Extension.createMessageTyped(11, NanoPBOptions.class, 8082);

    public static final class NanoPBOptions extends ExtendableMessageNano<NanoPBOptions> {
        private int bitField0_;
        private boolean longNames_;
        private int maxCount_;
        private int maxSize_;
        private boolean packedStruct_;
        private int type_;

        public NanoPBOptions() {
            clear();
        }

        public NanoPBOptions clear() {
            this.bitField0_ = 0;
            this.maxSize_ = 0;
            this.maxCount_ = 0;
            this.type_ = 0;
            this.longNames_ = true;
            this.packedStruct_ = false;
            this.unknownFieldData = null;
            this.cachedSize = -1;
            return this;
        }

        public void writeTo(CodedOutputByteBufferNano output) throws IOException {
            if ((this.bitField0_ & 1) != 0) {
                output.writeInt32(1, this.maxSize_);
            }
            if ((this.bitField0_ & 2) != 0) {
                output.writeInt32(2, this.maxCount_);
            }
            if ((this.bitField0_ & 4) != 0) {
                output.writeInt32(3, this.type_);
            }
            if ((this.bitField0_ & 8) != 0) {
                output.writeBool(4, this.longNames_);
            }
            if ((this.bitField0_ & 16) != 0) {
                output.writeBool(5, this.packedStruct_);
            }
            super.writeTo(output);
        }

        protected int computeSerializedSize() {
            int size = super.computeSerializedSize();
            if ((this.bitField0_ & 1) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(1, this.maxSize_);
            }
            if ((this.bitField0_ & 2) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(2, this.maxCount_);
            }
            if ((this.bitField0_ & 4) != 0) {
                size += CodedOutputByteBufferNano.computeInt32Size(3, this.type_);
            }
            if ((this.bitField0_ & 8) != 0) {
                size += CodedOutputByteBufferNano.computeBoolSize(4, this.longNames_);
            }
            if ((this.bitField0_ & 16) != 0) {
                return size + CodedOutputByteBufferNano.computeBoolSize(5, this.packedStruct_);
            }
            return size;
        }

        public NanoPBOptions mergeFrom(CodedInputByteBufferNano input) throws IOException {
            while (true) {
                int tag = input.readTag();
                switch (tag) {
                    case 0:
                        break;
                    case 8:
                        this.maxSize_ = input.readInt32();
                        this.bitField0_ |= 1;
                        continue;
                    case 16:
                        this.maxCount_ = input.readInt32();
                        this.bitField0_ |= 2;
                        continue;
                    case 24:
                        int value = input.readInt32();
                        switch (value) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.type_ = value;
                                this.bitField0_ |= 4;
                                break;
                            default:
                                continue;
                        }
                    case 32:
                        this.longNames_ = input.readBool();
                        this.bitField0_ |= 8;
                        continue;
                    case 40:
                        this.packedStruct_ = input.readBool();
                        this.bitField0_ |= 16;
                        continue;
                    default:
                        if (!storeUnknownField(input, tag)) {
                            break;
                        }
                        continue;
                }
                return this;
            }
        }
    }
}
