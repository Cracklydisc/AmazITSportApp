package de.greenrobot.event.util;

public interface HasExecutionScope {
    void setExecutionScope(Object obj);
}
