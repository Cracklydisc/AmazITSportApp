package de.greenrobot.event.util;

import android.util.Log;
import de.greenrobot.event.EventBus;
import java.lang.reflect.Constructor;

public class AsyncExecutor {
    private final EventBus eventBus;
    private final Constructor<?> failureEventConstructor;
    private final Object scope;

    class C12681 implements Runnable {
        final /* synthetic */ AsyncExecutor this$0;
        private final /* synthetic */ RunnableEx val$runnable;

        public void run() {
            try {
                this.val$runnable.run();
            } catch (Exception e) {
                try {
                    Object event = this.this$0.failureEventConstructor.newInstance(new Object[]{e});
                    if (event instanceof HasExecutionScope) {
                        ((HasExecutionScope) event).setExecutionScope(this.this$0.scope);
                    }
                    this.this$0.eventBus.post(event);
                } catch (Exception e1) {
                    Log.e(EventBus.TAG, "Original exception:", e);
                    throw new RuntimeException("Could not create failure event", e1);
                }
            }
        }
    }

    public static class Builder {
        private Builder() {
        }
    }

    public interface RunnableEx {
        void run() throws Exception;
    }
}
