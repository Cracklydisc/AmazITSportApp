package de.greenrobot.event.util;

import de.greenrobot.event.EventBus;

public class ErrorDialogConfig {
    EventBus eventBus;

    EventBus getEventBus() {
        return this.eventBus != null ? this.eventBus : EventBus.getDefault();
    }
}
