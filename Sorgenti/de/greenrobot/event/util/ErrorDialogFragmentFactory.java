package de.greenrobot.event.util;

import android.annotation.TargetApi;
import android.app.Fragment;

public abstract class ErrorDialogFragmentFactory<T> {
    protected final ErrorDialogConfig config;

    @TargetApi(11)
    public static class Honeycomb extends ErrorDialogFragmentFactory<Fragment> {
    }

    public static class Support extends ErrorDialogFragmentFactory<android.support.v4.app.Fragment> {
    }
}
