package de.greenrobot.event.util;

public class ThrowableFailureEvent implements HasExecutionScope {
    private Object executionContext;

    public void setExecutionScope(Object executionContext) {
        this.executionContext = executionContext;
    }
}
