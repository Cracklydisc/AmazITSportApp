package de.greenrobot.event;

import android.util.Log;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class SubscriberMethodFinder {
    private static final Map<Class<?>, List<SubscriberMethod>> methodCache = new HashMap();
    private final Map<Class<?>, Class<?>> skipMethodVerificationForClasses = new ConcurrentHashMap();

    SubscriberMethodFinder(List<Class<?>> skipMethodVerificationForClassesList) {
        if (skipMethodVerificationForClassesList != null) {
            for (Class<?> clazz : skipMethodVerificationForClassesList) {
                this.skipMethodVerificationForClasses.put(clazz, clazz);
            }
        }
    }

    List<SubscriberMethod> findSubscriberMethods(Class<?> subscriberClass) {
        synchronized (methodCache) {
            List<SubscriberMethod> subscriberMethods = (List) methodCache.get(subscriberClass);
        }
        if (subscriberMethods != null) {
            return subscriberMethods;
        }
        subscriberMethods = new ArrayList();
        Class<?> clazz = subscriberClass;
        HashMap<String, Class> eventTypesFound = new HashMap();
        StringBuilder methodKeyBuilder = new StringBuilder();
        while (clazz != null) {
            String name = clazz.getName();
            if (name.startsWith("java.") || name.startsWith("javax.") || name.startsWith("android.")) {
                break;
            }
            try {
                filterSubscriberMethods(subscriberMethods, eventTypesFound, methodKeyBuilder, clazz.getDeclaredMethods());
                clazz = clazz.getSuperclass();
            } catch (Throwable th) {
                Method[] methods = subscriberClass.getMethods();
                subscriberMethods.clear();
                eventTypesFound.clear();
                filterSubscriberMethods(subscriberMethods, eventTypesFound, methodKeyBuilder, methods);
            }
        }
        if (subscriberMethods.isEmpty()) {
            throw new EventBusException("Subscriber " + subscriberClass + " has no public methods called " + "onEvent");
        }
        synchronized (methodCache) {
            methodCache.put(subscriberClass, subscriberMethods);
        }
        return subscriberMethods;
    }

    private void filterSubscriberMethods(List<SubscriberMethod> subscriberMethods, HashMap<String, Class> eventTypesFound, StringBuilder methodKeyBuilder, Method[] methods) {
        for (Method method : methods) {
            String methodName = method.getName();
            if (methodName.startsWith("onEvent")) {
                int modifiers = method.getModifiers();
                Class<?> methodClass = method.getDeclaringClass();
                if ((modifiers & 1) != 0 && (modifiers & 5192) == 0) {
                    Class[] parameterTypes = method.getParameterTypes();
                    if (parameterTypes.length == 1) {
                        ThreadMode threadMode = getThreadMode(methodClass, method, methodName);
                        if (threadMode != null) {
                            Class<?> eventType = parameterTypes[0];
                            methodKeyBuilder.setLength(0);
                            methodKeyBuilder.append(methodName);
                            methodKeyBuilder.append('>').append(eventType.getName());
                            String methodKey = methodKeyBuilder.toString();
                            Class methodClassOld = (Class) eventTypesFound.put(methodKey, methodClass);
                            if (methodClassOld == null || methodClassOld.isAssignableFrom(methodClass)) {
                                subscriberMethods.add(new SubscriberMethod(method, threadMode, eventType));
                            } else {
                                eventTypesFound.put(methodKey, methodClassOld);
                            }
                        }
                    }
                } else if (!this.skipMethodVerificationForClasses.containsKey(methodClass)) {
                    Log.d(EventBus.TAG, "Skipping method (not public, static or abstract): " + methodClass + "." + methodName);
                }
            }
        }
    }

    private ThreadMode getThreadMode(Class<?> clazz, Method method, String methodName) {
        String modifierString = methodName.substring("onEvent".length());
        if (modifierString.length() == 0) {
            return ThreadMode.PostThread;
        }
        if (modifierString.equals("MainThread")) {
            return ThreadMode.MainThread;
        }
        if (modifierString.equals("BackgroundThread")) {
            return ThreadMode.BackgroundThread;
        }
        if (modifierString.equals("Async")) {
            return ThreadMode.Async;
        }
        if (this.skipMethodVerificationForClasses.containsKey(clazz)) {
            return null;
        }
        throw new EventBusException("Illegal onEvent method, check for typos: " + method);
    }
}
